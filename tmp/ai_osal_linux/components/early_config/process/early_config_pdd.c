/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        early_config_pdd.c
 *
 * This file contains the functions for the access tp PDD
 * 
 * global function:
 * -- s32EarlyConfig_PddReadDisplayConfig():
 *      read display configuration from PDD       
 * -- s32EarlyConfig_PddWriteDisplayConfig():
 *      write datastream display configuration to PDD
 * -- s32EarlyConfig_PddReadDriverName():
 *      read the driver name and the configuration
 *           
 * local function:
 * -- vEarlyConfig_PddInitConfigDisplay():
 *      set display configuration to init state
 * -- vEarlyConfig_PddGetStrMode():
 *      get the string for the mode from value
 * -- s32EarlyConfig_PddCreateDatastream():
 *       create the datastream for an element
 * -- s32EarlyConfig_PddCalcHash():    
 *       calc hash value for the element
 * -- s32EarlyConfig_PddGetResolution():
 *      get value for resolution from string
 * -- s32EarlyConfig_PddCheckCreateDisplayStr():
 *      check and create display string. 
 *      If not "display" in string, then old GM string.
 *      create the New-GM string "displayGM_XXXX"
 * -- s32EarlyConfig_PddCreateGMResolutionStr():
 *      if GM string create resolution string 
 *      
 * @date        2014-07-07
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include "system_types.h"
#include "system_definition.h"
#include "pdd.h"
#include "pdd_config_nor_user.h"
#include "early_config_private.h"

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
//Display
#define EARLY_CONFIG_DISPLAY_ELEMENT_RESOLUTION        "TrResolution"
#define EARLY_CONFIG_DISPLAY_ELEMENT_BC_MODE           "TrBackwardComp"
#define EARLY_CONFIG_DISPLAY_ELEMENT_LF_MODE           "TrLowFrequency"
#define EARLY_CONFIG_DISPLAY_ELEMENT_TIMING_LVDS       "TrTimingLVDS"
#define EARLY_CONFIG_DISPLAY_ELEMENT_CLOCK_EDGE_SELECT "TrClockEdgeSelectLVDS"
#define EARLY_CONFIG_DISPLAY_ELEMENT_MAX_SIZE     64
//RTC
#define EARLY_CONFIG_DRIVER_RTC_NAME              "TrRtcDriverName"
//Touch
#define EARLY_CONFIG_DRIVER_TOUCH_NAME            "TrTouchDriverName"
#define EARLY_CONFIG_FILE_TOUCH_NAME              "TrTouchConfigFileName"
//CSC   
#ifdef PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGCSC
#define EARLY_CONFIG_CSC_CURRENT_PROFILE          "TrCurrentProfile"
#define EARLY_CONFIG_CSC_HUE                      "TrHueValue"
#define EARLY_CONFIG_CSC_CONTRAST                 "TrContrastValue"
#define EARLY_CONFIG_CSC_SATURATION               "TrSaturationValue"
#define EARLY_CONFIG_CSC_BRIGHTNESS               "TrBrightnessValue"
#define EARLY_CONFIG_CSC_HUE_OFFSET               "TrHueOffsetValue"
#define EARLY_CONFIG_CSC_SATURATION_OFFSET        "TrSaturationOffsetValue"
#define EARLY_CONFIG_CSC_BRIGHTNESS_OFFSET        "TrBrightnessOffsetValue"
#define EARLY_CONFIG_CSC_GAMMA_FACTOR             "TrGammaFactor"
#endif
#define EARLY_CONFIG_VERSION_ELEMENT                     0x00
#define EARLY_CONFIG_VERSION_STREAM                      1

#define EARLY_CONFIG_MODE_INVALID                        -5

#define EARLY_CONFIG_MAX_SIZE_DATASTREAM                 200

//macro
#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE  
#define M_EARLY_CONFIG_ANALYSE_TRRESOUTION(Resolution) s32EarlyConfig_PddGetResolution(Resolution)
#else
#define M_EARLY_CONFIG_ANALYSE_TRRESOUTION(Resolution) s32EarlyConfig_PddCheckCreateDisplayStr(Resolution)
#endif

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/


/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/                            

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static void vEarlyConfig_PddInitConfigDisplay(tsEarlyConfig_PddDisplay *PpsEarlyConfigDisplay);
static void vEarlyConfig_PddGetStrMode(tS32 Ps32Mode,char** PcpMode);
static tS32 s32EarlyConfig_PddCreateDatastream(tString PstrElementName,tU8* Pu8vBufStream,void* PpvBufData,tS32 Vs32SizeData);
static tS32 s32EarlyConfig_PddCalcHash(tString PstrElementName); 
#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
static tS32 s32EarlyConfig_PddGetResolution(tsEarlyConfig_PddDisplayResolution* PtsResolution);
#else
static tS32 s32EarlyConfig_PddCheckCreateDisplayStr(tsEarlyConfig_PddDisplayResolution* PtsResolution);
static tS32 s32EarlyConfig_PddCreateGMResolutionStr(tsEarlyConfig_PddDisplayResolution* PtsResolution);
#endif

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddReadDisplayConfig()
*
* DESCRIPTION: read display configuration from PDD
*
* PARAMETERS:
*   PpsEarlyConfigDisplay: pointer to structure of display configuration 
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2014 06 18
*****************************************************************************/
tS32 s32EarlyConfig_PddReadDisplayConfig(tsEarlyConfig_PddDisplay *PpsEarlyConfigDisplay)
{
  tS32 Vs32ReturnCode=0;
  /*init variable for display config*/
  vEarlyConfig_PddInitConfigDisplay(PpsEarlyConfigDisplay);

#ifndef PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGDISPLAY
  Vs32ReturnCode=-ENOENT;
  fprintf(stderr, "early_config_err:%d pool 'EarlyConfigDisplay' isn't defined\n",Vs32ReturnCode);
#else
  /*allocate read buffer*/
  tU8 *Vu8pBuffer=malloc(EARLY_CONFIG_MAX_LENGHT_DATASTREAM);
  if(Vu8pBuffer==NULL)
  {
    Vs32ReturnCode-=ENOMEM;
    fprintf(stderr, "early_config_err:%d malloc()\n",Vs32ReturnCode);
  }
  else
  { /*read datastream*/
	  tU8  Vu8Info=0;
    Vs32ReturnCode=pdd_read_datastream_early_from_nor(PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGDISPLAY,Vu8pBuffer,EARLY_CONFIG_MAX_LENGHT_DATASTREAM,EARLY_CONFIG_VERSION_STREAM,&Vu8Info);
	  if(Vs32ReturnCode<PDD_OK)
	  {
	    fprintf(stderr, "early_config_err:%d pdd_read_datastream_early_from_nor()\n",Vs32ReturnCode);
	  }
	  else
	  {
	    M_DEBUG_STR_VALUE("early_config: pdd_read_datastream_early_from_nor() success size stream:%d\n",Vs32ReturnCode);	 
	    /* ----- get element for TrResolution ----*/
	    if(pdd_helper_get_element_from_stream(EARLY_CONFIG_DISPLAY_ELEMENT_RESOLUTION,Vu8pBuffer,(size_t)Vs32ReturnCode,
	                                          (void*) &PpsEarlyConfigDisplay->tResolution.cResolution[0],EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT,EARLY_CONFIG_VERSION_ELEMENT)==0)
	    { /*read string success*/
		    M_DEBUG_STR_STR("early_config: TrResolution   : %s\n",PpsEarlyConfigDisplay->tResolution.cResolution);
	      /*analyse resolution mode fom string*/
	      if(M_EARLY_CONFIG_ANALYSE_TRRESOUTION(&PpsEarlyConfigDisplay->tResolution)==0)
		    {
          PpsEarlyConfigDisplay->tResolution.bReadInfo=TRUE;
        }
		    else
		    { 
		      fprintf(stderr, "early_config_err: get element 'TrResolution' fails\n");
		    }       
      }
	    else
	    { /*resolution string is needed*/	     
	      fprintf(stderr, "early_config_err: get element 'TrResolution' fails\n");
      }
      /* ----  get element for TrBackwardComp ---- */
	    tS32 Vs32Mode;
	    if(pdd_helper_get_element_from_stream(EARLY_CONFIG_DISPLAY_ELEMENT_BC_MODE,Vu8pBuffer,(size_t)Vs32ReturnCode,
	  	                                      (void*) &Vs32Mode,sizeof(tS32),EARLY_CONFIG_VERSION_ELEMENT)==0)
	    {/*read string success*/   
	      PpsEarlyConfigDisplay->tBCMode.cpBCMode=NULL;
		    vEarlyConfig_PddGetStrMode(Vs32Mode,(char**)&PpsEarlyConfigDisplay->tBCMode.cpBCMode);
		    if(PpsEarlyConfigDisplay->tBCMode.cpBCMode!=NULL)
		    {
		      PpsEarlyConfigDisplay->tBCMode.bReadInfo=TRUE;
		      M_DEBUG_STR_VALUE("early_config: TrBackwardComp: %s",PpsEarlyConfigDisplay->tBCMode.cpBCMode);
        }		 
      }
	    if(PpsEarlyConfigDisplay->tBCMode.bReadInfo!=TRUE)
	    { 
	      fprintf(stderr, "early_config: get element 'TrBackwardComp' fails\n");
      }
	    /* ----  get element for TrLowFrequency ---- */
	    if(pdd_helper_get_element_from_stream(EARLY_CONFIG_DISPLAY_ELEMENT_LF_MODE,Vu8pBuffer,(size_t)Vs32ReturnCode,
	  	                                      (void*) &Vs32Mode,sizeof(tS32),EARLY_CONFIG_VERSION_ELEMENT)==0)
	    {/*read string success*/
	      PpsEarlyConfigDisplay->tLFMode.cpLFMode=NULL;
		    vEarlyConfig_PddGetStrMode(Vs32Mode,(char**)&PpsEarlyConfigDisplay->tLFMode.cpLFMode);
		    if( PpsEarlyConfigDisplay->tLFMode.cpLFMode!=NULL)
		    {
          PpsEarlyConfigDisplay->tLFMode.bReadInfo=TRUE;
			    M_DEBUG_STR_VALUE("early_config: TrLowFrequency: %s",PpsEarlyConfigDisplay->tLFMode.cpLFMode);
        }
      }
	    if( PpsEarlyConfigDisplay->tLFMode.bReadInfo!=TRUE)
	    { 
	      fprintf(stderr, "early_config: get element 'TrLowFrequency' fails\n");
	    }
    }
    free(Vu8pBuffer);
  }
  #endif
  return(Vs32ReturnCode);
}

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddReadDisplayConfigs()
*
* DESCRIPTION: read display configuration from PDD
*
* PARAMETERS:
*   PpsEarlyConfigDisplay: pointer to structure of display configuration 
*   Pu8Number:             number of configurations
*   PpcPoolName:           pool name
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2014 06 18
*****************************************************************************/
tS32 s32EarlyConfig_PddReadDisplayConfigs(tsEarlyConfig_PddDisplaysMore *PpsEarlyConfigDisplay,tU8 Pu8Number,char* PpcPoolName)
{
  tS32 Vs32ReturnCode=0;

  /*allocate read buffer*/
  tU8 *Vu8pBuffer=malloc(EARLY_CONFIG_MAX_LENGHT_DATASTREAM);
  if(Vu8pBuffer==NULL)
  {
    Vs32ReturnCode-=ENOMEM;
    fprintf(stderr, "early_config_err:%d malloc()\n",Vs32ReturnCode);
  }
  else
  { /*read datastream*/
	  tU8  Vu8Info=0;
    Vs32ReturnCode=pdd_read_datastream_early_from_nor(PpcPoolName,Vu8pBuffer,EARLY_CONFIG_MAX_LENGHT_DATASTREAM,EARLY_CONFIG_VERSION_STREAM,&Vu8Info);
	  if(Vs32ReturnCode<PDD_OK)
	  {
	    fprintf(stderr, "early_config_err:%d pdd_read_datastream_early_from_nor()\n",Vs32ReturnCode);
	  }
	  else
	  {
	    M_DEBUG_STR_VALUE("early_config: pdd_read_datastream_early_from_nor() success size stream:%d\n",Vs32ReturnCode);
      tU8  VubInc;
      char VcElementNameTiming[EARLY_CONFIG_DISPLAY_ELEMENT_MAX_SIZE];
      char VcElementNameClockEdgeSelect[EARLY_CONFIG_DISPLAY_ELEMENT_MAX_SIZE];
      tsEarlyConfig_PddDisplayResolution *VpsEarlyConfigDisplay;
      tsEarlyConfig_PddDisplayClockEdgeSelect *VpsEarlyConfigClockEdgeSelect;
      for(VubInc=1;VubInc<Pu8Number+1;VubInc++)
      { /*create element name*/
        VpsEarlyConfigDisplay=&PpsEarlyConfigDisplay->tResolution;
        VpsEarlyConfigClockEdgeSelect=&PpsEarlyConfigDisplay->tClockEdgeSelect;
        snprintf(VcElementNameTiming,EARLY_CONFIG_DISPLAY_ELEMENT_MAX_SIZE,"%s%d",EARLY_CONFIG_DISPLAY_ELEMENT_TIMING_LVDS,VubInc);     
        snprintf(VcElementNameClockEdgeSelect,EARLY_CONFIG_DISPLAY_ELEMENT_MAX_SIZE,"%s%d",EARLY_CONFIG_DISPLAY_ELEMENT_CLOCK_EDGE_SELECT,VubInc);     
        /* ----- get element for TrTimingLVDS ----*/
	      if(pdd_helper_get_element_from_stream(VcElementNameTiming,Vu8pBuffer,(size_t)Vs32ReturnCode,(void*) VpsEarlyConfigDisplay->cResolution,
                                              EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT,EARLY_CONFIG_VERSION_ELEMENT)==0)
	      { /*read string success*/
          VpsEarlyConfigDisplay->bReadInfo=TRUE;
        }
        /* ----- get element for TrClockEdgeSelect ----*/
        if(pdd_helper_get_element_from_stream(VcElementNameClockEdgeSelect,Vu8pBuffer,(size_t)Vs32ReturnCode,
	  	                                      (void*)&VpsEarlyConfigClockEdgeSelect->s32ClockEdgeSelect,sizeof(tS32),EARLY_CONFIG_VERSION_ELEMENT)==0)
	      {/*read  success*/   
	        VpsEarlyConfigClockEdgeSelect->cpClockEdgeSelect=NULL;
		      vEarlyConfig_PddGetStrMode(VpsEarlyConfigClockEdgeSelect->s32ClockEdgeSelect,(char**)&VpsEarlyConfigClockEdgeSelect->cpClockEdgeSelect);
		      if(VpsEarlyConfigClockEdgeSelect->cpClockEdgeSelect!=NULL)
		      {
		        VpsEarlyConfigClockEdgeSelect->bReadInfo=TRUE;
          }		 
        }      
        PpsEarlyConfigDisplay++;
      }
    }
  }
  free(Vu8pBuffer);
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddReadDriverName()
*
* DESCRIPTION: read the driver name for the kind of driver given with the string 
*              PcDriver. If the pool not exist/defined thuis function reeturns an error
*
* PARAMETERS:  PcDriver: kind of driver
*              PpcDriverName: driver name read from PDD
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2017 28 7
*****************************************************************************/
tS32 s32EarlyConfig_PddReadDriverName(char *PcDriver,char* PpcDriverName,char* PpcConfigFileName)
{
  tS32   Vs32ReturnCode=-ENOENT; //No such file or directory.
  char*  VcPoolName=NULL;
  char*  VcElementName=NULL;
  char*  VcElementNameConfig=NULL;
  // get pool name and driver
  if(strncmp("Rtc",PcDriver,strlen(PcDriver))==0)
  {
    #ifdef PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGRTCDRIVER
    VcPoolName=PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGRTCDRIVER;
    VcElementName=EARLY_CONFIG_DRIVER_RTC_NAME;
    #endif   
  } 
  if(strncmp("Touch",PcDriver,strlen(PcDriver))==0)
  {
    #ifdef PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGTOUCHDRIVER
	  VcPoolName=PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGTOUCHDRIVER;
    VcElementName=EARLY_CONFIG_DRIVER_TOUCH_NAME;
	  VcElementNameConfig=EARLY_CONFIG_FILE_TOUCH_NAME;
    #endif
  }
  if((VcPoolName!=NULL)&&(VcElementName!=NULL))
  { /*allocate read buffer*/
    tU8 *Vu8pBuffer=malloc(EARLY_CONFIG_MAX_LENGHT_DATASTREAM);
	  if(Vu8pBuffer==NULL)
	  {
	    Vs32ReturnCode-=ENOMEM;
      fprintf(stderr, "early_config_err:%d malloc()\n",Vs32ReturnCode);
	  }
	  else
	  { /*read datastream*/
      tU8  Vu8Info=0;
      Vs32ReturnCode=pdd_read_datastream_early_from_nor(VcPoolName,Vu8pBuffer,EARLY_CONFIG_MAX_LENGHT_DATASTREAM,EARLY_CONFIG_VERSION_STREAM,&Vu8Info);
	    if(Vs32ReturnCode<PDD_OK)
	    {
	      fprintf(stderr, "early_config_err:%d pdd_read_datastream_early_from_nor()\n",Vs32ReturnCode);
	    }
	    else
	    { //get element from stream
	      M_DEBUG_STR_VALUE("early_config: pdd_read_datastream_early_from_nor() success size stream:%d\n",Vs32ReturnCode);	 
	      /* ----- get element ----*/
		    size_t VSize=(size_t)Vs32ReturnCode;
	      Vs32ReturnCode=pdd_helper_get_element_from_stream(VcElementName,Vu8pBuffer,VSize,(void*) PpcDriverName,
			                                                EARLY_CONFIG_DRIVER_NAME_STRING_LENGHT,EARLY_CONFIG_VERSION_ELEMENT);
		    //get configuration
		    if((VcElementNameConfig!=NULL)&&(Vs32ReturnCode==0))
		    {
		      Vs32ReturnCode=pdd_helper_get_element_from_stream(VcElementNameConfig,Vu8pBuffer,VSize,(void*) PpcConfigFileName,
			                                                   EARLY_CONFIG_DRIVER_NAME_STRING_LENGHT,EARLY_CONFIG_VERSION_ELEMENT);
		    }
      }	  
		  free(Vu8pBuffer);
	  }
  }
  return(Vs32ReturnCode);
}
#ifdef EARYL_CONFIG_USE_CSC_LIB
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddReadCSC()
*
* DESCRIPTION: read the values for CSC  
*
* PARAMETERS:  PtsEarlyConfigCSC: pointer for the elements of the CSC
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2017 28 7
*****************************************************************************/
tS32 s32EarlyConfig_PddReadCSC(tsEarlyConfig_CSC* PtsEarlyConfigCSC)
{
  tS32   Vs32ReturnCode=-ENOENT; //No such file or directory.
  /*allocate read buffer*/
  tU8 *Vu8pBuffer=malloc(EARLY_CONFIG_MAX_LENGHT_DATASTREAM);
  if(Vu8pBuffer==NULL)
  {
    Vs32ReturnCode-=ENOMEM;
    fprintf(stderr, "early_config_err:%d malloc()\n",Vs32ReturnCode);
	}
	else
	{ /*read datastream*/   
#ifdef PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGCSC
    tU8  Vu8Info=0;
    Vs32ReturnCode=pdd_read_datastream_early_from_nor(PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGCSC,Vu8pBuffer,EARLY_CONFIG_MAX_LENGHT_DATASTREAM,EARLY_CONFIG_VERSION_STREAM,&Vu8Info);
	  if(Vs32ReturnCode<PDD_OK)
	  {
	    fprintf(stderr, "early_config_err:%d pdd_read_datastream_early_from_nor()\n",Vs32ReturnCode);
	  }
	  else
	  { //get element from stream
      tS32  Vs32Size=Vs32ReturnCode;
	    M_DEBUG_STR_VALUE("early_config: pdd_read_datastream_early_from_nor() success size stream:%d\n",Vs32Size);	 
	    /* ----- get elements  ----*/
	    Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_CURRENT_PROFILE,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tU8Profile,sizeof(tU8),EARLY_CONFIG_VERSION_ELEMENT);
      if (Vs32ReturnCode!=0)
        fprintf(stderr, "early_config_err: get element 'TrCurrentProfile' fails\n");
      else
      {
		    Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_HUE,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tsCscProp.hue,sizeof(tS16),EARLY_CONFIG_VERSION_ELEMENT);      
        if (Vs32ReturnCode!=0)
          fprintf(stderr, "early_config_err: get element 'TrHueValue' fails '%d'\n",Vs32ReturnCode);
        else
        {
          Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_CONTRAST,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tsCscProp.contrast,sizeof(tS16),EARLY_CONFIG_VERSION_ELEMENT);
          if (Vs32ReturnCode!=0)
            fprintf(stderr, "early_config_err: get element 'TrContrastValue' fails\n");
          else
          {
            Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_SATURATION,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tsCscProp.saturation,sizeof(tS16),EARLY_CONFIG_VERSION_ELEMENT);
            if (Vs32ReturnCode!=0)
              fprintf(stderr, "early_config_err: get element 'TrSaturationValue' fails\n");
            else
            {
              Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_BRIGHTNESS,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tsCscProp.brightness,sizeof(tS16),EARLY_CONFIG_VERSION_ELEMENT);
              if (Vs32ReturnCode!=0)
                fprintf(stderr, "early_config_err: get element 'TrBrightnessValue' fails\n");
              else
              {
                Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_HUE_OFFSET,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tsCscProp.hue_off,sizeof(tS16),EARLY_CONFIG_VERSION_ELEMENT);
                if (Vs32ReturnCode!=0)
                  fprintf(stderr, "early_config_err: get element 'TrHueOffsetValue' fails\n");
                else
                {
                  Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_SATURATION_OFFSET,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tsCscProp.saturation_off,sizeof(tS16),EARLY_CONFIG_VERSION_ELEMENT);
                  if (Vs32ReturnCode!=0)
                    fprintf(stderr, "early_config_err: get element 'TrSaturationOffsetValue' fails\n");
                  else
                  {
                    Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_BRIGHTNESS_OFFSET,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tsCscProp.brightness_off,sizeof(tS16),EARLY_CONFIG_VERSION_ELEMENT);     
                    if (Vs32ReturnCode!=0)
                      fprintf(stderr, "early_config_err: get element 'TrBrightnessOffsetValue' fails\n");
                    else
                    {
                      Vs32ReturnCode=pdd_helper_get_element_from_stream(EARLY_CONFIG_CSC_GAMMA_FACTOR,Vu8pBuffer,(size_t) Vs32Size,(void*) &PtsEarlyConfigCSC->tF64GammaValue,sizeof(tF64),EARLY_CONFIG_VERSION_ELEMENT);
                      if (Vs32ReturnCode!=0)
                        fprintf(stderr, "early_config_err: get element 'TrGammaFactor' fails\n");
                    }
                  }
                }
              }
            }
          }
        }
      }    
    }
#else
    /*for lint*/
    PtsEarlyConfigCSC->tF64GammaValue=0;
#endif
	  free(Vu8pBuffer);
  }
  return(Vs32ReturnCode);
}
#endif
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddWriteDisplayConfig()
*
* DESCRIPTION: write datastream display configuration to PDD
*
* PARAMETERS:
*   PpsEarlyConfigDisplay: pointer to structure of display configuration 
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2014 07 1
*****************************************************************************/
tS32 s32EarlyConfig_PddWriteDisplayConfig(tsEarlyConfig_PddDisplay *PpsEarlyConfigDisplay)
{
  tS32 Vs32ReturnCode=0;

  #ifndef PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGDISPLAY
  Vs32ReturnCode=-ENOENT;
  fprintf(stderr, "early_config_err:%d pool 'EarlyConfigDisplay' isn't defined\n",Vs32ReturnCode);
  #else
  tS32 Vs32SizeDataStream=0;
  tU8 *Vu8pBuffer=malloc(EARLY_CONFIG_MAX_SIZE_DATASTREAM);

  if(Vu8pBuffer==NULL)
  {
    Vs32ReturnCode-=ENOMEM;
    fprintf(stderr, "early_config: malloc() fails %d\n",Vs32ReturnCode);	
  }
  else
  {/*create datastream*/
	  tU8 *Vu8pBufferElement=Vu8pBuffer;
    memset(Vu8pBuffer,0,EARLY_CONFIG_MAX_SIZE_DATASTREAM);
    #ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
    /*------------ add element TrResolution ------*/
    snprintf(PpsEarlyConfigDisplay->tResolution.cResolution,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT,"%dx%d@%dHz",PpsEarlyConfigDisplay->tResolution.tResolutionValue.u32Heigh,
		                                                                                                                   PpsEarlyConfigDisplay->tResolution.tResolutionValue.u32Width,
																												                                                               PpsEarlyConfigDisplay->tResolution.tResolutionValue.u32Refresh);
    #else
	  s32EarlyConfig_PddCreateGMResolutionStr(&PpsEarlyConfigDisplay->tResolution); 
    #endif
	  /*check if string include blank*/
	  char  *VcpPos=strchr(PpsEarlyConfigDisplay->tResolution.cResolution,' ');
    if (VcpPos!=NULL)
	  {
	    fprintf(stderr, "early_config: eWriteDisplayConfig error found a blank in resolution string '%s'\n",PpsEarlyConfigDisplay->tResolution.cResolution);	
	  }
	  M_DEBUG_STR_STR("early_config: save resolution string: '%s'\n",PpsEarlyConfigDisplay->tResolution.cResolution);
    Vs32ReturnCode=s32EarlyConfig_PddCreateDatastream(EARLY_CONFIG_DISPLAY_ELEMENT_RESOLUTION,Vu8pBufferElement,
		                                                 (void*)PpsEarlyConfigDisplay->tResolution.cResolution,
		 											                           strlen(PpsEarlyConfigDisplay->tResolution.cResolution)+1);
	  if(Vs32ReturnCode>=0)
	  {
	    Vs32SizeDataStream+=Vs32ReturnCode;
	    Vu8pBufferElement+=Vs32ReturnCode;
	  }
    /*------------ add element TrBackwardComp ------*/
    if(Vs32ReturnCode>=0)
	  {
	    Vs32ReturnCode=s32EarlyConfig_PddCreateDatastream(EARLY_CONFIG_DISPLAY_ELEMENT_BC_MODE,Vu8pBufferElement,
		                                                   (void*)&PpsEarlyConfigDisplay->tBCMode.s32BCMode,sizeof(tS32));
	    if(Vs32ReturnCode>=0)
	    { 
	      Vs32SizeDataStream+=Vs32ReturnCode;
		    Vu8pBufferElement+=Vs32ReturnCode;
	    }
	  }
    /*------------ add element TrLowFrequency ------*/
	  if(Vs32ReturnCode>=0)
	  {
	    Vs32ReturnCode=s32EarlyConfig_PddCreateDatastream(EARLY_CONFIG_DISPLAY_ELEMENT_LF_MODE,Vu8pBufferElement,
		                                              (void*)&PpsEarlyConfigDisplay->tLFMode.s32LFMode,sizeof(tS32));	   
	    if(Vs32ReturnCode>=0)
	    {	
	      Vs32SizeDataStream+=Vs32ReturnCode;
		    Vu8pBufferElement+=Vs32ReturnCode;
	    }
	  }
    /*save datatsream*/
    if(Vs32ReturnCode>=0)
	  {  	
	    Vs32ReturnCode=pdd_write_data_stream(PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGDISPLAY,PDD_LOCATION_NOR_USER,Vu8pBuffer,Vs32SizeDataStream,EARLY_CONFIG_VERSION_STREAM);
	    if(Vs32ReturnCode<0)
	    {
	      fprintf(stderr, "early_config: pdd_write_data_stream() fails %d\n",Vs32ReturnCode);	
	    }
	  }
    free(Vu8pBuffer);
  }
  //check error and set error memory entry
  if(Vs32ReturnCode>=0)
  {
    M_DEBUG_STR_VALUE("early_config: pdd datastream saved to PDD_UserEarly: return code:%d \n",Vs32ReturnCode);
  }
  else
  {
    fprintf(stderr,"early_config_err:%d pdd datastream saved to PDD_UserEarly fails\n",Vs32ReturnCode);
    vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_SAVE_STREAM,(const tU8*)&Vs32ReturnCode,sizeof(Vs32ReturnCode));  
  }
  #endif
  return(Vs32ReturnCode);
}

/******************************************************************************
* FUNCTION: tS32 vEarlyConfig_PddInitConfigDisplay()
*
* DESCRIPTION: set display configuration to init state; 
*              call from function s32EarlyConfig_PddReadDisplayConfig 
*
* PARAMETERS:
*   PpsEarlyConfigDisplay: pointer to structure of display configuration 
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 06 18
*****************************************************************************/
static void vEarlyConfig_PddInitConfigDisplay(tsEarlyConfig_PddDisplay *PpsEarlyConfigDisplay)
{
  /*tReadDisplayInfo*/
  PpsEarlyConfigDisplay->tResolution.bReadInfo=FALSE;
  memset(PpsEarlyConfigDisplay->tResolution.cResolution,0,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT);
  PpsEarlyConfigDisplay->tBCMode.bReadInfo=FALSE;
  PpsEarlyConfigDisplay->tBCMode.cpBCMode=NULL;
  PpsEarlyConfigDisplay->tBCMode.s32BCMode=EARLY_CONFIG_MODE_INVALID;
  PpsEarlyConfigDisplay->tLFMode.bReadInfo=FALSE;
  PpsEarlyConfigDisplay->tLFMode.cpLFMode=NULL;
  PpsEarlyConfigDisplay->tLFMode.s32LFMode=EARLY_CONFIG_MODE_INVALID;
}

/******************************************************************************
* FUNCTION: tS32 vEarlyConfig_PddGetStrMode()
*
* DESCRIPTION: get the string for the mode from value
*
* PARAMETERS:
*     Ps32Mode: value mode
*     PcpMode: pointer for the string
*
* HISTORY:Created by Andrea Bueter 2014 07 02
*****************************************************************************/
static void vEarlyConfig_PddGetStrMode(tS32 Ps32Mode,char** PcpMode)
{
  switch(Ps32Mode)
  {
    case 0:
      *PcpMode="0\n";
	  break;
	  case 1:
	   *PcpMode="1\n";
	  break;
	  case -1:
	   *PcpMode="-1\n";
	  break;
	  default:
	  break;
  }
}

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddCreateDatastream()
*
* DESCRIPTION: create the datastream for an element
*
* PARAMETERS:
*   PstrElementName: element name
*   Pu8vBufStream: buffer for the stream
*   PpvBufData: buffer for the data
*   Vs32SizeData: size of the data
*
* RETURNS: error(<0) or framelenght
*
* HISTORY:Created by Andrea Bueter 2014 07 1
*****************************************************************************/
static tS32 s32EarlyConfig_PddCreateDatastream(tString PstrElementName,tU8* Pu8vBufStream,void* PpvBufData,tS32 Vs32SizeData)
{
  tS32 Vs32Pos=0;
  tS32 Vs32HashValue=s32EarlyConfig_PddCalcHash(PstrElementName);  //calc hash
  tU8  Vu8ElementVersion=EARLY_CONFIG_VERSION_ELEMENT;
  /* 4 bytes     ; 1 byte         ; 4 bytes   ;strlen(name); 4 byte        ; payload lenght*/
  /* frame lenght; element version; hash value;element name; payload length; data */
  tS32 Vs32FrameLength=sizeof(Vs32FrameLength)+sizeof(Vu8ElementVersion)+sizeof(Vs32HashValue)+strlen(PstrElementName)+1+sizeof(Vs32SizeData)+Vs32SizeData;	

  memmove(&Pu8vBufStream[Vs32Pos],&Vs32FrameLength,sizeof(Vs32FrameLength));       //frame lenght
  Vs32Pos+=sizeof(Vs32FrameLength);
  Pu8vBufStream[Vs32Pos]=Vu8ElementVersion;                                        //element version
  Vs32Pos+=sizeof(Vu8ElementVersion);
  memmove(&Pu8vBufStream[Vs32Pos],&Vs32HashValue,sizeof(Vs32HashValue));           //hash value
  Vs32Pos+=sizeof(Vs32HashValue);
  strcpy(&Pu8vBufStream[Vs32Pos],PstrElementName);                                 //element name
  Vs32Pos+=strlen(PstrElementName)+1;
  memmove(&Pu8vBufStream[Vs32Pos],&Vs32SizeData,sizeof(Vs32SizeData));             //payload lenght
  Vs32Pos+=sizeof(Vs32SizeData);
  memmove(&Pu8vBufStream[Vs32Pos],PpvBufData,Vs32SizeData);                        //data 
  Vs32Pos+=Vs32SizeData;
  if(Vs32Pos!=Vs32FrameLength)
  {
    Vs32FrameLength=-1; //error
    fprintf(stderr, "early_config_err: s32EarlyConfig_PddCreateDatastream() size different\n");
  }
  return(Vs32FrameLength);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddCalcHash()
*
* DESCRIPTION: calc hash value for the element
*
* PARAMETERS:
*     PstrElementName: element name
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 07 1
*****************************************************************************/
static tS32 s32EarlyConfig_PddCalcHash(tString PstrElementName)
{
   tS32 Vs32Hash = 0;
   tS32 Vs32Len = strlen(PstrElementName);
   tS32 Vs32Count;

   for (Vs32Count=0; Vs32Count<Vs32Len; Vs32Count++) 
   {
      Vs32Hash = 5*Vs32Hash + PstrElementName[Vs32Count];
   }
   return(Vs32Hash);
}

#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddGetResolution()
*
* DESCRIPTION: get value for resolution from string
*
* PARAMETERS:
*     PtsResolution: pointer for resolution configuration
*
* RETURNS: success(0) or error code(<0) 
*
* HISTORY:Created by Andrea Bueter 2014 06 20
*****************************************************************************/
static tS32 s32EarlyConfig_PddGetResolution(tsEarlyConfig_PddDisplayResolution* PtsResolution)
{
  tS32   Vs32ReturnCode=-1;
  char   VcResolution[EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT];

  memset(VcResolution,0,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT);
  memcpy(VcResolution,PtsResolution->cResolution,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT);
  char  *VcpPos;

  /*check if string include blank*/
  VcpPos=strchr(VcResolution,' ');
  if (VcpPos!=NULL)
  {
    fprintf(stderr, "early_config: GetResolution error found a blank in resolution string '%s'\n",VcResolution);	
  }
  /*replace Xx and @ =' '*/
  VcpPos=strchr(VcResolution,'x');
  if(VcpPos!=NULL)
  {
    *VcpPos=' ';
  }
  VcpPos=strchr(VcResolution,'X');
  if(VcpPos!=NULL)
  {
    *VcpPos=' ';
  }
  VcpPos=strchr(VcResolution,'@');
  if(VcpPos!=NULL)
  {
    *VcpPos=' ';
  }
  M_DEBUG_STR_STR("early_config: replace resolution string(X,x,@): %s \n",VcResolution);
  /*get values*/
  PtsResolution->tResolutionValue.u32Heigh= strtoul(VcResolution,&VcpPos,10);
  M_DEBUG_STR_VALUE("early_config: u32Heigh %d \n",PtsResolution->tResolutionValue.u32Heigh);
  PtsResolution->tResolutionValue.u32Width= strtoul(VcpPos,&VcpPos,10);
  M_DEBUG_STR_VALUE("early_config: u32Width %d \n",PtsResolution->tResolutionValue.u32Width);
  PtsResolution->tResolutionValue.u32Refresh= strtoul(VcpPos,&VcpPos,10);
  M_DEBUG_STR_VALUE("early_config: u32Refresh %d \n",PtsResolution->tResolutionValue.u32Refresh);
  /*check values*/
  if(   (PtsResolution->tResolutionValue.u32Width!=0)
	  &&(PtsResolution->tResolutionValue.u32Heigh!=0)
	  &&(PtsResolution->tResolutionValue.u32Refresh!=0))
  {
    Vs32ReturnCode=0;	
  }
  else
  {
    fprintf(stderr, "early_config_err: get value resolution\n");
  }
  return(Vs32ReturnCode);
}
#else
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddCheckCreateDisplayStr()
*
* DESCRIPTION: check and create display string. 
*              If not "display" in string, then old GM string.
*              create the New-GM string "displayGM_XXXX"
*
* PARAMETERS:
*     PtsResolution: pointer for resolution configuration
*
* RETURNS: success(0) or error code(<0) 
*
* HISTORY:Created by Andrea Bueter 2014 06 20
*****************************************************************************/
static tS32 s32EarlyConfig_PddCheckCreateDisplayStr(tsEarlyConfig_PddDisplayResolution* PtsResolution)
{
  tS32   Vs32ReturnCode=0;

  //first search for 'display' in the string
  if(strncmp(PtsResolution->cResolution,"display",strlen("display"))!=0)
  {//not equal
    char   VcResolution[EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT]={0};
    // second read from of_display, if strings with "displayGM"
	  if(s32EarlyConfig_ReadDisplayString(VcResolution,EARLY_CONFIG_KIND_ACTION_GET_ACTUAL)>=0)
    { //compare 
	    if(strncmp(VcResolution,"displayGM",strlen("displayGM"))==0)
	    {//GM- String
	      char  *VcpPos;
	      /*create new string*/
        snprintf(VcResolution,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT,"displayGM_%s",PtsResolution->cResolution);	
        VcpPos=strchr(VcResolution,'@');
        if(VcpPos!=NULL)
        {
          *VcpPos='_';
        }
        /*copy back*/
        memset(PtsResolution->cResolution,0,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT);
        memcpy(PtsResolution->cResolution,VcResolution,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT);
      }
	  }
  }
  M_DEBUG_STR_STR("early_config: display string: '%s' \n",PtsResolution->cResolution);
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddCreateGMResolutionStr()
*
* DESCRIPTION: if GM string create resolution string 
*
* PARAMETERS:
*     PtsResolution: pointer for resolution configuration
*
* RETURNS: success(0) or error code(<0) 
*
* HISTORY:Created by Andrea Bueter 2014 06 20
*****************************************************************************/
static tS32 s32EarlyConfig_PddCreateGMResolutionStr(tsEarlyConfig_PddDisplayResolution* PtsResolution)
{
  tS32   Vs32ReturnCode=0;
  //first search for 'displayGM' in the string
  if(strncmp(PtsResolution->cResolution,"displayGM",strlen("displayGM"))==0)
  {//equal => GM Display
    char  *VcpPos;
	  char   VcResolution[EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT]={0};
	  //copy cResolution into temp resolution
    memcpy(VcResolution,PtsResolution->cResolution,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT);
    //search 'M'
    VcpPos=strchr(VcResolution,'_');
	  if(VcpPos!=NULL)
	  { //copy string back from VcpPos
	    VcpPos++;
	    memset(PtsResolution->cResolution,0,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT);
	    memcpy(PtsResolution->cResolution,VcpPos,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT-strlen("displayGM")-1);
	    VcpPos=strchr(PtsResolution->cResolution,'_');
      if(VcpPos!=NULL)
      {
        *VcpPos='@';
      }
    }
	  M_DEBUG_STR_STR("early_config: change GM display string to: '%s' \n",PtsResolution->cResolution);
  }
  return(Vs32ReturnCode);
}
#endif