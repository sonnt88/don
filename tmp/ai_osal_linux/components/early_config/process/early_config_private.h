/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        early_config_private.h
 *
 * private header file for the process early_config
 *
 *
 * @date        2014-07-07
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#define EARLY_CONFIG_DEBUG
#define EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE   //write string to file of_display  

#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
#include <xf86drm.h>
#include <xf86drmMode.h>
#endif
#ifdef EARYL_CONFIG_USE_CSC_LIB
#include <ipu_dp_csc.h>
#endif
/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
/*---------- defines for trace -------------------------------------*/
#ifdef EARLY_CONFIG_DEBUG
#define M_DEBUG_STR_STR(Str,StrOut)                                    fprintf(stderr,Str,StrOut) 
#define M_DEBUG_STR_2STR(Str,StrOut,StrOut2)                           fprintf(stderr,Str,StrOut,StrOut2) 
#define M_DEBUG_STR_VALUE_STR(Str1,Value,Str2)                         fprintf(stderr,Str1,Value,Str2);
#define M_DEBUG_STR_VALUE(Str,Value)                                   fprintf(stderr, Str,Value);
#define M_DEBUG_STR_3VALUES(Str,Value1,Value2,Value3)                  fprintf(stderr, Str,Value1,Value2,Value3);
#define M_DEBUG_MODE(Str,mode)                                         vEarlyConfig_DebugMode(Str,mode);
#else
#define M_DEBUG_STR_STR(Str,StrOut)      
#define M_DEBUG_STR_2STR(Str,StrOut,StrOut2)                          
#define M_DEBUG_STR_VALUE_STR(Str1,Value,Str2)                         
#define M_DEBUG_STR_VALUE(Str,Value)                                   
#define M_DEBUG_STR_3VALUES(Str,Value1,Value2,Value3)                    
#define M_DEBUG_MODE(Str,mode)  
#endif

#define M_DEBUG_STR_4VALUES(Str,Value1,Value2,Value3,Value4)           fprintf(stderr, Str,Value1,Value2,Value3,Value4);
#define M_DEBUG_STR_5VALUES(Str,Value1,Value2,Value3,Value4,Value5)    fprintf(stderr, Str,Value1,Value2,Value3,Value4,Value5);
#define M_DEBUG_TIME()                                                 vEarlyConfig_DebugTime();

/*---------------------- for error memory  --------------------------------*/
//drm
#define EARLY_CONFIG_EM_ERROR_FOUND_NO_DEFAULT_RESOLUTION       0x01
#define EARLY_CONFIG_EM_ERROR_SAVED_RESOLUTION_NOT_VALID        0x02
#define EARLY_CONFIG_EM_ERROR_SET_RESOLUTION                    0x03
#define EARLY_CONFIG_EM_ERROR_GET_SET_RESOLTION                 0x04
//file
#define EARLY_CONFIG_EM_ERROR_WRITE_BC_MODE                     0x05
#define EARLY_CONFIG_EM_ERROR_WRITE_LF_MODE                     0x06
#define EARLY_CONFIG_EM_ERROR_WRITE_DISPLAY_STRING              0x07
//PDD
#define EARLY_CONFIG_EM_ERROR_SAVE_STREAM                       0x08
//driver
#define EARLY_CONFIG_EM_ERROR_NO_CONFIG_FOR_DRIVER              0x09
#define EARLY_CONFIG_EM_ERROR_LOAD_DRIVER                       0x0a
#define EARLY_CONFIG_EM_ERROR_SET_CSC                           0x0b
#define EARLY_CONFIG_EM_ERROR_SET_GAMMA                         0x0c
//clock edge select
#define EARLY_CONFIG_EM_ERROR_WRITE_CLOCK_EDGE_SELECT           0x0d
#define EARLY_CONFIG_EM_ERROR_CONFIGURATION_WRONG               0x0e

/*---------------------- length --------------------------------*/
#define EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT    32
#define EARLY_CONFIG_DRIVER_NAME_STRING_LENGHT           64

#define EARLY_CONFIG_MAX_LENGHT_DATASTREAM              500

#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
/*---------------------- name for device node  --------------------------------*/
#define EARLY_CONFIG_DISPLAY_DEVICE_NODE_NAME   "/dev/dri/card0"
#define EARLY_CONFIG_DISPLAY_DEVICE_NAME_LENGHT          32
#endif

/*---------------------- default vaules --------------------------------*/
#define EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT         -1
#define EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT         -1
#define EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT_STR    "-1\n"
#define EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT_STR    "-1\n"

#define EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT_GM    1
#define EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT_GM    1
#define EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT_STR_GM    "1\n"
#define EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT_STR_GM    "1\n"

//default Touch
#define EARLY_CONFIG_DEFAULT_TOUCH_DRIVER_NAME    "atmel_mxt_ts"
#define EARLY_CONFIG_DEFAULT_TOUCH_CONFIG_NAME    "DTS2020-mxtConfig8inch-V03.raw"

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/
#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
/*typdef struct resolution mode*/
typedef struct
{
  tU32 u32Width;
	tU32 u32Heigh;
	tU32 u32Refresh;
}tsEarlyConfig_PddReadResolution;
#endif

/*typedef for element TrResolution */
typedef struct
{
  tBool                               bReadInfo;
  char                                cResolution[EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT];
  #ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
  tsEarlyConfig_PddReadResolution     tResolutionValue;
  #endif
}tsEarlyConfig_PddDisplayResolution;

/*typedef for element TrBackwardComp */
typedef struct
{
  tBool         bReadInfo;
  tS32          s32BCMode;
  const char*   cpBCMode;
}tsEarlyConfig_PddDisplayBCMode;

/*typedef for element TrLowFrequency */
typedef struct
{
  tBool        bReadInfo;
  tS32         s32LFMode;
  const char*  cpLFMode;
}tsEarlyConfig_PddDisplayLFMode;

/*typedef for element TrClockEdgeSelectLVDS */
typedef struct
{
  tBool         bReadInfo;
  tS32          s32ClockEdgeSelect;
  const char*   cpClockEdgeSelect;
}tsEarlyConfig_PddDisplayClockEdgeSelect;

/*typedef read display configuration*/
typedef struct
{
  tsEarlyConfig_PddDisplayResolution tResolution;
  tsEarlyConfig_PddDisplayBCMode     tBCMode;
  tsEarlyConfig_PddDisplayLFMode     tLFMode;
}tsEarlyConfig_PddDisplay;

/*typedef read display configuration for more display*/
typedef struct
{
  tsEarlyConfig_PddDisplayResolution           tResolution;
  tsEarlyConfig_PddDisplayClockEdgeSelect      tClockEdgeSelect;
}tsEarlyConfig_PddDisplaysMore;

#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
struct tsEarlyConfig_DrmDisplayDev
{
	struct tsEarlyConfig_DrmDisplayDev  *tpNext;      //points to the next device in the single-linked list
	uint32_t                            u32Width;    //width of our buffer object
	uint32_t                            u32Height;   //height of our buffer object
	uint32_t                            u32Stride;   //stride value of our buffer object
	uint32_t                            u32Size;     //size of the memory mapped buffer
	uint32_t                            u32Handle;   //a DRM handle to the buffer object that we can draw into
	uint8_t                            *u8pMap;      //pointer to the memory mapped buffer
	drmModeModeInfo                     tMode;       //the display mode that we want to use
	uint32_t                            u32Fb;       //a framebuffer handle with our buffer object as scanout buffer
	uint32_t                            u32ConnId;   //the connector ID that we want to use with this buffer
	uint32_t                            u32CrtcId;   //the crtc ID that we want to use with this connector
	drmModeCrtc                        *tpSavedCrtc; //the configuration of the crtc before we changed it. We use it so we can restore the same mode when we exit.
};

typedef enum 
{
  EARLY_CONFIG_KIND_ACTION_SET,
  EARLY_CONFIG_KIND_ACTION_GET_SET
}teEarlyConfig_DRMKindAction;

/*typedef from drm access*/
typedef struct
{      
  char                                 cDevName[EARLY_CONFIG_DISPLAY_DEVICE_NAME_LENGHT];
  teEarlyConfig_DRMKindAction          eKindAction;
  tS32                                 s32Fd;
  tS32                                 s32NumberMode;
  struct tsEarlyConfig_DrmDisplayDev  *tspDisplayDevList;
}tsEarlyConfig_DRMAccess;
#else
typedef enum 
{
  EARLY_CONFIG_KIND_ACTION_GET_ACTUAL,
  EARLY_CONFIG_KIND_ACTION_GET_FIRST
}teEarlyConfig_FileKindAction;
#endif

#ifdef EARYL_CONFIG_USE_CSC_LIB
typedef struct
{
  CLRPROP tsCscProp;
  tF64    tF64GammaValue;
  tU8     tU8Profile;
}tsEarlyConfig_CSC;
#endif

/*typedef info early config*/
typedef struct
{
  tsEarlyConfig_PddDisplay             tsPddDisplay;
#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
  tsEarlyConfig_DRMAccess              tsDRMAccess;
#endif
}tsEarlyConfig_Info;

/******************************************************************************/
/* function                                                                   */
/******************************************************************************/
//early_config_pdd 
tS32 s32EarlyConfig_PddReadDisplayConfig(tsEarlyConfig_PddDisplay *PpsEarlyConfigDisplay);
tS32 s32EarlyConfig_PddReadDisplayConfigs(tsEarlyConfig_PddDisplaysMore *PpsEarlyConfigDisplay,tU8 Pu8Number,char* PpcPoolName);
tS32 s32EarlyConfig_PddWriteDisplayConfig(tsEarlyConfig_PddDisplay *PpsEarlyConfigDisplay);
tS32 s32EarlyConfig_PddReadDriverName(char *PcDriver,char* PpcDriverName,char* PpcConfigFileName);
#ifdef EARYL_CONFIG_USE_CSC_LIB
tS32 s32EarlyConfig_PddReadCSC(tsEarlyConfig_CSC*);
#endif

//early_config_drm.c
#ifdef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
tS32 s32EarlyConfig_GetConnectorType(tsEarlyConfig_Info *PpsEarlyConfigInfo);
#else
tS32 s32EarlyConfig_DrmResolution(tsEarlyConfig_Info *PpsEarlyConfigInfo);
#endif

//early_config_file.c
#ifdef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
tS32 s32EarlyConfig_SetDisplayTiming(char *PpsDisplayTimin,tBool PbSpecificTimingNode,tU8 Pu8TimingNode);
tS32 s32EarlyConfig_GetSetDisplay(tsEarlyConfig_PddDisplay *PpsEarlyConfigDisplay);
tS32 s32EarlyConfig_ReadDisplayString(char *PpsDisplayName,teEarlyConfig_FileKindAction PeKindAction);
#endif
tS32 s32EarlyConfig_SetModes(const char* PpcBufWriteBCMode,const char* PpcBufWriteLFMode);
tS32 s32EarlyConfig_SetClockEgdeSelect(const char* PpcBufClockEdgeSelect);

//early_config_kmod.c
tS32 s32EarlyConfig_KmodSetDriverName(char* PpcDriverName);
tS32 s32EarlyConfig_KmodStartDriverWithScript(char* PpcDriverName,char* PpcConfigFileName);

#ifdef EARYL_CONFIG_USE_CSC_LIB
//early_config_csc
void vEarlyConfig_CSCInit(tsEarlyConfig_CSC* PtsEarlyConfigCSC);
tS32 s32EarlyConfig_CSCAndGammaSet(tsEarlyConfig_CSC* PtsEarlyConfigCSC);
#endif

//early_config_trace
void vEarlyConfig_DebugMode(const char* PcMode,const char* PcModeValue);
void vEarlyConfig_DebugTime(void);
void vEarlyConfig_SetErrorEntry(tU8 Pu8Kind, const tU8* Pu8pData, tU32 Pu32Len);




