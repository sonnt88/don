/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        early_config_main.c
 *
 * This file contains the main function of the process. 
 * 
 * global function:
 * -- main(): entry function, checks between different options
 *
 * local function:
 *                  -- vEarlyConfig_PddConfigDisplay()
 *                        function to set the display configuration
 *                  -- s32EarlyConfig_PddSetConfigDisplay():
 *                        set the display configuration
 *                  -- bEarlyConfig_PddSetConfigDisplayDefault():
 *                        set the default display configuration
 *                  -- s32EarlyConfig_PddConfigLoadDriver():
 *                        This function loads a linux driver.
 *                  -- vEarlyConfig_PrintHelp():
 *                        helper function for the process 
 *
 * @date        2014-07-07
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <systemd/sd-daemon.h> 
#include "helper.h"
#include "system_types.h"
#include "system_definition.h"
#include "pdd.h"
#include "pdd_config_nor_user.h"
#include "early_config_private.h"

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/                            


/******************************************************************************/
/* local function                                                 */
/******************************************************************************/
static void  vEarlyConfig_PddConfigDisplay(char* PcStr);
static void  vEarlyConfig_PddConfigOneDisplay(void);
static void  vEarlyConfig_PddConfigMoreDisplays(tU8 Pu8Number);
static tS32  s32EarlyConfig_PddConfigLoadDriver(char* PcDriver);
static tS32  s32EarlyConfig_PddSetCSC(void);
static tS32  s32EarlyConfig_PddSetConfigDisplay(tsEarlyConfig_Info *PpsEarlyConfigInfo);
static tBool bEarlyConfig_PddSetConfigDisplayDefault(tsEarlyConfig_Info *PpsEarlyConfigInfo);
static void  vEarlyConfig_PrintHelp(void);

/******************************************************************************
* FUNCTION: int main(int argc, char *argv[])
*
* DESCRIPTION: main thread: checks between different options
*              - d: display configuration
*              - l: load driver (expample load RTC driver -lRtc)
*              - h: help
*              if no valid parameter given => display configuration starts
*
* PARAMETERS:
*      argc: number of strings
*      argv: string table
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2015 28 07
*****************************************************************************/
int main(int argc, char *argv[])
{
  const char* VcOptString = "hd:l:c";
  int         ViOpt;
  char*       VcpStr = NULL;
  tS32        Vs32ExitCode=0;	
  tBool       VbDone=FALSE;
  tBool       VbNotify=FALSE;
 
  M_DEBUG_TIME();
  while ((ViOpt = getopt(argc, argv, VcOptString)) != -1) 
  {
    switch (ViOpt) 
	  {
	    case 'h':
	    {	    
	      vEarlyConfig_PrintHelp();
		    VbDone=TRUE;
	    }break;
	    case 'd':
	    {/*read and set display configuration*/ 
        VcpStr=strdup(optarg);
        vEarlyConfig_PddConfigDisplay(VcpStr);      
        free(VcpStr);
		    VbDone=TRUE;
        VbNotify=TRUE;
	    }break;
	    case 'l':
	    { /*load driver */
	      VcpStr=strdup(optarg);
		    Vs32ExitCode=s32EarlyConfig_PddConfigLoadDriver(VcpStr);
		    free(VcpStr);
		    VbDone=TRUE;
	    }break;
      case 'c':
      {//set Color Space Conversion 
        Vs32ExitCode=s32EarlyConfig_PddSetCSC();
        VbDone=TRUE;
        VbNotify=TRUE;
      }break;
	    default:
	    {/*read and set display configuration*/ 
	      vEarlyConfig_PrintHelp();
		    VbDone=TRUE;
	    }break;
    }
  }
  if(VbDone==FALSE)
  { /*with no parameter => display configuration*/	
    vEarlyConfig_PddConfigOneDisplay();
    VbNotify=TRUE;
  }
  M_DEBUG_TIME();
  // --- notify systemd that process early_config ready
  if (VbNotify==TRUE)
  {
    sd_notify(0,"READY=1");
    while(1) 
    {
      sleep(1000); //second
    }
  }
  exit(Vs32ExitCode);
}
/******************************************************************************
* FUNCTION: vEarlyConfig_PddConfigDisplay
* 
* DESCRIPTION: function to set the display configuration
*                  --- read configuration data from PDD;
*                  --- if get valid data from PDD set configuration data 
*                  --- if read not valid data from PDD, get default displayand set the default BC mode and LF mode
*                  --- if setting change, save diaplay configuration to NOR 
*                  --- notify systemd that process early_config ready
*         
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2015 07 27
*****************************************************************************/
static void  vEarlyConfig_PddConfigDisplay(char* PcStr)
{
  if(strncmp("2",PcStr,strlen("2"))==0)
  {
    vEarlyConfig_PddConfigMoreDisplays(2);
  }
  else
  { 
    vEarlyConfig_PddConfigOneDisplay();
  } 
}
/******************************************************************************
* FUNCTION: vEarlyConfig_PddConfigOneDisplay
* 
* DESCRIPTION: function to set the display configuration
*                  --- read configuration data from PDD;
*                  --- if get valid data from PDD set configuration data 
*                  --- if read not valid data from PDD, get default displayand set the default BC mode and LF mode
*                  --- if setting change, save diaplay configuration to NOR 
*                  --- notify systemd that process early_config ready
*         
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2015 07 27
*****************************************************************************/
static void  vEarlyConfig_PddConfigOneDisplay(void)
{
  tS32               Vs32ReturnCode;	
  tsEarlyConfig_Info VtsEarlyConfigInfo;
  // --- read configuration data from PDD*/
  Vs32ReturnCode=s32EarlyConfig_PddReadDisplayConfig(&VtsEarlyConfigInfo.tsPddDisplay);
  // --- print out read status
  M_DEBUG_STR_3VALUES("early_config: read state resolution: %d read state BCMode:%d read state LFMode:%d\n",
	   			            VtsEarlyConfigInfo.tsPddDisplay.tResolution.bReadInfo,
		  		            VtsEarlyConfigInfo.tsPddDisplay.tLFMode.bReadInfo,
		                  VtsEarlyConfigInfo.tsPddDisplay.tBCMode.bReadInfo);
  // --- if read valid data from PDD set configuration data  
  if(Vs32ReturnCode>=0)
  { /*set configuration of the display*/
    Vs32ReturnCode=s32EarlyConfig_PddSetConfigDisplay(&VtsEarlyConfigInfo);
  }
  // --- if read not valid data from PDD, get default display and set the default BC mode and LF mode
  if(bEarlyConfig_PddSetConfigDisplayDefault(&VtsEarlyConfigInfo)==TRUE)
  { /*setting changed */
    // --- save display configuration to NOR 
    Vs32ReturnCode=s32EarlyConfig_PddWriteDisplayConfig(&VtsEarlyConfigInfo.tsPddDisplay);    
  } 
}

/******************************************************************************
* FUNCTION: vEarlyConfig_PddConfigMoreDisplays
* 
* DESCRIPTION: function to set the display configuration of two display
*                  --- read configuration data from PDD;
*                  --- if get valid data from PDD set configuration data                
*                  --- if setting change, save diaplay configuration to NOR 
*                  --- notify systemd that process early_config ready
*         
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2016 01 06
*****************************************************************************/
static void  vEarlyConfig_PddConfigMoreDisplays(tU8 Pu8Number)
{
  tS32                               Vs32ReturnCode=-1;	
  tsEarlyConfig_PddDisplaysMore      VtsDisplayConfigMore[Pu8Number];
  
  // init 
  memset(&VtsDisplayConfigMore[0],0,sizeof(tsEarlyConfig_PddDisplaysMore));
  // --- read configuration data from PDD for pool PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGTWODISPLAYS*/
  if(Pu8Number==2)
  {
    #ifndef PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGTWODISPLAYS
    Vs32ReturnCode=-ENOENT;
    fprintf(stderr, "early_config_err:%d pool 'EarlyConfigTwoDisplays' isn't defined\n",Vs32ReturnCode);
    #else
    Vs32ReturnCode=s32EarlyConfig_PddReadDisplayConfigs(&VtsDisplayConfigMore[0],Pu8Number,PDD_NOR_USER_DATASTREAM_NAME_EARLYCONFIGTWODISPLAYS);  
    #endif
  }
  // --- if read valid data from PDD set configuration data  
  if(Vs32ReturnCode>=0)
  { 
    tU8 Vu8Inc;
    tU8 Vu8NumberClockEdgeSelect=0;
    /*for each display configuration*/
    tsEarlyConfig_PddDisplayResolution*       VtspResolution;
    tsEarlyConfig_PddDisplayClockEdgeSelect *VtspClockEdgeSelect;
    for(Vu8Inc=0;Vu8Inc<Pu8Number;Vu8Inc++)
    { /*print out read status*/
      VtspResolution=&VtsDisplayConfigMore[Vu8Inc].tResolution;
      VtspClockEdgeSelect=&VtsDisplayConfigMore[Vu8Inc].tClockEdgeSelect;
      /*-----  TrTimingLVDS ---*/
      if(VtspResolution->bReadInfo==TRUE)
      {
        fprintf(stderr, "early_config:TrTimingLVDS%d: '%s' \n",Vu8Inc+1,VtspResolution->cResolution);
        /*set configuration of the display*/
        Vs32ReturnCode=s32EarlyConfig_SetDisplayTiming(VtspResolution->cResolution,TRUE,Vu8Inc+1);
      }
      else
        fprintf(stderr, "early_config:TrTimingLVDS%d: '-' \n",Vu8Inc+1);

      /*-----  TrClockEdgeSelectLVDS ---*/
      VtspClockEdgeSelect=&VtsDisplayConfigMore[Vu8Inc].tClockEdgeSelect;
      if(VtspClockEdgeSelect->bReadInfo==TRUE)
      {
        fprintf(stderr, "early_config:TrClockEdgeSelectLVDS%d: '%d'\n",Vu8Inc+1,VtspClockEdgeSelect->s32ClockEdgeSelect);
        /*set configuration of the display*/
        if((VtspClockEdgeSelect->s32ClockEdgeSelect==0)||(VtspClockEdgeSelect->s32ClockEdgeSelect==1))
        {
          Vu8NumberClockEdgeSelect++;
          if(Vu8NumberClockEdgeSelect>1)
          {/*at moment only one clock edge select could be set!!! */
            tU8 Vu8Temp=Vu8Inc+1;
            fprintf(stderr, "early_config_err:TrClockEdgeSelectLVDS%d could not be set; configuration error: only one parameter 'TrClockEdgeSelectLVDS' could be set at moment\n",Vu8Temp);
            vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_CONFIGURATION_WRONG,&Vu8Temp,sizeof(tU8));	   
          }
          else
          {
            Vs32ReturnCode=s32EarlyConfig_SetClockEgdeSelect(VtspClockEdgeSelect->cpClockEdgeSelect);
          }
        }
      }
      else
        fprintf(stderr, "early_config:TrClockEdgeSelectLVDS%d: '-' \n",Vu8Inc+1);
    }
  }
  else
  {/*print out no valid configuration read*/
    fprintf(stderr, "early_config_err:%d no valid configuration read\n",Vs32ReturnCode);
  }
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddSetConfigDisplay()

* DESCRIPTION: set the display configuration
*
* PARAMETERS:
*         PpsEarlyConfigInfo: info structure of the process
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2014 06 24
*****************************************************************************/
static tS32 s32EarlyConfig_PddSetConfigDisplay(tsEarlyConfig_Info *PpsEarlyConfigInfo)
{
  tS32 Vs32ReturnCode=0;	
  if(PpsEarlyConfigInfo->tsPddDisplay.tResolution.bReadInfo==TRUE)
  { // set display configuration
    #ifdef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
	  Vs32ReturnCode=s32EarlyConfig_SetDisplayTiming(PpsEarlyConfigInfo->tsPddDisplay.tResolution.cResolution,FALSE,0);
    #else
	  PpsEarlyConfigInfo->tsDRMAccess.eKindAction=EARLY_CONFIG_KIND_ACTION_SET;
    Vs32ReturnCode=s32EarlyConfig_DrmResolution(PpsEarlyConfigInfo);
    #endif	
  }
  if((PpsEarlyConfigInfo->tsPddDisplay.tBCMode.bReadInfo==TRUE)&&(PpsEarlyConfigInfo->tsPddDisplay.tLFMode.bReadInfo==TRUE))
  {
    s32EarlyConfig_SetModes(PpsEarlyConfigInfo->tsPddDisplay.tBCMode.cpBCMode,PpsEarlyConfigInfo->tsPddDisplay.tLFMode.cpLFMode);
  }  
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddSetConfigDisplayDefault()
*
* DESCRIPTION: set the default display configuration
*
* PARAMETERS:
*         PpsEarlyConfigInfo: info structure of the process
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2014 06 24
*****************************************************************************/
static tBool bEarlyConfig_PddSetConfigDisplayDefault(tsEarlyConfig_Info *PpsEarlyConfigInfo)
{
  tBool VbChange=FALSE;	
  //check if read valid parameter from PDD_UserEarly => if saved, saved the default value into PDD_UserEarly
  if(   (PpsEarlyConfigInfo->tsPddDisplay.tResolution.bReadInfo==FALSE)
	    ||(PpsEarlyConfigInfo->tsPddDisplay.tBCMode.bReadInfo==FALSE)
	    ||(PpsEarlyConfigInfo->tsPddDisplay.tLFMode.bReadInfo==FALSE))
  {/*get and set display configuration*/
    VbChange=TRUE;
	  #ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE	
	  PpsEarlyConfigInfo->tsDRMAccess.eKindAction=EARLY_CONFIG_KIND_ACTION_GET_SET; 
    if(s32EarlyConfig_DrmResolution(PpsEarlyConfigInfo)<0)
	  {
	    VbChange=FALSE;	 
	  }
    #else
    if(s32EarlyConfig_GetSetDisplay(&PpsEarlyConfigInfo->tsPddDisplay)<0)
	  {
	    VbChange=FALSE;
	  }
    #endif	
	  // search for 'displayGM' in the string
    if(strncmp(PpsEarlyConfigInfo->tsPddDisplay.tResolution.cResolution,"displayGM",strlen("displayGM"))==0)
    { //set default for GM BCMode/LFMode
	    PpsEarlyConfigInfo->tsPddDisplay.tBCMode.s32BCMode=EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT_GM;
	    PpsEarlyConfigInfo->tsPddDisplay.tBCMode.cpBCMode=EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT_STR_GM;
	    PpsEarlyConfigInfo->tsPddDisplay.tLFMode.s32LFMode=EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT_GM;
	    PpsEarlyConfigInfo->tsPddDisplay.tLFMode.cpLFMode=EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT_STR_GM;
	  }
	  else
	  {
	    PpsEarlyConfigInfo->tsPddDisplay.tBCMode.s32BCMode=EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT;
	    PpsEarlyConfigInfo->tsPddDisplay.tBCMode.cpBCMode=EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT_STR;
	    PpsEarlyConfigInfo->tsPddDisplay.tLFMode.s32LFMode=EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT;
	    PpsEarlyConfigInfo->tsPddDisplay.tLFMode.cpLFMode=EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT_STR;
	  }
	  //set BC mode set LF mode
	  if(s32EarlyConfig_SetModes(PpsEarlyConfigInfo->tsPddDisplay.tBCMode.cpBCMode,PpsEarlyConfigInfo->tsPddDisplay.tLFMode.cpLFMode)<0)
	  {
	    VbChange=FALSE;     
	  }	
  }
  return(VbChange);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddConfigLoadDriver()
*
* DESCRIPTION: This function loads a linux driver.
*
* PARAMETERS: PcDriver: string of the kind of driver, which should be loaded          
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2015 27 07
*****************************************************************************/
static tS32  s32EarlyConfig_PddConfigLoadDriver(char* PcDriver)
{  
  char VcDriverName[EARLY_CONFIG_DRIVER_NAME_STRING_LENGHT];
  char VcConfigFileName[EARLY_CONFIG_DRIVER_NAME_STRING_LENGHT];
  memset(VcConfigFileName,0,EARLY_CONFIG_DRIVER_NAME_STRING_LENGHT);
  memset(VcDriverName,0,EARLY_CONFIG_DRIVER_NAME_STRING_LENGHT);
 
  if(strncmp("Touch",PcDriver,strlen(PcDriver))==0)
  { /*set default*/
    strncpy(VcDriverName,EARLY_CONFIG_DEFAULT_TOUCH_DRIVER_NAME,strlen(EARLY_CONFIG_DEFAULT_TOUCH_DRIVER_NAME));
    strncpy(VcConfigFileName,EARLY_CONFIG_DEFAULT_TOUCH_CONFIG_NAME,strlen(EARLY_CONFIG_DEFAULT_TOUCH_CONFIG_NAME));    
  }  
  tS32 Vs32ReturnCode=s32EarlyConfig_PddReadDriverName(PcDriver,VcDriverName,VcConfigFileName);
  if (Vs32ReturnCode==0)
  {
    M_DEBUG_STR_STR("early_config: read driver name '%s' \n",VcDriverName);
	  //check if name of configuration file read
	  if (VcConfigFileName[0]==0)
	  {
	    Vs32ReturnCode=s32EarlyConfig_KmodSetDriverName(VcDriverName);
	  }
	  else
	  {
	    M_DEBUG_STR_STR("early_config: read configuration file name '%s' \n",VcConfigFileName);
	    s32EarlyConfig_KmodStartDriverWithScript(VcDriverName,VcConfigFileName);
	  }
  }
  else
  {
    fprintf(stderr, "early_config_err: %d, no configuration found for '%s' driver\n",Vs32ReturnCode,PcDriver);	
    if(strncmp("Touch",PcDriver,strlen(PcDriver))==0)
	  {//if Touch set default
	    Vs32ReturnCode=0;
	    M_DEBUG_STR_STR("early_config: set default driver '%s' \n",VcDriverName);
	    M_DEBUG_STR_STR("early_config: set default configuration file name '%s' \n",VcConfigFileName);
	    s32EarlyConfig_KmodStartDriverWithScript(VcDriverName,VcConfigFileName);
	  }
	  else
	  { /*error memory entry*/
	    vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_LOAD_DRIVER,VcDriverName,strlen(VcDriverName)+1);
	  }
  }
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_PddSetCSC()
*
* DESCRIPTION: This function reads the CSC pool from the PDD 
*              and sets this configuration 
*
* PARAMETERS: void
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2015 27 07
*****************************************************************************/
static tS32  s32EarlyConfig_PddSetCSC(void)
{
   tS32                Vs32ReturnCode=0;
   #ifdef EARYL_CONFIG_USE_CSC_LIB
   tsEarlyConfig_CSC   VtsEarlyConfigCSC;

   //init 
   vEarlyConfig_CSCInit(&VtsEarlyConfigCSC);
   #ifdef EARLY_CONFIG_DEBUG
   //print out configuration
   fprintf (stderr, "early_config: default configuration: Hue=%d, Contrast=%d, Saturation=%d, Brightness=%d, HueOffset=%d, SaturationOffset=%d, BrightnessOffset=%d\n",
	                   VtsEarlyConfigCSC.tsCscProp.hue, VtsEarlyConfigCSC.tsCscProp.saturation, VtsEarlyConfigCSC.tsCscProp.brightness, VtsEarlyConfigCSC.tsCscProp.contrast,
	                   VtsEarlyConfigCSC.tsCscProp.hue_off, VtsEarlyConfigCSC.tsCscProp.saturation_off, VtsEarlyConfigCSC.tsCscProp.brightness_off);
   #endif
   //read configuration
   Vs32ReturnCode=s32EarlyConfig_PddReadCSC(&VtsEarlyConfigCSC);
   if(Vs32ReturnCode==0)
   { 
     #ifdef EARLY_CONFIG_DEBUG
     //print out configuration
     fprintf (stderr, "early_config: read current profile: %d\n",VtsEarlyConfigCSC.tU8Profile);
     fprintf (stderr, "early_config: read configuration: Hue=%d, Contrast=%d, Saturation=%d, Brightness=%d, HueOffset=%d, SaturationOffset=%d, BrightnessOffset=%d\n",
			                 VtsEarlyConfigCSC.tsCscProp.hue, VtsEarlyConfigCSC.tsCscProp.contrast, VtsEarlyConfigCSC.tsCscProp.saturation, VtsEarlyConfigCSC.tsCscProp.brightness,
			                 VtsEarlyConfigCSC.tsCscProp.hue_off, VtsEarlyConfigCSC.tsCscProp.saturation_off, VtsEarlyConfigCSC.tsCscProp.brightness_off);
     fprintf (stderr, "early_config: read gamma factor: %.2f\n",VtsEarlyConfigCSC.tF64GammaValue);
     #endif
     //set configuration if profile valid
     if((VtsEarlyConfigCSC.tU8Profile!=0)&&(VtsEarlyConfigCSC.tU8Profile!=0xff))
     {
       Vs32ReturnCode=s32EarlyConfig_CSCAndGammaSet(&VtsEarlyConfigCSC);
       #ifdef EARLY_CONFIG_DEBUG
       if(Vs32ReturnCode==0)
       {         
         fprintf (stderr, "early_config: success: set CSC and gamma \n");
       }
       #endif
     }
     else
     {
       fprintf(stderr, "early_config_err: %d no valid profile found \n",VtsEarlyConfigCSC.tU8Profile);
     }
   }    
   #endif
   return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 vEarlyConfig_PrintHelp()
*
* DESCRIPTION: helper function for the process
*
* HISTORY:Created by Andrea Bueter 2015 27 07
*****************************************************************************/
static void  vEarlyConfig_PrintHelp(void)
{
  fprintf(stderr, "\n");
  fprintf(stderr, "========================early_config ===========================================\n");
  fprintf(stderr, " 1) without parameter: set display configuration (Datapool: EarlyConfigDisplay) \n");
  fprintf(stderr, " 2) with parameter: \n");
  fprintf(stderr, "       -h print out options\n");
  fprintf(stderr, "       -d1 set display configuration (Datapool: EarlyConfigDisplay)\n");
  fprintf(stderr, "       -d2 set display configuration for two displays (Datapool: EarlyConfigTwoDisplays)\n");
  fprintf(stderr, "       -l[typedriver] load driver (e.g. -lRtc) \n");
  fprintf(stderr, "           [typedriver]='Rtc' (Datapool: EarlyConfigRTCDriver) \n");
  fprintf(stderr, "       -c set Color Space Conversion  (Datapool: EarlyConfigCSC)\n");
  fprintf(stderr, "================================================================================\n");
}

