/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        early_config_file.c
 *
 * 
 * global function:
 * -- s32EarlyConfig_SetDisplayTiming():
 *      determine the path for the display string and write the 
 *      display string to the file in the vfs
 * -- s32EarlyConfig_GetSetDisplay():
 *      read the display string and set this display string
 * -- s32EarlyConfig_ReadDisplayString():
 *      read a display string from file
 * -- s32EarlyConfig_SetModes():
 *      get the path for the modes and set the modes to file
 *        
 * local function:
 * -- s32EarlyConfig_GetPathForDisplaySetting():
 *      determine the path for the display string
 * -- s32EarlyConfig_ReadStringFromFile():
 *      read the string from file
 * -- s32EarlyConfig_GetSizeStringOfFile():
 *      get size of  the file
 * -- s32EarlyConfig_SetMode():
 *      write the mode string to the file
 * -- s32EarlyConfig_WriteStringToFile():
 *      write string to file
 * -- s32EarlyConfig_GetPathForModeSetting():
 *      determine the path for lfmode od bcmode
 * -- s32EarlyConfig_ScanDir():
 *      scan the directory and subdirectories for files.
 * -- s32EarlyConfig_CheckFileCompatibleAndGetExtractPath():
 *      If the given file the compatible file, check if file describes the driver.
 *      Extract from the path of the compatible file, the needed strinf for find the modes files in the sysfs.      
 * -- s32EarlyConfig_CheckFileModesAndGetPathMode():
 *      if file name equal "bcmode" or "lfmode", search in path the string given form the device tree path.
 *      if found copy the sysfs path to PpStrResult.
 * -- vEarlyConfig_GetMidStr():
 *      helper function for get the string between delimiter
 * -- vEarlyConfig_SwapStr():
 *      helper function for swap the string.  
 * -- vEarlyConfig_RemoveLeadingZerosFromStr():
 *      helper function for remove leading zeros from the string.    
 *
 * @date        2015-26-03
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <dirent.h>
#include <limits.h>       /* limits.h defines "PATH_MAX". */
#include "system_types.h"
#include "system_definition.h"
#include "early_config_private.h"


/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
#ifdef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
#define EARLY_CONFIG_DISPLAY_DIR            "/sys/class/drm/"
#define EARLY_CONFIG_DISPLAY_DEVICE_NODE    "card0-"
#define EARLY_CONFIG_DISPLAY_DEVICE_DIR_NODE_SPECIFIC    "/sys/class/drm/card0-LVDS-"
#define EARLY_CONFIG_DISPLAY_FILE           "of_display"

#define EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT    PATH_MAX   /*The VFS (Virtuele File system can not manage file name > 255*/
#endif
#define EARLY_CONFIG_FILE_BCMODE		"bcmode" 
#define EARLY_CONFIG_FILE_LFMODE		"lfmode"

#define EARLY_CONFIG_FILE_COMPATIBLE	"compatible"
#define EARLY_CONFIG_DRIVER		        "ti,ds90ub92x" 

#define EARLY_CONFIG_FILE_CLOCK_EDGE_SELECT "/sys/devices/soc0/soc.0/2100000.aips-bus/21a4000.i2c/i2c-1/1-002c/pixel_clock_edge"

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/ 
#ifdef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
static tS32 s32EarlyConfig_GetPathForDisplaySetting(char* PpcPath,tBool PbSpecificTimingNode,tU8 Pu8TimingNode);
static tS32 s32EarlyConfig_ReadStringFromFile(const char* PpcPath,const char* PpcBufRead,int PiSize);
static tS32 s32EarlyConfig_GetSizeStringOfFile(const char* PpcPath);
#endif
static tS32 s32EarlyConfig_SetMode(const char* PpcBufWriteMode,char* PpcPath,const char* PpcFile);
static tS32 s32EarlyConfig_WriteStringToFile(const char* PpcPath,const char* PpcBufWrite);
static tS32 s32EarlyConfig_GetPathForModeSetting(char* PpcPath);
static tS32 s32EarlyConfig_ScanDir(const char *dir_name, void *func_ptr, char *work);
static tS32 s32EarlyConfig_CheckFileCompatibleAndGetExtractPath(const char *PpcStrPath, const char *PpcStrFileName, char *PpStrExtractPath);
static tS32 s32EarlyConfig_CheckFileModesAndGetPathMode(const char *PpcStrPath, const char *PpcStrFileName, char *PpStrResult);
static void vEarlyConfig_GetMidStr(char *, char , int );
static void vEarlyConfig_SwapStr(char *, char , char );
static void vEarlyConfig_RemoveLeadingZerosFromStr(char *);
/******************************************************************************/
/* declaration global function                                                 */
/******************************************************************************/

#ifdef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_SetDisplayTiming()
*
* DESCRIPTION: determine the path for the display string and write the 
*              display string to the file in the vfs
*
* PARAMETERS:  PpsDisplayTiming: name of the diplay timing
*              PbSpecificTimingNode: display timing for special node
*              Pu8TimingNode: number of the node
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2015 03 26
*                                  2016 01 08 change for specific node
*****************************************************************************/
tS32 s32EarlyConfig_SetDisplayTiming(char *PpsDisplayTiming,tBool PbSpecificTimingNode,tU8 Pu8TimingNode)
{
   tS32 Vs32ReturnCode=0;
   char VcPathFile[EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT];

   Vs32ReturnCode=s32EarlyConfig_GetPathForDisplaySetting(VcPathFile,PbSpecificTimingNode,Pu8TimingNode);
   if(Vs32ReturnCode==0)
   { //write to file      
     Vs32ReturnCode=s32EarlyConfig_WriteStringToFile(VcPathFile,PpsDisplayTiming);	 
   }
   if(Vs32ReturnCode>=0)
   {
     M_DEBUG_STR_STR("early_config: set '%s' success \n",PpsDisplayTiming);
   }
   else
   {//error memory entry
     vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_WRITE_DISPLAY_STRING,PpsDisplayTiming,strlen(PpsDisplayTiming)+1);	   
	   //PpsEarlyConfigDisplay->tResolution.bReadInfo=FALSE;
   }
   return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_GetSetDisplay()
*
* DESCRIPTION: read the display string and set this display string
*
* PARAMETERS: pointer to structure of display configuration
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2015 03 26
*****************************************************************************/
tS32 s32EarlyConfig_GetSetDisplay(tsEarlyConfig_PddDisplay *PpsEarlyConfigDisplay)
{
  tS32 Vs32ReturnCode=0;
  /*get actual display in list*/
  memset(PpsEarlyConfigDisplay->tResolution.cResolution,0,EARLY_CONFIG_DISPLAY_RESOLUTION_STRING_LENGHT);
  Vs32ReturnCode=s32EarlyConfig_ReadDisplayString(PpsEarlyConfigDisplay->tResolution.cResolution,EARLY_CONFIG_KIND_ACTION_GET_ACTUAL);
  /*set diplay*/
  if(Vs32ReturnCode>=0)
  {
    Vs32ReturnCode=s32EarlyConfig_SetDisplayTiming(PpsEarlyConfigDisplay->tResolution.cResolution,FALSE,0);
  }
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_ReadDisplayString()
*
* DESCRIPTION:read a display string from file
*
* PARAMETERS: PpsDisplayName:        string pointer
*             PeKindAction:          EARLY_CONFIG_KIND_ACTION_GET_ACTUAL => actual string
*                                    EARLY_CONFIG_KIND_ACTION_GET_FIRST  => first string
*                           
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2015 03 26
*****************************************************************************/
tS32 s32EarlyConfig_ReadDisplayString(char *PpsDisplayName,teEarlyConfig_FileKindAction PeKindAction)
{
  tS32 Vs32ReturnCode=0;
  char  VcPathFile[EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT];
  char* VcFile;
  //determine path of the file
  Vs32ReturnCode=s32EarlyConfig_GetPathForDisplaySetting(VcPathFile,FALSE,0);
  if(Vs32ReturnCode==0)
  { //determine lenght 
    Vs32ReturnCode=s32EarlyConfig_GetSizeStringOfFile(VcPathFile);
    if(Vs32ReturnCode>0)
    {  //allocate buffer
      VcFile=malloc(Vs32ReturnCode);
	    if(VcFile==NULL)
	    {
	      Vs32ReturnCode-=ENOMEM;
        fprintf(stderr, "early_config_err:%d malloc()\n",Vs32ReturnCode);
	    }
	    else
	    { 
	      memset(VcFile,0,Vs32ReturnCode);
        Vs32ReturnCode=s32EarlyConfig_ReadStringFromFile(VcPathFile,VcFile,Vs32ReturnCode);	
		    if(Vs32ReturnCode>=0)
		    { 
		      char *VcpPos=VcFile;
		      if(PeKindAction==EARLY_CONFIG_KIND_ACTION_GET_ACTUAL)
		      { //search char '>'
            VcpPos=strchr(VcFile,'>');		
		      }
          if(VcpPos!=NULL)
          { 
			      if(strlen(VcpPos)>2)
			      {
		          VcpPos++;
			        VcpPos++;
			        //search end of string
		          char *VcpPosEnd=strchr(VcpPos,0x0A);
			        if(VcpPosEnd!=NULL)
              {  
		            *VcpPosEnd=0;			  
			          int ViLen=strlen(VcpPos);
			          if(ViLen>0)
			          { //copy string      
                  memcpy(PpsDisplayName,VcpPos,ViLen);			
		              M_DEBUG_STR_STR("early_config: read string '%s'\n",PpsDisplayName);  
			          }
              }
            }
          }         
        }
      }
	    free(VcFile);
    }
  }
  return(Vs32ReturnCode);
}
#endif
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_SetModes()
*
* DESCRIPTION: get the path for the modes and set the modes to file
*
* PARAMETERS:
*   --PpcBufWriteBCMode: string for BC mode
*   --PpcBufWriteLFMode: string for LF mode
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2014 07 07
*****************************************************************************/
tS32 s32EarlyConfig_SetModes(const char* PpcBufWriteBCMode,const char* PpcBufWriteLFMode)
{
   tS32 Vs32ReturnCode=0;
   //search path, if one mode should be set 
   if (  (strncmp(EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT_STR,PpcBufWriteBCMode,strlen(EARLY_CONFIG_DISPLAY_DEVICE_BC_MODE_DEFAULT_STR))!=0)
	   ||(strncmp(EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT_STR,PpcBufWriteLFMode,strlen(EARLY_CONFIG_DISPLAY_DEVICE_LF_MODE_DEFAULT_STR))!=0))
   {//get file path 
     char  VcPathFile[EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT]={0};
     Vs32ReturnCode=s32EarlyConfig_GetPathForModeSetting(VcPathFile);
     if(Vs32ReturnCode==0)
     {
       Vs32ReturnCode=s32EarlyConfig_SetMode(PpcBufWriteBCMode,VcPathFile,EARLY_CONFIG_FILE_BCMODE);
	   if(Vs32ReturnCode>=0)
	   {
	     Vs32ReturnCode=s32EarlyConfig_SetMode(PpcBufWriteLFMode,VcPathFile,EARLY_CONFIG_FILE_LFMODE);
	   }     
     }
     else
     {
       fprintf(stderr, "early_config_err: path for file not found: error code %d\n",Vs32ReturnCode);
     }
   }
   return(Vs32ReturnCode);
}

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_SetClockEgdeSelect()
*
* DESCRIPTION: set clock edge select
*
* PARAMETERS:
*   --PpcBufClockEdgeSelect: string for clock edge select
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2016 03 18
*****************************************************************************/
tS32 s32EarlyConfig_SetClockEgdeSelect(const char* PpcBufClockEdgeSelect)
{
   tS32 Vs32ReturnCode=0;
   Vs32ReturnCode=s32EarlyConfig_WriteStringToFile(EARLY_CONFIG_FILE_CLOCK_EDGE_SELECT,PpcBufClockEdgeSelect);
   if(Vs32ReturnCode>=0)
   {
     M_DEBUG_STR_STR("early_config: set TrClockEdgeSelectLVDS:'%s' success \n",PpcBufClockEdgeSelect);
   }
   else
   {//error memory entry
     vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_WRITE_CLOCK_EDGE_SELECT,PpcBufClockEdgeSelect,strlen(PpcBufClockEdgeSelect)+1);	   
   }
   return(Vs32ReturnCode);
}

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
#ifdef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_GetPathForDisplaySetting()
*
* DESCRIPTION: determine the path for the display string
*
* PARAMETERS: PpcPathFile: string buffer for the path
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2014 03 26
*****************************************************************************/
static tS32 s32EarlyConfig_GetPathForDisplaySetting(char* PpcPathFile,tBool PbSpecificTimingNode,tU8 Pu8TimingNode)
{
   tS32           Vs32ReturnCode=0;
   DIR           *VpDir;
   struct dirent *VpDEntry=NULL;
   tBool          VbFound=FALSE;


   if(PbSpecificTimingNode==TRUE) 
   {
     snprintf(PpcPathFile,EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT,"%s%d/",EARLY_CONFIG_DISPLAY_DEVICE_DIR_NODE_SPECIFIC,Pu8TimingNode);
     VpDir=opendir(PpcPathFile);
     if(VpDir==NULL)
     { 
       Vs32ReturnCode=-errno;
	     fprintf(stderr, "early_config_err: no path with '%s' found: error code %d\n",PpcPathFile,Vs32ReturnCode);  
     }
     else
     {
       snprintf(PpcPathFile,EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT,"%s%d/%s",EARLY_CONFIG_DISPLAY_DEVICE_DIR_NODE_SPECIFIC,Pu8TimingNode,EARLY_CONFIG_DISPLAY_FILE);
		   M_DEBUG_STR_STR("early_config: file %s \n",PpcPathFile);
     }
     closedir(VpDir);
   }
   else
   {//open directory
     VpDir=opendir(EARLY_CONFIG_DISPLAY_DIR);
     if(VpDir!=NULL)
     { //8.1.2016 => get display for defined node
       //search directory with "card0-XXX" 
       //precondition: only one directory exist 
	     //=> bueter: 01.09.2015 more directories could be available (JAC M6)
       //workaround JAC M6: the last directory, which found should be used   
       while( NULL != (VpDEntry = readdir(VpDir)))
       {
	       if(strncmp(&VpDEntry->d_name[0],EARLY_CONFIG_DISPLAY_DEVICE_NODE,strlen(EARLY_CONFIG_DISPLAY_DEVICE_NODE))==0)
	       {
	         if(VbFound==TRUE)
		       { //01.09.2015 workaround JAC M6: the last directory, which found should be used => deactivate error code
		         //Vs32ReturnCode=-ECANCELED;
		         //precondition that only one directory exist is invalid  => print error
		         fprintf(stderr, "early_config_err: more paths with 'card0_xxx' found: error code %d\n",Vs32ReturnCode); 
		         //01.09.2015 workaround JAC M6: the last directory, which found should be used  => get path 
		         snprintf(PpcPathFile,EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT,"%s%s/%s",EARLY_CONFIG_DISPLAY_DIR,&VpDEntry->d_name[0],EARLY_CONFIG_DISPLAY_FILE);
		         M_DEBUG_STR_STR("early_config: file %s \n",PpcPathFile);
		       }
		       else
		       {
             snprintf(PpcPathFile,EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT,"%s%s/%s",EARLY_CONFIG_DISPLAY_DIR,&VpDEntry->d_name[0],EARLY_CONFIG_DISPLAY_FILE);
		         M_DEBUG_STR_STR("early_config: file %s \n",PpcPathFile);
		         VbFound=TRUE;          
           }
         }
       }
	     closedir(VpDir);
	     if(VbFound==FALSE)
	     {
	       Vs32ReturnCode=-ENOMEDIUM;
	       fprintf(stderr, "early_config_err: no path with 'card0_xxx' found: error code %d\n",Vs32ReturnCode);         
	     }
     }
     else
     {
       Vs32ReturnCode=-errno; 
	     fprintf(stderr, "early_config_err: open dir %s failed:%d\n",EARLY_CONFIG_DISPLAY_DIR,Vs32ReturnCode);
     }
   }
   return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_GetMode()
*
* DESCRIPTION: read the string from file
*
* PARAMETERS: PpcPath: path
*             PpcBufRead: read buffer
*             PiSize: size   
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2014 06 18
*****************************************************************************/
static tS32 s32EarlyConfig_ReadStringFromFile(const char* PpcPath,const char* PpcBufRead,int PiSize)
{
   tS32 Vs32ReturnCode=0;	
   int  ViFile;
 
   /*open file*/ 
   ViFile = open(PpcPath, O_RDONLY );
   /* check file handle */
   if(ViFile < 0)
   { /*  err*/
     Vs32ReturnCode=-errno;  
     fprintf(stderr, "early_config_err: open file %s\n",PpcPath);
   }
   else
   { /*read string*/	  
     Vs32ReturnCode=read(ViFile,(void*)PpcBufRead,PiSize);
	 if(Vs32ReturnCode<0)
	 {
	   Vs32ReturnCode=-errno; 
	   fprintf(stderr, "early_config_err: %d read file %s \n",Vs32ReturnCode,PpcPath);	 
	 }	 		
	 else
	 {
       M_DEBUG_STR_STR("early_config: read string:\n'%s'\n",PpcBufRead);   
	 }
	 if (close(ViFile)<0) 
	 {	     
	   Vs32ReturnCode=-errno; 
	   fprintf(stderr, "early_config_err: %d close file %s\n",Vs32ReturnCode,PpcPath);
	 }  	 
   }
   return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_GetSizeStringOfFile()
*
* DESCRIPTION: get size of  the file
*
* PARAMETERS:  PpcPath: file(include path)
*
* RETURNS: size or error code
*
* HISTORY:Created by Andrea Bueter 2015 03 27
*****************************************************************************/
static tS32 s32EarlyConfig_GetSizeStringOfFile(const char* PpcPath)
{
   tS32 Vs32ReturnCode=0;	
   int  ViFile;

   /*open file*/ 
   ViFile = open(PpcPath, O_RDONLY );
   /* check file handle */
   if(ViFile < 0)
   { /*  err*/
     Vs32ReturnCode=-errno;  
     fprintf(stderr, "early_config_err: open file %s\n",PpcPath);
   }
   else
   {
     struct stat VFileStat;	    
	 /*get length of file*/
	 if(fstat(ViFile,&VFileStat) >= 0)
	 {/*check lenght*/
	   Vs32ReturnCode= (tS32) VFileStat.st_size;			
	 }
	 if (close(ViFile)<0) 
	 {	     
	   Vs32ReturnCode=-errno; 
	   fprintf(stderr, "early_config_err: %d close file %s\n",Vs32ReturnCode,PpcPath);
	 }  	 
   }
   return(Vs32ReturnCode);
}
#endif

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_SetMode()
*
* DESCRIPTION: write the mode string to the file
*
* PARAMETERS:  PpcPath:         path
*              PpcFilename:     filename
*              PpcBufWriteMode: mode string
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2014 07 07
*****************************************************************************/
static tS32 s32EarlyConfig_SetMode(const char* PpcBufWriteMode,char* PpcPath,const char* PpcFilename)
{
  tS32 Vs32ReturnCode=0;	
  char VcPathFile[EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT];

  sprintf(VcPathFile,"%s/%s", PpcPath, PpcFilename);
  M_DEBUG_STR_STR("early_config: file %s \n",VcPathFile);
  Vs32ReturnCode=s32EarlyConfig_WriteStringToFile(VcPathFile,PpcBufWriteMode);
  if(Vs32ReturnCode>=0)
  {
    M_DEBUG_MODE(PpcFilename,PpcBufWriteMode);
  }
  else
  {//err memory entry
    if(strncmp(EARLY_CONFIG_FILE_BCMODE,PpcFilename,strlen(PpcFilename))==0)
	  {
	    fprintf(stderr, "early_config_err: set BCMode: error code %d\n",Vs32ReturnCode);
      vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_WRITE_BC_MODE,(const tU8*)&Vs32ReturnCode,sizeof(Vs32ReturnCode));	   
	  }
	  else
	  {
	    fprintf(stderr, "early_config_err: set LFMode: error code %d\n",Vs32ReturnCode);
      vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_WRITE_LF_MODE,(const tU8*)&Vs32ReturnCode,sizeof(Vs32ReturnCode));	    
	  }
  }
  return(Vs32ReturnCode);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_WriteStringToFile()
*
* DESCRIPTION: write string to file
*
* PARAMETERS:  PpcPath: file (include path)
*              PpcBufWrite: write buffer
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2014 07 07
*****************************************************************************/
static tS32 s32EarlyConfig_WriteStringToFile(const char* PpcPath,const char* PpcBufWrite)
{
  tS32 Vs32ReturnCode=0;	
  int  ViFile;

  /*open file*/ 
  ViFile = open(PpcPath, O_WRONLY /*O_RDWR*/ );
  /* check file handle */
  if(ViFile < 0)
  { /*  err*/
    Vs32ReturnCode=-errno;  
    fprintf(stderr, "early_config_err: open file %s\n",PpcPath);
  }
  else
  { /*write mode*/	  
    Vs32ReturnCode=write(ViFile,(const void*)PpcBufWrite,strlen(PpcBufWrite)+1);
    if(Vs32ReturnCode<0)
	  {
	    Vs32ReturnCode=-errno; 
	    fprintf(stderr, "early_config_err: %d write file %s\n",Vs32ReturnCode,PpcPath);	 
	  }	 
	  else
	  {
	    M_DEBUG_STR_STR("early_config: write string: '%s'\n",PpcBufWrite);
 	  }
	  if (close(ViFile)<0) 
	  {	     
	    Vs32ReturnCode=-errno; 
	    fprintf(stderr, "early_config_err: %d close file %s\n",Vs32ReturnCode,PpcPath);
	  }  	 
  }
  return(Vs32ReturnCode);
}

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_GetPathForModeSetting()
*
* DESCRIPTION: determine the path for lfmode od bcmode;
*              First search in device tree the file compatible.
*              Check if the file gets the information about the driver.
*              Extract the path.
*              Search in sysfs for the path of teh file lfmode bcmode
*
* PARAMETERS:  PpcPath: buffer for the path of the modes
*
* RETURNS: success or error code
*
* HISTORY:Created by Andrea Bueter 2015 29 04
*****************************************************************************/
static tS32 s32EarlyConfig_GetPathForModeSetting(char* PpcPath)
{
  tS32 Vs32ReturnCode=0;	

  Vs32ReturnCode=s32EarlyConfig_ScanDir("/proc/device-tree",s32EarlyConfig_CheckFileCompatibleAndGetExtractPath,PpcPath);
  if(Vs32ReturnCode==0)
  {
    Vs32ReturnCode=s32EarlyConfig_ScanDir("/sys/devices",s32EarlyConfig_CheckFileModesAndGetPathMode,PpcPath);	
  }
  M_DEBUG_STR_STR("early_config: path %s/ \n",PpcPath);
  return(Vs32ReturnCode);
}

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_ScanDir()
*
* DESCRIPTION: scan the directory and subdirectories for files.
*              For each file call a function to check the content.
*              The function pointer is given by the parameter.
*              
*
* PARAMETERS:  PpcDirName: direction name to scan
*              PptrFuncCheck: function pointer call for the each file
*              PpStrResult: result string 
*
* RETURNS:     <0 error
*              0: success
*              1: nothing found
*
* HISTORY:Created by Andrea Bueter 2015 12 05
*****************************************************************************/
tS32 s32EarlyConfig_ScanDir(const char *PpcDirName, void *PptrFuncCheck, char *PpStrResult)
{
  tS32 Vs32Return=2;
  DIR *VspDir; 
  int (*VptrFuncCheck)(const char*, const char *, char *) = PptrFuncCheck;

  /* Open the directory specified by "PpcDirName". */
  VspDir = opendir (PpcDirName);
  /* Check it was opened. */
  if (VspDir==NULL) 
  {        
    Vs32Return=-errno; 
	fprintf (stderr,"early_config_err:Cannot open directory '%s': %s\n", PpcDirName, strerror (errno));
  }
  while(Vs32Return==2) 
  {
    struct dirent* VpcDirEntry;
    const char*    VpcNameEntry;
    /* "Readdir" gets subsequent entries from "d". */
    VpcDirEntry = readdir(VspDir);
    if (VpcDirEntry==NULL) 
	{/* There are no more entries in this directory, so break out of the while loop. */
      Vs32Return=1;
    }
	else
	{
	  VpcNameEntry = VpcDirEntry->d_name;
	  //if file
      if((!(VpcDirEntry->d_type & DT_DIR))&&(PptrFuncCheck!=NULL)&&(VptrFuncCheck!=NULL)) 
	  {
        Vs32Return=VptrFuncCheck(PpcDirName, VpcNameEntry, PpStrResult);	 
	  }
      //directory
	  if(Vs32Return==2)
	  {
        if (VpcDirEntry->d_type & DT_DIR)
	    { /* Check that the directory is not "d" or d's parent. */
          if ((strcmp (VpcNameEntry, "..") != 0) && (strcmp (VpcNameEntry, ".") != 0)) 
		  {
            tS32 Vs32PathLength;
            char VcPath[EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT];
            Vs32PathLength = snprintf (VcPath, EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT,"%s/%s", PpcDirName, VpcNameEntry);
            if (Vs32PathLength >= EARLY_CONFIG_DISPLAY_MAX_PATH_LENGHT) 
			{
			  Vs32Return=-EACCES;
              fprintf (stderr,"early_config_err:Path length has got too long.\n");             
            }
		    else
			{ /* Recursively call "list_dir" with the new path. */
              if(s32EarlyConfig_ScanDir(VcPath, PptrFuncCheck, PpStrResult)==0)
			  {
			    Vs32Return=0;   
			  }
			}
		  }
		}
	  }
	}/*end else readdir*/
  }/*end while*/
  /* After going through all the entries, close the directory. */
  if (closedir(VspDir)) 
  {
    Vs32Return=-errno; 
    fprintf(stderr,"early_config_err: Could not close '%s': %s\n",PpcDirName, strerror (errno));
  }
  return(Vs32Return);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_CheckFileCompatibleAndGetExtractPath()
*
* DESCRIPTION: If the given file the compatible file, check if file describes the driver.
*              Extract from the path of the compatible file, the needed strinf for find the modes files 
*              in the sysfs.          
*
* PARAMETERS:  PpcStrPath: path to the file
*              PpcStrFileName: file name
*              PpStrExtractPath: buffer fro the extract path
*
* RETURNS:    0: success string found
*             2: string not found
*            -1: error: file could not open
*
* HISTORY:Created by Andrea Bueter 2015 12 05
*****************************************************************************/
static tS32 s32EarlyConfig_CheckFileCompatibleAndGetExtractPath(const char *PpcStrPath, const char *PpcStrFileName, char *PpStrExtractPath)
{
  tS32  Vs32Return=2;
  char VcBuffer[PATH_MAX];
  char VcStrPath[PATH_MAX];
  //compare filename wit compatible
  if (strcmp(PpcStrFileName, EARLY_CONFIG_FILE_COMPATIBLE ) == 0 ) 
  { //get complete path
    sprintf (VcStrPath,"%s/%s", PpcStrPath, PpcStrFileName);
	//open path
    int ViFile = open(VcStrPath, O_RDONLY);
    if (ViFile>-1) 
	{  //read file
      read(ViFile,VcBuffer, PATH_MAX );
      if (strcmp(VcBuffer, EARLY_CONFIG_DRIVER )==0) 
	  {// if file decribes the driver => extract path 
        vEarlyConfig_GetMidStr(VcStrPath,'/',5);
        vEarlyConfig_SwapStr(VcStrPath,'@','.');
        vEarlyConfig_RemoveLeadingZerosFromStr(VcStrPath);
        strcpy(PpStrExtractPath,VcStrPath);
        Vs32Return = 0;  //=> success
      }
      close(ViFile);
    } 
	else 
	{
	  fprintf(stderr,"early_config_err: error open (%s)\n",PpStrExtractPath);
      Vs32Return=-1;
    }
  }
  return(Vs32Return);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_CheckFileModesAndGetPathMode()
*
* DESCRIPTION: if file name equal "bcmode" or "lfmode", search in path the 
*              string given form the device tree path.
*              if found copy the sysfs path to PpStrResult.
*              
*
* PARAMETERS:  PpcStrPath: path to the file
*              PpcStrFileName: file name
*              PpStrResult: buffer for the string in device tree
*                           buffer for the path is sysfs for the mode
*
* RETURNS:    0: success string found
*             2: string not found
*
* HISTORY:Created by Andrea Bueter 2015 12 05
*****************************************************************************/
static tS32 s32EarlyConfig_CheckFileModesAndGetPathMode(const char *PpcStrPath, const char *PpcStrFileName, char *PpStrResult)
{
  tS32  Vs32Return=2;
  //check if file BCMode or LFMode
  if((strcmp(PpcStrFileName, EARLY_CONFIG_FILE_BCMODE )== 0)||(strcmp(PpcStrFileName,EARLY_CONFIG_FILE_LFMODE)==0))
  { //search first char of PpStrResult in VcpPath  
    const char* VcpPos=strchr(PpcStrPath,PpStrResult[0]);
	while(VcpPos!=NULL)
	{
      if (strncmp(VcpPos,PpStrResult,strlen(PpStrResult)) == 0) 
	  {
	    strcpy(PpStrResult,PpcStrPath);	
	    Vs32Return = 0;
		break;
	  }
	  else
	  {
	    VcpPos++;
	    VcpPos=strchr(VcpPos,PpStrResult[0]);
	  }	 
    }
  }
  return(Vs32Return);
}


/******************************************************************************
* FUNCTION: tS32 vEarlyConfig_GetMidStr()
*
* DESCRIPTION: helper function for get the string between delimiter
*              
* PARAMETERS:  PpcBuffer: buffer of the string
*              PcDelimiter: character of the delimiter     
*              PiPos: position of the delimiter from which the string should be copied.
*
*
* HISTORY:Created by Andrea Bueter 2015 12 05
*****************************************************************************/
void vEarlyConfig_GetMidStr(char *PpcBuffer, char PcDelimiter, int PiPos)
{
  int ViInc,ViIncOut,ViFoundDelimiter,ViStartCopy;
  int ViLength = strlen(PpcBuffer);

  ViInc = ViIncOut = ViFoundDelimiter = ViStartCopy = 0;

  while( ViInc < ViLength) 
  {
    if (ViStartCopy) 
	{
        PpcBuffer[ViIncOut++] = PpcBuffer[ViInc];
    }
    if (PpcBuffer[ViInc] == PcDelimiter) 
	{
      ++ViFoundDelimiter;
      if (ViStartCopy && (PpcBuffer[ViInc] == PcDelimiter))
	  {//end 
        PpcBuffer[ViIncOut-1] = 0x00;
        break;
      }
      if (ViFoundDelimiter == PiPos) 
	  {//begin
        ViStartCopy = 1;
      }
    }
    ++ViInc;
  }
}
/******************************************************************************
* FUNCTION: void vEarlyConfig_SwapStr()
*
* DESCRIPTION: helper function for swap the string. 
*              First search PcDelimiter. 
*              Then get the last part of the string. 
*              Add the new delimiter and copy the first string to the new string            
*
* PARAMETERS:  PpcBuffer: string, which should be swaped
*              PcDelimiter: delimiter in the given string
*              PcDelimiterNew: new delimiter 
*  
*
* HISTORY:Created by Andrea Bueter 2015 12 05
*****************************************************************************/
void vEarlyConfig_SwapStr(char *PpcBuffer, char PcDelimiter, char PcDelimiterNew)
{
  char VcBufTmp[PATH_MAX];
  int ViInc, ViStartCopy, ViIncOut;
  int ViLength = strlen(PpcBuffer);

  ViInc = ViStartCopy = ViIncOut = 0;
  while (ViInc < ViLength) 
  {
    if (ViStartCopy) 
	{ //get last part of the string
      VcBufTmp[ViIncOut++] = PpcBuffer[ViInc];
    }
    if (PpcBuffer[ViInc]==PcDelimiter) 
	{ //begin copy
      ViStartCopy = ViInc;
    }
    ++ViInc;
  }
  //add new delimiter
  VcBufTmp[ViIncOut++] = PcDelimiterNew;
  // add first part of the tsring
  ViInc = 0;
  while (ViInc<ViStartCopy) 
  {
    VcBufTmp[ViIncOut++] = PpcBuffer[ViInc++];
  }
  VcBufTmp[ViIncOut] = 0x00;
  //copy string back
  strcpy(PpcBuffer,VcBufTmp);
}
/******************************************************************************
* FUNCTION: void vEarlyConfig_RemoveLeadingZerosFromStr()
*
* DESCRIPTION: helper function for remove leading zeros from the string.           
*
* PARAMETERS:  PpcBuffer: string, which should be changed
*  
*
* HISTORY:Created by Andrea Bueter 2015 12 05
*****************************************************************************/
void vEarlyConfig_RemoveLeadingZerosFromStr(char *PpcBuffer)
{
  int ViInc = 0;
  int ViIncOut = 0;
  int ViLength = strlen(PpcBuffer);

  while (ViInc < ViLength) 
  {
    PpcBuffer[ViIncOut++] = PpcBuffer[ViInc];
    if ((PpcBuffer[ViInc] == '0') && (ViIncOut==1))
      ViIncOut = 0;
    ++ViInc;
  }
  PpcBuffer[ViIncOut] = 0x00;
}




