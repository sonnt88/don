/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        early_config_csc.c
 * 
 * global function:

 *        
 * local function:

 *        create framebuffer;
 *
 * @date        2015-12-21
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "system_types.h"
#include "system_definition.h"
#include "early_config_private.h"

#ifdef EARYL_CONFIG_USE_CSC_LIB
/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/     
static const char device_name[] = "/dev/dri/card0";

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
/******************************************************************************
* FUNCTION: vEarlyConfig_CSCInit
* 
* DESCRIPTION: init structure PtsEarlyConfigCSC
*                  --- call csc_set_default_value to get the default value
* RETURNS: svoid
*
* HISTORY:Created by Andrea Bueter 2015 12 22
*****************************************************************************/
void vEarlyConfig_CSCInit(tsEarlyConfig_CSC* PtsEarlyConfigCSC)
{
   /* Pass cprop structure in turn library will give back default color property
	    value and CSC identity matrix  */
	csc_set_default_value(&PtsEarlyConfigCSC->tsCscProp);
  PtsEarlyConfigCSC->tU8Profile=0xff;
  PtsEarlyConfigCSC->tF64GammaValue=1.0;
}

/******************************************************************************
* FUNCTION: s32EarlyConfig_CSCAndGammaSet
* 
* DESCRIPTION: set CSC and gamma value
*               
* RETURNS: 0: success; <0: error code
*
* HISTORY:Created by Andrea Bueter 2015 12 22
*****************************************************************************/
tS32 s32EarlyConfig_CSCAndGammaSet(tsEarlyConfig_CSC* PtsEarlyConfigCSC)
{
  tS32          Vs32ReturnCode=0;
  tU32          Vu32CrtcId;
  tBool         VbUpdate=true;
  CLRSPACETYPE  VtsType;
  int ViDrmFd = open(device_name, (O_RDWR | O_CLOEXEC));
	if (ViDrmFd < 0)
	{
    Vs32ReturnCode=-errno;
		fprintf(stderr, "early_config_err: %d open() fails\n",Vs32ReturnCode);
	}
  else
  { //----------   Update drm_imx_csc   ----------------------
	  /* By default crtc id will be retrieved by library interface (libipu_dp_csc.so) 
       if crtc id passed with value "0xFF" for the first time 
       and also supported crtc id's will be provided to application else 
       library will use the crtc id passed by the application.*/
	  Vu32CrtcId = INVALID_VALUE;
	  csc_update_crtc_id (Vu32CrtcId);
    /* By default CSC type will be "DRM_IMX_CSC_RGB2RGB" if type passed with value "0xFF" 
       else library will use type passed by the application.*/
	  VtsType = (CLRSPACETYPE) INVALID_VALUE;
	  csc_update_color_space_conversion_type(VtsType);
    /*compute and update IPU DP CSC matrix*/ 
    Vs32ReturnCode = csc_dp_compute_update_matrix (ViDrmFd, (bool)VbUpdate, &PtsEarlyConfigCSC->tsCscProp);
    if(Vs32ReturnCode<0)
    {
      fprintf(stderr, "early_config_err: %d csc_dp_compute_update_matrix() fails\n",Vs32ReturnCode);
      vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_SET_CSC,(const tU8*)&Vs32ReturnCode,sizeof(Vs32ReturnCode));  
    }    
    //----------   set gamma correction  ----------------------
    {
      tS32 Vs32Index = 0;
      tF64 VF64Corrected =0;
      tF64 VF64InvGamma = 1.0 / PtsEarlyConfigCSC->tF64GammaValue;
      tU16 Vu16Lum[DRM_IMX_GAMMA_SIZE] = 
      {
         0, 2, 4, 8, 16, 32, 64, 96,
         128, 160, 192, 224, 256, 288, 320, 352,
      };
      for (Vs32Index = 0; Vs32Index < DRM_IMX_GAMMA_SIZE; Vs32Index++) 
      {
        VF64Corrected = pow((tF64)Vu16Lum[Vs32Index], VF64InvGamma);
        Vu16Lum[Vs32Index] = (tU16)(round(VF64Corrected));
        /*[EF] According to spec one must still normalize the values to 255:*/
        if (Vu16Lum[Vs32Index] > 255)
          Vu16Lum[Vs32Index] = 255;
      }
      #ifdef EARLY_CONFIG_DEBUG
      fprintf(stderr, "early_config: gamma corrected values: %d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",Vu16Lum[0],Vu16Lum[1],Vu16Lum[2],Vu16Lum[3],Vu16Lum[4],Vu16Lum[5],Vu16Lum[6],Vu16Lum[7],
                                                                                                                Vu16Lum[8],Vu16Lum[9],Vu16Lum[10],Vu16Lum[11],Vu16Lum[12],Vu16Lum[13],Vu16Lum[14],Vu16Lum[15]);
      #endif
      //get new Vu32CrtcId
      Vu32CrtcId = PtsEarlyConfigCSC->tsCscProp.supported_crtc_id[0];
      csc_update_crtc_id(Vu32CrtcId);
      tS32 Vs32Error=drmModeCrtcSetGamma(ViDrmFd,Vu32CrtcId,DRM_IMX_GAMMA_SIZE,Vu16Lum, Vu16Lum, Vu16Lum);
      if(Vs32Error<0)
      {       
        fprintf(stderr, "early_config_err:%d errno:%d drmModeCrtcSetGamma() fails\n",Vs32ReturnCode,errno);
        vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_SET_GAMMA,(const tU8*)&errno,sizeof(errno));  
        /*set error code*/
        Vs32ReturnCode|=-errno;
      }
    }
    /*drop master*/
	  if(drmDropMaster(ViDrmFd)<0)
	  { 
		  fprintf(stderr, "early_config_err:%d: drmDropMaster() failed\n", errno);
		}		  
    //close(ViDrmFd); do not close the device
  }
  return(Vs32ReturnCode);
}
#endif

