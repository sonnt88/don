/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        early_config_drm.c
 *
 * This file contains the access to DRM modesetting
 * look to https://github.com/dvdhrm/docs/blob/master/drm-howto/modeset.c
 *         http://www.linuxhowtos.org/manpages/7/drm-kms.htm
 * 
 * global function:
 * -- s32EarlyConfig_DrmResolution():
 *      set/get the resolution    
 *        
 * local function:
 * -- s32EarlyConfig_DRMOpen():    
 *      This function opens the DRM character device.
 * -- s32EarlyConfig_DRMPrepare():
 *      for all connectors get resource information 
 * -- s32EarlyConfig_DRMSetupDev():
 *      This function finds the possible resolution and refresh-rate
 *      for one connector.
 * -- s32EarlyConfig_DRMCheckResolutionSupported():
 *      This function checks if the connector supports the requested mode
 * -- s32EarlyConfig_DRMFindCtrc():
 *       This function tries to find a suitable CRTC for the given connector. 
 * -- s32EarlyConfig_DRMCreateFb(): 
 *        create framebuffer;
 *
 * @date        2014-07-07
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "system_types.h"
#include "system_definition.h"
#include "early_config_private.h"


/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/     
#ifndef EARLY_CONFIG_SET_DISPLAY_CONFIG_TO_FILE
/*possible connector types; 07/2014 test with DRM_MODE_CONNECTOR_LVDS*/
static const char * connector_type[] = 
{
	[DRM_MODE_CONNECTOR_Unknown] =     "Unknown",
	[DRM_MODE_CONNECTOR_VGA] =         "VGA",
	[DRM_MODE_CONNECTOR_DVII] =        "DVII",
	[DRM_MODE_CONNECTOR_DVID] =        "DVID",
	[DRM_MODE_CONNECTOR_DVIA] =        "DVIA",
	[DRM_MODE_CONNECTOR_Composite] =   "Composite",
	[DRM_MODE_CONNECTOR_SVIDEO] =      "SVIDEO",
	[DRM_MODE_CONNECTOR_LVDS] =        "LVDS",
	[DRM_MODE_CONNECTOR_Component] =   "Component",
	[DRM_MODE_CONNECTOR_9PinDIN] =     "9PinDIN",
	[DRM_MODE_CONNECTOR_DisplayPort] = "DisplayPort",
	[DRM_MODE_CONNECTOR_HDMIA] =       "HDMIA",
	[DRM_MODE_CONNECTOR_HDMIB] =       "HDMIB",
	[DRM_MODE_CONNECTOR_TV] =          "TV",
	[DRM_MODE_CONNECTOR_eDP] =         "eDP",
};

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static tS32 s32EarlyConfig_DRMOpen(tsEarlyConfig_DRMAccess* PptsDRMAccess);
static tS32 s32EarlyConfig_DRMOpen(tsEarlyConfig_DRMAccess* PptsDRMAccess);
static tS32 s32EarlyConfig_DRMPrepare(tsEarlyConfig_Info *PpsEarlyConfigInfo);
static tS32 s32EarlyConfig_DRMSetupDev(drmModeRes *PtpResources, drmModeConnector *PtpConnector, 
                                       struct tsEarlyConfig_DrmDisplayDev *PtsDev,tsEarlyConfig_Info *PpsEarlyConfigInfo);
static tS32 s32EarlyConfig_DRMCheckResolutionSupported(drmModeConnector *PtpConnector,tsEarlyConfig_Info *PpsEarlyConfigInfo);
static tS32 s32EarlyConfig_DRMGetResolutionSupported(drmModeConnector *PtpConnector,tsEarlyConfig_Info *PpsEarlyConfigInfo);
static tS32 s32EarlyConfig_DRMFindCtrc(drmModeRes *PtpResources, drmModeConnector *PtpConnector, 
                                       struct tsEarlyConfig_DrmDisplayDev *PtsDev,tsEarlyConfig_Info *PpsEarlyConfigInfo);
static tS32 s32EarlyConfig_DRMCreateFb(tS32 Ps32Fd, struct tsEarlyConfig_DrmDisplayDev *PtsDev);

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_DrmResolution()
*
* DESCRIPTION: set the resolution 
*
* PARAMETERS:
*         PpsEarlyConfigInfo: info structure of the process
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2014 06 24
*****************************************************************************/
tS32 s32EarlyConfig_DrmResolution(tsEarlyConfig_Info *PpsEarlyConfigInfo)
{
	tS32                                Vs32Return=0;
	struct tsEarlyConfig_DrmDisplayDev *VtpDevIter;

	/*copy name of charater device to info structure*/
	memset(PpsEarlyConfigInfo->tsDRMAccess.cDevName,0,EARLY_CONFIG_DISPLAY_DEVICE_NAME_LENGHT);
	strncpy(PpsEarlyConfigInfo->tsDRMAccess.cDevName,EARLY_CONFIG_DISPLAY_DEVICE_NODE_NAME,EARLY_CONFIG_DISPLAY_DEVICE_NAME_LENGHT);

	M_DEBUG_STR_STR("early_config: using card '%s'\n", PpsEarlyConfigInfo->tsDRMAccess.cDevName);

	/* open the DRM device */
	Vs32Return = s32EarlyConfig_DRMOpen(&PpsEarlyConfigInfo->tsDRMAccess);
	if(Vs32Return!=0)
	{
	  fprintf(stderr, "early_config_err:%d: s32EarlyConfig_DRMOpen() failed\n", Vs32Return);
	}
	else
	{ /*set master*/
	    Vs32Return=drmSetMaster(PpsEarlyConfigInfo->tsDRMAccess.s32Fd);	 
	    if(Vs32Return!=0)
	    {
	      Vs32Return=-errno;
	      fprintf(stderr, "early_config_err:%d: drmSetMaster() failed\n", Vs32Return);
	    }
	    else
	    {/* prepare all connectors and CRTCs */
	      PpsEarlyConfigInfo->tsDRMAccess.tspDisplayDevList=NULL;	      
	      Vs32Return = s32EarlyConfig_DRMPrepare(PpsEarlyConfigInfo);
	      if (Vs32Return==0)
	      { /* perform actual modesetting on each found connector+CRTC */
	        for (VtpDevIter = PpsEarlyConfigInfo->tsDRMAccess.tspDisplayDevList; VtpDevIter; VtpDevIter = VtpDevIter->tpNext) 
	        {
	  	      VtpDevIter->tpSavedCrtc = drmModeGetCrtc(PpsEarlyConfigInfo->tsDRMAccess.s32Fd, VtpDevIter->u32CrtcId);
		      M_DEBUG_STR_4VALUES("current mode crtc %d: %dx%d@%uHz\n",VtpDevIter->u32CrtcId,VtpDevIter->tpSavedCrtc->mode.hdisplay,VtpDevIter->tpSavedCrtc->mode.vdisplay,VtpDevIter->tpSavedCrtc->mode.vrefresh);
		      M_DEBUG_STR_5VALUES("setting mode %u crtc %d: %dx%d@%uHz\n",PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode, VtpDevIter->u32CrtcId,VtpDevIter->tMode.hdisplay,VtpDevIter->tMode.vdisplay,VtpDevIter->tMode.vrefresh);
	          Vs32Return = drmModeSetCrtc(PpsEarlyConfigInfo->tsDRMAccess.s32Fd, VtpDevIter->u32CrtcId, VtpDevIter->u32Fb, 0, 0,&VtpDevIter->u32ConnId, 1, &VtpDevIter->tMode);
		      if (Vs32Return!=0)
		      {
			    Vs32Return=-errno;
		        fprintf(stderr, "early_config_err:%d set CRTC for connector %u\n",Vs32Return,VtpDevIter->u32ConnId);
		      }
	        }/*end for*/
		  }
          tS32 Vs32Error=drmDropMaster(PpsEarlyConfigInfo->tsDRMAccess.s32Fd);	 
	      if(Vs32Error!=0)
	      { 
		    fprintf(stderr, "early_config_err:%d: drmDropMaster() failed\n", Vs32Error);
		  }		  
		}
	}
	//check error
	if(Vs32Return<0)
	{
	  if(PpsEarlyConfigInfo->tsDRMAccess.eKindAction==EARLY_CONFIG_KIND_ACTION_SET)
	  {
	    fprintf(stderr, "early_config_err: %d set resolution\n",Vs32Return);
        vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_SET_RESOLUTION,(const tU8*)&Vs32Return,sizeof(Vs32Return));		
	  }
	  else
	  {
	    fprintf(stderr,"early_config_err:%d get and set resolution fails\n",Vs32Return);
	    vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_GET_SET_RESOLTION,(const tU8*)&Vs32Return,sizeof(Vs32Return));	  
	  }
	}
	else
	{
	   M_DEBUG_STR_STR("early_config: Resolution:%s\n",PpsEarlyConfigInfo->tsPddDisplay.tResolution.cResolution);
	}
	return(Vs32Return);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_DRMOpen()
*
* DESCRIPTION: This function opens the DRM character device. 
*              The name of the device is saved in the structure tsEarlyConfig_DRMAccess.
*              This function saved the handle in the structure.
*              After opening the file, we also check for the DRM_CAP_DUMB_BUFFER capability.
*              If the driver supports this capability, we can create simple memory-mapped
*              buffers without any driver-dependent code. 
* PARAMETERS:
*     PptsDRMAccess: pointer to information of DRM access
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2014 06 24
*****************************************************************************/
static tS32 s32EarlyConfig_DRMOpen(tsEarlyConfig_DRMAccess* PptsDRMAccess)
{
  tS32         Vs32Return=0;
  uint64_t     Vu64HasDumb;
  
  PptsDRMAccess->s32Fd= open(PptsDRMAccess->cDevName, O_RDWR | O_CLOEXEC);
  if (PptsDRMAccess->s32Fd < 0) 
  {
	Vs32Return = -errno;
	fprintf(stderr,"early_config_err:%d open '%s'\n",Vs32Return, PptsDRMAccess->cDevName);
  }
  else
  {
	if (drmGetCap(PptsDRMAccess->s32Fd, (uint64_t)DRM_CAP_DUMB_BUFFER, &Vu64HasDumb) < 0 ||!Vu64HasDumb) 
	{
		fprintf(stderr,"early_config_err: drm device '%s' does not support dumb buffers\n",PptsDRMAccess->cDevName);
		//close(PptsDRMAccess->s32Fd);
		Vs32Return=-EOPNOTSUPP;
	}
  }
  return(Vs32Return);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_DRMPrepare()
*
* DESCRIPTION: for all connector get resource information 
*              ( 07/2014 tested only for one connector (LVDS))
*
* PARAMETERS:
*      PpsEarlyConfigInfo: info structure of the process
*
* RETURNS:  success(0) or error code(<0)
*
* NOTE: The function s32EarlyConfig_DRMSetupDev() returns -ENOENT if the connector is currently
*       unused and no monitor is plugged in. So we can ignore this connector.
*
* HISTORY:Created by Andrea Bueter 2014 06 24
*****************************************************************************/
static tS32 s32EarlyConfig_DRMPrepare(tsEarlyConfig_Info *PpsEarlyConfigInfo)
{
  tS32                                      Vs32Return=0;
  drmModeRes                                *VtpResources;
  struct tsEarlyConfig_DrmDisplayDev        *VtsDev;

  /* retrieve resources */
  VtpResources = drmModeGetResources(PpsEarlyConfigInfo->tsDRMAccess.s32Fd);
  if (!VtpResources) 
  {
    Vs32Return=-errno;
    fprintf(stderr,"early_config_err:%d retrieve DRM resources\n",Vs32Return);
  }
  else
  { /* iterate all connectors */
    tS32  Vs32Count;
	if(VtpResources->count_connectors==0)
	{
	  Vs32Return=-ENXIO; /* 6 No such device or address */
	  fprintf(stderr, "early_config_err:%d no connector found",Vs32Return);
	}
	else
	{
	  for (Vs32Count = 0; Vs32Count < VtpResources->count_connectors; Vs32Count++) 
	  { /* get information for each connector */
	    drmModeConnector *VtpConnector = drmModeGetConnector(PpsEarlyConfigInfo->tsDRMAccess.s32Fd, VtpResources->connectors[Vs32Count]);
	    if (!VtpConnector) 
	    {
		  Vs32Return=-errno;
	      fprintf(stderr,"early_config:%d cannot retrieve DRM connector %u:%u\n",Vs32Return,Vs32Count, VtpResources->connectors[Vs32Count]);
	    }
	    else
	    {  /* create a device structure */
		   tS32 Vs32Size=sizeof(struct tsEarlyConfig_DrmDisplayDev);
		   VtsDev = malloc(Vs32Size);
		   if(VtsDev==NULL)
		   {
		     Vs32Return=-ENOMEM;
		     fprintf(stderr, "early_config_err:%d malloc()",Vs32Return);
		     break;
		   }
		   else
		   {
		     memset(VtsDev, 0, Vs32Size);
		     VtsDev->u32ConnId = VtpConnector->connector_id;
		     /* call helper function to prepare this connector */
		     Vs32Return = s32EarlyConfig_DRMSetupDev(VtpResources, VtpConnector, VtsDev,PpsEarlyConfigInfo);
		     if (Vs32Return!=0) 
		     {
		       if (Vs32Return != -ENOENT) 
		       {
		         Vs32Return=-errno;
		       }
		       free(VtsDev);
		       drmModeFreeConnector(VtpConnector);
		     }
		     else
		     { /* free connector data and link device into global list */
		       drmModeFreeConnector(VtpConnector);
		       VtsDev->tpNext = PpsEarlyConfigInfo->tsDRMAccess.tspDisplayDevList;
		       PpsEarlyConfigInfo->tsDRMAccess.tspDisplayDevList = VtsDev;
			   Vs32Return=0;
		     }
		   }
	    }
	  }/*end for*/
	}
	/* free resources again */
	drmModeFreeResources(VtpResources);
  }
  return(Vs32Return);
}

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_DRMSetupDev()
*
* DESCRIPTION: This function finds the possible resolution and refresh-rate
*              for one connector.
*              Therfore a CTRC has to find for the connector. 
*              A CRTC is an internal resource of each graphic card. 
*              With the CRTC you can set the mode.
*
* PARAMETERS:
*         PtpResources: pointer to the resource
*         PtpConnector: pointer to the connector
*         PtsDev:       pointer to information to connector device
*         PpsEarlyConfigInfo: info structure of the process 
*
* RETURNS: success(0) or error code(<0)
*
* HISTORY:Created by Andrea Bueter 2014 06 24
*****************************************************************************/
static tS32 s32EarlyConfig_DRMSetupDev(drmModeRes *PtpResources, drmModeConnector *PtpConnector, 
                                       struct tsEarlyConfig_DrmDisplayDev *PtsDev,tsEarlyConfig_Info *PpsEarlyConfigInfo)
{
  tS32 Vs32Return=0;
  /* check if a monitor is connected */
  if (PtpConnector->connection != DRM_MODE_CONNECTED) 
  {
    Vs32Return=-ENOENT;
    fprintf(stderr, "early_config_err:%d ignoring unused connector %u\n",Vs32Return,PtpConnector->connector_id);
	
  }
  else
  {	/* check if there is at least one valid mode */
	if (PtpConnector->count_modes == 0) 
	{
	  Vs32Return=-EFAULT;
	  fprintf(stderr, "early_config_err:%d no valid mode for connector %u\n",Vs32Return,PtpConnector->connector_id);
	}
	else
	{     
	  PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode=0;  //for action EARLY_CONFIG_KIND_ACTION_GET => get the first mode and try to get CTRC
	  if(PpsEarlyConfigInfo->tsDRMAccess.eKindAction==EARLY_CONFIG_KIND_ACTION_SET)
	  { // this function set PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode
	    Vs32Return=s32EarlyConfig_DRMCheckResolutionSupported(PtpConnector,PpsEarlyConfigInfo);		
	  }
	  else
	  {//EARLY_CONFIG_KIND_ACTION_GET_SET
	    Vs32Return=s32EarlyConfig_DRMGetResolutionSupported(PtpConnector,PpsEarlyConfigInfo);
	  }

	  if(Vs32Return>=0)
	  {
	    memcpy(&PtsDev->tMode, &PtpConnector->modes[PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode], sizeof(PtsDev->tMode));
	    PtsDev->u32Width = PtpConnector->modes[PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode].hdisplay;
	    PtsDev->u32Height = PtpConnector->modes[PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode].vdisplay;	
	    /* find a crtc for this connector */
	    Vs32Return = s32EarlyConfig_DRMFindCtrc(PtpResources, PtpConnector, PtsDev,PpsEarlyConfigInfo);
	    if (Vs32Return) 
	    {		 
		  fprintf(stderr, "early_config_err:%d no valid crtc for connector %u\n", Vs32Return,PtpConnector->connector_id);
	    }
	    else
	    { /* create a framebuffer for this CRTC */
	      Vs32Return = s32EarlyConfig_DRMCreateFb(PpsEarlyConfigInfo->tsDRMAccess.s32Fd, PtsDev);
	      if (Vs32Return) 
		  {
		    fprintf(stderr, "early_config_err:%d create framebuffer for connector %u\n",Vs32Return,PtpConnector->connector_id);
		  }
	    }
	  }
    }
  }
  return(Vs32Return);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_DRMCheckResolutionSupported()
*
* DESCRIPTION: This function checks if the connector supports the requested mode
*
* PARAMETERS:
*         PtpConnector: pointer to the connector
*         PpsEarlyConfigInfo: info structure of the process  
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 0 01
*****************************************************************************/
static tS32 s32EarlyConfig_DRMCheckResolutionSupported(drmModeConnector *PtpConnector,tsEarlyConfig_Info *PpsEarlyConfigInfo)
{
  tS32   Vs32Return=0;
  tS32   Vs32Count;
  tBool  VbFound=FALSE;
  M_DEBUG_STR_VALUE_STR("\nearly_config: Supported modes for connector %u (type %s):\n",PtpConnector->connector_id,connector_type[PtpConnector->connector_type]);
  for (Vs32Count = 0; Vs32Count < PtpConnector->count_modes; Vs32Count++) 
  {
    M_DEBUG_STR_4VALUES("\t%d: %ux%u@%uHz\n", Vs32Count,PtpConnector->modes[Vs32Count].hdisplay,PtpConnector->modes[Vs32Count].vdisplay,PtpConnector->modes[Vs32Count].vrefresh);
    /*check if mode available*/
    if(   (PtpConnector->modes[Vs32Count].hdisplay==PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Heigh)
	    &&(PtpConnector->modes[Vs32Count].vdisplay==PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Width)
        &&(PtpConnector->modes[Vs32Count].vrefresh==PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Refresh))
	{
	  PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode=Vs32Count;
	  VbFound=TRUE;
	}
  }/*end for*/
  if(VbFound==FALSE)
  {/*saved mode not available for connector*/
    fprintf(stderr, "early_config_err: resolution string: %ux%u@%uHz not found for connector %u (type %s)\n",	
				  PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Heigh,	      
		          PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Width,			      
			      PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Refresh,
			      PtpConnector->connector_id,connector_type[PtpConnector->connector_type]);
    vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_SAVED_RESOLUTION_NOT_VALID,PpsEarlyConfigInfo->tsPddDisplay.tResolution.cResolution,
			                   strlen(PpsEarlyConfigInfo->tsPddDisplay.tResolution.cResolution)+1);
	Vs32Return=-ENXIO;
	PpsEarlyConfigInfo->tsPddDisplay.tResolution.bReadInfo=FALSE;
  }
  else
  { /*mode found*/
    Vs32Return=Vs32Count;
  }
  return(Vs32Return);
}

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_DRMGetResolutionSupported()
*
* DESCRIPTION: This function checks if the connector supports the requested mode
*
* PARAMETERS:
*         PtpConnector: pointer to the connector
*         PpsEarlyConfigInfo: info structure of the process  
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 0 01
*****************************************************************************/
static tS32 s32EarlyConfig_DRMGetResolutionSupported(drmModeConnector *PtpConnector,tsEarlyConfig_Info *PpsEarlyConfigInfo)
{
  tS32   Vs32Return=0;
  tS32   Vs32Count;
  tBool  VbFound=FALSE;
  M_DEBUG_STR_VALUE_STR("\nearly_config: Supported modes for connector %u (type %s):\n",PtpConnector->connector_id,connector_type[PtpConnector->connector_type]);
  for (Vs32Count = 0; Vs32Count < PtpConnector->count_modes; Vs32Count++) 
  {
    M_DEBUG_STR_4VALUES("\t%d: %ux%u@%uHz\n", Vs32Count,PtpConnector->modes[Vs32Count].hdisplay,PtpConnector->modes[Vs32Count].vdisplay,PtpConnector->modes[Vs32Count].vrefresh);
    PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode=0;
	VbFound=TRUE;
  }/*end for*/
  if(VbFound==FALSE)
  {/*saved mode not available for connector*/
    fprintf(stderr, "early_config_err:  no resolution string found for connector %u (type %s)\n",					  
			      PtpConnector->connector_id,connector_type[PtpConnector->connector_type]);
    vEarlyConfig_SetErrorEntry(EARLY_CONFIG_EM_ERROR_FOUND_NO_DEFAULT_RESOLUTION,NULL,0);
	Vs32Return=-ENXIO;
	PpsEarlyConfigInfo->tsPddDisplay.tResolution.bReadInfo=FALSE;
  }
  else
  { /*mode found*/
    Vs32Return=Vs32Count;
	PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Heigh=PtpConnector->modes[PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode].hdisplay;
	PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Width=PtpConnector->modes[PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode].vdisplay;
    PpsEarlyConfigInfo->tsPddDisplay.tResolution.tResolutionValue.u32Refresh=PtpConnector->modes[PpsEarlyConfigInfo->tsDRMAccess.s32NumberMode].vrefresh;
  }
  return(Vs32Return);
}
/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_DRMFindCtrc()
*
* DESCRIPTION: This function tries to find a suitable CRTC for the given connector. 
*              With the DRM object Encoder the CRTC could be find
*              Encoders help the CRTC to convert data from a framebuffer into the right
*              format that can be used for the chosen connector. Each connector has a 
*              a limited list of encoders that it can use. And each encoder can only 
*              work with a limited list of CRTCs. This function try for each available 
*              encoder and ook for a CRTC that thisencoder can work with. 
*              This function gets the first working combination. 
*              Only one CRTC per connector can be drive.
*
* PARAMETERS:
*         PtpResources: pointer to the resource
*         PtpConnector: pointer to the connector
*         PtsDev:       pointer to information to connector device 
*         PpsEarlyConfigInfo: info structure of the process 
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 06 24
*****************************************************************************/
static tS32 s32EarlyConfig_DRMFindCtrc(drmModeRes *PtpResources, drmModeConnector *PtpConnector, struct tsEarlyConfig_DrmDisplayDev *PtsDev,tsEarlyConfig_Info *PpsEarlyConfigInfo)
{
	tS32								Vs32Return=-ENOENT;
	drmModeEncoder					   *VtpEncoder;
	tS32								Vs32CountCon;
	tS32								Vs32CountRes;
	tS32								Vs32Crtc=-1;
	struct tsEarlyConfig_DrmDisplayDev *VtpDevIter;

	/* first try the currently connected encoder+crtc */
	if (PtpConnector->encoder_id)
		VtpEncoder = drmModeGetEncoder(PpsEarlyConfigInfo->tsDRMAccess.s32Fd, PtpConnector->encoder_id);
	else
		VtpEncoder = NULL;
    /*check encoder*/
	if (VtpEncoder) 
	{
	  if (VtpEncoder->crtc_id) 
	  {
	    Vs32Crtc = VtpEncoder->crtc_id;
		for (VtpDevIter = PpsEarlyConfigInfo->tsDRMAccess.tspDisplayDevList; VtpDevIter; VtpDevIter = VtpDevIter->tpNext) 
		{
		  if (VtpDevIter->u32CrtcId == Vs32Crtc) 
		  {
		    Vs32Crtc = -1;
			break;
		  }
		}
		if(Vs32Crtc>=0)
		{ /*found ctrc*/
		  M_DEBUG_STR_3VALUES("early_config: found encoder %u and crtc %d for connector %u\n",VtpEncoder->encoder_id, Vs32Crtc, PtpConnector->connector_id);
		  PtsDev->u32CrtcId = Vs32Crtc;
		  Vs32Return=0;		 
		}
	  }
	  drmModeFreeEncoder(VtpEncoder);
	}
	/*check if Crtc found*/
	if (Vs32Crtc < 0)
	{/*ctr*/
	  M_DEBUG_STR_VALUE("early_config: no encoder bound to connector %u, searching...\n",	PtpConnector->connector_id);
	  /* If the connector is not currently bound to an encoder or if the encoder+crtc is already used 
	     by another connector (actually unlikely but lets be safe), iterate all other available encoders 
		 to find a matching CRTC. */
	  for (Vs32CountCon = 0; Vs32CountCon < PtpConnector->count_encoders; Vs32CountCon++) 
	  {
		VtpEncoder = drmModeGetEncoder(PpsEarlyConfigInfo->tsDRMAccess.s32Fd, PtpConnector->encoders[Vs32CountCon]);
		if (!VtpEncoder) 
		{
		  fprintf(stderr, "early_config_err:%d retrieve encoder %u:%u\n",errno,Vs32CountCon, PtpConnector->encoders[Vs32CountCon]);
		}
		else
		{
		  M_DEBUG_STR_VALUE("\tearly_config: found an encoder %u:\n", VtpEncoder->encoder_id);
		  /* iterate all global CRTCs */		 
		  for (Vs32CountRes = 0; Vs32CountRes < PtpResources->count_crtcs; Vs32CountRes++) 
		  {
			Vs32Crtc = PtpResources->crtcs[Vs32CountRes];
			M_DEBUG_STR_VALUE("\t\tearly_config: found a crtc %d\n", Vs32Crtc);
			M_DEBUG_STR_VALUE("\t\tearly_config: enc possible crtcs: %08x\n",VtpEncoder->possible_crtcs);
			/* check whether this CRTC works with the encoder */
			if (!(VtpEncoder->possible_crtcs & (1 << Vs32CountRes))) 
			{
				M_DEBUG_STR_VALUE("\t\tearly_config: can't use crtc %d\n",Vs32Crtc);
			}
			else
			{
			  M_DEBUG_STR_VALUE("\t\tearly_config: can use crtc %d\n", Vs32Crtc);
			  /* check that no other device already uses this CRTC */
			  for (VtpDevIter = PpsEarlyConfigInfo->tsDRMAccess.tspDisplayDevList; VtpDevIter; VtpDevIter = VtpDevIter->tpNext) 
			  {
				if (VtpDevIter->u32CrtcId == Vs32Crtc) 
				{
				  M_DEBUG_STR_VALUE("\t\tearly_config: but unfortunately crtc %d"" is already in use\n", Vs32Crtc);
				  Vs32Crtc = -1;
				  break;
				}
			  }/*end for*/
			  if(Vs32Crtc>=0)
			  { /*found ctrc*/
			    M_DEBUG_STR_3VALUES("early_config: found encoder %u and crtc %d for connector %u\n",VtpEncoder->encoder_id, Vs32Crtc, PtpConnector->connector_id);
				PtsDev->u32CrtcId = Vs32Crtc;
				/*set loop/counter to end*/			
				Vs32CountRes=PtpResources->count_crtcs;
                /*return success*/
				Vs32Return=0;				
				break;
			  }
			}
		  }/*end for*/
		  drmModeFreeEncoder(VtpEncoder);
		}/*end else*/
		if(Vs32Crtc>=0)
		{ /*ctrc found => break loop*/
		  break;
		}
	  }/*end for*/
	}
	if (Vs32Crtc < 0)
	{
	  fprintf(stderr, "early_config_err: find not suitable CRTC for connector %u\n",	PtpConnector->connector_id);	 
	}
    return(Vs32Return);
 /*PQM_authorized_491*/
}/*lint !e429*/ 

/******************************************************************************
* FUNCTION: tS32 s32EarlyConfig_DRMCreateFb()
*
* DESCRIPTION: create framebuffer; first create a dump buffer;
*              then create a create framebuffer object for the dumb-buffer;
*              then prepare buffer for memory mapping
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 06 24
*****************************************************************************/
static tS32 s32EarlyConfig_DRMCreateFb(tS32 Ps32Fd, struct tsEarlyConfig_DrmDisplayDev *PtsDev)
{
  tS32	                        Vs32Return=0;
  struct drm_mode_create_dumb   VtsCreatReq;
  struct drm_mode_destroy_dumb  VtsDestroyReq;
  struct drm_mode_map_dumb      VtsMapReq;

  /* create dumb buffer */
  memset(&VtsCreatReq, 0, sizeof(VtsCreatReq));
  VtsCreatReq.width = PtsDev->u32Width;
  VtsCreatReq.height = PtsDev->u32Height;
  VtsCreatReq.bpp = 32;
  Vs32Return = drmIoctl(Ps32Fd, DRM_IOCTL_MODE_CREATE_DUMB, &VtsCreatReq);
  if (Vs32Return < 0) 
  {
    Vs32Return=-errno;
    fprintf(stderr, "early_config_err:%d create dumb buffer\n",Vs32Return);
  }
  else
  {
    PtsDev->u32Stride = VtsCreatReq.pitch;
	PtsDev->u32Size = VtsCreatReq.size;
	PtsDev->u32Handle = VtsCreatReq.handle;
	/* create framebuffer object for the dumb-buffer */
	Vs32Return = drmModeAddFB(Ps32Fd, PtsDev->u32Width, PtsDev->u32Height, 24, 32, PtsDev->u32Stride, PtsDev->u32Handle, &PtsDev->u32Fb);
	if (Vs32Return) 
	{
		Vs32Return = -errno;
		fprintf(stderr, "early_config_err:%d create framebuffer\n",Vs32Return);
	}
	else
	{  /* prepare buffer for memory mapping */
	  memset(&VtsMapReq, 0, sizeof(VtsMapReq));
	  VtsMapReq.handle = PtsDev->u32Handle;
	  Vs32Return = drmIoctl(Ps32Fd, DRM_IOCTL_MODE_MAP_DUMB, &VtsMapReq);
	  if (Vs32Return) 
	  {
	    Vs32Return = -errno;
		fprintf(stderr, "early_config_err:%d map dumb buffer\n",Vs32Return);
		/*remove frame buffer*/
		drmModeRmFB(Ps32Fd, PtsDev->u32Fb);
	  }
	  else
	  {/* perform actual memory mapping */
	    PtsDev->u8pMap = mmap(0, PtsDev->u32Size, PROT_READ | PROT_WRITE, MAP_SHARED,Ps32Fd, (tS32)VtsMapReq.offset);
	    if (PtsDev->u8pMap == MAP_FAILED) 
	    {
		  Vs32Return = -errno;
		  fprintf(stderr, "early_config_err:%d mmap dumb buffer\n",Vs32Return);
		
		  /*remove frame buffer*/
		  drmModeRmFB(Ps32Fd, PtsDev->u32Fb);
	    }
		else
		{/* clear the framebuffer to 0 */
	      memset(PtsDev->u8pMap, 0, PtsDev->u32Size);
		}
	  }
	}
    if(Vs32Return<0)
	{/*destroy dumb*/
	  memset(&VtsDestroyReq, 0, sizeof(VtsDestroyReq));
	  VtsDestroyReq.handle = PtsDev->u32Handle;
	  drmIoctl(Ps32Fd, DRM_IOCTL_MODE_DESTROY_DUMB, &VtsDestroyReq);
	}
  }
  return Vs32Return;
}
#endif


