/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        early_config_trace.c
 *
 * global function:
 * -- vEarlyConfig_DebugMode():
 *           print out mode
 * -- vEarlyConfig_DebugTime():
 *           get actual start time an print 
 * -- vEarlyConfig_SetErrorEntry():
 *           set error memory entry
 *        
 *
 * @date        2015-26-03
 *
 * @note
 *
 *  &copy; Copyright BoschSoftTec GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "helper.h"
#include "system_types.h"
#include "system_definition.h"
#include "early_config_private.h"

/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
/*for EM entry*/
#define TR_COMP_EARLY_CONFIG                              ((256 * 151) +   5)
#define EARLY_CONFIG_LENGTH_ERROR_MEMORY                  100

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/

/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/     


/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
/******************************************************************************
* FUNCTION: void vEarlyConfig_DebugMode()
*
* DESCRIPTION: print out mode
*
* PARAMETERS:
*     --PcMode:      string mode type
*     --PcModeValue: string mode value
*
* HISTORY:Created by Andrea Bueter 2014 06 18
*****************************************************************************/
void vEarlyConfig_DebugMode(const char* PcMode,const char* PcModeValue)
{
  char PcOut[10];
  strncpy(PcOut,PcModeValue,sizeof(PcOut));
  PcOut[strlen(PcOut)-1]=0; //delete return character
  fprintf(stderr, "early_config: %s:%s \n",PcMode,PcOut);
}

/******************************************************************************
* FUNCTION: void vEarlyConfig_DebugTime()
*
* DESCRIPTION: get actual start time an print 
*
*
* HISTORY:Created by Andrea Bueter 2014 06 18
*****************************************************************************/
void vEarlyConfig_DebugTime(void)
{
  struct timespec VTimer={0,0};
  clock_gettime(CLOCK_MONOTONIC, &VTimer);
  fprintf(stderr, "early_config: time:%d s %d ms\n",VTimer.tv_sec,VTimer.tv_nsec/1000000);
}
/******************************************************************************
* FUNCTION: void vEarlyConfig_SetErrorEntry()
*
* DESCRIPTION: set error memory entry
*
* PARAMETERS:
*   --Pu8Kind: kind 
*   --Pu8pData: data
*   --Pu32Len: length of data
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 06 18
*****************************************************************************/
void  vEarlyConfig_SetErrorEntry(tU8 Pu8Kind, const tU8* Pu8pData, tU32 Pu32Len)
{
  tU8                Vu8Buffer[EARLY_CONFIG_LENGTH_ERROR_MEMORY]; 

  Vu8Buffer[0] = Pu8Kind;
  if ((Pu8pData!=NULL)&&((Pu32Len+1) <= sizeof(Vu8Buffer)))
  { /* copy information*/
    memmove(&Vu8Buffer[1],(void*)Pu8pData,Pu32Len);
  }/*end if*/
  else Pu32Len=0;
  WriteErrMemEntry((int)TR_COMP_EARLY_CONFIG,Vu8Buffer,Pu32Len+1,0);
}