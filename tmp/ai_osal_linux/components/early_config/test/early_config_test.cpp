
/******************************************************************************/
/* include the system interface                                               */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include "system_types.h"
#include "system_definition.h"
#include "pdd.h"
#include "pdd_trace.h"

#define DP_S_IMPORT_INTERFACE_BASE
#define DP_S_IMPORT_INTERFACE_FI
#include "dp_if.h"
#include "dp_effd_if.h"

#define  DP_DATAPOOL_ID_EARLY_CONFIG_TEST 0xeffd
//DISPLAY
#ifdef DP_U32_POOL_ID_EARLYCONFIGDISPLAY
  #if(DP_U32_POOL_ID_EARLYCONFIGDISPLAY>>16 == DP_DATAPOOL_ID_EARLY_CONFIG_TEST)
    #define EARYL_CONFIG_TEST_DISPLAY_ACTIV
  #endif
#endif

//RTC
#ifdef DP_U32_POOL_ID_EARLYCONFIGRTCDRIVER
  #if(DP_U32_POOL_ID_EARLYCONFIGRTCDRIVER>>16 == DP_DATAPOOL_ID_EARLY_CONFIG_TEST)
    #define EARYL_CONFIG_TEST_RTC_ACTIV    
  #endif
#endif

//Touch
#ifdef DP_U32_POOL_ID_EARLYCONFIGTOUCHDRIVER
  #if(DP_U32_POOL_ID_EARLYCONFIGTOUCHDRIVER>>16 == DP_DATAPOOL_ID_EARLY_CONFIG_TEST)
    #define EARYL_CONFIG_TEST_TOUCH_ACTIV
  #endif
#endif

//CSC
#ifdef DP_U32_POOL_ID_EARLYCONFIGCSC
  #if(DP_U32_POOL_ID_EARLYCONFIGCSC>>16 == DP_DATAPOOL_ID_EARLY_CONFIG_TEST)
    #define EARYL_CONFIG_TEST_CSC_ACTIV
  #endif
#endif

//two display
#ifdef DP_U32_POOL_ID_EARLYCONFIGTWODISPLAYS
  #if(DP_U32_POOL_ID_EARLYCONFIGTWODISPLAYS>>16 == DP_DATAPOOL_ID_EARLY_CONFIG_TEST)
    #define EARYL_CONFIG_TEST_TWO_DISPLAYS_ACTIV
  #endif
#endif


#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#include "etrace_if.h"


/******************************************************************************* 
|defines and macros 
|------------------------------------------------------------------------------*/
#define PDD_COMMAND    3
#define PDD_ARGUMENT   4

#define PDD_COMMAND_MAX_LEN   200

/******************************************************************************** 
|typedefs and struct defs
|------------------------------------------------------------------------------*/
/******************************************************************************/
/* static  variable                                                           */
/******************************************************************************/                            
tBool VbInit=FALSE;

/******************************************************************************/
/* declaration local function                                                 */
/******************************************************************************/
static void vEarlyConfigTestPrintHelp(void);
static void vEarlyConfigTestSetResolution(char*  VcpStr);
static void vEarlyConfigTestSetLFMode(char*  VcpStr);
static void vEarlyConfigTestSetBCMode(char*  VcpStr);
static void vEarlyConfigTestSetFlagManualDisplaySetting(char*  VcpStr);
static void vEarlyConfigTestSetNameRTCDriver(char*  VcpStr);
static void vEarlyConfigTestSetNameTouchDriver(char*  VcpStr);
static void vEarlyConfigTestSetConfigurationFileForTouchDriver(char*  VcpStr);
static void vEarlyConfigTestSetCSCCurrentProfile(char*  VcpStr);
static void vEarlyConfigTestSetCSCMatrix(char*  VcpStr);
static void vEarlyConfigTestSetCSCGammaFactor(char*  VcpStr);
static void vEarlyConfigTestSetTwoDisplayTimings(char*  VcpStr);
static void vEarlyConfigTestSetTwoDisplayClockEdgeSelects(char*  VcpStr);
static void vEarlyConfigTestPrintConfig(void);
static void vDPInit(void);
/******************************************************************************
* FUNCTION: int main(int argc, char *argv[])
*
* DESCRIPTION: start function of the proccess
*
* PARAMETERS:
*      argc: number of strings
*      argv: string table
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 05 06
*****************************************************************************/
int main(int argc, char *argv[])
{
  const char* VcOptString = "r:l:b:f:pd:t:c:n:m:g:e:a:k:";
  int         ViOpt;
	tBool       VbDone=FALSE;
	char*       VcpStr = NULL;

  fprintf(stderr, "\n");
  fprintf(stderr, "================================================================================\n");
  fprintf(stderr, " TEST: test helper functions for early_config, v3.0.0, 04, 2016\n");
  fprintf(stderr, "================================================================================\n");
  fprintf(stderr, "\n");

    while ((ViOpt = getopt(argc, argv, VcOptString)) != -1) 
    {
      switch (ViOpt) 
	    {
        case 'r':
		     VcpStr=strdup(optarg);
		     fprintf(stderr, "-r: save resolution '%s'\n",VcpStr);
         fprintf(stderr, "--------------------------------------------------------------------------------\n");
         vEarlyConfigTestSetResolution(VcpStr);
		     fprintf(stderr, "================================================================================\n");
		     VbDone=TRUE;
		     free(VcpStr);
        break;
		    case 'l':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-l: save LFMode\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetLFMode(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
        case 'b':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-b: save BCMode\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetBCMode(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
		    case 'f':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-f: save FlagManualDisplay\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetFlagManualDisplaySetting(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
		    case 'p':
		      fprintf(stderr, "-p: print saved configuration\n");
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestPrintConfig();
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
        break;
		    case 'd':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-d: save name RTC driver '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetNameRTCDriver(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
		    case 't':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-t: save name touch driver '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetNameTouchDriver(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
		    case 'c':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-c: save configuration file for the touch driver '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetConfigurationFileForTouchDriver(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
        case 'n':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-n: save the current profile number of the Color Space Conversion '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetCSCCurrentProfile(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
        case 'm':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-m: save the matrix of the Color Space Conversion '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetCSCMatrix(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
        case 'g':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-g: save the gamma value of the Color Space Conversion '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetCSCGammaFactor(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
        case 'e':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-e: save the display timing for two displays '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetTwoDisplayTimings(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
        case 'a':
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-f: save clock select edge for node LVDS1 and LVDS2 (-1,0,1) '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
		      vEarlyConfigTestSetTwoDisplayClockEdgeSelects(VcpStr);
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        break;
        case 'k':
        {
		      VcpStr=strdup(optarg);
		      fprintf(stderr, "-k: delete pool '%s'\n",VcpStr);
          fprintf(stderr, "--------------------------------------------------------------------------------\n");
          tS32 Vs32Size=pdd_get_data_stream_size(VcpStr,PDD_LOCATION_NOR_USER);
          if(Vs32Size<=0)
          {
            fprintf(stderr, "pool '%s' not found on location PDD_LOCATION_NOR_USER \n",VcpStr);			 
          }
          else
          {
            tePddLocation VeLocation=PDD_LOCATION_NOR_USER;
            char          Vu8Buffer[PDD_COMMAND_MAX_LEN];
            memset(Vu8Buffer,0x00,sizeof(Vu8Buffer));
            Vu8Buffer[PDD_COMMAND]=PDD_TEST_CMD_DELETE_DATASTREAM;
		        memcpy(&Vu8Buffer[PDD_ARGUMENT],&VeLocation,sizeof(tePddLocation));
	          memcpy(&Vu8Buffer[PDD_ARGUMENT+sizeof(tePddLocation)],VcpStr,strlen(VcpStr));
            pdd_vTraceCommand(&Vu8Buffer[0]);
          }
		      fprintf(stderr, "================================================================================\n");
		      VbDone=TRUE;
		      free(VcpStr);
        }break;
        default: 		
	        fprintf(stderr, "unknown command line option: \"-%c\"\n", optopt);
          break;
	    }/*end switch*/
    }/*end while*/
	  if(VbDone==FALSE)
	  {
	    vEarlyConfigTestPrintHelp();
	  }		
    OSAL_vProcessExit();
	  _exit(0);
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestPrintHelp
*
* DESCRIPTION: printout options 
*
* PARAMETERS:
*      argc: number of strings
*      argv: string table
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2013 03 12
*****************************************************************************/
static void vEarlyConfigTestPrintHelp()
{
	fprintf(stderr, "-r[Resolutuion]                    save display name (e.g. 800x480@60Hz)\n");
	fprintf(stderr, "-l[LFMode]                         save LFMode(-1,0,1)\n");
	fprintf(stderr, "-b[BCMode]                         save BCMode(-1,0,1)\n");
	fprintf(stderr, "-f[FlagManualDisplaySetting]       save FlagManualDisplaySetting(1,0)\n");
	fprintf(stderr, "-d[RtcDriverName]                  save RTC driver name (e.g. rtc-inc) \n");
	fprintf(stderr, "-t[TouchDriverName]                save touch driver name (e.g. atmel_mxt_ts)\n");
	fprintf(stderr, "-c[TouchConfigFileName]            save configuration file for the touch driver\
				         \n                                   (e.g. DTS2020-mxtConfig8inch-NCG3-V01.raw)\n");
  fprintf(stderr, "-n[CurrentProfile]                 save the current profile number of the Color Space Conversion \n");
  fprintf(stderr, "-m[Hue,Contrast,Saturation,Brightness,HueOffset,SaturationOffset,BrightnessOffset] \
                 \n                                   save the matrix of the Color Space Conversion \n");
  fprintf(stderr, "-g[GammaFactor]                    save the gamma value of the Color Space Conversion \n");
  fprintf(stderr, "-e[TimingLVDS1,TimingLVDS2]        save timing string for node LVDS1 and LVDS2 \n");
  fprintf(stderr, "-a[ClockEdgeLVDS1,ClockEdgeLVDS2]  save clock select edge for node LVDS1 and LVDS2 (-1,0,1)\n");
  fprintf(stderr, "-k[name of the pool]               delete pool (e.g. EarlyConfigRTCDriver,EarlyConfigTouchDriver,EarlyConfigTwoDisplays)\n");
	fprintf(stderr, "-p print saved configuration\n");
	fprintf(stderr, "================================================================================\n");
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetResolution
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 06 06
*****************************************************************************/
static void vEarlyConfigTestSetResolution(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_DISPLAY_ACTIV
  dp_tclEarlyConfigDisplayTrResolution      VoTrResolution;
  //set resolution string
  if(VoTrResolution.s32SetData(VcpStr)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save resolution '%s' fails \n",VcpStr);
  }
  else
  {
    fprintf(stderr, " save resolution '%s' success\n ",VcpStr);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetResolution(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetBCMode
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 06 06
*****************************************************************************/
static void vEarlyConfigTestSetBCMode(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_DISPLAY_ACTIV
  dp_tclEarlyConfigDisplayTrBackwardComp    VoTrBC;
  tS32                                      Vs32Mode=strtol(VcpStr,NULL,10);

  //set low frequency mode
  if(VoTrBC.s32SetData(Vs32Mode)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save BCMode %ld fails\n ",Vs32Mode);
  }
  else
  {
    fprintf(stderr, " save BCMode %ld success\n ",Vs32Mode);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetResolution(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetLFMode
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 06 06
*****************************************************************************/
static void vEarlyConfigTestSetLFMode(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_DISPLAY_ACTIV
  dp_tclEarlyConfigDisplayTrLowFrequency    VoTrLF;
  tS32                                      Vs32Mode=strtol(VcpStr,NULL,10);

  //set low frequency mode
  if(VoTrLF.s32SetData(Vs32Mode)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save LFMode %ld fails\n ",Vs32Mode);
  }
  else
  {
    fprintf(stderr, " save LFMode %ld success\n",Vs32Mode);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetResolution(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetFlagManualDisplaySetting
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 11 11
*****************************************************************************/
static void vEarlyConfigTestSetFlagManualDisplaySetting(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_DISPLAY_ACTIV
  dp_tclEarlyConfigDisplayTrFlagManualDisplaySetting  VoTrFlagManualDisplaySetting;
  tBool                                               VbFlag=(tBool) strtol(VcpStr,NULL,10);

  //set low frequency mode
  if(VoTrFlagManualDisplaySetting.s32SetData(VbFlag)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save FlagManualDisplaySetting %d fails\n ",VbFlag);
  }
  else
  {
    fprintf(stderr, " save FlagManualDisplaySetting %d success\n",VbFlag);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetResolution(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetLFMode
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 02 07
*****************************************************************************/
static void vEarlyConfigTestPrintConfig(void)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_DISPLAY_ACTIV
  dp_tclEarlyConfigDisplayTrResolution                VoTrResolution;
  dp_tclEarlyConfigDisplayTrBackwardComp              VoTrBC;
  dp_tclEarlyConfigDisplayTrLowFrequency              VoTrLF;
  dp_tclEarlyConfigDisplayTrFlagManualDisplaySetting  VoTrFlagManualDisplaySetting;
  tS32                                                Vs32BCModeRead=-2;
  tS32                                                Vs32LFModeRead=-2;
  tBool                                               VbFlag=TRUE;
  tChar                                               VstrResolutionString[32]; 
  #endif
  #ifdef EARYL_CONFIG_TEST_RTC_ACTIV
  dp_tclEarlyConfigRTCDriverTrRtcDriverName           VoTrRTCDriver;
  tChar                                               VstrDriverName[64]; 
  #endif
  #ifdef EARYL_CONFIG_TEST_TOUCH_ACTIV
  dp_tclEarlyConfigTouchDriverTrTouchConfigFileName    VoTrConfigFileName;
  dp_tclEarlyConfigTouchDriverTrTouchDriverName        VoTrTouchDriver;
  tChar                                                VstrName[64]; 
  #endif
  #ifdef EARYL_CONFIG_TEST_CSC_ACTIV
  dp_tclEarlyConfigCSCTrCurrentProfile          VrMyElemCurrentProfile;
  dp_tclEarlyConfigCSCTrHueValue                VrMyElemHueValue;
  dp_tclEarlyConfigCSCTrContrastValue           VrMyElemContrastValue;
  dp_tclEarlyConfigCSCTrSaturationValue         VrMyElemSaturationValue;
  dp_tclEarlyConfigCSCTrBrightnessValue         VrMyElemBrightnessValue;
  dp_tclEarlyConfigCSCTrHueOffsetValue          VrMyElemHueOffsetValue;
  dp_tclEarlyConfigCSCTrSaturationOffsetValue   VrMyElemSaturationOffsetValue;
  dp_tclEarlyConfigCSCTrBrightnessOffsetValue   VrMyElemBrightnessOffsetValue;
  dp_tclEarlyConfigCSCTrGammaFactor             VrGammaFactor;  
  #endif
  
  #ifdef EARYL_CONFIG_TEST_DISPLAY_ACTIV
  //get resolution string
  if(VoTrResolution.s32GetData(VstrResolutionString,sizeof(VstrResolutionString))<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get display fails \n");
  }
  else
  {
    fprintf(stderr, " Display name '%s' \n",VstrResolutionString);
  }
  // get BCMode
  if(VoTrBC.s32GetData(Vs32BCModeRead)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get BCMode fails \n");
  }
  else
  {
    fprintf(stderr, " BCMode %d \n",Vs32BCModeRead);
  }
  // get LFMode
  if(VoTrLF.s32GetData(Vs32LFModeRead)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " LFMode fails \n");
  }
  else
  {
    fprintf(stderr, " LFMode %d \n",Vs32LFModeRead);
  }
  // get FlagManualDisplaySetting
  if(VoTrFlagManualDisplaySetting.s32GetData(VbFlag)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get FlagManualDisplaySetting fails \n");	
  }
  else
  {
    fprintf(stderr, " FlagManualDisplaySetting %d \n",VbFlag);
  }
  #endif
  #ifdef EARYL_CONFIG_TEST_RTC_ACTIV
  // get RTC driver name
  fprintf(stderr, "--------------------------------------------------------------------------------\n");
  if(VoTrRTCDriver.s32GetData(VstrDriverName,sizeof(VstrDriverName))<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get RTC driver name fails \n");
  }
  else
  {
    fprintf(stderr, " RTC driver name '%s' \n",VstrDriverName);
  } 
  #endif
  #ifdef EARYL_CONFIG_TEST_TOUCH_ACTIV
  // get Touch driver name
  fprintf(stderr, "--------------------------------------------------------------------------------\n");
  if(VoTrTouchDriver.s32GetData(VstrName,sizeof(VstrName))<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get touch driver name fails \n");	
  }
  else
  {
    fprintf(stderr, " Touch driver name '%s' \n",VstrName);
  } 
  if(VoTrConfigFileName.s32GetData(VstrName,sizeof(VstrName))<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get configuration file name for touch driver fails \n");
  }
  else
  {
    fprintf(stderr, " Configuration file name for touch driver '%s' \n",VstrName);
  } 
  #endif
  #ifdef EARYL_CONFIG_TEST_CSC_ACTIV
  tU8    Vu8Profil;
  tS16   Vs16Hue;
  tS16   Vs16Contrast;
  tS16   Vs16Saturation;
  tS16   Vs16Brightness;
  tS16   Vs16HueOffset;
  tS16   Vs16SaturationOffset;
  tS16   Vs16BrightnessOffset;
  tF64   Vt64Gamma;

  fprintf(stderr, "--------------------------------------------------------------------------------\n");
  if(VrMyElemCurrentProfile.s32GetData(Vu8Profil)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get current profile fails \n");	
  }
  if(VrMyElemHueValue.s32GetData(Vs16Hue)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get hue fails \n");	
  }
  if(VrMyElemContrastValue.s32GetData(Vs16Contrast)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get contrast  fails \n");	
  }
  if(VrMyElemSaturationValue.s32GetData(Vs16Saturation)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get stauration  fails \n");	
  }
  if(VrMyElemBrightnessValue.s32GetData(Vs16Brightness)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get brightness  fails \n");	
  }
  if(VrMyElemHueOffsetValue.s32GetData(Vs16HueOffset)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get hue offset  fails \n");	
  }
  if(VrMyElemSaturationOffsetValue.s32GetData(Vs16SaturationOffset)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get saturation offset  fails \n");	
  }
  if(VrMyElemBrightnessOffsetValue.s32GetData(Vs16BrightnessOffset)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get brightness offset  fails \n");	
  }
  if(VrGammaFactor.s32GetData(Vt64Gamma)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get gamma factor  fails \n");	
  }
  fprintf(stderr, " CSC: current profile: %d \n",Vu8Profil);
  fprintf(stderr, " CSC: matrix: hue: %d, contrast: %d, saturation: %d, brightness: %d \n",Vs16Hue,Vs16Contrast,Vs16Saturation,Vs16Brightness);
  fprintf(stderr, " CSC: matrix: hue offset: %d, saturation offset: %d, brightness offset: %d \n",Vs16HueOffset,Vs16SaturationOffset,Vs16BrightnessOffset);
  fprintf(stderr, " CSC: gamma factor: %.2f \n",Vt64Gamma);
  #endif
  #ifdef EARYL_CONFIG_TEST_TWO_DISPLAYS_ACTIV
  tChar                                                VstrString[32]; 
  tS32                                                 Vs32ClockEdgeSelect;
  dp_tclEarlyConfigTwoDisplaysTrTimingLVDS1            VrTimingLVDS1;
  dp_tclEarlyConfigTwoDisplaysTrTimingLVDS2            VrTimingLVDS2;
  dp_tclEarlyConfigTwoDisplaysTrClockEdgeSelectLVDS1   VrClockSelectLVDS1;
  dp_tclEarlyConfigTwoDisplaysTrClockEdgeSelectLVDS2   VrClockSelectLVDS2;
  fprintf(stderr, "--------------------------------------------------------------------------------\n");
  //get  string 
  if(VrTimingLVDS1.s32GetData(VstrString,sizeof(VstrString))<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get element TrTimingLVDS1 fails \n");
  }
  else
  {
    fprintf(stderr, " TrTimingLVDS1 name '%s' \n",VstrString);
  }
  if(VrClockSelectLVDS1.s32GetData(Vs32ClockEdgeSelect)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get element TrClockEdgeSelectLVDS1 fails \n");
  }
  else
  {
    fprintf(stderr, " TrClockEdgeSelectLVDS1:%d\n",Vs32ClockEdgeSelect);
  }
  //get  string
  if(VrTimingLVDS2.s32GetData(VstrString,sizeof(VstrString))<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get element TrTimingLVDS2 fails \n");
  }
  else
  {
    fprintf(stderr, " TrTimingLVDS2 name '%s' \n",VstrString);
  }
  if(VrClockSelectLVDS2.s32GetData(Vs32ClockEdgeSelect)<DP_S32_NO_ERR)
  {
    fprintf(stderr, " get element TrClockEdgeSelectLVDS2 fails \n");
  }
  else
  {
    fprintf(stderr, " TrClockEdgeSelectLVDS2:%d\n",Vs32ClockEdgeSelect);
  }
  #endif
}

/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetNameRTCDriver
*
* DESCRIPTION: set the datapool element for the RTC driver
*
* PARAMETERS:  VcpStr: string of the driver name
*
* HISTORY:Created by Andrea Bueter 2015 07 24
*****************************************************************************/
static void vEarlyConfigTestSetNameRTCDriver(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_RTC_ACTIV
  dp_tclEarlyConfigRTCDriverTrRtcDriverName    VoTrRTCDriver;

  //set resolution string
  if(VoTrRTCDriver.s32SetData(VcpStr)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save driver name '%s' fails\n",VcpStr);
  }
  else
  {
    fprintf(stderr, " save driver name '%s' success\n",VcpStr);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetResolution(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetNameTouchDriver
*
* DESCRIPTION: set the datapool element for the Touch driver
*
* PARAMETERS:  VcpStr: string of the driver name
*
* HISTORY:Created by Andrea Bueter 2015 07 31
*****************************************************************************/
static void vEarlyConfigTestSetNameTouchDriver(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_TOUCH_ACTIV
  dp_tclEarlyConfigTouchDriverTrTouchDriverName    VoTrTouchDriver;

  //set resolution string
  if(VoTrTouchDriver.s32SetData(VcpStr)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save driver name '%s' fails\n",VcpStr);
  }
  else
  {
    fprintf(stderr, " save driver name '%s' success\n",VcpStr);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetNameTouchDriver(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetNameTouchDriver
*
* DESCRIPTION: set the datapool element for the Touch driver
*
* PARAMETERS:  VcpStr: string of the driver name
*
* HISTORY:Created by Andrea Bueter 2015 07 31
*****************************************************************************/
static void vEarlyConfigTestSetConfigurationFileForTouchDriver(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_TOUCH_ACTIV
  dp_tclEarlyConfigTouchDriverTrTouchConfigFileName    VoTrConfigFileName;

  //set resolution string
  if(VoTrConfigFileName.s32SetData(VcpStr)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save configuration file for touch driver '%s' fails\n",VcpStr);
  }
  else
  {
    fprintf(stderr, " save configuration file for touch driver '%s' success\n",VcpStr);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetNameTouchDriver(): Pool not defined\n");
  #endif
}

/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetCSCCurrentProfile
*
* DESCRIPTION: set the datapool element for the current profile
*
* PARAMETERS:  VcpStr: string of the current profile
*
* HISTORY:Created by Andrea Bueter 2015 12 22
*****************************************************************************/
static void vEarlyConfigTestSetCSCCurrentProfile(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_CSC_ACTIV
  dp_tclSrvIf                                   VdpSrvIf;
  dp_tclEarlyConfigCSCTrCurrentProfile          VrMyElemCurrentProfile;
  tU8                                           Vu8Profil=strtol(VcpStr,NULL,10);

  if(VrMyElemCurrentProfile.s32SetData(Vu8Profil)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save current profile '%d' fails\n",Vu8Profil);
  }
  else
  {
    if(VdpSrvIf.s32StorePoolNow(DP_U32_POOL_ID_EARLYCONFIGCSC)!=DP_S32_NO_ERR)
    {
      fprintf(stderr, " save current profile '%d' fails\n",Vu8Profil);
    }
    else
      fprintf(stderr, " save current profile '%d' success\n",Vu8Profil);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetCSCCurrentProfile(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetCSCMatrix
*
* DESCRIPTION: set the datapool element for the CSC matrix
*
* PARAMETERS:  VcpStr: string of the matrix
*
* HISTORY:Created by Andrea Bueter 2015 12 22
*****************************************************************************/
static void vEarlyConfigTestSetCSCMatrix(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_CSC_ACTIV
  dp_tclSrvIf                                   VdpSrvIf;
  dp_tclEarlyConfigCSCTrHueValue                VrMyElemHueValue;
  dp_tclEarlyConfigCSCTrContrastValue           VrMyElemContrastValue;
  dp_tclEarlyConfigCSCTrSaturationValue         VrMyElemSaturationValue;
  dp_tclEarlyConfigCSCTrBrightnessValue         VrMyElemBrightnessValue;
  dp_tclEarlyConfigCSCTrHueOffsetValue          VrMyElemHueOffsetValue;
  dp_tclEarlyConfigCSCTrSaturationOffsetValue   VrMyElemSaturationOffsetValue;
  dp_tclEarlyConfigCSCTrBrightnessOffsetValue   VrMyElemBrightnessOffsetValue;
  tS16   Vs16Hue=0;
  tS16   Vs16Contrast=0;
  tS16   Vs16Saturation=0;
  tS16   Vs16Brightness=0;
  tS16   Vs16HueOffset=0;
  tS16   Vs16SaturationOffset=0;
  tS16   Vs16BrightnessOffset=0;
  char*  VstrPos=VcpStr;
  Vs16Hue=strtol(VstrPos,&VstrPos,10);
  if(*VstrPos==',')
  {
    VstrPos++;
    Vs16Contrast=strtol(VstrPos,&VstrPos,10);
    if(*VstrPos==',')
    {
      VstrPos++;
      Vs16Saturation=strtol(VstrPos,&VstrPos,10);
      if(*VstrPos==',')
      {
        VstrPos++;
        Vs16Brightness=strtol(VstrPos,&VstrPos,10);
        if(*VstrPos==',')
        {
          VstrPos++;
          Vs16HueOffset=strtol(VstrPos,&VstrPos,10);
          if(*VstrPos==',')
          {
            VstrPos++;
            Vs16SaturationOffset=strtol(VstrPos,&VstrPos,10);
            if(*VstrPos==',')
            {
              VstrPos++;
              Vs16BrightnessOffset=strtol(VstrPos,&VstrPos,10);
            }
          }
        }
      }
    }
  }
  if(VrMyElemHueValue.s32SetData(Vs16Hue)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save hue '%d' fails\n",Vs16Hue);
  }
  if(VrMyElemContrastValue.s32SetData(Vs16Contrast)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save contrast '%d' fails\n",Vs16Contrast);
  }
  if(VrMyElemSaturationValue.s32SetData(Vs16Saturation)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save saturation '%d' fails\n",Vs16Saturation);
  }
  if(VrMyElemBrightnessValue.s32SetData(Vs16Brightness)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save brightness  '%d' fails\n",Vs16Brightness);
  }
  if(VrMyElemHueOffsetValue.s32SetData(Vs16HueOffset)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save hue offset '%d' fails\n",Vs16HueOffset);
  }
  if(VrMyElemSaturationOffsetValue.s32SetData(Vs16SaturationOffset)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save saturation offset '%d' fails\n",Vs16SaturationOffset);
  }
  if(VrMyElemBrightnessOffsetValue.s32SetData(Vs16BrightnessOffset)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save brightness offset '%d' fails\n",Vs16BrightnessOffset);
  }
  fprintf(stderr, " CSC: matrix: hue: %d, contrast: %d,saturation: %d, brightness: %d \n",Vs16Hue,Vs16Contrast,Vs16Saturation,Vs16Brightness);
  fprintf(stderr, " CSC: matrix: hue offset: %d, saturation offset: %d, brightness offset: %d \n",Vs16HueOffset,Vs16SaturationOffset,Vs16BrightnessOffset);
  if(VdpSrvIf.s32StorePoolNow(DP_U32_POOL_ID_EARLYCONFIGCSC)!=DP_S32_NO_ERR)
    fprintf(stderr, " save current CSC matric fails\n");
  else
    fprintf(stderr, " save current CSC matric success\n");
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetCSCMatrix(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetNameTouchDriver
*
* DESCRIPTION: set the datapool element for gamma factor
*
* PARAMETERS:  VcpStr: string of the gamma factor
*
* HISTORY:Created by Andrea Bueter 2015 12 22
*****************************************************************************/
static void vEarlyConfigTestSetCSCGammaFactor(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_CSC_ACTIV
  dp_tclEarlyConfigCSCTrGammaFactor             VrGammaFactor;  
  dp_tclSrvIf                                   VdpSrvIf;
  tF64                                          Vt64Gamma=strtof(VcpStr,NULL);
  if(VrGammaFactor.s32SetData(Vt64Gamma)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save current profile '%.2f' fails\n",Vt64Gamma);
  }
  else
  {
    if(VdpSrvIf.s32StorePoolNow(DP_U32_POOL_ID_EARLYCONFIGCSC)!=DP_S32_NO_ERR)
    {
      fprintf(stderr, " save current profile '%.2f' fails\n",Vt64Gamma);
    }
    else
      fprintf(stderr, " save current profile '%.2f' success\n",Vt64Gamma);
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetCSCGammaFactor(): Pool not defined\n");
  #endif
}

/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetTwoDisplayTimings
*
* DESCRIPTION: set the datapool element the pool for two displays
*
* PARAMETERS:  VcpStr: string of the pools
*
* HISTORY:Created by Andrea Bueter 2016 01 08
*****************************************************************************/
static void vEarlyConfigTestSetTwoDisplayTimings(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_TWO_DISPLAYS_ACTIV
  dp_tclEarlyConfigTwoDisplaysTrTimingLVDS1  VrTimingLVDS1;
  dp_tclEarlyConfigTwoDisplaysTrTimingLVDS2  VrTimingLVDS2;
  char*  VcpStr1=VcpStr;
  char*  VcpStr2=strchr(VcpStr,',');
  if(VcpStr2!=NULL)
  {
    *VcpStr2=0;    
    VcpStr2++;
  }
  if(strlen(VcpStr1)>0)
  {
    if(VrTimingLVDS1.s32SetData(VcpStr1)!=DP_S32_NO_ERR)
    {
      fprintf(stderr, " save resolution '%s' fails for TrTimingLVDS1 \n",VcpStr1);
    }     
    else
    {
      fprintf(stderr, " save resolution '%s' success for TrTimingLVDS1 \n",VcpStr1);
    }
  }
  if((VcpStr2!=NULL)&&(strlen(VcpStr2)>0))
  {
    if(VrTimingLVDS2.s32SetData(VcpStr2)!=DP_S32_NO_ERR)
    {
      fprintf(stderr, " save resolution '%s' fails for TrTimingLVDS2 \n",VcpStr2);
    }     
    else
    {
      fprintf(stderr, " save resolution '%s' success for TrTimingLVDS2 \n",VcpStr2);
    }
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetTwoDisplayTimings(): Pool not defined\n");
  #endif
}

/******************************************************************************
* FUNCTION: void vEarlyConfigTestSetTwoDisplayClockEdgeSelects
*
* DESCRIPTION: set the datapool element the pool for two displays
*
* PARAMETERS:  VcpStr: string clock edge select
*
* HISTORY:Created by Andrea Bueter 2016 03 31
*****************************************************************************/
static void vEarlyConfigTestSetTwoDisplayClockEdgeSelects(char*  VcpStr)
{
  vDPInit();
  #ifdef EARYL_CONFIG_TEST_TWO_DISPLAYS_ACTIV
  dp_tclEarlyConfigTwoDisplaysTrClockEdgeSelectLVDS1   VrClockSelectLVDS1;
  dp_tclEarlyConfigTwoDisplaysTrClockEdgeSelectLVDS2   VrClockSelectLVDS2;
  char*                                                VstrPos=VcpStr;
  tS32                                                 Vs32ClockEdgeSelect1=strtol(VstrPos,&VstrPos,10);
  tS32                                                 Vs32ClockEdgeSelect2=2;  /*2 not valid */
  if(*VstrPos==',')
  {
    VstrPos++;
    Vs32ClockEdgeSelect2=strtol(VstrPos,&VstrPos,10);
  }
  //set ClockSelectLVDS1
  if(VrClockSelectLVDS1.s32SetData(Vs32ClockEdgeSelect1)!=DP_S32_NO_ERR)
  {
    fprintf(stderr, " save TrClockEdgeSelectLVDS1 %ld fails\n ",Vs32ClockEdgeSelect1);
  }
  else
  {
    fprintf(stderr, " save TrClockEdgeSelectLVDS1 %ld success\n",Vs32ClockEdgeSelect1);
  }
  if(Vs32ClockEdgeSelect2!=2)
  {
    if(VrClockSelectLVDS2.s32SetData(Vs32ClockEdgeSelect2)!=DP_S32_NO_ERR)
    {
      fprintf(stderr, " save TrClockEdgeSelectLVDS2 %ld fails\n ",Vs32ClockEdgeSelect2);
    }
    else
    {
      fprintf(stderr, " save TrClockEdgeSelectLVDS2 %ld success\n",Vs32ClockEdgeSelect2);
    }
  }
  #else
  *VcpStr=0; //for lint
  fprintf(stderr, "vEarlyConfigTestSetTwoDisplayTimings(): Pool not defined\n");
  #endif
}
/******************************************************************************
* FUNCTION: void vDPInit()
*
* DESCRIPTION: 
*
* PARAMETERS:
*
* RETURNS: 
*
* HISTORY:Created by Andrea Bueter 2014 06 06
*****************************************************************************/
static void vDPInit(void)
{
  if(VbInit==FALSE)
  {
    ET_TRACE_OPEN; 
    #if defined(EARYL_CONFIG_TEST_TWO_DISPLAYS_ACTIV)  || defined(EARYL_CONFIG_TEST_CSC_ACTIV) || defined(EARYL_CONFIG_TEST_TOUCH_ACTIV) || defined(EARYL_CONFIG_TEST_RTC_ACTIV) || defined(EARYL_CONFIG_TEST_DISPLAY_ACTIV)
    vSetInitCallback_EFFD();
    #endif
	  VbInit=TRUE;
  }
}
