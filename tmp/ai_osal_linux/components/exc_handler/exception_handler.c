/*
 * exception_handler.c
 *
 * Implementation of "exception handler"
 * helper for printing some more information
 * of an exception to stdout and to "/dev/errmem"
 * (if "/dev/errmem" is is accessible)
 * the will print more informations for
 * aborted processes
 *
 * now only for x86 and arm architecture
 *
 */

#define _GNU_SOURCE
#include "OsalConf.h"

#include <ucontext.h>
#include <sys/reboot.h>
#include <sysexits.h>
#include <execinfo.h>
#include <sys/ipc.h>
#ifdef VARIANT_S_FTR_ADIT_SUPPORTED 
#include <linux/exchnd.h>
#endif


#ifdef LIBUNWIND_USED
#define UNW_LOCAL_ONLY 
#include <libunwind.h>
#endif //LIBUNWIND_USED

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#include "exception_handler.h"
#include "Linux_osal.h"

#define SIGHDR_PREFIX      "OSALEXC "

#define EXC_LOCK_MAGIC 0x56745612
#define EXC_LOCK_MAGIC_FAILED 0xdeaddead
#define EXC_LOCK_MAX_TRIES 4000 /* 4.000 * 1000 us --> 4 s max wait time*/

/* 
 * it would be better to include "errmem.h",
 * but it is not clear if the include patchess are right
 * So as first solution the define is written here directly
 */  



#define gettid() syscall(SYS_gettid)
#define tkill(x, sig) syscall(SYS_tkill, (x), (sig))



#define USE_CS_FILE                               /* define for file used for formatting OSAL signal handler callstacks */

#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE  
#ifdef UBUNTU_ENV
#define ERRMEM_FILE "/tmp/OSAL/errmem"
#define ERRMEM_CSFILE "/tmp/OSAL/errmemcs"
#else
#define ERRMEM_FILE "/var/opt/bosch/dynamic/errmem"
#define ERRMEM_CSFILE "/var/opt/bosch/dynamic/errmemcs"
#endif
#else
#define ERRMEM_CSFILE "/run/OSAL/errmemcs"
#define ERRMEM_FILE "/dev/errmem"
#endif


#ifdef UBUNTU_ENV
#define STACK_FILE "/tmp/OSAL/stack.txt"
#else
#define STACK_FILE "/var/opt/bosch/dynamic/stack.txt"
#endif

#define EXCEPTION_HANDLER_REBOOT_FILE "/opt/bosch/disable_reset.txt"

/* enable this to trace all signals to errormem */
#define SIGONLY_TO_ERRMEM

#define EXC_TRACE_AROUND_CODE_SIZE 8
#define EXC_STACK_AROUND_SP_SIZE 40

/* number of last equal callstack adresses that are accepted */
//#define EXC_NUM_EQUAL_ACCEPT 9
/* array bounds */

#define EXC_STACK_DEEPNESS 100
//#define EXC_MAX_CMDLINE 256
#define MAX_LINE_SIZE 256

#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
#define MAX_MAP_ENTRIES 2000
#else
#define MAX_MAP_ENTRIES 500
#endif

//#define SIG_INTERNAL  (SIGRTMIN+1) SIG_BACKTRACE == SIGUSR2
#define SIG_INTERNAL  (SIG_BACKTRACE+1)
#define SIG_INTERNAL2 (SIG_BACKTRACE+2)

#define SIG_TRIGGERSTACK  OSAL_STACK_SIGNAL

/* trace exception always on stdout */

#define FD_STDOUT 1

#define SIGMAX 32

/* lock for the exception handler */

static int *ptr_exc_handler_lock = 0;
//static int exc_shMemID = -1;
#define EXC_SHMEM_MAGIC   ((key_t) 0x2304)

/* default stack size for exceptions */

#ifdef LIBUNWIND_USED
#define STKSZ 0x180000 /* 1,5MB */
#else
#define STKSZ 0x4000   /* 16kB */
#endif

#ifndef PAGE_SIZE
#define PAGE_SIZE 4096
#endif

static int exit_handler_set = 0;

/* file descriptor for the error memory */
int errmem_fd = -1;
#ifdef USE_CS_FILE
static int errmem_cs_fd = -1;
#endif

static int stackfd = -1;
static void* pUnwModuleHandle = NULL;

/* array to hold the commandline */

char commandline[256] = { 0 };
char process_name[256] = { 0 };

struct SigActivation {
    int enabled;
    struct sigaction old_handler;
};

#define EXC_ENABLE_SIGNAL(x) [x] = {1, { .sa_handler = SIG_DFL } }
#define EXC_SIGNAL_IS_ENABLED(x) (sig_activated[x].enabled != 0)
#define EXC_OLD_HANDLER(x) (sig_activated[x].old_handler)



/* table to enable signals for backtracing */
static struct SigActivation sig_activated[SIGMAX] = {
    EXC_ENABLE_SIGNAL(SIGSEGV),/*lint !e651 PQM_authorized_470*/
    EXC_ENABLE_SIGNAL(SIGILL),/*lint !e651 PQM_authorized_470*/
    EXC_ENABLE_SIGNAL(SIGFPE),/*lint !e651 PQM_authorized_470*/
    EXC_ENABLE_SIGNAL(SIGSYS),/*lint !e651 PQM_authorized_470*/
    EXC_ENABLE_SIGNAL(SIGBUS),/*lint !e651 PQM_authorized_470*/
    EXC_ENABLE_SIGNAL(SIGABRT),/*lint !e651 PQM_authorized_470*/
//    EXC_ENABLE_SIGNAL(SIGPIPE),/*lint !e651 PQM_authorized_470*/
    EXC_ENABLE_SIGNAL(SIGXCPU),/*lint !e651 PQM_authorized_470*/
    EXC_ENABLE_SIGNAL(SIGXFSZ),/*lint !e651 PQM_authorized_470*/
    #if 0
    /* perhaps activate this too ? */
    EXC_ENABLE_SIGNAL(SIGSTOP),/*lint !e651 PQM_authorized_470*/
    EXC_ENABLE_SIGNAL(SIGTSTP),/*lint !e651 PQM_authorized_470*/
    #endif
};/*lint !e785 PQM_authorized_471 */

/*
 * use one of the register names for selection of the target
 * maybe another selection is better, but this works fine for now
 */

static const char *sigill_txt[] = {
    [ILL_ILLOPC] = "illegal opcode",
    [ILL_ILLOPN] = "illegal operand",
    [ILL_ILLADR] = "illegal addressing mode",
    [ILL_ILLTRP] = "illegal trap",
    [ILL_PRVOPC] = "privileded opcode",
    [ILL_PRVREG] = "privileded register",
    [ILL_COPROC] = "coprocessor error",
    [ILL_BADSTK] = "internal stack error",
};

static const char *sigfpe_txt[] = {
    [FPE_INTDIV] = "integer divide by zero",
    [FPE_INTOVF] = "integer overflow",
    [FPE_FLTDIV] = "floating-point divide by zero",
    [FPE_FLTOVF] = "floating-point overflow",
    [FPE_FLTUND] = "floating-point underflow",
    [FPE_FLTRES] = "floating-point inexact result",
    [FPE_FLTSUB] = "subscript out of range",
};

static const char *sigsegv_txt[] = {
    [SEGV_MAPERR] = "address not mapped to object",
    [SEGV_ACCERR] = "stack overflow",
//    [SEGV_BNDERR] = "Failed address bound checks",
//    [SEGV_PKUERR] = "Protection key fault",
};

static const char *sigbus_txt[] = {
    [BUS_ADRALN] = "invalid address alignment",
    [BUS_ADRERR] = "nonexistent physical address",
    [BUS_OBJERR] = "object-specific hardware error",
//    [BUS_MCEERR_AR] = "Hardware memory error consumed on a machine check",
//    [BUS_MCEERR_AO] = "Hardware memory error detected in process but not consumed",
};

static const char *sigio_txt[] = {
    [POLL_IN] = "Data input available",
    [POLL_OUT] = "Output buffers available",
    [POLL_MSG] = "Input message available",
    [POLL_ERR] = "I/O error",
    [POLL_PRI] = "High priority input available",
    [POLL_HUP] = "Device disconnected",
};

static const char *mmap_string = "OSALEXC ====== memory map ======\n";

static tS32 fifo = -1;
static tBool bExceptionActive = FALSE;

static tU32 u32SigPattern[32] = {/*SIGRTMAX-32*/ 0x00000001,0x00000002,0x00000004,0x00000008,0x00000010,0x00000020,0x00000040,0x00000080,0x00000100,
                                 0x00000200,0x00000400,0x00000800,0x00001000,0x00002000,0x00004000,0x00008000,0x00010000,0x00020000,0x00040000,
                                 0x00080000,0x00100000,0x00200000,0x00400000,0x00800000,0x01000000,0x02000000,0x04000000,0x08000000,0x10000000,
                                 0x20000000,0x40000000,0x80000000 /*SIGRTMAX*/};


extern OSAL_tSemHandle  hLicenceToKillSem;

                                 
static int exc_get_thread_info(char* name, int len)
{
    char buffer[128];
    int fd;
    int Ret = 0;
    int pid = getpid();
    int tid = gettid();
    snprintf(buffer,128,"/proc/%d/task/%d/comm",pid,tid);
    if((fd = open(buffer,O_RDONLY)) != -1)
    {
       Ret = read(fd,name,len);
  //     *(name+(len-1)) = '\0';
       close(fd);
       if(Ret > 0)
       {
           Ret = tid;
       }
       else
       {
           Ret = 0;
       }
    }
    else
    {
      strncpy(name,"unnamed",strlen("unnamed"));
    }
    return Ret;
}

#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
void vWriteToErrmemFile(int trClass, const char* pBuffer,int size,unsigned int TrcType)
{
  char cBuf[3];
  cBuf[2] = 0;
  if(size > (ERRMEM_MAX_ENTRY_LENGTH-3))size = ERRMEM_MAX_ENTRY_LENGTH-3;
  cBuf[0] = (tChar) (trClass & 0xFF);
  cBuf[1] = (tChar) (trClass >> 8) & 0xFF;
  if(TrcType)cBuf[2] = (tChar) TrcType;

   if(errmem_fd == -1)
   {
      errmem_fd = open(ERRMEM_FILE, O_RDWR | O_CREAT | O_APPEND , OSAL_ACCESS_RIGTHS);
   }
#ifdef USE_CS_FILE
   if(errmem_cs_fd == -1)
   {
      errmem_cs_fd = open(ERRMEM_CSFILE, O_RDWR | O_CREAT | O_APPEND , OSAL_ACCESS_RIGTHS);
      if(errmem_cs_fd != -1)
      {
            if(s32OsalGroupId)
            {
                if(fchown(errmem_cs_fd,(uid_t)-1,s32OsalGroupId) == -1)
                {
                    vWritePrintfErrmem("vWriteToErrmemFile open -> fchown error %d \n",errno);
                }
                if(fchmod(errmem_cs_fd, OSAL_ACCESS_RIGTHS) == -1)
                {
                    vWritePrintfErrmem("vWriteToErrmemFile open -> fchmod error %d \n",errno);
                }
             } 
       }
   }
#endif

   if(errmem_fd != -1)
   {
     time_t secSinceEpoch;
     struct tm mytm;
     trErrmemEntry  rErrmemEntry = {0};
     rErrmemEntry.u16Entry = trClass;
     memset(&rErrmemEntry.rEntryTime,0,sizeof(OSAL_trTimeDate));
   //     (tVoid) OSAL_s32ClockGetTime(&rErrmemEntry.rEntryTime);
     secSinceEpoch = time(NULL);
     if( NULL != gmtime_r(&secSinceEpoch, &mytm))
     {
            rErrmemEntry.rEntryTime.s32Second         = mytm.tm_sec;
            rErrmemEntry.rEntryTime.s32Minute         = mytm.tm_min;
            rErrmemEntry.rEntryTime.s32Hour           = mytm.tm_hour;
            rErrmemEntry.rEntryTime.s32Day            = mytm.tm_mday;
            rErrmemEntry.rEntryTime.s32Month          = mytm.tm_mon + 1;
            rErrmemEntry.rEntryTime.s32Year           = mytm.tm_year;
            rErrmemEntry.rEntryTime.s32Weekday        = mytm.tm_wday;
            rErrmemEntry.rEntryTime.s32Yearday        = mytm.tm_yday;
            rErrmemEntry.rEntryTime.s32Daylightsaving = mytm.tm_isdst; /* do we have locale info? */
     }
     rErrmemEntry.eEntryType = eErrmemEntryNormal;
     rErrmemEntry.u16EntryLength = (tU16)size+3;
     (tVoid) OSAL_pvMemoryCopy(&rErrmemEntry.au8EntryData[0], cBuf,3);
     if(TrcType)OSAL_pvMemoryCopy(&rErrmemEntry.au8EntryData[3], pBuffer,(tU32)size);
     else OSAL_pvMemoryCopy(&rErrmemEntry.au8EntryData[2], pBuffer,(tU32)size);
     write(errmem_fd, &rErrmemEntry, sizeof(rErrmemEntry));
   }
}
#endif


static void write_fd(int fd, const char *buffer, int size)
{
    
    if(fd == FD_STDOUT)
    {
        /* we write directly to stdout */
        write(fd, buffer, size);    
    }
    else
    {
#ifdef WRITE_TO_STDOUT
        write(FD_STDOUT, buffer, size);
#endif
        /* use vWriteToErrMem() for writing into error memory file
           and write() if we write to /dev/errmen        */
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
        vWriteToErrmemFile((TR_tenTraceClass)TR_COMP_OSALCORE, buffer, size,OSAL_STRING_OUT );
#else
        if(write(fd,buffer, size) == -1)
        {
           TraceString("write to ErrMem %d !!!!!",errno);
           if(errno == EBADF)
           {
              errmem_fd = open(ERRMEM_FILE, O_WRONLY, OSAL_ACCESS_RIGTHS);
              if(errmem_fd != -1)
              {
                 if(write(errmem_fd,buffer, size) == -1)
                 {
                     TraceString("Cannot write to ErrMem %s errno:%d",buffer,errno);
                 }
              }
              else
              {
                 TraceString("Cannot open errno:%d",errno);
              }
           }
        }
#endif
    }
}

static const struct sigaction act_ignore = {
    .sa_handler = SIG_IGN, /*lint -e785 */
};



static void sigaction_protfunc(int n_signal, siginfo_t *siginfo, void *ptr)
{
    char buffer[MAX_LINE_SIZE];
    int size;
    int fdesc;
    ((void)siginfo);
    ((void)ptr);

    if (errmem_fd != -1)fdesc = errmem_fd;
    else fdesc = FD_STDOUT;


    if(n_signal == pOsalData->u32TimSignal)
    {
       size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d (%s) Timer signal %d received and consumed -> Should not happened \n",
                       SIGHDR_PREFIX,getpid(),commandline,n_signal);
    }
    else
    {
        size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d (%s) Unexpected signal %d received and consumed -> Check Task Signalmask !!! \n",
                       SIGHDR_PREFIX,getpid(),commandline,n_signal);
    }
    write_fd(fdesc,buffer, size);
}

static const struct sigaction act_protocol = {
    .sa_sigaction = sigaction_protfunc,
    .sa_flags = SA_ONSTACK | SA_SIGINFO | SA_RESTART,
};

/*static void sigaction_sigsegvreplace(int n_signal, siginfo_t *siginfo, void *ptr)
{
    char buffer[MAX_LINE_SIZE];
    int size;
    int fdesc;
    ((void)siginfo);
    ((void)ptr);

    if (errmem_fd != -1)fdesc = errmem_fd;
    else fdesc = FD_STDOUT;


    size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d signal %d received a during unwinding !!! \n",
                       SIGHDR_PREFIX,getpid(),n_signal);
    write_fd(fdesc,buffer, size);
}

static const struct sigaction act_sigsegvreplace = {
    .sa_sigaction = sigaction_sigsegvreplace,
    .sa_flags = SA_ONSTACK | SA_SIGINFO | SA_RESTART,
};*/


/*********************************/
/* Creates / attaches shared mem area */
//void init_exception_handler_lock(){
  /* This function is only called from init_exception handler */
  /* The first time this is called is on bProcessAttached, here we are single threded */
  /* Therefore we do not have to lock the global vars 'shmemID, etc. */
  /* lock for the exception handler */

/*  struct shmid_ds exc_shMemID_DS;
  
  if(exc_shMemID == -1){
    exc_shMemID = shmget(EXC_SHMEM_MAGIC, sizeof(int), IPC_CREAT | OSAL_ACCESS_RIGTHS);
    if(exc_shMemID < 0){
      return;
    }else{
      if(s32OsalGroupId)
      {
        if (shmctl(exc_shMemID, IPC_STAT, &exc_shMemID_DS) == -1)
        {
           vWritePrintfErrmem("init_exception_handler_lock -> shmctl IPC_STAT error %d \n",errno);
        }
        else
        {
           exc_shMemID_DS.shm_perm.gid = s32OsalGroupId;
           exc_shMemID_DS.shm_perm.mode = OSAL_ACCESS_RIGTHS;
           if (shmctl(exc_shMemID, IPC_SET, &exc_shMemID_DS) == -1)
           {
             CheckErrMemEntry(getpid());
           }
        }
      }
    }
    
    ptr_exc_handler_lock = shmat(exc_shMemID, 0, 0);
    if (ptr_exc_handler_lock == (int*)-1) {
      ptr_exc_handler_lock = NULL;
    } 
  }
  return;
}*/

int* pGetLockAdress(void)
{
   return ptr_exc_handler_lock;
}

#define exc_print_wrong_lock(x) exc_print_wrong_lock_func(__FILE__, __LINE__, x)

void exc_print_wrong_lock_func(const char *file, int line, int value)
{
    char buffer[MAX_LINE_SIZE];
    int size;

    snprintf(buffer, sizeof(buffer), "exc_handler:wrong lock value 0x%08x detected at %s, %d\n",
        (unsigned int)value, file, line);
    buffer[sizeof(buffer)-1] = 0;
    size = strlen(buffer);    
    if (errmem_fd != -1)
    {
        write_fd(errmem_fd, buffer, size);
    }
    else
    {
        write_fd(FD_STDOUT, buffer, size);
    }
}

int exc_lock(int *l)
{
    int count;
    
    if ((*l == 0) || (*l == EXC_LOCK_MAGIC))
    {
        /* expected value here, try to lock */
        for (count =0; count < EXC_LOCK_MAX_TRIES; ++count)
        {
            if (__sync_lock_test_and_set(l, EXC_LOCK_MAGIC) == 0)/*lint !e746 *//*PQM_authorized_multi_472*/
            {
                break;
            }
            else
            {
                while(1)
                {
                   if(usleep(1000) == -1)
                   {
                       vWritePrintfErrmem("%s usleep Error:%d",SIGHDR_PREFIX,errno);
                       if(errno != EINTR)
                       {
                           break;
                       }
                   }
                   else
                   {
                      break;
                   }
                }
            }
        }
        if (count < EXC_LOCK_MAX_TRIES)
            return 0;
        else
            return (int)EXC_LOCK_MAGIC_FAILED;
    }
    else
    {
        TraceString("Exchdr: Wrong Lock Value: 0x%08x",*l);
        return *l;
    }
}

void exc_unlock(int *l)
{
    *l = 0;
    __sync_synchronize();/* lint -e746 *//*PQM_authorized_multi_472*/
}

int exception_handler_lock(void)
{
    /* get the global lock */
    int RetVal = exc_lock(ptr_exc_handler_lock);
    if((pOsalData)&&(RetVal == 0))
    {
       /* lock receibed , store owner and time for analysis when lock is not freed
          This is checked by OSAL Term task to detect stucking callstack generation */
      pOsalData->u32ExcTime = OSAL_ClockGetElapsedTime();
      pOsalData->s32ExcPid  = OSAL_ProcessWhoAmI();
      pOsalData->s32ExcTid  = OSAL_ThreadWhoAmI();
    }
    return RetVal;
}

void exception_handler_unlock(void)
{
    if(pOsalData)
    {
      /* check if unlock is called from the right task */
      if(pOsalData->s32ExcTid == OSAL_ThreadWhoAmI())
      {
         /* reset supervision data */
         pOsalData->u32ExcTime = 0;
         pOsalData->s32ExcPid  = 0;
         pOsalData->s32ExcTid  = 0;
      }
    }
    /* do the unlock always, */
    exc_unlock(ptr_exc_handler_lock);
}

static int dev_null_descr = -1;

/* open null device for adress area check */
static void init_check_access(void)
{
    if (dev_null_descr < 0)
    {
        dev_null_descr = open("/dev/null", O_WRONLY);
    }
}

/* check area adress: true -> access ok */
int exc_check_access(const void *addr)
{
    void *paged_addr;
//    unsigned char vec[1];

    paged_addr = ALIGN_VOIDP(addr);
/*    if(mincore(paged_addr, EXEC_PAGESIZE, vec) == 0)return TRUE;
    else Return FALSE;*/
    if (dev_null_descr < 0)
        return FALSE;
    return (write(dev_null_descr, paged_addr, EXEC_PAGESIZE) == EXEC_PAGESIZE);
}

#ifdef EXC_X86

/* x86 definitions */

#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
#define INSTRUCTION_POINTER(uc_link)  ((uc_link)->uc_mcontext.gregs[REG_RIP])
#define STACK_POINTER(uc_link)  ((uc_link)->uc_mcontext.gregs[REG_RSP])
#else
#define INSTRUCTION_POINTER(uc_link)  ((uc_link)->uc_mcontext.gregs[REG_EIP])
#define STACK_POINTER(uc_link)  ((uc_link)->uc_mcontext.gregs[REG_ESP])
#endif

#define EXC_IGNORE_POINTERS 1

/* register names for x86 */
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
static char *reg_names[NGREG] = {
    [REG_R8]  = "REG_R8 ",
    [REG_R9]  = "REG_R9 ",
    [REG_R10] = "REG_R10",
    [REG_R11] = "REG_R11",
    [REG_R12] = "REG_R12",
    [REG_R13] = "REG_R13",
    [REG_R14] = "REG_R14",
    [REG_R15] = "REG_R15",
    [REG_RDI] = "RDI    ",
    [REG_RSI] = "RSI    ",
    [REG_RBP] = "RBP    ",
    [REG_RBX] = "RBX    ",
    [REG_RDX] = "RDX    ",
    [REG_RAX] = "RAX    ",
    [REG_RCX] = "RCX    ",
    [REG_RSP] = "RSP    ",
    [REG_RIP] = "RIP    ",
    [REG_EFL] = "EFL    ", 
    [REG_RBX] = "RBX    ",
    [REG_RDX] = "RDX    ",
    [REG_RCX] = "RCX    ",
    [REG_RAX] = "RAX    ",
    [REG_CSGSFS] = "CSGSFS ",
    [REG_ERR] = "ERR    ",
    [REG_TRAPNO] = "TRAPNO ",
    [REG_OLDMASK]= "OLDMASK",
    [REG_CR2] = "CR2    "
};
#else
static char *reg_names[NGREG] = {
    [REG_GS] = "GS    ",
    [REG_FS] = "FS    ",
    [REG_ES] = "ES    ",
    [REG_DS] = "DS    ",
    [REG_EDI] = "EDI   ",
    [REG_ESI] = "ESI   ",
    [REG_EBP] = "EBP   ",
    [REG_ESP] = "ESP   ",
    [REG_EBX] = "EBX   ",
    [REG_EDX] = "EDX   ",
    [REG_ECX] = "ECX   ",
    [REG_EAX] = "EAX   ",
    [REG_TRAPNO] = "TRAPNO",
    [REG_ERR] = "ERR   ",
    [REG_EIP] = "EIP   ",
    [REG_CS] = "CS    ",
    [REG_EFL] = "EFL   ",
    [REG_UESP] = "UESP  ",
    [REG_SS] = "SS    "
};
#endif
/*
 * print out register information for ARM
 * only snprintf and write is used
 * to avoid memory allocations
 */

static void exc_print_registers(ucontext_t *uc_link, int fd)
{
    int i;
    char buffer[MAX_LINE_SIZE];
    int size;

    size = snprintf(buffer, MAX_LINE_SIZE, "%s ====== registers :\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);

    write_fd(fd, SIGHDR_PREFIX, strlen(SIGHDR_PREFIX));
    for (i = 0; i < NGREG; ++i) {
        size = snprintf(buffer, MAX_LINE_SIZE,
                "%s = 0x%" PRIxPTR " ", reg_names[i],
            (uintptr_t)uc_link->uc_mcontext.gregs[i]);
        write_fd(fd, buffer, size);
        if (((i%3) == 2))
        {
            write_fd(fd, "\n", 2);
            write_fd(fd, SIGHDR_PREFIX, strlen(SIGHDR_PREFIX));
        }
        else
            write_fd(fd, " ", 2);
    }
    write_fd(fd, "\n", 2);
}

#else

/* ARM definitions */

#define EXC_IGNORE_POINTERS 1

/*
 * print out register information for ARM
 * only snprintf and write is used
 * to avoid memory allocations
 */

#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
/* 
    X0 - X7 arguments and return value
    X8 indirect result (struct) location
    X9 - X15 temporary registers
    X16 - X17 intra-call-use registers (PLT, linker)
    X18 platform specific use (TLS)
    X19 - X28 callee-saved registers
    X29 frame pointer
    X30 link register
    SP stack pointer (XZR)
*/
static void exc_print_registers(ucontext_t *uc_link, int fd)
{
    char buffer[MAX_LINE_SIZE];
    int size;
    int i;
    size = snprintf(buffer, MAX_LINE_SIZE, "%s ====== registers :\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);

    for(i=0;i<8;i++)
    {
       size = snprintf(buffer, MAX_LINE_SIZE ,
              "%s REGS[0] = 0x%" PRIxPTR ",  REGS[1] = 0x%" PRIxPTR ",  REGS[2] =0x%" PRIxPTR " ,  REGS[3]  =0x%" PRIxPTR "\n",
               SIGHDR_PREFIX,
               (uintptr_t)uc_link->uc_mcontext.regs[0+(i*4)],
               (uintptr_t)uc_link->uc_mcontext.regs[1+(i*4)],
               (uintptr_t)uc_link->uc_mcontext.regs[2+(i*4)],
               (uintptr_t)uc_link->uc_mcontext.regs[3+(i*4)]);
        write_fd(fd, buffer, size);
    }
    size = snprintf(buffer, MAX_LINE_SIZE,
           "%s PSTATE  = 0x%" PRIxPTR ", SP  = 0x%" PRIxPTR ", LR  = 0x%" PRIxPTR ", PC  = 0x%" PRIxPTR "\n",
           SIGHDR_PREFIX,
           (uintptr_t)uc_link->uc_mcontext.pstate,
           (uintptr_t)uc_link->uc_mcontext.sp,
           (uintptr_t)uc_link->uc_mcontext.regs[23],
           (uintptr_t)uc_link->uc_mcontext.pc);
    write_fd(fd, buffer, size);
    size = snprintf(buffer, MAX_LINE_SIZE,
           "%s FAULT_ADDRESS = 0x%" PRIxPTR "\n",
           SIGHDR_PREFIX,
           (uintptr_t)uc_link->uc_mcontext.fault_address);
    write_fd(fd, buffer, size);
}
#else
static void exc_print_registers(ucontext_t *uc_link, int fd)
{
    char buffer[MAX_LINE_SIZE];
    int size;

    size = snprintf(buffer, MAX_LINE_SIZE, "%s ====== registers :\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);
    size = snprintf(buffer, MAX_LINE_SIZE ,
        "%s TRAP_NO = 0x%08lx, ERROR_CODE = 0x%08lx, OLDMASK =0x%08lx\n",
        SIGHDR_PREFIX,
        uc_link->uc_mcontext.trap_no,
        uc_link->uc_mcontext.error_code,
        uc_link->uc_mcontext.oldmask);
    write_fd(fd, buffer, size);
    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s R0  = 0x%08lx, R1  = 0x%08lx, R2  = 0x%08lx, R3  = 0x%08lx\n",
        SIGHDR_PREFIX,
        uc_link->uc_mcontext.arm_r0,
        uc_link->uc_mcontext.arm_r1,
        uc_link->uc_mcontext.arm_r2,
        uc_link->uc_mcontext.arm_r3);
    write_fd(fd, buffer, size);
    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s R4  = 0x%08lx, R5  = 0x%08lx, R6  = 0x%08lx, R7  = 0x%08lx\n",
        SIGHDR_PREFIX,
        uc_link->uc_mcontext.arm_r4,
        uc_link->uc_mcontext.arm_r5,
        uc_link->uc_mcontext.arm_r6,
        uc_link->uc_mcontext.arm_r7);
    write_fd(fd, buffer, size);
    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s R8  = 0x%08lx, R9  = 0x%08lx, R10 = 0x%08lx, FP  = 0x%08lx\n",
        SIGHDR_PREFIX,
        uc_link->uc_mcontext.arm_r8,
        uc_link->uc_mcontext.arm_r9,
        uc_link->uc_mcontext.arm_r10,
        uc_link->uc_mcontext.arm_fp);
    write_fd(fd, buffer, size);
    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s IP  = 0x%08lx, SP  = 0x%08lx, LR  = 0x%08lx, PC  = 0x%08lx\n",
        SIGHDR_PREFIX,
        uc_link->uc_mcontext.arm_ip,
        uc_link->uc_mcontext.arm_sp,
        uc_link->uc_mcontext.arm_lr,
        uc_link->uc_mcontext.arm_pc);
    write_fd(fd, buffer, size);
    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s CPSR = 0x%08lx, FAULT_ADDRESS = 0x%08lx\n",
        SIGHDR_PREFIX,
        uc_link->uc_mcontext.arm_cpsr,
        uc_link->uc_mcontext.fault_address);
    write_fd(fd, buffer, size);
}
#endif
#endif

/* function to read the backtrace written by backtrace_symbols_fd into a file and write each line with OSALEXC into errmem for easier
   filtering of errmem output */
static int WriteCallstackFromfile(int fdread,int fdwrite,int LineLen,int NrOffEntries)
{
    int offset = 0;
    int len = 0;
    int PreLen = strlen(SIGHDR_PREFIX);
    int Readlen,WriteLen;
    char buffer[LineLen];
    char* ptr;
    int Cnt = 0;
    
    strncpy(buffer, SIGHDR_PREFIX,PreLen);
    if((!fdread)||(!fdwrite))
    {
       return -1;
    }
    while(1)
    {
       if(lseek(fdread,offset, SEEK_SET) == -1)
       {
           return -1;
       }
       if((Readlen = read(fdread,&buffer[PreLen], LineLen-PreLen-1)) > 0)
       {
           ptr = strchr(buffer, ']'); /*lint !e505 */ 
           if (!ptr) /*lint !e774:strchr can result in NULL !*/
           {
              WriteLen = PreLen + Readlen;
              buffer[LineLen-1] = '\n';
              offset += Readlen;
           }
           else
           {
              len = (uintptr_t)ptr - (uintptr_t)&buffer[0];
              offset += len - PreLen + 2; /* ]\n */
              WriteLen = len + 2;
           //   buffer[PreLen + len + 1] = '\n';
           }
           write_fd(fdwrite,&buffer[0], WriteLen);
           Cnt++;
           if(Cnt > NrOffEntries)
           {
              write_fd(fdwrite,"Number of entries overwritten", strlen("Number of entries overwritten"));
              if(Cnt > NrOffEntries+10)break;
           }
       }
       else
       {
           break;
       }
    }
    return 0;
}


static char *fd_gets(char *buffer, int size, int fd)
{
    int current_offset;
    char *end_of_line;
    int len;
    
    current_offset = lseek(fd, 0, SEEK_CUR);
    if (current_offset < 0)
    {
        return NULL;
    }
    memset(buffer, 0, size);
    if (read(fd, buffer, size-1) <= 0)
    {
        return NULL;
    }
    buffer[size-1] = 0;
    end_of_line = strchr(buffer, '\n'); /*lint !e505 */ 
    if (!end_of_line) /*lint !e774:strchr can result in NULL !*/
    {
        end_of_line = buffer+size-2;
        buffer[size-2] = '\n';
    }
    len = end_of_line - buffer +1;
    buffer[len] = 0;
    lseek(fd, current_offset+len, SEEK_SET);
    return buffer;
}


static void exc_get_process_name(int pid, char *buffer, int size)
{
    int fd;
    char name_buffer[MAX_LINE_SIZE];
    const char *buf_ptr;
    int found;

    found = 0;
    /* first try to read the name directly from "proc/<pid>comm" */
    snprintf(name_buffer, MAX_LINE_SIZE-1, "/proc/%d/comm", pid);
    fd = open(name_buffer, O_RDONLY);
    if (fd >= 0)
    {
        if(fd_gets(buffer, MAX_LINE_SIZE, fd) == NULL)
        {
            strncpy(buffer, "<no name>", size-1);
            buffer[size-1] = 0; 
        }
        close(fd);
        goto exc_get_process_name_exit;    /*lint !e801 goto is better here*/
    }
    /* "proc/<pid>/comm" does not exists, try to parse "/proc/<pid>/status" */
    snprintf(name_buffer, MAX_LINE_SIZE-1, "/proc/%d/status", pid);
    fd = open(name_buffer, O_RDONLY);
    if (fd >= 0)
    {
        while(fd_gets(buffer, size, fd))
        {
            buffer[size-1] =0;
            if(strncmp(buffer, "Name:", 5) == 0) /*lint !e530 */
            {
                found = 1;
                buf_ptr = strrchr(buffer, '\t');
                if(buf_ptr)
                {
                    memmove(buffer, buf_ptr+1, strlen(buf_ptr+1)+1);
                }
                else
                {
                    strncpy(buffer, "<no name>", size-1);
                    buffer[size-1] = 0; 
                }
                break;
            }
        }
        close(fd);
    }
    if(!found)
    {
        strncpy(buffer,"<unknown>", size-1);
        buffer[size-1] = 0; 
    }
exc_get_process_name_exit:
    /* cut away trailing CR */
    while (buffer[strlen(buffer)-1] == '\n')
        buffer[strlen(buffer)-1] = 0;

}

/*
 * check if exception address is a stack overflow
 */
static int exc_is_stack_overflow(unsigned long fault_addr, unsigned long sp)
{
    if(sp > fault_addr)
        return (sp - fault_addr) < PAGE_SIZE;
    else
        return (fault_addr - sp) < PAGE_SIZE;
}

static void exc_print_thread(char *buffer, int buffer_size, int fd, int list_is_locked)
{
    int size;
    char cBuffer[32];
    const char *name = cBuffer;
    ((void)list_is_locked);
    memset(cBuffer,0,32);
    if(!exc_get_thread_info(cBuffer,16))
    {
       strncpy(cBuffer,"Unknown",strlen("Unknown"));
    }
    else
    {
       cBuffer[15] = '\0';
    }
    /* avoid \n after name */
    size = strlen(name);
    if(size>2)cBuffer[size-1] = '\0';

    size = snprintf(buffer, buffer_size,"%s ************ thread %s (TID %d)\n",
                    SIGHDR_PREFIX,name,(int)gettid());
    write_fd(fd, buffer, size);
}

/*
 * print out exception information
 * only snprintf and write is used
 * to avoid memory allocations
 */


static void exc_print_map(char *buffer, int fd)
{
    int f;
    char *s;
    int i;
    static char *end_of_map = "(map) ...\n";

    write_fd(fd, mmap_string, strlen(mmap_string));
    f = open("/proc/self/maps", O_RDONLY);

    if(f >= 0)
    {
        int offset = 0;
        offset = strlen(SIGHDR_PREFIX);
        strcpy(buffer, SIGHDR_PREFIX);
        for(i=0; i < MAX_MAP_ENTRIES; ++i)
        {
            s = fd_gets(buffer+offset, MAX_LINE_SIZE-offset, f);
            if(!s)
                break;
            write_fd(fd, buffer, strlen(buffer));
        }
        if(i == MAX_MAP_ENTRIES)
            write_fd(fd, end_of_map, strlen(end_of_map));
        close(f);
    }
}

static void exc_trace_out_memory(int fd, unsigned long addr, int start_index, int end_index, int trace_16_bit)
{
    char buffer[100];
    unsigned short *memory_16_bit;
    unsigned int *memory_32_bit;
    int i;
    int have_access;
    size_t size;
    
    if (trace_16_bit)
    {
        /*trace out 16 bit values because thumb code was executed */
        memory_16_bit = (unsigned short *)(addr & (~1));
        for(i=start_index; i <= end_index; ++i)
        {
            size = snprintf(buffer, sizeof(buffer), "%s 0x0%" PRIxPTR " : ",
                            SIGHDR_PREFIX,(uintptr_t)(memory_16_bit+i));
            have_access = exc_check_access(memory_16_bit+i); 
            if(i==0)
            {
                if(have_access)
                {
                    size +=  snprintf(buffer+size, sizeof(buffer),
                        " --> 0x%04x\n",
                        memory_16_bit[i]);
                }
                else
                {
                    size += snprintf(buffer+size, sizeof(buffer),
                        " --> ??????\n");
                }
            }
            else
            {
                if(have_access)
                {
                    size += snprintf(buffer+size, sizeof(buffer),
                        "     0x%04x\n",
                        memory_16_bit[i]);
                }
                else
                {
                    size += snprintf(buffer+size, sizeof(buffer),
                        "     ??????\n");
                }
            }
            write_fd(fd, buffer, size);
        }
    }
    else
    {
        /*trace out 32 bit values because arm code was executed */
        memory_32_bit = (unsigned int *)(addr & (~1));
        for(i=start_index; i <= end_index; ++i)
        {
            size = snprintf(buffer, sizeof(buffer), "%s 0x%" PRIxPTR " : ",
                            SIGHDR_PREFIX,(uintptr_t)(memory_32_bit+i));
            have_access = exc_check_access(memory_32_bit+i); 
            if(i==0) {
                if(have_access) {
                    size += snprintf(buffer+size, sizeof(buffer)-size,
                        " --> 0x%08x\n",
                        memory_32_bit[i]);
                } else {
                    size += snprintf(buffer+size, sizeof(buffer)-size,
                        " --> ??????????\n");
                }
            } else {
                if(have_access) {
                    size += snprintf(buffer+size, sizeof(buffer)-size,
                        "     0x%08x\n",
                        memory_32_bit[i]);
                } else {
                    size += snprintf(buffer+size, sizeof(buffer)-size,
                        "     ??????????\n");
                }
            }
            write_fd(fd, buffer, size);
        }
    }    
} 

static void exc_trace_out_memory_around_pc(int fd, ucontext_t *uc_link)
{
    int trace_16_bit;
    char buffer[80];
    unsigned long addr;
    size_t size;

    size = snprintf(buffer, sizeof(buffer), "%s ======= Memory around last code executed:\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);

#ifdef EXC_ARM
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
    addr = uc_link->uc_mcontext.pc;
    if (!exc_check_access((void *)addr)) {
        size = snprintf(buffer, sizeof(buffer), "%s ======= PC register not accessible -> use Link regiuster\n",SIGHDR_PREFIX);
        write_fd(fd, buffer, size);
       /* pc cannot be accessed, use lr instead */ 
        addr = uc_link->uc_mcontext.regs[23];
    }
#else
    addr = uc_link->uc_mcontext.arm_pc;
    if (!exc_check_access((void *)addr)) {
        size = snprintf(buffer, sizeof(buffer), "%s ======= PC register not accessible -> use Link regiuster\n",SIGHDR_PREFIX);
        write_fd(fd, buffer, size);
        /* pc cannot be accessed, use lr instead */ 
        addr = uc_link->uc_mcontext.arm_lr;
    }
#endif
    trace_16_bit = (((unsigned long)(addr)) & 1) == 1;
#else
    /* use only 32 bit dumps for intel */
    trace_16_bit = 0;
    addr =INSTRUCTION_POINTER(uc_link);
#endif
    exc_trace_out_memory(fd, addr, -EXC_TRACE_AROUND_CODE_SIZE, EXC_TRACE_AROUND_CODE_SIZE, trace_16_bit);
 }



#ifdef UBUNTU_ENV
tBool bGetStackStartAddress(int Tid,char* buffer, unsigned long addr)
{
    char* pRet = NULL;
    char szBuffer[100];
    tBool bRet = FALSE;
    int i;
    unsigned long ulVal = 0;
    unsigned long ulVal2 = 0;
    ((void)Tid); 

    int f = open("/proc/self/maps", O_RDONLY);
    if(f >= 0)
    {
        for(i=0; i < MAX_MAP_ENTRIES; ++i)
        {
            pRet = fd_gets(buffer, MAX_LINE_SIZE, f);
            if(!pRet)
                break;

             memcpy(szBuffer,buffer+9,8);
            szBuffer[8] = '\0';
            ulVal = strtoul(szBuffer,NULL,16);
            if ((errno == ERANGE && (ulVal == (tU32)LONG_MAX || ulVal == (tU32)LONG_MIN))
             || (errno != 0 && ulVal == 0)) 
            {  
               write_fd(errmem_fd, "OSALEXC strtol failed !!!", strlen("OSALEXC strtol failed !!!"));
               ulVal = 0;
            }
            if(addr <= ulVal)
            {
               memcpy(szBuffer,buffer,8);
               szBuffer[8] = '\0';
               ulVal2 = strtoul(szBuffer,NULL,16);
               if ((errno == ERANGE && (ulVal2 == (tU32)LONG_MAX || ulVal2 == (tU32)LONG_MIN))
                || (errno != 0 && ulVal2 == 0)) 
               {  
                  write_fd(errmem_fd, "OSALEXC strtol failed !!!", strlen("OSALEXC strtol failed !!!"));
                  ulVal2 = 0;
               }
               else
               {
                 if(addr >= ulVal2)
                 {
                    bRet = TRUE;
                    break;
                 }
               }
            }

        }
        close(f);
    }
    return bRet;
}
#else
tBool bGetStackStartAddress(int Tid,char* buffer,unsigned long addr)
{
    char* pRet = NULL;
    char Pattern[30];
    tBool bRet = FALSE;
    int i;
    ((void)addr);
    memset(Pattern,0,30);
    if(Tid != getpid())
    {
      snprintf(Pattern,30,"[stack:%d]",Tid);
    }
    else
    {
      strncpy(Pattern,"[stack]",strlen("[stack]"));
    }
    int f = open("/proc/self/maps", O_RDONLY);
    if(f >= 0)
    {
        for(i=0; i < MAX_MAP_ENTRIES; ++i)
        {
            pRet = fd_gets(buffer, MAX_LINE_SIZE, f);
            if(!pRet)
                break;
/*  2f6b6000-2f6c5000 rwxp 00000000 00:00 0          [stack:1234]*/
            if((pRet = strstr(buffer,Pattern))!= NULL)
            {
                break;
            }
        }
        close(f);
    }
    if(pRet)
    {
        bRet = TRUE;
    }
    else
    {
        char Buffer[50];
        snprintf(Buffer,50,"Cannot find Pattern %s \n",Pattern);
        write_fd(errmem_fd,Buffer,strlen(Buffer));
    }
    return bRet;
}
#endif


void WriteStackUsage(ucontext_t *uc_link,int fd)
{
    char szBuffer[MAX_LINE_SIZE];
    char Buffer[MAX_LINE_SIZE];
    int Tid = gettid();
    unsigned long addr = 0;
    unsigned long ulVal = 0;
    unsigned long ulVal2 = 0;
    int Val = 0;

    /* set stack pointer adress */
#ifdef EXC_ARM
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
    addr = uc_link->uc_mcontext.sp;
#else
    addr = uc_link->uc_mcontext.arm_sp;
#endif
#else
    addr =STACK_POINTER(uc_link);
#endif
    /* get mapped stack area */
    if(bGetStackStartAddress(Tid,Buffer,addr))
    {
       snprintf(szBuffer,MAX_LINE_SIZE,"%s TID %d Stack Area %s",SIGHDR_PREFIX,Tid,Buffer);
       if (fd > 0)
       {
          write(fd, szBuffer,strlen(szBuffer));
       }
       else
       {
          write_fd(errmem_fd, szBuffer,strlen(szBuffer));
       }
       memcpy(szBuffer,Buffer+9,8);
       szBuffer[8] = '\0';
       ulVal = strtoul(szBuffer,NULL,16);
       if ((errno == ERANGE && (ulVal == (tU32)LONG_MAX || ulVal == (tU32)LONG_MIN))
        || (errno != 0 && ulVal == 0)) 
        {  
           write_fd(errmem_fd, "OSALEXC strtol failed !!!", strlen("OSALEXC strtol failed !!!"));
           ulVal = 0;
        }

       memcpy(szBuffer,Buffer,8);
       szBuffer[8] = '\0';
       ulVal2 = strtoul(szBuffer,NULL,16);
       if ((errno == ERANGE && (ulVal2 == (tU32)LONG_MAX || ulVal2 == (tU32)LONG_MIN))
        || (errno != 0 && ulVal2 == 0)) 
        {  
           write_fd(errmem_fd, "OSALEXC strtol failed !!!", strlen("OSALEXC strtol failed !!!"));
           ulVal2 = 0;
        }
        Val = ulVal-ulVal2;
    }
    else
    {
        ulVal = addr;
    }

    snprintf(szBuffer,MAX_LINE_SIZE,"%s TID %d Stack Size:%d Bytes Used:%d Bytes Stack Pointer Adress 0x%x \n",
             SIGHDR_PREFIX,Tid,Val,(int)(ulVal-addr),(int)addr);
    if (fd > 0)
    {
       write(fd, szBuffer,strlen(szBuffer));
    }
    else
    {
       write_fd(errmem_fd, szBuffer,strlen(szBuffer));
    }
}

static void exc_trace_out_stack_memory(int fd, ucontext_t *uc_link)
{
    char buffer[80];
    unsigned long addr;
    size_t size;

    size = snprintf(buffer, sizeof(buffer), "%s ======= stack memory:\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);

#ifdef EXC_ARM
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
    addr = uc_link->uc_mcontext.sp;
#else
    addr = uc_link->uc_mcontext.arm_sp;
#endif
#else
    addr =STACK_POINTER(uc_link);
#endif
    exc_trace_out_memory(fd, addr, 0, EXC_STACK_AROUND_SP_SIZE, 0);
}


#ifdef LIBUNWIND_USED
#define BACKTRACE_UNW_TEXT " ====== backtrace (libunwind):\n"
#else
#define BACKTRACE_TEXT " ====== backtrace (glibc):\n"
#endif

#define IS_INDEX_NUMBER_VALID(index, array) \
            (((unsigned int)index) <= (sizeof(array)/sizeof(array[0])) \
                && array[index])


/**
  * split the printing in case backtrace segfaults itself
  * function is only called if "uc_link" and "siginfo" are valid
  */

static void eh_print_prologue(int n_signal, ucontext_t *uc_link,
    siginfo_t *siginfo, int fd)
{
    char buffer[MAX_LINE_SIZE];
    int size;
    unsigned int sp;

    size = snprintf(buffer, MAX_LINE_SIZE,
                    "%s ****** EXCEPTION in process PID=%d *******\n",
                    SIGHDR_PREFIX, getpid());
    write_fd(fd, buffer, size);
    switch (n_signal) {
    case SIGILL:
        if (IS_INDEX_NUMBER_VALID(siginfo->si_code, sigill_txt))
        {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s %s (%s), with fault addr = 0x%08lx\n",
                            SIGHDR_PREFIX, sys_siglist[n_signal],sigill_txt[siginfo->si_code],(unsigned long)siginfo->si_addr);
        } else {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s signal %s\n",
                            SIGHDR_PREFIX, sys_siglist[n_signal]);
        }
        break;
    case SIGFPE:
        if (IS_INDEX_NUMBER_VALID(siginfo->si_code, sigfpe_txt))
        {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s %s (%s), with fault addr = 0x%08lx\n",
                            SIGHDR_PREFIX, sys_siglist[n_signal],sigfpe_txt[siginfo->si_code],(unsigned long)siginfo->si_addr);
        } else {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s signal %s\n",
                            SIGHDR_PREFIX, sys_siglist[n_signal]);
        }
        break;
    case SIGSEGV:
     #ifdef EXC_ARM
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
            sp = uc_link->uc_mcontext.sp;
#else
            sp = uc_link->uc_mcontext.arm_sp;
#endif
    #else
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
           sp = uc_link->uc_mcontext.gregs[REG_RSP];
#else
           sp = uc_link->uc_mcontext.gregs[REG_ESP];
#endif
     #endif
        if (exc_is_stack_overflow((unsigned long)siginfo->si_addr, sp))
        {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s %s (possible stack overflow), with fault addr = 0x%08lx\n",
                            SIGHDR_PREFIX, sys_siglist[n_signal],(unsigned long)siginfo->si_addr);
        }
        else
        {
            if (IS_INDEX_NUMBER_VALID(siginfo->si_code, sigsegv_txt))
            {
                size = snprintf(buffer, MAX_LINE_SIZE,"%s %s (%s), with fault addr = 0x%08lx\n",
                                SIGHDR_PREFIX, sys_siglist[n_signal], sigsegv_txt[siginfo->si_code],(unsigned long)siginfo->si_addr);
            } else {
                size = snprintf(buffer, MAX_LINE_SIZE,"%s signal %s, with fault addr = 0x%08lx\n",
                                SIGHDR_PREFIX, sys_siglist[n_signal],(unsigned long)siginfo->si_addr);
            }
        }
        break;
    case SIGBUS:
        if (IS_INDEX_NUMBER_VALID(siginfo->si_code, sigbus_txt))
        {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s %s (%s), with fault addr = 0x%08lx\n", 
                            SIGHDR_PREFIX,sys_siglist[n_signal],sigbus_txt[siginfo->si_code],(unsigned long)siginfo->si_addr);
        } else {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s signal %s, with fault addr = 0x%08lx\n\n", 
                            SIGHDR_PREFIX,sys_siglist[n_signal],(unsigned long)siginfo->si_addr);
        }            
        break;
    case SIGIO:
        if (IS_INDEX_NUMBER_VALID(siginfo->si_code, sigio_txt))
        {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s %s (%s), \n", 
                            SIGHDR_PREFIX,sys_siglist[n_signal],sigio_txt[siginfo->si_code]);
        } else {
            size = snprintf(buffer, MAX_LINE_SIZE,"%s signal %s\n", 
                            SIGHDR_PREFIX,sys_siglist[n_signal]);
        }            
        break;
/*    case SIGABRT:
        {
            char procname[MAX_LINE_SIZE];
            exc_get_process_name(siginfo->si_pid,
                         procname, sizeof(procname));
            size = snprintf(buffer, MAX_LINE_SIZE,
                    "signal %s from pid %d (%s)\n",
                    sys_siglist[n_signal],
                    siginfo->si_pid,
                    procname);
            sigaction_backtrace_func(SIG_BACKTRACE, NULL, NULL);
        }
        break;*/
    default : size = snprintf(buffer, MAX_LINE_SIZE,
                              "%s signal \"%s\"\n",SIGHDR_PREFIX, sys_siglist[n_signal]);
        break;
    }

    write_fd(fd, buffer, size);

    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s command line : \"%s\"\n",SIGHDR_PREFIX, commandline);
    write_fd(fd, buffer, size);
    exc_print_thread(buffer, MAX_LINE_SIZE, fd, 0);
    exc_print_registers(uc_link, fd);
}

#ifdef LIBUNWIND_USED
typedef int (*fpunw_getcontext)(unw_context_t *ucp);
typedef int (*fpunw_init_local)(unw_cursor_t *c, unw_context_t *ctxt);
typedef int (*fpunw_step)(unw_cursor_t *);
typedef int (*fpunw_get_reg)(unw_cursor_t *, unw_regnum_t, unw_word_t *);
typedef int (*fpunw_backtrace)(void **buffer, int size);

static fpunw_getcontext pgetcontext = NULL;
static fpunw_init_local pinit_local = NULL;
static fpunw_step       pstep       = NULL;
static fpunw_get_reg    pget_reg    = NULL;
static fpunw_backtrace  pbacktrace  = NULL;

int s32UnwindInstalled = 0;

static void vPrintUnwindError(int Val, char* func)
{
                switch(Val)
                {
                case UNW_EUNSPEC:
                    vWritePrintfErrmem("Unwind problem %s error:%s \n",func,"UNW_EUNSPEC");
                    break;
                case UNW_ENOINFO:
                    vWritePrintfErrmem("Unwind problem %s error:%s \n",func,"UNW_ENOINFO");
                    break;
                case UNW_EBADVERSION:
                    vWritePrintfErrmem("Unwind problem %s error:%s \n",func,"UNW_EBADVERSION");
                    break;
                case UNW_EINVALIDIP:
                    vWritePrintfErrmem("Unwind problem %s error:%s \n",func,"UNW_EINVALIDIP");
                    break;
                case UNW_EBADFRAME:
                    vWritePrintfErrmem("Unwind problem %s error:%s \n",func,"UNW_EBADFRAME");
                    break;
                case UNW_ESTOPUNWIND:
                    vWritePrintfErrmem("Unwind problem %s error:%s \n",func,"UNW_ESTOPUNWIND");
                    break;
                case UNW_EBADREG:
                    vWritePrintfErrmem("Unwind problem %s error:%s \n",func,"UNW_EBADREG");
                    break;
                default:
                    vWritePrintfErrmem("Unwind problem %s error:%s \n",func,"Unknown");
                    break;
                }
}

/* function to get call chain adresses with libunwind for callstack generation */
tU32 u32Unwind(void** addr,tU32 u32Size)
{
   unw_cursor_t cursor; 
   unw_context_t uc;
   size_t j = 0;
   int Ret = 0;
   unw_word_t ip = 0;
 /*  unw_word_t sp = 0;
   char name[BUF_SIZE];
   unsigned offp = 0;*/
  
 //  int sigact_result;
 //  struct sigaction act_old;
  // sigact_result = sigaction(SIGSEGV, &act_sigsegvreplace, &act_old);

   if(pbacktrace)return pbacktrace(addr,u32Size);
	   
   memset(&cursor,0,sizeof(cursor));
#ifndef __LINT__ 
   Ret = pgetcontext(&uc);
   if(Ret == 0)
   {
#endif
      if((Ret = pinit_local (&cursor, &uc)) != 0)
      { 
         vPrintUnwindError(Ret,"OSALEXC unw_init_local");
      }
      else
      {
          write_fd(errmem_fd,"OSALEXC Start unwinding \n", strlen("OSALEXC Start unwinding \n"));
          while (pstep (&cursor) > 0)
          {
              if (j >= u32Size)return j;

    //          if((Ret=unw_get_proc_name (&cursor, &name[0], BUF_SIZE, &offp)) != UNW_ESUCCESS)return i;

              if (pget_reg (&cursor, UNW_REG_IP, &ip) < 0)return j;

    //          if (unw_get_reg(&cursor, UNW_REG_SP, &sp) < 0)return i;

    //          vWritePrintfErrmem("Name:%s ip = %lx, sp = %lx\n",name, (long) ip, (long) sp);
              
              addr[j++] = (void *) ip;
          }
      }
#ifndef __LINT__ 
   }
   else
   {
      vPrintUnwindError(Ret,"OSALEXC unw_getcontext");
   }
#endif
   /* Enable the signal back */
    //if(!sigact_result)
      // sigaction(SIGSEGV, &act_old, NULL);

   return j;
}
#endif


int ConnectLibUnwind(void)
{
   int Ret = 0;

   Ret++;
#ifdef LIBUNWIND_USED
   dlerror();    /* Clear any existing error */
   if((pUnwModuleHandle = dlopen("libunwind.so", RTLD_NOW)) != NULL)
   {
//      TraceString("dlopen libunwind executed !!!");
      pgetcontext = (fpunw_getcontext)(dlsym(pUnwModuleHandle, (const char*)"unw_getcontext"));/*lint !e611 */;
      pinit_local = (fpunw_init_local)(dlsym(pUnwModuleHandle, (const char*)"unw_init_local"));/*lint !e611 */
      pstep       = (fpunw_step)(dlsym(pUnwModuleHandle, (const char*)"unw_step"));/*lint !e611 */
      pget_reg    = (fpunw_get_reg)(dlsym(pUnwModuleHandle, (const char*)"unw_get_reg"));/*lint !e611 */
      pbacktrace  = (fpunw_backtrace)(dlsym(pUnwModuleHandle, (const char*)"unw_backtrace"));/*lint !e611 */

      if((!pgetcontext)||(!pinit_local)||(!pstep)||(!pget_reg))
      {
//        TraceString("dlsym for libunwind failed %d %d %d %d !!!",pgetcontext,pinit_local,pstep,pget_reg);
        if(pbacktrace)
        {
           Ret++;
        }
        else		
        {
           dlclose(pUnwModuleHandle);
        }
      }
      else
      {
         Ret++;
      }
   }
 #endif
//      TraceString("ConnectLibUnwind returns %d !!!",Ret);
   return Ret;
}
 
void vCheckLibUnwind(void)
{
#ifdef LIBUNWIND_USED
   if(s32UnwindInstalled == 0)
   {
      s32UnwindInstalled = ConnectLibUnwind();
   }
#endif
}

/* function to avoid multiple entries generated by malfunctions instead of recursive calls  */ 
static int vFilterEntries(void* pointers , int used_pointers)
{
    unsigned int* pAdress = (unsigned int*)pointers;
    unsigned int u32OldAdress = 0;
    int i;
    int count = 0;
    for(i=0;i<used_pointers;i++)
    {
        if(u32OldAdress == *pAdress)
        {
            count++;
            if(count == 3)break;
        }
        u32OldAdress = *pAdress;
        pAdress++;
    }
    return i;
}

/* funtion for print callstack information */
static void exc_print_callstack(void **pointers, int used_pointers,
                                int assembler_backtrace_used, int fd)
{
    char buffer[MAX_LINE_SIZE];
    int size;
    ((void)assembler_backtrace_used);
    write_fd(fd, SIGHDR_PREFIX, strlen(SIGHDR_PREFIX));
#ifdef LIBUNWIND_USED
    size = snprintf(buffer, MAX_LINE_SIZE,BACKTRACE_UNW_TEXT);
#else
    size = snprintf(buffer, MAX_LINE_SIZE,BACKTRACE_TEXT);
#endif //LIBUNWIND_USED
    write_fd(fd, buffer, size);

#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
#ifdef USE_CS_FILE
    used_pointers = vFilterEntries(pointers,used_pointers);
    backtrace_symbols_fd(pointers+EXC_IGNORE_POINTERS,
                        used_pointers-EXC_IGNORE_POINTERS,
                        errmem_cs_fd);
    WriteCallstackFromfile(errmem_cs_fd,fd,MAX_LINE_SIZE,used_pointers);
    /* remove file content and proceed*/
    if(truncate(ERRMEM_CSFILE,0) == -1)
    {
       size = snprintf(buffer, MAX_LINE_SIZE,
                       "%s truncate %s failed %d\n",SIGHDR_PREFIX,ERRMEM_CSFILE,errno);
       write_fd(fd, buffer, size);
    }
    if(lseek(errmem_cs_fd,0, SEEK_SET) == -1)
    {
       size = snprintf(buffer, MAX_LINE_SIZE,
                       "%s lseek %s failed %d\n",SIGHDR_PREFIX,ERRMEM_CSFILE,errno);
       write_fd(fd, buffer, size);
    }
#else //USE_CS_FILE
   #define MAX_DEEP 10
   #define TRC_BUF_SIZE 240
   int  i, CallStackDeep;
   char **strings;
   char cTraceBuffer[TRC_BUF_SIZE];

   CallStackDeep = backtrace(pointers, MAX_DEEP );
   strings       = backtrace_symbols(pointers+EXC_IGNORE_POINTERS,
                                     used_pointers-EXC_IGNORE_POINTERS);

   if( strings == NULL )
   {
      cTraceBuffer[0] = 0x07;
      sprintf((char*)(cTraceBuffer+1), "OSALEXC backtrace error");
      write_fd(fd,buffer,strlen(&buffer[0]));
   }
   else
   {
      int Len=strlen(SIGHDR_PREFIX);
      strncpy(&buffer[1],SIGHDR_PREFIX,Len);
      cTraceBuffer[0] = 0x07;
      for( i = 1; i < CallStackDeep; i++ ) // 1: we don't want to see the function call of OSAL_trace_callstack()
      {
         memset(cTraceBuffer+1+Len, 0, TRC_BUF_SIZE-1-Len);
         strncpy( cTraceBuffer+1+Len, strings[i], TRC_BUF_SIZE-2-Len );
         cTraceBuffer[ TRC_BUF_SIZE-1-Len] = 0;
         write_fd(fd,cTraceBuffer,strlen(cTraceBuffer));
      }
   }
   free(strings);
#endif //USE_CS_FILE

#else //VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
#ifdef USE_CS_FILE
    used_pointers = vFilterEntries(pointers,used_pointers);

    backtrace_symbols_fd(pointers+EXC_IGNORE_POINTERS,
                        used_pointers-EXC_IGNORE_POINTERS,
                        errmem_cs_fd);
    WriteCallstackFromfile(errmem_cs_fd,fd,MAX_LINE_SIZE,used_pointers);
    /* remove file content and proceed*/
    if(truncate(ERRMEM_CSFILE,0) == -1)
    {
       size = snprintf(buffer, MAX_LINE_SIZE,
                       "%s truncate %s failed %d\n",SIGHDR_PREFIX,ERRMEM_CSFILE,errno);
       write_fd(fd, buffer, size);
    }
    if(lseek(errmem_cs_fd,0, SEEK_SET) == -1)
    {
       size = snprintf(buffer, MAX_LINE_SIZE,
                       "%s lseek %s failed %d\n",SIGHDR_PREFIX,ERRMEM_CSFILE,errno);
       write_fd(fd, buffer, size);
    }

#else //USE_CS_FILE
    backtrace_symbols_fd(pointers+EXC_IGNORE_POINTERS,
            used_pointers-EXC_IGNORE_POINTERS,
            fd);
#endif //USE_CS_FILE
#endif // VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s *** end of current callstack *****\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);
}





/*
 * print function for backtrace
 * this will be call by normal signal handler
 */

static void exc_print_exception(int n_signal, ucontext_t *uc_link,
        siginfo_t *siginfo,
        void **pointers, int used_pointers,
        int assembler_backtrace_used, int fd)
{
    char buffer[MAX_LINE_SIZE];
    int size;

    ((void)n_signal);
    ((void)siginfo);

    exc_print_callstack(pointers,used_pointers,assembler_backtrace_used,fd);
    exc_print_map(buffer, fd);
    exc_trace_out_memory_around_pc(fd, uc_link);
    exc_trace_out_stack_memory(fd, uc_link);
    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s *** end of exception log *****\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);
}

/*
 * print function for backtrace
 * this will be call by signal handler for "SIG_INTERNAL"
 */


static void exc_print_backtrace(void **pointers, int used_pointers,
        int fd, int assembler_backtrace_used, ucontext_t *uc_link)
{
    char buffer[MAX_LINE_SIZE];

    exc_print_thread(buffer, MAX_LINE_SIZE, fd, 1);
    #ifdef EXC_X86
    pointers[1] = (void *)(INSTRUCTION_POINTER(uc_link));
    #else
    (void)uc_link;
    #endif

    exc_print_callstack(pointers,used_pointers,assembler_backtrace_used,fd);
}



/*
 * check for reboot enabled or not
 */
static int should_be_rebooted(void)
{
    struct stat buf;
    char* buffer;
    static int should_be_rebooted_flag = 1;
	
	/* first look for old fashion disable_reset file */
    if(stat(EXCEPTION_HANDLER_REBOOT_FILE, &buf) != -1)    
    {
       should_be_rebooted_flag = 0;
       const char msg[] = "reboot disabled due " EXCEPTION_HANDLER_REBOOT_FILE "\n";
       write_fd(errmem_fd, msg, strlen(msg));
    }
    /* for reset check export of OSAL_RESET=0  */
    buffer = getenv("OSAL_RESET");
    if(buffer)
    {
       should_be_rebooted_flag = atol(buffer);
       if(should_be_rebooted_flag)
       {
          const char msg[] = "reboot disabled due Linux Environment\n";
          write_fd(errmem_fd, msg, strlen(msg));
       }
       TraceString("should_be_rebooted_flag %s %d",buffer,should_be_rebooted_flag);
    }
    return should_be_rebooted_flag;
}


static const struct sigaction act_exit = {
    .sa_sigaction = NULL,
    .sa_flags = SA_SIGINFO | SA_RESTART | SA_NODEFER,
};

/*
 * check if the exception callstacks seems to be not ok
 */


/*static int exc_bad_callstack(void **pointers, int used_pointers)
{
    int equal;
    
    equal = 0;
    while (used_pointers > 1) {
        used_pointers--;
        if (pointers[used_pointers] == pointers[used_pointers-1])
            ++equal;
        else
            break;    
    }
    return (equal > EXC_NUM_EQUAL_ACCEPT);
}*/

/* function to get adress pointer for callstack generation vial libc */
static int eh_stack_unwind(void **pointers, ucontext_t *uc_link, int *assembler_backtrace_used, int fd)
{
    int used_pointers;
    ((void)assembler_backtrace_used);
    ((void)uc_link);
    if (fd > 0) {
        const char buffer[] = "OSALEXC ***** before glibc backtrace\n";
        write_fd(fd, buffer, strlen(buffer));
    }
    used_pointers = backtrace(pointers, EXC_STACK_DEEPNESS);
#ifdef EXC_X86
    pointers[1] = (void *)(INSTRUCTION_POINTER(uc_link));
#endif
/*#ifdef EXC_ARM
    if ((used_pointers <= EXC_IGNORE_POINTERS+3)
        || exc_bad_callstack(pointers, used_pointers))
    {
        * 
         *    because of taking all pointers from assembler backtrace,
         *    calulate out EXC_IGNORE_POINTERS
         *
        if (fd > 0) {
            const char buffer[] = "OSALEXC ***** before asm backtrace\n";
            write_fd(fd, buffer, strlen(buffer));
        }
        used_pointers = arm_backtrace_exception(pointers+EXC_IGNORE_POINTERS,
                EXC_STACK_DEEPNESS-EXC_IGNORE_POINTERS, uc_link)
                + EXC_IGNORE_POINTERS;
        *assembler_backtrace_used = 1;
    }
#endif*/
    return used_pointers;
}

/******************************************************************************************/
/*   Handler SIGNAL child start                                                                   */
/******************************************************************************************/
static void sigaction_child(int n_signal, siginfo_t *siginfo, void *ptr)
{
   char Buffer[150];
   tS32 s32Data[4]= {0,0,0,0};
   tS32 status;
   ((void)ptr);

   if(fifo == -1)
   {
      fifo = open(VOLATILE_DIR"/OSAL/EXC_HDR_FIFO", O_WRONLY /*| O_NONBLOCK*/);
      if(fifo == -1)
      {
          snprintf(Buffer,150,"%s PID:%d open fifo failed Error %d \n",SIGHDR_PREFIX,getpid(),errno);
          write_fd(errmem_fd, Buffer, strlen(Buffer));
      }
   }
   if(fifo != -1)
   {
      /* Wait for all dead processes.
       * We use a non-blocking call to be sure this signal handler will not
       * block if a child was cleaned up in another part of the program. */
      if(waitpid(-1,&status, WNOHANG) != -1)
      {
         s32Data[2] = WIFEXITED(status);
         s32Data[0] = n_signal;
         s32Data[1] = siginfo->si_pid;
         if(s32Data[2] == -1)
         {
            s32Data[2] = siginfo->si_status;
         }
         s32Data[3] = getpid();
         if(write(fifo,(char*)&s32Data[0],4*sizeof(tS32)) == -1)
         {
             snprintf(Buffer,150,"%s PID:%d write fifo failed Error %d \n",SIGHDR_PREFIX,getpid(),errno);
             write_fd(errmem_fd, Buffer, strlen(Buffer));
         }
      }
   }
}
/******************************************************************************************/
/*   SIGNAL child end                                                                     */
/******************************************************************************************/

/* function for generation callstack by using ADIT exception handler device for calling thread */
static int TriggerCallstackByExceptionHandler(int n_signal, /*siginfo_t *siginfo, void *ptr,*/ int fdesc)
{
   int iRet = 0;
#ifdef VARIANT_S_FTR_ADIT_SUPPORTED 
   struct exchnd_on_demand param;
   int rc;
   char buffer[MAX_LINE_SIZE];
   int size = 0;
   int exh_fd = open("/dev/exchnd", O_WRONLY);
   if (exh_fd <= 0) 
   {
        iRet = -1;
        return iRet;
   }
   param.pid = gettid();
   param.use_def = 0; /* Deprecated - Unused */
   param.modules[0] = EHM_PROCESSOR_REGISTERS;
   param.modules[1] = EHM_STACK_DUMP;
   param.modules[2] = EHM_BACKTRACE;
   param.modules[3] = EHM_MEMORY_MAP;
   param.modules[4] = EHM_THREAD_LIST;
   param.modules[5] = EHM_FAULT_ADDRESS;
   param.modules[6] = EHM_MEMORY_DUMP;
#ifdef EXC_ARM
   if(should_be_rebooted()) 
   {
      param.modules[7] = EHM_SYS_RESTART;
      size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d received signal:%d -> trigger ADIT exception handler with restart\n",
                      SIGHDR_PREFIX,getpid(),n_signal);
   } 
   else 
   {
      size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d received signal:%d -> trigger ADIT exception handler without restart\n",
                      SIGHDR_PREFIX,getpid(),n_signal);
   }
#else
   param.modules[7] = EHM_SYS_RESTART;
#endif
   write_fd(fdesc,buffer, size);
 /*  ucontext_t* uc_link = (ucontext_t *)ptr;
   eh_print_prologue(n_signal, uc_link, siginfo, fdesc);*/

   strncpy(param.msg, "I'm an EXCHND_ON_DEMAND trigger from OSAL", 256);
   rc = ioctl(exh_fd, IOCTL_EXCHND_ON_DEMAND, &param);
   if (rc >= 0) 
   {
      sleep(5000);
      iRet = 0;
   }
   else
   {
      iRet = -2;
   }
#else
   ((void)n_signal);
   ((void)fdesc);
   /* exc_print_map(buffer, fd);
    exc_trace_out_memory_around_pc(fd, uc_link);
    exc_trace_out_stack_memory(fd, uc_link);
    size = snprintf(buffer, MAX_LINE_SIZE,
        "%s *** end of exception log *****\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);*/
#endif
   return iRet;
}

/* find PID for given process name */
tS32 s32FindProcessId(char* Name)
{
  DIR *pDir;
  char szName[64];
  char buffer[64];
  struct dirent *pDEntry;
  tS32 s32Pid = 0;
  int fd;
  
  TraceString("Search Process %s",Name);
  pDir = opendir("/proc");
  if(pDir)
  {
    while( NULL != (pDEntry = readdir( pDir )) )
    {
       if((pDEntry->d_name[0] >= 0x30)&&(pDEntry->d_name[0] <= 0x39))
       {
             s32Pid = atoi(pDEntry->d_name);
             snprintf(szName,64,"/proc/%d/comm",s32Pid);
             fd = open(szName,O_RDONLY);
             if(fd != -1)
             {
                if(read(fd,buffer,64) > 0)
                {
                   if(strstr(buffer,Name))
                   {
                      close(fd);
                      TraceString("Found PID:%d for %s",s32Pid,Name);
                      break;
                   }
                }
                close(fd);
             }
             
       }
     }
     rewinddir(pDir);
     closedir(pDir);
  }
  return s32Pid;
}

/* function for generation of a callstack for specified TID */
void TriggerCallstackGenerationByTid(tS32 s32Pid)
{
#ifdef VARIANT_S_FTR_ADIT_SUPPORTED 
   TraceString("Generate Callstack for PID:%d",s32Pid);
   struct exchnd_on_demand param;
   int rc;
   int exh_fd = open("/dev/exchnd", O_WRONLY);
   if (exh_fd >= 0) 
   {
      param.pid = s32Pid;
      param.use_def = 0; /* Deprecated - Unused */
      param.modules[0] = EHM_BACKTRACE;
 
      strncpy(param.msg, "I'm an CALLSTACK_ON_DEMAND trigger from OSAL", 256);
      rc = ioctl(exh_fd, IOCTL_EXCHND_ON_DEMAND, &param);
      if(rc == -1)
      {
         TraceString("OSALEXH ioctl to ADIT EXC failed ");
      }
      close(exh_fd);
   }
#endif
}

/* function for generation of callstacks for specified PID */
void TriggerCallstackGenerationByPid(tS32 s32Pid)
{
#ifdef VARIANT_S_FTR_ADIT_SUPPORTED 
   TraceString("Generate Callstack for PID:%d",s32Pid);
   struct exchnd_on_demand param;
   int rc;
   int exh_fd = open("/dev/exchnd", O_WRONLY);
   if (exh_fd >= 0) 
   {
      param.pid = s32Pid;
      param.use_def = 0; /* Deprecated - Unused */
      param.modules[0] = EHM_BACKTRACE_ALL_THREADS;

      strncpy(param.msg, "I'm an CALLSTACK_ON_DEMAND trigger from OSAL", 256);
      rc = ioctl(exh_fd, IOCTL_EXCHND_ON_DEMAND, &param);
      if(rc == -1)
      {
         TraceString("OSALEXH ioctl to ADIT EXC failed ");
      }
      close(exh_fd);
   }
#else
   TraceString("Generate Callstack for PID:%d not supported",s32Pid);
#endif
}

/* function for generation of callstacks for specified process name */
void TriggerCallstackGenerationByName(char* PrcName)
{
   tS32 s32Pid = s32FindProcessId(PrcName);
   TriggerCallstackGenerationByPid(s32Pid);
}
/******************************************************************************************/
/*   SIGNAL exception start                                                               */
/******************************************************************************************/
/*
 * sigaction function for threads that runs into an exception SIGSEGV
 */

static void sigaction_func(int n_signal, siginfo_t *siginfo, void *ptr)
{
    ucontext_t *uc_link;
    void *pointers[EXC_STACK_DEEPNESS];
    int used_pointers = 0;
    int assembler_backtrace_used;
    int lock_return;
    char buffer[MAX_LINE_SIZE];
    int size;
    int fdesc;

    if (errmem_fd != -1)fdesc = errmem_fd;
    else fdesc = FD_STDOUT;


#ifdef OSAL_ERRMEM_TIMESTAMP
    vWriteUtcTime(errmem_fd);
#endif
    /* check if OSAL exception handling is enabled or SIGILL specifiec handling is required */
    if((s32ExcHdrStatus)||(n_signal == SIGILL))
    {
       /* try to generate exception output via ADIT exception handler 
          if this is successful the function will not return, when reset is not disabled */
       if(s32ExcHdrStatus == 1)
       {
         TriggerCallstackByExceptionHandler(n_signal,fdesc);
       }

    /* check for abort call from libunwind during callstack generation -> unexpected error case */
    if((n_signal == SIGABRT)&&(pOsalData->u32ExcHdrActiv) && (pOsalData->s32ExcTid == gettid()))
    {
       size = snprintf(buffer,MAX_LINE_SIZE,"%s ABORT PID:%d from libunwind map and stack info follows \n",
                      SIGHDR_PREFIX,getpid());
       write_fd(fdesc,buffer, size);
       /* generate data via OSAL */ 
       uc_link = (ucontext_t *)ptr;
       exc_print_map(buffer, fdesc);
       exc_trace_out_stack_memory(fdesc, uc_link);
       size = snprintf(buffer, MAX_LINE_SIZE,
       "%s *** end of exception log *****\n",SIGHDR_PREFIX);
       write_fd(fdesc, buffer, size);
    }
    else
    {
       /* start generating exception analysis data via OSAL signal handler,
          take the lock in the first step */
       lock_return = exception_handler_lock();
       if (lock_return != 0)
       {
          size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d Exception Print whithout lock \n",SIGHDR_PREFIX,getpid());
          write_fd(fdesc, buffer, size);
    //    exc_print_wrong_lock(lock_return);
    //    goto sigaction_func_out;    /*lint !e801 goto is better here*/    
       }
	   /* get stat address for analysis */
       uc_link = (ucontext_t *)ptr;
       assembler_backtrace_used = 0; /* switch of non portable assembler version of callstack generation */

       /* check if the received adress is accessible */
       if (exc_check_access(siginfo) && exc_check_access(uc_link))
       {
           /* set indication signal processing is in progress */ 
          pOsalData->u32ExcHdrActiv = pOsalData->u32ExcTime;
           /* print general information */
          eh_print_prologue(n_signal, uc_link, siginfo, fdesc);
          /* generate the callstack */
#ifdef LIBUNWIND_USED
          if(s32UnwindInstalled == 2)
          {
             used_pointers = u32Unwind(pointers, EXC_STACK_DEEPNESS);
          }
          else
#endif
          {
             used_pointers = eh_stack_unwind(pointers, uc_link, &assembler_backtrace_used, fdesc);
          }
          /* write callstack , map info etc. to errmem */
          exc_print_exception(n_signal, uc_link, siginfo,
                              pointers, used_pointers, assembler_backtrace_used,
                              fdesc);
       } 
       else 
       {
           /* ucontext_t was not a valid adress */
           size = snprintf(buffer, MAX_LINE_SIZE,
                           "%s signal \"%s\" (with invalid siginfo or uclink) in pid=%d\n", 
                           SIGHDR_PREFIX,sys_siglist[n_signal], getpid());
           write_fd(fdesc, buffer, size);
       }
       /* write thread information to errmem */
       char Name[32];
       exc_get_thread_info(Name,32);
       size = snprintf(buffer, MAX_LINE_SIZE-1,
                        "%s thread \"%s\" (tid %ld), commandline =\"%s\"\n",
                        SIGHDR_PREFIX,Name,gettid(),commandline);
       write_fd(fdesc, buffer, size);
       if (lock_return == 0)
       {
         /* free the global lock */  
         exception_handler_unlock();
       }
       else
       {
          size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d Exception Print whithout lock end \n",
                          SIGHDR_PREFIX,getpid());
          write_fd(fdesc,buffer, size);
       }
    }
    }
    else
    {
       size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d Deactivated OSAL Exception Handler received Signal %d\n",
                       SIGHDR_PREFIX,getpid(),n_signal);
       write_fd(fdesc,buffer, size);
    }
    /* set marker for eh_reboot is called by signal handler, to call suitable exit function */
    bExceptionActive = TRUE;
//sigaction_func_out:
    size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d Reboot via OSAL caused by signal:%d (exception)\n",
                    SIGHDR_PREFIX,getpid(),n_signal);
    write_fd(fdesc,buffer, size);
    /* set maker for valid eh_reboot call */
    pOsalData->bNoRebootCallstack = TRUE;
    eh_reboot();

    /* install default signal handler again for disabled reset */
    sigaction(n_signal, &act_exit, NULL);

    /* Raise the signal to the default handler to generate core dump file */
    raise(n_signal);
}
/******************************************************************************************/
/*   SIGNAL exception end                                                                 */
/******************************************************************************************/



/******************************************************************************************/
/*   SIGNAL STACK  start                                                                  */
/******************************************************************************************/
static int internal_lockstack = 0;

static void sigaction_checkstack_func(int n_signal, siginfo_t *siginfo, void *ptr)
{
    ((void)n_signal);
    ((void)siginfo);
    WriteStackUsage((ucontext_t*) ptr,stackfd);
    exc_unlock(&internal_lockstack);
}


static void sigaction_triggerstack_func(int n_signal, siginfo_t *siginfo, void *ptr)
{
    int sigact_result;
    int lock_return;
    struct sigaction act_old;
    DIR *pDir;
    char szName[64];
    struct dirent *pDEntry;
    int fd,tid =0;
    char szBuffer[128];

    /* make lint happy */
    (void)n_signal;
    (void)siginfo;
    (void)ptr;

    /* Block more signal to the process while we are in the handler */
    sigact_result = sigaction(n_signal, &act_ignore, &act_old);

    memset(szName,0,64);
    memset(szBuffer,0,128);
    snprintf(szName,64,"/proc/%d/cmdline",getpid());
    fd = open(szName,O_RDONLY);
    if(fd != -1)
    {
       if(read(fd,szName,64)<=0)
       {
       }
       close(fd);
    }
    else
    {
       snprintf(szName,64,"Cannot open path %s",szName);
       return;
    }

    stackfd = open(STACK_FILE, O_RDWR | O_CREAT | O_APPEND,OSAL_ACCESS_RIGTHS);
    if(stackfd == -1)
    {
       if(EEXIST == errno)
       {
          stackfd = open(STACK_FILE, O_RDWR,OSAL_ACCESS_RIGTHS);
       }
    }
    else
    {
       if(s32OsalGroupId)
       {
          if(fchown(fd,(uid_t)-1,s32OsalGroupId) == -1)
          {
             vWritePrintfErrmem("sigaction_triggerstack_func -> fchown error %d \n",errno);
          }
          if(fchmod(fd, OSAL_ACCESS_RIGTHS) == -1)
          {
             vWritePrintfErrmem("sigaction_triggerstack_func -> fchmod error %d \n",errno);
          }
       } 
    }
    vWriteUtcTime(stackfd);
    write(stackfd,"\n", strlen("\n"));
    snprintf(szBuffer,128,"%s SIG_TRIGGER_STACK (%d) received for %s \n",
            SIGHDR_PREFIX,SIG_TRIGGERSTACK,szName);
    write_fd(stackfd, szBuffer, strlen(szBuffer));

    snprintf(szName,64,"/proc/%d/task",getpid());
    pDir = opendir(szName);
    while( NULL != (pDEntry = readdir( pDir )) )
    {
      if(pDEntry->d_name[0] != '.')
      {
         tid = atoi(pDEntry->d_name);
         if(tid > 0)
         {
                  lock_return = exc_lock(&internal_lockstack);
                  if (lock_return == 0) 
                  {
                     if(tkill(tid, SIG_INTERNAL2) < 0)
                     {
                         vWritePrintfErrmem("%s tkill for task %d failed errno:%d \n",SIGHDR_PREFIX,tid,errno);
                     }
                  }
         }
      }
    }
    closedir(pDir);
    /* Enable the signal back */
    if(!sigact_result)
       sigaction(n_signal, &act_old, NULL);
}
/******************************************************************************************/
/*   SIGNAL STACK  end                                                                  */
/******************************************************************************************/



/******************************************************************************************/
/*  SIGNAL Callstack start                                                                */
/******************************************************************************************/
/*
 * sigaction function for threads that runs into internal signal SIG_INTERNAL
 */

static int internal_lock = 0;
static int SigHdrActive = 0;

static void sigaction_internal_func(int n_signal, siginfo_t *siginfo, void *ptr)
{
    ucontext_t *uc_link = (ucontext_t *)ptr;
    void *pointers[EXC_STACK_DEEPNESS];
    int used_pointers = 0;
    int assembler_backtrace_used;
    int lock_return = 0;
    tU32 u32CurTim;
    int size;
    int fdesc;
    char buffer[MAX_LINE_SIZE];
    
    (void)n_signal;
    (void)siginfo;
    assembler_backtrace_used = 0;
    if (errmem_fd != -1)fdesc = errmem_fd;
    else fdesc = FD_STDOUT;

//    unsigned int Time[2];
//    WriteStackUsage(uc_link,0);
    lock_return = exception_handler_lock();
    if (lock_return != 0)
    {
         u32CurTim = OSAL_ClockGetElapsedTime();
         u32CurTim = u32CurTim - pOsalData->u32ExcTime;
         size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d No Callstack Locked for %d msec by PID:%d TID:%d\n",
                         SIGHDR_PREFIX,getpid(),u32CurTim,pOsalData->s32ExcPid,pOsalData->s32ExcTid);
          write_fd(errmem_fd, buffer, size);
    }
    else
    {
//       Time[0] = OSAL_ClockGetElapsedTime();
#ifdef LIBUNWIND_USED
       size = snprintf(buffer,MAX_LINE_SIZE,"%s ***** before unwind backtrace\n",SIGHDR_PREFIX);
       if(s32UnwindInstalled == 2)
       {
          used_pointers = u32Unwind(pointers, EXC_STACK_DEEPNESS);
       }
       else
#endif
       {
          size = snprintf(buffer,MAX_LINE_SIZE,"%s ***** before glibc backtrace\n",SIGHDR_PREFIX);
          used_pointers = backtrace(pointers, EXC_STACK_DEEPNESS);
       }
       write_fd(fdesc, buffer, size);
       exc_print_backtrace(pointers, used_pointers,fdesc, assembler_backtrace_used, uc_link);

/*       Time[1] = OSAL_ClockGetElapsedTime();
       size = snprintf(buffer,MAX_LINE_SIZE,"%s Time for backtrace %u msec\n",SIGHDR_PREFIX,Time[1]-Time[0]);
       write_fd(errmem_fd, buffer, size);*/
       exception_handler_unlock();
    }
    exc_unlock(&internal_lock);

    SigHdrActive = 0;
}

void vPrintBitMask(tCString szBuffer, tU32 x)
{
   int count,idx = 0;
   int Bit[32];
   for (count=31; count >= 0; count--)
   {
     if(u32SigPattern[count] & x)Bit[idx] = 1;
     else Bit[idx] = 0;
     idx++;
   }
   TraceString("%s 32Bit Mask:0x%x -> %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
               szBuffer, x,
               Bit[0], Bit[1], Bit[2], Bit[3], Bit[4], Bit[5], Bit[6], Bit[7], Bit[8], Bit[9],
               Bit[10],Bit[11],Bit[12],Bit[13],Bit[14],Bit[15],Bit[16],Bit[17],Bit[18],Bit[19],
               Bit[20],Bit[21],Bit[22],Bit[23],Bit[24],Bit[25],Bit[26],Bit[27],Bit[28],Bit[29],
               Bit[30],Bit[31]);
}


tU32 u32CheckTskSignalConf(char* cPath,char* cPattern,tBool bRetUpper,tBool bPrintMask)
{
  //SigCgt: 00000007 fffbfeff -> default OSAL Task

  char szBuffer[1025];
  char* pTemp;
  char *pTemp2;
  int fd;
  tU32 u32Ret = 0;
  tU32 u32RetLow = 0;
  
  szBuffer[1024] = '\0';
  fd = open(cPath,O_RDONLY);
  if(fd != -1)
  {
    if(read(fd,&szBuffer[0],1024)<=0)
    {
       write_fd(errmem_fd, "read task status failed !!!\n", strlen("read task status failed !!!\n"));
       close(fd);
       return u32Ret;
    }
    close(fd);
  }
  else
  {
       write_fd(errmem_fd, "open task status failed !!!\n", strlen("open task status failed !!!\n"));
       return u32Ret;
  }

  if((pTemp = strstr(szBuffer,cPattern)) != 0)
  {
    /* find begin of 00000005 fffbfeff*/
    pTemp += strlen(cPattern)+1;
    if(bPrintMask)
    {
       char Buf[10];
       memcpy(Buf,pTemp+8,8);
       Buf[8] = '\0';
       errno = 0;    /* To distinguish success/failure after call */
       u32RetLow = strtoul(Buf,NULL,16);
       if ((errno == ERANGE && (u32RetLow == (tU32)LONG_MAX || u32RetLow == (tU32)LONG_MIN))
        || (errno != 0 && u32RetLow == 0)) 
       {
         write_fd(errmem_fd, "OSALEXC strtol failed !!!", strlen("OSALEXC strtol failed !!!"));
         u32RetLow = 0;
       }
    }

    /* seperate string 00000007 upper DWORD*/
    pTemp2 = pTemp;
    pTemp2 += 8;
    *pTemp2 = '\0';

    /* covert "7" to value */
    errno = 0;    /* To distinguish success/failure after call */
    u32Ret = strtoul(pTemp,NULL,16);
    if ((errno == ERANGE && (u32Ret == (tU32)LONG_MAX || u32Ret == (tU32)LONG_MIN))
     || (errno != 0 && u32Ret == 0)) 
    {
      write_fd(errmem_fd, "OSALEXC strtol failed !!!", strlen("OSALEXC strtol failed !!!"));
      u32Ret = 0;
    }
    if(bPrintMask)
    {
       vPrintBitMask(cPath,u32Ret);
       vPrintBitMask(cPath,u32RetLow);
    }
  }
  else
  {
    write_fd(errmem_fd, "OSALEXC Pattern not found !!!", strlen("OSALEXC Pattern not found !!!"));
  }
  if(bRetUpper)
  {
     return u32Ret;
  }
  else
  {
     return u32RetLow;
  }
}



int TriggerCallstacks(int Pid)
{
   int iRet = -1;
#ifdef VARIANT_S_FTR_ADIT_SUPPORTED 
   /* try to generate callstack via ADIT exception handler driver IF */
   struct exchnd_on_demand param;
   int rc;
   int exh_fd = open("/dev/exchnd", O_WRONLY);
   if (exh_fd <= 0) 
   {
        iRet = -1;
   }
   param.pid = Pid;
   param.use_def = 0; /* Deprecated - Unused */
   param.modules[0] = EHM_BACKTRACE_ALL_THREADS;

   strncpy(param.msg, "I'm an Callstack_ON_DEMAND trigger from OSAL", 256);
   rc = ioctl(exh_fd, IOCTL_EXCHND_ON_DEMAND, &param);
   if (rc >= 0) 
   {
      iRet = 0;
   }
   else
   {
      iRet = -2;
   }
   close(exh_fd);
#else
   ((void)Pid);
#endif
   return iRet;
}



/*
 * sigaction function that will be called by user to get backtrace 
 * for all threads SIG_BACKTRACE
 */
static void sigaction_backtrace_func(int n_signal, siginfo_t *siginfo, void *ptr)
{
    tU32 u32Val = 0;
    DIR *pDir;
    char szName[64];
    struct dirent *pDEntry;
    int fd,tid = 0;
    int pid = getpid();
    char szBuffer[MAX_LINE_SIZE];
    char buffer[MAX_LINE_SIZE];
    tU32 u32Temp;
    int sigact_result;
    int lock_return;
    struct sigaction act_old;
    tU32 u32Index = 0;

    /* make lint happy */
    (void)n_signal;
    (void)siginfo;
    (void)ptr;

    /* Block more signal to the process while we are in the handler */
    sigact_result = sigaction(n_signal, &act_ignore, &act_old);

#ifdef OSAL_ERRMEM_TIMESTAMP
    vWriteUtcTime(errmem_fd);
#endif

    /* SIGRTMIN is used , but it depends on the used Linux -> identify active 
       signal number for SIGRTMIN */
    u32Index = 32-(SIGRTMAX -SIGRTMIN);
    if(SIG_BACKTRACE != SIGRTMIN)
    {
       u32Index += (SIG_BACKTRACE - SIGRTMIN);
    }

    /* determine the process name */
    memset(szName,0,64);
    memset(szBuffer,0,128);
    snprintf(szName,64,"/proc/%d/cmdline",getpid());
    fd = open(szName,O_RDONLY);
    if(fd != -1)
    {
       if(read(fd,szName,64)<=0)
       {
       }
       close(fd);
    }
    else
    {
       snprintf(szName,64,"Cannot open path %s",szName);
       return;
    }
    /* store process info in errmem */
    snprintf(szBuffer,128,"%s SIG_BACKTRACE (%d) received for %s \n",
            SIGHDR_PREFIX,SIG_BACKTRACE,szName);
    write_fd(errmem_fd, szBuffer, strlen(szBuffer));

    /* lookup for all task of this process */
    snprintf(szName,64,"/proc/%d/task",getpid());
    pDir = opendir(szName);
    if(pDir)
    {
       /* check for all task */	
       while( NULL != (pDEntry = readdir( pDir )) )
       {
          if(pDEntry->d_name[0] != '.')
          {
             tid = atoi(pDEntry->d_name);
             if(tid > 0)
             {
               /* check if there are task waiting in non interruptable state, which will avoid 
                  complete callstack generation via OSAL signal handler */				
                snprintf(buffer,MAX_LINE_SIZE,"/proc/%d/task/%d",pid,tid);
                /* write OSAL_LI_GET_PS iformationto errmem and get task status */
                u32Temp = u32ScanPidTaskTidStat(buffer,TRUE,3);
                if(((char)u32Temp  == 'D')||((char)u32Temp  == 'T'))
                {
                   /* marker at least one task is in non interuptable state */	
                   u32Val = tid;
                }
             }
          }
       }
       rewinddir(pDir);
       /* check if OSAL internal Lock owner should be written to errmem */
       if(pOsalData->u32RecLockAccess)
       {
            PrintOsalLockStates();
       }
       if(u32Val)
       {
          /* at least one task in non interruptable state , we will generate top snapshot 
             because callstack generation is not possible */
          if(system("top -n 1 -b > /dev/errmem") == -1)
          {
             if(errno == EPERM)
             {
                vSendCallStackGenFromTerm(getpid(),MBX_GEN_TOP);
             }
          }
       }
       /* check is callstacks should be done via OSAL */
       if(pOsalData->bUseOsalCsGen)
       {
          if(u32Val)
          {
             /* task in non interruptable state if possible generate callstack via ADIT exception handler */
             u32Val = snprintf(szBuffer,128,"%s usage of ADIT exc hdr device because Task:%d \n",SIGHDR_PREFIX,(unsigned int)u32Val);
             write_fd(errmem_fd, szBuffer,u32Val);
             /* try to generate the callstack from outside of the signal handler */
             if(TriggerCallstacks(getpid()) != 0)
             {
                /* try to generate callstack via ADIT exception handler because driver access failed try to generate 
                   callstack via OSAL TERM Task because send signal failed due permissions and does not work for excluded 
                   processes from ADIT EXCHDR like mediaplayer */
                   vWritePrintfErrmem("OSAL EXCHDR IOCTL SIGUSR2 failed Error:%d CS GEN via OSAL Term Task \n",errno);
                   vSendCallStackGenFromTerm(getpid(),MBX_GEN_CS_USR2);
             }
          }
          else
          {
             /* special case for OSAL MQ overflow situation to get the blocked task as first entry -> GM Request*/
             if(pOsalData->u32OverflowPid == getpid())
             {
                snprintf(szBuffer,128,"%s/%d/status",szName,pOsalData->u32OverflowTid);
                /* check if task is not blocked by task */
                u32Val = u32CheckTskSignalConf(szBuffer,"SigBlk:",TRUE,FALSE);
                if(u32Val & u32SigPattern[u32Index])
                {
                   u32Val = snprintf(szBuffer,128,"%s Task %d has blocked signal mask 0x%x \n",SIGHDR_PREFIX,tid,(unsigned int)u32Val);
                   write_fd(errmem_fd, szBuffer,u32Val);
                }
                else
                {
                   /* check if signal will be processed by task */
                   u32Val = u32CheckTskSignalConf(szBuffer,"SigCgt:",TRUE,FALSE);
                   if(u32Val & u32SigPattern[u32Index])
                   {
                       /* take lock to wait for finished callstack before trigger the next one */
                      lock_return = exc_lock(&internal_lock);
                      if(lock_return == 0) 
                      {
                         /* call callstack generation function for my self */ 
                         if(tid == gettid())
                         {
                            sigaction_internal_func(SIG_INTERNAL, NULL,ptr);
                         }
                         else
                         {
                            /* send process internal signal to other task for callstack generation */
                            if(tkill(tid, SIG_INTERNAL) < 0)
                            {
                               vWritePrintfErrmem("%s tkill for task %d failed errno:%d \n",SIGHDR_PREFIX,tid,errno);
                            }
                         }
                      }
                   }
                }
                /* reset MQ overflow values */
                pOsalData->u32OverflowTid = 0;
                pOsalData->u32OverflowPid = 0;
             }

             /* start generating callstacks for all task of this process */
             while( NULL != (pDEntry = readdir( pDir )) )
             {
                if(pDEntry->d_name[0] != '.')
                {
                   tid = atoi(pDEntry->d_name);
                   if(tid > 0)
                   {
                      snprintf(szBuffer,128,"%s/%d/status",szName,tid);
                      /* check if task is not block by task */
                      u32Val = u32CheckTskSignalConf(szBuffer,"SigBlk:",TRUE,FALSE);
                      if(u32Val & u32SigPattern[u32Index])
//                    if(u32Val & 0x00000004) // for 34 with RT siganl starting with 32
                      {
                         u32Val = snprintf(szBuffer,128,"%s Task %d has blocked signal mask 0x%x \n",SIGHDR_PREFIX,tid,(unsigned int)u32Val);
                         write_fd(errmem_fd, szBuffer,u32Val);
                      }
                      else
                      {
                         /* check if signal will be processed by task */
                         u32Val = u32CheckTskSignalConf(szBuffer,"SigCgt:",TRUE,FALSE);
                         if(u32Val & u32SigPattern[u32Index])
                    //     if(u32Val & 0xc) SIG_BACKTRACE == SIGUSR2
                         {
                            /* take lock to wait for finished callstack before trigger the next one */
                            lock_return = exc_lock(&internal_lock);
                            if(lock_return == 0) 
                            {
                               if(tid == gettid())
                               {
                                  /* set marker to avoid leaving this handler before last callstack generation is finished */
                                  SigHdrActive = 1;
                                  /* call callstack generation function for my self */ 
                                  sigaction_internal_func(SIG_INTERNAL, NULL,ptr);
                               }
                               else
                               {
                                  /* set marker to avoid leaving this handler before last callstack generation is finished */
                                  SigHdrActive = 1;
                                   /* send process internal signal to other task for callstack generation */
                                  if(tkill(tid, SIG_INTERNAL) < 0)
                                  {
                                     vWritePrintfErrmem("%s tkill for task %d failed errno:%d \n",SIGHDR_PREFIX,tid,errno);
                                  }
                               }
                            }
                         }
                         else
                         {
                            u32Val = snprintf(szBuffer,128,"%s Task %d has not suitable signal mask 0x%x \n",SIGHDR_PREFIX,tid,(unsigned int)u32Val);
                            write_fd(errmem_fd, szBuffer,u32Val);
                         }
                      }
                   } 
                }
             }
             /* wait until last task has really finished to acitvate signal handling again*/
             while(SigHdrActive == 1)
             {
                sleep(1);
             }
         }
      }
      else
      {
         /* trigger ADIT exception handler for callstack */
         raise(SIGUSR2);
      }
   }
   else
   {
      u32Val = snprintf(szBuffer,128,"%s Cannot generate callstacks for %s errno:%d \n",SIGHDR_PREFIX,szName,errno);
      write_fd(errmem_fd, szBuffer,u32Val);
   }

    /* Enable the signal handler back */
    if(!sigact_result)
       sigaction(n_signal, &act_old, NULL);

    if(pDir)closedir(pDir);
}
/******************************************************************************************/
/*  SIGNAL Callstack end                                                                */
/******************************************************************************************/


/******************************************************************************************/
/*  SIGNAL sigonly start                                                                */
/******************************************************************************************/
static void sigaction_sigonly_output(int fd, siginfo_t *siginfo, const char *procfile)
{
    int size;
    char buffer[MAX_LINE_SIZE];
    char cBuffer[64];
    char* name = cBuffer;

    if(siginfo->si_signo != SIGCHLD)
    {
       write_fd(fd, SIGHDR_PREFIX, strlen(SIGHDR_PREFIX));

       if(!exc_get_thread_info(cBuffer,16))
       {
          strncpy(cBuffer,"Unknown",strlen("Unknown"));
       }
       else
       {
          cBuffer[15] = 0;
       }

       size = snprintf(buffer, MAX_LINE_SIZE-1,
                       "thread \"%s\" (tid %ld) of process \"%s\" (pid %d)\n"
                       "  got signal %d (%s) from process \"%s\" (pid %d)\n",
                       name,
                       gettid(),
                       process_name,
                       getpid(),
                       siginfo->si_signo,
                       sys_siglist[siginfo->si_signo],
                       procfile,
                       siginfo->si_pid);
       buffer[MAX_LINE_SIZE-1] = 0;
       write_fd(fd, buffer, size);
    }
}
/******************************************************************************************/
/*  SIGNAL sigonly end                                                                    */
/******************************************************************************************/


static void sigaction_sigonly_func(int n_signal, siginfo_t *siginfo, void *ptr);

#ifdef OSAL_EXIT_SIGNAL
/******************************************************************************************/
/*  SIGNAL OSAL Process exit start                                                        */
/******************************************************************************************/
tVoid OSAL_vProcessExit(tVoid);

static void sigaction_osalexit(int n_signal, siginfo_t *siginfo, void *ptr)
{
   ((void)n_signal);
   ((void)ptr);

   if(siginfo->si_pid != getpid())
   {
      if(siginfo->si_value.sival_int < (tS32)pOsalData->u32MaxNrProcElements)
      {
         if(hMQCbPrc[siginfo->si_value.sival_int])
         {
            OSAL_s32MessageQueueClose(hMQCbPrc[siginfo->si_value.sival_int]);
            hMQCbPrc[siginfo->si_value.sival_int] = 0;
         }
      }
   }
}

static const struct sigaction act_sigosalexit = {
    .sa_sigaction = sigaction_osalexit,
    .sa_flags = SA_ONSTACK | SA_SIGINFO | SA_RESTART,
};

/******************************************************************************************/
/*  SIGNAL OSAL Process exit end                                                          */
/******************************************************************************************/
#endif


/******************************************************************************************/
/*  SIGNAL TERM start                                                                     */
/******************************************************************************************/
static void sigaction_term_func(int n_signal, siginfo_t *siginfo, void *ptr)
{
   ((void)n_signal);
   ((void)ptr);
   ((void)siginfo);

   if(hLicenceToKillSem != OSAL_C_INVALID_HANDLE)
   {
      if(OSAL_s32SemaphorePost(hLicenceToKillSem) == OSAL_ERROR)
      {
          vWritePrintfErrmem("OSALEXC %s PID:%d Get SIGTERM from PID:%d -> call OSAL_s32SemaphorePost failed 0x%x \n",
                             commandline,getpid(),siginfo->si_pid, OSAL_u32ErrorCode());
      }
   }
   else
   {
      vWritePrintfErrmem("OSALEXC %s PID:%d Get SIGTERM from PID:%d -> call default handler for exit \n",commandline,getpid(),siginfo->si_pid);
      sigaction(n_signal, &EXC_OLD_HANDLER(n_signal), NULL);
      raise(n_signal);
   }
}
/******************************************************************************************/
/*  SIGNAL TERM end                                                                    */
/******************************************************************************************/

/*
 * definition used for installing the signal handlers
 */

static const struct sigaction act_exception = {
    .sa_sigaction = sigaction_func,
    .sa_flags = SA_ONSTACK | SA_SIGINFO | SA_RESTART,
};

static const struct sigaction act_backtrace = {
    .sa_sigaction = sigaction_backtrace_func,
    .sa_flags = SA_ONSTACK | SA_SIGINFO | SA_RESTART,
};

static const struct sigaction act_internal = {
    .sa_sigaction = sigaction_internal_func,
    .sa_flags = SA_ONSTACK | SA_SIGINFO | SA_RESTART,
};

static const struct sigaction act_sigonly = {
    .sa_sigaction = sigaction_sigonly_func,
    .sa_flags = SA_SIGINFO | SA_RESTART | SA_NODEFER,
};

static const struct sigaction act_sigchild = {
    .sa_sigaction = sigaction_child,
    .sa_flags = SA_SIGINFO | SA_RESTART | SA_NOCLDSTOP,
};

static const struct sigaction act_sigtriggerstack = {
    .sa_sigaction = sigaction_triggerstack_func,
    .sa_flags = SA_ONSTACK | SA_SIGINFO | SA_RESTART,
};

static const struct sigaction act_sigcheckstack = {
    .sa_sigaction = sigaction_checkstack_func,
    .sa_flags = SA_SIGINFO | SA_RESTART,  /* SA_ONSTACK  */
};

static const struct sigaction act_sigterm = {
    .sa_sigaction = sigaction_term_func,
    .sa_flags = SA_SIGINFO | SA_RESTART,  /* SA_ONSTACK  */
};

/* install/connect to FIFO for signal processing to to in normal task context with OSAL SIGHDR task*/
void InitSignalFifo(void) /*lint -e529 called from osalinit.cpp*/
{
   int sigfifo = -1;
   char Buffer[150];

   if(pOsalData->bFifoReady == FALSE)
   {
      /* make directory in volatile file system for OSAL informations */
      if(mkdir(VOLATILE_DIR"/OSAL",S_IFDIR | OSAL_ACCESS_RIGTHS) == -1)
      {
         if(errno != EEXIST)
         {
            vWritePrintfErrmem("%s PID:%d   -> mkdir(VOLATILE_DIR/OSAL failed \n",SIGHDR_PREFIX,getpid());
         }
      }
      else
      {
         if(chmod(VOLATILE_DIR"/OSAL", OSAL_VOLATILE_OSAL_PRC_ACCESS_RIGTHS) == -1)
         {
             vWritePrintfErrmem("%s PID:%d -> /run/OSAL chmod error %d \n",SIGHDR_PREFIX,getpid(),errno);
         }
      }
      sigfifo = mkfifo(VOLATILE_DIR"/OSAL/EXC_HDR_FIFO", OSAL_ACCESS_RIGTHS);
      if(sigfifo == -1)
      {
         if(errno != EEXIST)
         {
            snprintf(Buffer,150,"%s PID:%d mkfifo failed Error %d \n",SIGHDR_PREFIX,getpid(),errno);
            write_fd(errmem_fd, Buffer, strlen(Buffer));
            TraceString(Buffer);
         }
         else
         {
            pOsalData->bFifoReady = TRUE;
         }
      }
      else
      {
         pOsalData->bFifoReady = TRUE;
         //TraceString("%s PID:%d mkfifo installed",SIGHDR_PREFIX,getpid());
         if(s32OsalGroupId)
         {
      /*      if(fchown(sigfifo,(uid_t)-1,s32OsalGroupId) == -1)
            {
               if(errno == EPERM)
               {
                  TraceString("%s PID:%d open -> fchown error %d \n",SIGHDR_PREFIX,getpid(),errno);
               }
               else
               {
                  vWritePrintfErrmem("%s PID:%d open -> fchown error %d \n",SIGHDR_PREFIX,getpid(),errno);
               }
            }
            if(fchmod(sigfifo, OSAL_ACCESS_RIGTHS) == -1)
            {
               if(errno == EPERM)
               {
                  TraceString("%s PID:%d open -> fchmod error %d \n",SIGHDR_PREFIX,getpid(),errno);
               }
               else
               {
                  vWritePrintfErrmem("%s PID:%d open -> fchmod error %d \n",SIGHDR_PREFIX,getpid(),errno);
               }
            }*/
         }
      }
   }
#ifdef USE_CS_FILE
   if(errmem_cs_fd == -1)
   {
      errmem_cs_fd = open(ERRMEM_CSFILE, O_RDWR, OSAL_ACCESS_RIGTHS);
   }
   if(errmem_cs_fd == -1)
   {
      errmem_cs_fd = open(ERRMEM_CSFILE, O_RDWR | O_CREAT | O_APPEND , OSAL_ACCESS_RIGTHS);
      if(s32OsalGroupId)
      {
         if(fchown(errmem_cs_fd,(uid_t)-1,s32OsalGroupId) == -1)
         {
            vWritePrintfErrmem("InitSignalFifo ERRMEM_CSFILE -> fchown error %d \n",errno);
         }
         if(fchmod(errmem_cs_fd, OSAL_ACCESS_RIGTHS) == -1)
         {
            vWritePrintfErrmem("InitSignalFifo ERRMEM_CSFILE -> fchmod error %d \n",errno);
         }
      }
   }
#endif
}



static void sigaction_sigonly_func(int n_signal, siginfo_t *siginfo, void *ptr)
{
    char buffer[MAX_LINE_SIZE];
    int lock_return = 0;
    int size;
    (void)ptr;

    lock_return = exception_handler_lock();
    if (lock_return != 0)
    {
       size = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d sigaction_sigonly_func Print whithout lock \n",SIGHDR_PREFIX,getpid());
       write_fd(errmem_fd, buffer, size);
    }
    exc_get_process_name(siginfo->si_pid, buffer, sizeof(buffer));
    #ifdef SIGONLY_TO_ERRMEM
    if (errmem_fd != -1)
    {
        sigaction_sigonly_output(errmem_fd, siginfo, buffer);
    }
    #endif
    sigaction_sigonly_output(FD_STDOUT, siginfo, buffer);
    if (lock_return == 0)
    {
       exception_handler_unlock();
    }
    /* block default handler for SIGUSR2 needed for callstack generation with ADIT exception handler
       otherwise write signal to errmem and call default handler 
       Further we block all signals which lead to process termination and are not handled as Bosch 
       exceptions */
    switch(n_signal)
    {
       case SIGUSR2:
       case SIGUSR1:
 //      case SIGQUIT:
       case SIGALRM:
 //      case SIGINT:
 //      case SIGPIPE:
           break;
       default:
            sigaction(n_signal, &EXC_OLD_HANDLER(n_signal), NULL);
            raise(n_signal);
            /* enable sigaction_sigonly_func again */
            sigaction(n_signal, &act_sigonly, NULL);
           break;
   }
}


/*
 * entry to distributed exception handler
 */
#ifdef EXC_ARM
static int trigger_dexcep(void)
{
/*no include file for path as this should be the only location
for triggering dexcep from userspace...*/
#define DEXCEP_TRIGGER_PROC_EXCEPTION "1"
#define DEXCEP_PATH "/sys/devices/platform/dexcep/trigger_exception"
    int fd;
    fd = open(DEXCEP_PATH, O_WRONLY);
    if(fd < 0)
        return -1;        
    if(write(fd, DEXCEP_TRIGGER_PROC_EXCEPTION, 1) < 0)
        return -1;
    return 0;
}
#endif

/*
 * reboot function
 */

 
void vRebootMarker(void)
{
   struct tm mytm;
   int i = 0;
   char Buffer[100] = {0};
   time_t secSinceEpoch = time(NULL);

   Buffer[0] = 0;
   if( NULL != gmtime_r(&secSinceEpoch, &mytm))
   {
      i = snprintf(&Buffer[1],99,"OSAL Reboot UTC %d.%d.%d  %d:%d:%d ElapsedTime:%d\n",
	               mytm.tm_mday,mytm.tm_mon + 1,mytm.tm_year,mytm.tm_hour,mytm.tm_min,mytm.tm_sec,OSAL_ClockGetElapsedTime());
   }
   else
   {
      i = snprintf(&Buffer[1],99,"OSALElapsed Time %d ElapsedTime:%d\n",(int)secSinceEpoch,OSAL_ClockGetElapsedTime());
   }
   write_fd(errmem_fd, Buffer, i);
   Buffer[0] = 0xf1;
   TraceString(Buffer);
}
 
 
void eh_reboot(void)
{
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
        sync();   /* be sure that everything is on disk */
#endif

    vRebootMarker();

    /* check if eh_reboot is called by unexpected code -> GM Request */
    if(pOsalData->bNoRebootCallstack == FALSE)
    {
void OSAL_generate_callstack_secure(tBool bTrace);
         OSAL_generate_callstack_secure(TRUE);
    }
    if(should_be_rebooted())
    {
#ifdef EXC_ARM
#ifdef OSAL_GEN3
        fprintf(stderr, "rebooting (to prevent: touch %s\n", EXCEPTION_HANDLER_REBOOT_FILE);
        sleep(1);
        if(trigger_dexcep() < 0)
        {
           reboot(RB_AUTOBOOT);
           pOsalData->s32ResetRequestPid = getpid();
           raise(SIGPWR);
        }
#else
        if(bExceptionActive)
        {
           /* reboot from signal handler , OSAL_vProcessExit() should not be used */
           _exit(EX_SOFTWARE);
        }
        else
        {
           OSAL_vSetProcessExitCode(EX_SOFTWARE);
           /* reboot from a normal task context */
           OSAL_vProcessExit();
        }
#endif
#else
        /* on X86 we cannot do a complete system reset*/
        if(bExceptionActive)
        {
           /* reboot from signal handler , OSAL_vProcessExit() should not be used */
           _exit(EX_SOFTWARE);
        }
        else
        {
          /* reboot from a normal task context */
          OSAL_vSetProcessExitCode(EX_SOFTWARE);
          OSAL_vProcessExit();
        }
#endif
    } 
    /* reset marker only for disabled reset */
    pOsalData->bNoRebootCallstack = FALSE;
}

/*
 * subfunction for "backtrace_to_errmem"
 */

static void backtrace_to_fd(void **pointers, int used_pointers, int fd)
{
    char buffer[MAX_LINE_SIZE];
    int size;

    size = snprintf(buffer, MAX_LINE_SIZE,"%s ------ command line : \"%s\"\n",SIGHDR_PREFIX, commandline);
    write_fd(fd, buffer, size);
    size = snprintf(buffer, MAX_LINE_SIZE,"%s ------ backtrace :\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);

    exc_print_callstack(pointers,used_pointers,FALSE,fd);
    exc_print_map(buffer, fd);

    size = snprintf(buffer, MAX_LINE_SIZE,"%s ------ backtrace end:\n",SIGHDR_PREFIX);
    write_fd(fd, buffer, size);
}

/*
 * "backtrace_to_errmem" write the current callstack to error memory
 */


void backtrace_to_errmem(void)
{
    void *pointers[EXC_STACK_DEEPNESS];
    int used_pointers;
#ifdef LIBUNWIND_USED
    if(s32UnwindInstalled == 2)
    {
       used_pointers = u32Unwind(pointers, EXC_STACK_DEEPNESS);
    }
    else
#endif
    {
       used_pointers = backtrace(pointers, EXC_STACK_DEEPNESS);	
    }
    if (errmem_fd != -1)
        backtrace_to_fd(pointers, used_pointers, errmem_fd);
    backtrace_to_fd(pointers, used_pointers, FD_STDOUT);
}

/*
 * This function is called if an exit occurs to signal an
 * error (should not happen !)
 */
static void exit_handler(void) /*lint !e529 */ /* called from threadexit */
{
    char buffer[MAX_LINE_SIZE];
    static char *abnormal_exit_message = 
        "OSALEXC ******* process \"%s\" (PID %d) terminated via exit() call *********\n";
    char *exit_text;
    int length_of_text;
    
#ifdef OSAL_ERRMEM_TIMESTAMP
    vWriteUtcTime(errmem_fd);
#endif

    length_of_text = strlen(abnormal_exit_message) + sizeof(commandline) + 1;
    exit_text = (char *)alloca(length_of_text);
    snprintf(exit_text, length_of_text, abnormal_exit_message, commandline, getpid());
    fprintf(stderr, "%s", exit_text);
    if (errmem_fd != -1)
    {
        exc_print_thread(buffer, MAX_LINE_SIZE, errmem_fd, 0);
    }

    backtrace_to_errmem();
    if(errmem_fd != -1) {
        write_fd(errmem_fd, exit_text, strlen(exit_text));
    }
    length_of_text = snprintf(buffer,MAX_LINE_SIZE,"%s PID:%d Reboot via OSAL caused by exit handler\n",
                    SIGHDR_PREFIX,getpid());
    write_fd(errmem_fd,buffer, length_of_text);
    /* set maker for valid eh_reboot call */
    pOsalData->bNoRebootCallstack = TRUE;
    eh_reboot();
}


/*
 * initialisation for the exception handler
 * this function initialize the alternative stack
 * and installs the generic exception handler function
 * for each signal that is enabled in array
 * "sig_activated". Also the commandline is copied here
 * to print it out in case of an exception.
 * Additionally a trace is added for the exit handler.
 */

void init_exception_handler(const char *name) /*lint !e529 */ /* called in wrapperentry function*/
{
    int i;
    int fd;
    FILE *f;
    stack_t new_stack;
    struct sigaction oldact;
    int lock_return;
    int bExcludeExit = FALSE;
    static char *cannot_add_message = "OSALEXC cannot add exit handler\n";
    ((void)name);

    /* create/connect to lock used in the signal handler */
//    init_exception_handler_lock();
    ptr_exc_handler_lock = &pOsalData->ExhLockArea;

    lock_return = exception_handler_lock();
    if (lock_return != 0)
    {
        exc_print_wrong_lock(lock_return);
        return;        
    }

    /* check for main thread to execute things needed once for process */
    if (!exit_handler_set)
    {
       /* install alternate stack for signal handler processing to avoid using thread stack usage */
       new_stack.ss_sp = malloc(STKSZ);
       new_stack.ss_flags = 0;
       new_stack.ss_size = STKSZ;
       sigaltstack(&new_stack, NULL);

       /* get handle for errmem access */
       if (errmem_fd < 0)
       {
#ifdef VARIANT_S_FTR_ENABLE_ERRMEM_WRITE_TO_FILE
          fd = open(ERRMEM_FILE, O_RDWR | O_CREAT | O_APPEND,OSAL_ACCESS_RIGTHS);
          if(fd == -1)
          {
               if(EEXIST == errno)
               {
                  fd = open(ERRMEM_FILE, O_RDWR,OSAL_ACCESS_RIGTHS);
               }
          }
          else
          {
            if(s32OsalGroupId)
            {
                if(fchown(fd,(uid_t)-1,s32OsalGroupId) == -1)
                {
                    vWritePrintfErrmem("init_exception_handler open -> fchown error %d \n",errno);
                }
                if(fchmod(fd, OSAL_ACCESS_RIGTHS) == -1)
                {
                    vWritePrintfErrmem("init_exception_handler open -> fchmod error %d \n",errno);
                }
             } 
          }
#else
          if((fd = open(ERRMEM_FILE, O_WRONLY, OSAL_ACCESS_RIGTHS)) == -1)
          {
              TraceString("open(ERRMEM_FILE, O_WRONLY, OSAL_ACCESS_RIGTHS) for PID:%d failed errno:%d !!!!!!!!!",getpid(),errno);
          }
#endif
          errmem_fd = fd;
       }
       /* prepare check for accessing foreign memory -> null device*/
       init_check_access();
       /* install handler for critical signals defined in struct SigActivation sig_activated[SIGMAX] */
       for (i = 0; i < SIGMAX; ++i) {
           if (EXC_SIGNAL_IS_ENABLED(i)) {
               sigaction(i, &act_exception, &EXC_OLD_HANDLER(i));
           }
       }
       /* check if SIGCHILD handling for analysis purposes is required */
       if(pOsalData->u32CatchSigchild)
       {
          if(sigaction(SIGCHLD, &act_sigchild,NULL) == -1)
          {
              vWritePrintfErrmem("OSALEXC Pid:%d Cannot install SIGCHLD handler errno:%d \n",getpid(),errno);
          }
       }
       /* install handler for generation of callstacks for a specified process via SIGRTMIN */
       if(sigaction(SIG_BACKTRACE, &act_backtrace, NULL) == -1)
       {
              vWritePrintfErrmem("OSALEXC Pid:%d Cannot install SIG_BACKTRACE handler errno:%d \n",getpid(),errno);
       }
       /* install separate handler for SIGTERM to prepare process cleanup if required */
       if(sigaction(SIGTERM, &act_sigterm, NULL) == -1)
       {
           vWritePrintfErrmem("OSALEXC Pid:%d Cannot install SIGTERM handler errno:%d \n",getpid(),errno);
       }
       /* install handler for stack measurement not used in prodduct */
       if(sigaction(SIG_TRIGGERSTACK, &act_sigtriggerstack, NULL) == -1)
       {
           vWritePrintfErrmem("OSALEXC Pid:%d Cannot install SIG_TRIGGERSTACK handler errno:%d \n",getpid(),errno);
       }
#ifdef OSAL_EXIT_SIGNAL
       if(sigaction(SIG_OSALEXIT, &act_sigosalexit, NULL) == -1)
       {
           vWritePrintfErrmem("OSALEXC Pid:%d Cannot install SIG_OSALEXIT handler errno:%d \n",getpid(),errno);
       } 
#endif	   
       /* get command line  to write process name and commandline to process local variable */
       f = fopen("/proc/self/cmdline", "r");
       if (f) {
         if (fread(commandline, 1, sizeof(commandline)-1, f) > 0)
         commandline[sizeof(commandline)-1] = 0;
       else
         strcpy(commandline, "<no name>");
         fclose(f);
       } else {
         strcpy(commandline, "<no name>");
       }

       int bufsiz = strlen(commandline);
       for (i=bufsiz-1;i >= 0;i--)
       {
          if(commandline[i] == '/')
          {
             strncpy(process_name,&commandline[i+1],strlen(&commandline[i+1]));
             break;
          }
       }
//       exc_get_process_name(getpid(), process_name, sizeof(process_name)-1);

       /* set sigonly signal handler for all unset signals to log all u expected signals to errmem*/
       for (i = 1; i < SIGMAX; ++i)
       {
         if ((i == SIGKILL) || (i == SIGSTOP))
            continue;
         if (sigaction(i, NULL, &oldact) == 0)
         {
             if (!oldact.sa_sigaction)
             {
                sigaction(i, &act_sigonly, &EXC_OLD_HANDLER(i));
             }
         }
       }
       /* set marker for the next thread to skip some operations */
       exit_handler_set = 1;
       
       /* OSAL exit handling , different handlers are allowed 
          - detect exit call and provide callstack of the caller and call eh_reboot -> exit call not allowed
          - call OSAL_vProcessExit to cleanup OSAL resources at exit  -> exit call allowed */
		  
       /* check if exit call is allowed for this process default for UBUNTU environment */	  
       if(pOsalData->u32AllowExit)
       {
          /* install OSAL_vProcessExit as exit handler */ 
          if(atexit(OSAL_vProcessExit) != 0)
          {
             if(errmem_fd >= 0)
                 write_fd(errmem_fd, cannot_add_message,strlen(cannot_add_message)+1);
 	      }
	   }
	   else
	   {
              /* exit call is forbidden  check for up to 10 processes which are excluded via registry entry */
              for(i=0;i<MAX_BOSCH_CONF_APP;i++)
              {
                 if(pOsalData->rExitApp[i].szAppName[0])
                 {
                    if(strstr(commandline,(const char*)pOsalData->rExitApp[i].szAppName))
                    {
                       bExcludeExit = TRUE;
                       /* entry found leave while loop */
                       break;
                    }
                 }
                 else
                 {
                    /* no further entry leave while loop */
                    break;
                 }
             }
             /* if process is not excluded install default exit handler for Bosch products -> exit forbidden */
             if(bExcludeExit == FALSE)
             {
                if(atexit(exit_handler) != 0)
                {
                   fprintf(stderr, "%s", cannot_add_message);
                   if(errmem_fd >= 0)
                   write_fd(errmem_fd, cannot_add_message,strlen(cannot_add_message)+1);
                   /* set marker for valid eh_reboot call */
                   pOsalData->bNoRebootCallstack = TRUE;              
                   eh_reboot();
                   exit(-1);
                }
             }
          }
       exception_handler_unlock();
    } else {
       exception_handler_unlock();
    }

    /* istall handler needed for each thread for callstack generation or stack measurement */
    sigaction(SIG_INTERNAL,  &act_internal, NULL);
    sigaction(SIG_INTERNAL2, &act_sigcheckstack, NULL);

    /* For the situation that threads signal mask will be changed (for exmaple audio manager) for some threads in conflict with OSAL
       will install handlers to indicate this with error memory entry when wrong task receive this signal and to avoid calling 
       default signal handler which causes process termination */ 
    sigaction(pOsalData->u32TimSignal, &act_protocol, NULL); 
    sigaction(pOsalData->u32TimSignal-1, &act_protocol, NULL);
    sigaction(pOsalData->u32TimSignal-2, &act_protocol, NULL);
    sigaction(OSAL_CB_HDR_SIGNAL, &act_ignore, NULL);
#ifdef SIGNAL_EV_HDR
    sigaction(OSAL_SIGNAL_EVENT_AND, &act_ignore, NULL);
    sigaction(OSAL_SIGNAL_EVENT_OR, &act_ignore, NULL);
    sigaction(OSAL_SIGNAL_EVENT_XOR, &act_ignore, NULL);
    sigaction(OSAL_SIGNAL_EVENT_REPLACE, &act_ignore, NULL);
    sigaction(OSAL_SIGNAL_EVENT_PRC_AND , &act_ignore, NULL);
    sigaction(OSAL_SIGNAL_EVENT_PRC_OR, &act_ignore, NULL);
    sigaction(OSAL_SIGNAL_EVENT_PRC_XOR, &act_ignore, NULL);
    sigaction(OSAL_SIGNAL_EVENT_PRC_REPLACE, &act_ignore, NULL);
#endif
}

void exit_exception_handler(void)
{
    /*remove alternate stack from the process*/
    stack_t stack;
    if (sigaltstack(NULL, &stack) < 0)
    {
        perror("cannot get information about old stack");
        return;
    }
    stack.ss_flags = SS_DISABLE;
    if (sigaltstack(&stack, NULL) < 0)
    {
        perror("cannot remove old stack");
        return;
    }
    if (stack.ss_sp)
    {
        free(stack.ss_sp);
    }
#ifdef LIBUNWIND_USED
    if(s32UnwindInstalled == 2)
    {
       dlclose(pUnwModuleHandle);
    }
#endif
}

void vCheckSignalMaskPid(tU32 u32Sig,int PID,tCString szMaskType, int idx)
{
   char prcdirpath[64];
   char taskdirpath[64];
   char szBuffer[128];
   int fd,tid;
   DIR *pDirTsk;
//   struct dirent *pDEntryPrc;
   struct dirent *pDEntryTsk;
   tU32 u32Val = 0;
   int bRetUpper = FALSE;

   snprintf(prcdirpath,64,"/proc/%d",PID);
   fd = open(prcdirpath,O_RDONLY);
   if(fd != -1)
   {
      snprintf(taskdirpath,64,"%s/task",prcdirpath);
      pDirTsk = opendir(taskdirpath);
      if(pDirTsk)
      {
         while( NULL != (pDEntryTsk = readdir(pDirTsk)) )
         {
            if(pDEntryTsk->d_name[0] != '.')
            {
               tid = atoi(pDEntryTsk->d_name);
               if(tid > 0)
               {
                  snprintf(szBuffer,128,"%s/%d/status",taskdirpath,tid);
                  u32Val = u32CheckTskSignalConf(szBuffer,(char*)szMaskType,bRetUpper,TRUE);
                  if(u32Val & u32SigPattern[idx])
                  {
                     TraceString("%s %s:0x%x has Signal %d",szBuffer,szMaskType, u32Val,u32Sig);
                  }
                  else
                  {
                     if(u32Sig == pOsalData->u32TimSignal)
                     {
                        snprintf(szBuffer,128,"%s/%d/stat",taskdirpath,tid);
                        int fdesc = open(szBuffer,O_RDONLY);
                        char cStatBuf[64];
                        if(fdesc != -1)
                        {
                           if(read(fdesc,&cStatBuf[0],64)!=-1)
                           {
                              if(strstr(cStatBuf,"(Timer-"))
                              {
                                 if(!strcmp(szMaskType,"SigBlk:"))
                                 {
                                    TraceString("%s OSAL Timer Task %s 0x%x not has Signal %d -> expected",szBuffer,szMaskType,u32Val,u32Sig);
                                 }
                              }
                              else
                              {
                                 TraceString("%s %s 0x%x not has Signal %d not expected!!!!!!!!!!!!!!!!!",szBuffer,szMaskType,u32Val,u32Sig);
                              }
                           }
                           close(fdesc);
                        }
                     }
                     else
                     {
                        TraceString("%s %s 0x%x not has Signal %d !!!!!!!!!!!!!!!!!",szBuffer,szMaskType,u32Val,u32Sig);
                     }
                  }
               }
            }
         }
         closedir(pDirTsk);
      }
      close(fd);
   }
}

void vCheckSignalMask(tU32 u32Sig,tCString szMaskType,tBool bPrintMask)
{
   char prcdirpath[64];
   char taskdirpath[64];
   char szBuffer[128];
   int fd,tid;
   DIR *pDirPrc,*pDirTsk;
   struct dirent *pDEntryPrc;
   struct dirent *pDEntryTsk;
   tU32 u32Val = 0;
   int idx; 
   int bRetUpper = FALSE;

   if(u32Sig > 32)
   {
      idx = u32Sig-1-32;
      bRetUpper = TRUE;
   }
   else
   {
     idx = u32Sig-1;
   }
   pDirPrc = opendir("/run/OSAL/Processes");
   while( NULL != (pDEntryPrc = readdir(pDirPrc)))
   {
      if(pDEntryPrc->d_name[0] != '.')
      {
         snprintf(prcdirpath,64,"/proc/%s",pDEntryPrc->d_name);
         fd = open(prcdirpath,O_RDONLY);
         if(fd != -1)
         {
              snprintf(taskdirpath,64,"%s/task",prcdirpath);
              pDirTsk = opendir(taskdirpath);
              if(pDirTsk)
              {
                  while( NULL != (pDEntryTsk = readdir(pDirTsk)) )
                  {
                     if(pDEntryTsk->d_name[0] != '.')
                     {
                        tid = atoi(pDEntryTsk->d_name);
                        if(tid > 0)
                        {
                           snprintf(szBuffer,128,"%s/%d/status",taskdirpath,tid);
                           u32Val = u32CheckTskSignalConf(szBuffer,(char*)szMaskType,bRetUpper,bPrintMask);
                           if(u32Val & u32SigPattern[idx])
                           {
                               TraceString("%s %s:0x%x has Signal %d",szBuffer,szMaskType, u32Val,u32Sig);
                           }
                           else
                           {
                               if(u32Sig == pOsalData->u32TimSignal)
                               {
                                  snprintf(szBuffer,128,"%s/%d/stat",taskdirpath,tid);
                                  int fdesc = open(szBuffer,O_RDONLY);
                                  char cStatBuf[64];
                                  if(fdesc != -1)
                                  {
                                     if(read(fdesc,&cStatBuf[0],64)!=-1)
                                     {
                                         if(strstr(cStatBuf,"(Timer-"))
                                         {
                                            if(!strcmp(szMaskType,"SigBlk:"))
                                            {
                                                TraceString("%s OSAL Timer Task %s 0x%x not has Signal %d -> expected",szBuffer,szMaskType,u32Val,u32Sig);
                                            }
                                         }
                                         else
                                         {
                                            TraceString("%s %s 0x%x not has Signal %d not expected!!!!!!!!!!!!!!!!!",szBuffer,szMaskType,u32Val,u32Sig);
                                         }
                                     }
                                     close(fdesc);
                                  }
                               }
                               else
                               {
                                  TraceString("%s %s 0x%x not has Signal %d !!!!!!!!!!!!!!!!!",szBuffer,szMaskType,u32Val,u32Sig);
                               }
                           }
                        }
                     }
                  }
                  closedir(pDirTsk);
                  OSAL_s32ThreadWait(1000);
              }
              close(fd);
         }
      }
   }
   closedir(pDirPrc);
}

