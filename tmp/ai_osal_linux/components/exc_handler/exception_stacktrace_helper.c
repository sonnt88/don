#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <dlfcn.h>
#define __USE_GNU
#include <ucontext.h>
#include <asm/param.h>
#include <sys/mman.h>
#include <sys/user.h>

#include "exception_handler.h"

#ifdef __i386
  #define EXC_X86
#endif

/* c wrapper for threadsave random function */

int exc_rand_r(unsigned int *ptr)
{
	return rand_r(ptr);
}


/* check code area adress: true -> access ok */

int exc_check_access_code(unsigned long addr, ExcCheckAccessState *exc_check_access_state)
{
	unsigned long paged_addr;
	
	paged_addr = ALIGN_ULONG(addr);
	if (paged_addr != exc_check_access_state->last_paged_addr) {
		exc_check_access_state->last_result = exc_check_access((void *)paged_addr);
		exc_check_access_state->last_paged_addr = paged_addr;
	}
	return exc_check_access_state->last_result;
}

/* check stack area adress: true -> access ok */

int exc_check_access_stack(unsigned long addr, ExcCheckAccessState *exc_check_access_state)
{
	unsigned long paged_addr;
	
	paged_addr = ALIGN_ULONG(addr);
	if (paged_addr != exc_check_access_state->last_paged_addr) {
		exc_check_access_state->last_result = exc_check_access((void *)paged_addr);
		exc_check_access_state->last_paged_addr = paged_addr;
	}
	return (exc_check_access_state->last_result);
}


#ifndef EXC_X86
/* only for ARM */

#define EXECPTION_ITERATION_SIZE 65536

/* for debug traces in assembler function */

void debug_exeption_handler_line(unsigned long address, unsigned long data)
{
	printf("traceing %08lx (=%08lx)\n", address, data);
	fflush(stdout);
}

/* function for calculation of the stacktrace in an exceptions */

extern int arm_backtrace_asm(void **buffer, int size,
				unsigned long *backtrace_register_values,
				int iteration_count);

int arm_backtrace_exception(void **buffer, int size, ucontext_t *uc_link)
{
	unsigned long backtrace_register_values[16];
#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
    return 0;
#else
	
	backtrace_register_values[0] = uc_link->uc_mcontext.arm_r0;
	backtrace_register_values[1] = uc_link->uc_mcontext.arm_r1;
	backtrace_register_values[2] = uc_link->uc_mcontext.arm_r2;
	backtrace_register_values[3] = uc_link->uc_mcontext.arm_r3;
	backtrace_register_values[4] = uc_link->uc_mcontext.arm_r4;
	backtrace_register_values[5] = uc_link->uc_mcontext.arm_r5;
	backtrace_register_values[6] = uc_link->uc_mcontext.arm_r6;
	backtrace_register_values[7] = uc_link->uc_mcontext.arm_r7;
	backtrace_register_values[8] = uc_link->uc_mcontext.arm_r8;
	backtrace_register_values[9] = uc_link->uc_mcontext.arm_r9;
	backtrace_register_values[10] = uc_link->uc_mcontext.arm_r10;
	backtrace_register_values[11] = uc_link->uc_mcontext.arm_fp;
	backtrace_register_values[12] = uc_link->uc_mcontext.arm_ip;
	backtrace_register_values[13] = uc_link->uc_mcontext.arm_sp;
	backtrace_register_values[14] = uc_link->uc_mcontext.arm_lr;
	backtrace_register_values[15] = uc_link->uc_mcontext.arm_pc;
	return arm_backtrace_asm(buffer, size, backtrace_register_values,
		EXECPTION_ITERATION_SIZE);
 #endif
}


#endif

