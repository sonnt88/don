/*
 * exception_handler.h
 *
 * Definition for "exception handler"
 * helper for printing ssome more information
 * of an exception to stdout and to "/dev/errmem"
 * (if "/dev/errmem" is is accessible)
 * the will print more informations for
 * aborted processes
 *
 * now only for x86 and arm architecture
 *
 * Copyright (C) TMS GmbH Hildesheim 2010
 * Written by Ulrich Schulz (ulrich.schulz@tms-gmbh.de)
 *
 */

#ifndef __exception_handler_h_4590898454854
#define __exception_handler_h_4590898454854

#ifdef __cplusplus
extern "C" {
#endif


#define ALIGN_ULONG(x) ((unsigned long)(x)&~(EXEC_PAGESIZE-1))
#define ALIGN_VOIDP(x) ((void *)(ALIGN_ULONG(x)))

/* enable this to trace all signals to errormem */
/* #define SIGONLY_TO_ERRMEM */

/* struct for caching access informations */

typedef struct {
	unsigned long last_result;
	unsigned long last_paged_addr;
} ExcCheckAccessState;

/* init function for exception handler */

void init_exception_handler(const char *name);

/* exit function for exception handler */

void exit_exception_handler(void);

/* locking the exception output, returns 0 if all is ok.
   Else returns the wrong value in lock value or 0xdeaddead for timeout */

int exception_handler_lock(void);

/* unlocking the exception output */

void exception_handler_unlock(void);

/* call this to generate a backtrace to errmem */

void backtrace_to_errmem(void);

void eh_reboot(void);

/* 
 * call this if you want get a callstack of the current program flow
 * 
 * Arguments:
 * @buffer 		: memory to hold the callstack. This has to have a size
 *			  of "size" * sizeof(void *)
 * @size   		: size of the callstack (counted in sizeof(void *)
 * @iteration_size	: number of iterations done until abort
 *			  a good choice is 16384, smaller number generate
 *			  the callstack faster, but less accurate
 * @dummy		: set to zero, needed to get an extra register in
 *			  assembler function
 */

int arm_backtrace(void **buffer, int size, int iteration_size, int dummy);

/* 
 * this is called from the exception handler
 *
 * Arguments:
 *
 * @buffer 		: memory to hold the callstack. This has to have a size
 *			  of "size" * sizeof(void *)
 * @size   		: size of the callstack (counted in sizeof(void *)
 * @uc_link		: content filled from the exception handler
 */

int arm_backtrace_exception(void **buffer, int size, ucontext_t *uc_link);

/* check code area adress: true -> access ok */

int exc_check_access_code(unsigned long addr,
			ExcCheckAccessState *exc_check_access_state);

/* check code area adress: true -> access ok */

int exc_check_access_stack(unsigned long addr,
		ExcCheckAccessState *exc_check_access_state);

/* check adress: true -> access ok */

int exc_check_access(const void *addr);


#ifdef __cplusplus
}

#endif

#endif
