/*
 * exception_handler_internal.h
 *
 * Definition for "exception handler"
 * helper for printing ssome more information
 * of an exception to stdout and to "/dev/errmem"
 * (if "/dev/errmem" is is accessible)
 * the will print more informations for
 * aborted processes
 *
 * now only for x64 and arm architecture
 *
 * Copyright (C) TMS GmbH Hildesheim 2010
 * Written by Ulrich Schulz (ulrich.schulz@tms-gmbh.de)
 *
 */

#ifndef __exception_handler_internal_h_4590898454854
#define __exception_handler_internal_h_4590898454854

#ifdef __cplusplus
extern "C" {
#endif

    /*********************************************************************/
    /* TYPEDEFS                                                          */
    /*********************************************************************/

#ifdef __cplusplus
}

#endif

#endif
