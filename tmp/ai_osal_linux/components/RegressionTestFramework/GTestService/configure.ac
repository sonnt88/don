dnl Process this file with autoconf to produce a configure script.

AC_PREREQ(2.59)
AC_INIT(GTestService, 1.0)

AC_CANONICAL_SYSTEM
AM_INIT_AUTOMAKE()
AC_CONFIG_MACRO_DIR([m4])

cxxflags_save="$CXXFLAGS"
AC_PROG_CXX
CXXFLAGS="$cxxflags_save"

dnl Initialize Libtool
LT_INIT

# Get Protobuf library and include locations
AC_ARG_WITH([protobuf-include-path],
  [AS_HELP_STRING([--with-protobuf-include-path=<path>],
    [location of the Google Protocol Buffers headers])],
  [PROTOBUF_CPPFLAGS="-I$withval"],
  [PROTOBUF_CPPFLAGS='-I/usr/include'])
AC_SUBST([PROTOBUF_CPPFLAGS])

AC_ARG_WITH([protobuf-lib-path],
  [AS_HELP_STRING([--with-protobuf-lib-path=<path>], [location of the Google Protocol Buffers libraries)])],
  [PROTOBUF_LDFLAGS="-L$withval -lprotobuf"],
  [PROTOBUF_LDFLAGS='-lprotobuf'])
AC_SUBST([PROTOBUF_LDFLAGS])

# Get PersiGTest library and include locations
AC_ARG_WITH([persigtest-include-path],
  [AS_HELP_STRING([--with-persigtest-include-path=<path>],
    [location of the PersiGTest headers])],
  [PERSIGTEST_CPPFLAGS="-I$withval/GoogleMock/include -I$withval/GoogleTest/include -I$withval/GoogleTest -I$withval/PersiGTest/include"],
  [PERSIGTEST_CPPFLAGS='-I/usr/include/GoogleMock/include -I/usr/include/GoogleTest/include -I/usr/include/GoogleTest -I/usr/include/PersiGTest/include'])
AC_SUBST([PERSIGTEST_CPPFLAGS])

AC_ARG_WITH([persigtest-lib-path],
  [AS_HELP_STRING([--with-persigtest-lib-path=<path>], [location of the PersiGTest libraries])],
  [PERSIGTEST_LDFLAGS="-L$withval -lPersiGTest"],
  [PERSIGTEST_LDFLAGS='-lPersiGTest'])
AC_SUBST([PERSIGTEST_LDFLAGS])

AC_ARG_ENABLE(debug,
AS_HELP_STRING([--enable-debug],
               [enable debugging, default: no]),
[case "${enableval}" in
             yes) debug=true ;;
             no)  debug=false ;;
             *)   AC_MSG_ERROR([bad value ${enableval} for --enable-debug]) ;;
esac],
[debug=false])

AM_CONDITIONAL(DEBUG, test x"$debug" = x"true")

AC_ARG_ENABLE(coverage,
AS_HELP_STRING([--enable-coverage],
               [enable coverage analysis, default: no]),
[case "${enableval}" in
             yes) coverage=true ;;
             no)  coverage=false ;;
             *)   AC_MSG_ERROR([bad value ${enableval} for --enable-coverage]) ;;
esac],
[coverage=false])

AM_CONDITIONAL(COVERAGE, test x"$coverage" = x"true")

AC_CONFIG_FILES(Makefile src/Makefile)
AC_OUTPUT
