#!/bin/sh
make clean
rm -Rf build aclocal.m4 autom4te.cache/ compile config.guess config.log config.status configure config.sub depcomp INSTALL install-sh libtool ltmain.sh m4/ Makefile Makefile.in missing src/Makefile src/Makefile.in
aclocal
libtoolize
autoconf
automake --add-missing
./configure --prefix=$(pwd)/build \
--with-protobuf-include-path=$(pwd)/../libs/protobuf-2.4.1/build/include \
--with-protobuf-lib-path=$(pwd)/../libs/protobuf-2.4.1/build/lib \
--with-persigtest-include-path=$(pwd)/../libs/PersiGTest/build/include \
--with-persigtest-lib-path=$(pwd)/../libs/PersiGTest/build/lib --enable-debug

