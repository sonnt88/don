/* 
 * File:   GTestTerminationHandler.h
 * Author: oto4hi
 *
 * Created on May 8, 2012, 10:07 AM
 */

#ifndef GTESTTERMINATIONHANDLER_H
#define	GTESTTERMINATIONHANDLER_H

#include <semaphore.h>
#include <pthread.h>
#include <queue>
#include <utility>
#include <ProcessController/GTestProcessRegistry.h>

/**
 * \brief This singleton class receives SIGCHLD signals and forwards the child process termination information to corresponding GTestProcessController
 */
class GTestTerminationHandler {
private:
	std::queue< std::pair<pid_t, int> > pids;

	sem_t signalSemaphore;
	pthread_t asyncSignalHandlerThread;

    static GTestTerminationHandler instance;
    GTestTerminationHandler();
    GTestTerminationHandler(const GTestTerminationHandler& orig);
    virtual ~GTestTerminationHandler();
    static void* asyncSIGCHLDHandler(void* Ptr);
    static void SIGCHLDHandler(int Signal);
public:
    /**
     * \brief Singleton getter
     */
    static GTestTerminationHandler* GetGTestTerminationHandler();
};

#endif	/* GTESTTERMINATIONHANDLER_H */

