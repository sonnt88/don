/* 
 * File:   GTestQueuedOutputHandler.h
 * Author: oto4hi
 *
 * Created on May 21, 2012, 2:44 PM
 */

#ifndef GTESTQUEUEDOUTPUTHANDLER_H
#define	GTESTQUEUEDOUTPUTHANDLER_H

#include "GTestBaseOutputHandler.h"

/**
 * \brief base class for every queued output handler (e.g. the GTestExecOutputHandler, GTestListOutputHandler)
 * This base class simply stores every received line from the GTestProcessController in a thread safe queue
 */
class GTestQueuedOutputHandler : public GTestBaseOutputHandler {
protected:
    ThreadSafeQueue< std::string* > lineQueue;
    virtual void OnLineReceived(std::string* Line);
    virtual void OnProcessTerminated(int status);
public:
    /**
     * default constructor
     */
    GTestQueuedOutputHandler();

    /**
     * destructor
     */
    virtual ~GTestQueuedOutputHandler();
};

#endif	/* GTESTQUEUEDOUTPUTHANDLER_H */

