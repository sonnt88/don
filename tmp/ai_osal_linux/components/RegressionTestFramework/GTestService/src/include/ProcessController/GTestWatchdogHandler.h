/* 
 * File:   GTestWatchdogHandler.h
 * Author: aga2hi
 *
 * Created on 07 Oct, 2014, 17:07 AM
 */

#ifndef GTESTWATCHDOGHANDLER_H
#define	GTESTWATCHDOGHANDLER_H

#include <semaphore.h>
#include <pthread.h>
#include <queue>
#include <utility>
#include <ProcessController/GTestProcessRegistry.h>

/**
 * \brief This class receives SIGALRM signal and assumes test process has hung
 */
class GTestWatchdogHandler {
private:
	struct sigaction origSignalAction;
	unsigned int timeout;

    // We don't want any copies to be made
    GTestWatchdogHandler(const GTestWatchdogHandler& orig);
public:
    /**
     * \brief Watchdog constructor
     */
    GTestWatchdogHandler( void * ( * const clientHandler_ )( void ), const unsigned int );
	/**
	 * \brief Watchdog destructor
	 */
	virtual ~GTestWatchdogHandler();
	/**
	 * \brief Set or reset the watchdog countdown timer
	 */
	void Set() const;
	/**
	 * \brief Cancel the watchdog timeout without triggering the timeout action
	 */
	void Cancel() const;
};

#endif	/* GTESTWATCHDOGHANDLER_H */
