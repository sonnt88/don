/*
 * ChattyGTestExecOutputHandler.h
 *
 *  Created on: Jul 19, 2012
 *      Author: oto4hi
 */

#ifndef CHATTYGTESTEXECOUTPUTHANDLER_H_
#define CHATTYGTESTEXECOUTPUTHANDLER_H_

#include "GTestExecOutputHandler.h"
#include <Core/Tasks/BaseTask.h>
#include <Core/Interfaces/ITaskWorker.h>
#include <Core/BaseTaskSource.h>

/**
 * \brief The ChattyGTestExecOutputHandler forwards new test result events and the all test done event to an object implementing the ITaskWorker interface (e.g. the GTestServiceCore)
 */
class ChattyGTestExecOutputHandler: public GTestExecOutputHandler, public BaseTaskSouce {
protected:
	/**
	 * \brief OnNewTestResult() creates a new SendResultPacketTask to be enqueued in the TaskWorker
	 */
	virtual void OnNewTestResult(std::string TestCaseName, std::string TestName, TEST_STATE state);

	/**
	 * \brief OnAllLinesProcessed() creates a new SendTestsDonePacketTask or ReactOnCrashTask to be enqueued in the TaskWorker
	 */
	virtual void OnAllLinesProcessed();
public:
	/**
	 * \brief constructor
	 * @param TaskWorker pointer to ITaskWorker implementation to forward tasks to
	 * @param TestRun TestRunModel to work on
	 */
	ChattyGTestExecOutputHandler(ITaskWorker* TaskWorker, TestRunModel* TestRun);

	/**
	 * destructor
	 */
	~ChattyGTestExecOutputHandler();
};

#endif /* CHATTYGTESTEXECOUTPUTHANDLER_H_ */
