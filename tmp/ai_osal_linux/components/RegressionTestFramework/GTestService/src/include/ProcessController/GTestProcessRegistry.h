/* 
 * File:   GTestProcessRegistry.h
 * Author: oto4hi
 *
 * Created on May 8, 2012, 10:08 AM
 */

#ifndef GTESTPROCESSREGISTRY_H
#define	GTESTPROCESSREGISTRY_H

#include <map>
#include <ProcessController/GTestProcessController.h>
#include <pthread.h>

/**
 * \brief Singleton object to keep track of the mapping between child processes and GTestProcessController instances
 */
class GTestProcessRegistry {
private:
    static GTestProcessRegistry instance;
    std::map< pid_t, GTestProcessController* > processControllerMap;
    pthread_mutex_t mutex;
    
    GTestProcessRegistry();
    virtual ~GTestProcessRegistry();

    pid_t internalGetChildpid(GTestProcessController* ProcessController);
public:
    /**
     * \brief static singleton getter
     */
    static GTestProcessRegistry* GetGTestProcessRegistry();

    /**
     * \brief register a tuple of child process PID and GTestProcessController
     * @param Childpid PID of the GTestProcessController's child process
     * @param ProcessController pointer to the GTestProcessController
     */
    void RegisterGTestProcess(pid_t Childpid, GTestProcessController* ProcessController);

    /**
     * \brief Getter for the GTestProcessController belonging to a child process PID
     * @param Childpid child process PID
     */
    GTestProcessController* GetGTestProcessController(pid_t Childpid);

    /**
     * \brief Unregister the GTestProcessController belonging to a child process PID
     * @param Childpid child process PID
     */
    void UnregisterGTestProcess(pid_t Childpid);

    /**
     * \brief Get a child process PID by a ProcessController
     * @param ProcessController pointer to the GTestProcessController
     */
    pid_t GetChildpid(GTestProcessController* ProcessController);
};

#endif	/* GTESTPROCESSREGISTRY_H */

