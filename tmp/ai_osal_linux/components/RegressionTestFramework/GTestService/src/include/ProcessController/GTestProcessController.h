/* 
 * File:   GTestProcessController.h
 * Author: oto4hi
 *
 * Created on April 19, 2012, 2:26 PM
 */

#ifndef GTESTPROCESSCONTROLLER_H
#define	GTESTPROCESSCONTROLLER_H

#include <Models/GTestConfigurationModel.h>
#include <Utils/ThreadSafeQueue.h>
#include <string>
#include <map>
#include <vector>
#include <pthread.h>
#include <signal.h>
#include "GTestBaseOutputHandler.h"

/**
 * \brief This class controls the actual execution of the GTest binary as a child process.
 * The output of the child process is read asynchronously and forwarded to the output handlers registered
 * for this instance of GTestProcessController
 */
class GTestProcessController {
private:
	pid_t childpid;
	pthread_mutex_t runMutex;
	pthread_cond_t processTerminationCondition;

    int stdoutPipeFD[2];
    int stderrPipeFD[2];
    
    pthread_t ReadFromStdoutThread;
    pthread_t ReadFromStderrThread;
    
    bool stopAllThreads;
    bool running;
    
    sigset_t newMask;
    sigset_t oldMask;
    
    GTestConfigurationModel* config;
    
    static pthread_mutex_t criticalSectionMutex;
    static void beginCriticalSection();
    static void endCriticalSection();
    
    std::vector< GTestBaseOutputHandler* > stdoutOutputHandlers;
    std::vector< GTestBaseOutputHandler* > stderrOutputHandlers;
    
    void notifyOutputHandlersLineReceived(std::string* Line, std::vector< GTestBaseOutputHandler* > *outputHandlers);
    void notifyOutputHandlersProcessTerminated(int status);
    
    char** generateArgumentList();
    void freeArgumentList(char** argumentList);
    void appendToArgumentList(char*** pArgumentList, std::string ArgToAppend, int &length);
    
    void stopReadThreadFuncs();
    
    void readFromPipe(int fd, std::vector< GTestBaseOutputHandler* > *outputHandlers);
    
    void readFromStdoutPipe();
    void readFromStderrPipe();
    
    static void* staticReadFromStdoutPipeThreadFunc(void* thisPtr);
    static void* staticReadFromStderrPipeThreadFunc(void* thisPtr);

    GTestProcessController(GTestProcessController& orig)
	{
		throw std::runtime_error("copy constructor called");
	}

public:
    /**
     * \brief constructor
     * @param Config parameter configuration for the execution of the GTest binary
     */
    GTestProcessController(GTestConfigurationModel Config);
    
    /**
     * \brief register a stdout output handler
     */
    void RegisterStdoutOutputHandler( GTestBaseOutputHandler* OutputHandler );

    /**
	 * \brief register a stderr output handler
	 */
    void RegisterStderrOutputHandler( GTestBaseOutputHandler* OutputHandler );
    
    /**
     * \brief run the process controller
     * Run this function once the setup is complete, e.i. all output handler are registered
     */
    void Run();

    /**
     * \brief Inform the process controller that the child process has terminated
     * This is method is meant to be called by the GTestTerminationHandler.
     */
    void Stop(int status);

    /**
     * \brief Send a SIGKILL to the child process
     */
    void Kill();

    /**
     * block until the child process has terminated
     */
    void WaitForProcessTermination();

    /**
     * destructor
     */
    virtual ~GTestProcessController();
};

#endif	/* GTESTPROCESSCONTROLLER_H */

