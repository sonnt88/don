/*
 * PureTextSendingOutputHandler.h
 *
 *  Created on: 23.10.2012
 *      Author: oto4hi
 */

#ifndef PURETEXTSENDINGOUTPUTHANDLER_H_
#define PURETEXTSENDINGOUTPUTHANDLER_H_

#include "GTestBaseOutputHandler.h"
#include <Core/Tasks/SendTestOutputPacketTask.h>
#include <Core/Interfaces/ITaskWorker.h>

/**
 * \brief The GTestPureTextSendingOutputHandler enqueues a SendTestOutputPacketTask in it's TaskWorker every time a new line is received.
 */
class GTestPureTextSendingOutputHandler: public GTestBaseOutputHandler {
private:
	OUTPUT_SOURCE outputSource;
	ITaskWorker* taskWorker;
protected:
	/**
	 * \brief OnLineReceived() enqueues a SendTestOutputPacketTask in the TaskWorker's queue.
	 * @param Line received output line
	 */
	virtual void OnLineReceived(std::string* Line);
public:
	/**
	 * \brief constructor
	 * @param OutputSource the output source identifier
	 * @param TaskWorker pointer to an instance of a class implementing the ITaskWorker interface (e.g. the GTestServiceCore).
	 * @see OUTPUT_SOURCE
	 * @see ITaskWorker
	 */
	GTestPureTextSendingOutputHandler(OUTPUT_SOURCE OutputSource, ITaskWorker* TaskWorker);

	/**
	 * destructor
	 */
	virtual ~GTestPureTextSendingOutputHandler();
};

#endif /* PURETEXTSENDINGOUTPUTHANDLER_H_ */
