/* 
 * File:   IGTestOutputHandler.h
 * Author: oto4hi
 *
 * Created on May 8, 2012, 6:08 PM
 */

#ifndef IGTESTBASEOUTPUTHANDLER_H
#define	IGTESTBASEOUTPUTHANDLER_H

#include <string>
#include <Utils/ThreadSafeQueue.h>

/**
 * \brief base class for each output handler for the GTestProcessController
 * this can e.g. be output from stdout or stderr
 */
class GTestBaseOutputHandler {
    friend class GTestProcessController;
private:
    pthread_cond_t outputCompleteCondition;
    pthread_mutex_t outputCompleteMutex;
    bool processTerminated;
protected:
    /**
     * \brief check if the child process of the process controller is terminated
     */
    bool ProcessIsTerminated();

    /**
     * \brief handle a received line
     * @param Line pointer to the received line
     * The output handler is responsible for deleting the Line object
     */
    virtual void OnLineReceived(std::string* Line);

    /**
     * \brief handle the child process terminated event
     * @param status the exit value of the child process
     */
    virtual void OnProcessTerminated(int status);
public:
    /**
     * \brief default constructor
     */
    GTestBaseOutputHandler();

    /**
     * \brief wait until the child process of the GTestProcessController has been terminated and there is no more output receivable
     */
    virtual void WaitForOutputCompleted();
    virtual ~GTestBaseOutputHandler();
};

#endif	/* IGTESTBASEOUTPUTHANDLER_H */

