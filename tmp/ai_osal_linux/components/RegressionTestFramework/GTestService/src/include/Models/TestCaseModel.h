/* 
 * File:   GTestCaseModel.h
 * Author: oto4hi
 *
 * Created on April 19, 2012, 2:46 PM
 */

#ifndef TESTCASEMODEL_H
#define	TESTCASEMODEL_H

#include "TestModel.h"
#include "TestSetBaseModel.h"
#include <GTestServiceBuffers.pb.h>

/**
 * \brief data model for a test case (=test fixture)
 */
class TestCaseModel : public TestBaseModel, public TestSetBaseModel {
public:
	/**
	 * \brief constructor
	 * @param Name name of the test case
	 */
    TestCaseModel(std::string Name);

    /**
     * \brief constructor
     * @param Message Protobuf message representing a test case model
     */
    TestCaseModel(GTestServiceBuffers::TestCase Message);
    
    /**
     * \brief AddTest() adds a pointer to a test model object to it's internal list of test case members
     * @param Test pointer to the test to add
     */
    void AddTest(TestModel* Test);

    /**
     * \brief getter for the pointer to the test model with the specified index
     * @param Index index of the test model in the list
     */
    TestModel* operator [](int Index);

    /**
	 * \brief getter for the pointer to the test model with the specified name
	 * @param Name name of the test model
	 */
    TestModel* operator [](std::string Name);

    /**
	 * \brief getter for the pointer to the test model with the specified index
	 * @param Index index of the test model in the list
	 */
    TestModel* GetTest(int Index);

    /**
	 * \brief getter for the pointer to the test model with the specified name
	 * @param Name name of the test model
	 */
    TestModel* GetTest(std::string Name);
    
    /**
     * \brief returns the number of tests in the test case
     */
    int TestCount();

    /**
     * \brief converts the test case model into the Protobuf representation
     */
    void ToProtocolBuffer(::GTestServiceBuffers::TestCase* Message);

    /**
     * \brief destructor
     */
    virtual ~TestCaseModel();
};

#endif	/* TESTCASEMODEL_H */

