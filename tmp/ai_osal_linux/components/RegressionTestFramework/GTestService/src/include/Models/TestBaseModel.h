/* 
 * File:   TestBaseModel.h
 * Author: oto4hi
 *
 * Created on April 19, 2012, 7:27 PM
 */

#ifndef TESTBASEMODEL_H
#define	TESTBASEMODEL_H

#include <string>

/**
 * \brief base class for all test an test case data models
 */
class TestBaseModel {
protected:
    std::string name;
    bool defaultDisabled;
public:
    /**
     * \brief constructor
     * @param Name name of the test or test case
     */
    TestBaseModel(std::string Name);
    virtual ~TestBaseModel();
    
    /**
     * getter for the name property
     */
    std::string GetName() { return this->name; }

    /**
     * getter for the default disabled property
     */
    bool IsDefaultDisabled() { return this->defaultDisabled; }
};

#endif	/* TESTBASEMODEL_H */

