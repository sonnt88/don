/* 
 * File:   GTestConfigurationModel.h
 * Author: oto4hi
 *
 * Created on April 19, 2012, 2:33 PM
 */

#ifndef GTESTCONFIGURATIONMODEL_H
#define	GTESTCONFIGURATIONMODEL_H

#include <string>

#define GTEST_CONF_LIST_ONLY          (1)     /**< gtest option to only list tests (see GTest --gtest_list_tests option)*/
#define GTEST_CONF_EXEC_TESTS         (1<<1)  /**< gtest option to execute tests*/
#define GTEST_CONF_SHUFFLE_TESTS      (1<<2)  /**< gtest option to shuffle tests (see GTest --gtest_shuffle option)*/
#define GTEST_CONF_RUN_DISABLED_TESTS (1<<3)  /**< gtest option to also run disabled tests (see GTest --gtest_also_run_disabled_tests option)*/
#define GTEST_CONF_BREAK_ON_FAILURE   (1<<4)  /**< gtest option to break on failure (see GTest --gtest_break_on_failure option)*/
#define GTEST_CONF_USE_PERSI_FEATURES (1<<5)  /**< gtest option to use the persiGTest features (see PersiGTest --gtest_use_persi_features option)*/

/**
 * \brief data model for the configuration of a GTest run
 */
class GTestConfigurationModel {
private:
    std::string gTestExecutable;
    std::string workingDir;
    std::string filter;
    int 		gtestFlags;
    int         randomSeed;
    int         repeatCount;
public:
    /**
     * \brief constructor
     * @param GTestExecutable path the GTest executable
     * @param Filter GTest test filter string (see GTest --gtest_filter option)
     * @param RepeatCount GTest repeat parameter (see GTest --gtest_repeat option)
     * @param RandomSeed random seed for execution in shuffle mode (see GTest --gtest_shuffle option)
     * @param GTestConfFlags gtest configuration flags, either
     *    - GTEST_CONF_LIST_ONLY
     * or a combination of
     *    - GTEST_CONF_EXEC_TESTS
     *    - GTEST_CONF_SHUFFLE_TESTS
     *    - GTEST_CONF_RUN_DISABLED_TESTS
     *    - GTEST_CONF_BREAK_ON_FAILURE
     *    - GTEST_CONF_USE_PERSI_FEATURES
     */
    GTestConfigurationModel(std::string GTestExecutable, std::string WorkingDir, std::string Filter, int RepeatCount, int RandomSeed, int GTestConfFlags);

    /**
     * \brief copy constructor
     * creates a deep copy of the original object
     */
    GTestConfigurationModel(const GTestConfigurationModel& Other);
    
    /**
     * \brief getter for the GTest executable path
     */
    std::string GetGTestExecutable() { return this->gTestExecutable; }

    /**
	 * \brief getter for the test working directory
	 */
    std::string GetWorkingDir() { return this->workingDir; }

    /**
     * \brief getter for the test filter string
     */
    std::string GetFilter()          { return this->filter; }

    /**
     * \brief getter for the repeat count of the test runs
     */
    int  GetRepeatCount()            { return this->repeatCount; }

    /**
     * \brief getter for the random seed in case of shuffled test execution
     */
    int  GetRandomSeed()             { return this->randomSeed; }

    /**
     * \brief getter for the list tests flag
     */
    bool GetListOnlyFlag()           { return ((this->gtestFlags & GTEST_CONF_LIST_ONLY) != 0); }

    /**
     * \brief getter for the tests shuffling flag
     */
    bool GetShuffleFlag()            { return ((this->gtestFlags & GTEST_CONF_SHUFFLE_TESTS)      != 0 && !GetListOnlyFlag()); }

    /**
     * \brief getter for the run disabled tests flag
     */
    bool GetRunDisabledTestsFlag()   { return ((this->gtestFlags & GTEST_CONF_RUN_DISABLED_TESTS) != 0 && !GetListOnlyFlag()); }

    /**
     * \brief getter for the break on failure flag
     */
    bool GetBreakOnFailureFlag()     { return ((this->gtestFlags & GTEST_CONF_BREAK_ON_FAILURE)   != 0 && !GetListOnlyFlag()); }

    /**
     * \brief getter for the use persi features flag
     */
    bool GetUsePersiFeaturesFlags()  { return ((this->gtestFlags & GTEST_CONF_USE_PERSI_FEATURES) != 0 && !GetListOnlyFlag()); }

    /**
     * \brief getter for all flags
     */
    int GetGTestConfFlags()			 { return this->gtestFlags; }
    
    virtual ~GTestConfigurationModel();
};

#endif	/* GTESTCONFIGURATIONMODEL_H */

