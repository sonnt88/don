/* 
 * File:   TestRunModel.h
 * Author: oto4hi
 *
 * Created on April 19, 2012, 2:50 PM
 */

#ifndef TESTRUNMODEL_H
#define	TESTRUNMODEL_H

#include "TestCaseModel.h"
#include <GTestServiceBuffers.pb.h>
#include <string.h>

/**
 * \brief data model for a whole test run
 */
class TestRunModel : public TestSetBaseModel {
private:
	int iteration;
	std::map<int, TestModel* > testsById; //tests by id
	std::map<int, TestCaseModel* > testCasesByTestID; //test cases by test id

	int currentTestID;
	pthread_mutex_t mutex;

	void init();
public:
	/**
	 * \brief default constructor for an empty test run model
	 */
    TestRunModel();

    /**
     * \brief constructor to build the test run model from a Protobuf test run model representation
     * @param Message Protobuf test run model representation
     */
    TestRunModel(GTestServiceBuffers::AllTestCases Message);

    /**
     * \brief select (enable/disable) tests as specified by the Protobuf TestSelection parameter
     * @param TestSelection Protobuf test selection representation
     */
    void SelectTests(GTestServiceBuffers::TestLaunch* TestSelection);
    
    /**
     * \brief add a test case to the test run model
     * @param TestCase pointer to the test case instance to add
     */
    void AddTestCase( TestCaseModel* TestCase );

    /**
     * \brief getter for a the test case instance pointer with the index specified by the parameter
     * @param Index the test case index
     */
    TestCaseModel* GetTestCase(int Index);

    /**
	 * \brief getter for a the test case instance pointer with the name specified by the parameter
	 * @param Name the test case name
	 */
    TestCaseModel* GetTestCase(std::string Name);

    /**
	 * \brief getter for a the test case instance pointer with the index specified by the parameter
	 * @param Index the test case index
	 */
    TestCaseModel* operator [](int Index);

    /**
	 * \brief getter for a the test case instance pointer with the name specified by the parameter
	 * @param Name the test case name
	 */
    TestCaseModel* operator [](std::string Name);

    /**
	 * \brief getter for a the test case instance pointer with the unique test id specified by the parameter
	 * @param ID the unique test id
	 */
    TestModel* GetTestByID(int ID);

    TestCaseModel* GetTestCaseByTestID(int ID);

    /**
     * \brief return the number of test cases of this test run
     */
    int GetTestCaseCount();

    /**
     * \brief update the internal map of test IDs
     * This function has to be executed after one or many test cases have been added
     */
    void UpdateTestIDMap();

    /**
     * \brief returns the GTest filter string based on the current test selection
     */
    std::string GenerateTestFilterString();

    /**
     * \brief getter for the current test run iteration
     */
    int GetIteration();

    /**
     * \brief set the iteration value
     * This should only be called before the first test result is actually set.
     * @param Iteration iteration initialization value
     */
    void SetIteration(int Iteration);

    /**
     * \brief increment the test run iteration count by one
     */
    void NextIteration();

    /**
     * \brief set the iteration count bacl to zero
     */
    void ResetIteration();

    /**
     * \brief delete all test results
     */
    void ResetAllTestResults();

    /**
     * \brief setter for the current test id
     * @param TestID the new current test id value
     */
    void SetCurrentTestID(int TestID);

    /**
     * \brief getter for the current test id
     */
    int GetCurrentTestID();

    /**
     * \brief convert the test run model into the Protobuf representation
     * @param Message the pointer to an empty instance of a test run model Protobuf representation
     */
    void ToProtocolBuffer(::GTestServiceBuffers::AllTestCases* Message);

    /**
	 * \brief lock this test model instance
	 * If potentially many threads will access this model (and change it) it should be locked before.
	 */
	void LockInstance();

	/**
	 * \brief unlock this test run model instance
	 */
	void UnlockInstance();

    /**
     * destructor
     */
    virtual ~TestRunModel();
};

#endif	/* TESTRUNMODEL_H */

