/* 
 * File:   ThreadSafeQueue.h
 * Author: oto4hi
 *
 * Created on May 3, 2012, 2:24 PM
 */

#ifndef THREADSAFEQUEUE_H
#define	THREADSAFEQUEUE_H

#include <vector>
#include <pthread.h>
#include <stdexcept>
#include <stdio.h>
#include <iostream>

#include "Interfaces/IElementMatcher.h"

/**
 * \brief Specific exception indicating a read operation on an empty queue
 */
class EmptyQueueException : public std::runtime_error {
    public:
		/**
		 * constructor
		 * @param msg the exception message
		 */
        EmptyQueueException(std::string const& msg):
            std::runtime_error(msg)
        {}

};

/**
 * \brief a thread safe queue template implementation
 */
template <class T>
class ThreadSafeQueue {
private:
	std::vector<T> internalQueue;

	IElementMatcher<T>* matcher;

    pthread_mutex_t mutex;
    pthread_cond_t emptyCondition;

    void init()
    {
    	pthread_mutex_init(&(this->mutex), NULL);
		pthread_cond_init(&(this->emptyCondition), NULL);

		this->matcher = NULL;
    }

public:
    /**
     * \brief default constructor
     */
    ThreadSafeQueue()
    {
    	init();
    }

    /**
     * \brief constructor defining a specific matcher to check if Elements match each other
     */
    ThreadSafeQueue(IElementMatcher<T>* Matcher)
    {
    	init();
    	this->matcher = Matcher;
    }

    /**
     * \brief append an element to the end of the queue
     * @param Element the element to add
     */
    void PushBack(T Element)
    {
    	pthread_mutex_lock(&(this->mutex));
    	this->internalQueue.push_back(Element);
    	if (this->internalQueue.size() == 1)
    	{
    		pthread_cond_signal(&(this->emptyCondition));
    	}
    	pthread_mutex_unlock(&(this->mutex));
    }

    /**
     * \brief pop the front element of the queue
     */
    T PopFirst()
    {
    	pthread_mutex_lock(&(this->mutex));

    	if (!this->internalQueue.empty())
    	{
    		T firstElement = this->internalQueue.front();

    		this->internalQueue.erase(this->internalQueue.begin());

    		pthread_mutex_unlock(&(this->mutex));
    		return firstElement;
    	}

    	pthread_mutex_unlock(&(this->mutex));
    	throw EmptyQueueException("Queue is empty.");
    }

    /**
     * \brief get the current number of elements in the queue
     */
    int Size()
    {
    	pthread_mutex_lock(&(this->mutex));
    	int size = this->internalQueue.size();
    	pthread_mutex_unlock(&(this->mutex));
    	return size;
    }

    /**
     * \brief Check if a certain element exists in the queue
     * To be able to use this feature a matcher has to be provided with the constructor
     */
    bool Contains(T element)
    {
    	if (this->matcher == NULL)
    		throw std::runtime_error("unable to call Contains without having a matcher");

    	bool contains = false;
    	pthread_mutex_lock(&(this->mutex));
    	for (unsigned int index = 0; index < this->internalQueue.size(); index++)
    	{
    		if (this->matcher->Match(this->internalQueue[index], element))
    		{
    			contains = true;
    			break;
    		}
    	}
    	pthread_mutex_unlock(&(this->mutex));
    	return contains;
    }

    /**
     * \brief check if the queue is empty
     */
    bool Empty()
    {
    	pthread_mutex_lock(&(this->mutex));
    	bool empty = this->internalQueue.empty();
    	pthread_mutex_unlock(&(this->mutex));
    	return empty;
    }

    /**
     * \brief block until the queue has at least one element
     */
    void BlockUntilNotEmpty()
    {
    	pthread_mutex_lock(&(this->mutex));
    	if (this->internalQueue.empty())
    		pthread_cond_wait(&(this->emptyCondition), &(this->mutex));
    	pthread_mutex_unlock(&(this->mutex));
    }

    /**
     * \brief unblock every waiting party on this queue even if the queue is empty
     */
    void Unblock()
    {
    	pthread_mutex_lock(&(this->mutex));
    	pthread_cond_signal(&(this->emptyCondition));
    	pthread_mutex_unlock(&(this->mutex));
    }

    /**
     * \brief destructor
     */
    ~ThreadSafeQueue()
	{
		pthread_mutex_lock(&(this->mutex));

		if (this->matcher)
			delete matcher;

		this->internalQueue.clear();

		pthread_mutex_unlock(&(this->mutex));

		pthread_mutex_destroy(&(this->mutex));
		pthread_cond_destroy(&(this->emptyCondition));
	}
};

#endif	/* THREADSAFEQUEUE_H */

