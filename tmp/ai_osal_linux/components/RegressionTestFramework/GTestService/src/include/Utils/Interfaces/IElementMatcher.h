/*
 * IElementMatcher.h
 *
 *  Created on: Jul 24, 2012
 *      Author: oto4hi
 */

#ifndef IELEMENTMATCHER_H_
#define IELEMENTMATCHER_H_

/**
 * \brief Interface for a matcher for an arbitrary type
 */
template <class T>
class IElementMatcher {
public:
	/**
	 * \brief Test if Element1 matches Element2
	 */
	virtual bool Match(T Element1, T Element2) = 0;
	virtual ~IElementMatcher() {};
};

#endif /* IELEMENTMATCHER_H_ */
