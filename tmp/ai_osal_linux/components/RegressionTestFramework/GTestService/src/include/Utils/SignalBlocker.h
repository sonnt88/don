/*
 * SignalBlocker.h
 *
 *  Created on: 24.09.2012
 *      Author: oto4hi
 */

#ifndef SIGNALBLOCKER_H_
#define SIGNALBLOCKER_H_

#include <signal.h>
#include <pthread.h>

/**
 * \brief Helper class to block signals
 * If blocking or unblocking fails this class throws an std::runtime_error
 */
class SignalBlocker {
private:
	static void blockOrUnblock(int Signal, bool block)
	{
		int how = (block) ? SIG_BLOCK : SIG_UNBLOCK;

		sigset_t signalSet;
		sigemptyset(&signalSet);
		sigaddset(&signalSet, Signal);

		if (pthread_sigmask(how, &signalSet, NULL) == -1)
			throw std::runtime_error("error blocking/unblocking SIGCHLD");
	}
public:
	/**
	 * \brief Block a signal.
	 * @param Signal the signal to block
	 */
	static void Block(int Signal)
	{
		SignalBlocker::blockOrUnblock(SIGCHLD, true);
	}

	/**
	 * \brief Unblock a signal.
	 * @param Signal the signal to unblock
	 */
	static void Unblock(int Signal)
	{
		SignalBlocker::blockOrUnblock(SIGCHLD, false);
	}
};

#endif /* SIGNALBLOCKER_H_ */
