/*
 * TaskMatcher.h
 *
 *  Created on: Jul 25, 2012
 *      Author: oto4hi
 */

#ifndef TASKMATCHER_H_
#define TASKMATCHER_H_

#include <Core/Tasks/BaseTask.h>
#include "Interfaces/IElementMatcher.h"

/**
 * \brief Concrete matcher for BaseTask objects
 */
class TaskMatcher: public IElementMatcher<BaseTask*> {
public:
	/**
	 * \brief Test if two BaseTasks match
	 * @param Element1 pointer to first BaseTask object
	 * @param Element2 pointer to second BaseTask object
	 */
	virtual bool Match(BaseTask* Element1, BaseTask* Element2);
};

#endif /* TASKMATCHER_H_ */
