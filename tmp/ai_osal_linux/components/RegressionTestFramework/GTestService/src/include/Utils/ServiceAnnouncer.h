/*
 * ServiceAnnouncer.h
 *
 *  Created on: 30.11.2012
 *      Author: oto4hi
 */

#ifndef SERVICEANNOUNCER_H_
#define SERVICEANNOUNCER_H_

#include <pthread.h>
#include <string>

#define GTESTSERVICE_DISCOVERY_STRING "gtest service discovery"
#define GTESTSERVICE_ANSWER_STRING "gtest service answer"

#define DEFAULT_ANNOUNCE_UDP_PORT 5678

#ifndef FRIEND_TEST
#define FRIEND_TEST(TestCase, Test)
#endif

/**
 * \brief The ServiceAnnouncer announces the existence of the GTestService a UDP port
 * On the reception of a discovery packet the announcer answers with the IP address information,
 * the port and the name of it's service
 */
class ServiceAnnouncer {
private:
	FRIEND_TEST(ServiceAnnouncerTests, AnnounceService);

	static const char *discoveryString;
	static const char *answerString;

	static const int nameLength = 20;

	std::string name;

	int serviceport;
	int announceport;
	bool cont;
	bool running;

	pthread_t announceThread;
	pthread_mutex_t runMutex;

	void announceServiceFunc();
	static void* staticAnnounceServiceFunc(void* thisPtr);
public:
	/**
	 * \brief constructor
	 * @param ServiceTCPPort TCP listening port of the GTestService
	 * @param AnnounceUDPPort udp port the ServiceAnnouncer should be listening on for discovery packets
	 * @param Name the name of the GTestService instance (e.g. "Bugs Bunny's Tests")
	 */
	ServiceAnnouncer(int ServiceTCPPort, int AnnounceUDPPort, std::string Name);

	/**
	 * \brief Start the service announcer.
	 * The service announcer will run asynchronously
	 */
	void Run();

	/**
	 * \brief Stop the service announcer
	 */
	void Stop();

	/**
	 * destrutor
	 */
	virtual ~ServiceAnnouncer();
};

#endif /* SERVICEANNOUNCER_H_ */
