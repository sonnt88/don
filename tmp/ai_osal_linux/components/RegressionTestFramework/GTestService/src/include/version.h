/*
 * version.h
 *
 *  Created on: Jun 12, 2012
 *      Author: oto4hi
 */

#ifndef VERSION_H_
#define VERSION_H_

/**
 * the GTestService version
 */
#define GTEST_SERVICE_VERSION "0.1"

#endif /* VERSION_H_ */
