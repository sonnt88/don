/* 
 * File:   IPacketReceiver.h
 * Author: oto4hi
 *
 * Created on June 5, 2012, 4:33 PM
 */

#ifndef IPACKETRECEIVER_H
#define	IPACKETRECEIVER_H

#include <AdapterConnection/Interfaces/IConnection.h>
#include <AdapterConnection/AdapterPacket.h>

/**
 * \brief interface for a packet receiving class
 */
class IPacketReceiver {
public:
	/**
	 * handler for a received packet
	 * @param Connection pointer to the connection where the packet originates from
	 * @param Packet pointer to the data packet
	 */
    virtual void OnPacketReceive(IConnection* Connection, AdapterPacket* Packet) = 0;
    virtual ~IPacketReceiver() {};
};

#endif	/* IPACKETRECEIVER_H */

