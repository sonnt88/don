/* 
 * File:   IConnection.h
 * Author: oto4hi
 *
 * Created on June 5, 2012, 4:07 PM
 */

#ifndef ICONNECTION_H
#define	ICONNECTION_H

#include <AdapterConnection/AdapterPacket.h>

/**
 * \brief Interface for a GTestService TCP/IP connection
 */
class IConnection {
public:
	/**
	 * send a data packet
	 * @param Data the data to send
	 * @see AdapterPacket
	 */
    virtual void Send(AdapterPacket& Data) = 0;

    /**
     * receive a data packet
     * \return a pointer to the received data packet if a data packet has been received or null if no packet has been received or the connection has been closed.
     */
    virtual AdapterPacket* Receive() = 0;

    /**
     * close the connection
     */
    virtual void Close() = 0;

    /**
     * check if the connection has been closed. To determine if a connection has been closed from the peer Receive() has to be called first.
     */
    virtual bool IsClosed() = 0;

    /**
     * get the unique connection id
     */
    virtual int GetID() = 0;
    
    /**
     * destructor
     */
    virtual ~IConnection() {};
};

#endif	/* ICONNECTION_H */

