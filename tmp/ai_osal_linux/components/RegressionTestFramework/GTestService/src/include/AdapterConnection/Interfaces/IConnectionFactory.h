/* 
 * File:   IAdapterFactory.h
 * Author: oto4hi
 *
 * Created on June 5, 2012, 4:52 PM
 */

#ifndef IADAPTERFACTORY_H
#define	IADAPTERFACTORY_H

#include <AdapterConnection/Interfaces/IConnection.h>

/**
 * \brief interface that all connection factories derive from
 *
 * This is the interface for a GoF factory pattern for TCP/IP connections
 */
class IConnectionFactory {
public:
	/**
	 * factory method
	 * @param Sock a connected socket
	 */
    virtual IConnection* CreateConnection(int Sock) = 0;

    /**
     * destructor
     */
    virtual ~IConnectionFactory() {};
};

#endif	/* IADAPTERFACTORY_H */

