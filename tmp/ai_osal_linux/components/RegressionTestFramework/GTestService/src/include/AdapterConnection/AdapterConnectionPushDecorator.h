/* 
 * File:   AdapterConnectionPushDecorator.h
 * Author: oto4hi
 *
 * Created on June 5, 2012, 4:23 PM
 */

#ifndef ADAPTERCONNECTIONPUSHDECORATOR_H
#define	ADAPTERCONNECTIONPUSHDECORATOR_H

#include "Interfaces/IConnection.h"
#include "Interfaces/IPacketReceiver.h"

/**
 * \brief GoF decorator for an AdapterConnection that forwards data packets to an instance of a class that
 * implements the IPacketReceiver interface
 */
class AdapterConnectionPushDecorator : public IConnection {
private:
    IConnection* decoratee;
    IPacketReceiver* receiver;
public:
    /**
     * \brief constructor
     * @param Decoratee pointer to an object that implements the IConnection interface
     * @param Receiver pointer to the actual receiving object implementing the IPacketReceiver interface
     * @see IConnection
     * @see IPacketReceiver
     */
    AdapterConnectionPushDecorator(IConnection* Decoratee, IPacketReceiver* Receiver);
    
    /**
     * \brief send a data packet (pure delegate to decoratee)
     * @param Data data packet to send
     * @see AdapterPacket
     */
    virtual void Send(AdapterPacket& Data);

    /**
     * \brief receive a packet and forward it to the receiver object
     */
    virtual AdapterPacket* Receive();

    /**
     * \brief close the connection (pure delegate to decoratee)
     */
    virtual void Close();

    /**
     * \brief check if connection is closed (pure delegate to decoratee)
     */
    virtual bool IsClosed();

    /**
     * \brief get the unique connection ID (pure delegate to decoratee)
     */
    virtual int GetID();
    
    virtual ~AdapterConnectionPushDecorator();
};

#endif	/* ADAPTERCONNECTIONPUSHDECORATOR_H */

