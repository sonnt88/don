/*
 * IPFilter.h
 *
 *  Created on: 05.02.2013
 *      Author: oto4hi
 */

#ifndef IPFILTER_H_
#define IPFILTER_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>

/**
 * \brief model for a simple IP filter to specify which client IP addresses to allow
 */
class IPFilter {
private:
	std::string ipFilterString;
	std::string generateExceptionMessage(std::string IPFilterString);
	bool checkOnlyContainsDigits(std::string IntegerString);
	struct in_addr ipaddr;
	short fixedbits;
public:
	/**
	 * \brief constructor
	 * @param IPFilterString valid IP filter formats a strings like
	 *     - "192.168.10.1"
	 *     - "192.168.10.0/24"
	 *     - "192.168.0.0/16"
	 *     - "192.0.0.0/8"
	 */
	IPFilter(std::string IPFilterString);

	/**
	 * \brief copy constructor
	 */
	IPFilter(IPFilter &orig);

	/**
	 * \brief getter for the ip address part of the ip filter
	 */
	struct in_addr GetIPAddr();

	/**
	 * \brief get the string representation of the ip filter
	 */
	std::string GetIPFilterString();

	/**
	 * \brief get the number of fixed bits in the ip address part of the ip filter
	 */
	short GetFixedBits();

	/**
	 * \brief check if an ip address matches the filter
	 * @param IPAddr pointer to the ip address to match
	 */
	bool Matches(struct in_addr *IPAddr);

	virtual ~IPFilter();
};

#endif /* IPFILTER_H_ */
