/* 
 * File:   NetworkChunk.h
 * Author: oto4hi
 *
 * Created on May 23, 2012, 2:43 PM
 */

#ifndef DATACHUNK_H
#define	DATACHUNK_H

#include <stdlib.h>
#include <stdint.h>
#include "PacketIDs.h"
#include <arpa/inet.h>

/**
 * the packed fixed header size of an AdapterPacket
 */
#define ADAPTER_PACKET_HEADER_SIZE 16

/**
 * the maximum payload size for an Adapter packet
 */
#define ADAPTER_PACKET_MAX_PAYLOAD_SIZE 0x100000

/**
 * the resulting overall size of an AdapterPacket is the sum of ADAPTER_PACKET_HEADER_SIZE and ADAPTER_PACKET_MAX_PAYLOAD_SIZE
 */
#define ADAPTER_PACKET_MAX_SIZE (ADAPTER_PACKET_HEADER_SIZE+ADAPTER_PACKET_MAX_PAYLOAD_SIZE)

/**
 * \brief representation of the header of an AdapterPacket
 */
class AdapterPacketHeader
{
public:
	/**
	 * \brief constructor
	 * build the AdapterPacketHeader from a data chunk. All data is in network byte order here
	 */
	AdapterPacketHeader(char* Buffer);

	/**
	 * \brief copy constructor
	 * the copy constructor performs a deep copy of the original object
	 */
	AdapterPacketHeader(AdapterPacketHeader& orig);

	/**
	 * \brief constructor
	 * build the AdapterPacketHeader specifying each data element
	 * @param ID id of the packet (see PacketIDs.h for valid packet ids).
	 * @param Flags not implemented yet.
	 * @param Serial the packet serial, usually 0 (will be set by the send routine of an AdapterConnection).
	 * @param RequestSerial if the packet is a response to a query packet set this field to the packet serial of the query packet.
	 * @param PayloadSize payload size in bytes for this packet
	 */
	AdapterPacketHeader(char ID, char Flags, uint32_t Serial, uint32_t RequestSerial, uint32_t PayloadSize);
	char id;
	char flags;
	uint32_t serial;
	uint32_t requestSerial;
	uint32_t payloadSize;
};

/**
 * \brief data model for a data packet to the GTest adapter
 */
class AdapterPacket {
private:
    friend class AdapterConnection;
    
    AdapterPacketHeader header;
    char* payload;
    void setPayload(char* Payload);
    
    uint32_t connectionID;
public:
    /**
     * \brief copy constructor
     * performs a deep copy of the original AdapterPacket
     */
    AdapterPacket(AdapterPacket& orig);

    /**
     * \brief constructor
     * @param Header header object for this packet
     * @param Payload pointer to the payload. It's length has to be consistent with the PayloadSize entry in the header.
     * @param connectionID the ID of the sending connection (used for consistency checks)
     */
    AdapterPacket(AdapterPacketHeader Header, char* Payload, uint32_t connectionID);

    /**
     * \brief constructor
     * build the AdapterPacketHeader specifying each data element
	 * @param ID id of the packet (see PacketIDs.h for valid packet ids).
	 * @param Flags not implemented yet.
	 * @param Serial the packet serial, usually 0 (will be set by the send routine of an AdapterConnection).
	 * @param RequestSerial if the packet is a response to a query packet set this field to the packet serial of the query packet.
	 * @param PayloadSize payload size in bytes for this packet
	 * @param Payload pointer to the payload. It's length has to be consistent with the PayloadSize entry in the header.
     * @param connectionID the ID of the sending connection (used for consistency checks)
     */
    AdapterPacket(char ID, char Flags, uint32_t Serial, uint32_t RequestSerial, uint32_t PayloadSize, char* Payload, uint32_t connectionID);
    
    /**
     * \brief getter for the packet id
     */
    char GetID();

    /**
     * getter for the packet flags
     */
    char GetFlags();

    /**
	 * getter for the packet serial
	 */
    uint32_t GetSerial();

    /**
     * \brief getter for the packet request serial
     * getter for the ID of the packet that requested the answer that is represented with this AdapterPacket instance
     */
    uint32_t GetRequestSerial();

    /**
     * \brief getter for the payload size
     */
    uint32_t GetPayloadSize();

    /**
     * \brief Getter to the pointer of the packet payload.
     * Do not change the data content here. The changes will most likely affect the packet consistency.
     */
    char* GetPayload();
    
    virtual ~AdapterPacket();
};

#endif	/* DATACHUNK_H */

