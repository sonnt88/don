/* 
 * File:   AdapterConnectionPushDecoratorFactor.h
 * Author: oto4hi
 *
 * Created on June 5, 2012, 5:16 PM
 */

#ifndef ADAPTERCONNECTIONPUSHDECORATORFACTOR_H
#define	ADAPTERCONNECTIONPUSHDECORATORFACTOR_H

#include <AdapterConnection/Interfaces/IConnectionFactory.h>
#include <AdapterConnection/Interfaces/IPacketReceiver.h>

/**
 * \brief factory for AdapterConnectionPushDecorator objects with a pure AdapterConnection as decoratee
 */
class AdapterConnectionPushDecoratorFactory : public IConnectionFactory {
private:
    IPacketReceiver* receiver;
public:
    /**
     * constructor
     * @param Receiver pointer to actual receiver object
     * @see IPacketReceiver
     */
    AdapterConnectionPushDecoratorFactory(IPacketReceiver* Receiver);

    /**
     * \brief factory method
     * returns an AdapterConnectionPushDecorator object
     * @param Sock connected socket to peer
     */
    virtual IConnection* CreateConnection(int Sock);

    /**
     * \brief destructor
     */
    virtual ~AdapterConnectionPushDecoratorFactory();
};

#endif	/* ADAPTERCONNECTIONPUSHDECORATORFACTOR_H */

