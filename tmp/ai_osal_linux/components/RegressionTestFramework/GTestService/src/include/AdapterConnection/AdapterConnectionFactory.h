/* 
 * File:   AdapterConnectionFactory.h
 * Author: oto4hi
 *
 * Created on June 5, 2012, 4:54 PM
 */

#ifndef ADAPTERCONNECTIONFACTORY_H
#define	ADAPTERCONNECTIONFACTORY_H

#include <AdapterConnection/Interfaces/IConnectionFactory.h>
#include <AdapterConnection/Interfaces/IConnection.h>

/**
 * \brief Factory for AdapterConnection objects
 */
class AdapterConnectionFactory : public IConnectionFactory {
private:
    
public:
    AdapterConnectionFactory();
    
    /**
     * \brief factory method
     * This method returns a pointer to an AdapterConnection object
     * @param Sock a connected socket
     */
    virtual IConnection* CreateConnection(int Sock);
    
    virtual ~AdapterConnectionFactory();
};

#endif	/* ADAPTERCONNECTIONFACTORY_H */

