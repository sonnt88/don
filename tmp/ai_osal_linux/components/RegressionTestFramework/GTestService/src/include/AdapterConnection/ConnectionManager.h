/* 
 * File:   ConnectionManager.h
 * Author: oto4hi
 *
 * Created on May 10, 2012, 5:40 PM
 */

#ifndef CONNECTIONMANAGER_H
#define	CONNECTIONMANAGER_H

#include <pthread.h>
#include <map>
#include <vector>
#include <AdapterConnection/Interfaces/IConnectionFactory.h>
#include <AdapterConnection/IPFilter.h>

/**
 * \brief Class for asynchronous management of AdapterConnections
 * Looping in a separate thread this class is responsible for
 *     - accepting incoming connections
 *     - waiting for incoming packets and triggering the reception of packets for each AdapterConnection
 */
class ConnectionManager {
private:
    fd_set masterSet, readSet, writeSet;
    int maxSocket;
    
    IPFilter* ipFilter;

    IConnectionFactory* connectionFactory;
    
    std::map< int, IConnection* > activeConnections;
    std::vector< IConnection* > allConnections;
    
    pthread_t listenThread;
    pthread_mutex_t criticalSectionMutex;

    pthread_mutex_t startMutex;
    pthread_cond_t startCondition;

    pthread_mutex_t connectionMutex;
    
    bool running;
    bool stopAllThreads;
    bool listenSocketOpen;
    
    int port;
    
    void AcceptNewConnection(int ListenSocket, fd_set &MasterSet, int &MaxSocket);
    IConnection* GetAdapterConnectionBySock(int Sock);
    
    bool filterMatches(struct sockaddr_in *ClientAddr);

    int createNonBlockingSocket();
    void bindSocket(int Socket);
    void initSocketMasterSet(int ListenSocket);
    void setStartCondition();
    
    int setUpListenSocket();
    void receivePacketsInQueue(IConnection* Connection);
    void performSelect();
    void cleanupConnections();
    void stopListening(int ListenSocket);

    static void* listenThreadFunc(void* thisPtr);
    void beginCriticalSection();
    void endCriticalSection();
    
    void removeConnection(int Socket);
public:
    /**
     * \brief constructor
     * @param Port port to listen on for incoming connections
     * @param ConnectionFactory pointer to connection factory to use
     * @param IpFilter pointer to IPFilter to specify which clients to allow (NULL for no filter)
     * @see IConnectionFactory
     * @see IPFilter
     */
    ConnectionManager(int Port, IConnectionFactory* ConnectionFactory, IPFilter* IpFilter);

    /**
     * \brief start the main loop of the connection manager
     */
    void Start();

    /**
     * \brief stop the main loop of the connection manager
     */
    void Stop();
    
    /**
     * \brief send a data packet to every peer connected to an AdapterConnection object associated with this ConnectionManager
     * @param packet packet to be sent (the fields 'RequestSerial' and 'ConnectionID' should be set to 0 when multicasting)
     */
    void Multicast(AdapterPacket packet);

    /**
     * \brief get the number of active (connected) connections associated to this ConnectionManager
     */
    unsigned int GetConnectionCount();

    /**
     * \brief get AdapterConnection by index
     */
    IConnection* operator[] (unsigned int Index);
    
    /**
     * \brief delete a specific ConnectionAdapter
     */
    void DeleteConnection( IConnection* connection );
    
    /**
     * \brief destructor
     * the destructor will disconnect and destroy all AdapterConnections associated with the ConnectionManager instance
     */
    virtual ~ConnectionManager();
};

#endif	/* CONNECTIONMANAGER_H */

