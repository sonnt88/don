/* 
 * File:   AdapterConnection.h
 * Author: oto4hi
 *
 * Created on May 10, 2012, 5:14 PM
 */

#ifndef ADAPTERCONNECTION_H
#define	ADAPTERCONNECTION_H

#include "Interfaces/IConnection.h"
#include "AdapterPacket.h"
#include <Utils/ThreadSafeQueue.h>

#include <pthread.h>
#include <stdexcept>

namespace GTestServiceExceptions
{
	/**
	 * \brief exception to be able to divide between a simple runtime
	 * exception and an exception that is caused by a closed connection
	 */
	class ConnectionClosedException : public std::runtime_error
	{
	public:
		ConnectionClosedException(std::string const& msg):
			std::runtime_error(msg)
		{}
	};

};

/**
 * \brief representation of a GTestService TCP/IP connection
 *
 *
 */
class AdapterConnection : public IConnection
{
private:
    friend class ConnectionManager;
    
    unsigned int bytesStillToReceive;
    unsigned int currentReceiveBufferPosition;
    char receiveBuffer[ADAPTER_PACKET_MAX_SIZE];

    static uint32_t adapterConnectionCounter;
    static pthread_mutex_t connectionIDMutex;
    int connectionID;
    
    static uint32_t generatePacketSerial();
    static uint32_t currentPacketSerial;
    
    static pthread_mutex_t serialMutex;
    pthread_mutex_t socketMutex;
    
    int sock;
    bool closed;
    
    void internalClose();
    
    AdapterPacketHeader* ReceiveHeader();
    bool TryReceivePacketToTheEnd();

    int rawSend(char* Data, int Size);

    AdapterConnection(AdapterConnection& orig)
    {
    	throw std::runtime_error("copy constructor now callable for AdapterConnection");
    }

public:
    /**
     * Constructor for a connection object
     * @param Sock a connected socket
     */
    AdapterConnection(int Sock);
    
    /**
     * \brief send routine for an open connection
     *
     * This routine sends a data packet to the peer of the connection. If the connection is already closed
     * Send() will throw a GTestServiceExceptions::ConnectionClosedException
     * @param Data packet to send
     * @see AdapterPacket
     */
    virtual void Send(AdapterPacket& Data);

    /**
     * \brief receive a data packet from the peer
     * This routine receives a data packet from the connection peer. If the connection is open the routine
     * returns a valid pointer to an AdapterPacket object or NULL if no packet has been received.
     * If the connection has already been closed Receive() will throw a GTestServiceExceptions::ConnectionClosedException.
     */
    virtual AdapterPacket* Receive();

    /**
     * \brief close an open connection
     * This routine closes the TCP connection to the peer.
     * If Close() is called on a connection that has already been closed it will throw a GTestServiceExceptions::ConnectionClosedException.
     */
    virtual void Close();
    
    /**
     * \brief check if the connection is closed or not.
     */
    virtual bool IsClosed();

    /**
     * return the unique connection ID.
     */
    virtual int GetID();
    
    virtual ~AdapterConnection();
};

#endif	/* ADAPTERCONNECTION_H */

