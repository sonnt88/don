/*
 * SendVersionTask.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef SENDVERSIONTASK_H_
#define SENDVERSIONTASK_H_

#include "BaseReplyTask.h"

/**
 * \brief This task's handler will send the GTestService version to the requesting client.
 */
class SendVersionTask: public BaseReplyTask {
public:
	/**
	 * \brief constructor
	 * @param Connection pointer to the connection object associated with this task
	 * @param RequestSerial serial of the corresponding request packet
	 */
	SendVersionTask(IConnection* Connection, uint32_t RequestSerial);

	/**
	 * destructor
	 */
	virtual ~SendVersionTask() {};
};

#endif /* SENDVERSIONTASK_H_ */
