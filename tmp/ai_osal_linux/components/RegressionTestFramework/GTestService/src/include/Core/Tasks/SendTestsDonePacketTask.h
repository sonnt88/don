/*
 * SendTestsDonePacket.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef SENDTESTSDONEPACKET_H_
#define SENDTESTSDONEPACKET_H_

#include "BaseTask.h"

/**
 * \brief This task's handler will send a packet to all clients to communicate that the execution of all tests is done.
 */
class SendTestsDonePacketTask : public BaseTask{
public:
	/**
	 * constructor
	 */
	SendTestsDonePacketTask();

	/**
	 * destructor
	 */
	virtual ~SendTestsDonePacketTask() {};
};

#endif /* SENDTESTSDONEPACKET_H_ */
