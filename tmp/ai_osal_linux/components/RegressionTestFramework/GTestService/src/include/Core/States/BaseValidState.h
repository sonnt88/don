/*
 * BaseValidState.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef BASEVALIDSTATE_H_
#define BASEVALIDSTATE_H_

#include <Core/Interfaces/ICoreState.h>
#include <Core/GTestServiceCore.h>

/**
 * \brief all valid states derive from this interface
 * @see ICoreState for detailed documentation
 */
class BaseValidState : public ICoreState{
protected:
	GTestServiceCore* context;
public:
	BaseValidState(GTestServiceCore* Context);
	virtual ICoreState* HandleSendTestListTask(SendTestListTask* Task);
	virtual ICoreState* HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task);
	virtual ICoreState* HandleSendAllResultsTask(SendAllResultsTask* Task);
	virtual ~BaseValidState();
};

#endif /* BASEVALIDSTATE_H_ */
