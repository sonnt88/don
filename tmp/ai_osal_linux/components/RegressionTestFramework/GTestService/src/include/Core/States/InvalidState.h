/*
 * InvalidState.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef INVALIDSTATE_H_
#define INVALIDSTATE_H_

#include <Core/Interfaces/ICoreState.h>

/**
 * \brief this state will be entered if an unrecoverable error occurs, e.g. a non-existent gtest executable
 * @see ICoreState for detailed documentation
 */
class InvalidState : public ICoreState {
public:
	InvalidState();
	virtual ICoreState* HandleSendTestListTask(SendTestListTask* Task);
	virtual ICoreState* HandleSendCurrentStateTask(SendCurrentStateTask* Task);
	virtual ICoreState* HandleExecTestsTask(ExecTestsTask* Task);
	virtual ICoreState* HandleSendResultPacketTask(SendResultPacketTask* Task);
	virtual ICoreState* HandleSendResetPacketTask(SendResetPacketTask* Task);
	virtual ICoreState* HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task);
	virtual ICoreState* HandleSendAllResultsTask(SendAllResultsTask* Task);
	virtual ICoreState* HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task);
	virtual ICoreState* HandleReactOnCrashTask(ReactOnCrashTask* Task);
	virtual ICoreState* HandleAbortTestsTask(AbortTestsTask* Task);
	virtual ICoreState* HandleQuitTask(QuitTask* Task);
	virtual GTEST_SERVICE_STATE GetTypeOfState();
};

#endif /* INVALIDSTATE_H_ */
