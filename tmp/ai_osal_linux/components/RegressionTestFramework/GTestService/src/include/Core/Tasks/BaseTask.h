/*
 * GTestCoreTask.h
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#ifndef GTESTCORETASK_H_
#define GTESTCORETASK_H_

/**
 * type enum for every task that can be handled by the GTestService core class
 */
enum GTEST_CORE_TASKS {
	TASK_SEND_VERSION_TASK,
	TASK_SEND_CURRENT_STATE,
	TASK_SEND_TESTLIST,
	TASK_EXEC_TESTS,
	TASK_SEND_RESULT_PACKET,
	TASK_SEND_RESET_PACKET,
	TASK_SEND_TESTS_DONE_PACKET,
	TASK_SEND_TEST_OUTPUT_PACKET,
	TASK_SEND_ALL_RESULTS,
	TASK_REACT_ON_CRASH,
	TASK_ABORT_TESTS,
	TASK_QUIT
};

/**
 * \brief base class for every task that can be handled by the GTestService core class
 */
class BaseTask {
private:
	GTEST_CORE_TASKS typeOfTask;
protected:
	BaseTask(GTEST_CORE_TASKS TypeOfTask);
public:
	virtual GTEST_CORE_TASKS GetTypeOfTask();
	virtual ~BaseTask() {};
};



#endif /* GTESTCORETASK_H_ */
