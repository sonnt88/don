/*
 * SendAllResultsTask.h
 *
 *  Created on: 20.11.2012
 *      Author: oto4hi
 */

#ifndef SENDALLRESULTSTASK_H_
#define SENDALLRESULTSTASK_H_

#include <Core/Tasks/BaseReplyTask.h>

/**
 * \brief When executing this tasks all available test results will be resent to the client
 */
class SendAllResultsTask: public BaseReplyTask {
public:
	/**
	 * constructor
	 * @param Connection pointer to the corresponding client connection
	 * @param RequestSerial serial of the corresponding request packet
	 */
	SendAllResultsTask(IConnection* Connection, uint32_t RequestSerial);

	/**
	 * destructor
	 */
	virtual ~SendAllResultsTask() {};
};

#endif /* SENDALLRESULTSTASK_H_ */
