/*
 * QuitTask.h
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#ifndef QUITTASK_H_
#define QUITTASK_H_

#include <Core/Tasks/BaseTask.h>

/**
 * \brief the execution of this task will lead to the GTestServiceCore leaving the main loop
 */
class QuitTask: public BaseTask {
public:
	/**
	 * \brief constructor
	 */
	QuitTask();

	/**
	 * \brief destructor
	 */
	virtual ~QuitTask() {};
};

#endif /* QUITTASK_H_ */
