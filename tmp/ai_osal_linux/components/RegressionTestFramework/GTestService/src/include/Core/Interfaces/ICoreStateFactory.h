/*
 * ICoreStateFactory.h
 *
 *  Created on: 27.02.2013
 *      Author: oto4hi
 */

#ifndef ICORESTATEFACTORY_H_
#define ICORESTATEFACTORY_H_

#include "ICoreState.h"
#include <GTestServiceBuffers.pb.h>

class GTestServiceCore;

/**
 * \brief ICoreStateFactory is the interface for every ICoreState factory class
 */
class ICoreStateFactory {
public:
	/**
	 * \brief create the initial state for the GoF state machine
	 */
	virtual ICoreState* CreateInitialState() = 0;

	/**
	 * \brief factory method for an InvalidState
	 */
	virtual ICoreState* CreateInvalidState() = 0;

	/**
	 * \brief factory method for an IdleState
	 */
	virtual ICoreState* CreateIdleState() = 0;

	/**
	 * \brief factory method for a ResetState
	 * @param RemainingTests pointer to the Protobuf representation of a test selection allowing the list of remaining tests to be preserved following a power cycle
	 */
	virtual ICoreState* CreateResetState(GTestServiceBuffers::TestLaunch const * const RemainingTests) = 0;

	/**
	 * \brief factory method for a RunningState
	 * @param TestSelection pointer to the Protobuf representation of a test selection
	 */
	virtual ICoreState* CreateRunningState(GTestServiceBuffers::TestLaunch* TestSelection) = 0;

	/**
	 * \brief destructor
	 */
	virtual ~ICoreStateFactory() {};
};

#endif /* ICORESTATEFACTORY_H_ */
