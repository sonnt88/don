/*
 * LoggingCoreStateDecorator.h
 *
 *  Created on: 27.02.2013
 *      Author: oto4hi
 */

#ifndef LOGGINGCORESTATEDECORATOR_H_
#define LOGGINGCORESTATEDECORATOR_H_

#include <Core/Interfaces/ICoreState.h>
#include <Core/GTestServiceCore.h>
#include <string>

/**
 * \brief decorator for classes implementing the ICoreState interface. The decorated ICoreState implementation
 * prints out a short line on every task execution and on every state transition.
 */
class LoggingCoreStateDecorator: public ICoreState {
private:
	ICoreState* decoratee;
	GTestServiceCore* context;
	ICoreState* chooseNextStatePointer(ICoreState* NextStateFromHandler);
	std::string StateTypeAsString();
public:
	LoggingCoreStateDecorator(ICoreState* Decoratee, GTestServiceCore* Context);
	virtual ICoreState* HandleSendCurrentStateTask(SendCurrentStateTask* Task);
	virtual ICoreState* HandleSendTestListTask(SendTestListTask* Task);
	virtual ICoreState* HandleExecTestsTask(ExecTestsTask* Task);
	virtual ICoreState* HandleSendResultPacketTask(SendResultPacketTask* Task);
	virtual ICoreState* HandleSendResetPacketTask(SendResetPacketTask* Task);
	virtual ICoreState* HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task);
	virtual ICoreState* HandleSendAllResultsTask(SendAllResultsTask* Task);
	virtual ICoreState* HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task);
	virtual ICoreState* HandleReactOnCrashTask(ReactOnCrashTask* Task);
	virtual ICoreState* HandleAbortTestsTask(AbortTestsTask* Task);
	virtual ICoreState* HandleQuitTask(QuitTask* Task);
	virtual GTEST_SERVICE_STATE GetTypeOfState();
	virtual ~LoggingCoreStateDecorator();
};

#endif /* LOGGINGCORESTATEDECORATOR_H_ */
