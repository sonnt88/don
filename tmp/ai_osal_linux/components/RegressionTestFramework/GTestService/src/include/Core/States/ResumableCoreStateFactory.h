/*
 * ResumableCoreStateFactory.h
 *
 *  Created on: 01.03.2013
 *      Author: oto4hi
 */

#ifndef RESUMABLECORESTATEFACTORY_H_
#define RESUMABLECORESTATEFACTORY_H_

#include <Core/Interfaces/ICoreStateFactory.h>
#include <Core/GTestServiceCore.h>

class ResumableCoreStateFactory: public ICoreStateFactory {
private:
	ICoreStateFactory* decoratee;
	static const char* persistentTestSelectionFile;
	GTestServiceCore * context;

	GTestServiceBuffers::TestLaunch* reloadTestSelectionFromFile();
	void saveTestSelectionToFile(GTestServiceBuffers::TestLaunch const * const TestSelection);
	void deleteTestSelectionFile();
public:
	ResumableCoreStateFactory(ICoreStateFactory* Decoratee, GTestServiceCore * context_);
	virtual ICoreState* CreateInitialState();
	virtual ICoreState* CreateInvalidState();
	virtual ICoreState* CreateIdleState();
	virtual ICoreState* CreateResetState(GTestServiceBuffers::TestLaunch const * const);
	virtual ICoreState* CreateRunningState(GTestServiceBuffers::TestLaunch* TestSelection);
	virtual ~ResumableCoreStateFactory();
};

#endif /* RESUMABLECORESTATEFACTORY_H_ */
