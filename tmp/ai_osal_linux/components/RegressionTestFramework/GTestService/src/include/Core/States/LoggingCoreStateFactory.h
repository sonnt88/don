/*
 * LoggingCoreStateFactory.h
 *
 *  Created on: 27.02.2013
 *      Author: oto4hi
 */

#ifndef LOGGINGCORESTATEFACTORY_H_
#define LOGGINGCORESTATEFACTORY_H_

#include <Core/Interfaces/ICoreStateFactory.h>

/**
 * \brief factory for decorated instances of the classes implementing ICoreState.
 * ICoreState instances decorated by this factory print out a short line on every task
 * execution and on every state transition.
 *
 */
class LoggingCoreStateFactory: public ICoreStateFactory {
private:
	ICoreStateFactory* decoratee;
	GTestServiceCore* context;
public:
	LoggingCoreStateFactory(ICoreStateFactory* Decoratee, GTestServiceCore* Context);
	virtual ICoreState* CreateInitialState();
	virtual ICoreState* CreateInvalidState();
	virtual ICoreState* CreateIdleState();
	virtual ICoreState* CreateResetState(GTestServiceBuffers::TestLaunch const * const);
	virtual ICoreState* CreateRunningState(GTestServiceBuffers::TestLaunch* TestSelection);
	virtual ~LoggingCoreStateFactory();
};

#endif /* LOGGINGCORESTATEFACTORY_H_ */
