/*
 * GTestServiceCore.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef GTESTSERVICECORE_H_
#define GTESTSERVICECORE_H_

#include <Core/Interfaces/ITaskWorker.h>
#include <Core/Interfaces/ICoreStateFactory.h>

#include <Models/TestRunModel.h>
#include <AdapterConnection/ConnectionManager.h>
#include <AdapterConnection/Interfaces/IPacketReceiver.h>
#include <Utils/ThreadSafeQueue.h>

#include <pthread.h>

#ifndef FRIEND_TEST
#define FRIEND_TEST(TestCase, Test)
#endif

/** \brief This is the core class of the GTestService.
 *
 * It is implemented as a state machine with four states:
 * 		- invalid state: this state is entered if a fatal error occurs,
 * 			e.g. if the gtest executable could not be found.
 * 		- idle state: this is the initial state of the program. If no
 * 			tests are currently executed the core will remain in this state.
 * 		- running state: during the execution of tests the core will remain
 * 			in this state until the execution will stop regularly or due to an error.
 * 	The core continuously executes tasks in it's task queue. Depending on the current
 * 	state the handling strategy of a task may differ.
 */

class GTestServiceCore : public ITaskWorker, public IPacketReceiver {
	FRIEND_TEST(GTestServiceCoreTests, RunAndStop);
private :
	bool threaded;
	pthread_t coreThread;
	pthread_mutex_t startConditionMutex;
	pthread_cond_t startCondition;

	ICoreStateFactory* stateFactory;
	ICoreState* state;
	ThreadSafeQueue< BaseTask* > taskQueue;

	TestRunModel* testRunModel;
	std::string gtestPath;
	std::string workingDir;

	ConnectionManager* cmanager;

	bool usePersiFeatures;

	//part of the state pattern (although it's normally public)
	void setState(ICoreState* NewState);
	void init(std::string GTestPath, int PortNo, bool UsePersiFeatures);

	static void* threadFunc(void* thisPtr);
public:
	/**
	 * The GTestServiceCore constructor
	 * @param GTestPath the path to the GTest executable
	 * @param PortNo The GTestService listening TCP port
	 * @param UsePersiFeatures specify whether the persistence feature should be used. This will be forwarded to the GTest executable as a command line argument
	 * @param IpFilter pointer to an IPFilter object. This specifies the IPs that are allowed to connect to this service. If IpFilter is set to NULL the filter be left out.
	 * @see IPFilter
	 */
	GTestServiceCore(std::string GTestPath, std::string WorkingDir, int PortNo, bool UsePersiFeatures, IPFilter* IpFilter);

	/**
	 * \brief Enques another task to be executed by the GTestServiceCore.
	 * @param Task task to be executed
	 * @see BaseTask
	 * @see ITaskWorker
	 */
	virtual void EnqueueTask(BaseTask* Task);

	/**
	 * \brief packet handler
	 */
	virtual void OnPacketReceive(IConnection* Connection, AdapterPacket* Packet);

	/**
	 * Getter for the TestRunModel
	 * @see TestRunModel
	 */
	TestRunModel* GetTestRunModel();

	/**
	 * Getter for the path of the GTest executable
	 */
	std::string GetGTestPath();

	/**
	 * Getter for the test working directory
	 */
	std::string GetWorkingDir();

	/**
	 * Getter for the state factory
	 */
	ICoreStateFactory* GetStateFactory();

	/**
	 * Getter for the persistence flag that is supposed to be forwarded to the GTest process
	 */
	bool GetUsePersiFeaturesFlag();

	/**
	 * HandleTask is a delegate method to the actual handler function of the current state.
	 * @see BaseTask
	 * @see GoF state pattern
	 */
	int HandleTask(BaseTask* Task);

	/**
	 * Run() will run the GTestServiceCore state machine synchronously.
	 */
	int Run();

	/**
	 * RunThreaded() will run the GTestServiceCore state machine asynchronously (in a separate thread).
	 */
	void RunThreaded();

	/**
	 * Stop() sets a flag that signals the main loop to exit.
	 */
	void Stop();

	/**
	 * Getter for the connection manager instance
	 * @see ConnectionManager
	 */
	ConnectionManager* GetConnectionManager();

	~GTestServiceCore();
};

#endif /* GTESTSERVICECORE_H_ */
