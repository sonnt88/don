/*
 * ICoreState.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef ICORESTATE_H_
#define ICORESTATE_H_

#include <stdexcept>

#include <Core/Tasks/SendVersionTask.h>
#include <Core/Tasks/SendCurrentStateTask.h>
#include <Core/Tasks/SendTestListTask.h>
#include <Core/Tasks/ExecTestsTask.h>
#include <Core/Tasks/SendResultPacketTask.h>
#include <Core/Tasks/SendResetPacketTask.h>
#include <Core/Tasks/SendTestsDonePacketTask.h>
#include <Core/Tasks/SendAllResultsTask.h>
#include <Core/Tasks/SendTestOutputPacketTask.h>
#include <Core/Tasks/ReactOnCrashTask.h>
#include <Core/Tasks/AbortTestsTask.h>
#include <Core/Tasks/QuitTask.h>

#include <version.h>

enum GTEST_SERVICE_STATE {
	STATE_IDLE = 0,
	STATE_RUNNING_TESTS = 1,
	STATE_INVALID = 2,
	STATE_RESET = 3
};

/**
 * \brief base class for each state of the GTestServiceCore state machine
 */
class ICoreState {
protected:
	/**
	 * \brief send an acknowledgment packet for a given packet
	 * @param Task pointer to the BaseReplyTask object holding the request serial and the ConnectionID
	 * @param PacketID packet id to acknowledge
	 * @param Success if success is true the packet will be an ACK. Otherwise it will be a NACK
	 */
	void SendAck(BaseReplyTask* Task, int PacketID, bool Success)
	{
		char success = (Success) ? 1 : 0;

		AdapterPacket packet((char)PacketID,
			0, 0,
			Task->GetRequestSerial(),
			sizeof(char),
			&success,
			Task->GetConnection()->GetID());

		Task->GetConnection()->Send(packet);
	}

	/**
	 * \brief send the current state information to a client
	 * @param Task pointer to the SendCurrentStateTask object holding the request serial and the ConnectionID
	 */
	virtual void SendCurrentState(SendCurrentStateTask* Task)
	{
		char currentState = this->GetTypeOfState();
		int payloadSize = sizeof(char);

		AdapterPacket packet(
			(char)ID_UP_STATE,
			0, 0,
			Task->GetRequestSerial(),
			payloadSize,
			&currentState,
			Task->GetConnection()->GetID());

		Task->GetConnection()->Send(packet);
	}

public:
	/**
	 * \brief state machine strategy (see GoF) to handle a SendVersionTask
	 * @param Task pointer to the SendVersionTask object holding the request serial and the ConnectionID
	 */
	virtual ICoreState* HandleSendVersionTask(SendVersionTask* Task)
	{
		std::string version = GTEST_SERVICE_VERSION;
		AdapterPacket packet(
			(char)ID_UP_VERSION,
			0, 0,
			Task->GetRequestSerial(),
			version.length() + 1,
			const_cast< char* >(version.c_str()),
			Task->GetConnection()->GetID());

		Task->GetConnection()->Send(packet);

		return this;
	}

	/**
	 * \brief state machine strategy (see GoF) to handle a SendCurrentStateTask
	 */
	virtual ICoreState* HandleSendCurrentStateTask(SendCurrentStateTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a HandleSendTestListTask
	 */
	virtual ICoreState* HandleSendTestListTask(SendTestListTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a ExecTestsTask
	 */
	virtual ICoreState* HandleExecTestsTask(ExecTestsTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a SendResultPacketTask
	 */
	virtual ICoreState* HandleSendResultPacketTask(SendResultPacketTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a SendResetPacketTask
	 */
	virtual ICoreState* HandleSendResetPacketTask(SendResetPacketTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a SendTestsDonePacketTask
	 */
	virtual ICoreState* HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a SendTestsDonePacketTask
	 */
	virtual ICoreState* HandleSendAllResultsTask(SendAllResultsTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a SendTestOutputPacketTask
	 */
	virtual ICoreState* HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a ReactOnCrashTask
	 */
	virtual ICoreState* HandleReactOnCrashTask(ReactOnCrashTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a AbortTestsTask
	 */
	virtual ICoreState* HandleAbortTestsTask(AbortTestsTask* Task) = 0;

	/**
	 * \brief state machine strategy (see GoF) to handle a QuitTask
	 */
	virtual ICoreState* HandleQuitTask(QuitTask* Task) = 0;
	/**
	 * \brief delegate routine to handle specific Tasks
	 */
	virtual ICoreState* HandleTask(BaseTask* Task)
	{
		ICoreState* returnValue = this;
		switch (Task->GetTypeOfTask()) {
		case TASK_SEND_VERSION_TASK:
			returnValue = this->HandleSendVersionTask(dynamic_cast<SendVersionTask*>(Task));
			break;
		case TASK_SEND_CURRENT_STATE:
			returnValue = this->HandleSendCurrentStateTask(dynamic_cast<SendCurrentStateTask*>(Task));
			break;
		case TASK_SEND_TESTLIST:
			returnValue = this->HandleSendTestListTask(dynamic_cast<SendTestListTask*>(Task));
			break;
		case TASK_EXEC_TESTS:
			returnValue = this->HandleExecTestsTask(dynamic_cast<ExecTestsTask*>(Task));
			break;
		case TASK_SEND_RESULT_PACKET:
			returnValue = this->HandleSendResultPacketTask(dynamic_cast<SendResultPacketTask*>(Task));
			break;
		case TASK_SEND_RESET_PACKET:
			returnValue = this->HandleSendResetPacketTask(dynamic_cast<SendResetPacketTask*>(Task));
			break;
		case TASK_SEND_TESTS_DONE_PACKET:
			returnValue = this->HandleSendTestsDonePacketTask(dynamic_cast<SendTestsDonePacketTask*>(Task));
			break;
		case TASK_SEND_ALL_RESULTS:
			returnValue = this->HandleSendAllResultsTask(dynamic_cast<SendAllResultsTask*>(Task));
			break;
		case TASK_SEND_TEST_OUTPUT_PACKET:
			returnValue = this->HandleSendTestOutputPacketTask(dynamic_cast<SendTestOutputPacketTask*>(Task));
			break;
		case TASK_REACT_ON_CRASH:
			returnValue = this->HandleReactOnCrashTask(dynamic_cast<ReactOnCrashTask*>(Task));
			break;
		case TASK_ABORT_TESTS:
			returnValue = this->HandleAbortTestsTask(dynamic_cast<AbortTestsTask*>(Task));
			break;
		case TASK_QUIT:
			returnValue = this->HandleQuitTask(dynamic_cast<QuitTask*>(Task));
			break;
		default:
			throw std::runtime_error("Unexpected type of task: this should never occur.");
		}

		return returnValue;
	}

	virtual GTEST_SERVICE_STATE GetTypeOfState() = 0;

	virtual ~ICoreState() {};
};


#endif /* ICORESTATE_H_ */
