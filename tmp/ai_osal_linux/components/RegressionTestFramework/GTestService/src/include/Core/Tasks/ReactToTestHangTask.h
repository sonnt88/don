/*
 * ReactToTestHangTask.h
 *
 *  Created on: 10 Oct, 2014
 *      Author: aga2hi
 */

#ifndef REACTTOTESTHANGTASK_H_
#define REACTTOTESTHANGTASK_H_

#include <Core/Tasks/BaseTask.h>

/**
 * \brief This task signals that the test process has hung
 */
class ReactToTestHangTask : public BaseTask {
 public:
	/**
	 * \brief constructor
	 */
	ReactToTestHangTask();
};

#endif /* REACTTOTESTHANGTASK_H_ */
