/*
 * ResultPackageTask.h
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#ifndef RESULTPACKAGETASK_H_
#define RESULTPACKAGETASK_H_

#include <Core/Tasks/BaseTask.h>
#include <Models/TestModel.h>
#include <Models/TestStates.h>

/**
 * \brief This task signals the task handler to send out a single test result packet.
 */
class SendResultPacketTask : public BaseTask {
private:
	int iteration;
	TestModel* test;
	TEST_STATE state;
public:
	/**
	 * \brief constructor
	 * @param Test pointer to the data model of the test that has been executed
	 * @param Iteration number of iterations that have already been run for this test (including the current one)
	 * @param TestState the test result, either PASSED or FAILED
	 */

	SendResultPacketTask(TestModel* Test, int Iteration, TEST_STATE TestState);
	/**
	 * \brief getter for the test result of the single test
	 */
	TEST_STATE GetTestState();

	/**
	 * \brief getter for the test data model
	 */
	TestModel* GetTestModel();

	/**
	 * getter for the number of iterations for this test
	 */
	int GetIteration();

	/**
	 * \brief destructor
	 */
	virtual ~SendResultPacketTask() {};
};


#endif /* RESULTPACKAGETASK_H_ */
