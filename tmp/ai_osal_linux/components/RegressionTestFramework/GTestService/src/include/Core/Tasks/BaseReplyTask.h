/*
 * BaseReplyTask.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef BASEREPLYTASK_H_
#define BASEREPLYTASK_H_

#include "BaseTask.h"
#include <stdint.h>
#include <AdapterConnection/Interfaces/IConnection.h>

/**
 * \brief base class for every task that is supposed to send a reply packet to a client
 */
class BaseReplyTask: public BaseTask {
private:
	uint32_t requestSerial;
	IConnection* connection;
public:
	/**
	 * \brief constructor
	 * @param Connection pointer to the connection this task is associated to
	 * @param RequestSerial serial of the corresponding request packet
	 * @param TypeOfTask task type specifier
	 * @see GTEST_CORE_TASKS
	 */
	BaseReplyTask(IConnection* Connection, uint32_t RequestSerial, GTEST_CORE_TASKS TypeOfTask);

	/**
	 * \brief getter for the request serial
	 */
	uint32_t GetRequestSerial();

	/**
	 * \brief getter for the connection pointer
	 */
	IConnection* GetConnection();

	/**
	 * \brief destructor
	 */
	virtual ~BaseReplyTask() {};
};

#endif /* BASEREPLYTASK_H_ */
