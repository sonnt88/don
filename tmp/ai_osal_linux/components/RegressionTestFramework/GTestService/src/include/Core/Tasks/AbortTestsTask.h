/*
 * AbortTestsTask.h
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#ifndef ABORTTESTSTASK_H_
#define ABORTTESTSTASK_H_

#include "BaseReplyTask.h"

/**
 * \brief The execution of this task leads to a hard abort of the current test run. The child process running the tests will simply be killed.
 */
class AbortTestsTask: public BaseReplyTask {
public:
	/**
	 * \brief constructor
	 * @param Connection pointer to connection to send the ACK or NACK to
	 * @param RequestSerial of the corresponding client request packet
	 */
	AbortTestsTask(IConnection* Connection, uint32_t RequestSerial);

	/**
	 * \brief destructor
	 */
	virtual ~AbortTestsTask() {};
};

#endif /* ABORTTESTSTASK_H_ */
