/*
 * ReactOnCrashTask.h
 *
 *  Created on: 25.09.2012
 *      Author: oto4hi
 */

#ifndef REACTONCRASHTASK_H_
#define REACTONCRASHTASK_H_

#include <Core/Tasks/BaseTask.h>

/**
 * \brief this task tells the task handler that the test executable has crashed.
 */
class ReactOnCrashTask : public BaseTask {
public:
	/**
	 * \brief constructor
	 */
	ReactOnCrashTask();

	/**
	 * \brief destructor
	 */
	virtual ~ReactOnCrashTask() {};
};

#endif /* REACTONCRASHTASK_H_ */
