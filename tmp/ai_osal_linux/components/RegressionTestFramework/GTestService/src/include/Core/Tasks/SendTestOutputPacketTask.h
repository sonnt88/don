/*
 * SendTestOutputPacketTask.h
 *
 *  Created on: 23.10.2012
 *      Author: oto4hi
 */

#ifndef SENDTESTOUTPUTPACKETTASK_H_
#define SENDTESTOUTPUTPACKETTASK_H_

#include <Core/Tasks/BaseTask.h>
#include <stdint.h>
#include <limits>
#include <string>

/**
 * possible raw output sources
 */
typedef enum {
	OUTPUT_SOURCE_STDOUT,
	OUTPUT_SOURCE_STDERR
} OUTPUT_SOURCE;

/**
 * definition for no timestamp
 */
#define NO_TIMESTAMP std::numeric_limits<uint32_t>::max()

/**
 * \brief The handler of this task will send out a line of test raw test output from stdout or stderr to every connected client.
 */
class SendTestOutputPacketTask : public BaseTask
{
private:
	OUTPUT_SOURCE outputSource;
	std::string line;
	uint32_t timestamp;
public:
	/**
	 * \brief constructor
	 * @param OutputSource the source of the output line
	 * @param Line raw line to send
	 * @param Timestamp timestamp of the output line or NO_TIMESTAMP for no timestamp
	 */
	SendTestOutputPacketTask(OUTPUT_SOURCE OutputSource, std::string Line, uint32_t Timestamp);

	/**
	 * \brief getter for the output source of the line associated with this task
	 */
	OUTPUT_SOURCE GetOutputSource();

	/**
	 * getter for the actual output line
	 */
	std::string GetLine();

	/**
	 * getter for the timestamp associated with this output line
	 */
	uint32_t GetTimestamp();

	/**
	 * destructor
	 */
	virtual ~SendTestOutputPacketTask();
};

#endif /* SENDTESTOUTPUTPACKETTASK_H_ */
