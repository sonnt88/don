/*
 * CoreStateFactory.h
 *
 *  Created on: 27.02.2013
 *      Author: oto4hi
 */

#ifndef CORESTATEFACTORY_H_
#define CORESTATEFACTORY_H_

#include <Core/Interfaces/ICoreStateFactory.h>

/**
 * \brief factory for plain instances of the classes InvalidState, IdleState and RunningState
 */
class CoreStateFactory: public ICoreStateFactory {
private:
	GTestServiceCore* context;
public:
	CoreStateFactory(GTestServiceCore* Context);
	virtual ICoreState* CreateInitialState();
	virtual ICoreState* CreateInvalidState();
	virtual ICoreState* CreateIdleState();
	virtual ICoreState* CreateResetState(GTestServiceBuffers::TestLaunch const * const);
	virtual ICoreState* CreateRunningState(GTestServiceBuffers::TestLaunch* TestSelection);
	virtual ~CoreStateFactory();
};

#endif /* CORESTATEFACTORY_H_ */
