/*
 * ITaskSource.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef ITASKSOURCE_H_
#define ITASKSOURCE_H_

/**
 * \brief Any class deriving from this base class will be able to enqueue a task to the instance of a class that implements ITaskWorker
 */
class BaseTaskSouce {
protected:
	ITaskWorker* taskWorker;
public:
	/**
	 * constructor
	 * @param TaskWorker the instance of a class deriving from ITaskWorker (e.g. the GTestServiceCore)
	 * @see ITaskWorker
	 * @see GTestServiceCore
	 */
	BaseTaskSouce(ITaskWorker* TaskWorker)
	{
		if (TaskWorker == NULL)
			throw std::invalid_argument("TaskWorker cannot be NULL.");

		this->taskWorker = TaskWorker;
	}
};


#endif /* ITASKSOURCE_H_ */
