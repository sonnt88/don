/*
 * SendTestListTask.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef SENDTESTLISTTASK_H_
#define SENDTESTLISTTASK_H_

#include "BaseReplyTask.h"

/**
 * \brief This task sends out a complete list of all available tests from the test executable
 */
class SendTestListTask : public BaseReplyTask {
public:
	/**
	 * \brief constructor
	 * @param Connection pointer to the connection this task is associated to
	 * @param RequestSerial serial of the corresponding request packet
	 */
	SendTestListTask(IConnection* Connection, uint32_t RequestSerial);

	/**
	 * \brief destructor
	 */
	virtual ~SendTestListTask() {};
};

#endif /* SENDTESTLISTTASK_H_ */
