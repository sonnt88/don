/*
 * ITaskWorker.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef ITASKWORKER_H_
#define ITASKWORKER_H_

#include <Core/Tasks/BaseTask.h>

/**
 * \brief interface for a class that handles Tasks (e.g. the GTestServiceCore)
 */
class ITaskWorker {
public:
	/**
	 * \brief enqueue a single task
	 * @param Task pointer to task instance to enqueue
	 */
	virtual void EnqueueTask(BaseTask* Task) = 0;
	virtual ~ITaskWorker() {};
};


#endif /* ITASKWORKER_H_ */
