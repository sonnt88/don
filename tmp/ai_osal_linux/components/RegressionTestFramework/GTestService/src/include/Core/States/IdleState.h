/*
 * IdleState.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef IDLESTATE_H_
#define IDLESTATE_H_

#include "BaseValidState.h"

/**
 * \brief this is the default valid state. As long as no test is being executed the gtest service will remain in this state
 * @see ICoreState for detailed documentation
 */
class IdleState : public BaseValidState {
public:
	IdleState(GTestServiceCore* Context);
	virtual ICoreState* HandleSendCurrentStateTask(SendCurrentStateTask* Task);
	virtual ICoreState* HandleExecTestsTask(ExecTestsTask* Task);
	virtual ICoreState* HandleSendResultPacketTask(SendResultPacketTask* Task);
	virtual ICoreState* HandleSendResetPacketTask(SendResetPacketTask* Task);
	virtual ICoreState* HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task);
	virtual ICoreState* HandleReactOnCrashTask(ReactOnCrashTask* Task);
	virtual ICoreState* HandleAbortTestsTask(AbortTestsTask* Task);
	virtual ICoreState* HandleQuitTask(QuitTask* Task);
	virtual GTEST_SERVICE_STATE GetTypeOfState();
};

#endif /* IDLESTATE_H_ */
