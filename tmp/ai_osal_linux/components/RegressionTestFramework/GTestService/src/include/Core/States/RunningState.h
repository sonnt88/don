/*
 * RunningState.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef RUNNINGSTATE_H_
#define RUNNINGSTATE_H_

#include "BaseValidState.h"
#include <ProcessController/GTestProcessController.h>
#include <ProcessController/GTestExecOutputHandler.h>
#include <ProcessController/GTestPureTextSendingOutputHandler.h>
#include <Models/GTestConfigurationModel.h>
#include <GTestServiceBuffers.pb.h>

/**
 * \brief this is the state when tests are being executed
 * @see ICoreState for detailed documentation
 */
class RunningState : public BaseValidState {
private:
	GTestProcessController* processController;
	GTestExecOutputHandler* stdoutHandler;

	GTestPureTextSendingOutputHandler* stdoutTextForwarder;
	GTestPureTextSendingOutputHandler* stderrTextForwarder;

	GTestConfigurationModel* configModel;

	GTestConfigurationModel* CreateConfigurationModel(GTestServiceBuffers::TestLaunch* TestSelection);
	void RunProcessController();
	void WaitForProcessControllerToTerminate();
	TestModel* SendResultPacketForCrashedTest(int crashedTestID);
	void SendTestsDonePacket();

	// testRecord will keep track of completed tests.  Should there be a request for a power cycle,
	// we will be able to write an up to date TestSelection.pb file
	GTestServiceBuffers::TestLaunch testRecord;

protected:
	virtual void SendCurrentState(SendCurrentStateTask* Task);

public:
	RunningState(GTestServiceCore* Context, GTestServiceBuffers::TestLaunch* TestSelection);
	virtual ICoreState* HandleSendCurrentStateTask(SendCurrentStateTask* Task);
	virtual ICoreState* HandleExecTestsTask(ExecTestsTask* Task);
	virtual ICoreState* HandleSendResultPacketTask(SendResultPacketTask* Task);
	virtual ICoreState* HandleSendResetPacketTask(SendResetPacketTask* Task);
	virtual ICoreState* HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task);
	virtual ICoreState* HandleReactOnCrashTask(ReactOnCrashTask* Task);
	virtual ICoreState* HandleAbortTestsTask(AbortTestsTask* Task);
	virtual ICoreState* HandleQuitTask(QuitTask* Task);
	virtual GTEST_SERVICE_STATE GetTypeOfState();
	virtual ~RunningState();
};

#endif /* RUNNINGSTATE_H_ */
