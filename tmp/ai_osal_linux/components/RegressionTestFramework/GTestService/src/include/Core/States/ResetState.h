/*
 * ResetState.h
 *
 * (Based on IdleState.h)
 *
 *  Created on: 30.01.2014
 *      Author: aga2hi
 */

#ifndef RESETSTATE_H_
#define RESETSTATE_H_

#include "BaseValidState.h"

/**
 * \brief this is the reset state. Gtest service will remain in this state.  Purpose is to handle reset requests
 * @see ICoreState for detailed documentation
 */
class ResetState : public BaseValidState {
public:
	ResetState(GTestServiceCore* Context);
	virtual ICoreState* HandleSendCurrentStateTask(SendCurrentStateTask* Task);
	virtual ICoreState* HandleExecTestsTask(ExecTestsTask* Task);
	virtual ICoreState* HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task);
	virtual ICoreState* HandleSendResultPacketTask(SendResultPacketTask* Task);
	virtual ICoreState* HandleSendResetPacketTask(SendResetPacketTask* Task);
	virtual ICoreState* HandleSendAllResultsTask(SendAllResultsTask* Task);
	virtual ICoreState* HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task);
	virtual ICoreState* HandleReactOnCrashTask(ReactOnCrashTask* Task);
	virtual ICoreState* HandleAbortTestsTask(AbortTestsTask* Task);
	virtual ICoreState* HandleQuitTask(QuitTask* Task);
	virtual GTEST_SERVICE_STATE GetTypeOfState();
	virtual ICoreState* HandleSendTestListTask(SendTestListTask* Task);
	virtual ICoreState* SendResultPacketForCrashedTest(int crashedTestID);
};

#endif /* RESETSTATE_H_ */
