/*
 * ResetPacketTask.h
 *
 *  Created on: 09 Jan, 2014
 *      Author: aga2hi
 */

#ifndef RESETPACKETTASK_H_
#define RESETPACKETTASK_H_

#include <Core/Tasks/BaseTask.h>
#include <Models/TestModel.h>
#include <Models/TestStates.h>

/**
 * \brief This task signals the task handler to send a reset packet.
 */
class SendResetPacketTask : public BaseTask {
private:
	int iteration;
	TestModel* test;
	TEST_STATE state;
public:
	/**
	 * \brief constructor
	 * @param Test pointer to the data model of the test that has been executed
	 * @param Iteration number of iterations that have already been run for this test (including the current one)
	 * @param TestState the test result, either PASSED or FAILED
	 */

	SendResetPacketTask(TestModel* Test, int Iteration, TEST_STATE TestState);
	/**
	 * \brief getter for the test result of the single test
	 */
	TEST_STATE GetTestState();

	/**
	 * \brief getter for the test data model
	 */
	TestModel* GetTestModel();

	/**
	 * getter for the number of iterations for this test
	 */
	int GetIteration();

	/**
	 * \brief destructor
	 */
	virtual ~SendResetPacketTask() {};
};


#endif /* RESETPACKETTASK_H_ */
