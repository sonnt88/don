/*
 * SendCurrentTaskState.h
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#ifndef SENDCURRENTSTATETASK_H_
#define SENDCURRENTSTATETASK_H_

#include "BaseReplyTask.h"

/**
 * \brief Executing this task the current state of the GTestServiceCore state machine will be sent to the requesting client.
 */
class SendCurrentStateTask : public BaseReplyTask {
public:
	/**
	 * constructor
	 * @param Connection pointer to the corresponding client connection
	 * @param RequestSerial serial of the corresponding request packet
	 */
	SendCurrentStateTask(IConnection* Connection, uint32_t RequestSerial);

	/**
	 * destructor
	 */
	virtual ~SendCurrentStateTask() {};
};

#endif /* SENDCURRENTSTATETASK_H_ */
