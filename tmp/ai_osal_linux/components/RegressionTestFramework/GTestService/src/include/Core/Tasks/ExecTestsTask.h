/*
 * RunTestsTask.h
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#ifndef RUNTESTSTASK_H_
#define RUNTESTSTASK_H_

#include <Core/Tasks/BaseReplyTask.h>
#include <Models/GTestConfigurationModel.h>
#include <GTestServiceBuffers.pb.h>
#include <AdapterConnection/Interfaces/IConnection.h>

/**
 * \brief the execution of this task leads the execution of the GTest executable
 */
class ExecTestsTask: public BaseReplyTask {
private:
	GTestServiceBuffers::TestLaunch* testSelection;
public:
	/**
	 * \brief constructor
	 * @param Connection pointer to the connection object associated with this task
	 * @param RequestSerial serial of the corresponding request packet
	 * @param TestSelection pointer to the Protobuf object specifying which tests to run
	 */
	ExecTestsTask(IConnection* Connection, uint32_t RequestSerial, GTestServiceBuffers::TestLaunch* TestSelection);

	/**
	 * \brief getter for the test selection object
	 */
	GTestServiceBuffers::TestLaunch* GetTestSelectionPB();
	virtual ~ExecTestsTask();
};

#endif /* RUNTESTSTASK_H_ */
