// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GTestServiceArguments.proto

#ifndef PROTOBUF_GTestServiceArguments_2eproto__INCLUDED
#define PROTOBUF_GTestServiceArguments_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2004000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2004001 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_message_reflection.h>
// @@protoc_insertion_point(includes)

namespace GTestServiceArguments {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_GTestServiceArguments_2eproto();
void protobuf_AssignDesc_GTestServiceArguments_2eproto();
void protobuf_ShutdownFile_GTestServiceArguments_2eproto();

class CommandLineArguments;

// ===================================================================

class CommandLineArguments : public ::google::protobuf::Message {
 public:
  CommandLineArguments();
  virtual ~CommandLineArguments();
  
  CommandLineArguments(const CommandLineArguments& from);
  
  inline CommandLineArguments& operator=(const CommandLineArguments& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const CommandLineArguments& default_instance();
  
  void Swap(CommandLineArguments* other);
  
  // implements Message ----------------------------------------------
  
  CommandLineArguments* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const CommandLineArguments& from);
  void MergeFrom(const CommandLineArguments& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  // accessors -------------------------------------------------------
  
  // required string GTestExecutable = 1;
  inline bool has_gtestexecutable() const;
  inline void clear_gtestexecutable();
  static const int kGTestExecutableFieldNumber = 1;
  inline const ::std::string& gtestexecutable() const;
  inline void set_gtestexecutable(const ::std::string& value);
  inline void set_gtestexecutable(const char* value);
  inline void set_gtestexecutable(const char* value, size_t size);
  inline ::std::string* mutable_gtestexecutable();
  inline ::std::string* release_gtestexecutable();
  
  // required string WorkingDir = 2;
  inline bool has_workingdir() const;
  inline void clear_workingdir();
  static const int kWorkingDirFieldNumber = 2;
  inline const ::std::string& workingdir() const;
  inline void set_workingdir(const ::std::string& value);
  inline void set_workingdir(const char* value);
  inline void set_workingdir(const char* value, size_t size);
  inline ::std::string* mutable_workingdir();
  inline ::std::string* release_workingdir();
  
  // required string AnnouncedName = 3;
  inline bool has_announcedname() const;
  inline void clear_announcedname();
  static const int kAnnouncedNameFieldNumber = 3;
  inline const ::std::string& announcedname() const;
  inline void set_announcedname(const ::std::string& value);
  inline void set_announcedname(const char* value);
  inline void set_announcedname(const char* value, size_t size);
  inline ::std::string* mutable_announcedname();
  inline ::std::string* release_announcedname();
  
  // required uint32 TCPServicePort = 4;
  inline bool has_tcpserviceport() const;
  inline void clear_tcpserviceport();
  static const int kTCPServicePortFieldNumber = 4;
  inline ::google::protobuf::uint32 tcpserviceport() const;
  inline void set_tcpserviceport(::google::protobuf::uint32 value);
  
  // optional string IPFilter = 5;
  inline bool has_ipfilter() const;
  inline void clear_ipfilter();
  static const int kIPFilterFieldNumber = 5;
  inline const ::std::string& ipfilter() const;
  inline void set_ipfilter(const ::std::string& value);
  inline void set_ipfilter(const char* value);
  inline void set_ipfilter(const char* value, size_t size);
  inline ::std::string* mutable_ipfilter();
  inline ::std::string* release_ipfilter();
  
  // required bool UsePersiFeatures = 6;
  inline bool has_usepersifeatures() const;
  inline void clear_usepersifeatures();
  static const int kUsePersiFeaturesFieldNumber = 6;
  inline bool usepersifeatures() const;
  inline void set_usepersifeatures(bool value);
  
  // @@protoc_insertion_point(class_scope:GTestServiceArguments.CommandLineArguments)
 private:
  inline void set_has_gtestexecutable();
  inline void clear_has_gtestexecutable();
  inline void set_has_workingdir();
  inline void clear_has_workingdir();
  inline void set_has_announcedname();
  inline void clear_has_announcedname();
  inline void set_has_tcpserviceport();
  inline void clear_has_tcpserviceport();
  inline void set_has_ipfilter();
  inline void clear_has_ipfilter();
  inline void set_has_usepersifeatures();
  inline void clear_has_usepersifeatures();
  
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  
  ::std::string* gtestexecutable_;
  ::std::string* workingdir_;
  ::std::string* announcedname_;
  ::std::string* ipfilter_;
  ::google::protobuf::uint32 tcpserviceport_;
  bool usepersifeatures_;
  
  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(6 + 31) / 32];
  
  friend void  protobuf_AddDesc_GTestServiceArguments_2eproto();
  friend void protobuf_AssignDesc_GTestServiceArguments_2eproto();
  friend void protobuf_ShutdownFile_GTestServiceArguments_2eproto();
  
  void InitAsDefaultInstance();
  static CommandLineArguments* default_instance_;
};
// ===================================================================


// ===================================================================

// CommandLineArguments

// required string GTestExecutable = 1;
inline bool CommandLineArguments::has_gtestexecutable() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void CommandLineArguments::set_has_gtestexecutable() {
  _has_bits_[0] |= 0x00000001u;
}
inline void CommandLineArguments::clear_has_gtestexecutable() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void CommandLineArguments::clear_gtestexecutable() {
  if (gtestexecutable_ != &::google::protobuf::internal::kEmptyString) {
    gtestexecutable_->clear();
  }
  clear_has_gtestexecutable();
}
inline const ::std::string& CommandLineArguments::gtestexecutable() const {
  return *gtestexecutable_;
}
inline void CommandLineArguments::set_gtestexecutable(const ::std::string& value) {
  set_has_gtestexecutable();
  if (gtestexecutable_ == &::google::protobuf::internal::kEmptyString) {
    gtestexecutable_ = new ::std::string;
  }
  gtestexecutable_->assign(value);
}
inline void CommandLineArguments::set_gtestexecutable(const char* value) {
  set_has_gtestexecutable();
  if (gtestexecutable_ == &::google::protobuf::internal::kEmptyString) {
    gtestexecutable_ = new ::std::string;
  }
  gtestexecutable_->assign(value);
}
inline void CommandLineArguments::set_gtestexecutable(const char* value, size_t size) {
  set_has_gtestexecutable();
  if (gtestexecutable_ == &::google::protobuf::internal::kEmptyString) {
    gtestexecutable_ = new ::std::string;
  }
  gtestexecutable_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* CommandLineArguments::mutable_gtestexecutable() {
  set_has_gtestexecutable();
  if (gtestexecutable_ == &::google::protobuf::internal::kEmptyString) {
    gtestexecutable_ = new ::std::string;
  }
  return gtestexecutable_;
}
inline ::std::string* CommandLineArguments::release_gtestexecutable() {
  clear_has_gtestexecutable();
  if (gtestexecutable_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = gtestexecutable_;
    gtestexecutable_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}

// required string WorkingDir = 2;
inline bool CommandLineArguments::has_workingdir() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void CommandLineArguments::set_has_workingdir() {
  _has_bits_[0] |= 0x00000002u;
}
inline void CommandLineArguments::clear_has_workingdir() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void CommandLineArguments::clear_workingdir() {
  if (workingdir_ != &::google::protobuf::internal::kEmptyString) {
    workingdir_->clear();
  }
  clear_has_workingdir();
}
inline const ::std::string& CommandLineArguments::workingdir() const {
  return *workingdir_;
}
inline void CommandLineArguments::set_workingdir(const ::std::string& value) {
  set_has_workingdir();
  if (workingdir_ == &::google::protobuf::internal::kEmptyString) {
    workingdir_ = new ::std::string;
  }
  workingdir_->assign(value);
}
inline void CommandLineArguments::set_workingdir(const char* value) {
  set_has_workingdir();
  if (workingdir_ == &::google::protobuf::internal::kEmptyString) {
    workingdir_ = new ::std::string;
  }
  workingdir_->assign(value);
}
inline void CommandLineArguments::set_workingdir(const char* value, size_t size) {
  set_has_workingdir();
  if (workingdir_ == &::google::protobuf::internal::kEmptyString) {
    workingdir_ = new ::std::string;
  }
  workingdir_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* CommandLineArguments::mutable_workingdir() {
  set_has_workingdir();
  if (workingdir_ == &::google::protobuf::internal::kEmptyString) {
    workingdir_ = new ::std::string;
  }
  return workingdir_;
}
inline ::std::string* CommandLineArguments::release_workingdir() {
  clear_has_workingdir();
  if (workingdir_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = workingdir_;
    workingdir_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}

// required string AnnouncedName = 3;
inline bool CommandLineArguments::has_announcedname() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void CommandLineArguments::set_has_announcedname() {
  _has_bits_[0] |= 0x00000004u;
}
inline void CommandLineArguments::clear_has_announcedname() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void CommandLineArguments::clear_announcedname() {
  if (announcedname_ != &::google::protobuf::internal::kEmptyString) {
    announcedname_->clear();
  }
  clear_has_announcedname();
}
inline const ::std::string& CommandLineArguments::announcedname() const {
  return *announcedname_;
}
inline void CommandLineArguments::set_announcedname(const ::std::string& value) {
  set_has_announcedname();
  if (announcedname_ == &::google::protobuf::internal::kEmptyString) {
    announcedname_ = new ::std::string;
  }
  announcedname_->assign(value);
}
inline void CommandLineArguments::set_announcedname(const char* value) {
  set_has_announcedname();
  if (announcedname_ == &::google::protobuf::internal::kEmptyString) {
    announcedname_ = new ::std::string;
  }
  announcedname_->assign(value);
}
inline void CommandLineArguments::set_announcedname(const char* value, size_t size) {
  set_has_announcedname();
  if (announcedname_ == &::google::protobuf::internal::kEmptyString) {
    announcedname_ = new ::std::string;
  }
  announcedname_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* CommandLineArguments::mutable_announcedname() {
  set_has_announcedname();
  if (announcedname_ == &::google::protobuf::internal::kEmptyString) {
    announcedname_ = new ::std::string;
  }
  return announcedname_;
}
inline ::std::string* CommandLineArguments::release_announcedname() {
  clear_has_announcedname();
  if (announcedname_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = announcedname_;
    announcedname_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}

// required uint32 TCPServicePort = 4;
inline bool CommandLineArguments::has_tcpserviceport() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void CommandLineArguments::set_has_tcpserviceport() {
  _has_bits_[0] |= 0x00000008u;
}
inline void CommandLineArguments::clear_has_tcpserviceport() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void CommandLineArguments::clear_tcpserviceport() {
  tcpserviceport_ = 0u;
  clear_has_tcpserviceport();
}
inline ::google::protobuf::uint32 CommandLineArguments::tcpserviceport() const {
  return tcpserviceport_;
}
inline void CommandLineArguments::set_tcpserviceport(::google::protobuf::uint32 value) {
  set_has_tcpserviceport();
  tcpserviceport_ = value;
}

// optional string IPFilter = 5;
inline bool CommandLineArguments::has_ipfilter() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void CommandLineArguments::set_has_ipfilter() {
  _has_bits_[0] |= 0x00000010u;
}
inline void CommandLineArguments::clear_has_ipfilter() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void CommandLineArguments::clear_ipfilter() {
  if (ipfilter_ != &::google::protobuf::internal::kEmptyString) {
    ipfilter_->clear();
  }
  clear_has_ipfilter();
}
inline const ::std::string& CommandLineArguments::ipfilter() const {
  return *ipfilter_;
}
inline void CommandLineArguments::set_ipfilter(const ::std::string& value) {
  set_has_ipfilter();
  if (ipfilter_ == &::google::protobuf::internal::kEmptyString) {
    ipfilter_ = new ::std::string;
  }
  ipfilter_->assign(value);
}
inline void CommandLineArguments::set_ipfilter(const char* value) {
  set_has_ipfilter();
  if (ipfilter_ == &::google::protobuf::internal::kEmptyString) {
    ipfilter_ = new ::std::string;
  }
  ipfilter_->assign(value);
}
inline void CommandLineArguments::set_ipfilter(const char* value, size_t size) {
  set_has_ipfilter();
  if (ipfilter_ == &::google::protobuf::internal::kEmptyString) {
    ipfilter_ = new ::std::string;
  }
  ipfilter_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* CommandLineArguments::mutable_ipfilter() {
  set_has_ipfilter();
  if (ipfilter_ == &::google::protobuf::internal::kEmptyString) {
    ipfilter_ = new ::std::string;
  }
  return ipfilter_;
}
inline ::std::string* CommandLineArguments::release_ipfilter() {
  clear_has_ipfilter();
  if (ipfilter_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = ipfilter_;
    ipfilter_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}

// required bool UsePersiFeatures = 6;
inline bool CommandLineArguments::has_usepersifeatures() const {
  return (_has_bits_[0] & 0x00000020u) != 0;
}
inline void CommandLineArguments::set_has_usepersifeatures() {
  _has_bits_[0] |= 0x00000020u;
}
inline void CommandLineArguments::clear_has_usepersifeatures() {
  _has_bits_[0] &= ~0x00000020u;
}
inline void CommandLineArguments::clear_usepersifeatures() {
  usepersifeatures_ = false;
  clear_has_usepersifeatures();
}
inline bool CommandLineArguments::usepersifeatures() const {
  return usepersifeatures_;
}
inline void CommandLineArguments::set_usepersifeatures(bool value) {
  set_has_usepersifeatures();
  usepersifeatures_ = value;
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace GTestServiceArguments

#ifndef SWIG
namespace google {
namespace protobuf {


}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_GTestServiceArguments_2eproto__INCLUDED
