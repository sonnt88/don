/* 
 * File:   gtest_service.cpp
 * Author: oto4hi
 *
 * Created on April 19, 2012, 2:03 PM
 */

/**
 * \mainpage GTestService
 *
 * \section intro Introduction
 *
 * The GTestService software belongs to the Gen3 regression test framework.
 * The purpose of this framework is to make component testing as comfortable as
 * possible and yet to provide a very powerful framework. Since Googletest should
 * be used for unit testing in the scope of Gen3 projects also this regression
 * test framework is based on it. Thus being familiar with Googletest a developer
 * will intuitively get along with the regression test framework.
 *
 * The regression test framework mainly consists of four components:
 *     - the GTestStartService
 *     - the GTestService
 *     - the GTestAdapter
 *     - the GTestUI
 *
 * While the GTestStartService and the GTestService are runnable on both, target and host
 * system, the GTestAdapter and the GTestUI are supposed to only run on the host machine.
 *
 * \subsection components All Components Explained
 * \subsubsection The GTestService
 * The GTestService controls the execution of a Googletest based test binary. A
 * Google Protocol Buffers based protocol is used to configure the GTestService from
 * the host machine to execute tests. Everything that is configurable from the
 * command line when using the plain Googletest is also configurable via that protocol.
 * This includes
 *     - selecting tests
 *     - repeating tests
 *     - shuffling tests
 *     - setting a random seed when shuffling tests
 *     - enabling/disabling the persistence feature of the extended Googletest version (PersiGTest)
 * Once the GTestService is configured it runs independently from the host machine as long
 * as the number of test results does not exceed the maximum limit. Also the GTestService
 * provides an abstraction layer hiding the execution environment for tests (target/host)
 * from all upper layers of the regression test framework.
 *
 * \subsubsection gtestadapter The GTestAdapter
 * The GTestAdapter is a small software completely written in C# providing logging functionality
 * and scripting functionality for an automated run of regression tests. It acts as a proxy
 * for the GTestUI
 *
 * \subsubsection gtestui The GTestUI
 * As the name suggests the GTestUI is a graphical user interface for the regression test framework
 * It visualizes all test cases and tests and their results. The GTestUI can be seen as an optional feature
 * of the regression test framework.
 *
 * \subsubsection gteststartservice GTestStartService
 * The GTestStartService is a very small component that is responsible for restarting the GTestService
 * in case of a target crash. Therefore it uses persistent test execution information that is written when
 * starting a test run and deleted on a regular termination of the test process.
 *
 * \subsection simpleusecase A sequence diagram for a simple use case
 *
 * The following sequence diagram roughly visualizes the role of each component.
 * All function names here have nothing to do with the actual implementation.
 *
\msc
  GTestUI,GTestAdapter,GTestService,GTestExecutable;
  GTestService -> GTestExecutable [label="GenerateTestList"];
  GTestUI      -> GTestAdapter    [label="GetTestList"];
  GTestAdapter -> GTestService    [label="GetTestList"];
  GTestService -> GTestAdapter    [label="SendTestList"];
  GTestAdapter -> GTestUI         [label="SendTestList"];
  GTestService => GTestService    [label="SelectTests"];
  GTestService -> GTestAdapter    [label="ExecuteTests"];
  GTestAdapter -> GTestService    [label="ExecuteTests"];
  GTestService -> GTestExecutable [label="Run"];
  ---                             [label="repeat for every test result"];
  GTestExecutable -> GTestService [label="TestResultOutput"];
  GTestService -> GTestAdapter    [label="SendTestResult"];
  GTestAdapter => GTestAdapter    [label="ResultToLogfile"];
  GTestAdapter -> GTestUI         [label="SendTestResult"];
  GTestUI => GTestUI              [label="VisualizeTestResult"];
  ---                             [label="end loop"];
  GTestService -> GTestAdapter    [label="AllTestsDone"];
  GTestAdapter -> GTestUI         [label="AllTestsDone"];
\endmsc
 */

#include <Core/GTestServiceCore.h>
#include <Utils/ServiceAnnouncer.h>
#include <GTestServiceBuffers.pb.h>
#include <GTestServiceArguments.pb.h>
#include <iostream>
#include <signal.h>
#include <getopt.h>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <fstream>
#include <unistd.h>

#define DEFAULT_SERVICE_PORT 6789
#define PERSISTENT_CMD_LINE "GTestServiceCmdArgs.pb"

typedef struct {
	std::string ExecutablePath;
	std::string WorkingDir;
	std::string IPFilterString;
	int TCPServicePort;
	int UDPAnnouncePort;
	std::string HumanReadableName;
	bool UsePersiFeatures;
	bool Resume;
} GTestServiceStartupParams;

GTestServiceCore* core;

void SIGINThandler(int Signal)
{
    core->Stop();
}

void printhelp(char* appName)
{

	std::cout << "Usage: " << appName << " <options>\n";
	std::cout << "options:\n";
	std::cout << "\n";
	std::string htableLine = "+----------------------------------------+-----------+---------------------------+\n";

	std::cout << htableLine;
	std::cout << "| -h|--help                              | optional  |           print this help |\n";
	std::cout << htableLine;
	std::cout << "| -t|--test_executable=<test executable> | mandatory |   GTest executable to run |\n";
	std::cout << "|                                        |           |                tests from |\n";
	std::cout << htableLine;
	std::cout << "| -d|--working_dir=<working directory>   | optional  |     working directory for |\n";
	std::cout << "|                                        |           |                  test run |\n";
	std::cout << htableLine;
	std::cout << "| -p|--port=<port number>                | optional  |              default=" << DEFAULT_SERVICE_PORT << " |\n";
	std::cout << htableLine;
	std::cout << "| -f|--persi_feature                     | optional  | if set GTest will use the |\n";
	std::cout << "|                                        |           |       persistence feature |\n";
	std::cout << htableLine;
	std::cout << "| -n|--name=<name>                       | optional  |    custom name in service |\n";
	std::cout << "|                                        |           |              announcement |\n";
	std::cout << htableLine;
	std::cout << "| -i|--ipfilter=<ipfilter string>        | optional  | specify which connections |\n";
	std::cout << "|               e.g. \"192.168.1.0/24\"    |           |                 to accept |\n";
	std::cout << "|             or                         |           |                           |\n";
	std::cout << "|                    \"192.168.1.10\"      |           |                           |\n";
	std::cout << htableLine;
	std::cout << "| -r|--resume                            | optional  |      resume previous run, |\n";
	std::cout << "|                                        |           |   cannot be used together |\n";
	std::cout << "|                                        |           |     with any other option |\n";
	std::cout << htableLine;
}

GTestServiceStartupParams GetStartupParams(int argc, char** argv)
{
	GTestServiceStartupParams startupParams;
	startupParams.TCPServicePort = -1;
	startupParams.UsePersiFeatures = false;
	startupParams.Resume = false;

	int c;

	opterr = 0;
	std::stringstream sstream;

	int longIndex=0;
	static const char* optString = "t:p:n:fhi:rd:";
	static const struct option longOpts[] = {
	    { "test_executable", required_argument, NULL, 't' },
	    { "working_dir", required_argument, NULL, 'd' },
	    { "port", required_argument, NULL, 'p' },
	    { "name", required_argument, NULL, 'n' },
	    { "persi_feature", no_argument, NULL, 'f' },
	    { "ipfilter", required_argument, NULL, 'i' },
	    { "resume", no_argument, NULL, 'r' },
	    { "help", no_argument, NULL, 'h' },
	    { NULL, no_argument, NULL, 0 }
	};

	while ((c = getopt_long (argc, argv, optString, longOpts, &longIndex)) != -1)
	{
		switch (c)
		{
		case 't':
			if (!startupParams.ExecutablePath.empty())
			{
				std::cerr << "GTest executable cannot be specified twice.\n";
				exit(1);
			}
			startupParams.ExecutablePath = optarg;
			break;
		case 'd':
			if (!startupParams.WorkingDir.empty())
			{
				std::cerr << "Working directory cannot be specified twice.\n";
				exit(1);
			}
			startupParams.WorkingDir = optarg;
			break;
			break;
		case 'p':
			if (startupParams.TCPServicePort != -1)
				throw std::runtime_error("port has already been specified");
			sstream.clear();
			sstream << optarg;
			sstream >> startupParams.TCPServicePort;
			break;
		case 'f':
			startupParams.UsePersiFeatures = true;
			break;
		case 'h':
			printhelp(argv[0]);
			exit(0);
		case 'n':
			startupParams.HumanReadableName = optarg;
			break;
		case 'i':
			if (!startupParams.IPFilterString.empty())
			{
				std::cerr << "GTest IPFilter cannot be specified twice.\n";
				exit(1);
			}
			startupParams.IPFilterString = optarg;
			break;
		case 'r':
			startupParams.Resume = true;
			break;
		default:
			fprintf(stderr, "unknown command line option: \"-%c\"\n", optopt);
			printhelp(argv[0]);
			exit(1);
		}
	}

	return startupParams;
}

void ResumeFromFile(GTestServiceStartupParams& StartupParams, char* ProgramName)
{
	if (!StartupParams.ExecutablePath.empty() ||
	    !StartupParams.WorkingDir.empty() ||
		!StartupParams.IPFilterString.empty() ||
		 StartupParams.UsePersiFeatures ||
		 StartupParams.TCPServicePort != -1 ||
		!StartupParams.HumanReadableName.empty() )
	{
		std::cerr << "Error: resume (-r) cannot be used in combination with any other option.";
		printhelp(ProgramName);
		exit(1);
	}

	GTestServiceArguments::CommandLineArguments commandLineArgs;
	std::fstream cmdArgumentsIn(PERSISTENT_CMD_LINE, std::ios::in | std::ios::binary);
	if (!cmdArgumentsIn.good())
	{
		std::cerr << "Error: " << PERSISTENT_CMD_LINE << " does not exist.\n";
		exit(1);
	}
	if (!commandLineArgs.ParseFromIstream(&cmdArgumentsIn)) {
	  std::cerr << "Failed to parse " << PERSISTENT_CMD_LINE << ".\n";
	  exit(1);
	}

	StartupParams.ExecutablePath = commandLineArgs.gtestexecutable();
	StartupParams.WorkingDir = commandLineArgs.workingdir();
	StartupParams.TCPServicePort = commandLineArgs.tcpserviceport();
	StartupParams.UsePersiFeatures = commandLineArgs.usepersifeatures();
	StartupParams.HumanReadableName = commandLineArgs.announcedname();
	if (commandLineArgs.has_ipfilter())
		StartupParams.IPFilterString = commandLineArgs.ipfilter();

	if (!StartupParams.UsePersiFeatures)
	{
		std::cerr << "The persistence feature has not been used with the test run you are trying to resume.\n";
		std::cerr << "This test run is not resumable.\n";
		exit(1);
	}
}

bool ValidateStartupParams(GTestServiceStartupParams StartupParams)
{
	if (StartupParams.ExecutablePath.empty())
	{
		std::cerr << "GTest executable not specified (option: -t <gtest executable>)\n";
		return false;
	}

	if (StartupParams.TCPServicePort < 1 or StartupParams.TCPServicePort > 65535)
	{
		std::cerr << "port number is invalid (option: -p <port>)\n";
		return false;
	}

	return true;
}

void PersistStartupParams(GTestServiceStartupParams StartupParams)
{
	GTestServiceArguments::CommandLineArguments commandLineArgs;
	commandLineArgs.set_gtestexecutable(StartupParams.ExecutablePath);
	commandLineArgs.set_workingdir(StartupParams.WorkingDir);
	commandLineArgs.set_tcpserviceport(StartupParams.TCPServicePort);
	commandLineArgs.set_usepersifeatures(StartupParams.UsePersiFeatures);
	commandLineArgs.set_announcedname(StartupParams.HumanReadableName);

	if (!StartupParams.IPFilterString.empty())
		commandLineArgs.set_ipfilter(StartupParams.IPFilterString);

	std::fstream cmdArgumentsOut(PERSISTENT_CMD_LINE, std::ios::out | std::ios::binary | std::ios::trunc);
	if (!commandLineArgs.SerializeToOstream(&cmdArgumentsOut))
		std::cout << "saving arguments failed.\n";
	cmdArgumentsOut.close();
}

int main(int argc, char** argv)
{
	GTestServiceStartupParams startupParams = GetStartupParams(argc, argv);

	if (startupParams.Resume)
	{
		ResumeFromFile(startupParams, argv[0]);
	}
	else
	{
		char hostname[20];
		memset(hostname,0,20);
		gethostname(hostname, 20);

		if (startupParams.HumanReadableName.empty())
			startupParams.HumanReadableName = hostname;

		if(startupParams.TCPServicePort == -1)
			startupParams.TCPServicePort = DEFAULT_SERVICE_PORT;

		if (!ValidateStartupParams(startupParams))
		{
			printhelp(argv[0]);
			exit(1);
		}
	}    
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    std::cout << "GTestService running\n";

    signal(SIGINT, SIGINThandler);

    ServiceAnnouncer announcer = ServiceAnnouncer(
    		startupParams.TCPServicePort,
    		DEFAULT_ANNOUNCE_UDP_PORT,
    		startupParams.HumanReadableName);
    announcer.Run();

    IPFilter* IpFilter = NULL;

    if (!startupParams.IPFilterString.empty())
    {
  		try {
  			IpFilter = new IPFilter(startupParams.IPFilterString);
  		} catch (std::invalid_argument &e)
  		{
  			startupParams.IPFilterString = "";
  			std::cerr << e.what();
  		}
    }

    core = new GTestServiceCore(
    		startupParams.ExecutablePath,
    		startupParams.WorkingDir,
    		startupParams.TCPServicePort,
    		startupParams.UsePersiFeatures,
    		IpFilter);

    PersistStartupParams(startupParams);

    int returnValue = core->Run();
    delete core;

    announcer.Stop();
    
    std::cout << "\n... bye bye!\n";
    
    google::protobuf::ShutdownProtobufLibrary();

    remove(PERSISTENT_CMD_LINE);

    if (IpFilter)
    	delete IpFilter;

    return returnValue;
}

