/* 
 * File:   GTestListOutputHandler.cpp
 * Author: oto4hi
 * 
 * Created on May 8, 2012, 7:19 PM
 */

#include <ProcessController/GTestListOutputHandler.h>

GTestListOutputHandler::GTestListOutputHandler() : GTestQueuedOutputHandler() {
}

TestRunModel* GTestListOutputHandler::generateTestRunModel()
{
	if (this->lineQueue.Empty())
		throw std::runtime_error("Either the test run model has already been generated or there are no input lines.");

	TestRunModel* testRunModel = new TestRunModel();
    int testIDCounter = 0;
    
    while(!this->lineQueue.Empty())
    {
        std::string* line = this->lineQueue.PopFirst();
        std::string name = *line;
        
        delete line;

        name = name.erase(0, name.find_first_not_of(" \n\r\t"));
        name = name.erase(name.find_last_not_of(" \n\r\t")+1);
        
        if (name.length() == 0)
            continue;
        
        if (!name.compare(name.length()-1, 1, "."))
        {
            name = name.erase(name.find_last_not_of(".")+1);
            testRunModel->AddTestCase(new TestCaseModel(name));
        }
        else
        {   
            if (testRunModel->GetTestCaseCount() == 0)
                throw std::logic_error("There is no test case to add a test to.");
            
            TestCaseModel* testCase = testRunModel->GetTestCase(testRunModel->GetTestCaseCount() - 1);
            TestModel* test = new TestModel(name,testIDCounter);
            testCase->AddTest(test);

            if (testCase->IsDefaultDisabled())
            	test->Enabled = false;

            testIDCounter++;
        }
    }

    return testRunModel;
}

//generate the test run model; the caller will be responsible to delete the result
TestRunModel* GTestListOutputHandler::GetTestRunModel()
{
    this->WaitForOutputCompleted();
    return this->generateTestRunModel();
}
