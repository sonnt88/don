/* 
 * File:   GTestExecOutputHandler.cpp
 * Author: oto4hi
 * 
 * Created on May 9, 2012, 5:29 PM
 */

#include <stdexcept>
#include <algorithm>
#include <sstream>

#include <ProcessController/GTestExecOutputHandler.h>

#include <Utils/dbg_msg.h>

const std::string GTestExecOutputHandler::runString    = "[ RUN      ]";
const std::string GTestExecOutputHandler::passedString = "[       OK ]";
const std::string GTestExecOutputHandler::failedString = "[  FAILED  ]";
const std::string GTestExecOutputHandler::resetString  = "[  RESET   ]";

const std::string GTestExecOutputHandler::nextIterationStringHead = "Repeating all tests (iteration ";
const std::string GTestExecOutputHandler::nextIterationStringTail = ") . . .";

GTestExecOutputHandler::GTestExecOutputHandler(TestRunModel* TestRun) : GTestQueuedOutputHandler()
{
    if (!TestRun)
        throw std::invalid_argument("TestRun cannot be NULL.");
    
    this->testRunModel = TestRun;
    pthread_mutex_init(&(this->mutex), NULL);
    this->running = false;
    this->processFailure = false;
    this->resetRequested = false;
}

GTestExecOutputHandler::~GTestExecOutputHandler() 
{
	pthread_mutex_destroy(&(this->mutex));
}

void GTestExecOutputHandler::OnLineReceived(std::string* Line)
{
    GTestQueuedOutputHandler::OnLineReceived(Line);
}

void GTestExecOutputHandler::OnProcessTerminated(int status)
{
  if (status != 0 && status != 256) {//if something other than passed or failed happend -> e.g. a crash
    DBG_MSG( "setting this->processFailure to true" );
    this->processFailure = true;
  }

    GTestQueuedOutputHandler::OnProcessTerminated(status);
    this->lineQueue.Unblock();
}

std::string GTestExecOutputHandler::extractTestIdentifier(std::string Line, int StartPos)
{
    while(Line[StartPos] == ' ' || Line[StartPos] == '\r' || Line[StartPos] == '\t')
    {
        StartPos++;
        if (StartPos >= Line.size())
            throw std::invalid_argument("The line argument does not seem to contain a test case or test name.");
    }
    
    std::string subStr = Line.erase(0, StartPos);
    size_t posOfWhiteSpace = subStr.find_first_of(" \n\r\t");
    
    if (posOfWhiteSpace != std::string::npos)
        subStr = subStr.substr(0, posOfWhiteSpace);
    
    return subStr;
}

std::string GTestExecOutputHandler::extractTestCaseName(std::string Line, int StartPos)
{
    std::string testIdentifier = this->extractTestIdentifier(Line,StartPos);
    size_t posOfSeperator = testIdentifier.find(".");
    
    if (posOfSeperator == std::string::npos)
        throw std::invalid_argument("The line argument does not seem to contain a test case or test name.");
    
    std::string testCaseName = testIdentifier.substr(0,posOfSeperator);
    
    if (testCaseName.empty())
        throw std::invalid_argument("The line argument does not seem to contain a test case name.");
    
    return testCaseName;
}

std::string GTestExecOutputHandler::extractTestName(std::string Line, int StartPos)
{
    std::string testIdentifier = this->extractTestIdentifier(Line,StartPos);
    size_t posOfSeperator = testIdentifier.find(".");
    
    if (posOfSeperator == std::string::npos)
        throw std::invalid_argument("The line argument does not seem to contain a test case or test name.");
    
    if (posOfSeperator == Line.size() - 1)
        throw std::invalid_argument("The line argument does not seem to contain a test name.");
    
    std::string testCaseName = testIdentifier.substr(posOfSeperator + 1, Line.size() - posOfSeperator - 1);
    
    if (testCaseName.empty())
        throw std::invalid_argument("The line argument does not seem to contain a test case name.");
    
    return testCaseName;
}

void GTestExecOutputHandler::setTestState(std::string TestCaseName, std::string TestName, TEST_STATE state)
{
	TestModel* test = testRunModel->GetTestCase(TestCaseName)->GetTest(TestName);
	int iteration = this->testRunModel->GetIteration();

	switch (state)
	{
	case PASSED:
		test->AppendTestResult(iteration, true);
		testRunModel->SetCurrentTestID(-1);
		break;
	case FAILED:
		test->AppendTestResult(iteration, false);
		testRunModel->SetCurrentTestID(-1);
		break;
	case RESET:
		testRunModel->SetCurrentTestID(test->GetID());
		break;
	case RUNNING:
		testRunModel->SetCurrentTestID(test->GetID());
		break;
	default:
		throw std::runtime_error("unexpected test state");
	}

	this->OnNewTestResult(TestCaseName, TestName, state);
}

void GTestExecOutputHandler::setRunning(std::string Line)
{
	if (this->testRunModel->GetIteration() == 0)
			this->testRunModel->NextIteration();

	try {
		std::string testCaseName = extractTestCaseName(Line, this->runString.size());
		std::string testName = extractTestName(Line, this->runString.size());
		this->setTestState(testCaseName, testName, RUNNING);
	} catch ( std::invalid_argument& e ) {}
}

void GTestExecOutputHandler::setPassed(std::string Line)
{
	if (this->testRunModel->GetIteration() == 0)
		this->testRunModel->NextIteration();

    try {
        std::string testCaseName = extractTestCaseName(Line, this->runString.size());
        std::string testName = extractTestName(Line, this->runString.size());
        this->setTestState(testCaseName, testName, PASSED);
    } catch ( std::invalid_argument& e ) {}
}

void GTestExecOutputHandler::setFailed(std::string Line)
{
	if (this->testRunModel->GetIteration() == 0)
			this->testRunModel->NextIteration();

    try {
        std::string testCaseName = extractTestCaseName(Line, this->runString.size());
        std::string testName = extractTestName(Line, this->runString.size());
        this->setTestState(testCaseName, testName, FAILED);
    } catch ( std::invalid_argument& e ) {}
}

void GTestExecOutputHandler::setReset(std::string Line)
{
	if (this->testRunModel->GetIteration() == 0)
			this->testRunModel->NextIteration();

    try {
        std::string testCaseName = extractTestCaseName(Line, this->runString.size());
        std::string testName = extractTestName(Line, this->runString.size());
        this->setTestState(testCaseName, testName, RESET);
    } catch ( std::invalid_argument& e ) {}
}

// void GTestExecOutputHandler::handleResetRequest( void ) {
// }

bool GTestExecOutputHandler::isTestEndLine(std::string Line)
{
	if (Line[Line.length() - 1] == ')')
		return true;
	return false;
}

bool GTestExecOutputHandler::checkForNextIteration(std::string Line, int& iteration)
{
	int headAndTailLength = this->nextIterationStringHead.size() + this->nextIterationStringTail.size();
	//check if line is too short for a potential iteration information
	if (Line.size() <= headAndTailLength)
		return false;

	if (Line.compare(0,this->nextIterationStringHead.size(), this->nextIterationStringHead))
		return false;

	if (Line.compare(Line.size() - this->nextIterationStringTail.size(),
			this->nextIterationStringTail.size(), this->nextIterationStringTail))
		return false;

	std::string iterationAsString = Line.substr(this->nextIterationStringHead.size(),
			Line.size() - headAndTailLength);

	int iter;
	std::stringstream sstr;

	sstr << iterationAsString;
	sstr >> iter;

	if (!sstr.eof())
		return false;

	iteration = iter;

	return true;
}

void GTestExecOutputHandler::processTestEndLine(std::string Line)
{
  // check if the current line indicates a passed test
  if (!Line.compare(0, this->passedString.size(), this->passedString)) {
    this->setPassed(Line);
  }

  //check if the current line indicates a failed test
  else if (!Line.compare(0, this->failedString.size(), this->failedString)) {
    this->setFailed(Line);
  }

  //check if the current line indicates a reset request
  else if (!Line.compare(0, this->resetString.size(), this->resetString)) {
    this->resetRequested = true;
    this->setReset(Line);
  }
}

void GTestExecOutputHandler::processLine(std::string Line)
{
    if (!Line.compare(0, this->runString.size(), this->runString))
    {
        this->setRunning(Line);
        return;
    }

    if (this->isTestEndLine(Line))
    	this->processTestEndLine(Line);
	else
	{
		int iteration = 0;
		if (this->checkForNextIteration(Line, iteration))
		{
			if (this->testRunModel->GetIteration() + 1 <= iteration)
				this->testRunModel->SetIteration(iteration);
			else
			{
				if (this->testRunModel->GetIteration() != iteration)
					throw std::runtime_error("inconsistent iteration information for test run model");
			}
		}
	}
}

void GTestExecOutputHandler::processTestResultLines()
{
	std::string* line;
    while ( !this->ProcessIsTerminated() || !this->lineQueue.Empty() )
    {   
        try
        {
        	line = this->lineQueue.PopFirst();
        } catch (EmptyQueueException& e)
        {
        	continue;
        }

        if (line->length() > 0)
        	if ((*line)[line->length() - 1] == '\n')
        		line->erase(line->length() - 1, 1);

		this->processLine(*line);
		delete line;
    }

    this->OnAllLinesProcessed();
}

void* GTestExecOutputHandler::processTestResultLinesThreadFunc(void* thisPtr)
{
    GTestExecOutputHandler* outputHandler = (GTestExecOutputHandler*)thisPtr;
    outputHandler->processTestResultLines();
    
    pthread_exit(0);
}

void GTestExecOutputHandler::ProcessTestResultLines()
{
    pthread_mutex_lock(&(this->mutex));
    if (this->running)
    {
        pthread_mutex_unlock(&(this->mutex));
        throw std::runtime_error("Already processing test result lines.");
    }
    
    this->running = true;
    
    DBG_MSG("creating thread processTestResultLinesThread");
    int errCode = pthread_create(&(this->processTestResultLinesThread), NULL, this->processTestResultLinesThreadFunc, (void*)this);
    if (errCode)
	{
		std::stringstream sstream;
		sstream << "Error creating thread that processes result output. error code is " << strerror(errCode);
		throw std::runtime_error(sstream.str().c_str());
	}

    pthread_mutex_unlock(&(this->mutex));
}

void GTestExecOutputHandler::WaitForProcessTestResultCompleted()
{
    pthread_mutex_lock(&(this->mutex));
    
    if (!this->running)
    {
        pthread_mutex_unlock(&(this->mutex));
        throw std::runtime_error("Not processing test results.");
    }
    
    DBG_MSG("joining thread processTestResultLinesThread");
    int errCode = pthread_join(this->processTestResultLinesThread, NULL);
    if (errCode)
    {
		std::stringstream sstream;
		sstream << "Joining ReadFromStderrThread failed. error code is " << strerror(errCode);
		throw std::runtime_error(sstream.str().c_str());
	}

    this->running = false;
    
    pthread_mutex_unlock(&(this->mutex));
}
