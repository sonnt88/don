/* 
 * File:   GTestWatchdogHandler.cpp
 * Author: aga2hi
 * 
 * Created on 07 Oct, 2014, 17:00 PM
 */

#include <ProcessController/GTestWatchdogHandler.h>
#include <ProcessController/GTestProcessController.h>
#include <Utils/SignalBlocker.h>

#include <assert.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sstream>
#include <errno.h>
#include <string.h>
#include <semaphore.h>

#include <cstdio>
#include <iostream>
#include <Utils/dbg_msg.h>


// For semaphore initialisation see http://linux.die.net/man/3/sem_init
#define THREAD_SHARED_SEMAPHORE 0
#define SEMAPHORE_INITIAL_VALUE 0


// ////////////////////////////////////////////////////////////////////////////////
// Global (static) variables
// ////////////////////////////////////////////////////////////////////////////////


// Pointer to the client's watchdog handler
// shared between the C and C++ parts of the code
static void * ( * volatile GTestClientHandler )( void ) = NULL;

// Threads: Advanced Programming in UNIX Environment
// Section 11.3, thread identification
static pthread_t       GTestWatchdog_threadid;

// A thread-shared, unnamed semaphore.  For initialisation see
// http://linux.die.net/man/3/sem_init
// For an overview of semaphores see
// http://linux.die.net/man/7/sem_overview
static sem_t GTestWatchdog_Semaphore;

// Flag that indicates whether the loop in the thread should continue
static volatile int GTestWatchdog_LoopActive = 0;

// Sentinel that proves that thread has exited in controlled manner
static int GTestWatchdog_Sentinel = 0;


// ////////////////////////////////////////////////////////////////////////////////
// Global C functions
// ////////////////////////////////////////////////////////////////////////////////


// Signal handler - a simple C function
static void GTestWatchdog_SigHandler( int )
{
	// Reentrant functions, Advanced Programming in UNIX Environment
	// section 10.6, figure 10.4, page 305.
	//
	// Ensure that everything in here is signal-safe.
	//
	// Posting a semaphore is signal-safe, thread_* operations are
	// not.  Therefore, no condition variables or mutices are used
	// here. (What's the plural of mutex?)

	// Semaphores described http://linux.die.net/man/3/sem_post
	const int sem_post_err = sem_post( &GTestWatchdog_Semaphore );
	if( 0 != sem_post_err ) {
		perror( "GTestWatchdog_SigHandler(), sem_post() failed" );
	}  // Endif
}

// Thread function - a simple C function
static void * GTestWatchdog_ThreadFunc( void * )
{
	// Thread creator can kill this thread in two steps: -
	// (1) setting GTestWatchdog_LoopActive to zero
	// and then (2) posting semaphore GTestWatchdog_Semaphore
	do {
		// Wait for the signal handler to post the sempahore
		// See http://linux.die.net/man/3/sem_wait
		const int sem_wait_err = sem_wait( &GTestWatchdog_Semaphore );
		if( 0 != sem_wait_err ) {
			if( EINTR == errno ) {
				// We were interrupted by a signal handler - should we do anything?
			}
			perror( "GTestWatchdog_ThreadFunc(), sem_wait() failed" );
		}  // Endif

		// Call the client's function
		assert( GTestClientHandler );
		( void )GTestClientHandler();
	} while( GTestWatchdog_LoopActive );

	return &GTestWatchdog_Sentinel;
}

static void * GTestWatchdog_VoidFn( void )
{
	return NULL;
}


// ////////////////////////////////////////////////////////////////////////////////
// C++ Class Methods
// ////////////////////////////////////////////////////////////////////////////////


GTestWatchdogHandler::GTestWatchdogHandler( void * ( * const clientHandler_ )( void ), const unsigned int t ) :
	timeout( t )
{
	// Don't allow more than one instantiation of this class
	assert( NULL == ::GTestClientHandler );

	// Store client's watchdog handler - will be called by GTestWatchdog_ThreadFunc when signal is received
	assert( clientHandler_ != NULL );
	::GTestClientHandler = clientHandler_;

	// Initialisation of semaphore, http://linux.die.net/man/3/sem_init
	const int sem_init_err = sem_init( &GTestWatchdog_Semaphore, THREAD_SHARED_SEMAPHORE, SEMAPHORE_INITIAL_VALUE );
	if( 0 != sem_init_err ) {
		perror( "GTestWatchdogHandler(), sem_init() failed" );
		throw std::runtime_error( "GTestWatchdogHandler() unable to initialise semaphore" );
	}  // Endif

	// Before we initialise the thread, activate the loop within it
	GTestWatchdog_LoopActive = 1;

	// Thread creation described section 11.4 Advanced Programming in UNIX Environment, page 357
	void * const thread_arg = NULL;
	const pthread_attr_t * const thread_attr = NULL;
	void * ( * const thread_func )( void * ) = ::GTestWatchdog_ThreadFunc;
	const int thr_err = pthread_create( &GTestWatchdog_threadid, thread_attr, thread_func, thread_arg );
	if( 0 != thr_err ) {
		perror( "GTestWatchdogHandler(), pthread_create() failed" );
		throw std::runtime_error( "GTestWatchdogHandler() unable to create thread" );
	}  // Endif

	// Signal registration is described in Advanced Programming in the UNIX Environment,
	// section 10.14 page 324
	struct sigaction act;
	act.sa_handler = ::GTestWatchdog_SigHandler;
	act.sa_flags = 0;
	sigemptyset(&(act.sa_mask));
	const int sig_err = sigaction( SIGALRM, &act, &this->origSignalAction );
	if ( sig_err < 0 ) {
		perror( "GTestWatchdogHandler(), sigaction() failed" );
		throw std::runtime_error("GTestWatchdogHandler() unable to register SIGALRM handler");
	}

	// Start the timer - note that there is a race condition here, but I know of no solution
	// that works.  See Advanced Programming in UNIX Environment, figure 10.11 page 318.
	// If a solution is found then of course it goes here, which is why Set() ought to be
	// called in the constructor and not left to the caller.
	this->Set();
}

GTestWatchdogHandler::~GTestWatchdogHandler()
{
	// Cancel the timer and deregister the ALRM signal handler before we do anything else
	this->Cancel();
	const int sigact_err = sigaction( SIGALRM, &this->origSignalAction, NULL );
	if( 0 != sigact_err ) {
		perror( "~GTestWatchdogHandler(), sigaction() failed" );
	}  // Endif

	// Get the thread out of its loop so that we can clear its resources up.
	// As the alarm (timer) has been cancelled and the signal handler deregistered
	// there will be no race conditions when we alter these global variables.
	// posting a semaphore see http://linux.die.net/man/3/sem_post
	::GTestClientHandler = GTestWatchdog_VoidFn;
	::GTestWatchdog_LoopActive = 0;
	const int sem_post_err = sem_post( &GTestWatchdog_Semaphore );
	if( 0 != sem_post_err ) {
		perror( "~GTestWatchdogHandler(), sem_post() failed" );
	}  // Endif

	// Now that the thread has completed and is no longer waiting for the semaphore to clear
	// we can join the watchdog thread with this one.
	// Thread termination described section 11.5 APUE page 360
	void * thread_retval;
	const int thread_err = pthread_join( GTestWatchdog_threadid, &thread_retval );
	if( 0 != thread_err ) {
		perror( "~GTestWatchdogHandler(), pthread_join() failed" );
	}   // Endif
	assert( &::GTestWatchdog_Sentinel == thread_retval );  // Check that thread had not finished mysteriously

	// Destruction of semaphore, http://linux.die.net/man/3/sem_destroy
	// Note to maintainer: make sure that nothing else is blocked by
	// sem_wait() because you get undefined behaviour otherwise
	const int sem_dest_err = sem_destroy( &GTestWatchdog_Semaphore );
	if( 0 != sem_dest_err ) {
		perror( "~GTestWatchdogHandler(), sem_destroy() failed" );
	}  // Endif

	// Clear our static variables
	::GTestClientHandler = NULL;
}

void GTestWatchdogHandler::Set() const
{
	assert( ::GTestClientHandler != NULL );

	// See Advanced Programming in the UNIX Environment,
	// section 10.10, page 313
	const unsigned int secondsRemaining = alarm( this->timeout );
}

void GTestWatchdogHandler::Cancel() const
{
	// See Advanced Programming in the UNIX Environment,
	// section 10.10, page 313
	const unsigned int secondsRemaining = alarm( 0 );
}
