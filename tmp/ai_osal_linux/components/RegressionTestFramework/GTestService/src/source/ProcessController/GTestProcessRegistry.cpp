/* 
 * File:   GTestProcessRegistry.cpp
 * Author: oto4hi
 * 
 * Created on May 8, 2012, 10:08 AM
 */

#include <ProcessController/GTestProcessRegistry.h>

GTestProcessRegistry GTestProcessRegistry::instance;

GTestProcessRegistry::GTestProcessRegistry() {
	pthread_mutex_init(&(this->mutex), NULL);
}

GTestProcessRegistry::~GTestProcessRegistry() {
    this->processControllerMap.clear();
    pthread_mutex_destroy(&(this->mutex));
}

GTestProcessRegistry* GTestProcessRegistry::GetGTestProcessRegistry()
{
    return &(GTestProcessRegistry::instance);
}

pid_t GTestProcessRegistry::internalGetChildpid(GTestProcessController* ProcessController)
{
    for (std::map<int, GTestProcessController* >::iterator it = this->processControllerMap.begin(); 
            it !=  this->processControllerMap.end(); ++it)
    {
        if (this->processControllerMap[it->first] == ProcessController)
            return it->first;
    }
    
    return -1;
}

pid_t GTestProcessRegistry::GetChildpid(GTestProcessController* ProcessController)
{
	pid_t pid;
	pthread_mutex_lock(&(this->mutex));
	pid = this->internalGetChildpid(ProcessController);
	pthread_mutex_unlock(&(this->mutex));
	return pid;
}

void GTestProcessRegistry::RegisterGTestProcess(pid_t Childpid, GTestProcessController* ProcessController)
{
	pthread_mutex_lock(&(this->mutex));

    if (this->processControllerMap.find( Childpid ) != this->processControllerMap.end())
    {
    	pthread_mutex_unlock(&(this->mutex));
        throw std::runtime_error("An entry with this pid already exists.");
    }
    
    if (this->internalGetChildpid(ProcessController) >= 0)
    {
    	pthread_mutex_unlock(&(this->mutex));
    	throw std::runtime_error("This process controller is already registered.");
    }
    
    this->processControllerMap.insert(std::pair<pid_t,GTestProcessController*>(Childpid,ProcessController));

    pthread_mutex_unlock(&(this->mutex));
}

void GTestProcessRegistry::UnregisterGTestProcess(pid_t Childpid)
{
	pthread_mutex_lock(&(this->mutex));

    if (this->processControllerMap.find( Childpid ) == this->processControllerMap.end())
    {
    	pthread_mutex_unlock(&(this->mutex));
        throw std::runtime_error("Could not find any process controller for this pid.");
    }
    
    this->processControllerMap.erase(Childpid);
    pthread_mutex_unlock(&(this->mutex));
}

GTestProcessController* GTestProcessRegistry::GetGTestProcessController(pid_t Childpid)
{
	pthread_mutex_lock(&(this->mutex));

    if (this->processControllerMap.find( Childpid ) == this->processControllerMap.end())
    {
    	pthread_mutex_unlock(&(this->mutex));
        throw std::runtime_error("Could not find any process controller for this pid.");
    }

    GTestProcessController* controller = this->processControllerMap[Childpid];
    pthread_mutex_unlock(&(this->mutex));
    
    return controller;
}
