/* 
 * File:   GTestBaseOutputHandler.cpp
 * Author: oto4hi
 * 
 * Created on May 10, 2012, 11:46 AM
 */

#include <ProcessController/GTestBaseOutputHandler.h>

GTestBaseOutputHandler::GTestBaseOutputHandler() {
    pthread_mutex_init(&(this->outputCompleteMutex), NULL);
    pthread_cond_init (&(this->outputCompleteCondition), NULL);
    this->processTerminated = false;
}

GTestBaseOutputHandler::~GTestBaseOutputHandler() {
	pthread_mutex_destroy(&(this->outputCompleteMutex));
	pthread_cond_destroy(&(this->outputCompleteCondition));
}

void GTestBaseOutputHandler::OnLineReceived(std::string* Line)
{
    if (this->processTerminated)
        throw std::logic_error("The process has already been terminated. Adding another line is not possible in this state.");
}

void GTestBaseOutputHandler::OnProcessTerminated(int status)
{
	pthread_mutex_lock(&(this->outputCompleteMutex));
	if (this->processTerminated)
	{
		pthread_mutex_unlock(&(this->outputCompleteMutex));
		throw std::logic_error("The process has already been terminated. Terminating it multiple times does not make sense.");
	}

	pthread_cond_signal(&(this->outputCompleteCondition));
    this->processTerminated = true;

    pthread_mutex_unlock(&(this->outputCompleteMutex));
}

bool GTestBaseOutputHandler::ProcessIsTerminated()
{
    return this->processTerminated;
}

void GTestBaseOutputHandler::WaitForOutputCompleted()
{
    pthread_mutex_lock(&(this->outputCompleteMutex));

    if (this->processTerminated)
    {
    	pthread_mutex_unlock(&(this->outputCompleteMutex));
    	return;
    }

    pthread_cond_wait(&(this->outputCompleteCondition), &(this->outputCompleteMutex));

    pthread_mutex_unlock(&(this->outputCompleteMutex));
}
