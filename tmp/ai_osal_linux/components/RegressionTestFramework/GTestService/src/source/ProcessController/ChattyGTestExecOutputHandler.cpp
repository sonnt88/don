/*
 * ChattyGTestExecOutputHandler.cpp
 *
 *  Created on: Jul 19, 2012
 *      Author: oto4hi
 */

#include <ProcessController/ChattyGTestExecOutputHandler.h>
#include <Core/Tasks/SendResultPacketTask.h>
#include <Core/Tasks/SendTestsDonePacketTask.h>
#include <Core/Tasks/SendResetPacketTask.h>
#include <Core/Tasks/ReactOnCrashTask.h>
#include <iostream>
#include <sstream>
#include <Utils/dbg_msg.h>

ChattyGTestExecOutputHandler::ChattyGTestExecOutputHandler(ITaskWorker* TaskWorker, TestRunModel* TestRun)
	: GTestExecOutputHandler(TestRun), BaseTaskSouce(TaskWorker)
{
}

void ChattyGTestExecOutputHandler::OnNewTestResult(std::string TestCaseName, std::string TestName, TEST_STATE state)
{
	TestModel* model = this->testRunModel->GetTestCase(TestCaseName)->GetTest(TestName);

	switch (state)
	{
	case RESET:
	  {
	    SendResetPacketTask* task = new SendResetPacketTask(model, this->testRunModel->GetIteration(), state);
	    this->taskWorker->EnqueueTask(task);
	    break;
	  }
	case PASSED:
	case FAILED:
	{
		SendResultPacketTask* task = new SendResultPacketTask(model, this->testRunModel->GetIteration(), state);
		this->taskWorker->EnqueueTask(task);
		break;
	}
	case RUNNING:
		break;
	default:
		throw std::runtime_error("unexpected test state");
	}
}

void ChattyGTestExecOutputHandler::OnAllLinesProcessed()
{
	BaseTask* task = NULL;

	
	std::stringstream dbg;
	if( this->resetRequested )
	  dbg << "Reset (power cycle) had been requested";
	else
	  dbg << "Reset (power cycle) had not been requested";
	DBG_MSG( dbg.str().c_str() );

	// If a power cycle had been requested, then we haven't crashed
	// and we haven't finished all the tests either...
	if( !( this->resetRequested ) ) {

	  // ...otherwise, we must enqueue either a crash task
	  // or a send packets done task
	  if (this->processFailure) {
	    DBG_MSG( "enqueueing ReactOnCrashTask" );
	    task = dynamic_cast<BaseTask*>(new ReactOnCrashTask());
	  }
	  else {
	    DBG_MSG( "enqueueing SendTestsDonePacketTask" );
	    task = dynamic_cast<BaseTask*>(new SendTestsDonePacketTask());
	  }

	  this->taskWorker->EnqueueTask(task);

	}
}

ChattyGTestExecOutputHandler::~ChattyGTestExecOutputHandler()
{

}
