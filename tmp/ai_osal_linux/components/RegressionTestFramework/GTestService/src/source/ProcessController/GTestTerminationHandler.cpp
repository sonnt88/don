/* 
 * File:   GTestTerminationHandler.cpp
 * Author: oto4hi
 * 
 * Created on May 8, 2012, 10:07 AM
 */

#include <ProcessController/GTestTerminationHandler.h>
#include <ProcessController/GTestProcessController.h>
#include <Utils/SignalBlocker.h>

#include <sys/wait.h>
#include <sstream>
#include <errno.h>
#include <string.h>

#include <iostream>
#include <Utils/dbg_msg.h>

GTestTerminationHandler GTestTerminationHandler::instance;

GTestTerminationHandler::GTestTerminationHandler() {
	sem_init(&(this->signalSemaphore), 0, 0);
	this->asyncSignalHandlerThread = 0;

	struct sigaction act;
	act.sa_handler = this->SIGCHLDHandler;
	act.sa_flags = 0;

	sigemptyset(&(act.sa_mask));
	if (sigaction(SIGCHLD,&act,NULL) < 0)
		throw std::runtime_error("error registering SIGCHLD handler.");

	SignalBlocker::Unblock(SIGCHLD);

	int errCode = pthread_create(&(this->asyncSignalHandlerThread), NULL, GTestTerminationHandler::asyncSIGCHLDHandler, NULL);
	if (errCode)
	{
		std::stringstream sstream;
		sstream << "error creating async signal handler thread. error code is " << strerror(errCode);
		throw std::runtime_error(sstream.str().c_str());
	}

	SignalBlocker::Block(SIGCHLD);
}

GTestTerminationHandler::~GTestTerminationHandler() {
	sem_post(&(this->signalSemaphore));
	pthread_join(this->asyncSignalHandlerThread, NULL);
}

GTestTerminationHandler* GTestTerminationHandler::GetGTestTerminationHandler()
{
    return &(GTestTerminationHandler::instance);
}

void* GTestTerminationHandler::asyncSIGCHLDHandler(void* Ptr)
{
	SignalBlocker::Unblock(SIGCHLD);

	while (1)
	{
		if (sem_wait(&(GTestTerminationHandler::instance.signalSemaphore)))
		{
			if (errno == EINTR) //call was interrupted by signal handler
				continue;
			else
				throw std::runtime_error("sem_wait failed in asyncSIGCHLDHandler.");
		}

		SignalBlocker::Block(SIGCHLD);

		if (GTestTerminationHandler::instance.pids.empty())
			break;


		std::pair<pid_t, int> frontElement = GTestTerminationHandler::instance.pids.front();

	    pid_t childpid = frontElement.first;
	    int status = frontElement.second;

	    std::stringstream dbg;
	    dbg << "child PID "
		<< childpid
		<< " has finished with status "
		<< status;
	    DBG_MSG( dbg.str().c_str() );

	    GTestTerminationHandler::instance.pids.pop();

	    if (childpid < 0)
	        throw std::runtime_error("Wait for PID failed.");

	    GTestProcessRegistry* registry = GTestProcessRegistry::GetGTestProcessRegistry();
	    GTestProcessController* controller = registry->GetGTestProcessController(childpid);

	    DBG_MSG( "Unregistering child process and stopping the process controller" );
	    registry->UnregisterGTestProcess(childpid);
	    controller->Stop(status);

	    SignalBlocker::Unblock(SIGCHLD);
	}

	pthread_exit(0);
}

void GTestTerminationHandler::SIGCHLDHandler(int Signal)
{
	int status = 0;
	pid_t childpid = wait( &status );

	GTestTerminationHandler::instance.pids.push( std::make_pair(childpid, status) );
	sem_post(&(GTestTerminationHandler::instance.signalSemaphore));
}
