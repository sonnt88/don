/*
 * PureTextSendingOutputHandler.cpp
 *
 *  Created on: 23.10.2012
 *      Author: oto4hi
 */

#include <ProcessController/GTestPureTextSendingOutputHandler.h>
#include <Core/Tasks/SendTestOutputPacketTask.h>

GTestPureTextSendingOutputHandler::GTestPureTextSendingOutputHandler(OUTPUT_SOURCE OutputSource, ITaskWorker* TaskWorker)
	: GTestBaseOutputHandler()
{
	if (TaskWorker == NULL)
		throw std::runtime_error("TaskWorker cannot be null.");

	this->outputSource = OutputSource;
	this->taskWorker = TaskWorker;
}

void GTestPureTextSendingOutputHandler::OnLineReceived(std::string* Line)
{
	SendTestOutputPacketTask* task = new SendTestOutputPacketTask(this->outputSource, *Line, NO_TIMESTAMP);
	this->taskWorker->EnqueueTask(dynamic_cast< BaseTask* >(task));
	delete Line;
}

GTestPureTextSendingOutputHandler::~GTestPureTextSendingOutputHandler() {
	// TODO Auto-generated destructor stub
}

