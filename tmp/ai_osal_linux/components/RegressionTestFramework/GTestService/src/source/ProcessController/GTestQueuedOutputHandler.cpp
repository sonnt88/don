/* 
 * File:   GTestQueuedOutputHandler.cpp
 * Author: oto4hi
 * 
 * Created on May 21, 2012, 2:44 PM
 */

#include <ProcessController/GTestQueuedOutputHandler.h>

GTestQueuedOutputHandler::GTestQueuedOutputHandler() : GTestBaseOutputHandler() {
}

void GTestQueuedOutputHandler::OnLineReceived(std::string* Line)
{
    GTestBaseOutputHandler::OnLineReceived(Line);
    this->lineQueue.PushBack(Line);
}

void GTestQueuedOutputHandler::OnProcessTerminated(int status)
{
    GTestBaseOutputHandler::OnProcessTerminated(status);
}

GTestQueuedOutputHandler::~GTestQueuedOutputHandler() 
{
}

