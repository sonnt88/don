/* 
 * File:   GTestProcessController.cpp
 * Author: oto4hi
 * 
 * Created on April 19, 2012, 2:26 PM
 */

#include <ProcessController/GTestProcessController.h>
#include <stdexcept>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>

#include <ProcessController/GTestProcessRegistry.h>
#include <ProcessController/GTestTerminationHandler.h>
#include <sstream>

#include <Utils/dbg_msg.h>

pthread_mutex_t GTestProcessController::criticalSectionMutex = PTHREAD_MUTEX_INITIALIZER;

void GTestProcessController::beginCriticalSection()
{
    pthread_mutex_lock(&(GTestProcessController::criticalSectionMutex));
}

void GTestProcessController::endCriticalSection()
{
    pthread_mutex_unlock(&(GTestProcessController::criticalSectionMutex));
}

GTestProcessController::GTestProcessController(GTestConfigurationModel Config)
{
    GTestTerminationHandler::GetGTestTerminationHandler();
    this->config = new GTestConfigurationModel(Config);
    this->stopAllThreads = false;
    this->running = false;
    pthread_mutex_init(&(this->runMutex), NULL);
    pthread_cond_init(&(this->processTerminationCondition), NULL);

    this->childpid = -1;
    this->ReadFromStderrThread = 0;
    this->ReadFromStdoutThread = 0;
}

GTestProcessController::~GTestProcessController() {
	pthread_mutex_lock(&(this->runMutex));

    delete this->config;

    this->stdoutOutputHandlers.clear();
    this->stderrOutputHandlers.clear();

    pthread_mutex_unlock(&(this->runMutex));

    pthread_mutex_destroy(&(this->runMutex));
    pthread_cond_destroy(&(this->processTerminationCondition));
}

void GTestProcessController::freeArgumentList(char** argumentList)
{
    int Index = 0;
    while (argumentList[Index])
    {
        free(argumentList[Index]);
        Index++;
    }
    free(argumentList);
}

void GTestProcessController::appendToArgumentList(char*** pArgumentList, std::string ArgToAppend, int &length)
{
    length++;
    (*pArgumentList) = (char**)realloc((*pArgumentList), sizeof(char*) * (length + 1));
    (*pArgumentList)[length-1] = (char*)malloc(ArgToAppend.length() + 1);
    memcpy((*pArgumentList)[length-1], ArgToAppend.c_str(), ArgToAppend.length() + 1);
    (*pArgumentList)[length] = NULL;
}

char** GTestProcessController::generateArgumentList()
{
	char** argumentList = NULL;
    int listLength = 0;
    
    this->appendToArgumentList(&argumentList, this->config->GetGTestExecutable().c_str(), listLength);
    
    if (this->config->GetListOnlyFlag())
    {
        this->appendToArgumentList(&argumentList, "--gtest_list_tests", listLength);
    }
    else 
    {
    	if (this->config->GetUsePersiFeaturesFlags())
    		this->appendToArgumentList(&argumentList, "--gtest_use_persi_features", listLength);
        if (!this->config->GetFilter().empty())
            this->appendToArgumentList(&argumentList, "--gtest_filter=" + this->config->GetFilter(), listLength);
        if (this->config->GetShuffleFlag())
            this->appendToArgumentList(&argumentList, "--gtest_shuffle", listLength);
        if (this->config->GetRandomSeed())
        {
            std::stringstream ss;
            ss << this->config->GetRandomSeed();
            this->appendToArgumentList(&argumentList, "--gtest_random_seed=" + ss.str(), listLength);
        }
        if (this->config->GetRepeatCount() != 1)
        {
            std::stringstream ss;
            ss << this->config->GetRepeatCount();
            this->appendToArgumentList(&argumentList, "--gtest_repeat=" + ss.str(), listLength);
        }
        if (this->config->GetRunDisabledTestsFlag())
            this->appendToArgumentList(&argumentList, "--gtest_also_run_disabled_tests", listLength);
        if (this->config->GetBreakOnFailureFlag())
            this->appendToArgumentList(&argumentList, "--gtest_break_on_failure", listLength);
    }
    
    return argumentList;
}

void GTestProcessController::notifyOutputHandlersLineReceived(std::string* Line, std::vector< GTestBaseOutputHandler* > *outputHandlers)
{
    for (std::vector< GTestBaseOutputHandler* >::iterator it = outputHandlers->begin(); it != outputHandlers->end(); ++it)
        (*it)->OnLineReceived(new std::string(*Line));
}

void GTestProcessController::notifyOutputHandlersProcessTerminated(int status)
{
    for (std::vector< GTestBaseOutputHandler* >::iterator it = this->stdoutOutputHandlers.begin(); it != this->stdoutOutputHandlers.end(); ++it)
    {
        (*it)->OnProcessTerminated(status);
    }
    for (std::vector< GTestBaseOutputHandler* >::iterator it = this->stderrOutputHandlers.begin(); it != this->stderrOutputHandlers.end(); ++it)
    {
        (*it)->OnProcessTerminated(status);
    }
}

void GTestProcessController::readFromPipe(int fd, std::vector< GTestBaseOutputHandler* > *outputHandlers)
{
	FILE *stream;
	int c = EOF;
	stream = fdopen (fd, "r");

	int flags = fcntl(fd, F_GETFL, 0);
	flags |= O_NONBLOCK;
	fcntl(fd, F_SETFL, flags);

	std::string* line = new std::string("");

	bool continueRead = true;

	while ( continueRead )
	{
		if ( c != EOF )
		{
			(*line) += c;

			if (c == '\n')
			{
				this->notifyOutputHandlersLineReceived(line, outputHandlers);
				delete line;
				line = new std::string("");
			}
		}

		if (this->stopAllThreads)
		{
			if ( (c = fgetc(stream)) == EOF )
				continueRead = false;
		} else {
			if ( (c = fgetc(stream)) == EOF )
				usleep(100);
		}
	}

	if (line->length() > 0)
	{
		this->notifyOutputHandlersLineReceived(line, outputHandlers);
	}

	delete line;

	fclose (stream);
}

void GTestProcessController::readFromStdoutPipe()
{
    this->readFromPipe(this->stdoutPipeFD[0], &(this->stdoutOutputHandlers));
}

void GTestProcessController::readFromStderrPipe()
{
    this->readFromPipe(this->stderrPipeFD[0], &(this->stderrOutputHandlers));
}

void* GTestProcessController::staticReadFromStdoutPipeThreadFunc(void* thisPtr)
{
    GTestProcessController* controller = (GTestProcessController*)thisPtr;
    controller->readFromStdoutPipe();
    
    pthread_exit(0);
}

void* GTestProcessController::staticReadFromStderrPipeThreadFunc(void* thisPtr)
{
    GTestProcessController* controller = (GTestProcessController*)thisPtr;
    controller->readFromStderrPipe();
    
    pthread_exit(0);
}

void GTestProcessController::RegisterStdoutOutputHandler(GTestBaseOutputHandler* OutputHandler)
{
    this->stdoutOutputHandlers.push_back(OutputHandler);
}

void GTestProcessController::RegisterStderrOutputHandler(GTestBaseOutputHandler* OutputHandler)
{
    this->stderrOutputHandlers.push_back(OutputHandler);
}

void GTestProcessController::Stop(int status)
{
	pthread_mutex_lock(&(this->runMutex));

	if (!this->running)
	{
		pthread_mutex_unlock(&(this->runMutex));
		throw std::runtime_error("process controller is not running, thus it cannot be stopped.");
	}

    this->stopAllThreads = true;

    DBG_MSG("joining thread ReadFromStdoutThread");
    int errCode = pthread_join(this->ReadFromStdoutThread, NULL);
    if (errCode)
	{
		std::stringstream sstream;
		sstream << "Joining ReadFromStdoutThread failed. error code is " << strerror(errCode);
		throw std::runtime_error(sstream.str().c_str());
	}

    DBG_MSG("joining thread ReadFromStderrThread");
    errCode = pthread_join(this->ReadFromStderrThread, NULL);
    if (errCode)
    {
    	std::stringstream sstream;
    	sstream << "Joining ReadFromStderrThread failed. error code is " << strerror(errCode);
    	throw std::runtime_error(sstream.str().c_str());
    }

    this->notifyOutputHandlersProcessTerminated(status);

    this->childpid = -1;
    this->ReadFromStdoutThread = 0;
    this->ReadFromStderrThread = 0;

    this->running = false;

    pthread_cond_signal(&(this->processTerminationCondition));
    pthread_mutex_unlock(&(this->runMutex));
}

void GTestProcessController::Run()
{   
    this->beginCriticalSection();
    
    pthread_mutex_lock(&(this->runMutex));

    if (this->running)
    {
        this->endCriticalSection();
        throw std::runtime_error("Process controller already runs a process.");
    }
        
    if (pipe(stdoutPipeFD))
    {
        this->endCriticalSection();
        throw std::runtime_error("Error creating pipe for stdout.");
    }
    
    if (pipe(stderrPipeFD))
    {
        this->endCriticalSection();
        throw std::runtime_error("Error creating pipe for stderr.");
    }

    char absoluteGTestExecutablePath[PATH_MAX];
	const char* relativeGTestExecutablePath = this->config->GetGTestExecutable().c_str();

	if (realpath(relativeGTestExecutablePath, (char*)&absoluteGTestExecutablePath) != (char*)&absoluteGTestExecutablePath)
		throw std::runtime_error("Cannot resolve GTest executable path.");

	if (!this->config->GetWorkingDir().empty())
	{
		if (mkdir(this->config->GetWorkingDir().c_str(), 0777) == -1)
		{
			if (errno != EEXIST)
			{
				std::stringstream sstream;
				sstream << this->config->GetWorkingDir().c_str() << " does not exist and cannot be created.";
				throw std::runtime_error(sstream.str());
			}
		}
	}

    int shmid = shmget(IPC_PRIVATE, sizeof(sem_t), 0644 | IPC_CREAT);
    sem_t* startProcessSem = (sem_t*)shmat(shmid,(void *)0, 0);

    sem_init(startProcessSem, 1, 0);
    
    this->childpid = fork();
    
    if (this->childpid < 0)
    {
        this->endCriticalSection();
        throw std::runtime_error("Error forking process.");
    }
    
    if (this->childpid == 0)
    {
        this->endCriticalSection();
        
        char** argList = this->generateArgumentList();

	// For debug purposes only:
	std::stringstream dbg;
	dbg << "Child Process (PID "
	    << getpid()
	    << ") executing programme"
	    << " : "
	    << argList[ 0 ];
	
	for( int i = 1; argList[ i ]; ++i ) {
	  dbg << " " << argList[ i ];
	}
        DBG_MSG( dbg.str().c_str() );
        
        close(stdoutPipeFD[0]);
        close(stderrPipeFD[0]);
        
        dup2(stdoutPipeFD[1], fileno(stdout) );
        dup2(stderrPipeFD[1], fileno(stderr) );

        sem_wait(startProcessSem);
        shmdt((void*)startProcessSem);

        if (!this->config->GetWorkingDir().empty())
        	chdir(this->config->GetWorkingDir().c_str());

        execvp(absoluteGTestExecutablePath, argList);
    }
    else
    {   
        this->running = true;
        this->stopAllThreads = false;
        
        GTestProcessRegistry* registry = GTestProcessRegistry::GetGTestProcessRegistry();
        registry->RegisterGTestProcess(this->childpid, this);

        DBG_MSG("creating thread ReadFromStdoutThread");
        int errCode = pthread_create(&(this->ReadFromStdoutThread), NULL, this->staticReadFromStdoutPipeThreadFunc, (void*)this);
        if (errCode)
        {
        	std::stringstream sstream;
			sstream << "Error creating thread that reads from stdout. error code is " << strerror(errCode);
			throw std::runtime_error(sstream.str().c_str());
        }
        
        DBG_MSG("creating thread ReadFromStderrThread");
        errCode = pthread_create(&(this->ReadFromStderrThread), NULL, this->staticReadFromStderrPipeThreadFunc, (void*)this);
        if (errCode)
		{
			std::stringstream sstream;
			sstream << "Error creating thread that reads from stderr. error code is " << strerror(errCode);
			throw std::runtime_error(sstream.str().c_str());
		}

        sem_post(startProcessSem);
        sem_destroy(startProcessSem);
        shmdt((void*)startProcessSem);
        shmctl( shmid, IPC_RMID, 0 );

        pthread_mutex_unlock(&(this->runMutex));

        this->endCriticalSection();
        
        close(stdoutPipeFD[1]);
        close(stderrPipeFD[1]);
    }
}

void GTestProcessController::Kill()
{
	pthread_mutex_lock(&(this->runMutex));
	if (!(this->running))
		throw std::runtime_error("process controller is not running -> nothing to kill.");

	kill(this->childpid, SIGKILL);

	pthread_mutex_unlock(&(this->runMutex));
}

void GTestProcessController::WaitForProcessTermination()
{
	pthread_mutex_lock(&(this->runMutex));
	if (this->running)
		pthread_cond_wait(&(this->processTerminationCondition), &(this->runMutex));
	pthread_mutex_unlock(&(this->runMutex));
}
