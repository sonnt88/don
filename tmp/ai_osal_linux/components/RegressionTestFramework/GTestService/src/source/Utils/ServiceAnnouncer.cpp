/*
 * ServiceAnnouncer.cpp
 *
 *  Created on: 30.11.2012
 *      Author: oto4hi
 */

#include <stdlib.h>
#include <Utils/ServiceAnnouncer.h>
#include <sstream>
#include <stdexcept>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

const char* ServiceAnnouncer::discoveryString = GTESTSERVICE_DISCOVERY_STRING;
const char* ServiceAnnouncer::answerString = GTESTSERVICE_ANSWER_STRING;

void ServiceAnnouncer::announceServiceFunc()
{
	int sock = socket(AF_INET, SOCK_DGRAM, 0);

	struct sockaddr_in saSocket;
	memset(&saSocket, 0, sizeof(saSocket));
	saSocket.sin_family      = AF_INET;
	saSocket.sin_addr.s_addr = htonl(INADDR_ANY);
	saSocket.sin_port        = htons(this->announceport);

	if (bind(sock, (struct sockaddr *) &saSocket, sizeof(saSocket)) != 0)
	{
		throw std::runtime_error("bind on udp socket failed");
	}

	int recvBufferSize = strlen(this->discoveryString) + 1;
	char* recvBuffer = (char*)malloc(recvBufferSize);

	int answerStringSize = strlen(this->answerString) + 1;
	int sendBufferSize =  answerStringSize + ServiceAnnouncer::nameLength + sizeof (int32_t);
	char* sendBuffer = (char*)calloc(sendBufferSize,1);

	memcpy((void*)sendBuffer, (void*)(this->answerString), answerStringSize);
	memcpy((void*)&(sendBuffer[answerStringSize]), (void*)(this->name.c_str()), ServiceAnnouncer::nameLength - 1);

	*((uint32_t*)&(sendBuffer[sendBufferSize - sizeof(int32_t)])) = htonl(this->serviceport);

	while (this->cont)
	{
		struct sockaddr_in fromAddr;
		int fromAddrLen = sizeof(fromAddr);

		int rc = recvfrom(sock, (char *)recvBuffer, recvBufferSize, MSG_DONTWAIT, (struct sockaddr *) &fromAddr, (socklen_t*)&fromAddrLen);
		if (rc == recvBufferSize)
		{
			if (strncmp((char*)recvBuffer, this->discoveryString, recvBufferSize - 1) == 0)
			{
				struct sockaddr_in toAddr;
				memset(&toAddr, 0, sizeof(toAddr));

				toAddr.sin_family = AF_INET;
				toAddr.sin_addr.s_addr = fromAddr.sin_addr.s_addr;
				toAddr.sin_port        = fromAddr.sin_port;

				if (sendto(sock, (void*)sendBuffer, sendBufferSize, MSG_DONTWAIT, (struct sockaddr *)&toAddr, sizeof(toAddr) )  != sendBufferSize)
				{
					throw std::runtime_error("send failed");
				}
			}
		}
		usleep(10000);
	}

	free(recvBuffer);
	free(sendBuffer);

	close(sock);
}

void* ServiceAnnouncer::staticAnnounceServiceFunc(void* thisPtr)
{
	ServiceAnnouncer* pthis = (ServiceAnnouncer*)thisPtr;
	pthis->announceServiceFunc();
	return NULL;
}

ServiceAnnouncer::ServiceAnnouncer(int ServiceTCPPort, int AnnounceUDPPort, std::string Name)
{
	this->serviceport = ServiceTCPPort;
	this->announceport = AnnounceUDPPort;
	this->announceThread = 0;
	this->cont = true;
	this->running = false;
	this->name = Name;
	pthread_mutex_init(&(this->runMutex), NULL);
}

void ServiceAnnouncer::Run()
{
	pthread_mutex_lock(&(this->runMutex));
	if (this->running)
	{
		pthread_mutex_unlock(&(this->runMutex));
		throw std::runtime_error("service announcer is already running");
	}

	this->cont = true;

	int errCode = pthread_create(&(this->announceThread), NULL, this->staticAnnounceServiceFunc, (void*)this);
	if (errCode)
	{
		std::stringstream sstream;
		sstream << "Error creating service announcer thread. error code is " << strerror(errCode);
		pthread_mutex_unlock(&(this->runMutex));
		throw std::runtime_error(sstream.str().c_str());
	}

	this->running = true;

	pthread_mutex_unlock(&(this->runMutex));
}

void ServiceAnnouncer::Stop()
{
	pthread_mutex_lock(&(this->runMutex));

	if (!this->running)
	{
		pthread_mutex_unlock(&(this->runMutex));
		throw std::runtime_error("service announcer is not running.");
	}

	this->cont = false;

	int errCode = pthread_join(this->announceThread, NULL);
	if (errCode)
	{
		std::stringstream sstream;
		sstream << "Joining service announcer thread failed. error code is " << strerror(errCode);
		pthread_mutex_unlock(&(this->runMutex));
		throw std::runtime_error(sstream.str().c_str());
	}

	this->running = false;

	pthread_mutex_unlock(&(this->runMutex));
}

ServiceAnnouncer::~ServiceAnnouncer() {
	if (this->running)
		throw std::runtime_error("ServiceAnnouncer destructor called while announce thread still running.");

	pthread_mutex_destroy(&(this->runMutex));
}

