/*
 * TaskMatcher.cpp
 *
 *  Created on: Jul 25, 2012
 *      Author: oto4hi
 */

#include <Utils/TaskMatcher.h>

bool TaskMatcher::Match(BaseTask* Element1, BaseTask* Element2)
{
	return Element1->GetTypeOfTask() == Element2->GetTypeOfTask();
}
