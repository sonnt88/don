/* 
 * File:   TestModel.cpp
 * Author: oto4hi
 * 
 * Created on April 19, 2012, 2:49 PM
 */

#include <Models/TestModel.h>
#include <stdlib.h>
#include <stdexcept>
#include <GTestServiceBuffers.pb.h>

#include <iostream>

void TestModel::resetCounters()
{
	this->currentTestRun = 0;
	this->passedCount = 0;
}

void TestModel::ResetResults()
{
    this->testRunResults.clear();
    this->resetCounters();
}

TestModel::TestModel(std::string Name, int ID) : TestBaseModel(Name) {
    if (this->defaultDisabled)
        this->Enabled = false;
    else
        this->Enabled = true;

    this->id = ID;

    this->resetCounters();
    pthread_mutex_init(&this->mutex, NULL);
}

TestModel::TestModel(GTestServiceBuffers::Test Message) : TestBaseModel(Message.name())
{
	this->defaultDisabled = Message.defaultdisabled();
	this->Enabled = Message.enabled();
	this->id = Message.id();

	this->resetCounters();
	pthread_mutex_init(&this->mutex, NULL);
}

void TestModel::LockInstance()
{
	pthread_mutex_lock(&(this->mutex));
}

void TestModel::UnlockInstance()
{
	pthread_mutex_unlock(&(this->mutex));
}

int TestModel::GetFailedCount()
{
    return this->currentTestRun - this->passedCount;
}

int TestModel::GetPassedCount()
{
    return this->passedCount;
}

int TestModel::GetID()
{
	return this->id;
}

void TestModel::AppendTestResult(int Iteration, bool Passed)
{
	pthread_mutex_lock(&(this->mutex));

    this->testRunResults.insert(TestResultPair(Iteration, Passed));
    if (this->testRunResults.size() > MAX_RESULT_LIST_LENGTH)
    	this->testRunResults.erase(this->testRunResults.begin());

    if (Passed)
    	this->passedCount++;

    this->currentTestRun++;
    pthread_mutex_unlock(&(this->mutex));
}

bool TestModel::GetTestResult(int TestRunIndex)
{
    if (TestRunIndex < 0)
        throw std::invalid_argument("TestRunIndex cannot be smaller than zero.");

    if (this->testRunResults.find(TestRunIndex) == this->testRunResults.end())
    	throw std::runtime_error("result not found for this test run index: maybe the result buffer is not big enough and the result is already gone. Try to increase MAX_RESULT_LIST_LENGTH in TestModel.h.");

    return this->testRunResults[TestRunIndex];
}

 void TestModel::ToProtocolBuffer(::GTestServiceBuffers::Test* Message)
{
	 if (Message == NULL)
		 throw std::invalid_argument("Message cannot be null.");

	 Message->set_name(this->name);
	 Message->set_defaultdisabled(this->defaultDisabled);
	 Message->set_enabled(this->Enabled);
	 Message->set_id(this->id);
}

 std::map<int, bool>::iterator TestModel::ResultsBegin()
 {
	 return this->testRunResults.begin();
 }

 std::map<int, bool>::iterator TestModel::ResultsEnd()
 {
	 return this->testRunResults.end();
 }

 TestModel::~TestModel()
 {
 	pthread_mutex_destroy(&(this->mutex));
 }
