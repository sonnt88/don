/* 
 * File:   TestSetBaseModel.cpp
 * Author: oto4hi
 * 
 * Created on May 2, 2012, 5:20 PM
 */

#include <Models/TestSetBaseModel.h>
#include <stdexcept>

TestSetBaseModel::TestSetBaseModel() {
}

TestSetBaseModel::~TestSetBaseModel() {
    for (std::vector< TestBaseModel* >::iterator it = this->elements.begin(); it != this->elements.end(); ++it)
    {
        delete *it;
    }
    
    this->elements.clear();
}

bool TestSetBaseModel::contains(TestBaseModel* Element)
{
    for (std::vector< TestBaseModel* >::iterator it = this->elements.begin(); it != this->elements.end(); ++it)
    {
        if (!(*it)->GetName().compare(Element->GetName()))
            return true;
    }
    return false;
}
    
TestBaseModel* TestSetBaseModel::GetElementByIndex(int Index)
{
    if (Index < 0 || Index >= this->elements.size())
        throw std::invalid_argument("Index is out of bounds.");
        
    return this->elements[Index];
}

TestBaseModel* TestSetBaseModel::GetElementByName(std::string Name)
{
    for (std::vector< TestBaseModel* >::iterator it = this->elements.begin(); it != this->elements.end(); ++it)
    {
        if (!(*it)->GetName().compare(Name))
            return *it;
    }
    throw std::invalid_argument("Could not find any test with the specified name.");
}

void TestSetBaseModel::AddElement(TestBaseModel* Element)
{
    if (!Element)
        throw std::invalid_argument("'Test' cannot be zero.");
    
    if (this->contains(Element))
        throw std::runtime_error("Test already exists in this test case.");
    
    this->elements.push_back(Element);
}

