/* 
 * File:   NetworkChunk.cpp
 * Author: oto4hi
 * 
 * Created on May 23, 2012, 2:43 PM
 */

#include <AdapterConnection/AdapterPacket.h>
#include <string.h>

AdapterPacketHeader::AdapterPacketHeader(char* Buffer)
{
	this->id            = Buffer[0];
	this->flags         = Buffer[1];
	this->serial        = ntohl(*((uint32_t*)(&(Buffer[4]))));
	this->requestSerial = ntohl(*((uint32_t*)(&(Buffer[8]))));
	this->payloadSize   = ntohl(*((uint32_t*)(&(Buffer[12]))));
}

AdapterPacketHeader::AdapterPacketHeader(AdapterPacketHeader& orig)
{
	this->id = orig.id;
	this->flags = orig.flags;
	this->serial = orig.serial;
	this->requestSerial = orig.requestSerial;
	this->payloadSize = orig.payloadSize;
}

AdapterPacketHeader::AdapterPacketHeader(char ID, char Flags, uint32_t Serial, uint32_t RequestSerial, uint32_t PayloadSize)
{
	this->id = ID;
	this->flags = Flags;
	this->serial = Serial;
	this->requestSerial = RequestSerial;
	this->payloadSize = PayloadSize;
}

void AdapterPacket::setPayload(char* Payload)
{
	if (this->header.payloadSize)
	{
		this->payload = (char*)malloc(this->header.payloadSize);
		memcpy(this->payload, Payload, this->header.payloadSize);
	}
	else
		this->payload = NULL;
}

AdapterPacket::AdapterPacket(AdapterPacketHeader Header, char* Payload, uint32_t ConnectionID) :
		header(Header)
{
	this->setPayload(Payload);
	this->connectionID = ConnectionID;
}

AdapterPacket::AdapterPacket(AdapterPacket& orig) :
		header(orig.header)
{
	this->setPayload(orig.payload);
	this->connectionID = orig.connectionID;
}

AdapterPacket::AdapterPacket(char ID, char Flags, uint32_t Serial, uint32_t RequestSerial, uint32_t PayloadSize, char* Payload, uint32_t ConnectionID) :
		header(ID, Flags, Serial, RequestSerial, PayloadSize)
{
    this->setPayload(Payload);
    this->connectionID = ConnectionID;
}

AdapterPacket::~AdapterPacket()
{
	if (this->payload)
		free(this->payload);
}
    
char AdapterPacket::GetID()
{
    return this->header.id;
}

char AdapterPacket::GetFlags()
{
    return this->header.flags;
}

unsigned int AdapterPacket::GetSerial()
{
    return this->header.serial;
}

unsigned int AdapterPacket::GetRequestSerial()
{
	return this->header.requestSerial;
}

unsigned int AdapterPacket::GetPayloadSize()
{
    return this->header.payloadSize;
}

char* AdapterPacket::GetPayload()
{
    return this->payload;
}
