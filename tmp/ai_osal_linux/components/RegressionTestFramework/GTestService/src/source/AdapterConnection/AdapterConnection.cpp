/* 
 * File:   AdapterConnection.cpp
 * Author: oto4hi
 * 
 * Created on May 10, 2012, 5:14 PM
 */

#include <AdapterConnection/AdapterConnection.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

uint32_t AdapterConnection::adapterConnectionCounter = 0;
uint32_t AdapterConnection::currentPacketSerial = 0;
pthread_mutex_t AdapterConnection::serialMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t AdapterConnection::connectionIDMutex = PTHREAD_MUTEX_INITIALIZER;

uint32_t AdapterConnection::generatePacketSerial()
{
    uint32_t serial;
    pthread_mutex_lock(&serialMutex);
    currentPacketSerial++;
    serial = currentPacketSerial;
    pthread_mutex_unlock(&serialMutex);
    return serial;
}

AdapterConnection::AdapterConnection(int Sock)
{
    this->sock = Sock;
    this->closed = false;
    this->currentReceiveBufferPosition = 0;
    this->bytesStillToReceive = 0;
    pthread_mutex_init(&(this->socketMutex), NULL);
    
    pthread_mutex_lock(&(this->connectionIDMutex));
    this->adapterConnectionCounter++;
    this->connectionID = this->adapterConnectionCounter;
    pthread_mutex_unlock(&(this->connectionIDMutex));
}

int AdapterConnection::rawSend(char* Data, int Size)
{
	int rc = 0;
	int bytesSent = 0;

	while (bytesSent < Size)
	{
		rc = send(this->sock, &(Data[bytesSent]), Size-bytesSent, MSG_DONTWAIT);

		if (rc == 0)
			return 0;

		if (rc < 0)
		{
			if (!(errno == EWOULDBLOCK || errno == EAGAIN))
			{
				pthread_mutex_unlock(&(AdapterConnection::socketMutex));
				throw std::runtime_error("socket error: send failed");
			}
			rc = 0;
		}

		bytesSent += rc;
	}

	return bytesSent;
}

void AdapterConnection::Send(AdapterPacket& Data)
{
	pthread_mutex_lock(&(AdapterConnection::socketMutex));

	if (this->closed)
	{
		pthread_mutex_unlock(&(AdapterConnection::socketMutex));
		throw GTestServiceExceptions::ConnectionClosedException("cannot send on a closed connection.");
	}

	if (Data.GetPayloadSize() > ADAPTER_PACKET_MAX_PAYLOAD_SIZE)
	{
		pthread_mutex_unlock(&(AdapterConnection::socketMutex));
		throw std::invalid_argument("send: adapter packet payload size is too large");
	}

    char* buffer = (char*) malloc (Data.GetPayloadSize() + ADAPTER_PACKET_HEADER_SIZE);
    if (!buffer)
    {
    	pthread_mutex_unlock(&(AdapterConnection::socketMutex));
    	throw std::runtime_error("allocation of send buffer failed");
    }

    uint32_t serial = generatePacketSerial();
    uint32_t* intPtr;
    
    buffer[0] = Data.GetID();
    buffer[1] = Data.GetFlags();
    buffer[2] = 0; //
    buffer[3] = 0; //alignment to 4 bytes

    intPtr = (uint32_t*)(buffer + 4);
    *intPtr = htonl(serial);

    intPtr = (uint32_t*)(buffer + 8);
    *intPtr = htonl(Data.GetRequestSerial());

    intPtr = (uint32_t*)(buffer + 12);
    *intPtr = htonl(Data.GetPayloadSize());

    memcpy(buffer + ADAPTER_PACKET_HEADER_SIZE, Data.GetPayload(), Data.GetPayloadSize());
    
    int rc = this->rawSend(buffer, Data.GetPayloadSize() + ADAPTER_PACKET_HEADER_SIZE);
    free(buffer);
    
    if (rc == 0)
    {
        serial = 0;
        this->internalClose();
    }
    else
    {
    	if (rc < Data.GetPayloadSize())
    	{
    		pthread_mutex_unlock(&(AdapterConnection::socketMutex));
    		throw std::runtime_error("socket error: send uncomplete");
    	}
    }

    Data.header.serial = serial;
    pthread_mutex_unlock(&(AdapterConnection::socketMutex));
}

AdapterPacketHeader* AdapterConnection::ReceiveHeader()
{
	AdapterPacketHeader* header = NULL;
	if (currentReceiveBufferPosition < ADAPTER_PACKET_HEADER_SIZE)
	{
		int rc = recv(
				this->sock,
				&(this->receiveBuffer[currentReceiveBufferPosition]),
				ADAPTER_PACKET_HEADER_SIZE - currentReceiveBufferPosition,
				MSG_DONTWAIT);

		if (rc > 0)
			this->currentReceiveBufferPosition += rc;
		else
		{
			if ((errno != EWOULDBLOCK && errno != EAGAIN) || rc == 0 )
				this->internalClose();
		}
	}

	if (currentReceiveBufferPosition >= ADAPTER_PACKET_HEADER_SIZE)
	{
		header = new AdapterPacketHeader(reinterpret_cast<char*>(this->receiveBuffer));
	}

	return header;
}

bool AdapterConnection::TryReceivePacketToTheEnd()
{
	if (this->bytesStillToReceive == 0)
		return true;

	int rc = recv(
		this->sock,
		&(this->receiveBuffer[currentReceiveBufferPosition]),
		this->bytesStillToReceive,
		MSG_DONTWAIT);

	if (rc > 0)
	{
		this->currentReceiveBufferPosition += rc;
		this->bytesStillToReceive -= rc;
	}
	else
	{
		if ((errno != EWOULDBLOCK && errno != EAGAIN) || rc == 0 )
			this->internalClose();

		return false;
	}

	if (this->bytesStillToReceive < 0)
		throw std::runtime_error("amount of bytes that are still to be received cannot be smaller than 0.");

	return (this->bytesStillToReceive == 0);

}

AdapterPacket* AdapterConnection::Receive()
{
	AdapterPacket* packet = NULL;

	pthread_mutex_lock( &(this->socketMutex) );

	if (this->closed)
	{
		pthread_mutex_unlock( &(this->socketMutex) );
		throw GTestServiceExceptions::ConnectionClosedException("cannon receive on a closed connection.");
	}

	if ( (this->currentReceiveBufferPosition > ADAPTER_PACKET_HEADER_SIZE && this->bytesStillToReceive == 0) ||
	     (this->currentReceiveBufferPosition < ADAPTER_PACKET_HEADER_SIZE && this->bytesStillToReceive != 0) )
	{
		pthread_mutex_unlock( &(this->socketMutex) );
		this->internalClose();
		fprintf(stderr, "AdapterConnection internal state seems to be inconsistent. -> closing the socket.\n");
		return NULL;
	}

	AdapterPacketHeader* header = this->ReceiveHeader();

	if (this->currentReceiveBufferPosition == ADAPTER_PACKET_HEADER_SIZE) //no payload byte has been read so far
		this->bytesStillToReceive = header->payloadSize;

	if (header)
	{
		if (header->payloadSize > ADAPTER_PACKET_MAX_PAYLOAD_SIZE)
		{
			this->internalClose();
			fprintf(stderr, "receive: payload size is too large (%u) -> closed connection\n", header->payloadSize);
		} else
		{
			if (this->TryReceivePacketToTheEnd())
			{
				packet = new AdapterPacket(*header, &(this->receiveBuffer[ADAPTER_PACKET_HEADER_SIZE]), this->connectionID);
				this->currentReceiveBufferPosition = 0;
				this->bytesStillToReceive = 0;
			}
		}

		delete header;
	}

	pthread_mutex_unlock( &(this->socketMutex) );

	return packet;
}

void AdapterConnection::internalClose()
{
    close(this->sock);
    this->closed = true;
    this->currentReceiveBufferPosition = 0;
    this->bytesStillToReceive = 0;
}

void AdapterConnection::Close()
{
    pthread_mutex_lock(&(this->socketMutex));
    if (this->closed)
    	throw GTestServiceExceptions::ConnectionClosedException("AdapterConnection is already closed and cannot be closed again.");
    this->internalClose();
    pthread_mutex_unlock(&(this->socketMutex));
}

bool AdapterConnection::IsClosed()
{
    return this->closed;
}
    
AdapterConnection::~AdapterConnection()
{
	pthread_mutex_destroy(&(this->socketMutex));
}

int AdapterConnection::GetID()
{
    return this->connectionID;
}
