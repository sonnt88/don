/* 
 * File:   AdapterConnectionFactory.cpp
 * Author: oto4hi
 * 
 * Created on June 5, 2012, 4:54 PM
 */

#include <AdapterConnection/AdapterConnectionFactory.h>
#include <AdapterConnection/AdapterConnection.h>

AdapterConnectionFactory::AdapterConnectionFactory() {
}

AdapterConnectionFactory::~AdapterConnectionFactory() {
}

IConnection* AdapterConnectionFactory::CreateConnection(int Sock)
{
    return new AdapterConnection(Sock);
}
