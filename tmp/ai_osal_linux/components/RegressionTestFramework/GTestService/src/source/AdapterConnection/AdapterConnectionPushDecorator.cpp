/* 
 * File:   AdapterConnectionPushDecorator.cpp
 * Author: oto4hi
 * 
 * Created on June 5, 2012, 4:23 PM
 */

#include <AdapterConnection/Interfaces/IPacketReceiver.h>
#include <AdapterConnection/AdapterConnectionPushDecorator.h>
#include <stdexcept>
#include <iostream>

AdapterConnectionPushDecorator::AdapterConnectionPushDecorator(IConnection* Decoratee, IPacketReceiver* Receiver)
{
    if (Decoratee == NULL)
        throw std::invalid_argument("Decoratee cannot be null.");
    
    if (Receiver == NULL)
        throw std::invalid_argument("Receiver cannot be null.");
    
    this->decoratee = Decoratee;
    this->receiver = Receiver;
}
    
void AdapterConnectionPushDecorator::Send(AdapterPacket& Data)
{
    this->decoratee->Send(Data);
}

AdapterPacket* AdapterConnectionPushDecorator::Receive()
{
	bool connectionJustClosed = false;
	bool connectionWasOpen = !this->decoratee->IsClosed();

    AdapterPacket* packet = this->decoratee->Receive();

    if (connectionWasOpen && this->decoratee->IsClosed())
    	connectionJustClosed = true;

    if (packet || connectionJustClosed)
    	this->receiver->OnPacketReceive(this, packet);

    return packet;
}

void AdapterConnectionPushDecorator::Close()
{
    this->decoratee->Close();
}

bool AdapterConnectionPushDecorator::IsClosed()
{
    return this->decoratee->IsClosed();
}

int AdapterConnectionPushDecorator::GetID()
{
    return this->decoratee->GetID();
}
    
AdapterConnectionPushDecorator::~AdapterConnectionPushDecorator()
{
    delete this->decoratee;
}

