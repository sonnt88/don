/* 
 * File:   AdapterConnectionPushDecoratorFactor.cpp
 * Author: oto4hi
 * 
 * Created on June 5, 2012, 5:16 PM
 */

#include <AdapterConnection/AdapterConnectionPushDecoratorFactory.h>
#include <AdapterConnection/AdapterConnectionPushDecorator.h>
#include <AdapterConnection/AdapterConnection.h>
#include <stdexcept>

AdapterConnectionPushDecoratorFactory::AdapterConnectionPushDecoratorFactory(IPacketReceiver* Receiver)
{
    if (Receiver == NULL)
        throw std::invalid_argument("Receiver cannot be null.");
    
    this->receiver = Receiver;
}

AdapterConnectionPushDecoratorFactory::~AdapterConnectionPushDecoratorFactory()
{
    
}

IConnection* AdapterConnectionPushDecoratorFactory::CreateConnection(int Sock)
{
    AdapterConnection* connection = new AdapterConnection(Sock);
    AdapterConnectionPushDecorator* decoratedConnection = new AdapterConnectionPushDecorator(connection, this->receiver);
    return decoratedConnection;
}

