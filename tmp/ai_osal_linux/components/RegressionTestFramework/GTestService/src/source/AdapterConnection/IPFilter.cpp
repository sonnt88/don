/*
 * IPFilter.cpp
 *
 *  Created on: 05.02.2013
 *      Author: oto4hi
 */

#include <AdapterConnection/IPFilter.h>
#include <stdexcept>
#include <vector>
#include <sstream>
#include <string.h>

std::string IPFilter::generateExceptionMessage(std::string IPFilterString)
{
	std::string message = IPFilterString;
	message.append(" does not seem to represent an IP filter.");
	return message;
}

IPFilter::IPFilter(IPFilter &orig)
{
	memcpy(&(this->ipaddr), &(orig.ipaddr), sizeof(struct in_addr));
	this->fixedbits = orig.fixedbits;
	this->ipFilterString = orig.ipFilterString;
}

bool IPFilter::checkOnlyContainsDigits(std::string IntegerString)
{
	bool isInteger = true;
	for (int index = 0; index < IntegerString.size(); index++)
	{
		if ('0' > IntegerString[index] || IntegerString[index] > '9')
		{
			isInteger = false;
		}
	}
	return isInteger;
}

std::string IPFilter::GetIPFilterString()
{
	return this->ipFilterString;
}

IPFilter::IPFilter(std::string IPFilterString) {
	std::vector<std::string> splitResult;
	std::stringstream stream(IPFilterString);
	std::string word;

	while(getline(stream, word, '/'))
		splitResult.push_back(word);

	if (splitResult.size() < 1 || splitResult.size() > 2)
	{
		throw std::invalid_argument(this->generateExceptionMessage(IPFilterString));
	}

	int retVal = inet_aton(splitResult[0].c_str(), &(this->ipaddr));
	this->fixedbits = 32;

	if (!retVal)
	{
		throw std::invalid_argument(this->generateExceptionMessage(IPFilterString));
	}

	if (splitResult.size() > 1)
	{
		if (!this->checkOnlyContainsDigits(splitResult[1]))
		{
			throw std::invalid_argument(this->generateExceptionMessage(IPFilterString));
		}

		std::stringstream isstream(splitResult[1]);
		isstream >> this->fixedbits;
		if (isstream.fail() || (this->fixedbits % 8) || (this->fixedbits < 0 || this->fixedbits > 32))
		{
			throw std::invalid_argument(this->generateExceptionMessage(IPFilterString));
		}
	}

	this->ipFilterString = IPFilterString;
}

bool IPFilter::Matches(struct in_addr *IPAddr)
{
	in_addr_t mask = 0;
	for (int index = 0; index < 32; index++)
	{
		mask = mask << 1;
		if (index < this->fixedbits)
			mask += 1;
	}
	mask = htonl(mask);

	if ((mask & (IPAddr->s_addr)) == this->ipaddr.s_addr)
		return true;
	else
		return false;
}

struct in_addr IPFilter::GetIPAddr()
{
	return this->ipaddr;
}

short IPFilter::GetFixedBits()
{
	return this->fixedbits;
}

IPFilter::~IPFilter() {
	// TODO Auto-generated destructor stub
}

