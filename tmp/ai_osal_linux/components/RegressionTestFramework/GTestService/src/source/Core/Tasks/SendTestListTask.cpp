/*
 * SendTestListTask.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/Tasks/SendTestListTask.h>

SendTestListTask::SendTestListTask(IConnection* Connection, uint32_t RequestSerial) :
	BaseReplyTask(Connection, RequestSerial, TASK_SEND_TESTLIST)
{
}
