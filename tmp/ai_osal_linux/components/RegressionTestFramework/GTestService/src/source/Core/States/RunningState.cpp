/*
 * RunningState.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/States/RunningState.h>
#include <Core/States/IdleState.h>
#include <ProcessController/ChattyGTestExecOutputHandler.h>

#include <sstream>
#include "Utils/dbg_msg.h"

void RunningState::RunProcessController() {
	this->processController = new GTestProcessController(*(this->configModel));

	this->stdoutHandler = new ChattyGTestExecOutputHandler(this->context, this->context->GetTestRunModel());

	this->stdoutTextForwarder = new GTestPureTextSendingOutputHandler(OUTPUT_SOURCE_STDOUT, this->context);
	this->stderrTextForwarder = new GTestPureTextSendingOutputHandler(OUTPUT_SOURCE_STDERR, this->context);

	this->processController->RegisterStdoutOutputHandler(this->stdoutHandler);
	this->processController->RegisterStdoutOutputHandler(this->stdoutTextForwarder);
	this->processController->RegisterStderrOutputHandler(this->stderrTextForwarder);

	this->processController->Run();
	this->stdoutHandler->ProcessTestResultLines();
}

RunningState::RunningState(GTestServiceCore* Context, GTestServiceBuffers::TestLaunch* TestSelection) :
  BaseValidState(Context)
{
	Context->GetTestRunModel()->ResetIteration();
	Context->GetTestRunModel()->ResetAllTestResults();
	Context->GetTestRunModel()->SelectTests(TestSelection);

	this->stdoutHandler = NULL;
	this->stdoutTextForwarder = NULL;
	this->stderrTextForwarder = NULL;

	this->processController = NULL;

	this->configModel = this->CreateConfigurationModel(TestSelection);
	this->RunProcessController();
}

void RunningState::SendCurrentState(SendCurrentStateTask* Task)
{
	int payloadSize = 9 ; //sizeof(char) + 2*sizeof(uint32)
	char payload[9];

	char* pState            = (char*)(&payload[0]);
	int*  pCurrentIteration = (int*) (&payload[1]);
	int*  pTestID           = (int*) (&payload[5]);

	*pState = this->GetTypeOfState();

	this->context->GetTestRunModel()->LockInstance();
	*pCurrentIteration = this->context->GetTestRunModel()->GetIteration();
	*pTestID = this->context->GetTestRunModel()->GetCurrentTestID();
	this->context->GetTestRunModel()->UnlockInstance();

	AdapterPacket packet(
		(char)ID_UP_STATE,
		0, 0,
		Task->GetRequestSerial(),
		payloadSize,
		payload,
		Task->GetConnection()->GetID());

	Task->GetConnection()->Send(packet);
}

GTestConfigurationModel* RunningState::CreateConfigurationModel(GTestServiceBuffers::TestLaunch* TestSelection)
{
	uint32_t configurationFlags = GTEST_CONF_EXEC_TESTS | GTEST_CONF_RUN_DISABLED_TESTS;

	if (this->context->GetUsePersiFeaturesFlag())
		configurationFlags |= GTEST_CONF_USE_PERSI_FEATURES;

	uint32_t randomSeek = 0;
	uint32_t repeatCount = 1;


	if (TestSelection->has_shuffle())
	{
		if (TestSelection->shuffle())
			configurationFlags |= GTEST_CONF_SHUFFLE_TESTS;

		if (TestSelection->has_randomseek())
			randomSeek = TestSelection->randomseek();
	}

	if (TestSelection->has_numberofrepeats())
	{
		repeatCount = TestSelection->numberofrepeats();
		if (repeatCount >= LONG_MAX || repeatCount == 0)
			repeatCount = LONG_MAX - 1;
	}

	TestRunModel* runModel = this->context->GetTestRunModel();
	std::string testFilter = runModel->GenerateTestFilterString();

	GTestConfigurationModel* configModel = new GTestConfigurationModel(
			this->context->GetGTestPath(),
			this->context->GetWorkingDir(),
			testFilter,
			repeatCount,
			randomSeek,
			configurationFlags);
	return configModel;
}

ICoreState* RunningState::HandleSendCurrentStateTask(SendCurrentStateTask* Task)
{
	this->SendCurrentState(Task);
	return this;
}

ICoreState* RunningState::HandleExecTestsTask(ExecTestsTask* Task)
{
	this->SendAck(Task, ID_UP_ACK_TESTRUN, false);
	return this;
}

void RunningState::SendTestsDonePacket() {
	AdapterPacket packet((char) ID_UP_ALL_TESTS_DONE, 0, 0, 0, 0, NULL, 0);
	this->context->GetConnectionManager()->Multicast(packet);
}

ICoreState* RunningState::HandleSendResetPacketTask(SendResetPacketTask* Task) {
  // This is where we create a TestLaunch object, with a list of tests
  // that need to run after the power cycle.
  //
  // The list of tests offered by the test process are held in
  // BaseValidState::context->GetTestRunModel().
  // The test which have already complete are held in testRecord.
  //
  // Thus we can work out which tests remain

  // Before we start, add the current test to the test record
  testRecord.add_testid( Task->GetTestModel()->GetID() );

  GTestServiceBuffers::TestLaunch remainingTests;

  TestRunModel * const allTests( BaseValidState::context->GetTestRunModel() );
  const int            testRecordSize( testRecord.testid_size() );
  const int            testSize( allTests->GetTestCaseCount() );
  int                  index_testSelection( 0 );
  int                  index_testRecord( 0 );

  // Two loops are needed to go through all of the tests
  for( int index_testCase = 0; index_testCase < testSize; ++index_testCase ) {
    TestCaseModel * const testCase( allTests->GetTestCase( index_testCase ) );
    for( int index_test = 0; index_test < testCase->TestCount(); ++index_test ) {
      // Within this loop we will see each and every test
      TestModel * const test( testCase->GetTest( index_test ) );

      // If this test had been selected ...
      if( true == test->Enabled ) {
	// ... but hasn't yet run ...
	if( index_testSelection >= testRecordSize ) {
	  // ... then add it to the list of remaining tests
	  remainingTests.add_testid( test->GetID() );
	}  // End if
	++index_testSelection;
      }  // End if
    }  // End for
  }  // End for

  // Use the ProcessController to kill the test process.  This will also bring the
  // ProcessController to a stop.  It in turn brings other threads to a stop.
  DBG_MSG( "Killing the test process and waiting for process controller to terminate" );
  processController->Kill( );
  this->WaitForProcessControllerToTerminate();

  return this->context->GetStateFactory()->CreateResetState( &remainingTests );
}

ICoreState* RunningState::HandleSendResultPacketTask(SendResultPacketTask* Task)
{
	GTestServiceBuffers::TestResult testResult;

	const int id ( Task->GetTestModel()->GetID() );
	testResult.set_id(id);
	testResult.set_iteration(Task->GetIteration());
	testResult.set_success(Task->GetTestModel()->GetTestResult(Task->GetIteration()));

	// Add this test to the test record.  Should there be a request for a power cycle,
	// see HandleSendResetPacketTask, we will be able to write an up to date
	// TestSelection.pb file
	testRecord.add_testid( id );

	const int serializedSize = testResult.ByteSize();
	char serializedTestResult[ serializedSize ];
	testResult.SerializeToArray(serializedTestResult, serializedSize);

	AdapterPacket packet(
		(char)ID_UP_TESTRESULT,
		0 , 0,
		0,
		serializedSize,
		serializedTestResult,
		0);

	this->context->GetConnectionManager()->Multicast(packet);

	return this;
}

ICoreState* RunningState::HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task)
{
	SendTestsDonePacket();
	this->WaitForProcessControllerToTerminate();

	ICoreState* newIdleState = this->context->GetStateFactory()->CreateIdleState();

	return newIdleState;
}

TestModel* RunningState::SendResultPacketForCrashedTest(int crashedTestID)
{
	std::stringstream dbg;
	dbg << crashedTestID
	    << " about to send a result packet";
	DBG_MSG( dbg.str().c_str() );

	int iteration = this->context->GetTestRunModel()->GetIteration();
	TestModel* crashedTest = this->context->GetTestRunModel()->GetTestByID(crashedTestID);

	crashedTest->AppendTestResult(iteration, false);
	SendResultPacketTask* sendResultTask = new SendResultPacketTask(crashedTest, iteration, FAILED);

	this->HandleSendResultPacketTask(sendResultTask);

	delete sendResultTask;
	return crashedTest;
}

ICoreState* RunningState::HandleReactOnCrashTask(ReactOnCrashTask* Task)
{
	ICoreState* nextState = this;

	this->WaitForProcessControllerToTerminate();

	delete this->stdoutHandler;
	delete this->stdoutTextForwarder;
	delete this->stderrTextForwarder;

	delete this->processController;

	this->stdoutHandler = NULL;
	this->stdoutTextForwarder = NULL;
	this->stderrTextForwarder = NULL;

	this->processController = NULL;

	int crashedTestID = this->context->GetTestRunModel()->GetCurrentTestID();
	TestModel* crashedTest = NULL;

	std::stringstream dbg;
	dbg << crashedTestID
	    << " has crashed";
	DBG_MSG( dbg.str().c_str() );

	if (crashedTestID > -1)
	{
		crashedTest = SendResultPacketForCrashedTest(crashedTestID);
	}

	if (this->context->GetUsePersiFeaturesFlag())
	{
		/*leave the recovery to the persi feature -> just restart*/
	        DBG_MSG( " restart by calling RunProcessController()" );

		RunProcessController();
	} else {
		/* we do not have the persi feature -> very simple solution: just stop the
		 * execution and send an "all tests done" packet
		 */

		SendTestsDonePacket();
		nextState = this->context->GetStateFactory()->CreateIdleState();
	}

	return nextState;
}

ICoreState* RunningState::HandleAbortTestsTask(AbortTestsTask* Task)
{
	try {
		this->processController->Kill();
		this->stdoutHandler->WaitForProcessTestResultCompleted();
	} catch (std::runtime_error &e)
	{ }

	this->processController->WaitForProcessTermination();
	this->SendAck(Task, ID_UP_ACK_ABORT, true);

	ICoreState* newIdleState = this->context->GetStateFactory()->CreateIdleState();
	return newIdleState;
}

void RunningState::WaitForProcessControllerToTerminate() {
	this->stdoutHandler->WaitForProcessTestResultCompleted();
	this->processController->WaitForProcessTermination();
}

ICoreState* RunningState::HandleQuitTask(QuitTask* Task)
{
	try {
		this->processController->Kill();
	} catch (std::runtime_error &e)
	{ }

	WaitForProcessControllerToTerminate();
	return NULL;
}

GTEST_SERVICE_STATE RunningState::GetTypeOfState()
{
	return STATE_RUNNING_TESTS;
}

RunningState::~RunningState()
{
	if (this->stdoutHandler)
		delete this->stdoutHandler;

	if (this->stdoutTextForwarder)
		delete this->stdoutTextForwarder;

	if (this->stderrTextForwarder)
		delete this->stderrTextForwarder;

	if (this->processController)
		delete this->processController;

	if (this->configModel)
		delete this->configModel;
}
