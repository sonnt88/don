/*
 * SendVersionTask.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/Tasks/SendVersionTask.h>

SendVersionTask::SendVersionTask(IConnection* Connection, uint32_t RequestSerial) :
	BaseReplyTask(Connection, RequestSerial, TASK_SEND_VERSION_TASK)
{
}
