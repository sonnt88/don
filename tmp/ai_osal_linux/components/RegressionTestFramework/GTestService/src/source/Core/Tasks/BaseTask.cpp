/*
 * GTestCoreTask.cpp
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#include <Core/Tasks/BaseTask.h>

GTEST_CORE_TASKS BaseTask::GetTypeOfTask()
{
	return this->typeOfTask;
}

BaseTask::BaseTask(GTEST_CORE_TASKS TypeOfTask)
{
	this->typeOfTask = TypeOfTask;
}
