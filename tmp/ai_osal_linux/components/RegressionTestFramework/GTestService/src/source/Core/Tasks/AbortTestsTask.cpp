/*
 * AbortTestsTask.cpp
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#include <Core/Tasks/AbortTestsTask.h>

AbortTestsTask::AbortTestsTask(IConnection* Connection, uint32_t RequestSerial) :
	BaseReplyTask(Connection, RequestSerial, TASK_ABORT_TESTS)
{
}
