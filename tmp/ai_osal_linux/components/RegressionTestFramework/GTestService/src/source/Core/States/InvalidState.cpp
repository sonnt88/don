/*
 * InvalidState.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/States/InvalidState.h>

InvalidState::InvalidState()
{
}

ICoreState* InvalidState::HandleSendTestListTask(SendTestListTask* Task)
{
	return this;
}

ICoreState* InvalidState::HandleSendCurrentStateTask(SendCurrentStateTask* Task)
{
	this->SendCurrentState(Task);
	return this;
}

ICoreState* InvalidState::HandleExecTestsTask(ExecTestsTask* Task)
{
	this->SendAck(Task, ID_UP_ACK_TESTRUN, false);
	return this;
}

ICoreState* InvalidState::HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task)
{
	throw std::runtime_error("Cannot send a text output packet in invalid state.");
}

ICoreState* InvalidState::HandleSendResultPacketTask(SendResultPacketTask* Task)
{
	throw std::runtime_error("Cannot send a result packet in invalid state.");
}

ICoreState* InvalidState::HandleSendResetPacketTask(SendResetPacketTask* Task)
{
	throw std::runtime_error("Cannot send a reset packet in invalid state.");
}

ICoreState* InvalidState::HandleSendAllResultsTask(SendAllResultsTask* Task)
{
	throw std::runtime_error("Cannot send all result packets in invalid state.");
}

ICoreState* InvalidState::HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task)
{
	throw std::runtime_error("Cannot send a test done packet in invalid state.");
}

ICoreState* InvalidState::HandleReactOnCrashTask(ReactOnCrashTask* Task)
{
	throw std::runtime_error("There is nothing that could have been crashed in invalid state.");
}

ICoreState* InvalidState::HandleAbortTestsTask(AbortTestsTask* Task)
{
	this->SendAck(Task, ID_UP_ACK_ABORT, false);
	return this;
}

ICoreState* InvalidState::HandleQuitTask(QuitTask* Task)
{
	return NULL;
}

GTEST_SERVICE_STATE InvalidState::GetTypeOfState()
{
	return STATE_INVALID;
}

