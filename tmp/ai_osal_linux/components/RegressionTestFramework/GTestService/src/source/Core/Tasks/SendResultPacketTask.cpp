/*
 * ResultPackageTask.cpp
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#include <Core/Tasks/SendResultPacketTask.h>

TEST_STATE SendResultPacketTask::GetTestState()
{
	return this->state;
}

TestModel* SendResultPacketTask::GetTestModel()
{
	return this->test;
}

int SendResultPacketTask::GetIteration()
{
	return this->iteration;
}

SendResultPacketTask::SendResultPacketTask(TestModel* Test, int Iteration, TEST_STATE TestState) :
		BaseTask(TASK_SEND_RESULT_PACKET)
{
	this->test = Test;
	this->state = TestState;
	this->iteration = Iteration;
}

