/*
 * RunTestsTask.cpp
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#include <Core/Tasks/ExecTestsTask.h>
#include <stdexcept>

ExecTestsTask::ExecTestsTask(IConnection* Connection, uint32_t RequestSerial, GTestServiceBuffers::TestLaunch* TestSelection) :
	BaseReplyTask(Connection, RequestSerial, TASK_EXEC_TESTS)
{
	if (TestSelection == NULL)
		throw std::invalid_argument("TestSelection cannot be null.");

	this->testSelection = TestSelection;
}

GTestServiceBuffers::TestLaunch* ExecTestsTask::GetTestSelectionPB()
{
	return this->testSelection;
}

ExecTestsTask::~ExecTestsTask()
{
	delete this->testSelection;
}
