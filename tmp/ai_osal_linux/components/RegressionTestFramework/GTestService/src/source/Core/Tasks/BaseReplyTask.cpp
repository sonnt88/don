/*
 * BaseReplyTask.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/Tasks/BaseReplyTask.h>
#include <stdexcept>

BaseReplyTask::BaseReplyTask(IConnection* Connection,
		uint32_t RequestSerial,
		GTEST_CORE_TASKS TypeOfTask) :
		BaseTask(TypeOfTask)
{
	if (Connection == NULL)
		throw std::invalid_argument("Connection cannot be NULL.");

	this->connection = Connection;
	this->requestSerial = RequestSerial;
}

uint32_t BaseReplyTask::GetRequestSerial()
{
	return this->requestSerial;
}

IConnection* BaseReplyTask::GetConnection()
{
	return this->connection;
}

