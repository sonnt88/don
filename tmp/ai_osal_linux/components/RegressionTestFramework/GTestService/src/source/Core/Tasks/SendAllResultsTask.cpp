/*
 * SendAllResultsTask.cpp
 *
 *  Created on: 20.11.2012
 *      Author: oto4hi
 */

#include <Core/Tasks/SendAllResultsTask.h>

SendAllResultsTask::SendAllResultsTask(IConnection* Connection, uint32_t RequestSerial)
	: BaseReplyTask(Connection, RequestSerial, TASK_SEND_ALL_RESULTS)
{
}
