/*
 * SendCurrentTaskState.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/Tasks/SendCurrentStateTask.h>

SendCurrentStateTask::SendCurrentStateTask(IConnection* Connection, uint32_t RequestSerial) :
	BaseReplyTask(Connection, RequestSerial, TASK_SEND_CURRENT_STATE)
{
}

