/*
 * SendTestOutputPacketTask.cpp
 *
 *  Created on: 23.10.2012
 *      Author: oto4hi
 */

#include <Core/Tasks/SendTestOutputPacketTask.h>

SendTestOutputPacketTask::SendTestOutputPacketTask(OUTPUT_SOURCE OutputSource, std::string Line, uint32_t Timestamp) :
	BaseTask(TASK_SEND_TEST_OUTPUT_PACKET)
{
	this->outputSource = OutputSource;
	this->line         = Line;
	this->timestamp    = Timestamp;
}

OUTPUT_SOURCE SendTestOutputPacketTask::GetOutputSource()
{
	return this->outputSource;
}

std::string SendTestOutputPacketTask::GetLine()
{
	return this->line;
}

uint32_t SendTestOutputPacketTask::GetTimestamp()
{
	return this->timestamp;
}


SendTestOutputPacketTask::~SendTestOutputPacketTask() {
	// TODO Auto-generated destructor stub
}

