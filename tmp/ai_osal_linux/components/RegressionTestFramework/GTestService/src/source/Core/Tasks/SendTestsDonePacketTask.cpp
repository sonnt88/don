/*
 * SendTestsDonePacket.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/Tasks/SendTestsDonePacketTask.h>

SendTestsDonePacketTask::SendTestsDonePacketTask() : BaseTask(TASK_SEND_TESTS_DONE_PACKET)
{
}
