/*
 * ResumableCoreStateFactory.cpp
 *
 *  Created on: 01.03.2013
 *      Author: oto4hi
 */

#include <Core/States/ResumableCoreStateFactory.h>

#include <unistd.h>
#include <fstream>
#include <iostream>
#include <sstream>

#include <Utils/dbg_msg.h>

const char* ResumableCoreStateFactory::persistentTestSelectionFile = "TestSelection.pb";

ResumableCoreStateFactory::ResumableCoreStateFactory(ICoreStateFactory* Decoratee, GTestServiceCore * context_) :
  context( context_ )
{
	if (!Decoratee)
		throw std::invalid_argument("Decoratee cannot be null.");

	this->decoratee = Decoratee;
}

GTestServiceBuffers::TestLaunch* ResumableCoreStateFactory::reloadTestSelectionFromFile()
{
	std::fstream testSelectionIn(
			ResumableCoreStateFactory::persistentTestSelectionFile,
			std::ios::in | std::ios::binary
			);

	if (!testSelectionIn.good())
	{
	        std::stringstream dbg;
	        dbg << " unable to read from "
	            << ResumableCoreStateFactory::persistentTestSelectionFile;
	        DBG_MSG( dbg.str().c_str() );

		return NULL;
	}

	std::stringstream dbg;
	dbg << " about to read from "
	    << ResumableCoreStateFactory::persistentTestSelectionFile;
	DBG_MSG( dbg.str().c_str() );

	GTestServiceBuffers::TestLaunch* TestSelection = new GTestServiceBuffers::TestLaunch();

	if (!TestSelection->ParseFromIstream(&testSelectionIn)) {
		delete TestSelection;
		throw std::runtime_error("failed to parse persistent test selection file.");
	}
	return TestSelection;
}

void ResumableCoreStateFactory::deleteTestSelectionFile()
{
        std::stringstream dbg;
	dbg << "about to delete "
	    << ResumableCoreStateFactory::persistentTestSelectionFile;
	DBG_MSG( dbg.str().c_str() );

	remove(ResumableCoreStateFactory::persistentTestSelectionFile);
}

void ResumableCoreStateFactory::saveTestSelectionToFile(GTestServiceBuffers::TestLaunch const * const TestSelection)
{
        std::stringstream dbg;
	std::fstream testSelectionOut(
			ResumableCoreStateFactory::persistentTestSelectionFile,
			std::ios::out | std::ios::binary | std::ios::trunc
			);

	dbg << " about to write to "
	    << ResumableCoreStateFactory::persistentTestSelectionFile;
	DBG_MSG( dbg.str().c_str() );

	if (!TestSelection->SerializeToOstream(&testSelectionOut))
		throw std::runtime_error("saving test selection failed.\n");
	testSelectionOut.close();

	sync();
}

ICoreState* ResumableCoreStateFactory::CreateInitialState()
{
        std::stringstream dbg;

	DBG_MSG( " try to load test selection from file" );
	GTestServiceBuffers::TestLaunch* storedTestSelection = this -> reloadTestSelectionFromFile();
	if (storedTestSelection) {

	        dbg << " found storedTestSelection "
		    << "will create running state";
	        DBG_MSG( dbg.str().c_str() );

		return this->CreateRunningState(storedTestSelection);
	}
	else {

	        dbg << " no storedTestSelection "
		    << "will create idle state";
		DBG_MSG( dbg.str().c_str() );

		return this->CreateIdleState();
	}
}

ICoreState* ResumableCoreStateFactory::CreateInvalidState()
{
	this->deleteTestSelectionFile();
	return this->decoratee->CreateInvalidState();
}

ICoreState* ResumableCoreStateFactory::CreateIdleState()
{
	this->deleteTestSelectionFile();
	return this->decoratee->CreateIdleState();
}

ICoreState* ResumableCoreStateFactory::CreateResetState(GTestServiceBuffers::TestLaunch const * const remainingTests)
{
  /* Produces rather a lot of text on the screen ... 

  std::stringstream dbg;
  dbg << " remainingTests.testid_size() = "
      << remainingTests->testid_size();
  DBG_MSG( dbg.str().c_str() );
  for( int i = 0; i < remainingTests->testid_size(); ++i ) {
    std::stringstream dbg;
    dbg << " remaining test ID "
        << remainingTests->testid( i );
    DBG_MSG( dbg.str().c_str() );
  }  // end for

  /* */

  this->deleteTestSelectionFile( );
  this->saveTestSelectionToFile( remainingTests );

  // Should also send ID_UP_PCYCLE_REQUEST message to client
  // It belongs in this class, because only we know that the programme can resume again.
  // Tell client (eg. GTestAdapter.exe) that power cycle is required: -
  AdapterPacket packet((char) ID_UP_PCYCLE_REQUEST, 0, 0, 0, 0, NULL, 0);
  this->context->GetConnectionManager()->Multicast(packet);

  // It is not up to us to delete remainingTests
  return this->decoratee->CreateResetState( remainingTests );
}

ICoreState* ResumableCoreStateFactory::CreateRunningState(GTestServiceBuffers::TestLaunch* TestSelection)
{
	this->saveTestSelectionToFile(TestSelection);
	return this->decoratee->CreateRunningState(TestSelection);
}

ResumableCoreStateFactory::~ResumableCoreStateFactory()
{
	delete this->decoratee;
}
