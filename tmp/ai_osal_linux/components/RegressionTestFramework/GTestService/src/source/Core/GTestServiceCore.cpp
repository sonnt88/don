/*
 * GTestServiceCore.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/GTestServiceCore.h>
#include <GTestServiceBuffers.pb.h>
#include <Models/GTestConfigurationModel.h>
#include <ProcessController/GTestProcessController.h>
#include <ProcessController/GTestListOutputHandler.h>
#include <AdapterConnection/AdapterConnectionPushDecoratorFactory.h>

#include <Core/Tasks/SendVersionTask.h>
#include <Core/Tasks/SendCurrentStateTask.h>
#include <Core/Tasks/SendTestListTask.h>
#include <Core/Tasks/ExecTestsTask.h>
#include <Core/Tasks/AbortTestsTask.h>

#include <Core/States/CoreStateFactory.h>
#include <Core/States/LoggingCoreStateFactory.h>
#include <Core/States/ResumableCoreStateFactory.h>

#include <Utils/dbg_msg.h>
#include <sstream>
#include <unistd.h>

GTestServiceCore::GTestServiceCore(std::string GTestPath, std::string WorkingDir, int PortNo, bool UsePersiFeatures, IPFilter* IpFilter)
{
	this->threaded = false;
	this->coreThread = 0;

	this->usePersiFeatures = UsePersiFeatures;
	DBG_MSG( "Creating CoreStateFactory" );
	this->stateFactory = new CoreStateFactory(this);

	/*if the persi feature is used make the test run resumable*/
	if (UsePersiFeatures) {
	  DBG_MSG( "Creating ResumableCoreStateFactory( CoreStateFactory )" );
	  this->stateFactory = new ResumableCoreStateFactory(this->stateFactory, this);
	}

	this->workingDir = WorkingDir;

#ifndef TESTAPP

	/*comment the decorator to switch off logging*/
	DBG_MSG( "Creating LoggingCoreStateFactory( stateFactory )" );
	this->stateFactory = new LoggingCoreStateFactory(this->stateFactory, this);

#endif

	pthread_mutex_init(&(this->startConditionMutex), NULL);
	pthread_cond_init(&(this->startCondition), NULL);

	bool invalidGTestPath = false;
	if (GTestPath.empty())
		invalidGTestPath = true;
	else if (access(GTestPath.c_str(), X_OK))
		invalidGTestPath = true;

	this->testRunModel = NULL;
	this->cmanager = NULL;

	if (invalidGTestPath)
		this->state = this->stateFactory->CreateInvalidState();
	else
	{
		this->gtestPath = GTestPath;

		DBG_MSG( "Creating GTestConfigurationModel" );
		GTestConfigurationModel listTestsConfig(this->gtestPath, this->workingDir, "*", 0, 0, GTEST_CONF_LIST_ONLY);

		DBG_MSG( "Creating GTestProcessController" );
		GTestProcessController listTestsController(listTestsConfig);

		DBG_MSG( "Creating GTestListOutputHandler" );
		GTestListOutputHandler listTestsOutputHandler;

		DBG_MSG( "Registering output handler with GTestProcessController" );
		listTestsController.RegisterStdoutOutputHandler(&listTestsOutputHandler);

		DBG_MSG( "Now calling GTestProcessController::Run()" );
		listTestsController.Run();

		DBG_MSG(" Get test run model from GTestListOutputHandler" );
		this->testRunModel = listTestsOutputHandler.GetTestRunModel();
		DBG_MSG(" Got test run model, now Update ID map" );
		this->testRunModel->UpdateTestIDMap();

		this->state = this->stateFactory->CreateInitialState();
	}

	DBG_MSG( "Creating AdapterConnectionPushDecoratorFactory" );
	IConnectionFactory* connectionFactory = new AdapterConnectionPushDecoratorFactory(this);
	DBG_MSG( "Creating ConnectionManager, with association to AdapterConnectionPushDecoratorFactory" );
	this->cmanager = new ConnectionManager(PortNo, connectionFactory, IpFilter);
}

bool GTestServiceCore::GetUsePersiFeaturesFlag()
{
	return this->usePersiFeatures;
}

void GTestServiceCore::EnqueueTask(BaseTask* Task)
{
	this->taskQueue.PushBack(Task);
}

std::string GTestServiceCore::GetGTestPath()
{
	return this->gtestPath;
}

std::string GTestServiceCore::GetWorkingDir()
{
	return this->workingDir;
}

ICoreStateFactory* GTestServiceCore::GetStateFactory()
{
	return this->stateFactory;
}

void GTestServiceCore::OnPacketReceive(IConnection* Connection, AdapterPacket* Packet)
{
	if (!Packet)
		return; //the connection has bee closed: currently this won't be handled

	GTestServiceBuffers::TestLaunch* testSelection;

	switch (Packet->GetID())
	{
	case ID_DOWN_GET_VERSION:
	  DBG_MSG( "enqueueing send version task" );
		this->EnqueueTask( new SendVersionTask( Connection, Packet->GetSerial() ) );
		break;

	case ID_DOWN_GET_STATE:
	  DBG_MSG( "enqueueing send current state task" );
		this->EnqueueTask( new SendCurrentStateTask(Connection, Packet->GetSerial() ) );
		break;

	case ID_DOWN_GET_TESTLIST:
	        DBG_MSG( "enqueueing get test list task" );
		this->EnqueueTask( new SendTestListTask(Connection, Packet->GetSerial() ) );
		break;

	case ID_DOWN_RUN_TESTS:
		DBG_MSG( "getting test selection..." );
		testSelection = new GTestServiceBuffers::TestLaunch();
		testSelection->ParseFromArray(Packet->GetPayload(), Packet->GetPayloadSize());
		DBG_MSG( "enqueueing exec tests task" );
		this->EnqueueTask( new ExecTestsTask(Connection, Packet->GetSerial(), testSelection) );
		break;

	case ID_DOWN_ABORT_TESTRUN:
	  DBG_MSG( "enqueueing abort tests task" );
		this->EnqueueTask( new AbortTestsTask(Connection, Packet->GetSerial()) );
		break;

	case ID_DOWN_GET_ALL_RESULTS:
	  DBG_MSG( "enqueueing send all results task" );
		this->EnqueueTask( new SendAllResultsTask(Connection, Packet->GetSerial()) );
		break;

	default:
		std::cerr << "unexpected packet id -> closing connection ...\n";
		Connection->Close();
		break;
	}
}

void GTestServiceCore::setState(ICoreState* NewState)
{
	delete this->state;
	this->state = NewState;
}

TestRunModel* GTestServiceCore::GetTestRunModel()
{
	return this->testRunModel;
}

int GTestServiceCore::HandleTask(BaseTask* Task)
{
	ICoreState* newState = this->state->HandleTask(Task);

	if (newState != this->state)
		setState(newState);

	if (this->state == NULL)
		return 1;
	else
		return 0;
}

int GTestServiceCore::Run()
{
	bool leave = false;

	this->cmanager->Start();

	if (this->threaded)
	{
		pthread_mutex_lock(&(this->startConditionMutex));
		pthread_cond_signal(&(this->startCondition));
		pthread_mutex_unlock(&(this->startConditionMutex));
	}

	while (!leave)
	{
		this->taskQueue.BlockUntilNotEmpty();
		BaseTask* task = this->taskQueue.PopFirst();

		if (this->HandleTask(task) != 0)
			leave = true;

		delete task;
	}

	this->cmanager->Stop();

	return 0;
}

void* GTestServiceCore::threadFunc(void* thisPtr)
{
        DBG_MSG( "About to run GTestServiceCore::Run() in its own thread" );

	GTestServiceCore* pthis = (GTestServiceCore*)thisPtr;
	pthis->Run();

	pthread_exit(0);
}

void GTestServiceCore::RunThreaded()
{
	this->threaded = true;

	pthread_mutex_lock(&(this->startConditionMutex));

	DBG_MSG("creating thread coreThread");

	if (pthread_create(&(this->coreThread), NULL, GTestServiceCore::threadFunc, (void*)this))
		throw std::runtime_error("Failed to create the core thread.");

	pthread_cond_wait(&(this->startCondition), &(this->startConditionMutex));
	pthread_mutex_unlock(&(this->startConditionMutex));
}

void GTestServiceCore::Stop()
{
	this->taskQueue.PushBack(new QuitTask());

	if (this->threaded)
	{
		DBG_MSG("joining thread coreThread");
		int errCode = pthread_join(this->coreThread, NULL);
		if (errCode)
		{
			std::stringstream sstream;
			sstream << "Joining Core thread failed. error code is " << strerror(errCode);
			throw std::runtime_error(sstream.str().c_str());
		}
	}

	this->threaded = false;
}

ConnectionManager* GTestServiceCore::GetConnectionManager()
{
	return this->cmanager;
}

GTestServiceCore::~GTestServiceCore()
{
	 if (this->testRunModel)
		 delete testRunModel;

	 if (this->cmanager)
		 delete this->cmanager;

	 if (this->state)
		 delete this->state;

	 delete this->stateFactory;

	 pthread_mutex_destroy(&(this->startConditionMutex));
	 pthread_cond_destroy(&(this->startCondition));
}
