/*
 * ReactToTestHangTask.cpp
 *
 *  Created on: 10 Oct, 2014
 *      Author: aga2hi
 */

#include <Core/Tasks/ReactToTestHangTask.h>

ReactToTestHangTask::ReactToTestHangTask() : BaseTask(TASK_TEST_HANG)
{
	// Nothing more to do -
	// object acts as a signal, but carries no information itself
}
