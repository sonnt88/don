/*
 * IdleState.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/States/IdleState.h>
#include <Core/States/RunningState.h>

IdleState::IdleState(GTestServiceCore* Context) : BaseValidState(Context)
{
}

ICoreState* IdleState::HandleSendCurrentStateTask(SendCurrentStateTask* Task)
{
	this->SendCurrentState(Task);
	return this;
}

ICoreState* IdleState::HandleExecTestsTask(ExecTestsTask* Task)
{
	this->SendAck(Task, ID_UP_ACK_TESTRUN, true);
	ICoreState* newRunningState = this->context->GetStateFactory()->CreateRunningState(Task->GetTestSelectionPB());

	return newRunningState;
}

ICoreState* IdleState::HandleSendResultPacketTask(SendResultPacketTask* Task)
{
	throw std::runtime_error("send result packet in idle mode is not possible.");
}

ICoreState* IdleState::HandleSendResetPacketTask(SendResetPacketTask* Task)
{
	throw std::runtime_error("send reset packet in idle mode is not possible.");
}

ICoreState* IdleState::HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task)
{
	throw std::runtime_error("send tests done packet in idle mode is not possible.");
}

ICoreState* IdleState::HandleReactOnCrashTask(ReactOnCrashTask* Task)
{
	//might come from a process abortion
	return this;
}

ICoreState* IdleState::HandleAbortTestsTask(AbortTestsTask* Task)
{
	this->SendAck(Task, ID_UP_ACK_ABORT, false);
	return this;
}

ICoreState* IdleState::HandleQuitTask(QuitTask* Task)
{
	return NULL;
}

GTEST_SERVICE_STATE IdleState::GetTypeOfState()
{
	return STATE_IDLE;
}
