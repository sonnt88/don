/*
 * ResetPacketTask.cpp
 *
 *  Created on: 09 Jan, 2014
 *      Author: aga2hi
 */

#include <Core/Tasks/SendResetPacketTask.h>

TEST_STATE SendResetPacketTask::GetTestState()
{
	return this->state;
}

TestModel* SendResetPacketTask::GetTestModel()
{
	return this->test;
}

int SendResetPacketTask::GetIteration()
{
	return this->iteration;
}

SendResetPacketTask::SendResetPacketTask(TestModel* Test, int Iteration, TEST_STATE TestState) :
		BaseTask(TASK_SEND_RESET_PACKET)
{
	this->test = Test;
	this->state = TestState;
	this->iteration = Iteration;
}

