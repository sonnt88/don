/*
 * CoreStateFactory.cpp
 *
 *  Created on: 27.02.2013
 *      Author: oto4hi
 */

#include <Core/States/CoreStateFactory.h>
#include <Core/States/IdleState.h>
#include <Core/States/InvalidState.h>
#include <Core/States/RunningState.h>
#include <Core/States/ResetState.h>

CoreStateFactory::CoreStateFactory(GTestServiceCore* Context)
{
	if (!Context)
		throw std::invalid_argument("CoreStateFactory.cpp: Context may not be null.");

	this->context = Context;
}

ICoreState* CoreStateFactory::CreateInitialState()
{
	return this->CreateIdleState();
}

ICoreState* CoreStateFactory::CreateInvalidState()
{
	return new InvalidState();
}

ICoreState* CoreStateFactory::CreateIdleState()
{
	return new IdleState(this->context);
}

ICoreState* CoreStateFactory::CreateResetState(GTestServiceBuffers::TestLaunch const * const remainingTests)
{
        // It is not up to us to delete remainingTests
	return new ResetState(this->context);
}

ICoreState* CoreStateFactory::CreateRunningState(GTestServiceBuffers::TestLaunch* TestSelection)
{
	return new RunningState(this->context, TestSelection);
}

CoreStateFactory::~CoreStateFactory() {
	// TODO Auto-generated destructor stub
}

