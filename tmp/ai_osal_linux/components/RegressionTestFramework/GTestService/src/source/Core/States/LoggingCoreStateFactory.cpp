/*
 * LoggingCoreStateFactory.cpp
 *
 *  Created on: 27.02.2013
 *      Author: oto4hi
 */

#include <Core/States/LoggingCoreStateDecorator.h>
#include <Core/States/LoggingCoreStateFactory.h>

LoggingCoreStateFactory::LoggingCoreStateFactory(ICoreStateFactory* Decoratee, GTestServiceCore* Context)
{
	if (!Decoratee)
		throw std::invalid_argument("Decoratee cannot be null.");

	if (!Context)
		throw std::invalid_argument("Context cannot be null.");

	this->decoratee = Decoratee;
	this->context = Context;
}

ICoreState* LoggingCoreStateFactory::CreateInitialState()
{
	return new LoggingCoreStateDecorator(this->decoratee->CreateInitialState(), this->context);
}

ICoreState* LoggingCoreStateFactory::CreateInvalidState()
{
	return new LoggingCoreStateDecorator(this->decoratee->CreateInvalidState(), this->context);
}

ICoreState* LoggingCoreStateFactory::CreateIdleState()
{
	return new LoggingCoreStateDecorator(this->decoratee->CreateIdleState(), this->context);
}

ICoreState* LoggingCoreStateFactory::CreateResetState(GTestServiceBuffers::TestLaunch const * const remainingTests)
{
	return new LoggingCoreStateDecorator(this->decoratee->CreateResetState(remainingTests), this->context);
}

ICoreState* LoggingCoreStateFactory::CreateRunningState(GTestServiceBuffers::TestLaunch* TestSelection)
{
	return new LoggingCoreStateDecorator(this->decoratee->CreateRunningState(TestSelection), this->context);
}

LoggingCoreStateFactory::~LoggingCoreStateFactory()
{
	delete this->decoratee;
}

