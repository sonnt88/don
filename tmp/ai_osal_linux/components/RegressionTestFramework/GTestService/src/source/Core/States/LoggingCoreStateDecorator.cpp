/*
 * LoggingCoreStateDecorator.cpp
 *
 *  Created on: 27.02.2013
 *      Author: oto4hi
 */

#include <stdexcept>
#include <iostream>
#include <Core/States/LoggingCoreStateDecorator.h>

LoggingCoreStateDecorator::LoggingCoreStateDecorator(ICoreState* Decoratee, GTestServiceCore* Context) {
	if (!Decoratee)
		throw std::invalid_argument("Decoratee cannot be null.");

	if (!Context)
		throw std::invalid_argument("Context cannot be null.");

	this->decoratee = Decoratee;
	this->context = Context;

	std::cerr << "changing state to " << this->StateTypeAsString() << "\n";
}

ICoreState* LoggingCoreStateDecorator::chooseNextStatePointer(ICoreState* NextStateFromHandler)
{
	return (NextStateFromHandler == this->decoratee) ? this : NextStateFromHandler;
}

std::string LoggingCoreStateDecorator::StateTypeAsString()
{
	std::string stateAsString;
	switch (this->decoratee->GetTypeOfState())
	{
	case STATE_IDLE:
		stateAsString = "STATE_IDLE";
		break;
	case STATE_RUNNING_TESTS:
		stateAsString = "STATE_RUNNING_TESTS";
		break;
	case STATE_INVALID:
		stateAsString = "STATE_INVALID";
		break;
	case STATE_RESET:
		stateAsString = "STATE_RESET";
		break;
	default:
		throw std::runtime_error("LoggingCoreStateDecorator::StateTypeAsString(): unknown state type");
	}
	return stateAsString;
}

ICoreState* LoggingCoreStateDecorator::HandleSendCurrentStateTask(SendCurrentStateTask* Task)
{
	std::cerr << this->StateTypeAsString() << ": sending out current state" << "\n";
	ICoreState* nextState = decoratee->HandleSendCurrentStateTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleSendTestListTask(SendTestListTask* Task)
{
	std::cerr << this->StateTypeAsString() << ": sending out test list" << "\n";
	ICoreState* nextState = decoratee->HandleSendTestListTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleExecTestsTask(ExecTestsTask* Task)
{
	std::cerr << this->StateTypeAsString() << ": handling test execute task" << "\n";
	ICoreState* nextState = decoratee->HandleExecTestsTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleSendResetPacketTask(SendResetPacketTask* Task) {
	TestModel* testModel = Task->GetTestModel();
	int testID = testModel->GetID();
	TestCaseModel* testCaseModel = this->context->GetTestRunModel()->GetTestCaseByTestID(testID);

	std::cerr << this->StateTypeAsString() << ": sending out test reset packet (test name: " <<
			testCaseModel->GetName() << "." <<
	                testModel->GetName() << ")\n";

	ICoreState* nextState = decoratee->HandleSendResetPacketTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleSendResultPacketTask(SendResultPacketTask* Task)
{
	TestModel* testModel = Task->GetTestModel();
	int testID = testModel->GetID();
	TestCaseModel* testCaseModel = this->context->GetTestRunModel()->GetTestCaseByTestID(testID);

	int iteration = Task->GetIteration();
	bool success = Task->GetTestModel()->GetTestResult(iteration);

	std::cerr << this->StateTypeAsString() << ": sending out test result packet (test name: " <<
			testCaseModel->GetName() << "." <<
			testModel->GetName() <<
			", test ID: " << testID <<
			", iteration: " << iteration <<
			", success: " << success << ")\n";

	ICoreState* nextState = decoratee->HandleSendResultPacketTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task)
{
	std::cerr << this->StateTypeAsString() << ": sending out tests done packet" << "\n";
	ICoreState* nextState = decoratee->HandleSendTestsDonePacketTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleSendAllResultsTask(SendAllResultsTask* Task)
{
	std::cerr << this->StateTypeAsString() << ": sending out all test results" << "\n";
	ICoreState* nextState = decoratee->HandleSendAllResultsTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task)
{
	/*
	std::string source = (Task->GetOutputSource() == OUTPUT_SOURCE_STDOUT) ? "STDOUT" : "STDERR";
	std::string line = Task->GetLine();
	line.erase(line.find_last_not_of(" \n\r\t")+1);
	std::cerr << this->StateTypeAsString() << ": sending out test output packet" << "[" << source << ": " << line << "]\n";
	*/
	ICoreState* nextState = decoratee->HandleSendTestOutputPacketTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleReactOnCrashTask(ReactOnCrashTask* Task)
{
	int crashedTestID = this->context->GetTestRunModel()->GetCurrentTestID();
	int iteration = this->context->GetTestRunModel()->GetIteration();

	std::cerr << this->StateTypeAsString() << ": handling test execution crash (ID: " << crashedTestID << ", iteration: " << iteration <<")\n";
	ICoreState* nextState = decoratee->HandleReactOnCrashTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleAbortTestsTask(AbortTestsTask* Task)
{
	std::cerr << this->StateTypeAsString() << ": handling test abort task" << "\n";
	ICoreState* nextState = decoratee->HandleAbortTestsTask(Task);
	return this->chooseNextStatePointer(nextState);
}

ICoreState* LoggingCoreStateDecorator::HandleQuitTask(QuitTask* Task)
{
	std::cerr << this->StateTypeAsString() << ": handling quit task" << "\n";
	ICoreState* nextState = decoratee->HandleQuitTask(Task);
	return this->chooseNextStatePointer(nextState);
}

GTEST_SERVICE_STATE LoggingCoreStateDecorator::GetTypeOfState()
{
	return this->decoratee->GetTypeOfState();
}

LoggingCoreStateDecorator::~LoggingCoreStateDecorator()
{
	delete this->decoratee;
}

