/*
 * ResetState.cpp
 *
 * Based on InvalidState.cpp
 *
 *  Created on: 30.01.2014
 *      Author: aga2hi
 */

#include <Core/States/ResetState.h>

ResetState::ResetState(GTestServiceCore* Context) : BaseValidState(Context)
{
}

ICoreState* ResetState::HandleSendCurrentStateTask(SendCurrentStateTask* Task)
{
	this->SendCurrentState(Task);
	return this;
}

ICoreState* ResetState::HandleExecTestsTask(ExecTestsTask* Task)
{
	this->SendAck(Task, ID_UP_ACK_TESTRUN, false);
	return this;
}

ICoreState* ResetState::HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task)
{
	throw std::runtime_error("ResetState.cpp: Cannot send a text output packet in reset state.");
	return this;
}

ICoreState* ResetState::HandleSendResultPacketTask(SendResultPacketTask* Task)
{
	throw std::runtime_error("ResetState.cpp: Cannot send a result packet in reset state.");
	return this;
}

ICoreState* ResetState::HandleSendResetPacketTask(SendResetPacketTask* Task)
{
	throw std::runtime_error("ResetState.cpp: Cannot send a reset packet in reset state.");
	return this;
}

ICoreState* ResetState::HandleSendAllResultsTask(SendAllResultsTask* Task)
{
	throw std::runtime_error("Cannot send all result packets in reset state.");
	return this;
}

ICoreState* ResetState::HandleSendTestsDonePacketTask(SendTestsDonePacketTask* Task)
{
	throw std::runtime_error("ResetState.cpp: Cannot send a test done packet in reset state.");
	return this;
}

ICoreState* ResetState::HandleReactOnCrashTask(ReactOnCrashTask* Task)
{
	return this;
}

ICoreState* ResetState::HandleAbortTestsTask(AbortTestsTask* Task)
{
	this->SendAck(Task, ID_UP_ACK_ABORT, false);
	return this;
}

ICoreState* ResetState::HandleQuitTask(QuitTask* Task)
{
	return NULL;
}

GTEST_SERVICE_STATE ResetState::GetTypeOfState()
{
	return STATE_RESET;
}

ICoreState* ResetState::HandleSendTestListTask(SendTestListTask* Task)
{
	throw std::runtime_error("ResetState.cpp: Cannot send a test list packet in reset state.");
	return this;
}

ICoreState* ResetState::SendResultPacketForCrashedTest(int crashedTestID)
{
	throw std::runtime_error("ResetState.cpp: Cannot send a result packet for crashed test in reset state.");
	return this;
}

