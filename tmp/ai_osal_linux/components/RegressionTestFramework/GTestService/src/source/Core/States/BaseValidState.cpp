/*
 * BaseValidState.cpp
 *
 *  Created on: 19.09.2012
 *      Author: oto4hi
 */

#include <Core/States/BaseValidState.h>
#include <version.h>

BaseValidState::BaseValidState(GTestServiceCore* Context)
{
	if (Context == NULL)
		throw std::invalid_argument("Context cannot be null.");

	this->context = Context;
}

ICoreState* BaseValidState::HandleSendTestListTask(SendTestListTask* Task)
{
	GTestServiceBuffers::AllTestCases pbTestRunModel;
	this->context->GetTestRunModel()->ToProtocolBuffer(&pbTestRunModel);
	int serializedSize = pbTestRunModel.ByteSize();
	char* serializedTestRunModel = (char*)malloc(serializedSize);
	pbTestRunModel.SerializeToArray(serializedTestRunModel, serializedSize);

	AdapterPacket packet(
			(char)ID_UP_TESTLIST,
			0 , 0,
			Task->GetRequestSerial(),
			serializedSize,
			serializedTestRunModel,
			Task->GetConnection()->GetID());

	Task->GetConnection()->Send(packet);

	free(serializedTestRunModel);

	return this;
}

ICoreState* BaseValidState::HandleSendAllResultsTask(SendAllResultsTask* Task)
{
	TestRunModel* testRunModel = this->context->GetTestRunModel();

	for (int testCaseIndex = 0; testCaseIndex < testRunModel->GetTestCaseCount(); testCaseIndex++)
	{
		TestCaseModel* testCaseModel = testRunModel->GetTestCase(testCaseIndex);
		for (int testIndex = 0; testIndex < testCaseModel->TestCount(); testIndex++)
		{
			TestModel* testModel = testCaseModel->GetTest(testIndex);
			testModel->LockInstance();
			for (std::map<int, bool>::iterator it = testModel->ResultsBegin(); it != testModel->ResultsEnd(); ++it)
			{
				GTestServiceBuffers::TestResult testResult;

				testResult.set_id(testModel->GetID());
				testResult.set_iteration(it->first);
				testResult.set_success(it->second);

				int serializedSize = testResult.ByteSize();
				char* serializedTestResult = (char*)malloc(serializedSize);
				testResult.SerializeToArray(serializedTestResult, serializedSize);

				AdapterPacket packet(
					(char)ID_UP_TESTRESULT,
					0 , 0,
					Task->GetRequestSerial(),
					serializedSize,
					serializedTestResult,
					Task->GetConnection()->GetID());

				Task->GetConnection()->Send(packet);
				free(serializedTestResult);
			}
			testModel->UnlockInstance();
		}
	}

	AdapterPacket packet(
		(char)ID_UP_ALL_RESULTS_DONE,
		0 , 0,
		Task->GetRequestSerial(),
		0,
		NULL,
		Task->GetConnection()->GetID());

	Task->GetConnection()->Send(packet);

	return this;
}

ICoreState* BaseValidState::HandleSendTestOutputPacketTask(SendTestOutputPacketTask* Task)
{
	GTestServiceBuffers::OutputSource pbOutputSource;

	switch (Task->GetOutputSource()) {
	case OUTPUT_SOURCE_STDOUT:
		pbOutputSource = GTestServiceBuffers::STDOUT;
		break;
	case OUTPUT_SOURCE_STDERR:
		pbOutputSource = GTestServiceBuffers::STDERR;
		break;
	default:
		throw std::invalid_argument("unexpected output source");
	}

	GTestServiceBuffers::TestOutput pbTestOutput;
	pbTestOutput.set_output(Task->GetLine());
	pbTestOutput.set_source(pbOutputSource);

	if (Task->GetTimestamp() != NO_TIMESTAMP)
		pbTestOutput.set_timestamp(Task->GetTimestamp());

	int serializedSize = pbTestOutput.ByteSize();

	char* serializedTestOutput = (char*)malloc(serializedSize);
	pbTestOutput.SerializeToArray(serializedTestOutput, serializedSize);

	AdapterPacket packet(
		(char)ID_UP_TEST_OUTPUT,
		0 , 0,
		0,
		serializedSize,
		serializedTestOutput,
		0);

	this->context->GetConnectionManager()->Multicast(packet);

	free(serializedTestOutput);

	return this;
}

BaseValidState::~BaseValidState()
{
}
