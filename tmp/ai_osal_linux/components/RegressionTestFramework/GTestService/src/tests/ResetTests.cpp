#include <persigtest.h>

TEST(PowerCycleRequestTests, PrePowerCycleTest) {
  ASSERT_EQ( 1, 1 )  << "If this fails you can't trust anything";
}

#ifdef ENABLE_POWER_CYCLE_TESTS
TEST(PowerCycleRequestTests, RequestPowerCycle) {
  // Give the power cycle request to the framework:
  REQUEST_POWER_CYCLE(PowerCycleRequestTests,RequestPowerCycle);

  // We won't return, because there will be a power cycle
}
#endif

TEST(PowerCycleRequestTests, PostPowerCycleTest) {
  ASSERT_EQ( 1, 1 )  << "If this fails you can't trust anything";
}
