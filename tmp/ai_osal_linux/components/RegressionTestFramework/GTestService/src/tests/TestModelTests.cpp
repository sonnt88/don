#include <persigtest.h>
#include <Models/TestModel.h>
#include <stdexcept>

TEST(TestModelTests, CheckExceptionOnNameEmpty)
{
    EXPECT_THROW(TestModel model("",0), std::invalid_argument);
}

TEST(TestModelTests, CheckNoExceptionOnNameNotEmpty)
{
    EXPECT_NO_THROW(TestModel model("Test",0));
}

TEST(TestModelTests, CheckTestIsEnabled)
{
    TestModel model("Test",0);
    EXPECT_FALSE(model.IsDefaultDisabled());
    EXPECT_TRUE(model.Enabled);
}

TEST(TestModelTests, CheckTestIsDefaultDisabled)
{
    TestModel model("DISABLED_Test",0);
    EXPECT_TRUE(model.IsDefaultDisabled());
    EXPECT_FALSE(model.Enabled);
}

TEST(TestModelTests, CountFailedAndPassedTestRuns)
{
    TestModel model("Test",0);
    model.AppendTestResult(0, true);
    model.AppendTestResult(1, true);
    model.AppendTestResult(2, false);
    model.AppendTestResult(3, true);
    model.AppendTestResult(4, false);
    EXPECT_EQ(3, model.GetPassedCount());
    EXPECT_EQ(2, model.GetFailedCount());
}

TEST(TestModelTests, GetTestResults)
{
    TestModel model("Test",0);
    EXPECT_THROW(model.GetTestResult(-1), std::invalid_argument);
    model.AppendTestResult(0, true);
    model.AppendTestResult(1, true);
    model.AppendTestResult(2, false);
    model.AppendTestResult(3, true);
    model.AppendTestResult(4, false);
    EXPECT_EQ(true, model.GetTestResult(0));
    EXPECT_EQ(true, model.GetTestResult(1));
    EXPECT_EQ(false, model.GetTestResult(2));
    EXPECT_EQ(true, model.GetTestResult(3));
    EXPECT_EQ(false, model.GetTestResult(4));
    EXPECT_THROW(model.GetTestResult(5), std::runtime_error);
}
