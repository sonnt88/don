#include <persigtest.h>
#include <Models/GTestConfigurationModel.h>
#include <stdexcept>
#include <fstream>

class GTestConfigurationModelTests : public ::testing::Test
{
    protected:
        const char* executableFilename;
        void createExecutableTestFile();
        void createNonExecutableTestFile();
        void removeTestFile();
    public:
        GTestConfigurationModelTests()
        {
            this->executableFilename = "/tmp/foobar";
        }          
};

void GTestConfigurationModelTests::createExecutableTestFile()
{
    this->createNonExecutableTestFile();
    chmod(this->executableFilename, 0777);
}

void GTestConfigurationModelTests::createNonExecutableTestFile()
{
    std::ofstream outputFile(this->executableFilename, std::ios_base::trunc);
    outputFile << "# this is a dummy executable\n";
    outputFile << "#!/bin/sh\n";
    outputFile << "echo Hello World\n";
    outputFile.close();
    
    chmod(this->executableFilename, 0666);
}

void GTestConfigurationModelTests::removeTestFile()
{
    remove(this->executableFilename);
}

TEST_F(GTestConfigurationModelTests, CheckExecutableIsEmpty)
{
    EXPECT_THROW(GTestConfigurationModel model("", "", "", 0, 0, GTEST_CONF_EXEC_TESTS), std::invalid_argument);
}

TEST_F(GTestConfigurationModelTests, CheckExecutableDoesNotExist)
{
    EXPECT_THROW(GTestConfigurationModel model(this->executableFilename, "", "", 0, 0, GTEST_CONF_EXEC_TESTS), std::runtime_error);
}

TEST_F(GTestConfigurationModelTests, CheckExecutableIsNotExecutable)
{
    this->createNonExecutableTestFile();
    EXPECT_THROW(GTestConfigurationModel model(this->executableFilename, "", "", 0, 0, GTEST_CONF_EXEC_TESTS), std::runtime_error);
    this->removeTestFile();
}

TEST_F(GTestConfigurationModelTests, CheckNoShuffleButRandomSeed)
{
    this->createExecutableTestFile();
    EXPECT_THROW(GTestConfigurationModel model(this->executableFilename, "", "", 0, 56, GTEST_CONF_EXEC_TESTS), std::invalid_argument);
    this->removeTestFile();
}

TEST_F(GTestConfigurationModelTests, CheckRandomSeedOutOfRange)
{
    this->createExecutableTestFile();
    EXPECT_THROW(GTestConfigurationModel model(this->executableFilename, "", "", 0, -1, GTEST_CONF_EXEC_TESTS | GTEST_CONF_SHUFFLE_TESTS), std::invalid_argument);
    EXPECT_THROW(GTestConfigurationModel model2(this->executableFilename, "", "", 0, -10, GTEST_CONF_EXEC_TESTS | GTEST_CONF_SHUFFLE_TESTS), std::invalid_argument);
    EXPECT_THROW(GTestConfigurationModel model3(this->executableFilename, "", "", 0, 100000, GTEST_CONF_EXEC_TESTS | GTEST_CONF_SHUFFLE_TESTS), std::invalid_argument);
    EXPECT_THROW(GTestConfigurationModel model4(this->executableFilename, "", "", 0, 100001, GTEST_CONF_EXEC_TESTS | GTEST_CONF_SHUFFLE_TESTS), std::invalid_argument);
    EXPECT_THROW(GTestConfigurationModel model5(this->executableFilename, "", "", 0, 200000, GTEST_CONF_EXEC_TESTS | GTEST_CONF_SHUFFLE_TESTS), std::invalid_argument);
    this->removeTestFile();
}

TEST_F(GTestConfigurationModelTests, CheckCorrectInitialization)
{
    this->createExecutableTestFile();
    EXPECT_NO_THROW(GTestConfigurationModel model(this->executableFilename, "", "", 0, 178, GTEST_CONF_EXEC_TESTS | GTEST_CONF_SHUFFLE_TESTS));
    this->removeTestFile();
}

TEST_F(GTestConfigurationModelTests, CheckListOnlyConfig)
{
    this->createExecutableTestFile();
    EXPECT_THROW(GTestConfigurationModel model1(this->executableFilename, "", "", 0, 0, GTEST_CONF_LIST_ONLY | GTEST_CONF_EXEC_TESTS), std::invalid_argument);
    EXPECT_THROW(GTestConfigurationModel model2(this->executableFilename, "", "", 0, 0, GTEST_CONF_LIST_ONLY | GTEST_CONF_RUN_DISABLED_TESTS), std::invalid_argument);
    EXPECT_THROW(GTestConfigurationModel model3(this->executableFilename, "", "", 0, 0, GTEST_CONF_LIST_ONLY | GTEST_CONF_SHUFFLE_TESTS), std::invalid_argument);
    EXPECT_NO_THROW(GTestConfigurationModel model3(this->executableFilename, "", "", 0, 0, GTEST_CONF_LIST_ONLY));
    this->removeTestFile();
}

TEST_F(GTestConfigurationModelTests, CheckThrowOnNoFlags)
{
    this->createExecutableTestFile();
    EXPECT_THROW(GTestConfigurationModel model(this->executableFilename, "", "", 0, 0, 0), std::invalid_argument);
    this->removeTestFile();
}

TEST_F(GTestConfigurationModelTests, CheckCorrectPropertiesForListOnly)
{
    this->createExecutableTestFile();
    GTestConfigurationModel model(this->executableFilename, "", "", 0, 0, GTEST_CONF_LIST_ONLY);
    EXPECT_TRUE(model.GetListOnlyFlag());
    EXPECT_FALSE(model.GetShuffleFlag());
    EXPECT_FALSE(model.GetRunDisabledTestsFlag());
    EXPECT_EQ(0, model.GetRepeatCount());
    EXPECT_EQ(0, model.GetRandomSeed());
    this->removeTestFile();
}

TEST_F(GTestConfigurationModelTests, CheckCorrectPropertiesForTestExec)
{
    this->createExecutableTestFile();
    GTestConfigurationModel model(this->executableFilename, "", "", 20, 563, GTEST_CONF_EXEC_TESTS | GTEST_CONF_RUN_DISABLED_TESTS | GTEST_CONF_SHUFFLE_TESTS);
    EXPECT_FALSE(model.GetListOnlyFlag());
    EXPECT_TRUE(model.GetShuffleFlag());
    EXPECT_TRUE(model.GetRunDisabledTestsFlag());
    EXPECT_EQ(20, model.GetRepeatCount());
    EXPECT_EQ(563, model.GetRandomSeed());
    this->removeTestFile();
}

TEST_F(GTestConfigurationModelTests, CopyConstrutor)
{
    this->createExecutableTestFile();
    GTestConfigurationModel model(this->executableFilename, "", "", 20, 563, GTEST_CONF_EXEC_TESTS | GTEST_CONF_RUN_DISABLED_TESTS | GTEST_CONF_SHUFFLE_TESTS);
    GTestConfigurationModel copiedModel(model);
    EXPECT_EQ(model.GetFilter(), copiedModel.GetFilter());
    EXPECT_EQ(model.GetGTestExecutable(), copiedModel.GetGTestExecutable());
    EXPECT_EQ(model.GetListOnlyFlag(), copiedModel.GetListOnlyFlag());
    EXPECT_EQ(model.GetRandomSeed(), copiedModel.GetRandomSeed());
    EXPECT_EQ(model.GetRepeatCount(), copiedModel.GetRepeatCount());
    EXPECT_EQ(model.GetRunDisabledTestsFlag(), copiedModel.GetRunDisabledTestsFlag());
    EXPECT_EQ(model.GetShuffleFlag(), copiedModel.GetShuffleFlag());
    this->removeTestFile();
}
