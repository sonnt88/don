/*
 * TestHelper.h
 *
 *  Created on: Jul 6, 2012
 *      Author: oto4hi
 */

#ifndef TESTHELPER_H_
#define TESTHELPER_H_

#include <stdexcept>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


#define PATH_BUFFER_LEN 1024
#define GTEST_SERVICE_PORT_NO 6790

class TestHelper {
public:
	static std::string GetApplicationBinaryPath()
	{
		char selfPath[PATH_BUFFER_LEN];
		memset(selfPath, 0, PATH_BUFFER_LEN);

		if (PATH_BUFFER_LEN <= readlink("/proc/self/exe", selfPath, PATH_BUFFER_LEN) )
			throw std::runtime_error("Path is too long.");

		std::string returnValue = selfPath;

		return returnValue;
	}

	static int LocalConntect()
	{
	    int sock = socket(AF_INET, SOCK_STREAM, 0);
	    EXPECT_LE(0,sock);

	    struct hostent* server = gethostbyname("localhost");

	    EXPECT_TRUE( NULL != server );

	    struct sockaddr_in serv_addr;
	    bzero((char*) &serv_addr, sizeof(serv_addr));
	    serv_addr.sin_family = AF_INET;
	    bcopy((char*) server->h_addr_list[0], (char*)&serv_addr.sin_addr.s_addr, server->h_length);
	    serv_addr.sin_port = htons(GTEST_SERVICE_PORT_NO);

	    EXPECT_EQ(0, connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)));

	    return sock;
	}
};


#endif /* TESTHELPER_H_ */
