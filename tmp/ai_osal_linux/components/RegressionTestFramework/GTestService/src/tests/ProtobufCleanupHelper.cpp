/*
 * ProtobufCleanupHelper.cpp
 *
 *  Created on: 21.09.2012
 *      Author: oto4hi
 */

#include <GTestServiceBuffers.pb.h>
#include <iostream>

class ProtobufCleanupHelper {
private:
	static ProtobufCleanupHelper instance;
	ProtobufCleanupHelper();
	virtual ~ProtobufCleanupHelper();
};

ProtobufCleanupHelper ProtobufCleanupHelper::instance;

ProtobufCleanupHelper::ProtobufCleanupHelper()
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;
}

ProtobufCleanupHelper::~ProtobufCleanupHelper()
{
	google::protobuf::ShutdownProtobufLibrary();
}


