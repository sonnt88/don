/*
 * GTestProcessControllerTests.cpp
 *
 *  Created on: Jun 8, 2012
 *      Author: oto4hi
 */

#include <persigtest.h>
#include <ProcessController/GTestProcessController.h>
#include <ProcessController/GTestListOutputHandler.h>
#include <ProcessController/GTestExecOutputHandler.h>
#include <unistd.h>
#include <string>

#include "TestHelper.h"

class GTestProcessControllerTests : public ::testing::Test
{
protected:
	TestRunModel* testRunModel;
	std::string exePath;

	virtual void SetUp()
	{
		this->testRunModel = NULL;

		exePath = TestHelper::GetApplicationBinaryPath();

		GTestConfigurationModel confModel(exePath, "", "*", 1, 0, GTEST_CONF_LIST_ONLY);

		GTestProcessController processController(confModel);
		GTestListOutputHandler listTestsOutputHandler;
		processController.RegisterStdoutOutputHandler(&listTestsOutputHandler);
		processController.Run();

		testRunModel = listTestsOutputHandler.GetTestRunModel();
	}

	virtual void TearDown()
	{
		if (this->testRunModel)
			delete this->testRunModel;

		this->testRunModel = NULL;
	}
};

TEST_F(GTestProcessControllerTests, GetTestList)
{
	EXPECT_FALSE( NULL == this->testRunModel );

	::testing::UnitTest* unitTest = ::testing::UnitTest::GetInstance();
	EXPECT_EQ(unitTest->total_test_case_count(), testRunModel->GetTestCaseCount());

	for (int testCaseIndex=0; testCaseIndex < this->testRunModel->GetTestCaseCount(); testCaseIndex++)
	{
		::testing::TestCase* testCase = const_cast< ::testing::TestCase* >(unitTest->GetTestCase(testCaseIndex));
		EXPECT_EQ(testCase->total_test_count(), this->testRunModel->GetTestCase(testCaseIndex)->TestCount());
		std::string testCaseName(testCase->name());
		EXPECT_EQ(testCaseName, this->testRunModel->GetTestCase(testCaseIndex)->GetName());

		for (int testIndex=0; testIndex < testCase->total_test_count(); testIndex++)
		{
			::testing::TestInfo* testInfo = const_cast< ::testing::TestInfo* >(testCase->GetTestInfo(testIndex));
			TestModel* testModel = this->testRunModel->GetTestCase(testCaseIndex)->GetTest(testIndex);
			std::string testName(testInfo->name());
			EXPECT_EQ(testName,testModel->GetName());
		}
	}
}

TEST_F(GTestProcessControllerTests, ExecTest)
{
	EXPECT_FALSE( NULL == this->testRunModel );

	int numOfRepeats = 2;

	GTestConfigurationModel confModel(
			this->exePath,
			"",
			"ExecTests.DISABLED_TestSupposedTo*",
			numOfRepeats, 0,
			GTEST_CONF_EXEC_TESTS | GTEST_CONF_RUN_DISABLED_TESTS);

	GTestProcessController processController(confModel);
	GTestExecOutputHandler execTestsOutputHandler(this->testRunModel);
	GTestExecOutputHandler listTestsOutputHandler(this->testRunModel);
	processController.RegisterStdoutOutputHandler(&execTestsOutputHandler);
	processController.RegisterStderrOutputHandler(&listTestsOutputHandler);
	processController.Run();

	execTestsOutputHandler.ProcessTestResultLines();
	execTestsOutputHandler.WaitForProcessTestResultCompleted();

	::testing::UnitTest* unitTest = ::testing::UnitTest::GetInstance();
	for (int testCaseIndex=0; testCaseIndex < this->testRunModel->GetTestCaseCount(); testCaseIndex++)
	{
		::testing::TestCase* testCase = const_cast< ::testing::TestCase* >(unitTest->GetTestCase(testCaseIndex));
		for (int testIndex=0; testIndex < testCase->total_test_count(); testIndex++)
		{
			TestModel* testModel = this->testRunModel->GetTestCase(testCaseIndex)->GetTest(testIndex);

			if (testModel->GetName() == "DISABLED_TestSupposedToFail")
			{
				EXPECT_EQ(0, testModel->GetPassedCount());
				EXPECT_EQ(numOfRepeats, testModel->GetFailedCount());
			}
			else
			{
				if (testModel->GetName() == "DISABLED_TestSupposedToPass")
				{
					EXPECT_EQ(numOfRepeats, testModel->GetPassedCount());
					EXPECT_EQ(0, testModel->GetFailedCount());
				}
				else
				{
					EXPECT_EQ(0, testModel->GetPassedCount());
					EXPECT_EQ(0, testModel->GetFailedCount());
				}
			}
		}
	}

	EXPECT_EQ(numOfRepeats, this->testRunModel->GetIteration());
}
