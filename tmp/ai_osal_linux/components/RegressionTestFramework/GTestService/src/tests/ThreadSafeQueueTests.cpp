/*
 * ThreadSafeQueueTests.cpp
 *
 *  Created on: 05.09.2012
 *      Author: oto4hi
 */

#include <persigtest.h>
#include <Utils/ThreadSafeQueue.h>
#include <string>
#include <sstream>
#include <unistd.h>
#include <pthread.h>

class ThreadSafeQueueTests : public ::testing::Test
{
protected:
	static const int numOfQueueElements = 20;
	static void* enqueueFunc(void* ptr);
	static std::string generateQueueElement(int index);
	static void* addOneElement(void* ptr);
};

TEST_F(ThreadSafeQueueTests, EnqueueDequeue)
{
	ThreadSafeQueue<std::string> queue;

	for (int index = 0; index < 20; index++)
	{
		std::stringstream sstream;
		sstream << "string";
		sstream << index;
		queue.PushBack(sstream.str());
	}

	for (int index = 0; index < 20; index++)
	{
		std::string stringFromQueue = queue.PopFirst();
		std::stringstream sstream;
		sstream << "string";
		sstream << index;

		ASSERT_EQ(sstream.str(), stringFromQueue);
	}
}

std::string ThreadSafeQueueTests::generateQueueElement(int index)
{
	std::stringstream sstream;
	sstream << "string";
	sstream << index;
	std::string resultString = sstream.str();
	return resultString;
}

void* ThreadSafeQueueTests::enqueueFunc(void* ptr)
{
	ThreadSafeQueue<std::string>* queue = (ThreadSafeQueue<std::string>*)ptr;
	for (int index = 0; index < ThreadSafeQueueTests::numOfQueueElements; index++)
	{
		std::string queueElement = ThreadSafeQueueTests::generateQueueElement(index);
		queue->PushBack(queueElement);
		usleep(100);
	}
	return NULL;
}

TEST_F(ThreadSafeQueueTests, EnqueueDequeueThreaded)
{
	ThreadSafeQueue<std::string> queue;
	pthread_t enqueueThread = 0;
	ASSERT_EQ(0, pthread_create(&enqueueThread, NULL, enqueueFunc,(void*)&queue) );

	int index = 0;
	while (index < ThreadSafeQueueTests::numOfQueueElements)
	{
		try {
			std::string stringFromQueue = queue.PopFirst();
			std::string expectedString = ThreadSafeQueueTests::generateQueueElement(index);
			ASSERT_TRUE(expectedString.compare(stringFromQueue) == 0);
			stringFromQueue.erase(stringFromQueue.length() - 1, 1);
			index++;
		}
		catch (EmptyQueueException &e) {}
	}

	ASSERT_EQ(0, pthread_join(enqueueThread,NULL));
}

void* ThreadSafeQueueTests::addOneElement(void* ptr)
{
	usleep(1000);
	ThreadSafeQueue<int>* queue = (ThreadSafeQueue<int>*)ptr;
	queue->PushBack(1);
}

TEST_F(ThreadSafeQueueTests, WaitUntilNotEmpty)
{
	ThreadSafeQueue<int> queue;
	pthread_t addOneElementThread = 0;

	int errCode = pthread_create(&addOneElementThread, NULL, addOneElement, (void*)&queue);
	if (errCode)
	{
		std::stringstream sstream;
		sstream << "error creating thread. error code is " << strerror(errCode);
		throw std::runtime_error(sstream.str().c_str());
	}

	queue.BlockUntilNotEmpty();
	ASSERT_EQ(1, queue.PopFirst());
	ASSERT_EQ(0, pthread_join(addOneElementThread,NULL));
}
