/*
 * TestsForTests.cpp
 *
 *  Created on: Jul 18, 2012
 *      Author: oto4hi
 */

#include <persigtest.h>

// The following two tests will be run from "GTestProcessControllerTests.ExecTest"
TEST(ExecTests, DISABLED_TestSupposedToPass) { EXPECT_EQ(1,1); }
TEST(ExecTests, DISABLED_TestSupposedToFail) { EXPECT_EQ(0,1); }

// The following two tests will be run from "GTestServiceCoreTests.SelectAndRunTests"
TEST(DISABLED_TestCase1, Test1) { EXPECT_EQ(1,1); }
TEST(DISABLED_TestCase1, Test2) { EXPECT_EQ(1,1); }

TEST(DISABLED_SupposedToCrash, TestNotToCrash)       { EXPECT_EQ(1,1); }
TEST(DISABLED_SupposedToCrash, TestToCrash)          { *((int*)0) = 0xDEADBEEF; }
TEST(DISABLED_SupposedToCrash, SecondTestNotToCrash) { EXPECT_EQ(1,1); }
TEST(DISABLED_SupposedToCrash, ThirdTestNotToCrash)  { EXPECT_EQ(1,1); }
TEST(DISABLED_SupposedToCrash, SecondTestToCrash)    { *((int*)0) = 0xDEADBEEF; }
TEST(DISABLED_SupposedToCrash, FourthTestNotToCrash) { EXPECT_EQ(1,1); }
TEST(DISABLED_SupposedToCrash, FifthTestNotToCrash)  { EXPECT_EQ(1,1); }

TEST(DISABLED_TestCase2, Test1) { EXPECT_EQ(1,1); }
TEST(DISABLED_TestCase2, Test2) { EXPECT_EQ(1,1); }

TEST(DISABLED_ThreeSecondsRuntime, TestToBeAborted) { sleep(3); EXPECT_EQ(1,1); }

TEST(DISABLED_TestCase3, Test1) { EXPECT_EQ(1,1); }
TEST(DISABLED_TestCase3, Test2) { EXPECT_EQ(1,1); }
