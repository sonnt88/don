#include <persigtest.h>
#include <Models/TestCaseModel.h>
#include <stdexcept>

TEST(TestCaseModelTests, CheckExceptionOnNameEmpty)
{
    EXPECT_THROW(TestCaseModel model(""), std::invalid_argument);
}

TEST(TestCaseModelTests, CheckNoExceptionOnNameNotEmpty)
{
    EXPECT_NO_THROW(TestCaseModel model("TestCase"));
}

TEST(TestCaseModelTests, AddTests)
{
    TestCaseModel model("TestCase");
    TestModel* test1 = new TestModel("Test1",0);
    TestModel* test2 = new TestModel("Test2",1);
    TestModel* test3 = new TestModel("Test3",2);
    
    model.AddTest(test1);
    model.AddTest(test2);
    model.AddTest(test3);
    
    EXPECT_EQ(3, model.TestCount());
    
    EXPECT_EQ(test1, model[0]);
    EXPECT_EQ(test2, model[1]);
    EXPECT_EQ(test3, model[2]);
    
    EXPECT_EQ(test1, model["Test1"]);
    EXPECT_EQ(test2, model["Test2"]);
    EXPECT_EQ(test3, model["Test3"]);   
}

TEST(TestCaseModelTests, GetTestInvalidIndex)
{
    TestCaseModel model("TestCase");
    
    EXPECT_THROW(model[0], std::invalid_argument);
    
    model.AddTest(new TestModel("Test1",0));
    model.AddTest(new TestModel("Test2",1));
    model.AddTest(new TestModel("Test3",2));
    
    EXPECT_NO_THROW(model[0]);
    EXPECT_THROW(model[3], std::invalid_argument);
    EXPECT_THROW(model[4], std::invalid_argument);
}

TEST(TestCaseModelTests, GetTestInvalidName)
{
    TestCaseModel model("TestCase");
    
    EXPECT_THROW(model["Test1"], std::invalid_argument);
    
    model.AddTest(new TestModel("Test1",0));
    model.AddTest(new TestModel("Test2",1));
    model.AddTest(new TestModel("Test3",2));
    
    EXPECT_NO_THROW(model["Test1"]);
    EXPECT_THROW(model["Test4"], std::invalid_argument);
}

TEST(TestCaseModelTests, AddTestNullArgument)
{
    TestCaseModel model("TestCase");
    EXPECT_THROW(model.AddTest(NULL), std::invalid_argument);
}

TEST(TestCaseModelTests, AddAlreadyExistingTest)
{
    TestCaseModel model("TestCase");
    model.AddTest(new TestModel("Test",0));
    TestModel* secondTest = new TestModel("Test",1);
    EXPECT_THROW(model.AddTest(secondTest), std::runtime_error);
    delete secondTest;
}

