#include <persigtest.h>
#include <ProcessController/GTestListOutputHandler.h>

TEST(GTestListOutputHandlerTests, CreateTestRunModelValidInput)
{
    GTestListOutputHandler* handler = new GTestListOutputHandler();
    handler->OnLineReceived(new std::string("TestCase1."));
    handler->OnLineReceived(new std::string("  Test1"));
    handler->OnLineReceived(new std::string("  Test2"));
    handler->OnLineReceived(new std::string("TestCase2."));
    handler->OnLineReceived(new std::string("  Test1"));
    handler->OnLineReceived(new std::string("  Test2"));
    handler->OnLineReceived(new std::string("  Test3"));
    handler->OnProcessTerminated(0);
    handler->WaitForOutputCompleted();
    
    TestRunModel* model = handler->GetTestRunModel();
    EXPECT_EQ("TestCase1", model->GetTestCase(0)->GetName());
    EXPECT_EQ("Test1", model->GetTestCase(0)->GetTest(0)->GetName());
    EXPECT_EQ("Test2", model->GetTestCase(0)->GetTest(1)->GetName());
    EXPECT_EQ("TestCase2", model->GetTestCase(1)->GetName());
    EXPECT_EQ("Test1", model->GetTestCase(1)->GetTest(0)->GetName());
    EXPECT_EQ("Test2", model->GetTestCase(1)->GetTest(1)->GetName());
    EXPECT_EQ("Test3", model->GetTestCase(1)->GetTest(2)->GetName());
    
    delete model;
    delete handler;
}

TEST(GTestListOutputHandlerTests, CreateTestRunModelTestWithoutTestCase)
{
    GTestListOutputHandler* handler = new GTestListOutputHandler();
    handler->OnLineReceived(new std::string("  Test1"));
    
    handler->OnProcessTerminated(0);
    handler->WaitForOutputCompleted();
    
    TestRunModel* model = NULL;
    EXPECT_THROW(model = handler->GetTestRunModel(), std::logic_error);
    
    delete handler;
}

TEST(GTestListOutputHandlerTests, CreateTestRunModelEmptyTestCase)
{
    GTestListOutputHandler* handler = new GTestListOutputHandler();
    handler->OnLineReceived(new std::string("."));
    
    handler->OnProcessTerminated(0);
    handler->WaitForOutputCompleted();
    
    TestRunModel* model = NULL;
    EXPECT_THROW(model = handler->GetTestRunModel(), std::invalid_argument);
    
    delete handler;
}

TEST(GTestListOutputHandlerTests, CreateTestRunModelRobustnessTest)
{
    GTestListOutputHandler* handler = new GTestListOutputHandler();
    handler->OnLineReceived(new std::string("   TestCase1.  "));
    handler->OnLineReceived(new std::string("Test1"));
    handler->OnLineReceived(new std::string(" Test2 \t     "));
    handler->OnLineReceived(new std::string(" \r    TestCase2.\t\r\n "));
    handler->OnLineReceived(new std::string("  \r\t     Test1   "));
    handler->OnLineReceived(new std::string(" Test2 "));
    handler->OnLineReceived(new std::string("Test3 "));
    handler->OnProcessTerminated(0);
    handler->WaitForOutputCompleted();
    
    TestRunModel* model = handler->GetTestRunModel();
    EXPECT_EQ("TestCase1", model->GetTestCase(0)->GetName());
    EXPECT_EQ("Test1", model->GetTestCase(0)->GetTest(0)->GetName());
    EXPECT_EQ("Test2", model->GetTestCase(0)->GetTest(1)->GetName());
    EXPECT_EQ("TestCase2", model->GetTestCase(1)->GetName());
    EXPECT_EQ("Test1", model->GetTestCase(1)->GetTest(0)->GetName());
    EXPECT_EQ("Test2", model->GetTestCase(1)->GetTest(1)->GetName());
    EXPECT_EQ("Test3", model->GetTestCase(1)->GetTest(2)->GetName());
    
    delete model;
    delete handler;
}
