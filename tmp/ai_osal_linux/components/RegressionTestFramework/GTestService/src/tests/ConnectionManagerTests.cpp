#include <persigtest.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <AdapterConnection/ConnectionManager.h>
#include <AdapterConnection/Interfaces/IConnectionFactory.h>
#include <AdapterConnection/Interfaces/IPacketReceiver.h>
#include <AdapterConnection/AdapterConnectionPushDecoratorFactory.h>
#include <AdapterConnection/AdapterPacket.h>
#include <AdapterConnection/AdapterConnection.h>

#include "TestHelper.h"

using ::testing::MatcherInterface;
using ::testing::MatchResultListener;


class MockPacketReceiver : public IPacketReceiver
{
public:
    MOCK_METHOD2(OnPacketReceive, void(IConnection* Connection, AdapterPacket* Packet));
};

class ConnectionManagerTests : 	public ::testing::Test,
								public ::testing::WithParamInterface<int> {
protected:
	ConnectionManager* cmanager;
	MockPacketReceiver* packetReceiver;

    /**
     * start a new connection manager each time a test is started
     */
    virtual void SetUp()
	{
		packetReceiver = new MockPacketReceiver();
		IConnectionFactory* connectionFactory = new AdapterConnectionPushDecoratorFactory(packetReceiver);
		cmanager = new ConnectionManager(GTEST_SERVICE_PORT_NO, connectionFactory, NULL);
		cmanager->Start();
	}

    /**
     * shutdown the connection manager again
     */
	virtual void TearDown()
	{
		cmanager->Stop();
		delete cmanager;
		delete packetReceiver;
	}
};

/**
 * this matcher checks if two Adapter packets equal
 * (with the exception of the serial number which is increased by 1 each time a packet is sent)
 */
class AdapterPacketMatcher : public ::testing::MatcherInterface< AdapterPacket* >
{
private:
	AdapterPacket* value;
public:

	explicit AdapterPacketMatcher(AdapterPacket* Value)
	{
		this->value = Value;
	}

	virtual bool MatchAndExplain(AdapterPacket* Packet, ::testing::MatchResultListener* listener) const
	{
		bool result = true;

		if (Packet == NULL)
			return false;

		result |= ( Packet->GetID() == value->GetID() );
		result |= ( Packet->GetFlags() == value->GetFlags() );
		result |= ( Packet->GetRequestSerial() == value->GetRequestSerial() );
		result |= ( Packet->GetPayloadSize() == value->GetPayloadSize() );

		if (!result)
			return result;

		result |= (memcmp(Packet->GetPayload(), value->GetPayload(), value->GetPayloadSize()) == 0);
		return result;
	}

	virtual void DescribeTo(::std::ostream* os) const
	{
		*os << "adapter packet equals";
	}

	virtual void DescribeNegationTo(::std::ostream* os) const
	{
		*os << "adapter packet does not equal";
	}
};

inline ::testing::Matcher< AdapterPacket* > AdapterPacketEq(AdapterPacket* expectedPacket) {
	return ::testing::MakeMatcher(new AdapterPacketMatcher(expectedPacket));
}

TEST_P(ConnectionManagerTests, ReceivePacket)
{
	int bufferSize = GetParam();

	int* payload = (int*)malloc(sizeof(int)*bufferSize);
	for (int index=0; index < bufferSize; index++)
	{
		payload[index] = index;
	}

    int sock = TestHelper::LocalConntect();

    AdapterConnection* clientConnection = new AdapterConnection(sock);
	AdapterPacket packet(ID_DOWN_GET_VERSION, 0, 0, 0, sizeof(int) * bufferSize, (char*)payload, clientConnection->GetID());

	EXPECT_CALL(*(this->packetReceiver), OnPacketReceive(::testing::_,AdapterPacketEq(&packet))); //receive packet
	EXPECT_CALL(*(this->packetReceiver), OnPacketReceive(::testing::_,NULL));                     //close connection

    clientConnection->Send(packet);
    usleep(10000);
    clientConnection->Close();
    usleep(10000);
    delete clientConnection;
    free(payload);
}

INSTANTIATE_TEST_CASE_P(ReceivePacketsWithDifferentSizes,
		ConnectionManagerTests,
		::testing::Values(64,256,65535));
