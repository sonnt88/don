#include <persigtest.h>
#include <ProcessController/GTestListOutputHandler.h>
#include <ProcessController/GTestExecOutputHandler.h>

TEST(GTestExecOutputHandlerTests, SimulateStandardTestRun)
{
    GTestListOutputHandler* listHandler = new GTestListOutputHandler();
    listHandler->OnLineReceived(new std::string("TestCase1."));
    listHandler->OnLineReceived(new std::string("  Test1"));
    listHandler->OnLineReceived(new std::string("  Test2"));
    listHandler->OnLineReceived(new std::string("TestCase2."));
    listHandler->OnLineReceived(new std::string("  Test1"));
    listHandler->OnLineReceived(new std::string("  Test2"));
    listHandler->OnLineReceived(new std::string("  Test3"));
    listHandler->OnProcessTerminated(0);
    listHandler->WaitForOutputCompleted();
    
    TestRunModel* model = listHandler->GetTestRunModel();
    
    GTestExecOutputHandler* execHandler = new GTestExecOutputHandler(model);
    
    execHandler->ProcessTestResultLines();
    
    /* This is not exactly the GTest Syntax, except for the test results themselves. All below
    of the "========" - line should just be ignored by the output handler*/

    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase1.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test1 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase1.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test2 (2 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test1 (45 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test2"));
    execHandler->OnLineReceived(new std::string("[  FAILED  ] TestCase2.Test2 (3 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test3"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[==========] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[=SUMMARY:=] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test1"));
    execHandler->OnLineReceived(new std::string("[  FAILED  ] TestCase2.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test3"));
    execHandler->OnLineReceived(new std::string("1 FAILED"));
    
    execHandler->OnProcessTerminated(0);
    execHandler->WaitForProcessTestResultCompleted();
    
    EXPECT_EQ(1,model->GetTestCase(0)->GetTest(0)->GetPassedCount());
    EXPECT_EQ(0,model->GetTestCase(0)->GetTest(0)->GetFailedCount());
    
    EXPECT_EQ(1,model->GetTestCase(0)->GetTest(1)->GetPassedCount());
    EXPECT_EQ(0,model->GetTestCase(0)->GetTest(1)->GetFailedCount());
    
    EXPECT_EQ(1,model->GetTestCase(1)->GetTest(0)->GetPassedCount());
    EXPECT_EQ(0,model->GetTestCase(1)->GetTest(0)->GetFailedCount());
    
    EXPECT_EQ(0,model->GetTestCase(1)->GetTest(1)->GetPassedCount());
    EXPECT_EQ(1,model->GetTestCase(1)->GetTest(1)->GetFailedCount());
    
    EXPECT_EQ(1,model->GetTestCase(1)->GetTest(2)->GetPassedCount());
    EXPECT_EQ(0,model->GetTestCase(1)->GetTest(2)->GetFailedCount());
    
    delete model;
    delete execHandler;
    delete listHandler;
}
