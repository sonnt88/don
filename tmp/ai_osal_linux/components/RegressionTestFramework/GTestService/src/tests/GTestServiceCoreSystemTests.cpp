/*
 * GTestServiceCoreSystemTests.cpp
 *
 *  Created on: Jul 6, 2012
 *      Author: oto4hi
 */

#include <persigtest.h>
#include <Core/GTestServiceCore.h>

#include <AdapterConnection/AdapterPacket.h>
#include <AdapterConnection/AdapterConnection.h>
#include <Models/TestRunModel.h>

#include "TestHelper.h"
#include "version.h"

#include <vector>

class GTestServiceCoreSystemTests : public ::testing::Test {
protected:
	GTestServiceCore* core;
	AdapterConnection* clientConnection;

	std::vector<int> getTestIDsForTestFromModel(
			GTestServiceBuffers::AllTestCases* AllTests, const char** testCaseNames, int numberOfTestCases);

	AdapterPacket* ReceiveOtherThanTestOutputPacket();

	void LaunchTests(std::vector<int> TestIDs, int numberOfRepeats);
	GTestServiceBuffers::AllTestCases GetPBTestRunModel();

	void printOutputPacket(AdapterPacket* outputPacket);

	void abortFunc();
	static void* staticAbortFunc(void* thisPtr);

	virtual void SetUp();
	virtual void TearDown();
};

void GTestServiceCoreSystemTests::SetUp()
{
	std::string exePath = TestHelper::GetApplicationBinaryPath();
	this->core = new GTestServiceCore(exePath, "", GTEST_SERVICE_PORT_NO, false, NULL);

	this->core->RunThreaded();

	int sock = TestHelper::LocalConntect();
	clientConnection = new AdapterConnection(sock);
}

void GTestServiceCoreSystemTests::TearDown()
{
	if (!clientConnection->IsClosed())
		clientConnection->Close();

	delete clientConnection;

	this->core->Stop();
	delete core;
}

void GTestServiceCoreSystemTests::printOutputPacket(AdapterPacket* outputPacket)
{
	GTestServiceBuffers::TestOutput testOutput;
	testOutput.ParseFromArray(outputPacket->GetPayload(), outputPacket->GetPayloadSize());
	std::cout << testOutput.output();
}

AdapterPacket* GTestServiceCoreSystemTests::ReceiveOtherThanTestOutputPacket()
{
	AdapterPacket* answerPacket = NULL;

	do
	{
		if (answerPacket)
		{
			//this->printOutputPacket(answerPacket);
			delete answerPacket;
			answerPacket = NULL;
		}

		while (!answerPacket)
			answerPacket = this->clientConnection->Receive();

		/*if (answerPacket->GetID() != (char)ID_UP_TEST_OUTPUT)
		{
			fprintf(stderr, "received packet with id==%hhu\n", answerPacket->GetID());
		}*/

	} while (answerPacket->GetID() == (char)ID_UP_TEST_OUTPUT);

	return answerPacket;
}

TEST_F(GTestServiceCoreSystemTests, SendVersionPacket)
{
	AdapterPacket packet(ID_DOWN_GET_VERSION, 0, 0, 0, 0, NULL, this->clientConnection->GetID());
	this->clientConnection->Send(packet);

	AdapterPacket* answerPacket = this->ReceiveOtherThanTestOutputPacket();

	ASSERT_FALSE(NULL == answerPacket);
	ASSERT_EQ((char)ID_UP_VERSION, (char)(answerPacket->GetID()));
	ASSERT_EQ(packet.GetSerial(), answerPacket->GetRequestSerial());
	ASSERT_EQ(strlen(GTEST_SERVICE_VERSION), strlen(answerPacket->GetPayload()));
	ASSERT_TRUE(strcmp(GTEST_SERVICE_VERSION, answerPacket->GetPayload()) == 0);

	delete answerPacket;
}

TEST_F(GTestServiceCoreSystemTests, SendStatusPacket)
{
	AdapterPacket packet(ID_DOWN_GET_STATE, 0, 0, 0, 0, NULL, this->clientConnection->GetID());
	this->clientConnection->Send(packet);

	AdapterPacket* answerPacket = this->ReceiveOtherThanTestOutputPacket();

	ASSERT_EQ((char)ID_UP_STATE, (char)(answerPacket->GetID()));
	ASSERT_EQ(packet.GetSerial(), answerPacket->GetRequestSerial());
	ASSERT_EQ(sizeof(char), answerPacket->GetPayloadSize());
	ASSERT_EQ((char)STATE_IDLE, *(char*)(answerPacket->GetPayload()) );

	delete answerPacket;
}

TEST_F(GTestServiceCoreSystemTests, SendTestList)
{
	AdapterPacket packet(ID_DOWN_GET_TESTLIST, 0, 0, 0, 0, NULL, this->clientConnection->GetID());
	this->clientConnection->Send(packet);

	usleep(10000);

	AdapterPacket* answerPacket = this->ReceiveOtherThanTestOutputPacket();

	ASSERT_EQ( (char)ID_UP_TESTLIST, (char)(answerPacket->GetID()) );
	ASSERT_EQ( packet.GetSerial(), answerPacket->GetRequestSerial() );

	GTestServiceBuffers::AllTestCases pbTestRunModel;
	ASSERT_NO_THROW( pbTestRunModel.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize()));

	TestRunModel* testRunModel = new TestRunModel(pbTestRunModel);

	::testing::UnitTest* unitTest = ::testing::UnitTest::GetInstance();
	ASSERT_EQ(unitTest->total_test_case_count(), testRunModel->GetTestCaseCount());

	for (int testCaseIndex=0; testCaseIndex < testRunModel->GetTestCaseCount(); testCaseIndex++)
	{
		::testing::TestCase* testCase = const_cast< ::testing::TestCase* >(unitTest->GetTestCase(testCaseIndex));
		ASSERT_EQ(testCase->total_test_count(), testRunModel->GetTestCase(testCaseIndex)->TestCount());
		std::string testCaseName(testCase->name());
		ASSERT_EQ(testCaseName, testRunModel->GetTestCase(testCaseIndex)->GetName());

		for (int testIndex=0; testIndex < testCase->total_test_count(); testIndex++)
		{
			::testing::TestInfo* testInfo = const_cast< ::testing::TestInfo* >(testCase->GetTestInfo(testIndex));
			TestModel* testModel = testRunModel->GetTestCase(testCaseIndex)->GetTest(testIndex);
			std::string testName(testInfo->name());
			ASSERT_EQ(testName,testModel->GetName());
		}
	}

	delete testRunModel;
	delete answerPacket;
}

std::vector<int> GTestServiceCoreSystemTests::getTestIDsForTestFromModel(GTestServiceBuffers::AllTestCases* AllTests,
		const char** testCaseNames, int numberOfTestCases)
{
	std::vector<int> testIDs;

	for (int TestCaseIndex = 0; TestCaseIndex < AllTests->testcases_size(); TestCaseIndex++)
	{
		GTestServiceBuffers::TestCase testCase = AllTests->testcases(TestCaseIndex);
		for (int nameIndex = 0; nameIndex < numberOfTestCases; nameIndex++)
		{
			if (testCase.name() == testCaseNames[nameIndex])
			{
				for (int testIndex=0; testIndex < testCase.tests_size(); testIndex++)
				{
					testIDs.push_back(testCase.tests(testIndex).id());
				}
			}
		}
	}

	return testIDs;
}

void GTestServiceCoreSystemTests::LaunchTests(std::vector<int> TestIDs, int numberOfRepeats)
{
	AdapterPacket* answerPacket = NULL;

	GTestServiceBuffers::TestLaunch testLaunchBuffer;
	testLaunchBuffer.set_invert(false);
	testLaunchBuffer.set_numberofrepeats(numberOfRepeats);
	testLaunchBuffer.set_shuffle(false);
	testLaunchBuffer.set_randomseek(0);

	for (std::vector<int>::iterator it = TestIDs.begin(); it != TestIDs.end(); ++it)
	{
		int testID = *it;
		testLaunchBuffer.add_testid(testID);
	}

	int serializedSize = testLaunchBuffer.ByteSize();
	char* serializedLaunchBuffer = (char*)malloc(serializedSize);
	testLaunchBuffer.SerializeToArray(serializedLaunchBuffer, serializedSize);

	AdapterPacket launchPacket(
		(char)ID_DOWN_RUN_TESTS,
		0 , 0,
		0,
		serializedSize,
		serializedLaunchBuffer,
		this->clientConnection->GetID());

	this->clientConnection->Send(launchPacket);

	/*receive the test execution ACK*/
	answerPacket = this->ReceiveOtherThanTestOutputPacket();

	EXPECT_EQ((char)ID_UP_ACK_TESTRUN, answerPacket->GetID());

	free(serializedLaunchBuffer);
	delete answerPacket;
}

GTestServiceBuffers::AllTestCases GTestServiceCoreSystemTests::GetPBTestRunModel()
{
	/*get the test list*/
	AdapterPacket packet(ID_DOWN_GET_TESTLIST, 0, 0, 0, 0, NULL, this->clientConnection->GetID());
	this->clientConnection->Send(packet);

	AdapterPacket* answerPacket = this->ReceiveOtherThanTestOutputPacket();

	EXPECT_EQ( (char)ID_UP_TESTLIST, (char)(answerPacket->GetID()) );
	EXPECT_EQ( packet.GetSerial(), answerPacket->GetRequestSerial() );

	GTestServiceBuffers::AllTestCases pbTestRunModel;
	EXPECT_NO_THROW( pbTestRunModel.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize()));

	delete answerPacket;
	return pbTestRunModel;
}

TEST_F(GTestServiceCoreSystemTests, ExecTests)
{
	AdapterPacket* answerPacket = NULL;

	GTestServiceBuffers::AllTestCases pbTestRunModel = GetPBTestRunModel();
	ASSERT_TRUE( pbTestRunModel.IsInitialized() );

	/*define which tests to execute*/
	const char* testCaseNames[3] = {"DISABLED_TestCase1", "DISABLED_TestCase2", "DISABLED_TestCase3"};
	std::vector<int> testIDs = this->getTestIDsForTestFromModel(&pbTestRunModel, testCaseNames, 3);

	LaunchTests(testIDs, 1);

	/*receive all test result packets*/
	for (std::vector<int>::iterator it = testIDs.begin(); it != testIDs.end(); ++it)
	{
		answerPacket = this->ReceiveOtherThanTestOutputPacket();

		GTestServiceBuffers::TestResult testResult;
		testResult.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize());
		EXPECT_EQ(*it, testResult.id());

		delete answerPacket;
		answerPacket = NULL;
	}

	/*receive final tests done packet*/
	answerPacket = this->ReceiveOtherThanTestOutputPacket();

	EXPECT_EQ((char)ID_UP_ALL_TESTS_DONE, answerPacket->GetID());

	delete answerPacket;
}

void GTestServiceCoreSystemTests::abortFunc()
{
	usleep(1500000); //sleep 1.5 seconds

	AdapterPacket abortPacket(
		(char)ID_DOWN_ABORT_TESTRUN,
		0 , 0,
		0,
		0, NULL,
		this->clientConnection->GetID()
	);

	this->clientConnection->Send(abortPacket);
}

void* GTestServiceCoreSystemTests::staticAbortFunc(void* thisPtr)
{
	GTestServiceCoreSystemTests* instancePtr = static_cast<GTestServiceCoreSystemTests*>(thisPtr);
	instancePtr->abortFunc();
	return NULL;
}

TEST_F(GTestServiceCoreSystemTests, AbortTests)
{
	AdapterPacket* answerPacket = NULL;

	GTestServiceBuffers::AllTestCases pbTestRunModel = GetPBTestRunModel();
	ASSERT_TRUE( pbTestRunModel.IsInitialized() );

	/*define which tests to execute*/
	const char* testCaseNames[4] = {
			"DISABLED_TestCase1",
			"DISABLED_TestCase2",
			"DISABLED_ThreeSecondsRuntime",
			"DISABLED_TestCase3"};

	std::vector<int> testIDs = this->getTestIDsForTestFromModel(&pbTestRunModel, testCaseNames, 4);

	LaunchTests(testIDs, 1);

	pthread_t abort_thread = 0;
	int errCode = pthread_create(&(abort_thread), NULL, this->staticAbortFunc, (void*)this);
	EXPECT_EQ(0,errCode);

	/* receive all test result packets - the results for TestCase1 and TestCase2 should be there.
	 * All other results cannot be there because the test process has been aborted.
	 * We are expecting 4 test results.
	 */
	int receivedTestResults = 0;
	for (std::vector<int>::iterator it = testIDs.begin(); it != testIDs.end(); ++it)
	{
		answerPacket = this->ReceiveOtherThanTestOutputPacket();

		GTestServiceBuffers::TestResult testResult;
		testResult.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize());
		EXPECT_EQ(*it, testResult.id());

		delete answerPacket;
		answerPacket = NULL;

		if ((++receivedTestResults) >= 4)
			break;
	}

	/*receive final abort acknowledge*/
	answerPacket = this->ReceiveOtherThanTestOutputPacket();
	EXPECT_EQ((char)ID_UP_ACK_ABORT, answerPacket->GetID());

	EXPECT_EQ(0,pthread_join(abort_thread,NULL));

	delete answerPacket;
}

TEST_F(GTestServiceCoreSystemTests, CloseConnectionOnUnkownPacketID)
{
	AdapterPacket packetWithUnknownID(
			(char)0xFF,
			0 , 0,
			0,
			0,
			NULL,
			this->clientConnection->GetID());

	this->clientConnection->Send(packetWithUnknownID);

	usleep(10000);

	EXPECT_TRUE(NULL == this->clientConnection->Receive());
	EXPECT_TRUE(this->clientConnection->IsClosed());
}

TEST_F(GTestServiceCoreSystemTests, GetAllResultsWithRepeatLargerThanResultBuffer)
{
	int numberOfTestResultsNotInBuffer = 500;

	AdapterPacket* answerPacket = NULL;
	int repeat=TestModel::GetResultBufferSize() + numberOfTestResultsNotInBuffer;

	GTestServiceBuffers::AllTestCases pbTestRunModel = GetPBTestRunModel();
	ASSERT_TRUE( pbTestRunModel.IsInitialized() ) << "pbTestRunModel not initialised\n";

	/*define which tests to execute*/
	const char* testCaseNames[1] = {"DISABLED_TestCase1"};
	std::vector<int> testIDs = this->getTestIDsForTestFromModel(&pbTestRunModel, testCaseNames, 1);

	LaunchTests(testIDs, repeat);

	/*receive all test result packets*/
	for (int iterationCount = 0; iterationCount < repeat; iterationCount++)
	{
		for (std::vector<int>::iterator it = testIDs.begin(); it != testIDs.end(); ++it)
		{
			answerPacket = this->ReceiveOtherThanTestOutputPacket();
			EXPECT_EQ((char)ID_UP_TESTRESULT, answerPacket->GetID()) << "Test output packet not correct\n";

			GTestServiceBuffers::TestResult testResult;
			testResult.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize());
			EXPECT_EQ(*it, testResult.id()) << "Test result doesn't match iterator\n";
			EXPECT_EQ(iterationCount+1, testResult.iteration()) << "Test iteration does not match count\n";

			delete answerPacket;
			answerPacket = NULL;
		}
	}

	/*receive final tests done packet*/
	answerPacket = this->ReceiveOtherThanTestOutputPacket();
	EXPECT_EQ((char)ID_UP_ALL_TESTS_DONE, answerPacket->GetID()) << "Answer packet not ALL_TESTS_DONE\n";
	delete answerPacket;

	AdapterPacket requestPacket(
		(char)ID_DOWN_GET_ALL_RESULTS,
		0 , 0,
		0,
		0,
		NULL,
		this->clientConnection->GetID());

	this->clientConnection->Send(requestPacket);

	/*receive all test result packets*/
	for (std::vector<int>::iterator it = testIDs.begin(); it != testIDs.end(); ++it)
	{
	        for (int iterationCount = 0; iterationCount < TestModel::GetResultBufferSize(); iterationCount++)
		{
			answerPacket = this->ReceiveOtherThanTestOutputPacket();
			EXPECT_EQ((char)ID_UP_TESTRESULT, answerPacket->GetID()) << "Answer packet ID not correct\n";
			EXPECT_EQ(requestPacket.GetSerial(), answerPacket->GetRequestSerial()) << "Answer packet serial does not match request packet serial\n";

			GTestServiceBuffers::TestResult testResult;
			testResult.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize());
			EXPECT_EQ(*it, testResult.id()) << "Test result id does not match iterator\n";
			EXPECT_EQ(repeat - TestModel::GetResultBufferSize() + iterationCount + 1, testResult.iteration()) << "Test result iteration does not match complex calculation\n";

			delete answerPacket;
			answerPacket = NULL;
		}
	}

	/*receive final tests done packet*/
	answerPacket = this->ReceiveOtherThanTestOutputPacket();

	EXPECT_EQ((char)ID_UP_ALL_RESULTS_DONE, answerPacket->GetID()) << "Answer packet ID not correct\n";
	EXPECT_EQ(requestPacket.GetSerial(), answerPacket->GetRequestSerial()) << "Answer packet serial does not match request packet serial\n";

	delete answerPacket;

}

TEST_F(GTestServiceCoreSystemTests, ExecWithCrashingGTestNoPersi)
{
	AdapterPacket* answerPacket = NULL;

	GTestServiceBuffers::AllTestCases pbTestRunModel = GetPBTestRunModel();
	ASSERT_TRUE( pbTestRunModel.IsInitialized() );

	/*define which tests to execute*/
	const char* testCaseNames[1] = {"DISABLED_SupposedToCrash"};
	std::vector<int> testIDs = this->getTestIDsForTestFromModel(&pbTestRunModel, testCaseNames, 1);

	LaunchTests(testIDs, 1);

	/*receive all test result packets*/
	for (int testIndex = 0; testIndex < 2; testIndex++)
	{
		answerPacket = this->ReceiveOtherThanTestOutputPacket();

		GTestServiceBuffers::TestResult testResult;
		testResult.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize());
		EXPECT_EQ(testIDs[testIndex], testResult.id());
		EXPECT_EQ( (testIndex == 0), testResult.success());

		delete answerPacket;
		answerPacket = NULL;
	}

	/*receive final tests done packet*/
	answerPacket = this->ReceiveOtherThanTestOutputPacket();
	EXPECT_EQ((char)ID_UP_ALL_TESTS_DONE, answerPacket->GetID());
	delete answerPacket;

}

class GTestServiceCoreSystemTestsWithPersi : public GTestServiceCoreSystemTests,
                                             public ::testing::WithParamInterface<int>{
protected:
	virtual void SetUp();
};

void GTestServiceCoreSystemTestsWithPersi::SetUp()
{
	std::string exePath = TestHelper::GetApplicationBinaryPath();
	this->core = new GTestServiceCore(exePath, "", GTEST_SERVICE_PORT_NO, true, NULL);

	this->core->RunThreaded();

	int sock = TestHelper::LocalConntect();
	clientConnection = new AdapterConnection(sock);
}

TEST_P(GTestServiceCoreSystemTestsWithPersi, ExecWithCrashingGTestPersi)
{
        int numberOfIterations = GetParam();

	AdapterPacket* answerPacket = NULL;

	GTestServiceBuffers::AllTestCases pbTestRunModel = GetPBTestRunModel();
	ASSERT_TRUE( pbTestRunModel.IsInitialized() );

	/*define which tests to execute*/
	const char* testCaseNames[1] = {"DISABLED_SupposedToCrash"};
	std::vector<int> testIDs = this->getTestIDsForTestFromModel(&pbTestRunModel, testCaseNames, 1);

	LaunchTests(testIDs, numberOfIterations);

	for (int iterationCount = 0; iterationCount < numberOfIterations; iterationCount++)
	{
		/*receive all test result packets*/
		for (int testIndex = 0; testIndex < 7; testIndex++)
		{
			answerPacket = this->ReceiveOtherThanTestOutputPacket();

			if (testIndex == 1 || testIndex == 4)
			{
				delete answerPacket;
				answerPacket = this->ReceiveOtherThanTestOutputPacket();
			}

			GTestServiceBuffers::TestResult testResult;
			testResult.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize());
			EXPECT_EQ(testIDs[testIndex], testResult.id());
			EXPECT_EQ(iterationCount+1, testResult.iteration());

			switch (testIndex)
			{
			case 0:
			case 2:
			case 3:
			case 5:
			case 6:
				EXPECT_TRUE( testResult.success());
				break;
			case 1:
			case 4:
				EXPECT_FALSE( testResult.success() );
				break;
			default:
				FAIL();
				break;
			}

			delete answerPacket;
			answerPacket = NULL;
		}
	}

	/*receive final tests done packet*/
	answerPacket = this->ReceiveOtherThanTestOutputPacket();

	EXPECT_EQ((char)ID_UP_ALL_TESTS_DONE, answerPacket->GetID());

	delete answerPacket;
}

TEST_P(GTestServiceCoreSystemTestsWithPersi, SendAllTestResults)
{
        int numberOfRepeats = GetParam();

	AdapterPacket* answerPacket = NULL;

	GTestServiceBuffers::AllTestCases pbTestRunModel = GetPBTestRunModel();
	ASSERT_TRUE( pbTestRunModel.IsInitialized() );

	/*define which tests to execute*/
	const char* testCaseNames[4] = {
			"DISABLED_TestCase1",
			"DISABLED_SupposedToCrash",
			"DISABLED_TestCase2",
			"DISABLED_TestCase3"};

	std::vector<int> testIDs = this->getTestIDsForTestFromModel(&pbTestRunModel, testCaseNames, 4);

	LaunchTests(testIDs, numberOfRepeats);

	char id = 0;
	while (id != ID_UP_ALL_TESTS_DONE)
	{
		answerPacket = this->ReceiveOtherThanTestOutputPacket();
		id = answerPacket->GetID();
		delete answerPacket;
	}

	/* now get the complete result list again */

	AdapterPacket requestPacket(
		(char)ID_DOWN_GET_ALL_RESULTS,
		0 , 0,
		0,
		0,
		NULL,
		this->clientConnection->GetID());

	this->clientConnection->Send(requestPacket);

	/*receive all test result packets*/
	for (std::vector<int>::iterator it = testIDs.begin(); it != testIDs.end(); ++it)
	{
		for (int iterationCount = 0; iterationCount < numberOfRepeats; iterationCount++)
		{
			answerPacket = this->ReceiveOtherThanTestOutputPacket();
			EXPECT_EQ((char)ID_UP_TESTRESULT, answerPacket->GetID());
			EXPECT_EQ(requestPacket.GetSerial(), answerPacket->GetRequestSerial());

			GTestServiceBuffers::TestResult testResult;
			testResult.ParseFromArray(answerPacket->GetPayload(), answerPacket->GetPayloadSize());
			EXPECT_EQ(*it, testResult.id());
			EXPECT_EQ(iterationCount + 1, testResult.iteration());

			delete answerPacket;
			answerPacket = NULL;
		}
	}

	/*receive final tests done packet*/
	answerPacket = this->ReceiveOtherThanTestOutputPacket();

	EXPECT_EQ((char)ID_UP_ALL_RESULTS_DONE, answerPacket->GetID());
	EXPECT_EQ(requestPacket.GetSerial(), answerPacket->GetRequestSerial());

	delete answerPacket;
}

INSTANTIATE_TEST_CASE_P(TestsCasesWithDifferentRepeatCounts,
		GTestServiceCoreSystemTestsWithPersi,
		::testing::Values(1,2,3,4,5));
