/*
 * ServiceAnnouncerTests.cpp
 *
 *  Created on: 03.12.2012
 *      Author: oto4hi
 */

#include <persigtest.h>
#include <Utils/ServiceAnnouncer.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define TEST_ANNOUNCE_UDP_PORT 5679

class ServiceAnnouncerTests : public ::testing::TestWithParam<const char*> {
};


TEST_P(ServiceAnnouncerTests, AnnounceService)
{
	const char* discoveryPacket = GTESTSERVICE_DISCOVERY_STRING;
	const char* answerMagic =     GTESTSERVICE_ANSWER_STRING;
	const char* testname = GetParam();

	int announcedPort = 6000;
	int announcePacketSize = strlen(discoveryPacket) + 1;
	int answerMagicLength = strlen(answerMagic) + 1;
	int answerPacketLength = answerMagicLength + ServiceAnnouncer::nameLength + sizeof(int);

	ServiceAnnouncer announcer = ServiceAnnouncer(announcedPort, TEST_ANNOUNCE_UDP_PORT, testname);
	announcer.Run();

	int sock = socket(AF_INET, SOCK_DGRAM, 0);

	struct sockaddr_in saSocket;
	memset(&saSocket, 0, sizeof(saSocket));
	saSocket.sin_family      = AF_INET;
	saSocket.sin_addr.s_addr = htonl(INADDR_ANY);
	saSocket.sin_port        = htons(TEST_ANNOUNCE_UDP_PORT);

	char* recvBuffer = (char*)malloc(answerPacketLength);

	struct sockaddr_in fromAddr;
	int fromAddrLen = sizeof(fromAddr);
	int rc = 0;

	while (rc != answerPacketLength)
	{
		sendto(sock, (void*)discoveryPacket, announcePacketSize, MSG_DONTWAIT, (struct sockaddr *)&saSocket, sizeof(saSocket));
		rc = recvfrom(sock, (char *)recvBuffer, answerPacketLength, MSG_DONTWAIT, (struct sockaddr *) &fromAddr, (socklen_t*)&fromAddrLen);
		usleep(10000);
	}

	int32_t portValue = ntohl(*(int*)&(recvBuffer[answerPacketLength - sizeof(int32_t)]));

	EXPECT_EQ(0, strcmp(answerMagic, recvBuffer));
	EXPECT_EQ(0, strncmp(testname, &(recvBuffer[answerMagicLength]), ServiceAnnouncer::nameLength - 1));
	EXPECT_EQ(announcedPort, portValue);

	free(recvBuffer);
	announcer.Stop();
}

INSTANTIATE_TEST_CASE_P(TestsWithDifferentNameLengthValues,
		ServiceAnnouncerTests,
		::testing::Values(
				"",								//length = 0
				"a", 							//length = 1
				"normalname	",					//length = 10
				"verylonglonglongtestname"));	//length = 24
