import sys

global lineNo;
lineNo=0;


def main(args):
	f = file(args[0],"r")

	tk = tokenizer(f)

	t = tk.next()
	if not t==TOK_IDENT('package'):
		raise Exception("should start with 'package', not "+repr(t))
	t = tk.next()
	if not isinstance(t,TOK_IDENT):
		raise Exception("package needs name.")
	pack_name = t.name;
	t = tk.next()
	if not t==TOK_SYM(';'):
		raise Exception("expect ';' after fieldnum, not "+repr(t))

	global all_dtyp;
	all_dtyp = set()
	messages = dict()
	enums = dict()

	basetypes = {
		'int16':'short',
		'uint16':'ushort',
		'int32':'int',
		'uint32':'uint',
		'int64':'long',
		'uint64':'ulong',
		'bool':'bool',
		'string':'string',
		'float':'double'
	}

	while True:
		try:
			t = tk.next()
		except StopIteration,ex:
			break

		if t==TOK_IDENT('message'):
			msg = parse_message(tk);
			messages[msg.name] = msg
		elif t==TOK_IDENT('enum'):
			enu = parse_enum(tk);
			enums[enu.name] = enu
		else:
			raise Exception("unexpected token type. Expected 'message'. Found "+t.repr())


	# check datatypes:
	for dtyp in all_dtyp:
		if (dtyp not in basetypes) and (dtyp not in messages) and (dtyp not in enums):
				raise Exception("invalid type %s used."%dtyp)


	# output
	print "using ProtoBuf;\n"
	print "namespace %s\n{" % pack_name
	for enuName in enums:
		enu = enums[enuName]
		print "\n\t[ProtoContract]"
		print "\tpublic enum %s" % enuName
		print "\t{"
		_ls = None
		for val in enu.values:
			_key = enu.values[val]
			if _ls is not None:
				print _ls+","
			_ls = "\t\t%s = %d" % (_key,val)
		print _ls
		print "\t};"

	for msgName in messages:
		msg = messages[msgName]
		print "\n\t[ProtoContract]"
		print "\tpublic class %s" % msgName
		print "\t{"
		for fld in msg.fields:
			req = ""
			if fld.opt==1:
				req = ",IsRequired=true"
			print "\t\t[ProtoMember(%d%s)]" % (fld.fieldNum,req)
			if fld.type in basetypes:
				tp = basetypes[fld.type]
			else:
				tp = fld.type
			if fld.opt==2:
				tp = 'List<%s>' % tp;
			print "\t\tpublic %s %s;" % (tp,fld.name)
		print "\t}"

	print "}"



def parse_message(tk):
	global all_dtyp;
	global lineNo;
	t = tk.next()
	if not isinstance(t,TOK_IDENT):
		raise Exception("Expect message name, not "+repr(t))
	name = t.name
	t = tk.next()
	if not t==TOK_BRACK('{'):
		raise Exception("expect '{' after 'message', not "+repr(t))

	t = tk.next()
	bOpt=False
	bReq=False
	bRep=False
	msg = MESSAGE(name)
	while not t==TOK_BRACK('}'):
		if t == TOK_IDENT('optional'):
			typ = 0;
		elif t == TOK_IDENT('required'):
			typ = 1;
		elif t == TOK_IDENT('repeated'):
			typ = 2;
		else:
			raise Exception("expect one of 'optional', 'required' or 'repeated', not "+repr(t))
		t = tk.next()
		if not isinstance(t,TOK_IDENT):
			raise Exception("Expect datatype, not "+repr(t))
		dtyp = t.name
		all_dtyp.add(dtyp)
		t = tk.next()
		if not isinstance(t,TOK_IDENT):
			raise Exception("Expect item name, not "+repr(t))
		name = t.name
		t = tk.next()
		if not t==TOK_SYM('='):
			raise Exception(str(lineNo)+": expect '=' after itemname, not "+repr(t))
		t = tk.next()
		if not isinstance(t,TOK_NUMERAL):
			raise Exception("Expect fieldnumber, not "+repr(t))
		fieldNo = t.num
		if fieldNo<1:
			raise Exception(str(lineNo)+": fieldnumber must be >=1, not "+repr(fieldNo))
		t = tk.next()
		if not t==TOK_SYM(';'):
			raise Exception(str(lineNo)+": expect ';' after fieldnum, not "+repr(t))
		t = tk.next()
		fld = FIELD(name,dtyp,fieldNo,typ)
		msg.fields.append(fld)

	return msg;


def parse_enum(tk):
	global all_dtyp;

	t = tk.next()
	if not isinstance(t,TOK_IDENT):
		raise Exception("Expect enum name, not "+repr(t))
	name = t.name
	t = tk.next()
	if not t==TOK_BRACK('{'):
		raise Exception("expect '{' after 'enum', not "+repr(t))

	_count=0
	enu = ENUM(name)
	#print "enum "+enu.name
	_names = set()
	t=tk.next()
	while not t==TOK_BRACK('}'):
		# identifier
		if not isinstance(t,TOK_IDENT):
			raise Exception("Expect key name, not "+repr(t))
		_keyName = t.name
		t=tk.next()
		# equals sign
		if t==TOK_SYM('='):
			t=tk.next()
			# value
			if not isinstance(t,TOK_NUMERAL):
				raise Exception("Expect value for "+_keyName+", not "+repr(t))
			_keyVal = t.num
			_count = _keyVal
			t=tk.next()
		else:
			_keyVal = _count
		_count += 1
		# semicolon (optional)
		if t==TOK_SYM(';'):
			t=tk.next();

		if _keyVal in enu.values:
			raise Exception("Duplicate value %d in enum"%_keyVal)
		if _keyName in _names:
			raise Exception("Duplicate name %s in enum"%_keyName.repr())
		enu.values[_keyVal]=_keyName
		#print "%s = %d" % (_keyName,_keyVal)
		_names.add(_keyName)

	if len(enu.values)<1:
		raise Exception("enum should not be empty.")

	all_dtyp.add(enu.name)
	#print repr(all_dtyp)
	return enu;



class FIELD(object):
	__slots__=('name','type','fieldNum','opt')
	def __init__(self,name,type,id,optFlg):
		self.name=name
		self.type=type
		self.fieldNum=id
		self.opt=optFlg
class MESSAGE(object):
	__slots__=('name','fields')
	def __init__(self,name):
		self.name = name
		self.fields = []
class ENUM(object):
	__slots__=('name','values')
	def __init__(self,name):
		self.name = name
		self.values = dict()


class TOKEN(object):
	__slots__=()
	def __init__(self):
		pass
class TOK_IDENT(TOKEN):
	__slots__=('name')
	def __init__(self,identifier):
		TOKEN.__init__(self)
		self.name=identifier
	def __eq__(self,other):
		return type(self)==type(other) and self.name==other.name
	def __repr__(self):
		return "TOK_IDENT(%s)"%repr(self.name)
class TOK_NUMERAL(TOKEN):
	__slots__=('num')
	def __init__(self,value):
		TOKEN.__init__(self)
		self.num=value
	def __eq__(self,other):
		return type(self)==type(other) and self.num==other.num
class TOK_SYM(TOKEN):
	__slots__=('sym')
	def __init__(self,sym):
		TOKEN.__init__(self)
		self.sym=sym
	def __eq__(self,other):
		return type(self)==type(other) and self.sym==other.sym
	def __repr__(self):
		return "TOK_SYM(%s)"%repr(self.sym)
class TOK_BRACK(TOKEN):
	__slots__=('sym')
	def __init__(self,sym):
		TOKEN.__init__(self)
		self.sym=sym
	def __eq__(self,other):
		return type(self)==type(other) and self.sym==other.sym



def tokenizer(fil):
	global lineNo;
	lineNo=0;
	for line in fil:
		lineNo += 1
		line = line.split('//')[0]
		line = line.split('#')[0]
		line.strip()
		if line=="":
			continue
		i=0
		while i<len(line):
			b = line[i]
			if b in " \t\r\n":
				# whitespace
				i+=1;continue
			if b=='#' or (b=='/' and i+1<len(line) and line[i+1]=='/'):
				# comment
				break
			if b in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_":
				# identifier
				j=i+1
				while j<len(line):
					if line[j] not in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789":
						break
					j+=1
				yield TOK_IDENT(line[i:j])
				i=j;continue
			if (b in "0123456789") or (b=='-' and i+1<len(line) and line[i+1] in '0123456789'):
				# numeral
				j=i+1
				while j<len(line):
					if line[j] not in "0123456789":
						break
					j+=1
				yield TOK_NUMERAL(int(line[i:j]))
				i=j;continue
			if b in "(){}[]":
				# bracket
				yield TOK_BRACK(b)
				i+=1;continue
			if b in "=;":
				# symbol
				yield TOK_SYM(b)
				i+=1;continue
			raise Exception("unknown token at: "+line[i:])
	return




if __name__=="__main__":
	sys.exit(main(sys.argv[1:]))
