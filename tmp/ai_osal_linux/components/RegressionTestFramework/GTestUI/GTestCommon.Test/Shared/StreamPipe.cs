



namespace Shared
{
	/// <summary>
	/// A simple local pipe class with two IO.Stream ends. Passes data forth and back.
	/// The pipe has no buffer and will block one end until the other get/sends data.
	/// It does not support multiple async-requests queuing. (not yet?)
	/// </summary>
	public class StreamPipe : System.IO.Stream
	{
		/// <summary>
		/// Create Pipe. Creates and connects both ends of the pipe. property 'otherEnd' has the other IO.Stream reference.
		/// </summary>
		public StreamPipe()
		{
			m_closed = false;
			m_asyRead=null;
			m_asyWrite=null;
			m_statReceivedTotal=0u;
			m_mtx = new System.Threading.Mutex();
			m_peer = new StreamPipe(this);
		}

		private StreamPipe(StreamPipe peer)
		{
			m_closed = false;
			m_asyRead=null;
			m_asyWrite=null;
			m_statReceivedTotal=0u;
			m_mtx = peer.m_mtx;
			m_peer = peer;
		}

		/// <summary>
		/// other end of the pipe. The Constructor creates both. Use this to get second end.
		/// </summary>
		public StreamPipe otherEnd{get{return m_peer;}}

		public override bool CanRead{get{return true;}}
		public override bool CanSeek{get{return false;}}
		public override bool CanTimeout{get{return false;}}
		public override bool CanWrite{get{return true;}}
		public override long Length{get{throw new System.NotSupportedException();}}
		public override long Position{get{throw new System.NotSupportedException();}set{throw new System.NotSupportedException();}}

		public override System.IAsyncResult BeginRead(byte[] buffer, int offset, int count, System.AsyncCallback callback, object state)
		{
			m_mtx.WaitOne();
			if(m_closed)
				{m_mtx.ReleaseMutex();throw new System.IO.IOException("stream was closed.");}
			if(m_asyRead!=null)
				{m_mtx.ReleaseMutex();throw new System.IO.IOException("read already active.");}
			m_asyRead = new SPAsy(this,true,buffer,offset,count,callback,state);
			procRead();
			m_asyRead.compSyn = m_asyRead.total>0||m_asyRead.count<=0;
			m_asyRead.needSig = !m_asyRead.compSyn;
		  System.IAsyncResult res = m_asyRead;
			m_mtx.ReleaseMutex();
			return res;
		}

		public override System.IAsyncResult BeginWrite(byte[] buffer, int offset, int count, System.AsyncCallback callback, object state)
		{
			m_mtx.WaitOne();
			if(m_closed)
				{m_mtx.ReleaseMutex();throw new System.IO.IOException("stream was closed.");}
			if(m_asyWrite!=null)
			{
				m_mtx.ReleaseMutex();throw new System.IO.IOException("write already active.");
			}
			m_asyWrite = new SPAsy(this,false,buffer,offset,count,callback,state);
			procWrite();
			m_asyWrite.compSyn = m_asyWrite.count<=0;
			m_asyWrite.needSig = !m_asyWrite.compSyn;
		  System.IAsyncResult res = m_asyWrite;
			m_mtx.ReleaseMutex();
			return res;
		}

		/// <summary>
		/// shut down the pipe. Closes both ends.
		/// </summary>
		public override void Close()
		{
		  bool callOther = !m_closed;
			m_closed=true;
			m_peer.m_closed=true;
			if(m_asyRead!=null&&m_asyRead.needSig&&m_asyRead.count>0)
			{
				m_asyRead.needSig=false;
				m_asyRead.count=0;
				if(m_asyRead.cb!=null)
					m_asyRead.cb(m_asyRead);
				m_asyRead.evt.Set();
			}
			if(m_asyWrite!=null&&m_asyWrite.needSig&&m_asyWrite.count>0)
			{
				m_asyWrite.needSig=false;
				m_asyWrite.count=0;
				if(m_asyWrite.cb!=null)
					m_asyWrite.cb(m_asyWrite);
				m_asyWrite.evt.Set();
			}
			if(callOther)
				m_peer.Close();
			base.Close();
		}

		public override int EndRead(System.IAsyncResult asyncResult)
		{
			m_mtx.WaitOne();
		  SPAsy asy = m_asyRead;
			if(!object.ReferenceEquals(asyncResult,asy))
				{m_mtx.ReleaseMutex();throw new System.ArgumentException();}
			if(m_closed)
				{m_mtx.ReleaseMutex();throw new System.IO.IOException("stream was closed.");}
			while( !asy.IsCompleted )
			{
				m_mtx.ReleaseMutex();
				asy.evt.WaitOne();
				m_mtx.WaitOne();
			}
			if(!object.ReferenceEquals(asy,asyncResult))
				{m_mtx.ReleaseMutex();throw new System.IO.IOException("another thread completed the read IO.");}
		  int res = asy.total;
			m_statReceivedTotal += (uint)res;
			m_asyRead=null;
			m_mtx.ReleaseMutex();
			return res;
		}

		public override void EndWrite(System.IAsyncResult asyncResult)
		{
			m_mtx.WaitOne();
		  SPAsy asy = m_asyWrite;
			if(!object.ReferenceEquals(asyncResult,asy))
				{m_mtx.ReleaseMutex();throw new System.ArgumentException();}
			if(m_closed)
				{m_mtx.ReleaseMutex();throw new System.IO.IOException("stream was closed.");}
			while( !asy.IsCompleted )
			{
				m_mtx.ReleaseMutex();
				asy.evt.WaitOne();
				m_mtx.WaitOne();
			}
			if(!object.ReferenceEquals(asy,asyncResult))
				{m_mtx.ReleaseMutex();throw new System.IO.IOException("another thread completed the write IO.");}
			m_asyWrite=null;
			m_mtx.ReleaseMutex();
		}

		public override void Flush()
		{
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			return EndRead(BeginRead(buffer,offset,count,null,null));
		}

		public override int ReadByte()
		{
		  byte[] dummy = new byte[1];
			if( 1 != Read(dummy,0,1) )
				throw new System.IO.IOException();
			return dummy[0];
		}

		public override long Seek(long offset, System.IO.SeekOrigin origin)
		{
			throw new System.NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new System.NotSupportedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			EndWrite(BeginWrite(buffer,offset,count,null,null));
		}

		public override void WriteByte(byte value)
		{
		  byte[] dummy = new byte[]{value};
			Write(dummy,0,1);
		}

		private bool procRead()
		{
			if( m_asyRead==null )
				return false;
			// Take from other end's write async.
		  int num=0;
			if( m_asyRead.count>0 && m_peer.m_asyWrite!=null && m_peer.m_asyWrite.count>0 )
			{
				num = m_peer.m_asyWrite.count;
				if(num>m_asyRead.count)
					num=m_asyRead.count;
				System.Array.Copy( m_peer.m_asyWrite.buffer , m_peer.m_asyWrite.pos , m_asyRead.buffer , m_asyRead.pos , num );
				m_asyRead.pos+=num;
				m_asyRead.count-=num;
				m_asyRead.total += num;
				m_peer.m_asyWrite.pos+=num;
				m_peer.m_asyWrite.count-=num;
				m_peer.m_asyWrite.total+=num;
				if( m_peer.m_asyWrite.count<=0 && m_peer.m_asyWrite.needSig )
				{
					m_peer.m_asyWrite.needSig=false;
					if(m_peer.m_asyWrite.cb!=null)
						m_peer.m_asyWrite.cb(m_peer.m_asyWrite);
					m_peer.m_asyWrite.evt.Set();
				}
			}
			// check done
			if( m_asyRead.needSig && (m_asyRead.total>0||m_asyRead.count<=0) )
			{
				m_asyRead.needSig=false;
				if(m_asyRead.cb!=null)
					m_asyRead.cb(m_asyRead);
				m_asyRead.evt.Set();
			}
			return num>0;
		}

		private bool procWrite()
		{
			if( m_asyWrite==null )
				return false;
		  int num=0;
			// Feed to other peer's read.
			if( m_asyWrite.count>0 && m_peer.m_asyRead!=null && m_peer.m_asyRead.count>0 )
			{
				// error if already data in buffer. why wasn't that read before?
				num = m_peer.m_asyRead.count;
				if(num>m_asyWrite.count)
					num=m_asyWrite.count;
				System.Array.Copy( m_asyWrite.buffer , m_asyWrite.pos , m_peer.m_asyRead.buffer , m_peer.m_asyRead.pos , num );
				m_asyWrite.pos+=num;
				m_asyWrite.count-=num;
				m_asyWrite.total += num;
				m_peer.m_asyRead.pos+=num;
				m_peer.m_asyRead.count-=num;
				m_peer.m_asyRead.total+=num;
				if( m_peer.m_asyRead.needSig && (m_peer.m_asyRead.total>0||m_peer.m_asyRead.count<=0) )
				{
					m_peer.m_asyRead.needSig=false;
					if(m_peer.m_asyRead.cb!=null)
						m_peer.m_asyRead.cb(m_peer.m_asyRead);
					m_peer.m_asyRead.evt.Set();
				}
			}
			// check done.
			if( m_asyWrite.needSig && m_asyWrite.count<=0 )
			{
				m_asyWrite.needSig=false;
				if(m_asyWrite.cb!=null)
					m_asyWrite.cb(m_asyWrite);
				m_asyWrite.evt.Set();
			}
			return num>0;
		}




		private StreamPipe m_peer;
		private bool m_closed;
		private SPAsy m_asyRead;
		private SPAsy m_asyWrite;
		private uint m_statReceivedTotal;
		private System.Threading.Mutex m_mtx;

		private class SPAsy : System.IAsyncResult
		{
			public SPAsy(StreamPipe pp,bool bRead,byte[] buffer,int pos,int count,System.AsyncCallback cb,object state)			{pipe=pp;reading=bRead;this.buffer=buffer;this.pos=pos;this.count=count;total=0;needSig=false;compSyn=true;this.cb=cb;this.state=state;evt=new System.Threading.ManualResetEvent(false);}
			StreamPipe pipe;
			bool reading;
			public byte[] buffer;
			public int pos;
			public int count;
			public int total;
			public System.AsyncCallback cb;
			public object state;
			public bool needSig;
			public bool compSyn;
			public System.Threading.EventWaitHandle evt;

			public object AsyncState{get{return state;}}
			public System.Threading.WaitHandle AsyncWaitHandle{get{return evt;}}
			public bool CompletedSynchronously{get{return compSyn;}}
			public bool IsCompleted{get{return compSyn||count<=0||(reading&&total>0);}}
		}

	}

}