using NUnit.Framework;
using System.Collections.Generic;
using System.Net;

using GTestCommon.Utils.ServiceDiscovery;
using GTestCommon.Utils.ServiceDiscovery.Interfaces;
using GTestCommon.Utils.ServiceDiscovery.Models;
using GTestCommon.Utils.ServiceDiscovery.GTestDiscovery.GTestServiceDiscovery;
using GTestCommon.Utils.ServiceDiscovery.GTestDiscovery.GTestAdapterDiscovery;

namespace GTestCommon_Test
{

	[TestFixture]
	public class GTestServiceDiscovery_Test
	{
		private const int servicePort = 9999;
		private const int serviceAnnounceUDPPort = 4228;

		private const int adapterPort = 9998;
		private const int adapterAnnounceUDPPort = 4227;

		private const int discoveryTimeout = 200;

		private readonly string serviceName = "TestServiceName";
		private readonly string adapterName = "TestAdapterName";

		private ServiceAnnouncer serviceAnnouncer;
		private ServiceAnnouncer adapterAnnouncer;

		private void SetupAdapterAnnouncer()
		{
			ServiceModel serviceModel = new ServiceModel(new ServiceAddr(IPAddress.Any, servicePort), serviceName);
			IServiceAnnouncementPacketFactory packetFactory = new GTestServiceAnnouncementPacketFactory(serviceModel);

			ServiceAnnouncementModel announcementModel = new ServiceAnnouncementModel(serviceModel, serviceAnnounceUDPPort);
			serviceAnnouncer = new ServiceAnnouncer(announcementModel, packetFactory);

			serviceAnnouncer.Run();
			System.Threading.Thread.Sleep(100);
		}

		private void SetupServiceAnnouncer()
		{
			ServiceModel serviceModel = new ServiceModel(new ServiceAddr(IPAddress.Any, adapterPort), adapterName);
			IServiceAnnouncementPacketFactory packetFactory = new GTestAdapterAnnouncementPacketFactory(serviceModel);

			ServiceAnnouncementModel announcementModel = new ServiceAnnouncementModel(serviceModel, adapterAnnounceUDPPort);
			adapterAnnouncer = new ServiceAnnouncer(announcementModel, packetFactory);

			adapterAnnouncer.Run();
			System.Threading.Thread.Sleep(100);
		}

		[SetUp]
		public void setup()
		{
			SetupAdapterAnnouncer();
			SetupServiceAnnouncer();
		}

		[TearDown]
		public void teardown()
		{
			serviceAnnouncer.Stop();
			adapterAnnouncer.Stop();
		}

		[Test]
		public void AnnounceAndDiscoverService()
		{
			IServiceAnnouncementPacketFactory packetFactory = new GTestServiceAnnouncementPacketFactory();

			ServiceDiscovery discovery = new ServiceDiscovery(serviceAnnounceUDPPort, packetFactory);
			List<ServiceModel> serviceModelList = discovery.DiscoverServiceProviders(discoveryTimeout);

			Assert.AreEqual(1, serviceModelList.Count);
			Assert.AreEqual(0, serviceModelList[0].Name.CompareTo(serviceName));
			Assert.AreEqual(servicePort, serviceModelList[0].ServiceAddress.Port);
		}

		[Test]
		public void AnnounceAndDiscoverAdapter()
		{
			IServiceAnnouncementPacketFactory packetFactory = new GTestAdapterAnnouncementPacketFactory();

			ServiceDiscovery discovery = new ServiceDiscovery(adapterAnnounceUDPPort, packetFactory);
			List<ServiceModel> serviceModelList = discovery.DiscoverServiceProviders(discoveryTimeout);

			Assert.AreEqual(1, serviceModelList.Count);
			Assert.AreEqual(0, serviceModelList[0].Name.CompareTo(adapterName));
			Assert.AreEqual(adapterPort, serviceModelList[0].ServiceAddress.Port);
		}
	}
}
