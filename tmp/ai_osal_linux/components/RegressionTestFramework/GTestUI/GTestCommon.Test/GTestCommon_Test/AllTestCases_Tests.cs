using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
//using NUnit.Mocks;

using GTestCommon.Models;

namespace GTestCommon_Test
{   
    [TestFixture]
    class AllTestCases_Tests
    {
        private AllTestCases model;
        private bool eventHasOccurred;

        [TestFixtureSetUp]
        public void Init()
        {
        }

        [TestFixtureTearDown]
        public void Cleanup()
        {
        }

        [SetUp]
        public void TestInit()
        {
            model = new AllTestCases(true);

            for (int testGroupIndex = 0; testGroupIndex < 5; testGroupIndex++)
            {
                TestGroupModel testGroup = model.AppendNewTestGroup("TestGroup" + testGroupIndex);
                
                testGroup.AppendNewTest( "Test1" , (uint)(1+3*testGroupIndex) );
                testGroup.AppendNewTest( "DISABLED_Test2" , (uint)(2+3*testGroupIndex) );
                testGroup.AppendNewTest( "Test3" , (uint)(3+3*testGroupIndex) );
            }

            eventHasOccurred = false;
        }

        [TearDown]
        public void TestCleanup()
        {
        }        

        [Test]
        public void TestRunModelTestCount()
        {
            Assert.AreEqual(5, model.TestGroupCount);

            for (int testGroupIndex = 0; testGroupIndex < 5; testGroupIndex++)
            {
                Assert.AreEqual(3, model[testGroupIndex].TestCount);
            }
        }

        void OnTestEnabledChanged(TestGroupModel sender, Test testModel)
        {
            this.eventHasOccurred = true;
            Assert.AreEqual("TestGroup3", sender.Name);
            Assert.AreEqual("Test3", testModel.Name);
        }

        [Test]
        public void TestRaiseTestEnabledChanged()
        {
            model.eventTestEnabledChanged += new ExtTestGroupEventHandler(OnTestEnabledChanged);

            model[3][2].Enabled = false;

            Assert.IsTrue(this.eventHasOccurred);
        }

        [Test]
        public void AddSameIdThrows()
        {
            TestGroupModel group = this.model[0];
            Assert.Throws<ArgumentException>(delegate()
                {
                    group.AppendNewTest("test_with_same_ID",10);
                }
            );
        }

        [Test]
        public void FindTestById()
        {
            Test tst_by_id = this.model.getTestById(2);
            Test tst_by_index = this.model[0][1];
            // this must be first group, second test (knowing from how the set was set up.
            Assert.True( object.ReferenceEquals( tst_by_id , tst_by_index ) );
        }

        void OnTestGroupIsDone(TestGroupModel sender)
        {
            this.eventHasOccurred = true;
            Assert.AreEqual("TestGroup0", sender.Name);
        }

        [Test]
        public void TestRaiseTestGroupIsDone()
        {
            model.eventTestGroupIsDone += new TestGroupEventHandler(OnTestGroupIsDone);

            model[0][0].AddResult(true);
            model[0][1].AddResult(false);
            model[0][2].AddResult(true);

            Assert.AreEqual(0, model[0].RemainingTestCount);
            Assert.IsTrue(this.eventHasOccurred);
        }

        void OnTestGroupSelectionChanged(TestGroupModel sender)
        {
            this.eventHasOccurred = true;
        }

        [Test]
        public void TestRaiseTestGroupSelectionChanged_ALL_BUT_DEFAULT_DISABLED()
        {
            model.eventTestGroupSelectionChanged += new TestGroupEventHandler(OnTestGroupSelectionChanged);

            model[0][1].Enabled = false;
            Assert.AreEqual(TESTGROUPSELECTION.ALL_BUT_DEFAULT_DISABLED, model[0].TestGroupSelection);

            Assert.IsTrue(this.eventHasOccurred);
        }

        [Test]
        public void TestRaiseTestGroupSelectionChanged_ALL()
        {
            model[0][1].Enabled = false;
            model.eventTestGroupSelectionChanged += new TestGroupEventHandler(OnTestGroupSelectionChanged);

            model[0][1].Enabled = true;
            Assert.AreEqual(TESTGROUPSELECTION.ALL, model[0].TestGroupSelection);

            Assert.IsTrue(this.eventHasOccurred);
        }

        [Test]
        public void TestRaiseTestGroupSelectionChanged_PARTLY()
        {
            model.eventTestGroupSelectionChanged += new TestGroupEventHandler(OnTestGroupSelectionChanged);

            model[0][0].Enabled = false;
            Assert.AreEqual(TESTGROUPSELECTION.PARTLY, model[0].TestGroupSelection);

            Assert.IsTrue(this.eventHasOccurred);
        }

        [Test]
        public void TestRaiseTestGroupSelectionChanged_NONE()
        {
            model[0][0].Enabled = false;
            model[0][1].Enabled = false;
            model.eventTestGroupSelectionChanged += new TestGroupEventHandler(OnTestGroupSelectionChanged);

            model[0][2].Enabled = false;

            Assert.AreEqual(TESTGROUPSELECTION.NONE, model[0].TestGroupSelection);

            Assert.IsTrue(this.eventHasOccurred);
        }

        void OnTestExecutionStateChanged(TestGroupModel sender, Test testModel)
        {
            this.eventHasOccurred = true;

            Assert.AreEqual("TestGroup2", sender.Name);
            Assert.AreEqual("DISABLED_Test2", testModel.Name);
        }

        [Test]
        public void TestRaiseTestExecutionStateChanged()
        {
            model.eventTestExecutionStateChanged += new ExtTestGroupEventHandler(OnTestExecutionStateChanged);

            model[2][1].AddResult(true);

            Assert.IsTrue(this.eventHasOccurred);
        }

        [Test]
        public void EnableAllButDefaultDisabled()
        {
            model.EnableDefaultDISABLED_Tests = false;
            model.EnableAll();

            for (int TestGroupIndex = 0; TestGroupIndex < 5; TestGroupIndex++)
            {
                Assert.IsTrue(model[TestGroupIndex][0].Enabled);
                Assert.IsFalse(model[TestGroupIndex][1].Enabled);
                Assert.IsTrue(model[TestGroupIndex][2].Enabled);
            }
        }

        [Test]
        public void DisableAll()
        {
            model.DisableAll();

            for (int TestGroupIndex = 0; TestGroupIndex < 5; TestGroupIndex++)
            {
                Assert.IsFalse(model[TestGroupIndex][0].Enabled);
                Assert.IsFalse(model[TestGroupIndex][1].Enabled);
                Assert.IsFalse(model[TestGroupIndex][2].Enabled);
            }
        }

        [Test]
        public void EnableAll()
        {
            model.DisableAll();

            model.EnableDefaultDISABLED_Tests = true;
            model.EnableAll();

            for (int TestGroupIndex = 0; TestGroupIndex < 5; TestGroupIndex++)
            {
                Assert.IsTrue(model[TestGroupIndex][0].Enabled);
                Assert.IsTrue(model[TestGroupIndex][1].Enabled);
                Assert.IsTrue(model[TestGroupIndex][2].Enabled);
            }
        }
    }
}
