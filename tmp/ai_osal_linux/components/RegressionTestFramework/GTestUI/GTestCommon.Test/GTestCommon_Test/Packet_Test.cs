using GTestCommon.Links;
using NUnit.Framework;


namespace GTestCommon_Test
{

	[TestFixture]
	public class Packet_Test
	{
		public Packet_Test()
		{
			m_buffer = new byte[0x1000];
		}

		[SetUp]
		public void setup()
		{
		}

		[Test]
		public void encode()
		{
		  Packet pck = new Packet(Packet.PacketID.ID_DOWN_GET_VERSION);
		  uint size;
			size = pck.encode(m_buffer,0,10);
			Assert.AreEqual( size , Packet.PACKET_HEADER_SIZE+10 );
			// Id must be in first byte, payloadsize must be in offset 12.
			// factory expects this as it must know this BEFORE constructing the packet.
			Assert.AreEqual( (byte)pck.ID , m_buffer[0] );
			Assert.AreEqual( 10 , (uint)System.Net.IPAddress.NetworkToHostOrder((int)System.BitConverter.ToUInt32(m_buffer,12)) );
		}

		[Test]
		public void encode_decode()
		{
		  Packet pck1,pck2;
		  uint size;
			// encode head
			pck1 = new Packet(Packet.PacketID.ID_DOWN_GET_VERSION);
			pck1.serial = 0x2345678u;
			pck1.reqSerial = 0xFEDCBA98u;
			pck1.flags = (byte)0xAAu;
			size = pck1.encode(m_buffer,0,10);
			// decode head
			pck2 = new Packet(m_buffer,0,10);
			Assert.AreEqual( pck1.ID , pck2.ID );
			Assert.AreEqual( pck1.flags , pck2.flags );
			Assert.AreEqual( pck1.serial , pck2.serial );
			Assert.AreEqual( pck1.reqSerial , pck2.reqSerial );
		}

		[Test]
		public void encode_bufferTooSmall()
		{
		  Packet pck = new Packet(Packet.PacketID.ID_UP_ABORT);
		  byte[] buf = new byte[Packet.PACKET_HEADER_SIZE-1];
			Assert.Throws<System.ArgumentException>( delegate(){pck.encode(buf,0);} );
		}

		private byte[] m_buffer;
	}
}
