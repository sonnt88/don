using System.Collections.Generic;
using NUnit.Framework;



namespace GTestCommon_Test
{


[TestFixture]
class TcpPacketLink_Test
{
	private const ushort PORT = 27035;

	public TcpPacketLink_Test()
	{
		m_packetFactory = new TstPckFac();
		m_connEvt = new System.Threading.AutoResetEvent(false);
	}

	private class TstPck : GTestCommon.IPacket
	{
		public TstPck(){body=new byte[0];}
		public TstPck(uint size){body=new byte[(int)size];}
		public TstPck(byte[] data){body=(byte[])data.Clone();}
		public uint serial{get{return m_ser;}set{m_ser=value;}}
		public byte[] body;
		private uint m_ser;
	}

	private class TstPckFac : GTestCommon.IPacketConverter
	{
		public uint encodePacket(GTestCommon.IPacket pck,byte[] buffer,uint start,uint maxCount)
		{
		  TstPck p = (TstPck)pck;
		  uint len =(uint)p.body.Length;
			buffer[start] = (byte)(len>>8);
			buffer[start+1] = (byte)(len&255);
			System.Array.Copy( p.body , 0 , buffer , start+2 , len );
			countEnc++;
			return len+2;
		}
		public GTestCommon.IPacket decodePacket(byte[] buffer,uint start,uint count,out uint skipData)
		{
			skipData=0;
			if(count<1)return null;
			if(count<2)
				throw new GTestCommon.InsufficientData();
		  uint len = (((uint)buffer[start])<<8)+buffer[start+1];
			if(count<(uint)(len+2u))
				throw new GTestCommon.InsufficientData();
		  TstPck res = new TstPck(len);
			System.Array.Copy( buffer , start+2 , res.body , 0 , len );
			skipData = len+2;
			countDec++;
			return res;
		}
		public uint countEnc,countDec;
	}

	[SetUp]
	public void setup()
	{
		m_link1 = new GTestCommon.Links.TcpPacketLink(m_packetFactory);
		m_link2 = new GTestCommon.Links.TcpPacketUplink(m_packetFactory);
		m_conn=false;
		m_connEvt.Reset();
		m_link1.connect += new GTestCommon.Links.ConnectEventHandler(m_link1_connect);
	}

	void m_link1_connect(bool bNowConnected)
	{
		m_conn=bNowConnected;
		m_connEvt.Set();
	}

	private void startCli()
	{
		m_link1.Start("127.0.0.1:"+PORT.ToString());
	}

	private void startSer()
	{
		m_link2.Start(PORT.ToString());
	}

	[TearDown]
	public void uninit()
	{
		m_link1.Stop(false);
		m_link1.Dispose();
		m_link1=null;
		m_link2.Stop(false);
		m_link2.Dispose();
		m_link2=null;
	}

	[Test]
	public void connect()
	{
		startSer();
		startCli();
		Assert.True(m_connEvt.WaitOne(2500));		// ..... timeout. should suffice for a localhost connection!
		Assert.True(m_conn);
	}

	[Test]
	public void sendApacket_client2server()
	{
		// connect
		connect();
		// send packet
	  byte[] expect = new byte[]{10,20,30,40,50};
		m_link1.outQueue.Add(new TstPck(expect));
		// receive and compare
	  TstPck recv = m_link2.inQueue.Get(true) as TstPck;
		Assert.AreEqual( expect , recv.body );
	}

	[Test]
	public void sendApacket_server2client()
	{
		// connect
		connect();
		// send packet
	  byte[] expect = new byte[]{50,40,30,20,10};
		m_link2.outQueue.Add(new TstPck(expect));
		// receive and compare
	  TstPck recv = m_link1.inQueue.Get(true) as TstPck;
		Assert.AreEqual( expect , recv.body );
	}

	[Test]
	public void sendBigPacket()
	{
		// connect
		connect();
		// send packet
	  byte[] expect = new byte[16000];
		for( int i=0 ; i<expect.Length ; i++ )
			expect[i] = (byte)((7*i)&255);
		m_link1.outQueue.Add(new TstPck(expect));
		// receive and compare
	  TstPck recv = m_link2.inQueue.Get(true) as TstPck;
		Assert.AreEqual( expect , recv.body );
	}

	[Test]
	public void tryReconnectClient()
	{
		// connect and send a packet
		sendApacket_client2server();
		// stop server
		m_link2.Stop(false);
		// wait for client to notify.
		Assert.True(m_connEvt.WaitOne(2500));		// ..... timeout. should suffice for a localhost connection!
		Assert.False(m_conn);
		// reopen server
		startSer();
		Assert.True(m_connEvt.WaitOne(2500));		// ..... timeout. should suffice for a localhost connection!
		Assert.True(m_conn);
	}

	[Test]
	public void sendLots()
	{
		//connect
		connect();
		// set up sender
	  System.Threading.Thread thr;
	  System.Threading.ThreadStart thrProc = delegate()
		{
			System.Threading.Thread.Sleep(25);
			for( int i=0 ; i<500 ; i++ )
				m_link2.outQueue.Add(new TstPck(new byte[]{(byte)(i&255),20,30,40,50}));
		};
		thr = new System.Threading.Thread( thrProc );
		thr.Start();
		// loop here and consume the packets.
	  byte[] expect = new byte[]{10,20,30,40,50};
		for( int i=0 ; i<500 ; i++ )
		{
		  TstPck recv = m_link1.inQueue.Get(true) as TstPck;
			expect[0] = (byte)(i&255);
			Assert.AreEqual( expect , recv.body );
		}
	}

	private TstPckFac m_packetFactory;
	private GTestCommon.Links.TcpPacketLink m_link1;
	private GTestCommon.Links.TcpPacketUplink m_link2;
	private System.Threading.EventWaitHandle m_connEvt;
	private bool m_conn;
}
}
