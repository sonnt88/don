using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
//using NUnit.Mocks;

using GTestCommon.Models;

namespace GTestCommon_Test
{
    [TestFixture]
    class Test_Tests
    {
        [TestFixtureSetUp]
        public void Init()
        {
        }

        [TestFixtureTearDown]
        public void Cleanup()
        {
        }

        [SetUp]
        public void TestInit()
        {
        }

        [TearDown]
        public void TestCleanup()
        {
        }

        [Test]
        public void SetID()
        {
            Test model = new Test(null, "Test",42);
            Assert.AreEqual(42,model.ID);
        }

        [Test]
        public void EnabledChangeRaisesEvent()
        {
            Test model = new Test(null, "Test1",1);
            Assert.IsTrue(model.Enabled);
            Assert.IsFalse(model.DisabledByDefault);
        }

        [Test]
        public void DISABLED_CausesDefaultDisabledToBeTrue()
        {
            Test model = new Test(null, "DISABLED_Test1",5);
            Assert.IsTrue(model.Enabled);
            Assert.IsTrue(model.DisabledByDefault);
        }

        [Test]
        public void CheckPassedAndFailedCount()
        {
            Test model = new Test(null, "Test1",1);
            model.AddResult(true);
            model.AddResult(true);
            model.AddResult(false);
            model.AddResult(true);

            Assert.AreEqual(3, model.PassedCount);
            Assert.AreEqual(1, model.FailedCount);
            Assert.AreEqual(4, model.IterationCount);
        }

        [Test]
        public void ResetExecutionState()
        {
            Test model = new Test(null, "Test1",1);
            model.AddResult(true);
            model.AddResult(true);
            model.AddResult(false);
            model.AddResult(true);
            model.ResetExecutionState();

            Assert.AreEqual(0, model.IterationCount);
            Assert.AreEqual(0, model.PassedCount);
            Assert.AreEqual(0, model.FailedCount);
        }
    }
}