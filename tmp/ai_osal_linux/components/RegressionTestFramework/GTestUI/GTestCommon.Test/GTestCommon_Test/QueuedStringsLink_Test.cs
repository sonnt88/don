using System.Collections.Generic;
using NUnit.Framework;




namespace GTestCommon_Test
{


[TestFixture]
class QueuedStringsLink_Test
{
	public QueuedStringsLink_Test()
	{
//		m_pipeName = "\\\\.\\pipe\\testPipe_";
//		m_pipeName += System.Diagnostics.Process.GetCurrentProcess().Id.ToString();
//		m_pipeName += "_";
//		m_pipeName += System.Environment.TickCount.ToString();
//		m_pipeName += "_";
//		m_pipeName += (new System.Random()).Next().ToString();
//		m_pipeEnd1 = System.IO.File.Create( m_pipeName );
//		m_pipeEnd2 = System.IO.File.Open( m_pipeName , System.IO.FileMode.Open );
	}

	private class QSLT : GTestCommon.Links.QueuedStringsLink
	{
		public QSLT(System.IO.Stream recv,System.IO.Stream send)
		 : base(256)
		{
			inStream = recv;
			outStream = send;
		}

	}

	[SetUp]
	public void setup()
	{
	  Shared.StreamPipe pipe = new Shared.StreamPipe();
		m_pipeEnd1 = pipe;
		m_pipeEnd2 = pipe.otherEnd;
		m_link1 = new QSLT(m_pipeEnd1,m_pipeEnd1);
		m_link2 = new QSLT(m_pipeEnd2,m_pipeEnd2);
	}

	[TearDown]
	public void uninit()
	{
		m_link1.Dispose();
		m_link2.Dispose();
		m_link1=null;
		m_link2=null;
		m_pipeEnd1.Close();
		m_pipeEnd2.Close();
		m_pipeEnd1.Dispose();
		m_pipeEnd2.Dispose();
	}

	[Test]
	public void startStop()
	{
		Assert.IsFalse( m_link1.bIsStarted() );
		m_link1.Start("dummy garbage text which is ignored. $%&\"*#\\\0");
		Assert.IsTrue( m_link1.bIsStarted() );
	}

	[Test]
	public void sendALine()
	{
	  string data = "sending this string over.";
		m_link1.Start("");
		m_link2.Start("");
		m_link1.outQueue.Add(data);
	  string recData = m_link2.inQueue.Get(true);
		Assert.AreEqual(data,recData);
	}

	[Test]
	public void sendUntilBlocking()
	{
	  int cnt,num;
		m_link1.Start("");
		// push lines into link until queue fills (that is, stream no longer buffers data immediately).
		num=0;
		while( m_link1.outQueue.isEmpty() )
		{
			for( cnt=0 ; cnt<50 ; cnt++ )
				m_link1.outQueue.Add(string.Format("fillSpace {0}",num++));
			System.Threading.Thread.Sleep(25);
		}
		Assert.Greater( num , 0 );
		// now pick it all up.
		m_link2.Start("");
		for( cnt=0 ; cnt<num ; cnt++ )
		{
		  string expect = string.Format("fillSpace {0}",cnt);
		  string have = m_link2.inQueue.Get(true);
			Assert.AreEqual( expect , have );
		}
	}

	[Test]
	public void sendToWaitingReceiver()
	{
	  string line,expect;
		expect = "test, 1-2-3, test.";
		m_link2.Start("");
		m_link1.Start("");
		m_link1.outQueue.Add(expect);
		line = m_link2.inQueue.Get(true);
		Assert.AreEqual( line , expect );
	}
/*		not planned to be able to transfer string longer than the buffersize. Expected behavior? define? ..... TODO.
	[Test]
	public void sendBigString()
	{
	  string data = "";
		for( int i=0 ; i<1000 ; i++ )
			data+="0123456789ABCDEF";
		m_link1.Start("");
		m_link2.Start("");
		m_link1.outQueue.Add(data);
	  string recData = m_link2.inQueue.Get(true);
		Assert.AreEqual(data,recData);
	}
*/
	[Test]
	public void sendBothWays()
	{
	  string expect12,expect21;
		m_link1.Start("");
		m_link2.Start("");
		expect12 = "This goes from 1 to 2.";
		expect21 = "This goes from 2 to 1.";
		m_link1.outQueue.Add(expect12);
		m_link2.outQueue.Add(expect21);
	  string recv1,recv2;
		recv1 = m_link1.inQueue.Get(true);
		recv2 = m_link2.inQueue.Get(true);
		Assert.AreEqual( expect12 , recv2 );
		Assert.AreEqual( expect21 , recv1 );
	}

	[Test]
	public void stressBothWays()
	{
	  System.Threading.EventWaitHandle go = new System.Threading.ManualResetEvent(false);
	  StressHelper thr1 = new StressHelper(m_link1.outQueue,m_link1.inQueue,go);
	  StressHelper thr2 = new StressHelper(m_link2.outQueue,m_link2.inQueue,go);
		m_link1.Start("");
		m_link2.Start("");
	  System.Threading.WaitHandle[] doneEvents = new System.Threading.WaitHandle[]{thr1.doneEvt,thr2.doneEvt};
		// launch!
		go.Set();
		// wait done
		System.Threading.WaitHandle.WaitAll(doneEvents);
	}

	private class StressHelper
	{
		public StressHelper(GTestCommon.Queue<string> send,GTestCommon.Queue<string> recv,System.Threading.EventWaitHandle goSig)
		{
			m_go = goSig;
			m_send=send;
			m_recv=recv;
			m_done = new System.Threading.ManualResetEvent(false);
			m_thr = new System.Threading.Thread(thrProc);
			m_thr.Start();
		}
		private void thrProc()
		{
		  int rc;
			m_go.WaitOne();
			rc=0;
			// push lots of lines, consume input
			for( int i=0 ; i<1024 ; i++ )
			{
			  string line = "line "+i.ToString();
				m_send.Add(line);
				if((i&3)==0)
				while((line=m_recv.Get(false))!=null)
				{
					Assert.AreEqual( "line "+rc.ToString() , line );
					rc++;
				}
			}
			// consume rest.
			while( rc<1024 )
			{
			  string line=m_recv.Get(true);
				Assert.AreEqual( "line "+rc.ToString() , line );
				rc++;
			}
			// done.
			m_done.Set();
		}

		public System.Threading.WaitHandle doneEvt{get{return m_done;}}

		private System.Threading.Thread m_thr;
		private GTestCommon.Queue<string> m_send,m_recv;
		private System.Threading.EventWaitHandle m_go;
		private System.Threading.EventWaitHandle m_done;
	}


//	private string m_pipeName;
	private System.IO.Stream m_pipeEnd1;
	private System.IO.Stream m_pipeEnd2;
	private QSLT m_link1;
	private QSLT m_link2;

}
}

