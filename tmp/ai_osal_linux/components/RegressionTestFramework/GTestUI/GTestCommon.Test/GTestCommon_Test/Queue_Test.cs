using System.Collections.Generic;
using NUnit.Framework;


namespace GTestCommon_Test
{

	[TestFixture]
	public class Queue_Test
	{
		public Queue_Test()
		{
			m_q = null;
			m_sig = null;
		}

		[SetUp]
		public void setup()
		{
			m_q = new GTestCommon.Queue<uint>();
			m_sig = new System.Threading.AutoResetEvent(false);
		}

		[Test]
		public void falseTriggerEvent()
		{
			// queue should not err when false-triggered.
			// sender 'thread'
		  System.Threading.Thread thr = new System.Threading.Thread(
			delegate()
			{
				for( int i=0 ; i<3 ; i++ )
				{
					System.Threading.Thread.Sleep(5);
					((System.Threading.EventWaitHandle)m_q.getWaitHandle()).Set();
				}
				System.Threading.Thread.Sleep(5);
				// now really send.
				m_q.Add( 1084 );
			});
			thr.Start();
			// now wait on the queue. Should not fail on false signal from thread.
		  uint value = m_q.Get(true);
			Assert.AreEqual( 1084 , value );
		}

		[Test]
		public void fill500()
		{
			// fill many elements in queue (causes internal reallocs)
			for(uint i=0;i<1000;i++)
				m_q.Add(i+10);
		}

		[Test]
		public void putAndGet()
		{
			// fill some elements in queue
			for(uint i=0;i<100;i++)
				m_q.Add(i+100);
			// read back and compare.
			for(uint i=0;i<100;i++)
			{
			  uint val = m_q.Get(false);
				Assert.AreEqual( val , i+100 );
			}
		}

		[Test]
		public void waithandle()
		{
			// test setting of wait signal
		  System.Threading.WaitHandle wh = m_q.getWaitHandle();
			// valid handle?
			Assert.IsNotNull( wh );
			// handle not yet set?
			Assert.False( wh.WaitOne( 0 ) );
			// put element
			m_q.Add(42);
			// wait handle now set? (this will reset the handle)
			Assert.That( wh.WaitOne( 0 ) );
			// now clear? (using auto-reset handle)
			Assert.False( wh.WaitOne( 0 ) );
			// put second element
			m_q.Add(4711);
			// event should not be set.
			Assert.False( wh.WaitOne( 0 ) );
		}


		[Test]
		public void blockedGet()
		{
			// put and wait test
		  uint val;
		  System.Threading.Thread thr;
		  System.Threading.ThreadStart thrProc = delegate()
			{
				System.Threading.Thread.Sleep(25);
				m_q.Add(42);
			};
			thr = new System.Threading.Thread( thrProc );
			thr.Start();
			// now wait for the element.
			val = m_q.Get(true);
			Assert.AreEqual( val , 42 );
		}

		[Test]
		public void getFromEmpty()
		{
		  uint val;
			// get an element which is not there
			val = m_q.Get(false);
			// must be default<uint>
			Assert.AreEqual( val , 0 );
		}

		[Test]
		public void multiThreadReadWrite()
		{
		  uint loop;
			for( loop=0;loop<5;loop++ )
			{
			  System.Threading.Thread thr1;
			  System.Threading.Thread thr2;
			  System.Threading.ThreadStart thrProc1 = delegate()
				{
					for(uint i=0;i<10000;i++)
						m_q.Add(i+10001);
				};
			  System.Threading.ThreadStart thrProc2 = delegate()
				{
				  uint val;
					for(uint i=0;i<20000;i++)
					{
						val = m_q.Get(true);
						Assert.AreEqual( val , i+1 );
					}
					m_sig.Set();
				};

				// make threads
				thr1 = new System.Threading.Thread( thrProc1 );
				thr2 = new System.Threading.Thread( thrProc2 );
				// prefill queue
					for(uint i=0;i<10000;i++)
						m_q.Add(i+1);
				// run both threads, reader first. Reader is slower and has catching up to do.
				thr2.Start();thr1.Start();
				m_sig.WaitOne();
			}
		}

		private GTestCommon.Queue<uint> m_q;
		private System.Threading.EventWaitHandle m_sig;

	}
}
