using System;
using System.Collections.Generic;
using System.Text;

using NUnit.Framework;
//using NUnit.Mocks;

using GTestCommon.Models;

namespace GTestCommon_Test
{
    [TestFixture]
    class TestGroupModel_Tests
    {
        [TestFixtureSetUp]
        public void Init()
        {
        }

        [TestFixtureTearDown]
        public void Cleanup()
        {
        }

        [SetUp]
        public void TestInit()
        {
        }

        [TearDown]
        public void TestCleanup()
        {
        }

        [Test]
        public void CheckTestCount()
        {
            TestGroupModel model = new TestGroupModel(null, "TestGroup1");

            model.AppendNewTest("Test1",1);
            model.AppendNewTest("DISABLED_Test2",2);
            model.AppendNewTest("Test3",3);

            Assert.AreEqual(3, model.TestCount);
        }

        [Test]
        public void DISABLED_CausesDefaultDisabledToBeTrue()
        {
            TestGroupModel model = new TestGroupModel(null, "DISABLED_Test1Group");
            Assert.IsTrue(model.DisabledByDefault);
        }

        [Test]
        public void AddTest_sends_ID_up()
        {
            MockOverloaded_ATC suite = new MockOverloaded_ATC(false);
            TestGroupModel tCase = new TestGroupModel(suite,"testGroup1");
            suite.signalledID = 0;
            tCase.AppendNewTest( "Test" , 42 );
            Assert.AreEqual( 42 , suite.signalledID );
        }

    }

    internal class MockOverloaded_ATC : AllTestCases
    {
        public MockOverloaded_ATC(bool runDISABLED_Tests)
         : base(runDISABLED_Tests)
        {}

        public override void AddNewTestToIdMap(Test tst)
        {
            base.AddNewTestToIdMap(tst);
            signalledID = tst.ID;
        }

        public uint signalledID;
    }
}