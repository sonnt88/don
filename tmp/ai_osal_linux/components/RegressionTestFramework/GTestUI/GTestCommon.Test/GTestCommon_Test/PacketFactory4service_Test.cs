using System.Collections.Generic;
using NUnit.Framework;
using GTestCommon.Links;


namespace GTestCommon_Test
{


[TestFixture]
class PacketFactory4service_Test
{
	public PacketFactory4service_Test()
	{
		m_factory = new PacketFactory4service();
		m_encBuffer = new byte[65536];
	}

	[Test]
	public void convert_PacketGetVersion()
	{
	  PacketGetVersion pack1,pack2;
		pack1 = new PacketGetVersion();
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0u , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketGetVersion;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
	}

	[Test]
	public void convert_PacketGetState()
	{
	  PacketGetState pack1,pack2;
		pack1 = new PacketGetState();
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketGetState;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
	}

	[Test]
	public void convert_PacketGetTestList()
	{
	  PacketGetTestList pack1,pack2;
		pack1 = new PacketGetTestList();
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketGetTestList;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
	}

	[Test]
	public void convert_PacketRunTests()
	{
	  PacketRunTests pack1,pack2;
	  List<uint> id1,id2;
	  bool inv1;
	  uint checkSum1;
		id1 = new List<uint>(new uint[]{10,20,40,100});
		inv1 = true;checkSum1 = 0x07C007ECu;
		pack1 = new PacketRunTests(id1,inv1,checkSum1);
		pack1.randomSeed = 4711;
		pack1.numRepeats = 3;
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketRunTests;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
		id2 = new List<uint>(pack2.tests);
		Assert.AreEqual( id1 , id2 );
		Assert.AreEqual( inv1 , pack2.invertSelection );
		Assert.AreEqual( checkSum1 , pack2.testSetChecksum );
		Assert.AreEqual( pack1.randomSeed , pack2.randomSeed );
		Assert.AreEqual( pack1.numRepeats , pack2.numRepeats );
	}

	[Test]
	public void convert_PacketAbortTestsReq()
	{
	  PacketAbortTestsReq pack1,pack2;
		pack1 = new PacketAbortTestsReq();
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketAbortTestsReq;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
	}

	[Test]
	public void convert_PacketState()
	{
	  PacketState pack1,pack2;
	  byte state = 42;
		pack1 = new PacketState(state);
		pack1.serial = 42;
		pack1.iteration = 2;
		pack1.testID = 110;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketState;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
		Assert.AreEqual( state , pack2.state );
		Assert.AreEqual( pack1.iteration , pack2.iteration );
		Assert.AreEqual( pack1.testID , pack2.testID );
	}

	[Test]
	public void convert_PacketVersion()
	{
	  PacketVersion pack1,pack2;
	  string version1 = "DummyVersion v1.0";
		pack1 = new PacketVersion(version1);
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketVersion;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
		Assert.AreEqual( version1 , pack2.version );
	}

	[Test]
	public void convert_PacketTestList()
	{
	  PacketTestList pack1,pack2;
	  GTestCommon.Models.AllTestCases model1 = new GTestCommon.Models.AllTestCases(false);
	  GTestCommon.Models.TestGroupModel grp11 = model1.AppendNewTestGroup( "group1" );
	  GTestCommon.Models.TestGroupModel grp12 = model1.AppendNewTestGroup( "group2" );
		grp11.AppendNewTest("test___",1);
		grp12.AppendNewTest("test1",12);
		grp12.AppendNewTest("test2",9);
		pack1 = new PacketTestList();
		pack1.serial = 42;
		pack1.data = model1;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketTestList;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
		// compare testrunmodel (some numbers and names)
	  GTestCommon.Models.AllTestCases model2 = pack2.data;
		Assert.False( object.ReferenceEquals( model1 , model2 ) );
		Assert.AreEqual( model1.TestGroupCount , model2.TestGroupCount );
		Assert.AreEqual( model1.TestCount , model2.TestCount );
		Assert.AreEqual( model1[0].TestCount , model2[0].TestCount );
		Assert.AreEqual( model1[1].TestCount , model2[1].TestCount );
		Assert.AreEqual( "group1" , model2[0].Name );
		Assert.AreEqual( "group2" , model2[1].Name );
		Assert.AreEqual( "test___" , model2[0][0].Name );
		Assert.AreEqual( "test1" , model2[1][0].Name );
		Assert.AreEqual( "test2" , model2[1][1].Name );
	}

	[Test]
	public void convert_PacketAckTestRun()
	{
	  PacketAckTestRun pack1,pack2;
	  bool flag1 = true;
		pack1 = new PacketAckTestRun(flag1);
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketAckTestRun;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
		Assert.AreEqual( flag1 , pack2.successfulTestStart );
	}

	[Test]
	public void convert_PacketAbortAck()
	{
	  PacketAbortAck pack1,pack2;
		pack1 = new PacketAbortAck();
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketAbortAck;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
	}

	[Test]
	public void convert_PacketSendAllResultsDone()
	{
	  PacketSendAllResultsDone pack1,pack2;
		pack1 = new PacketSendAllResultsDone();
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketSendAllResultsDone;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
	}

	[Test]
	public void convert_PacketTestResult()
	{
	  PacketTestResult pack1,pack2;
	  GTestCommon.Models.TestResultModel mdl1,mdl2;
	  System.DateTime timeStamp = System.DateTime.Today;
	  timeStamp.AddHours(1.333);
		pack1 = new PacketTestResult();
		mdl1 = new GTestCommon.Models.TestResultModel( true , timeStamp , 2500 );
		pack1.serial = 42;
		pack1.result = mdl1;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketTestResult;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
		mdl2 = pack2.result;
		Assert.AreEqual( mdl1.success , mdl2.success );
		Assert.AreEqual( mdl1.duration , mdl2.duration );
/*		
	  long ticksDiff = mdl1.time.Ticks - mdl2.time.Ticks ;		// ticks are ns since 1.Jan 1970. Difference of <1e9 shall be ok.
		if(ticksDiff<0)ticksDiff=-ticksDiff;
		Assert.LessOrEqual( ticksDiff , 1000000000l );		// this is broken. shall the protobuf thing transport this? which precision?
 */
	}

	[Test]
	public void convert_PacketReqPower()
	{
	  PacketReqPower pack1,pack2;
		pack1 = new PacketReqPower(167,333);
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketReqPower;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
		Assert.AreEqual( pack1.maxTime , pack2.maxTime );
		Assert.AreEqual( pack1.maxTime , pack2.maxTime );
	}

	[Test]
	public void convert_PacketAllTestrunsDone()
	{
	  PacketAllTestrunsDone pack1,pack2;
		pack1 = new PacketAllTestrunsDone();
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as PacketAllTestrunsDone;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
	}

	[Test]
	public void convert_PacketUpTestOutput()
	{
		PacketUpTestOutput pack1, pack2;
		pack1 = new PacketUpTestOutput();
		pack1.output = "Blubber";
		pack1.timeStamp = 1000;
		pack1.source = PacketUpTestOutput.OutputSource.STDERR;
		uint bytes = m_factory.encodePacket(pack1, m_encBuffer, 0, (uint)m_encBuffer.Length);
		uint skipSize;
		pack2 = m_factory.decodePacket(m_encBuffer, 0, bytes, out skipSize) as PacketUpTestOutput;
		Assert.AreEqual(bytes, skipSize);
		Assert.NotNull(pack2);
		Assert.AreEqual(pack1.output, pack2.output);
		Assert.AreEqual(pack1.timeStamp, pack2.timeStamp);
		Assert.AreEqual(pack1.source, pack2.source);
	}



/*

	[Test]
	public void convert_()
	{
	   pack1,pack2;
		pack1 = new ();
		pack1.serial = 42;
	  uint bytes = m_factory.encodePacket( pack1 , m_encBuffer , 0 , (uint)m_encBuffer.Length );
	  uint skipSize;
		pack2 = m_factory.decodePacket( m_encBuffer , 0 , bytes , out skipSize ) as ;
		Assert.AreEqual( bytes , skipSize );
		Assert.NotNull( pack2 );
		Assert.AreEqual( pack1.ID , pack2.ID );
		Assert.AreEqual( pack1.serial , pack2.serial );
		Assert.AreEqual( pack1.GetType() , pack2.GetType() );
	}

 */


	private GTestCommon.IPacketConverter m_factory;
	private byte[] m_encBuffer;
}
}
