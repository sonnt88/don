import socket
import sys
import time
import thread
import struct


def main(args):
	ls = socket.socket()
	ls.bind(("0.0.0.0",666))
	ls.listen(3)

	while True:
		s = ls.accept()
		thread.start_new_thread(procIncoming,(s,))


class packet(object):
	__slots__ = ('ID','flags','ser','reqSer','datasize','data')
	def __init__(self,ID,flags,ser,reqSer,data):
		self.ID=ID
		self.flags=flags
		self.ser=ser
		self.reqSer=reqSer
		self.datasize=len(data)
		self.data=data


def procIncoming(args):
	sock = args[0]
	# sock.setsockopt(socket.SOL_SOCKET,socket.SO_LINGER,NOLINGER)
	print "new connect"
	try:
		data = ""
		while True:
			d = sock.recv(1024)
			if len(d)<1:break
			data = data+d
			if len(data)<16:
				continue
			vals = struct.unpack_from("BBxxIII",data)
			plen = vals[4]
			if len(data)<16+plen:
				continue;
			# now have packet.
			pck = packet(vals[0],vals[1],vals[2],vals[3],data[16:16+plen])
			print "  payloadSize=%d   data=%s" % (plen,repr(data[:16+plen]))
			data = data[16+plen:]

			procPack(pck,sock)
			time.sleep(0.25)
		sock.shutdown(socket.SHUT_RDWR)
	except socket.error,ex:
		pass
	print "lost connection"
	sock.close()

rSer=0;

def procPack(pck,sock):
	global rSer
	if pck.ID==0xA9:
		if len(pck.data)<2: return
		a = ord(pck.data[0])
		b = ord(pck.data[1])
		c = a+b
		print "%d + %d = %d" % (a,b,c)
		outData = struct.pack("BBBBIIIB",0xAA,0,0,0,rSer,pck.ser,1,c)
		sock.send(outData)
		rSer += 1
	elif pck.ID==0x00:
		ver = "testpeer 0.1"
		print "version: "+ repr(ver)
		outData = struct.pack("BBBBIII%ds"%len(ver),0x14,0,0,0,rSer,pck.ser,len(ver),ver)
		sock.send(outData)
		rSer += 1
	elif pck.ID==0x01:
		stt = 0
		print "state: "+ repr(stt)
		outData = struct.pack("BBBBIIIB",0x81,0,0,0,rSer,pck.ser,1,stt)
		sock.send(outData)
		rSer += 1



if __name__=="__main__":
	sys.exit(main(sys.argv[1:]))

