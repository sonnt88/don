using System.Collections.Generic;
using NUnit.Framework;
using GTestAdapter;
using GTestCommon;




namespace GTestAdapter_Test
{

[TestFixture]
public class DatabaseWriter_Test
{
	public DatabaseWriter_Test()
	{
	}

	[SetUp]
	public void setup()
	{
		m_db = new MockDatabase();
		m_writer = new GTestAdapter.DataLogging.DatabaseWriter( "." , new MockDbFactory(m_db) );
		m_testSet = createTestSet();
	}

	[TearDown]
	public void uninit()
	{
		m_testSet = null;
		m_writer.Close();
		m_writer.Dispose();
		Assert.AreEqual(m_db.State,System.Data.ConnectionState.Closed);
	}

	[Test]
	public void _setup()
	{
		// only running the setup/teardown. Just see if the database has tables now.
		Assert.Greater( m_db.Count , 0u );
	}

	[Test]
	public void addTestSet()
	{
		// keep counts
	  uint count1 = m_db.m_count_create;
	  uint count2 = m_db.m_count_insert;

		// store testSet in database
		m_writer.storeTestSet( m_testSet );

		// should have been 10 insert commands and no creates.
		Assert.AreEqual( count1 , m_db.m_count_create );
		Assert.AreEqual( count2+10 , m_db.m_count_insert );

		// try to find a test.
	  MockDatabase.Table tab_testCase = m_db.getTable("testCase");
	  MockDatabase.Table tab_test = m_db.getTable("test");
	}

	[Test]
	public void addTestSset_twice()
	{
		// first run other test.
		addTestSet();

	  uint count1 = m_db.m_count_create;
	  uint count2 = m_db.m_count_insert;

		// store to database again
		m_writer.storeTestSet( m_testSet );

		// should not have inserted or created anything.
		Assert.AreEqual( count1 , m_db.m_count_create );
		Assert.AreEqual( count2 , m_db.m_count_insert );
	}

	private GTestCommon.Models.AllTestCases createTestSet()
	{
	  GTestCommon.Models.AllTestCases testSet;
	  GTestCommon.Models.TestGroupModel grp1,grp2,grp3;
		testSet = new GTestCommon.Models.AllTestCases(false);
		grp1 = testSet.AppendNewTestGroup( "group1" );
		grp2 = testSet.AppendNewTestGroup( "group2" );
		grp3 = testSet.AppendNewTestGroup( "group3" );
		grp1.AppendNewTest("test1a",1);
		grp1.AppendNewTest("test1b",2);
		grp1.AppendNewTest("test1c",3);
		grp2.AppendNewTest("test2a",4);
		grp2.AppendNewTest("test2b",5);
		grp3.AppendNewTest("test3a",6);
		testSet.CheckSum = 0x07C007ECu;
		return testSet;
	}

	GTestAdapter.DataLogging.DatabaseWriter m_writer;
	MockDatabase m_db;
	GTestCommon.Models.AllTestCases m_testSet;


	private class MockDbFactory : GTestAdapter.DataLogging.IDbOpener
	{
		public MockDbFactory(System.Data.IDbConnection db){m_db=db;}
		public System.Data.IDbConnection openDB(){m_db.Open();return m_db;}
		public string getDBpath_for_errorMessage(){return "mock-db";}
		public void closeDB(System.Data.IDbConnection dbHandle){Assert.AreSame(dbHandle,m_db);m_db.Close();}
		System.Data.IDbConnection m_db;
	}

}


}
