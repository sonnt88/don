using System.Collections.Generic;
using System.Data;
using NUnit.Framework;
using System.Text.RegularExpressions;


namespace GTestAdapter_Test
{

internal class MockDatabaseCmd : IDbCommand
{
	public MockDatabaseCmd(MockDatabase parent)
	{
		m_db = parent;
	}

	public void Dispose()
	{
	}

	public string CommandText{get{return m_command;}set{m_command=value;}}

	public int CommandTimeout {get{return 1;}set{Assert.AreEqual(value,1);}}

	public CommandType CommandType{get{return CommandType.Text;}set{Assert.AreEqual(CommandType.Text,value);}}

	public IDbConnection Connection {get{return m_db;}set{Assert.Fail();}}

	public IDataParameterCollection Parameters{get{Assert.Fail();return null;}}

	public IDbTransaction Transaction{get{Assert.Fail();return null;}set{Assert.Fail();}}

	public UpdateRowSource UpdatedRowSource{get{Assert.Fail();return UpdateRowSource.None;}set{Assert.Fail();}}

	public void Cancel()
	{
		// mock does support cancelling.
		Assert.Fail();
	}

	public IDbDataParameter CreateParameter()
	{
		// mock does not support parameters.
		Assert.Fail();
		return null;
	}

	public int ExecuteNonQuery()
	{
		ExecuteReader();
		return 0;//............
	}

	public IDataReader ExecuteReader()
	{
	  MockDatabaseReader res;
	  MockDatabase.Table results;

		results = parseCommand(m_command);
		res = new MockDatabaseReader(m_db,results);

		//............
		return res;
	}

	public IDataReader ExecuteReader(CommandBehavior behavior)
	{
		// mock does not support behavior.
		Assert.Fail();
		return null;
	}

	public object ExecuteScalar()
	{
		// not supported by mock.
		Assert.Fail();
		return null;
	}

	public void Prepare()
	{
		return;
	}

	private MockDatabase.Table parseCommand(string cmd)
	{
	  Match m;
	  MockDatabase.Table res = null;
		cmd = cmd.Replace("\r","").Replace("\n","");
		m = sm_re_select_where.Match(cmd);
		if( m.Success )
		{
			// is a SELECT with WHERE
			res = parseSelectCommand(m.Groups[1].Value,m.Groups[2].Value,m.Groups[3].Value);
			m_db.m_count_select++;
			return res;
		}
		m = sm_re_select.Match(cmd);
		if( m.Success )
		{
			// is a SELECT
			res = parseSelectCommand(m.Groups[1].Value,m.Groups[2].Value,"");
			m_db.m_count_select++;
			return res;
		}
		m = sm_re_insert.Match(cmd);
		if( m.Success )
		{
			// is an INSERT
			parseInsertCommand(m.Groups[1].Value,m.Groups[2].Value,m.Groups[3].Value);
			m_db.m_count_insert++;
			return null;
		}
		m = sm_re_create_table.Match(cmd);
		if( m.Success )
		{
			// is a CREATE
		  MockDatabase.Table table;
			table = parseCreateTableCommand(m.Groups[1].Value,m.Groups[2].Value);
			m_db.addTable(table);
			m_db.m_count_create++;
			return null;
		}
		Assert.Fail();
		throw new MockDatabaseError("mock db cannot recognize command.");
	}

	private MockDatabase.Table parseSelectCommand(string selects,string tableNames,string whereClause)
	{
	  string[] selectParts = null;
	  string func=null;
		selectParts = selects.Split(',');
		if( tableNames.IndexOf(',')>=0 )
			throw new MockDatabaseError("mock does not support joined selects.");
		setTable( tableNames.Trim() );
		// find columns
	  List<int> resultCols = new List<int>();
	  MockDatabase.Table res = new MockDatabase.Table();
		foreach( string sel in selectParts )
		{
		  string s = sel.Trim();
			if( s.IndexOf(' ')>=0 || s.IndexOf('\t')>=0 )
				throw new MockDatabaseError("only support simple select table names.");
			// check for function()
		  Match m = sm_re_func.Match(s);
			if( m.Success )
			{
				// is a function like max(ID)
				if(func!=null)
					throw new MockDatabaseError("can only have one function in select.");
				func = m.Groups[1].Value.ToLower();
				s = m.Groups[2].Value;
			}
			// find column
		  int c = m_table.getColIndex(s);
			if( c<0 )
				throw new MockDatabaseError("cannot find column named "+s);
		  MockDatabase.Column col = m_table.cols[c];
			resultCols.Add(c);
			res.cols.Add(col);
		}
	  List<int> whereCols = new List<int>();
	  List<object> whereItems = new List<object>();
	  WhereClause wc = parseWhereClause(whereClause);

		// now loop rows and get results.
		foreach( MockDatabase.Row rw in m_table.rows )
		{
			// check where
			if(!wc.eval(m_table,rw))
				continue;		// where-clause did not match.
			// now take data to results
		  MockDatabase.Row newRow = new MockDatabase.Row();
			foreach( int c in resultCols )
			{
				newRow.items.Add( rw.items[c] );
			}
			res.rows.Add(newRow);
		}

		// process function
		if( func!=null )
		{
			if( resultCols.Count!=1 )
				throw new MockDatabaseError("Mock db can only use function with one output column.");
		  object val;
			if( res.cols[0].type==typeof(int) || func=="max" )
			{
			  int mx = -2147483648;
				val = null;
				foreach( MockDatabase.Row rw in res.rows )
					if( (int)rw.items[0] > mx )
						{mx = (int)rw.items[0];val=mx;}
			}else
				throw new MockDatabaseError("Mock db only supports max(int) function.");
			res.rows.Clear();
			res.rows.Add(new MockDatabase.Row());
			res.rows[0].items.Add(val);
		}

		return res;
	}

	private void parseInsertCommand(string tableName,string colNames,string values)
	{
	  string[] ca = colNames.Split(',');
	  string[] va = values.Split(',');		// ...... TODO: does not consider ',' inside a quote.
	  List<int> li = new List<int>();
		setTable( tableName.Trim() );
		if( ca.Length!=va.Length )
			throw new MockDatabaseError("number of values does not match columns specified.");
		// find columns.
		for( int i=0 ; i<ca.Length ; i++ )
		{
		  int c = m_table.getColIndex(ca[i].Trim());
			if(c<0)
				throw new MockDatabaseError("Cannot find column "+ca[i]+" for insert");
			li.Add(c);
		}
		// check not-nulls
		for( int c=0 ; c<m_table.cols.Count ; c++ )
		{
			if( m_table.cols[c].key && (!li.Contains(c)) )
				throw new MockDatabaseError("must have value for key column.");
			if( m_table.cols[c].notNull && (!li.Contains(c)) )
				throw new MockDatabaseError("must have value for non-null column.");
		}
		// convert values
	  MockDatabase.Row rw = new MockDatabase.Row();
		for( int c=0 ; c<m_table.cols.Count ; c++ )
			rw.items.Add(null);
		for( int i=0 ; i<ca.Length ; i++ )
		{
		  int c = li[i];		// col index
		  MockDatabase.Column col = m_table.cols[c];
			if( col.type==typeof(short) )
				rw.items[c] = (short)parseInt(va[i].Trim());
			if( col.type==typeof(int) )
				rw.items[c] = (int)parseInt(va[i].Trim());
			else if( col.type==typeof(long) )
				rw.items[c] = (long)parseInt(va[i].Trim());
			else if( col.type==typeof(float) )
				rw.items[c] = (float)parseFloat(va[i].Trim());
			else if( col.type==typeof(double) )
				rw.items[c] = (double)parseFloat(va[i].Trim());
			else if( col.type==typeof(string) )
				rw.items[c] = parseString(va[i].Trim());
			else
				throw new MockDatabaseError("Unsupported type in mock insert");
		}
		// add
		m_table.rows.Add(rw);
	}

	private MockDatabase.Table parseCreateTableCommand(string tableName,string lines)
	{
		tableName=tableName.Trim();
		if( m_db.getTable(tableName) != null )
			throw new MockDatabaseError("Table "+tableName+" already exists.");
	  MockDatabase.Table res = new MockDatabase.Table();
		res.name = tableName;
		foreach(string line in lines.Split(','))
		{
		  string[] words = splitWords(line);
			// each line has  name  type  options
			if( words.Length<2 )
				throw new MockDatabaseError("Error in create-table. Line needs at least name and type.");
		  MockDatabase.Column col = new MockDatabase.Column();
			col.name = words[0];
			switch (words[1].ToLower())
			{
			case "int": col.type = typeof(int);break;
			case "bigint": col.type = typeof(long);break;
			case "tinyint": col.type = typeof(sbyte);break;
			case "varchar(255)": col.type = typeof(string);break;
			case "tinytext":case "text":case "longtext": col.type = typeof(string);break;
			case "datetime":case "date": col.type = typeof(System.DateTime);break;
			default:
				Assert.Fail("unkonwn type "+words[1]);
				break;
			}
		  bool lastNot;
		  bool lastPrimary;
			lastNot=lastPrimary=false;
			for( int i=2 ; i<words.Length ; i++ )
			{
				switch(words[i].ToLower())
				{
				case "not": lastNot=true; continue;
				case "primary": lastNot=true; continue;
				case "key": col.key = true; break;
				case "null": col.notNull = lastNot; break;
				}
				lastNot=lastPrimary=false;
			}
			res.cols.Add(col);
		}
		return res;
	}

	private WhereClause parseWhereClause(string whereClause)
	{
	  int i;
		whereClause = whereClause.Trim();
		if( whereClause==null || whereClause=="" )
			return new WhereClause_Dummy();
		i=whereClause.IndexOf(" OR ");if(i<0)i=whereClause.IndexOf(" or ");
		if(i>=0)
		{
		  WhereClause_Op res = new WhereClause_Op();
			res.isAnd=false;
			res.a = parseWhereClause(whereClause.Substring(0,i));
			res.b = parseWhereClause(whereClause.Substring(i+4));
			return res;
		}
		i=whereClause.IndexOf(" AND ");if(i<0)i=whereClause.IndexOf(" and ");
		if(i>=0)
		{
		  WhereClause_Op res = new WhereClause_Op();
			res.isAnd=true;
			res.a = parseWhereClause(whereClause.Substring(0,i));
			res.b = parseWhereClause(whereClause.Substring(i+5));
			return res;
		}
	  WhereClause_Field resf = new WhereClause_Field();
	  string[] wp2 = whereClause.Split('=');
		if(wp2.Length!=2)throw new MockDatabaseError("Mock only understands simple '=' where clauses.");
	  int c = m_table.getColIndex(wp2[0].Trim());
		if( c<0 )
			throw new MockDatabaseError("cannot find column named "+wp2[0]);
	  MockDatabase.Column col = m_table.cols[c];
	  object v;
		if( col.type==typeof(int) || col.type==typeof(long) )
			v = parseInt(wp2[1]);
		else if( col.type==typeof(string) )
			v = parseString(wp2[1]);
		else if( col.type==typeof(float) )
			v = parseFloat(wp2[1]);
		else
			throw new MockDatabaseError("mock does not support all datatypes for where-clauses.");
		resf.c = c;
		resf.val = v;
		return resf;
	}

	private void setTable(string tableName)
	{
	  MockDatabase.Table tab = m_db.getTable(tableName);
		if(tab==null)
			throw new MockDatabaseError("unknown table "+tableName);
		m_table = tab;
	}

	private static string[] splitWords(string line)
	{
	  List<string> words = new List<string>();
	  string lin = line.Replace("\t"," ").Replace("\r"," ").Replace("\n"," ");
	  int idx=0;
		idx=0;
		while( idx<line.Length && line[idx]==' ' )idx++;
		while(idx<line.Length)
		{
		  int i = line.IndexOf(' ',idx);
			if(i<0)i=line.Length;
			if( i>idx )
				words.Add(line.Substring( idx , i-idx ));
			idx=i+1;
		}
		return words.ToArray();
	}

	private static long parseInt(string s)
	{
		s = s.Trim();
		return System.Convert.ToInt64(s);
	}

	private static double parseFloat(string s)
	{
		s = s.Trim();
		return System.Convert.ToDouble(s);
	}

	private static string parseString(string s)
	{
	  bool sQuot=false;
		s = s.Trim();
		if( s.Length>=2 && s.StartsWith("'") && s.EndsWith("'") )
			{sQuot=true;}
		else if( s.Length>=2 && s.StartsWith("\"") && s.EndsWith("\"") )
			{;}
		else
			throw new MockDatabaseError("Bad string format.");
		if( s.IndexOf('\\') >= 0 )
			throw new MockDatabaseError("mock does not support string with escapes.");
		s = s.Substring(1,s.Length-2);
		if( ( sQuot && s.IndexOf('\'')>=0 ) || ( (!sQuot) && s.IndexOf('\"')>=0 ) )
			throw new MockDatabaseError("bad string has quote inside.");
		return s;
	}

	abstract class WhereClause
	{
		public abstract bool eval(MockDatabase.Table tab,MockDatabase.Row rw);
	};
	class WhereClause_Dummy : WhereClause
	{
		public override bool eval(MockDatabase.Table tab,MockDatabase.Row rw){return true;}
	}
	class WhereClause_Field : WhereClause
	{
		public override bool eval(MockDatabase.Table tab,MockDatabase.Row rw)
		{
			if( sm_ints.Contains(val.GetType()) )
				return System.Convert.ToInt64(val)==System.Convert.ToInt64(rw.items[c]);
			if( sm_uints.Contains(val.GetType()) )
				return System.Convert.ToUInt64(val)==System.Convert.ToUInt64(rw.items[c]);
			if( sm_floats.Contains(val.GetType()) )
				return System.Convert.ToDouble(val)==System.Convert.ToDouble(rw.items[c]);
			if( val.GetType()==typeof(string) )
				return System.Convert.ToString(val)==System.Convert.ToString(rw.items[c]);
			throw new MockDatabaseError("Error comparing items.");
		}
		private List<System.Type> sm_ints = new List<System.Type>(new System.Type[]{typeof(sbyte),typeof(short),typeof(int),typeof(long)});
		private List<System.Type> sm_uints = new List<System.Type>(new System.Type[]{typeof(byte),typeof(ushort),typeof(uint),typeof(ulong)});
		private List<System.Type> sm_floats = new List<System.Type>(new System.Type[]{typeof(float),typeof(double)});
		public int c;
		public object val;
	}
	class WhereClause_Op : WhereClause
	{
		public override bool eval(MockDatabase.Table tab,MockDatabase.Row rw){if(isAnd){return a.eval(tab,rw)&&b.eval(tab,rw);}return a.eval(tab,rw)||b.eval(tab,rw);}
		public WhereClause a,b;
		public bool isAnd;
	}

	static Regex sm_re_select_where = new Regex( "select (.+) from (.+)\\s*where (.+)\\s*;" , RegexOptions.IgnoreCase|RegexOptions.Singleline );
	static Regex sm_re_select = new Regex( "select (.+) from (.+)\\s*;" , RegexOptions.IgnoreCase|RegexOptions.Singleline );
	static Regex sm_re_insert = new Regex( "insert\\s+into\\s*(\\S+)\\s*\\((.+)\\)\\s*values\\s*\\((.+)\\)\\s*;" , RegexOptions.IgnoreCase|RegexOptions.Singleline );
	static Regex sm_re_create_table = new Regex( "create\\s+table\\s+(\\S+)\\s*\\((.+)\\)\\s*;" , RegexOptions.IgnoreCase|RegexOptions.Singleline );
	static Regex sm_re_func = new Regex( "(\\S+)\\s*\\(\\s*(\\S*)\\s*\\)" , RegexOptions.Singleline );


	private string m_command;
	private MockDatabase m_db;
	private MockDatabase.Table m_table;
}

}
