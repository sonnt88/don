using GTestAdapter;
using GTestCommon;
using NUnit.Framework;




namespace GTestAdapter_Test
{

public class MockPacketLink<T> : GTestCommon.Links.IQueuedPacketLink<T>
{
	public MockPacketLink()
	{
		m_sendQ = new Queue<T>();
		m_recvQ = new Queue<T>();
		m_started=m_connected=false;
		m_sendSer = 0x100000;
	}

	public void Dispose()
	{
	}

	public Queue<T> inQueue{get{return m_recvQ;}}

	public Queue<T> outQueue{get{return m_sendQ;}}

	public bool Start(string connectTarget)
	{
		m_started=true;
		return true;
	}

	public void Stop(bool sendAll)
	{
		mock_disconnect();
		m_started=false;
	}

	public bool bIsStarted()
	{
		return m_started;
	}

	public bool bIsConnected()
	{
		return m_connected;
	}

	public event GTestCommon.Links.ConnectEventHandler connect;

	public T mock_pickup_sent_packet(bool bBlock)
		{return mock_pickup_sent_packet(bBlock?-1:0);}

	public T mock_pickup_sent_packet(int timeOut)
	{
	  T pck = m_sendQ.Get(timeOut);
		if( pck!=null && (pck is GTestCommon.Links.Packet) )
			(pck as GTestCommon.Links.Packet).serial = m_sendSer++;
		return pck;
	}

	public void mock_place_packet_to_recv(T pck)
	{
		m_recvQ.Add(pck);
	}

	public void mock_connect()
	{
		if( m_connected )return;
		Assert.True( m_started );
		m_connected=true;
		if( connect!=null )
			connect(true);
	}

	public void mock_disconnect()
	{
		if(!m_connected)return;
		m_connected=false;
		if( connect!=null )
			connect(false);
	}

	public T expect_packet( System.Type pckType )
		{System.Collections.Generic.List<System.Type> l = new System.Collections.Generic.List<System.Type>();l.Add(pckType);return expect_packet(l);}

	public T expect_packet( System.Type[] pckTypes )
		{return expect_packet(new System.Collections.Generic.List<System.Type>(pckTypes));}

	public T expect_packet( System.Collections.Generic.List<System.Type> pckTypes )
	{
	  T rpck = default(T);
		rpck = mock_pickup_sent_packet(2500);
		// have something? (not timeout?)
		Assert.NotNull( rpck );
		// the right type of packet?
		Assert.Contains( rpck.GetType() , pckTypes );
		return rpck;
	}

	private Queue<T> m_sendQ;
	private Queue<T> m_recvQ;
	private bool m_started,m_connected;
	private uint m_sendSer;
}

}
