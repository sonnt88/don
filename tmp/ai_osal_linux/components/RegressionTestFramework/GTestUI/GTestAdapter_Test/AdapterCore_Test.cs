using System.Collections.Generic;
using NUnit.Framework;
using GTestAdapter;
using GTestCommon;
using GTestCommon.Links;
using GTestCommon.Models;


namespace GTestAdapter_Test
{

	[TestFixture]
	public class AdapterCore_Test
	{
		public AdapterCore_Test()
		{
			m_adapter=null;
			m_worker=null;
		}

		[SetUp]
		public void setup()
		{
			// build adapter
			m_adapter = new GTestAdapter.AdapterCore(true);

			// build service mock
			m_srv = new MockService();

			// build link mocks.
			m_mock_UILink = new MockPacketLink<IPacket>();
			m_mock_serialLink = new MockPacketLink<string>();
			m_mock_localConsoleLink = new MockPacketLink<string>();

			// stick it all together.
			m_adapter.serviceLink = m_srv.link;
			m_adapter.UIlink = m_mock_UILink;
			m_adapter.consoleLink = m_mock_serialLink;
			m_adapter.localConsoleLink = m_mock_localConsoleLink;

		}

		[TearDown]
		public void uninit()
		{
			worker_stop();
			m_adapter.Dispose();
			m_adapter = null;
		}

		private void worker_start()
		{
			Assert.IsNull( m_worker );
			Assert.NotNull( m_adapter );
			m_workerDoneSig = new System.Threading.ManualResetEvent(false);
			m_worker = new System.Threading.Thread(
			delegate()
			{
				m_adapter.Run();
				m_workerDoneSig.Set();
			});
			// launch.
			m_worker.Start();
			// wait for a line with 'started' on mock local.
			while(true)
			{
			  string line = m_mock_localConsoleLink.mock_pickup_sent_packet(250000);
				Assert.NotNull( line );		// timed out? Not good.
				if( line.ToLower().IndexOf("started") >= 0 )
					break;
			}
		}

		private void worker_stop()
		{
			if(m_worker!=null)
			{
				// 'type' the workd "quit" into mocklocal to end the adapter.
				m_mock_localConsoleLink.mock_place_packet_to_recv("quit");
				m_workerDoneSig.WaitOne();
				m_workerDoneSig = null;
				m_worker = null;
			}
		}

		[Test]
		public void _setup()
		{
			// only running the setup/teardown. No body here.
		}

		[Test]
		public void _startStop()
		{
			// Start the worker and stop it again.
			worker_start();
			// stop in teardown.
		}

		[Test]
		public void startFail()
		{
			// the UI link will fail to start. Adapter should return error, not throw.
			m_adapter.UIlink = new MockBrokenLink<IPacket>();
			// now go into runloop. should exit with an error message
			m_adapter.Run();
			// check.
			Assert.NotNull( m_adapter.errorMessage );
			Assert.AreNotEqual( m_adapter.errorMessage , "" );
		}

		/// <summary>
		/// Test sequence:
		///  - adapter starts
		///  - service appears and adapter auto-connects
		///  - expect adapter sends GetVersion and GetState packets.
		/// </summary>
		[Test]
		public void t1_connectService_sends_version_and_state_queries()
		{
			worker_start();		// start adapter
			m_srv.link.mock_connect();// switch service to available
			// now there should be two packets coming. One getVersion and one getState. (any order)
		  bool bHaveVer=false;
		  bool bHaveStat=false;
		  MockService.Sniff watcher = delegate(IPacket pck,bool down)
				{
					if( pck is PacketGetState )
						bHaveStat=true;
					if( pck is PacketGetVersion )
						bHaveVer=true;
					return (bHaveStat&&bHaveVer);
				};
		  bool res = m_srv.processThings(watcher,2500);
			Assert.True(res);
		}

		/// <summary>
		/// Test sequence:
		///  - start adapter, with testSet, and service, allow autoconnect.
		///  - adapter sends some packets
		///  - expect adapter will also send (and be replied) a GetTestSet.
		/// </summary>
		[Test]
		public void t2_service_asks_testset()
		{
			worker_start();		// start adapter
			m_srv.link.mock_connect();// switch service to available
			// process packets. should see a PacketGetTestList.
		  MockService.Sniff watcher = delegate(IPacket pck,bool down)
				{return ( (!down) && (pck is PacketTestList) );};
		  bool res = m_srv.processThings(watcher,2500);
			Assert.True(res);
			// now the adapter knows the testset.
		}

		/// <summary>
		/// Test sequence:
		///  - part from t2.
		///  - Check that queue to UI is empty (because its not connected)
		///  - connect UI
		///  - expect adapter should send State, Version and TestSet without the UI asking.
		/// </summary>
		[Test]
		public void t3_UI_get_info_after_connect()
		{
			// setup, startup service, wait for info query.
			t2_service_asks_testset();
			// UI queue must be empty (not yet connected)
			Assert.IsNull( m_mock_UILink.mock_pickup_sent_packet(false) );
			// now connect the UI. We should get state,version,testlist.
			m_mock_UILink.mock_connect();
			// send queries from UI.
			m_mock_UILink.mock_place_packet_to_recv(new PacketGetVersion());
			m_mock_UILink.mock_place_packet_to_recv(new PacketGetState());
			m_mock_UILink.mock_place_packet_to_recv(new PacketGetTestList());
			// now wait for the three packets.
		  bool p1,p2,p3;
			p1=p2=p3=false;
			// for half second, run sim. UI should be getting version, state and testlist.
			for(int i=0;i<50;i++)
			{
				m_srv.processThings(null,5);
			  IPacket upck = m_mock_UILink.mock_pickup_sent_packet(false);
				if( upck is PacketState )
					p1=true;
				if( upck is PacketVersion )
					p2=true;
				if( upck is PacketTestList )
					p3=true;
				if(p1&&p2&&p3)break;		// make quicker
			}
			Assert.True(p1);
			Assert.True(p2);
			Assert.True(p3);
		}

		/// <summary>
		/// Test sequence:
		///  - part from t3.
		///  - UI sends RunTests packet with IDs [1,3,5,6]
		///  - process and handle packets in mock-service.
		///  - expect UI gets State packet (without asking) indicating test active.
		/// </summary>
		[Test]
		public void t4_UI_requests_test_start()
		{
			// setup, connect service and UI.
			t3_UI_get_info_after_connect();
			// UI sends test-selection
		  PacketRunTests pack;
		  uint[] testIDs = new uint[]{1,3,5,6};
			pack = new PacketRunTests( testIDs , false , m_srv.testSet.CheckSum );
			pack.numRepeats = 1;
			// send.
			m_mock_UILink.mock_place_packet_to_recv(pack);
			// wait result
		  bool good=false;
			for(int i=0;i<50;i++)
			{
				// process service
				m_srv.processThings(null,5);
				// look for UI packets.
			  IPacket res = m_mock_UILink.mock_pickup_sent_packet(false);
				if( res!=null && (res is PacketAckTestRun) && ((PacketAckTestRun)res).successfulTestStart )
					{good=true;break;}
			}
			Assert.True(good);
		}

		/// <summary>
		/// Test sequence:
		///  - part from t4
		///  - mock service 'completes' tests one by one, sends .......
		/// </summary>
		[Test]
		public void t5_UI_gets_test_results()
		{
			// setup all, UI starts testing.
			t4_UI_requests_test_start();
			// now tests have been started.
		  List<TestResultModel> results = new List<TestResultModel>();
		  bool good=false;
			// run loop until getting a tests-done packet. then look at results list.
			for(int i=0;i<50;i++)
			{
				// process service
				m_srv.processThings(null,5);
				// look for UI packets.
			  Packet res = (Packet)m_mock_UILink.mock_pickup_sent_packet(false);
				if( res is PacketTestResult )
					results.Add( ((PacketTestResult)res).result );
				if( res is PacketAllTestrunsDone )
					{good=true;break;}
			}
			Assert.True(good);		// was there the test-done packet?
			Assert.AreEqual( 4 , results.Count );
			// Assert.AreEqual( 1 , results[0].     check correct ID???
		}

		/// <summary>
		/// UI queries all results (again).
		/// </summary>
		[Test]
		public void t6_UI_query_all_results()
		{
			// setup all, run a set of tests, get results into adapter.
			t5_UI_gets_test_results();
			// disconnect UI, reconnect.
			m_mock_UILink.mock_disconnect();
			System.Threading.Thread.Sleep(1);
			m_mock_UILink.mock_connect();
			// now query all results.
			m_mock_UILink.mock_place_packet_to_recv( new PacketGetAllResults() );
			//
		  List<TestResultModel> results = new List<TestResultModel>();
		  bool good=false;
			for( int i=0 ; i<50 ; i++ )
			{
				// process service
				m_srv.processThings(null,5);
				// look at packets to UI
			  Packet res = (Packet)m_mock_UILink.mock_pickup_sent_packet(false);
				if( res is PacketTestResult )
					results.Add( ((PacketTestResult)res).result );
				if( res is PacketSendAllResultsDone )
					{good=true;break;}
			}
			// see that we got a alldone and four testresults.
			Assert.True( good );
			Assert.AreEqual( 4 , results.Count );
		}

		[Test]
		public void start_abort()
		{
		  Packet pck;
			worker_start();
			m_srv.link.mock_connect();
			m_mock_UILink.mock_connect();
			// process away the three greet packets.
			for( int i=0 ; i<5 ; i++ )
				m_srv.processThings( null , 5 );
			// now not longer using the complete service mock...
			// UI: send test-start
			m_mock_UILink.mock_place_packet_to_recv( new PacketRunTests( new uint[]{2,3,4} , false , m_srv.testSet.CheckSum ) );
			// Service: expect test-start
			m_srv.link.expect_packet( typeof(PacketRunTests) );
			// Service: ack, and one result
			m_srv.link.mock_place_packet_to_recv( new PacketAckTestRun(true) );
			pck = new PacketTestResult();
			((PacketTestResult)pck).result = new TestResultModel(2,true);
			m_srv.link.mock_place_packet_to_recv( pck );
			// UI: expect an ack and a result
			m_mock_UILink.expect_packet( typeof(PacketAckTestRun) );
			m_mock_UILink.expect_packet( typeof(PacketTestResult) );
			// UI: send abort
			m_mock_UILink.mock_place_packet_to_recv( new PacketAbortTestsReq() );
			// Service: expect abort packet
			m_srv.expect_servicePacket( typeof(PacketAbortTestsReq) , false );
			// Service: ack abort
			m_srv.link.mock_place_packet_to_recv( new PacketAbortAck() );
			// UI: expect abort ack
			m_mock_UILink.expect_packet( typeof(PacketAbortAck) );
			// UI: send getState
			m_mock_UILink.mock_place_packet_to_recv( new PacketGetState() );
			// UI: expect state with pasive.
			pck = (Packet)m_mock_UILink.expect_packet( typeof(PacketState) );
			Assert.AreEqual( 0 , ((PacketState)pck).state );
		}


		private GTestAdapter.AdapterCore m_adapter;
		private MockPacketLink<IPacket> m_mock_UILink;
		private MockPacketLink<string> m_mock_serialLink;
		private MockPacketLink<string> m_mock_localConsoleLink;
		private System.Threading.Thread m_worker;
		private System.Threading.EventWaitHandle m_workerDoneSig;

		private MockService m_srv;
	}
}
