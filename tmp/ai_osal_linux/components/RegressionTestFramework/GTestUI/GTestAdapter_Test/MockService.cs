using System.Collections.Generic;
using GTestAdapter;
using GTestCommon;
using GTestCommon.Links;
using GTestCommon.Models;
using NUnit.Framework;


namespace GTestAdapter_Test
{

public class MockService
{
	// function used to watch packets processed. return true to exit loop immediately. Default should return false.
	public delegate bool Sniff(IPacket pck,bool bDownToSrv);

	public MockService()
	{
		m_link = new MockPacketLink<IPacket>();
		m_testSet = new AllTestCases(false);
		m_testSet.AppendNewTestGroup("group1");
		m_testSet.AppendNewTestGroup("group2");
		m_testSet[0].AppendNewTest("test_1",1);
		m_testSet[0].AppendNewTest("test_2",2);
		m_testSet[0].AppendNewTest("test_3",3);
		m_testSet[1].AppendNewTest("test_A",4);
		m_testSet[1].AppendNewTest("test_B",5);
		m_testSet[1].AppendNewTest("test_C",6);
		m_testSet[1].AppendNewTest("test_D",7);
		m_testSet.CheckSum = 4711;
		m_testList = new List<Test>();
		for(int g=0;g<m_testSet.TestGroupCount;g++)
		{
		  TestGroupModel gm = m_testSet[g];
			for(int t=0;t<gm.TestCount;t++)
				m_testList.Add(gm[t]);
		}
		m_testingActive = false;
		m_linkSendSer = 0x1000;
	}

	public Packet expect_servicePacket( System.Type pckType , bool bProcessIt )
		{return expect_servicePacket(new System.Type[]{pckType},bProcessIt);}

	public Packet expect_servicePacket( System.Type[] pckTypes , bool bProcessIt )
		{return expect_servicePacket(new List<System.Type>(pckTypes),bProcessIt);}

	public Packet expect_servicePacket( List<System.Type> pckTypes , bool bProcessIt )
	{
	  Packet rpck = null;
		if(!bProcessIt)
			return (Packet)m_link.expect_packet( pckTypes );
		// catch and process packet.
	  Sniff watcher = delegate(IPacket pck,bool bDownToSrv)
			{
				if(!bDownToSrv)
					return false;		// only looking at down packets. continue loop if up packet.
				// our packet?
				if( pckTypes.Contains(pck.GetType()) )
					{rpck = (Packet)pck;return true;}
				// another? then break loop and return no packet.
				if( pck!=null )
					return true;
				return false;		// only for null-packet, continue loop
			};
		processThings( watcher , 2500 );
		return rpck;
	}


	/// <summary>
	/// Process available packets and return when idle.
	/// </summary>
	/// <param name="watcher">packet watcher function.</param>
	/// <param name="timeout">Maximum processing time. pass zero for just one pass. -1 for infinite.</param>
	/// <returns>returns true if loop was ended because the Sniff hit and returned true.</returns>
	public bool processThings(Sniff watcher,int timeout)
	{
	  int tickStart,duration;
		m_watcher=watcher;
		tickStart = System.Environment.TickCount;
		while(true)
		{
		  IPacket pck;
			// process stuff
			do{
				pck = m_link.mock_pickup_sent_packet(false);
				if(processServicePack(pck))
					return true;
				if( pck!=null && m_watcher!=null )
					if( m_watcher.Invoke(pck,true) )
					{
						m_watcher=null;
						return true;
					}
			}while(pck!=null);
			// check timeout
			if( timeout==0 )break;
			System.Threading.Thread.Sleep(5);
			if( timeout<0 )continue;
			duration = System.Environment.TickCount-tickStart;
			if( duration>timeout )break;
		}
		m_watcher=null;
		return false;
	}

	/// <summary>
	/// Process a packet as mock-service
	/// </summary>
	/// <param name="pck">packet to process.</param>
	/// <returns>true if the watcher is in place and returned true on this or a response packet.</returns>
	private bool processServicePack(IPacket pck)
	{
	  bool res = false;
		// handles some standard packets.
		if( pck is PacketGetState )
		{
			return havePacketForUplink(
					new PacketState((byte)( m_testingActive ? 1 : 0 ))		// 1? ..... TODO.
			);
		}
		if( pck is PacketGetVersion )
		{
			return havePacketForUplink(
					new PacketVersion("mock_service 0.0")
			);
		}
		if( pck is PacketGetTestList )
		{
		  PacketTestList pack = new PacketTestList();
			Assert.NotNull(m_testSet);
			pack.data = helper_clone_testRunModel(m_testSet);
			return havePacketForUplink(pack);
		}
		if( pck is PacketRunTests )
		{
		  PacketRunTests p = pck as PacketRunTests;
			if(m_testingActive)
			{
				// abort first?
				Assert.True(false);
			}
			if( ! p.invertSelection )
			{
				// normal test list
				m_selectedTests = new List<uint>(p.tests);
			}else{
			  Dictionary<uint,bool> mp = new Dictionary<uint,bool>();
				// invert test list.
				foreach( uint tID in p.tests )mp[tID]=true;
				m_selectedTests = new List<uint>();
				for( int i=0 ; i<m_testSet.TestGroupCount ; i++ )
				{
				  TestGroupModel grp = m_testSet[i];
					for( int j=0 ; j<grp.TestCount ; j++ )
					{
					  Test tst = grp[j];
						if(!mp.ContainsKey(tst.ID))
							m_selectedTests.Add(tst.ID);
					}
				}
			}
			m_selectedNumIterations = p.numRepeats;	// zero means infinite ? ......
			m_activeTest = 0;
			m_activeIteration = 0;
			m_testingActive = true;
			return havePacketForUplink(new PacketAckTestRun(true));
		}
		if( pck is PacketAbortTestsReq )
		{
			// stop test sequence.
			m_testingActive = false;
			return havePacketForUplink(new PacketAbortAck());
		}
		if( pck == null )
		{
			// null means pass time. Advance tests if active.
			if(m_testingActive)
			{
				if( m_activeIteration<m_selectedNumIterations && m_activeTest<(uint)m_selectedTests.Count )
				{
				  uint tstID = m_selectedTests[(int)m_activeTest];
					// complete 'test'.
				  PacketTestResult rpck = new PacketTestResult();
					rpck.result = new TestResultModel( tstID , (tstID&(tstID-1))!=0 );
					res = havePacketForUplink(rpck) || res;
					// move to next
					if( (++m_activeTest) >= (uint)m_selectedTests.Count )
					{
						m_activeTest=0;
						m_activeIteration++;
					}
				}else{
					// finish.
					m_testingActive = false;
					res = havePacketForUplink( new PacketAllTestrunsDone() ) || res;
				}
			}
		}
		return res;
	}


	static private AllTestCases helper_clone_testRunModel(GTestCommon.Models.AllTestCases mdl)
	{
		// misuse serialization to clone.
	  PacketTestList dummy = new PacketTestList();
		dummy.data = mdl;
	  PacketFactory4service fac = new PacketFactory4service();
	  byte[] buffer = new byte[0x4000];
	  uint bytes = fac.encodePacket( dummy , buffer , 0 , (uint)buffer.Length );
		dummy = (PacketTestList)fac.decodePacket( buffer , 0 , bytes , out bytes );
		return dummy.data;
	}

	private bool havePacketForUplink(Packet pck)
	{
		pck.serial = m_linkSendSer;
	  bool res=false;
		if(m_watcher!=null)
			res = m_watcher.Invoke(pck,false);
		m_link.mock_place_packet_to_recv(pck);
		return res;
	}

	public MockPacketLink<IPacket> link{get{return m_link;}}
	public AllTestCases testSet{get{return m_testSet;}}

	private MockPacketLink<IPacket> m_link;

	private AllTestCases m_testSet;
	private List<GTestCommon.Models.Test> m_testList;
	private List<uint> m_selectedTests;
	private uint m_selectedNumIterations;
	private bool m_testingActive;
	private uint m_activeTest;
	private uint m_activeIteration;
	private uint m_linkSendSer;

	private Sniff m_watcher;
}
}
