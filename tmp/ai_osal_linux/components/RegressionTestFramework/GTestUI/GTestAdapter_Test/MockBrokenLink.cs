using GTestCommon;
using NUnit.Framework;



namespace GTestAdapter_Test
{

/// <summary>
/// A link which will fail when calling Start().
/// </summary>
public class MockBrokenLink<T> : GTestCommon.Links.IQueuedPacketLink<T>
{
	public MockBrokenLink()
	{
		m_recvQ = new Queue<T>();
		m_sendQ = new Queue<T>();
	}

	public void Dispose(){}

	public Queue<T> inQueue{get{return m_recvQ;}}

	public Queue<T> outQueue{get{return m_sendQ;}}

	public bool Start(string dummyTarget)
	{
		return false;		// this mock always fails to start.
	}

	public void Stop(bool sendAll)
	{
	}

	public bool bIsStarted()
	{
		return false;
	}

	public bool bIsConnected()
	{
		return false;
	}

	public event GTestCommon.Links.ConnectEventHandler connect;

	private Queue<T> m_sendQ;
	private Queue<T> m_recvQ;
}

}
