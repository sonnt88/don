using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Text;

namespace GTestAdapter_Test
{

	[TestFixture]
	public class ExtProgLauncher_Test
	{
		public ExtProgLauncher_Test()
		{
		}

		[SetUp]
		public void setup()
		{
		}

		[TearDown]
		public void uninit()
		{
		}

		[Test]
		public void splitStr_easy()
		{
		  string[] expect={"a","b"};
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("a b",true);
			Assert.AreEqual( 0 , compareStrArr(res,expect) );
		}

		[Test]
		public void splitStr_3_items()
		{
		  string[] expect={"abc","b","g"};
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("abc b  g",true);
			Assert.AreEqual( 0 , compareStrArr(res,expect) );
		}

		[Test]
		public void splitStr_trailSpace()
		{
		  string[] expect={"abc","qwerty"};
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("abc qwerty   ",true);
			Assert.AreEqual( 0 , compareStrArr(res,expect) );
		}

		[Test]
		public void splitStr_leadSpace()
		{
		  string[] expect={"abc","qwerty"};
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring(" abc qwerty",true);
			Assert.AreEqual( 0 , compareStrArr(res,expect) );
		}

		[Test]
		public void splitStr_quotes()
		{
		  string[] expect={"foo bar","..."};
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("\"foo bar\" ...",true);
			Assert.AreEqual( 0 , compareStrArr(res,expect) );
		}

		[Test]
		public void splitStr_escQuote()
		{
		  string[] expect={"abc","de\"f"};
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("abc de\\\"f",true);
			Assert.AreEqual( 0 , compareStrArr(res,expect) );
		}

		[Test]
		public void splitStr_badQuot1()
		{
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("abc \"d\"ef\" foo",true);
			Assert.IsNull( res );
		}

		[Test]
		public void splitStr_badQuot2()
		{
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("f\"oo\" bar",true);
			Assert.IsNull( res );
		}

		[Test]
		public void splitStr_badQuot3()
		{
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("\"\"foo\"\" bar",true);
			Assert.IsNull( res );
		}

		[Test]
		public void splitStr_badQuot4()
		{
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("\"foo\"\"\" bar",true);
			Assert.IsNull( res );
		}

		[Test]
		public void splitStr_noStrip()
		{
		  string[] expect={"\"foo\"","b\\\"ar"};
		  string[] res;
			res = GTestAdapter.ExtProgLauncher.splitstring("\"foo\" b\\\"ar",false);
			Assert.AreEqual( 0 , compareStrArr(res,expect) );
		}


		[Test]
		public void execProcess_python()
		{
		  string exeName = "E:\\Python26\\python.exe";
		  string workDir = "E:\\Dahlhoff\\p\\scram\\";
		//  string[] args = {"scram.py"};
		  string[] args = {"scram.py"};
		  GTestAdapter.ExtProgLauncher.ExtProgLauncher_Result result;
			result = GTestAdapter.ExtProgLauncher.callProgram(workDir,exeName,args);
			Assert.NotNull( result );
			Assert.AreEqual( 0 , result.returnCode );
		}

		[Test]
		public void execProcess_python2()
		{
		  string cmdLine = "E:\\Python26\\python.exe --version";
		  GTestAdapter.ExtProgLauncher.ExtProgLauncher_Result result;
			result = GTestAdapter.ExtProgLauncher.callProgram(cmdLine);
			Assert.NotNull( result );
			Assert.AreEqual( 0 , result.returnCode );
			Assert.True( result.stdErr.StartsWith("Python") );
		}

		[Test]
		public void execProcess_batchFile2()
		{
		  string cmdLine = "C:\\temp\\helloworld.bat";
		  GTestAdapter.ExtProgLauncher.ExtProgLauncher_Result result;
			// call a batch file with a sigle line: "echo hello World"
			result = GTestAdapter.ExtProgLauncher.callProgram("C:\\temp","cmd.exe","/c "+cmdLine);
			Assert.NotNull( result );
			Assert.AreEqual( 0 , result.returnCode );
			Assert.True( result.stdOut.Contains("hello World") );
		}

		[Test]
		public void execProcess_badPath()
		{
		  string exeName = "C:\\temp\\dasgibtesnicht.exe";
		  string workDir = "C:\\";
		//  string[] args = {"scram.py"};
		  string[] args = {"a","b","c"};
		  GTestAdapter.ExtProgLauncher.ExtProgLauncher_Result result;
			result = GTestAdapter.ExtProgLauncher.callProgram(workDir,exeName,args);
			Assert.AreEqual( null , result );
		}



		private int compareStrArr(string[] a,string[] b)
		{
		  int i;
			for(i=0;i<a.Length;i++)
			{
			  int cm;
				if( i>=b.Length )return 1;
				cm = a[i].CompareTo( b[i] );
				if(cm!=0)return cm;
			}
			if( a.Length<b.Length )return -1;
			return 0;
		}

	}
}
