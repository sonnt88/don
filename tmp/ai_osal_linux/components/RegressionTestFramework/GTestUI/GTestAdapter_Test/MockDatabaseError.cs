using System.Collections.Generic;
using System.Data;
using NUnit.Framework;
using System.Text.RegularExpressions;


namespace GTestAdapter_Test
{

internal class MockDatabaseError : System.Data.DataException
{
	public MockDatabaseError(string message)
	 : base(message)
	{
	}

	public MockDatabaseError()
	 : base()
	{
	}

}

}





