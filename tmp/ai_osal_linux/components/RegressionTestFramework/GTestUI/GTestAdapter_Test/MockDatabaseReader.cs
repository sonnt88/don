using System.Collections.Generic;
using System.Data;
using NUnit.Framework;


namespace GTestAdapter_Test
{

internal class MockDatabaseReader : IDataReader
{
	public MockDatabaseReader(IDbConnection db,MockDatabase.Table results)
	{
		m_results = results;
		m_cur = -1;
	}

	public void Dispose()
	{
		Close();
	}

	//==========================================================================
	//                         methods for IDataReader
	//==========================================================================

	public int Depth{get{return 0;}}

	public bool IsClosed{get{return m_closed;}}

	public int RecordsAffected{get{return m_results.rows.Count;}}

	public void Close(){m_closed=true;}

	public DataTable GetSchemaTable()
	{
		// mock does not support this.
		Assert.Fail();
		return null;
	}

	public bool NextResult()
	{
		// mock does not support batch queries.
		Assert.Fail();
		return false;
	}

	public bool Read()
	{
		if(m_cur>=m_results.rows.Count)
			return false;
		m_cur++;
		return m_cur<m_results.rows.Count;
	}

	//==========================================================================
	//                         methods for IDataRecord
	//==========================================================================

	public int FieldCount{get{return m_results.cols.Count;}}
	public object this[int i]{get{return m_results.rows[m_cur].items[i];}}
	public object this[string name]{get{return this[GetOrdinal(name)];}}
	public bool GetBoolean(int i){return (bool)this[i];}
	public byte GetByte(int i){return (byte)this[i];}
	public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length){Assert.Fail();return 0;}
	public char GetChar(int i){return (char)this[i];}
	public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length){Assert.Fail();return 0;}
	public IDataReader GetData(int i){Assert.Fail();return null;}
	public string GetDataTypeName(int i){Assert.Fail();return null;}
	public System.DateTime GetDateTime(int i){return (System.DateTime)this[i];}
	public decimal GetDecimal(int i){return (decimal)this[i];}
	public double GetDouble(int i){return (double)this[i];}
	public System.Type GetFieldType(int i){return this[i].GetType();}
	public float GetFloat(int i){return (float)this[i];}
	public System.Guid GetGuid(int i){return (System.Guid)this[i];}
	public short GetInt16(int i){return (short)this[i];}
	public int GetInt32(int i){return (int)this[i];}
	public long GetInt64(int i){return (long)this[i];}
	public string GetName(int i){return (string)this[i];}
	public int GetOrdinal(string name){for(int i=0;i<m_results.cols.Count;i++){if(m_results.cols[i].name==name)return i;}throw new System.IndexOutOfRangeException();}
	public string GetString(int i){return (string)this[i];}
	public object GetValue(int i){return this[i];}
	public int GetValues(object[] values){System.Array.Copy(m_results.rows[m_cur].items.ToArray(),0,values,0,m_results.cols.Count);return m_results.cols.Count;}
	public bool IsDBNull(int i){return (this[i]==null);}

	//==========================================================================
	//                         members
	//==========================================================================

	MockDatabase.Table m_results;
	private int m_cur;
	private bool m_closed;
}

}
