using System.Collections.Generic;
using System.Data;
using NUnit.Framework;




namespace GTestAdapter_Test
{

public class MockDatabase : System.Data.IDbConnection
{
	public MockDatabase()
	{
		m_dataBase = new List<Table>();
		m_dbChosen = "";
	}

	public void Dispose()
	{
	}

	public string ConnectionString
	{
		get{return "mock";}
		set{Assert.AreEqual(value,"mock");}
	}

	public int ConnectionTimeout{get{return 1;}}

	public string Database{get{Assert.True(m_dbConnected);return m_dbChosen;}}

	public ConnectionState State{get{return ( m_dbConnected ? ConnectionState.Open : ConnectionState.Closed );}}

	public IDbTransaction BeginTransaction()
	{
		// mock does not support transactions.
		Assert.Fail();
		return null;
	}

	public IDbTransaction BeginTransaction(IsolationLevel il)
	{
		// mock does not support transactions.
		Assert.Fail();
		return null;
	}

	public void ChangeDatabase(string databaseName)
	{
		// only allow setting once in this mock. Ignore name. Just not emtpy.
		Assert.True( m_dbChosen=="" );
		Assert.AreNotEqual( databaseName , "" );
		m_dbChosen = databaseName ;
	}

	public void Close()
	{
		m_dbConnected=false;
	}

	public IDbCommand CreateCommand()
	{
		return new MockDatabaseCmd(this);
	}

	public void Open()
	{
		Assert.False( m_dbConnected );
		m_dbConnected = true;
	}

	public uint Count{get{return (uint)m_dataBase.Count;}}
	public Table this[uint idx]{get{return m_dataBase[(int)idx];}}
	public Table this[string name]{get{Table res = getTable(name);if(res==null){throw new System.IndexOutOfRangeException("not table named "+name);}return res;}}
	public IEnumerator<Table> GetEnumerator(){foreach(Table t in m_dataBase)yield return t;}

	internal Table getTable(string tableName)
	{
		foreach( Table tb in m_dataBase )
			if(tb.name==tableName)
				return tb;
		return null;
	}

	internal void addTable(Table newTab)
	{
		if(getTable(newTab.name)!=null)
			throw new MockDatabaseError("Cannot create table. Table "+newTab.name+" already in database.");
		m_dataBase.Add(newTab);
	}

	public class Table
	{
		public Table(){cols = new List<Column>();rows=new List<Row>();}

		public int getColIndex(string cn){for( int i=0 ; i<cols.Count ; i++ ){if(cols[i].name==cn)return i;}return -1;}

		public string name;
		public List<Column> cols;
		public List<Row> rows;
	}

	public class Column
	{
		public string name;
		public System.Type type;
		public bool key;
		public bool notNull;
	}

	public class Row
	{
		public Row() {items=new List<object>();}
		public List<object> items;

	}

	string m_dbChosen;
	bool m_dbConnected;
	private List<Table> m_dataBase;

	public uint m_count_select;
	public uint m_count_insert;
	public uint m_count_create;
}

}
