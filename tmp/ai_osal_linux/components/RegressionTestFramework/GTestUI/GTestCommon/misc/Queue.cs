using System.Collections.Generic;


namespace GTestCommon
{
	/// <summary>
	/// A thread safe queue which supports waiting for input.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public sealed class Queue<T>
	{
		public Queue()
		{
			m_items = new System.Collections.Generic.Queue<T>();
			m_mutex = new System.Threading.Mutex();
			m_event = new System.Threading.AutoResetEvent(false);
			m_WaitAllHandles = new System.Threading.WaitHandle[]{m_event,m_mutex};
		}

		/// <summary>
		/// Place an item into the queue. Resize internally if necessary.
		/// If another thread is waiting in the Get() call, it will return with the new item.
		/// If a thread is waiting on the waithandle (from getWaitHandle()), this handle will signal.
		/// </summary>
		/// <param name="item"></param>
		public void Add(T item)
		{
		  bool setSig;
			m_mutex.WaitOne();
			setSig = (m_items.Count<=0);		// only set signal if this is first item.
			m_items.Enqueue(item);
			m_mutex.ReleaseMutex();
			if(setSig)
			{
				m_event.Set();
				if( notEmptyEvent != null )
					notEmptyEvent(this);
			}
		}

		/// <summary>
		/// Get an item from the Queue.
		/// If queue is not empty, returns immediately with an item.
		/// If queue is empty and bBlock is true, wait for an item. Return if one arrives.
		/// If queue is empty and bBlock is false, return null or zero.
		/// </summary>
		/// <param name="bBlock">If this is true, thread will block and wait if queue is empty.</param>
		/// <returns>retrieved item or null/zero if empty and non-blocking.</returns>
		public T Get(bool bBlock)
		{
			if(bBlock)
				return Get(-1);
			else
				return Poll();
		}

		/// <summary>
		/// Get an item from the Queue.
		/// If queue is not empty, returns immediately with an item.
		/// If queue is empty and bBlock is true, wait for an item. Return if one arrives.
		/// If queue is empty and bBlock is false, return null or zero.
		/// </summary>
		/// <param name="timeOut">Timeout. zero to never block, -1 to wait infinitely.</param>
		/// <returns>retrieved item or null/zero if empty and non-blocking or timeout.</returns>
		public T Get(int timeOut)
		{
			if(timeOut==0)
				return Poll();
			m_mutex.WaitOne();
			// wait for something to come in
		  int endTim=0;
			if(timeOut>0)
				endTim = System.Environment.TickCount + timeOut;
			while( m_items.Count<=0 )
			{
				m_mutex.ReleaseMutex();
				if(timeOut>0)
				{
				  int timNow = System.Environment.TickCount;
					if(timNow>=endTim)
						return default(T);
					if(!System.Threading.WaitHandle.WaitAll(m_WaitAllHandles,endTim-timNow))
						return default(T);
				}else
					System.Threading.WaitHandle.WaitAll(m_WaitAllHandles);
			}
		  T res=m_items.Dequeue();
			m_mutex.ReleaseMutex();
			return res;
		}

		/// <summary>
		/// Get an item from the queue without blocking. Returns default(T) if empty.
		/// </summary>
		/// <returns>item or default(T) if empty.</returns>
		public T Poll()
		{
			m_mutex.WaitOne();
		  T res;
			if( m_items.Count>0 )
				res = m_items.Dequeue();
			else
				res = default(T);
			m_mutex.ReleaseMutex();
			return res;
		}

		/// <summary>
		/// Check if the queue is empty.
		/// </summary>
		/// <returns>True if empty.</returns>
		public bool isEmpty()
		{
		  bool res;
			m_mutex.WaitOne();
			res = (m_items.Count<=0);
			m_mutex.ReleaseMutex();
			return res;
		}

		/// <summary>
		/// Get the WaitHandle used for the queue.
		/// Will be set when an item is added into the empty queue.
		/// Same signal used internally for blocking Get() calls.
		/// </summary>
		/// <returns>The wait handle.</returns>
		public System.Threading.WaitHandle getWaitHandle()
		{
			return m_event;
		}

		private System.Collections.Generic.Queue<T> m_items;
		private System.Threading.Mutex m_mutex;
		private System.Threading.EventWaitHandle m_event;
		private System.Threading.WaitHandle[] m_WaitAllHandles;

		public delegate void QueueNotEmpty(Queue<T> self);

		/// <summary>
		/// Event to be notified when the queue switches from empty to non-empty.
		/// Note that the event is called from the thread calling Add().
		/// </summary>
		public event QueueNotEmpty notEmptyEvent;
	}
}


