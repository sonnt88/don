
namespace GTestCommon.misc
{

	/// <summary>
	/// A dummy Stream similar to the Null stream.
	/// Contrary to the Null stream, the Read() does not return EOF, but blocks.
	/// Write consumes any data like the Null stream.
	/// </summary>
	public class DummyStream : System.IO.Stream
	{
		private class DummyAsyncResult : System.IAsyncResult
		{
			public DummyAsyncResult(DummyStream str,bool read,System.AsyncCallback cb)
			{
				m_str = str;m_isRd=read;m_cb=cb;
				m_ended=false;
			}
			public DummyStream m_str;
			public bool m_isRd;
			public bool m_ended;	// Was EndXxxx called?
			public System.AsyncCallback m_cb;

			public object AsyncState {get{return this;}}
			public System.Threading.WaitHandle AsyncWaitHandle {get{return m_str.m_evt;}}
			public bool CompletedSynchronously {get{return !m_isRd;}}
			public bool IsCompleted {get{return m_ended;}}
		};

		public DummyStream()
		{
			m_isOpen=true;
			m_evt=new System.Threading.ManualResetEvent(false);
		}
		public override bool CanRead {get{return true;}}
		public override bool CanSeek {get{return false;}}
		public override bool CanTimeout {get{return false;}}
		public override bool CanWrite {get{return true;}}
		public override long Length {get{return 0;}}
		public override long Position {get{return 0;}set{}}
		public override System.IAsyncResult BeginRead(byte[] buffer, int offset, int count, System.AsyncCallback callback, object state)
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			return new DummyAsyncResult(this,true,callback);
		}
		public override System.IAsyncResult BeginWrite(byte[] buffer, int offset, int count, System.AsyncCallback callback, object state)
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			return new DummyAsyncResult(this,false,callback);
		}
		public override void Close()
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			m_isOpen=false;
		}
		public override int EndRead(System.IAsyncResult asyncResult)
		{
			DummyAsyncResult asr = asyncResult as DummyAsyncResult;
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			if( asr==null || asr.m_ended || !asr.m_isRd )
				throw new System.SystemException("Bad EndRead() call.");
			// would block forever
			throw new System.SystemException("EndRead() would block forever");
		}
		public override void EndWrite(System.IAsyncResult asyncResult)
		{
			DummyAsyncResult asr = asyncResult as DummyAsyncResult;
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			if( asr==null || asr.m_ended || asr.m_isRd )
				throw new System.SystemException("Bad EndWrite() call.");
			asr.m_ended=true;
		}
		public override void Flush()
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
		}
		public override int Read(byte[] buffer, int offset, int count)
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			if(count<1)return 0;
			throw new System.SystemException("Read() would block forever");
		}
		public override int ReadByte()
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			throw new System.SystemException("ReadByte() would block forever");
		}
		public override long Seek(long offset, System.IO.SeekOrigin origin)
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			throw new System.NotSupportedException();
		}
		public override void SetLength(long value)
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
			throw new System.NotSupportedException();
		}
		public override void Write(byte[] buffer, int offset, int count)
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
		}
		public override void WriteByte(byte value)
		{
			if(!m_isOpen)throw new System.IO.IOException("stream was closed.");
		}

		private bool m_isOpen;
		public System.Threading.ManualResetEvent m_evt;
	}
}
