

namespace GTestCommon
{

	public interface IPacket
	{
		uint serial{get;set;}
	}

	public interface IPacketConverter
	{
		/// <summary>
		/// Method to encode a packet.
		/// </summary>
		/// <param name="pck">reference to packet to encode.</param>
		/// <param name="buffer">byte buffer to encode into.</param>
		/// <param name="start">start offset into buffer.</param>
		/// <param name="maxCount">space available in buffer (after start)</param>
		/// <returns>Returns number of bytes used in buffer. throws exception if buffer too small.</returns>
		uint encodePacket(IPacket pck,byte[] buffer,uint start,uint maxCount);

		/// <summary>
		/// Decode a packet from bytes.
		/// </summary>
		/// <param name="buffer">byte buffer with data.</param>
		/// <param name="start">start offset in buffer.</param>
		/// <param name="count">number of valid bytes in buffer (after start)</param>
		/// <param name="skipData">out: number of bytes actually consumed.</param>
		/// <returns>Returns reference to generated packet instance.
		/// Returns Null if insufficient data. May still consume data in this case (skip garbage)
		/// </returns>
		IPacket decodePacket(byte[] buffer,uint start,uint count,out uint skipData);
	}


	/// <summary>
	/// Exception type thrown when trying to create a packet but there is not enough input data.
	/// </summary>
	public class InsufficientData : System.Exception
	{
	}

	/// <summary>
	/// Exception type thrown when trying to encode a packet but there is not enough free space in buffer.
	/// </summary>
	public class InsufficientSpace : System.Exception
	{
	}

}
