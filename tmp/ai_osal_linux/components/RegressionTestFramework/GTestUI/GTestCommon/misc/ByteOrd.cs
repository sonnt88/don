


namespace GTestCommon
{

public static class ByteOrd
{
	public static ulong h2n(ulong value){return (ulong)System.Net.IPAddress.HostToNetworkOrder((long)value);}
	public static long h2n(long value){return System.Net.IPAddress.HostToNetworkOrder(value);}
	public static uint h2n(uint value){return (uint)System.Net.IPAddress.HostToNetworkOrder((int)value);}
	public static int h2n(int value){return System.Net.IPAddress.HostToNetworkOrder(value);}
	public static ushort h2n(ushort value){return (ushort)System.Net.IPAddress.HostToNetworkOrder((short)value);}
	public static short h2n(short value){return System.Net.IPAddress.HostToNetworkOrder(value);}

	public static ulong n2h(ulong value){return (ulong)System.Net.IPAddress.NetworkToHostOrder((long)value);}
	public static long n2h(long value){return System.Net.IPAddress.NetworkToHostOrder(value);}
	public static uint n2h(uint value){return (uint)System.Net.IPAddress.NetworkToHostOrder((int)value);}
	public static int n2h(int value){return System.Net.IPAddress.NetworkToHostOrder(value);}
	public static ushort n2h(ushort value){return (ushort)System.Net.IPAddress.NetworkToHostOrder((short)value);}
	public static short n2h(short value){return System.Net.IPAddress.NetworkToHostOrder(value);}


}

}

