

namespace GTestCommon.Links
{



	public abstract class QueuedItemsLink<T> : IQueuedPacketLink<T> , System.IDisposable
	{
		public QueuedItemsLink()
		 : this( new Queue<T>() )
		{
		}

		public QueuedItemsLink( Queue<T> inQueue )
		{
			m_started = false;
			m_connected = false;
			m_inStream=null;
			m_outStream=null;

			m_sendQueue = new Queue<T>();
			m_recvQueue = inQueue;

			m_event = new System.Threading.AutoResetEvent(false);
			m_qevent = new System.Threading.ManualResetEvent(false);
			m_doneEvent = new System.Threading.ManualResetEvent(false);

			m_lossExceptionTypes = getConnectionLossExceptionTypes();
		}

		protected virtual uint defaultBufferSize{get{return 0x10000u;}}

		/// <summary>
		/// Starts the internal worker thread which then tries to connect.
		/// </summary>
		/// <param name="targetAddress">numeric IPv4 address with port.</param>
		/// <returns>returns true if successful.</returns>
		public virtual bool Start(string targetAddress)
		{
			if(m_started)
				throw new System.Exception("is already connected");
			if( m_sendQueue==null || m_recvQueue==null )
				throw new System.Exception("in and out queues not yet set.");
			if(m_readBuffer==null)
			{
				m_readBuffer = new byte[defaultBufferSize];		// must be enough for any packet type.
				m_sendBuffer = new byte[defaultBufferSize];		// must be enough for any packet type.
			}
			m_qevent.Reset();
			m_doneEvent.Reset();
			m_started = true;
			m_thr = new System.Threading.Thread(thr_mainLoop);
			m_thr.Start();
			return true;			// cannot wait for success. This class keeps trying endlessly.
		}

		/// <summary>
		/// Request this object to stop the connection. Will close socket and terminate worker thread.
		/// </summary>
		public virtual void Stop(bool sendAll)
		{
			m_sendAllBeforeQuitting = sendAll;
			if(!m_started)return;
			m_qevent.Set();
			m_doneEvent.WaitOne();
			m_thr = null;
			m_started = false;
		}

		public virtual void Dispose()
		{
			this.Stop(false);
		}

		public bool bIsStarted()
		{
			return m_started;
		}

		public bool bIsConnected()
		{
			return connected;
		}

		public Queue<T> inQueue{
			get{return m_recvQueue;}
		}

		public Queue<T> outQueue{
			get{return m_sendQueue;}
		}

		protected enum ReCon
		{
			CONNECTED = 0,
			FAILED = 1,
			TIMEOUT = 2,
			QUITSIGNAL = 3
		}

		/// <summary>
		/// Worker thread of this object.
		/// </summary>
		private void thr_mainLoop()
		{
		  bool bQuit=false;
			// thrad main function.
			// loop endless. Try to connect, process connection, send packets.
			// when loosing connect, immediately try to reconnect until successful or Stop() is called.
			while(!bQuit)
			{
			  System.Threading.WaitHandle[] handles = new System.Threading.WaitHandle[3];		// for case without active write
				handles[0]   = m_qevent;
				handles[1]   = null;	// set later for read handle
				handles[2]   = null;	// set later for write or sendQueue handle
				m_connected=false;
				try{
				  System.IAsyncResult asy_read;
				  System.IAsyncResult asy_write=null;
				  ReCon tcr;
					tcr = tryReconnect();
					if( tcr==ReCon.QUITSIGNAL )
						{bQuit=true;break;}
					if( tcr==ReCon.FAILED || tcr==ReCon.TIMEOUT )
						{System.Threading.Thread.Sleep(333);continue;}
					// got connection.
					connected=true;	// use setter -> trigger event
					m_event.Set();
					m_readBuffer_bytes=0;
					// start read. (and process away any data found there).
					asy_read = thr_startRead();
					if( asy_read == null )
						throw new System.IO.IOException();	// peer shut down. behave like thrown off.
					while(true)
					{
					  int waitRes;
					  int numBytes;
					  T pck;
						// check quit condition
						if(  bQuit  &&
						     ( (!m_sendAllBeforeQuitting) || (!this.connected) || (asy_write==null&&m_sendQueue.isEmpty()) )
						)
							break;
						// get handles and wait for event.
						handles[1] = asy_read.AsyncWaitHandle;
						handles[2] = ( asy_write==null ? m_sendQueue.getWaitHandle() : asy_write.AsyncWaitHandle );
						waitRes = System.Threading.WaitHandle.WaitAny(handles);
						switch(waitRes)
						{
						case 0:			// quit event
							bQuit=true;
							m_qevent.Reset();
							break;
						case 1:			// read data
							numBytes = m_inStream.EndRead(asy_read);
							asy_read=null;
							if(numBytes<=0)
								throw new System.IO.IOException();	// peer shut down.behave like thrown off.
							m_readBuffer_bytes+=(uint)numBytes;
							thr_procInData();
							asy_read = thr_startRead();
							if( asy_read == null )
								throw new System.IO.IOException();	// peer shut down.behave like thrown off.
							break;
						case 2:			// sending
							if( asy_write != null )
							{
								// event from write done
								m_outStream.EndWrite(asy_write);
								asy_write=null;
							}
							while(( pck = m_sendQueue.Get(false) )!=null)
							{
								// encode and send. break if write blocks.
								asy_write = thr_encodeAndSendPacket(pck);
								if(asy_write!=null)break;
							}
							break;
						}
					}
					// leaving because of quit signal.
					break;

				}catch(System.Exception ex)
				{
					// is an exception which indicates connection loss?
				  System.Type exType = ex.GetType();
					if( ! m_lossExceptionTypes.Contains(exType) )
						throw;			// not ours. pass on.
				}
				onLostConnection();
				connected=false;	// use setter -> trigger event
				System.Threading.Thread.Sleep(167);			// don't be banging on it...
				// TODO: ..... When waiting here, what to do with those packets in queue? drop?
			}
			m_doneEvent.Set();
		}

		/// <summary>
		/// Tries to reconnect the TCP link. Returns true if successful. False if quit signal received.
		/// </summary>
		/// <returns>True for connect success, False for quitsignal received.</returns>
		protected virtual ReCon tryReconnect()
		{
			// Simple function for objects that do not support dynamic reconnect.
			return ReCon.CONNECTED;
		}

		/// <summary>
		/// Start the worker thread's read. Process any data which comes in synchronously.
		/// </summary>
		/// <returns>async-result of the running read. null if stream ended.</returns>
		private System.IAsyncResult thr_startRead()
		{
		  int numBytes;
		  System.IAsyncResult asy;
			while(true)
			{
				asy = m_inStream.BeginRead(
						m_readBuffer  ,
						(int)m_readBuffer_bytes  ,
						(int)(m_readBuffer.Length-m_readBuffer_bytes)  ,
						null  ,
						null
				);
				if(asy.CompletedSynchronously)
				{
					numBytes = m_inStream.EndRead(asy);
					if(numBytes==0)
						return null;
					m_readBuffer_bytes += (uint)numBytes;
					thr_procInData();
				}else
					break;
			}
			return asy;
		}

		/// <summary>
		/// Try and process bytes from the input buffer and add packets to the receive-queue.
		/// </summary>
		private void thr_procInData()
		{
		  T pck;
		  uint used;
		  uint usedSum;
			usedSum=0;
			while(true)
			{
				try{
					pck = decodeItemFromReadBuffer( m_readBuffer , usedSum , m_readBuffer_bytes-usedSum , out used );
				}catch(InsufficientData)
					{break;}
				if(pck!=null)
				{
					m_recvQueue.Add(pck);
					if(used<1)
						throw new System.Exception("decoder error. Cannot decode packet with zero bytes.");
				}else if( used==0 && usedSum==0 && m_readBuffer_bytes>=m_readBuffer.Length )
				{
					// buffer is all full and still nothing consumed. Problem here.
					throw new System.Exception("Bad packet interpreter or packet too large for input buffer.");
				}
				usedSum += used;
				if(used==0)
					break;
			}
			// now shift up remaining bytes
			if( usedSum<m_readBuffer_bytes )
			{
				if(usedSum>0)
				{
				  uint remain = m_readBuffer_bytes-usedSum;
					if( remain*2 < m_readBuffer_bytes )
					{
						System.Array.Copy( m_readBuffer , usedSum , m_readBuffer , 0 , remain );
					}else{
					  uint i;
						// is there no memmove equivalent???
						for(i=0;i<remain;i++)
						{
							m_readBuffer[i] = m_readBuffer[usedSum+i];
						}
					}
				}
				m_readBuffer_bytes-=usedSum;
			}else
				m_readBuffer_bytes=0;
		}

		/// <summary>
		/// prototype for the method that detects, reads and converts an item from a bytebuffer.
		/// </summary>
		/// <param name="buffer">buffer with raw data.</param>
		/// <param name="start">starting point in buffer.</param>
		/// <param name="maxCount">number of available, valid bytes.</param>
		/// <param name="sizeUsed">out param how many bytes are used up for the item. (or are passed invalid).</param>
		/// <returns>Item or Null. Even with null, sizeUsed my be more than zero (skip garbage).</returns>
		protected abstract T decodeItemFromReadBuffer( byte[] buffer , uint start , uint maxCount , out uint sizeUsed );

		/// <summary>
		/// prototype for the method that places an item into the bytebuffer.
		/// </summary>
		/// <param name="pck">item to encode</param>
		/// <param name="buffer">target buffer.</param>
		/// <param name="?">start offset to place in.</param>
		/// <param name="maxCount">max bytes available. should throw if not enough space.</param>
		/// <returns></returns>
		protected abstract uint encodeItemToSendBuffer( T pck , byte[] buffer , uint start , uint maxCount );

		/// <summary>
		/// Encode a packet to bytes and send on socket.
		/// </summary>
		/// <param name="pck">packet to be encoded.</param>
		/// <returns>returns async-result if write is running. null if data was taken in socket immediately.</returns>
		private System.IAsyncResult thr_encodeAndSendPacket(T pck)
		{
		  System.IAsyncResult asy=null;
		  uint numBytes;
			numBytes = encodeItemToSendBuffer( pck , m_sendBuffer , 0u , (uint)m_sendBuffer.Length );
			if(numBytes>0)
			{
				asy = m_outStream.BeginWrite( m_sendBuffer , 0 , (int)numBytes , null , null );
				if(asy.CompletedSynchronously)
				{
					m_outStream.EndWrite(asy);
					asy=null;
				}
			}
			return asy;
		}

		/// <summary>
		/// This is called whenever the connection is lost. Use to cleanup things and get prepared for calls to tryReconnect().
		/// A connection is considered lost when an IO throws an exception from the list or a read returns 0 bytes.
		/// </summary>
		protected virtual void onLostConnection()
		{
		}

		protected virtual System.Collections.Generic.List<System.Type> getConnectionLossExceptionTypes()
		{
			return new System.Collections.Generic.List<System.Type>();
		}

		private bool connected
		{
			get{return m_connected;}
			set
			{
				if(value!=m_connected)
				{
					m_connected = value;
					if(connect!=null)
						connect(m_connected);
				}
			}
		}

		public event ConnectEventHandler connect;

		private bool m_started;
		private bool m_connected;
		private System.Threading.Thread m_thr;					// worker thread handle
		private System.Threading.EventWaitHandle m_event;		// event. For what? .....
		protected System.Threading.EventWaitHandle m_qevent;		// quit-signal. Set to end worker.
		private bool m_sendAllBeforeQuitting;
		private System.Threading.EventWaitHandle m_doneEvent;	// signal set by worker when exiting.
		protected System.IO.Stream m_inStream;					// stream to send to.
		protected System.IO.Stream m_outStream;					// stream to receive from (might be the same).
		private Queue<T> m_sendQueue;						// queue with packets to send over net.
		private Queue<T> m_recvQueue;						// queue to be filled with incoming packets from net.
		private byte[] m_readBuffer;							// our readbuffer to pass input data.
		private byte[] m_sendBuffer;							// buffer to call packet encoder on.
		private uint m_readBuffer_bytes;						// bytes currently in readBuffer
		private System.Collections.Generic.List<System.Type> m_lossExceptionTypes;			// list of exception types to be interpreted as connection loss (from stream IO)
	}



}
