



namespace GTestCommon.Links
{


	/// <summary>
	/// Class for handling a packet link on TCP, as uplink (listening socket). Derived from TcpPacketLink.
	/// </summary>
	public class TcpPacketUplink : TcpPacketLink
	{

		public TcpPacketUplink()
		 : base()
		{
			m_listenSocket = null;
		}

		public TcpPacketUplink(IPacketConverter fac)
		 : base(fac)
		{
			m_listenSocket = null;
		}

		public TcpPacketUplink(IPacketConverter fac,Queue<IPacket> inQueue)
		 : base(fac,inQueue)
		{
			m_listenSocket = null;
		}

		/// <summary>
		/// Creates listen-socket, opens port, and starts worker thread to process incoming connections.
		/// </summary>
		/// <param name="targetAddress">String representation of the port number. No IP address.</param>
		/// <returns>Returns true if successful.</returns>
		public override bool Start(string targetAddress)
		{
		  ushort port;
			if(!ushort.TryParse(targetAddress,out port))
				return false;
			m_listenSocket = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any,port);
			try{
				m_listenSocket.Start(2);
			}catch(System.Net.Sockets.SocketException)
			{
				return false;		// port invalid or not free.
			}
			return base.Start(string.Format("0.0.0.0:{0}",port));
		}

		public override void Stop(bool sendAll)
		{
			base.Stop(sendAll);
			if(m_listenSocket!=null)
			{
				m_listenSocket.Stop();
			}
			m_listenSocket=null;
		}

		/// <summary>
		/// Try to accept a connection from the listening socket.
		/// </summary>
		/// <returns>Returns the status of the attempt. Enum keys should be self-explanatory.</returns>
		protected override ReCon tryReconnect()
		{
			if(m_socket!=null)
				return ReCon.CONNECTED;		// is already.

			try{
			  System.IAsyncResult asy;
				asy = m_listenSocket.BeginAcceptTcpClient( null , null );
				if( ! asy.CompletedSynchronously )
				{
				  System.Threading.WaitHandle[] handles = new System.Threading.WaitHandle[2];
				  int waitRes;
					handles[0] = asy.AsyncWaitHandle;
					handles[1] = m_qevent;
					waitRes = System.Threading.WaitHandle.WaitAny(handles);
					if( waitRes==1 )
					{
						// quit signal received
						return ReCon.QUITSIGNAL;	// ..... don't know how to abort connect...
					}
				}
				// connect completed
				m_socket = m_listenSocket.EndAcceptTcpClient( asy );
				if( m_socket==null )
				{
					// this should never happen.
					return ReCon.FAILED;
				}
				// get stream object. use same for both.
				m_outStream = m_inStream = m_socket.GetStream();
				return ReCon.CONNECTED;
			}catch(System.Net.Sockets.SocketException)
			{
			}catch(System.IO.IOException)
			{}
			if(m_socket!=null)
				{m_socket.Close();m_socket=null;}
			return ReCon.FAILED;

		}

		private System.Net.Sockets.TcpListener m_listenSocket;

	}

}

