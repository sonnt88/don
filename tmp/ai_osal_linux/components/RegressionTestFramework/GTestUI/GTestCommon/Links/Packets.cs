using System.Collections.Generic;
using GTestCommon;



namespace GTestCommon.Links
{

	//=======================================================================
	public class Packet : IPacket
	{

		public const uint PACKET_HEADER_SIZE = 16;

		public enum PacketID
		{
		// packets adapter -> service
		ID_DOWN_GET_VERSION     = 0,             // content: <none>
		ID_DOWN_GET_STATE       = 1,             // content: <none>
		ID_DOWN_GET_TESTLIST    = 2,             // content: <none>
		ID_DOWN_RUN_TESTS       = 3,             // content: protobuf:TestSelection
		ID_DOWN_ABORT_TESTRUN   = 4,             // content: <none>
		ID_DOWN_GET_ALL_RESULTS = 5,             // content: <none>

		// reply packets service -> adapter
		ID_UP_STATE                 = 0x81,      // content: ubyte:enum GTEST_SERVICE_STATE, [uint32 Iteration, uint32 TestID]
		ID_UP_VERSION               = 0x80,      // content: string
		ID_UP_TESTLIST              = 0x82,      // content: protobuf:AllTestCases
		ID_UP_ACK_TESTRUN           = 0x83,      // content: ubyte: 0=fail, 1=success(test started)
		ID_UP_ABORT                 = 0x84,      // content: <none>
		ID_UP_SEND_ALL_RESULTS_DONE = 0x85,      // content: <none>

		// normal packets service -> adapter
		ID_UP_TESTRESULT      = 0x11,            // content: protobuf:TestResultModel
		ID_UP_REQUEST_POWER   = 0x12,            // content: protobuf:PowerDownRequest
		ID_UP_ALL_TESTS_DONE  = 0x13,            // content: <none>
		ID_UP_TEST_OUTPUT     = 0x14,            // content: protobuf:TestOutput
		ID_UP_PCYCLE_REQUEST  = 0x15,            // content: <none>

		// for testing the software...
		DUMMY_ADDREQUEST         = 0xA9,          // content: two ubytes, values to add.
		DUMMY_ADDRESULT          = 0xAA           // content: one ubyte: result.
		};

		public enum GTEST_SERVICE_STATE
		{
		IDLE = 0,
		RUNNING = 1
		}

		public Packet(PacketID ID)
		{
			m_ID = ID;
			m_flags = 0;
		}

		public virtual uint encode(byte[] buffer,int offset)
		{
			return encode(buffer,offset,0);
		}

		public virtual uint encode(byte[] buffer,int offset,uint payloadsize)
		{
			buffer[offset+0] = (byte)ID;
			buffer[offset+1] = flags;
			buffer[offset+2] = 0;
			buffer[offset+3] = 0;
			System.Array.Copy( System.BitConverter.GetBytes(ByteOrd.h2n(serial))     , 0 , buffer , offset+ 4 , 4 );
			System.Array.Copy( System.BitConverter.GetBytes(ByteOrd.h2n(reqSerial))  , 0 , buffer , offset+ 8 , 4 );
			System.Array.Copy( System.BitConverter.GetBytes(ByteOrd.h2n(payloadsize)), 0 , buffer , offset+ 12 , 4 );
			return (uint)(PACKET_HEADER_SIZE+payloadsize);
		}

		public Packet(byte[] buffer,int offset,uint payloadSize)
		{
		  uint checkSize;
			if( payloadSize<0 )
				throw new System.ArgumentException("need valid data");
			peekIdAndSize( buffer , offset , out m_ID , out checkSize );
			if( checkSize != payloadSize )
				throw new System.ArgumentException("payload size does not fit to this packet.");
			flags = buffer[offset+1];
			serial      = ByteOrd.n2h(System.BitConverter.ToUInt32(buffer,(int)offset+ 4));
			reqSerial   = ByteOrd.n2h(System.BitConverter.ToUInt32(buffer,(int)offset+ 8));
		}

		public static void peekIdAndSize(byte[] buffer,int offset,out PacketID ID,out uint payloadSize)
		{
			ID = (PacketID)buffer[offset];
			payloadSize = ByteOrd.n2h(System.BitConverter.ToUInt32(buffer,offset+12));
		}

		private PacketID m_ID;
		private byte m_flags;
		private uint m_serial;
		private uint m_reqSerial;

		// accessors
		public PacketID ID{get{return m_ID;}}
		public byte flags{get{return m_flags;}set{m_flags=value;}}
		public uint serial{get{return m_serial;}set{m_serial=value;}}
		public uint reqSerial{get{return m_reqSerial;}set{m_reqSerial=value;}}
	}


	//=======================================================================
	public class PacketGetVersion : Packet
	{
		public PacketGetVersion()
		 : base(PacketID.ID_DOWN_GET_VERSION)
		{
		}
		internal PacketGetVersion(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketGetState : Packet
	{
		public PacketGetState()
		 : base(PacketID.ID_DOWN_GET_STATE)
		{
		}
		internal PacketGetState(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketGetTestList : Packet
	{
		public PacketGetTestList()
		 : base(PacketID.ID_DOWN_GET_TESTLIST)
		{
		}
		internal PacketGetTestList(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketRunTests : Packet
	{
		public PacketRunTests(IEnumerable<uint> IDs,bool inv,ulong chk)		// ..... TODO: add data struct.
		 : base(PacketID.ID_DOWN_RUN_TESTS)
		{
			tests = new List<uint>(IDs);
			invertSelection = inv;
			testSetChecksum = chk;
			numRepeats = 1;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream();
		  GTestServiceBuffers.TestLaunch pb;
			// put data in protobuf element
			pb = new GTestServiceBuffers.TestLaunch();
			pb.TestID = new List<uint>(tests);
			pb.Invert = invertSelection;
			pb.NumberOfRepeats = numRepeats;
			pb.Shuffle = shuffleOrder;
			pb.Checksum = testSetChecksum;
			pb.RandomSeek = randomSeed;
			ProtoBuf.Serializer.Serialize<GTestServiceBuffers.TestLaunch>( ms , pb );
			ms.Position=0;
		  uint res = base.encode(buffer,offset,(uint)ms.Length);
			System.Array.Copy( ms.ToArray() , 0 , buffer , offset+PACKET_HEADER_SIZE , ms.Length );
			return res;
		}
		internal PacketRunTests(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer,(int)(offset+PACKET_HEADER_SIZE),(int)payloadSize);
		  GTestServiceBuffers.TestLaunch pb;
			// deser item
			pb = ProtoBuf.Serializer.Deserialize<GTestServiceBuffers.TestLaunch>( ms );
			if( pb.TestID != null )
				tests = new List<uint>(pb.TestID);
			else
				tests = new List<uint>();
			invertSelection = pb.Invert;
			numRepeats = pb.NumberOfRepeats;
			shuffleOrder = pb.Shuffle;
			testSetChecksum = pb.Checksum;
			randomSeed = pb.RandomSeek;
		}

		public List<uint> tests;
		public bool invertSelection;
		public uint numRepeats;
		public bool shuffleOrder;
		public ulong testSetChecksum;
		public uint randomSeed;
	}

	//=======================================================================
	public class PacketAbortTestsReq : Packet
	{
		public PacketAbortTestsReq()
		 : base(PacketID.ID_DOWN_ABORT_TESTRUN)
		{
		}
		internal PacketAbortTestsReq(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketGetAllResults : Packet
	{
		public PacketGetAllResults()
		 : base(PacketID.ID_DOWN_GET_ALL_RESULTS)
		{
		}
		internal PacketGetAllResults(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketState : Packet
	{
		public PacketState(byte state)
		 : base(PacketID.ID_UP_STATE)
		{
			this.state=state;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  bool incAll = (state!=0);
		  uint res = base.encode(buffer,offset,(uint)(1+(incAll?8:0)));
			buffer[offset+PACKET_HEADER_SIZE] = state;
			if(incAll)
			{
				System.Array.Copy( System.BitConverter.GetBytes(ByteOrd.h2n(iteration)) , 0 , buffer , offset+PACKET_HEADER_SIZE+1   , 4 );
				System.Array.Copy( System.BitConverter.GetBytes(ByteOrd.h2n(testID))    , 0 , buffer , offset+PACKET_HEADER_SIZE+1+4 , 4 );
			}
			return res;
		}
		internal PacketState(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
			if(payloadSize<1)
				throw new System.Exception("Need minimum 1 byte of payload.");
			state = buffer[offset+PACKET_HEADER_SIZE];
			if(state!=0)
			{
				if(payloadSize<1+8)
					throw new System.Exception("With active state, need 9 bytes of payload.");
				iteration = ByteOrd.n2h(System.BitConverter.ToUInt32(buffer,(int)(offset+PACKET_HEADER_SIZE+1)));
				testID    = ByteOrd.n2h(System.BitConverter.ToUInt32(buffer,(int)(offset+PACKET_HEADER_SIZE+1+4)));
			}
		}
		public byte state;
		public uint iteration;
		public uint testID;
	}

	//=======================================================================
	public class PacketVersion : Packet
	{
		public PacketVersion(string version)
		 : base(PacketID.ID_UP_VERSION)
		{
			this.version=version;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  byte[] t2b = System.Text.Encoding.ASCII.GetBytes(version);
		  uint res = base.encode(buffer,offset,(uint)t2b.Length);
			System.Array.Copy( t2b , 0 , buffer , offset+PACKET_HEADER_SIZE , t2b.Length );
			return res;
		}
		internal PacketVersion(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
			version = System.Text.Encoding.ASCII.GetString( buffer , (int)(offset+PACKET_HEADER_SIZE) , (int)(payloadSize) );
		}

		public string version;
	}

	//=======================================================================
	public class PacketTestList : Packet
	{
		public PacketTestList()
		 : base(PacketID.ID_UP_TESTLIST)
		{
			data = null;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream();
		  GTestServiceBuffers.AllTestCases pb;
			// put data in protobuf element
			pb = PacketFactory4service.convert_AllTestCases( data );
			ProtoBuf.Serializer.Serialize<GTestServiceBuffers.AllTestCases>( ms , pb );
			ms.Position=0;
		  uint res = base.encode(buffer,offset,(uint)ms.Length);
			System.Array.Copy( ms.ToArray() , 0 , buffer , offset+PACKET_HEADER_SIZE , ms.Length );
			return res;
		}
		internal PacketTestList(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer,(int)(offset+PACKET_HEADER_SIZE),(int)payloadSize);
		  GTestServiceBuffers.AllTestCases pb;
			// deser item
			pb = ProtoBuf.Serializer.Deserialize<GTestServiceBuffers.AllTestCases>( ms );
			data = PacketFactory4service.convert_AllTestCases( pb );
		}
		public Models.AllTestCases data;
	}

	//=======================================================================
	public class PacketAckTestRun : Packet
	{
		public PacketAckTestRun(bool testStarted)
		 : base(PacketID.ID_UP_ACK_TESTRUN)
		{
			this.successfulTestStart=testStarted;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  uint res = base.encode(buffer,offset,1);
			buffer[offset+PACKET_HEADER_SIZE] = (byte)( successfulTestStart ? 1 : 0 );
			return res;
		}
		internal PacketAckTestRun(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
			if(payloadSize<1)
				throw new System.Exception("Need 1 byte of payload.");
			successfulTestStart = (buffer[offset+PACKET_HEADER_SIZE]!=0);
		}
		public bool successfulTestStart;
	}

	//=======================================================================
	public class PacketAbortAck : Packet
	{
		public PacketAbortAck()
		 : base(PacketID.ID_UP_ABORT)
		{
		}
		internal PacketAbortAck(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketSendAllResultsDone : Packet
	{
		public PacketSendAllResultsDone()
		 : base(PacketID.ID_UP_SEND_ALL_RESULTS_DONE)
		{
		}
		internal PacketSendAllResultsDone(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketTestResult : Packet
	{
		public PacketTestResult()
		 : base(PacketID.ID_UP_TESTRESULT)
		{
			result = null;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream();
		  GTestServiceBuffers.TestResult tr;
			// put data in protobuf element
			tr = PacketFactory4service.convert_TestResult( result );
			ProtoBuf.Serializer.Serialize<GTestServiceBuffers.TestResult>( ms , tr );
			ms.Position=0;
		  uint res = base.encode(buffer,offset,(uint)ms.Length);
			System.Array.Copy( ms.ToArray() , 0 , buffer , offset+PACKET_HEADER_SIZE , ms.Length );
			return res;
		}
		internal PacketTestResult(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer,(int)(offset+PACKET_HEADER_SIZE),(int)payloadSize);
		  GTestServiceBuffers.TestResult tr;
			// deser item
			tr = ProtoBuf.Serializer.Deserialize<GTestServiceBuffers.TestResult>( ms );
			result = PacketFactory4service.convert_TestResult( tr );
		}
		public Models.TestResultModel result;
	}

	//=======================================================================
	public class PacketReqPower : Packet
	{
		public PacketReqPower(uint minTim,uint maxTim)
		 : base(PacketID.ID_UP_REQUEST_POWER)
		{
			this.minTime = minTim;
			this.minTime = minTim;
		}
		public PacketReqPower()
		 : this(0u,0u)
		{
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream();
		  GTestServiceBuffers.PowerDownRequest pdr;
			// put data in protobuf element
			pdr = new GTestCommon.GTestServiceBuffers.PowerDownRequest();
			pdr.minTime = minTime;
			pdr.maxTime = maxTime;
			ProtoBuf.Serializer.Serialize<GTestServiceBuffers.PowerDownRequest>( ms , pdr );
			ms.Position=0;
		  uint res = base.encode(buffer,offset,(uint)ms.Length);
			System.Array.Copy( ms.ToArray() , 0 , buffer , offset+PACKET_HEADER_SIZE , ms.Length );
			return res;
		}
		internal PacketReqPower(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer,(int)(offset+PACKET_HEADER_SIZE),(int)payloadSize);
		  GTestServiceBuffers.PowerDownRequest pdr;
			// deser item
			pdr = ProtoBuf.Serializer.Deserialize<GTestServiceBuffers.PowerDownRequest>( ms );
			minTime = pdr.minTime;
			maxTime = pdr.maxTime;
		}
		public uint minTime;
		public uint maxTime;
	}

	//=======================================================================
	public class PacketAllTestrunsDone : Packet
	{
		public PacketAllTestrunsDone()
		 : base(PacketID.ID_UP_ALL_TESTS_DONE)
		{
		}
		internal PacketAllTestrunsDone(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketUpTestOutput : Packet
	{
		public PacketUpTestOutput()
		 : base(PacketID.ID_UP_TEST_OUTPUT)
		{
			timeStamp = 0xFFFFFFFF;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream();
		  GTestServiceBuffers.TestOutput tr = new GTestServiceBuffers.TestOutput();
			// put data in protobuf element
			tr.Output = this.output;
			tr.Timestamp = this.timeStamp;
			switch(this.source)
			{
			case OutputSource.STDOUT:
				tr.Source = GTestServiceBuffers.OutputSource.STDOUT;
				break;
			case OutputSource.STDERR:
				tr.Source = GTestServiceBuffers.OutputSource.STDERR;
				break;
			default:
				throw new System.IndexOutOfRangeException("Invalid output source.");
			}
			ProtoBuf.Serializer.Serialize<GTestServiceBuffers.TestOutput>( ms , tr );
			ms.Position=0;
		  uint res = base.encode(buffer,offset,(uint)ms.Length);
			System.Array.Copy( ms.ToArray() , 0 , buffer , offset+PACKET_HEADER_SIZE , ms.Length );
			return res;
		}
		internal PacketUpTestOutput(byte[] buffer, int offset, uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
		  System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer,(int)(offset+PACKET_HEADER_SIZE),(int)payloadSize);
		  GTestServiceBuffers.TestOutput tr;
			// deser item
			tr = ProtoBuf.Serializer.Deserialize<GTestServiceBuffers.TestOutput>( ms );
			this.output = tr.Output;
			this.timeStamp = tr.Timestamp;
			switch(tr.Source)
			{
			case GTestServiceBuffers.OutputSource.STDOUT:
				this.source = OutputSource.STDOUT;
				break;
			case GTestServiceBuffers.OutputSource.STDERR:
				this.source = OutputSource.STDERR;
				break;
			default:
				throw new System.IndexOutOfRangeException("Invalid output source.");
			}
		}

		public enum OutputSource
		{
			STDOUT = 1,
			STDERR = 2
		};

		public string output;
		public uint timeStamp;
		public OutputSource source;
	}

	//=======================================================================
	public class PacketPcycleRequest : Packet
	{
		public PacketPcycleRequest()
		 : base(PacketID.ID_UP_PCYCLE_REQUEST)
		{
		}
		internal PacketPcycleRequest(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{}
	}

	//=======================================================================
	public class PacketAddNumbers : Packet
	{
		public PacketAddNumbers(byte a,byte b)
		 : base(PacketID.DUMMY_ADDREQUEST)
		{
			this.a=a;this.b=b;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  uint res = base.encode(buffer,offset,2);
			buffer[offset+PACKET_HEADER_SIZE] = a;
			buffer[offset+PACKET_HEADER_SIZE+1] = b;
			return res;
		}
		internal PacketAddNumbers(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
			if(payloadSize<2)
				throw new System.Exception("Need 2 bytes of payload.");
			a=buffer[offset+PACKET_HEADER_SIZE];
			b=buffer[offset+PACKET_HEADER_SIZE+1];
		}

		public byte a,b;
	}


	//=======================================================================
	public class PacketResultNumber : Packet
	{
		public PacketResultNumber(byte c)
		 : base(PacketID.DUMMY_ADDRESULT)
		{
			this.c=c;
		}
		public override uint encode(byte[] buffer,int offset)
		{
		  uint res = base.encode(buffer,offset,1);
			buffer[offset+PACKET_HEADER_SIZE] = c;
			return res;
		}
		internal PacketResultNumber(byte[] buffer,int offset,uint payloadSize)
		 : base(buffer,offset,payloadSize)
		{
			if(payloadSize<1)
				throw new System.Exception("Need 1 byte of payload.");
			c = buffer[offset+PACKET_HEADER_SIZE];
		}

		public byte c;



	}



}
