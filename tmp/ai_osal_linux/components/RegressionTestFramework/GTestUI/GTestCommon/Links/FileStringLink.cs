

namespace GTestCommon.Links
{


	public class FileStringLink : QueuedStringsLink
	{
		public FileStringLink()
		 : base(2048)
		{
		}

		public FileStringLink(Queue<string> inQueue)
		 : base(2048,inQueue)
		{
		}

		public override bool Start(string targetFileName)
		{
		  bool res;
		  System.IO.FileStream fstr;
			if(bIsStarted())
				return true;		// tolerate double call to Start() ...

			inStream = new GTestCommon.misc.DummyStream(); // System.IO.Stream.Null;
			fstr = new System.IO.FileStream(targetFileName, System.IO.FileMode.OpenOrCreate);
			outStream = fstr;
			fstr.Seek(0,System.IO.SeekOrigin.End);	// append.

			res = base.Start(null);

			return res;
		}

		public override void Stop(bool sendAll)
		{
			if(!bIsStarted())
				return;
			base.Stop(sendAll);
			inStream.Close();
			// close our file.
			outStream.Close();
			inStream=null;
			outStream=null;
		}

		protected override System.Collections.Generic.List<System.Type> getConnectionLossExceptionTypes()
		{
		  System.Collections.Generic.List<System.Type> res;
			res = base.getConnectionLossExceptionTypes();
			res.Add(typeof(System.IO.IOException));
			return res;
		}

	}


}
