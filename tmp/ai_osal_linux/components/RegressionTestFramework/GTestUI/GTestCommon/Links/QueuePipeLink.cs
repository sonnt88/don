



namespace GTestCommon.Links
{

	/// <summary>
	/// .Object with two IQueuedPacketLink ends to be used as pipe in tests.
	/// One end found on local interface, other end retrieved with GetOtherEnd().
	/// </summary>
	public class QueuePipeLink<T> : IQueuedPacketLink<T>
	{
		public QueuePipeLink()
		 : this( new Queue<T>() )
		{
		}

		public QueuePipeLink(Queue<T> inQueue)
		{
			m_q = inQueue;
			m_started = false;
			m_mtx = new System.Threading.Mutex();
			m_peer = new QueuePipeLink<T>(this);
		}

		private QueuePipeLink(QueuePipeLink<T> peer)
		{
			m_q = new Queue<T>();
			m_started = false;
			m_mtx = peer.m_mtx;
			m_peer = peer;
		}

		public void Dispose()
		{
		}

		public Queue<T> inQueue
		{
			get{return m_q;}
		}
		public Queue<T> outQueue
		{
			get{return m_peer.inQueue;}
		}

		public IQueuedPacketLink<T> OtherEnd()
		{
			return m_peer;
		}

		public bool Start(string dummyAddress)
		{
		  bool sig;
			if(m_started)return true;
			m_mtx.WaitOne();
			m_started = true;
			sig = m_peer.bIsStarted() && m_peer.connect!=null ;
			m_mtx.ReleaseMutex();
			if( sig )
				m_peer.connect.Invoke(true);
			return true;
		}

		public void Stop(bool sendAll)
		{
		  bool sig;
			if(!m_started)return;
			m_mtx.WaitOne();
			m_started = false;
			sig = m_peer.bIsStarted() && m_peer.connect!=null ;
			m_mtx.ReleaseMutex();
			if( sig )
				m_peer.connect.Invoke(false);
		}

		public bool bIsStarted()
		{
			return m_started;
		}

		public bool bIsConnected()
		{
			return m_started&&m_peer.m_started;
		}

		private bool m_started;
		private Queue<T> m_q;	// queue for packets to Get() here. other direction is in m_peer.
		private QueuePipeLink<T> m_peer;
		public event ConnectEventHandler connect;
		private System.Threading.Mutex m_mtx;	// same mutex shared by both ends.
	}
}

