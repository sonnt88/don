


namespace GTestCommon.Links
{
	/// <summary>
	/// Keep link to the GTest-Service. Will send and receive packets. Will auto-reconnect TCP stream when necessary.
	/// </summary>
	public class TcpPacketLink : QueuedItemsLink<IPacket>
	{
		public TcpPacketLink()
		 : this( new PacketFactory4service() )
		{
		}

		public TcpPacketLink( IPacketConverter fac )
		 : this( fac , new Queue<IPacket>() )
		{
		}

		public TcpPacketLink( IPacketConverter fac , Queue<IPacket> inQueue )
		 : base(inQueue)
		{
			m_IP = null;
			m_port=0;
			m_packetCodec = fac;
			m_socket=null;
			m_sendSer = 1;
		}

		/// <summary>
		/// Starts the internal worker thread which then tries to connect.
		/// </summary>
		/// <param name="targetAddress">numeric IPv4 address with port.</param>
		/// <returns>returns true if successful.</returns>
		public override bool Start(string targetAddress)
		{
			if(bIsStarted())
				throw new System.Exception("is already connected");
			if(!parseAddress(targetAddress,0))
				return false;
			m_socket=null;
			m_inStream=m_outStream=null;
			return base.Start(null);
		}

		/// <summary>
		/// Request this object to stop the connection. Will close socket and terminate worker thread.
		/// </summary>
		public override void Stop(bool sendAll)
		{
			if(!bIsStarted())
				return;
			base.Stop(sendAll);
			if(m_inStream!=null)
			{
				m_inStream.Close();
				m_inStream.Dispose();
			}
			if(m_socket!=null)
				m_socket.Close();
			m_inStream = null;
			m_outStream = null;
			m_socket = null;
		}

		/// <summary>
		/// This function parses a numeric IP address and sets the memver vars. Input like: "192.168.0.4:2000"
		/// </summary>
		/// <param name="to">IP address as string: e.g.   "192.168.0.4:2000"</param>
		/// <param name="defaultPort">port to assume when string does not contain a port. (':2000')</param>
		/// <returns>boolean: true if success.</returns>
		private bool parseAddress(string to,ushort defaultPort)
		{
		  string[] adr;
		  ushort port;
			adr = to.Split(new char[]{':'});
			port = defaultPort;
			if( adr.Length>=2 )
				if(!ushort.TryParse(adr[1],out port))
					return false;
			if( port==0 )
				return false;
			m_address = adr[0];
			m_IP = null;
			m_port = port;
			// now resolve			..... TODO: This can block some seconds. DNS is a network activity.
			if(!resolveAddress())
				return false;
			// done.
			return true;
		}

		/// <summary>
		/// Tries to reconnect the TCP link. Returns true if successful. False if quit signal received.
		/// </summary>
		/// <returns>True for connect success, False for quitsignal received.</returns>
		protected override ReCon tryReconnect()
		{
			if(m_socket!=null)
				return ReCon.CONNECTED;		// is already.
			if( m_IP==null )
			{
				if(!resolveAddress())
					throw new System.Exception("unable to resolve address "+m_address);
			}
			try{
			  System.IAsyncResult asy;
				m_socket = new System.Net.Sockets.TcpClient();
				try{
				// set options.
//					m_socket.Client.SetSocketOption( System.Net.Sockets.SocketOptionLevel.Tcp , System.Net.Sockets.SocketOptionName.KeepAlive , true );
				  byte[] ioCtlBytes = new byte[12];
				  ulong tcp_onoff = 1;	 // values of struct tcp_keepalive
				  ulong tcp_keepalivetime     = 2000;	// after two sec of idle, start sending keep-alive
				  ulong tcp_keepaliveinterval = 1000;	// if no response, resend this fast.
				  int res;
					System.Array.Copy( System.BitConverter.GetBytes(tcp_onoff)             , 0 , ioCtlBytes , 0 , 4 );
					System.Array.Copy( System.BitConverter.GetBytes(tcp_keepalivetime)     , 0 , ioCtlBytes , 4 , 4 );
					System.Array.Copy( System.BitConverter.GetBytes(tcp_keepaliveinterval) , 0 , ioCtlBytes , 8 , 4 );
					res = m_socket.Client.IOControl( System.Net.Sockets.IOControlCode.KeepAliveValues , ioCtlBytes , null );
				}catch(System.Net.Sockets.SocketException ex)
				{
					ex=ex;
					System.Console.Out.WriteLine("(DEBUG) cannot set socket keepalive.");
				}
				System.Console.Out.WriteLine("(DEBUG) trying to connect.");
				asy = m_socket.BeginConnect( m_IP , m_port , null , null );
				if( ! asy.CompletedSynchronously )
				{
				  System.Threading.WaitHandle[] handles = new System.Threading.WaitHandle[2];
				  int waitRes;
					handles[0] = asy.AsyncWaitHandle;
					handles[1] = m_qevent;
					waitRes = System.Threading.WaitHandle.WaitAny(handles);
					if( waitRes==1 )
					{
						// quit signal received
						m_socket.Close();m_socket=null;
						return ReCon.QUITSIGNAL;
					}
				}
				// connect completed
				m_socket.EndConnect( asy );
				// get stream object. use same for both.
				m_outStream = m_inStream = m_socket.GetStream();
				return ReCon.CONNECTED;
			}catch(System.Net.Sockets.SocketException ex)
			{
				ex=ex;
			}catch(System.IO.IOException ex)
			{
				ex=ex;
			}
			System.Console.Out.WriteLine("(DEBUG) connect attempt failed.");
			m_socket.Close();m_socket=null;
			return ReCon.FAILED;

		}

		protected override IPacket decodeItemFromReadBuffer( byte[] buffer , uint start , uint maxCount , out uint sizeUsed )
		{
		  IPacket pck;
			pck = m_packetCodec.decodePacket( buffer , start , maxCount , out sizeUsed );
			return pck;
		}

		protected override uint encodeItemToSendBuffer( IPacket pck , byte[] buffer , uint start , uint maxCount )
		{
			pck.serial = m_sendSer++;
			return m_packetCodec.encodePacket( pck , buffer , start , maxCount );
		}

		protected override void onLostConnection()
		{
			if(m_inStream!=null)
			{
				m_inStream.Close();
				m_inStream.Dispose();
			}
			if(m_socket!=null)
			{
				m_socket.Close();
				m_socket = null;
			}
			m_inStream = m_outStream = null;
		}

		protected override System.Collections.Generic.List<System.Type> getConnectionLossExceptionTypes()
		{
		  System.Collections.Generic.List<System.Type> res;
			res = base.getConnectionLossExceptionTypes();
			res.Add( typeof(System.IO.IOException) );
			res.Add( typeof(System.Net.Sockets.SocketException) );
			return res;
		}

		private bool resolveAddress()
		{
			// numerical address?
			if(resolveAddress_IPv4())
				return true;
			// name?
		  System.Net.IPAddress[] adr;
			try{
				adr = System.Net.Dns.GetHostAddresses(m_address);
			}catch( System.Net.Sockets.SocketException )
				{return false;}
			// can this return several? use first...
			if( adr.Length<1 )
				return false;
			m_IP = adr[0];
			return true;
		}

		private bool resolveAddress_IPv4()
		{
		  string[] parts = m_address.Split('.');
			if( parts.Length!=4 )return false;
		  byte[] vals = new byte[4];
			for( int i=0 ; i<4 ; i++ )
			{
				try{
				  ushort v = System.Convert.ToUInt16(parts[i]);
					if(v>255)throw new System.OverflowException();
					vals[i]=(byte)v;
				}catch(System.FormatException)
					{return false;}
				catch(System.OverflowException)
					{return false;}
			}
			m_IP = new System.Net.IPAddress(vals);
			return true;
		}

		private string m_address;								// string name to connect to.
		private System.Net.IPAddress m_IP;						// IP address to connect to
		private ushort m_port;									//   and port
		protected System.Net.Sockets.TcpClient m_socket;			// our socket handle while open. Null if currently not connected.
		private uint m_sendSer;									// serial number of next packet.
		private IPacketConverter m_packetCodec;					// factory for packets.
	}



}