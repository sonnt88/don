


namespace GTestCommon.Links
{

	/// <summary>
	/// Base class for a link using a Queue<string> and a stream which does not need reconnecting during run.
	/// Derived class must only override Start() and create the m_stream object prior to calling base.Start().
	/// </summary>
	public class QueuedStringsLink : QueuedItemsLink<string>
	{

		protected QueuedStringsLink(uint bufferSize)
		 : this( bufferSize , new Queue<string>() )
		{
		}

		protected QueuedStringsLink(uint bufferSize,Queue<string> inQueue)
		{
			if(bufferSize<=0)
				bufferSize = base.defaultBufferSize;
			m_bufSize = bufferSize;
		}

		protected override uint defaultBufferSize{get{return m_bufSize;}}

		/// <summary>
		/// Starts the worker thread after the superclass has created the stream object.
		/// </summary>
		/// <param name="dummyTarget">Dummy value. Only useful in superclass.</param>
		/// <returns>True if started.</returns>
		public override bool Start(string dummyTarget)
		{
			if(bIsStarted())
				throw new System.Exception("is already connected");
			if( m_inStream==null || m_outStream==null )
				throw new System.Exception("Superclass should have set up the stream objects.");

			return base.Start(null);
		}

		public void WriteLine(string line)
		{
			outQueue.Add(line);
		}

		public void WriteLine(string format,params object[] arg)
		{
			outQueue.Add(string.Format(format,arg));
		}

		protected System.IO.Stream inStream
		{
			get{return m_inStream;}
			set{
				if(bIsStarted())
					throw new System.Exception("Cannot set streams while started.");
				m_inStream = value;
			}
		}

		protected System.IO.Stream outStream
		{
			get{return m_outStream;}
			set{
				if(bIsStarted())
					throw new System.Exception("Cannot set streams while started.");
				m_outStream = value;
			}
		}

		protected override string decodeItemFromReadBuffer( byte[] buffer , uint start , uint maxCount , out uint sizeUsed )
		{
		  int i;
			sizeUsed = 0;
			if( m_skipCR && maxCount>0 && buffer[start]==10 )
				{sizeUsed++;start++;maxCount--;m_skipCR=false;}
			for( i=0 ; i<maxCount ; i++ )
			{
			  byte b = buffer[start+i];
				if( b==10 || b==13 )
				{
					// found line end.
					if(b==13)m_skipCR=true;
				  int oneOff=0;
					if( i>0 && buffer[start+i-1]==13 )
						oneOff--;		// remove the LF.
				  string line = System.Text.Encoding.UTF8.GetString( buffer , (int)start , i+oneOff );
					sizeUsed += (uint)(i+1);
					return line;
				}else{
					m_skipCR=false;
				}
			}
			return null;
		}

		protected override uint encodeItemToSendBuffer( string pck , byte[] buffer , uint start , uint maxCount )
		{
			if( pck.Length>=maxCount )
				pck=pck.Substring( 0 , (int)(maxCount-1) );		// ..... ugly. Send in parts?
		  uint numBytes;
			numBytes = (uint)System.Text.Encoding.UTF8.GetBytes( pck , 0 , pck.Length , buffer , (int)start );
			buffer[start+numBytes] = 10;		// add CR
			return numBytes+1;
		}



		private uint m_bufSize;
		private bool m_skipCR;
	}

}

