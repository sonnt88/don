using System.Collections.Generic;



namespace GTestCommon.Links
{


/// <summary>
/// A TextWriter class which writes output to a QueuedStringsLink.
/// </summary>
public class StringQueueWriter : System.IO.TextWriter
{
	public StringQueueWriter(QueuedStringsLink link)
	{
		m_qLink = link;
		m_bufCnt = 0;
		m_buf = new char[1024];
	}

	public override System.Text.Encoding Encoding{get{return System.Text.Encoding.UTF8;}}

	public override void Write(char value)
	{
		if(m_bufCnt>=m_buf.Length)
		{
			// realloc buffer
		  char[] newBuf = new char[m_buf.Length*2];
			System.Array.Copy( m_buf , 0 , newBuf , 0 , m_bufCnt );
			m_buf = newBuf;
		}
		// add item.
		m_buf[m_bufCnt++]=value;
		// line done?
		if(value==10)
		{
		  int len = (int)(m_bufCnt-1);
			if( len>0 && m_buf[len-1]==13 )
				len--;
			m_qLink.outQueue.Add(new string(m_buf,0,len));
			m_bufCnt=0;
		}
	}

	public override void WriteLine(string value)
	{
		if(m_bufCnt>0)
		{
			value = (new string(m_buf,0,(int)m_bufCnt))+value;
			m_bufCnt=0;
		}
	  string[] parts = value.Split('\n');
		foreach( string part in parts )
		{
			if( part.EndsWith("\r") )
				m_qLink.outQueue.Add( part.Substring(0,part.Length-1) );
			else
				m_qLink.outQueue.Add( part );
		}
	}

	private QueuedStringsLink m_qLink;

	private char[] m_buf;
	private uint m_bufCnt;
}

}
