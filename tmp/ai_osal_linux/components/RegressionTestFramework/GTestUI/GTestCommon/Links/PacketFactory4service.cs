



using System.Collections.Generic;
using GTestCommon;



namespace GTestCommon.Links
{

	//=======================================================================
	public class PacketFactory4service : IPacketConverter
	{

		public const uint PACKET_HEADER_SIZE = Packet.PACKET_HEADER_SIZE;

		public PacketFactory4service()
		{
		}

		public uint encodePacket(IPacket pck,byte[] buffer,uint start,uint maxCount)
		{
		  uint res;
		  Packet p = (Packet)pck;

			//System.Console.Out.WriteLine(">>out " + pck.GetType().FullName);
			if( pck is GTestCommon.Links.PacketPcycleRequest )
			{
				pck = pck;
			}
			res = p.encode(buffer, (int)start);
			return res;
		}

		public IPacket decodePacket(byte[] buffer,uint start,uint count,out uint skipData)
		{
		  Packet pck;
		  Packet.PacketID ID;
		  uint payloadSize;
			if(count<PACKET_HEADER_SIZE)
				{skipData=0;return null;}		// not enough for any packet type. Come back later.
			Packet.peekIdAndSize( buffer , (int)start , out ID , out payloadSize );
			if( payloadSize > 0x10000 )
			{
				// ..... TODO: just put message and go on?
				throw new System.Exception("packet payload too large...");
			}
			if( PACKET_HEADER_SIZE+payloadSize > count )
				{skipData=0;return null;}			// wait for more.
			// create instance
			pck=null;
			// TODO: ..... try-catch this call to catch protobuf-messages? And output to some error channel...
			switch(ID)
			{
			case Packet.PacketID.ID_DOWN_GET_VERSION:
				pck = new PacketGetVersion(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_DOWN_GET_STATE:
				pck = new PacketGetState(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_DOWN_GET_TESTLIST:
				pck = new PacketGetTestList(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_DOWN_RUN_TESTS:
				pck = new PacketRunTests(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_DOWN_ABORT_TESTRUN:
				pck = new  PacketAbortTestsReq(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_DOWN_GET_ALL_RESULTS:
				pck = new  PacketGetAllResults(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_STATE:
				pck = new PacketState(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_VERSION:
				pck = new PacketVersion(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_TESTLIST:
				pck = new PacketTestList(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_ACK_TESTRUN:
				pck = new PacketAckTestRun(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_ABORT:
				pck = new PacketAbortAck(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_SEND_ALL_RESULTS_DONE:
				pck = new PacketSendAllResultsDone(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_TESTRESULT:
				pck = new PacketTestResult(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_REQUEST_POWER:
				pck = new PacketReqPower(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_ALL_TESTS_DONE:
				pck = new PacketAllTestrunsDone(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.ID_UP_TEST_OUTPUT:
				pck = new PacketUpTestOutput(buffer, (int)start, payloadSize);
				break;
			case Packet.PacketID.ID_UP_PCYCLE_REQUEST:
				pck = new PacketPcycleRequest(buffer, (int)start, payloadSize);
				break;
			case Packet.PacketID.DUMMY_ADDREQUEST:
				pck = new PacketAddNumbers(buffer,(int)start,payloadSize);
				break;
			case Packet.PacketID.DUMMY_ADDRESULT:
				pck = new PacketResultNumber(buffer,(int)start,payloadSize);
				break;
			default:
				pck = new Packet(buffer,(int)start,payloadSize);
				break;
			}
			if(pck.ID!=ID)
				throw new System.Exception("runtime error. Bad packet ID.");
			skipData = PACKET_HEADER_SIZE+payloadSize;
			//System.Console.Out.WriteLine("<<in "+pck.GetType().FullName);
			return pck;
		}

		//=======================================================================
		//    Converter functions for the protobuf stuff.
		//=======================================================================
/*
		static public GTestServiceBuffers.TestSelection convert_TestLaunch(Data.TestLaunch sel)
		{
		  GTestServiceBuffers.TestLaunch res = new GTestServiceBuffers.TestSelection();
			res.TestID = new List<int>();
			foreach( uint tID in sel.testIDs )
				res.TestID.Add((int)tID);
			res.Invert = sel.invert;
			res.Checksum = (long)sel.checksum;
			return res;
		}

		static public Data.TestSelection convert_TestLaunch(GTestServiceBuffers.TestLaunch sel)
		{
		  Data.TestSelection res = new Data.TestLaunch();
			foreach( int tID in sel.TestID )
				res.testIDs.Add((uint)tID);
			res.invert = sel.Invert;
			res.checksum = (ulong)sel.Checksum;
			return res;
		}
*/
		static public GTestServiceBuffers.AllTestCases convert_AllTestCases(Models.AllTestCases data)
		{
		  GTestServiceBuffers.AllTestCases res = new GTestServiceBuffers.AllTestCases();
			res.TestCases = new List<GTestServiceBuffers.TestCase>();
			for( int i=0 ; i<data.TestGroupCount ; i++ )
			{
			  Models.TestGroupModel tc = data[i];
				res.TestCases.Add( convert_TestCase(tc) );
			}
			res.Checksum = data.CheckSum;
			return res;
		}

		static public Models.AllTestCases convert_AllTestCases(GTestServiceBuffers.AllTestCases trm)
		{
		  Models.AllTestCases res = new Models.AllTestCases(false);		// ..... TODO: false?
			if(trm.TestCases!=null)
			{
				foreach( GTestServiceBuffers.TestCase tcm in trm.TestCases )
					convert_TestCase(tcm,res);
			}
			res.CheckSum = (ulong)trm.Checksum;
			res.Repeat = 0;
			return res;
		}

		static public GTestServiceBuffers.TestCase convert_TestCase(Models.TestGroupModel tc)
		{
		  GTestServiceBuffers.TestCase res = new GTestServiceBuffers.TestCase();
			res.Tests = new List<GTestServiceBuffers.Test>();
			for( int i=0 ; i<tc.TestCount ; i++ )
			{
			  Models.Test t = tc[i];
				res.Tests.Add( convert_Test(t) );
			}
			res.Name = tc.Name;
			res.DefaultDisabled = tc.DisabledByDefault;
			return res;
		}

		static public void convert_TestCase(GTestServiceBuffers.TestCase tcm,Models.AllTestCases testSet)
		{
			testSet.AppendNewTestGroup( tcm.Name );
		  Models.TestGroupModel res = testSet[testSet.TestGroupCount-1];
			if(tcm.Tests!=null)
			{
				foreach( GTestServiceBuffers.Test tm in tcm.Tests )
					convert_Test(tm,res);
			}
			// TODO: ..... Cannot port back property DisabledByDefault. It is read-only.
		}

		static public GTestServiceBuffers.Test convert_Test(Models.Test tst)
		{
		  GTestServiceBuffers.Test res = new GTestServiceBuffers.Test();
			res.ID = tst.ID;
			res.Name = tst.Name;
			res.DefaultDisabled = tst.DisabledByDefault;
			res.Enabled = tst.Enabled;
			return res;
		}

		static public Models.Test convert_Test(GTestServiceBuffers.Test tm,Models.TestGroupModel group)
		{
		  Models.Test res;
			if(group!=null)
			{
				group.AppendNewTest( tm.Name , tm.ID );
				res = group[group.TestCount-1];
			}else{
				res = new Models.Test(null,tm.Name,tm.ID);
			}
			// ..... TODO: cannot port back DisabledByDefault: is read-only.
			res.Enabled = tm.Enabled;
			return res;
		}

		static public GTestServiceBuffers.TestResult convert_TestResult(Models.TestResultModel tstres)
		{
		  GTestServiceBuffers.TestResult res = new GTestServiceBuffers.TestResult();
			res.ID = tstres.testID;
			res.Iteration = tstres.iteration;
			res.Success = tstres.success;
			res.Milliseconds = tstres.duration;
			// TODO: ..... not transfer start time?
			return res;
		}

		static public Models.TestResultModel convert_TestResult(GTestServiceBuffers.TestResult tr)
		{
		  Models.TestResultModel res;
			res = new Models.TestResultModel(tr.ID,tr.Success);
			res.iteration = tr.Iteration;
			res.duration = tr.Milliseconds;
			return res;
		}
	}


}
