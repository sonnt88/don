

namespace GTestCommon.Links
{


public delegate void ConnectEventHandler(bool bNowConnected);

/// <summary>
/// Interface of an object which communicates data asynchronously using Queue<> objects for input and output.
/// To wait for incoming data, waitHandle or blocking Get() can be used on the inQueue.
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IQueuedPacketLink<T> : System.IDisposable
{
	Queue<T> inQueue{get;}				// inQueue can be set on some items in C-tor.
	Queue<T> outQueue{get;}				// outQueue is always created by object itself.
	bool Start(string connectTarget);	// start the link. Can connect now (not necessarily instantly)
	void Stop(bool sendAll);			// stop/disconnect the link.
	bool bIsStarted();					// query if Start() was called.
	bool bIsConnected();				// query if the link could establish connection.
	event ConnectEventHandler connect;	// event for getting notifications about connection buildup/loss.
}

}

