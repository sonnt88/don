


namespace GTestCommon.Links
{


	public class LocalConsole : QueuedStringsLink
	{
		public LocalConsole()
		 : base(2048)
		{
		}

		public LocalConsole(Queue<string> inQueue)
		 : base(2048,inQueue)
		{
		}

		public override bool Start(string dummyTarget)
		{
		  bool res;
			if(bIsStarted())
				return true;		// tolerate double call to Start() ...

			inStream = System.Console.OpenStandardInput();
			outStream = System.Console.OpenStandardOutput();

			res = base.Start(null);

			return res;
		}

		public override void Stop(bool sendAll)
		{
			if(!bIsStarted())
				return;
			base.Stop(sendAll);
			inStream.Close();
			outStream.Close();
			inStream=null;
			outStream=null;
		}

		protected override System.Collections.Generic.List<System.Type> getConnectionLossExceptionTypes()
		{
		  System.Collections.Generic.List<System.Type> res;
			res = base.getConnectionLossExceptionTypes();
			res.Add(typeof(System.IO.IOException));
			return res;
		}

	}


}
