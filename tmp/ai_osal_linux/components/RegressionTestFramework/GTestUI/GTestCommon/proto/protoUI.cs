using System.Collections.Generic;
using ProtoBuf;




namespace GTestCommon
{

namespace GTestServiceBuffers
{

	[ProtoContract]
	public enum OutputSource
	{
		STDOUT = 1,
		STDERR = 2
	};

	[ProtoContract]
	public class TestResult
	{
		[ProtoMember(1,IsRequired=true)]
		public uint ID;
		[ProtoMember(2,IsRequired=true)]
		public uint Iteration;
		[ProtoMember(3,IsRequired=true)]
		public bool Success;
		[ProtoMember(4)]
		public uint Milliseconds;
	}

	[ProtoContract]
	public class TestOutput
	{
		[ProtoMember(1,IsRequired=true)]
		public OutputSource Source;
		[ProtoMember(2,IsRequired=true)]
		public string Output;
		[ProtoMember(3,IsRequired=false)]
		public uint Timestamp;
	}

	[ProtoContract]
	public class TestLaunch
	{
		[ProtoMember(1)]
		public List<uint> TestID;
		[ProtoMember(2)]
		public bool Invert;
		[ProtoMember(3)]
		public uint NumberOfRepeats;
		[ProtoMember(4)]
		public bool Shuffle;
		[ProtoMember(5)]
		public uint RandomSeek;
		[ProtoMember(6)]
		public ulong Checksum;
	}

	[ProtoContract]
	public class TestCase
	{
		[ProtoMember(1,IsRequired=true)]
		public string Name;
		[ProtoMember(2,IsRequired=true)]
		public bool DefaultDisabled;
		[ProtoMember(3)]
		public List<Test> Tests;
	}

	[ProtoContract]
	public class AllTestCases
	{
		[ProtoMember(1)]
		public List<TestCase> TestCases;
		[ProtoMember(2)]
		public ulong Checksum;
	}

	[ProtoContract]
	public class Test
	{
		[ProtoMember(1,IsRequired=true)]
		public uint ID;
		[ProtoMember(2)]
		public string Name;
		[ProtoMember(3,IsRequired=true)]
		public bool DefaultDisabled;
		[ProtoMember(4,IsRequired=true)]
		public bool Enabled;
	}

	[ProtoContract]
	public class PowerDownRequest
	{
		[ProtoMember(1,IsRequired=true)]
		public uint minTime;
		[ProtoMember(2,IsRequired=true)]
		public uint maxTime;
	}
}
}


