using System;
using System.Collections.Generic;
using System.Text;
using GTestCommon.Utils.ServiceDiscovery.Models;
using GTestCommon.Utils.ServiceDiscovery.Interfaces;
using System.Net;

namespace GTestCommon.Utils.ServiceDiscovery.GTestDiscovery.GTestAdapterDiscovery
{
	public class GTestAdapterAnnouncementPacketFactory : IServiceAnnouncementPacketFactory 
	{
		private ServiceModel serviceModel;

		public GTestAdapterAnnouncementPacketFactory()
		{
			this.serviceModel = null;
		}

		public GTestAdapterAnnouncementPacketFactory(ServiceModel Service)
		{
			if (Service == null)
				throw new ArgumentNullException("Service cannot be null.");

			this.serviceModel = Service;
		}

		public ServiceDiscoveryPacket CreateDiscoveryPacket()
		{
			return new GTestAdapterDiscoveryPacket();
		}

		public ServiceDiscoveryAnswerPacket CreateAnswerPacket()
		{
			if (this.serviceModel == null)
				throw new InvalidOperationException("Error: in order to be able to create answer packets you have to specify a service model in the construcor of this GTestAdapterAnnouncementPacketFactory.");

			return new GTestAdapterDiscoveryAnswerPacket(this.serviceModel);
		}

		public ServiceDiscoveryAnswerPacket DeserializeAnswerPacket(byte[] Bytes, IPAddress ServiceIP)
		{
			return new GTestAdapterDiscoveryAnswerPacket(Bytes, ServiceIP);
		}

	}
}
