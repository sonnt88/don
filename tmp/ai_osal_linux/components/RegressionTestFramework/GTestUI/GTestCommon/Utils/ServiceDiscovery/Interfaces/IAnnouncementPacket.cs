using System;
using System.Collections.Generic;
using System.Text;

namespace GTestCommon.Utils.ServiceDiscovery.Interfaces
{
	public interface IAnnouncementPacket
	{
		byte[] AsBytes();
		int Length();
	}
}
