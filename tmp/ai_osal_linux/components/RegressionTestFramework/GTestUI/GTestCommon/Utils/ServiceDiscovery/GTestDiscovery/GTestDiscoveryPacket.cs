using System;
using System.Collections.Generic;
using System.Text;

namespace GTestCommon.Utils.ServiceDiscovery.GTestDiscovery
{
	public abstract class GTestDiscoveryPacket : ServiceDiscoveryPacket
	{
		protected string discoveryString;

		public GTestDiscoveryPacket(String DiscoveryString)
		{
			if (DiscoveryString == null)
				throw new ArgumentNullException("DiscoveryString cannot be null.");

			this.discoveryString = DiscoveryString;
		}

		public override byte[] AsBytes()
		{
			return stringToBytes(discoveryString);
		}

		public override int Length()
		{
			return discoveryString.Length + 1;
		}
	}
}
