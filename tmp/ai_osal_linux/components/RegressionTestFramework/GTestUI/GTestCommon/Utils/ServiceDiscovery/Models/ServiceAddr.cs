using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace GTestCommon.Utils.ServiceDiscovery.Models
{
	public class ServiceAddr
	{
		private IPAddress ipAddr;
		private int port;

		public IPAddress IPAddr { get { return ipAddr; } }
		public int Port { get { return port; } }

		public ServiceAddr(IPAddress IPAddr, int Port)
		{
			if (IPAddr == null)
				throw new ArgumentNullException("IPAddr cannot be null.");

			if (Port < 1 || Port > 65535)
				throw new ArgumentException("Port must be in the range of 0 < Port < 65536");

			this.ipAddr = IPAddr;
			this.port = Port;
		}

		public override bool Equals(Object Obj)
		{
			ServiceAddr Other = Obj as ServiceAddr;
			if (Other == null)
				return false;
			else
			{
				if (Other.IPAddr == this.IPAddr && Other.Port == this.Port)
					return true;
				else
					return false;
			}
		}

		public override int GetHashCode()
		{
			return this.IPAddr.GetHashCode() | this.Port.GetHashCode();
		}
	}
}
