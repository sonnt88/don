using System;
using System.Collections.Generic;
using System.Text;

namespace GTestCommon.Utils.ServiceDiscovery.GTestDiscovery.GTestAdapterDiscovery
{
	public class GTestAdapterDiscoveryPacket : GTestDiscoveryPacket
	{
		public GTestAdapterDiscoveryPacket()
			: base("gtest adapter discovery")
		{
		}
	}
}
