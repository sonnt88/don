using System;
using System.Collections.Generic;
using System.Text;
using GTestCommon.Utils.ServiceDiscovery.Models;

namespace GTestCommon.Utils.ServiceDiscovery
{
	public abstract class ServiceDiscoveryAnswerPacket : ServiceAnnouncementPacket
	{
		public abstract ServiceModel GetServiceModel();
	}
}
