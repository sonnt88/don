using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using GTestCommon.Utils.ServiceDiscovery.Interfaces;
using GTestCommon.Utils.ServiceDiscovery.Models;

namespace GTestCommon.Utils.ServiceDiscovery
{
	public class ServiceDiscovery
	{
		private int announcementUDPPort;
		private IServiceAnnouncementPacketFactory announcementPacketFactory;

		public List<ServiceModel> DiscoverServiceProviders(int timeout)
		{
			List<ServiceModel> resultList = new List<ServiceModel>();

			IPEndPoint broadcastEndPoint = new IPEndPoint(IPAddress.Broadcast, announcementUDPPort);
			UdpClient udp = new UdpClient();

			ServiceDiscoveryPacket discoveryPacket = announcementPacketFactory.CreateDiscoveryPacket();
			udp.Send(discoveryPacket.AsBytes(), discoveryPacket.Length(), broadcastEndPoint);

			udp.Client.ReceiveTimeout = 10;

			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			while (stopwatch.ElapsedMilliseconds < timeout)
			{
				try
				{
					IPEndPoint ServiceEndPoint = new IPEndPoint(IPAddress.Any, 0);
					byte[] recvBuffer = udp.Receive(ref ServiceEndPoint);

					ServiceDiscoveryAnswerPacket answerPacket = 
						this.announcementPacketFactory.DeserializeAnswerPacket(recvBuffer, ServiceEndPoint.Address);

					ServiceModel serviceModel = answerPacket.GetServiceModel();

					if (!resultList.Contains(serviceModel))
						resultList.Add(serviceModel);
				}
				catch (Exception) { }
			}

			udp.Close();
			stopwatch.Stop();

			return resultList;
		}

		public ServiceDiscovery(int AnnouncementUDPPort, IServiceAnnouncementPacketFactory AnnouncementPacketFactory)
		{
			if (AnnouncementUDPPort < 1 || AnnouncementUDPPort > 65535)
				throw new ArgumentNullException("AnnouncementUDPPort must be in the range 0 < AnnouncementUDPPort < 65536.");

			if (AnnouncementPacketFactory == null)
				throw new ArgumentNullException("AnnouncementPacketFactory cannot be null.");

			this.announcementUDPPort = AnnouncementUDPPort;
			this.announcementPacketFactory = AnnouncementPacketFactory;
		}
	}
}
