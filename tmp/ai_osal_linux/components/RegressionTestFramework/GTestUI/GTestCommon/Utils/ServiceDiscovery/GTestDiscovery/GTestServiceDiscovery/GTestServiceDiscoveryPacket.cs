using System;
using System.Collections.Generic;
using System.Text;

namespace GTestCommon.Utils.ServiceDiscovery.GTestDiscovery.GTestServiceDiscovery
{
	public class GTestServiceDiscoveryPacket : GTestDiscoveryPacket
	{
		public GTestServiceDiscoveryPacket()
			: base("gtest service discovery")
		{

		}
	}
}
