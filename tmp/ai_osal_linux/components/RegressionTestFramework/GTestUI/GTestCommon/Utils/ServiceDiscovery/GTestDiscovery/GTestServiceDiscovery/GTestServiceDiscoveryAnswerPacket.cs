using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using GTestCommon.Utils.ServiceDiscovery.Models;
using GTestCommon.Utils.ServiceDiscovery.GTestDiscovery;

namespace GTestCommon.Utils.ServiceDiscovery.GTestDiscovery.GTestServiceDiscovery
{
	public class GTestServiceDiscoveryAnswerPacket : GTestDiscoveryAnswerPacket
	{
		public GTestServiceDiscoveryAnswerPacket(ServiceModel Service) 
			: base(Service, "gtest service answer")
		{

		}

		public GTestServiceDiscoveryAnswerPacket(byte[] Bytes, IPAddress ServiceIP)
			: base(Bytes, ServiceIP, "gtest service answer")
		{

		}
	}
}
