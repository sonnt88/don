using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace GTestCommon.Utils.ServiceDiscovery.Models
{
	public class ServiceAnnouncementModel
	{
		private int announcePort;
		public int AnnouncePort { get { return this.announcePort; } }

		private ServiceModel serviceModel;
		public ServiceModel ServiceModel { get { return this.serviceModel; } }

		public ServiceAnnouncementModel(ServiceModel Service, int AnnounceUDPPort)
		{
			if (Service == null)
				throw new ArgumentNullException("Service cannot be null.");

			if (AnnounceUDPPort < 1 || AnnouncePort > 65535)
				throw new ArgumentException("Port be in the range 0 < Port < 65536");

			this.announcePort = AnnounceUDPPort;
			this.serviceModel = Service;
		}
	}

}
