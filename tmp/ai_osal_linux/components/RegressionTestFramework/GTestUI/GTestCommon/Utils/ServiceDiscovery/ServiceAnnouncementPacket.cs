using System;
using System.Collections.Generic;
using System.Text;
using GTestCommon.Utils.ServiceDiscovery.Interfaces;
using GTestCommon.Utils.ServiceDiscovery.GTestDiscovery.GTestServiceDiscovery;

namespace GTestCommon.Utils.ServiceDiscovery
{
	public abstract class ServiceAnnouncementPacket : IAnnouncementPacket
	{
		protected static byte[] stringToBytes(string text)
		{
			System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
			byte[] textAsBytes = enc.GetBytes(text);
			byte[] textAsBytesWithZero = new byte[textAsBytes.Length + 1];
			textAsBytes.CopyTo(textAsBytesWithZero, 0);
			return textAsBytesWithZero;
		}

		public abstract byte[] AsBytes();
		public abstract int Length();

		private bool rawEquals(byte[] rawPacket)
		{
			if (rawPacket.Length != this.Length())
				return false;

			byte[] thisRawPacket = this.AsBytes();

			for (int index = 0; index < this.Length(); index++)
			{
				if (thisRawPacket[index] != rawPacket[index])
					return false;
			}

			return true;
		}

		public override bool Equals(object Obj)
		{
			GTestServiceDiscoveryAnswerPacket packet = Obj as GTestServiceDiscoveryAnswerPacket;
			if (packet != null)
				return this.rawEquals(packet.AsBytes());

			byte[] rawPacket = Obj as byte[];
			if (rawPacket != null)
				return this.rawEquals(rawPacket);

			return false;
		}

		public override int GetHashCode()
		{
			return this.AsBytes().GetHashCode();
		}
	}
}
