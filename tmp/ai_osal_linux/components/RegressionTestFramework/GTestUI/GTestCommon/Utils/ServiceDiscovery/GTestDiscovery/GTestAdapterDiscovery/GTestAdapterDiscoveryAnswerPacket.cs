using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using GTestCommon.Utils.ServiceDiscovery.Models;

namespace GTestCommon.Utils.ServiceDiscovery.GTestDiscovery.GTestAdapterDiscovery
{
	class GTestAdapterDiscoveryAnswerPacket : GTestDiscoveryAnswerPacket
	{
		public GTestAdapterDiscoveryAnswerPacket(ServiceModel Service) 
			: base(Service, "gtest adapter answer")
		{

		}

		public GTestAdapterDiscoveryAnswerPacket(byte[] Bytes, IPAddress ServiceIP)
			: base(Bytes, ServiceIP, "gtest adapter answer")
		{

		}
	}
}
