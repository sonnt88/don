using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace GTestCommon.Utils.ServiceDiscovery.Interfaces
{
	public interface IServiceAnnouncementPacketFactory
	{
		ServiceDiscoveryPacket CreateDiscoveryPacket();
		ServiceDiscoveryAnswerPacket CreateAnswerPacket();
		ServiceDiscoveryAnswerPacket DeserializeAnswerPacket(byte[] Bytes, IPAddress ServiceIP);
	}
}
