using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace GTestCommon.Utils.ServiceDiscovery.Models
{
	public class ServiceModel
	{
		private ServiceAddr serviceAddr;
		public ServiceAddr ServiceAddress { get { return this.serviceAddr; } }

		private String name;
		public String Name { get { return this.name; } }

		public ServiceModel(ServiceAddr ServiceAddress, String Name)
		{
			if (ServiceAddress == null)
				throw new ArgumentNullException("ServiceAddress cannot be null.");

			if (Name == null)
				throw new ArgumentNullException("Name cannot be null.");

			this.name = Name;
			this.serviceAddr = ServiceAddress;
		}

		public override bool Equals(object Obj)
		{
			ServiceModel Other = Obj as ServiceModel;
			if (Other == null)
				return false;

			return (this.ServiceAddress == Other.ServiceAddress);
		}

		public override int GetHashCode()
		{
			return this.ServiceAddress.GetHashCode();
		}
	}
}
