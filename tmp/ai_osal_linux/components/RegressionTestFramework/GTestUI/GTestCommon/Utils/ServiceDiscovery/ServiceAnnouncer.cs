using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using GTestCommon.Utils.ServiceDiscovery.Interfaces;
using GTestCommon.Utils.ServiceDiscovery.Models;

namespace GTestCommon.Utils.ServiceDiscovery
{
	public class ServiceAnnouncer
	{
		private ServiceAnnouncementModel announcementModel;
		private IServiceAnnouncementPacketFactory announcementPacketFactory;

		private bool running;
		private bool cont;

		private Mutex runMutex;
		private Thread runThread;

		private void announceServiceFunc()
		{
			UdpClient udp = new UdpClient();
			udp.Client.Bind(new IPEndPoint(IPAddress.Any, announcementModel.AnnouncePort));

			udp.Client.ReceiveTimeout = 10;
			udp.Client.SendTimeout = 10;

			while (this.cont)
			{
				IAnnouncementPacket discoveryPacket = announcementPacketFactory.CreateDiscoveryPacket();
				IPEndPoint ClientEndPoint = new IPEndPoint(IPAddress.Any, announcementModel.AnnouncePort);

				try
				{
					byte[] recvBuffer = udp.Receive(ref ClientEndPoint);

					if (discoveryPacket.Equals(recvBuffer))
					{
						byte[] answerPacket = this.announcementPacketFactory.CreateAnswerPacket().AsBytes();
						udp.Send(answerPacket, answerPacket.Length, ClientEndPoint);
					}
				}
				catch (SocketException)
				{

				}
			}

			udp.Close();
		}

		public ServiceAnnouncer(ServiceAnnouncementModel AnnouncementData, IServiceAnnouncementPacketFactory AnnouncementPacketFactory)
		{
			if (AnnouncementData == null)
				throw new ArgumentNullException("AnnouncementData cannot be null.");

			if (AnnouncementPacketFactory == null)
				throw new ArgumentNullException("AnnouncementPacketFactory cannot be null.");

			this.announcementModel = AnnouncementData;
			this.announcementPacketFactory = AnnouncementPacketFactory;

			this.runMutex = new Mutex();
			this.cont = true;
			this.running = false;
		}

		public void Run()
		{
			this.runMutex.WaitOne();

			if (this.running)
			{
				this.runMutex.ReleaseMutex();
				throw new System.InvalidOperationException("Service announcer is already running.");
			}

			this.cont = true;

			runThread = new Thread(this.announceServiceFunc);
			runThread.Start();

			this.running = true;

			this.runMutex.ReleaseMutex();
		}

		public void Stop()
		{
			this.runMutex.WaitOne();

			if (!this.running)
			{
				this.runMutex.ReleaseMutex();
				throw new System.InvalidOperationException("Service announcer thread is not running.");
			}

			this.cont = false;

			this.runThread.Join();
			this.running = false;

			this.runMutex.ReleaseMutex();
		}
		
	}
}
