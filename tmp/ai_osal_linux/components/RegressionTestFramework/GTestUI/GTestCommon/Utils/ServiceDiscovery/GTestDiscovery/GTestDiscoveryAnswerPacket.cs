using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using GTestCommon.Utils.ServiceDiscovery.Models;

namespace GTestCommon.Utils.ServiceDiscovery.GTestDiscovery
{
	public abstract class GTestDiscoveryAnswerPacket : ServiceDiscoveryAnswerPacket
	{
		public static readonly int serviceNameLength = 20;

		private ServiceModel service;
		protected string answerString;

		public override byte[] AsBytes()
		{
			System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();

			//copy magic gtest service answer string into packet
			byte[] answerPacket = new byte[this.Length()];
			byte[] answerStringAsBytes = stringToBytes(answerString);
			answerStringAsBytes.CopyTo(answerPacket, 0);

			//copy the name value into the packet
			int bytesToCopy = (this.service.Name.Length < 20) ? this.service.Name.Length : 19;
			enc.GetBytes(this.service.Name, 0, bytesToCopy, answerPacket, answerStringAsBytes.Length);

			//copy the port number into the packet
			byte[] portAsBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(this.service.ServiceAddress.Port));
			portAsBytes.CopyTo(answerPacket, answerPacket.Length - sizeof(Int32));

			return answerPacket;
		}

		public override int Length()
		{
			return answerString.Length + 1 + serviceNameLength + sizeof(Int32);
		}

		public GTestDiscoveryAnswerPacket(ServiceModel Service, String AnswerString)
		{
			if (AnswerString == null)
				throw new ArgumentNullException("AnswerString cannot be null");

			if (Service == null)
				throw new ArgumentNullException("Service cannot be null.");

			this.answerString = AnswerString;
			this.service = Service;
		}

		public GTestDiscoveryAnswerPacket(byte[] Bytes, IPAddress ServiceIP, String AnswerString)
		{
			if (AnswerString == null)
				throw new ArgumentNullException("AnswerString cannot be null");

			this.answerString = AnswerString;

			if (Bytes == null)
				throw new ArgumentNullException("Bytes cannot be null.");

			if (Bytes.Length != this.Length())
				throw new ArgumentException("Bytes argument is not a serialized representation of a GTestServiceDiscoveryAnswerPacket object.");

			System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();

			String tmpString;
			tmpString = enc.GetString(Bytes, 0, answerString.Length + 1);

			if (tmpString.CompareTo(answerString) != 0)
				throw new ArgumentException("Bytes argument is not a serialized representation of a GTestServiceDiscoveryAnswerPacket object.");

			tmpString = enc.GetString(Bytes, answerString.Length + 1, serviceNameLength);
			int ServicePort = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(Bytes, Bytes.Length - sizeof(Int32)));

			if (ServicePort < 1 || ServicePort > 65535)
				throw new ArgumentException("Illegal port number in byte representation of GTestServiceDiscoveryAnswerPacket object.");

			this.service = new ServiceModel(new ServiceAddr(ServiceIP, ServicePort), tmpString);
		}

		public override ServiceModel GetServiceModel()
		{
			return this.service;
		}
	}
}
