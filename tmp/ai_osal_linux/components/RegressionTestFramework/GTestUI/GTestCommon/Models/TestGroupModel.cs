using System;
using System.Collections.Generic;
using System.Text;

namespace GTestCommon.Models
{
    public enum TESTGROUPSELECTION
    {
        NONE,
        PARTLY,
        ALL_BUT_DEFAULT_DISABLED,
        ALL
    }

    public delegate void TestGroupEventHandler(TestGroupModel sender);
    public delegate void ExtTestGroupEventHandler(TestGroupModel sender, Test testModel);

    /// <summary>
    /// Data class with data about a group of tests, also called a 'fixture' or a 'testcase'.
    /// </summary>
    public class TestGroupModel
    {
        private List<Test> tests;
        private Dictionary<string,Test> testsByName;

        private AllTestCases parent;
        public AllTestCases Parent{
            get{return parent;}
        }
        internal event TestGroupEventHandler eventTestGroupIsDone;
        internal event ExtTestGroupEventHandler eventTestIsDoneChanged;
        internal event ExtTestGroupEventHandler eventTestEnabledChanged;
        internal event TestGroupEventHandler eventTestGroupSelectionChanged;

        private TESTGROUPSELECTION testGroupSelection;
        public TESTGROUPSELECTION TestGroupSelection
        {
            get { return testGroupSelection; }
            private set
            {
                if (testGroupSelection != value)
                {
                    testGroupSelection = value;
                    RaiseTestGroupSelectionChanged();
                }
            }
        }

        private String name;
        public String Name
        {
            get { return this.name; }
        }

        public bool DisabledByDefault
        {
            get { return Name.StartsWith("DISABLED_"); }
        }

        private bool testGroupIsDone = false;
        public bool TestGroupIsDone
        {
            get { return this.testGroupIsDone; }
            set
            {
                if (this.testGroupIsDone != value)
                {
                    this.testGroupIsDone = value;
                    RaiseTestGroupIsDoneEvent();
                }
            }
        }

        /// <summary>
        /// Clear all stored test results.
        /// </summary>
        public void ResetExecutionState()
        {
            foreach( Test tst in tests )
                tst.ResetExecutionState();
        }

        public Test AppendNewTest(String Name,uint ID)
        {
            Test newTest = new Test(this,Name,ID);
            this.AppendTest(newTest);
            return newTest;
        }

        private void RaiseTestGroupSelectionChanged()
        {
            if (eventTestGroupSelectionChanged != null)
                eventTestGroupSelectionChanged(this);
        }

        private void RaiseTestGroupIsDoneEvent()
        {
            if (eventTestGroupIsDone != null)
                eventTestGroupIsDone(this);
        }

        private void RaiseTestEnabledChanged(Test testModel)
        {
            if (eventTestEnabledChanged != null)
                eventTestEnabledChanged(this, testModel);
        }

        public bool CompareSet(TestGroupModel other)
        {
            if( TestCount != other.TestCount )
                return false;
            foreach( Test tst in tests )
            {
              Test ot;
                try{
                    ot = other[tst.Name];
                }catch(System.Exception)
                    {return false;}
                if(!tst.CompareSet(ot))return false;
            }
            return true;
        }

        public TestGroupModel(AllTestCases Parent, String Name)
        {
            if (Name == null)
                throw new ArgumentNullException("A test group name cannot be null.");

            this.name = Name;
            this.parent = Parent;

            tests = new List<Test>();
            testsByName = new Dictionary<string,Test>();
            this.eventTestIsDoneChanged = null;
            this.eventTestGroupIsDone = null;
        }

        internal void Enable(bool enableDefaultDISABLED_Tests)
        {
            for (int testIndex = 0; testIndex < tests.Count; testIndex++)
            {
                if (!tests[testIndex].DisabledByDefault || enableDefaultDISABLED_Tests)
                    tests[testIndex].Enabled = true;
            }
        }

        internal void Disable()
        {
            for (int testIndex = 0; testIndex < tests.Count; testIndex++)
                tests[testIndex].Enabled = false;
        }

        /// <summary>
        /// Add a test to this group. Only to be called from Test's constructor.
        /// </summary>
        /// <param name="NewTest">New test to add.</param>
        private void AppendTest(Test NewTest)
        {
            // Name valid?
            if (NewTest == null)
                throw new ArgumentNullException("New test cannot be null.");

            // group set?
            if(NewTest.Group!=this)
                throw new ArgumentException("Test must point to this group");

            // not already added?
            if( null != this.tests.Find(delegate(Models.Test m){return object.ReferenceEquals(m,NewTest);}) )
                throw new ArgumentException("Test is already in this group.");

            if (this.testsByName.ContainsKey(NewTest.Name))
                throw new ArgumentException("A test with the name '" + NewTest.Name + "' already exists.");

            NewTest.eventTestExecutionStateChanged += new TestEventHandler(OnTestIsDoneChanged);
            NewTest.eventTestEnabledChanged += new TestEventHandler(OnTestEnabledChanged);
            this.tests.Add(NewTest);
            this.testsByName[NewTest.Name] = NewTest;
            if(this.parent!=null)
                this.parent.AddNewTestToIdMap(NewTest);
            CheckTestGroupSelection();
        }

        private void CheckTestGroupSelection()
        {
            bool oneTestEnabled = false;
            bool allButDefaultDisabledTestsEnabled = true;
            bool allTestsEnabled = true;

            for (int testIndex = 0; testIndex < tests.Count; testIndex++)
            {
                if (tests[testIndex].Enabled)
                    oneTestEnabled = true;
                else
                {
                    allTestsEnabled = false;
                    if (!tests[testIndex].DisabledByDefault)
                        allButDefaultDisabledTestsEnabled = false;
                }
            }

            if (EnabledTestCount == 0)
                this.TestGroupSelection = TESTGROUPSELECTION.NONE;
            else
            {
                if (allTestsEnabled)
                    this.TestGroupSelection = TESTGROUPSELECTION.ALL;
                else
                {
                    if (allButDefaultDisabledTestsEnabled)
                        this.TestGroupSelection = TESTGROUPSELECTION.ALL_BUT_DEFAULT_DISABLED;
                    else
                    {
                        if (oneTestEnabled)
                            this.TestGroupSelection = TESTGROUPSELECTION.PARTLY;
                        else
                            this.TestGroupSelection = TESTGROUPSELECTION.NONE;
                    }
                }
            }
        }

        private void OnTestEnabledChanged(Test sender)
        {
            CheckTestGroupSelection();
            RaiseTestEnabledChanged(sender);
        }

        private void OnTestIsDoneChanged(Test sender)
        {
            eventTestIsDoneChanged(this, sender);
            if (this.RemainingTestCount == 0)
            {
                this.testGroupIsDone = true;
                eventTestGroupIsDone(this);
            }
        }

        public int TestCount
        {
            get { return this.tests.Count; }
        }

        public int RemainingTestCount
        {
            get
            {
                int remainingTests = 0;
                foreach (Test test in this.tests)
                {
                    if (test.Enabled)
                        remainingTests += Repeat - test.IterationCount;
                }
                return remainingTests;
            }
        }

        public int EnabledTestCount
        {
            get
            {
                return TestCount - DisabledTestCount;
            }
        }

        public int DisabledTestCount
        {
            get
            {
                int disabledTests = 0;
                foreach (Test test in this.tests)
                {
                    if (!test.Enabled)
                        disabledTests++;
                }
                return disabledTests;
            }
        }

        public int FailedTestCount
        {
            get
            {
                int failedTests = 0;
                foreach (Test test in this.tests)
                    if (test.FailedCount > 0)
                        failedTests++;

                return failedTests;
            }
        }

        public int PassedTestCount
        {
            get
            {
                int passedTests = 0;
                foreach (Test test in this.tests)
                    if (test.PassedCount > 0 && test.FailedCount == 0)
                        passedTests++;
                
                return passedTests;
            }
        }

        public int Repeat { get { return parent.Repeat; } }

        public Test this[int Index]
        {
            get { return tests[Index]; }
        }

        public int IndexOf(Test test)
        {
            return tests.IndexOf(test);
        }

        public Test this[String Name]
        {
            get { return this.testsByName[Name]; }
        }
    }
}
