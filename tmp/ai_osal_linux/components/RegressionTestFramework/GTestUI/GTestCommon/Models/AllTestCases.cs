using System;
using System.Collections.Generic;
using System.Text;

namespace GTestCommon.Models
{
    /// <summary>
    /// Data class for data about an entire set of tests as reported by GTest.
    /// </summary>
    public class AllTestCases
    {
        private List<TestGroupModel> testGroups;
        private Dictionary<string,TestGroupModel> testGroupsByName;
        private Dictionary<uint,Test> testsById;
        private ulong checkSum;
        private uint highestID;

        public event ExtTestGroupEventHandler eventTestEnabledChanged; 
        public event TestGroupEventHandler eventTestGroupIsDone;
        public event TestGroupEventHandler eventTestGroupSelectionChanged;
        public event ExtTestGroupEventHandler eventTestExecutionStateChanged;

        private bool enableDefaultDISABLED_Tests;
        public bool EnableDefaultDISABLED_Tests
        {
            get { return this.enableDefaultDISABLED_Tests; }
            set
            {
                if (this.enableDefaultDISABLED_Tests != value)
                {
                    this.enableDefaultDISABLED_Tests = value;
                    resetDefaultDISABLED_Tests();
                }
            }
        }

        public int Repeat;		// TODO: ..... remove this field?

        public void resetDefaultDISABLED_Tests()
        {
            for (int testGroupIndex = 0; testGroupIndex < testGroups.Count; testGroupIndex++)
            {
                bool groupChanged = false;
                for (int testIndex = 0; testIndex < testGroups[testGroupIndex].TestCount; testIndex++)
                {
                    if (testGroups[testGroupIndex][testIndex].Name.StartsWith("DISABLED_") ||
                        testGroups[testGroupIndex].Name.StartsWith("DISABLED_"))
                    {
                        groupChanged = true;
                        testGroups[testGroupIndex][testIndex].Enabled = this.enableDefaultDISABLED_Tests;
                    }
                }
                if (groupChanged)
                    RaiseEventTestGroupSelectionChanged(testGroups[testGroupIndex]);
            }
        }

        public TestGroupModel AppendNewTestGroup(String Name)
        {
            TestGroupModel newTestGroup = new TestGroupModel(this, Name);
            this.AppendTestGroup(newTestGroup);
            return newTestGroup;
        }

        public int TestGroupCount
        {
            get { return testGroups.Count; }
        }

        public uint highestTestID{get{return this.highestID;}}

        public TestGroupModel this[int Index]			// ..... caution: This can be mistaken with get-by-ID.
        {
            get { return testGroups[Index]; }
        }

        /// <summary>
        /// Get a Test from its ID.
        /// </summary>
        /// <param name="ID">ID of test to get.</param>
        /// <returns>Test, or null if ID unknown.</returns>
        public Test getTestById(uint ID)
        {
            Test result;
            if(!testsById.TryGetValue(ID,out result))
                return null;				// ..... TODO: or throw some exception instead?
            return result;
        }

        public int IndexOf(TestGroupModel TestGroup)
        {
            return testGroups.IndexOf(TestGroup);
        }

        public ulong CheckSum{
            get{return checkSum;}
            set{checkSum=value;}
        }

        public void EnableAll()
        {
            for (int testGroupIndex = 0; testGroupIndex < testGroups.Count; testGroupIndex++)
                EnableTestGroup(testGroupIndex);
        }

        public void DisableAll()
        {
            for (int testGroupIndex = 0; testGroupIndex < testGroups.Count; testGroupIndex++)
                DisableTestGroup(testGroupIndex);
        }

        public void EnableTestGroup(int TestGroupIndex)
        {
            if (!this.testGroups[TestGroupIndex].DisabledByDefault || this.enableDefaultDISABLED_Tests)
                this.testGroups[TestGroupIndex].Enable(this.enableDefaultDISABLED_Tests);
        }

        public void DisableTestGroup(int TestGroupIndex)
        {
            this.testGroups[TestGroupIndex].Disable();
        }

        public void EnableTestGroup(String TestGroupName)
        {
            if (!this[TestGroupName].DisabledByDefault || this.enableDefaultDISABLED_Tests)
                this[TestGroupName].Enable(this.enableDefaultDISABLED_Tests);
        }

        public void DisableTestGroup(String TestGroupName)
        {
            this[TestGroupName].Disable();
        }

        /// <summary>
        /// Clear all stored test results.
        /// </summary>
        public void ResetExecutionState()
        {
            foreach( TestGroupModel grp in testGroups )
                grp.ResetExecutionState();
        }

        public TestGroupModel this[String Name]
        {
            get { return this.testGroupsByName[Name]; }
        }

        public bool CompareSet(AllTestCases other)
        {
            if( CheckSum != other.CheckSum )return false;
            if( TestGroupCount!=other.TestGroupCount )return false;
            if( TestCount!=other.TestCount )return false;
            foreach( TestGroupModel grp in testGroups )
            {
              TestGroupModel og;
                try{
                    og = other[grp.Name];
                }catch(System.Exception)
                    {return false;}
                if(!grp.CompareSet(og))
                    return false;
            }
            return true;
        }

        public AllTestCases(bool RunDISABLED_Tests)
        {
            this.eventTestExecutionStateChanged = null;
            this.eventTestGroupIsDone = null;
            this.testGroups = new List<TestGroupModel>();
            this.testGroupsByName = new Dictionary<string,TestGroupModel>();
            this.testsById = new Dictionary<uint,Test>();
            this.enableDefaultDISABLED_Tests = RunDISABLED_Tests;
            this.checkSum = 0;
            this.Repeat = 1;
            this.highestID = 0;
        }

        /// <summary>
        /// Add a TestCase to this set. Only to be called from TestGroupModel's constructor.
        /// </summary>
        /// <param name="TestGroup">Group to add.</param>
        private void AppendTestGroup(TestGroupModel TestGroup)
        {
            // check name
            if (TestGroup == null)
                throw new ArgumentNullException("Test Group cannot be null.");
            // check parent pointer.
            if(TestGroup.Parent!=this)
                throw new ArgumentException("New testGroup must link to this set.");
            // not already added?
            if( null != this.testGroups.Find(delegate(Models.TestGroupModel gm){return object.ReferenceEquals(gm,TestGroup);}) )
                throw new ArgumentException("TestGroup is already in this group.");
            // Already have one of this name?
            if( this.testGroupsByName.ContainsKey(TestGroup.Name) )
                throw new ArgumentException("There is another TestGroup with the same name.");

            TestGroup.eventTestIsDoneChanged += new ExtTestGroupEventHandler(OnTestIsDoneChanged);
            TestGroup.eventTestGroupIsDone += new TestGroupEventHandler(OnTestGroupIsDone);
            TestGroup.eventTestEnabledChanged += new ExtTestGroupEventHandler(OnTestEnabledChanged);
            TestGroup.eventTestGroupSelectionChanged += new TestGroupEventHandler(OnTestGroupSelectionChanged);
            this.testGroups.Add(TestGroup);
            this.testGroupsByName[TestGroup.Name]=TestGroup;
            // if the group has tests, add them to the ID dict.
            for( int i=0 ; i<TestGroup.TestCount ; i++ )
                AddNewTestToIdMap(TestGroup[i]);
            this.checkSum = 0ul;	// checksum is now invalid.
        }

        /// <summary>
        /// Called by TestGroupModel when a new test was just added, so that the AllTestCases can update the ID map.
        /// function is protected virtual so that a testing-Mock can overload (intercept) this call.
        /// </summary>
        /// <param name="tst">The test to add.</param>
        public virtual void AddNewTestToIdMap(Test tst)
        {
            if( testsById.ContainsKey(tst.ID) )
                throw new ArgumentException(string.Format("Cannot add Test. ID {0} already in use.",tst.ID));
            if( tst.Group==null || tst.Group.Parent==null || !object.ReferenceEquals(tst.Group.Parent,this) )
                throw new ArgumentException("Can only add test of an own group to map. (shall be only called from group)");
            testsById[tst.ID] = tst;
            if(tst.ID>highestID)
                highestID=tst.ID;
        }

        private void RaiseEventTestGroupSelectionChanged(TestGroupModel sender)
        {
            if (this.eventTestGroupSelectionChanged != null)
                eventTestGroupSelectionChanged(sender);
        }

        private void OnTestGroupSelectionChanged(TestGroupModel sender)
        {
            RaiseEventTestGroupSelectionChanged(sender);
        }

        private void RaiseEventTestEnabledChanged(TestGroupModel sender, Test testModel)
        {
            if (this.eventTestEnabledChanged != null)
                eventTestEnabledChanged(sender, testModel);
        }

        private void OnTestEnabledChanged(TestGroupModel sender, Test testModel)
        {
            RaiseEventTestEnabledChanged(sender, testModel);
        }

        private void RaiseEventTestGroupIsDone(TestGroupModel sender)
        {
            if (this.eventTestGroupIsDone != null)
                eventTestGroupIsDone(sender);
        }

        private void OnTestGroupIsDone(TestGroupModel sender)
        {
            RaiseEventTestGroupIsDone(sender);
        }

        private void RaiseEventTestIsDoneChanged(TestGroupModel testGroup, Test testModel)
        {
            if (this.eventTestExecutionStateChanged != null)
                eventTestExecutionStateChanged(testGroup, testModel);
        }

        private void OnTestIsDoneChanged(TestGroupModel testGroup, Test testModel)
        {
            RaiseEventTestIsDoneChanged(testGroup, testModel);
        }

        public int TestCount
        {
            get
            {
                int testCount = 0;
                for (int testGroupIndex = 0; testGroupIndex < testGroups.Count; testGroupIndex++)
                {
                    testCount += testGroups[testGroupIndex].TestCount;
                }
                return testCount;
            }
        }

        public int TestsToRun
        {
            get { return (TestCount - DisabledTestCount) * Repeat; }
        }

        public int DisabledTestCount
        {
            get
            {
                int disabledTestCount = 0;
                for (int testGroupIndex = 0; testGroupIndex < testGroups.Count; testGroupIndex++)
                {
                    disabledTestCount += testGroups[testGroupIndex].DisabledTestCount;
                }
                return disabledTestCount;
            }
        }

        public int PassedTestCount
        {
            get
            {
                int passedTestCount = 0;
                for (int testGroupIndex = 0; testGroupIndex < testGroups.Count; testGroupIndex++)
                {
                    passedTestCount += testGroups[testGroupIndex].PassedTestCount;
                }
                return passedTestCount;
            }
        }

        public int FailedTestCount
        {
            get
            {
                int failedTestCount = 0;
                for (int testGroupIndex = 0; testGroupIndex < testGroups.Count; testGroupIndex++)
                {
                    failedTestCount += testGroups[testGroupIndex].FailedTestCount;
                }
                return failedTestCount;
            }
        }

        public int RemainingTestCount
        {
            get
            {
                int remainingTestCount = 0;
                for (int testGroupIndex = 0; testGroupIndex < testGroups.Count; testGroupIndex++)
                {
                    remainingTestCount += testGroups[testGroupIndex].RemainingTestCount;
                }
                return remainingTestCount;
            }
        }

        public void AddResult(String TestGroupName, String TestName, bool Passed)
        {
            this[TestGroupName][TestName].AddResult(Passed);
        }

        public void AddResult(TestResultModel result)
        {
          Test tst = getTestById(result.testID);
            if( tst==null )
                throw new KeyNotFoundException("Cannot add test result. TestID not found in model!");
            tst.AddResult(result);
        }

        public void AddResult(TestResultModel result,bool allowReplace)
        {
          Test tst = getTestById(result.testID);
            if( tst==null )
                throw new KeyNotFoundException("Cannot add test result. TestID not found in model!");
            tst.AddResult(result,allowReplace);
        }
    }
}
