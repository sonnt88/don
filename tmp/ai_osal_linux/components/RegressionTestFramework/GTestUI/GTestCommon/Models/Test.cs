using System;
using System.Collections.Generic;
using System.Text;

namespace GTestCommon.Models
{
    public delegate void TestEventHandler(Test sender);

    /// <summary>
    /// Data class with data about a single test.
    /// </summary>
    public class Test
    {
        internal event TestEventHandler eventTestExecutionStateChanged;
        internal event TestEventHandler eventTestEnabledChanged;

        private String name;
        public String Name {
            get { return this.name; }
        }

        private bool enabled = true;
        public bool Enabled
        {
            get { return enabled; }
            set {
                if (this.enabled != value)
                {
                    this.enabled = value;
                    RaiseTestEnabledChangedEvent();
                }
            }
        }

        private TestGroupModel group;
        public TestGroupModel Group{
            get{return group;}
        }

        private uint id = 0;
        public uint ID{
            get{return id;}
        }

        public bool DisabledByDefault
        {
            get { return Name.StartsWith("DISABLED_"); }
        }

        /// <summary>
        /// Dictionary of results. Kes is the iteration number, which is also found in the test itself.
        /// </summary>
        private SortedDictionary<uint,TestResultModel> results;
        private uint results_nextKey;		// Value one above the highest key in results dict.

        public int IterationCount { get { return results.Count; } }

        public TestResultModel GetResult(uint iter)
        {
            return this.results[iter];
        }

        public IEnumerable<TestResultModel> iterResults()
        {
            foreach( uint iter in this.results.Keys )
            {
              TestResultModel res = results[iter];
                if( res.iteration!=iter || res.testID!=id )
                    throw new System.Exception("iter or testID was changed in result-set. Broken datastructs.");
                yield return res;
            }
        }

        public int PassedCount
        {
            get 
            {
                int passedCount = 0;
                foreach (TestResultModel res in results.Values)
                    if (res.success) passedCount++;
                return passedCount;
            }
        }

        public int FailedCount { get { return results.Count - PassedCount; } }

        private void RaiseTestExecutionStateChanged()
        {
            if (eventTestExecutionStateChanged != null)
                eventTestExecutionStateChanged(this);
        }

        private void RaiseTestEnabledChangedEvent()
        {
            if (eventTestEnabledChanged != null)
                eventTestEnabledChanged(this);
        }

        public bool CompareSet(Test other)
        {
            return name==other.name && id==other.id;
        }

        public Test(TestGroupModel Parent,String Name,uint ID)
        {
            if (Name == null)
                throw new ArgumentNullException("A test name cannot be null.");

            this.name = Name;
            this.id = ID;
            this.group = Parent;
            this.results = new SortedDictionary<uint,TestResultModel>();
            this.eventTestExecutionStateChanged = null;
        }

        /// <summary>
        /// Clear all stored test results.
        /// </summary>
        public void ResetExecutionState()
        {
            this.results.Clear();
            results_nextKey = 0u;
            RaiseTestExecutionStateChanged();
        }

        public void AddResult(bool Passed)
        {
            this.AddResult(new TestResultModel(ID,Passed));
        }

        public int Repeat { get { return group.Repeat; } }

        /// <summary>
        /// Add a result to the results dict. If iteration is set to 0xFFFFFFFFu (default value), the next free is used
        /// (not filling gaps)
        /// </summary>
        /// <param name="Result">The result to add.</param>
        /// <param name="allowReplace">Indicate if an other result with same iteration may be replaced.</param>
        public void AddResult(TestResultModel Result,bool allowReplace)
        {
            if(Result.iteration==0xFFFFFFFFu)
                Result.iteration = results_nextKey;
            if( (!allowReplace) && results.ContainsKey(Result.iteration) )
                throw new System.ArgumentException("iteration "+Result.iteration.ToString()+" already in results.");
            // check max number of results (only if defined >= 1 )
          int maxRep = 0;
            if( this.group!=null )
                maxRep = Repeat;
            if( this.IterationCount>=maxRep && maxRep>=1 )
                throw new Exception("Error: Maximum number of iterations has been reached for test " + this.Name + "." +
                    " Have you forgotten to call 'TestModel.ResetExecutionState'?");
            if( Result.testID != this.id )
                throw new ArgumentException("Cannot add testResult with a different testID.");

            this.results[Result.iteration] = Result;
            if( results_nextKey <= Result.iteration )
                results_nextKey = Result.iteration+1 ;
            RaiseTestExecutionStateChanged();
        }

        public void AddResult(TestResultModel Result)
        {
            AddResult(Result,false);
        }

    }
}
