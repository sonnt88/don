using System.Collections.Generic;
using System;


namespace GTestCommon.Models
{

    /// <summary>
    /// Simple data class for holding the result of a single run of one test.
    /// </summary>
    public class TestResultModel
    {

        /// <summary> Flag indicating success of this test-run. </summary>
        public bool success;
        /// <summary> When was this test run? </summary>
        public System.DateTime time;
        /// <summary> Time in milliseconds this test ran. </summary>
        public uint duration;

        public uint testID;
        public uint iteration;

        public class OutputReference
        {
            public string fileName;
            public uint offsetStart;
            public uint offsetEnd;
        }

        /// <summary> Text output of the GTest console from the run. </summary>
        public string GTestOutput;
        /// <summary> Output of the console output. </summary>
        public OutputReference consoleOutput;
        /// <summary> Output of TTFis during the test. </summary>
        public OutputReference ttFisOutput;



        public TestResultModel()
        {
            this.success = false;
            this.time = default(DateTime);
            this.duration = 0u;
            this.iteration = 0xFFFFFFFFu;
        }

        public TestResultModel(uint testID,bool success)
        {
            this.testID = testID;
            this.iteration = 0;
            this.success = success;
            this.time = DateTime.Now;
            this.duration = 0u;
            this.iteration = 0xFFFFFFFFu;
        }

        public TestResultModel(bool success,DateTime time,uint duration_ms)
        {
            this.success = success;
            this.time = time;
            this.duration = duration_ms;
            this.iteration = 0xFFFFFFFFu;
        }

    }

}
