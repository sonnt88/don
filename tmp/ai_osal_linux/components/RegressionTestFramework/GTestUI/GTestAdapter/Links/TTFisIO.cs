using GTestCommon;
using GTestCommon.Links;



// This is just a stup for later...
namespace GTestAdapter.Links
{


	public class TTFisIO : IQueuedPacketLink<string>
	{
		public TTFisIO()
		{
			m_sendQueue = null;
			m_recvQueue = null;

			m_qevent = new System.Threading.ManualResetEvent(false);
			m_doneEvent = new System.Threading.ManualResetEvent(false);
		}

		/// <summary>
		/// Opens serial port and starts worker thread.
		/// </summary>
		/// <param name="target">Dummy for interface definition. Not used.</param>
		/// <returns></returns>
		public bool Start(string target)
		{
			if(m_thr!=null)
				throw new System.Exception("is already connected");
			if( m_sendQueue==null || m_recvQueue==null )
				throw new System.Exception("in and out queues not yet set.");

			m_thr = new System.Threading.Thread(_threadProc);
			m_thr.Start(this);
			return true;
		}

		/// <summary>
		/// Request this object to stop the connection. Will close port and terminate worker thread.
		/// </summary>
		public void Stop(bool sendAll)
		{
			if(m_thr==null)
				return;
			m_qevent.Set();
			m_doneEvent.WaitOne();
			m_thr=null;
		}

		public void Dispose()
		{
			this.Stop(false);
		}

		public bool bIsStarted()
		{
			return m_thr!=null;
		}

		public bool bIsConnected()
		{
			return bIsStarted();
		}

		public Queue<string> inQueue{
			get{return m_recvQueue;}
			set
			{
				if(m_thr!=null)
					throw new System.Exception("cannot change queues while connected");
				m_recvQueue = value;
			}
		}

		public Queue<string> outQueue{
			get{return m_sendQueue;}
			set
			{
				if(m_thr!=null)
					throw new System.Exception("cannot change queues while connected");
				m_sendQueue = value;
			}
		}

		/// <summary>
		/// Stub to call worker thread. Need static method to call Thread().
		/// </summary>
		/// <param name="obj">handle to our object. The 'this' pointer.</param>
		static private void _threadProc(object obj)
		{
			((TTFisIO)obj).thr_mainLoop();
		}

		/// <summary>
		/// Worker thread of this object.
		/// </summary>
		private void thr_mainLoop()
		{
		  bool bFirst=true;
		  uint count=0;
			if( connect != null )
				connect(true);
			while(true)
			{
				if(m_qevent.WaitOne(bFirst?1000:2500))
					break;
				m_recvQueue.Add(string.Format("<TTFisDummy> ({0,2:0})Ping!",count));
				if(m_qevent.WaitOne(2500))
					break;
				m_recvQueue.Add("<TTFisDummy>         Pong!");
				bFirst=false;
				count++;
			}
			if( connect != null )
				connect(false);
			m_doneEvent.Set();
		}

		public event ConnectEventHandler connect;

		private System.Threading.Thread m_thr;					// worker thread handle

		private System.Threading.EventWaitHandle m_qevent;		// quit-signal. Set to end worker.
		private System.Threading.EventWaitHandle m_doneEvent;	// signal set by worker when exiting.

		private Queue<string> m_sendQueue;						// queue with packets to send over net.
		private Queue<string> m_recvQueue;						// queue to be filled with incoming packets from net.

	}

}
