using GTestCommon;
using GTestCommon.Links;




namespace GTestAdapter.Links
{


	public class SerialLink : GTestCommon.Links.QueuedStringsLink
	{
		public SerialLink()
		 : base(4096)
		{
			m_comPort = null;
		}

		/// <summary>
		/// Opens serial port and starts worker thread.
		/// </summary>
		/// <param name="comPortName">name of serial port. e.g.  "COM2"</param>
		/// <returns></returns>
		public override bool Start(string comPortName)
		{
		  bool res;
			if(bIsStarted())
				throw new System.Exception("is already connected");
			if( inQueue==null || outQueue==null )
				throw new System.Exception("in and out queues not yet set.");

			m_comPort = new System.IO.Ports.SerialPort(
					string.Format(comPortName)  ,
					115200  ,
					System.IO.Ports.Parity.None  ,
					8  ,
					System.IO.Ports.StopBits.One
			);
			m_comPort.Handshake = System.IO.Ports.Handshake.None;
			try{
				m_comPort.Open();
			}catch(System.IO.IOException)
			{
				// bad port name. Does not exist.
				m_comPort.Dispose();
				m_comPort=null;
				return false;
			}catch(System.UnauthorizedAccessException)
			{
				// port is not free for use.
				m_comPort.Dispose();
				m_comPort=null;
				return false;
			}

			inStream = m_comPort.BaseStream;
			outStream = inStream;

			// Call base class Start to launch worker thread.
			res = base.Start(null);

			if(!res)
			{
				m_comPort.Close();m_comPort.Dispose();m_comPort=null;
			}

			return res;
		}

		/// <summary>
		/// Request this object to stop the connection. Will close port and terminate worker thread.
		/// </summary>
		public override void Stop(bool sendAll)
		{
			if(!bIsStarted())
				return;
			base.Stop(sendAll);
			inStream=null;
			outStream=null;
			m_comPort.Close();
			m_comPort.Dispose();
			m_comPort = null;
		}


		private System.IO.Ports.SerialPort m_comPort;			// serial port handle

	}


}

