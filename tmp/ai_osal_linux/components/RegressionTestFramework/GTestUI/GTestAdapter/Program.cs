using System;
using System.Collections.Generic;
using System.Text;
using GTestCommon;
using GTestCommon.Links;
using GTestCommon.Utils.ServiceDiscovery;
using GTestCommon.Utils.ServiceDiscovery.Models;


namespace GTestAdapter
{
	internal class Program
	{

		static public string VersionString = "GTestAdapter v0.0";


		static int Main(string[] args)
		{
		  bool interactive;
		  int retVal;
			//return dummy.Dummy.tryServiceLink();
			//return dummy.Dummy.trySerialConsole();
			//return dummy.Dummy.tryTTFisDummy();
			//return dummy.Dummy.tryDatabase();
			//return dummy.Dummy.tryConsole();
			//return dummy.Dummy.tryTextWriter();

			// get settings
			sm_settings.loadDefaults();
			if(!sm_settings.parseArgs( args , 0u , System.Console.Out , System.Console.Error ))
				return 5;

			// suppress normal Ctrl-C breaking.
//			System.Console.TreatControlCAsInput = true;

            // prepare local
#if LOCAL_CONSOLE_LINK
			sm_console = new LocalConsole();
			sm_console.Start("");
#else
            sm_console = null;
#endif

            if (sm_settings.logFileName!=null)
			{
				sm_logfileLink = new FileStringLink();
				sm_logfileLink.Start(sm_settings.logFileName);
			}

		  AdapterCore mobj;
		  PacketFactory4service fac;
		  IQueuedPacketLink<IPacket> link_u,link_d;

			interactive = sm_settings.selectedInteractive;

			// create adapter core
			mobj = new AdapterCore(interactive);

			// create packet factory
			fac = new PacketFactory4service();

			// create link to UI
			link_u = null;
			if( interactive )
				link_u = new TcpPacketUplink(fac);

			// create link to service
			link_d = new TcpPacketLink(fac);

			// add to adapter core.
			mobj.serviceLink = link_d;
			if( interactive )
				mobj.UIlink = link_u;
			mobj.consoleLink = sm_console;

            // set our var
			sm_adapter = mobj;

			// set break-handler
			System.Console.CancelKeyPress += new ConsoleCancelEventHandler(onCtrlC);

			// set up service announcer
			ServiceAnnouncer anno=null;
			if(interactive )
			{
				anno = createAnnouncer( 14243u , System.Convert.ToUInt16(sm_settings.UIport) , "GTest adapter" );
				anno.Run();
			}

			// run!
			retVal = mobj.Run();

			// done. either start failed, or quit normally.
			if(interactive )
				anno.Stop();
			if( mobj.errorMessage != null )
			{
				// have error. was some fault.
				System.Console.Error.WriteLine(mobj.errorMessage);
				return 5;
			}

			if (sm_console != null)
                sm_console.Stop(true);
			if(sm_logfileLink!=null)
				sm_logfileLink.Stop(true);

			return retVal;
		}

		static void onCtrlC(object sender, ConsoleCancelEventArgs ev)
		{
			ev.Cancel = true;			// suppress normal breaking.
			sm_adapter.processBreak();
		}

		static public void PrintLocal(string format, params object[] arg)
		{
		  string line = string.Format(format,arg);
			if(sm_console!=null)
				sm_console.outQueue.Add(line);
#if !LOCAL_CONSOLE_LINK
            System.Console.WriteLine(line);
#endif
		}

		static public void PrintLocalLogged(string format, params object[] arg)
		{
		  string line = string.Format(format,arg);
			if(sm_console!=null)
				sm_console.outQueue.Add(line);
			if(sm_logfileLink!=null)
				sm_logfileLink.outQueue.Add(line);
		}

		static public void PrintLog(string format, params object[] arg)
		{
		  string line = string.Format(format,arg);
			if(sm_logfileLink!=null)
				sm_logfileLink.outQueue.Add(line);
		}

		private static ServiceAnnouncer createAnnouncer( uint announcePort , uint servicePort , string name )
		{
		  ServiceAnnouncementModel amdl;
		  ServiceModel smdl;
		  ServiceAddr sAddr;
		  GTestCommon.Utils.ServiceDiscovery.Interfaces.IServiceAnnouncementPacketFactory fac;
			sAddr = new ServiceAddr( System.Net.IPAddress.Any , (int)servicePort );
			smdl = new ServiceModel( sAddr , name );
			amdl = new ServiceAnnouncementModel( smdl , (int)announcePort );
			fac = new GTestAdapter.AnnouncerPackFac( servicePort , name );
			return new ServiceAnnouncer( amdl , fac );
		}

		public static Settings sm_settings = new Settings();
		private static AdapterCore sm_adapter = null;
		private static IQueuedPacketLink<string> sm_console = null;
		private static IQueuedPacketLink<string> sm_logfileLink = null;
	}
}

/*

		private void createAnnouncer( uint announcePort , uint servicePort )
		{
		  GTestCommon.Utils.ServiceDiscovery.Models.ServiceAnnouncementModel amdl;
		  GTestCommon.Utils.ServiceDiscovery.Models.ServiceModel smdl;
		  GTestCommon.Utils.ServiceDiscovery.Models.ServiceAddr sAddr;
			sAddr = new GTestCommon.Utils.ServiceDiscovery.Models.ServiceAddr( System.Net.IPAddress.Any , servicePort );
			smdl = new GTestCommon.Utils.ServiceDiscovery.Models.ServiceModel(new ServiceAddr(IPAddress.Any, servicePort), VersionString);
			mdl = new GTestCommon.Utils.ServiceDiscovery.Models.ServiceAnnouncementModel( smdl , (int)announcePort );
			m_announcer = new GTestCommon.Utils.ServiceDiscovery.ServiceAnnouncer(DataLogging, packfac);
		}

*/

