using System.Collections.Generic;
using GTestCommon;
using GTestCommon.Links;




namespace GTestAdapter
{
    /// <summary>
    /// Class to contain the state of a specific link.
    /// </summary>
	public class LinkStateUI : LinkState
	{
		public LinkStateUI(IQueuedPacketLink<IPacket> link,string linkPort)
		{
			m_link = link;
			m_linkPort = linkPort;
			m_connEvtH = new ConnectEventHandler(processConnectEvent);
			m_UIwaitingForTestList   = 0;
			m_UIwaitingForState      = 0;
			m_UIwaitingForTestStart  = 0;
			m_UIwaitingForAbort      = 0;
			m_UIwaitingForAllResults = 0;
			m_knownState = typeof(States.StateUnknown);
		}

		public override bool startUp(WaitAnyHandler waitObject,triggerCallback newPacketsCallback)
		{
			addToWaitAnyHandler(waitObject,newPacketsCallback);
			m_link.connect += m_connEvtH;
			if(!m_link.Start(m_linkPort))
				return false;
			// started.
			return true;
		}

		public override void shutDown()
		{
			m_link.connect -= m_connEvtH;
			m_link.Stop(true);
		}

		public override void putPacket(IPacket pck)
		{
			if(!(pck is Packet))
				return;
			if(m_link.bIsConnected())
			{
				// if one of those where we have waitflag, add its replynum.
				if( pck is PacketAbortAck )
				{
					((Packet)pck).reqSerial = m_UIwaitingForAbort;
					m_UIwaitingForAbort=0;
				}
				m_link.outQueue.Add(pck);
			}
		}

		public void informState(States.State0 state)
		{
		  System.Type newState = state.GetType();
			if( newState == m_knownState )
				return;		// nothing to do.
			// state changes. User-process only gets 'testing/starting' or other. So check if this changes.
		  bool isTestingForUi1 = (m_knownState==typeof(States.StateTesting)||m_knownState==typeof(States.StateTestStarting));
		  bool isTestingForUi2 = (newState==typeof(States.StateTesting)||newState==typeof(States.StateTestStarting));
			// check for transitions to notify.
			// assign
			m_knownState = newState;

			if(isTestingForUi1!=isTestingForUi2)
			{
				// need to inform UI.
				sendStateToUI();
			}
		}

		public void informTestStarting()
		{
		}

		public void informTestStarted()
		{
		}

		public void informTestEnded(bool wasAbort)
		{
		}

		/// <summary>
		/// Send the desctiption of the tests to UI.
		/// </summary>
		/// <param name="replySerial">The serial number to use in the reqSerial field of packets.</param>
		public void sendTestList(GTestCommon.Models.AllTestCases tests)
		{
		  PacketTestList pck;
			pck = new PacketTestList();
			pck.data = tests;
			pck.reqSerial = m_UIwaitingForTestList;
			this.putPacket( pck );
		}

		/// <summary>
		/// Send a stream of packets to the UI with all our stored results.
		/// </summary>
		/// <param name="replySerial">The serial number to use in the reqSerial field of packets.</param>
		public void sendTestResults(GTestCommon.Models.AllTestCases tests)
		{
			for( int i=0 ; i<tests.TestGroupCount ; i++ )
			{
			  GTestCommon.Models.TestGroupModel tstGrp;
				tstGrp = tests[i];
				for( int j=0 ; j<tstGrp.TestCount ; j++ )
				{
				  GTestCommon.Models.Test tst;
					tst = tstGrp[j];
					foreach( GTestCommon.Models.TestResultModel res in tst.iterResults() )
					{
					  PacketTestResult tr_pck = new PacketTestResult();
						tr_pck.reqSerial = m_UIwaitingForAllResults;
						tr_pck.result = res;
						this.putPacket(tr_pck);
					}
				}
			}
			// and close with done-packet
		  PacketSendAllResultsDone trd_pck = new PacketSendAllResultsDone();
			trd_pck.reqSerial = m_UIwaitingForAllResults;
			this.putPacket(trd_pck);
			// done.
			m_UIwaitingForAllResults = 0;
		}

		protected override void addToWaitAnyHandler(WaitAnyHandler waitObject,triggerCallback cb)
		{
			base.addToWaitAnyHandler(waitObject,cb);
			waitObject.addHandler(
					new WaitAnyHandler.handleSignal(this.callback)  ,
					m_link.inQueue.getWaitHandle()
			);
		}

		private void processConnectEvent(bool conn)
		{
			// do not process here directly. Not our thread. Place packet to self. Will trigger queue event.
			m_link.inQueue.Add(new IsConnected(conn));
		}

		private object callback()
		{
		  IPacket pck;
			// our event was there. We have input data in queue.
			while(( pck = m_link.inQueue.Get(false) )!=null)
			{
				processPacket(pck);
			}
			if(m_inBuffer.Count>0)
				m_callBack.Invoke();
			return null;
		}

		private void processPacket(IPacket pck)
		{
			if(!(pck is Packet))
				return;
			System.Console.Out.WriteLine( "packet in, " + pck.serial );
			if( pck is IsConnected )
			{
				m_UIwaitingForTestList = 0;
				m_UIwaitingForState = 0;
				m_knownState = typeof(States.StateUnknown);
				if( ((IsConnected)pck).isConnect )
				{
					// A UI process connected.
					GTestAdapter.Program.PrintLocal("UI process connected");
					// not sending info. UI must query that.
					// state
				}else{
					// The UI disconnected.
					GTestAdapter.Program.PrintLocal("UI process left");
				}
				return;
			}
		  Packet p = (Packet)pck;
			if( p is PacketGetVersion )
			{
				this.putPacket( new PacketVersion(GTestAdapter.Program.VersionString) );
				return;
			}else if( p is PacketGetState )
			{
				m_UIwaitingForState = p.serial;
				if( m_knownState!=typeof(States.StateUnknown) )
					sendStateToUI();
				// If unknown, state will come later. No need to 'request' this from adapter.
				return;
			}else if( p is PacketGetTestList )
			{
				m_UIwaitingForTestList = p.serial;
			}else if( p is PacketRunTests )
			{
				m_UIwaitingForTestStart = p.serial;
			}else if( p is PacketGetAllResults )
			{
				m_UIwaitingForAllResults = p.serial;
			}
			m_inBuffer.Enqueue(pck);
		}

		private void sendStateToUI()
		{
		  byte sendState;
		  Packet rpck;
			sendState = 0;
			if( m_knownState==typeof(States.StateTesting) || m_knownState==typeof(States.StateTestStarting) )
				sendState = 1;
			rpck = new PacketState( sendState );
			rpck.reqSerial = m_UIwaitingForState;
			m_UIwaitingForState = 0;
			this.putPacket( rpck );
		}

		public bool waitForState    {get{return m_UIwaitingForState!=0;}}
		public bool waitForTestList {get{return m_UIwaitingForTestList!=0;}}
		public bool waitForTestStart{get{return m_UIwaitingForTestStart!=0;}}
		public bool waitForAbort    {get{return m_UIwaitingForAbort!=0;}}

		private System.Type m_knownState;

		private IQueuedPacketLink<IPacket> m_link;
		private string m_linkPort;
		private ConnectEventHandler m_connEvtH;

		// state information
		private uint m_UIwaitingForTestList;
		private uint m_UIwaitingForState;
		private uint m_UIwaitingForTestStart;
		private uint m_UIwaitingForAbort;
		private uint m_UIwaitingForAllResults;
	};






	public class IsConnected : IPacket
	{
		public IsConnected(bool isAConnect)
		{
			m_isAConnect = isAConnect;
		}

		public uint serial
		{
			get{return (uint)(0x7FFFFF00u+(m_isAConnect?1:0));}
			set{if((value&0xFFFFFFFEu)!=0x7FFFFF00u){throw new System.Exception();}m_isAConnect=((value&1)!=0);}
		}

		public bool isConnect{get{return m_isAConnect;}}

		private bool m_isAConnect;
	};



}


