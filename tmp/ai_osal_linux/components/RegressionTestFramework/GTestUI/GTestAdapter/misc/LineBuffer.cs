using System.Collections.Generic;




namespace GTestAdapter
{




	public class LineBuffer : IEnumerable<string>
	{
		public LineBuffer()
		{
			m_lines = new List<string>();
			m_timestamps = new List<uint>();
			foreach( string s in this )
				;
		}
/*
		public class LineBufferEnum : IEnumerator<string>
		{
			LineBufferEnum(StringLineBuffer buf)
			{
				m_buf=buf;
				m_idx=-1;
			}
			public void Reset(){m_idx=-1;}
			public string Current{get{return m_buf.m_lines[m_idx];}}
			public object IEnumerator.Current{get{return Current;}}
			public bool MoveNext(){if(m_idx>=m_buf.m_lines.Count)return false;m_idx++;}
			public void Dispose(){}
			private StringLineBuffer m_buf;
			private int m_idx;
		}

		public IEnumerator<string> GetEnumerator()
		{
			return new LineBufferEnum(this);
		}
 */
		public IEnumerator<string> GetEnumerator()
		{
			foreach( string line in m_lines )
				yield return line;
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator(){return GetEnumerator();}

		public string this[uint lineNo]
		{
			get{return m_lines[(int)lineNo];}
		}

		/// <summary>
		/// Add a new line to the buffer.
		/// </summary>
		/// <param name="line">Line to add.</param>
		/// <param name="timestamp">timestamp or zero to reuse last value.</param>
		public void addLine( string line , uint timestamp )
		{
			if( timestamp>0 )
			{
				if( m_lines.Count>0 && timestamp<m_timestamps[m_lines.Count-1] )
					throw new System.ArgumentException("Added line must have higher timestamp than previous line.");
				m_lines.Add( line );
				m_timestamps.Add( timestamp );
			}else{
			  uint ts = ( m_lines.Count>0 ? m_timestamps[m_lines.Count-1] : 1 );
				m_timestamps.Add( ts );
				m_lines.Add( line );
			}
		}

		/// <summary>
		/// Drop all contents of the buffer.
		/// </summary>
		public void Clear()
		{
			m_lines.Clear();
			m_timestamps.Clear();
		}

		/// <summary>
		/// Get a line from the buffer.
		/// </summary>
		/// <param name="lineNo">line number to get.</param>
		/// <param name="timestamp">out parameter for this line's timestamp.</param>
		/// <returns>The line string.</returns>
		public string getLine( uint lineNo , out uint timestamp )
		{
			if( lineNo>=m_lines.Count )
				throw new System.ArgumentOutOfRangeException("Index out of range.");
			timestamp = m_timestamps[(int)lineNo];
			return m_lines[(int)lineNo];
		}

		public uint lineForTimestamp( uint timestamp )
		{
		  uint i1,i2;
			// binary search
			i1=0;i2=(uint)m_lines.Count;
			while(i2>i1)
			{
			  uint i=(i1+i2)>>1;
				if( timestamp<m_timestamps[(int)i] )
					{i1=i+1;}
				else if( timestamp>m_timestamps[(int)i] )
					{i2=i;}
				else
				{
					// have exact match. move backwards in case we have multiple lines with same stamp.
					while( i>0 && m_timestamps[(int)i-1]==timestamp )
						--i;
					i1=i;break;
				}
			}
			return i1;
		}


		private List<string> m_lines;
		private List<uint> m_timestamps;

	}




}

