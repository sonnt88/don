


namespace GTestAdapter
{


	public class StreamJoiner
	{

		public struct StreamDef
		{
			public GTestCommon.Queue<string> linesInput;
			public string startTag_regex;
			public string endTag_regex;
		}

		public StreamJoiner(StreamDef[] inputs,float maxTimeOffset)
		{
		  int i;
			m_inputs = new ConnectionData[inputs.Length];
			for(i=0;i<inputs.Length;i++)
			{
				m_inputs[i] = new ConnectionData();
				m_inputs[i].input = inputs[i].linesInput;
				m_inputs[i].startTag = null;
				m_inputs[i].endTag = null;
				if( inputs[i].startTag_regex != null )
					m_inputs[i].startTag = new System.Text.RegularExpressions.Regex(inputs[i].startTag_regex);
				if( inputs[i].endTag_regex != null )
					m_inputs[i].endTag = new System.Text.RegularExpressions.Regex(inputs[i].endTag_regex);
				m_inputs[i].lastMatch = System.DateTime.Now;
			}
			m_outQueue = new GTestCommon.Queue<string[]>();
		}

		public void notify()
		{
			//System.Environment.TickCount;
		}

		public GTestCommon.Queue<string[]> outQueue
		{
			get{return m_outQueue;}
		}





		private class ConnectionData
		{
		public GTestCommon.Queue<string> input;
		public System.Text.RegularExpressions.Regex startTag;
		public System.Text.RegularExpressions.Regex endTag;
		public System.DateTime lastMatch;
		}

		// data for streams we want to join
		private ConnectionData[] m_inputs;
		private GTestCommon.Queue<string[]> m_outQueue;

	}


}