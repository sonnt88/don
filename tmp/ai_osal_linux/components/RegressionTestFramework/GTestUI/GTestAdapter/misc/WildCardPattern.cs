

namespace GTestAdapter
{

	internal static class WildCardPattern
	{

		public static bool fit(string pattern,string against)
		{
			return fit(pattern,0,against,0);
		}

		private static bool fit(string pattern,int pOff,string against,int aOff)
		{
			// skip ? marks and normal (matching) chars
			while( pOff<pattern.Length && aOff<against.Length && pattern[pOff]!='*' )
			{
				if( pattern[pOff]!='?' && pattern[pOff]!=against[aOff] )
					return false;
				pOff++;aOff++;
			}
			// check type (can only be end in one string, or pattern has an asterisk)
			if(pOff>=pattern.Length)
			{
				// end of pattern
				return aOff>=against.Length;
			}else if( aOff>=against.Length )
			{
				// end of compare-string, but not pattern.
				// if remaining pattern is empty or only asterisks, is good, bad otherwise.
				return "" == pattern.Substring(pOff).Replace("*","");
			}
			// both strings not ending, pattern has asterisk. Skip askterisk(s)
			while( pOff<pattern.Length && pattern[pOff]=='*' )
				pOff++;
			// if pattern ends after this, its good.
			if(pOff>=pattern.Length)
				return true;
			// for diffrent offsets, try matching remainder.
			for( int ao=0 ; aOff+ao<=against.Length ; ao++ )
			{
				if( fit(pattern,pOff,against,aOff+ao) )
					return true;
			}
			return false;
		}


	}

}
