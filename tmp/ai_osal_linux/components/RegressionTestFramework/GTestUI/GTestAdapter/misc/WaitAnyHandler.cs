using System.Collections.Generic;


namespace GTestAdapter
{

    /// <summary>
    /// Class to encapsulate the System WaitAny function.
	/// The instance takes pairs of waithandles and associated delegates to call.
	/// A call to wait causes it to wait for any event to fire, then calls the associated delegate.
    /// </summary>
	public class WaitAnyHandler
	{
		public delegate object handleSignal();

		public WaitAnyHandler()
		{
			m_handlers = new handleSignal[1];
			m_handles = new System.Threading.WaitHandle[1];
			m_num = 0;
		}

		/// <summary>
		/// Add a waithandle and associated delegate to this object.
		/// </summary>
		/// <param name="function">Function delegate to call when this event is detected.</param>
		/// <param name="sysHandle">System waithandle to wait on in call to WaitAny.</param>
		public void addHandler(handleSignal function,System.Threading.WaitHandle sysHandle)
		{
			// resize up if needed
			if( m_num+1 > m_handlers.Length )
			{
			  handleSignal[] newHandlers = new handleSignal[(m_num*2+2+3)&0xFFFC];
				System.Array.Copy(m_handlers,newHandlers,m_num);
				m_handlers = newHandlers;
			}
			if( m_num+1 > m_handles.Length )
			{
			  System.Threading.WaitHandle[] newHandles = new System.Threading.WaitHandle[(m_num*2+2+3)&0xFFFC];
				System.Array.Copy(m_handles,newHandles,m_num);
				m_handles = newHandles;
			}
			// add
			m_handlers[m_num] = function;
			m_handles[m_num] = sysHandle;
			m_num++;
		}

		/// <summary>
		/// Wait for any of the events. Call the linked delegate function when it does.
		/// Returns after first hit or after timeout.
		/// </summary>
		/// <returns>Whatever object the delegate returns.</returns>
		public object wait(int timeOut)
		{
		  int waitRes;
			if(m_num<1)
				throw new System.NotSupportedException("no handles were added.");
			// resize array if needed (may not be too large)
			if( m_num < m_handles.Length )
			{
			  System.Threading.WaitHandle[] newHandles = new System.Threading.WaitHandle[m_num];
				System.Array.Copy(m_handles,newHandles,m_num);
				m_handles = newHandles;
			}
			// wait
			if(timeOut<0)timeOut=-1;
			waitRes = System.Threading.WaitHandle.WaitAny(m_handles,timeOut,false);
			if( waitRes<0 || waitRes>=m_num )
				return null;
			// have one. call handler function
			return m_handlers[waitRes].Invoke();
		}

		/// <summary>
		/// Wait for any of the events. Call the linked delegate function when it does.
		/// Returns after first hit.
		/// </summary>
		/// <returns>Should always return true unless WaitAny fails badly.</returns>
		public object wait()
		{
			return wait(-1);
		}

		handleSignal[] m_handlers;
		System.Threading.WaitHandle[] m_handles;
		int m_num;
	};
}
