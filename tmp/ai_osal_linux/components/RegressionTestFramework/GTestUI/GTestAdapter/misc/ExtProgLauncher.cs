using System;
using System.Collections.Generic;
using System.Text;

namespace GTestAdapter
{

	public static class ExtProgLauncher
	{

		public class ExtProgLauncher_Result
		{
			public int returnCode;
			public string stdOut;
			public string stdErr;
		};

		public static ExtProgLauncher_Result callProgram(string commandLine)
		{
			// split command and args
		  string[] parts,args;
			parts = splitstring(commandLine,false);
			if(parts.Length<1)return null;		// no command??
			args = new string[parts.Length-1];
			if(parts.Length>1)
				Array.Copy( parts , 1 , args , 0 , parts.Length-1 );
			commandLine = parts[0];
			parts=null;
			// now get working-dir
			parts = commandLine.Split( System.IO.Path.DirectorySeparatorChar );
			if( parts.Length<1 || parts[parts.Length-1].Length<1 )
				return null;
		  string workingDir = ".";
			if( parts.Length > 1 )
			{
			  string[] p2 = new string[parts.Length];
				Array.Copy( parts , 0 , p2 , 0 , parts.Length-1 );
				p2[parts.Length-1] = "";
				workingDir = string.Join( System.IO.Path.DirectorySeparatorChar.ToString() , p2 );
				commandLine = parts[parts.Length-1];
			}
			return callProgram( workingDir , commandLine , args );
		}

		public static ExtProgLauncher_Result callProgram(string workingDir,string command,string[] args)
		{
			return callProgram(workingDir,command,escapeAndConcat(args));
		}

		private struct thrDat
		{
			public StringBuilder coll;
			public System.IO.StreamReader io;
		}

		private static void thrFunc(object obj)
		{
		  thrDat coll = (thrDat)obj;
		  char[] buff = new char[4096];
		  StringBuilder partb = new StringBuilder();
			while(true)
			{
			  int num;
				num = coll.io.Read( buff , 0 , 4096 );
				if(num<=0)break;
//				coll.coll.Append(System.Text.Encoding.UTF8.GetString( buff , 0 , num ));
				for( int i=0 ; i<num ; i++ )
					partb.Append( buff[i] );
				coll.coll.Append(partb.ToString());
				partb.Remove( 0 , partb.Length );
			}
			return;
		}

		public static ExtProgLauncher_Result callProgram(string workingDir,string command,string args)
		{
		  System.Diagnostics.Process proc;
			proc = new System.Diagnostics.Process();
			proc.StartInfo.FileName = command;
			proc.StartInfo.CreateNoWindow = true;
			proc.StartInfo.Arguments = args;
			proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			proc.StartInfo.RedirectStandardOutput = true;
			proc.StartInfo.RedirectStandardError = true;
			proc.StartInfo.UseShellExecute = false;
			if( workingDir!=null )
				proc.StartInfo.WorkingDirectory = workingDir ;
			// create process.
			try{
				if(!proc.Start())return null;
			}catch(System.Exception)
			{
				// starting procecss failed. path is invalid.
				return null;
			}
			// create thread to consume stdin
		  thrDat coll1;
		  thrDat coll2;
			coll1.coll = new StringBuilder();
			coll1.io = proc.StandardOutput;
			coll2.coll = new StringBuilder();
			coll2.io = proc.StandardError;
		  System.Threading.Thread thr1,thr2;
			thr1 = new System.Threading.Thread( new System.Threading.ParameterizedThreadStart(thrFunc) );
			thr2 = new System.Threading.Thread( new System.Threading.ParameterizedThreadStart(thrFunc) );
			thr1.Start(coll1);
			thr2.Start(coll2);
			// wait for exit.
			proc.WaitForExit();
			thr1.Join(2500);
			thr2.Join(2500);
			thr1.Abort();
			thr2.Abort();
		  ExtProgLauncher_Result result;
			result = new ExtProgLauncher_Result();
			result.stdOut=coll1.coll.ToString();coll1.coll=null;
			result.stdErr=coll2.coll.ToString();coll2.coll=null;
//			System.Console.Out.WriteLine("stdout of process:");
//			System.Console.Out.WriteLine(result.stdOut);
			return result;
		}

		public static ExtProgLauncher_Result callProgram(string workingDir,string commandLine)
		{
		  string[] parts,args;
			parts = splitstring(commandLine,true);
			if(parts.Length<1)return null;		// no command??
			args = new string[parts.Length-1];
			Array.Copy( parts , args , parts.Length-1 );
			parts=null;
			return callProgram( workingDir , parts[0] , args );
		}

		public static string[] splitstring(string inp,bool stripQuotsAndEscapes)
		{
		  int i;
		  StringBuilder item = new StringBuilder();
		  List<string> res=new List<string>();
//		  List<char> item=new List<char>();
		  bool esc;
		  int quot;
			quot=0;esc=false;
			for(i=0;i<inp.Length;i++)
			{
			  char b=inp[i];
				if( esc )
				{
					if(quot>=2){return null;}
					item.Append(b);
					esc=false;
					continue;
				}
				if( b=='\\' )
				{
					esc=true;
					if(!stripQuotsAndEscapes)item.Append(b);
					continue;
				}
				if( b=='\"' )
				{
					if( item.Length>0 && quot==0 )return null;	// bad quoting. Start within string.
					if( quot>=2 )return null;		// quotes already closed.
					if(!stripQuotsAndEscapes)item.Append(b);
					quot++;continue;
				}
				if(( b==' ' || b=='\t' )&&(quot!=1))
				{
					// part done.
					if( item.Length>0 )
						res.Add( item.ToString() );
					item.Remove( 0 , item.Length );
					quot=0;
					continue;
				}
				if(quot>=2){return null;}
				item.Append(b);
			}
			if( esc || quot==1 )return null;	// end with open quote or escape?
			// end with item? (normal if no trailing spaces)
			if( item.Length>0 )
			{
				// last item
				res.Add( item.ToString() );
			}
			return res.ToArray();
		}

		public static string escapeAndConcat(string[] parts)
		{
		  List<string> res = new List<string>();
			foreach( string part in parts )
			{
			  string item = part.Replace( "\\" , "\\\\" ).Replace( "\"" , "\\\"" ).Replace( "\t" , "\\t" );
				if( item.IndexOf(' ')>=0 || item.IndexOf('\t')>=0 )
					item = "\"" + item + "\"" ;
				res.Add(item);
			}
			return string.Join( " " , res.ToArray() );
		}
	};

}
