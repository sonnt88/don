using GTestCommon.Utils.ServiceDiscovery;
using GTestCommon.Utils.ServiceDiscovery.Models;


namespace GTestAdapter
{
	public class AnnouncerPackFac : GTestCommon.Utils.ServiceDiscovery.Interfaces.IServiceAnnouncementPacketFactory
	{
		public AnnouncerPackFac( uint servicePort , string name )
		{
			m_port = servicePort;
			m_name = name;
		}


		class RequestPack : GTestCommon.Utils.ServiceDiscovery.ServiceDiscoveryPacket
		{
			public override byte[] AsBytes()
			{
			  byte[] res = new byte[data.Length];
				System.Array.Copy( data , res , data.Length );
				return res;
			}
			public override int Length() {return data.Length;}
			public static byte[] data = System.Text.Encoding.UTF8.GetBytes("GTestAdapter_service-call");
		}

		class ReplyPack : GTestCommon.Utils.ServiceDiscovery.ServiceDiscoveryAnswerPacket
		{
			public ReplyPack(ushort port,string name,System.Net.IPAddress ServiceIP)
			{
				m_port = port;
				m_name = name;
				m_IP = ServiceIP;
			}

			public override byte[] AsBytes()
			{
			  byte[] res = new byte[data.Length+2];
				System.Array.Copy( data , res , data.Length );
				res[data.Length+0] = (byte)(m_port);
				res[data.Length+1] = (byte)(m_port>>8);
				return res;
			}

			public override int Length() {return data.Length+2;}

			public override ServiceModel GetServiceModel()
			{
			  ServiceModel res;
			  ServiceAddr sAddr;
				sAddr = new ServiceAddr( m_IP , (int)(uint)m_port );
				res = new ServiceModel( sAddr , m_name );
				return res;
			}

			private ushort m_port;
			private string m_name;
			private System.Net.IPAddress m_IP;

			private static byte[] data = System.Text.Encoding.UTF8.GetBytes("GTestAdapter_service-reply");
		}




		public ServiceDiscoveryPacket CreateDiscoveryPacket()
		{
			return new RequestPack();
		}

		public ServiceDiscoveryAnswerPacket CreateAnswerPacket()
		{
			return new ReplyPack( (ushort)m_port , m_name , System.Net.IPAddress.Any );
		}

		public ServiceDiscoveryAnswerPacket DeserializeAnswerPacket(byte[] Bytes, System.Net.IPAddress ServiceIP)
		{
		  ReplyPack res;
			res = new ReplyPack( (ushort)m_port , m_name , ServiceIP );
			// bytes must match exactly.
			if(!Bytes.Equals( res.AsBytes() ))
				return null;		// mismatch. Not ours.
			return res;
		}

		private uint m_port;
		private string m_name;

	}
}
