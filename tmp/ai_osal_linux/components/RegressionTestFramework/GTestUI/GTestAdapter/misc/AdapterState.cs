

namespace GTestAdapter
{
	public enum State
	{
		IDLE = 1,			// waiting for commands or connects.
		TESTING = 2,		// service is running. collecting results.
		STARTTEST = 3,		// test-start sent, waitinf for reply.
		UKNONWN = 4			// don't know if service is testing or not.
	};
}
