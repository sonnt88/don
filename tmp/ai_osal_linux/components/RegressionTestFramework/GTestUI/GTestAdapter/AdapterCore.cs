using System.Collections.Generic;


using GTestCommon;
using GTestCommon.Links;


/*
 * Adapter core.
 * 
 * normal process (without restarts)
 * 
 * 1. Adapter connects to target.
 * 1. UI connects to adapter.
 * 2. UI requests to get list of tests.
 * 3. If not yet known, adapter gets testlist.
 * 4. Adapter replies testlist.
 * 5. UI sends testselection-and-run command.
 * 6. Adapter sends selection-and-run to service.
 * 7. Adapter receives status messages from service.
 * 8. Results are forwarded to database and to UI.
 * 9. Service sends 'done', adapter sends completion.
 * 10. Adapter goes back to idle state.

 *    port  666 Adapter -> Service
 *    port 9999 UI -> Adapter
 *    port 14243 UI -> Adapter service announcer.


 */



namespace GTestAdapter
{

	public class AdapterCore : System.IDisposable
	{

		private class InitFailException : System.Exception
		{
			public InitFailException(string reason)
			 : base(reason)
			{
			}
		}

		public AdapterCore(bool interactiveWithGUI)
		{
			m_interactive = interactiveWithGUI;
			m_serviceLink = null;
			m_consoleLink = null;
			m_ttfisLink = null;
			m_UIlink = null;
			m_localConsoleLink = null;
			m_state = new States.StateDisconnected(this);
			m_quitSig = new System.Threading.ManualResetEvent(false);
			m_timerSig = new System.Threading.AutoResetEvent(false);
			m_connectSig_service = new System.Threading.AutoResetEvent(false);
			m_testsModelValid = false;
			m_testsModel = null;
			m_testRunsStarted = 0;
			m_testRunsCompleted = 0;
			m_returnVal = 0;
			m_unboundResults = new List<GTestCommon.Models.TestResultModel>();
			m_nonInteractiveTimeCount = 0;
		}

		public void Dispose()
		{
			if(m_running)
				throw new System.Exception("Cannot dispose while running (in another thread)");
		}

		/// <summary>
		/// Link object for connecting to the GTest-Service. Must be set before calling Run().
		/// </summary>
		public IQueuedPacketLink<IPacket> serviceLink
		{
			get{return m_serviceLink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change serviceLink while running.");
				m_serviceLink = value;
			}
		}

		/// <summary>
		/// Link Object for connections to the serial console output.
		/// </summary>
		public IQueuedPacketLink<string> consoleLink
		{
			get{return m_consoleLink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change consoleeLink while running.");
				m_consoleLink = value;
			}
		}

		/// <summary>
		/// Link Object for local console to type directly to the adapter. debug commands...
		/// </summary>
		public IQueuedPacketLink<string> localConsoleLink
		{
			get{return m_localConsoleLink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change localConsoleLink while running.");
				m_localConsoleLink = value;
			}
		}

		/// <summary>
		/// Link object for connection to TTFis to receive outputs.
		/// </summary>
		public IQueuedPacketLink<string> ttfisLink
		{
			get{return m_ttfisLink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change ttfisLink while running.");
				m_ttfisLink = value;
			}
		}

		/// <summary>
		/// check if the Adapter was created in non-interactive mode.
		/// </summary>
		public bool isInteractive
		{
			get{return m_interactive;}
		}

		/// <summary>
		/// Link object for connecting to the GTest-UI. Must be set before calling Run().
		/// </summary>
		public IQueuedPacketLink<IPacket> UIlink
		{
			get{return m_UIlink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change UIlink while running.");
				m_UIlink = value;
			}
		}

		/// <summary>
		/// adapter (and main programm) Mainloop. Uses calling thread.
		/// Will not run unless all resources have been added.
		/// </summary>
		public int Run()
		{
			try
			{
				if(m_running)
					throw new InitFailException("Already a thread in Run()");
				// set running
				m_running=true;
				m_errorMessage=null;
				// check tcp links. (Can live without the other two)
				if( m_serviceLink==null )
					{throw new InitFailException("cannot Run() unless service link is set up.");}
				if( m_interactive )
				{
					if( m_UIlink==null )
						{throw new InitFailException("cannot Run() unless UI link is set up (when interactive).");}
				}
#if LOCAL_CONSOLE_LINK
				if( m_localConsoleLink==null )
				{
					m_localConsoleLink = new LocalConsole();
				}
#endif
				m_quit = false;

				// make string buffers.
				m_output_gtest = null;
				m_output_console = null;
				m_output_ttfis = null;

				if( m_serviceLink == null )
					m_output_gtest = new LineBuffer();
				if( m_output_console == null )
					m_output_console = new LineBuffer();
				if( m_output_ttfis == null )
					m_output_ttfis = new LineBuffer();

				// make object for encapsulating UI link and state
				if( m_interactive )
					m_UIlinkState = new GTestAdapter.LinkStateUI( m_UIlink , Program.sm_settings.UIport );

				// create handler for events. Processing several event sources in one thread.
				m_waitObj = new WaitAnyHandler();
				m_waitObj.addHandler( onQuitSig , m_quitSig );
				m_waitObj.addHandler( onDataFromService , m_serviceLink.inQueue.getWaitHandle() );
				if( m_consoleLink != null )
					m_waitObj.addHandler( onDataFromConsole , m_consoleLink.inQueue.getWaitHandle() );
				if( m_ttfisLink != null )
					m_waitObj.addHandler( onDataFromTTFis , m_ttfisLink.inQueue.getWaitHandle() );
#if LOCAL_CONSOLE_LINK
				m_waitObj.addHandler( onDataFromLocalConsole , m_localConsoleLink.inQueue.getWaitHandle() );
#endif
				m_waitObj.addHandler( onConnectSigService , m_connectSig_service );

				// add handler which sets signal for service-link connect-event.
				m_serviceLink.connect += new ConnectEventHandler(processConnectEvent);

				// start the links
				if(m_interactive)
					m_UIlinkState.startUp( m_waitObj , processUIPackets );
			  bool sflag;
			  string srvAdr = Program.sm_settings.serviceNetworkAddress;
				sflag = m_serviceLink.Start( srvAdr );
				if(!sflag)
					throw new InitFailException("error starting service link to "+srvAdr+".");
#if LOCAL_CONSOLE_LINK
				if(m_consoleLink!=null)
				{
				  string conName = Program.sm_settings.comPortName;
					sflag = m_consoleLink.Start( conName );
					if(!sflag)
						throw new InitFailException("error starting serial console link on "+conName+" (bad port name or in use).");
				}
#endif
				if(m_ttfisLink!=null)
				{
					sflag = m_ttfisLink.Start("COM2@DRAGON");
					if(!sflag)
						throw new InitFailException("error starting TTFis link.");
				}
#if LOCAL_CONSOLE_LINK
				sflag = m_localConsoleLink.Start("");
				if(!sflag)
					throw new InitFailException("error starting local console.");
#endif

				// if running non-interactive, set a timer to drive
				if(!m_interactive)
				{
					new System.Threading.Timer( tmrCallbackFunc , this , 100u , 100u );
					m_waitObj.addHandler( onTimer100ms , m_timerSig );
				}

				PrintLocalLogged("GTestAdapter started.");

				// main loop
				while( !m_quit )		// TODO: ..... How do we quit?     // found this:  SetConsoleCtrlHandler(new HandlerRoutine(ConsoleCtrlCheck), true);
				{
					// handler waits for signals and calls linked handlers.
					m_waitObj.wait();
				}

				PrintLocalLogged("Adapter shutting down.");
			}
			catch(InitFailException ex)
			{
				m_errorMessage = ex.Message;
			}
			// stop them
			if(m_interactive)
				m_UIlink.Stop(true);
			m_serviceLink.Stop(true);
			if(m_consoleLink!=null)
				m_consoleLink.Stop(true);
			if(m_ttfisLink!=null)
				m_ttfisLink.Stop(false);
            if (m_localConsoleLink != null)
                m_localConsoleLink.Stop(true);

			m_running=false;

			return m_returnVal;
		}

		/// <summary>
		/// This stub function is linked to the quit signal (from Ctrl-C). 
		/// It just sets the quit flag which will cause the main loop to exit.
		/// </summary>
		private object onQuitSig()
		{
			m_quit=true;
			return null;
		}

		/// <summary>
		/// When running non-interactive, a timer is set up which triggers 10 times a second.
		/// This timer drives the non-interactive session.
		/// </summary>
		private object onTimer100ms()
		{
			m_nonInteractiveTimeCount ++ ;
			if( m_state is States.StateDisconnected )
			{
				// disconnected. Either never started, or device in reboot (normal in test).
				if( m_testRunsStarted>0 )
				{
					// testing was already started. We are waiting for a reconnect for one minute.
					if( m_nonInteractiveTimeCount >= 10*60 )
					{
						m_returnVal = 5;
						PrintLocalLogged("[TIMEOUT] Timeout waiting for service to reconnect. Testing interrupted.\n");
						return onQuitSig();
					}
				}else{
					// testing never started. Waiting for initial connect.
					// timeout after 60 seconds.
					if( m_nonInteractiveTimeCount >= 10*60 )
					{
						m_returnVal = 5;
						PrintLocalLogged("[TIMEOUT] Timeout waiting for service connection.\n");
						return onQuitSig();
					}
				}
				return null;
			}
			if( m_state is States.StateIdle )
			{
				// is idle. Check if tests were run or not.
				if( m_testRunsStarted > 0 )
				{
					// had already started some tests.
					// Either they failed to run all or they completed.
					// Time to quit.
					return onQuitSig();
				}
				// Idle and not testing.
				// do we know the tests?
				if(!m_testsModelValid)
				{
					// no we don't. Wait for tests data.
					if( m_nonInteractiveTimeCount > 50 )
					{
						m_returnVal = 5;
						PrintLocalLogged("[TIMEOUT] Timeout waiting for model data from service.\n");
						onQuitSig();return null;
					}
					return null;	// no. wait some. Data will hopefully arrive.
				}
				// have all test descriptions.
				// if list-only is requested, dump list now and exit.
				if( Program.sm_settings.onlyListTests )
				{
					m_testRunsStarted=1;	// so we 'think' tests are done.
					// ............ TODO: dump list.
					PrintLocalLogged("[TESTLIST] begin");
				  int g_idx;
					for( g_idx=0 ; g_idx<m_testsModel.TestGroupCount ; g_idx++ )
					{
					  GTestCommon.Models.TestGroupModel grp;
					  int t_idx;
						grp = m_testsModel[g_idx];
						for( t_idx=0 ; t_idx<grp.TestCount ; t_idx++ )
						{
						  GTestCommon.Models.Test tst;
							tst = grp[t_idx];
							PrintLocalLogged("[LIST] \""+grp.Name+"."+tst.Name+"\"");
						}
					}
					PrintLocalLogged("[TESTLIST] end");
					// object with all data is: m_testsModel;
					return onQuitSig();
				}
				// Make a run-tests packet.
				m_nonInteractiveTimeCount = 0;
				if(!triggerTestsSelectedBySettings())
				{
					m_returnVal = 5;
					PrintLocalLogged("Error starting tests.\n");
					return onQuitSig();
				}
				// tests are started.
				return null;
			}
			// is not idle state.
			// if is testing state, wait forever. Otherwise, limit wait to 2 seconds.
			// class hierarchy has disconnected below testing. So disconnect also has no timeout.
			// ..... todo: Shall we add a timeout for disconnected? Target rebooting and taking forever?
			if( m_state is GTestAdapter.States.StateTesting )
				m_nonInteractiveTimeCount = 0;

			if( m_nonInteractiveTimeCount >= 50 )
			{
				// timeout. todo: ..... report error?
				m_returnVal = 5;
				PrintLocalLogged("[TIMEOUT] Timeout error.\n");
				return onQuitSig();
			}

			return null;
		}

		/// <summary>
		/// Build a command packet to start tests.
		/// Scans the tests in the testmodel and compares the name against the pattern in the settings.
		/// </summary>
		private bool triggerTestsSelectedBySettings()
		{
		  List<uint> testList,invTestList;
			if(!m_testsModelValid)
				return false;
			// build list of tests that match the pattern.
			testList = new List<uint>();
			invTestList = new List<uint>();
			for( int j=0 ; j<m_testsModel.TestGroupCount ; j++ )
			{
			  GTestCommon.Models.TestGroupModel grp;
				grp = m_testsModel[j];
				for( int i=0 ; i<grp.TestCount ; i++ )
				{
				  GTestCommon.Models.Test tst;
				  string tname;
				  bool match;
					tst = grp[i];
					tname = grp.Name + "." + tst.Name ;
					match = GTestAdapter.WildCardPattern.fit( Program.sm_settings.testSelectPattern , tname );
					if(match)
						match = ! GTestAdapter.WildCardPattern.fit( Program.sm_settings.testDeselectPattern , tname );
					if( match )
						testList.Add( tst.ID );
					else
						invTestList.Add( tst.ID );
				}
			}
			// build packet
		  PacketRunTests pck;
			if( testList.Count <= invTestList.Count )
				pck = new PacketRunTests( testList , false , m_testsModel.CheckSum );
			else
				pck = new PacketRunTests( invTestList , true , m_testsModel.CheckSum );
			pck.numRepeats = Program.sm_settings.testIterations;
			// process as if this was sent from the UI.
			m_state = m_state.process_UI_packet( pck );
			// done.
			return (m_state is GTestAdapter.States.StateTestStarting);
		}

		// asynchronous callback from system timer. use signal to synchronize this.
		private void tmrCallbackFunc(object state)
		{
			((AdapterCore)state).m_timerSig.Set();
		}

		/// <summary>
		/// This function is linked to the serviceLink by the WaitAnyHandler 
		/// and called, whenever there is data to read on it.
		/// Will loop available packets and process using the current state.
		/// </summary>
		private object onDataFromService()
		{
		  IPacket pck;
			// packet from service link
			while(( pck = m_serviceLink.inQueue.Get(false) )!=null)
			{
				if( pck is Packet )
				{
					if( pck is PacketPcycleRequest )
					{
						pck = pck;
					}
					m_state = m_state.process_service_packet((Packet)pck);
					m_nonInteractiveTimeCount=0;	// reset counter. There was action.
					if(m_interactive)
						m_UIlinkState.informState(m_state);
				}
			}
			return null;
		}

		private object onDataFromConsole()
		{
		  string line;
			// line from serial console
			while(( line = m_consoleLink.inQueue.Get(false) )!=null)
			{
				processConsoleOrTTFisLine(line,false);
			}
			return null;
		}

		private object onDataFromTTFis()
		{
		  string line;
			// line from ttfis
			while(( line = m_ttfisLink.inQueue.Get(false) )!=null)
			{
				processConsoleOrTTFisLine(line,true);
			}
			return null;
		}

		private object onDataFromLocalConsole()
		{
#if LOCAL_CONSOLE_LINK
		  string line;
			// line from serial console
			while(( line = m_localConsoleLink.inQueue.Get(false) )!=null)
			{
				m_state = m_state.process_console_command(line);
			}
			if(m_interactive)
				m_UIlinkState.informState(m_state);
#endif
			return null;
		}

		private object onConnectSigService()
		{
			m_state = m_state.process_connectSigService(m_serviceLink.bIsConnected());
			m_nonInteractiveTimeCount=0;	// Disconnect/Reconnect resets this counter.
			if(m_interactive)
				m_UIlinkState.informState(m_state);
			return null;
		}

		/// <summary>
		/// Asynchronous event telling us if service connects/disconnects.
		/// Signal over, processes this synchronously in  onConnectSigService()
		/// </summary>
		/// <param name="conn">flag if connected</param>
		private void processConnectEvent(bool conn)
		{
			// do not process here. Just signal. Process in onConnectSigService()
			m_connectSig_service.Set();
		}

		internal bool addTestResult(GTestCommon.Models.TestResultModel result)
		{
		  GTestCommon.Models.Test tst = null ;
			if( m_testsModel==null )return false;
			tst = m_testsModel.getTestById(result.testID);
			if( tst==null )
			{
				errorToUI( "got testresult for unknown test ID "+result.testID.ToString() , true );
				return false;
			}
			// store result.
			if( tst.IterationCount >= m_testsModel.Repeat )
				m_testsModel.Repeat = tst.IterationCount+1;		// don't need this annoying limit. just push it.
			tst.AddResult( result , true );
			// Send to UI if connected.
			if( m_interactive && m_UIlink.bIsConnected() )	// ..... ask m_UIlink for connected??
			{
			  PacketTestResult upck = new PacketTestResult();
				upck.result = result;
				m_UIlinkState.putPacket(upck);
			}
			// send to local console.
		  string tstRes;
			if( result.success )
				tstRes = "PASS";
			else
				tstRes = "FAIL";
			PrintLocalLogged( "[{0}] \""+tst.Group.Name+"."+tst.Name+"\"" , tstRes );
			return true;
		}

		/// <summary>
		/// Callback from LinkStateUI, picks up and processes any messages from UI.
		/// </summary>
		private void processUIPackets()
		{
			foreach(IPacket pck in m_UIlinkState.packets() )
			{
				if( pck is Packet )
				{
					m_state = m_state.process_UI_packet((Packet)pck);
				}else if( pck is IsConnected )
				{
					m_state = m_state.process_connectSigUI( ((IsConnected)pck).isConnect );
				}
			}
			m_UIlinkState.informState(m_state);
		}

		private void processConsoleOrTTFisLine(string line,bool isTTFis)
		{
			if(!isTTFis)
				m_output_console.addLine(line,(uint)System.Environment.TickCount);
			else
				m_output_ttfis.addLine(line,(uint)System.Environment.TickCount);
		}

		internal void processBreak()
		{
			// Ctrl-C pressed.
			m_quitSig.Set();
		}

		public void makeUpNewUUID()
		{
			System.Random rnd = new System.Random();
			m_current_UUID = (ulong)System.Environment.TickCount;
			m_current_UUID += (uint)rnd.Next();
		}

		public void errorToUI(string message,bool storeIfUnavailable)
		{
			PrintLocal("Error: {0}",message);
			// .....
			if(m_interactive&&m_UIlink.bIsConnected())
				{}
		}

		public void PrintLocal(string format, params object[] arg)
		{
		  string line = string.Format(format,arg);
#if !LOCAL_CONSOLE_LINK
          System.Console.WriteLine(line);
#else
			if(m_localConsoleLink!=null)
				m_localConsoleLink.outQueue.Add(line);
#endif
		}

		public void PrintLocalLogged(string format, params object[] arg)
		{
			PrintLocal(format,arg);
			Program.PrintLog(format,arg);
		}

		public string errorMessage{get{return m_errorMessage;}}

		private bool m_running;			// indicates that run loop is active.
		internal int m_returnVal;		// return value when exiting process
		private string m_errorMessage;	// error message left by Run if failing.
		private bool m_quit;			// indicates to work-loop to exit.
		private System.Threading.EventWaitHandle m_quitSig;
		private System.Threading.EventWaitHandle m_connectSig_service;
		private System.Threading.EventWaitHandle m_timerSig;

		private WaitAnyHandler m_waitObj;

		// Links for connection to device under test.
		internal IQueuedPacketLink<IPacket> m_serviceLink;
		internal IQueuedPacketLink<string> m_consoleLink;
		internal IQueuedPacketLink<string> m_ttfisLink;
		private IQueuedPacketLink<string> m_localConsoleLink;

		private LineBuffer m_output_gtest;
		private LineBuffer m_output_console;
		private LineBuffer m_output_ttfis;

		// operation mode
		private bool m_interactive;
		private uint m_nonInteractiveTimeCount;

		// Link to user interface.
		private IQueuedPacketLink<IPacket> m_UIlink;

		internal GTestAdapter.LinkStateUI m_UIlinkState;

		// state flags and vars
		private States.State0 m_state;				// state. Idle, testing, starting-testing, ...
		internal uint m_testRunsStarted;		// number of complete runs triggered.
		internal uint m_testRunsCompleted;		// number of complete runs done.
		internal uint m_running_testID;
		internal ulong m_current_UUID;
		internal uint m_running_iteration;
		internal bool m_testsModelValid;		// indicate if we are sure the testmodel is valid.
		internal GTestCommon.Models.AllTestCases m_testsModel;
		internal List<GTestCommon.Models.TestResultModel> m_unboundResults;
		internal string m_serviceVersion;
		// results storage for current UUID

	}

}
