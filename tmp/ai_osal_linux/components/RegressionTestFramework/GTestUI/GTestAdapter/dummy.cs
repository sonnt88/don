
using GTestAdapter;
using GTestCommon;


namespace dummy
{
	/// <summary>
	/// Dummy functions to test / play around  with some of the functionality.
	/// </summary>
	public class Dummy
	{

		static void chgHdlr(bool conn)
		{
			System.Console.WriteLine("EVENT: {0}",(conn?"connected":"disconnected"));
			System.Console.WriteLine("   (mtid: {0})",System.Threading.Thread.CurrentThread.ManagedThreadId);
		}

		// try service-link    (other end: use testpeer.py
		static public int tryServiceLink()
		{
		  Queue<IPacket> down,up;
		  GTestCommon.Links.IQueuedPacketLink<IPacket> connection;
		  GTestCommon.Links.Packet pck;
		  string address = "127.0.0.1:666";
			System.Console.WriteLine("mtid: {0}",System.Threading.Thread.CurrentThread.ManagedThreadId);
			connection = new GTestCommon.Links.TcpPacketLink( new GTestCommon.Links.PacketFactory4service() );
			connection.connect += new GTestCommon.Links.ConnectEventHandler(chgHdlr);
			up = connection.inQueue;
			down = connection.outQueue;
			connection.Start(address);

			for(int i=0;i<15;i++)
			{
			  System.Random rnd = new System.Random();
			  byte a,b;
				System.Threading.Thread.Sleep(500);
				a=(byte)rnd.Next(0,128);
				b=(byte)rnd.Next(0,128);
				down.Add( new GTestCommon.Links.PacketAddNumbers(a,b) );
				pck = (GTestCommon.Links.Packet)up.Get(true);
				if( pck is GTestCommon.Links.PacketResultNumber )
					System.Console.WriteLine( "  {0}+{1}={2}" , a , b , ((GTestCommon.Links.PacketResultNumber)pck).c );
				if( i==8)
				{
					connection.Stop(true);
					System.Threading.Thread.Sleep(100);
					connection.Start(address);
				}
			}

			connection.Stop(false);
			System.Threading.Thread.Sleep(1000);
			return 0;
		}
/* Paste this in Gen2 console to get slow, endless output:

echo while [ \"\" = \"\" ]\; do   > muell
echo \ sleep 1               >> muell
echo \ echo Ping!            >> muell
echo \ sleep 1               >> muell
echo \ echo \\ \\ Pong!      >> muell
echo done                    >> muell
chmod 755 muell
./muell


 */
		static public int trySerialConsole()
		{
		  Queue<string> down,up;
		  GTestCommon.Links.IQueuedPacketLink<string> ser;
		  System.IO.Stream inp;
		  System.Threading.Thread thr;

			ser = new GTestAdapter.Links.SerialLink();
			up = ser.inQueue;
			down = ser.outQueue;
			ser.Start("COM2");
			inp = System.Console.OpenStandardInput();

			thr = new System.Threading.Thread(serTerThrProc);
			thr.Start(new object[]{inp,down});

			while(true)
			{
			  string line;
				line = up.Get(true);
				System.Console.WriteLine(line);
			}

		}
		static private void serTerThrProc(object d)
		{
		  System.IO.Stream inp;
		  Queue<string> down;
			inp = (System.IO.Stream)(((object[])d)[0]);
			down= (Queue<string>)   (((object[])d)[1]);
			while(true)
			{
			  string line;
				line = System.Console.ReadLine();
				down.Add(line);
			}
		}

		static public int tryTTFisDummy()
		{
		  GTestCommon.Links.IQueuedPacketLink<string> tt;

			tt = new GTestAdapter.Links.TTFisIO();
			tt.Start("COM2@DRAGON");

			while(true)
			{
			  string line = tt.inQueue.Get(true);
				System.Console.WriteLine(line);
			}

			//return 0;
		}

		static public int tryDatabase()
		{
			GTestAdapter.DataLogging.DatabaseWriter dl = new GTestAdapter.DataLogging.DatabaseWriter("C:\\muell\\testOutputs","192.168.3.192","test","123456");

			// set up a small model for trying.
		  GTestCommon.Models.AllTestCases tdat = new GTestCommon.Models.AllTestCases(false);
			tdat.CheckSum = (ulong)System.Environment.TickCount;
		  GTestCommon.Models.TestGroupModel tcas1 = tdat.AppendNewTestGroup( "testcase1" );
		  GTestCommon.Models.TestGroupModel tcas2 = tdat.AppendNewTestGroup( "testcase2" );
		  GTestCommon.Models.Test tt1 = tcas1.AppendNewTest("TEST1",1);
		  GTestCommon.Models.Test tt2 = tcas1.AppendNewTest("TEST2",2);
		  GTestCommon.Models.Test tt3 = tcas1.AppendNewTest("TEST3",3);
		  GTestCommon.Models.Test tt4 = tcas2.AppendNewTest("TEST2",4);
		  GTestCommon.Models.TestResultModel tres;

			tdat.Repeat = 2;
			tt1.AddResult( true );
			tt1.AddResult( false );
			tt2.AddResult( true );

			tres = tt1.GetResult(0);

			tdat.CheckSum = 0x074007EC;

			dl.storeResultSet( (ulong)0x07C007ECu , tdat );

			dl.Close();

			return 0;
		}

		static public int tryConsole()
		{
		  GTestCommon.Links.IQueuedPacketLink<string> io;
		  bool bQuit;
			io = new GTestCommon.Links.LocalConsole();

			io.Start(null);

			bQuit=false;
			while(!bQuit)
			{
			  string line;
				io.inQueue.getWaitHandle().WaitOne();
				while((line=io.inQueue.Get(false))!=null)
				{
					if( line.ToLower()=="quit" || line.ToLower()=="exit" )
						{bQuit=true;break;}
					io.outQueue.Add( string.Format("echo1: '{0}'",line) );
					io.outQueue.Add( string.Format("echo2: '{0}'",line) );
				}
			}
			io.Stop(true);
			return 0;
		}

		private class TstTextWriter : System.IO.TextWriter
		{
			public TstTextWriter()
			{
				m_enc = System.Text.Encoding.UTF8;
			}

			public override void Write(char value)
			{
				System.Console.Write(value);
			}

			public override System.Text.Encoding Encoding
				{get{return m_enc;}}

			private System.Text.Encoding m_enc;
		}

		static public int tryTextWriter()
		{
		  System.IO.TextWriter wri;
			wri = new TstTextWriter();
			// ..... check remove resources?

			wri.WriteLine("Zahl: {0}",42);

			return 0;
		}

	}
}