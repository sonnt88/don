using System.Collections.Generic;

using GTestCommon;
using GTestCommon.Links;



namespace GTestAdapter
{

    /// <summary>
    /// Class to contain the state of a specific link.
    /// </summary>
	public abstract class LinkState
	{
		public delegate void triggerCallback();

		public LinkState()
		{
			m_inBuffer = new System.Collections.Generic.Queue<IPacket>();
		}

		public abstract bool startUp(WaitAnyHandler waitObject,triggerCallback newPacketsCallback);

		public abstract void shutDown();

	    /// <summary>
	    /// Place a packet to be sent out on this link.
	    /// </summary>
		public abstract void putPacket(IPacket pck);

		public virtual System.Collections.Generic.IEnumerable<IPacket> packets()
		{
		  IPacket pck;
			while(m_inBuffer.Count>0)
			{
				pck = m_inBuffer.Dequeue();
				yield return pck;
			}
		}

	    /// <summary>
	    /// Method to link a callback and an event. Should call packets() and pick any waiting input packets.
	    /// </summary>
		protected virtual void addToWaitAnyHandler(WaitAnyHandler waitObject,triggerCallback cb)
		{
			m_callBack = cb;
		}

		protected triggerCallback m_callBack;
		protected System.Collections.Generic.Queue<IPacket> m_inBuffer;		// buffer for processsed packages. Should be picked up in callback.
	};

}


