using System.Collections.Generic;


using GTestCommon;
using GTestCommon.Links;


/*
 * Adapter core.
 * 
 * normal process (without restarts)
 * 
 * 1. Adapter connects to target.
 * 1. UI connects to adapter.
 * 2. UI requests to get list of tests.
 * 3. If not yet known, adapter gets testlist.
 * 4. Adapter replies testlist.
 * 5. UI sends testselection-and-run command.
 * 6. Adapter sends selection-and-run to service.
 * 7. Adapter receives status messages from service.
 * 8. Results are forwarded to database and to UI.
 * 9. Service sends 'done', adapter sends completion.
 * 10. Adapter goes back to idle state.


 *    port 4242 Adapter -> Service
 *    port 4243 UI -> Adapter
 *    port 14243 UI -> Adapter service announcer.


 */



namespace GTestAdapter
{

	public class AdapterCore : System.IDisposable
	{
		public const string VersionString = "GTestAdapter v0.0";


		private class InitFailException : System.Exception
		{
			public InitFailException(string reason)
			 : base(reason)
			{
			}
		}


		public AdapterCore()
		{
			m_serviceLink = null;
			m_consoleLink = null;
			m_ttfisLink = null;
			m_UIlink = null;
			m_localConsoleLink = null;
			m_state = State.UKNONWN;
			m_quitSig = new System.Threading.ManualResetEvent(false);
			m_connectSig_service = new System.Threading.AutoResetEvent(false);
			m_connectSig_UI = new System.Threading.AutoResetEvent(false);
			m_testsModelValid = false;
			m_testsModel = null;
			m_UIwaitingForTestList = 0;
			m_UIwaitingForState = 0;
			m_UIwaitingForAbort = 0;
			m_unboundResults = new List<GTestCommon.Models.TestResultModel>();
		}

		public void Dispose()
		{
			if(m_running)
				throw new System.Exception("Cannot dispose while running (in another thread)");
		}

		/// <summary>
		/// Link object for connecting to the GTest-Service. Must be set before calling Run().
		/// </summary>
		public IQueuedPacketLink<IPacket> serviceLink
		{
			get{return m_serviceLink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change serviceLink while running.");
				m_serviceLink = value;
			}
		}

		/// <summary>
		/// Link Object for connections to the serial console output.
		/// </summary>
		public IQueuedPacketLink<string> consoleLink
		{
			get{return m_consoleLink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change consoleeLink while running.");
				m_consoleLink = value;
			}
		}

		/// <summary>
		/// Link Object for local console to type directly to the adapter. debug commands...
		/// </summary>
		public IQueuedPacketLink<string> localConsoleLink
		{
			get{return m_localConsoleLink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change localConsoleLink while running.");
				m_localConsoleLink = value;
			}
		}

		/// <summary>
		/// Link object for connection to TTFis to receive outputs.
		/// </summary>
		public IQueuedPacketLink<string> ttfisLink
		{
			get{return m_ttfisLink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change ttfisLink while running.");
				m_ttfisLink = value;
			}
		}

		/// <summary>
		/// Link object for connecting to the GTest-UI. Must be set before calling Run().
		/// </summary>
		public IQueuedPacketLink<IPacket> UIlink
		{
			get{return m_UIlink;}
			set{
				if( m_running )
					throw new System.ArgumentException("cannot change UIlink while running.");
				m_UIlink = value;
			}
		}


		private delegate void handleSignal();


		/// <summary>
		/// adapter (and main programm) Mainloop. Uses calling thread.
		/// Will not run unless all resources have been added.
		/// </summary>
		public void Run()
		{
		  handleSignal[] handlers;
		  System.Threading.WaitHandle[] handles;
		  int numH;
			try
			{
				if(m_running)
					throw new InitFailException("Already a thread in Run()");
				// set running
				m_running=true;
				m_errorMessage=null;
				// check tcp links. (Can live without the other two)
				if( m_serviceLink==null || m_UIlink==null )
					{throw new InitFailException("cannot Run() unless links are set up.");}
				if( m_localConsoleLink==null )
				{
					m_localConsoleLink = new LocalConsole();
				}
				m_quit = false;

				// make string buffers.
				m_output_gtest = null;
				m_output_console = null;
				m_output_ttfis = null;

				if( m_serviceLink != null )
					m_output_gtest = new LineBuffer();
				if( m_output_console != null )
					m_output_console = new LineBuffer();
				if( m_output_ttfis != null )
					m_output_ttfis = new LineBuffer();

				// create array of handles for waiting.
				handles = new System.Threading.WaitHandle[8];
				handlers = new handleSignal[8];
				numH=0;
				handles[numH] = m_quitSig;
				handlers[numH] = new handleSignal(onQuitSig);
				numH++;
				handles[numH] = m_serviceLink.inQueue.getWaitHandle();
				handlers[numH] = new handleSignal(onDataFromService);
				numH++;
				handles[numH] = m_UIlink.inQueue.getWaitHandle();
				handlers[numH] = new handleSignal(onDataFromUI);
				numH++;
				if( m_consoleLink != null )
				{
					handles[numH] = m_consoleLink.inQueue.getWaitHandle();
					handlers[numH] = new handleSignal(onDataFromConsole);
					numH++;
				}
				if( m_ttfisLink != null )
				{
					handles[numH] = m_ttfisLink.inQueue.getWaitHandle();
					handlers[numH] = new handleSignal(onDataFromTTFis);
					numH++;
				}
				handles[numH] = m_localConsoleLink.inQueue.getWaitHandle();
				handlers[numH] = new handleSignal(onDataFromLocalConsole);
				numH++;
				handles[numH] = m_connectSig_service;
				handlers[numH] = new handleSignal(onConnectSigService);
				numH++;
				handles[numH] = m_connectSig_UI;
				handlers[numH] = new handleSignal(onConnectSigUI);
				numH++;

				// reduce handles arraysize for call for WaitAny
				if( numH<handles.Length )
				{
				  System.Threading.WaitHandle[] newHandles = new System.Threading.WaitHandle[numH];
					System.Array.Copy(handles,newHandles,numH);
					handles = newHandles;
				}

				// add event
				m_serviceLink.connect += new ConnectEventHandler(processConnectEvent);
				m_UIlink.connect += new ConnectEventHandler(processUiConnectEvent);

				// start the links
			  bool sflag;
			  string srvAdr = Program.sm_settings.serviceNetworkAddress;
				sflag = m_serviceLink.Start( srvAdr );
				if(!sflag)
					throw new InitFailException("error starting service link to "+srvAdr+".");
				sflag = m_UIlink.Start( Program.sm_settings.UIport );
				if(!sflag)
					throw new InitFailException("error starting UI link (bad port or port in use by other program).");
				if(m_consoleLink!=null)
				{
				  string conName = Program.sm_settings.comPortName;
					sflag = m_consoleLink.Start( conName );
					if(!sflag)
						throw new InitFailException("error starting serial console link on "+conName+" (bad port name or in use).");
				}
				if(m_ttfisLink!=null)
				{
					sflag = m_ttfisLink.Start("COM2@DRAGON");
					if(!sflag)
						throw new InitFailException("error starting TTFis link.");
				}
				sflag = m_localConsoleLink.Start("");
				if(!sflag)
					throw new InitFailException("error starting local console.");

				PrintLocal("GTestAdapter started.");

				// main loop
				while( !m_quit )		// TODO: ..... How do we quit?     // found this:  SetConsoleCtrlHandler(new HandlerRoutine(ConsoleCtrlCheck), true);
				{
				  int waitRes;
					// wait for:
					//   packet from any of the queues, quitsignal, or timeout.
					waitRes = System.Threading.WaitHandle.WaitAny(handles);
					handlers[waitRes].Invoke();		// branch to one of the 'onXxxx' functions.
				}

				PrintLocal("Adapter shutting down.");
			}
			catch(InitFailException ex)
			{
				m_errorMessage = ex.Message;
			}
			// stop them
			m_UIlink.Stop(true);
			m_serviceLink.Stop(true);
			if(m_consoleLink!=null)
				m_consoleLink.Stop(false);
			if(m_ttfisLink!=null)
				m_ttfisLink.Stop(false);
			m_localConsoleLink.Stop(true);

			m_running=false;
		}

		private void onQuitSig()
		{
			m_quit=true;
		}

		private void onDataFromService()
		{
		  IPacket pck;
			// packet from service link
			while(( pck = m_serviceLink.inQueue.Get(false) )!=null)
			{
				processServiceLinkPacket(pck);
			}
		}

		private void onDataFromConsole()
		{
		  string line;
			// line from serial console
			while(( line = m_consoleLink.inQueue.Get(false) )!=null)
			{
				processConsoleOrTTFisLine(line,false);
			}
		}

		private void onDataFromTTFis()
		{
		  string line;
			// line from ttfis
			while(( line = m_ttfisLink.inQueue.Get(false) )!=null)
			{
				processConsoleOrTTFisLine(line,true);
			}
		}

		private void onDataFromLocalConsole()
		{
		  string line;
			// line from serial console
			while(( line = m_localConsoleLink.inQueue.Get(false) )!=null)
			{
				processLocalConsoleLine(line);
			}
		}

		private void onDataFromUI()
		{
		  IPacket pck;
			// packet from UI link
			while(( pck = m_UIlink.inQueue.Get(false) )!=null)
			{
				processUIPacket(pck);
			}
		}

		private void onTimer()
		{
		}

		private void onConnectSigService()
		{
			// consider our testmodel invalid until further notice. But keep it. If we ack the checksum, it is good again.
			m_testsModelValid = false;
			state = State.UKNONWN;
			if( m_serviceLink.bIsConnected() )
			{
				// service reconnected. Send version and state query.
				PrintLocal("got connection to service.");
				m_serviceLink.outQueue.Add( new GTestCommon.Links.PacketGetVersion() );
				m_serviceLink.outQueue.Add( new GTestCommon.Links.PacketGetState() );
				if( m_testsModel==null )
					m_serviceLink.outQueue.Add( new PacketGetTestList() );
				else
					m_serviceLink.outQueue.Add( new PacketGetTestList() );		// ..... request only checksum?
				m_unboundResults.Clear();
			}else{
				// service disconnected.
				PrintLocal("lost service connection.");
				m_unboundResults.Clear();
			}
		}

		private void onConnectSigUI()
		{
			m_UIwaitingForTestList = 0;
			m_UIwaitingForState = 0;
			if( m_UIlink.bIsConnected() )
			{
				// A UI process connected.
				PrintLocal("UI process connected");
				// not sending info. UI must query that.
				// state
			}else{
				// The UI disconnected.
				PrintLocal("UI process left");
			}
		}

		/// <summary>
		/// Asynchronous event telling us if service connects/disconnects.
		/// Signal over, processes this synchronously in  onConnectSigService()
		/// </summary>
		/// <param name="conn">flag if connected</param>
		private void processConnectEvent(bool conn)
		{
			// do not process here. Just signal. Process in onConnectSigService()
			m_connectSig_service.Set();
		}

		/// <summary>
		/// Asynchronous event telling us if service connects/disconnects.
		/// Signal over, processes this synchronously in  onConnectSigService()
		/// </summary>
		/// <param name="conn">flag if connected</param>
		private void processUiConnectEvent(bool conn)
		{
			// do not process here. Just signal. Process in onConnectSigService()
			m_connectSig_UI.Set();
		}

		/// <summary>
		/// Process a single packet received from the service link.
		/// </summary>
		/// <param name="pck">reference to the packet.</param>
		private void processServiceLinkPacket(IPacket pck)
		{
			if(!(pck is Packet))
				return;
		  Packet p = (Packet)pck;
			if( p is PacketState )
			{
			  PacketState pp = (PacketState)p;
				// is state. Tells us if service is running or not.
				if( pp.state==0 )	// TODO: ..... this should be an enum.
				{
					if (state != State.IDLE)
					{
						state = State.IDLE;
						// TODO: clean this up!
						PrintLocal("switching to state IDLE.");
						if (m_UIlink.bIsConnected())
						{
							Packet rpck = new PacketState(0);
							rpck.reqSerial = m_UIwaitingForState;
							m_UIlink.outQueue.Add(rpck);
							m_UIwaitingForState = 0;
						}
					}
				}
				else
				{
					// drop results when jumping from idle or unknown to testing? ..... TODO.
					state = State.TESTING;
					m_running_testID = pp.testID;
					m_running_iteration = pp.iteration;
				}
				// was UI waiting for this?
				if( m_UIwaitingForState>0 && m_UIlink.bIsConnected() )
				{
				  Packet rpck = new PacketState( (byte)(m_state==State.TESTING ? 1 : 0 ) );
					rpck.reqSerial = m_UIwaitingForState;
					m_UIlink.outQueue.Add( rpck );
					m_UIwaitingForState = 0;
				}
			}else if( p is PacketTestList )
			{
			  PacketTestList pp = (PacketTestList)p;
				// is a new testlist.
				if ((m_testsModel == null) || (pp.data.CheckSum==0) || (pp.data.CheckSum != m_testsModel.CheckSum) )
				{
					PrintLocal("Received new list of tests. Checksum={0:X8}",pp.data.CheckSum);
					// ..... delete old first?
					m_testsModel = pp.data;
					m_testsModelValid = true;
					// signal something? UI?
					// ..... TODO: signal UI.
					// and forward this to UI.
					if( m_UIwaitingForTestList>0 && m_UIlink.bIsConnected() )
					{
						// UI was waiting for this.
					  PacketTestList outp = new PacketTestList();
						outp.reqSerial = m_UIwaitingForTestList;
						outp.data = m_testsModel;
						m_UIlink.outQueue.Add(outp);
						m_UIwaitingForTestList = 0;
					}
					// now process the unbound results.
					foreach( GTestCommon.Models.TestResultModel res in m_unboundResults )
					{
						addTestResult( res );
					}
					m_unboundResults.Clear();
				}
			}else if( p is PacketAckTestRun )
			{
			  PacketAckTestRun pp = (PacketAckTestRun)p;
				if(pp.successfulTestStart)
				{
					state = State.TESTING;
					// ..... ?
				}else{
					state = State.IDLE;
					// ..... ?
				}
			}else if( p is PacketVersion )
			{
				m_serviceVersion = ((PacketVersion)p).version ;
				// ..... what do we do? check and throw if mismatch?
			}else if( p is PacketResultNumber )
			{
				PrintLocal("add result: {0}",((PacketResultNumber)p).c);
			}else if( p is PacketAbortAck )
			{
			  PacketAbortAck pp = (PacketAbortAck)p;
				// is an abort ack. Set state to idle
				PrintLocal("testrun aborted.");
				// send abort ack and set to idle.
				if( (m_state==State.TESTING||m_state==State.STARTTEST) )
				{
					m_state = State.IDLE;
					if( m_UIlink.bIsConnected() )
					{
					  Packet rpck = new PacketAbortAck();
						rpck.reqSerial = m_UIwaitingForAbort;
						m_UIlink.outQueue.Add(rpck);
					}
				}
				m_UIwaitingForAbort = 0;
				state = State.IDLE;
			}else if( p is PacketTestResult )
			{
			  PacketTestResult pp = (PacketTestResult)p;
				// is a result. Place in results set.
				if(! addTestResult(pp.result) )
					m_unboundResults.Add(pp.result);
			}else if( p is PacketAllTestrunsDone )
			{
				state = State.IDLE;		// UI is informed by setter of 'state'.
				PrintLocal("testrun completed.");
			}else if( p is PacketUpTestOutput )
			{
			  PacketUpTestOutput pp = (PacketUpTestOutput)p;
//				System.Console.WriteLine("DEBUG: output line: "+pp.output);
				// TODO: ..... output data must be logged to file and database.
				m_UIlink.outQueue.Add(pp);
		  }
		}

		private bool addTestResult(GTestCommon.Models.TestResultModel result)
		{
		  GTestCommon.Models.Test tst = null ;
			if( m_testsModel==null )return false;
			tst = m_testsModel.getTestById(result.testID);
			if( tst==null )
			{
				errorToUI( "got testresult for unknown test ID "+result.testID.ToString() , true );
				return false;
			}
			// store result.
			if( tst.IterationCount >= m_testsModel.Repeat )
				m_testsModel.Repeat = tst.IterationCount+1;		// don't need this annoying limit. just push it.
			tst.AddResult( result , true );
			// Send to UI if connected.
			if( m_UIlink.bIsConnected() )
			{
			  PacketTestResult upck = new PacketTestResult();
				upck.result = result;
				m_UIlink.outQueue.Add(upck);
			}
			return true;
		}

		/// <summary>
		/// Process a single packet received from the UI.
		/// </summary>
		/// <param name="pck">Reference to the packet.</param>
		private void processUIPacket(IPacket pck)
		{
			if(!(pck is Packet))
				return;
		  Packet p = (Packet)pck;
			if(p.serial==0)p.serial=0xFFFFFFFFu;		// dummy in case UI does not set the serials.
			if( p is PacketGetVersion )
			{
				// ..... Is UI requesting our version or that of the service?
				m_UIlink.outQueue.Add( new PacketVersion(VersionString) );
			}else if( p is PacketGetState )
			{
				// ..... use enum here.
				if( state!=State.UKNONWN )
				{
				  byte st = (byte)( state==State.IDLE ? 0 : 1 );
					m_UIlink.outQueue.Add( new PacketState( st ) );
					m_UIwaitingForState = 0;
				}else{
					m_UIwaitingForState = p.serial;
					m_serviceLink.outQueue.Add( new PacketGetState() );
				}
			}else if( p is PacketGetTestList )
			{
				if( m_testsModelValid )
				{
				  PacketTestList res;
					// we are sure. Send.
					res = new PacketTestList();
					res.data = m_testsModel;
					m_UIlink.outQueue.Add( res );
					m_UIwaitingForTestList = 0;
				}else{
					// don't know.
					m_UIwaitingForTestList = p.serial;
					if( m_serviceLink.bIsConnected() )	// if not connected, we will do so again in reconnect event...
					{
						if( m_testsModel==null )
						{
							// know nothing. request for ourselves.
							m_serviceLink.outQueue.Add( new PacketGetTestList() );
						}else{
							m_serviceLink.outQueue.Add( new PacketGetTestList() );		// ..... request only checksum?
						}
					}
				}
			}else if( p is PacketRunTests )
			{
			  PacketRunTests pp = (PacketRunTests)p;
				// Work to do. UI Is requesting to start tests.
				// ..... check checksum?
				if( state!=State.IDLE )
				{
					errorToUI("Cannot start testing. Not ready.",false);
					return;
				}
				if( !m_serviceLink.bIsConnected() )
				{
					errorToUI("Cannot start testing. Service not available (waiting to reconnect).",false);
					return;
				}
				if( m_testsModel==null || !m_testsModelValid )
				{
					errorToUI("Cannot start testing. Not having a valid testmodel.",false);	// ...... TODO: shut up and get it???
					return;
				}
				m_UIwaitingForTestStart = p.serial;
				state = State.STARTTEST;
				// drop all current results, set new UUID, start tests.
				m_testsModel.ResetExecutionState();
				m_unboundResults.Clear();
				makeUpNewUUID();
				// start service now.
				PrintLocal( "starting testrun. {0} IDs in list, repeat={1}, invert={2}" , pp.tests.Count , pp.numRepeats , pp.invertSelection?"true":"false" );
			  PacketRunTests dpck;
				dpck = new PacketRunTests(pp.tests,pp.invertSelection,pp.testSetChecksum);
				dpck.numRepeats = pp.numRepeats;
				if(dpck.numRepeats<1)
					dpck.numRepeats=1;
				m_testsModel.Repeat = (int)dpck.numRepeats;	// store in our model.
				m_serviceLink.outQueue.Add( dpck );
			}else if( p is PacketAbortTestsReq )
			{
				// UI requests to abort testing.
				// pass this on without checking if we believe that we are testing.
				m_UIwaitingForAbort = p.serial;
				if( m_serviceLink.bIsConnected() )
				{
					PrintLocal("try abort testrun.");
					m_serviceLink.outQueue.Add( new PacketAbortTestsReq() );
				}
			}else if( p is PacketAddNumbers )
			{
			  PacketAddNumbers pp = (PacketAddNumbers)p;
				m_UIlink.outQueue.Add( new PacketResultNumber( (byte)(pp.a+pp.b) ) );
			}else if( p is PacketGetAllResults )
			{
				// UI wants all our stored test results.
				if( m_testsModel!=null && m_testsModelValid )
				{
					// Blast out all results.
					for( int i=0 ; i<m_testsModel.TestGroupCount ; i++ )
					{
					  GTestCommon.Models.TestGroupModel tstGrp;
						tstGrp = m_testsModel[i];
						for( int j=0 ; j<tstGrp.TestCount ; j++ )
						{
						  GTestCommon.Models.Test tst;
							tst = tstGrp[j];
							foreach( GTestCommon.Models.TestResultModel res in tst.iterResults() )
							{
							  PacketTestResult tr_pck = new PacketTestResult();
								tr_pck.reqSerial = p.serial;
								tr_pck.result = res;
								m_UIlink.outQueue.Add(tr_pck);
							}
						}
					}
				}
				// and close with done-packet
			  PacketSendAllResultsDone trd_pck = new PacketSendAllResultsDone();
				trd_pck.reqSerial = p.serial;
				m_UIlink.outQueue.Add(trd_pck);
			}else{
				// unknown. drop.
				errorToUI(string.Format("unexpected packet type {0} from UI.",p.ID),false);
			}
		}

		private void processConsoleOrTTFisLine(string line,bool isTTFis)
		{
			if(!isTTFis)
				m_output_console.addLine(line,(uint)System.Environment.TickCount);
			else
				m_output_ttfis.addLine(line,(uint)System.Environment.TickCount);
		}

		private void processLocalConsoleLine(string line)
		{
			line = line.Trim();
			if(line=="")return;
			if( line.ToLower()=="help" || line=="?" )
			{
				localConsoleLink.outQueue.Add("available commands:");
				localConsoleLink.outQueue.Add("  quit              Exit the adapter.");
				localConsoleLink.outQueue.Add("  help              Show this help.");
				return;
			}
			if( line.ToLower()=="quit" || line.ToLower()=="exit" )
				{m_quitSig.Set();return;}
		  string resp;
			resp = string.Format("unknown command '{0}'. Use 'help' for list of commands.",line);
			localConsoleLink.outQueue.Add(resp);
		}

		internal void processBreak()
		{
			// Ctrl-C pressed.
			m_quitSig.Set();
		}

		private State state
		{
			get{return m_state;}
			set
			{
				if(m_state!=value)
				{
				  State previous = m_state;
					m_state=value;
					if(m_UIlink.bIsConnected())
					{
						if( m_state==State.IDLE && previous==State.STARTTEST )
						{
							// changing to idle. Tell UI that testing is done.
							m_UIlink.outQueue.Add(new PacketAbortTestsReq());
						}
						if( m_state==State.IDLE && previous==State.TESTING )
						{
							// changing to idle. Tell UI that testing is done.
							m_UIlink.outQueue.Add(new PacketAllTestrunsDone());
						}
						if( m_state==State.TESTING && previous==State.STARTTEST )
						{
							// changing from starttest to testing. Tell UI.
							m_UIlink.outQueue.Add(new PacketAckTestRun(true));
						}
					}
				}
			}
		}

		public void makeUpNewUUID()
		{
			System.Random rnd = new System.Random();
			m_current_UUID = (ulong)System.Environment.TickCount;
			m_current_UUID += (uint)rnd.Next();
		}

		public void PrintLocal(string format, params object[] arg)
		{
		  string line = string.Format(format,arg);
			m_localConsoleLink.outQueue.Add(line);
		}

		public void errorToUI(string message,bool storeIfUnavailable)
		{
			PrintLocal("Error: {0}",message);
			// .....
			if(m_UIlink.bIsConnected())
				{}
		}

		public enum State
		{
			IDLE = 1,			// waiting for commands or connects.
			TESTING = 2,		// service is running. collecting results.
			STARTTEST = 3,		// test-start sent, waitinf for reply.
			UKNONWN = 4			// don't know if service is testing or not.
		};

		public string errorMessage{get{return m_errorMessage;}}

		private bool m_running;			// indicates that run loop is active.
		private string m_errorMessage;	// error message left by Run if failing.
		private bool m_quit;			// indicates to work-loop to exit.
		private System.Threading.EventWaitHandle m_quitSig;
		private System.Threading.EventWaitHandle m_connectSig_service;
		private System.Threading.EventWaitHandle m_connectSig_UI;

		// Links for connection to device under test.
		private IQueuedPacketLink<IPacket> m_serviceLink;
		private IQueuedPacketLink<string> m_consoleLink;
		private IQueuedPacketLink<string> m_ttfisLink;
		private IQueuedPacketLink<string> m_localConsoleLink;

		private LineBuffer m_output_gtest;
		private LineBuffer m_output_console;
		private LineBuffer m_output_ttfis;

		// Link to user interface.
		private IQueuedPacketLink<IPacket> m_UIlink;

		// state flags and vars
		private State m_state;				// state. Idle, testing, starting-testing, ...
		private uint m_running_testID;
		private ulong m_current_UUID;
		private uint m_running_iteration;
		private bool m_testsModelValid;		// indicate if we are sure the testmodel is valid.
		private GTestCommon.Models.AllTestCases m_testsModel;
		private List<GTestCommon.Models.TestResultModel> m_unboundResults;
		private string m_serviceVersion;
		// results storage for current UUID

		// flags for keeping outstanding requests from UI.
		private uint m_UIwaitingForTestList;
		private uint m_UIwaitingForState;
		private uint m_UIwaitingForTestStart;
		private uint m_UIwaitingForAbort;
	}



}

