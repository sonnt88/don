using GTestCommon;
using GTestCommon.Links;


namespace GTestAdapter.States
{
	public class StateUnknown : State0
	{
		public StateUnknown(GTestAdapter.AdapterCore context) : base(context)
		{
		}

		public override State0 process_UI_packet(Packet pck)
		{
			if( pck is PacketRunTests )
			{
				// oops. Seem to be idle. Let's assume that, and process as idle.
			  State0 newState;
				newState = new States.StateIdle(m_ctx);
				newState = newState.process_UI_packet(pck);
				return newState;
			}
			return base.process_UI_packet(pck);
		}

		public override State0 process_service_packet(Packet pck)
		{
			if( pck is PacketState )
			{
			  PacketState pp = (PacketState)pck;
				if( pp.state == 1 )
				{
					// drop results when jumping from unknown to testing? ..... TODO.
					m_ctx.m_running_testID = pp.testID;
					m_ctx.m_running_iteration = pp.iteration;
					return new States.StateTesting(m_ctx);
				}else{
					return new States.StateIdle(m_ctx);
				}
			}else if( pck is PacketAbortAck )
			{
				// seems to be idle now.
				return new States.StateIdle(m_ctx);
			}else if( pck is PacketAllTestrunsDone )
			{
				// seems to be idle now.
				return new States.StateIdle(m_ctx);
			}
			return base.process_service_packet(pck);
		}

		public override State0 process_console_command(string line)
		{
			return base.process_console_command(line);
		}

		public override State0 process_connectSigService(bool connected)
		{
			return base.process_connectSigService(connected);
		}

		public override State0 process_connectSigUI(bool connected)
		{
			return base.process_connectSigUI(connected);
		}

	}
}
