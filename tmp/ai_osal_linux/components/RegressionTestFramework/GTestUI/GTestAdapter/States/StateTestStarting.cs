using GTestCommon;
using GTestCommon.Links;


namespace GTestAdapter.States
{
	public class StateTestStarting : StateTesting
	{
		public StateTestStarting(GTestAdapter.AdapterCore context) : base(context)
		{
		}

		public override State0 process_UI_packet(Packet pck)
		{
			return base.process_UI_packet(pck);
		}

		public override State0 process_service_packet(Packet pck)
		{
			if( pck is PacketState )
			{
			  PacketState pp = (PacketState)pck;
				if( pp.state == 1 )
				{
					// testing. Has really started. Inform.
					Program.PrintLocalLogged("Testing has started.\n");
					m_ctx.m_UIlinkState.putPacket( new PacketAckTestRun(true) );
					m_ctx.m_running_testID = pp.testID;
					m_ctx.m_running_iteration = pp.iteration;
					//m_ctx.m_testRunsStarted += 1;    no. was already counted when changing from Idle to TestStarting
					return new States.StateTesting(m_ctx);
				}else{
					// not testing. Means starting is aborted.
					m_ctx.m_UIlinkState.putPacket( new PacketAbortTestsReq() );
					return new States.StateIdle(m_ctx);
				}
			}else if( pck is PacketAckTestRun )
			{
			  PacketAckTestRun pp = (PacketAckTestRun)pck;
				if(pp.successfulTestStart)
				{
					// switch from TestStarting to Testing
					Program.PrintLocalLogged("Testing has started.\n");
					return new States.StateTesting(m_ctx);
				}else{
					// ..... todo: set return value for non-interactive?
					Program.PrintLocalLogged("ERROR starting testrun. Service NACK.\n");
					return new States.StateIdle(m_ctx);
				}
			}
			return base.process_service_packet(pck);
		}

		public override State0 process_console_command(string line)
		{
			return base.process_console_command(line);
		}

		public override State0 process_connectSigService(bool connected)
		{
			return base.process_connectSigService(connected);
		}

		public override State0 process_connectSigUI(bool connected)
		{
			return base.process_connectSigUI(connected);
		}

	}
}
