using GTestCommon;
using GTestCommon.Links;


namespace GTestAdapter.States
{
	public class StateIdle : State0
	{
		public StateIdle(GTestAdapter.AdapterCore context) : base(context)
		{
		}

		public override State0 process_UI_packet(Packet pck)
		{
			if( pck is PacketRunTests )
			{
			  PacketRunTests pp = (PacketRunTests)pck;
				// Work to do. UI Is requesting to start tests.
				// ..... check checksum?
				if( m_ctx.m_testsModel==null || !m_ctx.m_testsModelValid )
				{
					m_ctx.errorToUI("Cannot start testing. Not having a valid testmodel.",false);	// ...... TODO: shut up and get it???
					return this;
				}
				// drop all current results, set new UUID, start tests.
				m_ctx.m_testsModel.ResetExecutionState();
				m_ctx.m_unboundResults.Clear();
				m_ctx.makeUpNewUUID();
				// start service now.
				Program.PrintLocal( "starting testrun. {0} IDs in list, repeat={1}, invert={2}" , pp.tests.Count , pp.numRepeats , pp.invertSelection?"true":"false" );
			  PacketRunTests dpck;
				dpck = new PacketRunTests( pp.tests , pp.invertSelection , pp.testSetChecksum );
				dpck.numRepeats = pp.numRepeats;
				if(dpck.numRepeats<1)
					dpck.numRepeats=1;
				m_ctx.m_testsModel.Repeat = (int)dpck.numRepeats;	// store in our model.
				m_ctx.m_testRunsStarted ++;
				m_ctx.m_serviceLink.outQueue.Add( dpck );
				return new States.StateTestStarting(m_ctx);
			}
			return base.process_UI_packet(pck);
		}

		public override State0 process_service_packet(Packet pck)
		{
			if( pck is PacketState )
			{
			  PacketState pp = (PacketState)pck;
				if( pp.state == 1 )
				{
					// testing. Directly to testing means we got informed. ..... Warning?
					Program.PrintLocalLogged("Testing has started.\n");
					// drop results when jumping from idle to testing? ..... TODO.
					m_ctx.m_running_testID = pp.testID;
					m_ctx.m_running_iteration = pp.iteration;
					return new States.StateTesting(m_ctx);
				}
			}
			return base.process_service_packet(pck);
		}

		public override State0 process_console_command(string line)
		{
			return base.process_console_command(line);
		}

		public override State0 process_connectSigService(bool connected)
		{
			return base.process_connectSigService(connected);
		}

		public override State0 process_connectSigUI(bool connected)
		{
			return base.process_connectSigUI(connected);
		}

	}
}
