using GTestCommon;
using GTestCommon.Links;


namespace GTestAdapter.States
{
	public class StateTesting : State0
	{
		public StateTesting(GTestAdapter.AdapterCore context) : base(context)
		{
		}

		public override State0 process_UI_packet(Packet pck)
		{
			if( pck is PacketAbortTestsReq )
			{
				Program.PrintLocal("try to abort testrun.");
				// is passed down in baseclass.
				// The point is to always pass this, even if we don't beleive the state is 'testing'.
			}
			return base.process_UI_packet(pck);
		}

		public override State0 process_service_packet(Packet pck)
		{
			if( pck is PacketState )
			{
				if( ((PacketState)pck).state == 0 )
				{
					// not testing. Means we are done. Send all-done.
					m_ctx.m_UIlinkState.putPacket( new PacketAllTestrunsDone() );
					return new States.StateIdle(m_ctx);
				}
			}else if( pck is PacketAbortAck )
			{
				// testing aborted. Set to idle.
				Program.PrintLocal("testrun aborted.");
				m_ctx.m_UIlinkState.putPacket( new PacketAbortAck() );
				return new States.StateIdle(m_ctx);
			}else if( pck is PacketAllTestrunsDone )
			{
				Program.PrintLocalLogged("Testing has finished.\n");
				m_ctx.m_testRunsCompleted += 1;
				return new States.StateIdle(m_ctx);
			}
			return base.process_service_packet(pck);
		}

		public override State0 process_console_command(string line)
		{
			return base.process_console_command(line);
		}

		public override State0 process_connectSigService(bool connected)
		{
			return base.process_connectSigService(connected);
		}

		public override State0 process_connectSigUI(bool connected)
		{
			return base.process_connectSigUI(connected);
		}

	}
}
