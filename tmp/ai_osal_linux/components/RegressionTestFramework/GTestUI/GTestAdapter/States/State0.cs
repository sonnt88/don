using GTestCommon;
using GTestCommon.Links;



namespace GTestAdapter.States
{
	public class State0
	{
		public State0(GTestAdapter.AdapterCore context)
		{
			m_ctx = context;
		}

		/// <summary>
		/// Process a packet from UI.
		/// </summary>
		/// <param name="pck">reference to the packet.</param>
		/// <returns>new state</returns>
		public virtual State0 process_UI_packet(Packet pck)
		{
			if( pck is PacketGetTestList )
			{
				if( m_ctx.m_testsModelValid )
				{
					// we are sure. Send.
					m_ctx.m_UIlinkState.sendTestList(m_ctx.m_testsModel);
				}else{
					// don't know. Request from service.
					if(!(this is States.StateDisconnected))	// if not connected, we will do so again in reconnect event...
					{
						if( m_ctx.m_testsModel==null )
						{
							// know nothing. request for ourselves.
							m_ctx.m_serviceLink.outQueue.Add( new PacketGetTestList() );
						}else{
							m_ctx.m_serviceLink.outQueue.Add( new PacketGetTestList() );		// ..... request only checksum?
						}
					}
				}
			}else if( pck is PacketAbortTestsReq )
			{
				// UI requests to abort testing.
				// pass this on without checking if we believe that we are testing.
				m_ctx.m_serviceLink.outQueue.Add( new PacketAbortTestsReq() );
			}else if( pck is PacketAddNumbers )
			{
			  PacketAddNumbers pp = (PacketAddNumbers)pck;
				m_ctx.m_UIlinkState.putPacket( new PacketResultNumber( (byte)(pp.a+pp.b) ) );
			}else if( pck is PacketGetAllResults )
			{
				// UI wants all our stored test results.
				if( m_ctx.m_testsModel!=null && m_ctx.m_testsModelValid )
					m_ctx.m_UIlinkState.sendTestResults(m_ctx.m_testsModel);	// Blast out all results.
			}
			return this;
		}

		public virtual State0 process_service_packet(Packet pck)
		{
			if( pck is PacketVersion )
			{
				m_ctx.m_serviceVersion = ((PacketVersion)pck).version ;
				// ..... what do we do? check and throw if mismatch?
			}else if( pck is PacketResultNumber )
			{
			  PacketResultNumber pp = (PacketResultNumber)pck;
				Program.PrintLocal("add result: {0}",pp.c);
			}else if( pck is PacketTestResult )
			{
			  PacketTestResult pp = (PacketTestResult)pck;
				// is a result. Place in results set.
				if( m_ctx.addTestResult(pp.result) )
				{
					// stored in tests-model.
				}else{
					// cannot assign this (yet?). Store in unbound results.
					m_ctx.m_unboundResults.Add(pp.result);
				}
			}else if( pck is PacketTestList )
			{
			  PacketTestList pp = (PacketTestList)pck;
				// is a new testlist.
				if ((m_ctx.m_testsModel == null) || (pp.data.CheckSum==0) || (pp.data.CheckSum != m_ctx.m_testsModel.CheckSum) )
				{
					Program.PrintLocal("Received new list of tests. Checksum={0:X8}",pp.data.CheckSum);
					// ..... delete old first?
					m_ctx.m_testsModel = pp.data;
					m_ctx.m_testsModelValid = true;
					// signal something? UI?
					// ..... TODO: signal UI.
					// and forward this to UI.
					if( m_ctx.isInteractive && m_ctx.m_UIlinkState.waitForTestList )
					{
						// UI was waiting for this.
					  PacketTestList outp = new PacketTestList();
						outp.data = m_ctx.m_testsModel;
						m_ctx.m_UIlinkState.putPacket(outp);
					}
					// now process the unbound results. Should all fit now.
					foreach( GTestCommon.Models.TestResultModel res in m_ctx.m_unboundResults )
					{
						m_ctx.addTestResult( res );
					}
					m_ctx.m_unboundResults.Clear();
				}
			}else if( pck is PacketUpTestOutput )
			{
			  PacketUpTestOutput pp = (PacketUpTestOutput)pck;
//				System.Console.WriteLine("DEBUG: output line: "+pp.output);
				// TODO: ..... output data must be logged to file and database.
				if(m_ctx.isInteractive)
					m_ctx.m_UIlinkState.putPacket(pp);
			}else if( pck is PacketPcycleRequest )
			{
				// service requests we power-cycle.
				// Call the external program for this purpose.
			  GTestAdapter.ExtProgLauncher.ExtProgLauncher_Result result;
				result = GTestAdapter.ExtProgLauncher.callProgram(Program.sm_settings.filedata_powercycleConfig);
				// ..... check result???
			}
			return this;
		}

		public virtual State0 process_console_command(string line)
		{
			line = line.Trim();
			if( line.ToLower()=="help" || line=="?" )
			{
				Program.PrintLocal("available commands:");
				Program.PrintLocal("  quit              Exit the adapter.");
				Program.PrintLocal("  help              Show this help.");
			}else if( line.ToLower()=="quit" || line.ToLower()=="exit" )
			{
				m_ctx.processBreak();
			}else if(line!="")
			{
				Program.PrintLocal("unknown command '{0}'. Use 'help' for list of commands.",line);
			}
			return this;
		}

		public virtual State0 process_connectSigService(bool connected)
		{
			if(!connected)
			{
				// disconnecting
				Program.PrintLocal("lost service connection.");
				m_ctx.m_unboundResults.Clear();
				return new States.StateDisconnected(m_ctx);
			}else{
			}
			return this;
		}

		public virtual State0 process_connectSigUI(bool connected)
		{
			return this;
		}

		protected GTestAdapter.AdapterCore m_ctx;
	}
}
