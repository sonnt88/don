using GTestCommon;
using GTestCommon.Links;


namespace GTestAdapter.States
{
	public class StateDisconnected : StateTesting
	{
		public StateDisconnected(GTestAdapter.AdapterCore context) : base(context)
		{
		}

		public override State0 process_UI_packet(Packet pck)
		{
			return base.process_UI_packet(pck);
		}

		public override State0 process_service_packet(Packet pck)
		{
			// ..... should not receive any packets here.
			return base.process_service_packet(pck);
		}

		public override State0 process_console_command(string line)
		{
			return base.process_console_command(line);
		}

		public override State0 process_connectSigService(bool connected)
		{
			if( connected )
			{
				// service reconnected. Send version and state query.
				Program.PrintLocal("got connection to service.");
				m_ctx.m_serviceLink.outQueue.Add( new GTestCommon.Links.PacketGetVersion() );
				m_ctx.m_serviceLink.outQueue.Add( new GTestCommon.Links.PacketGetState() );
				if( m_ctx.m_testsModel==null )
					m_ctx.m_serviceLink.outQueue.Add( new PacketGetTestList() );
				else
					m_ctx.m_serviceLink.outQueue.Add( new PacketGetTestList() );		// ..... request only checksum?
				m_ctx.m_unboundResults.Clear();
				// switch state to unknown
				return new States.StateUnknown(m_ctx);
			}
			return base.process_connectSigService(connected);
		}

		public override State0 process_connectSigUI(bool connected)
		{
			return base.process_connectSigUI(connected);
		}

	}
}
