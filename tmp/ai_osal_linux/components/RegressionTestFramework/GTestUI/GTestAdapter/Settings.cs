


namespace GTestAdapter
{

	public class Settings
	{
		public Settings()
		{
		}

		private const string DEFAULT_SERVICE_ADDR  = "127.0.0.1:666";
		private const string DEFAULT_UIPORT        = "9999";
		private const string DEFAULT_TESTPATTERN   = "*";
		private const string DEFAULT_XCL_PATTERN   = "*.DISABLED*";
		private const int    DEFAULT_ITERS         = 1;
		private const string DEFAULT_PCYCLE_CONF   = null;
		private const string DEFAULT_DATA_NETADDR  = "localhost";
		private const string DEFAULT_DATA_USER     = "test";
		private const string DEFAULT_DATA_PASS     = "123456";
		private const string DEFAULT_COMPORT       = "COM2";
		private const string DEFAULT_LOGFILE       = null;

		public void loadDefaults()
		{
			onlyListTests          = false;
			serviceNetworkAddress  = DEFAULT_SERVICE_ADDR;
			UIport                 = DEFAULT_UIPORT;
			testSelectPattern      = DEFAULT_TESTPATTERN;
			testDeselectPattern    = DEFAULT_XCL_PATTERN;
			testIterations         = DEFAULT_ITERS;
			filename_powercycleConfig = DEFAULT_PCYCLE_CONF;
			filedata_powercycleConfig = null;
			databaseNetworkAddress = DEFAULT_DATA_NETADDR;
			databaseUsername       = DEFAULT_DATA_USER;
			databasePassword       = DEFAULT_DATA_PASS;
			comPortName            = DEFAULT_COMPORT;
			logFileName            = DEFAULT_LOGFILE;
			selectedInteractive=false;
			selectedNonInteractive=false;
		}

		public bool parseArgs(string[] args,uint index0,System.IO.TextWriter outp,System.IO.TextWriter errp)
		{
			// loop args
			for( int i=(int)index0 ; i<args.Length ; i++ )
			{
			  string arg = args[i];
				if( arg.StartsWith("/") )arg = "-"+arg.Substring(1);
				switch(arg.Trim().ToLower())
				{
				case "-l":
				case "--list":
					onlyListTests = true;
					selectedNonInteractive=true;
					break;
				case "-s":
				case "--service":
					// IP:port of the service process.
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					serviceNetworkAddress = args[i];
					break;
				case "-g":
				case "--guiport":
					// listen-port for the GUI
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					UIport = args[i];
					selectedInteractive=true;
					break;
				case "-d":
				case "--database":
					// IP:port for the database process
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					databaseNetworkAddress = args[i];
					break;
					// username for the database
				case "-u":
				case "--databaseuser":
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					databaseUsername = args[i];
					break;
				case "-p":
				case "--databasepass":
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					databasePassword = args[i];
					break;
					// serial port to use
				case "-c":
				case "--com":
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					comPortName = args[i];
					break;
					// pattern for tests to select, using '*' as wildcard.
				case "-t":
				case "--tests":
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					testSelectPattern = args[i];
					selectedNonInteractive=true;
					break;
					// negative pattern for tests.
				case "-x":
				case "--exclude":
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					testDeselectPattern = args[i];
					selectedNonInteractive=true;
					break;
				case "-i":
				case "--iterations":
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					{
					  int iter;
						try{
							iter = int.Parse(args[i]);
							if( iter<1 || iter>1000000 )
								throw new System.OverflowException();
						}catch(System.Exception)
						{
							errp.WriteLine("bad or invalid value for "+arg);return false;
						}
						testIterations = (uint)iter;
					}
					selectedNonInteractive=true;
					break;
					// logfile name.
				case "-w":
				case "--powercycle":
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					filename_powercycleConfig = args[i];
					filedata_powercycleConfig = readLineFromConfFile(filename_powercycleConfig);
					if( filedata_powercycleConfig == null )
						{errp.WriteLine("Cannot read line from "+args[i]);return false;}
					break;
				case "-f":
				case "--logfile":
					i++;if(i>=args.Length){errp.WriteLine("not value after "+arg);return false;}
					logFileName = args[i];
					break;
				case "-?":
				case "-h":
				case "--help":
					outp.WriteLine("arguments:");
					outp.WriteLine("  -n");
					outp.WriteLine("  --nogui");
					outp.WriteLine("        Disable the GUI, perform one run, exit afterwards.");
					outp.WriteLine("  -s");
					outp.WriteLine("  --service");
					outp.WriteLine("        Set the network address to find the service. e.g.  192.168.0.4:6789");
					outp.WriteLine("        default: "+DEFAULT_SERVICE_ADDR);
					outp.WriteLine("  -d");
					outp.WriteLine("  --database");
					outp.WriteLine("        Set the network address to find the MySql. e.g. 192.168.1.100");
					outp.WriteLine("        default: "+DEFAULT_DATA_NETADDR);
					outp.WriteLine("  -u");
					outp.WriteLine("  --databaseuser");
					outp.WriteLine("        Username to use to connect to MySql database.");
					outp.WriteLine("        default: "+DEFAULT_DATA_USER);
					outp.WriteLine("  -p");
					outp.WriteLine("  --databasepass");
					outp.WriteLine("        Password to use to connect to MySql database.");
					outp.WriteLine("        default: "+DEFAULT_DATA_PASS);
					outp.WriteLine("  -c");
					outp.WriteLine("  --com");
					outp.WriteLine("        Name of serial port. e.g. COM2");
					outp.WriteLine("        default: "+DEFAULT_COMPORT);
					outp.WriteLine("  -g");
					outp.WriteLine("  --GUIport");
					outp.WriteLine("        Set the port for the UI socket. e.g.  9999");
					outp.WriteLine("        Mutually exclusive with -t.");
					outp.WriteLine("        default: "+DEFAULT_UIPORT);
					outp.WriteLine("  -t");
					outp.WriteLine("  --tests");
					outp.WriteLine("        Select tests to execute. Use wildcard pattern against TestGroup.Test names.");
					outp.WriteLine("        Mutually exclusive with -g.");
					outp.WriteLine("        default: "+DEFAULT_TESTPATTERN);
					outp.WriteLine("  -x");
					outp.WriteLine("  --exclude");
					outp.WriteLine("        Deselect tests to execute. Use wildcard pattern against TestGroup.Test names.");
					outp.WriteLine("        Deselecting has precedence over selection with -t.");
					outp.WriteLine("        default: "+DEFAULT_XCL_PATTERN);
					outp.WriteLine("  -i");
					outp.WriteLine("  --iterations");
					outp.WriteLine("        Select number of iterations to execute.");
					outp.WriteLine("        default: "+DEFAULT_ITERS);
					outp.WriteLine("  -w");
					outp.WriteLine("  --powercycle");
					outp.WriteLine("        Set config-file for power-cycle function.");
					outp.WriteLine("        The first line of this file is executed when ");
					outp.WriteLine("        the service requests a power-cycle.");
					outp.WriteLine("  -f");
					outp.WriteLine("  --logfile");
					outp.WriteLine("        Name of logfile to write outputs to.");
					outp.WriteLine("  NOTE:");
					outp.WriteLine("    Selecting -g sets interactive mode.");
					outp.WriteLine("    Selecting -t and/or -i sets non-interactive mode.");
					outp.WriteLine("  -h");
					outp.WriteLine("  --help");
					outp.WriteLine("        Shows this information.");
					return false;
				}
			}
			// check missing settings
			if(serviceNetworkAddress==null)
				{errp.WriteLine("no service address set (-s)");return false;}
			if(UIport==null)
				{errp.WriteLine("no UI port set (-g)");return false;}
			if(databaseNetworkAddress==null)
				{errp.WriteLine("no database address set (-d)");return false;}
			if(databaseUsername==null)
				{errp.WriteLine("no database user set (-u)");return false;}
			if(databasePassword==null)
				{errp.WriteLine("no database password set (-p)");return false;}
			if(comPortName==null)
				{errp.WriteLine("no com port set (-c)");return false;}
			if( selectedInteractive && selectedNonInteractive )
				{errp.WriteLine("selected both interactive and non-interactive. (-g and one of -t, -x or -i)");return false;}
			// default to interactive. If both false, select interactive.
			if( !selectedNonInteractive )
				selectedInteractive=true;

			// done.
			return true;
		}

		static public string readLineFromConfFile(string fileName)
		{
		  System.IO.TextReader fh;
		  string line;
			line=null;fh=null;
			try
			{
				fh = new System.IO.StreamReader( fileName );
				while(line==null)
				{
					line = fh.ReadLine();
					if(line==null)break;
					line = line.Trim();
					// comment or empty line?
					if( line.Length<1 || line[0]=='#' )continue;
					break;		// have a line.
				}
			}catch(System.IO.IOException)
				{line=null;}
			finally
				{if(fh!=null)fh.Close();}
			return line;
		}



		public bool onlyListTests;
		public string serviceNetworkAddress;
		public string UIport;
		public string testSelectPattern;
		public string testDeselectPattern;
		public uint testIterations;
		public bool selectedInteractive;
		public bool selectedNonInteractive;
		public string filename_powercycleConfig;
		public string filedata_powercycleConfig;
		public string databaseNetworkAddress;
		public string databaseUsername;
		public string databasePassword;
		public string comPortName;
		public string logFileName;
	}

}
