using System.Collections.Generic;



namespace GTestAdapter.DataLogging
{

public class TableDesctiption
{
	public TableDesctiption( string tableName,System.Type csType )
	{
		m_csType = csType;
		m_name = tableName;
		m_fields = new List<FieldDesc>();
		m_allItems = new SortedDictionary<int,object>();
		m_accessed = false;
		m_keyName = null;
	}

	public object getById( MySql.Data.MySqlClient.MySqlConnection sql , int ID )
	{
	  MySql.Data.MySqlClient.MySqlCommand cmd = sql.CreateCommand();
	  MySql.Data.MySqlClient.MySqlDataReader rd;
	  List<string> fnam = new List<string>();
		m_accessed = true;
		foreach( FieldDesc fd in m_fields )
			fnam.Add(fd.name);
		cmd.CommandText = "SELECT "+string.Join(",",fnam.ToArray())+" FROM "+m_name+" WHERE "+keyName+" = "+ID.ToString()+";";
		rd = cmd.ExecuteReader();
	  object res = null;
		if(rd.Read())
		{
			// create our object.
			res = m_csType.GetConstructor(new System.Type[0]).Invoke(new object[0]);
			// loop attributes in result
			for( int i=0 ; i<fnam.Count ; i++ )
			{
			  object value = m_fields[i].type.outFunc( rd , i );
			  System.Reflection.FieldInfo fif = m_csType.GetField( m_fields[i].name );
				fif.SetValue(res,value);
			}
			if(rd.Read())
			{
				// oh damn. Have two or more entries.
				throw new System.Exception("found more than one entry for an ID! Error in database or handler.");
			}
		}
		rd.Close();rd.Dispose();
		return res;
	}

	public void put( MySql.Data.MySqlClient.MySqlConnection sql , object item )
	{
	  MySql.Data.MySqlClient.MySqlCommand cmd = sql.CreateCommand();
	  List<string> fnam = new List<string>();
	  List<string> fval = new List<string>();
		m_accessed = true;
		foreach( FieldDesc fd in m_fields )
		{
			fnam.Add(fd.name);
		  System.Reflection.FieldInfo fif = m_csType.GetField(fd.name);
		  object value = fif.GetValue(item);
			fval.Add( fd.type.inFunc(value) );
		}
		cmd.CommandText = "INSERT INTO "+m_name+" ("+string.Join(",",fnam.ToArray())+") VALUES ("+string.Join(",",fval.ToArray())+");";
	  int dbRes = cmd.ExecuteNonQuery();
		// return value? what does it mean?
		return;
	}

	public object checkHaveId( MySql.Data.MySqlClient.MySqlConnection sql , int ID )
	{
	  MySql.Data.MySqlClient.MySqlCommand cmd = sql.CreateCommand();
	  MySql.Data.MySqlClient.MySqlDataReader rd;
		m_accessed = true;
		cmd.CommandText = "SELECT "+keyName+" FROM "+m_name+" WHERE "+keyName+" = "+ID.ToString()+";";
		rd = cmd.ExecuteReader();
	  bool res = rd.Read();
		rd.Close();rd.Dispose();
		return res;
	}

	public List<int> getAllIds( MySql.Data.MySqlClient.MySqlConnection sql , int ID )
	{
	  MySql.Data.MySqlClient.MySqlCommand cmd = sql.CreateCommand();
	  MySql.Data.MySqlClient.MySqlDataReader rd;
		m_accessed = true;
		cmd.CommandText = "SELECT "+keyName+" FROM "+m_name+";";
		rd = cmd.ExecuteReader();
	  List<int> res = new List<int>();
		while(rd.Read())
			res.Add(rd.GetInt32(0));
		rd.Close();rd.Dispose();
		return res;
	}

	public int getNewId( MySql.Data.MySqlClient.MySqlConnection sql )
	{
	  MySql.Data.MySqlClient.MySqlCommand cmd = sql.CreateCommand();
	  MySql.Data.MySqlClient.MySqlDataReader rd;
	  List<string> fnam = new List<string>();
		m_accessed = true;
		foreach( FieldDesc fd in m_fields )
			fnam.Add(fd.name);
		cmd.CommandText = "SELECT "+keyName+" FROM "+m_name+" HAVING max("+keyName+");";
		rd = cmd.ExecuteReader();
	  int res = -1;
		if(rd.Read())
		{
			res = rd.GetInt32(0);
		}
	  bool good = !rd.Read();
		rd.Close();rd.Dispose();
		if(!good)
			throw new System.Exception("expected only one value for new ID.");
		return res;
	}



	public string make_createTableCommand()
	{
	  List<string> lines = new List<string>();
		m_accessed = true;
		lines.Add( "CREATE TABLE "+m_name );
		lines.Add( "(" );
	  string lin = null;
		foreach( FieldDesc fd in m_fields )
		{
			if(lin!=null)lines.Add(lin);
			lin = "  "+fd.name+" "+fd.type.dbType;
			if(fd.notNull)
				lin = lin + " NOT NULL";
			if(fd.type.type==FieldType.KEY)
				lin = lin + " PRIMARY KEY";
			lin = lin + ",";
		}
		if(lin==null)
			throw new System.ArgumentException("table cannot have no entries. should have added some fields.");
		lines.Add(lin.Substring(0,lin.Length-1));
		lines.Add( ");" );
		lines.Add( "" );
		return string.Join("\n",lines.ToArray());
	}

	public void addField( string name,FieldType type )
		{addField(name,type,false,false);}
	public void addField( string name,FieldType type , bool bNotNull )
		{addField(name,type,bNotNull,false);}
	public void addField( string name,FieldType type , bool bNotNull , bool bIndex )
	{
	  FieldDesc fd;
		if(m_accessed)
			throw new System.Exception("Cannot add fields after accessing the table.");
		if( type==FieldType.KEY )
		{
			if(m_keyName!=null)
				throw new System.Exception("Cannot have two key-fields in table.");
			m_keyName = name;
		}
	  FieldTypeDesc ftd = sm_fieldTypes[type];
		fd = new FieldDesc( name , ftd , bNotNull , bIndex );
		m_fields.Add(fd);
	}


	internal string keyName{get{if(m_keyName==null){throw new System.NullReferenceException("Key is not yet added to fields.");}return m_keyName;}}


	internal System.Type m_csType;
	internal string m_name;
	private List<FieldDesc> m_fields;
	private string m_keyName;
	private bool m_accessed;
	private SortedDictionary<int,object> m_allItems;

	//===============================================================================

	public enum FieldType
	{
		BYTE,
		WORD,
		INT24,
		INT,
		LONG,
		FLOAT,
		DOUBLE,
		DATE,
		DATETIME,
		VARCHAR,
		TEXT8,
		TEXT16,
		TEXT24,
		TEXT32,
		BLOB16,
		BLOB24,
		BLOB32,
		KEY,
		FKEY
	};

	private delegate string dbConvToDb(object value);
	private delegate object dbConvFromDb(MySql.Data.MySqlClient.MySqlDataReader reader,int index);

	private class FieldTypeDesc
	{
		public FieldTypeDesc(FieldType typ,string dbType,System.Type csType,dbConvToDb to,dbConvFromDb from)
		{
			this.m_type = typ;
			this.m_dbType = dbType;
			//this.inFunc = delegate(object value){return value.ToString();};
			//this.outFunc = delegate(MySql.Data.MySqlClient.MySqlDataReader reader,int index){return null;};
			this.inFunc = to;
			this.outFunc = from;
		}
		string m_dbType;
		FieldType m_type;

		public dbConvToDb inFunc;
		public dbConvFromDb outFunc;

		public FieldType type{get{return m_type;}}
		public string dbType{get{return m_dbType;}}
	};

	private class FieldDesc
	{
		public FieldDesc(string name,FieldTypeDesc type,bool bNotNull,bool bIndex)
		{
			if(type.type==FieldType.KEY)
			{
				bNotNull=true;bIndex=true;
			}
			m_name = name;
			m_type = type;
			m_bNotNull = bNotNull;
			m_bIndex = bIndex;
		}

		public FieldDesc(string name,FieldTypeDesc type,bool bNotNull)
		 : this(name,type,bNotNull,false){}
		public FieldDesc(string name,FieldTypeDesc type)
		 : this(name,type,false,false){}


		public string name{get{return m_name;}}
		public FieldTypeDesc type{get{return m_type;}}
		public bool notNull{get{return m_bNotNull;}}

		private string m_name;
		private FieldTypeDesc m_type;
		private bool m_bNotNull,m_bIndex;
	}


	private static Dictionary<FieldType,FieldTypeDesc> sm_fieldTypes = initFieldDescs();

	private static Dictionary<FieldType,FieldTypeDesc> initFieldDescs()
	{
	  List<FieldTypeDesc> tmp = new List<FieldTypeDesc>();
		tmp.Add(new FieldTypeDesc( FieldType.KEY     , "INT"          , typeof(int)    , convT_int    , convF_int ));
		tmp.Add(new FieldTypeDesc( FieldType.FKEY    , "INT"          , typeof(int)    , convT_int    , convF_int ));
		tmp.Add(new FieldTypeDesc( FieldType.BYTE    , "TINYINT"      , typeof(sbyte)  , convT_int    , convF_sbyte ));
		tmp.Add(new FieldTypeDesc( FieldType.WORD    , "SMALLINT"     , typeof(short)  , convT_int    , convF_short ));
		tmp.Add(new FieldTypeDesc( FieldType.INT24   , "MEDIUMINT"    , typeof(int)    , convT_int    , convF_int ));
		tmp.Add(new FieldTypeDesc( FieldType.INT     , "INT"          , typeof(int)    , convT_int    , convF_int ));
		tmp.Add(new FieldTypeDesc( FieldType.LONG    , "BIGINT"       , typeof(long)   , convT_long   , convF_long ));
		tmp.Add(new FieldTypeDesc( FieldType.FLOAT   , "FLOAT"        , typeof(float)  , convT_float  , convF_float ));
		tmp.Add(new FieldTypeDesc( FieldType.DOUBLE  , "DOUBLE"       , typeof(double) , convT_float  , convF_double ));
		tmp.Add(new FieldTypeDesc( FieldType.VARCHAR , "VARCHAR(255)" , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.TEXT8   , "TINYTEXT"     , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.TEXT16  , "TEXT"         , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.TEXT24  , "MEDIUMTEXT"   , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.TEXT32  , "LONGTEXT"     , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.BLOB16  , "BLOB"         , typeof(byte[]) , convT_blob   , convF_blob ));
		tmp.Add(new FieldTypeDesc( FieldType.BLOB24  , "MEDIUMBLOB"   , typeof(byte[]) , convT_blob   , convF_blob ));
		tmp.Add(new FieldTypeDesc( FieldType.BLOB32  , "LONGBLOB"     , typeof(byte[]) , convT_blob   , convF_blob ));
		tmp.Add(new FieldTypeDesc( FieldType.DATE    , "DATE"         , typeof(System.DateTime) , convT_date   , convF_date ));
	  Dictionary<FieldType,FieldTypeDesc> res = new Dictionary<FieldType,FieldTypeDesc>();
		foreach( FieldTypeDesc fd in tmp )
			res[fd.type] = fd;
		return res;
	}

	// conversion function for the datatypes
	private static string convT_int(object value)
	{
		return System.Convert.ToInt32(value).ToString();
	}
	private static string convT_long(object value)
	{
		return System.Convert.ToInt64(value).ToString();
	}
	private static string convT_float(object value)
	{
		return System.Convert.ToDouble(value).ToString();
	}
	private static string convT_string(object value)
	{
		return (System.String)value;
	}
	private static string convT_blob(object value)
	{
	  string tmp = System.Text.ASCIIEncoding.ASCII.GetString((byte[])value);
		tmp = tmp.Replace("\\","\\\\");
		tmp = tmp.Replace("\0","\\0");
		tmp = tmp.Replace("\"","\\\"");
		tmp = tmp.Replace("\'","\\\'");
		return (System.String)value;
	}

	private static object convF_int(MySql.Data.MySqlClient.MySqlDataReader reader,int index)
	{
		return reader.GetInt32(index);
	}

	private static object convF_sbyte(MySql.Data.MySqlClient.MySqlDataReader reader,int index)
	{
		return reader.GetByte(index);
	}

	private static object convF_short(MySql.Data.MySqlClient.MySqlDataReader reader,int index)
	{
		return reader.GetInt16(index);
	}

	private static object convF_long(MySql.Data.MySqlClient.MySqlDataReader reader,int index)
	{
		return reader.GetInt64(index);
	}

	private static object convF_float(MySql.Data.MySqlClient.MySqlDataReader reader,int index)
	{
		return reader.GetFloat(index);
	}

	private static object convF_double(MySql.Data.MySqlClient.MySqlDataReader reader,int index)
	{
		return reader.GetDouble(index);
	}

	private static object convF_string(MySql.Data.MySqlClient.MySqlDataReader reader,int index)
	{
		return reader.GetString(index);
	}

	private static object convF_blob(MySql.Data.MySqlClient.MySqlDataReader reader,int index)
	{
		//long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length);
	  List<byte[]> tmp = new List<byte[]>();
	  long didRead,walk;
		walk=0;
		while(true)
		{
		  byte[] xfer = new byte[4096];
			didRead = reader.GetBytes( index , walk , xfer , 0 , xfer.Length );
			if(didRead<=0)break;
			tmp.Add(xfer);
			walk += didRead;
		}
	  byte[] res = new byte[walk];
	  long fill;
	  int i;
		for( fill=0,i=0 ; fill<walk ; fill+=4096,i++ )
		{
		  int xfer = (int)(walk-fill);
			if(xfer>4096)xfer=4096;
			System.Array.Copy( tmp[i] , 0 , res , fill , xfer );
		}
		return res;
	}

}

}
