using System.Collections.Generic;



namespace GTestAdapter.DataLogging
{

public class TableDesctiption
{
	public TableDesctiption( string tableName,System.Type csType )
	{
		m_csType = csType;
		m_name = tableName;
		m_fields = new List<FieldDesc>();
		m_allItems = new SortedDictionary<int,object>();
		m_accessed = false;
		m_keyName = null;
	}

	public List<object> getItems( System.Data.IDbConnection sql , string whereClause )
	{
	  System.Data.IDbCommand cmd = sql.CreateCommand();
	  System.Data.IDataReader rd;
	  List<string> fnam = new List<string>();
		m_accessed = true;
		foreach( FieldDesc fd in m_fields )
			fnam.Add(fd.name);
	  string cmdLine = "SELECT "+string.Join(",",fnam.ToArray())+" FROM "+m_name;
		if( whereClause!=null && whereClause!=string.Empty )
			cmdLine = cmdLine + " WHERE "+whereClause;
		cmd.CommandText = cmdLine+";";
		rd = cmd.ExecuteReader();
	  List<object> res = new List<object>();
		while(rd.Read())
		{
			// create our object.
		  object item = m_csType.GetConstructor(new System.Type[0]).Invoke(new object[0]);
			// loop attributes in result
			for( int i=0 ; i<fnam.Count ; i++ )
			{
			  object value = m_fields[i].type.outFunc( rd , i );
			  System.Reflection.FieldInfo fif = m_csType.GetField( m_fields[i].name );
				fif.SetValue(item,value);
			}
			res.Add(item);
		}
		rd.Close();rd.Dispose();
		return res;
	}

	public object getById( System.Data.IDbConnection sql , int ID )
	{
	  List<object> l;
		l = getItems( sql , keyName+"="+ID.ToString() );
		if(l.Count<1)return null;
		if(l.Count>1)
			throw new System.Data.DataException("Database error. should only find one item for a key.");
		return l[0];
	}

	public void put( System.Data.IDbConnection sql , object item )
	{
	  System.Data.IDbCommand cmd = sql.CreateCommand();
	  List<string> fnam = new List<string>();
	  List<string> fval = new List<string>();
		m_accessed = true;
		foreach( FieldDesc fd in m_fields )
		{
			fnam.Add(fd.name);
		  System.Reflection.FieldInfo fif = m_csType.GetField(fd.name);
		  object value = fif.GetValue(item);
			fval.Add( fd.type.inFunc(value) );
		}
		cmd.CommandText = "INSERT INTO "+m_name+" ("+string.Join(",",fnam.ToArray())+") VALUES ("+string.Join(",",fval.ToArray())+");";
	  int dbRes = cmd.ExecuteNonQuery();
		// return value? what does it mean?
		return;
	}

	public object checkHaveId( System.Data.IDbConnection sql , int ID )
	{
	  System.Data.IDbCommand cmd = sql.CreateCommand();
	  System.Data.IDataReader rd;
		m_accessed = true;
		cmd.CommandText = "SELECT "+keyName+" FROM "+m_name+" WHERE "+keyName+" = "+ID.ToString()+";";
		rd = cmd.ExecuteReader();
	  bool res = rd.Read();
		rd.Close();rd.Dispose();
		return res;
	}

	public List<int> getAllIds( System.Data.IDbConnection sql , int ID )
	{
	  System.Data.IDbCommand cmd = sql.CreateCommand();
	  System.Data.IDataReader rd;
		m_accessed = true;
		cmd.CommandText = "SELECT "+keyName+" FROM "+m_name+";";
		rd = cmd.ExecuteReader();
	  List<int> res = new List<int>();
		while(rd.Read())
			res.Add(rd.GetInt32(0));
		rd.Close();rd.Dispose();
		return res;
	}

	public int getNewId( System.Data.IDbConnection sql )
	{
	  System.Data.IDbCommand cmd = sql.CreateCommand();
	  System.Data.IDataReader rd;
		m_accessed = true;
		cmd.CommandText = "SELECT max("+keyName+") FROM "+m_name+";";
		rd = cmd.ExecuteReader();
	  int res = 0;
		if(rd.Read())
		{
			if(!rd.IsDBNull(0))			// if the table is empty, result will be DbNull.
				res = rd.GetInt32(0);
		}
	  bool good = !rd.Read();
		rd.Close();rd.Dispose();
		if(!good)
			throw new System.Exception("expected only one value for new ID.");
		return res+1;
	}

	public bool doesTableExist( System.Data.IDbConnection sql , bool createIfNot )
	{
		// just try to get an id. if true or false, table is there. If throws, table is probably not there.
		try{
			checkHaveId( sql , 1 );
			return true;		// exists.
		}catch(System.Data.DataException)
		{
		}catch(System.Data.Common.DbException)
		{
		}
		// does not. Create
		if(!createIfNot)
			return false;		// create not requested.
	  System.Data.IDbCommand cmd = sql.CreateCommand();
		cmd.CommandText = make_createTableCommand();
		cmd.ExecuteNonQuery();
		return true;
	}


	public string make_createTableCommand()
	{
	  List<string> lines = new List<string>();
		m_accessed = true;
		lines.Add( "CREATE TABLE "+m_name );
		lines.Add( "(" );
	  string lin = null;
		foreach( FieldDesc fd in m_fields )
		{
			if(lin!=null)lines.Add(lin);
			lin = "  "+fd.name+" "+fd.type.dbType;
			if(fd.notNull)
				lin = lin + " NOT NULL";
			if(fd.type.type==FieldType.KEY)
				lin = lin + " PRIMARY KEY";
			lin = lin + ",";
		}
		if(lin==null)
			throw new System.ArgumentException("table cannot have no entries. should have added some fields.");
		lines.Add(lin.Substring(0,lin.Length-1));
		lines.Add( ");" );
		lines.Add( "" );
		return string.Join("\n",lines.ToArray());
	}

	public void addField( string name,FieldType type )
		{addField(name,type,false,false);}
	public void addField( string name,FieldType type , bool bNotNull )
		{addField(name,type,bNotNull,false);}
	public void addField( string name,FieldType type , bool bNotNull , bool bIndex )
	{
	  FieldDesc fd;
		if(m_accessed)
			throw new System.Exception("Cannot add fields after accessing the table.");
		if( type==FieldType.KEY )
		{
			if(m_keyName!=null)
				throw new System.Exception("Cannot have two key-fields in table.");
			m_keyName = name;
		}
	  FieldTypeDesc ftd = sm_fieldTypes[type];
		fd = new FieldDesc( name , ftd , bNotNull , bIndex );
		m_fields.Add(fd);
	}


	internal string keyName{get{if(m_keyName==null){throw new System.NullReferenceException("Key is not yet added to fields.");}return m_keyName;}}


	internal System.Type m_csType;
	internal string m_name;
	private List<FieldDesc> m_fields;
	private string m_keyName;
	private bool m_accessed;
	private SortedDictionary<int,object> m_allItems;

	//===============================================================================

	public enum FieldType
	{
		BOOL,
		BYTE,
		WORD,
		INT24,
		INT,
		LONG,
		FLOAT,
		DOUBLE,
		DATE,
		DATETIME,
		VARCHAR,
		TEXT8,
		TEXT16,
		TEXT24,
		TEXT32,
		BLOB16,
		BLOB24,
		BLOB32,
		KEY,
		FKEY
	};

	private delegate string dbConvToDb(object value);
	private delegate object dbConvFromDb(System.Data.IDataReader reader,int index);

	private class FieldTypeDesc
	{
		public FieldTypeDesc(FieldType typ,string dbType,System.Type csType,dbConvToDb to,dbConvFromDb from)
		{
			this.m_type = typ;
			this.m_dbType = dbType;
			//this.inFunc = delegate(object value){return value.ToString();};
			//this.outFunc = delegate(MySql.Data.MySqlClient.MySqlDataReader reader,int index){return null;};
			this.inFunc = to;
			this.outFunc = from;
		}
		string m_dbType;
		FieldType m_type;

		public dbConvToDb inFunc;
		public dbConvFromDb outFunc;

		public FieldType type{get{return m_type;}}
		public string dbType{get{return m_dbType;}}
	};

	private class FieldDesc
	{
		public FieldDesc(string name,FieldTypeDesc type,bool bNotNull,bool bIndex)
		{
			if(type.type==FieldType.KEY)
			{
				bNotNull=true;bIndex=true;
			}
			m_name = name;
			m_type = type;
			m_bNotNull = bNotNull;
			m_bIndex = bIndex;
		}

		public FieldDesc(string name,FieldTypeDesc type,bool bNotNull)
		 : this(name,type,bNotNull,false){}
		public FieldDesc(string name,FieldTypeDesc type)
		 : this(name,type,false,false){}


		public string name{get{return m_name;}}
		public FieldTypeDesc type{get{return m_type;}}
		public bool notNull{get{return m_bNotNull;}}

		private string m_name;
		private FieldTypeDesc m_type;
		private bool m_bNotNull,m_bIndex;
	}


	private static Dictionary<FieldType,FieldTypeDesc> sm_fieldTypes = initFieldDescs();

	private static Dictionary<FieldType,FieldTypeDesc> initFieldDescs()
	{
	  List<FieldTypeDesc> tmp = new List<FieldTypeDesc>();
		tmp.Add(new FieldTypeDesc( FieldType.BOOL    , "TINYINT"      , typeof(bool)   , convT_bool   , convF_bool ));
		tmp.Add(new FieldTypeDesc( FieldType.KEY     , "INT"          , typeof(int)    , convT_int    , convF_int ));
		tmp.Add(new FieldTypeDesc( FieldType.FKEY    , "INT"          , typeof(int)    , convT_int    , convF_int ));
		tmp.Add(new FieldTypeDesc( FieldType.BYTE    , "TINYINT"      , typeof(sbyte)  , convT_int    , convF_sbyte ));
		tmp.Add(new FieldTypeDesc( FieldType.WORD    , "SMALLINT"     , typeof(short)  , convT_int    , convF_short ));
		tmp.Add(new FieldTypeDesc( FieldType.INT24   , "MEDIUMINT"    , typeof(int)    , convT_int    , convF_int ));
		tmp.Add(new FieldTypeDesc( FieldType.INT     , "INT"          , typeof(int)    , convT_int    , convF_int ));
		tmp.Add(new FieldTypeDesc( FieldType.LONG    , "BIGINT"       , typeof(long)   , convT_long   , convF_long ));
		tmp.Add(new FieldTypeDesc( FieldType.FLOAT   , "FLOAT"        , typeof(float)  , convT_float  , convF_float ));
		tmp.Add(new FieldTypeDesc( FieldType.DOUBLE  , "DOUBLE"       , typeof(double) , convT_float  , convF_double ));
		tmp.Add(new FieldTypeDesc( FieldType.VARCHAR , "VARCHAR(255)" , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.TEXT8   , "TINYTEXT"     , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.TEXT16  , "TEXT"         , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.TEXT24  , "MEDIUMTEXT"   , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.TEXT32  , "LONGTEXT"     , typeof(string) , convT_string , convF_string ));
		tmp.Add(new FieldTypeDesc( FieldType.BLOB16  , "BLOB"         , typeof(byte[]) , convT_blob   , convF_blob ));
		tmp.Add(new FieldTypeDesc( FieldType.BLOB24  , "MEDIUMBLOB"   , typeof(byte[]) , convT_blob   , convF_blob ));
		tmp.Add(new FieldTypeDesc( FieldType.BLOB32  , "LONGBLOB"     , typeof(byte[]) , convT_blob   , convF_blob ));
		tmp.Add(new FieldTypeDesc( FieldType.DATE    , "DATE"         , typeof(System.DateTime) , convT_date   , convF_date ));
		tmp.Add(new FieldTypeDesc( FieldType.DATETIME, "DATETIME"     , typeof(System.DateTime) , convT_date   , convF_date ));
	  Dictionary<FieldType,FieldTypeDesc> res = new Dictionary<FieldType,FieldTypeDesc>();
		foreach( FieldTypeDesc fd in tmp )
			res[fd.type] = fd;
		return res;
	}

	// conversion function for the datatypes
	private static string convT_bool(object value)
	{
		return ( ((bool)value) ? "1" : "0" );
	}
	private static string convT_int(object value)
	{
		return System.Convert.ToInt32(value).ToString();
	}
	private static string convT_long(object value)
	{
		return System.Convert.ToInt64(value).ToString();
	}
	private static string convT_float(object value)
	{
		return System.Convert.ToDouble(value).ToString();
	}
	private static string convT_string(object value)
	{
		return quoteString((System.String)value);
	}
	private static string convT_date(object value)
	{
	  System.DateTime tim = (System.DateTime)value;
		return string.Format(
				"'{0:0000}-{1:00}-{2:00} {3:00}:{4:00}:{5:00}'"  ,
				tim.Year , tim.Month , tim.Day  ,
				tim.Hour , tim.Minute , tim.Second
		);
	}
	private static string convT_blob(object value)
	{
	  string tmp = System.Text.ASCIIEncoding.ASCII.GetString((byte[])value);
		tmp = tmp.Replace("\\","\\\\");
		tmp = tmp.Replace("\0","\\0");
		tmp = tmp.Replace("\"","\\\"");
		tmp = tmp.Replace("\'","\\\'");
		return "'"+(System.String)value+"'";
	}

	private static object convF_bool(System.Data.IDataReader reader,int index)
	{
		return ( reader.GetByte(index) != 0 );
	}

	private static object convF_int(System.Data.IDataReader reader,int index)
	{
		return reader.GetInt32(index);
	}

	private static object convF_sbyte(System.Data.IDataReader reader,int index)
	{
		return reader.GetByte(index);
	}

	private static object convF_short(System.Data.IDataReader reader,int index)
	{
		return reader.GetInt16(index);
	}

	private static object convF_long(System.Data.IDataReader reader,int index)
	{
		return reader.GetInt64(index);
	}

	private static object convF_float(System.Data.IDataReader reader,int index)
	{
		return reader.GetFloat(index);
	}

	private static object convF_double(System.Data.IDataReader reader,int index)
	{
		return reader.GetDouble(index);
	}

	private static object convF_string(System.Data.IDataReader reader,int index)
	{
		return reader.GetString(index);
	}

	private static object convF_date(System.Data.IDataReader reader,int index)
	{
		return reader.GetDateTime(index);
	}

	private static object convF_blob(System.Data.IDataReader reader,int index)
	{
		//long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length);
	  List<byte[]> tmp = new List<byte[]>();
	  long didRead,walk;
		walk=0;
		while(true)
		{
		  byte[] xfer = new byte[4096];
			didRead = reader.GetBytes( index , walk , xfer , 0 , xfer.Length );
			if(didRead<=0)break;
			tmp.Add(xfer);
			walk += didRead;
		}
	  byte[] res = new byte[walk];
	  long fill;
	  int i;
		for( fill=0,i=0 ; fill<walk ; fill+=4096,i++ )
		{
		  int xfer = (int)(walk-fill);
			if(xfer>4096)xfer=4096;
			System.Array.Copy( tmp[i] , 0 , res , fill , xfer );
		}
		return res;
	}

	private static string quoteString(string str)
	{
		if(str==null)
			str = string.Empty;
		str = str.Replace("\\","\\\\").Replace("\n","\\n").Replace("\r","").Replace("\x1A","\\Z").Replace("'","\\'");
		str = str.Replace("\0","\\0").Replace("\"","\\\"");
		return "'"+str+"'";
	}

}

}
