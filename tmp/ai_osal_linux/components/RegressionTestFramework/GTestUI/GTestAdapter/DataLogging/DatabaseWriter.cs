using System.Collections.Generic;

/*

Create user, set up tables:

create user 'test'@'%' identified by '123456';
grant all on *.* to 'test'@'%';
show grants for 'test';



Restart MySQL on Fedora:
	service mysqld start

 */


namespace GTestAdapter.DataLogging
{

	public class DatabaseWriter : System.IDisposable
	{

		public delegate System.Data.IDbConnection connectionOpener();

		/// <summary>
		/// Constructor for MySql. Takes servername, username and password.
		/// This constructor creates a factory object and calls the other constructor.
		/// </summary>
		/// <param name="filesPath">Path to root of files storage.</param>
		/// <param name="serverName">network name of server.</param>
		/// <param name="user">username for MySql server</param>
		/// <param name="pw">password for MySql server</param>
		public DatabaseWriter(string filesPath,string serverName,string user,string pw)
		 : this(filesPath,new MySqlOpener(serverName,user,pw))
		{}

		/// <summary>
		/// Constructor using a factory interface for opening the database.
		/// </summary>
		/// <param name="filesPath">Path to root of files storage.</param>
		/// <param name="DbFactory">interface for factory to create the database handle.</param>
		public DatabaseWriter(string filesPath,IDbOpener dbFactory)
		{
			m_fileDirectoryLocation = filesPath;

			m_sql_factory = dbFactory;

			try{
				// open/connect the database.
				m_sql = m_sql_factory.openDB();
				if(m_sql==null)
					throw new System.Data.DataException("Cannot open database.");

				// see if the tables exist.
				Data.SqlTestSet.desc.doesTableExist( m_sql , true );
				Data.SqlTestCase.desc.doesTableExist( m_sql , true );
				Data.SqlTest.desc.doesTableExist( m_sql , true );
				Data.SqlResultBlock.desc.doesTableExist( m_sql , true );
				Data.SqlResult.desc.doesTableExist( m_sql , true );

			}catch(System.Data.DataException ex)
			{
				System.Console.Error.WriteLine("Error accessing database "+m_sql_factory.getDBpath_for_errorMessage()+".");
				System.Console.Error.WriteLine(ex.Message);
				throw;
			}catch(System.Data.Common.DbException ex)
			{
				System.Console.Error.WriteLine("Error accessing database "+m_sql_factory.getDBpath_for_errorMessage()+".");
				System.Console.Error.WriteLine(ex.Message);
				throw;
			}

		}

		public void Dispose()
		{
			Close();
		}

		public void Close()
		{
			if(m_sql!=null)
			{
				m_sql_factory.closeDB(m_sql);
				m_sql = null;
			}
		}

		/// <summary>
		/// class usable as the factory method.
		/// </summary>
		private class MySqlOpener : IDbOpener
		{
			public MySqlOpener(string serverName,string userName,string passWord)
			{
				m_serverName = serverName;
				m_user = userName;
				m_pw = passWord;
			}
			public System.Data.IDbConnection openDB()
			{
			  string c;
//				c = "Provider=MySqlProv;";
//				c += ("Data Source="+m_serverName+";");
//				c += ("User id="+SQL_USERNAME+";");
//				c += ("Password="+SQL_PASSWORD+";");
				c = string.Format(
						"Server={0};Port={1};Uid={2};Pwd={3};"  ,
						m_serverName  ,
						3306  ,
						m_user  ,//..... TODO: SQL_USERNAME  ,
						m_pw  // TODO: ..... SQL_PASSWORD
				);
			  System.Data.IDbConnection res;
				res = new MySql.Data.MySqlClient.MySqlConnection(c);
				res.Open();
				res.ChangeDatabase("testResults");		// ..... hard-coded?
				return res;
			}
			public string getDBpath_for_errorMessage()
			{
				return m_serverName;
			}
			public void closeDB(System.Data.IDbConnection dbHandle)
			{
				dbHandle.Close();
				dbHandle.Dispose();
			}
			private string m_serverName,m_user,m_pw;
		}

		/// <summary>
		/// Store or update a set of results in the database.
		/// Checks and completes the testSet and the results-set.
		/// </summary>
		/// <param name="results">handle of results-set to store.</param>
		/// <param name="testSet">handle of associated testSet.</param>
		/// <returns>true if succeeded. False or throws if something went wrong.</returns>
		public bool storeResultSet( ulong UUID , GTestCommon.Models.AllTestCases testSet )
		{
		  List<object> lo;
		  Data.SqlResultBlock dbResults;
			// check
			if( UUID==0ul || testSet.CheckSum==0ul )
				throw new System.ArgumentException("resultset must have a valid UUID and testset-checksum.");

			// First check if the testSet is in the database.
		  Data.SqlTestSet dbTSet;
			dbTSet = pStoreTestSet( testSet );

			// Now  get the SqlResultBlock.
		  bool bCreated = false;
			lo = Data.SqlResultBlock.desc.getItems( m_sql , "UUID="+UUID.ToString()+" AND testSetID="+dbTSet.ID.ToString() );
			if(lo.Count>1)
				throw new System.Data.DataException("Error in database. Found duplicate resultssets with same UUID and testSet.");
			dbResults=null;
			if(lo.Count>0)
				dbResults = (Data.SqlResultBlock)lo[0];
			if( dbResults==null )
			{
				// not found. Create new entry.
				dbResults = new Data.SqlResultBlock();
				dbResults.UUID = (long)UUID;
				dbResults.testSetID = dbTSet.ID;
				dbResults.ID = Data.SqlResultBlock.desc.getNewId( m_sql );
				Data.SqlResultBlock.desc.put( m_sql , dbResults );
				bCreated = true;
			}



			// now loop the results.
		  List<Data.SqlResult> existing = new List<Data.SqlResult>();
			if(!bCreated)
			{
				// it was not freshly created, so check existing items in it.
			  List<object> tmp = Data.SqlResult.desc.getItems( m_sql , "resBlkID="+dbResults.ID );
				foreach(object o in tmp)
					existing.Add((Data.SqlResult)o);
			}
			// create a dict using both testID and iter as long for quick exist-check.
		  Dictionary<ulong,Data.SqlResult> rmap = new Dictionary<ulong,Data.SqlResult>();
			foreach( Data.SqlResult res in existing )
				rmap[ ( (((ulong)res.iteration)<<32) + (ulong)res.testID ) ] = res;
			// now loop result items here.
			for( int i=0 ; i<testSet.TestGroupCount ; i++ )
			{
			  GTestCommon.Models.TestGroupModel grp;
				grp = testSet[i];
				for( int j=0 ; j<grp.TestCount ; j++ )
				{
				  GTestCommon.Models.Test tst;
					tst = grp[j];
					foreach( GTestCommon.Models.TestResultModel res in tst.iterResults() )
					{
					  ulong dictKey = (((ulong)res.iteration)<<32) + (ulong)res.testID ;
						if( rmap.ContainsKey(dictKey) )
							continue;
						// not in database. Add this item.
					  Data.SqlResult dbRes = new Data.SqlResult();
						dbRes.ID = Data.SqlResult.desc.getNewId( m_sql );
						dbRes.iteration = (int)res.iteration;
						dbRes.success = res.success;
						dbRes.testID = (int)res.testID;
						dbRes.resBlkID = dbResults.ID;
						dbRes.time = res.time;
						Data.SqlResult.desc.put( m_sql , dbRes );
						rmap[dictKey] = dbRes;
					}
				}
			}

			return true;
		}

		public bool storeTestSet( GTestCommon.Models.AllTestCases testSet )
		{
			return pStoreTestSet(testSet)!=null;
		}

		/// <summary>
		/// Get the testSet into the database. Checks if already there and adds it if not.
		/// Also includes all testCases below.
		/// </summary>
		/// <param name="testSet">Testset to be added/checked.</param>
		/// <returns>returns the struct representing the database item.</returns>
		private Data.SqlTestSet pStoreTestSet( GTestCommon.Models.AllTestCases testSet )
		{
		  List<object> lo;
		  Data.SqlTestSet dbTSet;
			// check
			if( testSet.CheckSum==0ul )
				throw new System.ArgumentException("testSet must have a valid checksum.");
			// first get the SqlTestSet if already there.
			lo = Data.SqlTestSet.desc.getItems( m_sql , "checkSum="+testSet.CheckSum.ToString() );
			if(lo.Count>1)
				throw new System.Data.DataException("Error in database. Found duplicate testSets with same checksum.");
			dbTSet=null;
			if(lo.Count>0)
				dbTSet = (Data.SqlTestSet)lo[0];
			if( dbTSet==null )
			{
				// not found. Create new entry.
				dbTSet = new Data.SqlTestSet();
				dbTSet.checkSum = (long)testSet.CheckSum;
				dbTSet.ID = Data.SqlTestSet.desc.getNewId( m_sql );
				Data.SqlTestSet.desc.put( m_sql , dbTSet );
			}
			// now have it. check the testcases.
		  bool good=true;
			for( int i=0 ; i<testSet.TestGroupCount ; i++ )
			{
			  object sub = storeTestCase( testSet[i] , dbTSet );
				if( sub==null )
					good=false;
			}
			if(!good)
			{
				// ...... TODO: what if not good? one sub-item failed?
			}
			// done.
			return dbTSet;
		}

		private Data.SqlTestCase storeTestCase( GTestCommon.Models.TestGroupModel testCase , Data.SqlTestSet parent )
		{
		  List<object> lo;
		  Data.SqlTestCase dbTCas;
			// check
			if( testCase.Parent==null )
				throw new System.ArgumentException("cannot store a testCase not linked to a testSet.");
			if(parent==null)
				parent = pStoreTestSet( testCase.Parent );
			// first get the SqlTestSet if already there.
			// ..... TODO: escape name?
			lo = Data.SqlTestCase.desc.getItems( m_sql , "testSetID="+parent.ID.ToString()+" AND name='"+testCase.Name+"'" );
			if(lo.Count>1)
				throw new System.Data.DataException("Error in database. Found duplicate testCasees with same parent and name.");
			dbTCas=null;
			if(lo.Count>0)
				dbTCas = (Data.SqlTestCase)lo[0];
			if( dbTCas==null )
			{
				// not found. Create new entry.
				dbTCas = new Data.SqlTestCase();
				dbTCas.name = testCase.Name;
				dbTCas.testSetID = parent.ID;
				dbTCas.ID = Data.SqlTestCase.desc.getNewId( m_sql );
				Data.SqlTestCase.desc.put( m_sql , dbTCas );
			}
			// now have it. check the tests.
		  bool good=true;
			for( int i=0 ; i<testCase.TestCount ; i++ )
			{
			  object sub = storeTest( testCase[i] , dbTCas );
				if( sub==null )
					good=false;
			}
			if(!good)
			{
				// ...... TODO: what if not good? one sub-item failed?
			}
			// done.
			return dbTCas;
		}

		private Data.SqlTest storeTest( GTestCommon.Models.Test test , Data.SqlTestCase parent )
		{
		  List<object> lo;
		  Data.SqlTest dbTst;
			// check
			if( test.Group==null )
				throw new System.ArgumentException("cannot store a test not linked to a testCase.");
			if(parent==null)
				parent = storeTestCase( test.Group , null );
			// first get the SqlTest if already there.
			// ..... TODO: escape name?
			lo = Data.SqlTest.desc.getItems( m_sql , "testCaseID="+parent.ID.ToString()+" AND name='"+test.Name+"'" );
			if(lo.Count>1)
				throw new System.Data.DataException("Error in database. Found duplicate tests with same parent and name.");
			dbTst=null;
			if(lo.Count>0)
				dbTst = (Data.SqlTest)lo[0];
			if( dbTst==null )
			{
				// not found. Create new entry.
				dbTst = new Data.SqlTest();
				dbTst.name = test.Name;
				dbTst.testID = (int)test.ID;
				dbTst.testCaseID = parent.ID;
				dbTst.ID = Data.SqlTest.desc.getNewId( m_sql );
				Data.SqlTest.desc.put( m_sql , dbTst );
			}
			// done.
			return dbTst;
		}



		private IDbOpener m_sql_factory;
		private System.Data.IDbConnection m_sql;
		private string m_fileDirectoryLocation;


	}
}
