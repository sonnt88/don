using System.Data;


namespace GTestAdapter.DataLogging
{

public interface IDbOpener
{
	IDbConnection openDB();
	string getDBpath_for_errorMessage();
	void closeDB(IDbConnection dbHandle);
}

}
