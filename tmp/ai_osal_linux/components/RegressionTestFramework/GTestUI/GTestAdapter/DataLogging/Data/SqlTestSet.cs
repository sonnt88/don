using GTestAdapter.DataLogging;


namespace GTestAdapter.DataLogging.Data
{

	public class SqlTestSet
	{
		public int ID;		// KEY
		public long checkSum;

		public static TableDesctiption desc = createTableDesc();
		private static TableDesctiption createTableDesc()
		{
		  TableDesctiption res = new TableDesctiption("testSet",typeof(SqlTestSet));
			res.addField( "ID" , TableDesctiption.FieldType.KEY );
			res.addField( "checkSum" , TableDesctiption.FieldType.LONG , true , true );
			return res;
		}
	}

	public class SqlTestCase
	{
		public int ID;		// KEY
		public string name;
		public int testSetID;

		public static TableDesctiption desc = createTableDesc();
		private static TableDesctiption createTableDesc()
		{
		  TableDesctiption res = new TableDesctiption("testCase",typeof(SqlTestCase));
			res.addField( "ID" , TableDesctiption.FieldType.KEY );
			res.addField( "name" , TableDesctiption.FieldType.VARCHAR );
			res.addField( "testSetID" , TableDesctiption.FieldType.FKEY );
			return res;
		}
	}

	public class SqlTest
	{
		public int ID;		// KEY
		public int testID;
		public string name;
		public int testCaseID;

		public static TableDesctiption desc = createTableDesc();
		private static TableDesctiption createTableDesc()
		{
		  TableDesctiption res = new TableDesctiption("test",typeof(SqlTest));
			res.addField( "ID" , TableDesctiption.FieldType.KEY );
			res.addField( "testID" , TableDesctiption.FieldType.INT );
			res.addField( "name" , TableDesctiption.FieldType.VARCHAR );
			res.addField( "testCaseID" , TableDesctiption.FieldType.FKEY );
			return res;
		}
	}

	public class SqlResultBlock
	{
		public int ID;		// KEY
		public long UUID;
		public int testSetID;	// remote-key of testSet to be used.

		public static TableDesctiption desc = createTableDesc();
		private static TableDesctiption createTableDesc()
		{
		  TableDesctiption res = new TableDesctiption("resultBlock",typeof(SqlResultBlock));
			res.addField( "ID" , TableDesctiption.FieldType.KEY );
			res.addField( "UUID" , TableDesctiption.FieldType.LONG , true , true );
			res.addField( "testSetID" , TableDesctiption.FieldType.FKEY );
			return res;
		}
	}

	public class SqlResult
	{
		public int ID;		// KEY
		public int resBlkID;		// FKEY -> SqlResultBlock
		public int testID;			// test ID. Not database ID!
		public int iteration;
		public bool success;
		public System.DateTime time;

		public static TableDesctiption desc = createTableDesc();
		private static TableDesctiption createTableDesc()
		{
		  TableDesctiption res = new TableDesctiption("result",typeof(SqlResult));
			res.addField( "ID" , TableDesctiption.FieldType.KEY );
			res.addField( "resBlkID" , TableDesctiption.FieldType.FKEY );
			res.addField( "testID" , TableDesctiption.FieldType.INT );
			res.addField( "iteration" , TableDesctiption.FieldType.INT , true );
			res.addField( "success" , TableDesctiption.FieldType.BOOL );
			res.addField( "time" , TableDesctiption.FieldType.DATETIME );
			return res;
		}
	}

}

