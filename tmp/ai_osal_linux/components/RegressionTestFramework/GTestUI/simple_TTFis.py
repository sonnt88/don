import exceptions
import logging
import os.path
import sys
import time
import TTFisClient
from TTFisClient import Wait

cfg = {
       'TRCPath' : r'D:\VWRNS315CN\1007\Release',
       'LogPath' : r'D:\VWRNS315CN\1007\TTFisTraces'
}

# Identify scriptname
if sys.argv[0] == '-c' or len(sys.argv[0])==0 :
    scriptname = 'Unknown'
else :
    (scriptname, ext) = os.path.splitext(os.path.basename(sys.argv[0]))
    del ext

# Create a new directory for trace- and error-file. Name is derived from the Script-Filename
logpath   = cfg[ 'LogPath' ] + '\\' + scriptname + time.strftime('_Run_%y%m%d_%H%M%S')
if not os.path.exists( logpath ) :
    os.makedirs( logpath )

# Set filenames for trace- and error-file.
logfile   = logpath + '\\Trace_' + scriptname + '.pro'


# Create Logger-Object for this Script
# ====================================
# The object 'log' can be used within this script to add information to the Logfile
log     = logging.getLogger('TestCase')

# Configure python logging module
rootLogger = logging.getLogger('')
rootLogger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s,%(msecs)03d %(name)-12s %(levelname)-8s %(message)s', '%H:%M:%S')
handler   = logging.FileHandler(logfile, 'w')
handler.setLevel(logging.INFO)
handler.setFormatter(formatter)
rootLogger.addHandler(handler)


ttfis  = None
try :
    ttfis = TTFisClient.TTFisClient()

    trcfiles = []
    for file in ['master.trc', 'memadr_navi.trc', 'navapp.trc'] :
        newfile = cfg[ 'TRCPath' ] + '\\' + file
        if os.path.exists( newfile ) :
            trcfiles.append( newfile )
    del file, newfile
    log.info('Connect to Trace-Device')
    p = ttfis.Connect('paramount@usb', trcfiles)
    Wait(5, 's')

    log.info('Configure Trace Output')
    TraceConfig = """
        .TR_ENABLE_BLOCKMODE BLOCK 
        .TR_CONF_DISABLE_ALL
        .TR_CONF_ENABLE_ALL TR_LEVEL_FATAL[CUSTOMER]
        .TR_DISABLE_TIMESTAMP
        .TR_DISABLE_RTC_TIMESTAMP
        .TR_SAVE_CNFG_ON_SHUTDOWN
        .save_ffd
        w+ 1000
    """
    ttfis.Cmd(TraceConfig)

    log.info('Test for active communication to Trace-Device')
    retry = 0
    while retry < 3 :
        retry += 1
        res = ttfis.Wait4Trace('get connection', 2.0, ttfis.Cmd, 'TR_TEST_CONNECTION')
        if res :
            break
        else :
            log.warning('No response from Trace-Device! Retrying ...')
    else :
        log.error('No connection to target. Stop test execution')
        raise exceptions.RuntimeError, 'No connection to target. Stop test execution'

    # Start test execution
    pass

except :
    print 'Script terminated due to exception'
else :
    print 'Script completed successfully'
finally :
    if ttfis: 
        ttfis.Quit()
        del ttfis
    rootLogger.removeHandler(handler)
    handler.close()
    del log

# This is the end
