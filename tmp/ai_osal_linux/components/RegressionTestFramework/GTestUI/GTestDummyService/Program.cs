using System;
using System.Collections.Generic;
using System.Text;

namespace GTestDummyService
{
	class Program
	{


		static int Main(string[] args)
		{
			sm_port = null;
			if(!parseArgs(args,0u,ref sm_port))
				return 5;

		  System.Threading.Thread thr;
			thr = new System.Threading.Thread(threadProc);

			thr.Start();

			return 0;
		}

		static void threadProc()
		{
			// create stuff...
		  GTestCommon.Links.TcpPacketLink lnk = new GTestCommon.Links.TcpPacketUplink();
		  GTestAdapter_Test.MockService srv = new GTestAdapter_Test.MockService();
		  GTestCommon.Links.QueuedStringsLink con = new GTestCommon.Links.LocalConsole();
		  System.Threading.WaitHandle[] handles = new System.Threading.WaitHandle[4];
			// add another testgroup
		  GTestCommon.Models.AllTestCases testSet = srv.testSet;
		  GTestCommon.Models.TestGroupModel grp = testSet.AppendNewTestGroup("more tests");
		  System.Threading.EventWaitHandle evt = new System.Threading.ManualResetEvent(false);
		  bool timerStarted;
			for( uint i=0 ; i<15 ; i++ )
			{
				grp.AppendNewTest( string.Format("useless Test {0}",i+1u) , testSet.highestTestID+1 );
			}
			// start up the stuff
			srv.link.Start(null);
			srv.link.mock_connect();
			lnk.Start(sm_port);
			con.Start(null);
			// set up wait handles
			handles[0] = lnk.inQueue.getWaitHandle();
			handles[1] = srv.link.inQueue.getWaitHandle();
			handles[2] = con.inQueue.getWaitHandle();
			handles[3] = evt;
			// prepare loop
		  bool bQuit = false;
			// prepare fake 'watcher' just for artificial slowdown.
		  GTestAdapter_Test.MockService.Sniff slowDown;
			slowDown = new GTestAdapter_Test.MockService.Sniff(delegate(GTestCommon.IPacket pck,bool bDownToSrv)
				{
					if(!bDownToSrv)
						System.Threading.Thread.Sleep(25);
					return false;
				}
			);
			// send start-tests if selected
			if(sm_autoStart)
			{
			  GTestCommon.Links.PacketRunTests pck;
			  uint[] IDlist = new uint[0];
				pck = new GTestCommon.Links.PacketRunTests( IDlist , true , 0 );
				srv.link.outQueue.Add(pck);
			}

			timerStarted = false;

			System.Console.Error.WriteLine(String.Format("GTestService running (port={0})",sm_port));

			// main loop
			while(!bQuit)
			{
			  int w;
			  GTestCommon.IPacket pck;
				// Do the service simulation.
				while( srv.processThings(slowDown,10) )
				{
					checkTimer( ref evt , lnk.outQueue );
				}
				if(bQuit)break;
				// wait for events.
				w = System.Threading.WaitHandle.WaitAny(handles);
				switch(w)
				{
				case 0:		// packet from tcp
					while((pck=lnk.inQueue.Get(false))!=null)
						srv.link.outQueue.Add(pck);
					// if first time, start timer.
					if( ! timerStarted )
					{
						// start one-shot timer on 'evt'.
						timerStarted = true;
						new System.Threading.Timer( tmrCallbackFunc , evt , 1000u , System.Threading.Timeout.Infinite );
					}
					break;
				case 1:		// packet from service
					while((pck=srv.link.inQueue.Get(false))!=null)
						lnk.outQueue.Add(pck);
					break;
				case 2:		// commandline. just quit.
					{
					  string lin;
						while((lin=con.inQueue.Get(false))!=null)
							bQuit=true;
					}
					break;
				case 3:
					// the timer. Ignore. Is polled.
					break;
				}
				checkTimer( ref evt , lnk.outQueue );
			}
			lnk.Stop(true);lnk.Dispose();
			con.Stop(true);con.Dispose();
		}

		static private void checkTimer(ref System.Threading.EventWaitHandle evt,GTestCommon.Queue<GTestCommon.IPacket> q)
		{
			if(evt.WaitOne(0))
			{
				evt.Reset();
				System.Console.WriteLine("sending pcycle request packet.");
				q.Add( new GTestCommon.Links.PacketPcycleRequest() );
			}
		}

		static bool parseArgs(string[] args,uint index0,ref string UIport)
		{
			for( int i=(int)index0 ; i<args.Length ; i++ )
			{
			  string arg = args[i];
				if( arg.StartsWith("/") )arg = "-"+arg.Substring(1);
				switch(arg.Trim().ToLower())
				{
				case "-p":
				case "--port":
					i++;if(i>=args.Length){System.Console.Error.WriteLine("not value after "+arg);return false;}
					UIport = args[i];
					break;
				default:
					System.Console.Error.WriteLine("unknown arg "+arg);return false;
				}
			}
			if( UIport==null )
				{System.Console.Error.WriteLine("no value for port (-p).");return false;}
			return true;
		}

		// asynchronous callback from system timer. use signal to synchronize this.
		static private void tmrCallbackFunc(object state)
		{
			((System.Threading.EventWaitHandle)state).Set();
		}

		static string sm_port;

		static bool sm_autoStart = true;

	}
}
