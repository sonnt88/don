using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GTestCommon.Models;
using GTestUI.Controllers;
using System.Text.RegularExpressions;
using System.Threading;
using GTestUI.Controllers.OutputHandlers.Interfaces;
using GTestUI.Controllers.Interface;

namespace GTestUI
{
    enum PLAYACTION
    {
        RUN_TESTS,
        RESET
    }

    public partial class FormMain : Form
    {
        private ITestRunController controller;
        private AllTestCases model;
        private int SeedValue;

        private Thread guiThread;
        private bool stopThread;
        private String[] outputLines;
        private String searchFor;
        private bool searchCaseSensitive;
        private AsyncOperation asyncGuiOp;
        private List<int> searchResultIndexes;
        private int currentSearchResult;

        private FormTestRegEx formTestRegEx;

        private int outputLength;

        private PLAYACTION _actionOnToolStripButtonPlay_Click;
        private PLAYACTION actionOnToolStripButtonPlay_Click
        {
            get { return _actionOnToolStripButtonPlay_Click; }
            set
            {
                if (value == PLAYACTION.RUN_TESTS)
                {
                    this.toolStripButtonPlay.Image = Properties.Resources.play;
                    this.toolStripButtonPlay.Text = "run tests";
                }
                else
                {
                    this.toolStripButtonPlay.Image = Properties.Resources.reset;
                    this.toolStripButtonPlay.Text = "reset tests";
                }

                _actionOnToolStripButtonPlay_Click = value;
            }
        }

        public FormMain()
        {
            InitializeComponent();

            this.treeViewTestCases.ImageList = new ImageList();
            this.treeViewTestCases.ImageList.ImageSize = new Size(24, 24);
            this.treeViewTestCases.ImageList.Images.Add(Properties.Resources.group);
            this.treeViewTestCases.ImageList.Images.Add(Properties.Resources.test);
            this.treeViewTestCases.ImageList.Images.Add(Properties.Resources.disabled);
            this.treeViewTestCases.ImageList.Images.Add(Properties.Resources.passed);
            this.treeViewTestCases.ImageList.Images.Add(Properties.Resources.failed);

            this.guiThread = null;
            this.asyncGuiOp = AsyncOperationManager.CreateOperation(null);
            this.searchResultIndexes = new List<int>();

            this.initGTest();
        }

        private void initGTest()
        {
            try
            {
                GTestCommandLineOpts opts = new GTestCommandLineOpts(new List<String>(System.Environment.GetCommandLineArgs()));

                if (opts.ExecutablePath != null)
                {
                    CreateController_Process(opts.ExecutablePath);
                    if (opts.Shuffle)
                    {
                        this.shuffleTestsToolStripMenuItem.Checked = true;
                        if (opts.RandomSeed > -1)
                        {
                            this.textBoxRandomSeed.Text = System.Convert.ToString(opts.RandomSeed);
                            this.SeedValue = opts.RandomSeed;
                        }
                    }

                    this.numericUpDownReapeat.Value = opts.Repeat;

                    if (opts.RunDisabledTests)
                    {
                        this.enableAllTestsByDefaultToolStripMenuItem.Checked = true;
                    }

                    if (opts.Filter != null)
                        this.controller.EnableTestsByGTestFilter(opts.Filter);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception occured: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fillInStatusBar()
        {
          int max = model.TestsToRun;
          int cur = model.TestsToRun - model.RemainingTestCount;
            if( cur>max ) cur=max;
            if( cur<0 )cur=0;
            try{
                this.toolStripProgressBar1.Maximum = max;
                this.toolStripProgressBar1.Value = cur;
            }catch(ArgumentOutOfRangeException)
                {}   // ignore. Can happen when connecting to a running test. In this case the view is not yet set up properly.

            this.toolStripStatusLabelResults.Text = "TOTAL: " + model.TestCount + ", PASSED: " + model.PassedTestCount + ", FAILED: " + model.FailedTestCount + ", DISABLED: " + model.DisabledTestCount;
            this.statusStrip1.Update();
        }

        private void fillTestCaseTreeView(AllTestCases model)
        {
            this.treeViewTestCases.Nodes.Clear();
            for (int testGroupIndex = 0; testGroupIndex < model.TestGroupCount; testGroupIndex++)
            {
                TreeNode newGroupNode = new TreeNode(model[testGroupIndex].Name);
                newGroupNode.ImageIndex = (int)(IconIndexDefinition.Group);
                newGroupNode.SelectedImageIndex = newGroupNode.ImageIndex;
                
                for (int testIndex = 0; testIndex < model[testGroupIndex].TestCount; testIndex++)
                {
                    TreeNode newTestNode = new TreeNode(model[testGroupIndex][testIndex].Name);
                    if (model[testGroupIndex][testIndex].DisabledByDefault)
                        newTestNode.ImageIndex = (int)(IconIndexDefinition.Disabled);
                    else
                        newTestNode.ImageIndex = (int)(IconIndexDefinition.Test);
                    newTestNode.SelectedImageIndex = newTestNode.ImageIndex;
                    newTestNode.Checked = model[testGroupIndex][testIndex].Enabled;

                    newGroupNode.Nodes.Add(newTestNode);
                }

                this.treeViewTestCases.Nodes.Add(newGroupNode);
                RefreshTestGroupTreeNode(model[testGroupIndex]);
            }

            fillInStatusBar();

        }

        /// <summary>
        /// Adds a new TestRunController of the type controlling an external GTest process.
        /// 
        /// </summary>
        /// <param name="GTestBinaryFilename">path to GTest binary (exe file)</param>
        private void CreateController_Process(string GTestBinaryFilename)
        {
          bool incDisabled = this.enableAllTestsByDefaultToolStripMenuItem.Checked;
            SetNewController( new TestRunController_Process(GTestBinaryFilename,incDisabled) );
        }

        /// <summary>
        /// Adds a new TestRunController of the type connecting to a GTestAdapter via TCP/IP.
        /// 
        /// </summary>
        /// <param name="addressString">network address to adapter (IP:port)</param>
        private void CreateController_Adapter(string addressString)
        {
          bool incDisabled = this.enableAllTestsByDefaultToolStripMenuItem.Checked;
            SetNewController( new TestRunController_Adapter(addressString,incDisabled) );
        }

        private void SetNewController(ITestRunController newCtrl)
        {
            // same?
            if( object.ReferenceEquals( newCtrl , this.controller ) )
                return;
            // drop old first?
            if( this.controller!=null )
                dropController();
            // now set up.
            this.controller = newCtrl;		// ..... TODO: unlink old controller if present?
            this.controller.eventTestProcessFinished += new TestProcessFinishedEventHandler(OnTestProcessFinished);
            this.controller.eventTestOutputLineCaptured += new TestOutputLineCapturedEventHandler(OnTestOutputLineCaptured);
            this.controller.eventTestSetChanged += new TestSetChanged(OnTestSetChanged);
            this.controller.Repeat = (uint)(this.numericUpDownReapeat.Value);

            // set our this.model var, and link events in model.
            OnTestSetChanged();

            actionOnToolStripButtonPlay_Click = PLAYACTION.RUN_TESTS;
        }

        private void dropController()
        {
            if(controller==null)return;
            this.controller.Dispose();
            this.controller = null;
            if(model!=null)
            {
                // ..... unlink events?
            }
            model=null;
        }

        void OnTestOutputLineCaptured(String Line, Dictionary<String, Color> HighlightDictionary)
        {
            this.richTextBoxTestOutput.SelectionStart = outputLength;
            this.richTextBoxTestOutput.SelectedText = Line;

            if (HighlightDictionary != null)
            {
                foreach (String key in HighlightDictionary.Keys)
                {
                    int indexOfKey = Line.IndexOf(key);
                    if (indexOfKey > -1)
                    {
                        this.richTextBoxTestOutput.SelectionStart = outputLength + indexOfKey;
                        this.richTextBoxTestOutput.SelectionLength = key.Length;
                        this.richTextBoxTestOutput.SelectionColor = HighlightDictionary[key];
                        this.richTextBoxTestOutput.SelectionFont = new Font(this.richTextBoxTestOutput.Font, FontStyle.Bold);
                    }
                }
            }

            outputLength += Line.Length;

            /*this.richTextBoxTestOutput.SelectionStart = outputLength;
            this.richTextBoxTestOutput.ScrollToCaret();*/
            
        }

        void OnTestSetChanged()
        {
            // There is a new testSet. Drop all tests from UI and show this new set.
            // .........

            model = controller.Model;
            model.eventTestExecutionStateChanged += new ExtTestGroupEventHandler(OnEventTestExecutionStateChanged);
            model.eventTestEnabledChanged += new ExtTestGroupEventHandler(OnTestEnabledChanged);
            model.eventTestGroupSelectionChanged += new TestGroupEventHandler(OnTestGroupSelectionChanged);

            fillTestCaseTreeView(model);
            fillInStatusBar();

//            throw new System.Exception();
        }

        void OnTestProcessFinished()
        {
            actionOnToolStripButtonPlay_Click = PLAYACTION.RESET;
        }

        private void openGTestBinaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "select googletest binary";

			if (openDialog.ShowDialog() == DialogResult.OK)
			{
				this.richTextBoxTestOutput.Clear();
				CreateController_Process(openDialog.FileName);
			}
        }

        private void connectAdapterToolStripMenuItem_Click(object sender, EventArgs e)
        {
          FormConnAdapter f = new FormConnAdapter();
            //f.connect_address = "localhost:9999";
            //f.create_UIport = "9999";
            f.create_serviceAddress = "localhost:666";
            f.create_COMport = "COM2";
            f.ShowDialog();

            if( f.DialogResult != DialogResult.OK )
                return;		// user cancelled this.

            if( f.connect_selected )
            {
                // user selected to connect to an existing adapter.
                CreateController_Adapter( f.connect_address );
            }else{
                // user selected to connect to a newly created adapter.
              ushort UIport = System.Convert.ToUInt16(f.create_UIport);
                AdapterSpawner.SpawnNewAdapter(
                        f.create_serviceAddress  ,
                        UIport  ,
                        null  ,  null  ,  null  ,
                        f.create_COMport
                );
                // then connect to it.
                CreateController_Adapter( "127.0.0.1:"+f.create_UIport.ToString() );
            }
        }

        private void RefreshTestGroupTreeNode(TestGroupModel testGroupModel)
        {
            int index = this.model.IndexOf(testGroupModel);
            TreeNode groupNode = this.treeViewTestCases.Nodes[index];

            switch (testGroupModel.TestGroupSelection)
            {
                case TESTGROUPSELECTION.NONE:
                    groupNode.Checked = false;
                    groupNode.ForeColor = Color.Gray;
                    break;
                case TESTGROUPSELECTION.PARTLY:
                    groupNode.Checked = true;
                    groupNode.ForeColor = Color.Gray;
                    break;
                case TESTGROUPSELECTION.ALL_BUT_DEFAULT_DISABLED:
                    if (this.model.EnableDefaultDISABLED_Tests)
                    {
                        groupNode.Checked = true;
                        groupNode.ForeColor = Color.Gray;
                    }
                    else
                    {
                        groupNode.Checked = true;
                        groupNode.ForeColor = Color.Black;
                    }
                    break;
                case TESTGROUPSELECTION.ALL:
                    groupNode.Checked = true;
                    groupNode.ForeColor = Color.Black;
                    break;
                default:
                    throw new Exception("Unknown exception: should never reach this.");
            }
        }

        private void RefreshAllTestGroupNodes()
        {
            for (int testGroupIndex = 0; testGroupIndex < this.model.TestGroupCount; testGroupIndex++)
                RefreshTestGroupTreeNode(this.model[testGroupIndex]);
        }

        private void OnTestGroupSelectionChanged(TestGroupModel sender)
        {
            RefreshTestGroupTreeNode(sender);
        }

        void OnTestEnabledChanged(TestGroupModel TestGroup, Test tst)
        {
            TreeNode testGroupNode = this.treeViewTestCases.Nodes[this.model.IndexOf(TestGroup)];
            TreeNode testNode = testGroupNode.Nodes[TestGroup.IndexOf(tst)];

            testNode.Checked = tst.Enabled;
            fillInStatusBar();
        }

        void OnEventTestExecutionStateChanged(TestGroupModel TestGroup, Test tst)
        {
            TreeNode testGroupNode = this.treeViewTestCases.Nodes[this.model.IndexOf(TestGroup)];
            TreeNode testNode = testGroupNode.Nodes[TestGroup.IndexOf(tst)];

            if (tst.IterationCount > 0)
            {
                testNode.Text = tst.Name +
                    " (PASSED: " + System.Convert.ToString(tst.PassedCount) +
                    ", FAILED: " + System.Convert.ToString(tst.FailedCount) +
                    ", TOTAL: " + System.Convert.ToString(tst.PassedCount + tst.FailedCount) + ")";

                if (tst.FailedCount == 0)
                    testNode.ImageIndex = (int)(IconIndexDefinition.Passed);
                else
                    testNode.ImageIndex = (int)(IconIndexDefinition.Failed);
            }
            else
            {
                testNode.Text = tst.Name;
                testNode.ImageIndex = (int)(IconIndexDefinition.Test);
            }

            if (TestGroup.FailedTestCount > 0)
                testGroupNode.ImageIndex = (int)(IconIndexDefinition.Failed);
            else
                testGroupNode.ImageIndex = (int)(IconIndexDefinition.Group);

            testNode.SelectedImageIndex = testNode.ImageIndex;
            testGroupNode.SelectedImageIndex = testGroupNode.ImageIndex;

            fillInStatusBar();
        }

        private void toolStripButtonPlay_Click(object sender, EventArgs e)
        {
            if (this.controller == null)
                return;

            switch (actionOnToolStripButtonPlay_Click) {
                case PLAYACTION.RUN_TESTS:
                    {
                        int Seed = 0;
                        bool Shuffle = this.shuffleTestsToolStripMenuItem.Checked;
                        if (Shuffle)
                            Seed = this.SeedValue;

                        this.numericUpDownReapeat.Enabled = false;
                        this.outputLength = 0;
                        this.controller.StartTests(Shuffle, Seed);
                        break;
                    }
                case PLAYACTION.RESET:
                    {
                        this.richTextBoxTestOutput.Clear();
                        this.outputLength = 0;
                        this.controller.ResetTests();
                        this.actionOnToolStripButtonPlay_Click = PLAYACTION.RUN_TESTS;
                        this.numericUpDownReapeat.Enabled = true;
                        break;
                    }
                default:
                    throw new Exception("Unknown exception: this should never be reached.");
            }
        }

        private void toolStripButtonStop_Click(object sender, EventArgs e)
        {
            if (this.controller == null)
                return;

            this.controller.StopTests();
        }

        private void treeViewTestCases_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action == TreeViewAction.Unknown)
                return;

            TreeNode treeNode = e.Node;
            int testGroupIndex = -1;
            int testIndex = -1;

            //get indexes

            if (treeNode.Parent == null)
                testGroupIndex = this.treeViewTestCases.Nodes.IndexOf(treeNode);
            else
            {
                testGroupIndex = this.treeViewTestCases.Nodes.IndexOf(treeNode.Parent);
                testIndex = treeNode.Parent.Nodes.IndexOf(treeNode);
            }

            if (treeNode.Parent == null) //this is a group node
            {
                if (treeNode.Checked)
                    this.model.EnableTestGroup(testGroupIndex);
                else
                    this.model.DisableTestGroup(testGroupIndex);
            }
            else //this is a test node
            {
                this.model[testGroupIndex][testIndex].Enabled = treeNode.Checked;
            }

        }

        private void enableAllTestsByDefaultToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            if (this.controller == null)
                return;

            this.model.EnableDefaultDISABLED_Tests = this.enableAllTestsByDefaultToolStripMenuItem.Checked;
        }

        private void toolStripMenuItemCollapseAll_Click(object sender, EventArgs e)
        {
            this.treeViewTestCases.CollapseAll();

        }

        private void toolStripMenuItemExpandAll_Click(object sender, EventArgs e)
        {
            this.treeViewTestCases.ExpandAll();
        }

        private void enableAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.controller == null)
                return;

            this.model.EnableAll();    
        }

        private void disableAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.controller == null)
                return;

            this.model.DisableAll();
        }

        private void textBoxRandomSeed_TextChanged(object sender, EventArgs e)
        {
            int Value = 0;
            try {
                if (textBoxRandomSeed.Text.CompareTo("") != 0)
                {
                    Value = System.Convert.ToInt32(textBoxRandomSeed.Text);
                    if (Value >= 0 && Value <= 99999)
                        this.SeedValue = Value;
                }
                else
                {
                    this.SeedValue = 0;
                }
            }
            catch (Exception)
            {
            }

            if (textBoxRandomSeed.Text.CompareTo("") != 0)
                textBoxRandomSeed.Text = System.Convert.ToString(this.SeedValue);
        }

        private void shuffleTestsToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            if (shuffleTestsToolStripMenuItem.Checked)
            {
				labelRandomSeed.Visible = true;
                textBoxRandomSeed.Visible = true;
            }
            else
            {
				labelRandomSeed.Visible = false;
				textBoxRandomSeed.Visible = false;
            }

        }

        private void numericUpDownReapeat_ValueChanged(object sender, EventArgs e)
        {
            if (this.controller != null)
                this.controller.Repeat = (uint)(this.numericUpDownReapeat.Value);
        }

        private void ClearOutputColors()
        {
            this.richTextBoxTestOutput.SelectionStart = 0;
            this.richTextBoxTestOutput.SelectionLength = this.richTextBoxTestOutput.Text.Length;
            this.richTextBoxTestOutput.SelectionBackColor = SystemColors.ActiveCaptionText;
        }

        private void HightlightSubstringInOutput(int SelectionIndex, int Length)
        {
            asyncGuiOp.Post(new SendOrPostCallback(delegate(object obj)
            {
                this.richTextBoxTestOutput.SelectionStart = SelectionIndex;
                this.richTextBoxTestOutput.SelectionLength = Length;
                this.richTextBoxTestOutput.SelectionBackColor = Color.Yellow;
            }), null);
        }

        private void OnSearchFinished()
        {
            asyncGuiOp.Post(new SendOrPostCallback(delegate(object obj)
            {
                stopGuiThread();
                ShowSearchResult();

                if (searchResultIndexes.Count > 0)
                    this.richTextBoxTestOutput.Focus();

            }), null);
        }

        private void HighlightOutput(String StringToHightlight, StringComparison comparisonType)
        {
            int AbsoluteSelectionIndex = 0;
            for (int lineIndex = 0; lineIndex < outputLines.Length; lineIndex++)
            {
                int startIndexInLine = 0;
                while (true)
                {
                    int charIndexInLine = outputLines[lineIndex].IndexOf(StringToHightlight, startIndexInLine, comparisonType);
                    if (charIndexInLine < 0)
                        break;

                    searchResultIndexes.Add(AbsoluteSelectionIndex + charIndexInLine);
                    HightlightSubstringInOutput(AbsoluteSelectionIndex + charIndexInLine, StringToHightlight.Length);

                    startIndexInLine = charIndexInLine + StringToHightlight.Length;
                    if (startIndexInLine >= outputLines[lineIndex].Length)
                        break;
                }
                AbsoluteSelectionIndex += outputLines[lineIndex].Length + 1;

                if (stopThread)
                    break;
            }
        }

        private void HightlightOutput(Regex RegEx)
        {
            int AbsoluteSelectionIndex = 0;
            for (int lineIndex = 0; lineIndex < outputLines.Length; lineIndex++)
            {
                Match match = RegEx.Match(outputLines[lineIndex]);
                while (match.Success)
                {
                    searchResultIndexes.Add(AbsoluteSelectionIndex + match.Index);
                    HightlightSubstringInOutput(AbsoluteSelectionIndex + match.Index, match.Length);
                    match = match.NextMatch();
                }
                AbsoluteSelectionIndex += outputLines[lineIndex].Length + 1;

                if (stopThread)
                    break;
            }
        }

        private void searchOutputViaString()
        {
            StringComparison comparisonType;

            if (searchCaseSensitive)
                comparisonType = StringComparison.InvariantCulture;
            else
                comparisonType = StringComparison.InvariantCultureIgnoreCase;

            HighlightOutput(searchFor, comparisonType);

            if (!stopThread)
                OnSearchFinished();
        }

        private void searchOutputViaRegEx()
        {
            
            Regex regEx = null;

            if (searchCaseSensitive)
                regEx = new Regex(searchFor);
            else
                regEx = new Regex(searchFor, RegexOptions.IgnoreCase);

            HightlightOutput(regEx);

            if (!stopThread)
                OnSearchFinished();
        }

        private void stopGuiThread()
        {
            if (guiThread != null)
            {
                stopThread = true;
                guiThread.Join();
                guiThread = null;
            }
        }

        private bool RegExIsValid(String RegEx)
        {
            bool regExIsValid = true;
            try
            {
                Regex regEx = new Regex(RegEx);
            }
            catch (System.ArgumentException)
            {
                regExIsValid = false;
            }
            return regExIsValid;
        }

        private void verifyRegEx(object sender, EventArgs e)
        {
            if (cbUseRegEx.Checked)
            {
                if (RegExIsValid(this.richTextBoxSearch.Text))
                {
                    this.richTextBoxSearch.ForeColor = Color.Black;
                    this.buttonSearchOutput.Enabled = true;
                }
                else
                {
                    this.richTextBoxSearch.ForeColor = Color.Red;
                    this.buttonSearchOutput.Enabled = false;
                }
            }
        }

        private void performOutputSearch()
        {
            stopGuiThread();

            ClearOutputColors();

            searchFor = this.richTextBoxSearch.Text;
            outputLines = (string[])this.richTextBoxTestOutput.Lines.Clone();

            if (searchFor.Length == 0)
            {
                ClearOutputColors();
                return;
            }

            searchCaseSensitive = cbMatchCase.Checked;

            if (cbUseRegEx.Checked)
            {
                if (RegExIsValid(searchFor))
                {
                    this.richTextBoxSearch.ForeColor = Color.Black;
                    guiThread = new Thread(searchOutputViaRegEx);
                }
                else
                {
                    this.richTextBoxSearch.ForeColor = Color.Red;
                }
            }
            else
            {
                this.richTextBoxSearch.ForeColor = Color.Black;
                guiThread = new Thread(searchOutputViaString);
            }

            stopThread = false;
            searchResultIndexes.Clear();
            currentSearchResult = 0;
            guiThread.Start();
        }

        private void buttonSearchOutput_Click(object sender, EventArgs e)
        {
            performOutputSearch();
        }

        void OnFormTestRegExClosed(object sender, FormClosedEventArgs e)
        {
            if (formTestRegEx.Result == DialogResult.OK)
            {
                SwitchEnabledStateViaRegEx(formTestRegEx.TestGroupRegEx, formTestRegEx.TestRegEx, formTestRegEx.EnableSelection);
            }
            formTestRegEx = null;
        }

        private void SwitchEnabledStateViaRegEx(string TestGroupRegEx, string TestRegEx, bool EnableSelection)
        {
            if (controller == null)
                return;

            Regex fullRegEx = new Regex(TestGroupRegEx + @"\." + TestRegEx);

            for (int TestGroupIndex = 0; TestGroupIndex < model.TestGroupCount; TestGroupIndex++)
            {
                for (int TestIndex = 0; TestIndex < model[TestGroupIndex].TestCount; TestIndex++)
                {
                    string fullTestName = model[TestGroupIndex].Name + "." + model[TestGroupIndex][TestIndex].Name;
                    Match match = fullRegEx.Match(fullTestName);
                    if (match.Success)
                        model[TestGroupIndex][TestIndex].Enabled = EnableSelection;
                }
            }
        }

        private void enabledisableViaRegExToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (formTestRegEx == null)
            {
                formTestRegEx = new FormTestRegEx();
                formTestRegEx.FormClosed += new FormClosedEventHandler(OnFormTestRegExClosed);
                formTestRegEx.Show();
            }
            else
            {
                formTestRegEx.BringToFront();
            }
        }

        private void ShowSearchResult()
        {
            if (searchResultIndexes.Count == 0)
                return;

            richTextBoxTestOutput.SelectionStart = searchResultIndexes[currentSearchResult];
            richTextBoxTestOutput.ScrollToCaret();
        }

        private void moveToNextSearchResult()
        {
            if (currentSearchResult < searchResultIndexes.Count - 1)
                currentSearchResult++;

            ShowSearchResult();
        }

        private void moveToPreviousSearchResult()
        {
            if (currentSearchResult > 0)
                currentSearchResult--;

            ShowSearchResult();
        }

        private void buttonSearchNext_Click(object sender, EventArgs e)
        {
            moveToNextSearchResult();
        }

        private void buttonSearchPrev_Click(object sender, EventArgs e)
        {
            moveToPreviousSearchResult();
        }

        private void richTextBoxSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && buttonSearchOutput.Enabled)
                performOutputSearch();
        }

        private void richTextBoxTestOutput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'n' || e.KeyChar == 'N')
                moveToNextSearchResult();
            if (e.KeyChar == 'p' || e.KeyChar == 'P')
                moveToPreviousSearchResult();
        }

        private void onClose(object sender, FormClosedEventArgs e)
        {
            if(controller!=null)
                dropController();
        }

    }
}