using GTestUI.Controllers.Interface;
using System.Collections.Generic;
using GTestCommon;
using GTestCommon.Links;
using GTestCommon.Models;
using System.Drawing;

/*

cd /D C:\Dahlhoff\GTestAdapter
start GTestDummyService\bin\Debug\GTestDummyService.exe -p 666
GTestAdapter\bin\Debug\GTestAdapter.exe -g 9999 -s 127.0.0.1:666

 */


namespace GTestUI.Controllers
{

public class TestRunController_Adapter : ITestRunController
{
	public Dictionary<string, Color> hightlightDictionary;

        
	public TestRunController_Adapter(string address,bool includeDisabledTests)
	{
		hightlightDictionary = new Dictionary<string, Color>();

		hightlightDictionary.Add("[       OK ]", Color.Green);
		hightlightDictionary.Add("[  FAILED  ]", Color.Red);

		m_address = address;
		m_includeDisabledTests = includeDisabledTests;
		m_state = State.DONT_KNOW;
		m_connected = false;
		// create async marshall
		m_sync = System.ComponentModel.AsyncOperationManager.CreateOperation(null);
		// create emtpy dummy testSet here.
		m_testSet = new AllTestCases(m_includeDisabledTests);
		m_testSet.CheckSum = 1;
		m_testSetValid = false;		// it's a dummy...
		// space for results received early.
		m_bufferedResultPackets = new List<TestResultModel>();
		// create adapter connection
		m_link = new TcpPacketLink();
		m_link.connect += new ConnectEventHandler(eventHaveConnection);
		m_link.inQueue.notEmptyEvent += new GTestCommon.Queue<IPacket>.QueueNotEmpty(eventInData);
		m_link.Start(m_address);
		// we will get the testlist sometime later.
	}

	public void Dispose()
	{
		m_link.Stop(true);
		m_link.Dispose();
	}

	public AllTestCases Model{get{return m_testSet;}}
	public uint Repeat{get{return m_numRepeats;}set{m_numRepeats=value;}}
	public bool Connected{get{return m_connected&&m_testSetValid;}}

	public void StartTests(bool Shuffle, int Seed)
	{
	  List<uint> IDs;
	  bool inv;
		if( Connected && m_state==State.IDLE )
		{
		  PacketRunTests pck;
			if( m_numRepeats<1u )
				m_numRepeats = 1u;
			m_testSet.Repeat = (int)m_numRepeats;
			ResetTests();
			// build packet
			IDs = getSelectedTestIDs(m_testSet,out inv);
			pck = new PacketRunTests( IDs , inv , m_testSet.CheckSum );
			pck.numRepeats = m_numRepeats;
			// set local state
			m_state = State.TESTING;
			// and send.
			m_link.outQueue.Add(pck);
		}else{
			// ..... TODO: error   not-connected ?
		}
	}

	public void ResetTests()
	{
		for( int i=0 ; i<m_testSet.TestGroupCount ; i++ )
		{
		  TestGroupModel grp = m_testSet[i];
			for( int j=0 ; j<grp.TestCount ; j++ )
			{
			  Test tst = grp[j];
				tst.ResetExecutionState();
			}
		}
		m_bufferedResultPackets.Clear();
	}

	public void StopTests()
	{
		if(m_connected)
		{
			m_link.outQueue.Add( new PacketAbortTestsReq() );
		}
	}

	public void EnableTestsByGTestFilter(string FilterRegEx)
	{
		throw new System.Exception("not implemented.");
	}

	void eventHaveConnection(bool bNowConnected)
	{
		m_sync.Post( delegate(object o)
		{
			if((System.Boolean)o)
				onConnect();
			else
				onDisconnect();
		}
		, bNowConnected );
	}

	void eventInData(GTestCommon.Queue<IPacket> q)
	{
		m_sync.Post( delegate(object o)
		{
		  Packet pck;
			while((pck=(Packet)m_link.inQueue.Get(false))!=null)
				processInPacket(pck);
		}
		, q );
	}

	private void onConnect()
	{
		// have connection. Ask for version, state and testlist.
		m_connected = true;
		m_testSetValid = false;
		m_link.outQueue.Add( new PacketGetVersion() );
		m_link.outQueue.Add( new PacketGetState() );
		m_link.outQueue.Add( new PacketGetTestList() );
	}

	private void onDisconnect()
	{
	  bool prevState = Connected;
		// offline. what now?
		m_connected = false;
		m_testSetValid = false;
		// ..... TODO.
		// signal.
		if( (!prevState) && eventTestControllerConnected != null )
			eventTestControllerConnected(false);
	}

	private void processInPacket(Packet pck)
	{
		if( pck is PacketVersion )
		{
			// great. what now? .....
		}else
		if( pck is PacketState )
		{
		  PacketState pp = (PacketState)pck;
		  bool change;
			switch( pp.state )
			{
			case 0:
				if(m_state==State.TESTING)
				{
					if( eventTestProcessFinished!=null )
						eventTestProcessFinished.Invoke();
				}
				m_state = State.IDLE;break;
			case 1:
				change = (m_state != State.TESTING);
				m_state = State.TESTING;
				if(change)
				{
					// connecting into a running test.
					// query all results.
					m_link.outQueue.Add( new PacketGetAllResults() );
				}
				break;
			default:
				m_state = State.DONT_KNOW;
				// ...... error?
				break;
			}
			// and now? ..... TODO.
		}else
		if( pck is PacketTestList )
		{
		  PacketTestList pp = (PacketTestList)pck;
			if( m_testSet.CompareSet(pp.data) )
				return;		// same test set. nothing to be changed.
			// is diffrent. change. This will also drop all settings and results in the model.
			m_testSet = pp.data;
			if( eventTestSetChanged!=null )
				eventTestSetChanged.Invoke();
			if(!m_testSetValid)
			{
				m_testSetValid = true;
				if( m_connected && eventTestControllerConnected != null )
					eventTestControllerConnected(true);
			}
		}else
		if( pck is PacketTestResult )
		{
		  TestResultModel res = ((PacketTestResult)pck).result;
			try{
				m_testSet.AddResult( res , true );
			}catch(KeyNotFoundException)
			{
				// this happens when connecting to a running test. In this case, ignore.
				// ..... TODO: get those out later!
				m_bufferedResultPackets.Add(res);
			}
		}else
		if( pck is PacketAllTestrunsDone )
		{
			m_state = State.IDLE;
			if( eventTestProcessFinished!=null )
				eventTestProcessFinished.Invoke();
		}else
		if( pck is PacketAbortAck )
		{
			m_state = State.IDLE;
			if( eventTestProcessFinished!=null )
				eventTestProcessFinished.Invoke();
		}else
		if( pck is PacketSendAllResultsDone )
		{
		}else
		if( pck is PacketAckTestRun )
		{
			m_state = State.TESTING;
		}else
		if( pck is PacketUpTestOutput )
		{
		  PacketUpTestOutput pp = (PacketUpTestOutput)pck;
			processTestOutputLine( pp.output , pp.source );
		}else
		{
			// some unknown packet. What now? crash out? error? log somewhere?
			// ...... TODO: what now?
		}
	}

	public static List<uint> getSelectedTestIDs(AllTestCases testSet,out bool invertFlag)
	{
	  List<uint> enabled,disabled;
		enabled = new List<uint>();
		disabled = new List<uint>();
		for( int i=0 ; i<testSet.TestGroupCount ; i++ )
		{
		  TestGroupModel grp = testSet[i];
			for( int j=0 ; j<grp.TestCount ; j++ )
			{
			  Test tst = grp[j];
				if(tst.Enabled)
					enabled.Add(tst.ID);
				else
					disabled.Add(tst.ID);
			}
		}
		if( disabled.Count < enabled.Count )
		{
			// invert
			invertFlag = true;
			return disabled;
		}
		// normal output
		invertFlag = false;
		return enabled;
	}

	void processTestOutputLine(string outputLine, PacketUpTestOutput.OutputSource source)
	{
		if (eventTestOutputLineCaptured != null)
			eventTestOutputLineCaptured(outputLine, hightlightDictionary);
	}

	enum State
	{
	DONT_KNOW = 0,
	TESTING = 1,
	IDLE = 2
	};

	public event TestSetChanged eventTestSetChanged;
	public event TestProcessFinishedEventHandler eventTestProcessFinished;
	public event GTestUI.Controllers.OutputHandlers.Interfaces.TestOutputLineCapturedEventHandler eventTestOutputLineCaptured;
	public event TestControllerConnected eventTestControllerConnected;

	private State m_state;
	private bool m_connected;
	private AllTestCases m_testSet;
	private bool m_testSetValid;
	private bool m_includeDisabledTests;
	private uint m_numRepeats;



	private System.ComponentModel.AsyncOperation m_sync;
	private string m_address;
	private QueuedItemsLink<IPacket> m_link;
	private List<TestResultModel> m_bufferedResultPackets;

}

}
