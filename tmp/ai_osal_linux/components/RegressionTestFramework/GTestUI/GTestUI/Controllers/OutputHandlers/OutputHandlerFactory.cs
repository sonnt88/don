using System;
using System.Collections.Generic;
using System.Text;
using GTestCommon.Models;
using GTestUI.Controllers.OutputHandlers.Interfaces;

namespace GTestUI.Controllers.OutputHandlers
{
    class OutputHandlerFactory
    {
        public static ITestRunOutputHandler CreateTestRunOutputHandler()
        {
            return new GTestPrettyOutputHandler();
            //return new GTestCustomXMLOutputHandler();
        }

        public static ITestListOutputHandler CreateTestListHandler()
        {
            return new GTestTestListHandler();
        }
    }
}
