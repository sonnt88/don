using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using GTestUI.Controllers.OutputHandlers.Interfaces;
using System.Drawing;

namespace GTestUI.Controllers.OutputHandlers
{
    enum EXEC_STATES
    {
        STATE_ENVIRONMENT_SETUP
    }
    class GTestPrettyOutputHandler : ITestRunOutputHandler
    {
        public event TestResultEventHandler eventTestResult;
        public event TestOutputLineCapturedEventHandler eventTestOutputLineCaptured;

        public Dictionary<String, Color> hightlightDictionary;

        public GTestPrettyOutputHandler()
        {
            hightlightDictionary = new Dictionary<string, Color>();
            hightlightDictionary.Add("[       OK ]", Color.Green);
            hightlightDictionary.Add("[  FAILED  ]", Color.Red);
        }

        private void RaiseTestResultEvent(String TestGroupName, String TestName, bool Passed)
        {
            if (eventTestResult != null)
                eventTestResult(TestGroupName, TestName, Passed);
        }

        private void RaiseTestOutputLineCapturedEvent(String Line)
        {
            if (eventTestOutputLineCaptured != null)
                eventTestOutputLineCaptured(Line, hightlightDictionary);
        }

        public void HandleLine(string Line)
        {
            char[] charsToBeRemoved = {' ','\t','\r', '\n'};
            RaiseTestOutputLineCapturedEvent(Line.TrimEnd(charsToBeRemoved)+"\n");

            if (Line.StartsWith("[       OK ]") || Line.StartsWith("[  FAILED  ]"))
            {
                Regex resultRegEx =
                    new Regex(@"^\[.{10}\][\s](?<TestCase>[^\s]*)\.(?<Test>[^\s]*)[\s]\((?<Time>[0-9]*)[\s]ms\).*$");

                Match match = resultRegEx.Match(Line);
                if (match.Success)
                {
                    String TestGroupName = match.Groups["TestCase"].Value;
                    String TestName = match.Groups["Test"].Value;
                    bool Passed = Line.StartsWith("[       OK ]");
                    RaiseTestResultEvent(TestGroupName, TestName, Passed);
                }
            }
        }
    }
}
