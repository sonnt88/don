using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using System.IO;
using GTestUI.Controllers.OutputHandlers.Interfaces;
using System.Drawing;

namespace GTestUI.Controllers.OutputHandlers
{
    class GTestCustomXMLOutputHandler : ITestRunOutputHandler
    {
        public event TestResultEventHandler eventTestResult;
        public event TestOutputLineCapturedEventHandler eventTestOutputLineCaptured;

        private void RaiseTestResultEvent(String TestGroupName, String TestName, bool Passed)
        {
            if (eventTestResult != null)
                eventTestResult(TestGroupName, TestName, Passed);
        }

        private void RaiseTestOutputLineCapturedEvent(String Line)
        {
            if (eventTestOutputLineCaptured != null)
                eventTestOutputLineCaptured(Line, null);
        }

        private String currentXmlString;

        /// <summary>
        /// return the Regex match of the last node of the specified type
        /// </summary>
        /// <param name="NodeName">specifies the name of the node</param>
        private Match GetLastNodeMatch(String NodeName, ref String InputString)
        {
            Regex testGroupRegEx = new Regex(@"<\s*" + NodeName + @"\s[^>]*>");
            Match testGroupMatch = testGroupRegEx.Match(InputString);

            if (!testGroupMatch.Success)
                throw new ArgumentException("No such node: \"" + NodeName + "\"");

            Match resultMatch = null;
            while (testGroupMatch.Success)
            {
                resultMatch = testGroupMatch;
                testGroupMatch = testGroupMatch.NextMatch();
            }

            if (resultMatch != null)
                InputString = InputString.Substring(resultMatch.Index);

            return resultMatch;
        }

        /// <summary>
        /// we do not need to reparse the whole XML document each time,
        /// thats while we strip it down to a minimal size.
        /// </summary>
        private void StripXMLLines()
        {
            String newXMLString = "";
            Match match = GetLastNodeMatch("TestRun", ref currentXmlString);
            newXMLString += match.Value;

            match = GetLastNodeMatch("TestIteration", ref currentXmlString);
            newXMLString += match.Value;

            match = GetLastNodeMatch("TestCase", ref currentXmlString);
            newXMLString += match.Value;

            match = GetLastNodeMatch("Test", ref currentXmlString);
            newXMLString += currentXmlString;

            currentXmlString = newXMLString;
        }

        private bool checkWorthReparse(String Line)
        {
            String strippedLine = Line.Replace(" ", "");
            if (strippedLine.Contains("</Test>") ||
                strippedLine.Contains("</TestCase>") ||
                strippedLine.Contains("</TestRun>"))
                return true;
            else
                return false;
        }

        public GTestCustomXMLOutputHandler()
        {
            this.currentXmlString = "";
        }

        public void HandleLine(string Line)
        {
            currentXmlString += Line;

            RaiseTestOutputLineCapturedEvent(Line);

            if (!checkWorthReparse(Line))
                return;

            StripXMLLines();

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            settings.ValidationFlags = System.Xml.Schema.XmlSchemaValidationFlags.None;

            using (XmlReader xmlReader = XmlReader.Create(new StringReader(currentXmlString), settings))
            {
                String CurrentTestGroup = null;
                String CurrentTest = null;
                try
                {
                    while (xmlReader.Read())
                    {
                        if (xmlReader.Name.CompareTo("TestCase") == 0)
                        {
                            xmlReader.MoveToAttribute("name");
                            CurrentTestGroup = xmlReader.Value;
                        }
                        if (xmlReader.Name.CompareTo("Test") == 0)
                        {
                            xmlReader.MoveToAttribute("name");
                            CurrentTest = xmlReader.Value;
                        }
                        if (xmlReader.Name.CompareTo("TestResult") == 0)
                        {
                            xmlReader.MoveToAttribute("state");
                            bool passed = true;
                            if (xmlReader.Value.CompareTo("FAILED") == 0)
                                passed = false;

                            RaiseTestResultEvent(CurrentTestGroup, CurrentTest, passed);
                        }
                    }
                }
                catch
                {
                }
            }
        }
    }
}
