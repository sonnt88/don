using System;
using System.Collections.Generic;
using System.Text;

namespace GTestUI.Controllers.OutputHandlers.Interfaces
{
    interface IOutputHandler
    {
        void HandleLine(String Line);
    }
}
