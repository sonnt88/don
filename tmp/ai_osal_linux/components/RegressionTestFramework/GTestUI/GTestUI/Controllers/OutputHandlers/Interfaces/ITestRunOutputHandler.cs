using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GTestUI.Controllers.OutputHandlers.Interfaces
{
    public delegate void TestResultEventHandler(String TestGroupName, String TestName, bool Passed);
    public delegate void TestOutputLineCapturedEventHandler(String Line, Dictionary<String, Color> HighlightDictionary);

    interface ITestRunOutputHandler : IOutputHandler
    {
        event TestOutputLineCapturedEventHandler eventTestOutputLineCaptured;
        event TestResultEventHandler eventTestResult;
    }
}
