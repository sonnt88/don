using System;
using System.Collections.Generic;
using System.Text;

namespace GTestUI.Controllers.OutputHandlers.Interfaces
{
    public delegate void TestIdentifiedEventHandler(String TestGroupName, String TestName);
    public delegate void TestGroupIdentifiedEventHandler(String TestGroupName);

    interface ITestListOutputHandler : IOutputHandler
    {
        event TestIdentifiedEventHandler eventTestIdentified;
        event TestGroupIdentifiedEventHandler eventTestGroupIdentified;
    }
}
