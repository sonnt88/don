using System;
using System.Collections.Generic;
using System.Text;
using GTestCommon.Models;
using GTestUI.Controllers.OutputHandlers.Interfaces;

namespace GTestUI.Controllers.OutputHandlers
{
    class GTestTestListHandler : ITestListOutputHandler
    {
        public event TestIdentifiedEventHandler eventTestIdentified;
        public event TestGroupIdentifiedEventHandler eventTestGroupIdentified;

        private String currentTestGroupName = null;

        private void RaiseTestIdentifiedEvent(String TestGroupName, String TestName)
        {
            if (eventTestIdentified != null)
                eventTestIdentified(TestGroupName, TestName);
        }

        private void RaiseTestGroupIdentifiedEvent(String TestGroupName)
        {
            if (eventTestGroupIdentified != null)
                eventTestGroupIdentified(TestGroupName);
        }

        public void HandleLine(string Line)
        {
            if (Line.Trim().Length == 0)
                return;

            if (Line.StartsWith("  "))
            {
                String currentTestName = Line.Trim();
                RaiseTestIdentifiedEvent(currentTestGroupName, currentTestName);
            }
            else
            {
                currentTestGroupName = Line.Trim();

                //remove the '.' at the end of the group name
                currentTestGroupName = currentTestGroupName.Remove(currentTestGroupName.Length - 1, 1);

                RaiseTestGroupIdentifiedEvent(currentTestGroupName);
            }
        }
    }
}
