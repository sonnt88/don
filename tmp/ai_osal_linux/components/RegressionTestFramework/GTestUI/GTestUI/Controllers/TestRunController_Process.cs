using System;
using System.Collections.Generic;
using System.Text;
using GTestCommon.Models;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;
using GTestUI.Controllers.OutputHandlers;
using System.IO;
using GTestUI.Controllers.OutputHandlers.Interfaces;
using System.Drawing;
using GTestUI.Controllers.Interface;

namespace GTestUI.Controllers
{

    /// <summary>
    /// This controller handles a process with a GTest executable. It calls this as an external
    /// process with appropriate options to get the test list or run the tests.
    /// </summary>
    class TestRunController_Process : ITestRunController
    {
        public event TestSetChanged eventTestSetChanged;	// never used here...
        public event TestProcessFinishedEventHandler eventTestProcessFinished;
        public event TestOutputLineCapturedEventHandler eventTestOutputLineCaptured;
        public event TestControllerConnected eventTestControllerConnected;

        private AsyncOperation asyncOp;

        private bool stopProcess;
        private Thread WorkerThread;

        private void RaiseEventTestProcessFinished()
        {
            if (eventTestProcessFinished != null)
                eventTestProcessFinished();
        }

        private void RaiseEventTestOutputLineCaptured(String Line, Dictionary<String, Color> HighlightDictionary)
        {
            if (eventTestOutputLineCaptured != null)
                eventTestOutputLineCaptured(Line, HighlightDictionary);
        }

        private AllTestCases model;
        public AllTestCases Model
        {
            get { return this.model; }
        }

        private IOutputHandler outputHandler;

        private String binaryPath;
        private const String gtestListTestsArgs = "--gtest_list_tests";
        private String gtestExecTestsArgs;

        private ProcessStartInfo CreateStdProcessStartInfo()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = this.binaryPath;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.CreateNoWindow = true;
            return startInfo;
        }

        private void asyncShowMessageBox(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {

            asyncOp.Post(new SendOrPostCallback(delegate(object obj)
            {
                MessageBox.Show(text, caption, buttons, icon);
            }), null);
        }

        private void runGTestProcess(String Arguments)
        {
            ProcessStartInfo startInfo = CreateStdProcessStartInfo();
            startInfo.Arguments = Arguments;
            stopProcess = false;

            try
            {
                using (Process process = Process.Start(startInfo))
                {
                    while (!process.StandardOutput.EndOfStream)
                    {
                        if (stopProcess)
                        {
                            process.Kill();
                            return;
                        }
                        String line = process.StandardOutput.ReadLine();
                        outputHandler.HandleLine(line);
                    }
                    process.WaitForExit();
                }
            }
            catch (Exception e)
            {
                asyncShowMessageBox("The GTest process threw an exception of the type \"" + 
                    e.GetType().ToString() + "\". Message: \n " + e.Message, "Error", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnTestResult(string TestGroupName, string TestName, bool Passed)
        {
            bool testNotFound = false;
            try
            {
                Test tst = model[TestGroupName][TestName];
            }
            catch (KeyNotFoundException)
            {
                asyncShowMessageBox("Identified " + TestGroupName + "." + TestName + " in GTest output, but that test does not exist.", 
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                testNotFound = true;
            }

            if (testNotFound)
                return;

            asyncOp.Post(new SendOrPostCallback(delegate(object obj)
            {
                this.model.AddResult(TestGroupName, TestName, Passed);
            }), null);
        }

        private void execTestsThreadFunc()
        {
            runGTestProcess(gtestExecTestsArgs);

            asyncOp.Post(new SendOrPostCallback(delegate(object obj)
            {
                this.WorkerThread.Join();
                this.WorkerThread = null;
                RaiseEventTestProcessFinished();
            }), null);
        }

        private void CreateGTestExecTestsArgs(int RepeatCount, bool Shuffle, int Seed)
        {
            if (Seed < 0 || Seed > 99999)
                throw new ArgumentException("Seed must be in the range of [0..99999].");

            gtestExecTestsArgs  = "--gtest_repeat=" + System.Convert.ToString(RepeatCount);

            if (Shuffle)
            {
                gtestExecTestsArgs += " --gtest_shuffle";
                if (Seed > 0)
                    gtestExecTestsArgs += " --gtest_random_seed=" + System.Convert.ToString(Seed);
            }

            gtestExecTestsArgs += " --gtest_also_run_disabled_tests --gtest_filter=";
            List<String> filters = new List<string>();
            for (int testGroupIndex = 0; testGroupIndex < model.TestGroupCount; testGroupIndex++)
            {
                if (model[testGroupIndex].TestGroupSelection == TESTGROUPSELECTION.ALL)
                    filters.Add(model[testGroupIndex].Name + ".*");
                else
                {
                    for (int testIndex = 0; testIndex < model[testGroupIndex].TestCount; testIndex++)
                    {
                        if (model[testGroupIndex][testIndex].Enabled)
                            filters.Add(model[testGroupIndex].Name + "." + model[testGroupIndex][testIndex].Name);
                    }
                }
            }

            for (int filterIndex = 0; filterIndex < filters.Count; filterIndex++)
            {
                gtestExecTestsArgs += filters[filterIndex];
                if (filterIndex < filters.Count - 1)
                    gtestExecTestsArgs += ":";
            }
        }

        public void StartTests(bool Shuffle, int Seed)
        {
            int repeat = (int)Repeat;
            if (WorkerThread == null)
            {
                if (repeat == 0)
                    repeat = -1;
                ITestRunOutputHandler testRunOutputHandler = OutputHandlerFactory.CreateTestRunOutputHandler();
                testRunOutputHandler.eventTestResult += OnTestResult;
                testRunOutputHandler.eventTestOutputLineCaptured += new TestOutputLineCapturedEventHandler(OnTestOutputLineCaptured);

                this.outputHandler = testRunOutputHandler;

                WorkerThread = new Thread(execTestsThreadFunc);
                CreateGTestExecTestsArgs(repeat, Shuffle, Seed);

                WorkerThread.Start();
            }
        }

        private void OnTestOutputLineCaptured(string Line, Dictionary<String, Color> HighlightDictionary)
        {
            asyncOp.Post(new SendOrPostCallback(delegate(object obj)
            {
                RaiseEventTestOutputLineCaptured(Line, HighlightDictionary);
            }), null);        
        }

        public void ResetTests()
        {
            if (this.WorkerThread != null)
                throw new ApplicationException("Tests cannot be reset while beeing executed.");

            for (int testGroupIndex = 0; testGroupIndex < model.TestGroupCount; testGroupIndex++)
                for (int testIndex = 0; testIndex < model[testGroupIndex].TestCount; testIndex++)
                    this.model[testGroupIndex][testIndex].ResetExecutionState();
        }

        public void StopTests()
        {
            stopProcess = true;
        }

        public uint Repeat
        {
            get { return (uint)model.Repeat; }
            set { model.Repeat = (int)value; }
        }

        public bool Connected{get{return true;}}		// This controller does know a 'disconneted' state.

        public TestRunController_Process(String BinaryPath, bool RunDISABLED_Tests)
        {
            if (!File.Exists(BinaryPath))
                throw new FileNotFoundException("Could not find the specified file '" + BinaryPath + "'.");

            this.model = new AllTestCases(RunDISABLED_Tests);
            this.model.CheckSum = (ulong)System.Environment.TickCount;		// dummy. Not using checksum with this type of controller.
            binaryPath = BinaryPath;

            this.WorkerThread = null;
            asyncOp = AsyncOperationManager.CreateOperation(null);

            ITestListOutputHandler testListOutputHandler = OutputHandlerFactory.CreateTestListHandler();
            testListOutputHandler.eventTestGroupIdentified += new TestGroupIdentifiedEventHandler(OnTestGroupIdentified);
            testListOutputHandler.eventTestIdentified += new TestIdentifiedEventHandler(OnTestIdentified);

            this.outputHandler = testListOutputHandler;
            runGTestProcess(gtestListTestsArgs);
            model.resetDefaultDISABLED_Tests();
        }

        public void Dispose()
        {
            StopTests();
        }

        private void OnTestIdentified(string TestGroupName, string TestName)
        {
            model[TestGroupName].AppendNewTest(TestName,model.highestTestID+1);
        }

        private void OnTestGroupIdentified(string TestGroupName)
        {
            model.AppendNewTestGroup(TestGroupName);
        }

        public void EnableTestsByGTestFilter(String FilterRegEx)
        {
            if (FilterRegEx == null)
                throw new ArgumentNullException("FilterRegEx cannot be null.");

            model.DisableAll();
            ITestListOutputHandler testListOutputHandler = OutputHandlerFactory.CreateTestListHandler();
            testListOutputHandler.eventTestIdentified += new TestIdentifiedEventHandler(OnTestIdentifiedByGTestFilter);

            String arguments = gtestListTestsArgs;
            arguments += " --gtest_filter=" + FilterRegEx;

            this.outputHandler = testListOutputHandler;
            runGTestProcess(arguments);
        }

        private void OnTestIdentifiedByGTestFilter(string TestGroupName, string TestName)
        {
            if (!(TestName.StartsWith("DISABLED_") || TestGroupName.StartsWith("DISABLED_")) || this.model.EnableDefaultDISABLED_Tests)
                this.model[TestGroupName][TestName].Enabled = true;
        }
    }
}
