using GTestCommon.Models;
using GTestUI.Controllers.OutputHandlers.Interfaces;



namespace GTestUI.Controllers.Interface
{

public delegate void TestProcessFinishedEventHandler();
public delegate void TestSetChanged();
public delegate void TestControllerConnected(bool connected);

public interface ITestRunController : System.IDisposable
{
    event TestSetChanged eventTestSetChanged;
    event TestProcessFinishedEventHandler eventTestProcessFinished;
    event TestOutputLineCapturedEventHandler eventTestOutputLineCaptured;
    event TestControllerConnected eventTestControllerConnected;

    AllTestCases Model{get;}
    uint Repeat{get;set;}
    bool Connected{get;}

    void StartTests(bool Shuffle, int Seed);

    void ResetTests();

    void StopTests();

    void EnableTestsByGTestFilter(string FilterRegEx);



}

}
