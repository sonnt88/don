namespace GTestUI
{
    partial class FormTestRegEx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBoxTestGroup = new System.Windows.Forms.RichTextBox();
            this.richTextBoxTest = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.radioButtonEnable = new System.Windows.Forms.RadioButton();
            this.radioButtonDisable = new System.Windows.Forms.RadioButton();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonAbort = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "test group:";
            // 
            // richTextBoxTestGroup
            // 
            this.richTextBoxTestGroup.Location = new System.Drawing.Point(16, 24);
            this.richTextBoxTestGroup.Multiline = false;
            this.richTextBoxTestGroup.Name = "richTextBoxTestGroup";
            this.richTextBoxTestGroup.Size = new System.Drawing.Size(250, 20);
            this.richTextBoxTestGroup.TabIndex = 2;
            this.richTextBoxTestGroup.Text = ".*";
            this.richTextBoxTestGroup.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnTestCaseRegExTextBoxKeyUp);
            this.richTextBoxTestGroup.TextChanged += new System.EventHandler(this.TestRegExChanged);
            // 
            // richTextBoxTest
            // 
            this.richTextBoxTest.Location = new System.Drawing.Point(280, 24);
            this.richTextBoxTest.Multiline = false;
            this.richTextBoxTest.Name = "richTextBoxTest";
            this.richTextBoxTest.Size = new System.Drawing.Size(250, 20);
            this.richTextBoxTest.TabIndex = 3;
            this.richTextBoxTest.Text = ".*";
            this.richTextBoxTest.TextChanged += new System.EventHandler(this.TestRegExChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(277, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "test:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(268, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = ".";
            // 
            // radioButtonEnable
            // 
            this.radioButtonEnable.AutoSize = true;
            this.radioButtonEnable.Checked = true;
            this.radioButtonEnable.Location = new System.Drawing.Point(280, 63);
            this.radioButtonEnable.Name = "radioButtonEnable";
            this.radioButtonEnable.Size = new System.Drawing.Size(82, 17);
            this.radioButtonEnable.TabIndex = 6;
            this.radioButtonEnable.TabStop = true;
            this.radioButtonEnable.Text = "enable tests";
            this.radioButtonEnable.UseVisualStyleBackColor = true;
            // 
            // radioButtonDisable
            // 
            this.radioButtonDisable.AutoSize = true;
            this.radioButtonDisable.Location = new System.Drawing.Point(280, 91);
            this.radioButtonDisable.Name = "radioButtonDisable";
            this.radioButtonDisable.Size = new System.Drawing.Size(83, 17);
            this.radioButtonDisable.TabIndex = 7;
            this.radioButtonDisable.Text = "disable tests";
            this.radioButtonDisable.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(455, 60);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 8;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonAbort
            // 
            this.buttonAbort.Location = new System.Drawing.Point(455, 88);
            this.buttonAbort.Name = "buttonAbort";
            this.buttonAbort.Size = new System.Drawing.Size(75, 23);
            this.buttonAbort.TabIndex = 9;
            this.buttonAbort.Text = "Abort";
            this.buttonAbort.UseVisualStyleBackColor = true;
            this.buttonAbort.Click += new System.EventHandler(this.buttonAbort_Click);
            // 
            // FormTestRegEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 120);
            this.Controls.Add(this.buttonAbort);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.radioButtonDisable);
            this.Controls.Add(this.radioButtonEnable);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.richTextBoxTest);
            this.Controls.Add(this.richTextBoxTestGroup);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTestRegEx";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "specify tests to enable/disable";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTestRegEx_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBoxTestGroup;
        private System.Windows.Forms.RichTextBox richTextBoxTest;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButtonEnable;
        private System.Windows.Forms.RadioButton radioButtonDisable;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonAbort;
    }
}