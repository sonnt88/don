namespace GTestUI
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openGTestBinaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.connectAdapterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.testsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.enableAllTestsByDefaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.shuffleTestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripButtonPlay = new System.Windows.Forms.ToolStripButton();
			this.toolStripButtonStop = new System.Windows.Forms.ToolStripButton();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
			this.toolStripStatusLabelResults = new System.Windows.Forms.ToolStripStatusLabel();
			this.contextMenuStripTreeView = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.toolStripMenuItemCollapseAll = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItemExpandAll = new System.Windows.Forms.ToolStripMenuItem();
			this.enableAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.disableAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.enabledisableViaRegExToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.treeViewTestCases = new System.Windows.Forms.TreeView();
			this.groupBoxOutput = new System.Windows.Forms.GroupBox();
			this.buttonSearchOutput = new System.Windows.Forms.Button();
			this.richTextBoxTestOutput = new System.Windows.Forms.RichTextBox();
			this.richTextBoxSearch = new System.Windows.Forms.RichTextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.buttonSearchNext = new System.Windows.Forms.Button();
			this.cbMatchCase = new System.Windows.Forms.CheckBox();
			this.buttonSearchPrev = new System.Windows.Forms.Button();
			this.cbUseRegEx = new System.Windows.Forms.CheckBox();
			this.numericUpDownReapeat = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.labelRandomSeed = new System.Windows.Forms.Label();
			this.textBoxRandomSeed = new System.Windows.Forms.TextBox();
			this.menuStrip1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.contextMenuStripTreeView.SuspendLayout();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.groupBoxOutput.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownReapeat)).BeginInit();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.testsToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(740, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openGTestBinaryToolStripMenuItem,
            this.connectAdapterToolStripMenuItem,
            this.quitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// openGTestBinaryToolStripMenuItem
			// 
			this.openGTestBinaryToolStripMenuItem.Name = "openGTestBinaryToolStripMenuItem";
			this.openGTestBinaryToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.openGTestBinaryToolStripMenuItem.Text = "Open GTest Binary";
			this.openGTestBinaryToolStripMenuItem.Click += new System.EventHandler(this.openGTestBinaryToolStripMenuItem_Click);
			// 
			// connectAdapterToolStripMenuItem
			// 
			this.connectAdapterToolStripMenuItem.Name = "connectAdapterToolStripMenuItem";
			this.connectAdapterToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.connectAdapterToolStripMenuItem.Text = "Connect Adapter";
			this.connectAdapterToolStripMenuItem.Click += new System.EventHandler(this.connectAdapterToolStripMenuItem_Click);
			// 
			// quitToolStripMenuItem
			// 
			this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
			this.quitToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.quitToolStripMenuItem.Text = "Quit";
			this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
			// 
			// testsToolStripMenuItem
			// 
			this.testsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enableAllTestsByDefaultToolStripMenuItem,
            this.shuffleTestsToolStripMenuItem});
			this.testsToolStripMenuItem.Name = "testsToolStripMenuItem";
			this.testsToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
			this.testsToolStripMenuItem.Text = "Tests";
			// 
			// enableAllTestsByDefaultToolStripMenuItem
			// 
			this.enableAllTestsByDefaultToolStripMenuItem.CheckOnClick = true;
			this.enableAllTestsByDefaultToolStripMenuItem.Name = "enableAllTestsByDefaultToolStripMenuItem";
			this.enableAllTestsByDefaultToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
			this.enableAllTestsByDefaultToolStripMenuItem.Text = "enable all tests by default";
			this.enableAllTestsByDefaultToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.enableAllTestsByDefaultToolStripMenuItem_CheckStateChanged);
			// 
			// shuffleTestsToolStripMenuItem
			// 
			this.shuffleTestsToolStripMenuItem.CheckOnClick = true;
			this.shuffleTestsToolStripMenuItem.Name = "shuffleTestsToolStripMenuItem";
			this.shuffleTestsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
			this.shuffleTestsToolStripMenuItem.Text = "shuffle tests";
			this.shuffleTestsToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.shuffleTestsToolStripMenuItem_CheckStateChanged);
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonPlay,
            this.toolStripButtonStop});
			this.toolStrip1.Location = new System.Drawing.Point(0, 24);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(740, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// toolStripButtonPlay
			// 
			this.toolStripButtonPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButtonPlay.Image = global::GTestUI.Properties.Resources.play;
			this.toolStripButtonPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonPlay.Name = "toolStripButtonPlay";
			this.toolStripButtonPlay.Size = new System.Drawing.Size(23, 22);
			this.toolStripButtonPlay.Text = "start tests";
			this.toolStripButtonPlay.Click += new System.EventHandler(this.toolStripButtonPlay_Click);
			// 
			// toolStripButtonStop
			// 
			this.toolStripButtonStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButtonStop.Image = global::GTestUI.Properties.Resources.stop;
			this.toolStripButtonStop.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButtonStop.Name = "toolStripButtonStop";
			this.toolStripButtonStop.Size = new System.Drawing.Size(23, 22);
			this.toolStripButtonStop.Text = "force abort tests";
			this.toolStripButtonStop.Click += new System.EventHandler(this.toolStripButtonStop_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1,
            this.toolStripStatusLabelResults});
			this.statusStrip1.Location = new System.Drawing.Point(0, 498);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(740, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripProgressBar1
			// 
			this.toolStripProgressBar1.Name = "toolStripProgressBar1";
			this.toolStripProgressBar1.Size = new System.Drawing.Size(200, 16);
			// 
			// toolStripStatusLabelResults
			// 
			this.toolStripStatusLabelResults.Name = "toolStripStatusLabelResults";
			this.toolStripStatusLabelResults.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.toolStripStatusLabelResults.Size = new System.Drawing.Size(0, 17);
			// 
			// contextMenuStripTreeView
			// 
			this.contextMenuStripTreeView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemCollapseAll,
            this.toolStripMenuItemExpandAll,
            this.enableAllToolStripMenuItem,
            this.disableAllToolStripMenuItem,
            this.enabledisableViaRegExToolStripMenuItem});
			this.contextMenuStripTreeView.Name = "contextMenuStripTreeView";
			this.contextMenuStripTreeView.Size = new System.Drawing.Size(195, 114);
			// 
			// toolStripMenuItemCollapseAll
			// 
			this.toolStripMenuItemCollapseAll.Name = "toolStripMenuItemCollapseAll";
			this.toolStripMenuItemCollapseAll.Size = new System.Drawing.Size(194, 22);
			this.toolStripMenuItemCollapseAll.Text = "collapse all";
			this.toolStripMenuItemCollapseAll.Click += new System.EventHandler(this.toolStripMenuItemCollapseAll_Click);
			// 
			// toolStripMenuItemExpandAll
			// 
			this.toolStripMenuItemExpandAll.Name = "toolStripMenuItemExpandAll";
			this.toolStripMenuItemExpandAll.Size = new System.Drawing.Size(194, 22);
			this.toolStripMenuItemExpandAll.Text = "expand all";
			this.toolStripMenuItemExpandAll.Click += new System.EventHandler(this.toolStripMenuItemExpandAll_Click);
			// 
			// enableAllToolStripMenuItem
			// 
			this.enableAllToolStripMenuItem.Name = "enableAllToolStripMenuItem";
			this.enableAllToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
			this.enableAllToolStripMenuItem.Text = "enable all";
			this.enableAllToolStripMenuItem.Click += new System.EventHandler(this.enableAllToolStripMenuItem_Click);
			// 
			// disableAllToolStripMenuItem
			// 
			this.disableAllToolStripMenuItem.Name = "disableAllToolStripMenuItem";
			this.disableAllToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
			this.disableAllToolStripMenuItem.Text = "disable all";
			this.disableAllToolStripMenuItem.Click += new System.EventHandler(this.disableAllToolStripMenuItem_Click);
			// 
			// enabledisableViaRegExToolStripMenuItem
			// 
			this.enabledisableViaRegExToolStripMenuItem.Name = "enabledisableViaRegExToolStripMenuItem";
			this.enabledisableViaRegExToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
			this.enabledisableViaRegExToolStripMenuItem.Text = "enable/disable via RegEx";
			this.enabledisableViaRegExToolStripMenuItem.Click += new System.EventHandler(this.enabledisableViaRegExToolStripMenuItem_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 49);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.groupBoxOutput);
			this.splitContainer1.Size = new System.Drawing.Size(740, 449);
			this.splitContainer1.SplitterDistance = 290;
			this.splitContainer1.TabIndex = 3;
			this.splitContainer1.TabStop = false;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.treeViewTestCases);
			this.splitContainer2.Size = new System.Drawing.Size(740, 290);
			this.splitContainer2.SplitterDistance = 245;
			this.splitContainer2.TabIndex = 0;
			this.splitContainer2.TabStop = false;
			// 
			// treeViewTestCases
			// 
			this.treeViewTestCases.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.treeViewTestCases.CheckBoxes = true;
			this.treeViewTestCases.ContextMenuStrip = this.contextMenuStripTreeView;
			this.treeViewTestCases.Location = new System.Drawing.Point(3, 3);
			this.treeViewTestCases.Name = "treeViewTestCases";
			this.treeViewTestCases.Size = new System.Drawing.Size(239, 284);
			this.treeViewTestCases.TabIndex = 2;
			this.treeViewTestCases.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeViewTestCases_AfterCheck);
			// 
			// groupBoxOutput
			// 
			this.groupBoxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBoxOutput.Controls.Add(this.buttonSearchOutput);
			this.groupBoxOutput.Controls.Add(this.richTextBoxTestOutput);
			this.groupBoxOutput.Controls.Add(this.richTextBoxSearch);
			this.groupBoxOutput.Controls.Add(this.label3);
			this.groupBoxOutput.Controls.Add(this.buttonSearchNext);
			this.groupBoxOutput.Controls.Add(this.cbMatchCase);
			this.groupBoxOutput.Controls.Add(this.buttonSearchPrev);
			this.groupBoxOutput.Controls.Add(this.cbUseRegEx);
			this.groupBoxOutput.Location = new System.Drawing.Point(3, 3);
			this.groupBoxOutput.Name = "groupBoxOutput";
			this.groupBoxOutput.Size = new System.Drawing.Size(734, 149);
			this.groupBoxOutput.TabIndex = 9;
			this.groupBoxOutput.TabStop = false;
			this.groupBoxOutput.Text = "test output";
			// 
			// buttonSearchOutput
			// 
			this.buttonSearchOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSearchOutput.Location = new System.Drawing.Point(512, 16);
			this.buttonSearchOutput.Name = "buttonSearchOutput";
			this.buttonSearchOutput.Size = new System.Drawing.Size(67, 23);
			this.buttonSearchOutput.TabIndex = 6;
			this.buttonSearchOutput.Text = "search ...";
			this.buttonSearchOutput.UseVisualStyleBackColor = true;
			this.buttonSearchOutput.Click += new System.EventHandler(this.buttonSearchOutput_Click);
			// 
			// richTextBoxTestOutput
			// 
			this.richTextBoxTestOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.richTextBoxTestOutput.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.richTextBoxTestOutput.DetectUrls = false;
			this.richTextBoxTestOutput.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.richTextBoxTestOutput.Location = new System.Drawing.Point(6, 45);
			this.richTextBoxTestOutput.Name = "richTextBoxTestOutput";
			this.richTextBoxTestOutput.ReadOnly = true;
			this.richTextBoxTestOutput.Size = new System.Drawing.Size(722, 98);
			this.richTextBoxTestOutput.TabIndex = 0;
			this.richTextBoxTestOutput.Text = "";
			this.richTextBoxTestOutput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.richTextBoxTestOutput_KeyPress);
			// 
			// richTextBoxSearch
			// 
			this.richTextBoxSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.richTextBoxSearch.Location = new System.Drawing.Point(54, 18);
			this.richTextBoxSearch.Multiline = false;
			this.richTextBoxSearch.Name = "richTextBoxSearch";
			this.richTextBoxSearch.Size = new System.Drawing.Size(212, 20);
			this.richTextBoxSearch.TabIndex = 3;
			this.richTextBoxSearch.Text = "";
			this.richTextBoxSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.richTextBoxSearch_KeyUp);
			this.richTextBoxSearch.TextChanged += new System.EventHandler(this.verifyRegEx);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 21);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(42, 13);
			this.label3.TabIndex = 1;
			this.label3.Text = "search:";
			// 
			// buttonSearchNext
			// 
			this.buttonSearchNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSearchNext.Location = new System.Drawing.Point(659, 16);
			this.buttonSearchNext.Name = "buttonSearchNext";
			this.buttonSearchNext.Size = new System.Drawing.Size(68, 23);
			this.buttonSearchNext.TabIndex = 8;
			this.buttonSearchNext.Text = "next >";
			this.buttonSearchNext.UseVisualStyleBackColor = true;
			this.buttonSearchNext.Click += new System.EventHandler(this.buttonSearchNext_Click);
			// 
			// cbMatchCase
			// 
			this.cbMatchCase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbMatchCase.AutoSize = true;
			this.cbMatchCase.Location = new System.Drawing.Point(287, 20);
			this.cbMatchCase.Name = "cbMatchCase";
			this.cbMatchCase.Size = new System.Drawing.Size(81, 17);
			this.cbMatchCase.TabIndex = 4;
			this.cbMatchCase.Text = "match case";
			this.cbMatchCase.UseVisualStyleBackColor = true;
			this.cbMatchCase.CheckedChanged += new System.EventHandler(this.verifyRegEx);
			// 
			// buttonSearchPrev
			// 
			this.buttonSearchPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSearchPrev.Location = new System.Drawing.Point(585, 16);
			this.buttonSearchPrev.Name = "buttonSearchPrev";
			this.buttonSearchPrev.Size = new System.Drawing.Size(68, 23);
			this.buttonSearchPrev.TabIndex = 7;
			this.buttonSearchPrev.Text = "< previous";
			this.buttonSearchPrev.UseVisualStyleBackColor = true;
			this.buttonSearchPrev.Click += new System.EventHandler(this.buttonSearchPrev_Click);
			// 
			// cbUseRegEx
			// 
			this.cbUseRegEx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbUseRegEx.AutoSize = true;
			this.cbUseRegEx.Location = new System.Drawing.Point(374, 20);
			this.cbUseRegEx.Name = "cbUseRegEx";
			this.cbUseRegEx.Size = new System.Drawing.Size(136, 17);
			this.cbUseRegEx.TabIndex = 5;
			this.cbUseRegEx.Text = "use regular expressions";
			this.cbUseRegEx.UseVisualStyleBackColor = true;
			this.cbUseRegEx.CheckedChanged += new System.EventHandler(this.verifyRegEx);
			// 
			// numericUpDownReapeat
			// 
			this.numericUpDownReapeat.Location = new System.Drawing.Point(116, 27);
			this.numericUpDownReapeat.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.numericUpDownReapeat.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownReapeat.Name = "numericUpDownReapeat";
			this.numericUpDownReapeat.Size = new System.Drawing.Size(42, 20);
			this.numericUpDownReapeat.TabIndex = 0;
			this.numericUpDownReapeat.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownReapeat.ValueChanged += new System.EventHandler(this.numericUpDownReapeat_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.SystemColors.Control;
			this.label1.Location = new System.Drawing.Point(69, 30);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(45, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Repeat:";
			// 
			// labelRandomSeed
			// 
			this.labelRandomSeed.AutoSize = true;
			this.labelRandomSeed.Location = new System.Drawing.Point(175, 30);
			this.labelRandomSeed.Name = "labelRandomSeed";
			this.labelRandomSeed.Size = new System.Drawing.Size(78, 13);
			this.labelRandomSeed.TabIndex = 6;
			this.labelRandomSeed.Text = "Random Seed:";
			this.labelRandomSeed.Visible = false;
			// 
			// textBoxRandomSeed
			// 
			this.textBoxRandomSeed.Location = new System.Drawing.Point(255, 27);
			this.textBoxRandomSeed.Name = "textBoxRandomSeed";
			this.textBoxRandomSeed.Size = new System.Drawing.Size(100, 20);
			this.textBoxRandomSeed.TabIndex = 1;
			this.textBoxRandomSeed.Text = "0";
			this.textBoxRandomSeed.Visible = false;
			this.textBoxRandomSeed.TextChanged += new System.EventHandler(this.textBoxRandomSeed_TextChanged);
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(740, 520);
			this.Controls.Add(this.textBoxRandomSeed);
			this.Controls.Add(this.labelRandomSeed);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.numericUpDownReapeat);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.MinimumSize = new System.Drawing.Size(692, 515);
			this.Name = "FormMain";
			this.Text = "GTestUI";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.onClose);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.contextMenuStripTreeView.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.ResumeLayout(false);
			this.groupBoxOutput.ResumeLayout(false);
			this.groupBoxOutput.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownReapeat)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openGTestBinaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonPlay;
        private System.Windows.Forms.ToolStripButton toolStripButtonStop;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelResults;
        private System.Windows.Forms.ToolStripMenuItem testsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableAllTestsByDefaultToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTreeView;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCollapseAll;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExpandAll;
        private System.Windows.Forms.ToolStripMenuItem enableAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableAllToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TreeView treeViewTestCases;
        private System.Windows.Forms.GroupBox groupBoxOutput;
        private System.Windows.Forms.RichTextBox richTextBoxTestOutput;
        private System.Windows.Forms.NumericUpDown numericUpDownReapeat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem shuffleTestsToolStripMenuItem;
        private System.Windows.Forms.Label labelRandomSeed;
        private System.Windows.Forms.TextBox textBoxRandomSeed;
        private System.Windows.Forms.CheckBox cbUseRegEx;
        private System.Windows.Forms.CheckBox cbMatchCase;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSearchNext;
        private System.Windows.Forms.Button buttonSearchPrev;
        private System.Windows.Forms.RichTextBox richTextBoxSearch;
        private System.Windows.Forms.Button buttonSearchOutput;
        private System.Windows.Forms.ToolStripMenuItem enabledisableViaRegExToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem connectAdapterToolStripMenuItem;
    }
}

