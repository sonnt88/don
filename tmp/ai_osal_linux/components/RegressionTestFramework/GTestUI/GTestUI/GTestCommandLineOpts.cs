using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace GTestUI
{    
    class GTestCommandLineOpts
    {
        private String executablePath;
        public String ExecutablePath { get { return executablePath; } }

        private bool shuffle;
        public bool Shuffle { get { return shuffle; } }

        private String filter;
        public String Filter { get { return filter; } }

        private int randomSeed;
        public int RandomSeed { get { return randomSeed; } }

        private int repeat;
        public int Repeat { get { return repeat; } }

        private bool runDisabledTests;
        public bool RunDisabledTests { get { return runDisabledTests; } }

        private Match performRegExMatch(Regex RegEx, String Argument, String ArgumentName)
        {
            Match match = RegEx.Match(Argument);

            if (!match.Success)
                throw new ArgumentException(ArgumentName + " argument seems to be incorrect.");

            return match;
        }

        public GTestCommandLineOpts(List<String> commandLineArguments)
        {
            this.executablePath = null;
            this.shuffle = false;
            this.filter = "*";
            this.randomSeed = -1;
            this.repeat = 1;
            this.runDisabledTests = false;

            foreach (String argument in commandLineArguments)
            {
                if (argument.CompareTo("--gtest_also_run_disabled_tests") == 0)
                {
                    this.runDisabledTests = true;
                    continue;
                }

                if (argument.CompareTo("--gtest_shuffle") == 0)
                {
                    this.shuffle = true;
                    continue;
                }

                if (argument.StartsWith("--gtest_filter"))
                {
                    Regex regex = new Regex(@"--gtest_filter=(?<filter>.*)");
                    Match match = performRegExMatch(regex, argument, "gtest_filter");

                    this.filter = match.Groups["filter"].ToString();
                }

                if (argument.StartsWith("--gtest_repeat"))
                {
                    Regex regex = new Regex(@"--gtest_repeat=(?<repeat>[+|-]?[0-9]*)");
                    Match match = performRegExMatch(regex, argument, "gtest_repeat");

                    this.repeat = System.Convert.ToInt32(match.Groups["repeat"].ToString());

                    if (repeat < 1)
                        throw new ArgumentException("infinite unit test loops are currently not supported by this GUI.");
                }

                if (argument.StartsWith("--gtest_random_seed"))
                {
                    Regex regex = new Regex(@"--gtest_random_seed=(?<random_seed>[+|-]?[0-9]*)");
                    Match match = performRegExMatch(regex, argument, "gtest_random_seed");

                    this.randomSeed = System.Convert.ToInt32(match.Groups["random_seed"].ToString());

                    if (this.randomSeed < 0 || this.randomSeed > 99999)
                        throw new ArgumentException("random seed has to be a  number between 0 and 99999");
                }

                if (argument.StartsWith("--gtest_executable"))
                {
                    Regex regex = new Regex(@"--gtest_executable=(?<executable>.*)");
                    Match match = performRegExMatch(regex, argument, "gtest_executable");

                    this.executablePath = match.Groups["executable"].ToString();

                    if (!File.Exists(this.executablePath))
                        throw new ArgumentException("the specified gtest executable does not exist.");
                }
            }
        }
    }
}
