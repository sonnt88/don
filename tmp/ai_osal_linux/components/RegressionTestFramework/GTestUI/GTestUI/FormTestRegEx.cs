using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace GTestUI
{
    public partial class FormTestRegEx : Form
    {
        private DialogResult result;
        public DialogResult Result { get { return result; } }

        private string testGroupRegEx;
        public string TestGroupRegEx { get { return testGroupRegEx; } }

        private string testRegEx;
        public string TestRegEx { get { return testRegEx; } }

        public bool EnableSelection { get { return radioButtonEnable.Checked; } }

        public FormTestRegEx()
        {
            InitializeComponent();
            SetEnabledStateForButtonOk();
            result = DialogResult.None;
            testGroupRegEx = null;
            testRegEx = null;
        }

        private bool RegExIsValid(String RegEx)
        {
            bool regExIsValid = true;
            try
            {
                Regex regEx = new Regex(RegEx);
            }
            catch (System.ArgumentException)
            {
                regExIsValid = false;
            }
            return regExIsValid;
        }

        private void SetEnabledStateForButtonOk()
        {
            if (richTextBoxTestGroup.Text.Length > 0 && richTextBoxTest.Text.Length > 0 &&
                RegExIsValid(richTextBoxTestGroup.Text) && RegExIsValid(richTextBoxTest.Text))
                buttonOk.Enabled = true;
            else
                buttonOk.Enabled = false;
        }

        private void TestRegExChanged(object sender, EventArgs e)
        {
            RichTextBox textBox = sender as RichTextBox;

            if (RegExIsValid(textBox.Text))
                textBox.ForeColor = Color.Black;
            else
                textBox.ForeColor = Color.Red;

            SetEnabledStateForButtonOk();
        }

        private void buttonAbort_Click(object sender, EventArgs e)
        {
            result = DialogResult.Abort;
            Close();
        }

        private void FormTestRegEx_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (result == DialogResult.None)
                result = DialogResult.Abort;
        }

        private void affirmRegExInput()
        {
            testGroupRegEx = richTextBoxTestGroup.Text;
            testRegEx = richTextBoxTest.Text;
            result = DialogResult.OK;
            Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            affirmRegExInput();
        }

        private void OnTestCaseRegExTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && buttonOk.Enabled)
                affirmRegExInput();
        }
    }
}