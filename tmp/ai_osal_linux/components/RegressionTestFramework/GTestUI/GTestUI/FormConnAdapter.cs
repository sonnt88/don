using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;



namespace GTestUI
{
	public partial class FormConnAdapter : Form
	{
		private const string ItemForNoCom = "<None>";

		public FormConnAdapter()
		{
			InitializeComponent();
			// set combo box of serial ports
		  string[] ports = System.IO.Ports.SerialPort.GetPortNames();
			this.comboBoxCreComPort.Items.Add(ItemForNoCom);
			foreach( string portName in ports )
			{
				this.comboBoxCreComPort.Items.Add(portName);
			}
			this.comboBoxCreComPort.SelectedIndex = 0;

			// set combo box of adapter services.
		  GTestCommon.Utils.ServiceDiscovery.ServiceDiscovery discover;
			discover = new GTestCommon.Utils.ServiceDiscovery.ServiceDiscovery(
					14243  ,
					new GTestUI.AdapterSearchPckFactory()
			);
		  List<GTestCommon.Utils.ServiceDiscovery.Models.ServiceModel> hits;
			hits = discover.DiscoverServiceProviders( 333 );
			foreach( GTestCommon.Utils.ServiceDiscovery.Models.ServiceModel mdl in hits )
			{
				this.comboBoxService.Items.Add( mdl.ServiceAddress.IPAddr.ToString() + ":" + mdl.ServiceAddress.Port.ToString() );
			}
		}

		private void buttonOK_Click(object sender, EventArgs e)
		{
		  TabPage tp = tabControl.SelectedTab;
			// OK clicked. check if entries are good and set focus to bad element if not.
			if( tabControl.SelectedIndex == 0 )
			{
				// is the connect page
				if(!looksLikeAddress(connect_address,true))
				{
					comboBoxService.SelectAll();
					comboBoxService.Focus();
					return;
				}
			}else{
				// is the create page
				// select for empty items (null).
				// check entries
				if( !looksLikeAddress(create_serviceAddress,true) )
				{
					textBoxCreSrvAdr.SelectAll();
					textBoxCreSrvAdr.Focus();
					return;
				}
			}
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void comboBoxCreComPort_SelectedIndexChanged(object sender, EventArgs e)
		{

		}



		// ===========================================================

		/// <summary>
		/// Checks if an address looks good. Name or IP, with or without port.
		/// </summary>
		/// <param name="adr">address string to check.</param>
		/// <param name="withPort">does this allow a port part (with colon)</param>
		/// <returns>boolean indicating if address looks good.</returns>
		private bool looksLikeAddress(string adr,bool withPort)
		{
		  string portPart = null;
		  string namePart = null;
		  string[] parts = adr.Split(':');
			if( parts.Length<1 || parts.Length>2 || (parts.Length==2&&!withPort) )
				return false;
			namePart = parts[0];
			if(parts.Length>=2)
				portPart = parts[1];
			if(!( looksLikeNameAddress(namePart) || looksLikeIp4Address(namePart) ))
				return false;
			if( portPart!=null && !looksLikePort(portPart) )
				return false;
			return true;
		}

		private bool looksLikePort(string p)
		{
			try{
			  ushort port = System.Convert.ToUInt16(p);
				return true;
			}catch(System.FormatException)
				{}
			catch(System.OverflowException)
				{}
			return false;
		}

		private bool looksLikeNameAddress(string adr)
		{
			// may not have any of  /  '  "     \  *  :
		  int tmp;
			tmp = adr.IndexOfAny(new char[]{'/','\'','\"','\\',' ','\t','\r','\n','*',':','?',',',';'});
			if(tmp>=0)return false;
			// split in parts.
		  string[] parts = adr.Split('.');
			// no empty parts.
			foreach( string p in parts )
			{
				if( p=="" )return false;		// no empty parts such as 'calc..nethost'
				try{
					System.Convert.ToUInt64(p);
					return false;		// no numbers are allowed in named addresses
				}catch(System.FormatException)
					{}
			}
			// looks good.
			return true;
		}

		private bool looksLikeIp4Address(string adr)
		{
		  string[] parts = adr.Split('.');
		  byte[] ip = new byte[4];
			// must be 4 parts.
			if(parts.Length!=4)return false;
			for( int b=0 ; b<4 ; b++ )
			{
			  ushort value;
				try{
					value = System.Convert.ToUInt16(parts[b]);
				}catch(System.FormatException)
					{return false;}		// not a number.
				catch(System.OverflowException)
					{return false;}		// too big.
				// must be <256
				if(value>=256)return false;	// too big.
				ip[b] = (byte)value;
			}
			// do not allow 0.0.0.0
			if( ip[0]==0 && ip[1]==0 && ip[2]==0 && ip[3]==0 )
				return false;
			//good
			return true;
		}

		// ===========================================================

		public bool connect_selected
		{
			get{return tabControl.SelectedIndex==0;}
		}
		public string connect_address
		{
			get{return comboBoxService.Text;}
			set{comboBoxService.Text=value;}
		}

		public bool create_selected
		{
			get{return tabControl.SelectedIndex==1;}
		}
		public int create_UIport
		{
			get
			{
				return System.Convert.ToInt32(numericUpDownGTestAdapterPort.Value);
			}
			set 
			{
				int intValue = System.Convert.ToInt32(value);

				if (intValue >= numericUpDownGTestAdapterPort.Minimum &&
					intValue <= numericUpDownGTestAdapterPort.Maximum)
					numericUpDownGTestAdapterPort.Value = System.Convert.ToDecimal(value);
				else
					throw new ArgumentOutOfRangeException("Value is not a TCP port");
			}
		}
		public string create_serviceAddress
		{
			get{return textBoxCreSrvAdr.Text;}
			set
			{
				textBoxCreSrvAdr.Text=value;
			}
		}
		public int create_TTFisChannel
		{
			get
			{
				if (checkBoxUseTTFis.Checked)
					return System.Convert.ToInt32(numericUpDownTTFisPort.Value);
				else
					return -1;
			}
			set{
				int intValue = System.Convert.ToInt32(value);

				if (intValue >= numericUpDownTTFisPort.Minimum &&
					intValue <= numericUpDownTTFisPort.Maximum)
					numericUpDownTTFisPort.Value = System.Convert.ToDecimal(value);
				else
					throw new ArgumentOutOfRangeException("Value is not a TCP port");
			}
		}
		public string create_COMport
		{
			get{string res = (string)comboBoxCreComPort.SelectedItem;if(res==ItemForNoCom)res=null;return res;}
			set{
			  int i=0;
				foreach( string item in comboBoxCreComPort.Items )
				{
					if( item.ToLower() == value.ToLower() )
						{comboBoxCreComPort.SelectedIndex = i;return;}
					i++;
				}
			}
		}
	}
}

