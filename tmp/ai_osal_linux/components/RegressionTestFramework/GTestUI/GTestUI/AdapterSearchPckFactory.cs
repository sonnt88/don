using GTestCommon.Utils.ServiceDiscovery;
using GTestCommon.Utils.ServiceDiscovery.Models;


namespace GTestUI
{
	class AdapterSearchPckFactory : GTestCommon.Utils.ServiceDiscovery.Interfaces.IServiceAnnouncementPacketFactory
	{
		public AdapterSearchPckFactory()
		{
		}


		class RequestPack : GTestCommon.Utils.ServiceDiscovery.ServiceDiscoveryPacket
		{
			public override byte[] AsBytes()
			{
			  byte[] res = new byte[data.Length];
				System.Array.Copy( data , res , data.Length );
				return res;
			}
			public override int Length() {return data.Length;}
			public static byte[] data = System.Text.Encoding.UTF8.GetBytes("GTestAdapter_service-call");
		}

		class ReplyPack : GTestCommon.Utils.ServiceDiscovery.ServiceDiscoveryAnswerPacket
		{
			public ReplyPack(ushort port,string name,System.Net.IPAddress ServiceIP)
			{
				m_port = port;
				m_name = name;
				m_IP = ServiceIP;
			}

			public override byte[] AsBytes()
			{
			  byte[] res = new byte[data.Length+2];
				System.Array.Copy( data , res , data.Length );
				res[data.Length+0] = (byte)(m_port);
				res[data.Length+1] = (byte)(m_port>>8);
				return res;
			}

			public override int Length() {return data.Length+2;}

			public override ServiceModel GetServiceModel()
			{
			  ServiceModel res;
			  ServiceAddr sAddr;
				sAddr = new ServiceAddr( m_IP , (int)(uint)m_port );
				res = new ServiceModel( sAddr , m_name );
				return res;
			}

			private ushort m_port;
			private string m_name;
			private System.Net.IPAddress m_IP;

			private static byte[] data = System.Text.Encoding.UTF8.GetBytes("GTestAdapter_service-reply");
		}




		public ServiceDiscoveryPacket CreateDiscoveryPacket()
		{
			return new RequestPack();
		}

		public ServiceDiscoveryAnswerPacket CreateAnswerPacket()
		{
			//throw new System.Exception("This end should need this?");
			// dummy. This is called to get length of a reply packet... Uh. would not be constant, would it?
			return new ReplyPack( 0 , "" , System.Net.IPAddress.Parse("8.8.8.8") );
		}

		public ServiceDiscoveryAnswerPacket DeserializeAnswerPacket(byte[] Bytes, System.Net.IPAddress ServiceIP)
		{
		  ReplyPack res;
		  int i;
		  byte[] comp;
			if( Bytes.Length<2 )return null;
		  ushort port = (ushort)( Bytes[Bytes.Length-2] + 256u*Bytes[Bytes.Length-1] );
			res = new ReplyPack( port , "dummy" , ServiceIP );
			// bytes must match exactly.
			comp = res.AsBytes();
			for(i=0;i<Bytes.Length;i++)
				if(Bytes[i]!=comp[i])
					return null;		// mismatch. Not ours.
			return res;
		}

	}
}
