using System.Diagnostics;


namespace GTestUI
{

public static class AdapterSpawner
{
	public static bool SpawnNewAdapter(string serviceAddress,ushort UIport,string db_Address,string db_User,string db_Pass,string COMport)
	{
	  ProcessStartInfo info;
		info = new System.Diagnostics.ProcessStartInfo();
		info.CreateNoWindow = false;
		info.UseShellExecute = true;
		info.FileName = "GTestAdapter.exe";
	  string cmdLine;
		cmdLine = "-s "+serviceAddress;
		cmdLine += " -g "+UIport.ToString();
		if( COMport!=null )
			cmdLine += " -c "+COMport;
		if( db_Address!=null )
			cmdLine += " -d "+db_Address;
		if( db_User!=null )
			cmdLine += " -u "+db_User;
		if( db_Pass!=null )
			cmdLine += " -p "+db_Pass;
		info.Arguments = cmdLine;
	  Process proc = Process.Start(info);
		System.Threading.Thread.Sleep(250);	// ..... use signal from process.
		return true;
	}
}

}
