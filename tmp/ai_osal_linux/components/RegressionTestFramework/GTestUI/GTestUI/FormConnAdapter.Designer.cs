namespace GTestUI
{
	partial class FormConnAdapter
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
		if (disposing && (components != null))
		{
		components.Dispose();
		}
		base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControl = new System.Windows.Forms.TabControl();
			this.tabConnect = new System.Windows.Forms.TabPage();
			this.comboBoxService = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tabCreate = new System.Windows.Forms.TabPage();
			this.label6 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.comboBoxCreComPort = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxCreSrvAdr = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.buttonOK = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.numericUpDownGTestAdapterPort = new System.Windows.Forms.NumericUpDown();
			this.numericUpDownTTFisPort = new System.Windows.Forms.NumericUpDown();
			this.checkBoxUseTTFis = new System.Windows.Forms.CheckBox();
			this.tabControl.SuspendLayout();
			this.tabConnect.SuspendLayout();
			this.tabCreate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownGTestAdapterPort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownTTFisPort)).BeginInit();
			this.SuspendLayout();
			// 
			// tabControl
			// 
			this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl.Controls.Add(this.tabConnect);
			this.tabControl.Controls.Add(this.tabCreate);
			this.tabControl.Location = new System.Drawing.Point(4, 12);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(423, 240);
			this.tabControl.TabIndex = 3;
			// 
			// tabConnect
			// 
			this.tabConnect.Controls.Add(this.comboBoxService);
			this.tabConnect.Controls.Add(this.label1);
			this.tabConnect.Location = new System.Drawing.Point(4, 22);
			this.tabConnect.Name = "tabConnect";
			this.tabConnect.Padding = new System.Windows.Forms.Padding(3);
			this.tabConnect.Size = new System.Drawing.Size(412, 200);
			this.tabConnect.TabIndex = 0;
			this.tabConnect.Text = "connect";
			this.tabConnect.UseVisualStyleBackColor = true;
			// 
			// comboBoxService
			// 
			this.comboBoxService.FormattingEnabled = true;
			this.comboBoxService.Location = new System.Drawing.Point(124, 17);
			this.comboBoxService.Name = "comboBoxService";
			this.comboBoxService.Size = new System.Drawing.Size(221, 21);
			this.comboBoxService.TabIndex = 11;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(99, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "service net address";
			// 
			// tabCreate
			// 
			this.tabCreate.Controls.Add(this.checkBoxUseTTFis);
			this.tabCreate.Controls.Add(this.numericUpDownTTFisPort);
			this.tabCreate.Controls.Add(this.numericUpDownGTestAdapterPort);
			this.tabCreate.Controls.Add(this.label6);
			this.tabCreate.Controls.Add(this.panel1);
			this.tabCreate.Controls.Add(this.comboBoxCreComPort);
			this.tabCreate.Controls.Add(this.label2);
			this.tabCreate.Controls.Add(this.label5);
			this.tabCreate.Controls.Add(this.textBoxCreSrvAdr);
			this.tabCreate.Controls.Add(this.label4);
			this.tabCreate.Controls.Add(this.label3);
			this.tabCreate.Location = new System.Drawing.Point(4, 22);
			this.tabCreate.Name = "tabCreate";
			this.tabCreate.Padding = new System.Windows.Forms.Padding(3);
			this.tabCreate.Size = new System.Drawing.Size(415, 214);
			this.tabCreate.TabIndex = 1;
			this.tabCreate.Text = "create";
			this.tabCreate.UseVisualStyleBackColor = true;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(19, 72);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(154, 13);
			this.label6.TabIndex = 9;
			this.label6.Text = "Settings for GTestAdapter";
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.ForeColor = System.Drawing.SystemColors.ControlDark;
			this.panel1.Location = new System.Drawing.Point(22, 58);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(387, 2);
			this.panel1.TabIndex = 8;
			// 
			// comboBoxCreComPort
			// 
			this.comboBoxCreComPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxCreComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxCreComPort.Location = new System.Drawing.Point(180, 178);
			this.comboBoxCreComPort.Name = "comboBoxCreComPort";
			this.comboBoxCreComPort.Size = new System.Drawing.Size(229, 21);
			this.comboBoxCreComPort.TabIndex = 23;
			this.comboBoxCreComPort.SelectedIndexChanged += new System.EventHandler(this.comboBoxCreComPort_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(19, 181);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "serial port";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(19, 152);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(55, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "TTFis port";
			// 
			// textBoxCreSrvAdr
			// 
			this.textBoxCreSrvAdr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxCreSrvAdr.Location = new System.Drawing.Point(180, 99);
			this.textBoxCreSrvAdr.Name = "textBoxCreSrvAdr";
			this.textBoxCreSrvAdr.Size = new System.Drawing.Size(229, 20);
			this.textBoxCreSrvAdr.TabIndex = 21;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(19, 99);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(155, 13);
			this.label4.TabIndex = 2;
			this.label4.Text = "GTestService target IP address";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(19, 27);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(118, 13);
			this.label3.TabIndex = 0;
			this.label3.Text = "GTestAdapter TCP port";
			// 
			// buttonOK
			// 
			this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonOK.Location = new System.Drawing.Point(12, 267);
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.Size = new System.Drawing.Size(75, 23);
			this.buttonOK.TabIndex = 1;
			this.buttonOK.Text = "OK";
			this.buttonOK.UseVisualStyleBackColor = true;
			this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(344, 267);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 2;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			// 
			// numericUpDownGTestAdapterPort
			// 
			this.numericUpDownGTestAdapterPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.numericUpDownGTestAdapterPort.Location = new System.Drawing.Point(180, 22);
			this.numericUpDownGTestAdapterPort.Maximum = new decimal(new int[] {
            65534,
            0,
            0,
            0});
			this.numericUpDownGTestAdapterPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownGTestAdapterPort.Name = "numericUpDownGTestAdapterPort";
			this.numericUpDownGTestAdapterPort.Size = new System.Drawing.Size(229, 20);
			this.numericUpDownGTestAdapterPort.TabIndex = 24;
			this.numericUpDownGTestAdapterPort.Value = new decimal(new int[] {
            666,
            0,
            0,
            0});
			// 
			// numericUpDownTTFisPort
			// 
			this.numericUpDownTTFisPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.numericUpDownTTFisPort.Location = new System.Drawing.Point(180, 152);
			this.numericUpDownTTFisPort.Maximum = new decimal(new int[] {
            65534,
            0,
            0,
            0});
			this.numericUpDownTTFisPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDownTTFisPort.Name = "numericUpDownTTFisPort";
			this.numericUpDownTTFisPort.Size = new System.Drawing.Size(229, 20);
			this.numericUpDownTTFisPort.TabIndex = 25;
			this.numericUpDownTTFisPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// checkBoxUseTTFis
			// 
			this.checkBoxUseTTFis.AutoSize = true;
			this.checkBoxUseTTFis.Location = new System.Drawing.Point(22, 124);
			this.checkBoxUseTTFis.Name = "checkBoxUseTTFis";
			this.checkBoxUseTTFis.Size = new System.Drawing.Size(73, 17);
			this.checkBoxUseTTFis.TabIndex = 26;
			this.checkBoxUseTTFis.Text = "use TTFis";
			this.checkBoxUseTTFis.UseVisualStyleBackColor = true;
			// 
			// FormConnAdapter
			// 
			this.AcceptButton = this.buttonOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(431, 302);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonOK);
			this.Controls.Add(this.tabControl);
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.Name = "FormConnAdapter";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.Text = "FormConnAdapter";
			this.tabControl.ResumeLayout(false);
			this.tabConnect.ResumeLayout(false);
			this.tabConnect.PerformLayout();
			this.tabCreate.ResumeLayout(false);
			this.tabCreate.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownGTestAdapterPort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownTTFisPort)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabConnect;
		private System.Windows.Forms.TabPage tabCreate;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBoxCreSrvAdr;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonOK;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.ComboBox comboBoxCreComPort;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox comboBoxService;
		private System.Windows.Forms.NumericUpDown numericUpDownGTestAdapterPort;
		private System.Windows.Forms.NumericUpDown numericUpDownTTFisPort;
		private System.Windows.Forms.CheckBox checkBoxUseTTFis;
	}
}