// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      SCC_Test_Framework.cpp
// Author:    Alex Garratt
// Date:      11 November 2013
//
// Implements the class SCCComms.  See SCC_Test_Library.h
//
// Build:     build.pl scc_unit_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./scc_unit_tests_out.out
//            or from within the google test framework.  See
//            http://hi0vm019.de.bosch.com/wiki/index.php?title=V850_Unit_Test_Cases
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <inc.h>
#include <poll.h>

#include "dgram_service.h"

#include <string.h>
#include <errno.h>

#include "SCC_Test_Library.h"

#define POLL_TIMEOUT   1000

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::SCCInit()
//
// Initializes INC based communications, taking the client role.  The procedure comes
// from the Bluepedia web page: -
// hi0vm019.de.bosch.com/wiki/index.php?title=INC
//
// Basic procedure used by this method is this:
// (1) create a socket
// (2) initialize datagram service
// (3) find the address of the local port using hostname "scc-local"
// (4) find the address of the remote port using hostname "scc"
// (5) bind the socket to local address and port
// (6) connect to the remote address port
//
// This method uses strerror() to provide a description if initialization fails at
// any stage.  The sort of things that could go wrong are if calls to socket(),
// dgram_init(), gethostbyname(), bind() or connect() fail.
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
void
SCCComms::SCCInit( const int LogicalUnitNumber ) {
  struct hostent       *local, *remote;
  struct sockaddr_in   local_addr, remote_addr;

  /* Check the validity of the function's arguments: TO DO (Just initialize rc for now) */
  this->ReturnCode = SUCCESS;

  /* Create blocking socket */
  if( SUCCESS == this->ReturnCode ) {
    /* use AF_BOSCH_INC_AUTOSAR to facilitate eventual transition to TCP/IP: */
    socket_id = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);

    if( -1 == socket_id ) {
      this->Explanation = "socket()";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      socket_id = 0;
    }
  }

  /* Initialize datagram service */
  if( SUCCESS == this->ReturnCode ) {
    dgram = dgram_init( socket_id, DGRAM_MAX, NULL );

    if( NULL == dgram ) {
      this->Explanation = "dgram_init()";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      close( socket_id );
      socket_id = 0;
    }
  }

  /* Get local addresses and ports */
  if( SUCCESS == this->ReturnCode ) {
    local = gethostbyname( "scc-local" );
    if( NULL == local ) {
      this->Explanation = "gethostbyname()";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      SCCQuickShutdown();
    }
    else {
      local_addr.sin_family = AF_INET;
      memcpy( (char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length );
      local_addr.sin_port = htons( LogicalUnitNumber );
    }
  }

  /* Get remote addresses and ports */
  if( SUCCESS == this->ReturnCode ) {
    remote = gethostbyname( "scc" );
    if( NULL == remote ) {
      this->Explanation = "gethostbyname()";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      SCCQuickShutdown();
    }
    else {
      remote_addr.sin_family = AF_INET;
      memcpy( (char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length );
      remote_addr.sin_port = htons( LogicalUnitNumber );
    }
  }

  /* Bind socket to local address and port */
  if( SUCCESS == this->ReturnCode ) {
    int bd;
    bd = bind( socket_id, (struct sockaddr *) &local_addr, sizeof( local_addr ) );

    if( -1 == bd ) {
      this->Explanation = "bind()";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      SCCQuickShutdown();
    }
  }

  /* Connect to the remote address and port */
  if( SUCCESS == this->ReturnCode ) {
    int cn;
    cn = connect( socket_id, (struct sockaddr *) &remote_addr, sizeof( remote_addr ) );

    if( -1 == cn ) {
      this->Explanation = "connect()";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      SCCQuickShutdown();
    }
  }

  /* After all that, we might have succeeded */
  if( SUCCESS == this->ReturnCode ) {
    this->Explanation = "SCCInit()";
  }
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::SCCSendMsg()
//
// First checks whether the object has already set up a socket, which is done by the
// SCCInit() method.  Without this it is not possible to send any data.
//
// The underlying functionality is provided by dgram_send().
//
// This function uses the strerror() function to describe the fault if the call to
// dgram_send() fails.
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
void
SCCComms::SCCSendMsg( void *buffer, const size_t msg_size ) throw( std::logic_error ) {

  if( 0 == socket_id ) {
    // Class SCCComms has been used incorrectly, so throw an error
    throw std::logic_error( "Attempt to transmit data before INC has been initialized" );
  }
  else {
    int dgram_code( 0 );

    dgram_code = dgram_send( dgram, buffer, msg_size );

    if( -1 == dgram_code ) {
      this->Explanation = "dgram_send()";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
    }
    else if( 0 == dgram_code ) {
      this->ReturnCode = FAILURE;
      this->Explanation = "dgram_send(): Zero bytes sent";
    }
    else {
      this->ReturnCode = SUCCESS;
      this->Explanation = "SCCSendMsg()";
    }
  }
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::SCCRecvMsg()
//
// First checks whether the object has already set up a socket, which is done by the
// SCCInit() method.  Without this it is not possible to receive any data.
//
// This function is not re-entrant.
//
// The underlying functionality is provided by dgram_recv().  If this is interrupted
// by a signal or for some other reason, then it will be called again.  The caller will
// therefore always receive some data if the return code is SUCCESS.
//
// This function uses the strerror() function to describe the fault if the call to
// dgram_recv() fails.
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
void
SCCComms::SCCRecvMsg( char buffer[],
		      const size_t max_size,
		      size_t &amount_recvd ) throw( std::logic_error ) {

   struct pollfd rFds[1];
   int    s32Rc = -1;
   
  if( 0 == socket_id ) {
    // Class SCCComms has been used incorrectly, so throw an error
    throw std::logic_error( "Attempt to receive data before INC has been initialized" );
  }
  else {
    int dgram_code( 0 );

   rFds[0].fd = socket_id;
   rFds[0].events = POLLIN;   
   
   s32Rc = poll(rFds, 1, POLL_TIMEOUT);
   if (s32Rc < 0)
   {
      this->Explanation = "Poll Failed";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      amount_recvd = 0;
   } 
   else if (rFds[0].revents & POLLIN)   
   {
      do {
         dgram_code = dgram_recv( dgram, buffer, max_size );
      } while( ( EAGAIN == errno ) && ( dgram_code <= 0 ) );

      if( dgram_code > 0 ) {
        amount_recvd = dgram_code;
        this->ReturnCode = SUCCESS;
        this->Explanation = "SCCRecvMsg()";
      }
      else {
         this->Explanation = "dgram_recv()";
         this->Explanation += strerror( errno );
         this->ReturnCode = FAILURE;
         amount_recvd = 0;
      }
   }
   else
   {
      this->Explanation = "Poll Failed";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      amount_recvd = 0;
   }   
  } // 0 != socket_id
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::SCCRecvMsg()
// NOTE: This is an overloaded function.
//
// First checks whether the object has already set up a socket, which is done by the
// SCCInit() method.  Without this it is not possible to receive any data.
//
// This function is not re-entrant.
//
// The underlying functionality is provided by dgram_recv().  If this is interrupted
// by a signal or for some other reason, then it will be called again.  The caller will
// therefore always receive some data if the return code is SUCCESS.
//
// This function uses the strerror() function to describe the fault if the call to
// dgram_recv() fails.
//
// The function takes a fourth argument that defines the timeout for poll()
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
void
SCCComms::SCCRecvMsg( char buffer[],
		      const size_t max_size,
		      size_t &amount_recvd , int timeout ) throw( std::logic_error ) {

   struct pollfd rFds[1];
   int    s32Rc = -1;
   
  if( 0 == socket_id ) {
    // Class SCCComms has been used incorrectly, so throw an error
    throw std::logic_error( "Attempt to receive data before INC has been initialized" );
  }
  else {
    int dgram_code( 0 );

   rFds[0].fd = socket_id;
   rFds[0].events = POLLIN;   
   // Here poll() is called with the timeout specified in the function argument
   s32Rc = poll(rFds, 1, timeout);
   if (s32Rc < 0)
   {
      this->Explanation = "Poll Failed";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      amount_recvd = 0;
   } 
   else if (rFds[0].revents & POLLIN)   
   {
      do {
         dgram_code = dgram_recv( dgram, buffer, max_size );
      } while( ( EAGAIN == errno ) && ( dgram_code <= 0 ) );

      if( dgram_code > 0 ) {
        amount_recvd = dgram_code;
        this->ReturnCode = SUCCESS;
        this->Explanation = "SCCRecvMsg()";
      }
      else {
         this->Explanation = "dgram_recv()";
         this->Explanation += strerror( errno );
         this->ReturnCode = FAILURE;
         amount_recvd = 0;
      }
   }
   else
   {
      this->Explanation = "Poll Failed";
      this->Explanation += strerror( errno );
      this->ReturnCode = FAILURE;
      amount_recvd = 0;
   }   
  } // 0 != socket_id
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::SCCShutdown()
//
// The underlying functionality is provided by dgram_exit() and close().
// 
// This function uses the strerror() function to describe the fault if the call to
// dgram_recv() fails.
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
void
SCCComms::SCCShutdown() {
  int dgram_code( 0 );

  if( dgram != NULL ) {
    dgram_code = dgram_exit( dgram );
    dgram = NULL;
  }

  if( -1 == dgram_code ) {
    this->Explanation = "dgram_exit()";
    this->Explanation += strerror( errno );
    this->ReturnCode = FAILURE;
  }
  else {
    this->ReturnCode = SUCCESS;
    this->Explanation = "SCCShutdown()";
  }

  if( socket_id ) {
    close(socket_id);
    socket_id = 0;
  }
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::SCCGetReturnCode()
//
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
const SCCComms::SCCReturnCode &
SCCComms::SCCGetReturnCode( void ) {
  return this->ReturnCode;
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::SCCGetExplanation()
//
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
const std::string &
SCCComms::SCCGetExplanation( void ) {
  return this->Explanation;
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::~SCCComms()
//
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
SCCComms::~SCCComms( void ) {
  SCCQuickShutdown( );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Method:    SCCComms::SCCQuickShutdown()
//
// The underlying functionality is provided by dgram_exit() and close().
// 
// No return code or explanation is provided by this function.
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
void
SCCComms::SCCQuickShutdown() {

  if( dgram != NULL ) {
    dgram_exit( dgram );
    dgram = NULL;
  }

  if( socket_id ) {
    close(socket_id);
    socket_id = 0;
  }
}
