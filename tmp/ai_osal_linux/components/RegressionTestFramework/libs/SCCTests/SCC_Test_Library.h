// -*- mode: c++; -*-
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      SCC_Test_Library.cpp
// Author:    Alex Garratt
// Date:      11 November 2013
//
// Implements the class SCCComms.  Underlying technology is provided by INC, which
// presents a socket based interface.  The methods in the SCCComms class are really
// only a wrapper, but the SCCInit() method makes the task of setting up a client
// connection much easier.
//
// The SCCComms constructor does not initialize an scc connection, which is
// intentional because initialization is something that the client should perform
// explicitly as part of the test.  Initialization is done in the SCCInit()
// method.  SCCSendMsg() and SCCRecvMsg() carry out data transfer, but only
// if the communications has already been set up by SCCInit().  SCCShutdown()
// closes the connection, after which data may not be transfered unless SCCInit()
// is called again.  The destructor also calls SCCShutdown().
//
// The methods in the SCCComms class do not return any status codes to the caller.
// Instead, the caller must use one or both of the SCCGetReturnCode() and
// SCCGetExplanation() methods to find out what happened.
//
// Build:     build.pl SCC_unit_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./scc_unit_tests_out.out
//            or from within the google test framework.  See
//            http://hi0vm019.de.bosch.com/wiki/index.php?title=V850_Unit_Test_Cases
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#ifndef _SCC_TESTS_
#define _SCC_TESTS_

#include <string>
#include <stdexcept>
#include <stdlib.h>
#include "dgram_service.h"


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Class:     SCCComms
// Purpose:   To manage the initialization, data transfer and shutdown of INC link
//            between iMX and V850 for testing purposes.
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
class SCCComms {

public:

  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  // Data type used by the SCCGetReturnCode() method.  Shows whether the use of the
  // previous method worked or not.  UNINITIALIZED means there has been no previous
  // method (freshly constructed object, not initialized).
  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  enum SCCReturnCode{
    SUCCESS,
    FAILURE,
    UNINITIALIZED
  };

  SCCComms( void ) : socket_id( 0 ), dgram( NULL ), ReturnCode( UNINITIALIZED ) { }
  virtual ~SCCComms( void );

  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  // Method:    SCCComms::SCCGetReturnCode()
  // Purpose:   To provide the caller with a code describing the result of the last
  //            function call.
  //
  // Returns:   SUCCESS - if the function completed its task successfully
  //            FAILURE - if the function encountered an error, in which case an
  //                      explanation will be available upon a call to
  //                      SCCGetExplanation()
  //            UNINITIALIZED - straight after an SCCComms object has been
  //                            instantiated and before any functions have
  //                            been called
  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  virtual const SCCReturnCode &
  SCCGetReturnCode( void );

  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  // Method:    SCCComms::SCCGetExplanation()
  // Purpose:   To provide the caller with an explanation in the event that the last
  //            function call failed.
  //
  // Returns:   (upon success) the name of the function last called
  //            (upon failure) a description of what went wrong
  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  virtual const std::string &
  SCCGetExplanation( void );

  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  // Method:    SCCComms::SCCInit()
  // Purpose:   To prepare a connection between iMX software and SCC software,
  //            taking the client role.  Without using this method it is not
  //            possible to send or receive data over the INC link.  Use this
  //            method once before using SCCSendMsg() or SCCRecvMsg().
  //
  // Arguments: LogicalUnitNumber - the port with which data should be exchanged.
  //                                Valid numbers are any of the macros defined in
  //                                inc_ports.h
  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  virtual void
  SCCInit( const int LogicalUnitNumber );

  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  // Method:    SCCComms::SCCSendMsg()
  // Purpose:   To send data pointed to by the first  argument over the INC link already
  //            set up by SCCInit().  If SCCInit() has not already been used, then this
  //            method will fail.  This method will block until transmission is complete.
  //
  // Arguments: buffer         - a pointer to the data, which this method will transmit
  //            msg_size       - the amount of data to be sent, in octets
  // Throws:    logic_error    - if this method is used on an object that has not been
  //                             initialized by SCCInit()  
  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  virtual void
  SCCSendMsg( void *buffer, const size_t msg_size ) throw( std::logic_error );

  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  // Method:    SCCComms::SCCRecvMsg()
  // Purpose:   To send data pointed to by the first  argument over the INC link already
  //            set up by SCCInit().  If SCCInit() has not already been used, then this
  //            method will fail.  This method will block until reception is complete.
  //
  // Arguments: buffer         - a pointer to the buffer, into which this method will
  //                             copy received data
  //            max_size       - the size of the buffer pointed to by the first
  //                             argument.  This method will not write more data
  //                             than this.
  //            amount_recvd   - if this method completes successfully, then the
  //                             amount of data received will be copied into this
  //                             variable.
  // Throws:    logic_error    - if this method is used on an object that has not been
  //                             initialized by SCCInit()  
  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  virtual void
  SCCRecvMsg( char buffer[],
	      const size_t max_size,
	      size_t &amount_recvd ) throw( std::logic_error );

  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  // Method:    SCCComms::SCCRecvMsg()
  // NOTE: This is an overloaded function.
  // Purpose:   To send data pointed to by the first  argument over the INC link already
  //            set up by SCCInit().  If SCCInit() has not already been used, then this
  //            method will fail.  This method will block until reception is complete.
  //
  // Arguments: buffer         - a pointer to the buffer, into which this method will
  //                             copy received data
  //            max_size       - the size of the buffer pointed to by the first
  //                             argument.  This method will not write more data
  //                             than this.
  //            amount_recvd   - if this method completes successfully, then the
  //                             amount of data received will be copied into this
  //                             variable.
  //            timeout        - timeout for the poll() function
  // Throws:    logic_error    - if this method is used on an object that has not been
  //                             initialized by SCCInit()  
  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  virtual void
  SCCRecvMsg( char buffer[],
	      const size_t max_size,
	      size_t &amount_recvd, int timeout ) throw( std::logic_error );

  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  // Method:    SCCComms::SCCShutdown()
  // Purpose:   To shut a connection between iMX and SCC down.  After using this method
  //            it will not be possible to use SCCSendMsg() or SCCRecvMsg(), unless
  //            SCCInit() is used again.
  // ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
  virtual void
  SCCShutdown( void );

private:
  SCCComms( const SCCComms & ) { }
  void SCCQuickShutdown();

  int socket_id;
  sk_dgram *dgram;

  enum SCCReturnCode ReturnCode;
  std::string Explanation;
};

#endif  // _SCC_TESTS_
