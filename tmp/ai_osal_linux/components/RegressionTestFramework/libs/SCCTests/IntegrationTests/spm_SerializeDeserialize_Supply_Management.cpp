/*
  ###################################################
  ####### GENERATED CODE - DO NOT TOUCH #############
  ###################################################
  */


/*
   ===================================================
   GeneratorVersion : Script_1.4
   Generated On     : 10/29/2013 2:08:29 PM
   Description      :
   ===================================================
  */
#include "Std_MsgHelper.h"
// #include "supplymgmt_MsgHandler.h"
#include "spm_SerializeDeserialize_Supply_Management.h"


/*
   =================================================================
   Funtions
   =================================================================
  */
Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS( uint8  *pu8Buffer,
                                                                         uint8_t u8msgID,
                                                                         uint8_t u8HostApplicationStatus,
                                                                         uint8_t u8HostApplicationVersion){
   Length_t lReturn = 0x00;
   Length_t lLength; /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8HostApplicationStatus);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8HostApplicationVersion );
   lReturn   += lLength;
   pu8Buffer += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS */

Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS( uint8   *pu8Buffer,
                                                                           uint8_t *pu8msgID,
                                                                           uint8_t *pu8HostApplicationStatus,
                                                                           uint8_t *pu8HostApplicationVersion){
   Length_t lReturn = 0x00;
   Length_t lLength;  /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8HostApplicationStatus);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8HostApplicationVersion);
   lReturn   += lLength;
   pu8Buffer += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS */

Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS( uint8  *pu8Buffer,
                                                                         uint8_t u8msgID,
                                                                         uint8_t u8SCCapplicationStatus,
                                                                         uint8_t u8SCCApplicationVersion){
   Length_t lReturn = 0x00;
   Length_t lLength; /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8SCCapplicationStatus);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8SCCApplicationVersion );
   lReturn   += lLength;
   pu8Buffer += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS */

Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS( uint8   *pu8Buffer,
                                                                           uint8_t *pu8msgID,
                                                                           uint8_t *pu8SCCapplicationStatus,
                                                                           uint8_t *pu8SCCApplicationVersion){
   Length_t lReturn = 0x00;
   Length_t lLength;  /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8SCCapplicationStatus);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8SCCApplicationVersion);
   lReturn   += lLength;
   pu8Buffer += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS */

Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT( uint8  *pu8Buffer,
                                                               uint8_t u8msgID,
                                                               uint8_t u8RejectReason,
                                                               uint8_t u8RejectedMsgID){
   Length_t lReturn = 0x00;
   Length_t lLength; /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8RejectReason);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8RejectedMsgID);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT */

Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT( uint8   *pu8Buffer,
                                                                 uint8_t *pu8msgID,
                                                                 uint8_t *pu8RejectReason,
                                                                 uint8_t *pu8RejectedMsgID){
   Length_t lReturn = 0x00;
   Length_t lLength;  /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8RejectReason);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8RejectedMsgID);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT */

Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_GET_STATE( uint8  *pu8Buffer,
                                                                  uint8_t u8msgID){
   Length_t lReturn = 0x00;
   Length_t lLength; /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lSerialize_SCC_SUPPLY_MANAGEMENT_C_GET_STATE */

Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_GET_STATE( uint8   *pu8Buffer,
                                                                    uint8_t *pu8msgID){
   Length_t lReturn = 0x00;
   Length_t lLength;  /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_GET_STATE */

Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_GET_STATE( uint8  *pu8Buffer,
                                                                  uint8_t u8msgID,
                                                                  uint8_t u8PSState){
   Length_t lReturn = 0x00;
   Length_t lLength; /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8PSState);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lSerialize_SCC_SUPPLY_MANAGEMENT_R_GET_STATE */

Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_GET_STATE( uint8   *pu8Buffer,
                                                                    uint8_t *pu8msgID,
                                                                    uint8_t *pu8PSState){
   Length_t lReturn = 0x00;
   Length_t lLength;  /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8PSState);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_GET_STATE */

Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_SET_STATE( uint8  *pu8Buffer,
                                                                  uint8_t u8msgID,
                                                                  uint8_t u8SupplyId,
                                                                  uint8_t u8SupplyState){
   Length_t lReturn = 0x00;
   Length_t lLength; /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8SupplyId);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8SupplyState);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lSerialize_SCC_SUPPLY_MANAGEMENT_C_SET_STATE */

Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_SET_STATE( uint8 *pu8Buffer,
                                                                    uint8 *pu8msgID,
                                                                    uint8 *pu8SupplyId,
                                                                    uint8 *pu8SupplyState){
   Length_t lReturn = 0x00;
   Length_t lLength;  /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8SupplyId);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8SupplyState);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_SET_STATE */

Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_SET_STATE( uint8  *pu8Buffer,
                                                                  uint8_t u8msgID,
                                                                  uint8_t u8SupplyId,
                                                                  uint8_t u8SupplyState,
                                                                  uint8_t u8SupplyDiag){
   Length_t lReturn = 0x00;
   Length_t lLength; /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8SupplyId);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8SupplyState);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lSerialize_uint8(pu8Buffer, u8SupplyDiag);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lSerialize_SCC_SUPPLY_MANAGEMENT_R_SET_STATE */

Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_SET_STATE( uint8   *pu8Buffer,
                                                                    uint8_t *pu8msgID,
                                                                    uint8_t *pu8SupplyId,
                                                                    uint8_t *pu8SupplyState,
                                                                    uint8_t *pu8SupplyDiag){
   Length_t lReturn = 0x00;
   Length_t lLength;  /* Not initiased, since first operation is assigment */

   if( NULL == pu8Buffer ){
      return( lReturn );
   }
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8msgID);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8SupplyId);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8SupplyState);
   pu8Buffer += lLength;
   lReturn   += lLength;
   lLength    = SUPPLYMGMT_lDeSerialize_uint8(pu8Buffer, pu8SupplyDiag);
   pu8Buffer += lLength;
   lReturn   += lLength;

   (void)pu8Buffer;
   (void)lLength;

   return ( lReturn );
} /* lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_SET_STATE */

