// TODO
// 1.  CSetTestTempAll:  antworten beim z.b. set temperature nicht korrekt. error muss kommen.
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_thermal_management_tests.cpp
// Author:    W�stefeld
// Date:      19 November 2013
//
// Contains Scc Thermal Management test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include <string>
#include "spm_SerializeDeserialize_Thermal_Management.h"

#define SLEEP_ACTIVE 1

#define PRINTF    //printf
/* <  >  PDU_SPM_ThermalManagement */
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS              0x20
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_GET_STATE                     0x30
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_GET_TEMP                      0x36
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_REQ_FAN                       0x34
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_STATE                     0x32
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEMP                      0x38
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP                 0x3A
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN                  0x3C
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS              0x21
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_STATE                     0x31
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_TEMP                      0x37
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REJECT                        0x0B
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REQ_FAN                       0x35
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE                     0x33
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEMP                      0x39
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP                 0x3B
#define THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN                  0x3D
#define THERMALMGMT_ThermalMgmt_CatalogueVersion                           03
#define THERMALMGMT_THERMALMGMT_ENDTESTMODE                                0x7FFF

//0x1C2 = 450 :- Temperatures on INC are sent as *10 values.
#define TEMP_NORMAL                                                        0x01C2        
//0x352 = 850 :- Temperatures on INC are sent as *10 values.
#define TEMP_BAD                                                           0x0352   
//0x3B6 = 950 :- Temperatures on INC are sent as *10 values.
#define TEMP_POOR                                                          0x03B6   

#define IGNORE_PARAM                                                       TM_STATE_INVALID

namespace {
  SCCComms comms;
}
typedef struct
{
  uint8 MsgBuffer[10];
  uint16 MsgLength;
}tyMsgInfo;

uint8 ComponentStatus = TM_STATE_INVALID;

// The fixture for testing class ExampleSm.
class ThermalMgmt : public ::testing::Test {
protected:
    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:
   virtual void SetUp() 
   {
      comms.SCCInit( 0xC70C );
      ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
	  if(ComponentStatus != TM_ACTIVE)
	  {
	     tyMsgInfo TestMsg;
         tyMsgInfo ResponseMsg;     
         TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_ACTIVE, 0x02);
         ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_ACTIVE, 0x02);
         ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
         ComponentStatus = TM_ACTIVE;
		 PrepareForTest();
		 sleep(7);
	  }	  
   }
   virtual void TearDown()
   {
      comms.SCCShutdown();
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   void PrepareForTest()
   {
      for(int device = TM_MODULE_IMX_INTEL; device <= TM_MODULE_AMPHEATSINK; device++)
	  {
         {
		    tyMsgInfo TestMsg;
            tyMsgInfo ResponseMsg;     
            TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP, device, THERMALMGMT_THERMALMGMT_ENDTESTMODE);
            ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP, device, THERMALMGMT_THERMALMGMT_ENDTESTMODE);
  
            ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
	     }
		 {
            tyMsgInfo TestMsg;
            tyMsgInfo ResponseMsg;     
            TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_STATE, device, TM_STATE_GOOD);
            ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE, device, (uint8_t)TM_STATE_GOOD);
            
            ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
		 }
	  }
   }   
   void SendMessage(char* Msg, int MsgLength)
   {
      printf ("             SendMessage");
      for (size_t x=0; x < MsgLength; x++)
      {
         printf (" 0x%02X", Msg[x]);  
      }
      printf (" \n");
      comms.SCCSendMsg( ( void * ) Msg, MsgLength );
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   int ReceiveMsg(char* RecvMsgBuffer, int MsgBufferLength)
   {
      size_t recvd_msg_size = 0x00;
      comms.SCCRecvMsg( RecvMsgBuffer, MsgBufferLength, recvd_msg_size );
      printf ("             RecvMsg length %d \n", recvd_msg_size);
      printf ("                RecvMsg [ ");
      for (size_t ind=0; ind<recvd_msg_size; ++ind)
      {
         printf ("0x%02X ", RecvMsgBuffer[ind]);
      }
	  printf ("] \n");
      return recvd_msg_size;
   }   
   int ValidateTestResponse(uint8* TestMsg, uint8* ResponseMsg, uint16 TestMsgLength, uint16 ResponseMsgLength)
   {
      int cond = 0;
      size_t ind=0;
      size_t recvd_msg_size;
	  uint16 RcvBufferSize = 1024;
	  uint8 RcvBuffer[RcvBufferSize];
      
      SendMessage((char*)TestMsg,(int)TestMsgLength);
      do
      {
	     recvd_msg_size = ReceiveMsg((char*)RcvBuffer, (int)RcvBufferSize);
         if ((recvd_msg_size >= 1 ) && (ResponseMsg[0]  == RcvBuffer[0]))
         {
            // check for correct msg
			//This is added because the Cyclic Temp messages that are sent every 58 sec or any Temp Status Update messages should not 
			//cause the failure of any Test Cases
			if(((THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_TEMP == RcvBuffer[0]) \
			 || (THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE == RcvBuffer[0])) \
			&& (TM_MODULE_EMMC == RcvBuffer[1]) && (TM_MODULE_EMMC != TestMsg[1]))
			{
			   ResponseMsg[1] = IGNORE_PARAM;
			   ResponseMsg[2] = IGNORE_PARAM;
			   ResponseMsg[3] = IGNORE_PARAM;
			   ResponseMsg[4] = IGNORE_PARAM;
			}
			//Ignore Temperature Values
			if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_TEMP == ResponseMsg[0])
			{
			   ResponseMsg[2] = ResponseMsg[3] = IGNORE_PARAM;
			}
			for( ind=0; ind<recvd_msg_size; ++ind )
            {
			   if(IGNORE_PARAM != ResponseMsg[ ind ])
			   {
			      EXPECT_EQ( ResponseMsg[ ind ], RcvBuffer[ ind ] );
			   }
            }
         }
         else if ((recvd_msg_size == 3) && (THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REJECT  == RcvBuffer[0]) \
		       && (TestMsg[0]  == RcvBuffer[2])  )
         {
            // check for reject 
            cond=1;
            printf("             msg OK REJECT reason 0x%02X \n", RcvBuffer[1]);
         }
         else 
         {
            // ignore other msgs
            PRINTF ("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, ResponseMsg[0],ResponseMsg[1],ResponseMsg[2],ResponseMsg[3]);
         }
      }while ((cond == 0) && (recvd_msg_size > 0));
   }
};

//All test cases derived from this class will behave as if the CompStatus received from IMX == INACTIVE
class ThermalMgmtNoCompStatus : public ::testing::Test {
protected:
    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

   virtual void SetUp() 
   {
      comms.SCCInit( 0xC70C );
      ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
	  if(ComponentStatus != TM_INACTIVE)
	  {
	     tyMsgInfo TestMsg;
         tyMsgInfo ResponseMsg;     
         TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_INACTIVE, 0x02);
         ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_INACTIVE, 0x02);
         ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
         ComponentStatus = TM_INACTIVE;
	  }
   }
   virtual void TearDown()
   {
      comms.SCCShutdown();
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   //To ensure that the TEST_TEMP feature is not active leading to erratic R_TEMP_STATE responses
   void PrepareForTest()
   {
      for(int device = TM_MODULE_IMX_INTEL; device <= TM_MODULE_AMPHEATSINK; device++)
	  {
	     tyMsgInfo TestMsg;
         tyMsgInfo ResponseMsg;     
         TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP, device, THERMALMGMT_THERMALMGMT_ENDTESTMODE);
         ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP, device, THERMALMGMT_THERMALMGMT_ENDTESTMODE);
          
         ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
	  }
   }
   
   void SendMessage(char* Msg, int MsgLength)
   {
      printf ("             SendMessage");
      for (int x=0; x < MsgLength; x++)
      {
         printf (" 0x%02X", Msg[x]);  
      }
      printf (" \n");
      comms.SCCSendMsg( ( void * ) Msg, MsgLength );
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   int ReceiveMsg(char* RecvMsgBuffer, int MsgBufferLength)
   {
      size_t recvd_msg_size = 0x00;
      size_t ind=0;
      comms.SCCRecvMsg( RecvMsgBuffer, MsgBufferLength, recvd_msg_size );
      //EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
      printf ("             RecvMsg length %d \n", recvd_msg_size);
      printf ("                Result [ ");
      for( ind=0; ind < recvd_msg_size; ++ind )
      {
         printf (" 0x%02X", RecvMsgBuffer[ind]);
      }	  
	  printf (" ] \n");
      return recvd_msg_size;
   }
   int ValidateTestResponse(uint8* TestMsg, uint8* ResponseMsg, uint16 TestMsgLength, uint16 ResponseMsgLength)
   {
      int cond;
      int loop;
      size_t ind;
      size_t recvd_msg_size;
	  uint16 RcvBufferSize = 1024;
	  uint8 RcvBuffer[RcvBufferSize];
      
	  if(NULL != TestMsg)
	  {
	     SendMessage((char*)TestMsg,(int)TestMsgLength);
      }
           
      loop=0;
      cond=0;
      while (cond == 0 && loop < 20) 
      {
         PRINTF("loop==> %d \n", loop);
	     recvd_msg_size = ReceiveMsg((char*)RcvBuffer, (int)RcvBufferSize);
         if ((recvd_msg_size >= 1 ) && (ResponseMsg[0]  == RcvBuffer[0]))
         {
            // check for correct msg
            cond=1;
            for( ind=0; ind < recvd_msg_size; ++ind )
            {
			   if(IGNORE_PARAM != ResponseMsg[ ind ])
			   {
                  EXPECT_EQ( ResponseMsg[ ind ], RcvBuffer[ ind ] );
			   }
            }
         }
         else if ((recvd_msg_size == 3) && (0x0b  == RcvBuffer[0]) && (TestMsg[0]  == RcvBuffer[2])  )
         {
            // check for reject 
            cond=1;
            printf("             msg OK REJECT reason 0x%02X \n", RcvBuffer[1]);
         }
         else if (0 == recvd_msg_size)
         {
            printf(" !!! Timeout !!! \n");
         }
         else 
         {
            // ignore other msgs
            PRINTF ("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, ResponseMsg[0],ResponseMsg[1],ResponseMsg[2],ResponseMsg[3]);
         }
         loop++;
      }
      //ASSERT_EQ(1,cond); 
      if (loop >= 20)
      {
         printf(" !!! Timeout END !!! \n");
      }
   }
};

class ThermalMgmt_DeviceIdParams : public ThermalMgmt,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class ThermalMgmt_FanIdParams : public ThermalMgmt,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class ThermalMgmt_DeviceIdParams_NoComponentStatus : public ThermalMgmtNoCompStatus,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class ThermalMgmt_FanIdParams_NoComponentStatus : public ThermalMgmtNoCompStatus,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        ThermalMgmt_DeviceIdParams_NoComponentStatus,
                        ::testing::Values(TM_MODULE_IMX_INTEL, TM_MODULE_BLUERAY_DRIVE, TM_MODULE_DISPLAY, TM_MODULE_EMMC, TM_MODULE_CDDRIVE, TM_MODULE_DVDDRIVE, TM_MODULE_GPS, TM_MODULE_GYRO, TM_MODULE_AMPHEATSINK, 0x1B));
						
INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        ThermalMgmt_DeviceIdParams,
                        ::testing::Values(TM_MODULE_IMX_INTEL, TM_MODULE_BLUERAY_DRIVE, TM_MODULE_DISPLAY, TM_MODULE_EMMC, TM_MODULE_CDDRIVE, TM_MODULE_DVDDRIVE, TM_MODULE_GPS, TM_MODULE_GYRO, TM_MODULE_AMPHEATSINK, 0x1B));

INSTANTIATE_TEST_CASE_P(TestedFanIds,
                        ThermalMgmt_FanIdParams_NoComponentStatus,
                        ::testing::Values(TM_FAN_ALL, TM_FAN_1, TM_FAN_2, TM_FAN_3, TM_FAN_4, 0x08));
						
INSTANTIATE_TEST_CASE_P(TestedFanIds,
                        ThermalMgmt_FanIdParams,
                        ::testing::Values(TM_FAN_ALL, TM_FAN_1, TM_FAN_2, TM_FAN_3, TM_FAN_4, 0x08));



TEST_F( ThermalMgmt, CStatus_Active ) {
   
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_ACTIVE, 0x02);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_ACTIVE, 0x02);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

TEST_F( ThermalMgmt, CStatus_InActive ) {

   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_INACTIVE, 0x02);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_INACTIVE, 0x02);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

TEST_F( ThermalMgmt, CStatus_Active_Inactive ) {
   
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_ACTIVE, 0x02);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_ACTIVE, 0x02);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
   
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_INACTIVE, 0x02);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_INACTIVE, 0x02);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

TEST_F( ThermalMgmt, CStatus_Active_Inactive_Active ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, (uint8_t)TM_ACTIVE, (uint8_t)0x02);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_ACTIVE, 0x02);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
   
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_INACTIVE, 0x02);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_INACTIVE, 0x02);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
   
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_ACTIVE, 0x02);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_ACTIVE, 0x02);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}
//Version is not considered for any reject messages currently
/* TEST_F( ThermalMgmt, CStatus_VersionMismatch ) {
    tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, (uint8_t)THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, (uint8_t)TM_ACTIVE, (uint8_t)0x07);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_ACTIVE, 0x02);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
} */

//////////////////////////////////////////////////////////////////////////////////////////
// Test-2 CGetState
//       /opt/bosch/base/bin/inc_send_out.out -p 50956 -b 50956 -r scc 30
//////////////////////////////////////////////////////////////////////////////////////////
TEST_F( ThermalMgmt, CGetState ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_GET_STATE);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_STATE(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_STATE, TM_STATE_GOOD);  

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

TEST_F( ThermalMgmtNoCompStatus, CGetState_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_GET_STATE);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_STATE(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_STATE, TM_STATE_GOOD);  
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}
//////////////////////////////////////////////////////////////////////////////////////////
// Test-3 CSetState
//       /opt/bosch/base/bin/inc_send_out.out -p 50956 -b 50956 -r scc 32-04-20
//////////////////////////////////////////////////////////////////////////////////////////
TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CSetStateAll_GOOD_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_STATE, GetParam(), TM_STATE_GOOD);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE, GetParam(), (uint8_t)TM_STATE_GOOD);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CSetStateAll_BAD_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_STATE, GetParam(), TM_STATE_BAD);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE, GetParam(), TM_STATE_BAD);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

// This test case has been disabled since it causes iMx reset/shutdown and the INC-SCC tests are not enabled to resume after the reset/shutdown

/*TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CSetStateAll_POOR_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_STATE, GetParam(), TM_STATE_POOR);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE, GetParam(), TM_STATE_POOR);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}*/

TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CSetTempAll_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEMP, GetParam(), TEMP_NORMAL);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEMP, GetParam(), TEMP_NORMAL);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
} 

TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CReqFanAll_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_REQ_FAN, GetParam(), 0x09);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REQ_FAN(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REQ_FAN, GetParam(), 0x09);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( ThermalMgmt_FanIdParams_NoComponentStatus, CSetTestFanAll_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN, GetParam(), 0x09);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN, GetParam(), 0x09);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

//////////////////////////////////////////////////////////////////////////////////////////
// Test-3 CSetState
//       /opt/bosch/base/bin/inc_send_out.out -p 50956 -b 50956 -r scc 32-04-20
//////////////////////////////////////////////////////////////////////////////////////////
TEST_P( ThermalMgmt_DeviceIdParams, CSetStateAll_BAD_AfterComponentStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_STATE, GetParam(), TM_STATE_BAD);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE, GetParam(), TM_STATE_BAD);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

// This test case has been disabled since it causes iMx reset/shutdown and the INC-SCC tests are not enabled to resume after the reset/shutdown

/*TEST_P( ThermalMgmt_DeviceIdParams, CSetStateAll_POOR_AfterComponentStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_STATE, GetParam(), TM_STATE_POOR);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE, GetParam(), TM_STATE_POOR);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}*/

TEST_P( ThermalMgmt_DeviceIdParams, CSetStateAll_GOOD_AfterComponentStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_STATE, GetParam(), TM_STATE_GOOD);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE, GetParam(), TM_STATE_GOOD);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}
//////////////////////////////////////////////////////////////////////////////////////////
// Test-4 CReqFan
//       /opt/bosch/base/bin/inc_send_out.out -p 50956 -b 50956 -r scc 34-01-09
//////////////////////////////////////////////////////////////////////////////////////////
TEST_P( ThermalMgmt_DeviceIdParams, CReqFanAll_AfterCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_REQ_FAN, GetParam(), 0x09);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REQ_FAN(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REQ_FAN, GetParam(), 0x09);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
}
TEST_P( ThermalMgmt_DeviceIdParams, CReqFanAll_AfterCompStatus_invalidFanSpeed ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_REQ_FAN, GetParam(), 0x96);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REQ_FAN, TM_REJ_INVALID_PARAM, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_REQ_FAN);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
}

//////////////////////////////////////////////////////////////////////////////////////////
// Test-5 CSetTemp
//       /opt/bosch/base/bin/inc_send_out.out -p 50956 -b 50956 -r scc 38-01-20
//////////////////////////////////////////////////////////////////////////////////////////
TEST_P( ThermalMgmt_DeviceIdParams, CSetTempAll_GOOD_AfterCompStatusActive ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEMP, GetParam(), TEMP_NORMAL);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEMP, GetParam(), TEMP_NORMAL);

   if(TM_MODULE_EMMC == GetParam())
   {
      ResponseMsg.MsgBuffer[0] = THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REJECT;
      ResponseMsg.MsgBuffer[1] = TM_REJ_INVALID_PARAM;
	  ResponseMsg.MsgBuffer[2] = THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEMP;
	  ResponseMsg.MsgLength = 3;
   }
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
} 

TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CSetTempAll_GOOD_BeforeCompStatusActive ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEMP, GetParam(), TEMP_NORMAL);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEMP, GetParam(), TEMP_NORMAL);

   if(TM_MODULE_EMMC == GetParam())
   {
      ResponseMsg.MsgBuffer[0] = THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REJECT;
      ResponseMsg.MsgBuffer[1] = TM_REJ_SEQUENCE_ERROR;
	  ResponseMsg.MsgBuffer[2] = THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEMP;
	  ResponseMsg.MsgLength = 3;
   }
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
} 

//////////////////////////////////////////////////////////////////////////////////////////
// Test-6 CGetTemp
//       /opt/bosch/base/bin/inc_send_out.out -p 50956 -b 50956 -r scc 36-01
//////////////////////////////////////////////////////////////////////////////////////////

TEST_P( ThermalMgmt_DeviceIdParams, CGetTempAll ) {

   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_GET_TEMP, GetParam());
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_TEMP, GetParam(), (sint16_t)IGNORE_PARAM);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
}

TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CGetTempAll_BeforeCompStatusActive ) {

   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_GET_TEMP, GetParam());
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REJECT, IGNORE_PARAM, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_GET_TEMP);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
}

//////////////////////////////////////////////////////////////////////////////////////////
// Test-7 CSetTestTemp
//       /opt/bosch/base/bin/inc_send_out.out -p 50956 -b 50956 -r scc 3A-01-20-00
//////////////////////////////////////////////////////////////////////////////////////////

TEST_P( ThermalMgmt_DeviceIdParams, CSetTestTempAll_TEMP_GOOD ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP, GetParam(), TEMP_NORMAL);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP, GetParam(), TEMP_NORMAL);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CSetTestTempAll_TEMP_GOOD_BeforeCompStatusActive ) {
//printf ("!!! Testcase CSetTestTempAll disabled while no reply from V850 !!! \n");
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP, GetParam(), TEMP_NORMAL);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP, GetParam(), TEMP_NORMAL);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( ThermalMgmt_DeviceIdParams, CSetTestTempAll_TEMP_BAD ) {

   char TestMsg[] = { THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP , GetParam(), GLB_LOBYTE(TEMP_BAD), GLB_HIBYTE(TEMP_BAD) };
   int cond = 0;
   size_t recvd_msg_size;
   uint16 RcvBufferSize = 1024;
   uint8 RcvBuffer[RcvBufferSize];
    
   SendMessage((char*)TestMsg,(int)4);
   
   do 
   {
	  comms.SCCRecvMsg( (char*)RcvBuffer, (int)RcvBufferSize, recvd_msg_size );
      if (recvd_msg_size >= 1 )
	  {
	     if((THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP  == RcvBuffer[0]) \
		 || (THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_TEMP == RcvBuffer[0] ) \
		 && (TM_MODULE_EMMC == RcvBuffer[1]) && (TM_MODULE_EMMC == TestMsg[1]))
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
	        EXPECT_EQ( GetParam(), RcvBuffer[ 1 ] );
			EXPECT_EQ( GLB_LOBYTE(TEMP_BAD) , RcvBuffer[ 2 ] );
		    EXPECT_EQ( GLB_HIBYTE(TEMP_BAD) , RcvBuffer[ 3 ] );
			cond++;
	     }
	     else if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE  == RcvBuffer[0])
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
	        EXPECT_EQ( GetParam(), RcvBuffer[ 1 ] );
		    EXPECT_EQ( TM_STATE_BAD , RcvBuffer[ 2 ] );
			cond++;
	     }
		 else if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_STATE  == RcvBuffer[0])
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
		    EXPECT_EQ( TM_STATE_BAD , RcvBuffer[ 1 ] );
			cond++;
	     }
         else if ((recvd_msg_size == 3) && (0x0b  == RcvBuffer[0]) /* && (TestMsg[0]  == RcvBuffer[2]) */  )
         {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
            // check for reject 
            cond++;
            printf("             msg OK REJECT reason 0x%02X \n", RcvBuffer[1]);
         }
         else if (0 == recvd_msg_size)
         {
            printf(" !!! Timeout !!! \n");
         }
         else 
         {
            // ignore other msgs
            PRINTF ("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, RcvBuffer[0],RcvBuffer[1],RcvBuffer[2],RcvBuffer[3]);
         }
      }
   }while ((cond < 4 ) && (recvd_msg_size > 0));
   printf("\nResponse received: %d: ",cond);
   PrepareForTest();
   sleep(7);
}

TEST_P( ThermalMgmt_DeviceIdParams, CSetTestTempAll_TEMP_DISABLE_TEST_TEMP ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP, GetParam(), THERMALMGMT_THERMALMGMT_ENDTESTMODE);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP, GetParam(), THERMALMGMT_THERMALMGMT_ENDTESTMODE);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( ThermalMgmt_DeviceIdParams_NoComponentStatus, CSetTestTempAll_TEMP_DISABLE_TEST_TEMP_BeforeActiveCompStatus ) {

   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP, GetParam(), THERMALMGMT_THERMALMGMT_ENDTESTMODE);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP, GetParam(), THERMALMGMT_THERMALMGMT_ENDTESTMODE);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);    
}

//////////////////////////////////////////////////////////////////////////////////////////
// Test-8 CSetTestFan
//       /opt/bosch/base/bin/inc_send_out.out -p 50956 -b 50956 -r scc 3C-00-25
//////////////////////////////////////////////////////////////////////////////////////////

TEST_P( ThermalMgmt_FanIdParams, CSetTestFanAll_AfterCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN, GetParam(), 0x09);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN, GetParam(), 0x09);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
}
TEST_P( ThermalMgmt_FanIdParams, CSetTestFanAll_AfterCompStatus_invalidFanSpeed ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN(TestMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN, GetParam(), 0x96);
   ResponseMsg.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT(ResponseMsg.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN, TM_REJ_INVALID_PARAM, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
}


TEST_F( ThermalMgmt, SetTempToBadAndMakeCompStatInactiveAndCheckForReceivedResponse ) {
   char TestMsg[] = { THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP , TM_MODULE_EMMC, GLB_LOBYTE(TEMP_BAD), GLB_HIBYTE(TEMP_BAD) };     
   int cond = 0;
   uint16 RcvBufferSize = 1024;
   uint8 RcvBuffer[RcvBufferSize];
   tyMsgInfo TestMsg1;
   tyMsgInfo ResponseMsg1;   
   size_t recvd_msg_size;
   
   SendMessage((char*)TestMsg,(int)4);
  
   TestMsg1.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS(TestMsg1.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS, TM_INACTIVE, 0x02);
   ResponseMsg1.MsgLength = THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg1.MsgBuffer, THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS, TM_INACTIVE, 0x02);

   ValidateTestResponse(TestMsg1.MsgBuffer, ResponseMsg1.MsgBuffer, TestMsg1.MsgLength, ResponseMsg1.MsgLength);

   do 
   {
	  comms.SCCRecvMsg( (char*)RcvBuffer, (int)RcvBufferSize, recvd_msg_size );
      if (recvd_msg_size >= 1 ) 
	  {
/* 	     if((THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP  == RcvBuffer[0]) \
		 || (THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_TEMP == RcvBuffer[0]))
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
	        EXPECT_EQ( GetParam(), RcvBuffer[ 1 ] );
			EXPECT_EQ( GLB_LOBYTE(TEMP_BAD) , RcvBuffer[ 2 ] );
		    EXPECT_EQ( GLB_HIBYTE(TEMP_BAD) , RcvBuffer[ 3 ] );
			cond++;
	     }
	     else if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE  == RcvBuffer[0])
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
	        EXPECT_EQ( GetParam(), RcvBuffer[ 1 ] );
		    EXPECT_EQ( TM_STATE_BAD , RcvBuffer[ 2 ] );
			cond++;
	     }
		 else if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_STATE  == RcvBuffer[0])
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
		    EXPECT_EQ( TM_STATE_BAD , RcvBuffer[ 1 ] );
			cond++;
	     }
         else */ if ((recvd_msg_size == 3) && (THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_REJECT  == RcvBuffer[0]) /* && (TestMsg[0]  == RcvBuffer[2]) */  )
         {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
            // check for reject 
            cond = 5;
            printf("             msg OK REJECT reason 0x%02X \n", RcvBuffer[1]);
         }
         else if (0 == recvd_msg_size)
         {
            printf(" !!! Timeout !!! \n");
         }
         else 
         {
            // ignore other msgs
            PRINTF ("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, RcvBuffer[0],RcvBuffer[1],RcvBuffer[2],RcvBuffer[3]);
         }
      }
   }while ((cond < 4 ) && (recvd_msg_size > 0));
   printf("\nResponse received: %d: ",cond);
   PrepareForTest();
   sleep(7);
}

TEST_F( ThermalMgmt_DeviceIdParams, CSetTestTempEmmCBad_RcvRGetTempBeforeStateChange ) {

   char TestMsg[] = { THERMALMGMT_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP , TM_MODULE_EMMC, GLB_LOBYTE(TEMP_BAD), GLB_HIBYTE(TEMP_BAD) };
   int cond = 0;
   size_t recvd_msg_size;
   uint16 RcvBufferSize = 1024;
   uint8 RcvBuffer[RcvBufferSize];
   bool RGetTempReceived = FALSE;
   int timeout = 10000;

   SendMessage((char*)TestMsg,(int)4);

   do
   {
      // This test expects the reception of INC messages that arrive 5 seconds after the test INC message has been sent.
	  // Hence the timeout is set to 10 seconds instead of the default POLL_TIMEOUT.
	  comms.SCCRecvMsg( (char*)RcvBuffer, (int)RcvBufferSize, recvd_msg_size, timeout);
      if (recvd_msg_size >= 1 )
	  {
	     if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP  == RcvBuffer[0]) 
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
	        EXPECT_EQ( TM_MODULE_EMMC, RcvBuffer[ 1 ] );
			EXPECT_EQ( GLB_LOBYTE(TEMP_BAD) , RcvBuffer[ 2 ] );
		    EXPECT_EQ( GLB_HIBYTE(TEMP_BAD) , RcvBuffer[ 3 ] );
			cond++;
	     }
		 // For an R_GET_TEMP message, check if the device is EmmC and ignore other cyclically sent R_GET_TEMP messages
		 else if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_TEMP == RcvBuffer[0] && TM_MODULE_EMMC == RcvBuffer[1])
		 {
		    RGetTempReceived = TRUE;
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
			EXPECT_EQ( GLB_LOBYTE(TEMP_BAD) , RcvBuffer[ 2 ] );
		    EXPECT_EQ( GLB_HIBYTE(TEMP_BAD) , RcvBuffer[ 3 ] );
			cond++;
		 }
		 // Check that R_SET_STATE is received only after R_GET_TEMP for the device that triggered the state change has been received.
	     else if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_SET_STATE  == RcvBuffer[0] && RGetTempReceived == TRUE)
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
	        EXPECT_EQ( TM_MODULE_EMMC, RcvBuffer[ 1 ] );
		    EXPECT_EQ( TM_STATE_BAD , RcvBuffer[ 2 ] );
			cond++;
	     }
		 // Check that R_GET_STATE is received only after R_GET_TEMP for the device that triggered the state change has been received.
		 else if(THERMALMGMT_SCC_THERMAL_MANAGEMENT_R_GET_STATE  == RcvBuffer[0])
	     {
		    printf("\nRecv Msg: \n");
	        for(int index = 0; index < recvd_msg_size; index++)
	        {
	           printf(" 0x%02X  ",RcvBuffer[index]);
	        }
	        printf("\n");
		    EXPECT_EQ( TM_STATE_BAD , RcvBuffer[ 1 ] );
			cond++;
	     }
         else 
         {
            // ignore other msgs
            PRINTF ("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, RcvBuffer[0],RcvBuffer[1],RcvBuffer[2],RcvBuffer[3]);
         }
      }
      else if (0 == recvd_msg_size)
      {
          printf(" !!! Timeout !!! \n");
      }
   }while ((cond < 4 ) && (recvd_msg_size > 0));
   
   if(cond < 4)
   {
       ADD_FAILURE();
       printf("The INC messages were not received at all OR they were not received in the expected order.\n");
   }
   
   printf("\nResponse received: %d: ",cond);
   PrepareForTest();
   sleep(7);
}