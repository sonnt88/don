// TODO
// 1.  CSetTestTempAll:  antworten beim z.b. set temperature nicht korrekt. error muss kommen.
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_thermal_management_tests.cpp
// Author:    W�stefeld
// Date:      19 November 2013
//
// Contains Scc Thermal Management test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include <string>
#include "spm_SerializeDeserialize_System_Statemachine.h"
#include "IncComPduEnum.h"

#define PRINTF    //printf
/* <  >  PDU_SPM_ThermalManagement */
#define SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS                0x20
#define SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS                0x21
#define SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE           0x30
#define SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE           0x31
#define SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER                  0x32
#define SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER                  0x33
#define SCC_SYSTEM_STATEMACHINE_C_SET_SYSTEMSTATE                 0x34
#define SCC_SYSTEM_STATEMACHINE_R_SET_SYSTEMSTATE                 0x35
#define SCC_SYSTEM_STATEMACHINE_R_SCC_SYSTEMSTATE                 0x37
#define SCC_SYSTEM_STATEMACHINE_R_APP_SYSTEMSTATE                 0x39
#define SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE            0x3A
#define SCC_SYSTEM_STATEMACHINE_R_SEND_APP_SYSTEMSTATE            0x3B

#define SCC_SYSTEM_STATEMACHINE_R_REJECT                          0x0B

#define SYSTEM_STATEMACHINE_CatalogueVersion                      0x01

#define SYSTEMSM_INVALID                                          0xFF
#define SYSTEMSM_INVALID_MSG_VAL                                  0x50

namespace {
  SCCComms comms;
}
typedef struct
{
  uint8 MsgBuffer[50];
  uint16 MsgLength;
}tyMsgInfo;

uint8 SmComponentStatus = SYSTEMSM_INVALID;

// The fixture for testing class.
class SystemSm : public ::testing::Test {
protected:
    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:
   virtual void SetUp() 
   {
      comms.SCCInit( 0xC717 );
      ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
	  if(SmComponentStatus != INCCOM_APPL_STATUS_ACTIVE)
	  {
	     tyMsgInfo TestMsg;
         tyMsgInfo ResponseMsg;     
         TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
         ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
         ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
         SmComponentStatus = INCCOM_APPL_STATUS_ACTIVE;
	  }	  
   }
   virtual void TearDown()
   {
      comms.SCCShutdown();
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   void SendMessage(char* Msg, int MsgLength)
   {
      printf ("             SendMessage");
      for (size_t x=0; x < MsgLength; x++)
      {
         printf (" 0x%02X", Msg[x]);  
      }
      printf (" \n");
      comms.SCCSendMsg( ( void * ) Msg, MsgLength );
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   int ReceiveMsg(char* RecvMsgBuffer, int MsgBufferLength)
   {
      size_t recvd_msg_size = 0x00;
      comms.SCCRecvMsg( RecvMsgBuffer, MsgBufferLength, recvd_msg_size );
      printf ("             RecvMsg length %d \n", recvd_msg_size);
      printf ("                RecvMsg [ ");
      for (size_t ind=0; ind<recvd_msg_size; ++ind)
      {
         printf ("0x%02X ", RecvMsgBuffer[ind]);
      }
	  printf ("] \n");
      return recvd_msg_size;
   }   
   int ValidateTestResponse(uint8* TestMsg, uint8* ResponseMsg, uint16 TestMsgLength, uint16 ResponseMsgLength)
   {
      int cond = 0;
      size_t ind=0;
      size_t recvd_msg_size;
	  uint16 RcvBufferSize = 1024;
	  uint8 RcvBuffer[RcvBufferSize];
      
      SendMessage((char*)TestMsg,(int)TestMsgLength);
      do
      {
	     recvd_msg_size = ReceiveMsg((char*)RcvBuffer, (int)RcvBufferSize);
         if ((recvd_msg_size >= 1 ) && (ResponseMsg[0]  == RcvBuffer[0]))
         {
		    printf("!!!!  length %d \n", recvd_msg_size);
            // check for correct msg			
			for( ind=0; ind<recvd_msg_size; ++ind )
            {
		       EXPECT_EQ( ResponseMsg[ ind ], RcvBuffer[ ind ] );
			   printf("!!!!  ResponseMsg[%d]: %d\n", ind, ResponseMsg[ind]);
            }
			cond = 1;
         } 
         else if ((recvd_msg_size == 3) && (SCC_SYSTEM_STATEMACHINE_R_REJECT  == RcvBuffer[0]) \
		       && (TestMsg[0]  == RcvBuffer[2])  )
         {
            // check for reject 
            cond=1;
            printf("             msg OK REJECT reason 0x%02X \n", RcvBuffer[1]);
         }
         else 
         {
            // ignore other msgs
            printf("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, ResponseMsg[0],ResponseMsg[1],ResponseMsg[2],ResponseMsg[3]);
         }
      }while ((cond == 0) && (recvd_msg_size > 0));
   }
};

//All test cases derived from this class will behave as if the CompStatus received from IMX == INACTIVE
class SystemSmNoCompStatus : public ::testing::Test {
protected:
    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:
   virtual void SetUp() 
   {
      comms.SCCInit( 0xC717 );
      ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
	  if(SmComponentStatus != INCCOM_APPL_STATUS_INACTIVE)
	  {
	     tyMsgInfo TestMsg;
         tyMsgInfo ResponseMsg;     
         TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, INCCOM_APPL_STATUS_INACTIVE, 0x01);
         ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_INACTIVE, 0x01);
         ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
         SmComponentStatus = INCCOM_APPL_STATUS_INACTIVE;
	  }
   }
   virtual void TearDown()
   {
      comms.SCCShutdown();
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   void SendMessage(char* Msg, int MsgLength)
   {
      printf ("             SendMessage");
      for (int x=0; x < MsgLength; x++)
      {
         printf (" 0x%02X", Msg[x]);  
      }
      printf (" \n");
      comms.SCCSendMsg( ( void * ) Msg, MsgLength );
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   int ReceiveMsg(char* RecvMsgBuffer, int MsgBufferLength)
   {
      size_t recvd_msg_size = 0x00;
      size_t ind=0;
      comms.SCCRecvMsg( RecvMsgBuffer, MsgBufferLength, recvd_msg_size );
      //EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
      printf ("             RecvMsg length %d \n", recvd_msg_size);
      printf ("                Result [ ");
      for( ind=0; ind < recvd_msg_size; ++ind )
      {
         printf (" 0x%02X", RecvMsgBuffer[ind]);
      }	  
	  printf (" ] \n");
      return recvd_msg_size;
   }
   int ValidateTestResponse(uint8* TestMsg, uint8* ResponseMsg, uint16 TestMsgLength, uint16 ResponseMsgLength)
   {
      int cond;
      int loop;
      size_t ind;
      size_t recvd_msg_size;
	  uint16 RcvBufferSize = 1024;
	  uint8 RcvBuffer[RcvBufferSize];
	        
	  if(NULL != TestMsg)
	  {
	     SendMessage((char*)TestMsg,(int)TestMsgLength);
      }
           
      loop=0;
      cond=0;
      while (cond == 0 && loop < 20) 
      {
         PRINTF("loop==> %d \n", loop);
	     recvd_msg_size = ReceiveMsg((char*)RcvBuffer, (int)RcvBufferSize);
         if ((recvd_msg_size >= 1 ) && (ResponseMsg[0]  == RcvBuffer[0]))
         {
            // check for correct msg
            cond=1;
            for( ind=0; ind < recvd_msg_size; ++ind )
            {
               EXPECT_EQ( ResponseMsg[ ind ], RcvBuffer[ ind ] );
			   printf("!!!!  ResponseMsg[%d]: %d\n", ind, ResponseMsg[ind]);
            }
         } 
         else if ((recvd_msg_size == 3) && (0x0b  == RcvBuffer[0]) && (TestMsg[0]  == RcvBuffer[2])  )
         {
            // check for reject 
            cond=1;
            printf("             msg OK REJECT reason 0x%02X \n", RcvBuffer[1]);
         }
         else if (0 == recvd_msg_size)
         {
            printf(" !!! Timeout !!! \n");
         }
		 else if (0 == recvd_msg_size)
         {
            printf(" !!! Timeout !!! \n");
         }
         else 
         {
            // ignore other msgs
            PRINTF ("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, ResponseMsg[0],ResponseMsg[1],ResponseMsg[2],ResponseMsg[3]);
         }
         loop++;
      }
      //ASSERT_EQ(1,cond); 
      if (loop >= 20)
      {
         printf(" !!! Timeout END !!! \n");
      }
   }
};

class SystemSm_StateMachineTypeParams : public SystemSm,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class SystemSm_StateMachineTypeParamsNoComponentStatus : public SystemSmNoCompStatus,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class SystemSm_StateMachineTriggerParams : public SystemSm,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class SystemSm_StateMachineTriggerParamsNoComponentStatus : public SystemSmNoCompStatus,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class SystemSm_SccSystemState : public SystemSm,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class SystemSm_SccSystemStateNoComponentStatus : public SystemSmNoCompStatus,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class SystemSm_AppSystemState : public SystemSm,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

class SystemSm_AppSystemStateNoComponentStatus : public SystemSmNoCompStatus,
                                   public ::testing::WithParamInterface<unsigned int>
{
   // this is the base class for the parameterised tests
};

INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SystemSm_StateMachineTypeParamsNoComponentStatus,
                        ::testing::Values(INCCOM_STATEMACHINETYPE_DEFAULT, INCCOM_STATEMACHINETYPE_AIVI_NISSAN_CMFB, INCCOM_STATEMACHINETYPE_AIVI_NISSAN_CMFCD, INCCOM_STATEMACHINETYPE_AIVI_NISSAN_OTHER,\
						INCCOM_STATEMACHINETYPE_AIVI_RENAULT_C1A, INCCOM_STATEMACHINETYPE_AIVI_RENAULT_T4VS, INCCOM_STATEMACHINETYPE_AIVI_NISSAN_C1A));
						
INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SystemSm_StateMachineTypeParams,
                        ::testing::Values(INCCOM_STATEMACHINETYPE_DEFAULT, INCCOM_STATEMACHINETYPE_AIVI_NISSAN_CMFB, INCCOM_STATEMACHINETYPE_AIVI_NISSAN_CMFCD, INCCOM_STATEMACHINETYPE_AIVI_NISSAN_OTHER,\
						INCCOM_STATEMACHINETYPE_AIVI_RENAULT_C1A, INCCOM_STATEMACHINETYPE_AIVI_RENAULT_T4VS, INCCOM_STATEMACHINETYPE_AIVI_NISSAN_C1A));

INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SystemSm_StateMachineTriggerParamsNoComponentStatus,
                        ::testing::Values(INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS, INCCOM_STATEMACHINETRIGGER_EMERGENCY_OFF, INCCOM_STATEMACHINETRIGGER_FAST_SHUTDOWN, INCCOM_STATEMACHINETRIGGER_DIMMING,\
						INCCOM_STATEMACHINETRIGGER_DOWNLOAD, INCCOM_STATEMACHINETRIGGER_DIAG_ACTIVITY, INCCOM_STATEMACHINETRIGGER_DIAGNOSIS, INCCOM_STATEMACHINETRIGGER_PHONE, \
						INCCOM_STATEMACHINETRIGGER_S_CONTACT, INCCOM_STATEMACHINETRIGGER_IGNITION, INCCOM_STATEMACHINETRIGGER_TRANSPORTMODE, INCCOM_STATEMACHINETRIGGER_DOOROPEN, \
						INCCOM_STATEMACHINETRIGGER_DOORLOCK));
						
INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SystemSm_StateMachineTriggerParams,
                        ::testing::Values(INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS, INCCOM_STATEMACHINETRIGGER_EMERGENCY_OFF, INCCOM_STATEMACHINETRIGGER_FAST_SHUTDOWN, INCCOM_STATEMACHINETRIGGER_DIMMING,\
						INCCOM_STATEMACHINETRIGGER_DOWNLOAD, INCCOM_STATEMACHINETRIGGER_DIAG_ACTIVITY, INCCOM_STATEMACHINETRIGGER_DIAGNOSIS, INCCOM_STATEMACHINETRIGGER_PHONE, \
						INCCOM_STATEMACHINETRIGGER_S_CONTACT, INCCOM_STATEMACHINETRIGGER_IGNITION, INCCOM_STATEMACHINETRIGGER_TRANSPORTMODE, INCCOM_STATEMACHINETRIGGER_DOOROPEN, \
						INCCOM_STATEMACHINETRIGGER_DOORLOCK));

//The sending of param INCCOM_SCCSYSTEMSTATE_HMI_NOT_ALLOWED will cause a restart of the CPU by the SCC.
//Since restarting SCC test after a CPU restart is not yet supported, the parameter INCCOM_SCCSYSTEMSTATE_HMI_NOT_ALLOWED is deliberately commented out
//for SystemSm_SccSystemState; the param remains for SystemSm_SccSystemStateNoComponentStatus as the msg will anyway be rejected
INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SystemSm_SccSystemStateNoComponentStatus,
                        ::testing::Values(INCCOM_SCCSYSTEMSTATE_HMI_NOT_ALLOWED , INCCOM_SCCSYSTEMSTATE_HMI_ALLOWED));
						
// INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        // SystemSm_SccSystemState,
                        // ::testing::Values( INCCOM_SCCSYSTEMSTATE_HMI_NOT_ALLOWED, INCCOM_SCCSYSTEMSTATE_HMI_ALLOWED));

INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SystemSm_AppSystemStateNoComponentStatus,
                        ::testing::Values(INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_SUSPEND , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_STANDBY , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_OFF , \
						INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_DOWNLOAD , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_ON , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_BACKGROUND , \
						INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_DOOR_OPEN , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_IGNITION , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_DIAGNOSIS , \
						INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_STATE_PROFILE , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_PREPARE_SHUTDOWN , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_SHUTDOWN));

INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SystemSm_AppSystemState,
                        ::testing::Values(INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_SUSPEND , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_STANDBY , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_OFF , \
						INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_DOWNLOAD , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_ON , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_BACKGROUND , \
						INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_DOOR_OPEN , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_IGNITION , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_DIAGNOSIS , \
						INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_STATE_PROFILE , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_PREPARE_SHUTDOWN , INCCOM_APPSYSTEMSTATE_SPM_SYSTEM_SHUTDOWN));

//////////////////////////////////////////////////////////////////////////////////////////
// Test-2 SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS
//       /opt/bosch/base/bin/inc_send_out.out -p 50967 -b 50967 -r scc 0x20
//////////////////////////////////////////////////////////////////////////////////////////
TEST_F( SystemSm, CStatus_Active ) {
   
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

TEST_F( SystemSm, CStatus_InActive ) {

   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, INCCOM_APPL_STATUS_INACTIVE, 0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_INACTIVE, 0x01);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

TEST_F( SystemSm, CStatus_Active_Inactive ) {
   
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
   
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, INCCOM_APPL_STATUS_INACTIVE, 0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_INACTIVE, 0x01);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

TEST_F( SystemSm, CStatus_Active_Inactive_Active ) 
{
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, (uint8_t)INCCOM_APPL_STATUS_ACTIVE, (uint8_t)0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
   
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, INCCOM_APPL_STATUS_INACTIVE, 0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_INACTIVE, 0x01);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
   
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

//Check for Reject messages when Invalid params are sent by the CPU:
//RejectReason - INCCOM_REJECT_REASON_REJ_INVALID_PARA
TEST_F( SystemSm, CStatus_InvalidParameter ) 
{
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, (INCCOM_APPL_STATUS_INACTIVE + SYSTEMSM_INVALID_MSG_VAL), 0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT, INCCOM_REJECT_REASON_REJ_INVALID_PARA, 
                                                                                SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

//Version is not considered for any reject messages currently
//Nothing happens on the v850 - no reject msgs even - for version mismatch
/* TEST_F( SystemSm, CStatus_VersionMismatch ) {
    tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS(TestMsg.MsgBuffer, (uint8_t)SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS, (uint8_t)INCCOM_APPL_STATUS_ACTIVE, (uint8_t)0x01);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS, INCCOM_APPL_STATUS_ACTIVE, 0x01);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}
 */
////////////////////////////////////////////////////////////////////////////////////////////////
// Test-2 SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE
//       /opt/bosch/base/bin/inc_send_out.out -p 50967 -b 50967 -r scc 0x30 + StateMachineType
////////////////////////////////////////////////////////////////////////////////////////////////
TEST_P( SystemSm_StateMachineTypeParams, CSetStatemachineType ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE, GetParam());
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE, GetParam());  

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

//v850 rejects messages that are sent when the CompStatus is still set to INACTIVE with a Reject Reason = INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR
TEST_P( SystemSm_StateMachineTypeParamsNoComponentStatus, CSetStatemachineType_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE, GetParam());
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT,
                                                                                INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR,   
                                                                                SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

//Check for Reject messages when Invalid params are sent by the CPU:
//RejectReason - INCCOM_REJECT_REASON_REJ_INVALID_PARA
TEST_P( SystemSm_StateMachineTypeParams, CSetStatemachineType_InvalidSMType ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE, (GetParam()+SYSTEMSM_INVALID_MSG_VAL));
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT, INCCOM_REJECT_REASON_REJ_INVALID_PARA, 
                                                                                SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Test-3 SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER
//       /opt/bosch/base/bin/inc_send_out.out -p 50967 -b 50967 -r scc 0x32 + TriggerType + TriggerState
/////////////////////////////////////////////////////////////////////////////////////////////////////////

//v850 rejects messages that are sent when the CompStatus is still set to INACTIVE with a Reject Reason = INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR
TEST_P( SystemSm_StateMachineTriggerParamsNoComponentStatus, SmTriggerType_TriggerActive_BeforeCompStatus  ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;       
  
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER, GetParam(), INCCOM_TRIGGERSTATE_ACTIVE);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT, 
                                                                                INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR,   
                                                                                SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

//v850 rejects messages that are sent when the CompStatus is still set to INACTIVE with a Reject Reason = INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR
TEST_P( SystemSm_StateMachineTriggerParamsNoComponentStatus, SmTriggerType_TriggerInactive_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;    
   /*Variables - NumOfTriggers and the StateMachineTriggerList are hardcoded to match v850 values as this message handling is not clear yet*/
   uint8 NumOfTriggers = 0x00;
   uint8 StateMachineTriggerList1[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   uint8 StateMachineTriggerList2[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   uint8 StateMachineTriggerList3[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER, GetParam(), INCCOM_TRIGGERSTATE_INACTIVE);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT, 
                                                                                INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR,   
                                                                                SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER);
   
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( SystemSm_StateMachineTriggerParams, SmTriggerType_TriggerInactive ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   /*Variables - NumOfTriggers and the StateMachineTriggerList are hardcoded to match v850 values as this message handling is not clear yet*/
   uint8 NumOfTriggers = 0x00;
   uint8 SizeOfArray   = 0x08;
   uint8 StateMachineTriggerList1[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   uint8 StateMachineTriggerList2[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   uint8 StateMachineTriggerList3[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER, GetParam(), INCCOM_TRIGGERSTATE_INACTIVE);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER, NumOfTriggers, StateMachineTriggerList1, SizeOfArray, \
                           StateMachineTriggerList2, SizeOfArray, StateMachineTriggerList3, SizeOfArray);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( SystemSm_StateMachineTriggerParams, SmTriggerType_TriggerActive ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   /*Variables - NumOfTriggers and the StateMachineTriggerList are hardcoded to match v850 values as this message handling is not clear yet*/
   uint8 NumOfTriggers = 0x00;
   uint8 SizeOfArray = 0x08;   
   uint8 StateMachineTriggerList1[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   uint8 StateMachineTriggerList2[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   uint8 StateMachineTriggerList3[8] = {INCCOM_STATEMACHINETRIGGER_SET_SD_CARD_ACCESS};
   
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER, GetParam(), INCCOM_TRIGGERSTATE_ACTIVE);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER, NumOfTriggers, StateMachineTriggerList1, SizeOfArray, \
                           StateMachineTriggerList2, SizeOfArray, StateMachineTriggerList3, SizeOfArray);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

//Check for Reject messages when Invalid params are sent by the CPU:
//RejectReason - INCCOM_REJECT_REASON_REJ_INVALID_PARA
TEST_P( SystemSm_StateMachineTriggerParams, SmTriggerType_TriggerActive_InvalidTriggerType  ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;       
  
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER, (GetParam()+SYSTEMSM_INVALID_MSG_VAL), INCCOM_TRIGGERSTATE_ACTIVE);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT, INCCOM_REJECT_REASON_REJ_INVALID_PARA, 
                                                                                SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

//Check for Reject messages when Invalid params are sent by the CPU:
//RejectReason - INCCOM_REJECT_REASON_REJ_INVALID_PARA
TEST_P( SystemSm_StateMachineTriggerParams, SmTriggerType_InvalidTriggerState  ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;       
  
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER, GetParam(), (INCCOM_TRIGGERSTATE_ACTIVE+SYSTEMSM_INVALID_MSG_VAL));
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT, INCCOM_REJECT_REASON_REJ_INVALID_PARA, 
                                                                                SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Test-4 SCC_SYSTEM_STATEMACHINE_C_SET_SYSTEMSTATE
//       /opt/bosch/base/bin/inc_send_out.out -p 50967 -b 50967 -r scc 0x34 + SccSystemState
/////////////////////////////////////////////////////////////////////////////////////////////////////////
TEST_P( SystemSm_SccSystemState, CSetSystemState )
{
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SYSTEMSTATE, INCCOM_SCCSYSTEMSTATE_HMI_ALLOWED);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SCC_SYSTEMSTATE(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_SET_SYSTEMSTATE,INCCOM_SCCSYSTEMSTATE_HMI_ALLOWED);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
} 

//v850 rejects messages that are sent when the CompStatus is still set to INACTIVE with a Reject Reason = INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR,
TEST_P( SystemSm_SccSystemStateNoComponentStatus, CSetSystemState_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SYSTEMSTATE, GetParam());
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT,
                                                                                INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR,   
                                                                                SCC_SYSTEM_STATEMACHINE_C_SET_SYSTEMSTATE);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);    
}

//Check for Reject messages when Invalid params are sent by the CPU:
//RejectReason - INCCOM_REJECT_REASON_REJ_INVALID_PARA
TEST_P( SystemSm_SccSystemState, CSetSystemState_InvalidSccSystemState )
{
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SET_SYSTEMSTATE, (SYSTEMSM_INVALID_MSG_VAL));
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT, INCCOM_REJECT_REASON_REJ_INVALID_PARA, 
                                                                                SCC_SYSTEM_STATEMACHINE_C_SET_SYSTEMSTATE);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
} 

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Test-5 SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE
//       /opt/bosch/base/bin/inc_send_out.out -p 50967 -b 50967 -r scc 0x3A + AppSystemState
/////////////////////////////////////////////////////////////////////////////////////////////////////////
TEST_P( SystemSm_AppSystemState, CSendAppSystemState )
{
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE, GetParam());
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SEND_APP_SYSTEMSTATE(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_SET_SYSTEMSTATE,INCCOM_SCCSYSTEMSTATE_HMI_ALLOWED);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
} 

//v850 rejects messages that are sent when the CompStatus is still set to INACTIVE with a Reject Reason = INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR
TEST_P( SystemSm_AppSystemStateNoComponentStatus, CSendAppSystemState_BeforeCompStatus ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE, GetParam());
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT,  
                                                                                INCCOM_REJECT_REASON_REJ_SEQUENCE_ERROR,
                                                                                SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);    
}
//Check for Reject messages when Invalid params are sent by the CPU:
//RejectReason - INCCOM_REJECT_REASON_REJ_INVALID_PARA
TEST_P( SystemSm_SccSystemState, CSendAppSystemState_InvalidAppSystemState )
{
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE(TestMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE, 0x12);
   ResponseMsg.MsgLength = SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT(ResponseMsg.MsgBuffer, SCC_SYSTEM_STATEMACHINE_R_REJECT, INCCOM_REJECT_REASON_REJ_INVALID_PARA, 
                                                                                SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE);

   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
}

