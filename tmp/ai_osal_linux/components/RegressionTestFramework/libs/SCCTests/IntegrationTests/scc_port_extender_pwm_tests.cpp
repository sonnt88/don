// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_port_extender_pwm_tests.cpp
// Author:    W�stefeld
// Date:      19 November 2013
//
// Contains Port Extender PWM test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include <string>
#include <stdlib.h>


namespace {
  SCCComms comms;
}  

TEST( PortExtPwm, RemoveDriver ) {

 sleep(2);

 int ret = system("rmmod gpio-inc");
 
 EXPECT_GT( ret, -1) << strerror( errno );

}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step One: Establish communications with AutoSAR  PORT_EXTENDER_GPIO
TEST( PortExtPwm, TestInit) {

  comms.SCCInit( 0xC70A );
  ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( PortExtPwm, CStatus ) {
  char TestMsg[] = { 0x20, 0x01 };

  comms.SCCSendMsg( ( void * ) TestMsg, 2 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( PortExtPwm, RStatus ) {
  char buffer[ 1024 ];
  char TestMsg[] = { 0x21, 0x01 };
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 2, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( TestMsg[ ind ], buffer[ ind ] );
}



// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Four: Shut the communications link down
TEST( PortExtPwm, TestShutdown ) {

  comms.SCCShutdown();
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

