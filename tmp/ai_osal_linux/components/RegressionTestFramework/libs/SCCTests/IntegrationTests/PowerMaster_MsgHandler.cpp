

/* ################################################### */
/* ####### GENERATED CODE - DO NOT TOUCH ############# */
/* ################################################### */


/* =================================================== */
/* GeneratorVersion : Script_2.1 */
/* Generated On     : 14.11.2014 14:26:42 */
/* Description      :  */
/* =================================================== */
#include "Std_MsgHelper.h"
#include "PowerMaster_MsgHandler.h"


/*=================================================================*/
/* Funtions */
/*=================================================================*/
Length_t POWERMASTER_lSerialize_C_CTRL_RESET_EXECUTION( uint8 * pu8Buffer, uint8 u8CtrlReset)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8CtrlReset);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_CTRL_RESET_EXECUTION */

Length_t POWERMASTER_lDeSerialize_C_CTRL_RESET_EXECUTION( uint8 * pu8Buffer, uint8 * pu8CtrlReset)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8CtrlReset);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_CTRL_RESET_EXECUTION */

Length_t POWERMASTER_lSerialize_C_EXTEND_POWER_OFF_TIMEOUT( uint8 * pu8Buffer, uint16 u16Seconds)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16Seconds);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_EXTEND_POWER_OFF_TIMEOUT */

Length_t POWERMASTER_lDeSerialize_C_EXTEND_POWER_OFF_TIMEOUT( uint8 * pu8Buffer, uint16 * pu16Seconds)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16Seconds);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_EXTEND_POWER_OFF_TIMEOUT */

Length_t POWERMASTER_lSerialize_C_GET_DATA( uint8 * pu8Buffer, uint8 u8DataMode,
                                            uint8 u8RequestedMsg)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8DataMode);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8RequestedMsg);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_GET_DATA */

Length_t POWERMASTER_lDeSerialize_C_GET_DATA( uint8 * pu8Buffer, uint8 * pu8DataMode,
                                              uint8 * pu8RequestedMsg)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8DataMode);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8RequestedMsg);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_GET_DATA */

Length_t POWERMASTER_lSerialize_C_INDICATE_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 u8ApplicationMode,
                                                             uint8 u8ResetReason,
                                                             uint8 u8MajorVersion,
                                                             uint8 u8MinorVersion)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8ApplicationMode);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8ResetReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8MajorVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8MinorVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_INDICATE_CLIENT_APP_STATE */

Length_t POWERMASTER_lDeSerialize_C_INDICATE_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 * pu8ApplicationMode,
                                                               uint8 * pu8ResetReason,
                                                               uint8 * pu8MajorVersion,
                                                               uint8 * pu8MinorVersion)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8ApplicationMode);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8ResetReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8MajorVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8MinorVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_INDICATE_CLIENT_APP_STATE */

Length_t POWERMASTER_lSerialize_C_PROC_RESET_REQUEST( uint8 * pu8Buffer, uint8 u8ProcID,
                                                      uint16 u16ResetTime)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8ProcID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16ResetTime);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_PROC_RESET_REQUEST */

Length_t POWERMASTER_lDeSerialize_C_PROC_RESET_REQUEST( uint8 * pu8Buffer, uint8 * pu8ProcID,
                                                        uint16 * pu16ResetTime)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8ProcID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16ResetTime);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_PROC_RESET_REQUEST */

Length_t POWERMASTER_lSerialize_C_REQ_CLIENT_BOOT_MODE( uint8 * pu8Buffer, uint32 u32Magic1,
                                                        uint32 u32Magic2,
                                                        uint8 u8BootMode,
                                                        uint32 u32Magic3,
                                                        uint32 u32Magic4,
                                                        uint8 u8NotBootMode)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint32(pu8Buffer, u32Magic1);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint32(pu8Buffer, u32Magic2);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8BootMode);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint32(pu8Buffer, u32Magic3);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint32(pu8Buffer, u32Magic4);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8NotBootMode);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_REQ_CLIENT_BOOT_MODE */

Length_t POWERMASTER_lDeSerialize_C_REQ_CLIENT_BOOT_MODE( uint8 * pu8Buffer, uint32 * pu32Magic1,
                                                          uint32 * pu32Magic2,
                                                          uint8 * pu8BootMode,
                                                          uint32 * pu32Magic3,
                                                          uint32 * pu32Magic4,
                                                          uint8 * pu8NotBootMode)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint32(pu8Buffer, pu32Magic1);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint32(pu8Buffer, pu32Magic2);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8BootMode);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint32(pu8Buffer, pu32Magic3);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint32(pu8Buffer, pu32Magic4);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8NotBootMode);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_REQ_CLIENT_BOOT_MODE */

Length_t POWERMASTER_lSerialize_C_SET_WAKEUP_CONFIG( uint8 * pu8Buffer, uint32 u32WupConfig)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint32(pu8Buffer, u32WupConfig);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_SET_WAKEUP_CONFIG */

Length_t POWERMASTER_lDeSerialize_C_SET_WAKEUP_CONFIG( uint8 * pu8Buffer, uint32 * pu32WupConfig)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint32(pu8Buffer, pu32WupConfig);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_SET_WAKEUP_CONFIG */

Length_t POWERMASTER_lSerialize_C_SHUTDOWN_IN_PROGRESS( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_SHUTDOWN_IN_PROGRESS */

Length_t POWERMASTER_lDeSerialize_C_SHUTDOWN_IN_PROGRESS( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_SHUTDOWN_IN_PROGRESS */

Length_t POWERMASTER_lSerialize_C_STARTUP_FINISHED( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_STARTUP_FINISHED */

Length_t POWERMASTER_lDeSerialize_C_STARTUP_FINISHED( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_STARTUP_FINISHED */

Length_t POWERMASTER_lSerialize_C_WAKEUP_EVENT_ACK( uint8 * pu8Buffer, uint8 u8WupEvent,
                                                    uint16 u16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8WupEvent);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_WAKEUP_EVENT_ACK */

Length_t POWERMASTER_lDeSerialize_C_WAKEUP_EVENT_ACK( uint8 * pu8Buffer, uint8 * pu8WupEvent,
                                                      uint16 * pu16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8WupEvent);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_WAKEUP_EVENT_ACK */

Length_t POWERMASTER_lSerialize_C_WAKEUP_STATE_ACK( uint8 * pu8Buffer, uint16 u16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_WAKEUP_STATE_ACK */

Length_t POWERMASTER_lDeSerialize_C_WAKEUP_STATE_ACK( uint8 * pu8Buffer, uint16 * pu16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_WAKEUP_STATE_ACK */

Length_t POWERMASTER_lSerialize_C_WATCHDOG( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_C_WATCHDOG */

Length_t POWERMASTER_lDeSerialize_C_WATCHDOG( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_C_WATCHDOG */

Length_t POWERMASTER_lSerialize_R_CTRL_RESET_EXECUTION( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_CTRL_RESET_EXECUTION */

Length_t POWERMASTER_lDeSerialize_R_CTRL_RESET_EXECUTION( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_CTRL_RESET_EXECUTION */

Length_t POWERMASTER_lSerialize_R_EXTEND_POWER_OFF_TIMEOUT( uint8 * pu8Buffer, uint16 u16Timeout)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16Timeout);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_EXTEND_POWER_OFF_TIMEOUT */

Length_t POWERMASTER_lDeSerialize_R_EXTEND_POWER_OFF_TIMEOUT( uint8 * pu8Buffer, uint16 * pu16Timeout)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16Timeout);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_EXTEND_POWER_OFF_TIMEOUT */

Length_t POWERMASTER_lSerialize_R_INDICATE_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 u8ApplicationMode,
                                                             uint8 u8ResetReason,
                                                             uint8 u8MajorVersion,
                                                             uint8 u8MinorVersion,
                                                             uint8 u8Result)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8ApplicationMode);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8ResetReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8MajorVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8MinorVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8Result);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_INDICATE_CLIENT_APP_STATE */

Length_t POWERMASTER_lDeSerialize_R_INDICATE_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 * pu8ApplicationMode,
                                                               uint8 * pu8ResetReason,
                                                               uint8 * pu8MajorVersion,
                                                               uint8 * pu8MinorVersion,
                                                               uint8 * pu8Result)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8ApplicationMode);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8ResetReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8MajorVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8MinorVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8Result);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_INDICATE_CLIENT_APP_STATE */

Length_t POWERMASTER_lSerialize_R_PROC_RESET_REQUEST( uint8 * pu8Buffer, uint8 u8ProcID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8ProcID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_PROC_RESET_REQUEST */

Length_t POWERMASTER_lDeSerialize_R_PROC_RESET_REQUEST( uint8 * pu8Buffer, uint8 * pu8ProcID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8ProcID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_PROC_RESET_REQUEST */

Length_t POWERMASTER_lSerialize_R_REJECT( uint8 * pu8Buffer, uint8 u8RejectReason,
                                          uint8 u8MsgCodeRejected)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8RejectReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8MsgCodeRejected);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_REJECT */

Length_t POWERMASTER_lDeSerialize_R_REJECT( uint8 * pu8Buffer, uint8 * pu8RejectReason,
                                            uint8 * pu8MsgCodeRejected)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8RejectReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8MsgCodeRejected);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_REJECT */

Length_t POWERMASTER_lSerialize_R_REQ_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 u8ApplicationMode)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8ApplicationMode);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_REQ_CLIENT_APP_STATE */

Length_t POWERMASTER_lDeSerialize_R_REQ_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 * pu8ApplicationMode)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8ApplicationMode);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_REQ_CLIENT_APP_STATE */

Length_t POWERMASTER_lSerialize_R_REQ_CLIENT_BOOT_MODE( uint8 * pu8Buffer, uint8 u8BootMode)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8BootMode);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_REQ_CLIENT_BOOT_MODE */

Length_t POWERMASTER_lDeSerialize_R_REQ_CLIENT_BOOT_MODE( uint8 * pu8Buffer, uint8 * pu8BootMode)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8BootMode);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_REQ_CLIENT_BOOT_MODE */

Length_t POWERMASTER_lSerialize_R_SECURITY_VERSION_MISMATCH( uint8 * pu8Buffer, uint8 u8ProcBoardID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8ProcBoardID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_SECURITY_VERSION_MISMATCH */

Length_t POWERMASTER_lDeSerialize_R_SECURITY_VERSION_MISMATCH( uint8 * pu8Buffer, uint8 * pu8ProcBoardID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8ProcBoardID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_SECURITY_VERSION_MISMATCH */

Length_t POWERMASTER_lSerialize_R_SET_WAKEUP_CONFIG( uint8 * pu8Buffer, uint32 u32WupConfig)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint32(pu8Buffer, u32WupConfig);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_SET_WAKEUP_CONFIG */

Length_t POWERMASTER_lDeSerialize_R_SET_WAKEUP_CONFIG( uint8 * pu8Buffer, uint32 * pu32WupConfig)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint32(pu8Buffer, pu32WupConfig);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_SET_WAKEUP_CONFIG */

Length_t POWERMASTER_lSerialize_R_SHUTDOWN_IN_PROGRESS( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_SHUTDOWN_IN_PROGRESS */

Length_t POWERMASTER_lDeSerialize_R_SHUTDOWN_IN_PROGRESS( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_SHUTDOWN_IN_PROGRESS */

Length_t POWERMASTER_lSerialize_R_STARTUP_FINISHED( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_STARTUP_FINISHED */

Length_t POWERMASTER_lDeSerialize_R_STARTUP_FINISHED( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_STARTUP_FINISHED */

Length_t POWERMASTER_lSerialize_R_STARTUP_INFO( uint8 * pu8Buffer, uint8 u8StartType,
                                                uint8 u8DisconnectInfo)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8StartType);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8DisconnectInfo);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_STARTUP_INFO */

Length_t POWERMASTER_lDeSerialize_R_STARTUP_INFO( uint8 * pu8Buffer, uint8 * pu8StartType,
                                                  uint8 * pu8DisconnectInfo)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8StartType);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8DisconnectInfo);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_STARTUP_INFO */

Length_t POWERMASTER_lSerialize_R_WAKEUP_EVENT( uint8 * pu8Buffer, uint8 u8WupEvent,
                                                uint16 u16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8WupEvent);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_WAKEUP_EVENT */

Length_t POWERMASTER_lDeSerialize_R_WAKEUP_EVENT( uint8 * pu8Buffer, uint8 * pu8WupEvent,
                                                  uint16 * pu16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8WupEvent);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_WAKEUP_EVENT */

Length_t POWERMASTER_lSerialize_R_WAKEUP_EVENT_ACK( uint8 * pu8Buffer, uint16 u16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_WAKEUP_EVENT_ACK */

Length_t POWERMASTER_lDeSerialize_R_WAKEUP_EVENT_ACK( uint8 * pu8Buffer, uint16 * pu16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_WAKEUP_EVENT_ACK */

Length_t POWERMASTER_lSerialize_R_WAKEUP_STATE_ACK( uint8 * pu8Buffer, uint16 u16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_WAKEUP_STATE_ACK */

Length_t POWERMASTER_lDeSerialize_R_WAKEUP_STATE_ACK( uint8 * pu8Buffer, uint16 * pu16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_WAKEUP_STATE_ACK */

Length_t POWERMASTER_lSerialize_R_WAKEUP_REASON( uint8 * pu8Buffer, uint8 u8WupReason)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8WupReason);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_WAKEUP_REASON */

Length_t POWERMASTER_lDeSerialize_R_WAKEUP_REASON( uint8 * pu8Buffer, uint8 * pu8WupReason)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8WupReason);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_WAKEUP_REASON */

Length_t POWERMASTER_lSerialize_R_WAKEUP_STATE( uint8 * pu8Buffer, uint8 u8WupSrc,
                                                uint8 u8WupState,
                                                uint16 u16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8WupSrc);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint8(pu8Buffer, u8WupState);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lSerialize_uint16(pu8Buffer, u16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_WAKEUP_STATE */

Length_t POWERMASTER_lDeSerialize_R_WAKEUP_STATE( uint8 * pu8Buffer, uint8 * pu8WupSrc,
                                                  uint8 * pu8WupState,
                                                  uint16 * pu16MsgHandle)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8WupSrc);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint8(pu8Buffer, pu8WupState);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = POWERMASTER_lDeSerialize_uint16(pu8Buffer, pu16MsgHandle);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_WAKEUP_STATE */

Length_t POWERMASTER_lSerialize_R_WAKEUP_STATE_VECTOR( uint8 * pu8Buffer, uint32 u32WupStates)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lSerialize_uint32(pu8Buffer, u32WupStates);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_WAKEUP_STATE_VECTOR */

Length_t POWERMASTER_lDeSerialize_R_WAKEUP_STATE_VECTOR( uint8 * pu8Buffer, uint32 * pu32WupStates)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = POWERMASTER_lDeSerialize_uint32(pu8Buffer, pu32WupStates);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_WAKEUP_STATE_VECTOR */

Length_t POWERMASTER_lSerialize_R_WATCHDOG( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_R_WATCHDOG */

Length_t POWERMASTER_lDeSerialize_R_WATCHDOG( uint8 * pu8Buffer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_R_WATCHDOG */


