#if !defined(_SYSTEMSM_MSGHANDLER_H)
#define _SYSTEMSM_MSGHANDLER_H


/* ################################################### */
/* ####### GENERATED CODE - DO NOT TOUCH ############# */
/* ################################################### */


/* =================================================== */
/* GeneratorVersion : Script_2.1 */
/* Generated On     : 7/8/201611:10:18 AM */
/* Description      :  */
/* =================================================== */

#include "Std_MsgHelper.h"

/* <  >  PDU_SystemSm */
#define SYSTEMSM_U8_INC_MSGID_C_COMPONENT_STATUS                           0x20
#define SYSTEMSM_U8_INC_MSGID_C_SEND_APP_SYSTEMSTATE                       0x3A
#define SYSTEMSM_U8_INC_MSGID_C_SET_SCC_SYSTEMSTATE                        0x34
#define SYSTEMSM_U8_INC_MSGID_C_SET_SM_TRIGGER                             0x32
#define SYSTEMSM_U8_INC_MSGID_C_SET_STATEMACHINE_TYPE                      0x30
#define SYSTEMSM_U8_INC_MSGID_R_APP_SYSTEMSTATE                            0x39
#define SYSTEMSM_U8_INC_MSGID_R_COMPONENT_STATUS                           0x21
#define SYSTEMSM_U8_INC_MSGID_R_REJECT                                     0x0B
#define SYSTEMSM_U8_INC_MSGID_R_SCC_SYSTEMSTATE                            0x37
#define SYSTEMSM_U8_INC_MSGID_R_SEND_APP_SYSTEMSTATE                       0x3B
#define SYSTEMSM_U8_INC_MSGID_R_SET_SCC_SYSTEMSTATE                        0x35
#define SYSTEMSM_U8_INC_MSGID_R_SET_SM_TRIGGER                             0x33
#define SYSTEMSM_U8_INC_MSGID_R_SET_STATEMACHINE_TYPE                      0x31
#define SYSTEMSM_U8_INC_MSGLEN_C_COMPONENT_STATUS                          0x03
#define SYSTEMSM_U8_INC_MSGLEN_C_SEND_APP_SYSTEMSTATE                      0x02
#define SYSTEMSM_U8_INC_MSGLEN_C_SET_SCC_SYSTEMSTATE                       0x02
#define SYSTEMSM_U8_INC_MSGLEN_C_SET_SM_TRIGGER                            0x03
#define SYSTEMSM_U8_INC_MSGLEN_C_SET_STATEMACHINE_TYPE                     0x02
#define SYSTEMSM_U8_INC_MSGLEN_R_APP_SYSTEMSTATE                           0x02
#define SYSTEMSM_U8_INC_MSGLEN_R_COMPONENT_STATUS                          0x03
#define SYSTEMSM_U8_INC_MSGLEN_R_REJECT                                    0x03
#define SYSTEMSM_U8_INC_MSGLEN_R_SCC_SYSTEMSTATE                           0x02
#define SYSTEMSM_U8_INC_MSGLEN_R_SEND_APP_SYSTEMSTATE                      0x02
#define SYSTEMSM_U8_INC_MSGLEN_R_SET_SCC_SYSTEMSTATE                       0x02
#define SYSTEMSM_U8_INC_MSGLEN_R_SET_SM_TRIGGER                            0x1A
#define SYSTEMSM_U8_INC_MSGLEN_R_SET_STATEMACHINE_TYPE                     0x02
/* < enumeration >  eSystemSm_ApplicationStatus */
enum eSystemSm_ApplicationStatus
{
   SystemSm_ACTIVE = 0x01,
   SystemSm_INACTIVE = 0x02
}; /*eSystemSm_ApplicationStatus */
/* < enumeration >  eSystemSm_AppSystemState */
enum eSystemSm_AppSystemState
{
   SystemSm_SPM_SYSTEM_SUSPEND = 0x00,
   SystemSm_SPM_SYSTEM_STANDBY = 0x01,
   SystemSm_SPM_SYSTEM_OFF = 0x02,
   SystemSm_SPM_SYSTEM_DOWNLOAD = 0x03,
   SystemSm_SPM_SYSTEM_ON = 0x04,
   SystemSm_SPM_SYSTEM_BACKGROUND = 0x05,
   SystemSm_SPM_SYSTEM_DOOR_OPEN = 0x06,
   SystemSm_SPM_SYSTEM_IGNITION = 0x07,
   SystemSm_SPM_SYSTEM_DIAGNOSIS = 0x08,
   SystemSm_SPM_SYSTEM_STATE_PROFILE = 0x09,
   SystemSm_SPM_SYSTEM_PREPARE_SHUTDOWN = 0x10,
   SystemSm_SPM_SYSTEM_SHUTDOWN = 0x11
}; /*eSystemSm_AppSystemState */
/* < enumeration >  eSystemSm_RejReason */
enum eSystemSm_RejReason
{
   SystemSm_REJ_NO_REASON = 0x00,
   SystemSm_REJ_UNKNOWN_MESSAGE = 0x01,
   SystemSm_REJ_INVALID_PARAM = 0x02,
   SystemSm_REJ_TMP_UNAVAILBLE = 0x03,
   SystemSm_REJ_VERSION_MISMATCH = 0x04,
   SystemSm_REJ_NOT_SUPPORTED = 0x05,
   SystemSm_REJ_SEQUENCE_ERROR = 0X06
}; /*eSystemSm_RejReason */
/* < enumeration >  eSystemSm_SccSystemState */
enum eSystemSm_SccSystemState
{
   SystemSm_HMI_NOT_ALLOWED = 0x00,
   SystemSm_HMI_ALLOWED = 0x01
}; /*eSystemSm_SccSystemState */
/* < enumeration >  eSystemSm_StateMachineTrigger */
enum eSystemSm_StateMachineTrigger
{
   SYSTEMSM_SET_SD_CARD_ACCESS = 0x00,
   SYSTEMSM_EMERGENCY_OFF = 0x01,
   SYSTEMSM_FAST_SHUTDOWN = 0x02,
   SYSTEMSM_DIMMING = 0x03,
   SYSTEMSM_DOWNLOAD = 0x04,
   SYSTEMSM_DIAG_ACTIVITY = 0x05,
   SYSTEMSM_DIAGNOSIS = 0x06,
   SYSTEMSM_PHONE = 0x07,
   SYSTEMSM_S_CONTACT = 0x08,
   SYSTEMSM_IGNITION = 0x09,
   SYSTEMSM_TRANSPORTMODE = 0x0A,
   SYSTEMSM_DOOROPEN = 0x0B,
   SYSTEMSM_DOORLOCK = 0x0C
}; /*eSystemSm_StateMachineTrigger */
/* < enumeration >  eSystemSm_StateMachineType */
enum eSystemSm_StateMachineType
{
   SYSTEMSM_DEFAULT = 0x00,
   SYSTEMSM_AIVI_NISSAN_CMF_B = 0x01,
   SYSTEMSM_AIVI_NISSAN_CMF_CD = 0x02,
   SYSTEMSM_AIVI_NISSAN_OTHER = 0x03,
   SYSTEMSM_AIVI_RENAULT_C1A = 0x04,
   SYSTEMSM_AIVI_RENAULT_T4VS = 0x05,
   SYSTEMSM_AIVI_NISSAN_C1A = 0x06
}; /*eSystemSm_StateMachineType */
/* < enumeration >  eSystemSm_TriggerState */
enum eSystemSm_TriggerState
{
   SYSTEMSM_INACTIVE = 0x00,
   SYSTEMSM_ACTIVE = 0x01
}; /*eSystemSm_TriggerState */

/*=================================================================*/
/* Funtion Prototypes */
/*=================================================================*/
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                         uint8_t u8HostApplStat,
                                                                         uint8_t u8HostApplVer);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                           uint8_t * pu8HostApplStat,
                                                                           uint8_t * pu8HostApplVer);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                             uint8_t u8AppSystemState);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                               uint8_t * pu8AppSystemState);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                            uint8_t u8SccSystemState);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                              uint8_t * pu8SccSystemState);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                       uint8_t u8StateMachineTrigger,
                                                                       uint8_t u8TriggerState);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                         uint8_t * pu8StateMachineTrigger,
                                                                         uint8_t * pu8TriggerState);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                              uint8 u8StateMachineType);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                                uint8 * pu8StateMachineType);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgId,
                                                                        uint8_t u8AppSystemState);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgId,
                                                                          uint8_t * pu8AppSystemState);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                         uint8_t u8SCCApplStat,
                                                                         uint8_t u8SCCApplVer);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                           uint8_t * pu8SCCApplStat,
                                                                           uint8_t * pu8SCCApplVer);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT( uint8 * pu8Buffer, uint8_t u8msgID,
                                                               uint8_t u8RejectReason,
                                                               uint8_t u8RejectedMsgID);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                 uint8_t * pu8RejectReason,
                                                                 uint8_t * pu8RejectedMsgID);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                        uint8_t u8SccSystemState);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                          uint8_t * pu8SccSystemState);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SEND_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                             uint8_t u8SccSystemState);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SEND_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                               uint8_t * pu8SccSystemState);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                            uint8_t u8SccSystemState);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                              uint8_t * pu8SccSystemState);
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                       uint8_t u8NumOfTriggers,
                                                                       uint8_t * pu8StateMachineTrigger_List1, Length_t  lNumOfu8StateMachineTrigger_List1,
                                                                       uint8_t * pu8StateMachineTrigger_List2, Length_t  lNumOfu8StateMachineTrigger_List2,
                                                                       uint8_t * pu8StateMachineTrigger_List3, Length_t  lNumOfu8StateMachineTrigger_List3);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                         uint8_t * pu8NumOfTriggers,
                                                                         uint8_t * pu8StateMachineTrigger_List1, Length_t  * plNumOfu8StateMachineTrigger_List1,
                                                                         uint8_t * pu8StateMachineTrigger_List2, Length_t  * plNumOfu8StateMachineTrigger_List2,
                                                                         uint8_t * pu8StateMachineTrigger_List3, Length_t  * plNumOfu8StateMachineTrigger_List3);

#define SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List1_SIZE (8)


#define SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List2_SIZE (8)


#define SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List3_SIZE (8)

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                              uint8_t u8StateMachineType);
Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                                uint8_t * pu8StateMachineType);

/* Below are redirecting macros for basic types in Std_MsgHelper.h */
#define SYSTEMSM_lDeSerialize_uint8_t             lDeSerialize_uint8
#define SYSTEMSM_lSerialize_uint8_t               lSerialize_uint8
#define SYSTEMSM_lDeSerialize_uint8_t_Array       lDeSerialize_uint8_Array
#define SYSTEMSM_lSerialize_uint8_t_Array         lSerialize_uint8_Array

#define SYSTEMSM_lDeSerialize_uint8             lDeSerialize_uint8
#define SYSTEMSM_lSerialize_uint8               lSerialize_uint8
#define SYSTEMSM_lDeSerialize_uint8_Array       lDeSerialize_uint8_Array
#define SYSTEMSM_lSerialize_uint8_Array         lSerialize_uint8_Array


#endif /* _SYSTEMSM_MSGHANDLER_H */
