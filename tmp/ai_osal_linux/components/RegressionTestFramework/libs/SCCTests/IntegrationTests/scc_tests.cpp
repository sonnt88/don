// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_rtc_tests.cpp
// Author:    W�stefeld
// Date:      19 November 2013
//
// Contains RTC test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include <string>
#include <iostream>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include <linux/rtc.h>

#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>

#include <errno.h>


int system(const char *command);

#define RTC0 "/dev/rtc0"

namespace {
  int fd;
  struct rtc_time rtc_tm;
};


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step open rtc
TEST( DevRtc, Open ) {
	
  fd = open(RTC0, O_RDWR);
  EXPECT_GT( fd, -1);	
	
}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step read rtc
TEST( DevRtc, Read1 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " ";
  
  EXPECT_GT( retval, -1);	
	
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step write rtc
TEST( DevRtc, Write_100 ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 100;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " ";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step read rtc
TEST( DevRtc, Read2 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " ";
  
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Write_199 ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 199;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " ";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step read rtc
TEST( DevRtc, Read3 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " ";
  
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Write_201 ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 201;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " ";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step close rtc
TEST( DevRtc, CloseRtc ) {
	
  fd = close(fd);
  EXPECT_GT( fd, -1);	
	
}

TEST( DevRtc, Remove ) {

 int ret = system("rmmod rtc-inc");
 
 EXPECT_GT( ret, -1) << strerror( errno );

}
