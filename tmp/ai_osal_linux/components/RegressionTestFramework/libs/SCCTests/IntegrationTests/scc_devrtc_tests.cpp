// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_rtc_tests.cpp
// Author:    W�stefeld
// Date:      19 November 2013
//
// Contains RTC test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include <string>
#include <iostream>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include <linux/rtc.h>

#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>

#include <errno.h>


int system(const char *command);

#define RTC0 "/dev/rtc0"

namespace {
  int fd;
  struct rtc_time rtc_tm;
};


TEST( DevRtc, InitDriver ) {

 int ret = system("insmod /lib/modules/3.8.13.13-01921-g93325ef/kernel/drivers/rtc/rtc-inc.ko");
 
 EXPECT_GT( ret, -1) << strerror( errno );

}




// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step open rtc
TEST( DevRtc, Open ) {
	
  fd = open(RTC0, O_RDWR);
  EXPECT_GT( fd, -1);	
	
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step read rtc
TEST( DevRtc, Read1 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
	
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step write rtc
TEST( DevRtc, Write_Y100 ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 100;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step read rtc
TEST( DevRtc, Read_Y100 ) {

  int retval, check = -1;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  if (  rtc_tm.tm_mday  ==  10
     && rtc_tm.tm_mon   ==  11
     && rtc_tm.tm_year  == 100
     && rtc_tm.tm_hour  ==  20
     && rtc_tm.tm_min   ==  10
     && rtc_tm.tm_sec   ==  25 )
   {
	   check = 0;
   } 
  
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
  EXPECT_EQ( check,   0);	
	
}

TEST( DevRtc, Write_199 ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 199;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step read rtc
TEST( DevRtc, Read3 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Write_201 ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 201;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Write_01 ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  =   1;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Write_Invalid_Seconds ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =   16;
  rtc_tm.tm_mon   =   11;
  rtc_tm.tm_year  =  102;
  rtc_tm.tm_hour  =   20;
  rtc_tm.tm_min   =   10;
  rtc_tm.tm_sec   =   60;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Write_Invalid_Minute ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  =  102;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  60;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Write_Invalid_Hour ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  =  203;
  rtc_tm.tm_hour  =  24;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Write_Invalid_Day ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =   0;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  =  203;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Write_Invalid_Month ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  0;
  rtc_tm.tm_year  =  203;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Write_Invalid_Year ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  =  211;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Write_Invalid_LeapYear ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  29;
  rtc_tm.tm_mon   =   1; // 0 - 11 LINUX !!!  
  rtc_tm.tm_year  =  111;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}








// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step write rtc
TEST( DevRtc, Write_Month_0 ) {

  int retval;	
  //struct rtc_time rtc_tm;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  0;
  rtc_tm.tm_year  = 111;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step read rtc
TEST( DevRtc, Read_Month_0 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Write_Month_1 ) {

  int retval;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  1;
  rtc_tm.tm_year  = 111;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Read_Month_1 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Write_Month_2 ) {

  int retval;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  2;
  rtc_tm.tm_year  = 111;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Read_Month_2 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
                
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Write_Month_10 ) {

  int retval;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  10;
  rtc_tm.tm_year  = 111;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Read_Month_10 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Write_Month_11 ) {

  int retval;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  11;
  rtc_tm.tm_year  = 111;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Read_Month_11 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
	
}

TEST( DevRtc, Write_Month_12 ) {

  int retval;	
	
  rtc_tm.tm_mday  =  10;
  rtc_tm.tm_mon   =  12;
  rtc_tm.tm_year  = 111;
  rtc_tm.tm_hour  =  20;
  rtc_tm.tm_min   =  10;
  rtc_tm.tm_sec   =  25;
  
  std::cout << "            write Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
	
  retval = ioctl(fd, RTC_SET_TIME, &rtc_tm);
  EXPECT_EQ( retval, -1);	
	
}

TEST( DevRtc, Read_Month_12 ) {

  int retval;	
	
  rtc_tm.tm_mday = 0;
  rtc_tm.tm_mon   = 0;
  rtc_tm.tm_year   = 0;
  rtc_tm.tm_hour  = 0;
  rtc_tm.tm_min   = 0;
  rtc_tm.tm_sec    = 0;
	
  retval = ioctl(fd, RTC_RD_TIME, &rtc_tm);
  
  std::cout << "            read Date ";  
  std::cout << rtc_tm.tm_mday;
  std::cout << ".";
  std::cout << rtc_tm.tm_mon;
  std::cout << ".";
  std::cout << rtc_tm.tm_year;
  std::cout << " ";
  std::cout << rtc_tm.tm_hour;
  std::cout << ":";
  std::cout << rtc_tm.tm_min;
  std::cout << ":";
  std::cout << rtc_tm.tm_sec;
  std::cout << " \n";
  
  EXPECT_GT( retval, -1);	
	
}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step close rtc
TEST( DevRtc, Close ) {
	
  fd = close(fd);
  EXPECT_GT( fd, -1);	
	
}

