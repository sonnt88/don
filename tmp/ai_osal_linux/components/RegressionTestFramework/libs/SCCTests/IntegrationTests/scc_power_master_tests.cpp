// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_power_master_tests.cpp
// Author:    Ramanauskas 
// Date:      21 Mai 2014
//
// Contains Scc Power Master test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include "PowerMaster_MsgHandler.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sched.h>

 
#define PRINTF                           //printf
#define IGNORE_PARAM                     0x7F
#define SPM_SPMS_R_REJECT_LEN            0x03
#define SPMS_SPM_C_GET_DATA_LEN          0x03
#define SPM_SPMS_R_WAKEUP_STATE_ACK_LEN  0x03
#define SPMS_SPM_C_WAKEUP_STATE_ACK_LEN  0x03
/* #define TRUE           0x01 */
/* <  >  PDU_PowerMaster */

int system(const char *command);

using namespace std;
typedef pair<int,int> ResponseMsgsType;

namespace {
  SCCComms comms;
}

typedef struct
{
  uint8 MsgBuffer[10];
  uint16 MsgLength;
}tyMsgInfo;

//Param 1 = Message ; Param 2 = Message Length
const ResponseMsgsType ResponseMsgsForCGetData_AsSpecified[] = 
{
 make_pair(POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_STATE_VECTOR, 0x05),
 make_pair(POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_REASON      , 0x02),
 make_pair(POWERMASTER_u8MsgCode_SPM_SPMS_R_STARTUP_INFO       , 0x03)
};

const ResponseMsgsType MsgParamsForCClientAppState[] = 
{
 make_pair(SPM_u8ApplicationMode_STATE_TESTMANAGER , SPM_u8ResetReasonCPU_RESET_REASON_CPU_INTENDED),
 make_pair(SPM_u8ApplicationMode_STATE_TESTMANAGER , SPM_u8ResetReasonCPU_RESET_REASON_CPU_EXCEPTIONAL),
 make_pair(SPM_u8ApplicationMode_STATE_NORMAL      , SPM_u8ResetReasonCPU_RESET_REASON_CPU_INTENDED),
 make_pair(SPM_u8ApplicationMode_STATE_NORMAL      , SPM_u8ResetReasonCPU_RESET_REASON_CPU_EXCEPTIONAL),
 make_pair(SPM_u8ApplicationMode_STATE_DOWNLOAD    , SPM_u8ResetReasonCPU_RESET_REASON_CPU_INTENDED),
 make_pair(SPM_u8ApplicationMode_STATE_DOWNLOAD    , SPM_u8ResetReasonCPU_RESET_REASON_CPU_EXCEPTIONAL)
};

// The fixture for testing class ExampleSm.
class PowerMaster : public ::testing::Test {
protected:
    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

   virtual void SetUp() 
   {
      comms.SCCInit( 0xC701 );
      ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   virtual void TearDown()
   {
      comms.SCCShutdown();
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   void CheckResponseMessage(int mainLoopRun, uint8 *TestMsg, uint8 *TestMsgResult, uint16 TestMsgLength, uint16 ResponseMsgLength)
   {
      int cond;
      int loop;
      int mainLoop=0;
	  int Received_Responses_Count = 0;
      char buffer[ 1024 ];
      size_t recvd_msg_size;
	  
      while (mainLoop < mainLoopRun)
      {
     	 if (( mainLoop % 10 ) == 0)
            printf("PM test %d / %d\n", mainLoop, mainLoopRun);
            
         // send --------------
         comms.SCCSendMsg( ( void * ) TestMsg, TestMsgLength );
         EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
		 printf("\nSending Test Msg: ");
	     for(int BufferCount = 0; BufferCount <  TestMsgLength; BufferCount++)
		 {
			printf (" 0x%02X", TestMsg[BufferCount]);
		 }
		 printf("\n");
         // rcv -------------------
         loop=0;
         cond=0;
		 
		 while (cond == 0 && loop < 20) 
         {
            printf("loop==> %d \n", loop);
            comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );
            EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
			printf("recvd_msg_size==> %d", recvd_msg_size);
			printf("\nReceived Msg: ");
			for(int BufferCount = 0; BufferCount <  (int)recvd_msg_size; BufferCount++)
		    {
		       printf (" 0x%02X", buffer[BufferCount]); 
		    }
			printf("\n");
			
            if ((recvd_msg_size == ResponseMsgLength) && (TestMsgResult[0] == buffer[0]))
            {
			   cond=1;
			   Received_Responses_Count++;
			   PRINTF("msg OK length OK, Received message Length %d \n", recvd_msg_size);
			   // check for correct msg
			   for(int ReceivedBufferCount = 0; ReceivedBufferCount< ResponseMsgLength; ReceivedBufferCount++)
			   {
			      if(IGNORE_PARAM != TestMsgResult[ReceivedBufferCount])
				  {
				     EXPECT_EQ(TestMsgResult[ReceivedBufferCount], buffer[ReceivedBufferCount]);		    
				  }
				  else
				  {
				     printf("\nParam Msg[%d] = 0x%02X is Ignored in this Test case", ReceivedBufferCount, TestMsg[ReceivedBufferCount]);
				  }
                }
			}
		    else if ((recvd_msg_size == 0x03) && (POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT  == buffer[0]) && (TestMsg[0] == buffer[2]))
            {
            // check for reject 
            cond=1;
            printf("--> msg OK REJECT reason 0x%02X \n", buffer[1]);
            }
            else 
            {
              // ignore other msgs
              PRINTF("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, buffer[0],buffer[1],buffer[2],buffer[3]);
            }
            loop++;
         }
	     ASSERT_EQ(1,cond); 
         mainLoop++;
	  }
	  printf("\n%d responses received for %d Sent messages\n",Received_Responses_Count,mainLoopRun );
   }
};

class PowerMaster_CGetDataParams : public PowerMaster,
                                   public ::testing::WithParamInterface<ResponseMsgsType>
{
   // this is the base class for the parameterised tests
};

class PowerMaster_CClientAppStateParams : public PowerMaster,
                                   public ::testing::WithParamInterface<ResponseMsgsType>
{
   // this is the base class for the parameterised tests
};

class PowerMaster_CCtrlResetExecution: public PowerMaster,
                                       public ::testing::WithParamInterface<int>
{
   // this is the base class for the parameterised tests
};

class PowerMaster_CWakeUpStateAck: public PowerMaster,
                                public ::testing::WithParamInterface<int>
{
   // this is the base class for the parameterised tests
};

class PowerMaster_CWakeUpEvent: public PowerMaster,
                                public ::testing::WithParamInterface<int>
{
   // this is the base class for the parameterised tests
};

class PowerMaster_ProcResetRequest: public PowerMaster,
                                    public ::testing::WithParamInterface<int>
{
   // this is the base class for the parameterised tests
};

class PowerMaster_CClientAppStateV850ResetReasons: public PowerMaster,
                                    public ::testing::WithParamInterface<int>
{
   // this is the base class for the parameterised tests
};

class PowerMaster_ParamTypeInteger : public PowerMaster,
                                   public ::testing::WithParamInterface<int>
{
   // this is the base class for the parameterised tests
};


INSTANTIATE_TEST_CASE_P(TestMsg_CGetData_AsSpecified,
                        PowerMaster_CGetDataParams,
                        ::testing::ValuesIn(ResponseMsgsForCGetData_AsSpecified));
						
INSTANTIATE_TEST_CASE_P(CClientAppStateParams,
                        PowerMaster_CClientAppStateParams,
                        ::testing::ValuesIn(MsgParamsForCClientAppState));
						
INSTANTIATE_TEST_CASE_P(CCtrlResetExecutionParams,
                        PowerMaster_CCtrlResetExecution,
                         ::testing::Range(1, 10, 1));
						
INSTANTIATE_TEST_CASE_P(WakeUpStateParams,
                        PowerMaster_CWakeUpStateAck,
                         ::testing::Values(0, 1, 2, 4, 8, 16, 32, 64, 128));
						 
INSTANTIATE_TEST_CASE_P(WakeUpEventParams,
                        PowerMaster_CWakeUpEvent,
                         ::testing::Range(1, 12, 1));						

INSTANTIATE_TEST_CASE_P(V850ResetReasonsParams,
                        PowerMaster_CClientAppStateV850ResetReasons,
                         ::testing::Values(SPM_u8ResetReasonVCC_RESET_REASON_VCC_HW_WATCHDOG, SPM_u8ResetReasonVCC_RESET_REASON_VCC_POR, SPM_u8ResetReasonVCC_RESET_REASON_VCC_COLDSTART, \
						                   SPM_u8ResetReasonVCC_RESET_REASON_VCC_APPMODE_CHANGE, SPM_u8ResetReasonVCC_RESET_REASON_VCC_LPW, SPM_u8ResetReasonVCC_RESET_REASON_VCC_PLL_OSZ,\
										   SPM_u8ResetReasonVCC_RESET_REASON_VCC_SW, SPM_u8ResetReasonVCC_RESET_REASON_VCC_WARMSTART));					 

//For testing EXTEND_POWER_OFF_TIMEOUT message when the new timeout is greater than the old timeout
INSTANTIATE_TEST_CASE_P(PowerMaster_ExtendPowerOffTimeout_NewTimeGreaterThanOld,
                        PowerMaster_ParamTypeInteger,
                         ::testing::Range(1, 10000, 100));	
						 
//For testing EXTEND_POWER_OFF_TIMEOUT message when the new timeout is smaller than the old timeout
INSTANTIATE_TEST_CASE_P(PowerMaster_ExtendPowerOffTimeout_NewTimeSmallerThanOld,
						PowerMaster_ParamTypeInteger,
                         ::testing::Range(10000, 1, -100));	
						 
// INSTANTIATE_TEST_CASE_P(ProcReset,
                        // PowerMaster_ProcResetRequest_ParamsForRejectResponse,
                         // ::testing::Values(PROC_BLUETOOTH, PROC_WIRELESS_LAN, PROC_GNSS, PROC_XM, PROC_IPOD, PROC_ADR1, PROC_ADR2, PROC_VIDEO));
						 
TEST_P( PowerMaster_CGetDataParams, CGetData_AsSpecified ) {
  int cond;
  int loop;
  int mainLoop=0;
  int mainLoopRun =10;
  int Received_Responses_Count = 0;
  
  tyMsgInfo TestMsg;
  tyMsgInfo ResponseMsg;   
  TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_GET_DATA;
  TestMsg.MsgLength = POWERMASTER_lSerialize_C_GET_DATA((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8DataMode_AS_SPECIFIED, GetParam().first) + PM_PDU_MSG_CODE_LENGTH; 
  ResponseMsg.MsgBuffer[0] = GetParam().first;
  ResponseMsg.MsgLength = GetParam().second;    
  
  char buffer[ 1024 ];
 // char TestMsgResult[] = { GetParam().first, 0x10, 0x01, 0x00, 0x00};
  size_t recvd_msg_size;
   
  while (mainLoop < mainLoopRun)
  {
     if (( mainLoop % 10 ) == 0)
        printf("PM test %d / %d\n", mainLoop, mainLoopRun);
        
     // send --------------
     comms.SCCSendMsg( ( void * ) TestMsg.MsgBuffer, TestMsg.MsgLength );
     EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
     printf("\nSending Test Msg");
     for(int BufferCount = 0; BufferCount <  TestMsg.MsgLength; BufferCount++)
     {
        printf (" 0x%02X", TestMsg.MsgBuffer[BufferCount]);
     }
     printf("\n");
     printf("\nSent message Length %d \n", TestMsg.MsgLength);
       // rcv -------------------
     loop=0;
     cond=0;
     while (cond == 0 /* && loop < 20 && recvd_msg_size > 0 */)
     {
        printf("\nloop==> %d \n", loop);
        comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );
        EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
        if ((recvd_msg_size == GetParam().second) && (buffer[0] == GetParam().first))
        {
             // check for correct msg
           cond=1;
		   Received_Responses_Count++;
		   printf("\nmsg OK length OK, Received message Length %d \n", recvd_msg_size);
		   printf("\nReceived Msg: \n");
		   for(int buffer_count=0; buffer_count < recvd_msg_size; buffer_count++)
		   {
		      printf("0x%02X", buffer[buffer_count]);
		   }
		   printf("\n");
           /*for( int ind( 0 ); ind < recvd_msg_size; ++ind )
           {
               EXPECT_EQ( TestMsgResult[ ind ], buffer[ ind ] );
           }*/
        } 
        else if ((recvd_msg_size == SPM_SPMS_R_REJECT_LEN) && (POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT  == buffer[0]) && (TestMsg.MsgBuffer[0] == buffer[2]))
        {
              // check for reject 
              cond=1;
              printf("--> msg OK REJECT reason %d \n", buffer[1]);
        }
        else 
        {
              // ignore other msgs
              printf("!!!! msg !OK length %d Result[%d,%d,%d,%d,%d]\n", recvd_msg_size, buffer[0],buffer[1],buffer[2],buffer[3],buffer[4]);
        }
        loop++;
        }
        ASSERT_EQ(1,cond); 
        mainLoop++;
  }
  printf("%d responses received for \n",Received_Responses_Count );
  for(int byte_count =0; byte_count < SPMS_SPM_C_GET_DATA_LEN; byte_count++)
  {
    printf("TestMsg[%d]: 0x%x  ", byte_count, TestMsg.MsgBuffer[byte_count]);
  }
  printf("\n");   
}

TEST_F( PowerMaster, PMTest_CGetData_AllData ) {
   int mainLoopRun =10;
   int cond;
   int loop;
   int mainLoop=0;
   int Received_Responses_Count = 0;
   size_t recvd_msg_size;
   char buffer[ 1024 ];
   tyMsgInfo TestMsg;
   tyMsgInfo TestMsgResult1;   
   tyMsgInfo TestMsgResult2;  
   tyMsgInfo TestMsgResult3; 
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_GET_DATA;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_GET_DATA((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8DataMode_ALL_DATA, 0x00) + PM_PDU_MSG_CODE_LENGTH; 
   
   TestMsgResult1.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_STATE_VECTOR;
   TestMsgResult1.MsgLength = POWERMASTER_lSerialize_R_WAKEUP_STATE_VECTOR((TestMsgResult1.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), IGNORE_PARAM) + PM_PDU_MSG_CODE_LENGTH;  
	  
   TestMsgResult2.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_EVENT;
   TestMsgResult2.MsgLength = POWERMASTER_lSerialize_R_WAKEUP_EVENT((TestMsgResult2.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), IGNORE_PARAM, IGNORE_PARAM) + PM_PDU_MSG_CODE_LENGTH; 
	  
   TestMsgResult3.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_REASON;
   TestMsgResult3.MsgLength = POWERMASTER_lSerialize_R_WAKEUP_REASON((TestMsgResult3.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), IGNORE_PARAM) + PM_PDU_MSG_CODE_LENGTH; 

   while (mainLoop < mainLoopRun)
   {
     if (( mainLoop % 10 ) == 0)
        printf("PM test %d / %d\n", mainLoop, mainLoopRun);
        
     // send --------------
     comms.SCCSendMsg( ( void * ) TestMsg.MsgBuffer, TestMsg.MsgLength );
     EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
     printf("\nSending Test Msg");
     for(int BufferCount = 0; BufferCount <  TestMsg.MsgLength; BufferCount++)
     {
        printf (" 0x%02X", TestMsg.MsgBuffer[BufferCount]);
     }
     printf("\n");
     printf("\nSent message Length %d \n", TestMsg.MsgLength);
       // rcv -------------------
     loop=0;
     cond=0;   
     do
     {
        printf("\nloop==> %d \n", loop);
        comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );
       /*  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation(); */
		 if ((recvd_msg_size == TestMsgResult1.MsgLength) || (recvd_msg_size == TestMsgResult2.MsgLength) \
		  || (recvd_msg_size == TestMsgResult3.MsgLength))
         {
		    if((TestMsgResult1.MsgBuffer[0] == buffer[0])|| (TestMsgResult2.MsgBuffer[0] == buffer[0]) || (TestMsgResult3.MsgBuffer[0] == buffer[0]))
		    {
               // check for correct msg
			   Received_Responses_Count++;
               printf("\nmsg OK length OK, Received message Length %d \n", recvd_msg_size);
			   printf("\nReceived Msg: \n");
			   for(int byte_count=0; byte_count < recvd_msg_size; byte_count++)
		       {
			      printf (" 0x%02X", buffer[byte_count]);
			   }
			   printf("\n");
			}
				
         } 
         else if ((recvd_msg_size == SPM_SPMS_R_REJECT_LEN) && (POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT  == buffer[0]) && (TestMsg.MsgBuffer[0] == buffer[2]))
         {
            // check for reject 
            cond=1;
            printf("--> msg OK REJECT reason 0x%x \n", buffer[1]);
         }
         else 
         {
            // ignore other msgs
            PRINTF("!!!! msg !OK length %d Result[0x%x,0x%x,0x%x,0x%x]\n", recvd_msg_size, buffer[0],buffer[1],buffer[2],buffer[3]);
         }
	 }while (cond == 0 /* && loop < 20 */&& recvd_msg_size > 0 );
     mainLoop++;
  }
  printf("%d responses received for \n",Received_Responses_Count );
  for(int byte_count =0; byte_count < SPMS_SPM_C_GET_DATA_LEN; byte_count++)
  {
    printf("TestMsg[%d]: 0x%x  ", byte_count, TestMsg.MsgBuffer[byte_count]);
  }
  printf("\n");   
    
}

TEST_P( PowerMaster_CCtrlResetExecution, CtrlResetExecution ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_CTRL_RESET_EXECUTION;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_CTRL_RESET_EXECUTION((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam()) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_CTRL_RESET_EXECUTION;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_CTRL_RESET_EXECUTION((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH)) + PM_PDU_MSG_CODE_LENGTH;  

   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( PowerMaster_CClientAppStateParams, CClientAppState ) {
  int mainLoopRun =10;
  tyMsgInfo TestMsg;
  tyMsgInfo ResponseMsg;   
  TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE;
  TestMsg.MsgLength = POWERMASTER_lSerialize_C_INDICATE_CLIENT_APP_STATE((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam().first, GetParam().second, POWERMASTER_PowerMaster_CatalogueVersion_Major, POWERMASTER_PowerMaster_CatalogueVersion_Minor) + PM_PDU_MSG_CODE_LENGTH; 
  ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE;
  ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_INDICATE_CLIENT_APP_STATE((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), IGNORE_PARAM, IGNORE_PARAM, POWERMASTER_PowerMaster_CatalogueVersion_Major, POWERMASTER_PowerMaster_CatalogueVersion_Minor, SPM_u8Result_OK) + PM_PDU_MSG_CODE_LENGTH;  

  CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);    
} 

 //Testing REQ_CLIENT_BOOT_MODE
TEST_F( PowerMaster, PMTest_Req_Client_Boot_Mode_WithWrongMagics ) {
   int mainLoopRun = 10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_REQ_CLIENT_BOOT_MODE;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_REQ_CLIENT_BOOT_MODE((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u32Magic_Magic1, SPM_u32Magic_Magic2, SPM_u8BootMode_EMMC, SPM_u32Magic_Magic2, SPM_u32Magic_Magic4, SPM_u8NotBootMode_EMMC) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_REJECT((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8RejectReason_INVALID_PARAMETER, TestMsg.MsgBuffer[0]) + PM_PDU_MSG_CODE_LENGTH;  

   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

TEST_F( PowerMaster, PMTest_Req_Client_Boot_Mode_WithWrongBootMode ) {
   int mainLoopRun = 10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_REQ_CLIENT_BOOT_MODE;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_REQ_CLIENT_BOOT_MODE((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u32Magic_Magic1, SPM_u32Magic_Magic2, (SPM_u8BootMode_EMMC + 1), SPM_u32Magic_Magic3, SPM_u32Magic_Magic4, SPM_u8NotBootMode_EMMC) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_REJECT((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8RejectReason_INVALID_PARAMETER, TestMsg.MsgBuffer[0]) + PM_PDU_MSG_CODE_LENGTH;  

   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);    
}

//Test Message WakeUp Event 
//Cannot use the Serialize functions as we cannot give wrong message lengths with those
/* TEST_P( PowerMaster_CWakeUpEvent, WakeupEventAck_WrongParamLength ) {  
   int mainLoopRun =10;
   uint8 TestMsg[] = { SPMS_SPM_C_WAKEUP_EVENT_ACK, GetParam()};
   uint8 TestMsgResult[] = { POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT, SPM_u8RejectReason_INVALID_PARAMETER, TestMsg[0]};
   
   CheckResponseMessage(mainLoopRun , TestMsg, TestMsgResult, SPMS_SPM_C_WAKEUP_EVENT_ACK_LEN, SPM_SPMS_R_WAKEUP_EVENT_ACK_LEN);
} */

//Test Msg WakeUp State 
TEST_F( PowerMaster, WakeupStateAck_CorrectMessageParams ) {
  int mainLoopRun =10;
  tyMsgInfo TestMsg;
  tyMsgInfo ResponseMsg;   
  TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_WAKEUP_STATE_ACK;
  TestMsg.MsgLength = POWERMASTER_lSerialize_C_WAKEUP_STATE_ACK((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), 0x01) + PM_PDU_MSG_CODE_LENGTH; 
  ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_STATE_ACK;
  ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_WAKEUP_STATE_ACK((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), IGNORE_PARAM) + PM_PDU_MSG_CODE_LENGTH;  

  CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
   
}

//Send with Wrong params and expect a Reject
 TEST_F( PowerMaster, WakeupStateAck_WrongParamLength ) {
  
  int mainLoopRun =10;
  uint8 TestMsg[] = { POWERMASTER_u8MsgCode_SPMS_SPM_C_WAKEUP_STATE_ACK, 0x00, 0x00,0x00};

  uint8 TestMsgResult[] = {POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT, SPM_u8RejectReason_INVALID_PARAMETER, POWERMASTER_u8MsgCode_SPMS_SPM_C_WAKEUP_STATE_ACK};
  size_t recvd_msg_size;
   
  CheckResponseMessage(mainLoopRun , TestMsg, TestMsgResult, (uint16)(SPMS_SPM_C_WAKEUP_STATE_ACK_LEN +1), (uint16)SPM_SPMS_R_REJECT_LEN);   
}

//Testing if the Client App state gives Response as Catalogue mismatch when unmatched version nos are sent from IMX
TEST_F( PowerMaster, PMTest_ClientAppState_WithWrongVersionInfo ) {
  int mainLoopRun =10;
  tyMsgInfo TestMsg;
  tyMsgInfo ResponseMsg;   
  TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE;
  TestMsg.MsgLength = POWERMASTER_lSerialize_C_INDICATE_CLIENT_APP_STATE((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8ApplicationMode_STATE_NORMAL, SPM_u8ResetReasonCPU_RESET_REASON_CPU_INTENDED, 0x03, 0xA) + PM_PDU_MSG_CODE_LENGTH; 
  ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE;
  ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_INDICATE_CLIENT_APP_STATE((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), IGNORE_PARAM, IGNORE_PARAM, POWERMASTER_PowerMaster_CatalogueVersion_Major, POWERMASTER_PowerMaster_CatalogueVersion_Minor, SPM_u8Result_CATALOGUE_VERSION_MISMATCH) + PM_PDU_MSG_CODE_LENGTH;  

  CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
} 

//Disabling until Test cases can be resumed after reset 
//Testing ProcMessage
/* TEST_F( PowerMaster, PMTest_ForTestingStartUpMessagesAfterOneImxReset_TriggerImxReset ) {
   int mainLoopRun =10;
   int ret;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_PROC_RESET_REQUEST;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_PROC_RESET_REQUEST((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8ProcID_PROC_CPU, 0x0A) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_PROC_RESET_REQUEST;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_PROC_RESET_REQUEST((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8ProcID_PROC_CPU) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
   ret = sched_yield();
   sync();   

   sleep(2);

}  */

//Disabling until Test cases can be resumed after reset
//Testing if the v850 sends all the start up messages with the right parameters after an IMX reset
//Also testing ProcMessage

/* TEST_F(PowerMaster, PMTest_StartUpInfoMessagesAfterOneImxReset_AfterImxReset ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_INDICATE_CLIENT_APP_STATE((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8ApplicationMode_STATE_NORMAL, SPM_u8ResetReasonCPU_RESET_REASON_CPU_INTENDED, POWERMASTER_PowerMaster_CatalogueVersion_Major, POWERMASTER_PowerMaster_CatalogueVersion_Minor) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_STARTUP_INFO;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_STARTUP_INFO((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8StartType_RESTART_CPU, SPM_u8DisconnectInfo_BATTERY_DISCONNECT) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);      
}  */

//To check with Debug Wdg pin enabled
//Testing for the Startup Finished message 
TEST_F( PowerMaster, PMTest_StartUpFinishedMessage ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_STARTUP_FINISHED;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_STARTUP_FINISHED((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH)) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_STARTUP_FINISHED;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_STARTUP_FINISHED((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH)) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
}

//Disabling until Utests can run after reset successfully
//Testing C_Shutdown_In_Progress
/* TEST_F(PowerMaster, PMTest_ShutdownMessage ) {
   int mainLoopRun =10;
   int ret;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_SHUTDOWN_IN_PROGRESS;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_SHUTDOWN_IN_PROGRESS((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH)) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_SHUTDOWN_IN_PROGRESS;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_SHUTDOWN_IN_PROGRESS((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH)) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
   
   ret = sched_yield();
   sync();   

   sleep(2);

} */

//Testing SPMS_SPM_C_SET_WAKEUP_CONFIG - using values from PowerMaster_CWakeUpStateAck class
TEST_P( PowerMaster_CWakeUpStateAck, WakeUpConfig ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_SET_WAKEUP_CONFIG;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_SET_WAKEUP_CONFIG((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam()) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_SET_WAKEUP_CONFIG;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_SET_WAKEUP_CONFIG((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam()) + PM_PDU_MSG_CODE_LENGTH;  
   
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

//Testing if not sending the StartUpFinished Message leads to a reset of IMX
/* TEST_F( PowerMaster, PMTest_StartUpFinishedMessage_NotSent ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_INDICATE_CLIENT_APP_STATE((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), STATE_NORMAL, SPM_u8ResetReasonCPU_RESET_REASON_CPU_INTENDED, POWERMASTER_PowerMaster_CatalogueVersion_Major, POWERMASTER_PowerMaster_CatalogueVersion_Minor) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_INDICATE_CLIENT_APP_STATE((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), IGNORE_PARAM, IGNORE_PARAM, POWERMASTER_PowerMaster_CatalogueVersion_Major, POWERMASTER_PowerMaster_CatalogueVersion_Minor, OK) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
  
  //Do not send StartupFinished message and check the Client App State Response to check if there has been a reset of IMX
   sleep(100);
}
 */

//Testing if sending an unknown message gives reject
TEST_F( PowerMaster, PMTest_SendUnknownMsgID ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = (POWERMASTER_u8MsgCode_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE+60);
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_INDICATE_CLIENT_APP_STATE((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8ApplicationMode_STATE_NORMAL, SPM_u8ResetReasonCPU_RESET_REASON_CPU_INTENDED, POWERMASTER_PowerMaster_CatalogueVersion_Major, POWERMASTER_PowerMaster_CatalogueVersion_Minor) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_REJECT((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), SPM_u8RejectReason_UNKNOWN_MESSAGE, TestMsg.MsgBuffer[0]) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  	  
} 

//For testing EXTEND_POWER_OFF_TIMEOUT message when wakeup is still Active
TEST_P( PowerMaster_ParamTypeInteger, PowerOffTimeouts_WakeupStillActive ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_EXTEND_POWER_OFF_TIMEOUT;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_EXTEND_POWER_OFF_TIMEOUT((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam()) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_EXTEND_POWER_OFF_TIMEOUT((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), 0x00) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
}

//For testing EXTEND_POWER_OFF_TIMEOUT message
/* TEST_P( PowerMaster_ParamTypeInteger, PowerOffTimeouts_NewTimeoutsGreaterThanOld ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_EXTEND_POWER_OFF_TIMEOUT;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_EXTEND_POWER_OFF_TIMEOUT((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam()) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_EXTEND_POWER_OFF_TIMEOUT((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam()) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
} */

//For testing EXTEND_POWER_OFF_TIMEOUT message
/* TEST_P( PowerMaster_ParamTypeInteger, PowerOffTimeouts_NewTimeoutSmallerThanOld ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_EXTEND_POWER_OFF_TIMEOUT;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_EXTEND_POWER_OFF_TIMEOUT((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam()-1) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_EXTEND_POWER_OFF_TIMEOUT((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), (GetParam())) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);  
}
 */
// TO DO
/* //For testing EXTEND_POWER_OFF_TIMEOUT message - sum of extend timings > Max limit
TEST_P( PowerMaster_ParamTypeInteger, PowerOffTimeouts_OverallExtendTimeGreaterThanMaxTimeout ) {
   int mainLoopRun =10;
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;   
   TestMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPMS_SPM_C_EXTEND_POWER_OFF_TIMEOUT;
   TestMsg.MsgLength = POWERMASTER_lSerialize_C_EXTEND_POWER_OFF_TIMEOUT((TestMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), GetParam()) + PM_PDU_MSG_CODE_LENGTH; 
   ResponseMsg.MsgBuffer[0] = POWERMASTER_u8MsgCode_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT;
   ResponseMsg.MsgLength = POWERMASTER_lSerialize_R_EXTEND_POWER_OFF_TIMEOUT((ResponseMsg.MsgBuffer + PM_PDU_MSG_CODE_LENGTH), (GetParam()-1)) + PM_PDU_MSG_CODE_LENGTH;  
  
   CheckResponseMessage(mainLoopRun, TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
  
} */







