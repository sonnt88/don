#if !defined( _SUPPLYMGMT_MSGHANDLER_H )
   #define _SUPPLYMGMT_MSGHANDLER_H


/*
  ###################################################
  ####### GENERATED CODE - DO NOT TOUCH #############
  ###################################################
  */


/*
   ===================================================
   GeneratorVersion : Script_1.4
   Generated On     : 10/29/20132:08:29 PM
   Description      :
   ===================================================
  */

#include "Std_MsgHelper.h"

/*
   <  >  PDU_SPM_supplymgmt
   < enumeration >  eSMApplicationStatus
  */
   enum eSMApplicationStatus
   {
      SM_ACTIVE   = 0x01,
      SM_INACTIVE = 0x02,
   }; /*eSMApplicationStatus */
/* < enumeration >  eSMPSStates */
   enum eSMPSStates
   {
      SM_STATE_INVALID = 0xFF,
      SM_STATE_GOOD    = 0x01,
      SM_STATE_BAD     = 0x02,
      SM_STATE_POOR    = 0x03,
   }; /*eSMPSStates */
/* < enumeration >  eSMRejReason */
   enum eSMRejReason
   {
      SM_REJ_NO_REASON        = 0x00,
      SM_REJ_UNKNOWN_MESSAGE  = 0x01,
      SM_REJ_INVALID_PARAM    = 0x02,
      SM_REJ_TMP_UNAVAILBLE   = 0x03,
	  SM_REJ_VERSION_MISMATCH = 0x04
   }; /*eSMRejReason */
/* < enumeration >  eSMSupplyDiag */
   enum eSMSupplyDiag
   {
      SM_SUPPLY_DIAG_OK                = 0x00,
      SM_SUPPLY_DIAG_SHORT2GND         = 0x01,
      SM_SUPPLY_DIAG_SHORT2VCC         = 0x02,
      SM_SUPPLY_DIAG_AUTO_POWEROFF_LPW = 0x03,
      SM_SUPPLY_DIAG_OVERTEMP          = 0x04,
   }; /*eSMSupplyDiag */
/* < enumeration >  eSMSupplyIds */
   enum eSMSupplyIds
   {
      SM_SUPPLYID_INVALID    = 0xff,
      SM_SUPPLYID_AUDIO      = 0x00,
      SM_SUPPLYID_EXT1       = 0x01,
      SM_SUPPLYID_EXT2       = 0x02,
      SM_SUPPLYID_DISPLAY    = 0x03,
      SM_SUPPLYID_TV_CARD    = 0x04,
      SM_SUPPLYID_TUNER_CARD = 0x05,
      SM_SUPPLYID_AUD_CARD   = 0x06,
      SM_SUPPLYID_SDARS_CARD = 0x07,
      SM_SUPPLYID_RSE        = 0x08,
   }; /*eSMSupplyIds */
/* < enumeration >  eSMSupplyStates */
   enum eSMSupplyStates
   {
      SM_SUPPLY_STATE_INVALID = 0xff,
      SM_SUPPLY_STATE_OFF     = 0x00,
      SM_SUPPLY_STATE_ON      = 0x01,
   }; /*eSMSupplyStates */

/*
   =================================================================
   Funtion Prototypes
   =================================================================
  */
   Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS( uint8  *pu8Buffer,
                                                                            uint8_t u8msgID,
                                                                            uint8_t u8HostApplicationStatus,
                                                                            uint8_t u8HostApplicationVersion);

   Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS( uint8   *pu8Buffer,
                                                                              uint8_t *pu8msgID,
                                                                              uint8_t *pu8HostApplicationStatus,
                                                                              uint8_t *pu8HostApplicationVersion);

   Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS( uint8  *pu8Buffer,
                                                                            uint8_t u8msgID,
                                                                            uint8_t u8SCCapplicationStatus,
                                                                            uint8_t u8SCCApplicationVersion);

   Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS( uint8   *pu8Buffer,
                                                                              uint8_t *pu8msgID,
                                                                              uint8_t *pu8SCCapplicationStatus,
                                                                              uint8_t *pu8SCCApplicationVersion);

   Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT( uint8  *pu8Buffer,
                                                                  uint8_t u8msgID,
                                                                  uint8_t u8RejectReason,
                                                                  uint8_t u8RejectedMsgID);

   Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT( uint8   *pu8Buffer,
                                                                    uint8_t *pu8msgID,
                                                                    uint8_t *pu8RejectReason,
                                                                    uint8_t *pu8RejectedMsgID);

   Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_GET_STATE( uint8  *pu8Buffer,
                                                                     uint8_t u8msgID);

   Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_GET_STATE( uint8   *pu8Buffer,
                                                                       uint8_t *pu8msgID);

   Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_GET_STATE( uint8  *pu8Buffer,
                                                                     uint8_t u8msgID,
                                                                     uint8_t u8PSState);

   Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_GET_STATE( uint8   *pu8Buffer,
                                                                       uint8_t *pu8msgID,
                                                                       uint8_t *pu8PSState);

   Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_SET_STATE( uint8  *pu8Buffer,
                                                                     uint8_t u8msgID,
                                                                     uint8_t u8SupplyId,
                                                                     uint8_t u8SupplyState);

   Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_C_SET_STATE( uint8   *pu8Buffer,
                                                                       uint8_t *pu8msgID,
                                                                       uint8_t *pu8SupplyId,
                                                                       uint8_t *pu8SupplyState);

   Length_t SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_SET_STATE( uint8  *pu8Buffer,
                                                                     uint8_t u8msgID,
                                                                     uint8_t u8SupplyId,
                                                                     uint8_t u8SupplyState,
                                                                     uint8_t u8SupplyDiag);

   Length_t SUPPLYMGMT_lDeSerialize_SCC_SUPPLY_MANAGEMENT_R_SET_STATE( uint8   *pu8Buffer,
                                                                       uint8_t *pu8msgID,
                                                                       uint8_t *pu8SupplyId,
                                                                       uint8_t *pu8SupplyState,
                                                                       uint8_t *pu8SupplyDiag);

/* Below are redirecting macros for basic types in Std_MsgHelper.h */
   #define SUPPLYMGMT_lDeSerialize_uint8              lDeSerialize_uint8
   #define SUPPLYMGMT_lDeSerialize_uint16             lDeSerialize_uint16
   #define SUPPLYMGMT_lDeSerialize_uint32             lDeSerialize_uint32
   #define SUPPLYMGMT_lSerialize_uint8                lSerialize_uint8
   #define SUPPLYMGMT_lSerialize_uint16               lSerialize_uint16
   #define SUPPLYMGMT_lSerialize_uint32               lSerialize_uint32
   #define SUPPLYMGMT_lSerialize_uint8_Array          lSerialize_uint8_Array
   #define SUPPLYMGMT_lDeSerialize_uint8_Array        lDeSerialize_uint8_Array
   #define SUPPLYMGMT_lDeSerialize_uint16_Array       lDeSerialize_uint16_Array
   #define SUPPLYMGMT_lSerialize_uint16_Array         lSerialize_uint16_Array
   #define SUPPLYMGMT_lDeSerialize_uint32_Array       lDeSerialize_uint32_Array
   #define SUPPLYMGMT_lSerialize_uint32_Array         lSerialize_uint32_Array

   #define SUPPLYMGMT_lDeSerialize_sint8              lDeSerialize_sint8
   #define SUPPLYMGMT_lDeSerialize_sint16             lDeSerialize_sint16
   #define SUPPLYMGMT_lDeSerialize_sint32             lDeSerialize_sint32
   #define SUPPLYMGMT_lSerialize_sint8                lSerialize_sint8
   #define SUPPLYMGMT_lSerialize_sint16               lSerialize_sint16
   #define SUPPLYMGMT_lSerialize_sint32               lSerialize_sint32
   #define SUPPLYMGMT_lSerialize_sint8_Array          lSerialize_sint8_Array
   #define SUPPLYMGMT_lDeSerialize_sint8_Array        lDeSerialize_sint8_Array
   #define SUPPLYMGMT_lDeSerialize_sint16_Array       lDeSerialize_sint16_Array
   #define SUPPLYMGMT_lSerialize_sint16_Array         lSerialize_sint16_Array
   #define SUPPLYMGMT_lDeSerialize_sint32_Array       lDeSerialize_sint32_Array
   #define SUPPLYMGMT_lSerialize_sint32_Array         lSerialize_sint32_Array

#endif /* _SUPPLYMGMT_MSGHANDLER_H */

