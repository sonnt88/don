// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_loopback_test.cpp
// Author:    Alex Garratt
// Date:      11 November 2013
//
// Contains simple loopback test of INC subsystem.  Can also be used as a simple example
// of how the SCC_Test_Library may be used.  See SCC_Test_Library.h
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familiar with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build.pl SCC_unit_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./scc_unit_tests_out.out
//            or from within the google test framework.  See
//            http://hi0vm019.de.bosch.com/wiki/index.php?title=V850_Unit_Test_Cases
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>

namespace {
  SCCComms comms;
  const char TestMsg[] = { 0x01, 0x02, 0x03 };
  const int  TestMsgLen( 3 );
  const unsigned int SCC_Loopback_Service( 50944 );
};

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step One: Establish communications with AutoSAR
TEST( SCCLoopback, TestInit ) {
  comms.SCCInit( SCC_Loopback_Service );
  ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( SCCLoopback, TestWrite ) {
  comms.SCCSendMsg( ( void * ) TestMsg, TestMsgLen );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( SCCLoopback, TestRead ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( TestMsgLen, recvd_msg_size ) << "Loopback Test: "
                                             "Received message is the wrong length";
  EXPECT_EQ( 0x01, buffer[ 0 ] );
  EXPECT_EQ( 0x02, buffer[ 1 ] );
  EXPECT_EQ( 0x03, buffer[ 2 ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Four: Shut the communications link down
TEST( SCCLoopback, TestShutdown ) {
  comms.SCCShutdown();
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
