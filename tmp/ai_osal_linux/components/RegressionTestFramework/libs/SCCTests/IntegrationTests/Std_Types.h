#ifndef _TYPES_H
   #define _TYPES_H

/****************************************************************************
 * Filename          : TYPES.h
 * Module            :
 * Project           :
 * ----------------------------------------------------------------------------
 * Copyright         : 2007 Robert Bosch, Bangalore
 * Author            : Binu M
 ****************************************************************************/

   typedef unsigned char uint8_t;
   typedef signed char sint8_t;
   typedef unsigned short uint16_t;
   typedef signed short sint16_t;
#if OSAL_CONF == OSAL_LINUX
      typedef unsigned int uint32_t;
#else
      typedef unsigned long uint32_t;
#endif
   typedef signed long sint32_t;

   typedef unsigned char uint8;
   typedef signed char sint8;
   typedef unsigned short uint16;
   typedef signed short sint16;
   typedef unsigned long uint32;
   typedef signed long sint32;

/*
   ==========================================================================
   Config types
   ==========================================================================
  */
   typedef unsigned short smctr;
   typedef unsigned short MemorySizeType;

/*
   ==========================================================================
   Custom defines
   ==========================================================================
  */
#ifndef NULL
      #define NULL ( 0 )
#endif

   #define NULL_PTR NULL

#ifndef TRUE
      #define TRUE ( 1 )
#endif

#ifndef FALSE
      #define FALSE ( 0 )
#endif

#endif /* _TYPES */
/** END OF FILE *************************************************************/

