

/* ################################################### */
/* ####### GENERATED CODE - DO NOT TOUCH ############# */
/* ################################################### */


/* =================================================== */
/* GeneratorVersion : Script_2.1 */
/* Generated On     : 06-06-2016 01:46:01 PM */
/* Description      :  */
/* =================================================== */
#include "Std_MsgHelper.h"
#include "spm_SerializeDeserialize_Thermal_Management.h"


/*=================================================================*/
/* Funtions */
/*=================================================================*/
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                           uint8_t u8HostApplicationStatus,
                                                                           uint8_t u8HostAppVersion)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8HostApplicationStatus);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8HostAppVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                             uint8_t * pu8HostApplicationStatus,
                                                                             uint8_t * pu8HostAppVersion)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8HostApplicationStatus);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8HostAppVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_STATE( uint8 * pu8Buffer, uint8_t u8msgID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_STATE */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_GET_STATE( uint8 * pu8Buffer, uint8_t * pu8msgID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_C_GET_STATE */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_TEMP( uint8 * pu8Buffer, uint8_t msgID,
                                                                   uint8_t ModuleID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, ModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_TEMP */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_GET_TEMP( uint8 * pu8Buffer, uint8_t * pmsgID,
                                                                     uint8_t * pModuleID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pmsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_C_GET_TEMP */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                  uint8_t u8TMModuleId,
                                                                  uint8_t u8PercentageFan)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMModuleId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8PercentageFan);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                    uint8_t * pu8TMModuleId,
                                                                    uint8_t * pu8PercentageFan)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMModuleId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8PercentageFan);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                    uint8_t u8TMModuleId,
                                                                    uint8_t u8TMModuleState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMModuleId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMModuleState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                      uint8_t * pu8TMModuleId,
                                                                      uint8_t * pu8TMModuleState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMModuleId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMModuleState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP( uint8 * pu8Buffer, uint8_t msgId,
                                                                   uint8_t ModuleID,
                                                                   sint16_t temperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, msgId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, ModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_sint16_t(pu8Buffer, temperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP( uint8 * pu8Buffer, uint8_t * pmsgId,
                                                                     uint8_t * pModuleID,
                                                                     sint16_t * ptemperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pmsgId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_sint16_t(pu8Buffer, ptemperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                       uint8_t u8TMFanId,
                                                                       uint8_t u8PercentageFan)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMFanId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8PercentageFan);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                         uint8_t * pu8TMFanId,
                                                                         uint8_t * pu8PercentageFan)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMFanId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8PercentageFan);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP( uint8 * pu8Buffer, uint8_t msgId,
                                                                        uint8_t ModuleID,
                                                                        sint16_t temperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, msgId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, ModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_sint16_t(pu8Buffer, temperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP( uint8 * pu8Buffer, uint8_t * pmsgId,
                                                                          uint8_t * pModuleID,
                                                                          sint16_t * ptemperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pmsgId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_sint16_t(pu8Buffer, ptemperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                           uint8_t u8SCCapplicationStatus,
                                                                           uint8_t u8SCCAppVersion)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8SCCapplicationStatus);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8SCCAppVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                             uint8_t * pu8SCCapplicationStatus,
                                                                             uint8_t * pu8SCCAppVersion)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8SCCapplicationStatus);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8SCCAppVersion);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_STATE( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                    uint8_t u8TMState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_STATE */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_GET_STATE( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                      uint8_t * pu8TMState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_GET_STATE */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_TEMP( uint8 * pu8Buffer, uint8_t msgID,
                                                                   uint8_t ModuleID,
                                                                   sint16_t temperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, ModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_sint16_t(pu8Buffer, temperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_TEMP */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_GET_TEMP( uint8 * pu8Buffer, uint8_t * pmsgID,
                                                                     uint8_t * pModuleID,
                                                                     sint16_t * ptemperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pmsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_sint16_t(pu8Buffer, ptemperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_GET_TEMP */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                 uint8_t u8RejectReason,
                                                                 uint8_t u8RejectedMsgID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8RejectReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8RejectedMsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                   uint8_t * pu8RejectReason,
                                                                   uint8_t * pu8RejectedMsgID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8RejectReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8RejectedMsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REQ_FAN( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                  uint8_t u8TMModuleId,
                                                                  uint8_t u8PercentageFan)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMModuleId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8PercentageFan);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_REQ_FAN */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_REQ_FAN( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                    uint8_t * pu8TMModuleId,
                                                                    uint8_t * pu8PercentageFan)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMModuleId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8PercentageFan);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_REQ_FAN */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                    uint8_t u8TMModuleId,
                                                                    uint8_t u8TMModuleState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMModuleId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMModuleState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                      uint8_t * pu8TMModuleId,
                                                                      uint8_t * pu8TMModuleState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMModuleId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMModuleState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP( uint8 * pu8Buffer, uint8_t msgID,
                                                                   uint8_t ModuleID,
                                                                   sint16_t temperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, ModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_sint16_t(pu8Buffer, temperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP( uint8 * pu8Buffer, uint8_t * pmsgID,
                                                                     uint8_t * pModuleID,
                                                                     sint16_t * ptemperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pmsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_sint16_t(pu8Buffer, ptemperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                       uint8_t u8TMFanId,
                                                                       uint8_t u8PercentageFan)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8TMFanId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, u8PercentageFan);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                         uint8_t * pu8TMFanId,
                                                                         uint8_t * pu8PercentageFan)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8TMFanId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pu8PercentageFan);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN */

Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP( uint8 * pu8Buffer, uint8_t msgId,
                                                                        uint8_t ModuleID,
                                                                        sint16_t temperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, msgId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_uint8_t(pu8Buffer, ModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lSerialize_sint16_t(pu8Buffer, temperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP */

Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP( uint8 * pu8Buffer, uint8_t * pmsgId,
                                                                          uint8_t * pModuleID,
                                                                          sint16_t * ptemperature)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pmsgId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_uint8_t(pu8Buffer, pModuleID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = THERMALMGMT_lDeSerialize_sint16_t(pu8Buffer, ptemperature);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP */


