#ifndef _STD_MSGHELPER_H
#define _STD_MSGHELPER_H
/****************************************************************************
* Filename          : Std_MsgHelper.h
*----------------------------------------------------------------------------
* Copyright         : 2007 Robert Bosch, Bangalore
* Author            : Binu M
* Version           : V0.3
****************************************************************************
* History
* V0.3   prm4kor      Added seperate APIs for serialize/deserialize sint8, sin16
*                     sint32
****************************************************************************/

#include "Std_Types.h"

/*==========================================================================*/
/* Config types */
/*==========================================================================*/
typedef uint16   Length_t;

// FIXME (bne2hi): warning : "LITTLE_ENDIAN" redefined [enabled by default]
#ifndef LITTLE_ENDIAN
   #define LITTLE_ENDIAN   1
#endif // #ifndef LITTLE_ENDIAN
// FIXME (bne2hi): warning : "BIG_ENDIAN" redefined [enabled by default]
#ifndef BIG_ENDIAN
   #define BIG_ENDIAN      2
#endif // #ifndef BIG_ENDIAN

#define ENDIAN          LITTLE_ENDIAN

/*==========================================================================*/
/* basic macros */
/*==========================================================================*/
/* the following four macros work with unsigned and signed data types for   */
/* the parameters w=word and lw=long word */
#define GLB_HIBYTE(w)     ( (uint8) (((uint16)(w)) >> 8) )
#define GLB_LOBYTE(w)     ( (uint8) (((uint16)(w)) &  0xFF) )
#define GLB_HIWORD(lw)    ( (uint16) (((uint32)(lw)) >> 16) )
#define GLB_LOWORD(lw)    ( (uint16) (((uint32)(lw)) &  0xFFFF) )

/* the following two macros work with unsigned and signed data types for    */
/* the parameters hib,lob=byte and hiw,low=word */
#define GLB_WORD(hib,lob)   ( (((uint16)(hib)) << 8)  + ((uint16)(lob)) )
#define GLB_DWORD(hiw,low)  ( (((uint32)(hiw)) << 16) + ((uint32)(low)) )

/*==========================================================================*/
/* De/Serialiser for basic data types */
/*==========================================================================*/
__inline Length_t lDeSerialize_uint8(uint8 * pu8Buffer, uint8 * pu8Var)
{
  *pu8Var = *pu8Buffer;
  return(sizeof(uint8));
}

__inline Length_t lDeSerialize_sint8(uint8 * pu8Buffer, sint8 * ps8Var)
{
  *ps8Var = *pu8Buffer;
  return(sizeof(sint8));
}

__inline Length_t lDeSerialize_uint16(uint8 * pu8Buffer, uint16 * pu16Var)
{
  #if (ENDIAN == BIG_ENDIAN)
   *pu16Var = GLB_WORD((*pu8Buffer),(*(pu8Buffer+1)));
  #else
   *pu16Var = GLB_WORD((*(pu8Buffer+1)),(*pu8Buffer));
  #endif
  
  return(sizeof(uint16));
}

// FIXME (bne2hi):  error : invalid conversion from 'sint16* {aka short int*}' to 'uint16* {aka short unsigned int*}'
__inline Length_t lDeSerialize_sint16(uint8 * pu8Buffer, sint16 * ps16Var)
{
  #if (ENDIAN == BIG_ENDIAN)
   *ps16Var = GLB_WORD((*pu8Buffer),(*(pu8Buffer+1)));
  #else
   *ps16Var = GLB_WORD((*(pu8Buffer+1)),(*pu8Buffer));
  #endif

  return(sizeof(sint16));
}

__inline Length_t lDeSerialize_uint32(uint8 * pu8Buffer, uint32 * pu32Var)
{
  #if (ENDIAN == BIG_ENDIAN)
    *pu32Var = GLB_DWORD(GLB_WORD((*pu8Buffer),(*(pu8Buffer+1))),GLB_WORD((*(pu8Buffer+2)),(*(pu8Buffer+3)))) ;
  #else
    *pu32Var = GLB_DWORD(GLB_WORD((*(pu8Buffer+3)),(*(pu8Buffer+2))),GLB_WORD((*(pu8Buffer+1)),(*(pu8Buffer)))) ;
  #endif

  return(sizeof(uint32));
}

__inline Length_t lDeSerialize_sint32(uint8 * pu8Buffer, sint32 * ps32Var)
{
  #if (ENDIAN == BIG_ENDIAN)
    *ps32Var = GLB_DWORD(GLB_WORD((*pu8Buffer),(*(pu8Buffer+1))),GLB_WORD((*(pu8Buffer+2)),(*(pu8Buffer+3)))) ;
  #else
    *ps32Var = GLB_DWORD(GLB_WORD((*(pu8Buffer+3)),(*(pu8Buffer+2))),GLB_WORD((*(pu8Buffer+1)),(*(pu8Buffer)))) ;
  #endif

  return(sizeof(sint32));
}

__inline Length_t lSerialize_uint8(uint8 * pu8Buffer, uint8 u8Var)
{
  *pu8Buffer = u8Var;
  return(sizeof(uint8));
}

__inline Length_t lSerialize_sint8(uint8 * pu8Buffer, sint8 s8Var)
{
  *pu8Buffer = s8Var;
  return(sizeof(sint8));
}

__inline Length_t lSerialize_uint16(uint8 * pu8Buffer, uint16 u16Var)
{
  #if (ENDIAN == BIG_ENDIAN)
    *(pu8Buffer + 0)    = GLB_HIBYTE(u16Var); 
    *(pu8Buffer + 1)    = GLB_LOBYTE(u16Var);
  #else
    *(pu8Buffer + 0)    = GLB_LOBYTE(u16Var);
    *(pu8Buffer + 1)    = GLB_HIBYTE(u16Var); 
  #endif

  return(sizeof(uint16));
}

__inline Length_t lSerialize_sint16(uint8 * pu8Buffer, sint16 s16Var)
{
  #if (ENDIAN == BIG_ENDIAN)
    *(pu8Buffer + 0)    = GLB_HIBYTE(s16Var); 
    *(pu8Buffer + 1)    = GLB_LOBYTE(s16Var);
  #else
    *(pu8Buffer + 0)    = GLB_LOBYTE(s16Var);
    *(pu8Buffer + 1)    = GLB_HIBYTE(s16Var); 
  #endif

  return(sizeof(sint16));
}

__inline Length_t lSerialize_uint32(uint8 * pu8Buffer, uint32 u32Var)
{
  #if (ENDIAN == BIG_ENDIAN)
    *(pu8Buffer + 0) = GLB_HIBYTE(GLB_HIWORD(u32Var));
    *(pu8Buffer + 1) = GLB_LOBYTE(GLB_HIWORD(u32Var));
    *(pu8Buffer + 2) = GLB_HIBYTE(GLB_LOWORD(u32Var));
    *(pu8Buffer + 3) = GLB_LOBYTE(GLB_LOWORD(u32Var));
  #else
    *(pu8Buffer + 0) = GLB_LOBYTE(GLB_LOWORD(u32Var));
    *(pu8Buffer + 1) = GLB_HIBYTE(GLB_LOWORD(u32Var));
    *(pu8Buffer + 2) = GLB_LOBYTE(GLB_HIWORD(u32Var));
    *(pu8Buffer + 3) = GLB_HIBYTE(GLB_HIWORD(u32Var));
  #endif

  return(sizeof(uint32));
}

__inline Length_t lSerialize_sint32(uint8 * pu8Buffer, sint32 s32Var)
{
  #if (ENDIAN == BIG_ENDIAN)
    *(pu8Buffer + 0) = GLB_HIBYTE(GLB_HIWORD(s32Var));
    *(pu8Buffer + 1) = GLB_LOBYTE(GLB_HIWORD(s32Var));
    *(pu8Buffer + 2) = GLB_HIBYTE(GLB_LOWORD(s32Var));
    *(pu8Buffer + 3) = GLB_LOBYTE(GLB_LOWORD(s32Var));
  #else
    *(pu8Buffer + 0) = GLB_LOBYTE(GLB_LOWORD(s32Var));
    *(pu8Buffer + 1) = GLB_HIBYTE(GLB_LOWORD(s32Var));
    *(pu8Buffer + 2) = GLB_LOBYTE(GLB_HIWORD(s32Var));
    *(pu8Buffer + 3) = GLB_HIBYTE(GLB_HIWORD(s32Var));
  #endif

  return(sizeof(sint32));
}

__inline Length_t lSerialize_uint8_Array(uint8 *pu8Destination,
                                             uint8 *pu8Source,
                                             Length_t lNumOfObjects)
{
  Length_t lCounter = lNumOfObjects;
  while (lCounter > (Length_t)0x00)
  {
    *pu8Destination = *pu8Source;
    pu8Destination++;
    pu8Source++;
	lCounter--;
  }
  return( lNumOfObjects * sizeof(uint8) );
}

#define lDeSerialize_uint8_Array(d,s,l)   lSerialize_uint8_Array(d,s,l)

#define lDeSerialize_uint16_Array(d,s,l)  lSerialize_uint8_Array((uint8 *)d, (uint8 *)s, (l * sizeof(uint16)))
#define lSerialize_uint16_Array(d,s,l)    lSerialize_uint8_Array((uint8 *)d, (uint8 *)s, (l * sizeof(uint16)))

#define lDeSerialize_uint32_Array(d,s,l)  lSerialize_uint8_Array((uint8 *)d, (uint8 *)s, (l * sizeof(uint32)))
#define lSerialize_uint32_Array(d,s,l)    lSerialize_uint8_Array((uint8 *)d, (uint8 *)s, (l * sizeof(uint32)))

/* Support for sint<x>  */
// FIXME (bne2hi): error :   initializing argument 2 of 'Length_t lDeSerialize_uint16(uint8*, uint16*)'
//#define lDeSerialize_sint16             lDeSerialize_uint16
#define lSerialize_sint8_Array          lSerialize_uint8_Array   
#define lDeSerialize_sint8_Array        lDeSerialize_uint8_Array 
#define lDeSerialize_sint16_Array       lDeSerialize_uint16_Array
#define lSerialize_sint16_Array         lSerialize_uint16_Array  
#define lDeSerialize_sint32_Array       lDeSerialize_uint32_Array
#define lSerialize_sint32_Array         lSerialize_uint32_Array  	



#endif /* _STD_MSGHELPER_H */
/** END OF FILE *************************************************************/

