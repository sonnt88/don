// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_supply_mangement_tests.cpp
// Author:    W�stefeld
// Date:      19 November 2013
//
// Contains Supply Mangement test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include <string>
#include "spm_SerializeDeserialize_Supply_Management.h"

#define PRINTF //printf

#define SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS        0x20
#define SCC_SUPPLY_MANAGEMENT_C_GET_STATE               0x30
#define SCC_SUPPLY_MANAGEMENT_C_SET_STATE               0x32
#define SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS        0x21
#define SCC_SUPPLY_MANAGEMENT_R_GET_STATE               0x31
#define SCC_SUPPLY_MANAGEMENT_R_SET_STATE               0x33
#define SCC_SUPPLY_MANAGEMENT_R_REJECT                  0x0B

#define SupplyMgmt_CatalogueVersion 01
#define IGNORE_PARAM                                    SM_SUPPLY_STATE_INVALID


namespace {
  SCCComms comms;
}

using namespace std;
typedef pair<int,int> ResponseMsgsType;

typedef struct
{
  uint8 MsgBuffer[10];
  uint16 MsgLength;
}tyMsgInfo;

//Param 1 = Message ; Param 2 = Message Length
const ResponseMsgsType CSetStateResponseMsgs[] = 
{
 make_pair(SM_SUPPLYID_AUDIO     , SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_AUDIO     , SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_AUDIO     , SM_SUPPLY_STATE_INVALID),
 make_pair(SM_SUPPLYID_EXT1      , SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_EXT1      , SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_EXT1      , SM_SUPPLY_STATE_INVALID),
 make_pair(SM_SUPPLYID_EXT2      , SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_EXT2      , SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_EXT2      , SM_SUPPLY_STATE_INVALID),
 make_pair(SM_SUPPLYID_DISPLAY   , SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_DISPLAY   , SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_DISPLAY   , SM_SUPPLY_STATE_INVALID),
 make_pair(SM_SUPPLYID_TV_CARD   , SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_TV_CARD   , SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_TV_CARD   , SM_SUPPLY_STATE_INVALID),
 make_pair(SM_SUPPLYID_TUNER_CARD, SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_TUNER_CARD, SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_TUNER_CARD, SM_SUPPLY_STATE_INVALID),
 make_pair(SM_SUPPLYID_AUD_CARD  , SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_AUD_CARD  , SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_AUD_CARD  , SM_SUPPLY_STATE_INVALID),
 make_pair(SM_SUPPLYID_SDARS_CARD, SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_SDARS_CARD, SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_SDARS_CARD, SM_SUPPLY_STATE_INVALID),
 make_pair(SM_SUPPLYID_RSE       , SM_SUPPLY_STATE_ON),
 make_pair(SM_SUPPLYID_RSE       , SM_SUPPLY_STATE_OFF),
 make_pair(SM_SUPPLYID_RSE       , SM_SUPPLY_STATE_INVALID)
};

uint8 CompStatus = SM_INACTIVE;


// The fixture for testing class ExampleSm.
class SupplyMgmt : public ::testing::Test {
protected:
    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

   virtual void SetUp() 
   {
      comms.SCCInit( 0xC70D );
      ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
	  if(CompStatus != SM_ACTIVE)
	  {
	     tyMsgInfo TestMsg;
         tyMsgInfo ResponseMsg;     
         TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
         ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
      
         ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
		 CompStatus = SM_ACTIVE;
	  }
   }
   virtual void TearDown()
   {
      comms.SCCShutdown();
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   void SendMessage(char* Msg, int MsgLength)
   {
      printf ("             SendMessage");
      for (size_t x=0; x < MsgLength; x++)
      {
         printf (" 0x%02X", Msg[x]);  
      }
      printf (" \n");
      comms.SCCSendMsg( ( void * ) Msg, MsgLength );
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   int ReceiveMsg(char* RecvMsgBuffer, int MsgBufferLength)
   {
      size_t recvd_msg_size = 0x00;
      comms.SCCRecvMsg( RecvMsgBuffer, MsgBufferLength, recvd_msg_size );
      printf ("             RecvMsg length %d \n", recvd_msg_size);
      printf ("                RecvMsg [ ");
      for (size_t ind=0; ind<recvd_msg_size; ++ind)
      {
         printf ("0x%02X ", RecvMsgBuffer[ind]);
      }
	  printf ("] \n");
      return recvd_msg_size;
   }   
   void SendComponentStatus(int ApplicationStatus)
   {
      tyMsgInfo TestMsg;
      tyMsgInfo ResponseMsg;     
      TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, ApplicationStatus, SupplyMgmt_CatalogueVersion);
      ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, ApplicationStatus, SupplyMgmt_CatalogueVersion);
      
      ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
   }
   int ValidateTestResponse(uint8* TestMsg, uint8* ResponseMsg, uint16 TestMsgLength, uint16 ResponseMsgLength)
   {
      int cond = 0;
      int loop;
      size_t ind=0;
      size_t recvd_msg_size;
	  uint16 RcvBufferSize = 1024;
	  uint8 RcvBuffer[RcvBufferSize];
      
      SendMessage((char*)TestMsg,(int)TestMsgLength);
      do
      {
	     recvd_msg_size = ReceiveMsg((char*)RcvBuffer, (int)RcvBufferSize);
         if ((recvd_msg_size >= 1 ) && (ResponseMsg[0]  == RcvBuffer[0]))
         {
            // check for correct msg			
			for( ind=0; ind<recvd_msg_size; ++ind )
            {
			   if(IGNORE_PARAM != ResponseMsg[ ind ])
			   {
			      EXPECT_EQ( ResponseMsg[ ind ], RcvBuffer[ ind ] );
			   }
            }
         } 
         else if ((recvd_msg_size == 3) && (SCC_SUPPLY_MANAGEMENT_R_REJECT  == RcvBuffer[0]) \
		       && (TestMsg[0]  == RcvBuffer[2])  )
         {
            // check for reject 
            cond=1;
            printf("             msg OK REJECT reason 0x%02X \n", RcvBuffer[1]);
         }
         else 
         {
            // ignore other msgs
            PRINTF ("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, ResponseMsg[0],ResponseMsg[1],ResponseMsg[2],ResponseMsg[3]);
         }
      }while ((cond == 0) && (recvd_msg_size > 0));
   }
};

// The fixture for testing class ExampleSm.
class SupplyMgmt_CompStatusInactive : public ::testing::Test {
protected:
    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

   virtual void SetUp() 
   {
      comms.SCCInit( 0xC70D );
      ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
	  if(CompStatus != SM_INACTIVE)
	  {
	     tyMsgInfo TestMsg;
         tyMsgInfo ResponseMsg;     
         TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_INACTIVE, SupplyMgmt_CatalogueVersion);
         ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_INACTIVE, SupplyMgmt_CatalogueVersion);
      
         ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);
		 CompStatus = SM_INACTIVE;
	  }
   }
   virtual void TearDown()
   {
      comms.SCCShutdown();
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   void SendMessage(char* Msg, int MsgLength)
   {
      printf ("             SendMessage");
      for (size_t x=0; x < MsgLength; x++)
      {
         printf (" 0x%02X", Msg[x]);  
      }
      printf (" \n");
      comms.SCCSendMsg( ( void * ) Msg, MsgLength );
      EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
   }
   int ReceiveMsg(char* RecvMsgBuffer, int MsgBufferLength)
   {
      size_t recvd_msg_size = 0x00;
      comms.SCCRecvMsg( RecvMsgBuffer, MsgBufferLength, recvd_msg_size );
      printf ("             RecvMsg length %d \n", recvd_msg_size);
      printf ("                RecvMsg [ ");
      for (size_t ind=0; ind<recvd_msg_size; ++ind)
      {
         printf ("0x%02X ", RecvMsgBuffer[ind]);
      }
	  printf ("] \n");
      return recvd_msg_size;
   }   
   int ValidateTestResponse(uint8* TestMsg, uint8* ResponseMsg, uint16 TestMsgLength, uint16 ResponseMsgLength)
   {
      int cond = 0;
      int loop;
      size_t ind=0;
      size_t recvd_msg_size;
	  uint16 RcvBufferSize = 1024;
	  uint8 RcvBuffer[RcvBufferSize];
      
      SendMessage((char*)TestMsg,(int)TestMsgLength);
      do
      {
	     recvd_msg_size = ReceiveMsg((char*)RcvBuffer, (int)RcvBufferSize);
         if ((recvd_msg_size >= 1 ) && (ResponseMsg[0]  == RcvBuffer[0]))
         {
            // check for correct msg			
			for( ind=0; ind<recvd_msg_size; ++ind )
            {
			   if(IGNORE_PARAM != ResponseMsg[ ind ])
			   {
			      EXPECT_EQ( ResponseMsg[ ind ], RcvBuffer[ ind ] );
			   }
            }
         } 
         else if ((recvd_msg_size == 3) && (SCC_SUPPLY_MANAGEMENT_R_REJECT  == RcvBuffer[0]) \
		       && (TestMsg[0]  == RcvBuffer[2])  )
         {
            // check for reject 
            cond=1;
            printf("             msg OK REJECT reason 0x%02X \n", RcvBuffer[1]);
         }
         else 
         {
            // ignore other msgs
            PRINTF ("!!!! msg !OK length %d Result[%d,%d,%d,%d]\n", recvd_msg_size, ResponseMsg[0],ResponseMsg[1],ResponseMsg[2],ResponseMsg[3]);
         }
      }while ((cond == 0) && (recvd_msg_size > 0));
   }
};

class SupplyMgmt_DeviceIdParams : public SupplyMgmt,
                                   public ::testing::WithParamInterface<ResponseMsgsType>
{
   // this is the base class for the parameterised tests
};

class SupplyMgmt_DeviceIdParams_CompStatusInactive : public SupplyMgmt_CompStatusInactive,
                                   public ::testing::WithParamInterface<ResponseMsgsType>
{
   // this is the base class for the parameterised tests
};
/* SM_SUPPLYID_AUDIO - disabled while no response */
INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SupplyMgmt_DeviceIdParams,
                        ::testing::ValuesIn(CSetStateResponseMsgs));

						/* SM_SUPPLYID_AUDIO - disabled while no response */
INSTANTIATE_TEST_CASE_P(TestedDeviceIds,
                        SupplyMgmt_DeviceIdParams_CompStatusInactive,
                        ::testing::ValuesIn(CSetStateResponseMsgs));
//////////////////////////////////////////////////////////////////////////////////////////
//       CStatus
//       /opt/bosch/base/bin/inc_send_out.out -p 50957 -b 50957 -r scc 20-01-01
//////////////////////////////////////////////////////////////////////////////////////////

//CStatus with Host application Active
TEST_F( SupplyMgmt, CStatus_Active ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

//////////////////////////////////////////////////////////////////////////////////////////
//       CStatus
//       /opt/bosch/base/bin/inc_send_out.out -p 50957 -b 50957 -r scc 20-02-01
//////////////////////////////////////////////////////////////////////////////////////////
//CStatus with Application Status Inactive
TEST_F( SupplyMgmt, CStatus_Inactive ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_INACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_INACTIVE, SupplyMgmt_CatalogueVersion);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_F( SupplyMgmt, CStatus_Active_Inactive ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
   
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_INACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_INACTIVE, SupplyMgmt_CatalogueVersion);
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_F( SupplyMgmt, CStatus_Active_Inactive_Active ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
   
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_INACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_INACTIVE, SupplyMgmt_CatalogueVersion);
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
   
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_COMPONENT_STATUS, SM_ACTIVE, SupplyMgmt_CatalogueVersion);
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength); 
}

TEST_F( SupplyMgmt, SendUnknownMessage ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, (SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS + 0x50), SM_INACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_REJECT, SM_REJ_UNKNOWN_MESSAGE, SCC_SUPPLY_MANAGEMENT_C_GET_STATE);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_F( SupplyMgmt, VersionMismatch ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS , SM_INACTIVE, (SupplyMgmt_CatalogueVersion + 0x10));
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_REJECT, SM_REJ_VERSION_MISMATCH, SCC_SUPPLY_MANAGEMENT_C_GET_STATE);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}
//////////////////////////////////////////////////////////////////////////////////////////
//       CGetState
//       /opt/bosch/base/bin/inc_send_out.out -p 50957 -b 50957 -r scc 30
//////////////////////////////////////////////////////////////////////////////////////////
TEST_F( SupplyMgmt, CGetState ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_GET_STATE(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_GET_STATE);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_GET_STATE(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_GET_STATE, SM_STATE_GOOD);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);         
       
}

//////////////////////////////////////////////////////////////////////////////////////////
//       CSetState
//       /opt/bosch/base/bin/inc_send_out.out -p 50957 -b 50957 -r scc 32-01-01
//////////////////////////////////////////////////////////////////////////////////////////
TEST_P( SupplyMgmt_DeviceIdParams, CSetState ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_SET_STATE, GetParam().first , GetParam().second );
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_SET_STATE(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_SET_STATE, GetParam().first , GetParam().second , SM_SUPPLY_DIAG_OK);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   

}

TEST_F( SupplyMgmt_CompStatusInactive, CGetState ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_GET_STATE(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_GET_STATE);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_REJECT, SM_REJ_UNKNOWN_MESSAGE, SCC_SUPPLY_MANAGEMENT_C_GET_STATE);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);         
       
}

TEST_F( SupplyMgmt_CompStatusInactive, SendUnknownMessage ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS(TestMsg.MsgBuffer, (SCC_SUPPLY_MANAGEMENT_C_COMPONENT_STATUS + 0x50), SM_INACTIVE, SupplyMgmt_CatalogueVersion);
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_REJECT, SM_REJ_UNKNOWN_MESSAGE, SCC_SUPPLY_MANAGEMENT_C_GET_STATE);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}

TEST_P( SupplyMgmt_DeviceIdParams_CompStatusInactive, CSetState ) {
   tyMsgInfo TestMsg;
   tyMsgInfo ResponseMsg;     
   TestMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_C_SET_STATE(TestMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_C_SET_STATE, GetParam().first , GetParam().second );
   ResponseMsg.MsgLength = SUPPLYMGMT_lSerialize_SCC_SUPPLY_MANAGEMENT_R_REJECT(ResponseMsg.MsgBuffer, SCC_SUPPLY_MANAGEMENT_R_REJECT, SM_REJ_UNKNOWN_MESSAGE, SCC_SUPPLY_MANAGEMENT_C_SET_STATE);
  
   ValidateTestResponse(TestMsg.MsgBuffer, ResponseMsg.MsgBuffer, TestMsg.MsgLength, ResponseMsg.MsgLength);   
}


