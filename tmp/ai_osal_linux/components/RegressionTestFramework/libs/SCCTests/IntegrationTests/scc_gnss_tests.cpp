// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_gnss_tests.cpp
// Author:    W�stefeld
// Date:      19 November 2013
//
// Contains GNSS test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include <string>

namespace {
  SCCComms comms;
}  


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step One: Establish communications with AutoSAR  PORT_EXTENDER_GPIO
TEST( GNSS, TestInit) {

  comms.SCCInit( 0xC708 );
  ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( GNSS, CStatus ) {
  char TestMsg[] = { 0x20, 0x01, 0x01 };

  comms.SCCSendMsg( ( void * ) TestMsg, 3 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( GNSS, RStatus ) {
  char buffer[ 1024 ];
  char TestMsg[] = { 0x21, 0x01, 0x01 };
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( TestMsg[ ind ], buffer[ ind ] );
}



// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Four: Shut the communications link down
TEST( GNSS, TestShutdown) {

  comms.SCCShutdown();
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

