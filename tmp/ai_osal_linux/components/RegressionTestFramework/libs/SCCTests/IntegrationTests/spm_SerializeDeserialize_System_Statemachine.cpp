

/* ################################################### */
/* ####### GENERATED CODE - DO NOT TOUCH ############# */
/* ################################################### */


/* =================================================== */
/* GeneratorVersion : Script_2.1 */
/* Generated On     : 7/8/2016 11:10:18 AM */
/* Description      :  */
/* =================================================== */
#include "Std_MsgHelper.h"
#include "spm_SerializeDeserialize_System_Statemachine.h"


/*=================================================================*/
/* Funtions */
/*=================================================================*/
Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                         uint8_t u8HostApplStat,
                                                                         uint8_t u8HostApplVer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8HostApplStat);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8HostApplVer);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                           uint8_t * pu8HostApplStat,
                                                                           uint8_t * pu8HostApplVer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8HostApplStat);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8HostApplVer);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_COMPONENT_STATUS */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                             uint8_t u8AppSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8AppSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                               uint8_t * pu8AppSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8AppSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SEND_APP_SYSTEMSTATE */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                            uint8_t u8SccSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8SccSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                              uint8_t * pu8SccSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8SccSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SCC_SYSTEMSTATE */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                       uint8_t u8StateMachineTrigger,
                                                                       uint8_t u8TriggerState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8StateMachineTrigger);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8TriggerState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                         uint8_t * pu8StateMachineTrigger,
                                                                         uint8_t * pu8TriggerState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8StateMachineTrigger);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8TriggerState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_SM_TRIGGER */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                              uint8 u8StateMachineType)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8(pu8Buffer, u8StateMachineType);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                                uint8 * pu8StateMachineType)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8(pu8Buffer, pu8StateMachineType);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_C_SET_STATEMACHINE_TYPE */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgId,
                                                                        uint8_t u8AppSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8AppSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_R_APP_SYSTEMSTATE */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgId,
                                                                          uint8_t * pu8AppSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgId);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8AppSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_APP_SYSTEMSTATE */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                         uint8_t u8SCCApplStat,
                                                                         uint8_t u8SCCApplVer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8SCCApplStat);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8SCCApplVer);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                           uint8_t * pu8SCCApplStat,
                                                                           uint8_t * pu8SCCApplVer)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8SCCApplStat);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8SCCApplVer);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_COMPONENT_STATUS */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT( uint8 * pu8Buffer, uint8_t u8msgID,
                                                               uint8_t u8RejectReason,
                                                               uint8_t u8RejectedMsgID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8RejectReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8RejectedMsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                 uint8_t * pu8RejectReason,
                                                                 uint8_t * pu8RejectedMsgID)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8msgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8RejectReason);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8RejectedMsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_REJECT */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                        uint8_t u8SccSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8SccSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_R_SCC_SYSTEMSTATE */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                          uint8_t * pu8SccSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8SccSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SCC_SYSTEMSTATE */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SEND_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                             uint8_t u8SccSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8SccSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_R_SEND_APP_SYSTEMSTATE */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SEND_APP_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                               uint8_t * pu8SccSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8SccSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SEND_APP_SYSTEMSTATE */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                            uint8_t u8SccSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8SccSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SCC_SYSTEMSTATE */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SCC_SYSTEMSTATE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                              uint8_t * pu8SccSystemState)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8SccSystemState);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SCC_SYSTEMSTATE */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                       uint8_t u8NumOfTriggers,
                                                                       uint8_t * pu8StateMachineTrigger_List1, Length_t  lNumOfu8StateMachineTrigger_List1,
                                                                       uint8_t * pu8StateMachineTrigger_List2, Length_t  lNumOfu8StateMachineTrigger_List2,
                                                                       uint8_t * pu8StateMachineTrigger_List3, Length_t  lNumOfu8StateMachineTrigger_List3)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8NumOfTriggers);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t_Array(pu8Buffer, pu8StateMachineTrigger_List1, lNumOfu8StateMachineTrigger_List1/*SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List1_SIZE */);
  lLength    = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List1_SIZE;
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t_Array(pu8Buffer, pu8StateMachineTrigger_List2, lNumOfu8StateMachineTrigger_List2/*SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List2_SIZE */);
  lLength    = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List2_SIZE;
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t_Array(pu8Buffer, pu8StateMachineTrigger_List3, lNumOfu8StateMachineTrigger_List3/*SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List3_SIZE */);
  lLength    = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List3_SIZE;
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                         uint8_t * pu8NumOfTriggers,
                                                                         uint8_t * pu8StateMachineTrigger_List1, Length_t  * plNumOfu8StateMachineTrigger_List1,
                                                                         uint8_t * pu8StateMachineTrigger_List2, Length_t  * plNumOfu8StateMachineTrigger_List2,
                                                                         uint8_t * pu8StateMachineTrigger_List3, Length_t  * plNumOfu8StateMachineTrigger_List3)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8NumOfTriggers);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t_Array(pu8Buffer, pu8StateMachineTrigger_List1, SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List1_SIZE);
  lLength    = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List1_SIZE;

  if ( NULL != plNumOfu8StateMachineTrigger_List1){
    *plNumOfu8StateMachineTrigger_List1 = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List1_SIZE;
  }
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t_Array(pu8Buffer, pu8StateMachineTrigger_List2, SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List2_SIZE);
  lLength    = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List2_SIZE;

  if ( NULL != plNumOfu8StateMachineTrigger_List2){
    *plNumOfu8StateMachineTrigger_List2 = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List2_SIZE;
  }
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t_Array(pu8Buffer, pu8StateMachineTrigger_List3, SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List3_SIZE);
  lLength    = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List3_SIZE;

  if ( NULL != plNumOfu8StateMachineTrigger_List3){
    *plNumOfu8StateMachineTrigger_List3 = SYSTEMSM_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER_u8StateMachineTrigger_List3_SIZE;
  }
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_SM_TRIGGER */

Length_t SYSTEMSM_lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE( uint8 * pu8Buffer, uint8_t u8MsgID,
                                                                              uint8_t u8StateMachineType)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lSerialize_uint8_t(pu8Buffer, u8StateMachineType);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE */

Length_t SYSTEMSM_lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE( uint8 * pu8Buffer, uint8_t * pu8MsgID,
                                                                                uint8_t * pu8StateMachineType)
{
  Length_t lReturn = 0x00;
  Length_t lLength = 0x00;

  if( NULL == pu8Buffer ) {
    return(lReturn);
  }
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8MsgID);
  pu8Buffer += lLength;
  lReturn   += lLength;
  lLength    = SYSTEMSM_lDeSerialize_uint8_t(pu8Buffer, pu8StateMachineType);
  pu8Buffer += lLength;
  lReturn   += lLength;

  (void)pu8Buffer;
  (void)lLength;

  return (lReturn);
} /* lDeSerialize_SCC_SYSTEM_STATEMACHINE_R_SET_STATEMACHINE_TYPE */


