#if !defined(_POWERMASTER_MSGHANDLER_H)
#define _POWERMASTER_MSGHANDLER_H


/* ################################################### */
/* ####### GENERATED CODE - DO NOT TOUCH ############# */
/* ################################################### */


/* =================================================== */
/* GeneratorVersion : Script_2.1 */
/* Generated On     : 14.11.201414:26:42 */
/* Description      :  */
/* =================================================== */

#include "Std_MsgHelper.h"

/* <  >  PDU_SPM_PowerMaster */
#define PM_PDU_MSG_CODE_LENGTH					                           0x01
#define POWERMASTER_MaxValue_ProcResetRequest_u16ResetTime                 1000
#define POWERMASTER_PowerMaster_CatalogueVersion_Major                     2
#define POWERMASTER_PowerMaster_CatalogueVersion_Minor                     10
#define POWERMASTER_PowerMaster_CatalogueVersion_Patch                     2
#define POWERMASTER_MaxValue_WriteErrorMemory_u8BufferLength               58
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_ADD_ERROR_MEMORY_ENTRY            0x63
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_CTRL_RESET_EXECUTION              0x31
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_EXTEND_POWER_OFF_TIMEOUT          0x37
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_INDICATE_CLIENT_APP_STATE         0xC1
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_PROC_RESET_REQUEST                0x33
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_REJECT                            0x21
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_REQ_CLIENT_APP_STATE              0xC5
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_REQ_CLIENT_BOOT_MODE              0xC7
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_SECURITY_VERSION_MISMATCH         0x61
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_SET_WAKEUP_CONFIG                 0x53
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_SHUTDOWN_IN_PROGRESS              0x51
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_STARTUP_FINISHED                  0x35
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_STARTUP_INFO                      0xC3
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_EVENT                      0x4D
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_EVENT_ACK                  0x43
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_REASON                     0x4F
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_STATE                      0x49
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_STATE_ACK                  0x45
#define POWERMASTER_u8MsgCode_SPM_SPMS_R_WAKEUP_STATE_VECTOR               0x47
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_CTRL_RESET_EXECUTION              0x30
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_EXTEND_POWER_OFF_TIMEOUT          0x36
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_GET_DATA                          0x20
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_INDICATE_CLIENT_APP_STATE         0xC0
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_PROC_RESET_REQUEST                0x32
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_REQ_CLIENT_BOOT_MODE              0xC6
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_SET_WAKEUP_CONFIG                 0x52
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_SHUTDOWN_IN_PROGRESS              0x50
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_STARTUP_FINISHED                  0x34
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_WAKEUP_EVENT_ACK                  0x42
#define POWERMASTER_u8MsgCode_SPMS_SPM_C_WAKEUP_STATE_ACK                  0x44
#define POWERMASTER_u8MsgCode_WDG_WDGS_R_WATCHDOG                          0x63
#define POWERMASTER_u8MsgCode_WDGS_WDG_C_WATCHDOG                          0x62
/* < enumeration >  SPM_u8BootMode */
enum SPM_u8BootMode
{
   SPM_u8BootMode_EMMC = 0x00,
   SPM_u8BootMode_USB = 0x01
}; /*SPM_u8BootMode */
/* < enumeration >  SPM_u8NotBootMode */
enum SPM_u8NotBootMode
{
   SPM_u8NotBootMode_EMMC = 0xFF,
   SPM_u8NotBootMode_USB = 0xFE
}; /*SPM_u8NotBootMode */
/* < enumeration >  SPM_u32WupConfig */
enum SPM_u32WupConfig
{
   SPM_u32WupConfig_ON_TIPPER = 0x00,
   SPM_u32WupConfig_CAN = 0x01,
   SPM_u32WupConfig_MOST = 0x02,
   SPM_u32WupConfig_IGN_PIN = 0x03,
   SPM_u32WupConfig_TELPHONE_MUTE = 0x04,
   SPM_u32WupConfig_MAXVALUE = 0x05
}; /*SPM_u32WupConfig */
/* < enumeration >  SPM_u8ApplicationMode */
enum SPM_u8ApplicationMode
{
   SPM_u8ApplicationMode_STATE_NORMAL = 0x00,
   SPM_u8ApplicationMode_STATE_DOWNLOAD = 0x01,
   SPM_u8ApplicationMode_STATE_TESTMANAGER = 0x02,
   SPM_u8ApplicationMode_MAXVALUE = 0x03
}; /*SPM_u8ApplicationMode */
/* < enumeration >  SPM_u8CtrlReset */
enum SPM_u8CtrlReset
{
   SPM_u8CtrlReset_PREVENT_ALL_RESETS = 0x00,
   SPM_u8CtrlReset_PREVENT_COM_WD_RESET = 0x01,
   SPM_u8CtrlReset_MAXVALUE = 0x02
}; /*SPM_u8CtrlReset */
/* < enumeration >  SPM_u8DataMode */
enum SPM_u8DataMode
{
   SPM_u8DataMode_ALL_DATA = 0x00,
   SPM_u8DataMode_AS_SPECIFIED = 0x10,
   SPM_u8DataMode_MAXVALUE = 0x11
}; /*SPM_u8DataMode */
/* < enumeration >  SPM_u8DisconnectInfo */
enum SPM_u8DisconnectInfo
{
   SPM_u8DisconnectInfo_BATTERY_DISCONNECT = 0x00,
   SPM_u8DisconnectInfo_THEFT_PROTECT = 0x01,
   SPM_u8DisconnectInfo_MAXVALUE = 0x02
}; /*SPM_u8DisconnectInfo */
/* < enumeration >  SPM_u32Magic */
enum SPM_u32Magic
{
   SPM_u32Magic_Magic1 = 0xACACACAC,
   SPM_u32Magic_Magic2 = 0xCACACACA,
   SPM_u32Magic_Magic3 = 0x0F0F0F0F,
   SPM_u32Magic_Magic4 = 0xF0F0F0F0
}; /*SPM_u32Magic */
/* < enumeration >  SPM_u8ProcBoardID */
enum SPM_u8ProcBoardID
{
   SPM_u8ProcBoardID_NONE = 0x00,
   SPM_u8ProcBoardID_ADR = 0x01,
   SPM_u8ProcBoardID_TV = 0x02,
   SPM_u8ProcBoardID_SDARS = 0x03,
   SPM_u8ProcBoardID_MAXVALUE = 0x04
}; /*SPM_u8ProcBoardID */
/* < enumeration >  SPM_u8ProcID */
enum SPM_u8ProcID
{
   SPM_u8ProcID_PROC_BLUETOOTH = 0x01,
   SPM_u8ProcID_PROC_WIRELESS_LAN = 0x02,
   SPM_u8ProcID_PROC_GNSS = 0x03,
   SPM_u8ProcID_PROC_XM = 0x04,
   SPM_u8ProcID_PROC_IPOD = 0x05,
   SPM_u8ProcID_PROC_ADR1 = 0x06,
   SPM_u8ProcID_PROC_ADR2 = 0x07,
   SPM_u8ProcID_PROC_VIDEO = 0x08,
   SPM_u8ProcID_PROC_VCC = 0xFD,
   SPM_u8ProcID_PROC_CPU = 0xFE,
   SPM_u8ProcID_PROC_SYSTEM = 0xFF,
   SPM_u8ProcID_MAXVALUE = 0x100
}; /*SPM_u8ProcID */
/* < enumeration >  SPM_u8RejectReason */
enum SPM_u8RejectReason
{
   SPM_u8RejectReason_NO_REASON = 0x00,
   SPM_u8RejectReason_UNKNOWN_MESSAGE = 0x01,
   SPM_u8RejectReason_INVALID_PARAMETER = 0x03,
   SPM_u8RejectReason_SEQUENCE_ERROR = 0x06,
   SPM_u8RejectReason_CATALOGUE_VERSION_MISMATCH = 0x0E,
   SPM_u8RejectReason_MAXVALUE = 0x0F
}; /*SPM_u8RejectReason */
/* < enumeration >  SPM_u8RequestedMsg */
enum SPM_u8RequestedMsg
{
   SPM_u8RequestedMsg_R_WAKEUP_STATE_VECTOR = 0x47,
   SPM_u8RequestedMsg_R_WAKEUP_EVENT = 0x4D,
   SPM_u8RequestedMsg_R_WAKEUP_REASON = 0x4F,
   SPM_u8RequestedMsg_R_STARTUP_INFO = 0xC3,
   SPM_u8RequestedMsg_MAXVALUE = 0xC4
}; /*SPM_u8RequestedMsg */
/* < enumeration >  SPM_u8ResetReasonCPU */
enum SPM_u8ResetReasonCPU
{
   SPM_u8ResetReasonCPU_RESET_REASON_CPU_INTENDED = 0x00,
   SPM_u8ResetReasonCPU_RESET_REASON_CPU_EXCEPTIONAL = 0x01
}; /*SPM_u8ResetReasonCPU */
/* < enumeration >  SPM_u8ResetReasonVCC */
enum SPM_u8ResetReasonVCC
{
   SPM_u8ResetReasonVCC_RESET_REASON_VCC_HW_WATCHDOG = 0x00,
   SPM_u8ResetReasonVCC_RESET_REASON_VCC_POR = 0x01,
   SPM_u8ResetReasonVCC_RESET_REASON_VCC_COLDSTART = 0x02,
   SPM_u8ResetReasonVCC_RESET_REASON_VCC_APPMODE_CHANGE = 0x03,
   SPM_u8ResetReasonVCC_RESET_REASON_VCC_LPW = 0x04,
   SPM_u8ResetReasonVCC_RESET_REASON_VCC_PLL_OSZ = 0x05,
   SPM_u8ResetReasonVCC_RESET_REASON_VCC_SW = 0x06,
   SPM_u8ResetReasonVCC_RESET_REASON_VCC_WARMSTART = 0x07,
   SPM_u8ResetReasonVCC_MAXVALUE = 0x08
}; /*SPM_u8ResetReasonVCC */
/* < enumeration >  SPM_u8Result */
enum SPM_u8Result
{
   SPM_u8Result_OK = 0x01,
   SPM_u8Result_CATALOGUE_VERSION_MISMATCH = 0x04,
   SPM_u8Result_MAXVALUE = 0x05
}; /*SPM_u8Result */
/* < enumeration >  SPM_u8StartType */
enum SPM_u8StartType
{
   SPM_u8StartType_COLDSTART = 0x00,
   SPM_u8StartType_RESTART_VCC = 0x01,
   SPM_u8StartType_WARMSTART = 0x02,
   SPM_u8StartType_RESTART_CPU = 0x03
}; /*SPM_u8StartType */
/* < enumeration >  SPM_u8WupEvent */
enum SPM_u8WupEvent
{
   SPM_u8WupEvent_POWER_ON_RESET = 0x00,
   SPM_u8WupEvent_CSD_HOME_PRESSED = 0x01,
   SPM_u8WupEvent_DRIVERS_DOOR_OPENED = 0x02,
   SPM_u8WupEvent_DEBUG_WAKEUP = 0x03,
   SPM_u8WupEvent_RTC_WAKEUP = 0x04,
   SPM_u8WupEvent_ON_TIPPER = 0x05,
   SPM_u8WupEvent_IGNITION_PIN = 0x06,
   SPM_u8WupEvent_CD_INSERT_DETECTED = 0x07,
   SPM_u8WupEvent_CD_EJECT_DETECTED = 0x08,
   SPM_u8WupEvent_MOST_ACTIVE_DETECTED = 0x09,
   SPM_u8WupEvent_MOST_RBD_DETECTED = 0x0A,
   SPM_u8WupEvent_MOST_ECL_STG_DETECTED = 0x0B,
   SPM_u8WupEvent_MOST_UNDEF_DETECTED = 0x0C,
   SPM_u8WupEvent_NO_WAKEUP_EVENT = 0xFF,
   SPM_u8WupEvent_MAXVALUE = 0x100
}; /*SPM_u8WupEvent */
/* < enumeration >  SPM_u8WupReason */
enum SPM_u8WupReason
{
   SPM_u8WupReason_UNKNOWN = 0x00,
   SPM_u8WupReason_POWER_ON_RESET = 0x01,
   SPM_u8WupReason_ON_TIPPER_WAKEUP = 0x02,
   SPM_u8WupReason_CAN_WAKEUP = 0x03,
   SPM_u8WupReason_MOST_WAKEUP = 0x04,
   SPM_u8WupReason_IGN_PN_WAKEUP = 0x05,
   SPM_u8WupReason_TEL_MUTE_WAKEUP = 0x06,
   SPM_u8WupReason_DEBUG_WAKEUP = 0x07,
   SPM_u8WupReason_RTC_WAKEUP = 0x08,
   SPM_u8WupReason_CD_INSERT_DETECTED = 0x09,
   SPM_u8WupReason_CD_EJECT_DETECTED = 0x0A,
   SPM_u8WupReason_RESET = 0xFF,
   SPM_u8WupReason_MAXVALUE = 0x100
}; /*SPM_u8WupReason */
/* < enumeration >  SPM_u8WupSrc */
enum SPM_u8WupSrc
{
   SPM_u8WupSrc_CAN = 0x00,
   SPM_u8WupSrc_FLEXRAY = 0x01,
   SPM_u8WupSrc_MOST = 0x02,
   SPM_u8WupSrc_LIN = 0x03,
   SPM_u8WupSrc_IGN_PIN = 0x04,
   SPM_u8WupSrc_TELEPHONE_MUTE = 0x05,
   SPM_u8WupSrc_MOST_RBD = 0x06,
   SPM_u8WupSrc_MOST_ECL_STG = 0x07,
   SPM_u8WupSrc_MOST_UNDEF = 0x08,
   SPM_u8WupSrc_S_CONTACT = 0x09,
   SPM_u8WupSrc_MAXVALUE = 0x0A
}; /*SPM_u8WupSrc */
/* < enumeration >  SPM_u8WupState */
enum SPM_u8WupState
{
   SPM_u8WupState_WAKEUP_STATE_INACTIVE = 0x00,
   SPM_u8WupState_WAKEUP_STATE_ACTIVE = 0x01,
   SPM_u8WupState_MAXVALUE = 0x02
}; /*SPM_u8WupState */

/*=================================================================*/
/* Funtion Prototypes */
/*=================================================================*/
Length_t POWERMASTER_lSerialize_C_CTRL_RESET_EXECUTION( uint8 * pu8Buffer, uint8 u8CtrlReset);
Length_t POWERMASTER_lDeSerialize_C_CTRL_RESET_EXECUTION( uint8 * pu8Buffer, uint8 * pu8CtrlReset);
Length_t POWERMASTER_lSerialize_C_EXTEND_POWER_OFF_TIMEOUT( uint8 * pu8Buffer, uint16 u16Seconds);
Length_t POWERMASTER_lDeSerialize_C_EXTEND_POWER_OFF_TIMEOUT( uint8 * pu8Buffer, uint16 * pu16Seconds);
Length_t POWERMASTER_lSerialize_C_GET_DATA( uint8 * pu8Buffer, uint8 u8DataMode,
                                            uint8 u8RequestedMsg);
Length_t POWERMASTER_lDeSerialize_C_GET_DATA( uint8 * pu8Buffer, uint8 * pu8DataMode,
                                              uint8 * pu8RequestedMsg);
Length_t POWERMASTER_lSerialize_C_INDICATE_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 u8ApplicationMode,
                                                             uint8 u8ResetReason,
                                                             uint8 u8MajorVersion,
                                                             uint8 u8MinorVersion);
Length_t POWERMASTER_lDeSerialize_C_INDICATE_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 * pu8ApplicationMode,
                                                               uint8 * pu8ResetReason,
                                                               uint8 * pu8MajorVersion,
                                                               uint8 * pu8MinorVersion);
Length_t POWERMASTER_lSerialize_C_PROC_RESET_REQUEST( uint8 * pu8Buffer, uint8 u8ProcID,
                                                      uint16 u16ResetTime);
Length_t POWERMASTER_lDeSerialize_C_PROC_RESET_REQUEST( uint8 * pu8Buffer, uint8 * pu8ProcID,
                                                        uint16 * pu16ResetTime);
Length_t POWERMASTER_lSerialize_C_REQ_CLIENT_BOOT_MODE( uint8 * pu8Buffer, uint32 u32Magic1,
                                                        uint32 u32Magic2,
                                                        uint8 u8BootMode,
                                                        uint32 u32Magic3,
                                                        uint32 u32Magic4,
                                                        uint8 u8NotBootMode);
Length_t POWERMASTER_lDeSerialize_C_REQ_CLIENT_BOOT_MODE( uint8 * pu8Buffer, uint32 * pu32Magic1,
                                                          uint32 * pu32Magic2,
                                                          uint8 * pu8BootMode,
                                                          uint32 * pu32Magic3,
                                                          uint32 * pu32Magic4,
                                                          uint8 * pu8NotBootMode);
Length_t POWERMASTER_lSerialize_C_SET_WAKEUP_CONFIG( uint8 * pu8Buffer, uint32 u32WupConfig);
Length_t POWERMASTER_lDeSerialize_C_SET_WAKEUP_CONFIG( uint8 * pu8Buffer, uint32 * pu32WupConfig);
Length_t POWERMASTER_lSerialize_C_SHUTDOWN_IN_PROGRESS( uint8 * pu8Buffer);
Length_t POWERMASTER_lDeSerialize_C_SHUTDOWN_IN_PROGRESS( uint8 * pu8Buffer);
Length_t POWERMASTER_lSerialize_C_STARTUP_FINISHED( uint8 * pu8Buffer);
Length_t POWERMASTER_lDeSerialize_C_STARTUP_FINISHED( uint8 * pu8Buffer);
Length_t POWERMASTER_lSerialize_C_WAKEUP_EVENT_ACK( uint8 * pu8Buffer, uint8 u8WupEvent,
                                                    uint16 u16MsgHandle);
Length_t POWERMASTER_lDeSerialize_C_WAKEUP_EVENT_ACK( uint8 * pu8Buffer, uint8 * pu8WupEvent,
                                                      uint16 * pu16MsgHandle);
Length_t POWERMASTER_lSerialize_C_WAKEUP_STATE_ACK( uint8 * pu8Buffer, uint16 u16MsgHandle);
Length_t POWERMASTER_lDeSerialize_C_WAKEUP_STATE_ACK( uint8 * pu8Buffer, uint16 * pu16MsgHandle);
Length_t POWERMASTER_lSerialize_C_WATCHDOG( uint8 * pu8Buffer);
Length_t POWERMASTER_lDeSerialize_C_WATCHDOG( uint8 * pu8Buffer);
Length_t POWERMASTER_lSerialize_R_CTRL_RESET_EXECUTION( uint8 * pu8Buffer);
Length_t POWERMASTER_lDeSerialize_R_CTRL_RESET_EXECUTION( uint8 * pu8Buffer);
Length_t POWERMASTER_lSerialize_R_EXTEND_POWER_OFF_TIMEOUT( uint8 * pu8Buffer, uint16 u16Timeout);
Length_t POWERMASTER_lDeSerialize_R_EXTEND_POWER_OFF_TIMEOUT( uint8 * pu8Buffer, uint16 * pu16Timeout);
Length_t POWERMASTER_lSerialize_R_INDICATE_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 u8ApplicationMode,
                                                             uint8 u8ResetReason,
                                                             uint8 u8MajorVersion,
                                                             uint8 u8MinorVersion,
                                                             uint8 u8Result);
Length_t POWERMASTER_lDeSerialize_R_INDICATE_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 * pu8ApplicationMode,
                                                               uint8 * pu8ResetReason,
                                                               uint8 * pu8MajorVersion,
                                                               uint8 * pu8MinorVersion,
                                                               uint8 * pu8Result);
Length_t POWERMASTER_lSerialize_R_PROC_RESET_REQUEST( uint8 * pu8Buffer, uint8 u8ProcID);
Length_t POWERMASTER_lDeSerialize_R_PROC_RESET_REQUEST( uint8 * pu8Buffer, uint8 * pu8ProcID);
Length_t POWERMASTER_lSerialize_R_REJECT( uint8 * pu8Buffer, uint8 u8RejectReason,
                                          uint8 u8MsgCodeRejected);
Length_t POWERMASTER_lDeSerialize_R_REJECT( uint8 * pu8Buffer, uint8 * pu8RejectReason,
                                            uint8 * pu8MsgCodeRejected);
Length_t POWERMASTER_lSerialize_R_REQ_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 u8ApplicationMode);
Length_t POWERMASTER_lDeSerialize_R_REQ_CLIENT_APP_STATE( uint8 * pu8Buffer, uint8 * pu8ApplicationMode);
Length_t POWERMASTER_lSerialize_R_REQ_CLIENT_BOOT_MODE( uint8 * pu8Buffer, uint8 u8BootMode);
Length_t POWERMASTER_lDeSerialize_R_REQ_CLIENT_BOOT_MODE( uint8 * pu8Buffer, uint8 * pu8BootMode);
Length_t POWERMASTER_lSerialize_R_SECURITY_VERSION_MISMATCH( uint8 * pu8Buffer, uint8 u8ProcBoardID);
Length_t POWERMASTER_lDeSerialize_R_SECURITY_VERSION_MISMATCH( uint8 * pu8Buffer, uint8 * pu8ProcBoardID);
Length_t POWERMASTER_lSerialize_R_SET_WAKEUP_CONFIG( uint8 * pu8Buffer, uint32 u32WupConfig);
Length_t POWERMASTER_lDeSerialize_R_SET_WAKEUP_CONFIG( uint8 * pu8Buffer, uint32 * pu32WupConfig);
Length_t POWERMASTER_lSerialize_R_SHUTDOWN_IN_PROGRESS( uint8 * pu8Buffer);
Length_t POWERMASTER_lDeSerialize_R_SHUTDOWN_IN_PROGRESS( uint8 * pu8Buffer);
Length_t POWERMASTER_lSerialize_R_STARTUP_FINISHED( uint8 * pu8Buffer);
Length_t POWERMASTER_lDeSerialize_R_STARTUP_FINISHED( uint8 * pu8Buffer);
Length_t POWERMASTER_lSerialize_R_STARTUP_INFO( uint8 * pu8Buffer, uint8 u8StartType,
                                                uint8 u8DisconnectInfo);
Length_t POWERMASTER_lDeSerialize_R_STARTUP_INFO( uint8 * pu8Buffer, uint8 * pu8StartType,
                                                  uint8 * pu8DisconnectInfo);
Length_t POWERMASTER_lSerialize_R_WAKEUP_EVENT( uint8 * pu8Buffer, uint8 u8WupEvent,
                                                uint16 u16MsgHandle);
Length_t POWERMASTER_lDeSerialize_R_WAKEUP_EVENT( uint8 * pu8Buffer, uint8 * pu8WupEvent,
                                                  uint16 * pu16MsgHandle);
Length_t POWERMASTER_lSerialize_R_WAKEUP_EVENT_ACK( uint8 * pu8Buffer, uint16 u16MsgHandle);
Length_t POWERMASTER_lDeSerialize_R_WAKEUP_EVENT_ACK( uint8 * pu8Buffer, uint16 * pu16MsgHandle);
Length_t POWERMASTER_lSerialize_R_WAKEUP_STATE_ACK( uint8 * pu8Buffer, uint16 u16MsgHandle);
Length_t POWERMASTER_lDeSerialize_R_WAKEUP_STATE_ACK( uint8 * pu8Buffer, uint16 * pu16MsgHandle);
Length_t POWERMASTER_lSerialize_R_WAKEUP_REASON( uint8 * pu8Buffer, uint8 u8WupReason);
Length_t POWERMASTER_lDeSerialize_R_WAKEUP_REASON( uint8 * pu8Buffer, uint8 * pu8WupReason);
Length_t POWERMASTER_lSerialize_R_WAKEUP_STATE( uint8 * pu8Buffer, uint8 u8WupSrc,
                                                uint8 u8WupState,
                                                uint16 u16MsgHandle);
Length_t POWERMASTER_lDeSerialize_R_WAKEUP_STATE( uint8 * pu8Buffer, uint8 * pu8WupSrc,
                                                  uint8 * pu8WupState,
                                                  uint16 * pu16MsgHandle);
Length_t POWERMASTER_lSerialize_R_WAKEUP_STATE_VECTOR( uint8 * pu8Buffer, uint32 u32WupStates);
Length_t POWERMASTER_lDeSerialize_R_WAKEUP_STATE_VECTOR( uint8 * pu8Buffer, uint32 * pu32WupStates);
Length_t POWERMASTER_lSerialize_R_WATCHDOG( uint8 * pu8Buffer);
Length_t POWERMASTER_lDeSerialize_R_WATCHDOG( uint8 * pu8Buffer);

/* Below are redirecting macros for basic types in Std_MsgHelper.h */
#define POWERMASTER_lDeSerialize_uint8             lDeSerialize_uint8
#define POWERMASTER_lSerialize_uint8               lSerialize_uint8
#define POWERMASTER_lDeSerialize_uint8_Array       lDeSerialize_uint8_Array
#define POWERMASTER_lSerialize_uint8_Array         lSerialize_uint8_Array

#define POWERMASTER_lDeSerialize_uint16             lDeSerialize_uint16
#define POWERMASTER_lSerialize_uint16               lSerialize_uint16
#define POWERMASTER_lDeSerialize_uint16_Array       lDeSerialize_uint16_Array
#define POWERMASTER_lSerialize_uint16_Array         lSerialize_uint16_Array

#define POWERMASTER_lDeSerialize_uint32             lDeSerialize_uint32
#define POWERMASTER_lSerialize_uint32               lSerialize_uint32
#define POWERMASTER_lDeSerialize_uint32_Array       lDeSerialize_uint32_Array
#define POWERMASTER_lSerialize_uint32_Array         lSerialize_uint32_Array


#endif /* _POWERMASTER_MSGHANDLER_H */
