// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// File:      scc_rtc_tests.cpp
// Author:    W�stefeld
// Date:      19 November 2013
//
// Contains RTC test of INC subsystem.  
//
// The main() function is provided by the persistant google test code, which is part of
// PersiGTest.  See RegressionTestFramework/libs/PersiGTest
//
// It is assumed that the maintainer of this file is familier with Google Test.  See
// http://code.google.com/p/googletest/wiki/Documentation
//
// Build:     build lib_V850_tests
// Target:    Gen 3, ARM
//
// Usage:     Either run on its own from a terminal window like this:
//            $ ./lib_V850_tests_unit_tests_out.out
//            or from within the google test framework like this:
//            $ ./gtestservice_out.out -t lib_V850_tests_unit_tests_out.out -d /tmp -f
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
#include <SCC_Test_Library.h>
#include <persigtest.h>
#include <string>
#include <iostream>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include <linux/rtc.h>

#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>

#include <errno.h>

int system(const char *command);

namespace {
  SCCComms comms;
  const char TestMsg0[] = { 0x20, 0x01, 0x01 };
  const char TettMsgResp0[] = { 0x21, 0x01, 0x01 };
  const char TestMsg1[] = { 0x30 };
  const char TettMsgResp1[] = { 0x31, 0x08, 0x29, 0x00, 0x18, 0x0A, 0x0D };
  const char TestMsg[] = { 0x32, 0x08, 0x29, 0x00, 0x18, 0x0A, 0x0D };
  const char SCC_RTC_R_SET_DATE_TIME[] = { 0x33 };
  const char RejInvalidPara[] = { 0x0B, 0x02, 0x32 };
  
  int fd;
  struct rtc_time rtc_tm;
};



TEST( RTC, RemoveDriver ) {

 sleep(2);

 int ret = system("rmmod rtc-inc");
 
 EXPECT_GT( ret, -1) << strerror( errno );

}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step One: Establish communications with AutoSAR  RTC
TEST( RTC, TestInit ) {

  comms.SCCInit( 0xC728 );
  ASSERT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, CStatus ) {

  comms.SCCSendMsg( ( void * ) TestMsg0, 3 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, RStatus ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "RStatus(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( TettMsgResp0[ ind ], buffer[ ind ] );
}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, CSetTimeDate ) {

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, RSetTimeDate ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, CGetTimeDate ) {

  comms.SCCSendMsg( ( void * ) TestMsg1, 1 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}



// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, RGetTimeDate ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 7, recvd_msg_size ) << "RGetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( TettMsgResp1[ ind ], buffer[ ind ] );
}



// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_Invalid_Seconds ) {
  // Set HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 14, 35, 61, 1, 1, 13 };

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, Rej_Invalid_Seconds ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( RejInvalidPara[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_Invalid_Minute ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 14, 60, 00, 1, 1, 13 };

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, Rej_Invalid_Minute ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( RejInvalidPara[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_Invalid_Hour ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 24, 35, 00, 1, 1, 13 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, Rej_Invalid_Hour ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( RejInvalidPara[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_Invalid_Day ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 14, 35, 00, 0, 1, 13 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, Rej_Invalid_Day ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( RejInvalidPara[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_Invalid_Month ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 14, 35, 00, 25, 0, 13 };

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, Rej_Invalid_Month ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( RejInvalidPara[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_Invalid_Year ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 14, 35, 00, 25, 1, 100 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, Rej_Invalid_Year ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( RejInvalidPara[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Two: Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_Invalid_LeapYear ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 14, 35, 00, 29, 2, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, Rej_Invalid_LeapYear ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 3, recvd_msg_size ) << "TestRead1(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( RejInvalidPara[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_0_Hour ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 0, 35, 0, 28, 2, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_0_Hour_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_23_Hour ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 23, 35, 0, 28, 2, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_23_Hour_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_0_Min ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 1,  0, 0, 28, 2, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_0_Min_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_59_Min ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 23,  59, 0, 28, 2, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_59_Min_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_0_Sec ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 1,  1, 0, 28, 2, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_0_Sec_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_59_Sec ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 23,  59, 59, 28, 2, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_59_Sec_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_1_Day ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 1,  1, 0, 1, 12, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_1_Day_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_31_day ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 23,  59, 59, 31, 12, 12 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_31_Day_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}



// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_1_Month ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 1,  1, 0, 28, 1, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_1_Month_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_12_Month ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 23,  59, 59, 28, 12, 11 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_12_Month_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_0_Year ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 1,  1, 0, 28, 1, 0 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_0_Year_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
//  Try to send test message to AutoSAR
TEST( RTC, SetTimeDate_99_Year ) {
  // HH:MM:SS DD.MM.JJ
  char TestMsg[] = { 0x32, 23,  59, 59, 28, 12, 99 };
  

  comms.SCCSendMsg( ( void * ) TestMsg, 7 );
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}
// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Three: Try to receive the same test message back again
TEST( RTC, SetTimeDate_99_Year_Resp ) {
  char buffer[ 1024 ];
  size_t recvd_msg_size;

  comms.SCCRecvMsg( buffer, 1024, recvd_msg_size );

  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
  EXPECT_EQ( 1, recvd_msg_size ) << "RSetTimeDate(): Received message wrong length";
  for( int ind( 0 ); ind < recvd_msg_size; ++ind )
    EXPECT_EQ( SCC_RTC_R_SET_DATE_TIME[ ind ], buffer[ ind ] );
}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
// Step Shut the communications link down
TEST( RTC, TestShutdown ) {

  comms.SCCShutdown();
  EXPECT_EQ( SCCComms::SUCCESS, comms.SCCGetReturnCode() ) << comms.SCCGetExplanation();
}

