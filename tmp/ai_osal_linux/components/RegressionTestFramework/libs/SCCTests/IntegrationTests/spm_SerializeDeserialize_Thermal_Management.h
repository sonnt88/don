#if !defined(_SPM_SERIALIZEDESERIALIZE_THERMAL_MANAGEMENT_H)
#define _SPM_SERIALIZEDESERIALIZE_THERMAL_MANAGEMENT_H


/* ################################################### */
/* ####### GENERATED CODE - DO NOT TOUCH ############# */
/* ################################################### */


/* =================================================== */
/* GeneratorVersion : Script_2.1 */
/* Generated On     : 06-06-201601:46:01 PM */
/* Description      :  */
/* =================================================== */

#include "Std_MsgHelper.h"

/* <  >  PDU_SPM_thermalmgmt */
#define THERMALMGMT_U8_INC_MSGID_C_COMPONENT_STATUS                        0x20
#define THERMALMGMT_U8_INC_MSGID_C_GET_STATE                               0x30
#define THERMALMGMT_U8_INC_MSGID_C_GET_TEMP                                0x36
#define THERMALMGMT_U8_INC_MSGID_C_REQ_FAN                                 0x34
#define THERMALMGMT_U8_INC_MSGID_C_SET_STATE                               0x32
#define THERMALMGMT_U8_INC_MSGID_C_SET_TEMP                                0x38
#define THERMALMGMT_U8_INC_MSGID_C_SET_TEST_FAN                            0x3C
#define THERMALMGMT_U8_INC_MSGID_C_SET_TEST_TEMP                           0x3A
#define THERMALMGMT_U8_INC_MSGID_R_COMPONENT_STATUS                        0x21
#define THERMALMGMT_U8_INC_MSGID_R_GET_STATE                               0x31
#define THERMALMGMT_U8_INC_MSGID_R_GET_TEMP                                0x37
#define THERMALMGMT_U8_INC_MSGID_R_REJECT                                  0x0B
#define THERMALMGMT_U8_INC_MSGID_R_REQ_FAN                                 0x35
#define THERMALMGMT_U8_INC_MSGID_R_SET_STATE                               0x33
#define THERMALMGMT_U8_INC_MSGID_R_SET_TEMP                                0x39
#define THERMALMGMT_U8_INC_MSGID_R_SET_TEST_FAN                            0x3D
#define THERMALMGMT_U8_INC_MSGID_R_SET_TEST_TEMP                           0x3B
#define THERMALMGMT_U8_INC_MSGLEN_C_COMPONENT_STATUS                       0x03
#define THERMALMGMT_U8_INC_MSGLEN_C_GET_STATE                              0x01
#define THERMALMGMT_U8_INC_MSGLEN_C_GET_TEMP                               0x02
#define THERMALMGMT_U8_INC_MSGLEN_C_REQ_FAN                                0x03
#define THERMALMGMT_U8_INC_MSGLEN_C_SET_STATE                              0x03
#define THERMALMGMT_U8_INC_MSGLEN_C_SET_TEMP                               0x04
#define THERMALMGMT_U8_INC_MSGLEN_C_SET_TEST_FAN                           0x03
#define THERMALMGMT_U8_INC_MSGLEN_C_SET_TEST_TEMP                          0x04
#define THERMALMGMT_U8_INC_MSGLEN_R_COMPONENT_STATUS                       0x03
#define THERMALMGMT_U8_INC_MSGLEN_R_GET_STATE                              0x02
#define THERMALMGMT_U8_INC_MSGLEN_R_GET_TEMP                               0x04
#define THERMALMGMT_U8_INC_MSGLEN_R_REJECT                                 0x03
#define THERMALMGMT_U8_INC_MSGLEN_R_REQ_FAN                                0x03
#define THERMALMGMT_U8_INC_MSGLEN_R_SET_STATE                              0x03
#define THERMALMGMT_U8_INC_MSGLEN_R_SET_TEMP                               0x04
#define THERMALMGMT_U8_INC_MSGLEN_R_SET_TEST_FAN                           0x03
#define THERMALMGMT_U8_INC_MSGLEN_R_SET_TEST_TEMP                          0x04
/* < enumeration >  eTMApplicationStatus */
enum eTMApplicationStatus
{
   TM_ACTIVE = 0x01,
   TM_INACTIVE = 0x02
}; /*eTMApplicationStatus */
/* < enumeration >  eTMFans */
enum eTMFans
{
   TM_FAN_ALL = 0x00,
   TM_FAN_1 = 0x01,
   TM_FAN_2 = 0x02,
   TM_FAN_3 = 0x03,
   TM_FAN_4 = 0x04
}; /*eTMFans */
/* < enumeration >  eTMGPIOStates */
enum eTMGPIOStates
{
   TM_STATE_LOW = 0x00,
   TM_STATE_HIGH = 0x01
}; /*eTMGPIOStates */
/* < enumeration >  eTMModules */
enum eTMModules
{
   TM_MODULE_IMX_INTEL = 0x01,
   TM_MODULE_BLUERAY_DRIVE = 0x02,
   TM_MODULE_DISPLAY = 0x03,
   TM_MODULE_EMMC = 0x04,
   TM_MODULE_CDDRIVE = 0x05,
   TM_MODULE_DVDDRIVE = 0x06,
   TM_MODULE_GPS = 0x07,
   TM_MODULE_GYRO = 0x08,
   TM_MODULE_AMPHEATSINK = 0x09,
   TM_MODULE_POWER_AMP = 0x0A,
   TM_MODULE_SCC = 0x0B,
   TM_MODULE_AUX_BATT = 0x0C,
   TM_MODULE_PCB_SENSOR_BOTTOM_TEMP = 0x0D,
   TM_MODULE_PCB_SENSOR_TOP_TEMP = 0x0E
}; /*eTMModules */
/* < enumeration >  eTMRejReason */
enum eTMRejReason
{
   TM_REJ_NO_REASON = 0x00,
   TM_REJ_UNKNOWN_MESSAGE = 0x01,
   TM_REJ_INVALID_PARAM = 0x02,
   TM_REJ_TMP_UNAVAILBLE = 0x03,
   TM_REJ_VERSION_MISMATCH = 0x04,
   TM_REJ_NOT_SUPPORTED = 0x05,
   TM_REJ_SEQUENCE_ERROR = 0x06
}; /*eTMRejReason */
/* < enumeration >  eTMStates */
enum eTMStates
{
   TM_STATE_INVALID = 0xFF,
   TM_STATE_GOOD = 0x01,
   TM_STATE_BAD = 0x02,
   TM_STATE_POOR = 0x03
}; /*eTMStates */

/*=================================================================*/
/* Funtion Prototypes */
/*=================================================================*/
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                           uint8_t u8HostApplicationStatus,
                                                                           uint8_t u8HostAppVersion);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                             uint8_t * pu8HostApplicationStatus,
                                                                             uint8_t * pu8HostAppVersion);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_STATE( uint8 * pu8Buffer, uint8_t u8msgID);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_GET_STATE( uint8 * pu8Buffer, uint8_t * pu8msgID);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_GET_TEMP( uint8 * pu8Buffer, uint8_t msgID,
                                                                   uint8_t ModuleID);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_GET_TEMP( uint8 * pu8Buffer, uint8_t * pmsgID,
                                                                     uint8_t * pModuleID);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                  uint8_t u8TMModuleId,
                                                                  uint8_t u8PercentageFan);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_REQ_FAN( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                    uint8_t * pu8TMModuleId,
                                                                    uint8_t * pu8PercentageFan);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                    uint8_t u8TMModuleId,
                                                                    uint8_t u8TMModuleState);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_STATE( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                      uint8_t * pu8TMModuleId,
                                                                      uint8_t * pu8TMModuleState);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP( uint8 * pu8Buffer, uint8_t msgId,
                                                                   uint8_t ModuleID,
                                                                   sint16_t temperature);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEMP( uint8 * pu8Buffer, uint8_t * pmsgId,
                                                                     uint8_t * pModuleID,
                                                                     sint16_t * ptemperature);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                       uint8_t u8TMFanId,
                                                                       uint8_t u8PercentageFan);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_FAN( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                         uint8_t * pu8TMFanId,
                                                                         uint8_t * pu8PercentageFan);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP( uint8 * pu8Buffer, uint8_t msgId,
                                                                        uint8_t ModuleID,
                                                                        sint16_t temperature);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_C_SET_TEST_TEMP( uint8 * pu8Buffer, uint8_t * pmsgId,
                                                                          uint8_t * pModuleID,
                                                                          sint16_t * ptemperature);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                           uint8_t u8SCCapplicationStatus,
                                                                           uint8_t u8SCCAppVersion);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_COMPONENT_STATUS( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                             uint8_t * pu8SCCapplicationStatus,
                                                                             uint8_t * pu8SCCAppVersion);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_STATE( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                    uint8_t u8TMState);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_GET_STATE( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                      uint8_t * pu8TMState);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_GET_TEMP( uint8 * pu8Buffer, uint8_t msgID,
                                                                   uint8_t ModuleID,
                                                                   sint16_t temperature);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_GET_TEMP( uint8 * pu8Buffer, uint8_t * pmsgID,
                                                                     uint8_t * pModuleID,
                                                                     sint16_t * ptemperature);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                 uint8_t u8RejectReason,
                                                                 uint8_t u8RejectedMsgID);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_REJECT( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                   uint8_t * pu8RejectReason,
                                                                   uint8_t * pu8RejectedMsgID);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_REQ_FAN( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                  uint8_t u8TMModuleId,
                                                                  uint8_t u8PercentageFan);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_REQ_FAN( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                    uint8_t * pu8TMModuleId,
                                                                    uint8_t * pu8PercentageFan);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                    uint8_t u8TMModuleId,
                                                                    uint8_t u8TMModuleState);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_STATE( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                      uint8_t * pu8TMModuleId,
                                                                      uint8_t * pu8TMModuleState);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP( uint8 * pu8Buffer, uint8_t msgID,
                                                                   uint8_t ModuleID,
                                                                   sint16_t temperature);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEMP( uint8 * pu8Buffer, uint8_t * pmsgID,
                                                                     uint8_t * pModuleID,
                                                                     sint16_t * ptemperature);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN( uint8 * pu8Buffer, uint8_t u8msgID,
                                                                       uint8_t u8TMFanId,
                                                                       uint8_t u8PercentageFan);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_FAN( uint8 * pu8Buffer, uint8_t * pu8msgID,
                                                                         uint8_t * pu8TMFanId,
                                                                         uint8_t * pu8PercentageFan);
Length_t THERMALMGMT_lSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP( uint8 * pu8Buffer, uint8_t msgId,
                                                                        uint8_t ModuleID,
                                                                        sint16_t temperature);
Length_t THERMALMGMT_lDeSerialize_SCC_THERMAL_MANAGEMENT_R_SET_TEST_TEMP( uint8 * pu8Buffer, uint8_t * pmsgId,
                                                                          uint8_t * pModuleID,
                                                                          sint16_t * ptemperature);

/* Below are redirecting macros for basic types in Std_MsgHelper.h */
#define THERMALMGMT_lDeSerialize_uint8_t             lDeSerialize_uint8
#define THERMALMGMT_lSerialize_uint8_t               lSerialize_uint8
#define THERMALMGMT_lDeSerialize_uint8_t_Array       lDeSerialize_uint8_Array
#define THERMALMGMT_lSerialize_uint8_t_Array         lSerialize_uint8_Array

#define THERMALMGMT_lDeSerialize_sint16_t             lDeSerialize_sint16
#define THERMALMGMT_lSerialize_sint16_t               lSerialize_sint16
#define THERMALMGMT_lDeSerialize_sint16_t_Array       lDeSerialize_sint16_Array
#define THERMALMGMT_lSerialize_sint16_t_Array         lSerialize_sint16_Array


#endif /* _THERMALMGMT_MSGHANDLER_H */
