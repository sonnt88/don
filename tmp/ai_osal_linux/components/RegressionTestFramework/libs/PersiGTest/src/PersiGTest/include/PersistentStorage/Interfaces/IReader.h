/*
 * IReader.h
 *
 *  Created on: Dec 12, 2011
 *      Author: oto4hi
 */

#ifndef IREADER_H_
#define IREADER_H_

#include <PersistentStorage/IOException.h>

class IReader
{
public:
	virtual void* read(unsigned int& Length)
	{
		throw new IOException("Error: try to call IReader::read() (pure interface function).");
	}
	virtual ~IReader() {}
};

#endif /* IREADER_H_ */
