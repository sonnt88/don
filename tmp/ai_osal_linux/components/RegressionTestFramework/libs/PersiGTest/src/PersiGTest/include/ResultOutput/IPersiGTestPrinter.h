/*
 * IPersiGTestPrinter.h
 *
 *  Created on: Feb 13, 2012
 *      Author: oto4hi
 */

#ifndef IPERSIGTESTPRINTER_H_
#define IPERSIGTESTPRINTER_H_

#include <gtest/gtest.h>
#include <PersistentStateModels/FrameworkState/PersistentFrameworkState.h>

namespace testing {
	namespace persistence {
		class IPersiGTestPrinter : public ::testing::EmptyTestEventListener{
		public:
			virtual void OnTestIterationEnd(const ::testing::UnitTest& unit_test, int iteration, ::testing::persistence::PersistentFrameworkState* state) {};
		};
	}
};

#endif /* IPERSIGTESTPRINTER_H_ */
