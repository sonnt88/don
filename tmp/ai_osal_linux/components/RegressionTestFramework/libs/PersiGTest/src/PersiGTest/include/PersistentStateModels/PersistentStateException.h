/*
 * PersistentFrameworkStateException.h
 *
 *  Created on: Dec 8, 2011
 *      Author: oto4hi
 */

#ifndef PERSISTENTFRAMEWORKSTATEEXCEPTION_H_
#define PERSISTENTFRAMEWORKSTATEEXCEPTION_H_

#include <exception>

namespace testing {
	namespace persistence {

		class PersistentStateException : std::exception{
		private:
			const char* _what;
		public:
			PersistentStateException(const char* what);
			virtual const char* what() const throw();
		};

	};
};

#endif /* PERSISTENTFRAMEWORKSTATEEXCEPTION_H_ */
