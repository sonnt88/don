#include "IPersiGTestPrinter.h"

namespace testing {

  namespace internal {
    enum GTestColor {
      COLOR_DEFAULT,
      COLOR_RED,
      COLOR_GREEN,
      COLOR_YELLOW
    };

    extern void ColoredPrintf(GTestColor color, const char* fmt, ...);
    extern void PrintFullTestCommentIfPresent(const TestInfo& test_info);
  }

  namespace persistence {

    class PrettyPersiUnitTestResultPrinter : public ::testing::persistence::IPersiGTestPrinter {
    private:
      TestEventListener* prettyUnitTestResultPrinter;

      void PrintFailedTests(::testing::persistence::PersistentFrameworkState* state) {
        ::testing::UnitTest* unit_test = ::testing::UnitTest::GetInstance();

        const int failed_test_count = state->GetFailedTestCount();
        if (failed_test_count == 0) {
          return;
        }

        for (int i = 0; i < unit_test->total_test_case_count(); ++i) {
          const TestCase* test_case = unit_test->GetTestCase(i);
          int TestCaseIndex = i;

          for (int j = 0; j < test_case->total_test_count(); ++j) {
            const TestInfo& test_info = *test_case->GetTestInfo(j);
            int TestIndex = test_case->test_indices_[j];

            if (state->GetResult(TestCaseIndex, TestIndex) == FAILED)
            {
              ::testing::internal::ColoredPrintf(::testing::internal::COLOR_RED, "[  FAILED  ] ");
              printf("%s.%s", test_case->name(), test_info.name());
              ::testing::internal::PrintFullTestCommentIfPresent(test_info);
              printf("\n");
            }
          }
        }
      }

    public:
      PrettyPersiUnitTestResultPrinter()
      {
        ::testing::TestEventListeners& listeners = ::testing::UnitTest::GetInstance()->listeners();
        prettyUnitTestResultPrinter = listeners.default_result_printer();
      }

      virtual void OnTestProgramStart(const UnitTest& unit_test) 
      {
        prettyUnitTestResultPrinter->OnTestProgramStart(unit_test);
      }

      virtual void OnTestIterationStart(const UnitTest& unit_test, int iteration) 
      {
        prettyUnitTestResultPrinter->OnTestIterationStart(unit_test, iteration);
      }

      virtual void OnEnvironmentsSetUpStart(const UnitTest& unit_test)
      {
        prettyUnitTestResultPrinter->OnEnvironmentsSetUpStart(unit_test);
      }

      virtual void OnEnvironmentsSetUpEnd(const UnitTest& unit_test) 
      {
        prettyUnitTestResultPrinter->OnEnvironmentsSetUpEnd(unit_test);
      }

      virtual void OnTestCaseStart(const TestCase& test_case) 
      {
        prettyUnitTestResultPrinter->OnTestCaseStart(test_case);
      }

      virtual void OnTestStart(const TestInfo& test_info) 
      {
        prettyUnitTestResultPrinter->OnTestStart(test_info);
      }

      virtual void OnTestPartResult(const TestPartResult& test_part_result) 
      {
        prettyUnitTestResultPrinter->OnTestPartResult(test_part_result) ;
      }

      virtual void OnTestEnd(const TestInfo& test_info)
      {
        prettyUnitTestResultPrinter->OnTestEnd(test_info);
      }

      virtual void OnTestCaseEnd(const TestCase& test_case) 
      {
        prettyUnitTestResultPrinter->OnTestCaseEnd(test_case);
      }

      virtual void OnEnvironmentsTearDownStart(const UnitTest& unit_test) 
      {
        prettyUnitTestResultPrinter->OnEnvironmentsTearDownStart(unit_test);
      }

      virtual void OnEnvironmentsTearDownEnd(const UnitTest& unit_test) 
      {
        prettyUnitTestResultPrinter->OnEnvironmentsTearDownEnd(unit_test);
      }

      virtual void OnTestIterationEnd(const UnitTest& unit_test, int iteration) 
      {
      }

      virtual void OnTestIterationEnd(const ::testing::UnitTest& unit_test, int iteration, ::testing::persistence::PersistentFrameworkState* state)
      {
        unsigned int failedTestCount = state->GetFailedTestCount();
			  unsigned int passedTestCount = state->GetPassedTestCount();
			  unsigned int totalTestCount = unit_test.total_test_count();

        ::testing::internal::ColoredPrintf(::testing::internal::COLOR_GREEN,  "[==========] ");
        printf("%d from %d ran.", passedTestCount + failedTestCount, totalTestCount);
        
        if (GTEST_FLAG(print_time)) {
          printf(" (%s ms total)",
            internal::StreamableToString(unit_test.elapsed_time()).c_str());
        }
        
        printf("\n");
        ::testing::internal::ColoredPrintf(::testing::internal::COLOR_GREEN,  "[  PASSED  ] ");
        printf("%d.\n", passedTestCount);

        //int num_failures = unit_test.failed_test_count();
        if (state->GetFailedTestCount() > 0) {
          //const int failed_test_count = unit_test.failed_test_count();
          ::testing::internal::ColoredPrintf(::testing::internal::COLOR_RED,  "[  FAILED  ] ");
          printf("%d, listed below:\n", failedTestCount);
          PrintFailedTests(state);
          //printf("\n%2d FAILED %s\n", num_failures,
          //  num_failures == 1 ? "TEST" : "TESTS");
          
          printf("\n%2d FAILED %s\n", failedTestCount,
            failedTestCount == 1 ? "TEST" : "TESTS");
          
        }
        
        int num_disabled = totalTestCount - (passedTestCount + failedTestCount);
        if (num_disabled && !GTEST_FLAG(also_run_disabled_tests)) {
          //if (!num_failures) {
          if (!failedTestCount) {
            printf("\n");  // Add a spacer if no FAILURE banner is displayed.
          }
          
          ::testing::internal::ColoredPrintf(::testing::internal::COLOR_YELLOW,
            "  YOU HAVE %d DISABLED %s\n\n",
            num_disabled,
            num_disabled == 1 ? "TEST" : "TESTS");
        }
        
        // Ensure that Google Test output is printed before, e.g., heapchecker output.
        fflush(stdout);
      }

      virtual void OnTestProgramEnd(const UnitTest& unit_test) 
      {
        prettyUnitTestResultPrinter->OnTestProgramEnd(unit_test);
      }

    };
  }
}
