/*
 * IPersistentState.h
 *
 *  Created on: Dec 19, 2011
 *      Author: oto4hi
 */

#ifndef IPERSISTENTSTATE_H_
#define IPERSISTENTSTATE_H_

#include <PersistentStateModels/PersistentStateException.h>

namespace testing {
	namespace persistence {

		class IPersistentState {
		public:
			virtual void Persist()
			{
				throw new PersistentStateException("Error: try to call IPersistentState::Persist() (pure interface function).");
			}
			virtual bool Restore()
			{
				throw new PersistentStateException("Error: try to call IPersistentState::Restore() (pure interface function).");
			}
			virtual ~IPersistentState() {}
		};

	}
}

#endif /* IPERSISTENTSTATE_H_ */
