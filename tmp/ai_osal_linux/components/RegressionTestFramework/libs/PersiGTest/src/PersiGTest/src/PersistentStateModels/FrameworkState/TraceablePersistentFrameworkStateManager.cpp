/*
 * TraceablePersistentFrameworkStateManager.cpp
 *
 *  Created on: Dec 16, 2011
 *      Author: oto4hi
 */

#include <PersistentStateModels/FrameworkState/TraceablePersistentFrameworkStateManager.h>

using namespace ::testing;
using namespace ::testing::persistence;

TraceablePersistentFrameworkStateManager::TraceablePersistentFrameworkStateManager(int argc, char** argv, IReader* Reader, IWriter* Writer, IPersiGTestPrinter* Printer) :
		PersistentFrameworkStateManager(argc, argv, Reader, Writer)
{
    this->printer = Printer;
}

TraceablePersistentFrameworkStateManager::~TraceablePersistentFrameworkStateManager()
{
}

void TraceablePersistentFrameworkStateManager::OnTestProgramStart(const UnitTest& unit_test)
{
    PersistentFrameworkStateManager::OnTestProgramStart(unit_test);
    printer->OnTestProgramStart(unit_test);
}

void TraceablePersistentFrameworkStateManager::OnTestIterationStart(const UnitTest& unit_test, int iteration)
{
    PersistentFrameworkStateManager::OnTestIterationStart(unit_test, iteration);
    printer->OnTestIterationStart(unit_test, this->state->GetCurrentIteration());
}

void TraceablePersistentFrameworkStateManager::OnEnvironmentsSetUpStart(const UnitTest& unit_test)
{
    PersistentFrameworkStateManager::OnEnvironmentsSetUpStart(unit_test);
    printer->OnEnvironmentsSetUpStart(unit_test);
}

void TraceablePersistentFrameworkStateManager::OnEnvironmentsSetUpEnd(const UnitTest& unit_test)
{
    PersistentFrameworkStateManager::OnEnvironmentsSetUpEnd(unit_test);
    printer->OnEnvironmentsSetUpEnd(unit_test);
}

void TraceablePersistentFrameworkStateManager::OnTestCaseStart(const TestCase& test_case)
{
    PersistentFrameworkStateManager::OnTestCaseStart(test_case);
    printer->OnTestCaseStart(test_case);
}

void TraceablePersistentFrameworkStateManager::OnTestStart(const TestInfo& test_info)
{
    PersistentFrameworkStateManager::OnTestStart(test_info);
    printer->OnTestStart(test_info);
}

void TraceablePersistentFrameworkStateManager::OnTestPartResult(const TestPartResult& test_part_result)
{
    PersistentFrameworkStateManager::OnTestPartResult(test_part_result);
    printer->OnTestPartResult(test_part_result);
}

void TraceablePersistentFrameworkStateManager::OnTestEnd(const TestInfo& test_info)
{
    PersistentFrameworkStateManager::OnTestEnd(test_info);
    printer->OnTestEnd(test_info);
}

void TraceablePersistentFrameworkStateManager::OnTestCaseEnd(const TestCase& test_case)
{
    PersistentFrameworkStateManager::OnTestCaseEnd(test_case);
    printer->OnTestCaseEnd(test_case);
}

void TraceablePersistentFrameworkStateManager::OnEnvironmentsTearDownStart(const UnitTest& unit_test)
{
    PersistentFrameworkStateManager::OnEnvironmentsTearDownStart(unit_test);
    printer->OnEnvironmentsTearDownStart(unit_test);
}

void TraceablePersistentFrameworkStateManager::OnEnvironmentsTearDownEnd(const UnitTest& unit_test)
{
    PersistentFrameworkStateManager::OnEnvironmentsTearDownEnd(unit_test);
    printer->OnEnvironmentsTearDownEnd(unit_test);
}

void TraceablePersistentFrameworkStateManager::OnTestIterationEnd(const UnitTest& unit_test, int iteration)
{
    PersistentFrameworkStateManager::OnTestIterationEnd(unit_test, iteration);
    printer->OnTestIterationEnd(unit_test, this->state->GetCurrentIteration(), this->state );
}

void TraceablePersistentFrameworkStateManager::OnTestProgramEnd(const UnitTest& unit_test)
{
    PersistentFrameworkStateManager::OnTestProgramEnd(unit_test);
    printer->OnTestProgramEnd(unit_test);
}

