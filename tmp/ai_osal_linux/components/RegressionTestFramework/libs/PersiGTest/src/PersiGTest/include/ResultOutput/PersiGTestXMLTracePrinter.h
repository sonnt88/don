/*
 * PersiGTestXMLTracePrinter.h
 *
 *  Created on: Feb 13, 2012
 *      Author: oto4hi
 */

#ifndef PERSIGTESTXMLTRACEPRINTER_H_
#define PERSIGTESTXMLTRACEPRINTER_H_

#include "traceprintf.h"
#include "IPersiGTestPrinter.h"
#include <stdarg.h>

namespace testing {
	namespace persistence {

		enum INDENT_LEVEL {
			INDENT_LEVEL_TESTRUN,
			INDENT_LEVEL_ITERATION,
			INDENT_LEVEL_TESTCASE,
			INDENT_LEVEL_TEST,
			INDENT_LEVEL_TESTRESULT,
			INDENT_LEVEL_TESTPARTRESULTDETAILS,
			INDENT_LEVEL_SUMMARYDATA
		};

		class PersiGTestXMLTracePrinter : public ::testing::persistence::IPersiGTestPrinter {
			private:
				char* indentString;
				static const int tabWidth = 2;
				int executedTestCount;

				void SetIndentString(int IndentLevel)
				{
					int indent = tabWidth * (int)IndentLevel;

					indentString = (char*) realloc (indentString, indent + 1);
					memset((void*)indentString, (int)' ', indent);
					indentString[indent] = 0;
				}

				void printWithIndent(const char* line, ...)
				{
					va_list argList;
					va_start(argList, line);

					char lineToPrint[1024];
					vsprintf(lineToPrint, line, argList);

					TracePrintf(TR_LEVEL_FATAL, "%s%s", indentString, lineToPrint);

					va_end(argList);
				}

				void BeginRawNode(INDENT_LEVEL IndentLevel, const char* NodeName)
				{
					SetIndentString(IndentLevel);
					printWithIndent("<%s>", NodeName);
					SetIndentString(IndentLevel + 1);
					printWithIndent("<![CDATA[");
				}

				void EndRawNode(INDENT_LEVEL IndentLevel, const char* NodeName)
				{
					SetIndentString(IndentLevel + 1);
					printWithIndent("]]>");
					SetIndentString(IndentLevel);
					printWithIndent("</%s>", NodeName);
				}

			public:
				PersiGTestXMLTracePrinter()
				{
					indentString = NULL;
					SetIndentString(INDENT_LEVEL_TESTRUN);
					executedTestCount = 0;
				}

				~PersiGTestXMLTracePrinter()
				{
					free(indentString);
				}

				virtual void OnTestProgramStart(const UnitTest& unit_test)
				{
					SetIndentString(INDENT_LEVEL_TESTRUN);
					const char* shuffle = GTEST_FLAG(shuffle) ? "true" : "false";
					printWithIndent("<TestRun Shuffle=\"%s\" RandomSeed=\"%d\">", shuffle, unit_test.random_seed());
				}

				virtual void OnTestIterationStart(const UnitTest& unit_test, int iteration)
				{
					SetIndentString(INDENT_LEVEL_ITERATION);
					printWithIndent("<TestIteration count=\"%d\">", iteration);
				}

				virtual void OnEnvironmentsSetUpStart(const UnitTest& unit_test)
				{
					BeginRawNode(INDENT_LEVEL_TESTCASE, "EnvironmentsSetUp");
				}

				virtual void OnEnvironmentsSetUpEnd(const UnitTest& unit_test)
				{
					EndRawNode(INDENT_LEVEL_TESTCASE, "EnvironmentsSetUp");
				}

				virtual void OnTestCaseStart(const TestCase& test_case)
				{
					SetIndentString(INDENT_LEVEL_TESTCASE);
					printWithIndent("<TestCase name=\"%s\">", test_case.name());

					if (test_case.test_to_run_count() > 0)
					{
						BeginRawNode(INDENT_LEVEL_TEST, "TestCaseSetUp");
					}
				}

				virtual void OnTestStart(const ::testing::TestInfo& test_info)
				{
					if (executedTestCount == 0)
						EndRawNode(INDENT_LEVEL_TEST, "TestCaseSetUp");

					SetIndentString(INDENT_LEVEL_TEST);
					printWithIndent("<Test name=\"%s\">", test_info.name());

					BeginRawNode(INDENT_LEVEL_TESTRESULT, "TestOutput");
				}

				virtual void OnTestPartResult(const ::testing::TestPartResult& test_part_result)
				{
				  if (test_part_result.failed())
				  {
					  EndRawNode(INDENT_LEVEL_TESTRESULT, "TestOutput");

					  SetIndentString(INDENT_LEVEL_TESTRESULT);
					  printWithIndent("<TestPartResult state=\"FAILED\">");

					  SetIndentString(INDENT_LEVEL_TESTPARTRESULTDETAILS);
                                          const char* filename = (test_part_result.file_name() == NULL) ? "unknown_filename" : test_part_result.file_name();
                                          const char* summary = (test_part_result.summary() == NULL) ? "" : test_part_result.summary();
                                          
					  printWithIndent("<PositionOfFailure>%s:%d</PositionOfFailure>", filename, summary);

					  printWithIndent("<Summary>");

					  SetIndentString(INDENT_LEVEL_SUMMARYDATA);
					  printWithIndent("<![CDATA[");

					  SetIndentString(INDENT_LEVEL_TESTRUN);
					  printWithIndent(test_part_result.summary());

					  SetIndentString(INDENT_LEVEL_SUMMARYDATA);
					  printWithIndent("]]>");

					  SetIndentString(INDENT_LEVEL_TESTPARTRESULTDETAILS);
					  printWithIndent("</Summary>");
            
					  SetIndentString(INDENT_LEVEL_TESTRESULT);
					  printWithIndent("</TestPartResult>");

					  BeginRawNode(INDENT_LEVEL_TESTRESULT, "TestOutput");
				  }
				}

				virtual void OnTestEnd(const ::testing::TestInfo& test_info)
				{
					EndRawNode(INDENT_LEVEL_TESTRESULT, "TestOutput");

					SetIndentString(INDENT_LEVEL_TESTRESULT);
					if (test_info.result()->Passed())
					{
						printWithIndent("<TestResult state=\"PASSED\"/>");
					} else {
						printWithIndent("<TestResult state=\"FAILED\"/>");
					}
					SetIndentString(INDENT_LEVEL_TEST);
					printWithIndent("</Test>");

					::testing::UnitTest* unit_test = ::testing::UnitTest::GetInstance();
					::testing::TestCase* test_case = const_cast< ::testing::TestCase* >(unit_test->current_test_case());

					executedTestCount++;

					if (test_case->test_to_run_count() == executedTestCount )
					{
						BeginRawNode(INDENT_LEVEL_TEST, "TestCaseTearDown");
					}
				}

				virtual void OnTestCaseEnd(const TestCase& test_case)
				{
					EndRawNode(INDENT_LEVEL_TEST, "TestCaseTearDown");

					SetIndentString(INDENT_LEVEL_TESTCASE);
					printWithIndent("</TestCase>");
					executedTestCount = 0;
				}

				virtual void OnEnvironmentsTearDownStart(const UnitTest& unit_test)
				{
					BeginRawNode(INDENT_LEVEL_TESTCASE, "EnvironmentsTearDown");
				}

				virtual void OnEnvironmentsTearDownEnd(const UnitTest& unit_test)
				{
					EndRawNode(INDENT_LEVEL_TESTCASE, "EnvironmentsTearDown");
				}

				void OnTestIterationEnd(const ::testing::UnitTest& unit_test, int iteration, ::testing::persistence::PersistentFrameworkState* state)
				{
					unsigned int failedTestCount = state->GetFailedTestCount();
					unsigned int passedTestCount = state->GetPassedTestCount();
					unsigned int totalTestCount = unit_test.total_test_count();

					SetIndentString(INDENT_LEVEL_TESTCASE);
					printWithIndent("<IterationResult total=\"%d\" passed=\"%d\" failed=\"%d\" disabled=\"%d\"/>",
							totalTestCount, passedTestCount, failedTestCount, totalTestCount - (passedTestCount + failedTestCount));

					SetIndentString(INDENT_LEVEL_ITERATION);
					printWithIndent("</TestIteration>");
				}

				virtual void OnTestProgramEnd(const UnitTest& unit_test)
				{
					SetIndentString(INDENT_LEVEL_TESTRUN);
					printWithIndent("</TestRun>");
				}
		};
	}
}


#endif /* PERSIGTESTXMLTRACEPRINTER_H_ */
