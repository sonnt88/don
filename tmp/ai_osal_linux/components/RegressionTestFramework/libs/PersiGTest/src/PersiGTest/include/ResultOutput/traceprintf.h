#ifndef TRACEPRINTF_H_
#define TRACEPRINTF_H_

#if (defined OSAL_OS) && (!defined NO_TRACE)

#define OSAL_S_IMPORT_INTERFACE_TYPES

//we do not want to use the OSAL new and delete operators here
#define OSAL_MEMORY_HEADER

#include <osal_if.h>

#include <system_types.h>
//#include <ostrace.h>
#include <tri_types.h>

extern tVoid TracePrintf(TR_tenTraceLevel TraceLevel, tPCChar pchFormat, ...);
extern tVoid vTracePrintf(TR_tenTraceLevel TraceLevel, tPCChar pchFormat, va_list argList);

#else

#include <stdio.h>
#define TracePrintf(TraceLevel, pchFormat, ...) fprintf(stdout, pchFormat"\n", ##__VA_ARGS__)
#define vTracePrintf(TraceLevel, pchFormat, argList) vfprintf(stdout, pchFormat"\n", argList)

#endif //(defined OSAL_OS) && (!defined NO_TRACE)

#endif //TRACEPRINTF_H_
