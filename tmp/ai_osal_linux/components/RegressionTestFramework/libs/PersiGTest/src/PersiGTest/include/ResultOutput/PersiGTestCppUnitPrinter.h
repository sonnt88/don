/* 
 * File:   PersiGTestCppUnitPrinter.h
 * Author: oto4hi
 *
 * Created on April 19, 2012, 11:44 AM
 */

#ifndef PERSIGTESTCPPUNITPRINTER_H
#define	PERSIGTESTCPPUNITPRINTER_H

namespace testing {
	namespace persistence {
		class PersiGTestCppUnitPrinter : public ::testing::persistence::IPersiGTestPrinter {
                private:
                        
                    char* error_string;
                    
                    virtual void OnTestStart(const TestInfo& test_info) {
                        error_string = NULL;
                    }
                    
                    virtual void OnTestPartResult(const ::testing::TestPartResult& test_part_result)
                    {
                        if (test_part_result.failed())
                        {   
                            const char* filename = (test_part_result.file_name() == NULL) ? "unknown_filename" : test_part_result.file_name();
                            
                            int sizeToAlloc = strlen(filename) + 12;

                            this->error_string = (char*)realloc(this->error_string, sizeToAlloc);
                            sprintf(error_string, "%s:%d", filename, test_part_result.line_number());
                        }
                    }
                    
                    virtual void OnTestEnd(const TestInfo& test_info) {
                        ::testing::UnitTest* unit_test = ::testing::UnitTest::GetInstance();
                        ::testing::TestCase* test_case = const_cast< testing::TestCase* >(unit_test->current_test_case());
                        fprintf(stdout, "%s::%s : ", test_case->name(), test_info.name());
                        if (test_info.result()->Passed())
                            fprintf(stdout, "OK\n");
                        else
                        {
                            fprintf(stdout, "%s FAILED\n", this->error_string);
                            free(this->error_string);
                            this->error_string = NULL;
                        }
                        
                    }
                            
                    
                    virtual void OnTestIterationEnd(const ::testing::UnitTest& unit_test, int iteration, ::testing::persistence::PersistentFrameworkState* state) 
                    {
                        if (state->GetFailedTestCount() > 0)
                            fprintf(stdout,"Failures !!!\n");
                        
                        fprintf(stdout, "Run: %d   Failure total: %d   Failures: %d   Errors: 0\n", state->GetPassedTestCount() + state->GetFailedTestCount(), 
                                state->GetFailedTestCount(), state->GetFailedTestCount());
                    }
                };
        }
}

#endif	/* PERSIGTESTCPPUNITPRINTER_H */

