/*
 * SequencialFileWriter.cpp
 *
 *  Created on: Dec 8, 2011
 *      Author: oto4hi
 */


#include <PersistentStorage/SequencialFileWriter.h>
#include <stdlib.h>
#include <fstream>
#include <cstdio>
#include <string.h>

SequencialFileWriter::SequencialFileWriter(const char* Path)
{
	if (!Path)
                throw new IOException("Path cannot be NULL.");

	unsigned int len = strlen(Path) + 1;

	this->path = (char*)malloc(len);
	memcpy(this->path, Path, len);
}

SequencialFileWriter::~SequencialFileWriter()
{
	if (this->path)
		free(this->path);
}

void SequencialFileWriter::write(void* Buffer, unsigned int Length)
{
	std::ofstream file(this->path, std::ios::out|std::ios::binary);

	if (file.is_open())
	{
		file.write((char*)Buffer, Length);
		file.close();
	}
}

void SequencialFileWriter::remove()
{
	std::remove(this->path);
}
