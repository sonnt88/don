/*
 * persigtest.h
 *
 *  Created on: Feb 15, 2012
 *      Author: oto4hi
 */

#ifndef PERSIGTEST_H_
#define PERSIGTEST_H_

#ifdef OSAL_OS
#include <osconfig.h>
#endif

#include <PersistentStateModels/TestState/PersistentTestState.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <ResultOutput/traceprintf.h>
#include <iostream>

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCTION__
#endif //_MSC_VER

#define REQUEST_POWER_CYCLE(TESTCASE,TESTNAME) std::cout << "[  RESET   ] " #TESTCASE "." #TESTNAME << " (0 ms)" << std::endl; HANG: sched_yield(); goto HANG;
#define HELLO() TracePrintf(TR_LEVEL_FATAL, "I am \"%s\".", __PRETTY_FUNCTION__)
#define BREAK_TEST() _BREAK_TEST(HasFailure())

void _BREAK_TEST(bool hasFailure);

#include <PersistentStorage/IOFactory.h>
#include <PersistentStateModels/FrameworkState/TraceablePersistentFrameworkStateManager.h>
#include <ResultOutput/PersiGTestSimpleTracePrinter.h>
#include <ResultOutput/PersiGTestXMLTracePrinter.h>
#include <ResultOutput/PrettyPersiUnitTestResultPrinter.h>
#include <ResultOutput/PersiGTestCppUnitPrinter.h>

#endif /* PERSIGTEST_H_ */
