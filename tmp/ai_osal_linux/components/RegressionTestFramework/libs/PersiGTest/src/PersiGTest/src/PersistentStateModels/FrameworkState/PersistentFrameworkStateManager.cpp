/*
 * PersistentFrameworkStateManager.cpp
 *
 *  Created on: Dec 8, 2011
 *      Author: oto4hi
 */

#include <ResultOutput/traceprintf.h>
#include <PersistentStateModels/FrameworkState/PersistentFrameworkStateManager.h>

using namespace testing::persistence;

void PersistentFrameworkStateManager::restoreInternalGTestState()
{
	::testing::TestInfo* lastTestInfo = NULL;
	::testing::UnitTest* unit_test = ::testing::UnitTest::GetInstance();
	int numberOfTestCases = this->state->GetTestCaseCount();

	if (numberOfTestCases != unit_test->total_test_case_count())
		throw new PersistentStateException("Error: invalid number of test cases - does the stored test case data set really belong to these tests?");

	// now decide whether to run a test or not
	for (int testCaseIndex = 0; testCaseIndex < numberOfTestCases; testCaseIndex++)
	{
		if (unit_test->GetTestCase(testCaseIndex)->total_test_count() != (int)(this->state->GetTestCount(testCaseIndex)))
			throw new PersistentStateException("Error: invalid number of tests - does the stored test case data set really belong to these tests?");

		::testing::TestCase* testCase = const_cast< ::testing::TestCase* >(unit_test->GetTestCase(testCaseIndex));

		int testsToRun = 0;

		for (int testIndex = 0; testIndex < testCase->total_test_count(); testIndex++)
		{
			int actualTestIndex = testCase->test_indices_[testIndex];
			bool should_run = (this->state->GetResult(testCaseIndex, actualTestIndex) == SHOULD_RUN ||
				this->state->GetResult(testCaseIndex, actualTestIndex) == HALTED) ? 1 : 0;

			if (this->state->GetResult(testCaseIndex, actualTestIndex) == RUNNING)
			{
				lastTestInfo = testCase->test_info_list_[actualTestIndex];

				unit_test->impl_->set_current_test_case(testCase);
				unit_test->impl_->set_current_test_info(lastTestInfo);
			}

			testCase->test_info_list_[actualTestIndex]->should_run_ = should_run;

			if (should_run)
				testsToRun++;
		}
		testCase->set_should_run(testsToRun > 0);
	}

	if (lastTestInfo)
	{
		const ::testing::TestPartResult testPartResult =
				::testing::TestPartResult(::testing::TestPartResult::kFatalFailure, "unknown", 1, "test failed");

		TestResult* testResult = const_cast<TestResult*>(lastTestInfo->result());
		testResult->AddTestPartResult(testPartResult);
		this->OnTestEnd(*lastTestInfo);
	}
}

PersistentFrameworkStateManager::PersistentFrameworkStateManager(int argc, char** argv, IReader* Reader, IWriter* Writer)
{
	this->state = new PersistentFrameworkState(Reader, Writer);
	this->state->SetArgs(argc, argv);
	this->firstTestAfterHalt = false;
	this->restoredState = false;
}

PersistentFrameworkStateManager::~PersistentFrameworkStateManager()
{
	delete this->state;
}

void PersistentFrameworkStateManager::OnTestProgramStart(const ::testing::UnitTest& unit_test)
{
	PersistentFrameworkState* originalState = new PersistentFrameworkState(*(this->state));
	originalState->SetRandomSeed(unit_test.impl_->random_seed_);

	PersistentFrameworkState* restoredState = new PersistentFrameworkState(*(this->state));

	delete this->state;

	originalState->Init();

	if (restoredState->Restore())
	{
		if (originalState->IsStructurallyEquivalent(*restoredState))
		{
			this->state = restoredState;
			this->restoredState = true;

			if (state->GetResult(state->GetCurrentTestCase(), state->GetCurrentTest()) == HALTED)
				this->firstTestAfterHalt = true;

			delete originalState;
		}
		else
		{
			TracePrintf(TR_LEVEL_FATAL, "Dropping persisted FrameworkState because it does not fit the current test run.");
			this->state = originalState;
			delete restoredState;
		}
	}
	else
	{
		this->state = originalState;
		delete restoredState;
	}

	restoreInternalGTestState();

  GTEST_FLAG(repeat) = this->state->GetIterationCount() - this->state->GetCurrentIteration();
	unit_test.impl_->random_seed_ = this->state->GetRandomSeed();
}

void PersistentFrameworkStateManager::OnTestIterationStart(const ::testing::UnitTest& unit_test, int iteration)
{
	if (iteration > 0)
	{
		this->state->SetRandomSeed(unit_test.impl_->random_seed_);
		this->state->NextIteration();
		this->state->Persist();
		this->restoreInternalGTestState();
	}
}

void PersistentFrameworkStateManager::OnEnvironmentsSetUpStart(const ::testing::UnitTest& unit_test)
{

}

void PersistentFrameworkStateManager::OnEnvironmentsSetUpEnd(const ::testing::UnitTest& unit_test)
{
}

void PersistentFrameworkStateManager::OnTestCaseStart(const ::testing::TestCase& test_case)
{
	int testCaseIndex = 0;
	::testing::TestCase* testCase = NULL;

	// search for the test case index
	do {
		testCase = const_cast< ::testing::TestCase* >( ::testing::UnitTest::GetInstance()->GetTestCase(testCaseIndex) );
		if ( strcmp(testCase->name(), test_case.name()) == 0)
			break;
		else
			testCaseIndex++;
	} while (testCase != NULL);

	// set the test case index in the persistent state
	if (testCase != NULL)
	{
		this->state->SetCurrentTestCase(testCaseIndex);
		this->state->SetCurrentTest(0);
		this->state->Persist();
	}
}

void PersistentFrameworkStateManager::OnTestStart(const ::testing::TestInfo& test_info)
{
	::testing::TestCase* testCase = const_cast< ::testing::TestCase* >( ::testing::UnitTest::GetInstance()->current_test_case() );
	int testInfoIndex = 0;
	::testing::TestInfo* testInfo = NULL;
	do {
		testInfo = const_cast< ::testing::TestInfo* >( testCase->GetTestInfo(testInfoIndex) );
		if ( strcmp(testInfo->name(), test_info.name()) == 0 )
			break;
		else
			testInfoIndex++;
	} while  (testInfo != NULL);
	if (testInfo != NULL)
	{
		::testing::UnitTest* unit_test = ::testing::UnitTest::GetInstance();
		::testing::TestCase* test_case = const_cast< ::testing::TestCase* >(unit_test->current_test_case());
		this->state->SetCurrentTest(test_case->test_indices_[testInfoIndex]);
	}

	this->state->SetResult(RUNNING);
	this->state->Persist();
}

void PersistentFrameworkStateManager::OnTestPartResult(const ::testing::TestPartResult& test_part_result)
{

}

void PersistentFrameworkStateManager::OnTestEnd(const ::testing::TestInfo& test_info)
{
	this->state->SetResult( (test_info.result()->Passed()) ? PASSED : FAILED );
	this->state->Persist();
	this->firstTestAfterHalt = false;
}

void PersistentFrameworkStateManager::OnTestCaseEnd(const ::testing::TestCase& test_case)
{

}

void PersistentFrameworkStateManager::OnEnvironmentsTearDownStart(const ::testing::UnitTest& unit_test)
{

}

void PersistentFrameworkStateManager::OnEnvironmentsTearDownEnd(const ::testing::UnitTest& unit_test)
{

}

void PersistentFrameworkStateManager::OnTestIterationEnd(const ::testing::UnitTest& unit_test, int iteration)
{
}

void PersistentFrameworkStateManager::OnTestProgramEnd(const ::testing::UnitTest& unit_test)
{
	this->state->Remove();
}

void PersistentFrameworkStateManager::HaltTest()
{
	this->state->SetResult(HALTED);
	this->state->Persist();
}

void PersistentFrameworkStateManager::BreakAllTests()
{
#ifdef OSAL_OS
	_exit(0);
#else
	exit(0);
#endif
}
