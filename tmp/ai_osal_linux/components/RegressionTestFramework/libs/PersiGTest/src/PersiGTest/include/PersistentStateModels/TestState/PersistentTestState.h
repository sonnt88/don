/*
 * persigtest.h
 *
 *  Created on: Dec 15, 2011
 *      Author: oto4hi
 */

#ifndef PERSISTENTTESTSTATE_H_
#define PERSISTENTTESTSTATE_H_

#include <gtest/gtest.h>
#include <exception>
#include <PersistentStorage/Interfaces/IReader.h>
#include <PersistentStorage/Interfaces/IWriter.h>
#include <PersistentStateModels/Interfaces/IPersistentState.h>

namespace testing {
	namespace persistence {

		class PersistentTestState : public IPersistentState {
		private:
			void* pBuffer;
			unsigned int bufferLength;

			IWriter* writer;
			IReader* reader;

			void clear();
		public:
			virtual void Persist();
			virtual bool Restore();

			static char* GetCurrentTestStateFilename();

			PersistentTestState(IReader* Reader, IWriter* Writer);
			void SetBuffer(void* pBuffer, unsigned int Length);

			void* GetBuffer(unsigned int& Length);

			~PersistentTestState();
		};
	}
}

//shortcut methods

void PERSIST(void* pBuffer, unsigned int length);
bool RESTORE(void*& pBuffer, unsigned int& length);

#endif /* PERSISTENTTESTSTATE_H_ */
