/*
 * ReadWriteException.cpp
 *
 *  Created on: Dec 12, 2011
 *      Author: oto4hi
 */

#include <iostream>
#include <PersistentStorage/IOException.h>

IOException::IOException(const char* what) {
	// TODO Auto-generated constructor stub
	this->_what = what;
	std::cout << what << "\n";
}

const char* IOException::what() const throw()
{
	return this->_what;
}



