/*
 * persigtest.cpp
 *
 *  Created on: Mar 1, 2012
 *      Author: oto4hi
 */

#include <persigtest.h>
#include <PersistentStateModels/FrameworkState/PersistentFrameworkStateManager.h>

extern ::testing::persistence::PersistentFrameworkStateManager* persistentFrameworkStateManager;

void _BREAK_TEST(bool hasFailure)
{
	//we only want to allow to return to that test iff there are no failures until now
	if (!hasFailure)
		persistentFrameworkStateManager->HaltTest();

	persistentFrameworkStateManager->BreakAllTests();
}

