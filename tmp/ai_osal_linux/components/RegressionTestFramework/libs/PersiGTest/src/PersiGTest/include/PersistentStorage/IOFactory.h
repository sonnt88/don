/*
 * IOFactory.h
 *
 *  Created on: Jan 19, 2012
 *      Author: oto4hi
 */

#ifndef IOFACTORY_H_
#define IOFACTORY_H_

#include "Interfaces/IReader.h"
#include "Interfaces/IWriter.h"

class IOFactory {
private:
	static char* GetCurrentTestStateFilename();
public:
	static IReader* CreateTestStateReader();
	static IWriter* CreateTestStateWriter();
	static IReader* CreateFrameworkStateReader();
	static IWriter* CreateFrameworkStateWriter();
};

#endif /* IOFACTORY_H_ */
