/*
 * ReadWriteException.h
 *
 *  Created on: Dec 12, 2011
 *      Author: oto4hi
 */

#ifndef READWRITEEXCEPTION_H_
#define READWRITEEXCEPTION_H_

#include <exception>

class IOException : public std::exception
{
private:
	const char* _what;
public:
	IOException(const char* what);
	virtual const char* what() const throw();
};


#endif /* READWRITEEXCEPTION_H_ */
