// Copyright 2006, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <iostream>
#include <persigtest.h>
#include <execinfo.h>
#include <signal.h>
#include <string.h>
#include <sstream>

::testing::persistence::PersistentFrameworkStateManager* persistentFrameworkStateManager;

/*======================Start: build environment specific definitions======================*/

#ifdef OSAL_OS

  #ifndef SYSTEM_PROGRAM
    #ifndef NO_ANCHORS
      #include <anchors.h>
    #endif
  #endif

  #if OSAL_OS==OSAL_TENGINE || defined(HAS_VSTARTAPP)

    #ifdef SYSTEM_PROGRAM
      #define MAIN GTEST_API_ int main
      #define RETURN return retVal
    #else // SYSTEM_PROGRAM is not defined:
      extern "C"{
        void vStartApp(int argc, char **argv);
      }
      #define MAIN void vStartApp
      #define RETURN _exit(0)
    #endif //SYSTEM_PROGRAM

  #else

    #define MAIN GTEST_API_ int main

    #if OSAL_OS==OSAL_LINUX
      #define RETURN _exit(0)
    #else //OSAL_OS!=OSAL_LINUX:
      #define RETURN return retVal
    #endif //OSAL_OS==OSAL_LINUX

  #endif //OSAL_OS==OSAL_TENGINE

#else //OSAL_OS is not defined:

  #define MAIN GTEST_API_ int main
  #define RETURN return retVal

#endif //OSAL_OS

#define STACKFRAME_COUNT 20
void SIGSEGVhandler(int Signal)
{
	void *array[STACKFRAME_COUNT];
	size_t size;
	// get void*'s for all entries on the stack
	size = backtrace(array, STACKFRAME_COUNT);

	// print out all the frames to stderr
	fprintf(stderr, "======================================Error: signal %d:======================================\n", Signal);

	fsync(fileno(stdout));
	backtrace_symbols_fd(array, size, 2);
	exit(1);
}

/*=======================End: build environment specific definitions=======================*/

MAIN(int argc, char **argv)
{
  int myargc = 0;
  char** myargv = (char**)malloc(sizeof(char*) * argc);

  bool usePersiFeatures = false;
  bool printPersiHelp = false;

  IReader* reader = NULL;
  IWriter* writer = NULL;

  std::stringstream maxIntStringstream;
  maxIntStringstream << "--gtest_repeat=" << LONG_MAX - 1;

  std::string maxIntString;
  maxIntStringstream >> maxIntString;

  ::testing::persistence::IPersiGTestPrinter* printer = NULL;
  ::testing::TestEventListener* default_result_printer;

  for (int argIndex = 0; argIndex < argc; argIndex++)
  {
	  if ( !strncmp(argv[argIndex], "--gtest_use_persi_features", 27) )
	  {
		  usePersiFeatures = true;
	  } else
	  {
		  if ( !strncmp(argv[argIndex], "--help", 7) || !strncmp(argv[argIndex], "-h", 3) )
		  {
			  printPersiHelp = true;
		  }

		  myargv[myargc] = argv[argIndex];
		  myargc++;
	  }
  }

  if (usePersiFeatures)
  {
	  for (int argIndex = 1; argIndex < myargc; argIndex++)
	  {
		  if ( !strncmp(myargv[argIndex], "--gtest_repeat=", 15 ) )
		  {
			  try {
				  std::stringstream repeatNumberAsStringStream;
				  std::string repeatNumberAsString((char*)(myargv[argIndex]+15));

				  int numberOfRepeats = 0;
				  repeatNumberAsStringStream << repeatNumberAsString;
				  repeatNumberAsStringStream >> numberOfRepeats;

				  if (numberOfRepeats < 0)
					  myargv[argIndex] = const_cast<char*>(maxIntString.c_str());

			  } catch (...) {}
		  }
	  }
  }


  if (printPersiHelp)
  {
	  fprintf(stdout, "Test Framework Behaviour:\n");
	  fprintf(stdout, "  --gtest_use_persi_features\n");
	  fprintf(stdout, "      Switch on persistence features.\n");
	  fprintf(stdout, "\n");
  }

  if (usePersiFeatures)
  {
	  //signal(SIGSEGV, SIGSEGVhandler);
	  reader = IOFactory::CreateFrameworkStateReader();
	  writer = IOFactory::CreateFrameworkStateWriter();

	  //::testing::persistence::IPersiGTestPrinter* printer = new ::testing::persistence::PersiGTestXMLTracePrinter();
	  //::testing::persistence::IPersiGTestPrinter* printer = new ::testing::persistence::PersiGTestCppUnitPrinter();

	  printer = new ::testing::persistence::PrettyPersiUnitTestResultPrinter();

	  ::testing::TestEventListeners& listeners = ::testing::UnitTest::GetInstance()->listeners();
	  default_result_printer = listeners.Release(listeners.default_result_printer());
    
	  persistentFrameworkStateManager =
			  new ::testing::persistence::TraceablePersistentFrameworkStateManager(argc, argv, reader, writer, printer);

	  listeners.Append(persistentFrameworkStateManager);
  }

  testing::InitGoogleMock(&myargc, myargv);

  // Ensures that the tests pass no matter what value of
  // --gmock_catch_leaked_mocks and --gmock_verbose the user specifies.
  testing::GMOCK_FLAG(catch_leaked_mocks) = true;
  testing::GMOCK_FLAG(verbose) = testing::internal::kWarningVerbosity;

  int retVal = RUN_ALL_TESTS();

  if (usePersiFeatures)
  {
	  delete reader;
	  delete writer;
	  delete printer;
	  delete default_result_printer;
  }
  
  free(myargv);

  RETURN;
}

