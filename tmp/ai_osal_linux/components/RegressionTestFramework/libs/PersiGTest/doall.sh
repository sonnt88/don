#!/bin/sh
make clean
rm -Rf build aclocal.m4 autom4te.cache/ compile config.guess config.log config.status configure config.sub depcomp INSTALL install-sh libtool ltmain.sh m4/ Makefile Makefile.in missing src/Makefile src/Makefile.in
aclocal
libtoolize
autoconf
automake --add-missing
./configure --prefix=$(pwd)/build
