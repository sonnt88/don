#!/bin/sh
rm -Rf $_SWBUILDROOT/GTestUI
cp -Rf $_SWNAVIROOT/../ai_osal_linux/components/RegressionTestFramework/GTestUI $_SWBUILDROOT/generated
xbuild /property:configuration=Release /target:GTestCommon $_SWBUILDROOT/generated/GTestUI/GTestUI.sln
xbuild /property:configuration=Release /target:GTestAdapter $_SWBUILDROOT/generated/GTestUI/GTestUI.sln
xbuild /property:configuration=Release /target:GTestUI $_SWBUILDROOT/generated/GTestUI/GTestUI.sln
#manual postbuild step
if [[ ! -d $_SWBUILDROOT/GTestUI/GTestUI/bin/Release ]]; then
  mkdir -p $_SWBUILDROOT/GTestUI/GTestUI/bin/Release
fi
cp -f $_SWBUILDROOT/generated/GTestUI/GTestAdapter/bin/Release/GTestAdapter.exe $_SWBUILDROOT/GTestUI/GTestUI/bin/Release/

