/* 
 * File:   ProcessControllerTest.cpp
 * Author: aga2hi
 *
 * Created on 12 June 2015, 4:24:52 PM
 */

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Library Functions

#include <sys/poll.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <poll.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <stdexcept>

// Project Functions

#include "ProcessController.h"

// Google Test functions

#include <gtest/gtest.h>

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Global Declarations

#define INIT_FLAG_VALUE 4
#define CHILD_EXIT_STATUS 2

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Class Declarations
 */

struct ParentLoopback : ProcessController::ParentFunctor {
    std::vector< std::string > wordStore;
    std::string finalMsg;

    ParentLoopback() {
    }

    ~ParentLoopback() {
    }

	virtual void clear() {
		wordStore.clear();
		finalMsg.clear();
	}
    virtual void operator()(void);
};

struct ChildLoopback : ProcessController::ChildFunctor {
    volatile int flag;

    ChildLoopback() : flag(INIT_FLAG_VALUE) {
    }

    ~ChildLoopback() {
    }

	virtual void clear() { }
    virtual void operator()(void);
};

struct ParentStatus : ProcessController::ParentFunctor {
    std::vector< std::string > wordStore;

    ParentStatus() {
    }

    ~ParentStatus() {
    }

	virtual void clear() {
		wordStore.clear();
	}
    virtual void operator()(void);
};

struct ParentInteractive : ProcessController::ParentFunctor {
    virtual void operator()(void);
};

struct ChildInteractive : ProcessController::ChildFunctor {
    virtual void operator()(void);
};

struct ParentUnbuffered : ProcessController::ParentFunctor {
    virtual void operator()(void);
    std::string errMsg;
};

struct ParentCrash : ProcessController::ParentFunctor {
	virtual void operator()(void);

	std::vector< std::string > recvdData;
	bool timeout;
};

struct ChildDivideZero : ProcessController::ChildFunctor {
	virtual void operator()(void);
};

struct ChildHang : ProcessController::ChildFunctor {
	virtual void operator()(void);
};

struct ChildNullPtr : ProcessController::ChildFunctor {
	virtual void operator()(void);
};

struct ChildAbort :  ProcessController::ChildFunctor {
	virtual void operator()(void);
};

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Methods
 */

// This exercises the loopback

void
ParentLoopback::operator()(void) {
    char * fgetFeedback;
    FILE * const toChild(GetOutputFileBuffer());
    FILE * const fromChild(GetInputFileBuffer());
    FILE * const fromErr(GetErrorFileBuffer());

    static const std::size_t bufferSize = 256;
    char buffer[ bufferSize ];

	// Send our "message" to the client, and make sure
	// that there were no errors

    const int fputsCheck = std::fputs("Hello\nThere\nSailor\nEXIT\nNot\n", toChild);
	if( EOF == fputsCheck ) {
		// Didn't work
		std::stringstream err;
		err << "ParentLoopback::operator(); ";
		err << "fputs() : " << strerror( errno );
		err << "\n";
		throw std::runtime_error( err.str().c_str() );
	}
    const int fflushCheck = std::fflush(toChild);
	if( EOF == fflushCheck ) {
		// Didn't work
		std::stringstream err;
		err << "ParentLoopback::operator(); ";
		err << "fflush() : " << strerror( errno );
		err << "\n";
		throw std::runtime_error( err.str().c_str() );
	}

	// Now see what we get back

    do {
        // We use fgets() here.  It purports to do just what we want: -
        // buffered input from a file descriptor, one line at a time.

		// But we have also discovered, during the course of this software
		// development, that it is unreliable.  When the other end of the
		// pipe hangs up, which happens when the associated process ends,
		// then fgets() is supposed to return NULL.  Unfortunately, fgets()
		// sometimes fills the buffer with rubbish and returns a pointer to
		// it.

        // But we have little choice here, if we want to test buffered
        // data transfer

        fgetFeedback = std::fgets(buffer, bufferSize, fromChild);
        if (fgetFeedback == buffer)
            wordStore.push_back(buffer);
    } while (feof(fromChild) == 0);

    fgetFeedback = std::fgets(buffer, bufferSize, fromErr);
    finalMsg = buffer;
}

// This terminates the child with SIGKILL

void
ParentStatus::operator()(void) {
    char * fgetFeedback;

    static const std::size_t bufferSize = 256;
    char buffer[ bufferSize ];

    FILE * const toChild(GetOutputFileBuffer());
    FILE * const fromChild(GetInputFileBuffer());

    std::fputs("Big\nFish\n", toChild);
    std::fflush(toChild);

    // The child loopback should send the two messages
    // back, and wait for the next message

    fgetFeedback = std::fgets(buffer, bufferSize, fromChild);
    if (fgetFeedback == buffer) {
        wordStore.push_back(buffer);
    }
    fgetFeedback = std::fgets(buffer, bufferSize, fromChild);
    if (fgetFeedback == buffer) {
        wordStore.push_back(buffer);
    }

    // If we get this far then we know that the child
    // process is running.  Terminate it and find out
    // how ProcessController coped

    TerminateChildProc();
}

// This is just loopback until "EXIT"

void
ChildLoopback::operator()(void) {
    std::string buffer;

	// Now we perform the loopback test

    while (std::cin >> buffer) {
        if ("EXIT" == buffer) {
            // NB cerr is not buffered; we don't need endl
            std::cerr << "CHILD : EXIT";
            break;
        }
        std::cout << "CHILD : " << buffer << std::endl;
    }

	// Write to flag (a global variable) and set exit
	// code.  Both can be test for.

    flag = INIT_FLAG_VALUE + 1;
    std::exit(CHILD_EXIT_STATUS);
}

// These mimicks the google test programme

void
ParentInteractive::operator()(void) {
    static const std::size_t bufferSize = 256;
    char buffer[ bufferSize ];

    FILE * const toChild(GetOutputFileBuffer());
    FILE * const fromChild(GetInputFileBuffer());

    do {
        // We use fgets() here.  It purports to do just what we want: -
        // buffered input from a file descriptor, one line at a time.

        // But we have also discovered, during the course of this software
        // development, that it is unreliable.  When the other end of the
        // pipe hangs up, which happens when the associated process ends,
        // then fgets() is supposed to return NULL.  Unfortunately, fgets()
        // sometimes fills the buffer with rubbish and returns a pointer to
        // it.

        // But we have little choice here, if we want to test buffered
        // data transfer

        char * const fgetFeedback = std::fgets(buffer, bufferSize, fromChild);
        if (fgetFeedback == buffer) {
            if (std::strcmp(buffer, "[ PASS ]\n") == 0) {
                std::fputs("OK\n", toChild);
            } else if (std::strcmp(buffer, "[ FAIL ]\n") == 0) {
                std::fputs("CLOSE\n", toChild);
            } else if (std::strcmp(buffer, "[ CLOSE ]\n") == 0) {
                TerminateChildProc();
            }
        }
    } while (feof(fromChild) == 0);
}

void
ChildInteractive::operator()(void) {
    std::string buffer;
    std::cout << "[ PASS ]" << std::endl;
    while (std::cin >> buffer) {
        if ("OK" == buffer) {
            std::cout << "[ FAIL ]" << std::endl;
        }
        if ("CLOSE" == buffer) {
            std::cout << "[ CLOSE ]" << std::endl;
			break;
        }
    }

    std::exit(CHILD_EXIT_STATUS);
}

// This communicates without buffers

void
ParentUnbuffered::operator()(void) {
    static const std::size_t bufferSize(256);
    char buffer[ bufferSize ];

    const int toChild(GetOutputFileDescriptor());
    const int fromChild(GetInputFileDescriptor());
    bool childStillRunning(true);

    std::string savedLine;

    do {
        const ssize_t rdStatus = read(fromChild, buffer, bufferSize);

        if (rdStatus == 0) {
            // end of file
            errMsg.assign("ParentUnbuffered::operator() - read() ");
            errMsg.append("returned end of file");
            childStillRunning = false;
        } else if (rdStatus == -1) {
            // error
            errMsg.assign("ParentUnbuffered::operator() - read() ");
            errMsg.append(strerror(errno));
            childStillRunning = false;
        } else {

            // Add the buffer to the line that we've saved, in
            // case we have not received sufficient characters

            savedLine.append(buffer, rdStatus);

            // Now find out what we've got

            if (savedLine.length() >= 9) {

                if (savedLine.substr(0, 9) == "[ PASS ]\n") {
                    savedLine.erase(0, 9);
                    write(toChild, "OK\n", 3);
                    fsync(toChild);
                } else if (savedLine.substr(0, 9) == "[ FAIL ]\n") {
                    savedLine.erase(0, 9);
                    write(toChild, "CLOSE\n", 6);
                    fsync(toChild);
                }
            }

            if (savedLine.length() >= 10) {

                if (savedLine.substr(0, 10) == "[ CLOSE ]\n") {
                    savedLine.erase(0, 10);
                    childStillRunning = false;
                }
            }
        }
    } while (childStillRunning);
}

// Uses poll() to monitor file descriptors connecting
// parent to child.  Meanwhile, child crashes or hangs.

void
ParentCrash::operator()(void) {
	// First of all, reset data

	timeout = false;
	recvdData.clear();

	// Just wait on a file descriptor
	// (we don't actually expect to
	// receive anything -- but if we
	// do then something has gone
	// wrong, and not necessarily
	// our programme)

	const int timeoutMilliSeconds(500);
	const nfds_t numberFileDescriptors(2);
	struct pollfd testpoll[numberFileDescriptors];
	testpoll[0].fd = GetErrorFileDescriptor();
	testpoll[0].events = POLLIN | POLLPRI;
	testpoll[0].revents = 0;
	testpoll[1].fd = GetInputFileDescriptor();
	testpoll[1].events = POLLIN | POLLPRI;
	testpoll[1].revents = 0;

	// Now wait for something to happen on a file descriptor

	bool repeatRequired( false );
	do {
		// Make sure we don't go around the loop forever
		repeatRequired = false;

		// Now poll the file descriptors
		const int pollCheck = poll( testpoll, numberFileDescriptors, timeoutMilliSeconds );

		std::stringstream err;
		switch( pollCheck ) {
		case -1:
			if( EINTR == errno ) {
				// We've received a signal.  No matter, go round the
				// loop once more.

				repeatRequired = true;
			} else {
				// An error occured.  Report it to the maintainer of
				// this software and let him sort it out.

				err << "ParentCrash::operator(); poll () - ";
				err << strerror( errno );
				throw std::runtime_error( err.str().c_str() );
			}  // End else
			break;

		case 0:
			// Timeout - child process is still running
			// so we must terminate it.

			TerminateChildProc();
			timeout = true;
			break;

		default:
			// Something has happened on one or more file descriptors.
			// This isn't the place to deal with it though.

			break;
		}  //  End switch
	} while( repeatRequired );

	// Find out what the file descriptors have to say

	for( int index = 0; index < numberFileDescriptors; ++index ) {

		if(( POLLIN & testpoll[ index ].revents ) ||
		   ( POLLPRI & testpoll[ index ].revents )) {

			// We have something to read !

			const size_t buffSize( 256 );
			char buffer[ buffSize ];

			const ssize_t rdCheck = read( testpoll[ index ].fd, buffer, buffSize );
			std::stringstream err;
			switch( rdCheck)  {
			case -1:
				// read() has encountered an error

				err << "ParentCrash::operator(); read() - ";
				err << strerror( errno );
				throw std::runtime_error( err.str().c_str() );
				break;
			case 0:
				// End of file - which might happen if the other end of
				// the pipe has terminated

				break;
			default:
				// Data has arrived!!

				recvdData.push_back( std::string( buffer, rdCheck ) );
				break;
			}  // End switch

		}  // End if

		if( POLLERR & testpoll[ index ].revents ) {

			// poll() has encountered an error in
			// this file descriptor

			throw std::runtime_error( "ParentCrash::operator(); poll() - "
									  "error in file descriptor" );
		}  // End if

	}  // End for
}

void
ChildDivideZero::operator()(void) {
	for( int n = -2; n < 2; ++n ) {
		const int q = n / n;
	}
}

void
ChildHang::operator()(void) {
 hang:
	sleep( 10 );
	goto hang;
}

void
ChildNullPtr::operator()(void) {
	int * const nullptr = NULL;
	*nullptr = 0;
}

void
ChildAbort::operator()(void) {
	abort();
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Global Variables
 */

namespace {
    ChildLoopback childLoopbackTest;
    ParentLoopback parentLoopbackTest;
    ProcessController loopbackTest(&parentLoopbackTest, &childLoopbackTest);

    ParentStatus parentStatusTest;
    ProcessController statusTest(&parentStatusTest, &childLoopbackTest);
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

TEST(ProcessControllerTests, LoopbackInit) {

	// If we run these tests without GTestService, and set a
	// repeat larger than 1, then we will need to reset the
	// global variables.

	parentLoopbackTest.clear();
	parentStatusTest.clear();
	childLoopbackTest.clear();

    loopbackTest.ConfigureStdoutPipe();
    loopbackTest.ConfigureStdinPipe();
    loopbackTest.ConfigureStderrPipe();

    ASSERT_NO_THROW(loopbackTest.Run()) << "ProcessController::Run() threw exception";
}

TEST(ProcessControllerTests, LoopbackExit) {

	ASSERT_TRUE(loopbackTest.ChildExitNormally()) << "child loopback crashed";
}

TEST(ProcessControllerTests, LoopbackExitStatus) {

	ASSERT_EQ(CHILD_EXIT_STATUS, loopbackTest.GetChildExitStatus())
		<< "child loopback returned wrong exit status";
}

TEST(ProcessControllerTests, LoopbackMemorySpace) {

	ASSERT_EQ(INIT_FLAG_VALUE, childLoopbackTest.flag )
		<< "child loopback did not run in its own memory space";
}

TEST(ProcessControllerTests, LoopbackIPC) {

	ASSERT_EQ(3, parentLoopbackTest.wordStore.size())
		<< "parent loopback word store has "
		<< parentLoopbackTest.wordStore.size()
		<< " words instead of 3";

	ASSERT_TRUE((parentLoopbackTest.wordStore.size() > 0) && (parentLoopbackTest.wordStore[0] == "CHILD : Hello\n"))
		<< "first word in parent loopback word store is "
		<< parentLoopbackTest.wordStore[0]
		<< " and should be \"Child : Hello\"";

	ASSERT_TRUE((parentLoopbackTest.wordStore.size() > 1) && (parentLoopbackTest.wordStore[1] == "CHILD : There\n"))
		<< "second word in parent loopback word store is "
		<< parentLoopbackTest.wordStore[1]
		<< " and should be \"Child : There\"";

	ASSERT_TRUE((parentLoopbackTest.wordStore.size() > 2) && (parentLoopbackTest.wordStore[2] == "CHILD : Sailor\n"))
		<< "third word in parent loopback word store is "
		<< parentLoopbackTest.wordStore[2]
		<< " and should be \"Child : Sailor\"";

	ASSERT_EQ("CHILD : EXIT", parentLoopbackTest.finalMsg)
		<< "final message received by parent loopback was wrong";
}

TEST(ProcessControllerTests, StatusInit) {

    statusTest.ConfigureStdoutPipe();
    statusTest.ConfigureStdinPipe();

    ASSERT_NO_THROW(statusTest.Run()) << "ProcessController::Run() threw exception";
}

TEST(ProcessControllerTests, StatusMessageCount) {

	ASSERT_EQ(2, parentStatusTest.wordStore.size()) << "Wrong number of messages received";
}

TEST(ProcessControllerTests, StatusIPC) {

	ASSERT_EQ("CHILD : Big\n", parentStatusTest.wordStore[0]) << "First message wrong";

	ASSERT_EQ("CHILD : Fish\n", parentStatusTest.wordStore[1]) << "Second message wrong";

	for (int i = 2; i < parentStatusTest.wordStore.size(); ++i) {
		FAIL() << "parent status has unexpected words :" << parentStatusTest.wordStore[i];
	}
}

TEST(ProcessControllerTests, StatusExit) {

	ASSERT_FALSE(statusTest.ChildExitNormally())
		<< "process controller thinks child terminated normally";
}

TEST(ProcessControllerTests, StatusSignalNumber) {

	ASSERT_EQ(SIGKILL, statusTest.GetChildSignalNumber())
		<< "child loopback terminated by wrong signal number";
}

TEST(ProcessControllerTests, RerunLoopback) {

    ChildLoopback * childTest;
    ParentLoopback * parentTest;
    ProcessController * newTest;

    // Configure a new test fixture

    childTest = new ChildLoopback;
    parentTest = new ParentLoopback;
    ASSERT_NO_THROW(newTest = new ProcessController(parentTest, childTest));

    newTest->ConfigureStdoutPipe();
    newTest->ConfigureStdinPipe();
    newTest->ConfigureStderrPipe();

    // Make sure it does not hang or crash when you run it
    // multiple times

    for (int index = 0; index < 2; ++index) {
        ASSERT_NO_THROW(newTest->Run())
			<< "Running Loopback with index = "
			<< index;
    }

    // Now make sure that the pipe communications worked
    // for both test runs

	ASSERT_EQ(6, parentTest->wordStore.size())
		<< "parent word store has wrong number of words";

	ASSERT_TRUE((parentTest->wordStore.size() > 3) &&
				(parentTest->wordStore[0] == parentTest->wordStore[3]))
		<< "first and fourth words in parent word store do not match";

	ASSERT_TRUE((parentTest->wordStore.size() > 4) &&
				(parentTest->wordStore[1] == parentTest->wordStore[4]))
		<< "second and fifth words in parent word store do not match";

	ASSERT_TRUE((parentTest->wordStore.size() > 5) &&
				(parentTest->wordStore[2] == parentTest->wordStore[5]))
		<< "third and sixth words in parent word store do not match";

    // Make sure destructors do not hang or crash

    delete newTest;
    delete childTest;
    delete parentTest;
}

TEST(ProcessControllerTests, InteractiveRun) {

    // The purpose is to test that line-buffering
    // works properly

    ChildInteractive childGoogle;
    ParentInteractive parentGoogle;

    // Configure a new test fixture

    ProcessController newTest(&parentGoogle, &childGoogle);

    newTest.ConfigureStdoutPipe(ProcessController::LINE);
    newTest.ConfigureStdinPipe(ProcessController::LINE);

    // Now run it

    ASSERT_NO_THROW(newTest.Run());

    // If we get this far it has worked.  Otherwise it
    // will hang or throw an exception
}

TEST(ProcessControllerTests, UnbufferedRun) {

    // The purpose of this test is to prove that un-buffered
    // file descriptor communications work properly

    ChildInteractive childProc;
    ParentUnbuffered parentProc;

    // Configure a new test fixture

    ProcessController newTest(&parentProc, &childProc);

    newTest.ConfigureStdoutPipe(ProcessController::NONE);
    newTest.ConfigureStdinPipe(ProcessController::NONE);

    // Now run it

    ASSERT_NO_THROW(newTest.Run());

    // Find out if there was an error

	ASSERT_TRUE(parentProc.errMsg.empty()) << parentProc.errMsg.c_str();

	ASSERT_EQ(CHILD_EXIT_STATUS, newTest.GetChildExitStatus()) <<
		"child process returned wrong exit status";

    // Make sure that the pointers to buffers
    // don't point anywhere

	ASSERT_EQ(NULL, parentProc.GetOutputFileBuffer()) << "Output file buffer not NULL";

	ASSERT_EQ(NULL, parentProc.GetErrorFileBuffer()) << "Error file buffer not NULL";

	ASSERT_EQ(NULL, parentProc.GetInputFileBuffer()) << "Input file buffer not NULL";
}

TEST(ProcessControllerTests, DivideByZero) {
	ParentCrash mentor;
	ChildDivideZero badKid;
	ProcessController governor( &mentor, &badKid );

	governor.ConfigureStdoutPipe(ProcessController::NONE);
	governor.ConfigureStderrPipe(ProcessController::NONE);

	ASSERT_NO_THROW( governor.Run() ) << "ProcessController threw exception";

	ASSERT_FALSE( governor.ChildExitNormally() ) << "Child exited normally!";
	ASSERT_EQ( SIGFPE, governor.GetChildSignalNumber() ) << "Child process got wrong signal";
	ASSERT_FALSE( mentor.timeout ) << "Child process timed-out";
	ASSERT_TRUE( mentor.recvdData.empty() ) << "Parent process received data!";
}

TEST(ProcessControllerTests, HangForever) {
	ParentCrash mentor;
	ChildHang badKid;
	ProcessController governor( &mentor, &badKid );

	governor.ConfigureStdoutPipe(ProcessController::NONE);
	governor.ConfigureStderrPipe(ProcessController::NONE);

	ASSERT_NO_THROW( governor.Run() ) << "ProcessController threw exception";

	ASSERT_FALSE( governor.ChildExitNormally() ) << "Child exited normally!";
	ASSERT_EQ( SIGKILL, governor.GetChildSignalNumber() ) << "Child process got wrong signal";
	ASSERT_TRUE( mentor.timeout ) << "Child process timed-out";
	ASSERT_TRUE( mentor.recvdData.empty() ) << "Parent process received data!";
}

TEST(ProcessControllerTests, NullPtr) {
	ParentCrash mentor;
	ChildNullPtr badKid;
	ProcessController governor( &mentor, &badKid );

	governor.ConfigureStdoutPipe(ProcessController::NONE);
	governor.ConfigureStderrPipe(ProcessController::NONE);

	ASSERT_NO_THROW( governor.Run() ) << "ProcessController threw exception";

	ASSERT_FALSE( governor.ChildExitNormally() ) << "Child exited normally!";
	ASSERT_EQ( SIGSEGV, governor.GetChildSignalNumber() ) << "Child process got wrong signal";
	ASSERT_FALSE( mentor.timeout ) << "Child process timed-out";
	ASSERT_TRUE( mentor.recvdData.empty() ) << "Parent process received data!";
}

TEST(ProcessControllerTests, Abort) {
	ParentCrash mentor;
	ChildAbort badKid;
	ProcessController governor( &mentor, &badKid );

	governor.ConfigureStdoutPipe(ProcessController::NONE);
	governor.ConfigureStderrPipe(ProcessController::NONE);

	ASSERT_NO_THROW( governor.Run() ) << "ProcessController threw exception";

	ASSERT_FALSE( governor.ChildExitNormally() ) << "Child exited normally!";
	ASSERT_EQ( SIGABRT, governor.GetChildSignalNumber() ) << "Child process got wrong signal";
	ASSERT_FALSE( mentor.timeout ) << "Child process timed-out";
	ASSERT_TRUE( mentor.recvdData.empty() ) << "Parent process received data!";
}
