/* 
 * File:   SortingOfficeTests.cpp
 * Author: aga2hi
 *
 * Created on 10th June 2015, 3:29:17 PM
 */

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Library functions

#include <stdio.h>
#include <unistd.h>
#include <error.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <cassert>

// Google Test functions

#include <gtest/gtest.h>

// Project functions

#include "SortingOffice.h"

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Global Variables

typedef void Sigfunc(int);
class MsgCapture;

SortingOffice * postBox;
MsgCapture * msgCheck;
const size_t NumberOfPipes(31);
const size_t PipeSize(2);
int testPipeArray[ NumberOfPipes ][ PipeSize ];
bool testPipeInitialisedCondition[ NumberOfPipes ];
const unsigned int TIMEOUT(30000);
bool SortingOfficeStoppedBySignal(false);
bool SortingOfficeExitActive(false);
bool SortingOfficeExitRegistered(false);

#define CREATE_PIPE( ind )       pipe( testPipeArray[ ind ] )
#define READ_END_OF_PIPE( ind )  testPipeArray[ ind ][ 0 ]
#define WRITE_END_OF_PIPE( ind ) testPipeArray[ ind ][ 1 ]

pthread_t SortingOffice_Thread;

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Thread function

void *
SortingOffice_ThreadFunction(void *) {
    const unsigned int waitus(10000);
    for (size_t ind = 0; ind < NumberOfPipes; ++ind) {
        usleep(waitus);
        // This is where we write a message to the pipe
        std::stringstream message;
        message << ind;
        write(WRITE_END_OF_PIPE(ind), message.str().c_str(), message.str().length());
    } //  End for

    postBox->Stop();
    return NULL;
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Exit handler

void
cleanExit(void) {

	// We can't deregister an exit handler, so we play with flags

	if( SortingOfficeExitActive == false )
		return;

	// Close the pipes

    for (size_t ind(0); ind < NumberOfPipes; ++ind) {
        if (true == testPipeInitialisedCondition[ ind ]) {
            close(READ_END_OF_PIPE(ind));
            close(WRITE_END_OF_PIPE(ind));
        } //  Endif
    } //  Endfor

	// Deregister the signal handlers

    signal(SIGUSR1, SIG_DFL);
    signal(SIGUSR2, SIG_DFL);
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Signal handlers

void
stopSortingOffice(int) {
    // Runs in a signal handler
    pthread_cancel(SortingOffice_Thread);
    postBox->Stop();
    SortingOfficeStoppedBySignal = true;
}

void
DoNothingMuch(int) {
    // No surprise, do nothing (in a signal handler)
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

struct MsgCapture : SortingOffice::Functor {
    MsgCapture();

    virtual ~MsgCapture() {
    }

    virtual void operator()(int);
    virtual void check(bool & testPass);
private:
    bool testArray[ NumberOfPipes ];
};

MsgCapture::MsgCapture() {
    for (size_t ind = 0; ind < NumberOfPipes; ++ind) {
        testArray[ ind ] = false;
    }
}

void
MsgCapture::check(bool & testPass) {
    testPass = true;
    for (size_t ind = 0; ind < NumberOfPipes; ++ind) {
        if (false == testArray[ ind ]) {
            testPass = false;
        }
    }
}

void
MsgCapture::operator()(int fileDescriptor) {
    char line[ 256 ];
    const ssize_t lineLength = read(fileDescriptor, line, 256);
    line[ lineLength ] = '\0';
    const size_t index = static_cast<size_t> (atoi(line));
    if (index < NumberOfPipes) {
        testArray[ index ] = true;
    } else {
        throw std::runtime_error("Message from pipe not within range");
    }
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Simple C++ Test Suite
 */

TEST(SortingOfficeTests, Initialise) {

    msgCheck = new MsgCapture;
    postBox = new SortingOffice;

    // Register the exit function, if it hasn't been already
	// (Linux won't deregister an exit function and will let
	// you register more than one, so include our own check)

	SortingOfficeExitActive = true;
	if( SortingOfficeExitRegistered == false ) {
		const int atExitReturn = atexit(cleanExit);
		ASSERT_EQ(0, atExitReturn) << "atexit() failed";
		SortingOfficeExitRegistered = true;
	}

    // Register signal handlers
    // There is a better way to register signals.  The example given
    // figure 10.18 on page 328 of Advanced Programming in the UNIX
    // Environment shows how to register a signal in such a way that
    // it restarts system calls.

    Sigfunc * const usr1Return = signal(SIGUSR1, stopSortingOffice);
    Sigfunc * const usr2Return = signal(SIGUSR2, DoNothingMuch);
    ASSERT_NE(SIG_ERR, usr1Return);
    ASSERT_NE(SIG_ERR, usr2Return);

    // Initialise the threads

    pthread_attr_t * const SortingOffice_ThreadAttr = NULL;
    void * const SortingOffice_ThreadFunctionArgument = NULL;
    const int threadCreateCheck = pthread_create(& SortingOffice_Thread,
            SortingOffice_ThreadAttr,
            SortingOffice_ThreadFunction,
            SortingOffice_ThreadFunctionArgument);
    ASSERT_EQ(0, threadCreateCheck) << "pthread_create() failed";

    // Initialise the pipes

    for (size_t ind = 0; ind < NumberOfPipes; ++ind) {
        const int pipeReturn = CREATE_PIPE(ind);
        ASSERT_EQ(0, pipeReturn) << "CREATE_PIPE() failed";
        testPipeInitialisedCondition[ ind ] = true;
    } //  Endfor
}

TEST(SortingOfficeTests, Registration) {

    // An exception should be raised when trying to deregister a
    // handler from an empty sorting office -- make sure it is

	ASSERT_THROW(postBox->DeregisterHandler(0xDEADBEEF), std::runtime_error) <<
		"Deregistering a handler from an empty sorting office "
		"should have thrown an exception but did not";

    for (size_t ind = 0; ind < NumberOfPipes; ++ind) {
        // Register a handler once..

        postBox->RegisterHandler(READ_END_OF_PIPE(ind), msgCheck);

        // And then try to register it again... and expect an exception
        // to be raised

		ASSERT_THROW(postBox->RegisterHandler(READ_END_OF_PIPE(ind), msgCheck), std::runtime_error) <<
			"Registering same handler twice should have raised an exception but did not";

        // Deregister the handler once ...

        SortingOffice::Callback handler;
        handler = postBox->DeregisterHandler(READ_END_OF_PIPE(ind));
		ASSERT_EQ(msgCheck, handler) << "Deregister() did not return correct handler";

        // And then try to deregister the same handler again... and expect
        // an exception to be raised

		ASSERT_THROW(postBox->DeregisterHandler(READ_END_OF_PIPE(ind)), std::runtime_error) <<
			"DeregisterHandler() allowed us to deregister the "
			"same handler twice and should not";

        // Register the handler again and leave it there.  This time
        // there should be no exception

        ASSERT_NO_THROW(postBox->RegisterHandler(READ_END_OF_PIPE(ind), msgCheck)) << "RegisterHandler() failed";
    }
}

TEST(SortingOfficeTests, DeliverMessages) {

    bool testPass(false);
    SortingOffice::DeliveryCode whatHappenned;

	// Start the sorting office

    whatHappenned = postBox->DeliverMessages(TIMEOUT);
	ASSERT_EQ(SortingOffice::STOPPED, whatHappenned) << "DeliverMessages() did not finish in expected manner";

	// Find out what happened

    msgCheck->check(testPass);
    if (SortingOfficeStoppedBySignal) {
		ASSERT_FALSE(testPass) << "some messages were not delivered correctly";
    } else {
		ASSERT_TRUE(testPass) << "some messages were not delivered correctly";
    }
}

TEST(SortingOfficeTests, Cleanup) {

    void * threadRetVal;
    const int threadCheck = pthread_join(SortingOffice_Thread, &threadRetVal);

	ASSERT_EQ(0, threadCheck) << "thread did not return correctly";

	// The exit handler closes the pipes and deregisters the signal handlers

	cleanExit();

	// We can't deregister an exit handler, so we play with flags

	SortingOfficeExitActive = false;

	// Return the heap memory

    delete postBox;
    delete msgCheck;
}
