/* 
 * File:   FDBufferTests.cpp
 * Author: aga2hi
 *
 * Created on 10 June 2015, 7:57:47 PM
 */


#include <gtest/gtest.h>

#include <unistd.h>  // int pipe(int fd[2]); -1 on error, 0 if OK
#include <cassert>
#include <cstring>
#include <stdlib.h>
#include <iostream>
#include <vector>

#include "FileDescriptorBuffer.h"

#define LINE_LENGTH 256
#define WRITE_END 1
#define READ_END  0

/*
 * Test Classes
 */

struct IncomingLine : FileDescriptorBuffer::Functor {

    virtual void operator()(std::string & incomingLine) {
        savedLine = incomingLine;
    };

    std::string savedLine;
};

struct IncomingText : FileDescriptorBuffer::Functor {

	virtual void operator()(std::string & incomingLine) {
		savedText.push_back(incomingLine);
	};

	std::vector<std::string> savedText;
};

struct FileDescriptorBufferTest : FileDescriptorBuffer {
    FileDescriptorBufferTest(Callback _clientFunctor);

    virtual ~FileDescriptorBufferTest() {
    }

    virtual ssize_t fetchInputData(const int fd, char buffer[], const size_t bufferSize);

    const char * pointer;
    size_t chunk;
};

FileDescriptorBufferTest::FileDescriptorBufferTest(Callback _clientFunctor)
: FileDescriptorBuffer(_clientFunctor) {
}

ssize_t
FileDescriptorBufferTest::fetchInputData(const int, char buffer[], const size_t) {
    std::strncpy(buffer, pointer, chunk);
    pointer += chunk;
    return chunk;
}

/*
 * Global Variables
 */

IncomingLine ReadLine;
FileDescriptorBuffer GetLine(&ReadLine);
int Pipe[2];
const char HalfLine[] = "ONE ";
const char OneLine[] = "LINE\n";
const char FirstExpectedLine[] = "ONE LINE";
const char SecondLine[] = "TWO LINES\n";
const char SecondExpectedLine[] = "TWO LINES";
const char EmptyLine[] = "\n";
const char ThirdExpectedLine[] = "";

/*
 * Tests
 */

TEST(FileDescriptorBufferTest, Initialise) {

    const int pStatus = pipe(Pipe);
    ASSERT_EQ(0, pStatus) << "pipe() creation failed";;

    const ssize_t wStatus = write(Pipe[WRITE_END], HalfLine, std::strlen(HalfLine));
    ASSERT_LT(0, wStatus) << "write() to pipe failed";
}

TEST(FileDescriptorBufferTest, TestFirstPartialLine) {

    // Read what is in the pipe and process it line by line --
    // however no complete line has been written to it yet, so
    // the callback should not have been called.

    GetLine(Pipe[READ_END]);

	ASSERT_NE(false, ReadLine.savedLine.empty());

    // Complete the line, so that the next read of the pipe
    // should pick up a whole line

    const ssize_t wStatus = write(Pipe[WRITE_END], OneLine, std::strlen(OneLine));
    ASSERT_LT(0, wStatus);
}

TEST(FileDescriptorBufferTest, TestFirstCompleteLine) {

    // We now should have one whole line in the pipe

    GetLine(Pipe[READ_END]);

	ASSERT_FALSE(ReadLine.savedLine.empty()) << "FileDescriptorHandler should have picked up "
		"a whole line, but savedLine is empty";

	// Make sure the saved line has the right value

	std::string expectedLine(FirstExpectedLine);
	ASSERT_EQ(expectedLine, ReadLine.savedLine) << "FileDescriptorHandler did not pick up the first line correctly.";

    // Now write a whole line in one go

    const ssize_t wStatus = write(Pipe[WRITE_END], SecondLine, std::strlen(SecondLine));
    ASSERT_GT(wStatus, 0);
}

TEST(FileDescriptorBufferTest, TestSecondLine) {

    // We should have a whole line ready to read

    GetLine(Pipe[READ_END]);

	ASSERT_FALSE(ReadLine.savedLine.empty()) << "FileDescriptorHandler should have picked up "
		"a whole line, but savedLine is empty";

	// Make sure the saved line has the right value

	std::string expectedLine(SecondExpectedLine);
	ASSERT_EQ(expectedLine, ReadLine.savedLine) << "FileDescriptorHandler did not pick up the "
		"first line correctly";

    // Write an empty line -- should clear the contents of saved line and leave
    // it empty

    const ssize_t wStatus = write(Pipe[WRITE_END], EmptyLine, std::strlen(EmptyLine));
    ASSERT_GT(wStatus, 0);
}

TEST(FileDescriptorBufferTest, ThirdEmptyLine) {

    // We should get a line with nothing in it, but a line nevertheless
    // (not the same as an empty line!)

    GetLine(Pipe[READ_END]);

	ASSERT_NE(false, ReadLine.savedLine.empty()) << "FileDescriptorHandler should have picked up "
		"a line (with nothing in it)";

	// Make sure the saved line has the right value

	std::string expectedLine(ThirdExpectedLine);
	ASSERT_EQ(expectedLine, ReadLine.savedLine) << "FileDescriptorHandler did not pick up the "
		"first line correctly";
}

TEST(FileDescriptorBufferTest, RepeatedReads) {

	// Test text

	const std::string SourceText = 
		"Running main() from gtest_main.cc\n"
		"[==========] Running 3 tests from 1 test case.\n"
		"[----------] Global test environment set-up.\n"
		"[----------] 3 tests from TrivialTest\n"
		"[ RUN      ] TrivialTest.Initialise\n"
		"[       OK ] TrivialTest.Initialise (0 ms)\n"
		"\n"
		"[ RUN      ] TrivialTest.Check\n"
		"/home/views/aga2hi_AI_PRJ_CF3_BASE_SW_14.0F47.vws/ai_osal_linux/components/RegressionTestFramework/New/GTestSelfTest/DummyTestCases/DummyTest.cpp:9: Failure\n"
		"Value of: 0\n"
		"Expected: 1\n"
		"Supposed to fail\n"
		"[  FAILED  ] TrivialTest.Check (1 ms)\n"
		"\n"
		"[ RUN      ] TrivialTest.Shutdown\n"
		"[       OK ] TrivialTest.Shutdown (0 ms)\n"
		"\n"
		"[----------] 3 tests from TrivialTest (3192 ms total)\n"
		"\n"
		"[----------] Global test environment tear-down\n"
		"[==========] 3 tests from 1 test case ran. (3194 ms total)\n"
		"[  PASSED  ] 2 tests.\n"
		"[  FAILED  ] 1 test, listed below:\n"
		"[  FAILED  ] TrivialTest.Check\n"
		"\n"
		" 1 FAILED TEST\n";

	// Test harness

    IncomingText ReadText;
    FileDescriptorBufferTest GetText(&ReadText);

    // Variables needed to run the test.  Each time we go round the loop
	// We will have a new chunk size (buffersize)

    const int numTestRuns(SourceText.length() + 1);

    // Start of test

    for (int chunkSize = 1; chunkSize < numTestRuns; ++chunkSize) {

        GetText.pointer = SourceText.c_str();
        GetText.chunk = chunkSize;

        // By calling GetText.operator() we call
        // FileDescriptorBufferTest::fetchInputData()
        // until every byte in SourceText has been
        // consumed.

        size_t numWrites = SourceText.length() / chunkSize;
        const size_t lastChunk = SourceText.length() % chunkSize;

        for (; numWrites; --numWrites) {
            GetText(0);
        } //  End for

        if (lastChunk) {
            GetText.chunk = lastChunk;
            GetText(0);
        } //  End if

		// Now compare ReadText.savedText with SourceText

		EXPECT_EQ("Running main() from gtest_main.cc", ReadText.savedText.at(0));
		EXPECT_EQ("[==========] Running 3 tests from 1 test case.", ReadText.savedText.at(1));
		EXPECT_EQ("[----------] Global test environment set-up.", ReadText.savedText.at(2));
		EXPECT_EQ("[----------] 3 tests from TrivialTest", ReadText.savedText.at(3));
		EXPECT_EQ("[ RUN      ] TrivialTest.Initialise", ReadText.savedText.at(4));
		EXPECT_EQ("[       OK ] TrivialTest.Initialise (0 ms)", ReadText.savedText.at(5));
		EXPECT_EQ("", ReadText.savedText.at(6));
		EXPECT_EQ("[ RUN      ] TrivialTest.Check", ReadText.savedText.at(7));
		EXPECT_EQ("/home/views/aga2hi_AI_PRJ_CF3_BASE_SW_14.0F47.vws/ai_osal_linux/components/RegressionTestFramework/New/GTestSelfTest/DummyTestCases/DummyTest.cpp:9: Failure", ReadText.savedText.at(8));
		EXPECT_EQ("Value of: 0", ReadText.savedText.at(9));
		EXPECT_EQ("Expected: 1", ReadText.savedText.at(10));
		EXPECT_EQ("Supposed to fail", ReadText.savedText.at(11));
		EXPECT_EQ("[  FAILED  ] TrivialTest.Check (1 ms)", ReadText.savedText.at(12));
		EXPECT_EQ("", ReadText.savedText.at(13));
		EXPECT_EQ("[ RUN      ] TrivialTest.Shutdown", ReadText.savedText.at(14));
		EXPECT_EQ("[       OK ] TrivialTest.Shutdown (0 ms)", ReadText.savedText.at(15));
		EXPECT_EQ("", ReadText.savedText.at(16));
		EXPECT_EQ("[----------] 3 tests from TrivialTest (3192 ms total)", ReadText.savedText.at(17));
		EXPECT_EQ("", ReadText.savedText.at(18));
		EXPECT_EQ("[----------] Global test environment tear-down", ReadText.savedText.at(19));
		EXPECT_EQ("[==========] 3 tests from 1 test case ran. (3194 ms total)", ReadText.savedText.at(20));
		EXPECT_EQ("[  PASSED  ] 2 tests.", ReadText.savedText.at(21));
		EXPECT_EQ("[  FAILED  ] 1 test, listed below:", ReadText.savedText.at(22));
		EXPECT_EQ("[  FAILED  ] TrivialTest.Check", ReadText.savedText.at(23));
		EXPECT_EQ("", ReadText.savedText.at(24));
		EXPECT_EQ(" 1 FAILED TEST", ReadText.savedText.at(25));

		// Empty the store, otherwise we'll end up with a huge vector

		ReadText.savedText.clear();
	}  //  End for
}
