/* 
 * File:   GTestProcessControlTest.cpp
 * Author: aga2hi
 *
 * Created on 18 June 2015, 4:40:20 PM
 */

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Library functions

#include <cstdlib>
#include <iostream>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <stdexcept>
#include <vector>

// Google Test functions

#include <gtest/gtest.h>

// Project Functions

#include "GTestExecOutputHandler.h"
#include "GTestListOutputHandler.h"
#include "SortingOffice.h"

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Test Harness classes
 */

struct GTestResultsAdaptor : SortingOffice::Functor {
    GTestResultsAdaptor(GTestExecOutputHandler & adaptee, FILE * source);
    void operator()(int fileDesriptor);

    GTestExecOutputHandler & resultsParser;
    FILE * fromGoogleTest;
};

GTestResultsAdaptor::GTestResultsAdaptor(GTestExecOutputHandler& adaptee, FILE* source)
: fromGoogleTest(source), resultsParser(adaptee) {
}

void GTestResultsAdaptor::operator()(int) {
    char * fgetFeedback;

    static const std::size_t bufferSize = 256;
    char buffer[ bufferSize ];

    assert(fromGoogleTest != NULL);

    do {
        fgetFeedback = std::fgets(buffer, bufferSize, fromGoogleTest);
        if (fgetFeedback == buffer) {

            // Strip the newline character away

            char * const nl = std::strchr(buffer, '\n');
            if (NULL != nl) {
                *nl = '\0';
            }

            // Send each line to the output handler

            resultsParser.OnLineReceived(new std::string(buffer));
        }
    } while (feof(fromGoogleTest) == 0);
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Global Variables
 */

namespace {
    TestRunModel ResultsTestRun;
    GTestResultsAdaptor * OutputHandler;
    FILE * googleResults(NULL);
}

#define WRITE_END 1
#define READ_END  0

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Callback class, will be called for each new test result
 */

struct ResultList : GTestExecOutputHandler::Functor {

    ResultList() : mismatch(false) {
    }
    virtual void
    operator()(const std::string & TestCaseName,
            const std::string & TestName,
            int TestID,
            TEST_STATE state);

    std::vector<std::string> TestCaseNames;
    std::vector<std::string> TestNames;
    std::vector<int> TestIDs;
    std::vector<TEST_STATE> States;

    std::string RunningTestCase;
    std::string RunningTest;
    int RunningID;

    bool mismatch;
};

void
ResultList::operator()(const std::string & _testCaseName,
        const std::string & _testName,
        int _testID,
        TEST_STATE _state) {
    if (RUNNING == _state) {
        // Remember the name of the running test
        RunningTestCase = _testCaseName;
        RunningTest = _testName;
        RunningID = _testID;
    } else {
        // Store test result against test name and ID, as long
        // as names and ID are as expected
        if ((_testCaseName == RunningTestCase) && (_testName == RunningTest) && (_testID == RunningID)) {
            TestCaseNames.push_back(_testCaseName);
            TestNames.push_back(_testName);
            TestIDs.push_back(_testID);
            States.push_back(_state);
        } else {
            mismatch = true;
        }
    }
}

namespace {
    ResultList callbackTest;
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Google Test Suite
 */

TEST(GTestResultsParserTest, ListOutputTest) {

    GTestListOutputHandler* handler = new GTestListOutputHandler();

    handler->OnLineReceived(new std::string("Running main() from gtest_main.cc"));
    handler->OnLineReceived(new std::string("TestCase1."));
    handler->OnLineReceived(new std::string("  Test1"));
    handler->OnLineReceived(new std::string("  Test2"));
    handler->OnLineReceived(new std::string("TestCase2."));
    handler->OnLineReceived(new std::string("  Test1"));
    handler->OnLineReceived(new std::string("  Test2"));
    handler->OnLineReceived(new std::string("  Test3"));

    TestRunModel* model = handler->GetTestRunModel();

	ASSERT_EQ("TestCase1", model->GetTestCase(0)->GetName()) << "First test case was wrong";

	ASSERT_EQ("Test1", model->GetTestCase(0)->GetTest(0)->GetName()) << "First test name was wrong";

	ASSERT_EQ("Test2", model->GetTestCase(0)->GetTest(1)->GetName()) << "Second test name was wrong";

	ASSERT_EQ("TestCase2", model->GetTestCase(1)->GetName()) << "Second test case was wrong";

	ASSERT_EQ("Test1", model->GetTestCase(1)->GetTest(0)->GetName()) << "Third test name was wrong";

	ASSERT_EQ("Test2", model->GetTestCase(1)->GetTest(1)->GetName()) << "Fourth test name was wrong";

	ASSERT_EQ("Test3", model->GetTestCase(1)->GetTest(2)->GetName()) << "Fifth test name was wrong";

    delete model;
    delete handler;
}

TEST(GTestResultsParserTest, BadTestName) {

    GTestListOutputHandler* handler = new GTestListOutputHandler();
    handler->OnLineReceived(new std::string("  Test1"));

    TestRunModel* model = NULL;

    // We expect an exception to be thrown because we have introduced
    // a test name (Test1) without a test case

	ASSERT_THROW(model = handler->GetTestRunModel(), std::logic_error)
		<< "std::logic_error should have been thrown";

    delete handler;
}

TEST(GTestResultsParserTest, BadTestCase) {

    GTestListOutputHandler* handler = new GTestListOutputHandler();
    handler->OnLineReceived(new std::string("."));

    TestRunModel* model = NULL;

    // We expect an exception to be thrown because we have not described
    // either a valid test case or name

	ASSERT_THROW(model = handler->GetTestRunModel(), std::invalid_argument)
		<< "std::invalid_argument should have been thrown";

    delete handler;
}
TEST(GTestResultsParserTest, MessyListOutputTests) {

    GTestListOutputHandler* handler = new GTestListOutputHandler();
    handler->OnLineReceived(new std::string("   TestCase1.  "));
    handler->OnLineReceived(new std::string("Test1"));
    handler->OnLineReceived(new std::string(" Test2 \t     "));
    handler->OnLineReceived(new std::string(" \r    TestCase2.\t\r\n "));
    handler->OnLineReceived(new std::string("  \r\t     Test1   "));
    handler->OnLineReceived(new std::string(" Test2 "));
    handler->OnLineReceived(new std::string("Test3 "));

    handler->OnProcessTerminated(0);
    handler->WaitForOutputCompleted();

    TestRunModel* model = handler->GetTestRunModel();

	ASSERT_EQ("TestCase1", model->GetTestCase(0)->GetName())
		<< "First TestCase was wrong";

	ASSERT_EQ("Test1", model->GetTestCase(0)->GetTest(0)->GetName())
		<< "First TestName was wrong";

	ASSERT_EQ("Test2", model->GetTestCase(0)->GetTest(1)->GetName())
		<< "Second TestName was wrong";

	ASSERT_EQ("TestCase2", model->GetTestCase(1)->GetName())
		<< "Second TestCase was wrong";

	ASSERT_EQ("Test1", model->GetTestCase(1)->GetTest(0)->GetName())
		<< "Third TestName was wrong";

	ASSERT_EQ("Test2", model->GetTestCase(1)->GetTest(1)->GetName())
		<< "Fourth TestName was wrong";

	ASSERT_EQ("Test3", model->GetTestCase(1)->GetTest(2)->GetName())
		<< "Fifth TestName was wrong";

    delete model;
    delete handler;
}

TEST(GTestResultsParserTest, ResultsOutputTest) {

    GTestListOutputHandler* listHandler = new GTestListOutputHandler();
    listHandler->OnLineReceived(new std::string("TestCase1."));
    listHandler->OnLineReceived(new std::string("  Test1"));
    listHandler->OnLineReceived(new std::string("  Test2"));
    listHandler->OnLineReceived(new std::string("TestCase2."));
    listHandler->OnLineReceived(new std::string("  Test1"));
    listHandler->OnLineReceived(new std::string("  Test2"));
    listHandler->OnLineReceived(new std::string("  Test3"));

    listHandler->OnProcessTerminated(0);
    listHandler->WaitForOutputCompleted();

    TestRunModel* model = listHandler->GetTestRunModel();

    GTestExecOutputHandler* execHandler = new GTestExecOutputHandler(model);

    execHandler->ProcessTestResultLines();

    /* This is not exactly the GTest Syntax, except for the test results themselves. All below
    of the "========" - line should just be ignored by the output handler*/

    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase1.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test1 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase1.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test2 (2 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test1 (45 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test2"));
    execHandler->OnLineReceived(new std::string("[  FAILED  ] TestCase2.Test2 (3 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test3"));
    execHandler->OnLineReceived(new std::string("[  RESET   ] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[==========] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[=SUMMARY:=] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test1"));
    execHandler->OnLineReceived(new std::string("[  FAILED  ] TestCase2.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test3"));
    execHandler->OnLineReceived(new std::string("1 FAILED"));

	// Just make sure that these functions don't crash -- kept for compatibility
	// and should probably be removed now that results parsing does not run in
	// a separate thread

    execHandler->OnProcessTerminated(0);
    execHandler->WaitForProcessTestResultCompleted();

	// Find out what the parser has decided...

	ASSERT_EQ(1, model->GetTestCase(0)->GetTest(0)->GetPassedCount()) << "test 0, 0 had wrong passed count";

	ASSERT_EQ(0, model->GetTestCase(0)->GetTest(0)->GetFailedCount()) << "test 0, 0 had wrong failed count";

	ASSERT_EQ(1, model->GetTestCase(0)->GetTest(1)->GetPassedCount()) << "test 0, 1 had wrong passed count";

	ASSERT_EQ(0, model->GetTestCase(0)->GetTest(1)->GetFailedCount()) << "test 0, 1 had wrong failed count";

	ASSERT_EQ(1, model->GetTestCase(1)->GetTest(0)->GetPassedCount()) << "test 1, 0 had wrong passed count";

	ASSERT_EQ(0, model->GetTestCase(1)->GetTest(0)->GetFailedCount()) << "test 1, 0 had wrong failed count";

	ASSERT_EQ(0, model->GetTestCase(1)->GetTest(1)->GetPassedCount()) << "test 1, 1 had wrong passed count";

	ASSERT_EQ(1, model->GetTestCase(1)->GetTest(1)->GetFailedCount()) << "test 1, 1 had wrong failed count";

	ASSERT_EQ(0, model->GetTestCase(1)->GetTest(2)->GetPassedCount()) << "test 1, 2 had wrong passed count";

	ASSERT_EQ(0, model->GetTestCase(1)->GetTest(2)->GetFailedCount()) << "test 1, 2 had wrong failed count";

    delete model;
    delete execHandler;
    delete listHandler;
}

TEST(GTestResultsParserTest, CallbackOutputTest) {

	// Similar to ResultsOutputTest, but the GTestExecOutputHandler is instantiated differently
	// and we test the list of results in callbackTest

    GTestListOutputHandler* listHandler = new GTestListOutputHandler();
    listHandler->OnLineReceived(new std::string("TestCase1."));
    listHandler->OnLineReceived(new std::string("  Test1"));
    listHandler->OnLineReceived(new std::string("  Test2"));
    listHandler->OnLineReceived(new std::string("TestCase2."));
    listHandler->OnLineReceived(new std::string("  Test1"));
    listHandler->OnLineReceived(new std::string("  Test2"));
    listHandler->OnLineReceived(new std::string("  Test3"));

    listHandler->OnProcessTerminated(0);
    listHandler->WaitForOutputCompleted();

    TestRunModel* model = listHandler->GetTestRunModel();

    model->ResetIteration();
    model->ResetAllTestResults();

    GTestExecOutputHandler* execHandler = new GTestExecOutputHandler(model, &callbackTest);

    /* This is not exactly the GTest Syntax, except for the test results themselves. All below
    of the "========" - line should just be ignored by the output handler*/

    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase1.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test1 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase1.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test2 (2 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test1 (45 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test2"));
    execHandler->OnLineReceived(new std::string("[  FAILED  ] TestCase2.Test2 (3 ms)"));
    execHandler->OnLineReceived(new std::string("[ RUN      ] TestCase2.Test3"));
    execHandler->OnLineReceived(new std::string("[  RESET   ] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[==========] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[=SUMMARY:=] TestCase2.Test3 (0 ms)"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test1"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase1.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test1"));
    execHandler->OnLineReceived(new std::string("[  FAILED  ] TestCase2.Test2"));
    execHandler->OnLineReceived(new std::string("[       OK ] TestCase2.Test3"));
    execHandler->OnLineReceived(new std::string("1 FAILED"));

    // General test

	ASSERT_FALSE(callbackTest.mismatch) << "One or more test cases and/or tests did not match";


    // Test Case 1, Test 1

	ASSERT_EQ(PASSED, callbackTest.States[0]) << "result[0] was wrong";

	ASSERT_EQ("TestCase1", callbackTest.TestCaseNames[0]) << "test case[0] was wrong";

	ASSERT_EQ("Test1", callbackTest.TestNames[0]) << "test[0] was wrong";

	ASSERT_EQ(0, callbackTest.TestIDs[0]) << "test id[0] was wrong";


    // Test Case 1, Test 2

	ASSERT_EQ(PASSED, callbackTest.States[1]) << "result[1] was wrong";

	ASSERT_EQ("TestCase1", callbackTest.TestCaseNames[1]) << "test case[1] was wrong";

	ASSERT_EQ("Test2", callbackTest.TestNames[1]) << "test[1] was wrong";

	ASSERT_EQ(1, callbackTest.TestIDs[1]) << "test id[1] was wrong";


    // Test Case 2, Test 1

	ASSERT_EQ(PASSED, callbackTest.States[2]) << "result[2] was wrong";

	ASSERT_EQ("TestCase2", callbackTest.TestCaseNames[2]) << "test case[2] was wrong";

	ASSERT_EQ("Test1", callbackTest.TestNames[2]) << "test[2] was wrong";

	ASSERT_EQ(2, callbackTest.TestIDs[2]) << "test id[2] was wrong";


    // Test Case 2, Test 2

	ASSERT_EQ(FAILED, callbackTest.States[3]) << "result[3] was wrong";

	ASSERT_EQ("TestCase2", callbackTest.TestCaseNames[3]) << "test case[3] was wrong";

	ASSERT_EQ("Test2", callbackTest.TestNames[3]) << "test[3] was wrong";

	ASSERT_EQ(3, callbackTest.TestIDs[3]) << "test id[3] was wrong";


    // Test Case 2, Test 3

	ASSERT_EQ(RESET, callbackTest.States[4]) << "result[4] was wrong";

	ASSERT_EQ("TestCase2", callbackTest.TestCaseNames[4]) << "test case[4] was wrong";

	ASSERT_EQ("Test3", callbackTest.TestNames[4]) << "test[4] was wrong";

	ASSERT_EQ(4, callbackTest.TestIDs[4]) << "test id[4] was wrong";


    delete model;
    delete execHandler;
    delete listHandler;
}
