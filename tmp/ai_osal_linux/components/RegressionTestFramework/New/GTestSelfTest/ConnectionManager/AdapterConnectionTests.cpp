/*
 * AdapterConnectionTests.cpp
 *
 *  Created on: Jun 8, 2012
 *      Author: oto4hi
 */

#include <errno.h>
#include <gtest/gtest.h>
#include <AdapterConnection.h>

class MyReceiver : public IPacketReceiver {
	virtual void OnPacketReceive(IConnection* c, AdapterPacket* p) {};
};

TEST(AdapterConnectionTests, AdapterConnectionPushDecoratorThrowOnNullDecoratee)
{
	MyReceiver receiver;
	AdapterConnectionPushDecorator* connectionDecorator = NULL;

	EXPECT_THROW(connectionDecorator = new AdapterConnectionPushDecorator(NULL, &receiver), std::invalid_argument);

	AdapterConnection connection(1);
	EXPECT_THROW(connectionDecorator = new AdapterConnectionPushDecorator (&connection, NULL), std::invalid_argument);
}
