/* 
 * File:   GTestProcessControlTest.cpp
 * Author: aga2hi
 *
 * Created on 19 June 2015, 6:19:20 PM
 */

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Library functions

#include <stdexcept>

// Google Test functions

#include <gtest/gtest.h>

// Project Functions

#include <IPFilter.h>

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

TEST(IPFilterTests, MatchIPFilterExpectSuccess)
{
	IPFilter IpFilter("192.168.10.0/24");

	struct in_addr IPAddr;
	inet_aton("192.168.10.1", &IPAddr);

	EXPECT_TRUE(IpFilter.Matches(&IPAddr));
}

TEST(IPFilterTests, MatchIPFilterExpectFailure)
{
	IPFilter IpFilter("192.168.10.0/24");

	struct in_addr IPAddr;
	inet_aton("192.168.11.1", &IPAddr);

	EXPECT_FALSE(IpFilter.Matches(&IPAddr));
}

TEST(IPFilterTests, HandleIllegalFilterString1)
{
	EXPECT_THROW(new IPFilter("lalala"), std::invalid_argument);
}

TEST(IPFilterTests, HandleIllegalFilterString2)
{
	EXPECT_THROW(new IPFilter("192.168.10.11.0/24"), std::invalid_argument);
}

TEST(IPFilterTests, HandleIllegalFilterString3)
{
	EXPECT_THROW(new IPFilter("192.168.10.0/24foobar"), std::invalid_argument);
}

TEST(IPFilterTests, HandleIllegalFilterString4)
{
	EXPECT_THROW(new IPFilter(""), std::invalid_argument);
}

TEST(IPFilterTests, ExactMatchIPExpectSuccess)
{
	IPFilter IpFilter("192.168.10.1");

	struct in_addr IPAddr;
	inet_aton("192.168.10.1", &IPAddr);

	EXPECT_TRUE(IpFilter.Matches(&IPAddr));
}

TEST(IPFilterTests, ExactMatchIPExpectFailure)
{
	IPFilter IpFilter("192.168.10.1");

	struct in_addr IPAddr;
	inet_aton("192.168.10.2", &IPAddr);

	EXPECT_FALSE(IpFilter.Matches(&IPAddr));
}
