#include <errno.h>
#include <gtest/gtest.h>
#include <TestRunModel.h>
#include <stdexcept>

class TestRunModelTests : public ::testing::Test {
protected:
	TestRunModel* GenerateComplexTestRunModel();
};

TestRunModel* TestRunModelTests::GenerateComplexTestRunModel()
{
	TestRunModel* runModel = new TestRunModel();

	TestCaseModel* testCase1 = new TestCaseModel("TestCase1");
	TestCaseModel* testCase2 = new TestCaseModel("TestCase2");
	TestCaseModel* testCase3 = new TestCaseModel("TestCase3");

	TestModel* test1 = new TestModel("Test1",0);
	TestModel* test2 = new TestModel("Test2",1);
	TestModel* test3 = new TestModel("Test3",2);
	TestModel* test4 = new TestModel("Test4",3);
	TestModel* test5 = new TestModel("Test5",4);
	TestModel* test6 = new TestModel("Test6",5);

	testCase1->AddTest(test1);
	testCase1->AddTest(test2);

	testCase2->AddTest(test3);
	testCase2->AddTest(test4);

	testCase3->AddTest(test5);
	testCase3->AddTest(test6);

	runModel->AddTestCase(testCase1);
	runModel->AddTestCase(testCase2);
	runModel->AddTestCase(testCase3);

	return runModel;
}

TEST_F(TestRunModelTests, AddTestCases)
{
    TestRunModel model;
    TestCaseModel* testCase1 = new TestCaseModel("TestCase1");
    TestCaseModel* testCase2 = new TestCaseModel("TestCase2");
    TestCaseModel* testCase3 = new TestCaseModel("TestCase3");
    
    model.AddTestCase(testCase1);
    model.AddTestCase(testCase2);
    model.AddTestCase(testCase3);
    
    EXPECT_EQ(3, model.GetTestCaseCount());
    
    EXPECT_EQ(testCase1, model[0]);
    EXPECT_EQ(testCase2, model[1]);
    EXPECT_EQ(testCase3, model[2]);
    
    EXPECT_EQ(testCase1, model["TestCase1"]);
    EXPECT_EQ(testCase2, model["TestCase2"]);
    EXPECT_EQ(testCase3, model["TestCase3"]);
}

TEST_F(TestRunModelTests, GetTestCaseInvalidIndex)
{
    TestRunModel model;
    
    EXPECT_THROW(model[0], std::invalid_argument);
    
    model.AddTestCase(new TestCaseModel("TestCase1"));
    model.AddTestCase(new TestCaseModel("TestCase2"));
    model.AddTestCase(new TestCaseModel("TestCase3"));
    
    EXPECT_NO_THROW(model[0]);
    EXPECT_THROW(model[3], std::invalid_argument);
    EXPECT_THROW(model[4], std::invalid_argument);
}

TEST_F(TestRunModelTests, GetTestCaseInvalidName)
{
    TestRunModel model;
    
    EXPECT_THROW(model["TestCase1"], std::invalid_argument);
    
    model.AddTestCase(new TestCaseModel("TestCase1"));
    model.AddTestCase(new TestCaseModel("TestCase2"));
    model.AddTestCase(new TestCaseModel("TestCase3"));
    
    EXPECT_NO_THROW(model["TestCase1"]);
    EXPECT_THROW(model["TestCase4"], std::invalid_argument);
}

TEST_F(TestRunModelTests, AddTestCaseNullArgument)
{
    TestRunModel model;
    EXPECT_THROW(model.AddTestCase(NULL), std::invalid_argument);
}

TEST_F(TestRunModelTests, AddAlreadyExistingTestCase)
{
    TestRunModel model;
    model.AddTestCase(new TestCaseModel("TestCase"));
    TestCaseModel* secondTestCase = new TestCaseModel("TestCase");
    EXPECT_THROW(model.AddTestCase(secondTestCase), std::runtime_error);
    delete secondTestCase;
}

TEST_F(TestRunModelTests, SerializeTestModel)
{
	TestRunModel* modelOrig = this->GenerateComplexTestRunModel();

	::GTestServiceBuffers::AllTestCases pbModel;

	modelOrig->ToProtocolBuffer(&pbModel);
	std::string serializedModel = pbModel.SerializeAsString();

	::GTestServiceBuffers::AllTestCases pbModel2;
	EXPECT_TRUE(pbModel2.ParseFromString(serializedModel));

	TestRunModel modelDeserialized(pbModel2);

	EXPECT_EQ(modelOrig->GetTestCaseCount(), modelDeserialized.GetTestCaseCount());

	for (int TestCaseIndex = 0; TestCaseIndex < modelOrig->GetTestCaseCount(); TestCaseIndex++)
	{
		TestCaseModel* testCaseOrig = modelOrig->GetTestCase(TestCaseIndex);
		TestCaseModel* testCaseDeserialized = modelDeserialized.GetTestCase(TestCaseIndex);

		EXPECT_EQ(testCaseOrig->GetName(), testCaseDeserialized->GetName());
		EXPECT_EQ(testCaseOrig->TestCount(), testCaseDeserialized->TestCount());

		for (int TestIndex = 0; TestIndex < testCaseOrig->TestCount(); TestIndex++)
		{
			TestModel* testOrig = testCaseOrig->GetTest(TestIndex);
			TestModel* testDeserialized = testCaseDeserialized->GetTest(TestIndex);

			EXPECT_EQ(testOrig->GetName(), testDeserialized->GetName());
			EXPECT_EQ(testOrig->GetPassedCount(), testDeserialized->GetPassedCount());
			EXPECT_EQ(testOrig->GetFailedCount(), testDeserialized->GetFailedCount());
		}
	}

	delete modelOrig;
}

TEST_F(TestRunModelTests, SerializeTestModelNullArguments)
{
	TestRunModel runModel;
	TestCaseModel testCaseModel("TestCase");
	TestModel testModel("Test",0);

	EXPECT_THROW(runModel.ToProtocolBuffer(NULL), std::invalid_argument);
	EXPECT_THROW(testCaseModel.ToProtocolBuffer(NULL), std::invalid_argument);
	EXPECT_THROW(testModel.ToProtocolBuffer(NULL), std::invalid_argument);
}

TEST_F(TestRunModelTests, CheckGenerateFilterStringAllEnabled)
{
	TestRunModel* runModel = this->GenerateComplexTestRunModel();

	std::string filterString = runModel->GenerateTestFilterString();
	EXPECT_EQ("TestCase1.*:TestCase2.*:TestCase3.*", filterString);

	delete runModel;
}

TEST_F(TestRunModelTests, CheckGenerateFilterStringFirstTestDisabled)
{
	TestRunModel* runModel = this->GenerateComplexTestRunModel();
	runModel->GetTestCase(0)->GetTest(0)->Enabled = false;

	std::string filterString = runModel->GenerateTestFilterString();
	EXPECT_EQ("TestCase1.Test2:TestCase2.*:TestCase3.*", filterString);

	delete runModel;
}

TEST_F(TestRunModelTests, CheckGenerateFilterStringSecondTestCaseDisabled)
{
	TestRunModel* runModel = this->GenerateComplexTestRunModel();
	runModel->GetTestCase(1)->GetTest(0)->Enabled = false;
	runModel->GetTestCase(1)->GetTest(1)->Enabled = false;

	std::string filterString = runModel->GenerateTestFilterString();
	EXPECT_EQ("TestCase1.*:TestCase3.*", filterString);

	delete runModel;
}

TEST_F(TestRunModelTests, CheckGenerateFilterStringAllTestsDisabled)
{
	TestRunModel* runModel = this->GenerateComplexTestRunModel();
	runModel->GetTestCase(0)->GetTest(0)->Enabled = false;
	runModel->GetTestCase(0)->GetTest(1)->Enabled = false;
	runModel->GetTestCase(1)->GetTest(0)->Enabled = false;
	runModel->GetTestCase(1)->GetTest(1)->Enabled = false;
	runModel->GetTestCase(2)->GetTest(0)->Enabled = false;
	runModel->GetTestCase(2)->GetTest(1)->Enabled = false;

	std::string filterString = runModel->GenerateTestFilterString();
	EXPECT_EQ("-*", filterString);

	delete runModel;
}

TEST_F(TestRunModelTests, CheckGenerateFilterStringAllTestCasesPartiallyEnabled)
{
	TestRunModel* runModel = this->GenerateComplexTestRunModel();
	runModel->GetTestCase(0)->GetTest(0)->Enabled = true;
	runModel->GetTestCase(0)->GetTest(1)->Enabled = false;
	runModel->GetTestCase(1)->GetTest(0)->Enabled = false;
	runModel->GetTestCase(1)->GetTest(1)->Enabled = true;
	runModel->GetTestCase(2)->GetTest(0)->Enabled = true;
	runModel->GetTestCase(2)->GetTest(1)->Enabled = false;

	std::string filterString = runModel->GenerateTestFilterString();
	EXPECT_EQ("TestCase1.Test1:TestCase2.Test4:TestCase3.Test5", filterString);

	delete runModel;
}
