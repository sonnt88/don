/* 
 * File:   GTestProcessControlTest.cpp
 * Author: aga2hi
 *
 * Created on 11 June 2015, 7:54:20 PM
 */

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// Library functions

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <iostream>
#include <vector>
#include <string>

// Google Test functions

#include <gtest/gtest.h>

// Project Functions

#include <GTestConfigurationModel.h>
#include <GTestProcessControl.h>
#include <ProcessController.h>
#include <FileDescriptorBuffer.h>
#include <SortingOffice.h>

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

#define TEST_PROG  "gtestselftestdummytest"
#define WORK_DIR   "/var/tmp"
#define FILTER     ""
#define REPEAT     0
#define RAND_SEED  0
#define EXEC_FLAGS GTEST_CONF_EXEC_TESTS
#define LIST_FLAGS GTEST_CONF_LIST_ONLY

#define LAST_LINE  " 1 FAILED TEST"

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Extra Classes
 */

struct GoogleOutputTest : FileDescriptorBuffer::Functor {

    GoogleOutputTest();
    ~GoogleOutputTest();

	void setOutputFileBuffer(FILE * _fb) {toGTest = _fb;}

    void operator()(std::string & incomingLine);

    std::vector<std::string> googleOutput;
	FILE * toGTest;
	int barLineCount;
	bool clientStillRunning;
};

GoogleOutputTest::GoogleOutputTest() : toGTest(NULL), barLineCount( 0 ), clientStillRunning( true ) {
}

GoogleOutputTest::~GoogleOutputTest() {
}

void
GoogleOutputTest::operator ()(std::string & incomingLine) {

	// Make sure that object has been set up correctly

	if( NULL == toGTest ) {
		throw std::runtime_error("GTestProcessControlTest.cpp; "
								 "GoogleOutputTest::operator (); toGTest not set up");
	}

	// Add incomingLine it to googleOutput

	googleOutput.push_back(incomingLine);

	// Send acknowledgement back to Google Test

	const std::string::size_type pass = incomingLine.find("[       OK ]");
	const std::string::size_type fail = incomingLine.find("[  FAILED  ]");

	if ((pass != std::string::npos) || (fail != std::string::npos)) {
		if( clientStillRunning ) {
			// We don't want to write to toGTest if there is nothing
			// listening to the pipe

			const int putStatus = fputc('\n', toGTest);

			if (putStatus == EOF) {
				perror("GoogleOutputTest::operator (); fputc() : ");
				throw std::runtime_error("GoogleOutputTest::operator (); fputc() failed");
			}  //  End if
		}  //  End if
	}  //  End if

	// Find out if the test has finished, which happens the second time
	// that Google test sends the string [==========]

	const std::string::size_type end = incomingLine.find("[==========]");

	if (end != std::string::npos) {
		++barLineCount;
		if( barLineCount == 2 ) {
			barLineCount = 0;
			clientStillRunning = false;
		}
	}
}

struct ParentTestFunctor : ProcessController::ParentFunctor {

	ParentTestFunctor(GoogleOutputTest * const _got);
	~ParentTestFunctor() {}

	void operator()(void);

	GoogleOutputTest * const googleOutput;
	SortingOffice::DeliveryCode soExitCode;
};

ParentTestFunctor::ParentTestFunctor(GoogleOutputTest * const _got)
	: googleOutput(_got) { }

void
ParentTestFunctor::operator()(void) {

	const unsigned int timeoutMilliseconds(10000);

	// It's only when we get here, inside the parent process,
	// when we know the output FILE *.  We couldn't have done
	// this from within the ParentTestFunctor's constructor.

	googleOutput->setOutputFileBuffer(GetOutputFileBuffer());

	// FileDescriptorBuffer provides line-orientated text from
	// the file descriptor dealt with by SortingOffice.  It is
	// necessary because the FILE * functions in the standard
	// library can not be used after the multiplexing functions
	// poll() or select().

	FileDescriptorBuffer bufferedIO(googleOutput);

	// SortingOffice associates file descriptors with handlers,
	// whose operator() function is called when there is data to
	// read.

	SortingOffice msgDistributor;
	msgDistributor.RegisterHandler(GetInputFileDescriptor(), &bufferedIO);

	// DeliverMessages uses poll() to listen to file descriptors
	// and calls bufferedUI.operator() when there is something
	// to read, or exits either when the file descriptor has hung-up,
	// which happens when the child process has finished, or
	// after a timeout.

	soExitCode = msgDistributor.DeliverMessages(timeoutMilliseconds);

	// Find out why we are not sorting messages any more

	if (SortingOffice::TIMEOUT == soExitCode) {
		// Child process is still running; put it out of its
		// misery

		TerminateChildProc();
	}
}

class ChildTestFunctorAdapter : public ProcessController::ChildFunctor {
    GTestProcessControl * gtpc;

public:

    ChildTestFunctorAdapter(GTestProcessControl * const adaptee) :
    gtpc(adaptee) {
    }

    ~ChildTestFunctorAdapter() {
        delete gtpc;
    }

    void operator()(void) {
        gtpc->Run();
    }
};

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Global Variables
 */

namespace {
	const int PATHLENGTH( 1024 );
	char Pathname[PATHLENGTH];

    GoogleOutputTest * gtestOutput_gp;
	ParentTestFunctor * dataController_gp;

	ChildTestFunctorAdapter * execTestsChildProcess_gp;
	ChildTestFunctorAdapter * listTestsChildProcess_gp;

    ProcessController * procController_gp;
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/*
 * Google Test Suite
 */

TEST(GTestProcessControlTests, InitialiseProgrammePath) {

	// The pathname is supposed to be held by the environment variable GTestPath
	// We need it so that we can look for TEST_PROG

	char const * const cwdCheck = getenv( "GTestPath" );

	ASSERT_FALSE(NULL == cwdCheck) << "getenv() did not work, test can't proceed";
	ASSERT_GT(PATHLENGTH, strlen(cwdCheck) + strlen(TEST_PROG) + 2)
		<< "pathname will be too long, test can't proceed";

	strcat(Pathname, cwdCheck);
	strcat(Pathname, "/");
	strcat(Pathname, TEST_PROG);

	fprintf(stderr, "Looking for test programme at %s\n", Pathname); // For test purposes
}

TEST(GTestProcessControlTests, InitialiseChildExecutor) {

	// Now create the objects we need to execute Google test in a child process

    static GTestConfigurationModel execModel(Pathname, WORK_DIR, FILTER, REPEAT, RAND_SEED, EXEC_FLAGS);
    ASSERT_NO_THROW(execTestsChildProcess_gp = new ChildTestFunctorAdapter(new GTestProcessControl(execModel)))
		<< "Exception thrown by: new ChildTestFunctorAdapter(new GTestProcessControl(execModel))";

	ASSERT_FALSE(NULL == execTestsChildProcess_gp) << "ChildTestFunctorAdapter was not instantiated";
}

TEST(GTestProcessControlTests, InitialiseParentExecutor) {

    // Now create the objects we need for the parent process

    ASSERT_NO_THROW(gtestOutput_gp = new GoogleOutputTest)
		<< "Exception thrown by: new GoogleOutputTest";

	ASSERT_FALSE(NULL == gtestOutput_gp) << "GoogleOutputTest was not instantiated";

	ASSERT_NO_THROW(dataController_gp = new ParentTestFunctor(gtestOutput_gp))
		<< "Exception thrown by: new ParentTestFunctor";

	ASSERT_FALSE(NULL == dataController_gp) << "ParentTestFunctor was not instantiated";
}

TEST(GTestProcessControlTests, RunExecuteTests) {

	ASSERT_FALSE(NULL == dataController_gp) << "Can't run google test without ParentTestFunctor";
	ASSERT_FALSE(NULL == execTestsChildProcess_gp) << "Can't run google test without ChildTestFunctorAdapter";

    ASSERT_NO_THROW(procController_gp = new ProcessController(dataController_gp, execTestsChildProcess_gp))
		<< "Exception thrown by: new ProcessController(dataController, googleTest)";

	ASSERT_FALSE(NULL == procController_gp) << "ProcessController was not instantiated";

    procController_gp->ConfigureStdoutPipe(ProcessController::LINE);
    procController_gp->ConfigureStdinPipe(ProcessController::LINE);

    // Now run the tests (We'll inspect the results in a later test)

    ASSERT_NO_THROW(procController_gp->Run()) << "Exception thrown by: ProcessController::Run()";

    const int expectedReturnCode(1);
    const int actualReturnCode(procController_gp->GetChildExitStatus());

	ASSERT_EQ(expectedReturnCode, actualReturnCode) << "Child process exit status wrong";
}

TEST(GTestProcessControlTests, TestExecOutput) {

	ASSERT_FALSE(NULL == gtestOutput_gp) << "Can't inspect test output with NULL gtestOutput_gp";

    std::string expectedLine[23];
    expectedLine[0] = "Running main() from gtest_main.cc";
    expectedLine[1] = "[==========] Running 3 tests from 1 test case.";
    expectedLine[2] = "[----------] Global test environment set-up.";
    expectedLine[3] = "[----------] 3 tests from TrivialTest";
    expectedLine[4] = "[ RUN      ] TrivialTest.Initialise";
    expectedLine[5] = "[       OK ] TrivialTest.Initialise (0 ms)";
    expectedLine[6] = "[ RUN      ] TrivialTest.Check";
    expectedLine[7] = "/home/aga2hi/Software/GTestProcessControl/GoogleTests/Dummy_Test.cpp:9: Failure";
    expectedLine[8] = "Value of: 0";
    expectedLine[9] = "Expected: 1";
    expectedLine[10] = "Supposed to fail";
    expectedLine[11] = "[  FAILED  ] TrivialTest.Check (0 ms)";
    expectedLine[12] = "[ RUN      ] TrivialTest.Shutdown";
    expectedLine[13] = "[       OK ] TrivialTest.Shutdown (0 ms)";
    expectedLine[15] = "";
    expectedLine[16] = "[----------] Global test environment tear-down";
    expectedLine[18] = "[  PASSED  ] 2 tests.";
    expectedLine[19] = "[  FAILED  ] 1 test, listed below:";
    expectedLine[20] = "[  FAILED  ] TrivialTest.Check";
    expectedLine[21] = "";
    expectedLine[22] = " 1 FAILED TEST";

    // Inspect gtestOutput

    for (int index = 0; index < 23; ++index) {
		// The following lines cannot be checked easily
        if (14 == index) continue;
        if (17 == index) continue;
		if (7 == index) continue;
		// With some of the lines, those with timing information, we can only test a substring
		if (5 == index ) {
			// Test first 35 characters - the remainder contain timing information
			const std::string incoming(gtestOutput_gp->googleOutput[index].substr(0, 35));
			const std::string expected(expectedLine[index].substr(0, 35));

			EXPECT_EQ(expected, incoming);
			if( expected != incoming)
				std::cerr << "GTestProcessControlTests.TestExecOutput; Line "
						  << index
						  << "\nExpected "
						  << expected
						  << "\nReceived "
						  << incoming
						  << "\n";
			continue;
		}
		if (11 == index) {
			// Test first 30 characters - the remainder contain timing information
			const std::string incoming(gtestOutput_gp->googleOutput[index].substr(0, 30));
			const std::string expected(expectedLine[index].substr(0, 30));

			EXPECT_EQ(expected, incoming);
			if( expected != incoming)
				std::cerr << "GTestProcessControlTests.TestExecOutput; Line "
						  << index
						  << "\nExpected "
						  << expected
						  << "\nReceived "
						  << incoming
						  << "\n";
			continue;
		}
		if (13 == index) {
			// Test first 33 characters - the remainder contain timing information
			const std::string incoming(gtestOutput_gp->googleOutput[index].substr(0, 33));
			const std::string expected(expectedLine[index].substr(0, 33));

			EXPECT_EQ(expected, incoming);
			if( expected != incoming)
				std::cerr << "GTestProcessControlTests.TestExecOutput; Line "
						  << index
						  << "\nExpected "
						  << expected
						  << "\nReceived "
						  << incoming
						  << "\n";
			continue;
		}
		// We can test this line though
		EXPECT_EQ(expectedLine[index], gtestOutput_gp->googleOutput[index]);
		if (expectedLine[index] != gtestOutput_gp->googleOutput[index]) 
			std::cerr << "GTestProcessControlTests.TestExecOutput; Line "
					  << index
					  << "\nExpected "
					  << expectedLine[index]
					  << "\nReceived "
					  << gtestOutput_gp->googleOutput[index]
					  << "\n";
    }
}

TEST(GTestProcessControlTests, CleanupExecData) {

    // Cleanup, just to make sure programme doesn't crash when deleting data objects

	delete execTestsChildProcess_gp;
	delete dataController_gp;
    delete procController_gp;
    delete gtestOutput_gp;

	execTestsChildProcess_gp = NULL;
	dataController_gp = NULL;
	procController_gp = NULL;
	gtestOutput_gp = NULL;
}

TEST(GTestProcessControlTests, InitialiseChildLister) {

    // Next, construct the child objects

    static GTestConfigurationModel listModel(
            Pathname, WORK_DIR, FILTER, REPEAT, RAND_SEED, GTEST_CONF_LIST_ONLY);

    ASSERT_NO_THROW(listTestsChildProcess_gp = new ChildTestFunctorAdapter(new GTestProcessControl(listModel)))
		<< "Exception thrown by: new ChildTestFunctorAdapter(new GTestProcessControl(listModel))";
	ASSERT_FALSE(NULL == listTestsChildProcess_gp) << "Could not instantiate ChildTestFunctorAdapter";
}

TEST(GTestProcessControlTests, InitialiseParentLister) {

    ASSERT_NO_THROW(gtestOutput_gp = new GoogleOutputTest)
		<< "Exception thrown by: new GoogleOutputTest";

	ASSERT_FALSE(NULL == gtestOutput_gp) << "Could not instantiate GoogleOutputTest";

	ASSERT_NO_THROW(dataController_gp = new ParentTestFunctor(gtestOutput_gp))
		<< "Exception thrown by: nw ParentTestFunctor";

	ASSERT_FALSE(NULL == dataController_gp) << "Could not instantiate ParentTestFunctor";
}

TEST(GTestProcessControlTests, RunListTests) {

	ASSERT_FALSE(NULL == dataController_gp) << "Can not run process without GoogleOutputTest";
	ASSERT_FALSE(NULL == listTestsChildProcess_gp) << "Can not run process without ChildTestFunctorAdapter";

    ASSERT_NO_THROW(procController_gp = new ProcessController(dataController_gp, listTestsChildProcess_gp))
		<< "Exception thrown by: new ProcessController(dataController_gp, listTestsChildProcess_gp)";

	ASSERT_FALSE(NULL == procController_gp) << "Could not instantiate ProcessController";

	// May seem strange, but dataController_gp contains a GoogleOutputTest
	// object, and it was designed to be used with RunExecuteTests and
	// expects to be able to send acknowledgement to stdout.  That's why
	// we need to configure both pipes.

    procController_gp->ConfigureStdoutPipe(ProcessController::LINE);
    procController_gp->ConfigureStdinPipe(ProcessController::LINE);

    // Now run the tests (We'll inspect the results in a later test)

	ASSERT_NO_THROW(procController_gp->Run()) << "procController_gp->Run() threw exception";

    const int expectedReturnCode(0);
    const int actualReturnCode(procController_gp->GetChildExitStatus());

	ASSERT_EQ(expectedReturnCode, actualReturnCode) << "Child process exit status wrong";
}

TEST(GTestProcessControlTests, TestListOutput) {

    std::string expectedLine[5];
    expectedLine[0] = "Running main() from gtest_main.cc";
    expectedLine[1] = "TrivialTest.";
    expectedLine[2] = "  Initialise";
    expectedLine[3] = "  Check";
    expectedLine[4] = "  Shutdown";

	// Sanity check

	ASSERT_FALSE(NULL == gtestOutput_gp) << "Can't read test results with NULL gtestOutput";

    // Inspect gtestOutput

    for (int index = 0; index < 5; ++index) {
		EXPECT_EQ(expectedLine[index], gtestOutput_gp->googleOutput[index]);
		if (expectedLine[index] != gtestOutput_gp->googleOutput[index]) 
			std::cerr << "GTestProcessControlTests.TestListOutput; Line "
					  << index
					  << "\nExpected "
					  << expectedLine[index]
					  << "\nReceived "
					  << gtestOutput_gp->googleOutput[index]
					  << "\n";
    }
}

TEST(GTestProcessControlTests, CleanupListData) {

    // Cleanup, just to make sure programme doesn't crash when deleting data objects

	delete execTestsChildProcess_gp;
	delete dataController_gp;
    delete procController_gp;
    delete gtestOutput_gp;
}
