#include <gtest/gtest.h>
#include <iostream>
#include <unistd.h>

TEST( TrivialTest, Initialise ) {
}

TEST( TrivialTest, Fail ) {
	EXPECT_EQ( 1, 0 ) << "Supposed to fail";
}

TEST( TrivialTest, Hang ) {
 HANG:
	sleep( 100 );
	goto HANG;
}

TEST( TrivialTest, DivZero ) {
	int i;
	for( i = -1; i < 2; ++i ) {
		const int j = 2/i;
	}
}

TEST( TrivialTest, NullPtr ) {
	int * const np = 0;
	*np = 666;
}

TEST( TrivialTest, Abort ) {
	abort();
}

TEST( TrivialTest, Shutdown ) {
}
