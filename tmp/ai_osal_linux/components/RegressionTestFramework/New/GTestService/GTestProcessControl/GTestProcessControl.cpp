/* 
 * File:   GTestProcessController.cpp
 * Author: oto4hi
 * 
 * Created on April 19, 2012, 2:26 PM
 */

#include "GTestProcessControl.h"
#include <stdexcept>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <limits.h>
#include <unistd.h>
#include <sys/poll.h>
#include <exception>

#include <sstream>

#include <assert.h>

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

GTestProcessControl::GTestProcessControl(GTestConfigurationModel Config) {
    this->config = new GTestConfigurationModel(Config);
}

GTestProcessControl::~GTestProcessControl() {
    delete this->config;
}

void
GTestProcessControl::freeArgumentList(char** argumentList) {
    int Index = 0;
    while (argumentList[Index]) {
        free(argumentList[Index]);
        Index++;
    }
    free(argumentList);
}

void
GTestProcessControl::appendToArgumentList(char*** pArgumentList,
        std::string ArgToAppend,
        int &length) {
    length++;
    (*pArgumentList) = (char**) realloc((*pArgumentList), sizeof (char*) * (length + 1));
    (*pArgumentList)[length - 1] = (char*) malloc(ArgToAppend.length() + 1);
    memcpy((*pArgumentList)[length - 1], ArgToAppend.c_str(), ArgToAppend.length() + 1);
    (*pArgumentList)[length] = NULL;
}

char**
GTestProcessControl::generateArgumentList() {
    char** argumentList = NULL;
    int listLength = 0;

    this->appendToArgumentList(&argumentList, this->config->GetGTestExecutable().c_str(), listLength);

    if (this->config->GetListOnlyFlag()) {
        this->appendToArgumentList(&argumentList, "--gtest_list_tests", listLength);
    } else {
        if (this->config->GetUsePersiFeaturesFlags())
            this->appendToArgumentList(&argumentList, "--gtest_use_persi_features", listLength);
        if (!this->config->GetFilter().empty())
            this->appendToArgumentList(&argumentList, "--gtest_filter=" + this->config->GetFilter(), listLength);
        if (this->config->GetShuffleFlag())
            this->appendToArgumentList(&argumentList, "--gtest_shuffle", listLength);
        if (this->config->GetRandomSeed()) {
            std::stringstream ss;
            ss << this->config->GetRandomSeed();
            this->appendToArgumentList(&argumentList, "--gtest_random_seed=" + ss.str(), listLength);
        }
        if (this->config->GetRunDisabledTestsFlag())
            this->appendToArgumentList(&argumentList, "--gtest_also_run_disabled_tests", listLength);
        if (this->config->GetBreakOnFailureFlag())
            this->appendToArgumentList(&argumentList, "--gtest_break_on_failure", listLength);
    }

    return argumentList;
}

void
GTestProcessControl::Run() {
    char absoluteGTestExecutablePath[PATH_MAX];
    const char* relativeGTestExecutablePath = this->config->GetGTestExecutable().c_str();

    if (realpath(relativeGTestExecutablePath, (char*) &absoluteGTestExecutablePath) !=
            (char*) &absoluteGTestExecutablePath) {
        throw std::runtime_error("GTestProcessControl::Run() - "
                "Cannot resolve GTest executable path.");
    }

    if (!this->config->GetWorkingDir().empty()) {
        if (mkdir(this->config->GetWorkingDir().c_str(), 0777) == -1) {
            if (errno != EEXIST) {
                std::stringstream sstream;
                sstream << "GTestProcessControl::Run() - "
                        << this->config->GetWorkingDir().c_str()
                        << " does not exist and cannot be created.";
                throw std::runtime_error(sstream.str());
            }
        }
    }

    char** argList = this->generateArgumentList();

    if (!this->config->GetWorkingDir().empty())
        chdir(this->config->GetWorkingDir().c_str());

    execvp(absoluteGTestExecutablePath, argList);
}
