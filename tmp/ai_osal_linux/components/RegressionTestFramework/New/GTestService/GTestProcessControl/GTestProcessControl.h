/* 
 * File:   GTestProcessController.h
 * Author: oto4hi
 *
 * Created on April 19, 2012, 2:26 PM
 */

#ifndef GTESTPROCESSCONTROL_H
#define	GTESTPROCESSCONTROL_H

#include "GTestConfigurationModel.h"
#include <string>
#include <map>
#include <vector>
#include <exception>
#include <stdexcept>

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

/**
 * \brief This class controls the actual execution of the GTest binary as a child process.
 * The output of the child process is read asynchronously and forwarded to the output handlers registered
 * for this instance of GTestProcessController
 */
class GTestProcessControl {
private:
    GTestConfigurationModel * config;

    char ** generateArgumentList();
    void freeArgumentList(char** argumentList);
    void appendToArgumentList(char*** pArgumentList,
            std::string ArgToAppend,
            int &length);

    GTestProcessControl(GTestProcessControl& orig) {
        throw std::runtime_error("copy constructor called");
    }

public:
    /**
     * \brief constructor
     * @param Config parameter configuration for the execution of the GTest binary
     */
    GTestProcessControl(GTestConfigurationModel Config);

    /**
     * \brief run the Google Test programme
     */
    void Run();

    /**
     * destructor
     */
    virtual ~GTestProcessControl();
};

#endif	/* GTESTPROCESSCONTROL_H */

