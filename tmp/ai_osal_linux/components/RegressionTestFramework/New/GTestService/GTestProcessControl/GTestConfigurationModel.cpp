/* 
 * File:   GTestConfigurationModel.cpp
 * Author: oto4hi
 * 
 * Created on April 19, 2012, 2:33 PM
 */

#include "GTestConfigurationModel.h"
#include <stdexcept>
#include <unistd.h>
#include <sstream>

GTestConfigurationModel::GTestConfigurationModel(
        std::string GTestExecutable,
        std::string WorkingDir,
        std::string Filter,
        int RepeatCount,
        int RandomSeed,
        int GTestConfFlags) {
    if (GTestExecutable.empty())
        throw std::invalid_argument("GTestConfigurationModel::GTestConfigurationModel() - "
            "GTestExecutable may not be empty.");

    if (access(GTestExecutable.c_str(), X_OK))
        throw std::runtime_error("The specified GTestExecuable does not exist "
            "or is not executable.");

    this->workingDir = WorkingDir;

    if (GTestConfFlags & GTEST_CONF_LIST_ONLY) {
        if (GTestConfFlags - GTEST_CONF_LIST_ONLY)
            throw std::invalid_argument(
                "GTestConfigurationModel::GTestConfigurationModel() - "
                "GTEST_CONF_LIST_ONLY "
                "forbids all other flags");

        this->gTestExecutable = GTestExecutable;
        this->filter = "";
        this->randomSeed = 0;
        this->repeatCount = 0;
        this->gtestFlags = GTestConfFlags;

    } else {
        if (GTestConfFlags & GTEST_CONF_EXEC_TESTS) {
            if (RandomSeed != 0 && !(GTestConfFlags & GTEST_CONF_SHUFFLE_TESTS))
                throw std::invalid_argument(
                    "GTestConfigurationModel::GTestConfigurationModel() - "
                    "Shuffle has to be set "
                    "if RandomSeed != 0.");

            if (RandomSeed < 0 || RandomSeed > 99999)
                throw std::invalid_argument(
                    "GTestConfigurationModel::GTestConfigurationModel() - "
                    "RandomSeed must be [0..99999].");

            this->gTestExecutable = GTestExecutable;
            this->filter = Filter;
            this->randomSeed = RandomSeed;
            this->repeatCount = RepeatCount;
            this->gtestFlags = GTestConfFlags;

        } else {
            std::stringstream msg;
            msg << "GTestConfigurationModel::GTestConfigurationModel() - ";
            msg << "Flags = ";
            msg << std::hex;
            msg << GTestConfFlags;
            msg << " \n";
            msg << "Flags & GTEST_CONF_EXEC_TESTS = ";
            msg << (GTestConfFlags & GTEST_CONF_EXEC_TESTS);
            msg << " \nBecause GTEST_CONF_EXEC_TESTS = ";
            msg << GTEST_CONF_EXEC_TESTS;
            msg << "\n";
            msg << std::dec;
            msg << "Either GTEST_CONF_EXEC_TESTS or "
                    "GTEST_CONF_LIST_ONLY have to be set.\n";

            throw std::invalid_argument(msg.str().c_str());
        }
    }
}

GTestConfigurationModel::GTestConfigurationModel(const GTestConfigurationModel& Other) {
    this->filter = Other.filter;
    this->gTestExecutable = Other.gTestExecutable;
    this->workingDir = Other.workingDir;
    this->randomSeed = Other.randomSeed;
    this->repeatCount = Other.repeatCount;
    this->gtestFlags = Other.gtestFlags;
}

GTestConfigurationModel::~GTestConfigurationModel() {
}

