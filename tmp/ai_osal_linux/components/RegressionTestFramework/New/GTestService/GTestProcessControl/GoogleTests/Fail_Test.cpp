#include <gtest/gtest.h>
#include <iostream>
#include <unistd.h>

TEST( TrivialTest, Initialise ) {
}

TEST( TrivialTest, Check ) {
	std::cerr << "stderr";
	EXPECT_EQ( 1, 0 ) << "Supposed to fail";
}

TEST( TrivialTest, Hang ) {
 HANG:
	sleep( 100 );
	goto HANG;
}

TEST( TrivialTest, Crash ) {
	int i;
	for( i = -1; i < 2; ++i ) {
		const int j = 2/i;
	}
}

TEST( TrivialTest, Shutdown ) {
}
