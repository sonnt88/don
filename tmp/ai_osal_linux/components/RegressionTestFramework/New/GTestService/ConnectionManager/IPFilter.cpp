/* 
 * File:   IPFilter.cpp
 * Author: aga2hi
 * 
 * Created on March 6, 2015, 6:36 PM
 */

#include "IPFilter.h"
#include <stdexcept>
#include <vector>
#include <sstream>
#include <cstring>
#include <string>
#include <cstdio>
#include <iostream>
#include <iomanip>

IPFilter::IPFilter(std::string IPFilterString) {
    std::vector<std::string> splitResult;
    std::stringstream stream(IPFilterString);
    std::string word;

    while (std::getline(stream, word, '/')) {
        splitResult.push_back(word);
    }

    if (splitResult.size() < 1 || splitResult.size() > 2) {
        throw std::invalid_argument(this->generateExceptionMessage(IPFilterString));
    }

    int retVal = inet_aton(splitResult[0].c_str(), &(this->ipaddr));
    this->fixedbits = 32;

    if (!retVal) {
        throw std::invalid_argument(this->generateExceptionMessage(IPFilterString));
    }

    if (splitResult.size() > 1) {
        if (!this->checkOnlyContainsDigits(splitResult[1])) {
            throw std::invalid_argument(this->generateExceptionMessage(IPFilterString));
        }

        std::stringstream isstream(splitResult[1]);
        isstream >> this->fixedbits;
        if (isstream.fail() || (this->fixedbits % 8) || (this->fixedbits < 0 || this->fixedbits > 32)) {

            throw std::invalid_argument(this->generateExceptionMessage(IPFilterString));
        }
    }

    this->ipFilterString = IPFilterString;
}

IPFilter::IPFilter(const IPFilter& orig) {

    std::memcpy(&(this->ipaddr), &(orig.ipaddr), sizeof (struct in_addr));
    this->fixedbits = orig.fixedbits;
    this->ipFilterString = orig.ipFilterString;
}

IPFilter::~IPFilter() {
}

std::string
IPFilter::generateExceptionMessage(std::string IPFilterString) {
    std::string message = "IPFilter::generateExceptionMessage() - ";
    message += IPFilterString;
    message.append(
            " does not seem to represent an IP filter.");

    return message;
}

bool
IPFilter::checkOnlyContainsDigits(std::string IntegerString) {
    bool isInteger = true;
    for (int index = 0; index < IntegerString.size(); index++) {
        if ('0' > IntegerString[index] || IntegerString[index] > '9') {

            isInteger = false;
        }
    }
    return isInteger;
}

std::string
IPFilter::GetIPFilterString() {

    return this->ipFilterString;
}

bool
IPFilter::Matches(struct in_addr *IPAddr) {
    in_addr_t mask = 0;
    for (int index = 0; index < 32; index++) {
        mask = mask << 1;
        if (index < this->fixedbits) {
            mask += 1;
        }
    }
    mask = htonl(mask);

#ifdef _DEBUG_ON_
    std::cerr << "IPFilter::Matches( ";
    std::cerr << ((0xFF000000 & IPAddr->s_addr) >> 24);
    std::cerr << ".";
    std::cerr << ((0xFF0000 & IPAddr->s_addr) >> 16);
    std::cerr << ".";
    std::cerr << ((0xFF00 & IPAddr->s_addr) >> 8);
    std::cerr << ".";
    std::cerr << (0xFF & IPAddr->s_addr);
    std::cerr << " )\n";

    std::cerr << "mask = ";
    std::cerr << std::hex << mask << std::dec;
    std::cerr << "\n";

    std::cerr << "this->ipaddr.s_addr = ";
    std::cerr << ((0xFF000000 & this->ipaddr.s_addr) >> 24);
    std::cerr << ".";
    std::cerr << ((0xFF0000 & this->ipaddr.s_addr) >> 16);
    std::cerr << ".";
    std::cerr << ((0xFF00 & this->ipaddr.s_addr) >> 8);
    std::cerr << ".";
    std::cerr << (0xFF & this->ipaddr.s_addr);
    std::cerr << "\n";
    
    std::cerr << "Address match : ";
    std::cerr << (IPAddr->s_addr == this->ipaddr.s_addr);
    std::cerr << "\n";
#endif

    if ((mask & (IPAddr->s_addr)) == (mask &(this->ipaddr.s_addr))) {
        return true;
    } else {
        return false;
    }
}

struct
in_addr IPFilter::GetIPAddr() {

    return this->ipaddr;
}

short
IPFilter::GetFixedBits() {
    return this->fixedbits;
}
