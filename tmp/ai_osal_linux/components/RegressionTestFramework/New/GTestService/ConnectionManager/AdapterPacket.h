/* 
 * File:   AdapterPacket.h
 * Author: aga2hi
 *
 * Created on March 5, 2015, 6:34 PM
 */

#ifndef ADAPTERPACKET_H
#define	ADAPTERPACKET_H

#include <stdlib.h>
#include <stdint.h>
#include "PacketIDs.h"
#include <arpa/inet.h>

// ////////// ////////// ////////// ////////// //////////

#define ADAPTER_PACKET_HEADER_SIZE 16
#define ADAPTER_PACKET_MAX_PAYLOAD_SIZE 0x100000
#define ADAPTER_PACKET_MAX_SIZE (ADAPTER_PACKET_HEADER_SIZE+ADAPTER_PACKET_MAX_PAYLOAD_SIZE)

// ////////// ////////// ////////// ////////// //////////

class AdapterPacketHeader {
public:
    AdapterPacketHeader(char* Buffer);

    AdapterPacketHeader(const AdapterPacketHeader& orig);

    AdapterPacketHeader(char ID,
            char Flags,
            uint32_t Serial,
            uint32_t RequestSerial,
            uint32_t PayloadSize);

    char id;
    char flags;
    uint32_t serial;
    uint32_t requestSerial;
    uint32_t payloadSize;
};

// ////////// ////////// ////////// ////////// //////////

class AdapterPacket {
public:
    AdapterPacket(AdapterPacketHeader Header,
            char* Payload,
            uint32_t connectionID);
    AdapterPacket(char ID,
            char Flags,
            uint32_t Serial,
            uint32_t RequestSerial,
            uint32_t PayloadSize,
            char* Payload,
            uint32_t connectionID);
    AdapterPacket(const AdapterPacket& orig);
    virtual ~AdapterPacket();

    char GetID();
    char GetFlags();
    uint32_t GetSerial();
    uint32_t GetRequestSerial();
    uint32_t GetPayloadSize();
    char* GetPayload();

private:
    friend class AdapterConnection;

    AdapterPacketHeader header;
    char* payload;
    void setPayload(char* Payload);

    uint32_t connectionID;
};

#endif	/* ADAPTERPACKET_H */

