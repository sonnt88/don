/* 
 * File:   IPFilter.h
 * Author: aga2hi
 *
 * Created on March 6, 2015, 6:36 PM
 */

#ifndef IPFILTER_H
#define	IPFILTER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>

class IPFilter {
public:
    IPFilter(std::string IPFilterString);
    IPFilter(const IPFilter& orig);
    virtual ~IPFilter();

    struct in_addr GetIPAddr();
    std::string GetIPFilterString();
    short GetFixedBits();
    bool Matches(struct in_addr *IPAddr);
private:

    std::string ipFilterString;
    std::string generateExceptionMessage(std::string IPFilterString);
    bool checkOnlyContainsDigits(std::string IntegerString);
    struct in_addr ipaddr;
    short fixedbits;
};

#endif	/* IPFILTER_H */

