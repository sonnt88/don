/* 
 * File:   ConnectionManager.h
 * Author: aga2hi
 *
 * Created on March 5, 2015, 4:25 PM
 */

#ifndef CONNECTIONMANAGER_H
#define	CONNECTIONMANAGER_H

#include <pthread.h>
#include <map>
#include <vector>
#include "IPFilter.h"
#include "AdapterConnection.h"

class ConnectionManager {
public:
    ConnectionManager(int Port,
            IConnectionFactory* ConnectionFactory,
            IPFilter* IpFilter);
    ConnectionManager(const ConnectionManager& orig);
    virtual ~ConnectionManager();

    void Start();
    void Stop();
    void Multicast(AdapterPacket packet);
    unsigned int GetConnectionCount();
    IConnection* operator[] (unsigned int Index);
    void DeleteConnection(IConnection* connection);

private:

    fd_set masterSet, readSet, writeSet;
    int maxSocket;

    IPFilter* ipFilter;

    IConnectionFactory* connectionFactory;

    std::map< int, IConnection* > activeConnections;
    std::vector< IConnection* > allConnections;

    pthread_t listenThread;
    pthread_mutex_t criticalSectionMutex;

    pthread_mutex_t startMutex;
    pthread_cond_t startCondition;

    pthread_mutex_t connectionMutex;

    bool running;
    bool stopAllThreads;
    bool listenSocketOpen;

    int port;

    void AcceptNewConnection(int ListenSocket, fd_set &MasterSet, int &MaxSocket);
    IConnection* GetAdapterConnectionBySock(int Sock);

    bool filterMatches(struct sockaddr_in *ClientAddr);

    int createNonBlockingSocket();
    void bindSocket(int Socket);
    void initSocketMasterSet(int ListenSocket);
    void setStartCondition();

    int setUpListenSocket();
    void receivePacketsInQueue(IConnection* Connection);
    void performSelect();
    void cleanupConnections();
    void stopListening(int ListenSocket);

    static void* listenThreadFunc(void* thisPtr);
    void beginCriticalSection();
    void endCriticalSection();

    void removeConnection(int Socket);
};

#endif	/* CONNECTIONMANAGER_H */

