/* 
 * File:   PacketIDs.h
 * Author: aga2hi
 *
 * Created on March 6, 2015, 11:18 AM
 */

#ifndef PACKETIDS_H
#define	PACKETIDS_H

#define RESPONSE(request) ((1<<7) + request)
#define SERVICE_PACKET(ID) ((1<<4) + ID)

#define ID_DOWN_GET_VERSION     0             /** required packet payload: <none> */
#define ID_DOWN_GET_STATE       1             /** required packet payload: <none> */
#define ID_DOWN_GET_TESTLIST    2             /** required packet payload: <none> */
#define ID_DOWN_RUN_TESTS       3             /** required packet payload: protobuf:TestSelection */
#define ID_DOWN_ABORT_TESTRUN   4             /** required packet payload: <none> */
#define ID_DOWN_GET_ALL_RESULTS 5             /** required packet payload: <none> */

#define ID_UP_STATE       	        RESPONSE(ID_DOWN_GET_STATE)           /** required packet payload: ubyte:enum GTEST_SERVICE_STATE, [uint32 Iteration, uint32 TestID] */
#define ID_UP_VERSION     	        RESPONSE(ID_DOWN_GET_VERSION)         /** required packet payload: string */
#define ID_UP_TESTLIST    	        RESPONSE(ID_DOWN_GET_TESTLIST)        /** required packet payload: protobuf:TestRunModel */
#define ID_UP_ACK_TESTRUN 	        RESPONSE(ID_DOWN_RUN_TESTS)           /** required packet payload: ubyte: 0=fail, 1=success(test started) */
#define ID_UP_ACK_ABORT     	    RESPONSE(ID_DOWN_ABORT_TESTRUN)       /** required packet payload: ubyte: 0=fail (e.g. nothing to abort), 1=success */
#define ID_UP_ALL_RESULTS_DONE      RESPONSE(ID_DOWN_GET_ALL_RESULTS)     /** required packet payload: <none> */

#define ID_UP_TESTRESULT  		SERVICE_PACKET(1)                         /** required packet payload: protobuf:TestResultModel */
#define ID_UP_REQUEST_POWER	    SERVICE_PACKET(2)                         /** required packet payload: protobuf:PowerDownRequest */
#define ID_UP_ALL_TESTS_DONE    SERVICE_PACKET(3)                         /** required packet payload: <none> */
#define ID_UP_TEST_OUTPUT       SERVICE_PACKET(4)                         /** required packet payload: protobuf:TestOutput */
#define ID_UP_PCYCLE_REQUEST    SERVICE_PACKET(5)                         /** required packet payload: <none> */


#endif	/* PACKETIDS_H */

