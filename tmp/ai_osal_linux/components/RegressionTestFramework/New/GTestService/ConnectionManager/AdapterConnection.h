/* 
 * File:   AdapterConnection.h
 * Author: aga2hi
 *
 * Created on March 5, 2015, 4:28 PM
 */

#ifndef ADAPTERCONNECTION_H
#define	ADAPTERCONNECTION_H


#include "AdapterPacket.h"
//#include <Utils/ThreadSafeQueue.h>

#include <pthread.h>
#include <stdexcept>

// ////////// ////////// ////////// ////////// //////////

class IConnection;
class IPacketReceiver;

// ////////// ////////// ////////// ////////// //////////

class IConnectionFactory {
public:
    virtual IConnection* CreateConnection(int Sock) = 0;

    virtual ~IConnectionFactory() {
    };
};

// ////////// ////////// ////////// ////////// //////////

class AdapterConnectionFactory : public IConnectionFactory {
private:
    IPacketReceiver* receiver;
public:
    AdapterConnectionFactory(IPacketReceiver* Receiver);

    virtual IConnection* CreateConnection(int Sock);

    virtual ~AdapterConnectionFactory();
};

// ////////// ////////// ////////// ////////// //////////

class IConnection {
public:
    virtual void Send(AdapterPacket& Data) = 0;
    virtual AdapterPacket* Receive() = 0;
    virtual void Close() = 0;
    virtual bool IsClosed() = 0;
    virtual int GetID() = 0;

    virtual ~IConnection() {
    };
};

// ////////// ////////// ////////// ////////// //////////

class AdapterConnection : public IConnection {
public:
    AdapterConnection(int Sock);
    virtual ~AdapterConnection();

    virtual void Send(AdapterPacket& Data);
    virtual AdapterPacket* Receive();
    virtual void Close();
    virtual bool IsClosed();
    virtual int GetID();

private:
    friend class ConnectionManager;

    unsigned int bytesStillToReceive;
    unsigned int currentReceiveBufferPosition;
    char receiveBuffer[ADAPTER_PACKET_MAX_SIZE];

    static uint32_t adapterConnectionCounter;
    static pthread_mutex_t connectionIDMutex;
    int connectionID;

    static uint32_t generatePacketSerial();
    static uint32_t currentPacketSerial;

    static pthread_mutex_t serialMutex;
    pthread_mutex_t socketMutex;

    int sock;
    bool closed;

    void internalClose();

    AdapterPacketHeader* ReceiveHeader();
    bool TryReceivePacketToTheEnd();

    int rawSend(char* Data, int Size);

    AdapterConnection(const AdapterConnection& orig) {
        throw std::runtime_error("copy constructor now callable for AdapterConnection");
    }
};

// ////////// ////////// ////////// ////////// //////////

class AdapterConnectionPushDecorator : public IConnection {
private:
    IConnection* decoratee;
    IPacketReceiver* receiver;
public:
    AdapterConnectionPushDecorator(IConnection* Decoratee,
            IPacketReceiver* Receiver);

    virtual void Send(AdapterPacket& Data);
    virtual AdapterPacket* Receive();
    virtual void Close();
    virtual bool IsClosed();
    virtual int GetID();

    virtual ~AdapterConnectionPushDecorator();
};

// ////////// ////////// ////////// ////////// //////////

class IPacketReceiver {
public:
    virtual void OnPacketReceive(IConnection* Connection,
            AdapterPacket* Packet) = 0;

    virtual ~IPacketReceiver() {
    };
};

// ////////// ////////// ////////// ////////// //////////

namespace GTestServiceExceptions {

    class ConnectionClosedException : public std::runtime_error {
    public:

        ConnectionClosedException(std::string const& msg) :
        std::runtime_error(msg) {
        }
    };

};

#endif	/* ADAPTERCONNECTION_H */

