/* 
 * File:   ConnectionManager.cpp
 * Author: aga2hi
 * 
 * Created on March 5, 2015, 4:25 PM
 */

#include "ConnectionManager.h"
#include "AdapterConnection.h"
#include "IPFilter.h"
#include <stdexcept>
#include <sys/ioctl.h>
#include <string.h>
#include <errno.h>
#include <algorithm>
#include <unistd.h>

#include <sstream>

ConnectionManager::ConnectionManager(int Port,
        IConnectionFactory* ConnectionFactory,
        IPFilter* IpFilter) {
    if (Port <= 0)
        throw std::invalid_argument("ConnectionManager::ConnectionManager() - "
                "Port must be greater than zero.");

    if (ConnectionFactory == NULL)
        throw std::invalid_argument("ConnectionManager::ConnectionManager() - "
                "ConnectionFactory cannot be null.");

    this->ipFilter = IpFilter;

    this->connectionFactory = ConnectionFactory;

    pthread_mutex_init(&(this->criticalSectionMutex), NULL);
    pthread_mutex_init(&(this->startMutex), NULL);
    pthread_cond_init(&(this->startCondition), NULL);
    pthread_mutex_init(&(this->connectionMutex), NULL);

    this->running = false;
    this->stopAllThreads = false;
    this->port = Port;
}

ConnectionManager::ConnectionManager(const ConnectionManager& orig) {
    // Neither defined nor declared in the original files
}

ConnectionManager::~ConnectionManager() {
    if (this->listenSocketOpen)
        throw std::runtime_error("ConnectionManager::~ConnectionManager() - "
                "listen socket still seems to be open. Did you forget to call Stop()?");

    delete this->connectionFactory;

    pthread_mutex_destroy(&(this->criticalSectionMutex));
    pthread_mutex_destroy(&(this->startMutex));
    pthread_cond_destroy(&(this->startCondition));
    pthread_mutex_destroy(&(this->connectionMutex));
}

void
ConnectionManager::beginCriticalSection() {
    pthread_mutex_lock(&(this->criticalSectionMutex));
}

void
ConnectionManager::endCriticalSection() {
    pthread_mutex_unlock(&(this->criticalSectionMutex));
}

IConnection*
ConnectionManager::GetAdapterConnectionBySock(int Sock) {
    std::map<int, IConnection* >::iterator it = this->activeConnections.begin();
    for (; it != this->activeConnections.end(); ++it) {
        if (it->first == Sock)
            return it->second;
    }
    throw std::invalid_argument("ConnectionManager::GetAdapterConnectionBySock() "
            "no such connection");
}

int
ConnectionManager::createNonBlockingSocket() {
    int reuseAddr = 1;
    int on = 1;
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock < 0)
        throw std::runtime_error("ConnectionManager::createNonBlockingSocket() "
            "Failed to create listening socket");

    if (0 > setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuseAddr, sizeof (reuseAddr)))
        throw std::runtime_error("ConnectionManager::createNonBlockingSocket() "
            "setsockopt failed");

    if (0 > ioctl(sock, FIONBIO, (char *) &on))
        throw std::runtime_error("ConnectionManager::createNonBlockingSocket() "
            "ioctl on listen socket failed");

    return sock;
}

void
ConnectionManager::bindSocket(int Socket) {
    struct sockaddr_in addr;

    memset(&addr, 0, sizeof (addr));

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(this->port);

    if (0 > bind(Socket, (struct sockaddr *) &addr, sizeof (addr)))
        throw std::runtime_error("ConnectionManager::bindSocket() "
            "bind failed");
}

bool
ConnectionManager::filterMatches(struct sockaddr_in *ClientAddr) {
    if (!(this->ipFilter))
        return true;

    return this->ipFilter->Matches(&(ClientAddr->sin_addr));
}

void
ConnectionManager::AcceptNewConnection(int ListenSocket,
        fd_set &MasterSet,
        int &MaxSocket) {
    int newSocket;
    IConnection* connection = NULL;

    do {
        struct sockaddr_in clientAddr;
        unsigned int clientAddrLen = sizeof (clientAddr);
        newSocket = accept(ListenSocket, (struct sockaddr*) &clientAddr, &clientAddrLen);

        if (newSocket < 0)
            break;

        if (this->filterMatches(&clientAddr)) {
            connection = this->connectionFactory->CreateConnection(newSocket);

            pthread_mutex_lock(&(this->connectionMutex));
            this->activeConnections.insert(std::pair< int, IConnection*>(newSocket, connection));
            this->allConnections.push_back(connection);
            pthread_mutex_unlock(&(this->connectionMutex));

            FD_SET(newSocket, &MasterSet);
            if (newSocket > MaxSocket)
                MaxSocket = newSocket;
        } else {
            close(newSocket);
        }

    } while (true);
}

void
ConnectionManager::removeConnection(int Socket) {
    FD_CLR(Socket, &(this->masterSet));
    if (Socket == this->maxSocket) {
        while (FD_ISSET(this->maxSocket, &(this->masterSet)) == false)
            this->maxSocket -= 1;
    }

    this->activeConnections.erase(Socket);
}

void
ConnectionManager::Multicast(AdapterPacket packet) {
    pthread_mutex_lock(&(this->connectionMutex));
    std::map<int, IConnection* >::iterator it = this->activeConnections.begin();
    for (; it != this->activeConnections.end(); ++it) {
        IConnection* connection = it->second;
        connection->Send(packet);
    }
    pthread_mutex_unlock(&(this->connectionMutex));
}

void
ConnectionManager::initSocketMasterSet(int ListenSocket) {
    FD_ZERO(&(this->masterSet));
    this->maxSocket = ListenSocket;
    FD_SET(ListenSocket, &(this->masterSet));
}

void
ConnectionManager::setStartCondition() {
    pthread_mutex_lock(&(this->startMutex));
    pthread_cond_signal(&(this->startCondition));
    pthread_mutex_unlock(&(this->startMutex));
}

int
ConnectionManager::setUpListenSocket() {
    int listenSocket = this->createNonBlockingSocket();
    this->bindSocket(listenSocket);
    this->listenSocketOpen = true;

    if (0 > listen(listenSocket, 32))
        throw std::runtime_error("ConnectionManager::setUpListenSocket() "
            "listen failed");

    return listenSocket;
}

void
ConnectionManager::performSelect() {
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 1000;

    memcpy(&(this->readSet), &(this->masterSet), sizeof (this->masterSet));
    memcpy(&(this->writeSet), &(this->masterSet), sizeof (this->masterSet));
    int rc = select(this->maxSocket + 1, &(this->readSet), &(this->writeSet), NULL, &timeout);

    if (rc < 0)
        throw std::runtime_error("ConnectionManager::performSelect() "
            "select failed");
}

void
ConnectionManager::cleanupConnections() {
    //close all remaining connections
    std::map<int, IConnection* >::iterator mit = this->activeConnections.begin();
    for (; mit != this->activeConnections.end(); ++mit)
        mit->second->Close();

    std::vector< IConnection* >::iterator vit = this->allConnections.begin();
    for (; vit != this->allConnections.end(); ++vit)
        delete *vit;

    this->activeConnections.clear();
    this->allConnections.clear();
}

void
ConnectionManager::stopListening(int ListenSocket) {
    if (close(ListenSocket))
        throw std::runtime_error("ConnectionManager::stopListening() "
            "close on listen socket failed.");

    this->listenSocketOpen = false;
}

void
ConnectionManager::receivePacketsInQueue(IConnection* Connection) {
    AdapterPacket* packet = NULL;
    do {
        if (packet)
            delete packet;
        try {
            packet = Connection->Receive();
        } catch (GTestServiceExceptions::ConnectionClosedException& e) {
            packet = NULL;
        }
    } while (packet);
}

void*
ConnectionManager::listenThreadFunc(void* thisPtr) {
    ConnectionManager* connectionManager = (ConnectionManager*) thisPtr;
    int listenSocket = connectionManager->setUpListenSocket();
    connectionManager->initSocketMasterSet(listenSocket);
    connectionManager->setStartCondition();

    do {
        connectionManager->performSelect();

        for (int sock = 0; sock <= connectionManager->maxSocket; ++sock) {
            if (sock == listenSocket) {
                if (FD_ISSET(sock, &(connectionManager->readSet)));
                connectionManager->AcceptNewConnection(listenSocket,
                        connectionManager->masterSet,
                        connectionManager->maxSocket);
            } else {
                IConnection* connection = NULL;

                try {
                    connection = connectionManager->GetAdapterConnectionBySock(sock);
                } catch (std::invalid_argument& e) {
                    continue;
                }

                if (FD_ISSET(sock, &(connectionManager->readSet))) {
                    connectionManager->receivePacketsInQueue(connection);
                }

                /*if (FD_ISSET(sock, &(connectionManager->writeSet)))
                {
                }*/

                if (connection->IsClosed())
                    connectionManager->DeleteConnection(connection);
            }
        }
    } while (!connectionManager->stopAllThreads);

    connectionManager->cleanupConnections();
    connectionManager->stopListening(listenSocket);

    pthread_exit(0);
}

void
ConnectionManager::DeleteConnection(IConnection* connection) {
    pthread_mutex_lock(&(this->connectionMutex));
    std::map<int, IConnection* >::iterator mit = this->activeConnections.begin();
    for (; mit != this->activeConnections.end(); ++mit) {
        if (mit->second == connection) {
            try {
                mit->second->Close();
            } catch (GTestServiceExceptions::ConnectionClosedException& e) {
                //can happen if the peer closes it's connection shortly before
                //the ConnectionManager does
            }
            this->removeConnection(mit->first);
            break;
        }
    }

    std::vector< IConnection* >::iterator vit = this->allConnections.begin();
    for (; vit != this->allConnections.end(); ++vit) {
        if (*vit == connection) {
            this->allConnections.erase(vit);
            delete connection;
            break;
        }
    }
    pthread_mutex_unlock(&(this->connectionMutex));
}

void
ConnectionManager::Start() {
    this->beginCriticalSection();

    if (this->running) {
        this->endCriticalSection();
        throw std::runtime_error("ConnectionManager::Start() "
                "Connection manager is already running.");
    }

    pthread_mutex_lock(&(this->startMutex));

    this->stopAllThreads = false;
    pthread_create(&(this->listenThread), NULL, this->listenThreadFunc, (void*) this);

    //wait for listen socket to be ready
    pthread_cond_wait(&(this->startCondition), &(this->startMutex));

    pthread_mutex_unlock(&(this->startMutex));

    this->endCriticalSection();
}

void
ConnectionManager::Stop() {
    this->stopAllThreads = true;
    int status;

    int errCode = pthread_join(this->listenThread, (void**) &status);
    if (errCode) {
        std::stringstream sstream;
        sstream << "ConnectionManager::Stop() ";
        sstream << "Joining ConnectionManager thread failed. error code is "
                << strerror(errCode);
        throw std::runtime_error(sstream.str().c_str());
    }
}

unsigned int
ConnectionManager::GetConnectionCount() {
    return this->activeConnections.size();
}

IConnection*
ConnectionManager::operator [](unsigned int Index) {
    if (this->allConnections.size() <= Index)
        return NULL;

    return this->allConnections[Index];
}
