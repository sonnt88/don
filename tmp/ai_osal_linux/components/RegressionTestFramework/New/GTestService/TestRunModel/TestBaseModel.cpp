/* 
 * File:   TestBaseModel.cpp
 * Author: oto4hi
 * 
 * Created on April 19, 2012, 7:27 PM
 */

#include "TestBaseModel.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <arpa/inet.h>
#include <string.h>

TestBaseModel::TestBaseModel(std::string Name) {
    if (Name.empty())
        throw std::invalid_argument("TestBaseModel::TestBaseModel() - "
            "The name of a test or test case  cannot be empty.");

    std::string disabledPrefix = "DISABLED_";

    if (!Name.compare(0, disabledPrefix.size(), disabledPrefix))
        this->defaultDisabled = true;
    else
        this->defaultDisabled = false;

    this->name = Name;
}

TestBaseModel::~TestBaseModel() {
}
