/* 
 * File:   TestModel.h
 * Author: oto4hi
 *
 * Created on April 19, 2012, 2:49 PM
 */

#ifndef TESTMODEL_H
#define	TESTMODEL_H

#include <map>
#include "TestBaseModel.h"
#include "GTestServiceBuffers.pb.h"

/**
 * \brief test data model
 */

typedef std::pair<int, bool> TestResultPair;

class TestModel : public TestBaseModel {
private:
    static const int MAX_RESULT_LIST_LENGTH = 1024;

    std::map<int, bool> testRunResults;
    int id;

    int passedCount;
    int currentTestRun;

    void resetCounters();

    // pthread_mutex_t mutex;

public:
    /**
     * \brief constructor
     * @param Name name of the test
     * @param ID unique id of the test
     */
    TestModel(std::string Name, int ID);

    /**
     * \brief constructor
     * @param Message Protobuf test representation to build the test model from
     */
    TestModel(GTestServiceBuffers::Test Message);

    /**
     * \brief property specifying whether the test is enabled or not
     */
    bool Enabled;

    /**
     * \brief returns the maximum number of results that can be saved for a test
     * This is the maximum number of repeated test results that can be saved
     */
    static int GetResultBufferSize() {
        return MAX_RESULT_LIST_LENGTH;
    }

    /**
     * \brief getter for the amount of successful runs of this test
     */
    int GetPassedCount();

    /**
     * \brief getter for the amount of failed runs of this test
     */
    int GetFailedCount();

    /**
     * \brief getter for the unique test id
     */
    int GetID();

    /**
     * \brief reset all test results and execution information
     */
    void ResetResults();

    /**
     * \brief append another result to the list of results.
     * If the number of results exceeds MAX_RESULT_LIST_LENGTH the oldest result will be deleted.
     * @param Iteration the current test run iteration
     * @param Passed specify whether the test has passed or not.
     */
    void AppendTestResult(int Iteration, bool Passed);

    /**
     * \brief getter for the result of the test with the specified iteration index
     * @param TestRunIndex test iteration index
     */
    bool GetTestResult(int TestRunIndex);

    /**
     * \brief lock this test model instance
     * If many threads can access this model (and change it) it should be locked beforehand.
     */
    void LockInstance();

    /**
     * \brief unlock this test model instance
     */
    void UnlockInstance();

    /**
     * \brief return the first iterator for the result vector of this test
     */
    std::map<int, bool>::iterator ResultsBegin();

    /**
     * \brief return the last iterator for the result vector of this test
     */
    std::map<int, bool>::iterator ResultsEnd();

    /**
     * \brief convert this test model to the Protobuf representation
     * @param Message pointer to empty GTestServiceBuffers::Test instance
     */
    void ToProtocolBuffer(::GTestServiceBuffers::Test* Message);

    /**
     * \brief destructor
     */
    virtual ~TestModel();
};

#endif	/* TESTMODEL_H */

