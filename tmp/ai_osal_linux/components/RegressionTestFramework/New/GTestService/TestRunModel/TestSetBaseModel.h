/* 
 * File:   TestSetBaseModel.h
 * Author: oto4hi
 *
 * Created on May 2, 2012, 5:20 PM
 */

#ifndef TESTSETBASEMODEL_H
#define	TESTSETBASEMODEL_H

#include "TestBaseModel.h"
#include <vector>

/**
 * \brief base class for each test object container
 * @see TestCase
 * @see TestRunModel
 */
class TestSetBaseModel {
protected:
    std::vector<TestBaseModel*> elements;
    bool contains(TestBaseModel* Element);
    void AddElement(TestBaseModel* Element);

    TestBaseModel* GetElementByIndex(int Index);
    TestBaseModel* GetElementByName(std::string Name);
public:
    /**
     * \brief default constructor
     */
    TestSetBaseModel();

    /**
     * destructor
     */
    virtual ~TestSetBaseModel();
};

#endif	/* TESTSETBASEMODEL_H */

