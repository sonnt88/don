/* 
 * File:   TestRunModel.cpp
 * Author: oto4hi
 * 
 * Created on April 19, 2012, 2:50 PM
 */

#include "TestRunModel.h"
#include <stdlib.h>
#include <stdexcept>
#include <iostream>
#include <sstream>

void TestRunModel::init() {
    this->iteration = 0;
    this->currentTestID = -1;
    // pthread_mutex_init(&this->mutex, NULL);
}

TestRunModel::TestRunModel() : TestSetBaseModel() {
    this->init();
}

TestRunModel::TestRunModel(GTestServiceBuffers::AllTestCases Message) {
    this->init();
    for (int index = 0; index < Message.testcases().size(); index++) {
        this->AddTestCase(new TestCaseModel(Message.testcases(index)));
    }
}

TestRunModel::~TestRunModel() {
    // pthread_mutex_destroy(&(this->mutex));
}

TestCaseModel* TestRunModel::operator [](int Index) {
    return this->GetTestCase(Index);
}

TestCaseModel* TestRunModel::operator [](std::string Name) {
    return this->GetTestCase(Name);
}

int TestRunModel::GetTestCaseCount() {
    return this->elements.size();
}

int TestRunModel::GetIteration() {
    return this->iteration;
}

void TestRunModel::NextIteration() {
    // pthread_mutex_lock(&(this->mutex));
    this->iteration++;
    // pthread_mutex_unlock(&(this->mutex));
}

void TestRunModel::SetIteration(int Iteration) {
    // pthread_mutex_lock(&(this->mutex));
    if (Iteration < this->iteration)
        throw std::invalid_argument("TestRunModel::SetIteration(): "
            "Iteration argument must not be smaller than "
            "the current iteration value.");

    this->iteration = Iteration;
    // pthread_mutex_unlock(&(this->mutex));
}

void TestRunModel::ResetIteration() {
    // pthread_mutex_lock(&(this->mutex));
    this->iteration = 0;
    // pthread_mutex_unlock(&(this->mutex));
}

void TestRunModel::ResetAllTestResults() {
    std::map<int, TestModel* >::iterator it = this->testsById.begin();
    for (; it != this->testsById.end(); ++it) {
        (*it).second->ResetResults();
    }
}

void TestRunModel::SelectTests(GTestServiceBuffers::TestLaunch* TestSelection) {
    bool enabledState(TestSelection->invert());
    for (int testCaseIndex = 0;
            testCaseIndex < this->GetTestCaseCount();
            testCaseIndex++) {
        TestCaseModel* testCase = this->GetTestCase(testCaseIndex);

        for (int testIndex = 0;
                testIndex < testCase->TestCount();
                testIndex++) {
            TestModel* test = testCase->GetTest(testIndex);
            test->Enabled = enabledState;
        } // End for
    } // End for

    for (int testSelectionIndex = 0;
            testSelectionIndex < TestSelection->testid_size();
            testSelectionIndex++) {
        int testID = TestSelection->testid(testSelectionIndex);
        TestModel* test = this->GetTestByID(testID);
        test->Enabled = !enabledState;
    } //  End for
}

std::string TestRunModel::GenerateTestFilterString(void) {
    std::string filterString = "";

    for (int testCaseIndex = 0;
            testCaseIndex < this->GetTestCaseCount();
            testCaseIndex++) {
        bool wholeTestCaseEnabled = true;
        std::string testCaseFilterString = "";

        TestCaseModel* testCase = this->GetTestCase(testCaseIndex);
        for (int testIndex = 0; testIndex < testCase->TestCount(); testIndex++) {
            TestModel* test = testCase->GetTest(testIndex);
            if (test->Enabled) {
                if (testCaseFilterString.length() > 0)
                    testCaseFilterString += ":";

                testCaseFilterString += testCase->GetName() + "." + test->GetName();
            } else {
                wholeTestCaseEnabled = false;
            }

        } // End for

        if (testCaseFilterString.length() > 0) {
            if (filterString.length() > 0)
                filterString += ":";

            if (wholeTestCaseEnabled)
                filterString += testCase->GetName() + ".*";
            else
                filterString += testCaseFilterString;
        }
    } // End for

    if (filterString.length() == 0)
        filterString = "-*";

    return filterString;
}

std::string
TestRunModel::GeneratePartialTestFilterString(const std::string & testCaseName,
        const std::string & testName) {
    std::string filterString = "";
    bool testsBeingAllowed(false);

    for (int testCaseIndex = 0;
            testCaseIndex < this->GetTestCaseCount();
            testCaseIndex++) {
        bool wholeTestCaseEnabled = true;
        std::string testCaseFilterString = "";

        TestCaseModel* testCase = this->GetTestCase(testCaseIndex);
        for (int testIndex = 0; testIndex < testCase->TestCount(); testIndex++) {
            TestModel* test = testCase->GetTest(testIndex);
            if (test->Enabled && testsBeingAllowed) {
                if (testCaseFilterString.length() > 0)
                    testCaseFilterString += ":";

                testCaseFilterString += testCase->GetName() + "." + test->GetName();
            } else {
                wholeTestCaseEnabled = false;
            }

            if ((testCase->GetName() == testCaseName) && (test->GetName() == testName)) {
                testsBeingAllowed = true;
            }
        } // End for

        if (testCaseFilterString.length() > 0) {
            if (filterString.length() > 0)
                filterString += ":";

            if (wholeTestCaseEnabled)
                filterString += testCase->GetName() + ".*";
            else
                filterString += testCaseFilterString;
        } // End if
    } // End for

    if (filterString.length() == 0)
        filterString = "-*";

    return filterString;
}

TestModel* TestRunModel::GetTestByID(int ID) {
    return this->testsById[ID];
}

TestCaseModel* TestRunModel::GetTestCaseByTestID(int ID) {
    return this->testCasesByTestID[ID];
}

void TestRunModel::UpdateTestIDMap() {
    this->testsById.clear();
    for (int testCaseIndex = 0;
            testCaseIndex < this->GetTestCaseCount();
            testCaseIndex++) {
        TestCaseModel* testCase = this->GetTestCase(testCaseIndex);
        for (int testIndex = 0; testIndex < testCase->TestCount(); testIndex++) {
            std::pair < std::map<int, TestModel* >::iterator, bool> ret1;
            std::pair < std::map<int, TestCaseModel* >::iterator, bool> ret2;

            TestModel* test = testCase->GetTest(testIndex);
            int testID = test->GetID();

            ret1 = this->testsById.insert(std::make_pair(testID, test));

            if (!ret1.second)
                throw std::runtime_error("TestRunModel::UpdateTestIDMap(): "
                    "test id is not unique");

            ret2 = this->testCasesByTestID.insert(std::make_pair(testID, testCase));

            if (!ret2.second)
                throw std::runtime_error("TestRunModel::UpdateTestIDMap(): "
                    "test id is not unique");
        }
    }
}

void TestRunModel::AddTestCase(TestCaseModel* TestCase) {
    this->AddElement(TestCase);
}

TestCaseModel* TestRunModel::GetTestCase(int Index) {
    return dynamic_cast<TestCaseModel*> (this->GetElementByIndex(Index));
}

TestCaseModel* TestRunModel::GetTestCase(std::string Name) {
    return dynamic_cast<TestCaseModel*> (this->GetElementByName(Name));
}

void
TestRunModel::ToProtocolBuffer(::GTestServiceBuffers::AllTestCases* Message) {
    if (Message == NULL)
        throw std::invalid_argument("TestRunModel::ToProtocolBuffer(): "
            "Message cannot be null.");

    for (unsigned int index = 0; index < this->elements.size(); index++) {
        TestCaseModel* testCaseModel =
                static_cast<TestCaseModel*> (this->elements[index]);
        testCaseModel->ToProtocolBuffer(Message->add_testcases());
    }
}

int TestRunModel::GetCurrentTestID() {
    return this->currentTestID;
}

void TestRunModel::SetCurrentTestID(int TestID) {
    // pthread_mutex_lock(&(this->mutex));
    this->currentTestID = TestID;
    // pthread_mutex_unlock(&(this->mutex));
}

void TestRunModel::LockInstance() {
    // pthread_mutex_lock(&(this->mutex));
}

void TestRunModel::UnlockInstance() {
    // pthread_mutex_unlock(&(this->mutex));
}
