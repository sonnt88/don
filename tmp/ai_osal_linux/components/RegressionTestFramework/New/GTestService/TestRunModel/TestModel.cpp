/* 
 * File:   TestModel.cpp
 * Author: oto4hi
 * 
 * Created on April 19, 2012, 2:49 PM
 */

#include "TestModel.h"
#include <stdlib.h>
#include <stdexcept>
#include "GTestServiceBuffers.pb.h"
#include <sstream>

#include <iostream>

void TestModel::resetCounters() {
    this->currentTestRun = 0;
    this->passedCount = 0;
}

void TestModel::ResetResults() {
    LockInstance();
    this->testRunResults.clear();
    UnlockInstance();
    this->resetCounters();
}

TestModel::TestModel(std::string Name, int ID) : TestBaseModel(Name) {
    if (this->defaultDisabled)
        this->Enabled = false;
    else
        this->Enabled = true;

    this->id = ID;

    this->resetCounters();
    // pthread_mutex_init(&this->mutex, NULL);
}

TestModel::TestModel(GTestServiceBuffers::Test Message) : TestBaseModel(Message.name()) {
    this->defaultDisabled = Message.defaultdisabled();
    this->Enabled = Message.enabled();
    this->id = Message.id();

    this->resetCounters();
    // pthread_mutex_init(&this->mutex, NULL);
}

void TestModel::LockInstance() {
    // pthread_mutex_lock(&(this->mutex));
}

void TestModel::UnlockInstance() {
    // pthread_mutex_unlock(&(this->mutex));
}

int TestModel::GetFailedCount() {
    return this->currentTestRun - this->passedCount;
}

int TestModel::GetPassedCount() {
    return this->passedCount;
}

int TestModel::GetID() {
    return this->id;
}

void TestModel::AppendTestResult(int Iteration, bool Passed) {
    LockInstance();

    const bool insertSucceeded =
            this->testRunResults.insert(TestResultPair(Iteration, Passed)).second;
    if (insertSucceeded == false) {
        std::stringstream errMsg;
        errMsg << "TestModel::AppendTestResult( ";
        errMsg << Iteration;
        errMsg << ", ";
        errMsg << Passed;
        errMsg << " ) - failed to append new test result";
        throw std::runtime_error(errMsg.str().c_str());
    }

    if (this->testRunResults.size() > MAX_RESULT_LIST_LENGTH) {
        this->testRunResults.erase(this->testRunResults.begin());
    }

    if (Passed)
        this->passedCount++;

    this->currentTestRun++;
    UnlockInstance();
}

bool TestModel::GetTestResult(int TestRunIndex) {
    if (TestRunIndex < 0)
        throw std::invalid_argument("TestModel::GetTestResult() - "
            "TestRunIndex may not be smaller than zero.");

    LockInstance();
    bool testRunCheck(this->testRunResults.find(TestRunIndex) == this->testRunResults.end());
    UnlockInstance();

    if (testRunCheck) {
        std::stringstream exc;
        exc << "TestModel::GetTestResult(int TestRunIndex): "
                "result not found for this test run index, "
                << TestRunIndex << ",\n"
                "perhaps the result buffer is not big enough and the result "
                "has already gone. \n"
                "Or it could be that std::map<int, bool> testRunResults has "
                "become corrupt.\n";
        throw std::runtime_error(exc.str().c_str());
    }

    LockInstance();
    bool testResult(this->testRunResults[TestRunIndex]);
    UnlockInstance();
    return testResult;
}

void TestModel::ToProtocolBuffer(::GTestServiceBuffers::Test* Message) {
    if (Message == NULL)
        throw std::invalid_argument("TestModel::ToProtocolBuffer() - "
            "Message cannot be null.");

    Message->set_name(this->name);
    Message->set_defaultdisabled(this->defaultDisabled);
    Message->set_enabled(this->Enabled);
    Message->set_id(this->id);
}

std::map<int, bool>::iterator
TestModel::ResultsBegin() {
    // We can't protect this function with a mutex, because it is going to be 
    // called by BaseValidState::HandleSendAllResultsTask()
    // and it will have protected its code with the same mutex.  So we'll 
    // imediately get a deadlock.
    //
    // However, it is believed that no other function calls this, which means 
    // that it is protected by a mutex.  Crap way to
    // organise code, mind.
    return this->testRunResults.begin();
}

std::map<int, bool>::iterator
TestModel::ResultsEnd() {
    // We can't protect this function with a mutex, because it is going to be 
    // called by BaseValidState::HandleSendAllResultsTask()
    // and it will have protected its code with the same mutex.  So we'll 
    // imediately get a deadlock.
    //
    // However, it is believed that no other function calls this, which means 
    // that it is protected by a mutex.  Crap way to
    // organise code, mind.
    return this->testRunResults.end();
}

TestModel::~TestModel() {
    // pthread_mutex_destroy(&(this->mutex));
}
