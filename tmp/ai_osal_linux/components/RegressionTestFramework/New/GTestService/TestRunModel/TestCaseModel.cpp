/* 
 * File:   GTestCaseModel.cpp
 * Author: oto4hi
 * 
 * Created on April 19, 2012, 2:46 PM
 */

#include "TestCaseModel.h"
#include <stdexcept>

TestCaseModel::TestCaseModel(std::string Name) : TestBaseModel(Name) {
}

TestCaseModel::TestCaseModel(GTestServiceBuffers::TestCase Message) : TestBaseModel(Message.name()) {
    this->defaultDisabled = Message.defaultdisabled();

    for (int index = 0; index < Message.tests().size(); index++) {
        this->AddTest(new TestModel(Message.tests(index)));
    }
}

TestCaseModel::~TestCaseModel() {
}

TestModel* TestCaseModel::operator [](int Index) {
    return this->GetTest(Index);
}

TestModel* TestCaseModel::operator [](std::string Name) {
    return this->GetTest(Name);
}

void TestCaseModel::AddTest(TestModel* Test) {
    this->AddElement(Test);
}

int TestCaseModel::TestCount() {
    return this->elements.size();
}

TestModel* TestCaseModel::GetTest(int Index) {
    return dynamic_cast<TestModel*> (this->GetElementByIndex(Index));
}

TestModel* TestCaseModel::GetTest(std::string Name) {
    return dynamic_cast<TestModel*> (this->GetElementByName(Name));
}

void TestCaseModel::ToProtocolBuffer(::GTestServiceBuffers::TestCase* Message) {
    if (Message == NULL)
        throw std::invalid_argument("Message cannot be null.");

    Message->set_name(this->name);
    Message->set_defaultdisabled(this->defaultDisabled);
    for (unsigned int index = 0; index < this->elements.size(); index++) {
        TestModel* testModel = static_cast<TestModel*> (this->elements[index]);
        testModel->ToProtocolBuffer(Message->add_tests());
    }
}
