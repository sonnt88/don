/*
 * TestStates.h
 *
 *  Created on: Jul 20, 2012
 *      Author: oto4hi
 */

#ifndef TESTSTATES_H_
#define TESTSTATES_H_

/**
 * \brief possible states for a single test
 */
enum TEST_STATE {
    PASSED,
    FAILED,
    RUNNING,
    RESET
};

#endif /* TESTSTATES_H_ */
