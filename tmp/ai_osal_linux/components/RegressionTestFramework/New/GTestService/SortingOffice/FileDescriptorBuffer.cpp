/* 
 * File:   FileDescriptorBuffer.cpp
 * Author: aga2hi
 * 
 * Created on April 13, 2015, 2:25 PM
 */

#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <cstring>
#include <cassert>
#include <stdexcept>
#include "FileDescriptorBuffer.h"

#define LINE_LENGTH BUFSIZ

FileDescriptorBuffer::FileDescriptorBuffer(Callback _clientFunctor)
: clientFunctor(_clientFunctor) {
    if (_clientFunctor == NULL) {
        throw std::runtime_error("FileDescriptorBuffer::FileDescriptorBuffer() : "
                "argument not allowed to be NULL");
    }
}

FileDescriptorBuffer::FileDescriptorBuffer(const FileDescriptorBuffer& orig)
: clientFunctor(orig.clientFunctor) {
}

FileDescriptorBuffer::~FileDescriptorBuffer() {
}

// Used by operator() to read data from file descriptor.  But a derived
// class may provide its own function, perhaps for test purposes.

ssize_t
FileDescriptorBuffer::fetchInputData(const int fd, char buffer[], const size_t bufferSize) {
    return read(fd, buffer, bufferSize);
}

void
FileDescriptorBuffer::operator()(int fd) {

    const int bufferSize(LINE_LENGTH);
    char buffer[bufferSize];
    static std::string savedLine;

    // Get data from the file descriptor, copy into buffer.
    // A derived class may have its own fetchInputData, for
    // example for testing purposes

    const ssize_t rdStatus = fetchInputData(fd, buffer, bufferSize - 1);

    // Do we have something worth reading?

    if (rdStatus > 0) {

        bool moreToRead = false;

        // Add the buffer to what we have already got

        savedLine.append(buffer, rdStatus);

        // Now work our way through the buffer, splitting
        // it up into lines and calling the client's function
        // for each one

        do {

            // Split buffer up into lines, if possible

            std::string::size_type delim = savedLine.find('\n');

            // Do we have a complete line to work with?

            if (delim == std::string::npos) {

                // There was no line feed character.

                moreToRead = false;

            } else {

                // Check that we can call the client's function
                // and create an object that makes it easier

                assert(clientFunctor);
                Functor & runClientFunction(* clientFunctor);
                std::string clientArgument(savedLine.substr(0, delim));

                // We have a complete line.  Call the client's function.

                runClientFunction(clientArgument);

                // Remove the parts of savedLine that have just been
                // given to the client + the newline character.

                savedLine.erase(0, delim + 1);

                // Now find out if there is anything left in the buffer

                if (!savedLine.empty()) {
                    moreToRead = true;
                }
            }
        } while (moreToRead);

    } else {

        // read() produced an error, shouldn't get here
        perror("read()");

    }
}
