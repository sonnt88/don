/* 
 * File:   FileDescriptorBuffer.h
 * Author: aga2hi
 *
 * Created on April 13, 2015, 2:25 PM
 */
#include <string>
#include "SortingOffice.h"

#ifndef FILEDESCRIPTORBUFFER_H
#define	FILEDESCRIPTORBUFFER_H

class FileDescriptorBuffer : public SortingOffice::Functor {
public:
    // Client classes should derive from FileDescriptorBuffer::Functor

    class Functor {
    public:
        // Client code goes here

        virtual void operator()(std::string & incomingLine) = 0;
    };
    typedef Functor * Callback;

    FileDescriptorBuffer(Callback _clientFunctor);
    FileDescriptorBuffer(const FileDescriptorBuffer& orig);
    virtual ~FileDescriptorBuffer();

    // Sorting Office calls this, but client code goes in
    // the Functor.  Uses fetchInputData() to read from file
    // descriptor.

    virtual void operator()(int fd);

protected:

    // Behaves like standard function read(): - returns the number of bytes
    // read from file descriptor fd.  A return of zero or negative will
    // be treated by operator() as an error.

    virtual ssize_t fetchInputData(const int fd, char buffer[], const size_t bufferSize);

private:

    Callback clientFunctor;
};

#endif	/* FILEDESCRIPTORBUFFER_H */

