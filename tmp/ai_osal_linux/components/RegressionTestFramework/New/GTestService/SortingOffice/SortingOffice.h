#include <sys/poll.h>

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

#ifndef _SORTING_OFFICE_
#define _SORTING_OFFICE_

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

class SortingOffice {
public:
	// User methods should derive from SortingOffice::Functor
	struct  Functor {
		virtual void operator()( int ) = 0;
	};
	// The handler methods work on pointers to objects of type Functor
	typedef Functor * Callback;
	enum    DeliveryCode { TIMEOUT, STOPPED, HUNG_UP };

	                     SortingOffice();
	virtual              ~SortingOffice();

	virtual void         RegisterHandler( const int fileDescriptor, Callback function );
	virtual Callback     DeregisterHandler( const int fileDescriptor );
	virtual DeliveryCode DeliverMessages( const unsigned int timeoutMilliseconds );
	virtual void         Stop();

protected:
	virtual void         setup_pollfd( const int fileDescriptor, Callback function );
	virtual void         resize_arrays( void );

private:
	// SortingOffice contains a pair of dynamic arrays, associated.
	pollfd*              pollfdArray;
	Callback*            callbackArray;
	int                  sizeOfAssociativeArray;
	int                  numberElementsInArray;
	int                  writeLoopback;
	int                  readLoopback;

	// Objects of this type shall not be copied, otherwise the
	// poll() calls will work on the same file descriptors
	                     SortingOffice( const SortingOffice & );
	SortingOffice&       operator=( const SortingOffice & );
};

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

#endif // _SORTING_OFFICE_
