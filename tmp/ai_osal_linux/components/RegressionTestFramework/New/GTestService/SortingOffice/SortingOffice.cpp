// Part of GTestService
#include <unistd.h>
#include <stdlib.h>
#include <sys/poll.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <exception>
#include <stdexcept>
#include "SortingOffice.h"

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

SortingOffice::SortingOffice() :
	pollfdArray( NULL ), callbackArray( NULL ), sizeOfAssociativeArray( 5 ),
    numberElementsInArray( 0 ), writeLoopback( 0 ), readLoopback( 0 )
{
	// Pipe
	int loopback[ 2 ];

	// Allocate memory for the two arrays
	pollfdArray = ( pollfd * )calloc( sizeOfAssociativeArray, sizeof( pollfd ) );
	if( NULL == pollfdArray ) {
		throw std::runtime_error( "SortingOffice::SortingOffice() - calloc() failed" );
	}  //  Endif
	callbackArray = ( Callback * )calloc( sizeOfAssociativeArray, sizeof( Callback ) );
	if( NULL == callbackArray ) {
		throw std::runtime_error( "SortingOffice::SortingOffice() - calloc() failed" );
	}  //  Endif

	// Set up the loopback pipe, so that SortingOffice::Stop() can work
	const int pipeReturn = pipe( loopback );
	if( -1 == pipeReturn ) {
		throw std::runtime_error( "SortingOffice::SortingOffice() - pipe() failed" );
	}  //  Endif
	writeLoopback = loopback[ 1 ];
	readLoopback = loopback[ 0 ];
	setup_pollfd( readLoopback, NULL );

	// Make sure everything has been set up correctly
	assert( NULL != pollfdArray );
	assert( NULL != callbackArray );
	assert( 5 == sizeOfAssociativeArray );
	assert( 1 == numberElementsInArray );
	assert( 0 != writeLoopback );
	assert( 0 != readLoopback );
}

SortingOffice::~SortingOffice()
{
	close( writeLoopback );
	close( readLoopback );
	free( pollfdArray );
	free( callbackArray );
}

void
SortingOffice::RegisterHandler( const int fileDescriptor, Callback function )
{
	// Sanity check
	if( NULL == function ) {
		throw std::runtime_error( "SortingOffice::RegisterHandler(): null function" );
	}

	// Make sure that fileDescriptor has not already been registered
	for( int ind( 0 ); ind < numberElementsInArray; ++ind ) {
		if( pollfdArray[ ind ].fd == fileDescriptor ) {
			throw std::runtime_error( "SortingOffice::RegisterHandler(): this file descriptor has already been registered" );
		}  //  End if
	}  //  End for

	// Check that the array is not full - and resize if necessary
	if( numberElementsInArray == sizeOfAssociativeArray ) {
		sizeOfAssociativeArray += 5;
		resize_arrays( );
	}  //  Endif

	setup_pollfd( fileDescriptor, function );

	// Sanity check
	assert( callbackArray[ numberElementsInArray - 1 ] == function );
	assert( pollfdArray[ numberElementsInArray - 1 ].fd == fileDescriptor );
}

void
SortingOffice::resize_arrays( void )
{
	// New size must have been given to us in member data sizeOfAssociativeArray

	const size_t newFDArraySize( sizeOfAssociativeArray * sizeof( pollfd ) );
	pollfdArray = ( pollfd * )realloc( pollfdArray, newFDArraySize );
	if( NULL == pollfdArray ) {
		throw std::runtime_error( "SortingOffice::RegisterHandler() - realloc() failed" );
	}  //  Endif

	const size_t newCBArraySize( sizeOfAssociativeArray * sizeof( Callback ) );
	callbackArray = ( Callback * )realloc( callbackArray, newCBArraySize );
	if( NULL == callbackArray ) {
		throw std::runtime_error( "SortingOffice::RegisterHandler() - realloc() failed" );
	}  //  Endif
}

void
SortingOffice::setup_pollfd( const int fileDescriptor, Callback function )
{
	// Now store the file descriptor in the pollfdArray
	pollfdArray[ numberElementsInArray ].fd = fileDescriptor;
	pollfdArray[ numberElementsInArray ].events = POLLIN;

	// Now store the function in the Callback array
	callbackArray[ numberElementsInArray ] = function;

	++numberElementsInArray;
	assert( numberElementsInArray <= sizeOfAssociativeArray );
}

SortingOffice::Callback
SortingOffice::DeregisterHandler( const int fileDescriptor )
{
	int  index( 0 );
	bool fileDescriptorNotFound( true );
	Callback handler;

	assert( numberElementsInArray != 0 );
	assert( fileDescriptor != readLoopback );

	// Look in pollfdArray for caller's file descriptor
	for( index = 0; index < numberElementsInArray; ++index ) {
		if( fileDescriptor == pollfdArray[ index ].fd ) {
			fileDescriptorNotFound = false;
			break;
		}  //  Endif
	}  //  Endfor

	if( fileDescriptorNotFound ) {
		throw std::runtime_error( "SortingOffice::DeregisterHandler() - file descriptor not found" );
	}

	// Before we obliterate it, get a pointer to the handler
	handler = callbackArray[ index ];

	// Half-swap with last items (top) in both arrays, leaving top duplicated
	const int top( numberElementsInArray - 1 );
	pollfdArray[ index ] = pollfdArray[ top ];
	callbackArray[ index ] = callbackArray[ top ];

	// Now allow the last items to be overwritten (no point writing over them here)
	--numberElementsInArray;

	// Can we save any memory?
	if( sizeOfAssociativeArray == ( numberElementsInArray + 10 ) ) {
		sizeOfAssociativeArray -= 5;
		resize_arrays( );
	}  //  Endif

	// Sanity checks
	assert( NULL != callbackArray );
	assert( NULL != pollfdArray );
	for( int ind( 0 ); ind < numberElementsInArray; ++ind ) {
		assert( pollfdArray[ ind ].fd != fileDescriptor );
	}  //  Endfor
	assert( sizeOfAssociativeArray >= numberElementsInArray );
	assert( sizeOfAssociativeArray <= ( numberElementsInArray + 10 ) );

	return handler;
}

SortingOffice::DeliveryCode
SortingOffice::DeliverMessages( const unsigned int timeoutMilliseconds )
{
	DeliveryCode returnVal;
	bool         pollStillRunning = true;

	assert( NULL != pollfdArray );
	assert( NULL != callbackArray );
	assert( 0 < numberElementsInArray );

	do {
		const int pollReturn = poll( pollfdArray, numberElementsInArray, timeoutMilliseconds );

		if( ( -1 == pollReturn ) ) {
			// Error occurred
			if( EINTR != errno ) {
				// Not interrupted by a signal, so it's a real error
				std::string errMsg( "poll() in SortingOffice::DeliverMessages() - " );
				errMsg += strerror( errno );
				throw std::runtime_error( errMsg.c_str() );
			}  //  Endif
		}  //  Endif
		else if( 0 == pollReturn ) {
			// Timeout occured
			returnVal = TIMEOUT;
			pollStillRunning = false;
		}  //  Endif
		else {
			// Data has arrived
			// Go through each of the elements in pollfdArray,
			// find out which has something for us to read
			for( int ind( 0 ); ind < numberElementsInArray; ++ind ) {
				const short & pollEvents( pollfdArray[ ind ].revents );

				if( pollEvents & POLLERR ) {
					// There was an error on this file descriptor
					throw std::runtime_error( "SortingOffice::DeliverMessages() - "
											  "a file descriptor has encountered an error" );
				}  //  Endif

				if( pollEvents & POLLHUP ) {
					// Hung up event on this file descriptor
					pollStillRunning = false;
					returnVal = HUNG_UP;
				}  //  Endif

				if( pollEvents & POLLNVAL ) {
					// File descriptor is invalid
					throw std::runtime_error( "SortingOffice::DeliverMessages() - "
											  "a file descriptor is invalid" );
				}  //  Endif

				if( pollEvents & POLLIN ) {
					// Find out if we have something to do or not
					const int & readFileDescriptor( pollfdArray[ ind ].fd );
					if( readFileDescriptor == readLoopback ) {
						// Internal file descriptor, means we have been told to stop
						returnVal = STOPPED;
						pollStillRunning = false;
					}  //  Endif
					else {
						// Get the callback function
						const Callback & function = callbackArray[ ind ];

						// Run the callback, passing it the file descriptor
						assert( NULL != function );
						( * function )( readFileDescriptor );

					}  //  Endelse
				}  //  Endif
			}  //  Endfor
		}  //  Endelse

	} while( true == pollStillRunning );

	return returnVal;
}

void
SortingOffice::Stop()
{
	// Need to get SortingOffice::DeliverMessages() out of its poll()
	//
	// It is OK to run this from within a signal handler, because write()
	// appears on the list of reentrant functions, figure 10.4
	// Advanced Programming in the UNIX Environment, second edition.
	const ssize_t writeReturn = write( writeLoopback, "STOP", 4 );
	if( -1 == writeReturn ) {
		throw std::runtime_error( "SortingOffice::Stop(), write() failed" );
	}  //  Endif
}

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////
