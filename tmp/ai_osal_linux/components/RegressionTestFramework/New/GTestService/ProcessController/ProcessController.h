#include <pthread.h> // pthread_t, pthread_mutex_t, pthread_cond_t
#include <unistd.h>  // pid_t
#include <signal.h>  // sigset_t
#include <vector>
#include <string>


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

#ifndef _PROCESS_CONTROLLER_
#define _PROCESS_CONTROLLER_

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

class ProcessController {
public:
    // User methods should derive from ParentFunctor and ChildFunctor

    class ParentFunctor {
        // The three FILE pointers will only be configured
        // by the ProcessController, not ParentFunctor's
        // constructor nor the client

        friend class ProcessController;
        FILE * toChild;
        FILE * fromChild;
        FILE * fromErrChild;
        int fdToChild;
        int fdFromChild;
        int fdFromErrChild;
        ProcessController * procController;

    public:

        ParentFunctor();
        ~ParentFunctor();

        // Client code goes here...

        virtual void operator()(void) = 0;

        // The client can use the following six
        // functions to communicate with the child
        // process

        FILE * GetOutputFileBuffer();
        FILE * GetErrorFileBuffer();
        FILE * GetInputFileBuffer();

        int GetOutputFileDescriptor();
        int GetErrorFileDescriptor();
        int GetInputFileDescriptor();

        // The client can use the following
        // function to terminate the child
        // process

        void TerminateChildProc();
    }; // End class ParentFunctor

    struct ChildFunctor {
        // Client code goes here...

        virtual void operator()(void) = 0;
    }; // End struct ChildFunctor

    enum BufferStyle {
        FULL,
        LINE,
        NONE
    };

    // The handler methods work on pointers to objects of type Functor
    typedef ParentFunctor * ParentCallback;
    typedef ChildFunctor * ChildCallback;

private:
    pid_t childpid;

    int stdinPipeFD[2];
    int stdoutPipeFD[2];
    int stderrPipeFD[2];
    bool stdinPipeConfigured;
    bool stdoutPipeConfigured;
    bool stderrPipeConfigured;
    BufferStyle stdinBuffer;
    BufferStyle stdoutBuffer;
    BufferStyle stderrBuffer;

    ChildCallback childFunction;
    ParentCallback parentFunction;
    int childReturnStatus;
    int childProcessSignal;
    bool childProcessComplete;

protected:
    void UpdateChildReturnStatus(const int status);

public:
    ProcessController(ParentCallback parent, ChildCallback child);
    virtual ~ProcessController();

    // Use the following three functions in order to request
    // establishment of communications between parent and
    // child processes

    // Conigure the child process' stdout pipe
    virtual void ConfigureStdoutPipe(BufferStyle _buffer = FULL);
    // Configure the child process' stderr pipe
    virtual void ConfigureStderrPipe(BufferStyle _buffer = FULL);
    // Configure the child process' stdin pipe
    virtual void ConfigureStdinPipe(BufferStyle _buffer = FULL);

    virtual void Run();
    virtual void Kill();

    // Use the following function to retrieve information
    // about the child process after it terminated

    // Get return code (undefined if terminated by signal)

    virtual int GetChildExitStatus() {
        return childReturnStatus;
    }
    // true if it reached its end, returned or called exit()

    virtual bool ChildExitNormally() {
        return childProcessComplete;
    }
    // Get signal number that terminated it (undefined if
    // not terminated by signal)

    virtual int GetChildSignalNumber() {
        return childProcessSignal;
    }
};

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

#endif // _PROCESS_CONTROLLER_
