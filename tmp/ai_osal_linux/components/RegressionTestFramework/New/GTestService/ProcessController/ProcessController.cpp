#include "ProcessController.h"

// NB Requires rt libraries otherwise linker complains that references to
// sem_wait(), sem_post() etc are undefined.

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

#include <unistd.h>    // pipe(), execvp() ...
#include <pthread.h>   // pthread_create(), pthread_mutex_init()
#include <string.h>    // strerror()
#include <errno.h>     // errno
#include <limits.h>    // PATH_MAX
#include <stdlib.h>    // realpath()
#include <sys/stat.h>  // mkdir()
#include <sys/types.h> // mkdir()
#include <stdio.h>     // stdin, stdout, stderr, fileno()
#include <sys/wait.h>  // wait()
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <stdexcept>   // runtime_error

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// class ParentFunctor starts here

ProcessController::ParentFunctor::ParentFunctor() :
toChild(NULL), fromChild(NULL), fromErrChild(NULL), procController(NULL) {
}

ProcessController::ParentFunctor::~ParentFunctor() {
    if (toChild)
        fclose(toChild);
    if (fromChild)
        fclose(fromChild);
    if (fromErrChild)
        fclose(fromErrChild);
}

void
ProcessController::ParentFunctor::TerminateChildProc() {
    if (procController == NULL) {
        throw std::runtime_error("ProcessController::"
                "ParentFunctor::TerminateChildProc() - "
                "procController not instantiated");
    }
    procController->Kill();
}

FILE *
ProcessController::ParentFunctor::GetOutputFileBuffer() {
    return toChild;
}

FILE *
ProcessController::ParentFunctor::GetErrorFileBuffer() {
    return fromErrChild;
}

FILE *
ProcessController::ParentFunctor::GetInputFileBuffer() {
    return fromChild;
}

int
ProcessController::ParentFunctor::GetOutputFileDescriptor() {
    return fdToChild;
}

int
ProcessController::ParentFunctor::GetErrorFileDescriptor() {
    return fdFromErrChild;
}

int
ProcessController::ParentFunctor::GetInputFileDescriptor() {
    return fdFromChild;
}


// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// ////////// //////////

// class ProcessController starts here

ProcessController::ProcessController(ParentCallback parent, ChildCallback child) :
childReturnStatus(-1),
childProcessSignal(-1),
childFunction(child),
parentFunction(parent),
childpid(-1),
childProcessComplete(true),
stdoutPipeConfigured(false),
stderrPipeConfigured(false),
stdinPipeConfigured(false),
stdinBuffer(FULL),
stdoutBuffer(FULL),
stderrBuffer(FULL) {
    parent->procController = this;
}

ProcessController::~ProcessController() {
}

void
ProcessController::ConfigureStdoutPipe(BufferStyle _buffer /*= FULL*/) {
    stdoutPipeConfigured = true;
    stdoutBuffer = _buffer;
}

void
ProcessController::ConfigureStderrPipe(BufferStyle _buffer /*= FULL*/) {
    stderrPipeConfigured = true;
    stderrBuffer = _buffer;
}

void
ProcessController::ConfigureStdinPipe(BufferStyle _buffer /*= FULL*/) {
    stdinPipeConfigured = true;
    stdinBuffer = _buffer;
}

void ProcessController::Run() {
    if (0 < childpid) {
        throw std::runtime_error("ProcessController::Run() - "
                "Process controller already running a process.");
    }

    // Reset the flags, in case Run() has been called before

    childReturnStatus = -1;
    childProcessSignal = -1;
    childProcessComplete = true;

    // Create the pipes, as requested
    // Pipes are described in Advanced Programming in the
    // UNIX Environment, section 15.2, page 496

    if (stdinPipeConfigured) {
        const int pipeReturn = pipe(stdinPipeFD);
        if (pipeReturn) {
            std::stringstream err;
            err << "ProcessController::Run( ); stdin pipe "
                    << strerror(errno);
            throw std::runtime_error(err.str().c_str());
        }
        // Does the client want pipe communication to be buffered?
        if ((stdinBuffer == LINE) || (stdinBuffer == FULL)) {
            // Create a buffer for the parent to write into
            parentFunction->toChild = fdopen(stdinPipeFD[ 1 ], "w");
            // Configure buffer for line buffering, if requested
            if (stdinBuffer == LINE) {
                // setvbuf described Advanced Programming for the
                // UNIX Environment, section 5.4, page 136
                const int setbufStatus = setvbuf(parentFunction->toChild, NULL, _IOLBF, BUFSIZ);
                if (0 != setbufStatus) {
                    std::stringstream err;
                    err << "ProcessController::Run( ); stdin setvbuf ";
                    err << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                }
            }
        }
        // Tell client the file descriptor of its pipe to child process
        parentFunction->fdToChild = stdinPipeFD[ 1 ];
    }
    if (stdoutPipeConfigured) {
        const int pipeReturn = pipe(stdoutPipeFD);
        if (pipeReturn) {
            std::stringstream err;
            err << "ProcessController::Run( ); stdout pipe "
                    << strerror(errno);
            throw std::runtime_error(err.str().c_str());
        }
        // Does the client want pipe communications to be buffered?
        if ((stdoutBuffer == LINE) || (stdoutBuffer == FULL)) {
            // Create a buffer for the parent to read
            parentFunction->fromChild = fdopen(stdoutPipeFD[ 0 ], "r");
            // Configure buffer for line buffering, if requested
            if (stdoutBuffer == LINE) {
                // setvbuf described Advanced Programming for the
                // UNIX Environment, section 5.4, page 136
                const int setbufStatus = setvbuf(parentFunction->fromChild, NULL, _IOLBF, BUFSIZ);
                if (0 != setbufStatus) {
                    std::stringstream err;
                    err << "ProcessController::Run( ); stdout setvbuf ";
                    err << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                }
            }
        }
        // Tell client the file descriptor of its pipe from child process
        parentFunction->fdFromChild = stdoutPipeFD[ 0 ];
    }
    if (stderrPipeConfigured) {
        const int pipeReturn = pipe(stderrPipeFD);
        if (pipeReturn) {
            std::stringstream err;
            err << "ProcessController::Run( ); stderr pipe " << strerror(errno);
            throw std::runtime_error(err.str().c_str());
        }
        // Does the client want pipe communications to be buffered?
        if ((stderrBuffer == LINE) || (stderrBuffer == FULL)) {
            // Create a buffer for the parent to read
            parentFunction->fromErrChild = fdopen(stderrPipeFD[ 0 ], "r");
            // Configure buffer for line buffering, if requested
            if (stderrBuffer == LINE) {
                // setvbuf described Advanced Programming for the
                // UNIX Environment, section 5.4, page 136
                const int setbufStatus = setvbuf(parentFunction->fromErrChild, NULL, _IOLBF, BUFSIZ);
                if (0 != setbufStatus) {
                    std::stringstream err;
                    err << "ProcessController::Run( ); stdout setvbuf ";
                    err << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                }
            }
        }
        // Tell client the file descriptor of its pipe from child process
        parentFunction->fdFromErrChild = stderrPipeFD[ 0 ];
    }

    // create a new process

    this->childpid = fork();
    if (this->childpid < 0) {
        // fork() didn't work
        std::stringstream errMsg;
        errMsg << "ProcessController::Run() - fork() - "
                << strerror(errno);
        throw std::runtime_error(errMsg.str().c_str());
    }

    // Finish the configuration of the pipes and run both of the
    // client's handlers in the correct processes

    if (this->childpid == 0) {
        // This is the child process,
        // see Advanced Programming in the Unix Environment,
        // section 8,3, page 211
        ChildFunctor & RunChildFunction(* childFunction);
        // See Advanced Programming in the UNIX Environment section 3.12,
        // page 76 for explanation of dup2
        if (stdinPipeConfigured) {
            close(stdinPipeFD[ 1 ]); // Close the write end
            if (stdinPipeFD[ 0 ] != STDIN_FILENO) {
                const int dupfd = dup2(stdinPipeFD[ 0 ], STDIN_FILENO);
                if (dupfd != STDIN_FILENO) {
                    std::stringstream err;
                    err << "ProcessController::Run() - dup2("
                            << stdinPipeFD[ 0 ]
                            << ", "
                            << STDIN_FILENO
                            << ") [stdin] "
                            << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                } //  End if
                close(stdinPipeFD[ 0 ]); // Not needed after duplication
            } //  End if
            // Set buffering to line buffering, if requested
            if (stdinBuffer == LINE) {
                // setvbuf described Advanced Programming for the
                // UNIX Environment, section 5.4, page 136
                const int setbufStatus = setvbuf(stdin, NULL, _IOLBF, BUFSIZ);
                if (0 != setbufStatus) {
                    std::stringstream err;
                    err << "ProcessController::Run( ); stdin setvbuf ";
                    err << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                }
            }
        }
        if (stdoutPipeConfigured) {
            close(stdoutPipeFD[ 0 ]); // Close the read end
            if (STDOUT_FILENO != stdoutPipeFD[ 1 ]) {
                const int dupfd = dup2(stdoutPipeFD[ 1 ], STDOUT_FILENO);
                if (dupfd != STDOUT_FILENO) {
                    std::stringstream err;
                    err << "ProcessController::Run() - dup2("
                            << stdoutPipeFD[ 1 ]
                            << ", "
                            << STDOUT_FILENO
                            << ") [stdout] "
                            << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                } //  End if
                close(stdoutPipeFD[ 1 ]); // Not needed after duplication
            } //  End if
            // Set buffering to line buffering, if requested
            if (stdoutBuffer == LINE) {
                // setvbuf described Advanced Programming for the
                // UNIX Environment, section 5.4, page 136
                const int setbufStatus = setvbuf(stdout, NULL, _IOLBF, BUFSIZ);
                if (0 != setbufStatus) {
                    std::stringstream err;
                    err << "ProcessController::Run( ); stdin setvbuf ";
                    err << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                }
            }
        }
        if (stderrPipeConfigured) {
            close(stderrPipeFD[ 0 ]); // Close the read end
            if (STDERR_FILENO != stderrPipeFD[ 1 ]) {
                const int dupfd = dup2(stderrPipeFD[ 1 ], STDERR_FILENO);
                if (dupfd != STDERR_FILENO) {
                    std::stringstream err;
                    err << "ProcessController::Run() - dup2("
                            << stderrPipeFD[ 1 ]
                            << ", "
                            << STDERR_FILENO
                            << ") [stderr] "
                            << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                } //  End if
                close(stderrPipeFD[ 1 ]); // Not needed after duplication
            } //  End if
            // Set buffering to line buffering, if requested
            if (stderrBuffer == LINE) {
                // setvbuf described Advanced Programming for the
                // UNIX Environment, section 5.4, page 136
                const int setbufStatus = setvbuf(stderr, NULL, _IOLBF, BUFSIZ);
                if (0 != setbufStatus) {
                    std::stringstream err;
                    err << "ProcessController::Run( ); stdin setvbuf ";
                    err << strerror(errno);
                    throw std::runtime_error(err.str().c_str());
                }
            }
        } //  End if

        // Finally, we can run the child's handler
        RunChildFunction();
        exit(0);

    } else {
        // This is the parent process,
        // see Advanced Programming in the Unix Environment, section 8,3, page 211
        ParentFunctor & RunParentFunction(* parentFunction);

        // Close the file descriptors that we don't need
        if (stderrPipeConfigured) {
            // Close the output end of stderr
            close(stderrPipeFD[ 1 ]);
        }
        if (stdoutPipeConfigured) {
            // Close the output end of stdout
            close(stdoutPipeFD[ 1 ]);
        }
        if (stdinPipeConfigured) {
            // Close the input end of stdin
            close(stdinPipeFD[ 0 ]);
        }

        // Now call the parent's handler
        RunParentFunction();

        // waitpid() described in Advanced Programming
        // in the UNIX Environment, section 8.6, page 222
        // Or, even better, Linux System Programming
        // (Process Management -> Waiting for Terminated Child Processes)
        // page 142
        bool signalInterrupted(false);
        do {
            // This must be the only wait() function.  If you wait
            // for a pid that has nothing to do with us (for example
            // the child process's resources have already been
            // collected by a call to wait) then this waitpid() hangs.
            //
            // Of course it's a bug in wait().  Under what
            // circumstance should a system call like wait() hang
            // with no possible way to get out?
            //
            // According to Linux System Programming, page 143
            // (Process Management->Waiting for Terminated Child
            // Processes) it should return -1 if pid no longer
            // exists or has no relation to us.
            //
            // So wait() must never be called twice.
            //
            // If the client has already called wait() then we
            // will hang at this point.  But how do we protect
            // ourselves from that without introducing a race
            // condition?
            const int waitOptions(0);
            int childStatus;
            const int waitState = waitpid(childpid, &childStatus, waitOptions);

            if (waitState == childpid) {
                // We have just cleaned child process' resources up
                UpdateChildReturnStatus(childStatus);
                // Use childpid as a flag to indicate we shouldn't use
                // it in kill() or waitpid() any more
                childpid = -1;
            }
            if (waitState == -1) {
                // Either there was a signal, wait() had already been
                // called, or there was a real problem
                if (EINTR == errno) {
                    // Go around the loop again
                    signalInterrupted = true;
                } else if (ECHILD == errno) {
                    // Process has no children; wait() must have
                    // been called before.  No problem though.
                    // Use chilpid as a flag to indicate we shouldn't use
                    // it in waitpid() or kill() again.

                    // This is where we should end up if we call
                    // wait() twice.  But as already mentioned, this
                    // branch can not be tested.
                    childpid = -1;
                } else {
                    // There really has been a problem
                    std::stringstream exceptMsg;
                    exceptMsg << "ProcessController::Run() - "
                            "waitpid() - "
                            << strerror(errno);
                    throw std::runtime_error(exceptMsg.str().c_str());
                }
            }
        } while (signalInterrupted);

        // Close the remaining file descriptors
        if (stderrPipeConfigured) {
            close(stderrPipeFD[ 0 ]);
            //stderrPipeConfigured = false;
        }
        if (stdoutPipeConfigured) {
            close(stdoutPipeFD[ 0 ]);
            //stdoutPipeConfigured = false;
        }
        if (stdinPipeConfigured) {
            close(stdinPipeFD[ 1 ]);
            //stdinPipeConfigured = false;
        }
    }
}

void
ProcessController::Kill() {
    if ((-1 != this->childpid) && (0 != childpid)) {
        kill(this->childpid, SIGKILL);
    }
}

void
ProcessController::UpdateChildReturnStatus(const int status) {
    // See Linux System Programming, page 139
    // Advanced Programming in the UNIX Environment section 8.6
    if (WIFEXITED(status)) {
        // Child exited normally
        childReturnStatus = WEXITSTATUS(status);
        childProcessComplete = true;
    } else {
        // Child stopped by other means (signal, crash, whatever)
        childProcessComplete = false;
        if (WIFSIGNALED(status)) {
            // Child process stopped (killed?) by a signal
            childProcessSignal = WTERMSIG(status);
        }
    }
}