/* 
 * File:   GTestLister.cpp
 * Author: aga2hi
 * 
 * Created on March 12, 2015, 7:36 PM
 */

#include <stdlib.h>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <iostream>
#include <vector>
#include <string>

#include "GTestLister.h"

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

class ParentTestFunctor : public ProcessController::ParentFunctor {
public:

    ParentTestFunctor(GTestListOutputHandler * lh) : listHandler(lh) {
    }

    virtual ~ParentTestFunctor() {
    }

    void operator()(void);
    TestRunModel* GetTestRunModel();
private:

    GTestListOutputHandler * listHandler;
};

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

void
ParentTestFunctor::operator ()(void) {
    FILE * const fromGTest = GetInputFileBuffer();
    char * fgetFeedback;

    static const std::size_t bufferSize = 256;
    char buffer[ bufferSize ];

    assert(fromGTest != NULL);

    do {
        fgetFeedback = std::fgets(buffer, bufferSize, fromGTest);
        if (fgetFeedback == buffer) {

            // Strip the newline character away

            char * const nl = std::strchr(buffer, '\n');
            if (NULL != nl) {
                *nl = '\0';
            }

            // Now add buffer to the test run model

            listHandler->OnLineReceived(new std::string(buffer));
        }

        // When the other process terminates, the pipe receives
        // the hang-up event, and feof() will return non-zero

    } while (feof(fromGTest) == 0);

    // The other process has now finished
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

class ChildTestFunctorAdapter : public ProcessController::ChildFunctor {
    GTestProcessControl * gtpc;

public:

    ChildTestFunctorAdapter(GTestProcessControl * const adaptee) :
    gtpc(adaptee) {
    }

    ~ChildTestFunctorAdapter() {
        delete gtpc;
    }

    void operator()(void) {
        gtpc->Run();
    }
};


/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

GTestLister::GTestLister(GTestConfigurationModel & configModel)
: theTestRunModel(NULL) {

    // Construct the process control aparatus

    GTestListOutputHandler listHandler;
    ChildTestFunctorAdapter googleTest(new GTestProcessControl(configModel));
    ParentTestFunctor gtestOutput(& listHandler);

    ProcessController procController(&gtestOutput, &googleTest);
    procController.ConfigureStdoutPipe();

    // Run the google test programme, fetching list of tests.
    // stdout from googleTest is piped to gtestOutput, and
    // both functions run in seperate processes

    procController.Run();

    // When we get here, then both the parent process (gtestOutput)
    // and the child process (googleTest) have returned.  Get the
    // test run model from the list handler and update its
    // test id map (awkward two-step procedure, but TestRunModel
    // was designed to be used that way)

    theTestRunModel = listHandler.GetTestRunModel();
    theTestRunModel->UpdateTestIDMap();
}

TestRunModel *
GTestLister::GetTestRunModel() {

    return theTestRunModel;
}

GTestLister::~GTestLister() {

    delete theTestRunModel;
}

