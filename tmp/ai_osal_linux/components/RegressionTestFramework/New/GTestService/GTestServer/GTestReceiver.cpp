/*
 * File:   GTestReceiver.cpp
 * Author: aga2hi
 *
 * Created on March 10, 2015, 4:01 PM
 */

#include <stdio.h>
#include <iostream>
#include <TestRunModel.h>
#include "GTestReceiver.h"
#include "GTestRunner.h"
#include "GTestServiceBuffers.pb.h"

namespace {
    const unsigned int TestsRunOnceAtATime(1);
}

GTestReceiver::GTestReceiver(TestRunModel * trm, GTestConfigurationModel & cm)
: testRunModel(trm), configModel(cm) {
}

GTestReceiver::~GTestReceiver() {
}

void
GTestReceiver::OnPacketReceive(IConnection* Connection,
        AdapterPacket* Packet) {

    if (!Packet)
        return; //the connection has been closed: currently this won't be handled

    switch (Packet->GetID()) {
        case ID_DOWN_GET_VERSION:
            std::cerr << "ID_DOWN_GET_VERSION\n";
            SendVersion(Connection, Packet->GetSerial());
            break;

        case ID_DOWN_GET_STATE:
            std::cerr << "ID_DOWN_GET_STATE\n";
            SendState(Connection, Packet->GetSerial());
            break;

        case ID_DOWN_GET_TESTLIST:
            std::cerr << "ID_DOWN_GET_TESTLIST\n";
            SendTestList(Connection, Packet->GetSerial());
            break;

        case ID_DOWN_RUN_TESTS:
            std::cerr << "ID_DOWN_RUN_TESTS\n";
            RunTests(Connection, Packet);
            break;

        case ID_DOWN_ABORT_TESTRUN:
            std::cerr << "ID_DOWN_ABORT_TESTRUN\n";
            std::cerr << "(not yet supported)\n";
            break;

        case ID_DOWN_GET_ALL_RESULTS:
            std::cerr << "ID_DOWN_GET_ALL_RESULTS\n";
            std::cerr << "(not yet supported)\n";
            break;

        default:
            std::cerr << "unexpected packet id -> closing connection ...\n";
            Connection->Close();
            break;
    }
}

void
GTestReceiver::SendVersion(IConnection * const Connection, const unsigned long int serial) {

    std::string version = GTEST_SERVICE_VERSION;

    // Payload for the ID_UP_VERSION message is the above string

    AdapterPacket packet(
            (char) ID_UP_VERSION,
            0, 0,
            serial,
            version.length() + 1,
            const_cast<char*> (version.c_str()),
            Connection->GetID());

    Connection->Send(packet);
}

void
GTestReceiver::SendState(IConnection * const Connection, const unsigned long int serial) {

    char currentState = STATE_IDLE;
    int payloadSize = sizeof (char);

    // Payload for the ID_UP_STATE message is the above char

    AdapterPacket packet(
            (char) ID_UP_STATE,
            0, 0,
            serial,
            payloadSize,
            &currentState,
            Connection->GetID());

    Connection->Send(packet);
}

void
GTestReceiver::SendTestList(IConnection* Connection, const unsigned long int serial) {

    // Turn Test Run Model into byte stream

    GTestServiceBuffers::AllTestCases pbTestRunModel;
    testRunModel->ToProtocolBuffer(&pbTestRunModel);
    int serializedSize = pbTestRunModel.ByteSize();
    char serializedTestRunModel[serializedSize];
    pbTestRunModel.SerializeToArray(serializedTestRunModel, serializedSize);

    // Send it as payload

    AdapterPacket packet(
            (char) ID_UP_TESTLIST,
            0, 0,
            serial,
            serializedSize,
            serializedTestRunModel,
            Connection->GetID());

    Connection->Send(packet);
}

void
GTestReceiver::RunTests(IConnection * const connectionManager,
        AdapterPacket * const packet) try {

    // In case we need to run google test process again;

    bool testsHaventFinished(false);

    // The user might ask for the test to be run many times
    // though default will be once

    unsigned int repetitions(1);

    // First of all get the user's selection of tests
    // via the received packet (second argument)

    GTestServiceBuffers::TestLaunch* testSelection;
    testSelection = new GTestServiceBuffers::TestLaunch();
    testSelection->ParseFromArray(packet->GetPayload(), packet->GetPayloadSize());

    // Now clear the test run model up, because
    // it may contain results from a previous
    // run.  If we are recovering from a
    // power cycle, then we wouldn't do this.

    testRunModel->ResetIteration();
    testRunModel->ResetAllTestResults();

    // connectionManager knows how to send messages to the client
    // (eg GTestAdapter).
    // ResultReporter knows how to construct messages (eg. test
    // results), and uses connectionManager to send them.

    ResultReporter reporter(connectionManager);

    // Has the user specified how many times the test is to be run?

    if (testSelection->has_numberofrepeats()) {
        repetitions = testSelection->numberofrepeats();
    }

    for (unsigned int testCount = 0;
            testCount < repetitions;
            ++testCount,
            reporter.IncrementIteration(),
            testRunModel->NextIteration()) {

        // Now we can enable or disable tests in the
        // test run model according to the test
        // selection

        testRunModel->SelectTests(testSelection);

        // Now generate google test's filter, which tells
        // it which tests it should run

        std::string * gtestFilter = new std::string(
                testRunModel->GenerateTestFilterString()
                );

        // Construct the process control aparatus

        do {

            // Construct a new configuration model that is consistent
            // with the test selection.  GTestProcessControl will
            // need this; without it it won't know what programme
            // to run.

            GTestConfigurationModel testRunConfiguration(
                    configModel.GetGTestExecutable(),
                    configModel.GetWorkingDir(),
                    *gtestFilter,
                    TestsRunOnceAtATime,
                    configModel.GetRandomSeed(),
                    configModel.GetGTestConfFlags()
                    ); // There must be a better way of doing this TO DO !!

            // THERE IS A BETTER WAY OF DOING THAT, SEE
            // RunningState::CreateConfigurationModel(GTestServiceBuffers::TestLaunch* TestSelection)
            // IN RunningState.cpp, line 136

            // gtestOutput receives lines of output from google test
            // and hands them to resultsHandler, for parsing.

            ParentExecFunctor gtestOutput(testRunModel, &reporter);

            // googleTest knows how to run a google test programme, as
            // long as it has been given a configuration model.

            ChildTestFunctorAdapter googleTest(new GTestProcessControl(testRunConfiguration));

            // Finally procController knows how to run two functions
            // at the same time in their own processes, with pipes
            // connecting them together.

            ProcessController procController(&gtestOutput, &googleTest);
            procController.ConfigureStdoutPipe(ProcessController::NONE);
            procController.ConfigureStdinPipe(ProcessController::LINE);
            procController.ConfigureStderrPipe(ProcessController::LINE);

            // Run the google test programme, fetching tests results.
            // stdout from googleTest is piped to gtestOutput.

            procController.Run();

            // We should find out how the google test process finished

            if (false == procController.ChildExitNormally()) {

                // Send report to standard out, telling user which test
                // didn't finish

                std::cout << "Google Test programme terminated abnormally\n";
                std::cout << "It received signal ";
                std::cout << procController.GetChildSignalNumber();
                if (gtestOutput.GetSortingOfficeExitCode() == SortingOffice::TIMEOUT) {
                    std::cout << "\nGoogle Test programme timed out and was terminated";
                }
                std::cout << "\nLast Test Case Name was ";
                std::cout << reporter.GetTestCaseName();
                std::cout << "\nLast Test Name was ";
                std::cout << reporter.GetTestName();
                std::cout << std::endl;

                testsHaventFinished = true;

                // Replace the gtestFilter string, because we now
                // only want to run the tests that haven't started

                delete gtestFilter;
                gtestFilter = new std::string(
                        testRunModel->GeneratePartialTestFilterString(
                        reporter.GetTestCaseName(), reporter.GetTestName())
                        );

                // We ought to check gtestFilter.  If it contains "-*"
                // then there are no more tests left, i.e. the last
                // one crashed.

                if (gtestFilter->compare(EmptyTestFilter) == 0) {
                    delete gtestFilter;
                    testsHaventFinished = false;
                }

            } else {
                // Google test process finished of its own accord,
                // which means all the tests have run

                delete gtestFilter;
                testsHaventFinished = false;
            }
        } while (testsHaventFinished);

    } // End for( testCount < repetitions )

    // Both processes have just finished, and all we need to
    // do is inform our client programme (eg GTestAdapter)
    // that we have finished

    reporter.SendAllTestsFinished();

    // Clean up the heap

    delete testSelection;

} catch (std::exception & e) {
    fputs("GTestReceiver::RunTests() has caught an exception\n", stderr);
    fputs(e.what(), stderr);
}
