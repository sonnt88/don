/* 
 * File:   GTestLister.h
 * Author: aga2hi
 *
 * Created on March 12, 2015, 7:36 PM
 */

#ifndef GTESTLISTER_H
#define	GTESTLISTER_H

#include "GTestConfigurationModel.h"
#include "ProcessController.h"
#include "GTestProcessControl.h"
#include "GTestListOutputHandler.h"
#include "TestRunModel.h"

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

class GTestLister {
public:
    GTestLister(GTestConfigurationModel & cm);
    virtual ~GTestLister();

    TestRunModel* GetTestRunModel();

private:

    // Not copyable
    GTestLister(const GTestLister& orig);

    TestRunModel * theTestRunModel;
};

#endif	/* GTESTLISTER_H */

