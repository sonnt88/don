/* 
 * File:   main.cpp
 * Author: aga2hi
 *
 * Created on March 10, 2015, 2:14 PM
 */

/**
 * SPECIAL NOTE : YOU MUST LINK AGAINST PROTOBUF LIBRARY
 *                /usr/lib/libprotobuf.so.7
 *                Use this include path as well:
 *                /usr/local/include
 *
 * If you want to build this test in Netbeans, it won't work
 * unless you fiddle with the properties like this:
 *
 * Right-click the GTestServer project and choose properties
 *
 * Choose Linker, under Build
 *
 * In the right-hand pane, click the ... button to the right of Libraries
 * 
 * Click the Add Option button on the right
 * 
 * Select Other Option and then type in the following
 * -pthread -L/usr/local/lib -lprotobuf -lz -lpthread
 */

#include <iostream>
#include <cstdlib>
#include <unistd.h>  // sleep()
#include <cstdio>
#include <sstream>
#include <getopt.h>
#include "AdapterConnection.h"
#include "ConnectionManager.h"
#include "IPFilter.h"
#include "GTestReceiver.h"
#include "GTestLister.h"

using namespace std;

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

#define DEFAULT_SERVICE_PORT 6789

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

#define FILTER     ""
#define REPEAT     0
#define RAND_SEED  0
#define EXEC_FLAGS GTEST_CONF_EXEC_TESTS
#define LIST_FLAGS GTEST_CONF_LIST_ONLY

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

#define OVERWRITE_ENV 1
#define PATHLENGTH 1024
static char Pathname[PATHLENGTH];

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
// Command line arguments represented as a structure

/* ********** ********** ********** ********** ********** ********** */

struct GTestServiceuserCommands {
    std::string ExecutablePath;
    std::string WorkingDir;
    std::string IPFilterString;
    int TCPServicePort;
    int UDPAnnouncePort;
    std::string HumanReadableName;
    bool UsePersiFeatures;
    bool Resume;
    int
    GetExecTestsFlags();
};

/*
 * Calculate the flags value for the GTestConfigurationModel
 * constructor (it was designed that way)
 */

int
GTestServiceuserCommands::GetExecTestsFlags() {
    int theFlags(GTEST_CONF_EXEC_TESTS);
    if (UsePersiFeatures) {
        theFlags |= GTEST_CONF_USE_PERSI_FEATURES;
    }
    return theFlags;
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
// Show help/instructions on standard out

/* ********** ********** ********** ********** ********** ********** */

void printhelp(char* appName) {

    std::cout << "Usage: " << appName << " <options>\n";
    std::cout << "options:\n";
    std::cout << "\n";
    std::string htableLine = "+----------------------------------------+-----------+---------------------------+\n";

    std::cout << htableLine;
    std::cout << "| -h|--help                              | optional  |           print this help |\n";
    std::cout << htableLine;
    std::cout << "| -t|--test_executable=<test executable> | mandatory |   GTest executable to run |\n";
    std::cout << "|                                        |           |                tests from |\n";
    std::cout << htableLine;
    std::cout << "| -d|--working_dir=<working directory>   | optional  |     working directory for |\n";
    std::cout << "|                                        |           |                  test run |\n";
    std::cout << htableLine;
    std::cout << "| -p|--port=<port number>                | optional  |              default=" << DEFAULT_SERVICE_PORT << " |\n";
    std::cout << htableLine;
    std::cout << "| -f|--persi_feature                     | optional  | if set GTest will use the |\n";
    std::cout << "|                                        |           |       persistence feature |\n";
    std::cout << htableLine;
    std::cout << "| -n|--name=<name>                       | optional  |    custom name in service |\n";
    std::cout << "|                                        |           |              announcement |\n";
    std::cout << htableLine;
    std::cout << "| -i|--ipfilter=<ipfilter string>        | optional  | specify which connections |\n";
    std::cout << "|               e.g. \"192.168.1.0/24\"    |           |                 to accept |\n";
    std::cout << "|             or                         |           |                           |\n";
    std::cout << "|                    \"192.168.1.10\"      |           |                           |\n";
    std::cout << htableLine;
    std::cout << "| -r|--resume                            | optional  |      resume previous run, |\n";
    std::cout << "|                                        |           |   cannot be used together |\n";
    std::cout << "|                                        |           |     with any other option |\n";
    std::cout << htableLine;
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/*
 * Check that the startup parameters are usable
 */

bool
ValidateuserCommands(GTestServiceuserCommands userCommands) {
    if (userCommands.ExecutablePath.empty()) {
        std::cerr << "GTest executable not specified (option: -t <gtest executable>)\n";
        return false;
    }

    if (userCommands.TCPServicePort < 1 or userCommands.TCPServicePort > 65535) {
        std::cerr << "port number is invalid (option: -p <port>)\n";
        return false;
    }

    return true;
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/*
 * Turn command line arguments into GTestServiceuserCommands object
 */

GTestServiceuserCommands
GetuserCommands(int argc, char** argv) {

    GTestServiceuserCommands userCommands;
    userCommands.TCPServicePort = -1;
    userCommands.UsePersiFeatures = false;
    userCommands.Resume = false;

    int c;

    opterr = 0;
    std::stringstream sstream;

    int longIndex = 0;
    static const char* optString = "t:p:n:fhi:rd:";
    static const struct option longOpts[] = {
        { "test_executable", required_argument, NULL, 't'},
        { "working_dir", required_argument, NULL, 'd'},
        { "port", required_argument, NULL, 'p'},
        { "name", required_argument, NULL, 'n'},
        { "persi_feature", no_argument, NULL, 'f'},
        { "ipfilter", required_argument, NULL, 'i'},
        { "resume", no_argument, NULL, 'r'},
        { "help", no_argument, NULL, 'h'},
        { NULL, no_argument, NULL, 0}
    };

    while ((c = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1) {
        switch (c) {
            case 't':
                if (!userCommands.ExecutablePath.empty()) {
                    std::cerr << "GTest executable cannot be specified twice.\n";
                    exit(1);
                }
                userCommands.ExecutablePath = optarg;
                break;
            case 'd':
                if (!userCommands.WorkingDir.empty()) {
                    std::cerr << "Working directory cannot be specified twice.\n";
                    exit(1);
                }
                userCommands.WorkingDir = optarg;
                break;
                break;
            case 'p':
                if (userCommands.TCPServicePort != -1)
                    throw std::runtime_error("port has already been specified");
                sstream.clear();
                sstream << optarg;
                sstream >> userCommands.TCPServicePort;
                break;
            case 'f':
                userCommands.UsePersiFeatures = true;
                break;
            case 'h':
                printhelp(argv[0]);
                exit(0);
            case 'n':
                userCommands.HumanReadableName = optarg;
                break;
            case 'i':
                if (!userCommands.IPFilterString.empty()) {
                    std::cerr << "GTest IPFilter cannot be specified twice.\n";
                    exit(1);
                }
                userCommands.IPFilterString = optarg;
                break;
            case 'r':
                userCommands.Resume = true;
                break;
            default:
                fprintf(stderr, "unknown command line option: \"-%c\"\n", optopt);
                printhelp(argv[0]);
                exit(1);
        }
    }

    if (userCommands.Resume) {
        std::cout << "Sorry, resume from file not yet supported\n";
        exit(0);
    } else {
        char hostname[20];
        memset(hostname, 0, 20);
        gethostname(hostname, 20);

        if (userCommands.HumanReadableName.empty())
            userCommands.HumanReadableName = hostname;

        if (userCommands.TCPServicePort == -1)
            userCommands.TCPServicePort = DEFAULT_SERVICE_PORT;

        if (!ValidateuserCommands(userCommands)) {
            printhelp(argv[0]);
            exit(1);
        }
    }

    return userCommands;
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/*
 * Get instructions from the client (eg. GTestAdapter)
 */
int
ListenToClient(GTestServiceuserCommands & userCommands) {

    // Create a configuration model, which has the name of the
    // google test programme and the arguments it needs to
    // generate a list of tests

    GTestConfigurationModel testLister(
            userCommands.ExecutablePath,
            userCommands.WorkingDir,
            FILTER,
            REPEAT,
            RAND_SEED,
            GTEST_CONF_LIST_ONLY);

    // Run the google test programme using the configuration
    // model above, and then construct a test run model

    GTestLister gtest_list_tests(testLister);
    TestRunModel * trm = gtest_list_tests.GetTestRunModel();

    // Create a new configuration model that has the arguments
    // it needs to run the tests

    GTestConfigurationModel testRunner(
            userCommands.ExecutablePath,
            userCommands.WorkingDir,
            FILTER,
            REPEAT,
            RAND_SEED,
            userCommands.GetExecTestsFlags());

    // Instantiate a connection manager
    // using:
    //  (1) AdapterConnectionFactory
    //  (2) GTestReceiver, which needs the test run model and config model
    //  (3) IPFilter
    //
    // The connection manager and adapter connection factory
    // handle communications with the client (GTestAdapter).
    // GTestReceiver executes the instructions that come from
    // that client over a socket.

    ConnectionManager connectionManager(userCommands.TCPServicePort,
            new AdapterConnectionFactory(new GTestReceiver(trm, testRunner)),
            new IPFilter(userCommands.IPFilterString));

    // Now start listening out for client (GTestAdapter)
    // connections in its own thread.  When a connection
    // is made then GTestReceiver will receive and
    // execute instructions from the client.

    connectionManager.Start();

    // Listen to commands typed in by the user (running
    // in this, the main, thread).

    std::string instruction;
    while (std::cin >> instruction) {
        if ("stop" == instruction) {
            break;
        }
    }

    // Now shut down

    connectionManager.Stop();
    return 0;
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/*
 * Start the programme
 */
int
main(int argc, char** argv) {
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    // Write our pathname to an environment variable
    // so that child processes can find out where
    // we are (expected reason: test purposes)

    const char * cwdCheck = getcwd(Pathname, PATHLENGTH);
    assert(Pathname == cwdCheck);
    const int envCheck = setenv("GTestPath", Pathname, OVERWRITE_ENV);
    assert(0 == envCheck);

    // Find out what the user wants us to do by
    // parsing the command line arguments.  Next
    // function will call exit() if command line
    // arguments make no sense, or if help was
    // requested.

    GTestServiceuserCommands userCommands =
            GetuserCommands(argc, argv);

    // If we get here then we have enough information
    // to be getting along with.  Original GTestService
    // starts the service announcer at this point:
    // TO DO

    std::cout << "GTestService running\n";

    // Now listen to instructions from the client

    ListenToClient(userCommands);

    return 0;
}
