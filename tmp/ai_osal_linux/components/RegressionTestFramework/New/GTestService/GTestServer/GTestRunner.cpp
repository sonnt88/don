/* 
 * File:   GTestRunner.cpp
 * Author: aga2hi
 * 
 * Created on March 13, 2015, 8:55 PM
 */
#include <stdlib.h>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <iostream>
#include <vector>
#include <string>
#include <errno.h>
#include <stdexcept>
#include <sstream>

#include "GTestRunner.h"
#include "SortingOffice.h"
#include "AdapterConnection.h"
#include "AdapterPacket.h"
#include "GTestServiceBuffers.pb.h"


/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

// StderrHandler is a kind of FileDescriptorBuffer::Functor

StderrHandler::StderrHandler(ResultReporter * const rr) : reporter(rr) {
}

void
StderrHandler::operator()(std::string & incomingLine) {
    reporter->SendLineOutput(ResultReporter::GTEST_SOURCE_STDERR, incomingLine);
}


/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

// StdoutHandler is a kind of FileDescriptorBuffer::Functor

StdoutHandler::StdoutHandler(GTestExecOutputHandler & rh, ResultReporter * rr, FILE * const _toGTest)
: adaptee(rh), reporter(rr), toGTest(_toGTest) {
}

StdoutHandler::StdoutHandler(const StdoutHandler& org)
: adaptee(org.adaptee), reporter(org.reporter), toGTest(org.toGTest) {
}

void
StdoutHandler::operator()(std::string & incomingLine) {
    // A new line has arrived from Google Test's stdout

    // Forward the line up to the client so that it
    // can show it to the user

    reporter->SendLineOutput(ResultReporter::GTEST_SOURCE_STDOUT, incomingLine);

    // Parse the line, getting the test results.
    // Update the test run model and send results
    // up to the client programme (eg. GTestAdapter)
    // and acknowledgements to Google test

    adaptee.OnLineReceived(& incomingLine);
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

ParentExecFunctor::ParentExecFunctor(TestRunModel * trm, ResultReporter * rr)
: resultsHandler(trm, rr), reporter(rr) {
}

void
ParentExecFunctor::operator ()(void) {

    const unsigned int timeoutMilliseconds(10000);

    // It's only now that we are in the process controller that we
    // know the file descriptors.  Tell the reporter what they are
    // so that it can send acknowledgements.

    reporter->RegisterAcknowledgementFD(GetOutputFileBuffer());

    // stdoutReceiver reads data from stdout file descriptor, and breaks
    // it up into lines of text.  Those lines of text are given to
    // stdoutHandler, which parses the line in order to send a result
    // to the client, forwards the line to the client, and also sends
    // acknowledgement back to Google test.

    StdoutHandler stdoutHandler(resultsHandler, reporter, GetOutputFileBuffer());
    FileDescriptorBuffer stdoutReceiver(&stdoutHandler);

    // stderrReceiver reads data from stderr file descriptor and
    // breaks it up into line of text.  Those lines of text are
    // given to stderrHandler, which forwards them onto the client

    StderrHandler stderrHandler(reporter);
    FileDescriptorBuffer stderrReceiver(&stderrHandler);

    // Now we create a sorting office object, whose job it will
    // be to wait for something to read from one of its file
    // descriptors -- or timeout

    SortingOffice googleDistribution;
    googleDistribution.RegisterHandler(GetInputFileDescriptor(), &stdoutReceiver);
    googleDistribution.RegisterHandler(GetErrorFileDescriptor(), &stderrReceiver);

    // Now run the sorting office

    sortingOfficeExitCode = googleDistribution.DeliverMessages(timeoutMilliseconds);

    // We will get here either when the child process has terminated
    // or there has been a timeout

    if (SortingOffice::TIMEOUT == sortingOfficeExitCode) {
        // Google test programme is still running, so kill it
        // otherwise this parent process will wait() forever

        TerminateChildProc();
    }
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

ResultReporter::ResultReporter(IConnection * _conMan)
: connectionManager(_conMan), iteration(0), ackFD(NULL) {
}

void
ResultReporter::IncrementIteration() {
    ++iteration;
}

void
ResultReporter::RegisterAcknowledgementFD(FILE * const _ackFD) {
    ackFD = _ackFD;
}

void
ResultReporter::operator()(const std::string & TestCaseName,
        const std::string & TestName,
        int TestID,
        TEST_STATE state) {

    // state can be PASSED, FAILED, RUNNING or RESET
    // We only send a test result if it is PASSED
    // or FAILED

    bool success(false);
    bool needToSendResult(false);
    if (PASSED == state) {
        success = true;
        needToSendResult = true;

        runningTestID = 0;
        runningTestCaseName.clear();
        runningTestName.clear();
    }
    if (FAILED == state) {
        success = false;
        needToSendResult = true;

        runningTestID = 0;
        runningTestCaseName.clear();
        runningTestName.clear();
    }
    if (RUNNING == state) {
        needToSendResult = false;

        runningTestID = TestID;
        runningTestCaseName = TestCaseName;
        runningTestName = TestName;
    }
    if (needToSendResult) {

        // Build a test result message, see
        // RunningState::HandleSendResultPacketTask()
        // in RunningState.cpp line 211

        GTestServiceBuffers::TestResult testResult;

        testResult.set_id(TestID);
        testResult.set_iteration(iteration);
        testResult.set_success(success);

        const int serializedSize = testResult.ByteSize();
        char serializedTestResult[ serializedSize ];
        testResult.SerializeToArray(serializedTestResult, serializedSize);

        AdapterPacket packet(
                (char) ID_UP_TESTRESULT,
                0, 0,
                0,
                serializedSize,
                serializedTestResult,
                0);
        connectionManager->Send(packet);

        // Send acknowledgement to Google test so that it
        // can carry on with the next test

        if (ackFD != NULL) {
            const int fputresult = fputc('\n', ackFD);
            if (fputresult == EOF) {
                std::stringstream err;
                err << "ResultReporter::operator(); ";
                err << "fputc() could not send acknowledgement: - ";
                err << strerror(errno);
                throw std::runtime_error(err.str().c_str());
            }
        }
    }
}

void
ResultReporter::SendAllTestsFinished() {

    // When all the tests have run, see
    // RunningState::SendTestsDonePacket()
    // in RunningState.cpp line 189
    // to find out how to send an
    // AllTestsDone packet to the client

    AdapterPacket packet(
            (char) ID_UP_ALL_TESTS_DONE, 0, 0, 0, 0, NULL, 0);
    connectionManager->Send(packet);
}

void
ResultReporter::SendLineOutput(const LineSource src, const std::string & line) {
    GTestServiceBuffers::OutputSource pbOutputSource;
    GTestServiceBuffers::TestOutput pbTestOutput;

    switch (src) {
        case GTEST_SOURCE_STDOUT:
            pbOutputSource = GTestServiceBuffers::STDOUT;
            break;
        case GTEST_SOURCE_STDERR:
            pbOutputSource = GTestServiceBuffers::STDERR;
            break;
        default:
            throw std::invalid_argument("ResultReporter::SendLineOutput() - "
                    "unexpected line source");
    }

    pbTestOutput.set_output(line);
    pbTestOutput.set_source(pbOutputSource);

    int serializedSize = pbTestOutput.ByteSize();
    char* serializedTestOutput = (char*) malloc(serializedSize);

    pbTestOutput.SerializeToArray(serializedTestOutput, serializedSize);

    AdapterPacket packet(
            (char) ID_UP_TEST_OUTPUT,
            0, 0,
            0,
            serializedSize,
            serializedTestOutput,
            0);
    connectionManager->Send(packet);
}

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

ChildTestFunctorAdapter::ChildTestFunctorAdapter(GTestProcessControl * const adaptee)
: gtpc(adaptee) {
}

ChildTestFunctorAdapter::~ChildTestFunctorAdapter() {
    delete gtpc;
}

void
ChildTestFunctorAdapter::operator()(void) {
    gtpc->Run();
}
