/* 
 * File:   GTestRunner.h
 * Author: aga2hi
 *
 * Created on March 13, 2015, 8:55 PM
 */

#ifndef GTESTRUNNER_H
#define	GTESTRUNNER_H

#include "GTestConfigurationModel.h"
#include "ProcessController.h"
#include "GTestProcessControl.h"
#include "GTestExecOutputHandler.h"
#include "TestRunModel.h"
#include "ConnectionManager.h"
#include "FileDescriptorBuffer.h"

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

class ResultReporter : public GTestExecOutputHandler::Functor {
    IConnection * connectionManager;
    int iteration;
    FILE * ackFD;

    int runningTestID;
    std::string runningTestCaseName;
    std::string runningTestName;
public:

    ResultReporter(IConnection * _conMan);

    // RegisterAcknowledgementFD(FILE *) should be called
    // before test results arrive, that is, before
    // operator() is called, so that we know where to send
    // acknowledgements

    void
    RegisterAcknowledgementFD(FILE * const _ackFD);

    // Next function is meant to be called automatically
    // by GTestExecOutputHandler each time a new test
    // result is known

    virtual void
    operator()(const std::string & TestCaseName,
            const std::string & TestName,
            int TestID,
            TEST_STATE state);

    // The next three functions tell the caller which
    // test has not finished.  The intention is to
    // find out which test timed-out, or crashed or
    // whatever.

    virtual int
    GetTestID() {
        return runningTestID;
    }

    virtual const std::string &
    GetTestCaseName() {
        return runningTestCaseName;
    }

    virtual const std::string &
    GetTestName() {
        return runningTestName;
    }

    // The next function forwards the output from Google test
    // to the client so that it can display them

    enum LineSource {
        GTEST_SOURCE_STDOUT, GTEST_SOURCE_STDERR
    };

    void
    SendLineOutput(const LineSource src, const std::string & line);

    // The next two functions are meant to be called
    // at the end of a test run -- either to repeat
    // it (a new iteration of the tests) or report
    // to the client programme (eg. GTestAdapter)
    // that all the tests have finished.

    void
    IncrementIteration();
    void
    SendAllTestsFinished();
};

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

class ParentExecFunctor : public ProcessController::ParentFunctor {
public:

    ParentExecFunctor(TestRunModel * trm, ResultReporter * rr);

    virtual ~ParentExecFunctor() {
    }

    virtual void operator()(void);

    virtual SortingOffice::DeliveryCode GetSortingOfficeExitCode() {
        return sortingOfficeExitCode;
    }

private:

    GTestExecOutputHandler resultsHandler;
    SortingOffice::DeliveryCode sortingOfficeExitCode;
    ResultReporter * reporter;
};

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

class ChildTestFunctorAdapter : public ProcessController::ChildFunctor {
    GTestProcessControl * gtpc;

public:

    ChildTestFunctorAdapter(GTestProcessControl * const adaptee);

    virtual
    ~ChildTestFunctorAdapter();

    void
    operator()(void);
};

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

// StdoutHandler needs to both parse the standard output from Google
// test and forward it onto the client

// As far as parsing is concerned, it acts simply as an adaptor
// to the GTestExecOutputHandler object, which does not have a
// operator() function

// As far as forwarding is concerned, it acts as an adaptee to
// the ResultReporter object, whose operator() function is not
// meant to be used here

class StdoutHandler : public FileDescriptorBuffer::Functor {
public:

    StdoutHandler(GTestExecOutputHandler & rh, ResultReporter * rr, FILE * const _toGTest);
    StdoutHandler(const StdoutHandler & org);

    virtual ~StdoutHandler() {
    }

    virtual void operator()(std::string & incomingLine);

private:

    GTestExecOutputHandler & adaptee;
    ResultReporter * const reporter;
    FILE * const toGTest;
};

/* ********** ********** ********** ********** ********** ********** */
/* ********** ********** ********** ********** ********** ********** */

/* ********** ********** ********** ********** ********** ********** */

// StderrHandler just forwards stderr output from Google test to
// the client

// It acts as an adaptee to the ResultReporter object, whose
// operator() function is not meant to be used here

class StderrHandler : public FileDescriptorBuffer::Functor {
public:

    StderrHandler(ResultReporter * const);

    virtual ~StderrHandler() {
    }

    virtual void operator()(std::string & incomingLine);

private:

    ResultReporter * const reporter;
};

#endif	/* GTESTRUNNER_H */

