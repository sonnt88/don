/* 
 * File:   GTestServiceControl.h
 * Author: aga2hi
 *
 * Created on March 10, 2015, 4:01 PM
 */

#ifndef GTESTSERVICECONTROL_H
#define	GTESTSERVICECONTROL_H

#include "AdapterConnection.h"
#include "TestRunModel.h"
#include "GTestConfigurationModel.h"

#define GTEST_SERVICE_VERSION "0.1"

enum GTEST_SERVICE_STATE {
    STATE_IDLE = 0,
    STATE_RUNNING_TESTS = 1,
    STATE_INVALID = 2,
    STATE_RESET = 3
};

class GTestReceiver : public IPacketReceiver {
public:
    GTestReceiver(TestRunModel * trm, GTestConfigurationModel & cm);
    virtual ~GTestReceiver();

    virtual void
    OnPacketReceive(IConnection* Connection, AdapterPacket* Packet);
    virtual void
    SendVersion(IConnection * const Connection, const unsigned long int serial);
    virtual void
    SendState(IConnection * const Connection, const unsigned long int serial);
    virtual void
    SendTestList(IConnection * const Connection, const unsigned long int serial);
    virtual void
    RunTests(IConnection * const Connection, AdapterPacket * const packet);
private:

    // Not copyable
    GTestReceiver(const GTestReceiver& orig);

    TestRunModel * testRunModel;
    GTestConfigurationModel configModel;
};

#endif	/* GTESTSERVICECONTROL_H */

