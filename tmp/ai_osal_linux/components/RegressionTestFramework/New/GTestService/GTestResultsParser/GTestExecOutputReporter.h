/*
 * GTestExecOutputReporter.h
 *
 *  Created on: Jul 19, 2012
 *      Author: oto4hi
 */

#ifndef CHATTYGTESTEXECOUTPUTHANDLER_H_
#define CHATTYGTESTEXECOUTPUTHANDLER_H_

#include "GTestExecOutputHandler.h"

/**
 * \brief The ChattyGTestExecOutputHandler forwards new test result events
 *  and the all tests finished event to the client
 */
class GTestExecOutputReporter : public GTestExecOutputHandler {
private:
protected:
    /**
     * \brief OnNewTestResult() send test result to the client
     */
    virtual void OnNewTestResult(std::string TestCaseName,
            std::string TestName,
            TEST_STATE state);

    /**
     * \brief OnAllLinesProcessed() sends all tests finished to client
     *  or ReactOnCrash
     */
    virtual void OnAllLinesProcessed();
public:
    /**
     * \brief constructor
     * @param trm        TestRunModel, to whom we send test results
     */
    GTestExecOutputReporter(TestRunModel* trm);

    /**
     * destructor
     */
    ~GTestExecOutputReporter();
};

#endif /* CHATTYGTESTEXECOUTPUTHANDLER_H_ */
