/* 
 * File:   GTestExecOutputHandler.cpp
 * Author: oto4hi
 * 
 * Created on May 9, 2012, 5:29 PM
 */

#include <stdexcept>
#include <algorithm>
#include <sstream>

#include "GTestExecOutputHandler.h"

#include <cassert>

const std::string GTestExecOutputHandler::runString = "[ RUN      ]";
const std::string GTestExecOutputHandler::passedString = "[       OK ]";
const std::string GTestExecOutputHandler::failedString = "[  FAILED  ]";
const std::string GTestExecOutputHandler::resetString = "[  RESET   ]";

const std::string GTestExecOutputHandler::nextIterationStringHead =
        "Repeating all tests (iteration ";
const std::string GTestExecOutputHandler::nextIterationStringTail = ") . . .";

GTestExecOutputHandler::GTestExecOutputHandler(TestRunModel* TestRun)
: ReactToTestResult(NULL)
/*: GTestQueuedOutputHandler() */ {
    if (!TestRun) {
        throw std::invalid_argument(
                "GTestExecOutputHandler::GTestExecOutputHandler() - "
                "TestRun cannot be NULL.");
    }

    this->testRunModel = TestRun;
    this->running = false;
    this->processFailure = false;
    this->resetRequested = false;
}

GTestExecOutputHandler::GTestExecOutputHandler(TestRunModel* TestRun, 
        Callback onNewTestResult)
: ReactToTestResult(onNewTestResult), testRunModel(TestRun) {
    if (!TestRun) {
        throw std::invalid_argument(
                "GTestExecOutputHandler::GTestExecOutputHandler() - "
                "TestRun cannot be NULL.");
    }

    this->running = false;
    this->processFailure = false;
    this->resetRequested = false;
}

GTestExecOutputHandler::~GTestExecOutputHandler() {
}

void GTestExecOutputHandler::OnLineReceived(std::string* Line) {
    // THIS IS OUR ENTRY POINT

    // Do we still want to call GTestQueuedOutputHandler::OnLineReceived(Line);

    // We need to get to GTestExecOutputHandler::processLine(std::string Line)
    // That's where the line parsing is done
    GTestExecOutputHandler::processLine(*Line);
}

void GTestExecOutputHandler::OnProcessTerminated(int status) {
    // Process Termination not handled this way any more
}

std::string
GTestExecOutputHandler::extractTestIdentifier(std::string Line, int StartPos) {
    while (Line[StartPos] == ' ' ||
            Line[StartPos] == '\r' ||
            Line[StartPos] == '\t') {
        StartPos++;
        if (StartPos >= Line.size())
            throw std::invalid_argument(
                "GTestExecOutputHandler::extractTestIdentifier() - "
                "The line argument does not seem to contain a test case "
                "or test name.");
    }

    std::string subStr = Line.erase(0, StartPos);
    size_t posOfWhiteSpace = subStr.find_first_of(" \n\r\t");

    if (posOfWhiteSpace != std::string::npos)
        subStr = subStr.substr(0, posOfWhiteSpace);

    return subStr;
}

std::string
GTestExecOutputHandler::extractTestCaseName(std::string Line, int StartPos) {
    std::string testIdentifier = this->extractTestIdentifier(Line, StartPos);
    size_t posOfSeperator = testIdentifier.find(".");

    if (posOfSeperator == std::string::npos)
        throw std::invalid_argument(
                "GTestExecOutputHandler::extractTestCaseName(): "
            "Line argument doesn't contain a test case or test name.");

    std::string testCaseName = testIdentifier.substr(0, posOfSeperator);

    if (testCaseName.empty())
        throw std::invalid_argument(
                "GTestExecOutputHandler::extractTestCaseName(): "
            "Line argument doesn't contain a test case name.");

    return testCaseName;
}

std::string 
GTestExecOutputHandler::extractTestName(std::string Line, int StartPos) {
    std::string testIdentifier = this->extractTestIdentifier(Line, StartPos);
    size_t posOfSeperator = testIdentifier.find(".");

    if (posOfSeperator == std::string::npos)
        throw std::invalid_argument("GTestExecOutputHandler::extractTestName(): "
            "Line argument doesn't contain a test case or test name.");

    if (posOfSeperator == Line.size() - 1)
        throw std::invalid_argument("GTestExecOutputHandler::extractTestName(): "
            "Line argument doesn't contain a test name.");

    std::string testCaseName = testIdentifier.substr(
            posOfSeperator + 1, Line.size() - posOfSeperator - 1);

    if (testCaseName.empty())
        throw std::invalid_argument("GTestExecOutputHandler::extractTestName(): "
            "Line argument doesn't contain a test case name.");

    const std::string notAllowed(" \t,;");
    const std::string::size_type posNotAllowed=testCaseName.find_first_of(notAllowed);
    if (std::string::npos != posNotAllowed) {
        testCaseName.erase(posNotAllowed);
    } // end if

    return testCaseName;
}

void GTestExecOutputHandler::setTestState(std::string TestCaseName,
        std::string TestName,
        TEST_STATE state) {
    TestModel* test = testRunModel->GetTestCase(TestCaseName)->GetTest(TestName);
    int iteration = this->testRunModel->GetIteration();

    switch (state) {
        case PASSED:
            test->AppendTestResult(iteration, true);
            testRunModel->SetCurrentTestID(-1);
            break;
        case FAILED:
            test->AppendTestResult(iteration, false);
            testRunModel->SetCurrentTestID(-1);
            break;
        case RESET:
            testRunModel->SetCurrentTestID(test->GetID());
            break;
        case RUNNING:
            testRunModel->SetCurrentTestID(test->GetID());
            break;
        default:
            throw std::runtime_error("GTestExecOutputHandler::setTestState(): "
                    "unexpected test state");
    }

    if(ReactToTestResult) {
        Functor & reactToTestResult(* ReactToTestResult);
        // If the client tries to call test->GetTestResult(iteration)
        // then TestModel will throw an exception.  Why?
        // Because it is such a heap of shit.
        // We can't use TestModel as a store of test results.
        // result and testID as seperate arguments
        reactToTestResult( TestCaseName, TestName, test->GetID(), state );
    }
}

void GTestExecOutputHandler::setRunning(std::string Line) {
    if (this->testRunModel->GetIteration() == 0) {
        this->testRunModel->NextIteration();
    }

    std::string testCaseName = extractTestCaseName(Line, this->runString.size());
    std::string testName = extractTestName(Line, this->runString.size());
    this->setTestState(testCaseName, testName, RUNNING);
}

void GTestExecOutputHandler::setPassed(std::string Line) {
    if (this->testRunModel->GetIteration() == 0) {
        this->testRunModel->NextIteration();
    }

    std::string testCaseName = extractTestCaseName(Line, this->runString.size());
    std::string testName = extractTestName(Line, this->runString.size());
    const TEST_STATE state(PASSED);
    this->setTestState(testCaseName, testName, state);
}

void GTestExecOutputHandler::setFailed(std::string Line) {
    if (this->testRunModel->GetIteration() == 0) {
        this->testRunModel->NextIteration();
    }

    std::string testCaseName = extractTestCaseName(Line, this->runString.size());
    std::string testName = extractTestName(Line, this->runString.size());
    const TEST_STATE state(FAILED);
    this->setTestState(testCaseName, testName, state);
}

void GTestExecOutputHandler::setReset(std::string Line) {
    if (this->testRunModel->GetIteration() == 0) {
        this->testRunModel->NextIteration();
    }

    std::string testCaseName = extractTestCaseName(Line, this->runString.size());
    std::string testName = extractTestName(Line, this->runString.size());
    const TEST_STATE result(RESET);
    this->setTestState(testCaseName, testName, result);
}

// void GTestExecOutputHandler::handleResetRequest( void ) {
// }

bool GTestExecOutputHandler::isTestEndLine(std::string Line) {
    if (Line[Line.length() - 1] == ')')
        return true;
    return false;
}

bool
GTestExecOutputHandler::checkForNextIteration(std::string Line, int& iteration) {
    // Note that output handler does not manage test repeats any more
}

void GTestExecOutputHandler::processTestEndLine(std::string Line) {
    // check if the current line indicates a passed test
    if (!Line.compare(0, this->passedString.size(), this->passedString)) {
        this->setPassed(Line);
    }//check if the current line indicates a failed test
    else if (!Line.compare(0, this->failedString.size(), this->failedString)) {
        this->setFailed(Line);
    }//check if the current line indicates a reset request
    else if (!Line.compare(0, this->resetString.size(), this->resetString)) {
        this->resetRequested = true;
        this->setReset(Line);
    }
}

void GTestExecOutputHandler::processLine(std::string Line) {
    if (!Line.compare(0, this->runString.size(), this->runString)) {
        this->setRunning(Line);
        return;
    }

    if (this->isTestEndLine(Line)) {
        this->processTestEndLine(Line);
    }
}

void GTestExecOutputHandler::ProcessTestResultLines() {
#ifdef _NO_NEW_THREADS
#endif  // _NO_NEW_THREADS
}

void GTestExecOutputHandler::WaitForProcessTestResultCompleted() {
#ifdef _NO_NEW_THREADS
#endif  // _NO_NEW_THREADS
}
