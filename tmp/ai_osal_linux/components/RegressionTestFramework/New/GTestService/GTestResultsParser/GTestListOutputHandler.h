/* 
 * File:   GTestListOutputHandler.h
 * Author: oto4hi
 *
 * Created on May 8, 2012, 7:19 PM
 */

#ifndef GTESTLISTOUTPUTHANDLER_H
#define	GTESTLISTOUTPUTHANDLER_H

#include "TestRunModel.h"
#include <cstdio>
#include <string>
#include <vector>

#ifndef FRIEND_TEST
#define FRIEND_TEST(TestCase, Test)
#endif

/**
 * \brief This output handler is used to build a TestRunModel from the test list output of a GTest binary
 * run by the GTestProcessController
 */
class GTestListOutputHandler {
private:
    FRIEND_TEST(GTestListOutputHandlerTests, CreateTestRunModelValidInput);
    FRIEND_TEST(GTestListOutputHandlerTests, CreateTestRunModelTestWithoutTestCase);
    FRIEND_TEST(GTestListOutputHandlerTests, CreateTestRunModelEmptyTestCase);
    FRIEND_TEST(GTestListOutputHandlerTests, CreateTestRunModelEmptyTest);
    FRIEND_TEST(GTestListOutputHandlerTests, CreateTestRunModelRobustnessTest);
    FRIEND_TEST(GTestExecOutputHandlerTests, SimulateStandardTestRun);

    std::vector<std::string *> gtestList;

    /**
     * \brief generate the TestRunModel from the GTest output lines stored in the queue of the GTestQueuedOutputHandler
     */
    TestRunModel* generateTestRunModel();
public:
    /**
     * \brief default constructor
     */
    GTestListOutputHandler();

    /**
     * \brief getter for the test run model
     * the test run model will be generated on the call of this function
     */
    TestRunModel* GetTestRunModel();

    /**
     * \brief lines of text from google test arrive here
     */
    void OnLineReceived(std::string *);

    /**
     * \brief clear stored data, recover memory
     */
    void CleanUp();

    /**
     * \brief Dunno what this is supposed to do; we don't need it anyway
     */
    void OnProcessTerminated(int) {
    }

    /**
     * \brief Dunno what this is supposed to do; we don't need it anyway
     */
    void WaitForOutputCompleted() {
    }

    /**
     * \brief destructor
     */
    virtual ~GTestListOutputHandler();
};

#endif	/* GTESTLISTOUTPUTHANDLER_H */

