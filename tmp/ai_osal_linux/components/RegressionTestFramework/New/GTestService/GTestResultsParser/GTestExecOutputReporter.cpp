/*
 * GTestExecOutputReporter.cpp
 *
 *  Created on: Jul 19, 2012
 *      Author: oto4hi
 */

#include "GTestExecOutputReporter.h"

#include <iostream>
#include <sstream>
#include <cassert>
#include <stdexcept>

#define TIMEOUT_PERIOD 30


// ////////////////////////////////////////////////////////////////////////////////
// Global (static) variables
// ////////////////////////////////////////////////////////////////////////////////


// ////////////////////////////////////////////////////////////////////////////////
// Global C functions
// ////////////////////////////////////////////////////////////////////////////////


// A plain C function that runs in a seperate thread


// ////////////////////////////////////////////////////////////////////////////////
// C++ Class Methods
// ////////////////////////////////////////////////////////////////////////////////

GTestExecOutputReporter::GTestExecOutputReporter(TestRunModel* trm)
: GTestExecOutputHandler(trm) {
    assert(NULL != trm);
}

void GTestExecOutputReporter::OnNewTestResult(std::string TestCaseName,
        std::string TestName,
        TEST_STATE state) {
    TestModel* model = this->testRunModel->
            GetTestCase(TestCaseName)->GetTest(TestName);

    switch (state) {
        case RESET:
        {
            break;
        }
        case PASSED:
        case FAILED:
        {
            break;
        }
        case RUNNING:
            break;
        default:
            throw std::runtime_error("ChattyGTestExecOutputHandler::"
                    "OnNewTestResult() - unexpected test state");
    }
}

void GTestExecOutputReporter::OnAllLinesProcessed() {

    // We get called when the test process has terminated and
    // all the lines of output have been processed - or -
    // a power cycle has been requested.

    // We need to distinguish here whether a power cycle had
    // been requested, or whether the test process really has
    // terminated.
    if (!(this->resetRequested)) {

        // We must enqueue either a crash task or a send
        // packets done task

        if (this->processFailure) {
            // The test process has crashed

        } else {
            // No more test results to process; tests over

        }

        // Now tell GTestAdapter the good news (all tests complete)
    }
}

GTestExecOutputReporter::~GTestExecOutputReporter() {
}
