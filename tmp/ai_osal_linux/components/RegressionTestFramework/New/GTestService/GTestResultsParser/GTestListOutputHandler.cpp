/* 
 * File:   GTestListOutputHandler.cpp
 * Author: oto4hi
 * 
 * Created on May 8, 2012, 7:19 PM
 */

#include "GTestListOutputHandler.h"
#include <stdexcept>

GTestListOutputHandler::GTestListOutputHandler() {
}

GTestListOutputHandler::~GTestListOutputHandler() {
    CleanUp();
}

void
GTestListOutputHandler::CleanUp() {
    std::vector<std::string *>::iterator line = gtestList.begin();
    std::vector<std::string *>::iterator last = gtestList.end();

    for (; line != last; ++line) {
        if (NULL != *line) {
            delete *line;
            *line = NULL;
        }
    }

    gtestList.clear();
}

TestRunModel*
GTestListOutputHandler::generateTestRunModel() {
    TestRunModel* testRunModel = new TestRunModel();
    int testIDCounter = 0;

    std::vector<std::string *>::iterator line = gtestList.begin();
    std::vector<std::string *>::iterator last = gtestList.end();

    for (; line != last; ++line) {
        std::string & name = **line;

        name = name.erase(0, name.find_first_not_of(" \n\r\t"));
        name = name.erase(name.find_last_not_of(" \n\r\t") + 1);

        if (name.length() == 0)
            continue;

        if (!name.compare(name.length() - 1, 1, ".")) {
            name = name.erase(name.find_last_not_of(".") + 1);
            testRunModel->AddTestCase(new TestCaseModel(name));
        } else {
            if (testRunModel->GetTestCaseCount() == 0)
                throw std::logic_error(
                    "GTestListOutputHandler::generateTestRunModel() - "
                    "There is no test case to which to add a test.");

            TestCaseModel* testCase = testRunModel->
                    GetTestCase(testRunModel->GetTestCaseCount() - 1);
            TestModel* test = new TestModel(name, testIDCounter);
            testCase->AddTest(test);

            if (testCase->IsDefaultDisabled())
                test->Enabled = false;

            testIDCounter++;
        }
    }

    return testRunModel;
}

void
GTestListOutputHandler::OnLineReceived(std::string * line) {
    // Don't want to add the first line from a real google test run
    // "Running main() from gtest_main.cc"
    if (std::string::npos == line->find("main()")) {
        gtestList.push_back(line);
    }
}

// generate the test run model; it's the caller's responsibility
// to delete the result afterwards

TestRunModel*
GTestListOutputHandler::GetTestRunModel() {
    return this->generateTestRunModel();
}
