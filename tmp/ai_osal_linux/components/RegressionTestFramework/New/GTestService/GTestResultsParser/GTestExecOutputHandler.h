/* 
 * File:   GTestExecOutputHandler.h
 * Author: oto4hi
 *
 * Created on May 9, 2012, 5:29 PM
 */

#ifndef GTESTEXECOUTPUTHANDLER_H
#define	GTESTEXECOUTPUTHANDLER_H

/* #include "GTestQueuedOutputHandler.h" */
#include <string>
#include "TestRunModel.h"
#include "TestStates.h"

#ifndef FRIEND_TEST
#define FRIEND_TEST(TestCase, Test)
#endif

/**
 * \brief output handler for a test run
 * This output handler asynchronously evaluates the GTest output line by line.
 * Results of executed tests are added to the global test run model.
 * The event handlers OnNewTestResult() and OnAllLinesProcessed() can be used by
 * classes deriving from GTestExecOutputHandler
 */
class GTestExecOutputHandler /*: public GTestQueuedOutputHandler*/ {
public:

    class Functor {
    public:
        virtual void
        operator()(const std::string & TestCaseName,
                const std::string & TestName,
                int TestID,
                TEST_STATE state) = 0;
    };
    typedef Functor * Callback;

private:
    // FRIEND_TEST(GTestExecOutputHandlerTests, SimulateStandardTestRun);

    static const std::string runString;
    static const std::string passedString;
    static const std::string failedString;
    static const std::string resetString;
    static const std::string nextIterationStringHead;
    static const std::string nextIterationStringTail;

    bool running;
    Callback ReactToTestResult;

    // pthread_t processTestResultLinesThread;

    std::string extractTestIdentifier(std::string Line, int StartPos);
    std::string extractTestCaseName(std::string Line, int StartPos);
    std::string extractTestName(std::string Line, int StartPos);

    bool isTestEndLine(std::string Line);

    void processLine(std::string Line);
    void processTestResultLines();
    static void* processTestResultLinesThreadFunc(void* thisPtr);

    bool checkForNextIteration(std::string Line, int& iteration);

    void processTestEndLine(std::string Line);
    void setTestState(std::string TestCaseName,
            std::string TestName,
            TEST_STATE state);

    void setRunning(std::string Line);
    void setPassed(std::string Line);
    void setFailed(std::string Line);
    void setReset(std::string);
protected:
    TestRunModel* testRunModel;
    bool processFailure;
    bool resetRequested;

    /**
     * \brief handler for a new test result
     * @param TestCaseName name of the corresponding test case
     * @param TestName name of the test
     * @param state test result, either PASSED or FAILED
     */
    virtual void OnNewTestResult(std::string TestCaseName,
            std::string TestName,
            TEST_STATE state) {
    };

    /**
     * \brief handler for the end of input
     */
    virtual void OnAllLinesProcessed() {
    };

public:
    /**
     * \brief handle a received line
     * @param Line pointer to the received line
     * The output handler is responsible for deleting the Line object
     */
    virtual void OnLineReceived(std::string* Line);

    /**
     * \brief consturctor
     * @param TestRun pointer to the TestRunModel to use
     */
    GTestExecOutputHandler(TestRunModel* TestRun);

    /**
     * \brief constructor
     * @param TestRun pointer to the TestRunModel
     * @param onNewTestResult to be called for each new test restult
     */
    GTestExecOutputHandler(TestRunModel* TestRun, Callback onNewTestResult);

    /**
     * \brief start to asynchronously process the incoming output lines
     */
    void ProcessTestResultLines();

    /**
     * \brief block until the processing of lines has finished
     */
    void WaitForProcessTestResultCompleted();

    /**
     * \brief handle the child process terminated event
     * @param status the exit value of the child process
     */
    virtual void OnProcessTerminated(int status);

    /**
     * destructor
     */
    virtual ~GTestExecOutputHandler();
};

#endif	/* GTESTEXECOUTPUTHANDLER_H */

