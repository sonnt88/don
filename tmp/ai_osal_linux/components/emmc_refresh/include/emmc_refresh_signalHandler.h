/*****************************************************************************************
* Copyright (C) RBEI, 2013
* This software is property of Robert Bosch.
* Unauthorized duplication and disclosure to third parties is prohibited.
******************************************************************************************/
/******************************************************************************
* FILE         : emmc_refresh.h
*             
* DESCRIPTION  : This file is the header file for the emmc refresh utility                                
*                
*             
* AUTHOR(s)    :  (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*-----------|---------------------|--------------------------------------
*01.AUG.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/
#ifndef REFRESH_SIGNALHANDLER_HEADER

#define REFRESH_SIGNALHANDLER_HEADER
/************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#define _GNU_SOURCE
#define _LARGEFILE64_SOURCE
#define _FILE_OFFSET_BITS 64

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

#define TERMINATED    0x02
#define MAXLENGTH     255

typedef int           tS32;
typedef unsigned char tU8;
typedef char          tS8;
typedef unsigned int  tU32;
extern tU32 u32EnableDebug;

#define debug_print(...) \
            if (u32EnableDebug && (plogFilePtr != NULL))\
               fprintf(plogFilePtr, ##__VA_ARGS__); 
//#define debug_print(...) \
               fprintf(stdout, ##__VA_ARGS__); 
#else
#error REFRESH_SIGNALHANDLER_HEADER included several times
#endif
