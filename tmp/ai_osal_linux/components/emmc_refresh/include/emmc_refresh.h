/*****************************************************************************************
* Copyright (C) RBEI, 2013
* This software is property of Robert Bosch.
* Unauthorized duplication and disclosure to third parties is prohibited.
******************************************************************************************/
/******************************************************************************
* FILE         : emmc_refresh.h
*             
* DESCRIPTION  : This file is the header file for the emmc refresh utility                                
*                
*             
* AUTHOR(s)    :  (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
*-----------|---------------------|--------------------------------------
*06.JUN.2016| Version 1.1         | CHAKITHA SARASWATHI (RBEI/ECF5)
*                                 | CFG3-1958: Implements refresh for the
*                                 | targets using micron eMMC.
* -----------------------------------------------------------------------
***************************************************************************/
#ifndef EMMC_REFRESH_HEADER

#define EMMC_REFRESH_HEADER
/************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#define _GNU_SOURCE
#define _LARGEFILE64_SOURCE
#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <limits.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
/************************************************************************
|defines and macros (scope: global)
|-----------------------------------------------------------------------*/
#define METADATA_DIR_PATH "/var/opt/bosch/persistent/emmc_refresh/"
#define LOG_FILE_PATH "/var/opt/bosch/persistent/emmc_refresh/debug.txt"

#define CONFIG_PATH "/var/opt/bosch/static/emmc_refresh/"

#define METADATA_FILE_PATH "/var/opt/bosch/persistent/emmc_refresh/metadata.txt"
#define DUMMY_METADATA_PATH "/var/opt/bosch/persistent/emmc_refresh/metadata_dummy.txt"
#define METADATA_BACKUP_PATH "/var/opt/bosch/persistent/emmc_refresh/metadata_backup.txt"

#define ROOT_DEVICE_NODE	"mount  | grep -w / | awk {'print $1'}"
#define debug_print(...) \
            if (u32EnableDebug && (plogFilePtr != NULL))\
               fprintf(plogFilePtr, ##__VA_ARGS__); 
//#define debug_print(...) \
               fprintf(stdout, ##__VA_ARGS__); 
/* After every 6.25% of the scetors covered, create a Metadata Backup File
   This Percentage is chosen so that it is an integral multiple of the 128 sector*/
   
#define BACKUP_THRESHHOLD(s32Total_sector)  ((s32Total_sector <= 65536)?\
                                            (s32Total_sector):((s32Total_sector*8)/128))   
#define SWAB_ALIGN_OFFSET 2
#define INPUT_BLOCK_SLOP(page_size) (page_size - 1)

#define REFRESH_INPROGRESS 				0x00
#define TERMINATED         				0x02
#define NOREFRESH_NEEDED   				0x01
#define REFRESH_NEEDED     				0x0

#define SECTOR_SIZE      				512
#define TOTAL_MONTHS     				12
#define MAX_CHAR_BYTES   				16
#define BASE10           				10
#define BASE16           				16
#define MAXLENGTH        				255
#define NODE_LENGTH       				32
#define SYSFS_PATH_LENGTH 				64
#define DIR_LENGTH        				64
#define eMMC_REFRESH_MAX_PARTITIONS  	10
#define eMMC_REFRESH_PARTITION_NAME  	20
#define CHECKSUM_COUNT           		8
#define eMMC_TOTAL_PARTITION			4	
#define eMMC_DEV_NODE_OFFSET			6
#define	eMMC_REFRESH_WAIT_TIME			120
#define METADATA_BACKUP

#define SECTORCOUNT_4GB        			8388608
#define SECTORCOUNT_8GB        			16777216
#define SECTORCOUNT_16GB       			33554432
#define SECTORCOUNT_32GB       			67108864
#define REFRESHCOUNT_INITMAGIC   		0xDEADDEAD
#define TRUE							1
#define FALSE							0
#define MILLISECONDS_PER_SECOND			1000
/************************************************************************
|typedefs and struct defs (scope: global)
|-----------------------------------------------------------------------*/
typedef int           tS32;
typedef unsigned int  tU32;
typedef char          tS8 ;
typedef unsigned char tU8 ;

typedef enum
{
    eMMC_REFRESH_NOERROR            =  0,
    eMMC_REFRESH_INVALID_ARGUMENT   = -1,
    eMMC_REFRESH_ACCESS_ERROR       = -2,
    eMMC_REFRESH_OPEN_ERROR         = -3,
    eMMC_REFRESH_NO_MEMORY          = -4,
    eMMC_REFRESH_READ_ERROR         = -5,
    eMMC_REFRESH_SEEK_ERROR         = -6,
    eMMC_REFRESH_CONVERSION_ERROR   = -7,
    eMMC_REFRESH_INVALID_FILESIZE   = -8,
    eMMC_REFRESH_EMPTY_METAFILE     = -9,
    eMMC_REFRESH_CORRUPTED_FILE     = -10,
    eMMC_REFRESH_OUT_OF_RANGE       = -11,
    eMMC_REFRESH_RENAME_ERROR       = -12,
    eMMC_REFRESH_TOTALSIZE_ERROR    = -13
    
}ErrorValues;

typedef enum
{
		eMMC_SIZE_DEFAULT	=  0, 
    	eMMC_SIZE_4GB 		=  4,
    	eMMC_SIZE_8GB  		=  8,
    	eMMC_SIZE_16GB  	=  16,
    	eMMC_SIZE_32GB  	=  32,
    	eMMC_SIZE_64GB  	=  64
}eMMC_DEV_SIZE;

typedef enum
{
		eMMC_VENDOR_DEFAULT		=  	0x00,
    	SAMSUNG_VENDOR_ID 		=	0x15,
    	MICRON_VENDOR_ID  		=	0xFE,
    	MICRON_VENDOR_ID_I		=	0x13    	
    
}eMMC_DEV_VENDOR;

typedef enum
{
		eMMC_DEFAULT		=  	0x00, 
    	eMMC_SAMSUNG, 		
    	eMMC_MICRON 
    
}eMMC_DEV_VENDOR_TYPE;



struct refresh_data {
        tU32 u32no_of_sectors_covered;
        tU32 u32partition_covered;
        tU32 u32day, u32month, u32year;
        tU32 u32due_day, u32due_month, u32due_year;
        tU32 u32checksum[8];
        tU32 u32total_numbers_of_sectors;
        tU32 u32refresh_type;
        tU32 u32refresh_size;
        tU32 u32Refresh_threshold;
        tU32 u32TotalRefreshCount;
        tU32 u32Refresh_normal_delay_ms;
        tU32 u32Refresh_fastmode_delay_ms;
        tU32 u32Refresh_multiFactor;
		tU32 u32EnableRPMBRefresh;	
};

struct date {
     tU32 u32day, u32month, u32year;
};
struct partition {
      tU32 u32total_no_partition;
      tU8 Partition_name[eMMC_REFRESH_MAX_PARTITIONS][eMMC_REFRESH_PARTITION_NAME];
};

/************************************************************************
| variable declaration (scope: global)
|-----------------------------------------------------------------------*/
struct refresh_data emmcRefresh_data;
struct date present_date, threshold_date;
struct partition emmc_partition = { eMMC_TOTAL_PARTITION,"mmcblkXboot0","mmcblkXboot1","mmcblkX","mmcblkXrpmb"};
char* cpVendorList [] = {
							"DEFAULT",
							"SAMSUNG",
							"MICRON"};

tU8 u8Error[MAXLENGTH];
tU32 u32Refresh_state = TERMINATED;
tU32 u32Refresh_ChunkSize;
tU32 u32Refresh_multiFactor = 1;
tU32 u32EnableDebug = 0;
FILE *plogFilePtr = NULL;
struct timespec delay;
/************************************************************************
|function prototypes (scope: global)
|-----------------------------------------------------------------------*/
extern tS32 signal_handler_setup(void);
#else
#error emmc_refresh.h included several times
#endif
