/*****************************************************************************************
* Copyright (C) RBEI, 2013
* This software is property of Robert Bosch.
* Unauthorized duplication and disclosure to third parties is prohibited.
******************************************************************************************/
/******************************************************************************
* FILE         : emmc_refresh-signalHandler.c
*             
* DESCRIPTION  : This contains the all signal handling for emmc refresh                            
*                
*             
* AUTHOR(s)    :  (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*-----------|---------------------|--------------------------------------
*01.AUG.2013| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*08.OCT.2015| Version 1.3         | SWATHI BOLAR (RBEI/ECF5)
*           |                     | eMMC refresh enabled for all projects.
* -----------------------------------------------------------------------
***************************************************************************/
/************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#include "emmc_refresh_signalHandler.h"
tS32 signal_handler_setup(void);
void stop_signal_handler(tS32 sig) ;
extern tS8 *real_buf;
extern tS32 refresh_fd;
extern tS32 u32Refresh_state;
extern FILE *plogFilePtr;
/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
/********************************************************************************
* FUNCTION        : stop_signal_handler
* PARAMETER       :                                                     
* RETURNVALUE     : 
*                   
* DESCRIPTION     :  This contains the signal handling              
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*1.AUG.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
void stop_signal_handler(tS32 sig) 
{
  debug_print("OUCH! - I got signal %d-%s\n", sig,strsignal(sig));
  
   if(u32Refresh_state != TERMINATED)
   {    
       if(real_buf != NULL)
       {
          free(real_buf);
          real_buf = NULL;
       }  
       if(refresh_fd != (tS32)NULL)
       {
          close(refresh_fd); 
          refresh_fd = (tS32)NULL;
       } 
       if(plogFilePtr != NULL)
       {  
          fflush(plogFilePtr);
          fclose(plogFilePtr); 
          plogFilePtr = NULL;
       } 
       u32Refresh_state = TERMINATED;
    }
    fflush(stdout);
    exit(0);
}
/********************************************************************************
* FUNCTION        : signal_handler_setup
* PARAMETER       :                                                     
* RETURNVALUE     : 
*                   
* DESCRIPTION     :  This contains the signal handler setup              
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*21.AUG.2013| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
tS32 signal_handler_setup	(	void	) 
{
	struct sigaction 	act;
	tS32 				result;

	act.sa_handler = stop_signal_handler;

	result = sigemptyset(&act.sa_mask);

	if (result != 0) 
	{
		perror("sigemptyset failed: ");
		return -1;
	}

	act.sa_flags = SA_RESTART | SA_ONSTACK;

	result |= sigaction( SIGINT,	&act, NULL);
	result |= sigaction( SIGTERM,	&act, NULL);
	result |= sigaction( SIGSEGV,	&act, NULL); 
	result |= sigaction( SIGABRT,	&act, NULL);

	if (result != 0) 
	{
		perror("sigaction failed: ");
		return -1;
	}

    return 0;
}
