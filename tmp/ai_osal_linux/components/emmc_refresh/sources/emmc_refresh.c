/*****************************************************************************************
* Copyright (C) RBEI, 2013
* This software is property of Robert Bosch.
* Unauthorized duplication and disclosure to third parties is prohibited.
******************************************************************************************/
/******************************************************************************
* FILE         : emmc_refresh.c
*             
* DESCRIPTION  : This contains the implementation for the eMMC refresh.
*                Due to the Errors caused by Read disturb reasons there arises a need to improve 
*                the life time  of the eMMC card. This can be improved by refreshing data in the 
*                eMMC card. This proposal was suggested by eMMC chip vendors.
*                Algorithm followed here is as follows:
*                - Configuration For the eMMC refresh is stored in a Config file 
*                - For mmcblk1boot0, mmcblkboot1 and mmcblk1 partition is read sequentially in chunks
*                  specified in the config File. The start address to read is obtained from the Metadata file stored.
*                - For every fixed number of Chunks read, a Metadata file is  Updated. This 
*                   contains information regarding the No of sectors covered, Refresh due date etc. 
*                - The above process repeats till all the bytes in the eMMC is covered.
*                            
* AUTHOR(s)    : SWATHI BOLAR (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initial version 1.0 | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*15.JAN.2015| Version 1.2         | SWATHI BOLAR (RBEI/ECF5)
*           |                     | Manufacturing ID check added.
* -----------------------------------------------------------------------
*08.JAN.2016| Version 1.3         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | eMMC refresh enabled for all projects.
* ----------------------------------------------------------------------
*25.JAN.2016| Version 1.4         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | Fix for the issue CFG3-1696.
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.5         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | CFG3-1958:Implements refresh for the 
*           |                     | Micron eMMC chips.
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.6         | BALAJI VISHWANADHULA (RBEI/ECF5)
*           |                     | modified for Extended CSD Revision check support. 
*------------------------------------------------------------------------
*01.SEP.2016| Version 1.7         | Selvakumar Kalimuthu (RBEI/ECF22)
*           |                     | modified for eMMC revision 5.0 and 5.1. 
* -----------------------------------------------------------------------
*02.SEP.2016| Version 1.8         | SWATHI BOLAR (RBEI/ECF5)
*           |                     | Code Cleanup.
*------------------------------------------------------------------------
*01.SEP.2016| Version 1.9         | Selvakumar Kalimuthu (RBEI/ECF22)
*           |                     | Config file optimization 
* -----------------------------------------------------------------------
***************************************************************************/

/************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/
#include "emmc_refresh.h"
static tS32 s32StartEmmcRefresh(tU8* pu8RefreshPartition);
static tS32 s32Read_RefreshMetaData(const tU8* pU8MetadataPath);
static tS32 s32Read_Sysfs_File(tU8* pu8nodePath, tS32 s32Base);
static tS32 s32Write_RefreshMetaData(const tU8* pU8MetadataPath);
static tS32 s32Read_ConfigFile(const tU8* pU8ConfigPath, tU8 u8ReadDefaultConfig,eMMC_DEV_SIZE enDevSize);
static inline tU8* ptr_align (tU8 const *ptr, size_t alignment) ;
static void vcalculate_checksum( tU32 *u32checksum);
static tS32 s32ValidateChecksum( const struct refresh_data *peMMCRefreshdata);
static tS32 s32CheckConfigFile(eMMC_DEV_SIZE enDevSize, eMMC_DEV_VENDOR enDevVendor);
static void s32Backup_metadataFile(void);
static tS32 s32Check_MetadataFile(void);
static void vLoadMetadataFile( struct refresh_data eMMCRefreshdata );
static void vLoadConfigFile( struct refresh_data eMMCRefreshdata );
static tS32 s32eMMCRefresh_setup(void);
static tS32 s32CheckMMCDevNode(void);
static void vStartLogging(void);
static void vSet_delay(tU32 u32delay_ms);
void writetoerrmem(const tU8* buffer);


tU8 *real_buf = NULL;
tS32 refresh_fd = (tS32)NULL;
/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/

/********************************************************************************
* FUNCTION		  : writetoerrmem
* PARAMETER 	  : 													
* RETURNVALUE	  : 
*					
* DESCRIPTION	  : This function writes the error to the error memory			 
*------------------------------------------------------------------------
* Date		|		Version 	  | Author & comments
*-----------|---------------------|--------------------------------------
*1.AUG.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1		  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
void writetoerrmem(const tU8* buffer)
{
	int fd;
	fd = open("/dev/errmem", O_WRONLY);
	if( fd != -1)
	{
		 if(write(fd, buffer,strlen(buffer)) <= 0)
		{
		   /* Trace Error*/
		   debug_print("eMMC refresh: Unable to write into error memory %d\n",errno);
		}	 
		(void)close(fd);
	}
	else
	{
	   /* Trace Error*/
	   debug_print("eMMC refresh: Unable to Open error memory %d\n",errno);
	}
}

/********************************************************************************
* FUNCTION        : s32Read_Sysfs_File 
* PARAMETER       : pu8nodePath: Sys Fs File Path
                    s32Base    :  s32Base  10/16                                           
* RETURNVALUE     : 0  on sucess
*                   negative values on Failure
* DESCRIPTION     : This function reads the file from the  
*                   sysfs interface            
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*15.JAN.2015| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
*           |                     | modified for Manufacturing ID check support.
* -----------------------------------------------------------------------
**********************************************************************************/
static tS32 s32Read_Sysfs_File(tU8* pu8nodePath,tS32 s32Base) 
{
    tS32 fd;
    tU8 u8SysfsBuffer[MAX_CHAR_BYTES];
    tS32 s32Result;
    memset(u8SysfsBuffer,'\0', MAX_CHAR_BYTES); 
    if ( NULL == pu8nodePath)
    {  
       s32Result = (tS32) eMMC_REFRESH_INVALID_ARGUMENT;
       snprintf(u8Error,MAXLENGTH,"%s: SYSFS path is NULL",__func__);
       writetoerrmem(u8Error); 
    }
    else
    {
        fd = open(pu8nodePath, O_RDONLY);
        if (fd < 0) 
        {  
           s32Result = (tS32) eMMC_REFRESH_OPEN_ERROR;
           snprintf(u8Error,MAXLENGTH,"%s: %s error,"
            "unable to open %d ",__func__,pu8nodePath,errno);
           writetoerrmem(u8Error); 
        }
        else
        {
            s32Result = read(fd, u8SysfsBuffer, MAX_CHAR_BYTES);
            if (s32Result <= 0) 
            {
                s32Result = (tS32) eMMC_REFRESH_READ_ERROR;
                snprintf(u8Error,MAXLENGTH,"%s: %s error,unable to read %d",
						__func__,
                       pu8nodePath,errno);
                writetoerrmem(u8Error); 
            }
            else
            {   
                /* convert string to s32Base 10 integer value*/
                s32Result = (tS32)strtoul(u8SysfsBuffer, NULL, s32Base);
                /* Check for various possible errors for strtoul */
                if ((errno == ERANGE && (s32Result == (tS32) LONG_MAX || s32Result == (tS32) LONG_MIN))
                       || (errno != 0 && s32Result == 0))
                { 
                    s32Result = (tS32) eMMC_REFRESH_CONVERSION_ERROR;
                    snprintf(u8Error,MAXLENGTH,"%s: %s error,"
                    "string conversion error %d",__func__,pu8nodePath,errno);
                    writetoerrmem(u8Error); 
                }
            }
        }
    }
    return s32Result;
}
/********************************************************************************
* FUNCTION        :  vSet_delay
* PARAMETER       :  tU32 u32delay_ms                                               
* RETURNVALUE     :  0
* DESCRIPTION     :  Set delay for every read. This Delay is needed to ensure that
* we do not utilize the entire eMMC bandwidth.           
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19.Aug.2016| Initialversion 1.0  | Balaji (RBEI/ECF5)
*-----------|---------------------|--------------------------------------
*02.Sep.2016| Version 1.1         | Swathi Bolar (RBEI/ECF5)
* -----------------------------------------------------------------------
*********************************************************************************/
static void vSet_delay(tU32 u32delay_ms)
{
    /*conversion from millisecond to second*/
    delay.tv_sec = (long)(u32delay_ms / MILLISECONDS_PER_SECOND); 
    /*conversion from remaining millisecond to nanosecond.*/
    delay.tv_nsec = (long)((u32delay_ms % MILLISECONDS_PER_SECOND )* MILLISECONDS_PER_SECOND * MILLISECONDS_PER_SECOND );   
}

/********************************************************************************
* FUNCTION        :  s32CheckMMCDevNode
* PARAMETER       :  None                                               
* RETURNVALUE     :  returns mmc device node number
* DESCRIPTION     :  This will read the and parse /proc/cmdline to identify the  
*                    mmc device node and node number.Also check the mount point 
*					 type is MMC or not
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*20.SEP.2016| Version 1.9         | Selvakumar Kalimuthu (RBEI/ECF22)
*           |                     | Idendify the eMMC device node. 
*------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32CheckMMCDevNode(void)
{
	tU8 	u8SysfsBuffer[ NODE_LENGTH ] ={ 0 };
	tS32 	s32Result = -1;
	tS32    fdDeviceType = -1;
	FILE 	*fpRootDevice;
	tU8		*pRootDev;

	/*Read the mounts and parse the root device mount point. */
	fpRootDevice = popen(ROOT_DEVICE_NODE , "r");
	if(NULL == fpRootDevice)
	{
		s32Result = (tS32) eMMC_REFRESH_OPEN_ERROR;
		snprintf(u8Error,MAXLENGTH,"%s: %s error," "unable to open %d ",__func__,ROOT_DEVICE_NODE,errno);
		writetoerrmem(u8Error); 
	}
	else
	{
		/*Read the Root device mount point*/
        if (fgets(u8SysfsBuffer, sizeof(u8SysfsBuffer), fpRootDevice)!=NULL)
		{
			pRootDev = strstr(u8SysfsBuffer,"mmcblk");
			
 	   		/*Check the mmc device exiting on the root device node */
 	   		if ( NULL ==  pRootDev )
 	   		{
 	   			s32Result = (tS32) eMMC_REFRESH_OPEN_ERROR;
				snprintf(u8Error,MAXLENGTH,"%s: %s error," "unable to open %d ", __func__, ROOT_DEVICE_NODE, errno);
				writetoerrmem(u8Error);	
 	   		}
			else
 	   		{
 	   			/*Read the mmc device mount point number*/
 	   			s32Result = (tS32)( pRootDev [ eMMC_DEV_NODE_OFFSET ] - (tS32)'0');

 	   			sprintf(u8SysfsBuffer,"/sys/block/mmcblk%d/device/type",s32Result);

				/*Check type of the device as MMC*/	
				fdDeviceType = 	open ( u8SysfsBuffer, O_RDONLY );
	
			    if (fdDeviceType < 0) 
			    {  
			 	  s32Result = (tS32) eMMC_REFRESH_OPEN_ERROR;
			 	  snprintf(u8Error,MAXLENGTH,"%s: %s error," "unable to open %d ",__func__,u8SysfsBuffer,errno);
			 	  writetoerrmem(u8Error); 
			    }
				else
				{	
					memset(u8SysfsBuffer,0,NODE_LENGTH);
					
					if (read(fdDeviceType, u8SysfsBuffer, NODE_LENGTH) <= 0) 
					{
						s32Result = (tS32) eMMC_REFRESH_READ_ERROR;
						snprintf(u8Error,MAXLENGTH,"%s: Dev Type error,unable to read %d",__func__,errno);
						writetoerrmem(u8Error); 
					}
					else
					{
						printf("Dev Node Num : %d Type : %s\n",s32Result,u8SysfsBuffer);
						
						if( strncmp(u8SysfsBuffer,"MMC",3) )
						{
							s32Result	= (tS32)eMMC_REFRESH_ACCESS_ERROR;						
						}
					}
 	   			}
 	   		}
			
			close (fdDeviceType);

		}
		else
		{
			s32Result = (tS32) eMMC_REFRESH_READ_ERROR;
			snprintf(u8Error,MAXLENGTH,"%s: %s error,unable to read %d",__func__,ROOT_DEVICE_NODE,errno);
			writetoerrmem(u8Error); 
		}
		pclose(fpRootDevice);
	}

	return s32Result;
}

/********************************************************************************
* FUNCTION        :  s32eMMCRefresh_setup
* PARAMETER       :  None                                               
* RETURNVALUE     :  returns 0 on success and Error Values on Failure
* DESCRIPTION     :  This performs the initial setup needed for emmc refresh.
*                    Checks the config and Metadata file for data integrity.
*                    If everything is OK, It decides whether Refresh is needed
*                    or Not.
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*15.JAN.2015| Version 1.2         | SWATHI BOLAR (RBEI/ECF5)
*           |                     | modified for Manufacturing ID check support.
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.3         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | CFG3-1958:Implements refresh for the 
*           |                     | Micron eMMC chips.
*------------------------------------------------------------------------
*30.JUN.2016| Version 1.4         | BALAJI VISHWANADHULA (RBEI/ECF5)
*           |                     | modified for Extended CSD Revision check support. 
*------------------------------------------------------------------------
*01.SEP.2016| Version 1.5         | Selvakumar Kalimuthu (RBEI/ECF22)
*           |                     | modified for eMMC revision 5.0 and 5.1. 
*------------------------------------------------------------------------
*20.SEP.2016| Version 1.9         | Selvakumar Kalimuthu (RBEI/ECF22)
*           |                     | eMMC refresh for all the device without any filtering. 
*------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32eMMCRefresh_setup()
{
    tS32 			s32ReturnValue 	= 	-1;
	eMMC_DEV_SIZE 	enDeviceSize	=	eMMC_SIZE_DEFAULT;
    tS32 			s32VendorID,
					s32Ext_csd_rev,
					s32Total_sector = 	0,
					s32NodeNum,
					s32Count;
    tU8 			sysfsnode [ SYSFS_PATH_LENGTH ];

	s32NodeNum = s32CheckMMCDevNode();

	if ( 0 > s32NodeNum )
	{
	    snprintf(u8Error,MAXLENGTH,"%s: unable to read emmc Devive Node %d",__func__,errno);
        writetoerrmem(u8Error); 
        s32ReturnValue = (tS32) eMMC_REFRESH_READ_ERROR;
	}
	else
	{
		/*Load the Dev node number and get the partition name*/
		for ( s32Count = 0; s32Count < eMMC_TOTAL_PARTITION; s32Count++ )
		{
			emmc_partition.Partition_name [ s32Count ]  [ eMMC_DEV_NODE_OFFSET ] = (tU8)(s32NodeNum + '0');
		}

	    /* Check the EXT_CSD_REV version 
			8  is Revision 1.8 (for MMC v5.1)    
	        7  is Revision 1.7 (for MMC v5.0)
	        6  is Revision 1.6 (for MMC v4.5, v4.51)
	        5  is Revision 1.5 (for MMC v4.41)
	        4  is Revision 1.4 (Obsolete)
	        3  is Revision 1.3 (for MMC v4.3)
	        2  is Revision 1.2 (for MMC v4.2)
	        1  is Revision 1.1 (for MMC v4.1)
	        0  is Revision 1.0 (for MMC v4.0)*/
	    snprintf(sysfsnode,SYSFS_PATH_LENGTH,"/sys/block/mmcblk%d/device/ext_csd_rev",s32NodeNum);
	    s32Ext_csd_rev = s32Read_Sysfs_File(sysfsnode, BASE10);

		/* Check the Vendor*/						
	    snprintf(sysfsnode,SYSFS_PATH_LENGTH,"/sys/block/mmcblk%d/device/manfid",s32NodeNum);
	    s32VendorID = s32Read_Sysfs_File(sysfsnode, BASE16);

		
		/* Read total size of the partition from sysfs interface */
		snprintf(sysfsnode,SYSFS_PATH_LENGTH,"/sys/block/mmcblk%d/size",s32NodeNum);
		s32Total_sector = s32Read_Sysfs_File(sysfsnode, BASE10);  
		
	    if ( (0 > s32Ext_csd_rev ) || \
			 (0 > s32VendorID ) || \
			 (0 > s32Total_sector ) )
	    {
	        snprintf(u8Error,MAXLENGTH,"%s unable to read csd_rev(%d) VendorID(%d) sector(%d) from sysfs %d"			,__func__
																														,s32Ext_csd_rev
																														,s32VendorID
																														,s32Total_sector
																														,errno);
	        writetoerrmem(u8Error); 
	        s32ReturnValue = (tS32) eMMC_REFRESH_READ_ERROR;
	    }
		else 
		{
			printf("venid:%X Rev:%X Size:%X\n",(tU32)s32VendorID,(tU32)s32Ext_csd_rev,s32Total_sector);
			debug_print("vendor id = %u Revision = %u Size [%X] \n",(tU32)s32VendorID,(tU32)s32Ext_csd_rev,s32Total_sector);		

			/*Check the eMMC device size 4, 8, 16 and 32 GB. >32GB us go with default configuration*/
			if(((tU32)s32Total_sector < SECTORCOUNT_4GB))
			{
				enDeviceSize = eMMC_SIZE_4GB;
			}
			else if(((tU32)s32Total_sector > SECTORCOUNT_4GB) && ((tU32)s32Total_sector <= SECTORCOUNT_8GB))
			{
				enDeviceSize = eMMC_SIZE_8GB;
			}
			else if(((tU32)s32Total_sector > SECTORCOUNT_8GB) && ((tU32)s32Total_sector <= SECTORCOUNT_16GB))
			{
				enDeviceSize = eMMC_SIZE_16GB;
			}
			else if(((tU32)s32Total_sector > SECTORCOUNT_16GB) && ((tU32)s32Total_sector <=SECTORCOUNT_32GB))
			{
				enDeviceSize = eMMC_SIZE_32GB;
			}
			else
			{
				/*eMMC Refresh has been discussed for only 8GB and 32GB Micron. 
				To ensure little improvement in the data retention we perform refresh
				with default config*/
				debug_print("%s: s32Total_sector = %x\n \n",__func__,(tU32)s32Total_sector);
				enDeviceSize = eMMC_SIZE_DEFAULT;
			}		 

			s32ReturnValue = s32CheckConfigFile(enDeviceSize,(eMMC_DEV_VENDOR)s32VendorID);		
		}	


		
	    if(s32ReturnValue == 0)
	    {   
	        vStartLogging();
	        /* Future provision when Software refresh is no longer needed
	        If in Config file "Refresh_type: 1" then S/W refresh is intended*/
	        if(emmcRefresh_data.u32refresh_type != 1)
	        {
	            debug_print("%s: Refresh type in Config File"\
	            " is %u\n No refresh Needed!\n", __func__, emmcRefresh_data.u32refresh_type);
	            printf("%s : %u in config file..eMMC refresh not intended.\n", __func__,emmcRefresh_data.u32refresh_type);
	            s32ReturnValue = NOREFRESH_NEEDED; 
	        }
	        else if(access(METADATA_DIR_PATH, F_OK | R_OK)== -1)
	        {
	          /* InitialCase Check if the folder for eMMC refresh exists in Persistent partition*/
	            s32ReturnValue = mkdir(METADATA_DIR_PATH, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	            if(s32ReturnValue < 0)
	            {
	                snprintf(u8Error,MAXLENGTH,"%s: unable to create dir %d", __func__, errno);
	                writetoerrmem(u8Error); 
	                s32ReturnValue = (tS32) eMMC_REFRESH_ACCESS_ERROR;
	            }
	            else
	            {
	                s32ReturnValue = 0;
	            }
	        }
	        if(s32ReturnValue == 0)
	        {
	            if(emmc_partition.u32total_no_partition > 0)
	            {   
	                /*Check if the Metadata File is OK */
	                s32ReturnValue = s32Check_MetadataFile();           
	            }
	            else
	            {
	                snprintf(u8Error,MAXLENGTH,"%s: Total no of partitions to refresh <= 0", __func__);
	                writetoerrmem(u8Error);
	                s32ReturnValue = (tS32)eMMC_REFRESH_CORRUPTED_FILE;
	            }            
	        }
	    }

	    if(s32ReturnValue == 0)
	    {    
	        /* Obtain Present date Info*/    
	         time_t now = time(NULL);
	         struct tm *t = localtime(&now);
	         present_date.u32day = (tU32) t->tm_mday;
	         present_date.u32month = (tU32) t->tm_mon + 1;
	         present_date.u32year = (tU32) t->tm_year + 1900;
	         threshold_date.u32day = emmcRefresh_data.u32due_day;
	         threshold_date.u32month = (emmcRefresh_data.u32due_month + 3);
	         threshold_date.u32year = emmcRefresh_data.u32due_year;
	         if (threshold_date.u32month > TOTAL_MONTHS) 
	        {
	            threshold_date.u32month = threshold_date.u32month % TOTAL_MONTHS;
	            threshold_date.u32year = threshold_date.u32year + 1;
	        }
	         /* Compare the present date and the Refresh due Date and decide if refresh is needed 
	         If Present Date = Due date + 3 months Then High speed refresh
	         Else If Present Data > Due date       Then Normal refresh
	         Else if Present Date < Due Date       Then No refresh */
	         
	         if((present_date.u32year > threshold_date.u32year) || 
	        ((present_date.u32year == threshold_date.u32year)&&(present_date.u32month > threshold_date.u32month)) ||
	        ((present_date.u32year == threshold_date.u32year)&&(present_date.u32month == threshold_date.u32month)&&
	        (present_date.u32day > emmcRefresh_data.u32due_day)))
	        {
	           u32Refresh_multiFactor =  emmcRefresh_data.u32Refresh_multiFactor;
	           debug_print("\n%s: High speed refresh advised %u \n", __func__, u32Refresh_multiFactor);
	           vSet_delay(emmcRefresh_data.u32Refresh_fastmode_delay_ms); 
	           s32ReturnValue = REFRESH_NEEDED;
	        }
	        else if( (present_date.u32year > emmcRefresh_data.u32due_year)||
	                ((present_date.u32year == emmcRefresh_data.u32due_year)&&
	                    (present_date.u32month > emmcRefresh_data.u32due_month)) || 
	                 ( (present_date.u32year == emmcRefresh_data.u32due_year)&&
	                      (present_date.u32month == emmcRefresh_data.u32due_month)&&
	                          (present_date.u32day > emmcRefresh_data.u32due_day) ) )
	        {
	            u32Refresh_multiFactor = 1;
	            debug_print("%s: refresh advised \n", __func__ );
	            vSet_delay(emmcRefresh_data.u32Refresh_normal_delay_ms); 
	            s32ReturnValue=(tS32)REFRESH_NEEDED;
	        }
	        else
	        {
	            debug_print("%s: No eMMC refresh needed\n", __func__);
	            printf("Present Date:%u-%u-%u\n",present_date.u32day,present_date.u32month,present_date.u32year);
	            printf("Due date:%u-%u-%u\n",emmcRefresh_data.u32due_day,emmcRefresh_data.u32due_month,emmcRefresh_data.u32due_year);
	            printf("No refresh needed");
	            s32ReturnValue = NOREFRESH_NEEDED;
	        }
	        /* Special case when there is an issue with the system time */
	        if((s32ReturnValue == NOREFRESH_NEEDED) &&((emmcRefresh_data.u32no_of_sectors_covered != 0)
	                                  ||(emmcRefresh_data.u32partition_covered != 0)))
	        {
	            vSet_delay(emmcRefresh_data.u32Refresh_normal_delay_ms); 
	            u32Refresh_multiFactor = 1;
	            debug_print("System Time Error: sector covered =%u partition=%u\n",
	            emmcRefresh_data.u32no_of_sectors_covered,emmcRefresh_data.u32partition_covered);
	            s32ReturnValue=(tS32)REFRESH_NEEDED;
	        }
	    }
	}
    return  s32ReturnValue;       
}
/********************************************************************************
* FUNCTION        :  s32CheckConfigFile
* PARAMETER       :  const tU8* pU8ConfigPath                                              
* RETURNVALUE     :  returns 0 on success and Error Values on Failure
* DESCRIPTION     :  Checks and reads the config file 
*                                
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.5         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | CFG3-1958:Implements refresh for the 
*           |                     | Micron eMMC chips.
*------------------------------------------------------------------------
*20.SEP.2016| Version 1.9         | Selvakumar Kalimuthu (RBEI/ECF22)
*           |                     | Config file optimization 
*------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32CheckConfigFile(eMMC_DEV_SIZE enDevSize, eMMC_DEV_VENDOR enDevVendor)
{
    tS32 					s32ReturnValue;
	tU8 					u8ConfigFileName [ MAXLENGTH ] ={ 0 },
							u8ReadDefaultConfig = FALSE;
	eMMC_DEV_VENDOR_TYPE	enVendorType = eMMC_DEFAULT;

	/*If the device is > 32GB we take the default configuration for refresh. */
	if( eMMC_SIZE_DEFAULT != enDevSize )
	{
		switch ( enDevVendor )
		{
			case eMMC_VENDOR_DEFAULT:
			{
				enVendorType = eMMC_DEFAULT;
			}
				break;
			case SAMSUNG_VENDOR_ID:
			{
				enVendorType = eMMC_SAMSUNG;			
			}
				break;
			case MICRON_VENDOR_ID:
			case MICRON_VENDOR_ID_I:
			{
				enVendorType = eMMC_MICRON;	
			}
				break;
			default:
			{
				debug_print("%s: s32VendorID = %X\n Unknown Vendor\n", __func__, (tU32)enDevVendor);	
				enVendorType = eMMC_DEFAULT;
			}	
		}
	}	
	
	if( eMMC_DEFAULT == enVendorType )
	{
		u8ReadDefaultConfig = TRUE;	
	}
	
	sprintf(u8ConfigFileName,CONFIG_PATH"config_%s.txt",cpVendorList[enVendorType]);
	s32ReturnValue = s32Read_ConfigFile(u8ConfigFileName,u8ReadDefaultConfig,enDevSize);

	if( ( (tS32)eMMC_REFRESH_OUT_OF_RANGE == s32ReturnValue ) && ( eMMC_DEFAULT != enVendorType))
	{
		/*Config file not having the details to proceed the refresh, Go with the default configuration  */
		debug_print("%s: Config Details not Found on %s \n Go with Default config\n", __func__, u8ConfigFileName );
		sprintf(u8ConfigFileName,CONFIG_PATH"config_%s.txt",cpVendorList[eMMC_DEFAULT]);
		enDevSize = eMMC_SIZE_DEFAULT;
		s32ReturnValue = s32Read_ConfigFile(u8ConfigFileName,TRUE,enDevSize);			
	}

    return  s32ReturnValue;       
}
/********************************************************************************
* FUNCTION        :  s32Check_MetadataFile
* PARAMETER       :  None                                               
* RETURNVALUE     :  returns 0 on success and Error Values on Failure
* DESCRIPTION     :  This function checks the Validity of the Metadata file 
*                    and tries to recover if any errors are seen.               
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.5         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | CFG3-1958:Implements refresh for the 
*           |                     | Micron eMMC chips.
*------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32Check_MetadataFile()
{
    FILE *fp;
    tS32 s32ReturnValue = 0,s32CheckMetaDataBk = 0;
    fp = fopen(METADATA_FILE_PATH, "r"); 
	
    if(NULL != fp ) 
    {
        s32ReturnValue = s32Read_RefreshMetaData(METADATA_FILE_PATH);
        if( ( s32ReturnValue != (tS32)eMMC_REFRESH_NOERROR ) || (emmcRefresh_data.u32due_year == 0) || (emmcRefresh_data.u32due_month == 0) )
        {
           /*file corruption case */
			s32ReturnValue = (tS32) eMMC_REFRESH_CORRUPTED_FILE;
			debug_print("%s: Invalid due date entries \n", __func__);
        }

        fclose(fp);
    }
	else
	{
		debug_print("%s:No meta file Error-%d\n", __func__, errno);		
		s32ReturnValue = (tS32) eMMC_REFRESH_OPEN_ERROR;
	}

	if ( s32ReturnValue < 0 ) 
    {   

        /* check if the backup file already exists,
           if yes restore metadata.txt from the backup file */
        s32CheckMetaDataBk =  access(METADATA_BACKUP_PATH, F_OK);
		
        if( s32CheckMetaDataBk != -1)//file Backup exists
        {
            s32ReturnValue = s32Read_RefreshMetaData(METADATA_BACKUP_PATH);
        }
		
        if ( ( s32CheckMetaDataBk == -1 ) || \
			 ( s32ReturnValue != (tS32)eMMC_REFRESH_NOERROR ) )
        {
            /*Create meta data file - Assuming initial case*/
            emmcRefresh_data.u32partition_covered = emmc_partition.u32total_no_partition; 
            emmcRefresh_data.u32no_of_sectors_covered = 0;
            /*0xDEADDEAD is used as magic so that never this value should be reached*/
            emmcRefresh_data.u32TotalRefreshCount = REFRESHCOUNT_INITMAGIC;
        }           
		
        s32ReturnValue = s32Write_RefreshMetaData(METADATA_FILE_PATH);
		
    }

	
    if( s32ReturnValue == 0)
    { 
       debug_print("%s: meta read\n", __func__ );
       s32ReturnValue = s32Read_RefreshMetaData(METADATA_FILE_PATH);
    }
    return s32ReturnValue;
}
/********************************************************************************
* FUNCTION        :  s32Backup_metadataFile
* PARAMETER       :  None                                                
* RETURNVALUE     :  None
* DESCRIPTION     :  Create a Backup of the metadata file 
*                                
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
#ifdef METADATA_BACKUP
static void s32Backup_metadataFile() 
{
    (void)s32Write_RefreshMetaData(METADATA_BACKUP_PATH);
}
#endif
/********************************************************************************
* FUNCTION        :  s32calculate_checksum
* PARAMETER       :  u32checksum - Checksum Array                                              
* RETURNVALUE     :  none
* DESCRIPTION     :  Checksum for the data is calculated.By simple method  
*                    We are storing the Hex value to just ensure data integrity        
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static void vcalculate_checksum( tU32 * u32checksum)
{   
    u32checksum[0] = emmcRefresh_data.u32due_year;
    u32checksum[1] = emmcRefresh_data.u32due_month;
    u32checksum[2] = emmcRefresh_data.u32due_day;
    u32checksum[3] = emmcRefresh_data.u32year;
    u32checksum[4] = emmcRefresh_data.u32month;
    u32checksum[5] = emmcRefresh_data.u32day;
    u32checksum[6] = emmcRefresh_data.u32partition_covered;
    u32checksum[7] = emmcRefresh_data.u32no_of_sectors_covered;
    return;
}

/********************************************************************************
* FUNCTION        :  s32calculate_checksum
* PARAMETER       :  peMMCRefreshdata - Refresh metadata content
* RETURNVALUE     :  CRC Success/Failure
* DESCRIPTION     :  Checksum for the data is Validated.By simple method  
*                    We are storing the Hex value to just ensure data integrity        
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*19.SEP.2016| Initialversion 1.0  | SELVAKUMAR KALIMUTHU (RBEI/ECF2)
* -----------------------------------------------------------------------
**********************************************************************************/
static tS32 s32ValidateChecksum( const struct refresh_data *peMMCRefreshdata)
{
    tS32 s32ReturnValue = (tS32)eMMC_REFRESH_NOERROR;

    if( ( peMMCRefreshdata->u32checksum[0] != peMMCRefreshdata->u32due_year) || 
	    ( peMMCRefreshdata->u32checksum[1] != peMMCRefreshdata->u32due_month ) ||
	    ( peMMCRefreshdata->u32checksum[2] != peMMCRefreshdata->u32due_day ) ||
    	( peMMCRefreshdata->u32checksum[3] != peMMCRefreshdata->u32year ) ||
    	( peMMCRefreshdata->u32checksum[4] != peMMCRefreshdata->u32month ) ||
    	( peMMCRefreshdata->u32checksum[5] != peMMCRefreshdata->u32day ) ||
    	( peMMCRefreshdata->u32checksum[6] != peMMCRefreshdata->u32partition_covered ) ||
    	( peMMCRefreshdata->u32checksum[7] != peMMCRefreshdata->u32no_of_sectors_covered ) )
    	
	{
		s32ReturnValue = (tS32)eMMC_REFRESH_CORRUPTED_FILE;
	}
	
    return s32ReturnValue;
}


/********************************************************************************
* FUNCTION        :  vStartLogging
* PARAMETER       :  None                                               
* RETURNVALUE     :  None
* DESCRIPTION     :  Only for dynamic debugging
*                    If "EnableDebug" in Config file is made 1 then all operations are logged            
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static void vStartLogging()
{   

   if(u32EnableDebug)
   { 
       plogFilePtr = fopen(LOG_FILE_PATH,"a+");
   }
   else
   {
		if(access(LOG_FILE_PATH, F_OK)== -1)
		{
			(void)remove(LOG_FILE_PATH);
		}			
   }

}
/********************************************************************************
* FUNCTION        : ptr_align 
* PARAMETER       : ptr            -     buffer
*                   alignment      -     page size                                             
* RETURNVALUE     : pointer after alignment
* DESCRIPTION     : This function is needed for the page alignment.                
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
static inline tU8 *ptr_align (tU8 const *ptr, size_t alignment) 
{
   tU8 *p0 = (tU8 *)ptr;
   tU32 p1 = (tU32)p0;
   p1 = p1 % alignment;
    if(p1 != 0 )
    {
       p0 = (p0 + (alignment - p1));
    }
    return p0;
}
/********************************************************************************
* FUNCTION        :  s32StartEmmcRefresh
* PARAMETER       :  pu8RefreshPartition - Partition Name                                               
* RETURNVALUE     :  returns 0 on success and Error Values on Failure
* DESCRIPTION     :  This performs the EMMC refresh 
*                                
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.5         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | CFG3-1958:Implements refresh for the 
*           |                     | Micron eMMC chips.
*------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32StartEmmcRefresh(tU8* pu8RefreshPartition)
{
    tS32 s32Total_sector;
    tU32 u32sectorIndex;
    tS32 s32ReturnValue = 0, s32ReadCounter = 0;
    tS32 s32Threshold = 0;
    tU8 *ibuf;
    tU32 u32page_size;
    off64_t offset;
    tU8 emmcnode[NODE_LENGTH],sysfsnode[SYSFS_PATH_LENGTH];


    snprintf(emmcnode,NODE_LENGTH,"/dev/%s",pu8RefreshPartition);
    snprintf(sysfsnode,SYSFS_PATH_LENGTH,"/sys/block/%s/size",pu8RefreshPartition);
    
    /* Read total size of the partition from sysfs interface */
    s32Total_sector = s32Read_Sysfs_File(sysfsnode, BASE10);
    debug_print("%s: Total_sector:%d \n ",pu8RefreshPartition,s32Total_sector);
    if(s32Total_sector <= 0)
    {
        s32ReturnValue = (tS32) eMMC_REFRESH_TOTALSIZE_ERROR;
        debug_print("%s: %s Read Total Size failed \n", __func__, emmcnode);
        snprintf(u8Error,MAXLENGTH,"%s: %s Get Total size Failed,"
         "size=%d  errno %d", __func__,emmcnode, s32Total_sector,errno);
        writetoerrmem(u8Error);  
    }
    else
    {
        refresh_fd = open(emmcnode, O_RDWR | O_LARGEFILE | O_SYNC | O_DIRECT) ;
        if (refresh_fd < 0)
        {
           debug_print("%s: Open failed \n", __func__);
           snprintf(u8Error,MAXLENGTH,"%s: Open %s Failed  errno %d", __func__,
                     emmcnode, errno);
           writetoerrmem(u8Error);            
           s32ReturnValue = (tS32) eMMC_REFRESH_OPEN_ERROR;
        }
        else
        {  
           emmcRefresh_data.u32total_numbers_of_sectors = (tU32)s32Total_sector;
           /*Create Backup On First time refresh
           At all Time Backup Metadata is expected to be present*/
           if(access(METADATA_BACKUP_PATH, F_OK)== -1)
           {
               s32Backup_metadataFile();
           }    
           if (emmcRefresh_data.u32no_of_sectors_covered != 0) 
           {
               /*Seek the pointer to the Sector to be refreshed next */
                offset = lseek64(refresh_fd,(off64_t)((unsigned int)emmcRefresh_data.u32no_of_sectors_covered * 
                          (off64_t)SECTOR_SIZE), SEEK_CUR);
                if (offset == -1) 
                {
                   s32ReturnValue = (int) eMMC_REFRESH_SEEK_ERROR;
                   debug_print("%s: lseek failed \n", __func__);
                   snprintf(u8Error,MAXLENGTH,"%s: Error %d on seeking the pointer", __func__,errno);
                   writetoerrmem(u8Error);     
                } 
            }
            if(s32ReturnValue == (tS32)eMMC_REFRESH_NOERROR)
            {
                u32page_size = (tU32)getpagesize();  
                /* Allocate with extra space needed for page alignment*/
                real_buf = (tS8 *) malloc( (SECTOR_SIZE * u32Refresh_ChunkSize) * sizeof(tS8) +
                            INPUT_BLOCK_SLOP(u32page_size));
                if ( NULL == real_buf) 
                { 
                    s32ReturnValue = (tS32) eMMC_REFRESH_NO_MEMORY;
                    snprintf(u8Error,MAXLENGTH,"%s: Malloc Failed for %d Bytes-error %d", __func__,
                    (SECTOR_SIZE * u32Refresh_ChunkSize) * sizeof(tS8) + INPUT_BLOCK_SLOP(u32page_size),errno);
                    writetoerrmem(u8Error);
                }
                else
                {    
                    /* Page Aligning the Buffer */                        
                    ibuf = real_buf; 
                    ibuf = ptr_align(ibuf, u32page_size);                
                    u32Refresh_state = REFRESH_INPROGRESS;
                    for(u32sectorIndex = emmcRefresh_data.u32no_of_sectors_covered;
                        ((u32sectorIndex < (tU32)s32Total_sector) && (s32ReturnValue >= 0));
                        (u32sectorIndex += u32Refresh_ChunkSize), s32ReadCounter++ ) 
                    {                    

                        s32ReturnValue = read(refresh_fd, ibuf, (SECTOR_SIZE * u32Refresh_ChunkSize));
                        debug_print("read Blk %u - Read %d\n",u32sectorIndex,s32ReturnValue);
                        /*Read Error*/                        
                        if(s32ReturnValue < 0)
                        {   
                            s32ReturnValue = (tS32) eMMC_REFRESH_READ_ERROR;
                            debug_print("%s: Read Error- errno %d", __func__,errno);
                            snprintf(u8Error,MAXLENGTH,"%s: Read Error- errno %d", __func__,errno);
                            writetoerrmem(u8Error); 
                            fflush(stdout);
                            break;
                        }
                        if(s32ReturnValue != (tS32)(SECTOR_SIZE * u32Refresh_ChunkSize)) 
                        {  /*If the return Value is less than SECTOR_SIZE * u32Refresh_ChunkSize
                              check if all the sectors are completed, If no then it is an error */
                            s32ReturnValue = (tS32)u32sectorIndex + (s32ReturnValue/SECTOR_SIZE);
                            if(s32ReturnValue < s32Total_sector)
                            {
                                s32ReturnValue = (tS32) eMMC_REFRESH_READ_ERROR;
                                debug_print("%s: Read Error- errno %d", __func__,errno);
                                snprintf(u8Error,MAXLENGTH,"%s:Partition%d Read"
                                "Error-%d read -errno %d", __func__,emmcRefresh_data.u32partition_covered,
                                s32ReturnValue,errno);
                                writetoerrmem(u8Error); 
                                break;
                            }
                            else if (s32ReturnValue >= s32Total_sector) 
                            {
                                emmcRefresh_data.u32no_of_sectors_covered = 0;
                                emmcRefresh_data.u32partition_covered = emmcRefresh_data.u32partition_covered + 1;
                            }
                        }
                        else
                        {
                            emmcRefresh_data.u32no_of_sectors_covered += (tU32)u32Refresh_ChunkSize;
                            if (emmcRefresh_data.u32no_of_sectors_covered >= (tU32)s32Total_sector) 
                            {
                                emmcRefresh_data.u32no_of_sectors_covered = 0;
                                emmcRefresh_data.u32partition_covered = emmcRefresh_data.u32partition_covered + 1;
                            }
                        }
                        /*In case of Normal Refresh Mode, u32Refresh_multiFactor=1.
                        In fast refresh mode, we need to do more read before performing a write.
                        This is ensured that the total number of write is still in agreement with the 
                        Vendor requirements*/
                        s32Threshold = (((tS32)emmcRefresh_data.u32Refresh_threshold) * ((tS32)u32Refresh_multiFactor));


                        if((s32ReadCounter == s32Threshold) ||
                             (emmcRefresh_data.u32no_of_sectors_covered == 0))
                        {
                            s32ReturnValue = s32Write_RefreshMetaData(METADATA_FILE_PATH);
                            s32ReadCounter = 0;
                            fflush(stdout);
                        }
                        if(s32ReturnValue < 0) 
                        {
                            break;
                        }
                        #ifdef METADATA_BACKUP
                        else if(emmcRefresh_data.u32no_of_sectors_covered % 
                                        (tU32)(BACKUP_THRESHHOLD(s32Total_sector)) == 0) 
                        {
                            s32Backup_metadataFile();
                        }
                        #endif                        
                        /* Delay between the requests to ensure that most of the eMMC bandwidth 
                         is not eaten and we do not disturb the �fluent� behaviour of the device.*/
                         nanosleep(&delay,NULL);                             
                    }
                    u32Refresh_state = TERMINATED;
                    free(real_buf); 
                    real_buf = NULL;
                }
            }
         }
         close(refresh_fd); 
         refresh_fd = (tS32)NULL;
   }
   debug_print("%s: returns-%d \n ", __func__, s32ReturnValue);

   return s32ReturnValue;
}
/********************************************************************************
* FUNCTION        :  s32Read_RefreshMetaData
* PARAMETER       :  pU8MetadataPath - Metadata Path                                               
* RETURNVALUE     :  0 on success and the error number on Failure
* DESCRIPTION     :   Read Refresh meta data file 
*                                
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.5         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | CFG3-1958:Implements refresh for the 
*           |                     | Micron eMMC chips.
* -----------------------------------------------------------------------
*20.SEP.2016| Version 1.9         | SELVAKUMAR KALIMUTHU (RBEI/ECF2)
*           |                     | Validate Checksum of medatadaa's
*------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32Read_RefreshMetaData(const tU8* pU8MetadataPath)
{
    FILE *fp;
	long l_size;
    tS32 s32ReturnValue = (tS32)eMMC_REFRESH_NOERROR;
    tS32 s32Readcount = 0;
    tS8  s8Token = 0;

	struct refresh_data emmcRefreshdata;
	
    fp = fopen(pU8MetadataPath, "r");
    if ( NULL == fp) 
    {
        snprintf(u8Error,MAXLENGTH,"%s: error %d, Open %s Failed ", __func__,
                errno,pU8MetadataPath);
        writetoerrmem(u8Error); 
        s32ReturnValue = (tS32) eMMC_REFRESH_OPEN_ERROR;
    }
    else
    {
    	if(fseek(fp, 0, SEEK_END)== -1)
        {
            /*Seek failure*/
            s32ReturnValue = (tS32) eMMC_REFRESH_SEEK_ERROR;
            debug_print("%s: unknown error-%d\n", __func__,errno);
        }
        l_size = ftell(fp); 
        if (l_size == -1)
        {
            /*file corruption case 1*/
            s32ReturnValue = (tS32) eMMC_REFRESH_INVALID_FILESIZE;
            debug_print("%s: ftell returns -1 Error %d\n", __func__,errno);
        }
         /* Minimum size of the metadata file is 41. Less than 41 is an error
         i.e. 0,0,1,1,1900,1,7,1900-76C-7-1-76C-1-1-0-0*/
        else if(( l_size == 0) || ( l_size <  41))
        {
           /*file corruption case 3*/
           s32ReturnValue = (tS32) eMMC_REFRESH_EMPTY_METAFILE;
           debug_print("%s: metadata file size is %ld \n", __func__,l_size);
        }
		
		if(s32ReturnValue >= 0)
	    {  
	    	/*Move to start offset of the file if no corruption on the file*/
	    	fseek(fp, 0, SEEK_SET);
				
	        fscanf(fp,"%u,%u,%u,%u,%u,%u,%u,%u\n", &emmcRefreshdata.u32no_of_sectors_covered,
	            &emmcRefreshdata.u32partition_covered,&emmcRefreshdata.u32day, &emmcRefreshdata.u32month, 
	            &emmcRefreshdata.u32year, &emmcRefreshdata.u32due_day, &emmcRefreshdata.u32due_month,
	            &emmcRefreshdata.u32due_year);
	            
	       /* Reading Checksum value */
	       for(s32Readcount=0; s32Readcount < CHECKSUM_COUNT; s32Readcount++)
	       {
	           fscanf(fp,"-%x",&emmcRefreshdata.u32checksum[s32Readcount]);
	       }

		   /* For the backward compatibility we have added this condition. We have added the total refresh count to the end of metadata.txt 
			* in our latest emmc refresh release.  
			*/
	       fscanf(fp,"%c",&s8Token);
	       if(s8Token != '-')
	       {
	           emmcRefreshdata.u32TotalRefreshCount = REFRESHCOUNT_INITMAGIC;
	       }
	       else
	       {
	           fscanf(fp,"%x",&emmcRefreshdata.u32TotalRefreshCount);
	       }
		   
		   fclose(fp);
		   	
		   if( eMMC_REFRESH_NOERROR != (ErrorValues)s32ValidateChecksum ( &emmcRefreshdata)  )
		   {   /*File corruption case 3*/
			   debug_print("%s: CHECKSUM ERROR Metadata Corrupted : %s\n", __func__,pU8MetadataPath);
               snprintf(u8Error,MAXLENGTH,"%s: Metadata Corrupted-"
                      "Error : %d", __func__,s32ReturnValue);
             	writetoerrmem(u8Error);			   
			   remove(pU8MetadataPath);
			   s32ReturnValue = (tS32) eMMC_REFRESH_CORRUPTED_FILE;
		   }
		   else
		   {
				vLoadMetadataFile ( emmcRefreshdata );		   		
		   }
		}
    }  
    return s32ReturnValue;
}

/********************************************************************************
* FUNCTION        :  vLoadMetadataFile
* PARAMETER       :  peMMCRefreshdata - Metadata Path                                               
* RETURNVALUE     :  None
* DESCRIPTION     :  Load the refresh meta data
*                                
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*23.SEP.2016| Version 1.9         | SELVAKUMAR KALIMUTHU (RBEI/ECF2)
*           |                     | Validate Checksum of medatadaa's
*------------------------------------------------------------------------
**********************************************************************************/
static void vLoadMetadataFile( struct refresh_data eMMCRefreshdata )
{
	tS32 	s32Readcount;				

	emmcRefresh_data.u32no_of_sectors_covered	=	eMMCRefreshdata.u32no_of_sectors_covered;
	emmcRefresh_data.u32partition_covered		=	eMMCRefreshdata.u32partition_covered;
	emmcRefresh_data.u32day 					=	eMMCRefreshdata.u32day;
	emmcRefresh_data.u32month					=	eMMCRefreshdata.u32month;
	emmcRefresh_data.u32year					=	eMMCRefreshdata.u32year;
	emmcRefresh_data.u32due_day 				=	eMMCRefreshdata.u32due_day;
	emmcRefresh_data.u32due_month				=	eMMCRefreshdata.u32due_month;
	emmcRefresh_data.u32due_year				=	eMMCRefreshdata.u32due_year;

	for(s32Readcount=0; s32Readcount < CHECKSUM_COUNT; s32Readcount++)
	{
		emmcRefresh_data.u32checksum[s32Readcount]= eMMCRefreshdata.u32checksum[s32Readcount]; ;
	}

	emmcRefresh_data.u32TotalRefreshCount	=	eMMCRefreshdata.u32TotalRefreshCount;

	debug_print("Number of sectors covered: %u\t  Partition being read %u\t Last Refresh Date: %u/%u/%u\t Refresh Due Date: %u/%u/%u\n",
		emmcRefresh_data.u32no_of_sectors_covered,emmcRefresh_data.u32partition_covered, emmcRefresh_data.u32day, emmcRefresh_data.u32month, 
		emmcRefresh_data.u32year, emmcRefresh_data.u32due_day, emmcRefresh_data.u32due_month,emmcRefresh_data.u32due_year);
	
	debug_print("checksum %x-%x-%x-%x-%x-%x-%x-%x \n",emmcRefresh_data.u32checksum[0],
		emmcRefresh_data.u32checksum[1],emmcRefresh_data.u32checksum[2],emmcRefresh_data.u32checksum[3],
		emmcRefresh_data.u32checksum[4],emmcRefresh_data.u32checksum[5],emmcRefresh_data.u32checksum[6],
		emmcRefresh_data.u32checksum[7]);
	
	debug_print("Total Number of refresh Done %u\n",emmcRefresh_data.u32TotalRefreshCount);
}


/********************************************************************************
* FUNCTION        :  s32Read_ConfigFile
* PARAMETER       :  const tU8* pU8ConfigPath                                               
* RETURNVALUE     :  0 on success and the error number on Failure
* DESCRIPTION     :   Read Config File for /var/opt/bosch/static/emmc_refresh/config.txt
*                                
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.5         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | CFG3-1958:Implements refresh for the 
*           |                     | Micron eMMC chips.
*------------------------------------------------------------------------
*30.JUN.2016| Version 1.9         | SELVAKUMAR KALIMUTHU (RBEI/ECF2)
*           |                     | Config file optimization
*------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32Read_ConfigFile(const tU8* pU8ConfigPath, tU8 u8ReadDefaultConfig,eMMC_DEV_SIZE enDevSize)
{
    FILE 				*fp;  
    tS32  				s32ReturnValue = 0; 
    tU32 				u32SizeinGB,u32EnDebug = 0;
	struct refresh_data emmcRefreshData = { 0 };
    if (NULL != pU8ConfigPath)
    {
        fp = fopen(pU8ConfigPath, "r");
        if (NULL == fp)
        {
            snprintf(u8Error,MAXLENGTH,"%s: error %d, Open Config File Failed ", __func__, errno);
            writetoerrmem(u8Error); 
            s32ReturnValue = (tS32) eMMC_REFRESH_OPEN_ERROR;
        }
        else
        {
        	while ( fscanf(fp,"Size_GB  : %u\n",&u32SizeinGB) != -1 ) 
        	{
	        	/* Read all the Data specified in the Configuration File */
	            fscanf(fp,"Refresh_type: %u\nRefresh_size: %u\nRefresh_threshold: %u\n",
	                &emmcRefreshData.u32refresh_type, &emmcRefreshData.u32refresh_size,
	                &emmcRefreshData.u32Refresh_threshold); 

				fscanf(fp,"Normal_delay:%u\nFastMode_delay:%u\nMultiFactor:%u\n",
	                &emmcRefreshData.u32Refresh_normal_delay_ms, &emmcRefreshData.u32Refresh_fastmode_delay_ms,
	                &emmcRefreshData.u32Refresh_multiFactor); 	
				/*Read RBMP partition refresh details, 0-Ignore RPMB partition refresh, 1- include RPMB partition for refresh*/
				fscanf(fp,"rpmb: %u\n",&emmcRefreshData.u32EnableRPMBRefresh);

				/* FUTURE USE- Provision for debugging */
				fscanf(fp,"Enable Logs: %u \n",&u32EnDebug); 

				if ( ( u32SizeinGB == (tU32)enDevSize ) ||
					 ( (tU8)TRUE == u8ReadDefaultConfig ) )
				{
					debug_print( "Size_GB: %u\n",u32SizeinGB ); 
					u32EnableDebug = u32EnDebug;
					vLoadConfigFile (emmcRefreshData);
					s32ReturnValue = (tS32) eMMC_REFRESH_NOERROR;
					break;
				}
				else
				{
					s32ReturnValue = (tS32)eMMC_REFRESH_OUT_OF_RANGE;
				}
        	}

			fclose(fp);

            u32Refresh_ChunkSize = emmcRefresh_data.u32refresh_size;
            
            //protection added to prevent divide by 0 Error
            if ( ( 0 == u32Refresh_ChunkSize ) && \
				 ( eMMC_REFRESH_OUT_OF_RANGE != (ErrorValues)s32ReturnValue ) )	
                s32ReturnValue = (tS32)eMMC_REFRESH_CORRUPTED_FILE;

        }
    }
    else
    {
        snprintf(u8Error,MAXLENGTH,"%s: pU8ConfigPath is NULL ", __func__);
        writetoerrmem(u8Error); 
        s32ReturnValue = (tS32) eMMC_REFRESH_INVALID_ARGUMENT;
    }
     return s32ReturnValue;

}

/********************************************************************************
* FUNCTION        :  vLoadConfigFile
* PARAMETER       :  eMMCRefreshdata                                               
* RETURNVALUE     :  None
* DESCRIPTION     :  load the config file informations          
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*28.SEP.2016| Version 1.9         | SELVAKUMAR KALIMUTHU (RBEI/ECF2)
*           |                     | Config file optimization
*------------------------------------------------------------------------
**********************************************************************************/
static void vLoadConfigFile( struct refresh_data eMMCRefreshdata )
{
	tU32 u32count;
	 
	/* Copy the details to the functioning structure.*/
	emmcRefresh_data.u32refresh_type 				=	eMMCRefreshdata.u32refresh_type;
	emmcRefresh_data.u32refresh_size 				=	eMMCRefreshdata.u32refresh_size;
	emmcRefresh_data.u32Refresh_threshold 			=	eMMCRefreshdata.u32Refresh_threshold;
	emmcRefresh_data.u32Refresh_normal_delay_ms 	=	eMMCRefreshdata.u32Refresh_normal_delay_ms;
	emmcRefresh_data.u32Refresh_fastmode_delay_ms 	=	eMMCRefreshdata.u32Refresh_fastmode_delay_ms;
	emmcRefresh_data.u32Refresh_multiFactor			=	eMMCRefreshdata.u32Refresh_multiFactor;
	emmcRefresh_data.u32EnableRPMBRefresh			=	eMMCRefreshdata.u32EnableRPMBRefresh;
	emmc_partition.u32total_no_partition			=	eMMC_TOTAL_PARTITION;

	if ( 0 == emmcRefresh_data.u32EnableRPMBRefresh )
	{
		/*NOTE: mmcblk1rpmb can be ignored. In config file we add mmcblk1rpmb partition as last.
		 * In case of 2x eMMC device or if the partition order will change, This is not preper approach 
		 */ 				
		emmc_partition.u32total_no_partition -= 1;	
	}
	
	debug_print("read_config :Refresh_type: %u\trefresh_size: %u\trefresh_threshold: %u\n", 
	emmcRefresh_data.u32refresh_type, emmcRefresh_data.u32refresh_size, emmcRefresh_data.u32Refresh_threshold);    
	
	debug_print("NormalMode delay: %u\nFastMode delay: %u\nMultiplication Factor: %u\n", 
	emmcRefresh_data.u32Refresh_normal_delay_ms, emmcRefresh_data.u32Refresh_fastmode_delay_ms, 
	emmcRefresh_data.u32Refresh_multiFactor); 
	 
	debug_print("total_no_partition: %u\n",emmc_partition.u32total_no_partition);  
	
	// retrieve the partition names from config file
	for(u32count = 0; ((u32count < emmc_partition.u32total_no_partition) && (u32count < 10));u32count++)
	{
		debug_print("Partition %u: %s\n",u32count+1,emmc_partition.Partition_name[u32count]);
	}
	
	debug_print("RPMB: %u \n",emmcRefresh_data.u32EnableRPMBRefresh);
	debug_print("Enable Logs: %u \n",u32EnableDebug);	
	


}
/********************************************************************************
* FUNCTION        :  s32Write_RefreshMetaData
* PARAMETER       :  None                                               
* RETURNVALUE     :  0 on success and the error number on Failure
* DESCRIPTION     :  Update refresh data to the metadata file in the persistent 
*                    location in eMMC (/var/opt/bosch/persistent/emmc_refresh/metadata.txt)          
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initialversion 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*30.JUN.2016| Version 1.5         | CHAKITHA SARASWATHI (RBEI/ECF5)
*           |                     | CFG3-1958:Implements refresh for the 
*           |                     | Micron eMMC chips.
*------------------------------------------------------------------------
**********************************************************************************/
static tS32 s32Write_RefreshMetaData(const tU8* pU8MetadataPath)
{
    FILE *fp;
    tS32 FilePtr;
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    tS32 s32ReturnValue = 0;
    /* Step1:  Open a Dummy File*/
    fp = fopen(DUMMY_METADATA_PATH, "w+");
    if ( NULL == fp) 
    {
        debug_print("%s: Fail to open Metadata file on emmc card\n", __func__);
        snprintf(u8Error,MAXLENGTH,"%s: Meta data file"
        "open failed error %d", __func__,errno);
        writetoerrmem(u8Error); 
        s32ReturnValue = (tS32) eMMC_REFRESH_OPEN_ERROR;
    }
    else
    {   /* Initialising Present day / due date in case of First refresh*/
        if(emmcRefresh_data.u32due_day == 0) 
        {
            emmcRefresh_data.u32day = (tU32) t->tm_mday;
            emmcRefresh_data.u32month = (tU32) t->tm_mon + 1;
            emmcRefresh_data.u32year = (tU32) t->tm_year + 1900;
    
            emmcRefresh_data.u32due_day = (tU32) t->tm_mday;
            emmcRefresh_data.u32due_month = (tU32)  t->tm_mon + 1;
            emmcRefresh_data.u32due_year = (tU32) t->tm_year + 1900;
        }
        /* Update only the Present date if not First refresh*/
        else 
        {
            emmcRefresh_data.u32day = (tU32) t->tm_mday;
            emmcRefresh_data.u32month = (tU32) t->tm_mon + 1;
            emmcRefresh_data.u32year = (tU32) t->tm_year + 1900;
        }
		
         /*NOTE: mmcblk1rpmb is currently excluded from Refresh.*/
        if ((emmcRefresh_data.u32no_of_sectors_covered == 0) &&
            (emmcRefresh_data.u32partition_covered == emmc_partition.u32total_no_partition))
        {   
            /*On first refresh or After complete refresh(After partitions) updating the due date.
              Due date = Present date + 6 months */
            if(emmcRefresh_data.u32due_year < emmcRefresh_data.u32year)
            {
                emmcRefresh_data.u32due_year = emmcRefresh_data.u32year;
            }
            emmcRefresh_data.u32due_month = emmcRefresh_data.u32month + 6;
            if (emmcRefresh_data.u32due_month > TOTAL_MONTHS) 
            {
                emmcRefresh_data.u32due_month = emmcRefresh_data.u32due_month % TOTAL_MONTHS;
                emmcRefresh_data.u32due_year = emmcRefresh_data.u32year + 1;
            }
            emmcRefresh_data.u32partition_covered = 0;
            
            if(emmcRefresh_data.u32TotalRefreshCount == REFRESHCOUNT_INITMAGIC)
                emmcRefresh_data.u32TotalRefreshCount = 0;
            else
                emmcRefresh_data.u32TotalRefreshCount++;
        }
        vcalculate_checksum(emmcRefresh_data.u32checksum);
		/* For the backward compatibility we have added this condition. We have added the total refresh count to the end of metadata.txt 
		 * in our latest emmc refresh release.  
		 */
		if(emmcRefresh_data.u32TotalRefreshCount == REFRESHCOUNT_INITMAGIC)
		{
        	emmcRefresh_data.u32TotalRefreshCount = 0;		
		}		

        /*Step 2 : Update the dummy File*/
        fprintf(fp,"%u,%u,%u,%u,%u,%u,%u,%u-%x-%x-%x-%x-%x-%x-%x-%x-%x\n", emmcRefresh_data.u32no_of_sectors_covered,
            emmcRefresh_data.u32partition_covered,emmcRefresh_data.u32day, emmcRefresh_data.u32month, emmcRefresh_data.u32year,
            emmcRefresh_data.u32due_day, emmcRefresh_data.u32due_month, emmcRefresh_data.u32due_year,
            emmcRefresh_data.u32checksum[0],emmcRefresh_data.u32checksum[1],emmcRefresh_data.u32checksum[2],
            emmcRefresh_data.u32checksum[3],emmcRefresh_data.u32checksum[4],emmcRefresh_data.u32checksum[5],
            emmcRefresh_data.u32checksum[6],emmcRefresh_data.u32checksum[7],emmcRefresh_data.u32TotalRefreshCount);
        debug_print("%u,%u,%u,%u,%u,%u,%u,%u-%x-%x-%x-%x-%x-%x-%x-%x-%x\n", emmcRefresh_data.u32no_of_sectors_covered,
            emmcRefresh_data.u32partition_covered,emmcRefresh_data.u32day, emmcRefresh_data.u32month, emmcRefresh_data.u32year,
            emmcRefresh_data.u32due_day, emmcRefresh_data.u32due_month, emmcRefresh_data.u32due_year,
            emmcRefresh_data.u32checksum[0],emmcRefresh_data.u32checksum[1],emmcRefresh_data.u32checksum[2],
            emmcRefresh_data.u32checksum[3],emmcRefresh_data.u32checksum[4],emmcRefresh_data.u32checksum[5],
            emmcRefresh_data.u32checksum[6],emmcRefresh_data.u32checksum[7],emmcRefresh_data.u32TotalRefreshCount);
        
        /* Step 3: Ensuring that the data is physically stored on eMMC */
        fflush(fp);
        FilePtr = fileno(fp);
        if (FilePtr == -1)
        {
           system("sync");
        }
        else
        {
           fsync(FilePtr);
        }
        /*Step 4: Rename the file -atomic FS operation */
        s32ReturnValue = rename(DUMMY_METADATA_PATH,pU8MetadataPath);
        /*Note: Open file descriptors for oldpath are unaffected by rename(automic operation).*/
        if(s32ReturnValue == (tS32)eMMC_REFRESH_NOERROR)
        {
            if (FilePtr == -1)
            {
               system("sync");
            }
            else
            {
               fsync(FilePtr);
            }
        }
        else
        {
            debug_print("%s: Fail to rename dummy metadata file %d\n", __func__,errno);
            snprintf(u8Error,MAXLENGTH,"%s: Meta data file rename"
            " failed error %d", __func__,errno);
            writetoerrmem(u8Error); 
            s32ReturnValue = (tS32) eMMC_REFRESH_RENAME_ERROR;
        }
        fclose(fp);
    }
    return s32ReturnValue;
}

/********************************************************************************
* FUNCTION        :  main
* PARAMETER       :  tS32 argc, tS8* argv[]                                               
* RETURNVALUE     :  tS32
* DESCRIPTION     :  This function initialises the signal handler, eMMC refresh structures and
*                    starts eMMC refresh process either from 1st sector or from the sector
*                    refreshed in previous power cycle.        
*------------------------------------------------------------------------
* Date      |       Version       | Author & comments
*-----------|---------------------|--------------------------------------
*03.DEC.2014| Initial version 1.0  | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
*12.DEC.2014| Version 1.1         | SWATHI BOLAR (RBEI/ECF5)
* -----------------------------------------------------------------------
**********************************************************************************/
tS32 main()
{   
    tS32 	s32ReturnValue;
    tU32 	u32count = 0; 
    tS32	s32SleepInSec = (tS32)eMMC_REFRESH_WAIT_TIME;
	
	while ( 0 < s32SleepInSec )
	{
	    /* wait for two minutes for target to start */
	    s32SleepInSec = (tS32)sleep( (tU32)s32SleepInSec );
	}	
	
    s32ReturnValue = signal_handler_setup();
    if(s32ReturnValue >= 0)
    {
        /* setup includes validation & reading of config file & metadata file */
        s32ReturnValue = s32eMMCRefresh_setup();
        if(s32ReturnValue == REFRESH_NEEDED)
        {  
            /* Refresh the 4 partitions in 4.51 and higher chip, namely:
             0 - mmcblk1boot0
             1 - mmcblk1boot1
             2 - mmcblk1 (User Partition)
             3 - mmcblk1rpmb
             total_no_partition = 4
            */
            for(u32count = emmcRefresh_data.u32partition_covered; 
                 u32count < emmc_partition.u32total_no_partition; u32count++)
            {
                debug_print("Partition %u: %s\n",u32count+1,emmc_partition.Partition_name[u32count]);
                emmcRefresh_data.u32partition_covered = u32count;
                /*Start refresh for the Particular partition*/
                s32ReturnValue = s32StartEmmcRefresh(emmc_partition.Partition_name[u32count]);
                if(s32ReturnValue < 0)
                {
                    snprintf(u8Error,MAXLENGTH,"eMMC Refresh: %s: StartEmmcRefresh Returned error %d -"
                     "Errno %d",emmc_partition.Partition_name[u32count],s32ReturnValue,errno);
                    writetoerrmem(u8Error); 
                    break;
                }
            }
        }
        else if (s32ReturnValue < 0)
        {   
            snprintf(u8Error,MAXLENGTH,"eMMC Refresh: eMMCRefresh_setup Returned error %d -"
            "Errno %d ",s32ReturnValue,errno);
            writetoerrmem(u8Error);          
        }
        /* The systemd will assume the non return value as an error.
        Hence When the refresh process exits when no refresh is needed
        We force the main to return 0 to prevent errors from systemd*/
        if(s32ReturnValue == NOREFRESH_NEEDED)
            
        {
            s32ReturnValue = 0;//condition is not error 
        }
    }
    else
    {
        snprintf(u8Error,MAXLENGTH,"eMMC Refresh: signal_handler_setup Returned error %d"
        "-Errno %d ",s32ReturnValue,errno);
        writetoerrmem(u8Error);          
    }
    return s32ReturnValue;
}
/*END*/
