/******************************************************************************\
 *
 * FILE:         system_definition.h
 *
 *
 * DESCRIPTION:  
 *               
 *
 * AUTHOR:       CM/DI ESI 1 - Dirk Tiemann
 *
 * COPYRIGHT:    (c) 2002 Blaupunktwerke GmbH, Hildesheim
 *
\******************************************************************************/

/* PVCS Change Description */
/*
  $Log:   //hi230124/projects/Vasco/swnavi/archives/products/system/system_definition.h-arc  $
 * 
 *    Rev 1.0   Feb 14 2003 13:36:40   TND2HI
 * Initial revision.
*/

/******************************************************************************/
#ifndef SYSTEM_DEFINITION_HEADER
#define SYSTEM_DEFINITION_HEADER

#ifndef NULL
#ifdef __cplusplus
#define NULL                     0
#else
#define NULL                    ( (void*) 0 )
#endif
#endif

#ifndef FALSE
#define FALSE                  (0)
#endif

#ifndef TRUE
#define TRUE                   (1)
#endif

#endif  /* SYSTEM_DEFINITION_HEADER */

/******************************************************************************
| EOF
|----------------------------------------------------------------------------*/
