/******************************************************************************\
 *
 * FILE:         system_limit.h
 *
 *
 * DESCRIPTION:  This file contains lower and upper limits for the standard
 *               types. 
 *               
 *
 * AUTHOR:       CM/DI ESI 1 - Dirk Tiemann
 *
 * COPYRIGHT:    (c) 2002 Blaupunktwerke GmbH, Hildesheim
 *
\******************************************************************************/

/* PVCS Change Description */
/*
  $Log:   //hi230124/projects/Vasco/swnavi/archives/products/system/system_limit.h-arc  $
 * 
 *    Rev 1.2   12 Jun 2003 10:24:36   MRG2HI
 * one step back
 * 
 *    Rev 1.0   Feb 14 2003 13:37:10   TND2HI
 * Initial revision.
*/

/******************************************************************************/
#ifndef SYSTEM_LIMIT_HEADER
#define SYSTEM_LIMIT_HEADER

#ifdef __cplusplus
extern "C" {
#endif



/*****************************************************************************
| lower and upper limits (scope: global)
|---------------------------------------------------------------------------*/


/* -- Target-independent limits: -- */

#define LIMIT_C_MIN_U8          0U
#define LIMIT_C_MAX_U8          255U

#define LIMIT_C_MIN_S8          -128
#define LIMIT_C_MAX_S8          127


#define LIMIT_C_MIN_U16         0U
#define LIMIT_C_MAX_U16         65535U

#define LIMIT_C_MIN_S16         -32768
#define LIMIT_C_MAX_S16         32767


#define LIMIT_C_MIN_U32         0UL
#define LIMIT_C_MAX_U32         4294967295UL

#define LIMIT_C_MIN_S32         ((tS32)(-2147483647L - 1))
#define LIMIT_C_MAX_S32         2147483647L


/* -- Target-dependent limits: -- */

/* LINUX: */
#if defined (VASCO_OS_LINUX)


#define LIMIT_C_MIN_UCHAR       0U
#define LIMIT_C_MAX_UCHAR       255U

#define LIMIT_C_MIN_CHAR        -128
#define LIMIT_C_MAX_CHAR        127


#define LIMIT_C_MIN_USHORT      0U
#define LIMIT_C_MAX_USHORT      65535U

#define LIMIT_C_MIN_SHORT       -32768
#define LIMIT_C_MAX_SHORT       32767


#define LIMIT_C_MIN_UINT        0UL
#define LIMIT_C_MAX_UINT        4294967295UL

#define LIMIT_C_MIN_INT         -2147483648L
#define LIMIT_C_MAX_INT         2147483647L


#define LIMIT_C_MIN_ULONG       0UL
#define LIMIT_C_MAX_ULONG       4294967295UL

#define LIMIT_C_MIN_LONG        -2147483648L
#define LIMIT_C_MAX_LONG        2147483647L


/* SOLARIS: */
#elif defined (VASCO_OS_SOLARIS)


#define LIMIT_C_MIN_UCHAR       0U
#define LIMIT_C_MAX_UCHAR       255U

#define LIMIT_C_MIN_CHAR        -128
#define LIMIT_C_MAX_CHAR        127


#define LIMIT_C_MIN_USHORT      0U
#define LIMIT_C_MAX_USHORT      65535U

#define LIMIT_C_MIN_SHORT       -32768
#define LIMIT_C_MAX_SHORT       32767


#define LIMIT_C_MIN_UINT        0UL
#define LIMIT_C_MAX_UINT        4294967295UL

#define LIMIT_C_MIN_INT         -2147483648L
#define LIMIT_C_MAX_INT         2147483647L


#define LIMIT_C_MIN_ULONG       0UL
#define LIMIT_C_MAX_ULONG       4294967295UL

#define LIMIT_C_MIN_LONG        -2147483648L
#define LIMIT_C_MAX_LONG        2147483647L


/* RX732: */
#elif defined (VASCO_OS_RX732)


#define LIMIT_C_MIN_UCHAR       0U
#define LIMIT_C_MAX_UCHAR       255U

#define LIMIT_C_MIN_CHAR        -128
#define LIMIT_C_MAX_CHAR        127


#define LIMIT_C_MIN_USHORT      0U
#define LIMIT_C_MAX_USHORT      65535U

#define LIMIT_C_MIN_SHORT       -32768
#define LIMIT_C_MAX_SHORT       32767


#define LIMIT_C_MIN_UINT        0UL
#define LIMIT_C_MAX_UINT        4294967295UL

#define LIMIT_C_MIN_INT         -2147483648L
#define LIMIT_C_MAX_INT         2147483647L


#define LIMIT_C_MIN_ULONG       0UL
#define LIMIT_C_MAX_ULONG       4294967295UL

#define LIMIT_C_MIN_LONG        -2147483648L
#define LIMIT_C_MAX_LONG        2147483647L


/* WINDOWS NT/CE/Nucleus: */
#elif defined (VASCO_OS_WINNT)  || defined (VASCO_OS_WINCE) || defined (VASCO_OS_NUCLEUS)|| defined (VASCO_OS_TENGINE)


#define LIMIT_C_MIN_UCHAR       0U
#define LIMIT_C_MAX_UCHAR       255U

#define LIMIT_C_MIN_CHAR        -128
#define LIMIT_C_MAX_CHAR        127


#define LIMIT_C_MIN_USHORT      0U
#define LIMIT_C_MAX_USHORT      65535U

#define LIMIT_C_MIN_SHORT       -32768
#define LIMIT_C_MAX_SHORT       32767


#define LIMIT_C_MIN_UINT        0UL
#define LIMIT_C_MAX_UINT        4294967295UL

#define LIMIT_C_MIN_INT         -2147483648L
#define LIMIT_C_MAX_INT         2147483647L


#define LIMIT_C_MIN_ULONG       0UL
#define LIMIT_C_MAX_ULONG       4294967295UL

#define LIMIT_C_MIN_LONG        -2147483648L
#define LIMIT_C_MAX_LONG        2147483647L


/* VXWORKS: */
#elif defined (VASCO_OS_VXWORKS)


#define LIMIT_C_MIN_UCHAR       0U
#define LIMIT_C_MAX_UCHAR       255U

#define LIMIT_C_MIN_CHAR        -128
#define LIMIT_C_MAX_CHAR        127


#define LIMIT_C_MIN_USHORT      0U
#define LIMIT_C_MAX_USHORT      65535U

#define LIMIT_C_MIN_SHORT       -32768
#define LIMIT_C_MAX_SHORT       32767


#define LIMIT_C_MIN_UINT        0UL
#define LIMIT_C_MAX_UINT        4294967295UL

#define LIMIT_C_MIN_INT         -2147483648L
#define LIMIT_C_MAX_INT         2147483647L


#define LIMIT_C_MIN_ULONG       0UL
#define LIMIT_C_MAX_ULONG       4294967295UL

#define LIMIT_C_MIN_LONG        -2147483648L
#define LIMIT_C_MAX_LONG        2147483647L


#else
#error The file system_limit.h is not prepared for the current target!
#endif


#ifdef __cplusplus
}
#endif


#endif  /* SYSTEM_LIMIT_HEADER */
/******************************************************************************
| EOF
|----------------------------------------------------------------------------*/
