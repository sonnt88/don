/******************************************************************************\
 *
 * FILE:         system_types.h
 *
 *
 * DESCRIPTION:  
 *               
 *
 * AUTHOR:       CM/DI 
 *
 * COPYRIGHT:    (c) 2016 Bosch GmbH, Hildesheim
 *
\******************************************************************************/

#ifndef SYSTEM_TYPES_HEADER
#define SYSTEM_TYPES_HEADER

/* --Include needed for size_t. */
#include "stddef.h"

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************
| typedefs (scope: global)
|---------------------------------------------------------------------------*/

typedef unsigned char           tBool;

/* -- Target-independent types: -- */


typedef unsigned char           tU8;
typedef signed char			    tS8; // --->    typedef signed char             tS8;
typedef char                    tChar;

/* sollen entfallen !!! 06.02.03 Tn */
typedef tChar                   tC8;   
typedef tU8                     tUChar;
typedef unsigned int		    tC16;


typedef char*                   tString;
typedef const char*             tCString;

typedef unsigned short          tU16;
typedef short                   tS16;


typedef unsigned short          tUShort;
typedef short                   tShort;

typedef unsigned int            tUInt;
typedef int                     tInt;

typedef unsigned long           tULong;
typedef long                    tLong;


typedef float                   tFloat;
typedef double                  tDouble;
typedef long double             tLDouble;

typedef size_t                  tSize;

// typedef void                    tVoid;
#define tVoid void       
// ---> typedef of void is nonstandard and leads to compiler errors

typedef unsigned int            tUBitfield;
typedef int                     tBitfield;

typedef float                   tF32;


/* -- Target-dependent types: -- */
typedef int                     tS32;
typedef double                  tF64;
#include <stdint.h>
typedef uint64_t			    tU64;
typedef int64_t                 tS64;

#ifdef VARIANT_S_FTR_ENABLE_64_BIT_SUPPORT
typedef unsigned int            tU32;/* required int is  32 bit value*/
#else
typedef unsigned long           tU32;/* required int is  32 bit value*/
#endif


#ifdef __cplusplus
}
#endif


#endif  /* SYSTEM_TYPES_HEADER */
/******************************************************************************
| EOF
|----------------------------------------------------------------------------*/
