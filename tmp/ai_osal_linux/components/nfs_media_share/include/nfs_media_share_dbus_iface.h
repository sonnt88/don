/************************************************************************
| FILE:         nfs_media_share_dbus_iface.h
| PROJECT:      BaseSW
| SW-COMPONENT: NMS application
|------------------------------------------------------------------------
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file corresponding to 
|               nfs_media_share_dbus_iface.c
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2011 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 21.06.16  | Initial revision           | kpa3kor
*************************************************************************/
#ifndef __NMS_DBUS_IFACE_H__
#define __NMS_DBUS_IFACE_H__

#include <glib.h>
#include <gio/gio.h>
#include <stdio.h>
#include <stdlib.h>
#include "nfs_media_share_dbus_generated.h"

#define NMS_DEBUG(format, ...) 		g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define NMS_CRITICAL(format, ...) 	g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define NMS_WARNING(format, ...) 	g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

void nfs_media_share_dbus_service_start (void);
NfsMediaShare *nfs_media_share_object_get(void);

#endif