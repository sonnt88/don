/************************************************************************
| FILE:         nfs_media_share.h
| PROJECT:      BaseSW
| SW-COMPONENT: NMS application
|------------------------------------------------------------------------
|------------------------------------------------------------------------
| DESCRIPTION:  This is the header file corresponding to 
|               nfs_media_share.c
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2011 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 27.04.16  | Initial revision           | kpa3kor
*************************************************************************/

#ifndef __NFS_MEDIA_SHARE_H__
#define __NFS_MEDIA_SHARE_H__

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <poll.h>
#include <errno.h>
#include <sys/mount.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <semaphore.h>
#include <string.h>
#include <glib.h>
#include <pthread.h>
#include <dbus/dbus-glib.h>
#include "automounter_types.h"
#include "automounter_api.h"
#include "automounter_api_ctrl.h"
#include "automounter_api_info.h"
#include "automounter_api_events.h"

#define BOOL unsigned int
/* can be changed to a higher value */
#define MAX_EXPORTED_PARTITIONS 	8
#define USB_DEVICE 			"usb"
#define SD_CARD_DEVICE		"sd_card"
#define EXPORT_PARAMS 		"rw,sync,no_root_squash,no_subtree_check "
#define SLASH 				"/"

#define VM
#ifndef VM
	#define EXPORT_ADDR		"172.17.0.6:" // export to host
#else
	#define EXPORT_ADDR		"172.17.0.2:" // export to VM
#endif

#define EXPORT_CMD 			"exportfs -io "
#define UNEXPORT_CMD 		"exportfs -u "
#define DEV_MEDIA			"dev.media"
#define REMOUNT_CMD			"mount -o remount ,rw "
#define NMS_SEM_ACCESS 		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP

typedef void * (* ThreadEntry) (void *);

typedef struct{
	char sMode[4];
	char sDeviceType[8];
	char sPartIdentifier[64];
	char sMountPoint[64];
	char sFileSystem[32];
	int s32PartitionNumber;
	unsigned int u32Status;
}trMountedPartition;

enum {
EN_NODEVICE_DETECTED = 0,
EN_PARTITION_MOUNTED,
EN_PARTITION_UNMOUNTED,
EN_PARTITION_REMOUNTED
};

GVariant *get_all_exported_partition_info(void);
void vNmsAmThreadCreate(void);

#endif