/************************************************************************
| FILE:         nfs_media_share_dbus_iface.c
| PROJECT:      BaseSW
| SW-COMPONENT: NMS application
|------------------------------------------------------------------------
|------------------------------------------------------------------------
| DESCRIPTION:  This file contains the sources for providing interface 
|               as part of NFS Media Share appln
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2011 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 21.06.16  | Initial revision           | kpa3kor
*************************************************************************/

#include "nfs_media_share.h"
#include "nfs_media_share_dbus_iface.h"

static guint u32NMS_DbusOwnderID;
static NfsMediaShare *NMS_Object = NULL;

NfsMediaShare* nfs_media_share_object_get (void)
{
	if (NMS_Object != NULL)
		return NMS_Object;
	else
		return NULL;
}

static gboolean handle_get_export_snapshots(NfsMediaShare *object, GDBusMethodInvocation *invocation, const gchar *arg_AppName)
{
	GVariant *exported_partitions;
	gboolean bRet = TRUE;
	NMS_DEBUG(" invoked by %s", arg_AppName);
	
	/* fetch all exported partition data */
	exported_partitions = get_all_exported_partition_info();
	
	if (exported_partitions != NULL)
	{
		NMS_DEBUG("handle_get_export_snapshots complete");
		
		nfs_media_share_complete_get_export_snapshots(object, invocation, exported_partitions);
		
		return bRet;
	}
	else
	{
		NMS_DEBUG("Cannot get snapshot");

		g_dbus_method_invocation_return_dbus_error(invocation, "org.rbcm.Nfs.MediaShare.Failed", "Cannot get snapshot");

		/* Get snapshot failure */
		nfs_media_share_complete_get_export_snapshots(object, invocation, FALSE);
	}
	
	return bRet;
}

static void nfs_media_share_register_handlers(NfsMediaShare *object)
{
	NMS_DEBUG("handle_get_export_snapshots");
	
	g_signal_connect(object, "handle-get-export-snapshots", G_CALLBACK (handle_get_export_snapshots), NULL);
}

void nfs_media_share_bus_aquired (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	(void) user_data;
	NMS_DEBUG(" %s", name);
	
	/* create a new skeleton object */
	NMS_Object = nfs_media_share_skeleton_new();
	
	if (NULL != NMS_Object)
	{
		/* provide signal handlers for the method exported */
		nfs_media_share_register_handlers(NMS_Object);
		
		/* create automounter thread */
		vNmsAmThreadCreate();
		
		/* Exports interface NMS_Object at object_path "/org/rbcm/Nfs/MediaShare" on connection */
		if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON(NMS_Object), connection, "/org/rbcm/Nfs/MediaShare", NULL)) /*lint !e826 */
			NMS_CRITICAL(" skeleton export error ");
	}
}

void nfs_media_share_name_aquired (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	(void) connection;
	(void) user_data;
	NMS_DEBUG(" %s", name);
}

void nfs_media_share_name_lost (GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	(void) connection;
	(void) user_data;
	NMS_DEBUG(" %s ", name);
}

void nfs_media_share_dbus_service_start (void)
{
	GMainLoop *loop = g_main_loop_new(NULL, FALSE);
	
	u32NMS_DbusOwnderID = g_bus_own_name (G_BUS_TYPE_SYSTEM,
						"org.rbcm.Nfs.MediaShare",
						G_BUS_NAME_OWNER_FLAGS_NONE,
						nfs_media_share_bus_aquired,
						nfs_media_share_name_aquired,
						nfs_media_share_name_lost,
						NULL,
						NULL);
						
	NMS_DEBUG("g_main_loop_run: with owner id %d",u32NMS_DbusOwnderID);
	
	g_main_loop_run(loop);
	
	g_bus_unown_name(u32NMS_DbusOwnderID);
}

