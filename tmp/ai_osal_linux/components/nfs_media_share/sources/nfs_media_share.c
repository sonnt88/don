/************************************************************************
| FILE:         nfs_media_share.c
| PROJECT:      BaseSW
| SW-COMPONENT: NMS application
|------------------------------------------------------------------------
|------------------------------------------------------------------------
| DESCRIPTION:  This is the source for the NFS Media Share
|                 
|                
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2011 Robert Bosch GmbH
| HISTORY:      
| Date      | Modification               | Author
| 27.04.16  | Initial revision           | kpa3kor
*************************************************************************/

#include "nfs_media_share.h"
#include "nfs_media_share_dbus_iface.h"


/*************************************************************************
* Variables declaration (scope: Global)
**************************************************************************/
static trMountedPartition rMountedPartition[MAX_EXPORTED_PARTITIONS];
static int s32TotalPartitionsExported = 0;
static sem_t* NMS_lock;
static int ReqId = -1;

GVariant *get_all_exported_partition_info(void)
{
	GVariant *out_arg = NULL;
    GVariantBuilder *builder1=NULL, *builder2=NULL;
	int i = 0;
	
	/* if partitions exported? then build the data */
	if (s32TotalPartitionsExported > 0)
	{
		builder2 = g_variant_builder_new(G_VARIANT_TYPE("a{sv}"));
		sem_wait(NMS_lock);
		for(i = 0; i < MAX_EXPORTED_PARTITIONS; i++)
		{
			if(rMountedPartition[i].sPartIdentifier[0] != 0)
			{
				builder1 = g_variant_builder_new(G_VARIANT_TYPE("a{sv}"));
				if (builder1 != NULL)
				{
					/* build the data keeping rMountedPartition[i].sPartIdentifier as the KEY
						as this is expected to be unique for each partition */
					g_variant_builder_add (builder1, "{sv}", "DeviceType", g_variant_new_string(rMountedPartition[i].sDeviceType));
					g_variant_builder_add (builder1, "{sv}", "MountPoint", g_variant_new_string(rMountedPartition[i].sMountPoint));
					g_variant_builder_add (builder1, "{sv}", "FileSystem", g_variant_new_string(rMountedPartition[i].sFileSystem));
					g_variant_builder_add (builder1, "{sv}", "Mode", g_variant_new_string(rMountedPartition[i].sMode));
					g_variant_builder_add (builder1, "{sv}", "PartitionNumber", g_variant_new_int32(rMountedPartition[i].s32PartitionNumber));
					g_variant_builder_add (builder1, "{sv}", "Status", g_variant_new_uint32(rMountedPartition[i].u32Status));
					g_variant_builder_add(builder2, "{sv}", rMountedPartition[i].sPartIdentifier, g_variant_new("a{sv}", builder1));
					g_variant_builder_clear(builder1);
				}
				else
				{
					NMS_DEBUG(" builder1 is NULL ");
				}
			}
		}
		sem_post(NMS_lock);
		out_arg = g_variant_new("a{sv}", builder2);
		g_variant_builder_clear (builder2);
	}
	return out_arg;
}

GVariant *get_exported_partition_info(int index)
{
	GVariant *out_arg;
    GVariantBuilder *builder;
	NMS_DEBUG("index %d", index);
	/* build the partition info for export/unexport signal */
	builder = g_variant_builder_new(G_VARIANT_TYPE("a{sv}"));
	g_variant_builder_add (builder, "{sv}", "PartitionID", g_variant_new_string(rMountedPartition[index].sPartIdentifier));
	g_variant_builder_add (builder, "{sv}", "DeviceType", g_variant_new_string(rMountedPartition[index].sDeviceType));
	g_variant_builder_add (builder, "{sv}", "MountPoint", g_variant_new_string(rMountedPartition[index].sMountPoint));
	g_variant_builder_add (builder, "{sv}", "FileSystem", g_variant_new_string(rMountedPartition[index].sFileSystem));
	g_variant_builder_add (builder, "{sv}", "Mode", g_variant_new_string(rMountedPartition[index].sMode));
	g_variant_builder_add (builder, "{sv}", "PartitionNumber", g_variant_new_int32(rMountedPartition[index].s32PartitionNumber));
	g_variant_builder_add (builder, "{sv}", "Status", g_variant_new_uint32(rMountedPartition[index].u32Status));

    out_arg = g_variant_builder_end(builder);
    g_variant_builder_clear(builder);
	return out_arg;
}

static int s32NmsStorePartition(const partition_info_t *partition_info)
{
	int s32Ret = -1;
	int i;

	for(i=0;i < MAX_EXPORTED_PARTITIONS;i++)
	{
		if(rMountedPartition[i].sPartIdentifier[0] == 0)
		{
			s32TotalPartitionsExported++;
			NMS_DEBUG("partition_name %s on position %d",partition_info->identifier,i);
			strncpy(rMountedPartition[i].sPartIdentifier,partition_info->identifier,strlen(partition_info->identifier));
			s32Ret = i;
			break;
		}
	}
	return s32Ret;
}

static int s32NmsFindPartition(const char *partition_name)
{
	int s32Ret = -1;
	int i;
   
	NMS_DEBUG("partition_name %s",partition_name);
   
	if(partition_name)
	{
		for(i=0;i<MAX_EXPORTED_PARTITIONS;i++)
		{
			if(rMountedPartition[i].sPartIdentifier[0] != 0)
			{
				if(!strncmp(rMountedPartition[i].sPartIdentifier,partition_name,strlen(partition_name)))
				{
					NMS_DEBUG(" partition_name %s on position %d",partition_name,i);
					s32Ret = i;
					break;
				}
			}
		}
	}
	return s32Ret;
}

void nms_establish_connection_success(void)
{
	if(automounter_api_get_snapshot(SNAPSHOT_MOUNTED_PARTITIONS_ONLY, &ReqId) != RESULT_OK)
	{
		NMS_DEBUG("error automounter_api_get_snapshot -> failed");
	}
}

void nms_establish_connection_failure(void)
{
	NMS_DEBUG("establish_connection_failure");
}

void nms_connection_lost()
{
	automounter_api_state_t state = automounter_api_get_state();
	error_code_t error = automounter_api_try_connect();
	NMS_DEBUG(" Automounter State %d ",state);
	if ((error == RESULT_OK) || (error == RESULT_DAEMON_NOT_RUNNING))
	{
	   NMS_DEBUG("connection_lost reconnected");
	}
	else
	{
	   NMS_WARNING("connection_lost reconnection failed Error:%d",error);
	}
}

void nms_device_detected(const device_info_t *device_info)
{
	NMS_DEBUG("Parent device: %s",device_info->interface_id);
    NMS_DEBUG("Identifier: %s",device_info->identifier);
    NMS_DEBUG("Detected partitions: %d",device_info->detected_partition_cnt);
}

void nms_device_nomedia(const device_info_t *device_info)
{
	NMS_DEBUG("Parent device: %s",device_info->interface_id);
    NMS_DEBUG("Identifier: %s",device_info->identifier);
}

void nms_device_automounted(const device_info_t *device_info)
{
	NMS_DEBUG("Parent device: %s",device_info->interface_id);
    NMS_DEBUG("Identifier: %s",device_info->identifier);
    NMS_DEBUG("Detected partitions: %d",device_info->detected_partition_cnt);
}

void nms_device_unmounted(const device_info_t *device_info)
{
	NMS_DEBUG("Parent device: %s",device_info->interface_id);
    NMS_DEBUG("Identifier: %s",device_info->identifier);
}

void nms_device_invalid(const device_info_t *device_info)
{
	NMS_DEBUG("Parent device: %s",device_info->interface_id);
    NMS_DEBUG("Identifier: %s",device_info->identifier);
}

void nms_partition_detected(const partition_info_t *partition_info, const device_info_t *device_info)
{
	(void) device_info;
	NMS_DEBUG("%s",partition_info->identifier);
}

void nms_partition_unsupported(const partition_info_t *partition_info, const device_info_t *device_info)
{
	(void) device_info;
	NMS_DEBUG("%s",partition_info->identifier);
	NMS_DEBUG("unsupported reason: %s",automounter_api_get_partition_unsupported_reason_string(partition_info->unsupported_reason));
}

void vNmsSendDbusExportSignal (int index)
{
	GVariant *exported_partition = NULL;
	NfsMediaShare *object;
	NMS_DEBUG("");
	/*  fetch the dbus object */
	object = nfs_media_share_object_get();
	if (object != NULL)
	{
		/*  build the unexport data */
		exported_partition = get_exported_partition_info(index);
		NMS_DEBUG("emitting dbus export signal");
		/* emit the dbus export signal */
		nfs_media_share_emit_export_signal(object, exported_partition);
	}
}

void vNmsExportPartition(const char *mountpoint,int index)
{
	char export_cmd[200] = {0};
	/* frame the EXPORT_CMD */
	strcpy(export_cmd,EXPORT_CMD);
	/* copy the export params */
	strncat(export_cmd,EXPORT_PARAMS,strlen(EXPORT_PARAMS));
	/* copy the client name */
	strncat(export_cmd,EXPORT_ADDR,strlen(EXPORT_ADDR));
	/* copy the mount point */
	strncat(export_cmd,mountpoint,strlen(mountpoint));
	NMS_DEBUG(" %s ",export_cmd);
	system(export_cmd);
	/* send the dbus export signal */
	vNmsSendDbusExportSignal(index);
}

static BOOL vNmsCheckForDevMedia(const char *mountpoint)
{
	char devfile[100];
	struct stat filestat;
	BOOL bRet = FALSE;
	/* check if /<mountpoint>/dev.media is present*/
	strcpy(devfile,mountpoint);
	strncat(devfile,SLASH,strlen(SLASH));
	strncat(devfile,DEV_MEDIA,strlen(DEV_MEDIA));
	if ( stat(devfile, &filestat) != -1 )
	{
		int ReqID = 0;
		/* remount the file system as read-write as it is a development media */
		NMS_DEBUG("%s found; remounting it", devfile);
		if ( automounter_api_remount_partition_by_mountpoint(mountpoint,"rw",ReqID,NULL) != RESULT_OK )
		{
			NMS_DEBUG("remounting %s failed", mountpoint);
			bRet = FALSE;
		}
		else
		{
			NMS_DEBUG("remounting %s completed",mountpoint);
			bRet = TRUE;
		}
	}
	
	return bRet;
}

void vNmsHandleMount(const partition_info_t *partition_info)
{
	int i;
	char *pStr = NULL;
	BOOL bMode = FALSE;
    i = s32NmsFindPartition(partition_info->identifier);
	if(i == -1)
	{
		sem_wait(NMS_lock);
		i = s32NmsStorePartition(partition_info);
		if(rMountedPartition[i].u32Status != EN_PARTITION_MOUNTED)
		{
			if(rMountedPartition[i].u32Status == EN_PARTITION_REMOUNTED)
			{
				NMS_DEBUG("partition_mounted %s remounting detected",partition_info->identifier);
			}
			else
			{
				/* check if the media is for development purpose */
				bMode = vNmsCheckForDevMedia(partition_info->mount_point);
				if (bMode == TRUE)
					strncpy(rMountedPartition[i].sMode,"rw",2);
				else 
					strncpy(rMountedPartition[i].sMode,"ro",2);
				
				/* store the partition identifier */
				strncpy(rMountedPartition[i].sPartIdentifier,partition_info->identifier,strlen(partition_info->identifier));
				/* store the mount point */
				strncpy(rMountedPartition[i].sMountPoint,partition_info->mount_point,strlen(partition_info->mount_point));
				/* evaluate if the device is sd_card or usb */
				pStr = (char *) strstr(partition_info->interface_id,"mmcblk");
				if(pStr)
					strncpy(rMountedPartition[i].sDeviceType,SD_CARD_DEVICE,strlen(SD_CARD_DEVICE));
				else
					strncpy(rMountedPartition[i].sDeviceType,USB_DEVICE,strlen(USB_DEVICE));
				/* store the filesystem type */
				strncpy(rMountedPartition[i].sFileSystem,partition_info->mount_fs,strlen(partition_info->mount_fs));
				/* store the partition number */
				rMountedPartition[i].s32PartitionNumber = partition_info->partition_no;
				/* store the status */
				rMountedPartition[i].u32Status = EN_PARTITION_MOUNTED;
				/* export the partition partition */
				vNmsExportPartition(partition_info->mount_point,i);
			}
		}
		sem_post(NMS_lock);
	}
}

void nms_partition_mounted(const partition_info_t *partition_info, const device_info_t *device_info)
{
	(void) device_info;
	NMS_DEBUG("%s",partition_info->identifier);
	vNmsHandleMount(partition_info);
}

void nms_partition_mount_err(const partition_info_t *partition_info, const device_info_t *device_info)
{
	(void) device_info;
	NMS_DEBUG("%s",partition_info->identifier);
}

void nms_partition_remounting(const partition_info_t *partition_info, const device_info_t *device_info)
{
	int i;
	(void) device_info;
	i = s32NmsFindPartition(partition_info->identifier);
	if(i != -1)
	{
		sem_wait(NMS_lock);
		rMountedPartition[i].u32Status = EN_PARTITION_REMOUNTED;
		sem_post(NMS_lock);
	}
	NMS_DEBUG("%s",partition_info->identifier);
}

void vNmsSendDbusUnexportSignal (int index)
{
	GVariant *unexported_partition = NULL;
	NfsMediaShare *object;
	/*  fetch the dbus object */
	object = nfs_media_share_object_get();
	if (object != NULL)
	{
		/*  build the unexport data */
		unexported_partition = get_exported_partition_info(index);
		/* emit the dbus unexport signal */
		nfs_media_share_emit_unexport_signal(object, unexported_partition);
	}
}

void vNmsUnexportPartition(const char *mountpoint, int index)
{
	char unexport_cmd[100] = {0};
	/* Frame the export command */
	strcpy(unexport_cmd,UNEXPORT_CMD);
	/* copy the client name */
	strncat(unexport_cmd,EXPORT_ADDR,strlen(EXPORT_ADDR));
	/* copy the mount point */
	strncat(unexport_cmd,mountpoint,strlen(mountpoint));
	NMS_DEBUG(" %s ",unexport_cmd);
	system(unexport_cmd);
	/* send the dbus unexport signal */
	vNmsSendDbusUnexportSignal(index);
}

void vNmsHandleUnmount(const partition_info_t *partition_info)
{
	int i = s32NmsFindPartition(partition_info->identifier);
	if(i != -1)
	{
		sem_wait(NMS_lock);
		/* update the status to unmounted */
		rMountedPartition[i].u32Status = EN_PARTITION_UNMOUNTED;
		/* unexport the partition partition */
		vNmsUnexportPartition(partition_info->mount_point, i);
		/* clean all the entries */
		rMountedPartition[i].s32PartitionNumber = -1;
		memset(rMountedPartition[i].sMode,0,sizeof(rMountedPartition[i].sMode));
		memset(rMountedPartition[i].sDeviceType,0,sizeof(rMountedPartition[i].sDeviceType));
		memset(rMountedPartition[i].sPartIdentifier,0,sizeof(rMountedPartition[i].sPartIdentifier));
		memset(rMountedPartition[i].sMountPoint,0,sizeof(rMountedPartition[i].sMountPoint));
		memset(rMountedPartition[i].sFileSystem,0,sizeof(rMountedPartition[i].sFileSystem));
		s32TotalPartitionsExported--;
		sem_post(NMS_lock);
	}
}

void nms_partition_unmounted(const partition_info_t *partition_info, const device_info_t *device_info)
{	
	(void)device_info;
	NMS_DEBUG("%s",partition_info->identifier);
	vNmsHandleUnmount(partition_info);
}

void nms_partition_invalid(const partition_info_t *partition_info, const device_info_t *device_info)
{	
	(void)device_info;
	NMS_DEBUG("%s",partition_info->identifier);
	vNmsHandleUnmount(partition_info);
}

void nms_update_device_info(const device_info_t *device_info, int request_id)
{
	(void)request_id;
	(void)device_info;
	NMS_DEBUG(" ");
}

void nms_update_partition_info(const partition_info_t *partition_info,const device_info_t *device_info, int request_id)
{
	(void)request_id;
	(void)device_info;
	NMS_DEBUG(" ");
	vNmsHandleMount(partition_info);
}

void nms_snapshot_complete(int request_id)
{
	(void)request_id;
	NMS_DEBUG("reqid -> %d ", request_id);
}

static automounter_api_callbacks_t  nms_rFunction =
{
	.on_establish_connection_success = nms_establish_connection_success,
	.on_establish_connection_failure = nms_establish_connection_failure,
	.on_connection_lost              = nms_connection_lost,
	.on_device_detected              = nms_device_detected,
	.on_device_nomedia               = nms_device_nomedia,
	.on_device_automounted           = nms_device_automounted,
	.on_device_unmounted             = nms_device_unmounted,
	.on_device_invalid               = nms_device_invalid,
	.on_partition_detected           = nms_partition_detected,
	.on_partition_unsupported        = nms_partition_unsupported,
	.on_partition_mounted            = nms_partition_mounted,
	.on_partition_mount_err          = nms_partition_mount_err,
	.on_partition_remounting         = nms_partition_remounting,
	.on_partition_unmounted          = nms_partition_unmounted,
	.on_partition_invalid            = nms_partition_invalid,
	.on_update_device_info           = nms_update_device_info,
	.on_update_partition_info        = nms_update_partition_info,
	.on_snapshot_complete            = nms_snapshot_complete,
};


static void vNmsAmHandleConnection(void)
{
	NMS_lock = sem_open("NMS_LOCK", O_CREAT, NMS_SEM_ACCESS, 0);
	if (NMS_lock != SEM_FAILED)
	{
		int am_fd = automounter_api_get_pollfd();
		struct pollfd poll_fd = {.fd = am_fd, .events = POLLIN,.revents = POLLOUT};
		if (-1 != am_fd)
		{
			sem_post(NMS_lock);
			while (TRUE)
			{
				int rc = poll(&poll_fd, 1, -1);
				if (rc > 0)
				{
					if (poll_fd.revents == POLLIN)
					{
						NMS_DEBUG("automounter_api_dispatch_event");
						automounter_api_dispatch_event();
					}
					else
					{
						NMS_DEBUG("automounter: unexpected event");
					}
				}
				else
				{
					if (EINTR != errno)
					{
						NMS_DEBUG("automounter: poll failed");
					}
				}
			}
		}
		else
		{
			NMS_DEBUG("error automounter_api_get_pollfd -> %d",am_fd);
		}
	}
	else
	{
		NMS_CRITICAL("error creating semaphore NMS_LOCK %d ",errno);
	}
}

static void vNmsAmConnect(void)
{
	int error = automounter_api_init("NMS", LOGGER_LEVEL_ERROR, (bool)true);

	NMS_DEBUG(" %s",automounter_api_get_version_string());

	if (RESULT_OK != error)
	{
		NMS_CRITICAL("error automounter_api_init -> %d", error);
	}
	else
	{
		automounter_api_register_callbacks(&nms_rFunction);
		error = automounter_api_try_connect();
		if ((error == RESULT_OK)
		||
		(error == RESULT_DAEMON_NOT_RUNNING))
		{
			vNmsAmHandleConnection();
			automounter_api_disconnect();
		}
		else
		{
			NMS_CRITICAL("error automounter_api_try_connect -> %d ",error);
		}
		automounter_api_deinit();
	}
	NMS_DEBUG("vNmsAmConnect end");
}

void vNmsAmThreadCreate(void)
{
	pthread_t tid;
	int err;

	/* create a thread for automounter communication */
	err = pthread_create(&tid, NULL, (ThreadEntry)vNmsAmConnect, NULL);
	if (err != 0)
	{
		NMS_CRITICAL("cant create NMS-AM-thread ");
		exit(EXIT_FAILURE);
	}
	else
	{
		pthread_setname_np(tid,"NMS-AM-thread");
		NMS_DEBUG("NMS-AM-thread created ");
	}

}

int main()
{
	/* register and start dbus service */
	nfs_media_share_dbus_service_start();

	return 1;
}
