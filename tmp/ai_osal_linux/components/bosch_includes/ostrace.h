/************************************************************************
| FILE:         osaltrace.h
| PROJECT:      BMW_L6
| SW-COMPONENT: OSAL_CORE
|------------------------------------------------------------------------
| DESCRIPTION:  Definitions and prototypes for OSAL traces
|------------------------------------------------------------------------
| COPYRIGHT:    (c) 2005 Blaupunkt GmbH
| HISTORY:
| Date                 | Modification            | Author
| 03.10.05  | Initial revision           | MRK2HI
| 31.03.09  | Added new trace class for  | JEV1KOR, RBEI/ECF1
|           | LFS_Test module.           |
| --.--.--  | ----------------           | -------, -----
| 27.10.09  | Added new trace class for  | anc3kor, RBEI/ECF1
|           | dev_illumination module.   |
| --.--.--  | ----------------           | -------, -----
| --.--.--  | ----------------           | -------, -----
| 11.10.12  |Added new trace class for   | nro2kor, RBEI/ECF5
|           | dev_AcousticSRC module.    |
| --.--.--  | ----------------           | -------, -----

|************************************************************************/
#ifndef LLD_TRACE_HEADER
#define LLD_TRACE_HEADER


#ifdef __cplusplus
extern"C"{
#endif

#ifndef LOBYTE
#define LOBYTE( wValue )    ((tU8)( wValue ))
#endif /*#ifndef LOBYTE*/

#ifndef HIBYTE
#define HIBYTE( wValue )    ((tU8)((tU16)( wValue ) >> 8))
#endif /*#ifndef HIBYTE*/

#ifndef LOWORD
#define LOWORD( dwValue )   ((tU16)(tU32)( dwValue ))
#endif /*#ifndef LOWORD*/

#ifndef HIWORD
#define HIWORD( dwValue )   ((tU16)((((tU32)( dwValue )) >> 16) & 0xFFFF))
#endif /*HIWORD*/

#ifndef LODWORD
#define LODWORD( ddwValue )   ((tU32)(tU64)(ddwValue ))
#endif /*#ifndef LODWORD*/

#ifndef HIDWORD
#define HIDWORD(ddwValue) ((tU32)((((tU64)( ddwValue))>>32)&0xFFFFFFFF))
//#define HIDWORD(ddwValue) ((tU32)(((*(tU64*)(ddwValue))).hi))
#endif

#ifndef MAX_TRACE_SIZE
#define MAX_TRACE_SIZE 256
#endif


#ifndef TR_CLASS_EXCEPTION
#define TR_CLASS_EXCEPTION    ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 199)) 
#endif
#ifndef TR_CLASS_FD_CRYPT
#define TR_CLASS_FD_CRYPT             ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 220))
#endif
#ifndef TR_TTFIS_LINUX_OSALIO
#define TR_TTFIS_LINUX_OSALIO     ((TR_tenTraceChan)127)
#endif

#ifndef TR_TTFIS_LINUX_OSALCORE
#define TR_TTFIS_LINUX_OSALCORE     ((TR_tenTraceChan)126)
#endif


/************************************************************************
 * Macros to fill trace buffers                                         *
 ************************************************************************/
#define OSAL_M_INSERT_T8(buf, Byte) \
    (buf)[0] = (tU8) ((Byte) & 0xFF);

#define OSAL_M_INSERT_T16(buf, Word) \
    (buf)[0] = (tU8) (((Word) >> 8) & 0xFF); \
    (buf)[1] = (tU8) (((Word)     ) & 0xFF);

#define OSAL_M_INSERT_T32(buf, Long) \
    (buf)[0] = (tU8) (((Long) >> 24) & 0xFF); \
    (buf)[1] = (tU8) (((Long) >> 16) & 0xFF); \
    (buf)[2] = (tU8) (((Long) >>  8) & 0xFF); \
    (buf)[3] = (tU8) (((Long)      ) & 0xFF);

#define OSAL_M_INSERT_T64(buf,tU64 ) \
   (buf)[0] = ((tU64 ) >> 56) & 0xFF; \
   (buf)[1] = ((tU64 ) >> 48) & 0xFF; \
   (buf)[2] = ((tU64 ) >> 40) & 0xFF; \
   (buf)[3] = ((tU64 ) >> 32) & 0xFF; \
   (buf)[4] = ((tU64 ) >> 24) & 0xFF; \
   (buf)[5] = ((tU64 ) >> 16) & 0xFF; \
   (buf)[6] = ((tU64 ) >>  8) & 0xFF; \
   (buf)[7] = ((tU64 )      ) & 0xFF;

/************************************************************************
 * Trace classes                                                        *
 ************************************************************************/

/* The following trace components exist for OSAL:
 * - TR_COMP_OSALCORE for OSAL core traces
 * - TR_COMP_OSALIO for traces of OSAL devices and low-level devices
 * - TR_COMP_OSALTEST for traces of the OSAL test applications
 */

/* Trace classes for TR_COMP_OSALCORE */
#define OSAL_C_TR_CLASS_SYS_CNTRL      (TR_COMP_OSALCORE +  1)
#define OSAL_C_TR_CLASS_SYS_ERROR      (TR_COMP_OSALCORE +  2)
#define OSAL_C_TR_CLASS_SYS_MEM        (TR_COMP_OSALCORE +  3)
#define OSAL_C_TR_CLASS_SYS_PERF       (TR_COMP_OSALCORE +  4)
#define OSAL_C_TR_CLASS_SYS_MEMPOOL    (TR_COMP_OSALCORE +  5)
#define OSAL_C_TR_CLASS_SYS_SEM        (TR_COMP_OSALCORE +  6)
#define OSAL_C_TR_CLASS_SYS_MTX        (TR_COMP_OSALCORE +  7)
#define OSAL_C_TR_CLASS_SYS_FLG        (TR_COMP_OSALCORE +  8)
#define OSAL_C_TR_CLASS_SYS_MQ         (TR_COMP_OSALCORE +  9)
#define OSAL_C_TR_CLASS_SYS_SM         (TR_COMP_OSALCORE + 10)
#define OSAL_C_TR_CLASS_SYS_TIM        (TR_COMP_OSALCORE + 11)
#define OSAL_C_TR_CLASS_SYS_MSGPOOL    (TR_COMP_OSALCORE + 12)
#define OSAL_C_TR_CLASS_SYS_PRC        (TR_COMP_OSALCORE + 13)

/* Trace classes for TR_COMP_OSALIO */
#define OSAL_C_TR_CLASS_LLD_BPCD        (TR_COMP_OSALIO +  1)
#define OSAL_C_TR_CLASS_DEV_CDAUDIO     (TR_COMP_OSALIO +  2)
#define OSAL_C_TR_CLASS_DEV_CDCTRL      (TR_COMP_OSALIO +  3)
#define OSAL_C_TR_CLASS_LLD_CDFS        (TR_COMP_OSALIO +  4)
#define OSAL_C_TR_CLASS_DEV_CDROM       (TR_COMP_OSALIO +  5)
#define OSAL_C_TR_CLASS_DEV_CACHEDCDFS  (TR_COMP_OSALIO +  6)
#define OSAL_C_TR_CLASS_LLD_I2C         (TR_COMP_OSALIO +  7)
#define OSAL_C_TR_CLASS_DEV_MP3         (TR_COMP_OSALIO +  8)
#define OSAL_C_TR_CLASS_DEV_SPEECHDATA  (TR_COMP_OSALIO +  9)
#define OSAL_C_TR_CLASS_DEV_FFS         (TR_COMP_OSALIO + 10)
#define OSAL_C_TR_CLASS_DEV_SPI_ALL     (TR_COMP_OSALIO + 11)
#define OSAL_C_TR_CLASS_DEV_SPI0        (TR_COMP_OSALIO + 12)
#define OSAL_C_TR_CLASS_DEV_SPI1        (TR_COMP_OSALIO + 13)
#define OSAL_C_TR_CLASS_LLD_SPI0        (TR_COMP_OSALIO + 14)
#define OSAL_C_TR_CLASS_LLD_SPI1        (TR_COMP_OSALIO + 15)
#define OSAL_C_TR_CLASS_DEV_KDS         (TR_COMP_OSALIO + 16)
#define OSAL_C_TR_CLASS_DEV_ERRMEM      (TR_COMP_OSALIO + 17)
#define OSAL_C_TR_CLASS_DEV_FSCK        (TR_COMP_OSALIO + 18)
#define OSAL_C_TR_CLASS_DEV_RAMDISK     (TR_COMP_OSALIO + 19)
#define OSAL_C_TR_CLASS_DEV_CARD        (TR_COMP_OSALIO + 20)
#define OSAL_C_TR_CLASS_LLD_ATAPICDVD   (TR_COMP_OSALIO + 21)
#define OSAL_C_TR_CLASS_DEV_GYRO        (TR_COMP_OSALIO + 22)
#define OSAL_C_TR_CLASS_DEV_GPS         (TR_COMP_OSALIO + 23)
#define OSAL_C_TR_CLASS_DEV_ODO         (TR_COMP_OSALIO + 24)
#define OSAL_C_TR_CLASS_DRV_IPN         (TR_COMP_OSALIO + 25)
#define OSAL_C_TR_CLASS_DEV_ACOUSTICIN  (TR_COMP_OSALIO + 31)
#define OSAL_C_TR_CLASS_DEV_ACOUSTICOUT (TR_COMP_OSALIO + 32)
#define OSAL_C_TR_CLASS_DRV_FLASH       (TR_COMP_OSALIO + 41)
#define OSAL_C_TR_CLASS_DRV_REGISTRY    (TR_COMP_OSALIO + 45)
#define OSAL_C_TR_CLASS_DRV_USBF        (TR_COMP_OSALIO + 50)
#define OSAL_C_TR_CLASS_DRV_MOST        (TR_COMP_OSALIO + 57)
#define OSAL_C_TR_CLASS_DRV_TIMER       (TR_COMP_OSALIO + 80)
#define OSAL_C_TR_CLASS_DRV_PWM         (TR_COMP_OSALIO + 81)
#define OSAL_C_TR_CLASS_DRV_CHENC       (TR_COMP_OSALIO + 82)
#define OSAL_C_TR_CLASS_ASYNC_IO        (TR_COMP_OSALIO + 92)
#define OSAL_C_TR_CLASS_DEV_TOUCHSCREEN (TR_COMP_OSALIO + 93)
#define OSAL_C_TR_CLASS_DEV_RTC         (TR_COMP_OSALIO + 99)
#define OSAL_C_TR_CLASS_APP_POWERMASTER (TR_COMP_OSALIO + 100)
#define OSAL_C_TR_CLASS_DEV_SWC         (TR_COMP_OSALIO + 101)
#define OSAL_C_TR_CLASS_DEV_SADC        (TR_COMP_OSALIO + 102)
#define OSAL_C_TR_CLASS_FD_CRYPT        (TR_COMP_OSALIO + 103)
#define OSAL_C_TR_CLASS_DRV_SDCARD      (TR_COMP_OSALIO + 104)
#define OSAL_C_TR_CLASS_PRM             (TR_COMP_OSALIO + 105)
#define OSAL_C_TR_CLASS_DEV_UART        (TR_COMP_OSALIO + 106)
#define OSAL_C_TR_CLASS_DEV_BT          (TR_COMP_OSALIO + 107)
#define OSAL_C_TR_CLASS_DEV_I2C         (TR_COMP_OSALIO + 108)
#define OSAL_C_TR_CLASS_DRV_ILLUMINATION (TR_COMP_OSALIO + 109)
#define OSAL_C_TR_CLASS_DEV_ADAS        (TR_COMP_OSALIO + 110)
#define OSAL_C_TR_CLASS_DEV_ACC         (TR_COMP_OSALIO + 111)
#define OSAL_C_TR_CLASS_DEV_GPIO        (TR_COMP_OSALIO + 112)
#define OSAL_C_TR_CLASS_LLD_TOUCH       (TR_COMP_OSALIO + 113)
#define OSAL_C_TR_CLASS_DEV_MKP         (TR_COMP_OSALIO + 114)
#define OSAL_C_TR_CLASS_DEV_ROTENCODER  (TR_COMP_OSALIO + 115)
#define OSAL_C_TR_CLASS_DEV_ACOUSTICIN_LINUX    (TR_COMP_OSALIO + 116)
#define OSAL_C_TR_CLASS_DEV_ACOUSTICOUT_LINUX   (TR_COMP_OSALIO + 117)
#define OSAL_C_TR_CLASS_FD_DEVICE_CTRL  (TR_COMP_OSALIO + 118)
#define OSAL_C_TR_CLASS_DEV_WUP         (TR_COMP_OSALIO + 119)
#define OSAL_C_TR_CLASS_DEV_REGISTRY    (TR_COMP_OSALIO + 120)
#define OSAL_C_TR_CLASS_DEV_PWM_IN		(TR_COMP_OSALIO + 121)
#define OSAL_C_TR_CLASS_DEV_ACOUSTICSRC_LINUX		(TR_COMP_OSALIO + 122)
#define OSAL_C_TR_CLASS_LSIM_NET_DATA_DISP (TR_COMP_OSALIO + 123)
#define OSAL_C_TR_CLASS_SENSOR_DISPATCHER (TR_COMP_OSALIO + 124)
#define OSAL_C_TR_CLASS_SENSOR_RINGBUFFER (TR_COMP_OSALIO + 125)
#define OSAL_C_TR_CLASS_DEV_GNSS         (TR_COMP_OSALIO + 126)
#define OSAL_C_TR_CLASS_DEV_ADR3CTRL     (TR_COMP_OSALIO + 127)
#define OSAL_C_TR_CLASS_DEV_ACOUSTICECNR (TR_COMP_OSALIO + 128)
#define OSAL_C_TR_CLASS_DEV_ABS  (TR_COMP_OSALIO + 129)
#define OSAL_C_TR_CLASS_DEV_USBPOWER     (TR_COMP_OSALIO + 130)

/* Trace classes for TR_COMP_OSALTEST */

/************************************************************************
 * Error memory identifiers                                             *
 ************************************************************************/

/* Since error memory codes for a component are based on its trace
 * component, the OSAL error memory codes are defined here:
 * - TR_COMP_OSALCORE for OSAL core errors
 * - TR_COMP_OSALIO for errors of OSAL devices and low-level devices
 */


/************************************************************************
 * Function prototypes                                                  *
 ************************************************************************/

/* NOTE: these functions have to be called from a thread context! */
extern tBool LLD_bOpenTrace(tVoid);
extern tVoid LLD_vCloseTrace(tVoid);
extern tBool LLD_bIsTraceActive(tU32 u32Class, tU32 u32Level);
extern tVoid LLD_vTrace(tU32 u32Class, tU32 u32Level, const void* pvData, tU32 u32Length);


#ifdef __cplusplus
}
#endif


#else
#error ostrace.h included several times
#endif
