/************************************************************************
 * \file: mc_dlt.h
 *
 * \version: $Id: mc_dlt.h$
 *
 * This header file declares constants for dlt
 *
 * \author: Vijaya Raja.J ,
 *          Sundhar.A
 *
 * \copyright: (c) 2013 - 2014 RBCM, Hildesheim, Germany
 *
 * \history
 * 28-May-2014  J VijayaRaja Initial Version
 ***********************************************************************/
#ifndef MC_DLT_H 
#define MC_DLT_H

/**************************************************************************
 * DLT  Application IDs
 *************************************************************************
 **
 * Definitions for MC DLT APID
 *
 * An Application ID is specified as a 4 Bytes Alphanumeric character with
 * and assigned as follows:
 *   +------------------+---------------+--------------+----------------+
 *   | Byte 1           | Byte 2        | Byte 3       |  Byte 4        |
 *   +------------------+---------------+--------------+----------------+
 *  ["Stakeholder- 
 *          Identifier"][  Component Specific ID                        ]
 *  [       "C"        ][ Each byte value "0-9","A-Z"                   ]
 *
 * The APPID shall be assigned based on the following criteria:
 *   1.	Differentiate components based on if it belongs to ADIT, RBCM or DENSO.
 *   2.	No consideration of domain (node) on which the component is running.
 *   3.	Components are assigned numeric values instead of short names.
 *
 * Based on the criteria above, an Application ID shall be assigned as follows:
 *
 * 1st Byte  A for ADIT specific Components
 *            C for RBCM Linux
 * 	          V for RBCM V850
 *            D for DENSO
 * 2nd, 3rd and 4th Bytes  These will be alphanumeric characters (0 -9 to A-Z).
 *
 *   Please comment each update with the following template:
 *  <Application path> <Application ShortName>, <date of entry>
 */
 
/*Start - <20.08.2015>,  jov1kor, Below APPID's are requested by Loga Stefan (CM-AI/EPD3) <Stefan.Loga@de.bosch.com>*/
#define DLT_APPID_UI_LOGIC_00           LL00      /* Tuner application */
#define DLT_APPID_UI_LOGIC_01           LL01      /* Climate Application */
#define DLT_APPID_UI_LOGIC_02           LL02      /* Apps/MirrorLink */
#define DLT_APPID_UI_LOGIC_03           LL03      /* Audio */
#define DLT_APPID_UI_LOGIC_04           LL04      /* Common */
#define DLT_APPID_UI_LOGIC_05           LL05      /* Connectivity */
#define DLT_APPID_UI_LOGIC_06           LL06      /* Custom */
#define DLT_APPID_UI_LOGIC_07           LL07      /* Diagnosis */
#define DLT_APPID_UI_LOGIC_08           LL08      /* Drive */
#define DLT_APPID_UI_LOGIC_09           LL09      /* Hybrid */
#define DLT_APPID_UI_LOGIC_10           LL10      /* Internet */
#define DLT_APPID_UI_LOGIC_11           LL11      /* Media */
#define DLT_APPID_UI_LOGIC_12           LL12      /* MultiDrive */
#define DLT_APPID_UI_LOGIC_13           LL13      /* OnOff */
#define DLT_APPID_UI_LOGIC_14           LL14      /* Phone */
#define DLT_APPID_UI_LOGIC_15           LL15      /* Picture */
#define DLT_APPID_UI_LOGIC_16           LL16      /* Settings */
#define DLT_APPID_UI_LOGIC_17           LL17      /* SpeechReco */
#define DLT_APPID_UI_LOGIC_18           LL18      /* Update */


#define DLT_APPID_UI_FRAMEWORK_00       UF00      /* Internal messaging */
#define DLT_APPID_UI_FRAMEWORK_01       UF01      /* GLIB framework */
#define DLT_APPID_UI_FRAMEWORK_02       UF02      /* UI server */
#define DLT_APPID_UI_FRAMEWORK_03       UF03      /* Artisan model */
#define DLT_APPID_UI_FRAMEWORK_04       UF04      /* DBus communication */
#define DLT_APPID_UI_FRAMEWORK_05       UF05      /* Common framework */

#define DLT_APPID_APP_INFRA_00          LAI0      /* Reserved for Application Infrastructure */
#define DLT_APPID_APP_INFRA_01          LAI1      /* Reserved for Application Infrastructure */
#define DLT_APPID_APP_INFRA_02          LAI2      /* Reserved for Application Infrastructure */
#define DLT_APPID_APP_INFRA_03          LAI3      /* Reserved for Application Infrastructure */

#define DLT_APPID_APP_WPV_00            "WPV0"      /* Reserved for Web Portal Viewer application - PSARCC-13643 */
#define DLT_EXT_THIRDPARTY_NAVI_APP     "EXTN"      /* For External third party navigation - Requested by MAO Neil (CM/ESW21-CN) */

/*End - <06.12.2016>,  haa7kor */

 /**************************************************************************
 * DLT  Non-Verbose Message IDs
 *************************************************************************
 *
 * Message ID is the ID to characterize the information, which is transported by the message itself. 
 * A message ID identifies a log or trace message uniquely. It can be used for identifying the source of
 * a message end and it can be used for characterizing the payload of a message.It is a unique 4 byte 
 * message ID is sent along with the payload.  
 *
 * Total available range for Message ID is from 0x00000000 to 0xFFFFFFFF(This is huge range of 4G uniqe Message IDs). 
 *
 *   +-------------------+-------------------+
 *   | Message ID Range  | StakeHolder       |
 *   +-------------------+-------------------+
 *   | 0x00000000 -      |     ADIT          |
 *   |     0x3FFFFFFF    |                   |
 *   +-------------------+-------------------+
 *   | 0x40000000 -      |     DENSO         |
 *   |    0x7FFFFFFF     |                   |
 *   +-------------------+-------------------+
 *   | 0x80000000 -      |     RBCM          |
 *   |    0xBFFFFFFF     |                   |
 *   +-------------------+-------------------+
 *   | 0xC0000000 -      |     V850          |
 *   |    0xFFFFFFFF     |                   |
 *   +-------------------+-------------------+
 *
 *
*/

#endif /* MC_DLT_H */
