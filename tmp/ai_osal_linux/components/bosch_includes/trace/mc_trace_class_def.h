/************************************************************************
 * file: mc_trace_classes.h
 *
 * version: $Id: mc_trace_classes.h $
 *
 * This header file contains class definitions for trace.
 * These class definitions have to be moved to the component specific files.
 * As a temporary solution these classes are moved from mc_trace.h file.
 * Please do not add any new classes in this file.
 *
 * author: Dhanasekaran.D
 *
 * copyright: (c) 2010 RBCM, Hildesheim, Germany.
 *
 * history
 * 12-Feb-2010  Dhanasekaran.D Initial Version
 ***********************************************************************/


#ifndef __BOSCH_TRACE_CLASSES_H
#define __BOSCH_TRACE_CLASSES_H

#define TR_FIRST_COMPONENT_CLASS_ID 			(24)

/*  component-internal class defenitions  for TR_COMP_SYSTEM  */ 
#define TR_CLASS_DC_DIAG						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 0))
#define TR_CLASS_DOWNLOADER						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 1)) 
#define	TR_CLASS_RTC							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 2))
#define TR_CLASS_KMT							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 3)) 
/*#define   TR_CLASS_AIL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 4))   */ 
#define TR_CLASS_DVD_CACHE						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 5)) 
#define TR_CLASS_DVDTEST						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 6)) 
#define TR_CLASS_SYSMANAGER						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 7)) 
#define TR_CLASS_LPM1							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 8)) 
#define TR_CLASS_LPM2							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 9)) 
#define TR_CLASS_LPM3							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 10)) 
#define TR_CLASS_LPM4							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 11)) 
#define TR_CLASS_EAC_SENSOR_SIGNALS             ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 12)) 
#define TR_CLASS_EAC_GPS_RECEIVER               ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 13)) 
#define TR_CLASS_DIAGNOSIS_VD_BASE              ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 14)) 
#define TR_CLASS_DIAGNOSIS_VD_CMDI              ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 15)) 
#define TR_CLASS_DIAGNOSIS_VD_KWP2000           ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 16)) 
#define TR_CLASS_DIAGNOSIS_VD_MCNET             ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 17)) 
#define TR_CLASS_MACCH							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 18)) 
#define TR_CLASS_MACCH_DRV						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 19))
#define TR_CLASS_NAVI							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 20)) 
#define TR_CLASS_VD_AMPLIFY						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 21)) 
#define TR_CLASS_PROXY_APP						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 22)) 
#define TR_CLASS_PROXY_SYS						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 23)) 
#define TR_CLASS_PRM							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 24)) 
#define TR_CLASS_FANCONTROL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 25)) 
#define TR_CLASS_FWL							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 26)) 
#define TR_CLASS_VD_TUNER						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 27)) 
#define TR_CLASS_CE_TAccH						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 28)) 
#define TR_CLASS_CE_TConnect					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 29)) 
#define TR_CLASS_NT_TAccH						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 30)) 
#define TR_CLASS_NT_TConnect					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 31)) 
#define TR_CLASS_VD_AMFM_TUN					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 32)) 
#define TR_CLASS_VD_TMC_TUN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 33)) 
#define TR_CLASS_VD_CAN							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 34)) 
#define TR_CLASS_DIAGNOSIS_RPM					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 35)) 
#define TR_CLASS_FC_SELFTEST					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 36)) 
#define TR_CLASS_FC_COM2CCA						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 37)) 
/*   #define     TR_CLASS_NAV_MAPMATCHING       ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 38))   */ 
/*   #define     TR_CLASS_NAV_PRESENTATION      ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 39))   */ 
/*   #define     TR_CLASS_NAV_DEADREC			((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 40))   */ 
/*   #define     TR_CLASS_NAV_LDB				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 41))   */ 
/*   #define TR_CLASS_NAV_RG					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 42))   */ 
#define TR_CLASS_NAV_SPEECH						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 43)) 
/*  #define      TR_CLASS_NAV_EDM				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 44))   */ 
#define TR_CLASS_NAV_RCALC						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 45)) 
/*   #define     TR_CLASS_NAV_NAVISTATEMANAGER  ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 46))   */ 
/*   #define     TR_CLASS_NAV_MDB				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 47))   */ 
/*   #define     TR_CLASS_NAV_MADS				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 48))   */ 
#define TR_CLASS_NAV_NAVAPI						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 49)) 
/*   #define     TR_CLASS_NAV_LI_MESSAGES		((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 50))   */ 
/*   #define     TR_CLASS_NAV_LI_STATES			((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 51))   */ 
/*   #define     TR_CLASS_DAPI_DEVMAN			((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 52))   */ 
/*   #define     TR_CLASS_DAPI_LISA				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 53))   */ 
/*   #define     TR_CLASS_DAPI_SBS				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 54))   */ 
/*   #define     TR_CLASS_DAPI_RAWDATA			((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 55))   */ 
/*   #define     TR_CLASS_DAPI_RESINFO			((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 56))   */ 
/*   #define     TR_CLASS_DAPI_COMM				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 57))   */ 
/*   #define     TR_CLASS_DAPI_FRAMEWORK		((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 58))   */ 
#define TR_CLASS_VD_SENSOR						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 59)) 
#define TR_CLASS_VD_TMC							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 60)) 
#define TR_CLASS_VD_VOICE						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 61)) 
/*   #define 		 TR_CLASS_TIMA				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 62))   */ 
#define TR_CLASS_VD_TELEFON						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 63)) 
#define TR_CLASS_VD_TIM							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 64)) 
#define TR_CLASS_VD_TVTUNER						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 65)) 
#define TR_CLASS_VD_TCS							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 66)) 
#define TR_CLASS_VD_EXTMUTE						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 67)) 
#define TR_CLASS_CONFIG_DB						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 68)) 
#define TR_CLASS_COMP_MANAGER					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 69)) 
#define TR_CLASS_LOC_DESC						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 70)) 
#define TR_CLASS_ATLAS_CLASS					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 71)) 
#define TR_CLASS_CENTRAL_ATLAS					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 72)) 
#define TR_CLASS_CENTRAL_ROUTE					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 73)) 
#define TR_CLASS_ROUTE_CLASS					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 74)) 
#define TR_CLASS_ROUTE_CONTAINER				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 75)) 
#define TR_CLASS_ROUTE_GUIDANCE					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 76)) 
#define TR_CLASS_MAP_CONTAINER					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 77)) 
#define TR_CLASS_CACHE_CONTROL					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 78)) 
#define TR_CLASS_CACHE_TRANSFORM				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 79)) 
#define TR_CLASS_DESTINPUT_CONTEXT              ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 80)) 
#define TR_CLASS_NAVIGROP_CONTEXT               ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 81)) 
#define TR_CLASS_RG_CONTEXT						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 82)) 
#define TR_CLASS_AUDIO_CTL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 83)) 
#define TR_CLASS_ROUTEOP_CTL					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 84)) 
#define TR_CLASS_DYNRG_CTL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 85)) 
#define TR_CLASS_CALIB_CTL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 86)) 
#define TR_CLASS_WAYB_CTL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 87)) 
#define TR_CLASS_GOS_CTL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 88)) 
#define TR_CLASS_CBBSKIN_CLASS					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 89)) 
#define TR_CLASS_AUTORBD_TCTL					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 90)) 
#define TR_CLASS_AUTO_EFELDSKIN					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 91)) 
#define TR_CLASS_AUTO_FRAMECTL					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 92)) 
#define TR_CLASS_FCS_CLASS						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 93)) 
#define TR_CLASS_AUTO_RBCBB						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 94)) 
#define TR_CLASS_DTSS_CLASS						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 95)) 
#define TR_CLASS_AUTO_FUNCWHEEL					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 96)) 
#define TR_CLASS_FWS_CLASS						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 97)) 
#define TR_CLASS_LBS_CLASS						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 98)) 
#define TR_CLASS_RB_LBOX						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 99)) 
#define TR_CLASS_MS_CLASS						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 100)) 
#define TR_CLASS_SK_CLASS						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 101)) 
#define TR_CLASS_RB_SPELL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 102)) 
#define TR_CLASS_SPELL_SKIN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 103)) 
#define TR_CLASS_BS_CLASS						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 104)) 
#define TR_CLASS_SDS_PROXY						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 105)) 
#define TR_CLASS_SDS_MGR						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 106)) 
#define TR_CLASS_SDS_CONTR						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 107)) 
#define TR_CLASS_SDS_HIFPROMPT					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 108)) 
#define TR_CLASS_SDS_HIFRECOG					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 109)) 
#define TR_CLASS_SDS_CONTEXT					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 110)) 
#define TR_CLASS_CENT_ARB						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 111)) 
#define TR_CLASS_TRANS_CTL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 112)) 
#define TR_CLASS_AUDIO_SWITCH					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 113)) 
#define TR_CLASS_ARBT_ACCESS					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 114)) 
#define TR_CLASS_AUDIO_RESTARTER				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 115)) 
#define TR_CLASS_VD_AUCONNECT					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 116)) 
#define TR_CLASS_AUSKIN_PROFILE					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 117)) 
#define TR_CLASS_AUDISCPLAY_DVD1				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 118)) 
#define TR_CLASS_AUDISCPLAY_MAG1				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 119)) 
#define TR_CLASS_FCAUDISC_PLAYER				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 120)) 
#define TR_CLASS_CENT_AUDISCPLAY				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 121)) 
#define TR_CLASS_MP3PLAY_DVD1					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 122)) 
#define TR_CLASS_MP3PLAY_MMC1					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 123)) 
#define TR_CLASS_FC_MP3PLAY						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 124)) 
#define TR_CLASS_FC_CENTMP3PLAYER				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 125)) 
#define TR_CLASS_EVENTLOG						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 126)) 
#define TR_CLASS_SUPERVISOR						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 127)) 
#define TR_CLASS_COPRO							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 128)) 
#define TR_CLASS_DRV_DSP						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 129)) 
#define TR_CLASS_DRV_IPN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 130)) 
#define TR_CLASS_DRV_SPIHWL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 131)) 
#define TR_CLASS_DRV_MOST						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 132)) 
/*   #define   TR_CLASS_DRV_CAN					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 133))   */ 
/*   #define   TR_CLASS_DRV_CANSUP				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 134))   */ 
/*   #define   TR_CLASS_DRV_CANBUKA				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 135))   */ 
#define TR_CLASS_DRV_USBUHCI					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 136)) 
#define TR_CLASS_DRV_USBHID						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 137)) 
#define TR_CLASS_DRV_BLUETOOTH					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 138)) 
#define TR_CLASS_DRV_NE2000						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 139)) 
#define TR_CLASS_DRV_AMD970PCI					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 140)) 
#define TR_CLASS_DRV_NORTRUEFFS					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 141)) 
#define TR_CLASS_DRV_UNANDTRUEFFS				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 142)) 
#define TR_CLASS_DRV_NORFLASHFX					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 143)) 
#define TR_CLASS_DRV_UNANDFLASHFX				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 144)) 
#define TR_CLASS_DRV_PRM						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 145)) 
#define TR_CLASS_DRV_RCD						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 146)) 
#define TR_CLASS_DRV_RCCCMN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 147)) 
#define TR_CLASS_DRV_DIRPM						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 148)) 
#define TR_CLASS_DRV_DDLCD						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 149)) 
#define TR_CLASS_DRV_DDVAG						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 150)) 
#define TR_CLASS_DRV_XRAVINNULLDISP             ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 151)) 
#define TR_CLASS_DRV_AUXCLOCK					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 152)) 
#define TR_CLASS_DRV_GYRO						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 153)) 
#define TR_CLASS_DRV_OVC						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 154)) 
#define TR_CLASS_DRV_ODOMETER					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 155)) 
#define TR_CLASS_DRV_ATAPICACHED				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 156)) 
#define TR_CLASS_DRV_ATAPIUNCACHED              ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 157)) 
#define TR_CLASS_DRV_ENCODER					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 158)) 
#define TR_CLASS_DRV_KBDGER						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 159)) 
#define TR_CLASS_DRV_SERIALSIU					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 160)) 
#define TR_CLASS_DRV_SYSMGNTIO					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 161)) 
#define TR_CLASS_DRV_OKI98XX					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 162)) 
#define TR_CLASS_DRV_ES1371						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 163)) 
#define TR_CLASS_DRV_EXCEPTIONHANDLER           ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 164)) 
#define TR_CLASS_PLAYGROUND						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 165)) 
#define TR_CLASS_FC_CENTSPEAKER					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 166)) 
#define TR_CLASS_FC_SPEAKERSET					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 167)) 
#define TR_CLASS_CENTEMAIL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 168)) 
#define TR_CLASS_FC_EMAIL						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 169)) 
#define TR_CLASS_SMS							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 170)) 
#define TR_CLASS_TXTPATTERN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 171)) 
#define TR_CLASS_CENTPROFILEADMIN				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 172)) 
#define TR_CLASS_PROFILEADMIN					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 173)) 
#define TR_CLASS_CURRFRONTPROFILE				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 174)) 
#define TR_CLASS_PROFILCONF						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 175)) 
#define TR_CLASS_CURRENTUSER					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 176)) 
#define TR_CLASS_USERADMIN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 177)) 
#define TR_CLASS_FC_KEYLOGIN					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 178)) 
#define TR_CLASS_WAPCORE						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 179)) 
#define TR_CLASS_WAP							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 180)) 
#define TR_CLASS_CENTWAP						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 181)) 
#define TR_CLASS_SDS_HUCONTROLLER				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 182)) 
#define TR_CLASS_SDS_ICCONTROLLER				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 183)) 
#define TR_CLASS_HUSIM							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 184)) 
#define TR_CLASS_HMI_ENGINE						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 185)) 
#define TR_CLASS_DRV_HMI						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 186)) 
#define TR_CLASS_ACCHU_MGR						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 187)) 
#define TR_CLASS_LOCALE_MGR						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 188)) 
#define TR_CLASS_UIBROKER						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 189)) 
#define TR_CLASS_FC_RADIO						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 190)) 
#define TR_CLASS_FC_CENTRALRADIO				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 191)) 
#define TR_CLASS_GDM							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 192)) 
#define TR_CLASS_SDS_SPEECHSESSION              ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 193)) 
#define TR_CLASS_SDS_GDM						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 194)) 
#define TR_CLASS_MAP_PLOTTER					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 195)) 
#define TR_CLASS_MOSTCCAGW						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 196)) 
#define TR_CLASS_UNKNOWN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 197)) 
#define TR_CLASS_DRV_CGI						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 198)) 
#define TR_CLASS_EXCEPTION						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 199)) 
#define TR_CLASS_VD_DIALOG						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 201)) 
#define TR_CLASS_ODI_CSM						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 203))   
#define TR_CLASS_DCA							((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 204))   
#define TR_CLASS_DRV_SPM						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 205)) 
#define TR_CLASS_LLD_CANDESC					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 206)) 
#define TR_CLASS_DEV_DIAGEOL					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 207)) 
#define TR_CLASS_LLD_FFD						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 208)) 
#define TR_CLASS_DRV_SPM_CSM					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 209)) 
#define TR_CLASS_DRV_SPM_VN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 210)) 
#define TR_CLASS_DRV_SPM_CVM					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 211)) 
#define TR_CLASS_DRV_SPM_CGI					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 212)) 
#define TR_CLASS_DEV_FLASHBLOCK                 ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 213)) 
#define  TR_CLASS_DEV_DIAGCUSTOM				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 214)) 
#define TR_CLASS_AMR_WB_DECODER					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 215)) 
/*  #define 	 TR_CLASS_DAPI_TMC				((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 196))   */ 
/*  #define      TR_CLASS_AMT					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 197))   */ 
/*  #define      TR_CLASS_SCD					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 198))   */ 
/*  #define      TR_CLASS_SINA					((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 199))   */ 
/*   #define     TR_CLASS_SINA_ARCHIVESTRATEGY  ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 200))   */ 
#define  TR_CLASS_DRV_SRAM						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 216)) 
#define	 TR_CLASS_LLD_LIN						((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 217))
#define  TR_CLASS_LVDS                       ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 218))
#define TR_CLASS_DEV_DIAGPROD             ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 219))
#define TR_CLASS_FD_CRYPT             ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 220))
#define TR_CLASS_DEV_WUP                        ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 221))
#define TR_CLASS_DEV_VOLT                        ((TR_tenTraceClass)(TR_FIRST_COMPONENT_CLASS_ID + 222))

/* class definitions for component = "GLOBAL"					*/ 
#define TR_CLASS_VERSION					    ((TR_tenTraceClass)(TR_COMP_GLOBAL + 0))	/*  sec2hi, 13.06.03 (new)  */
#define TR_CLASS_POST_MORTEM				    ((TR_tenTraceClass)(TR_COMP_GLOBAL + 1))	/*  sec2hi, 13.06.03 (new)  */ 
#define TR_CLASS_ASSERT                         ((TR_tenTraceClass)(TR_COMP_RBCM_GLOBAL + 2))	/*  mrh, 30.08.05, ram2hi, 30.03.10 */ 

/* class definitions for component = "LIB"						*/
/* #define TR_CLASS_AIL							((TR_tenTraceClass)(TR_COMP_LIB + 0)) */
/* #define TR_CLASS_SCD							((TR_tenTraceClass)(TR_COMP_LIB + 1)) */
/* #define TR_CLASS_AMT							((TR_tenTraceClass)(TR_COMP_LIB + 2)) */
#define TR_CLASS_EXH							((TR_tenTraceClass)(TR_COMP_LIB + 3))
#define TR_CLASS_AHL							((TR_tenTraceClass)(TR_COMP_LIB + 4))

/* class definition for component "TRACE"							*/
#define TR_CLASS_TRIP							((TR_tenTraceClass)(TR_COMP_TRACE + TR_FIRST_COMPONENT_CLASS_ID+ 0))
#define TR_CLASS_TRIP_SESSION					((TR_tenTraceClass)(TR_COMP_TRACE + TR_FIRST_COMPONENT_CLASS_ID+ 1))
#define TR_CLASS_TRACE_MSG_ERRORS				((TR_tenTraceClass)(TR_COMP_TRACE + TR_FIRST_COMPONENT_CLASS_ID+ 2))
#define TR_CLASS_DSP_TRACE						((TR_tenTraceClass)(TR_COMP_TRACE + TR_FIRST_COMPONENT_CLASS_ID+ 3))

/* class definitions for component = "TESTER"					*/
#define TR_CLASS_TESTER							((TR_tenTraceClass)(TR_COMP_TESTER + TR_FIRST_COMPONENT_CLASS_ID+ 0))


/* class definitions for component = "OBN"						*/
#define TR_CLASS_OBN							((TR_tenTraceClass)(TR_COMP_OBN + 0))

/* class definitions for component = "SRVREC" (ServiceRecorder) */
#define TR_CLASS_SRVREC									((TR_tenTraceClass)(TR_COMP_SRVREC + 0))
#define TR_CLASS_SRVREC_DIAGNOSIS						((TR_tenTraceClass)(TR_COMP_SRVREC + 1))
#define TR_CLASS_SRVREC_HANDLE_DIAGNOSIS				((TR_tenTraceClass)(TR_COMP_SRVREC + 2))
#define TR_CLASS_SRVREC_REPORT_MEMORY					((TR_tenTraceClass)(TR_COMP_SRVREC + 3))
#define TR_CLASS_SRVREC_SBS_LISTMATCHING				((TR_tenTraceClass)(TR_COMP_SRVREC + 4))
#define TR_CLASS_SRVREC_DVD_MGNT						((TR_tenTraceClass)(TR_COMP_SRVREC + 5))
#define TR_CLASS_SRVREC_TRAFFIC_MESSAGE_TELEMATIC		((TR_tenTraceClass)(TR_COMP_SRVREC + 6))
#define TR_CLASS_SRVREC_RESOURCE_INFORMATION			((TR_tenTraceClass)(TR_COMP_SRVREC + 7))
#define TR_CLASS_SRVREC_RAW_DATA						((TR_tenTraceClass)(TR_COMP_SRVREC + 8))
#define TR_CLASS_SRVREC_LOCATION_INPUT					((TR_tenTraceClass)(TR_COMP_SRVREC + 9))
#define TR_CLASS_SRVREC_SBS_SUPPORT						((TR_tenTraceClass)(TR_COMP_SRVREC + 10))
#define TR_CLASS_SRVREC_NAVI							((TR_tenTraceClass)(TR_COMP_SRVREC + 11))
#define TR_CLASS_SRVREC_SENSORS							((TR_tenTraceClass)(TR_COMP_SRVREC + 12))
#define TR_CLASS_SRVREC_TRAFFIC_MESSAGE_PRESENTATION	((TR_tenTraceClass)(TR_COMP_SRVREC + 13))
#define TR_CLASS_SRVREC_TRAFFIC_MESSAGE_DATA			((TR_tenTraceClass)(TR_COMP_SRVREC + 14))
#define TR_CLASS_SRVREC_TRAFFIC_MESSAGE_BROADCAST		((TR_tenTraceClass)(TR_COMP_SRVREC + 15))
#define TR_CLASS_SRVREC_VOICE							((TR_tenTraceClass)(TR_COMP_SRVREC + 16))
#define TR_CLASS_SRVREC_TMC_LIST_DATA					((TR_tenTraceClass)(TR_COMP_SRVREC + 17))
#define TR_CLASS_SRVREC_DAPI_ROADNETWORK				((TR_tenTraceClass)(TR_COMP_SRVREC + 18))
#define TR_CLASS_SRVREC_DAPI_ARCHIVE					((TR_tenTraceClass)(TR_COMP_SRVREC + 19))
#define TR_CLASS_SRVREC_RDD								((TR_tenTraceClass)(TR_COMP_SRVREC + 20))
#define TR_CLASS_SRVREC_NAVI_INTERNAL					((TR_tenTraceClass)(TR_COMP_SRVREC + 21))
#define TR_CLASS_SRVREC_ARCHIVE_CONTROL					((TR_tenTraceClass)(TR_COMP_SRVREC + 22))
#define TR_CLASS_SRVREC_LIM								((TR_tenTraceClass)(TR_COMP_SRVREC + 23))
#define TR_CLASS_SRVREC_RC_ENGINEERING					((TR_tenTraceClass)(TR_COMP_SRVREC + 24))
#define TR_CLASS_SRVREC_MCNET_MANAGEMENT				((TR_tenTraceClass)(TR_COMP_SRVREC + 25))
#define TR_CLASS_SRVREC_DESTMEM_MANAGEMENT				((TR_tenTraceClass)(TR_COMP_SRVREC + 26))
#define TR_CLASS_SRVREC_RG_ENGINEERING					((TR_tenTraceClass)(TR_COMP_SRVREC + 27))
#define TR_CLASS_SRVREC_RTF								((TR_tenTraceClass)(TR_COMP_SRVREC + 28))
#define TR_CLASS_SRVREC_POSITION						((TR_tenTraceClass)(TR_COMP_SRVREC + 29))
#define TR_CLASS_SRVREC_GUIDANCE						((TR_tenTraceClass)(TR_COMP_SRVREC + 30))
#define TR_CLASS_SRVREC_SENSORS_IERROR					((TR_tenTraceClass)(TR_COMP_SRVREC + 31))
#define TR_CLASS_SRVREC_OBN								((TR_tenTraceClass)(TR_COMP_SRVREC + 32))
#define TR_CLASS_SRVREC_DAPI_ARCHIVE_INTERN				((TR_tenTraceClass)(TR_COMP_SRVREC + 33))
#define TR_CLASS_SRVREC_MAP_INTERNAL					((TR_tenTraceClass)(TR_COMP_SRVREC + 34))
#define TR_CLASS_SRVREC_EDM_ENGINEERING					((TR_tenTraceClass)(TR_COMP_SRVREC + 35))
#define TR_CLASS_SRVREC_VR_HMI							((TR_tenTraceClass)(TR_COMP_SRVREC + 36))
#define TR_CLASS_SRVREC_FASTMAP							((TR_tenTraceClass)(TR_COMP_SRVREC + 37))
#define TR_CLASS_SRVREC_VD_DIMMING						((TR_tenTraceClass)(TR_COMP_SRVREC + 38))
#define TR_CLASS_SRVREC_TELEFON							((TR_tenTraceClass)(TR_COMP_SRVREC + 39))
#define TR_CLASS_SRVREC_SINA_ENGINEERING				((TR_tenTraceClass)(TR_COMP_SRVREC + 40))
#define TR_CLASS_SRVREC_HEATCTRL						((TR_tenTraceClass)(TR_COMP_SRVREC + 41))
#define TR_CLASS_SRVREC_SECURITY						((TR_tenTraceClass)(TR_COMP_SRVREC + 42))
#define TR_CLASS_SRVREC_ROUTECALCULATION				((TR_tenTraceClass)(TR_COMP_SRVREC + 43))
#define TR_CLASS_SRVREC_EDM_INTERNAL					((TR_tenTraceClass)(TR_COMP_SRVREC + 44))
#define TR_CLASS_SRVREC_TMC_TUNER						((TR_tenTraceClass)(TR_COMP_SRVREC + 45))
#define TR_CLASS_SRVREC_AUXCTRL_EXTBOX					((TR_tenTraceClass)(TR_COMP_SRVREC + 46))
#define TR_CLASS_SRVREC_TEA								((TR_tenTraceClass)(TR_COMP_SRVREC + 47))
#define TR_CLASS_SRVREC_UAM								((TR_tenTraceClass)(TR_COMP_SRVREC + 48))
#define TR_CLASS_SRVREC_WEATHER							((TR_tenTraceClass)(TR_COMP_SRVREC + 49))
#define TR_CLASS_SRVREC_WEATHER_TUNER					((TR_tenTraceClass)(TR_COMP_SRVREC + 50))
#define TR_CLASS_SRVREC_WDB_INTERNAL					((TR_tenTraceClass)(TR_COMP_SRVREC + 51))
#define TR_CLASS_SRVREC_CLIMATE_CONTROL					((TR_tenTraceClass)(TR_COMP_SRVREC + 52))
#define TR_CLASS_SRVREC_CLOCK							((TR_tenTraceClass)(TR_COMP_SRVREC + 53))
#define TR_CLASS_SRVREC_STATISTICS						((TR_tenTraceClass)(TR_COMP_SRVREC + 54))
#define TR_CLASS_SRVREC_EXT_VR							((TR_tenTraceClass)(TR_COMP_SRVREC + 55))
#define TR_CLASS_SRVREC_MDI								((TR_tenTraceClass)(TR_COMP_SRVREC + 56))
#define TR_CLASS_SRVREC_PHONE_GENERAL					((TR_tenTraceClass)(TR_COMP_SRVREC + 57))
#define TR_CLASS_SRVREC_PHONE_AUDIO						((TR_tenTraceClass)(TR_COMP_SRVREC + 58))
#define TR_CLASS_SRVREC_REGION_SELECTION				((TR_tenTraceClass)(TR_COMP_SRVREC + 59))
#define TR_CLASS_SRVREC_REGION_KNITTER					((TR_tenTraceClass)(TR_COMP_SRVREC + 60))
#define TR_CLASS_SRVREC_TUNERSYNC						((TR_tenTraceClass)(TR_COMP_SRVREC + 61))
#define TR_CLASS_SRVREC_VEHICLE_DATA					((TR_tenTraceClass)(TR_COMP_SRVREC + 62))
#define TR_CLASS_SRVREC_OPS								((TR_tenTraceClass)(TR_COMP_SRVREC + 63))
#define TR_CLASS_SRVREC_RVC								((TR_tenTraceClass)(TR_COMP_SRVREC + 64))
#define TR_CLASS_SRVREC_CHIME							((TR_tenTraceClass)(TR_COMP_SRVREC + 65))
#define TR_CLASS_SRVREC_IPOD							((TR_tenTraceClass)(TR_COMP_SRVREC + 66))
#define TR_CLASS_SRVREC_ADASIS							((TR_tenTraceClass)(TR_COMP_SRVREC + 67))
#define TR_CLASS_SRVREC_NAVREPEATER						((TR_tenTraceClass)(TR_COMP_SRVREC + 68))
#define TR_CLASS_SRVREC_MEDIAPLAYER_EXTBOX				((TR_tenTraceClass)(TR_COMP_SRVREC + 69))
#define TR_CLASS_SRVREC_TUNER_EXTBOX					((TR_tenTraceClass)(TR_COMP_SRVREC + 70))
#define TR_CLASS_SRVREC_VEHICLEFUNCTION_EXTBOX			((TR_tenTraceClass)(TR_COMP_SRVREC + 71))
#define TR_CLASS_SRVREC_AUDIO_EXTBOX					((TR_tenTraceClass)(TR_COMP_SRVREC + 72))
#define TR_CLASS_SRVREC_XM_TUNER						((TR_tenTraceClass)(TR_COMP_SRVREC + 73))
#define TR_CLASS_SRVREC_TEA_ENGINEERING					((TR_tenTraceClass)(TR_COMP_SRVREC + 74))
#define TR_CLASS_SRVREC_UPS								((TR_tenTraceClass)(TR_COMP_SRVREC + 75))
#define TR_CLASS_SRVREC_PHONE_NDI						((TR_tenTraceClass)(TR_COMP_SRVREC + 76))
#define TR_CLASS_SRVREC_REMOTE_CLIENT					((TR_tenTraceClass)(TR_COMP_SRVREC + 77))
#define TR_CLASS_SRVREC_MDB_ENGINEERING					((TR_tenTraceClass)(TR_COMP_SRVREC + 78))
#define TR_CLASS_SRVREC_DIAGLOG							((TR_tenTraceClass)(TR_COMP_SRVREC + 79))
#define TR_CLASS_SRVREC_DIAGLIB							((TR_tenTraceClass)(TR_COMP_SRVREC + 80))
#define TR_CLASS_SRVREC_DIAG_DEFSET						((TR_tenTraceClass)(TR_COMP_SRVREC + 81))
#define TR_CLASS_SRVREC_DAB_TUNER						((TR_tenTraceClass)(TR_COMP_SRVREC + 82))
#define TR_CLASS_SRVREC_DAB_MECA						((TR_tenTraceClass)(TR_COMP_SRVREC + 83))
#define TR_CLASS_SRVREC_SPM								((TR_tenTraceClass)(TR_COMP_SRVREC + 84))
#define TR_CLASS_SRVREC_CONTEXTMANAGER					((TR_tenTraceClass)(TR_COMP_SRVREC + 85))
#define TR_CLASS_SRVREC_AUDIOFUNC						((TR_tenTraceClass)(TR_COMP_SRVREC + 86))
#define TR_CLASS_SRVREC_TUNERCONTROL					((TR_tenTraceClass)(TR_COMP_SRVREC + 87))
#define TR_CLASS_SRVREC_SPI								((TR_tenTraceClass)(TR_COMP_SRVREC + 88))
#define TR_CLASS_SRVREC_CDVD							((TR_tenTraceClass)(TR_COMP_SRVREC + 89))
#define TR_CLASS_SRVREC_MP3								((TR_tenTraceClass)(TR_COMP_SRVREC + 90))
#define TR_CLASS_SRVREC_DIAGDEBUG						((TR_tenTraceClass)(TR_COMP_SRVREC + 91))
#define TR_CLASS_SRVREC_DOWNLOAD						((TR_tenTraceClass)(TR_COMP_SRVREC + 92))
#define TR_CLASS_SRVREC_GATEWAY							((TR_tenTraceClass)(TR_COMP_SRVREC + 93))
#define TR_CLASS_SRVREC_SPI_RADIO						((TR_tenTraceClass)(TR_COMP_SRVREC + 94))
#define TR_CLASS_SRVREC_SPI_KEYBOARD					((TR_tenTraceClass)(TR_COMP_SRVREC + 95))
#define TR_CLASS_SRVREC_SPI_DISPLAY						((TR_tenTraceClass)(TR_COMP_SRVREC + 96))
#define TR_CLASS_SRVREC_NAVIFUNC_NAVI					((TR_tenTraceClass)(TR_COMP_SRVREC + 97))
#define TR_CLASS_SRVREC_NAVIFUNC_SENSOR					((TR_tenTraceClass)(TR_COMP_SRVREC + 98))
#define TR_CLASS_SRVREC_NAVIFUNC_TIMA					((TR_tenTraceClass)(TR_COMP_SRVREC + 99))
#define TR_CLASS_SRVREC_DRVFUNC_CDC						((TR_tenTraceClass)(TR_COMP_SRVREC + 100))
#define TR_CLASS_SRVREC_DRVFUNC_CD						((TR_tenTraceClass)(TR_COMP_SRVREC + 101))
#define TR_CLASS_SRVREC_AUDIO_SRC						((TR_tenTraceClass)(TR_COMP_SRVREC + 102))
#define TR_CLASS_SRVREC_UIDIAG							((TR_tenTraceClass)(TR_COMP_SRVREC + 103))
#define TR_CLASS_SRVREC_MAPCTRL							((TR_tenTraceClass)(TR_COMP_SRVREC + 104))
#define TR_CLASS_SRVREC_AUDIODIAG						((TR_tenTraceClass)(TR_COMP_SRVREC + 105))
#define TR_CLASS_SRVREC_TUNERDIAG						((TR_tenTraceClass)(TR_COMP_SRVREC + 106))
#define TR_CLASS_SRVREC_DDL_TEST						((TR_tenTraceClass)(TR_COMP_SRVREC + 107))
#define TR_CLASS_SRVREC_AMFM_AUD						((TR_tenTraceClass)(TR_COMP_SRVREC + 108))
#define TR_CLASS_SRVREC_AUDIO_DIAG						((TR_tenTraceClass)(TR_COMP_SRVREC + 109))
#define TR_CLASS_SRVREC_AMFM_AUD_TUN					((TR_tenTraceClass)(TR_COMP_SRVREC + 110))
#define TR_CLASS_SRVREC_AMFM_AUD_AUD					((TR_tenTraceClass)(TR_COMP_SRVREC + 111))
#define TR_CLASS_SRVREC_AMFM_AUD_DAB					((TR_tenTraceClass)(TR_COMP_SRVREC + 112))
#define TR_CLASS_SRVREC_KBD								((TR_tenTraceClass)(TR_COMP_SRVREC + 113))
#define TR_CLASS_SRVREC_MMGR							((TR_tenTraceClass)(TR_COMP_SRVREC + 114))
#define TR_CLASS_SRVREC_CLUSTER							((TR_tenTraceClass)(TR_COMP_SRVREC + 115))
#define TR_CLASS_SRVREC_SDS_VOICE_CONTROL				((TR_tenTraceClass)(TR_COMP_SRVREC + 116))
#define TR_CLASS_SRVREC_SDS_RECOGNIZER					((TR_tenTraceClass)(TR_COMP_SRVREC + 117))
#define TR_CLASS_SRVREC_SDS_TTS							((TR_tenTraceClass)(TR_COMP_SRVREC + 118))
#define TR_CLASS_SRVREC_SDS_PROMPT_PLAYER				((TR_tenTraceClass)(TR_COMP_SRVREC + 119))
#define TR_CLASS_SRVREC_SDS_RECORDER					((TR_tenTraceClass)(TR_COMP_SRVREC + 120))
#define TR_CLASS_SRVREC_SDS_SPEECH_DATA_PROVIDER		((TR_tenTraceClass)(TR_COMP_SRVREC + 121))
#define TR_CLASS_SRVREC_SDS_TEST_BASEAPP				((TR_tenTraceClass)(TR_COMP_SRVREC + 122))
#define TR_CLASS_SRVREC_ACR								((TR_tenTraceClass)(TR_COMP_SRVREC + 123))
#define TR_CLASS_SRVREC_MAP_3D_CTRL						((TR_tenTraceClass)(TR_COMP_SRVREC + 124))
#define TR_CLASS_SRVREC_FRAMETEST_ADDITIONAL_TEST_SERVICE	((TR_tenTraceClass)(TR_COMP_SRVREC + 125))
#define TR_CLASS_SRVREC_FRAMETEST_TEST_SERVICE				((TR_tenTraceClass)(TR_COMP_SRVREC + 126))
#define TR_CLASS_SRVREC_CSFW_CCA_TEST_SERVICE				((TR_tenTraceClass)(TR_COMP_SRVREC + 127))
#define TR_CLASS_SRVREC_CSFW_CCA_SAMPLE_SERVICE				((TR_tenTraceClass)(TR_COMP_SRVREC + 128))
#define TR_CLASS_SRVREC_DEMO_MESSAGE_SERVICE				((TR_tenTraceClass)(TR_COMP_SRVREC + 129))
#define TR_CLASS_SRVREC_MOSTCCAGWSERVICE					((TR_tenTraceClass)(TR_COMP_SRVREC + 130))

/* class definitions for component = "NAVI"					*/
#define TR_CLASS_NAV_PC									((TR_tenTraceClass)(TR_COMP_NAVI + 0))
#define TR_CLASS_NAV_POSCALCDISPATCH					((TR_tenTraceClass)(TR_COMP_NAVI + 1))
#define TR_CLASS_NAV_POSCALCDISPATCH_MSGFLOW			((TR_tenTraceClass)(TR_COMP_NAVI + 2))
#define TR_CLASS_NAV_POSCALCDISPATCH_STATUS				((TR_tenTraceClass)(TR_COMP_NAVI + 3))
#define TR_CLASS_NAV_DEADREC							((TR_tenTraceClass)(TR_COMP_NAVI + 4))
#define TR_CLASS_NAV_DEADREC_MSGFLOW					((TR_tenTraceClass)(TR_COMP_NAVI + 5))
#define TR_CLASS_NAV_DEADREC_SENSORDATA					((TR_tenTraceClass)(TR_COMP_NAVI + 6))
#define TR_CLASS_NAV_DEADREC_CALIBRATION				((TR_tenTraceClass)(TR_COMP_NAVI + 7))
#define TR_CLASS_NAV_DEADREC_STATUS						((TR_tenTraceClass)(TR_COMP_NAVI + 8))
#define TR_CLASS_NAV_MAPMATCHING						((TR_tenTraceClass)(TR_COMP_NAVI + 9))
#define TR_CLASS_NAV_LDB								((TR_tenTraceClass)(TR_COMP_NAVI + 10))
#define TR_CLASS_NAV_LDB_MSGFLOW						((TR_tenTraceClass)(TR_COMP_NAVI + 11))
#define TR_CLASS_NAV_LDB_STATUS							((TR_tenTraceClass)(TR_COMP_NAVI + 12))
#define TR_CLASS_NAV_PRESENTATION						((TR_tenTraceClass)(TR_COMP_NAVI + 13))
#define TR_CLASS_NAV_PRESENTATION_MSGFLOW				((TR_tenTraceClass)(TR_COMP_NAVI + 14))
#define TR_CLASS_NAV_PRESENTATION_STATUS				((TR_tenTraceClass)(TR_COMP_NAVI + 15))
#define TR_CLASS_NAV_POSHORIZON							((TR_tenTraceClass)(TR_COMP_NAVI + 16))
#define TR_CLASS_NAV_POSHORIZON_MSGFLOW					((TR_tenTraceClass)(TR_COMP_NAVI + 17))
#define TR_CLASS_NAV_POSHORIZON_STATUS					((TR_tenTraceClass)(TR_COMP_NAVI + 18))
#define TR_CLASS_NAV_POSMSG_FLOW						((TR_tenTraceClass)(TR_COMP_NAVI + 19))
#define TR_CLASS_NAV_PCCOMP_FFS_LOG						((TR_tenTraceClass)(TR_COMP_NAVI + 20))
#define TR_CLASS_NAV_POSSELECTION						((TR_tenTraceClass)(TR_COMP_NAVI + 21))
#define TR_CLASS_NAV_POSSELECTION_MSGFLOW				((TR_tenTraceClass)(TR_COMP_NAVI + 22))
#define TR_CLASS_NAV_POSSELECTION_STATUS				((TR_tenTraceClass)(TR_COMP_NAVI + 23))

 /* rgcomp */
#define TR_CLASS_NAV_RG									((TR_tenTraceClass)(TR_COMP_NAVI + 32)) 
#define TR_CLASS_NAV_RG_RGIF							((TR_tenTraceClass)(TR_COMP_NAVI + 33)) 
#define TR_CLASS_NAV_RG_ROUTEGUIDANCE					((TR_tenTraceClass)(TR_COMP_NAVI + 34)) 
#define TR_CLASS_NAV_RG_ACOUSTIC						((TR_tenTraceClass)(TR_COMP_NAVI + 35)) 
#define TR_CLASS_NAV_RG_ACOUSTIC_LOADDEC				((TR_tenTraceClass)(TR_COMP_NAVI + 36)) 
#define TR_CLASS_NAV_RG_ACOUSTIC_SPEECH					((TR_tenTraceClass)(TR_COMP_NAVI + 37)) 
#define TR_CLASS_NAV_RG_CDB								((TR_tenTraceClass)(TR_COMP_NAVI + 38)) 
#define TR_CLASS_NAV_RG_PDB								((TR_tenTraceClass)(TR_COMP_NAVI + 39)) 
#define TR_CLASS_NAV_RG_IDB								((TR_tenTraceClass)(TR_COMP_NAVI + 40)) 
#define TR_CLASS_NAV_RG_HDB								((TR_tenTraceClass)(TR_COMP_NAVI + 41)) 

/* edmcomp */
#define TR_CLASS_NAV_EDM								((TR_tenTraceClass)(TR_COMP_NAVI + 64))
#define TR_CLASS_NAV_EDM_TASK							((TR_tenTraceClass)(TR_COMP_NAVI + 65))
#define TR_CLASS_NAV_EDM_TASK_MSGFLOW					((TR_tenTraceClass)(TR_COMP_NAVI + 66))
#define TR_CLASS_NAV_EDM_TASK_ZOCDB						((TR_tenTraceClass)(TR_COMP_NAVI + 67))
#define TR_CLASS_NAV_EDM_TASK_NTMM						((TR_tenTraceClass)(TR_COMP_NAVI + 68))
#define TR_CLASS_NAV_EDM_TASK_TIMER						((TR_tenTraceClass)(TR_COMP_NAVI + 69))
#define TR_CLASS_NAV_EDM_TASK_SC_SYSTEM					((TR_tenTraceClass)(TR_COMP_NAVI + 70))
#define TR_CLASS_NAV_EDM_TASK_SC_COUNTRY				((TR_tenTraceClass)(TR_COMP_NAVI + 71))
#define TR_CLASS_NAV_EDM_TASK_SC_POSITION				((TR_tenTraceClass)(TR_COMP_NAVI + 72))
#define TR_CLASS_NAV_EDM_TASK_SC_RECTM					((TR_tenTraceClass)(TR_COMP_NAVI + 73))
#define TR_CLASS_NAV_EDM_TASK_SC_PROCTM					((TR_tenTraceClass)(TR_COMP_NAVI + 74))
#define TR_CLASS_NAV_EDM_TASK_SC_UTM					((TR_tenTraceClass)(TR_COMP_NAVI + 75))
#define TR_CLASS_NAV_EDM_TASK_SC_MDB					((TR_tenTraceClass)(TR_COMP_NAVI + 76))
#define TR_CLASS_NAV_EDM_TASK_SC_NCLIENTS				((TR_tenTraceClass)(TR_COMP_NAVI + 77))
#define TR_CLASS_NAV_EDM_NTLDS							((TR_tenTraceClass)(TR_COMP_NAVI + 78))
#define TR_CLASS_NAV_EDM_NTLDS_MSGFLOW					((TR_tenTraceClass)(TR_COMP_NAVI + 79))
#define TR_CLASS_NAV_EDM_NTLDS_RDBMAN					((TR_tenTraceClass)(TR_COMP_NAVI + 80))
#define TR_CLASS_NAV_EDM_NTLDS_CONVERTTM				((TR_tenTraceClass)(TR_COMP_NAVI + 81))
#define TR_CLASS_NAV_EDM_NTLDS_NTLIF					((TR_tenTraceClass)(TR_COMP_NAVI + 82))
#define TR_CLASS_NAV_EDM_NTLDS_SC_SYSTEM				((TR_tenTraceClass)(TR_COMP_NAVI + 83))
#define TR_CLASS_NAV_EDM_WORKER							((TR_tenTraceClass)(TR_COMP_NAVI + 84))


/* mdbcomp */
#define TR_CLASS_NAV_MDB								((TR_tenTraceClass)(TR_COMP_NAVI + 96))
#define TR_CLASS_NAV_MDB_DATAACCESS						((TR_tenTraceClass)(TR_COMP_NAVI + 97))
#define TR_CLASS_NAV_MDB_STATES							((TR_tenTraceClass)(TR_COMP_NAVI + 98))
#define TR_CLASS_NAV_MDB_OPTIMIZER						((TR_tenTraceClass)(TR_COMP_NAVI + 99))
#define TR_CLASS_NAV_MDB_ROADDATASERVER					((TR_tenTraceClass)(TR_COMP_NAVI + 100))
#define TR_CLASS_NAV_MDB_ROADDATASTREAM					((TR_tenTraceClass)(TR_COMP_NAVI + 101))
#define TR_CLASS_NAV_MDB_MSGFLOW						((TR_tenTraceClass)(TR_COMP_NAVI + 102))

#define TR_CLASS_NAV_SYSTESTTRACES						((TR_tenTraceClass)(TR_COMP_NAVI + 264))


/* mapdataserver */
#define TR_CLASS_NAV_MADS								((TR_tenTraceClass)(TR_COMP_NAVI + 128))
#define TR_CLASS_NAV_MADS_DATASERVER					((TR_tenTraceClass)(TR_COMP_NAVI + 129))
#define TR_CLASS_NAV_MADS_DISPLAYSERVER					((TR_tenTraceClass)(TR_COMP_NAVI + 130))
#define TR_CLASS_NAV_MADS_MSGFLOW					    ((TR_tenTraceClass)(TR_COMP_NAVI + 131))
#define TR_CLASS_NAV_MADS_STATES					    ((TR_tenTraceClass)(TR_COMP_NAVI + 132))
#define TR_CLASS_NAV_MADS_EXTRAPOL					    ((TR_tenTraceClass)(TR_COMP_NAVI + 133))
#define TR_CLASS_NAV_MADS_TRACE_FOR_MAP					((TR_tenTraceClass)(TR_COMP_NAVI + 134))
#define TR_CLASS_NAV_MADS_TRACE_FOR_EXTRAPOL			((TR_tenTraceClass)(TR_COMP_NAVI + 135))
#define TR_CLASS_NAV_MADS_MAPCONTROL					((TR_tenTraceClass)(TR_COMP_NAVI + 136))


/* rccomp */ 
#define TR_CLASS_NAV_RCCOMP								((TR_tenTraceClass)(TR_COMP_NAVI + 160))
#define TR_CLASS_NAV_RCCOMP_DLD							((TR_tenTraceClass)(TR_COMP_NAVI + 161))
#define TR_CLASS_NAV_RCCOMP_DLD_MSGFLOW					((TR_tenTraceClass)(TR_COMP_NAVI + 162))
#define TR_CLASS_NAV_RCCOMP_DLD_WORK					((TR_tenTraceClass)(TR_COMP_NAVI + 163))
#define TR_CLASS_NAV_RCCOMP_RIF							((TR_tenTraceClass)(TR_COMP_NAVI + 164))
#define TR_CLASS_NAV_RCCOMP_RIF_MSGFLOW					((TR_tenTraceClass)(TR_COMP_NAVI + 165))
#define TR_CLASS_NAV_RCCOMP_RIF_THDB					((TR_tenTraceClass)(TR_COMP_NAVI + 166))
#define TR_CLASS_NAV_RCCOMP_RIF_RIDB					((TR_tenTraceClass)(TR_COMP_NAVI + 167))
#define TR_CLASS_NAV_RCCOMP_RIF_RFOLLOW					((TR_tenTraceClass)(TR_COMP_NAVI + 168))
#define TR_CLASS_NAV_RCCOMP_RIF_ENABLE_LOG_NOROUTE_TO_FFS					((TR_tenTraceClass)(TR_COMP_NAVI + 169))
#define TR_CLASS_NAV_RCCOMP_RIF_DISABLE_LOG_NOROUTE_TO_FFS					((TR_tenTraceClass)(TR_COMP_NAVI + 170))
#define TR_CLASS_NAV_RCCOMP_RCALC						((TR_tenTraceClass)(TR_COMP_NAVI + 171))
#define TR_CLASS_NAV_RCCOMP_RCALC_MSGFLOW				((TR_tenTraceClass)(TR_COMP_NAVI + 172))
#define TR_CLASS_NAV_RCCOMP_RCALC_PROCESS				((TR_tenTraceClass)(TR_COMP_NAVI + 173))
#define TR_CLASS_NAV_RCCOMP_RCALC_GCL					((TR_tenTraceClass)(TR_COMP_NAVI + 174))
#define TR_CLASS_NAV_RCCOMP_RCALC_CALC					((TR_tenTraceClass)(TR_COMP_NAVI + 175))
#define TR_CLASS_NAV_RCCOMP_RCALC_ROUTELIST				((TR_tenTraceClass)(TR_COMP_NAVI + 176))
#define TR_CLASS_NAV_RCCOMP_RDB							((TR_tenTraceClass)(TR_COMP_NAVI + 177))
#define TR_CLASS_NAV_RCCOMP_RDB_READ					((TR_tenTraceClass)(TR_COMP_NAVI + 178))
#define TR_CLASS_NAV_RCCOMP_RDB_CONVERT					((TR_tenTraceClass)(TR_COMP_NAVI + 179))
#define TR_CLASS_NAV_RCCOMP_RDB_DATABASE				((TR_tenTraceClass)(TR_COMP_NAVI + 180))
#define TR_CLASS_NAV_RCCOMP_RDBLOADER					((TR_tenTraceClass)(TR_COMP_NAVI + 181))
#define TR_CLASS_NAV_RCCOMP_RDBLOADER_MSGFLOW			((TR_tenTraceClass)(TR_COMP_NAVI + 182))
#define TR_CLASS_NAV_RCCOMP_RDBLOADER_PROCESS			((TR_tenTraceClass)(TR_COMP_NAVI + 183))
#define TR_CLASS_NAV_RCCOMP_RI_WORKER					((TR_tenTraceClass)(TR_COMP_NAVI + 184))
#define TR_CLASS_NAV_RCCOMP_RI_WORKER_MSGFLOW			((TR_tenTraceClass)(TR_COMP_NAVI + 185))
#define TR_CLASS_NAV_RCCOMP_RI_WORKER_PROCESS			((TR_tenTraceClass)(TR_COMP_NAVI + 186))
#define TR_CLASS_NAV_RCCOMP_LFW							((TR_tenTraceClass)(TR_COMP_NAVI + 187))
#define TR_CLASS_NAV_RCCOMP_LFW_CAT						((TR_tenTraceClass)(TR_COMP_NAVI + 188))  /*  CalcAreaTable  */
#define TR_CLASS_NAV_RCCOMP_LFW_RES						((TR_tenTraceClass)(TR_COMP_NAVI + 189)) /*  ResultList  */ 

/* navi state manager */
#define TR_CLASS_NAV_NAVISTATEMANAGER					((TR_tenTraceClass)(TR_COMP_NAVI + 192))

#define TR_CLASS_NAV_LI									((TR_tenTraceClass)(TR_COMP_NAVI + 200))
#define TR_CLASS_NAV_LI_MESSAGES						((TR_tenTraceClass)(TR_COMP_NAVI + 201))
#define TR_CLASS_NAV_LI_STATES							((TR_tenTraceClass)(TR_COMP_NAVI + 202))


#define TR_CLASS_NAV_TRIP_DATA							((TR_tenTraceClass)(TR_COMP_NAVI + 232))


/* class definitions for component = "DAPI"					*/
#define TR_CLASS_DAPI_DEVMAN							((TR_tenTraceClass)(TR_COMP_DAPI + 0))
#define TR_CLASS_DAPI_DEVMAN_DATA_AVAIL					((TR_tenTraceClass)(TR_COMP_DAPI + 1))
#define TR_CLASS_DAPI_DEVMAN_CPR						((TR_tenTraceClass)(TR_COMP_DAPI + 2))

#define TR_CLASS_DAPI_LISA								((TR_tenTraceClass)(TR_COMP_DAPI + 16))
#define TR_CLASS_DAPI_SBS								((TR_tenTraceClass)(TR_COMP_DAPI + 17))
#define TR_CLASS_DAPI_RAWDATA							((TR_tenTraceClass)(TR_COMP_DAPI + 18))
#define TR_CLASS_DAPI_RESINFO							((TR_tenTraceClass)(TR_COMP_DAPI + 19))
#define TR_CLASS_DAPI_COMM								((TR_tenTraceClass)(TR_COMP_DAPI + 20))
#define TR_CLASS_DAPI_FRAMEWORK							((TR_tenTraceClass)(TR_COMP_DAPI + 21))
#define TR_CLASS_DAPI_ARCHIVE							((TR_tenTraceClass)(TR_COMP_DAPI + 22))

#define TR_CLASS_DAPI_TMC								((TR_tenTraceClass)(TR_COMP_DAPI + 32))
#define TR_CLASS_DAPI_TMC_ARCHIVE						((TR_tenTraceClass)(TR_COMP_DAPI + 33))
#define TR_CLASS_DAPI_TMC_ACCESS						((TR_tenTraceClass)(TR_COMP_DAPI + 34))
#define TR_CLASS_DAPI_TMC_HL_IF							((TR_tenTraceClass)(TR_COMP_DAPI + 35))
#define TR_CLASS_DAPI_TMC_KNIT							((TR_tenTraceClass)(TR_COMP_DAPI + 36))

#define TR_CLASS_DAPI_RNW								((TR_tenTraceClass)(TR_COMP_DAPI + 48))
#define TR_CLASS_DAPI_RNW_ARC_STORE						((TR_tenTraceClass)(TR_COMP_DAPI + 49))
#define TR_CLASS_DAPI_RNW_ARC_LISTS						((TR_tenTraceClass)(TR_COMP_DAPI + 50))
#define TR_CLASS_DAPI_RNW_ARC_CONTENT					((TR_tenTraceClass)(TR_COMP_DAPI + 51))
#define TR_CLASS_DAPI_RNW_ARC_CTRL						((TR_tenTraceClass)(TR_COMP_DAPI + 52))
#define TR_CLASS_DAPI_RNW_MAP							((TR_tenTraceClass)(TR_COMP_DAPI + 53))
#define TR_CLASS_DAPI_RNW_ACCESS						((TR_tenTraceClass)(TR_COMP_DAPI + 54))
#define TR_CLASS_DAPI_RNW_HL_IF							((TR_tenTraceClass)(TR_COMP_DAPI + 55))
#define TR_CLASS_DAPI_RNW_KNIT							((TR_tenTraceClass)(TR_COMP_DAPI + 56))
#define TR_CLASS_DAPI_RNW_PATCH							((TR_tenTraceClass)(TR_COMP_DAPI + 57))

#define TR_CLASS_DAPI_MAP								((TR_tenTraceClass)(TR_COMP_DAPI + 64))
#define TR_CLASS_DAPI_MAP_STOPWATCH						((TR_tenTraceClass)(TR_COMP_DAPI + 65))
#define TR_CLASS_DAPI_MAP_CONVERTER						((TR_tenTraceClass)(TR_COMP_DAPI + 66))
#define TR_CLASS_DAPI_MAP_MANAGER						((TR_tenTraceClass)(TR_COMP_DAPI + 67))
#define TR_CLASS_DAPI_MAP_WORKER						((TR_tenTraceClass)(TR_COMP_DAPI + 68))
#define TR_CLASS_DAPI_MAP_ID_CONTROLLER					((TR_tenTraceClass)(TR_COMP_DAPI + 69))
#define TR_CLASS_DAPI_MAP_TCICACHE						((TR_tenTraceClass)(TR_COMP_DAPI + 70))
#define TR_CLASS_DAPI_MAP_TILELOADER					((TR_tenTraceClass)(TR_COMP_DAPI + 71))
#define TR_CLASS_DAPI_MAP_KNIT							((TR_tenTraceClass)(TR_COMP_DAPI + 72))
#define TR_CLASS_DAPI_MAP_CONNECT						((TR_tenTraceClass)(TR_COMP_DAPI + 73))

#define TR_CLASS_DAP_RS									((TR_tenTraceClass)(TR_COMP_DAPI + 80))
#define TR_CLASS_DAPI_RS_MANAGER						((TR_tenTraceClass)(TR_COMP_DAPI + 81))
#define TR_CLASS_DAPI_RS_WORKER							((TR_tenTraceClass)(TR_COMP_DAPI + 82))

/* class definitions for component = "SINA"					*/ 
#define TR_CLASS_SINA									((TR_tenTraceClass)(TR_COMP_SINA + 0))
#define TR_CLASS_SINA_ARCHIVESTRATEGY					((TR_tenTraceClass)(TR_COMP_SINA + 1))
#define TR_CLASS_SINA_ASTHREAD							((TR_tenTraceClass)(TR_COMP_SINA + 2))
#define TR_CLASS_SINA_AS_MESSAGES						((TR_tenTraceClass)(TR_COMP_SINA + 3))
#define TR_CLASS_SINA_AS_STATECHART						((TR_tenTraceClass)(TR_COMP_SINA + 4))
#define TR_CLASS_SINA_AS_DEST_GLUE						((TR_tenTraceClass)(TR_COMP_SINA + 5))
#define TR_CLASS_SINA_AS_LOCINPUTCIRCLE					((TR_tenTraceClass)(TR_COMP_SINA + 6))
#define TR_CLASS_SINA_AS_POSITIONCIRCLE					((TR_tenTraceClass)(TR_COMP_SINA + 7))
#define TR_CLASS_SINA_AS_ROUTEONECELLS					((TR_tenTraceClass)(TR_COMP_SINA + 8))
#define TR_CLASS_SINA_AS_TMCSEGMENTS					((TR_tenTraceClass)(TR_COMP_SINA + 9))
#define TR_CLASS_SINA_AS_POSITIONDB						((TR_tenTraceClass)(TR_COMP_SINA + 10))
#define TR_CLASS_SINA_AS_TRIPDB							((TR_tenTraceClass)(TR_COMP_SINA + 11))
#define TR_CLASS_SINA_AS_REGPARAMETER					((TR_tenTraceClass)(TR_COMP_SINA + 12))
#define TR_CLASS_SINA_AS_TMCLOCATIONS					((TR_tenTraceClass)(TR_COMP_SINA + 13))
#define TR_CLASS_SINA_AS_PERFORMANCE					((TR_tenTraceClass)(TR_COMP_SINA + 14))
#define TR_CLASS_SINA_AS_POSITIONRING					((TR_tenTraceClass)(TR_COMP_SINA + 15))

/* class definitions for component = "STAT"					*/
#define TR_CLASS_STAT									((TR_tenTraceClass)(TR_COMP_STAT + 0))

/* class definitions for component = "TEA"					*/
#define TR_CLASS_TEA									((TR_tenTraceClass)(TR_COMP_TEA + 0))

/* class definitions for component = "TIMA"					*/
#define TR_CLASS_TIMA									((TR_tenTraceClass)(TR_COMP_TIMA + 0))

/* class definitions for component = "MAPENGINE"			*/
#define TR_CLASS_MAPENGINE								((TR_tenTraceClass)(TR_COMP_MAPENGINE + 0))

/* class definitions for component = "NAVILIB"				*/
#define TR_CLASS_RIA									((TR_tenTraceClass)(TR_COMP_NAVLIB + 0))
#define TR_CLASS_RTF									((TR_tenTraceClass)(TR_COMP_NAVLIB + 1))
#define TR_CLASS_ERR									((TR_tenTraceClass)(TR_COMP_NAVLIB + 2))
#define TR_CLASS_NFC									((TR_tenTraceClass)(TR_COMP_NAVLIB + 3))
#define TR_CLASS_NAVAPP_ENTRY							((TR_tenTraceClass)(TR_COMP_NAVLIB + 4))
/* class definitions for component = "MCU"					*/
#define TR_CLASS_MCU									((TR_tenTraceClass)(TR_COMP_MCU + 0))
#define TR_CLASS_MCU_MSG_MANAGER						((TR_tenTraceClass)(TR_COMP_MCU + 1))
#define TR_CLASS_MCU_DEST_MEMORY						((TR_tenTraceClass)(TR_COMP_MCU + 2))
#define TR_CLASS_MCU_LI									((TR_tenTraceClass)(TR_COMP_MCU + 3))
#define TR_CLASS_MCU_MSG								((TR_tenTraceClass)(TR_COMP_MCU + 4))
#define TR_CLASS_MCU_MSG_MULITMANAGER					((TR_tenTraceClass)(TR_COMP_MCU + 5))
#define TR_CLASS_MCU_ROUTE								((TR_tenTraceClass)(TR_COMP_MCU + 6))
#define TR_CLASS_MCU_MAP								((TR_tenTraceClass)(TR_COMP_MCU + 7))
#define TR_CLASS_MCU_POSITION							((TR_tenTraceClass)(TR_COMP_MCU + 8))
#define TR_CLASS_MCU_ROUTEGUIDANCE						((TR_tenTraceClass)(TR_COMP_MCU + 9))
#define TR_CLASS_MCU_SYSTEM								((TR_tenTraceClass)(TR_COMP_MCU + 10))
#define TR_CLASS_MCU_CSM								((TR_tenTraceClass)(TR_COMP_MCU + 11))
#define TR_CLASS_MCU_TMC								((TR_tenTraceClass)(TR_COMP_MCU + 12))

/* class definitions for component = "DMM"					*/
#define TR_CLASS_DMM									((TR_tenTraceClass)(TR_COMP_DMM + 0))

/* class definitions for component = "UMM"					*/
#define TR_CLASS_UMM									((TR_tenTraceClass)(TR_COMP_UMM + 0))

/* class definitions for component = "BROWSER"              */
#define TR_CLASS_BROWSER								((TR_tenTraceClass)(TR_COMP_BROWSER + 0))

/* class definitions for component = "ANALYSIS"             */
#define TR_CLASS_TIME_TRACKER							((TR_tenTraceClass)(TR_COMP_ANALYSIS + 1))
#define TR_CLASS_PERFORMANCE_ANALYZER					((TR_tenTraceClass)(TR_COMP_ANALYSIS + 2))
#define TR_CLASS_MEMORY_TRACE							((TR_tenTraceClass)(TR_COMP_ANALYSIS + 3))
#define TR_CLASS_SYSTEM_INFORMATION						((TR_tenTraceClass)(TR_COMP_ANALYSIS + 4))

/* class definitions for component = "NETWORKING"           */
#define TR_CLASS_MOST									((TR_tenTraceClass)(TR_COMP_NETWORKING + 1))
#define TR_CLASS_MOST_NSL								((TR_tenTraceClass)(TR_COMP_NETWORKING + 2))

/* class definitions for component = "CONNECTIVITY"         */
#define TR_CLASS_PHONE									((TR_tenTraceClass)(TR_COMP_CONNECTIVITY + 1))
#define TR_CLASS_SAP									((TR_tenTraceClass)(TR_COMP_CONNECTIVITY + 9))
#define TR_CLASS_ABP									((TR_tenTraceClass)(TR_COMP_CONNECTIVITY + 10))
#define TR_CLASS_A2DB									((TR_tenTraceClass)(TR_COMP_CONNECTIVITY + 11))
#define TR_CLASS_HFP									((TR_tenTraceClass)(TR_COMP_CONNECTIVITY + 12))

/*****************************************************************************
 *****************************************************************************
 *                    Please do not add any new Trace classes.               *
 * Trace classes have to be defined in the component specific files only.    *
 * Please contact Haefen Holger von (CM-AI/PJ-CF33) for adding new Components*
 *****************************************************************************
 *****************************************************************************/

#endif /* BOSCH_TRACE_H */
