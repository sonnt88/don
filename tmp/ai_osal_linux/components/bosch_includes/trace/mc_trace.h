/************************************************************************
 * \file: mc_trace.h
 *
 * \version: $Id: mc_trace.h$
 *
 * This header file declares constants for trace
 *
 * \author: C. Resch,
 *          Dhanasekaran.D
 *
 * \copyright: (c) 2009 - 2010 RBCM, Hildesheim, Germany
 *
 * \history
 * 17-Jul-2009  C Resch Initial Version
 * 12-Feb-2010  Dhanasekaran.D enum replaced by #define and Trace classes 
 *              are moved to mc_trace_class_def.h file.
 * 05-Oct-2011  Dhanasekaran.D started allocating sharing CompID with Index
 *              to avoid Gen2 trace protocol rework.
 * 12-Jan-2012  Dhanasekaran.D IDs merged from tri_types.h for VWTRTN
 ***********************************************************************/

#ifndef __BOSCH_TRACE_H 
#define __BOSCH_TRACE_H

#include "mc_trace_class_def.h"
/*****************************************************************************
 * Definitions for trace callback channels
 * =======================================
 *  
 * A callback channel is identified by a unsigned 1-Byte number. The ComponentID
 * should match to the callback channel ID.   
 *
 *   
 *   +---------------+---------------+-------------------------------+
 *   | Component ID  | Purpose       | Comments                      |
 *   +---------------+---------------+-------------------------------+
 *   | 0-12          | Trace / System| historical reasons            |
 *   +---------------+---------------+-------------------------------+
 *   | 13-179        | Mother        | Reserved Component IDs for MCs|
 *   |               | Companies     |                               |
 *   +---------------+---------------+-------------------------------+
 *   | 180-255       | ADIT          | product relevant              | 
 *   +---------------+---------------+-------------------------------+
 *
 *   Please comment each update with the following template:
 *   <packagename>, <date of entry>
 ******************************************************************************/

/********************TRACE IDs in platform specific files***********************
 * Paramount: tri_types.h, @di_trace
 * Gen1: mc_trace.h @di_tengine_os, gen1 integration branch
 * Gen2: mc_trace.h @di_tengine_os, gen2 integration branch
 *
 * Assign new IDs only when its free on all 3 platforms.
 * 
 *\\bosch.com\dfsrb\DfsDE\DIV\CM\DI\Projects\Common\TTFIS\Documents\other\TraceIDs.xls
 ******************************************************************************/
#define TR_TTFIS_SUPERVISOR       ((TR_tenTraceChan)13) 
#define TR_TTFIS_COPRO            ((TR_tenTraceChan)14)
#define TR_TTFIS_DIAG_RPM         ((TR_tenTraceChan)16)
#define TR_TTFIS_DIAG_CMDI        ((TR_tenTraceChan)17)
#define TR_TTFIS_SOCKET           ((TR_tenTraceChan)18)
#define TR_TTFIS_B3               ((TR_tenTraceChan)19)	/*TR_TTFIS_B3 value must be always 19.*/ 
#define TR_TTFIS_TRIP             ((TR_tenTraceChan)21)
#define TR_TTFIS_CDCTRL           ((TR_tenTraceChan)22)
#define TR_TTFIS_LIN              ((TR_tenTraceChan)23)
#define TR_TTFIS_OSALTEST         ((TR_tenTraceChan)24)
#define TR_TTFIS_TRACE            ((TR_tenTraceChan)25)
#define TR_TTFIS_SETE             ((TR_tenTraceChan)27)
#define TR_TTFIS_DRVMGR           ((TR_tenTraceChan)29)
#define	TR_TTFIS_DRIVER_ASSIST_VIDEO ((TR_tenTraceChan)30) /* Channel = "VIDEO", dhd2hi, <10-Jun-2010>*/
#define TR_TTFIS_TUNER            ((TR_tenTraceChan)32)
#define TR_TTFIS_HMI              ((TR_tenTraceChan)33)
#define TR_TTFIS_OEDT             ((TR_tenTraceChan)34)
#define TR_TTFIS_SIM_CGI          ((TR_tenTraceChan)35)
#define TR_TTFIS_EXC              ((TR_tenTraceChan)36)
#define TR_TTFIS_BOSCH_AUDIO      ((TR_tenTraceChan)37)
#define TR_TTFIS_DCM_AMFMTUNER    ((TR_tenTraceChan)38)
#define TR_TTFIS_NAVIFUNC         ((TR_tenTraceChan)39)
#define TR_TTFIS_UI               ((TR_tenTraceChan)40)
#define TR_TTFIS_SENSOR           ((TR_tenTraceChan)41)
#define TR_TTFIS_MEMORY_BASE      ((TR_tenTraceChan)42)
#define TR_TTFIS_MEMORY_NAV       ((TR_tenTraceChan)43)
#define TR_TTFIS_OSALCORE         ((TR_tenTraceChan)44)
#define TR_TTFIS_DISPLAY          ((TR_tenTraceChan)45)
#define TR_TTFIS_KEYBOARD         ((TR_tenTraceChan)46)
#define TR_TTFIS_TUNERMASTER      ((TR_tenTraceChan)47)
#define TR_TTFIS_OSALIO           ((TR_tenTraceChan)48)
#define TR_TTFIS_UMM              ((TR_tenTraceChan)49)
#define TR_TTFIS_MP3D             ((TR_tenTraceChan)50)
#define TR_TTFIS_EMBEDDEDRADIOSW  ((TR_tenTraceChan)51)
#define TR_TTFIS_UC               ((TR_tenTraceChan)53)
#define TR_TTFIS_DMM              ((TR_tenTraceChan)54)
#define TR_TTFIS_MOST_NSL         ((TR_tenTraceChan)55)
#define TR_TTFIS_FD_CSM           ((TR_tenTraceChan)56)
#define TR_TTFIS_SCANSOFT_LIBRARY	((TR_tenTraceChan)57)
#define TR_TTFIS_AUDIOPLAYER      ((TR_tenTraceChan)58)
#define TR_TTFIS_FFD              ((TR_tenTraceChan)59)
#define TR_TTFIS_PHONE            ((TR_tenTraceChan)60)
#define TR_TTFIS_CLIMATECTRL      ((TR_tenTraceChan)61)
#define TR_TTFIS_REMOTEDISPLAY    ((TR_tenTraceChan)62)
#define TR_TTFIS_PMT              ((TR_tenTraceChan)63)
#define TR_TTFIS_MIDDLEWARE       ((TR_tenTraceChan)64)  /* merged from tri_types.h for VWTRTN, dhd3kor, <11-Jan-2012>*/
#define TR_TTFIS_AMFM_AUD_PROXY   ((TR_tenTraceChan)65)
#define TR_TTFIS_TOUCH            ((TR_tenTraceChan)66)
#define TR_TTFIS_VD_DIMMING       ((TR_tenTraceChan)67)
#define TR_TTFIS_CLOCK            ((TR_tenTraceChan)68)
#define TR_TTFIS_DIAGNOSIS        ((TR_tenTraceChan)69)
#define TR_TTFIS_DOWNLOAD         ((TR_tenTraceChan)70)
#define TR_CLASS_ODI              ((TR_tenTraceChan)71)
#define TR_TTFIS_DEVICEMANAGER    ((TR_tenTraceChan)72)  /* Channelname = DEVICEMANAGER, sam5hi, <14-Oct-2010> */
#define TR_TTFIS_IPODAUTH         ((TR_tenTraceChan)73)  /* Channelname = IPODAUTH,  sam5hi, <14-Oct-2010>*/ 
#define TR_TTFIS_AUDIOCUES        ((TR_tenTraceChan)74)  /* Channelname = AUDIOCUES,  sam5hi, <14-Oct-2010>*/
#define TR_TTFIS_VEHICLEDATA      ((TR_tenTraceChan)75)  /* As like Gen1 TR_TTFIS_VEHICLEDATA, dhd3kor, <06-Oct-2011>*/
#define TR_TTFIS_PFBMOSTCONTROL	  ((TR_tenTraceChan)76)  /* As like Gen1 TR_TTFIS_PFBMOSTCONTROL, dhd3kor, <06-Oct-2011>*/
#define TR_TTFIS_CAN2CSFW         ((TR_tenTraceChan)77)  /* As like Gen1 TR_TTFIS_CAN2CSFW, dhd3kor, <06-Oct-2011>*/
#define	TR_TTFIS_NAVI             ((TR_tenTraceChan)80)
#define TR_TTFIS_IPODCONTROL      ((TR_tenTraceChan)83)  /* Channelname = TR_TTFIS_IPODCONTROL, dhd3kor, <21-Sep-2011>*/
#define TR_TTFIS_DAPI             ((TR_tenTraceChan)84)
#define TR_TTFIS_DATAPROVIDER	  ((TR_tenTraceChan)85)  /* for G3G, jov1kor, <29-Jul-2013>*/
#define TR_TTFIS_MOSTCCAGW        ((TR_tenTraceChan)87)  /* Channelname = MOSTCCAGW,  dhd3kor, <29-Aug-2011>*/
#define TR_TTFIS_TIMA             ((TR_tenTraceChan)88)
#define TR_TTFIS_SAAL             ((TR_tenTraceChan)89) /* Channelname = "SAAL", dhd2hi, <10-Jun-2010>*/
#define TR_TTFIS_ANIMATION        ((TR_tenTraceChan)91)  /* Channelname = ANIMATION,  sam5hi, <14-Oct-2010>*/
#define TR_TTFIS_SINA             ((TR_tenTraceChan)92)
#define TR_TTFIS_NAVIUTIL         ((TR_tenTraceChan)93) /* Channelname = "NAVIUTIL", dhd2hi, <10-Jun-2010>*/
#define TR_TTFIS_CONNECTIVITY     ((TR_tenTraceChan)94) /* Channelname = "CONNECTIVITY", dhd2hi, <10-Jun-2010>*/
#define TR_TTFIS_NAVLIB           ((TR_tenTraceChan)96)
#define TR_TTFIS_LINUX_OEDT       ((TR_tenTraceChan)97) /*06.04.11,sam6kor, Assigning new channel IDs for Procoedt on LINUX */
#define TR_TTFIS_FC_MAP3D         ((TR_tenTraceChan)98)
#define TR_TTFIS_MAPENGINE        ((TR_tenTraceChan)99)
#define TR_TTFIS_NAVRES1          ((TR_tenTraceChan)100)
#define TR_TTFIS_NAVRES2          ((TR_tenTraceChan)101)
#define TR_TTFIS_NAVRES3          ((TR_tenTraceChan)102)
#define TR_TTFIS_NAVRES4          ((TR_tenTraceChan)103)
#define TR_TTFIS_NAVRES5          ((TR_tenTraceChan)104)
#define TR_TTFIS_NAVRES6          ((TR_tenTraceChan)105)
#define TR_TTFIS_VOICECONTROL     ((TR_tenTraceChan)106)
#define TR_TTFIS_TTS              ((TR_tenTraceChan)107)
#define TR_TTFIS_RECOGNIZER       ((TR_tenTraceChan)108)
#define TR_TTFIS_PROMPTPLAYER     ((TR_tenTraceChan)109)
#define TR_TTFIS_SPEECHDATAPROVIDER ((TR_tenTraceChan)110)
#define TR_TTFIS_VOICERECORDER    ((TR_tenTraceChan)111)
#define TR_TTFIS_ODI_CSM          ((TR_tenTraceChan)112)
#define TR_TTFIS_AUDIO_ROUTER_MGMT ((TR_tenTraceChan)113) /* for Nissan LCN2Kai, dhd3kor, <22-Aug-2012>*/
#define TR_TTFIS_CSM_USERMODE     ((TR_tenTraceChan)114)
#define TR_TTFIS_DABTUNER         ((TR_tenTraceChan)115)
#define TR_TTFIS_TTPROF           ((TR_tenTraceChan)116)
#define TR_TTFIS_AUDARBITMA       ((TR_tenTraceChan)117)
#define TR_TTFIS_AV_TEST_SERVER   ((TR_tenTraceChan)118)
#define TR_TTFIS_MEMTRACE_BASE    ((TR_tenTraceChan)120) /* Channels for memory traces for processes  */ 
#define TR_TTFIS_MEMTRACE_NAV     ((TR_tenTraceChan)121)
#define TR_TTFIS_MEMTRACE_SBS     ((TR_tenTraceChan)122)
#define TR_TTFIS_MEMTRACE_HMI     ((TR_tenTraceChan)123)
#define TR_TTFIS_MEMTRACE_MAP     ((TR_tenTraceChan)124)
#define TR_TTFIS_MEMTRACE_MM      ((TR_tenTraceChan)125)
#define TR_TTFIS_LINUX_OSALCORE   ((TR_tenTraceChan)126) /*for Nissan LCN2Kai, jov1kor, <26-Feb-2013>*/
#define TR_TTFIS_LINUX_OSALIO     ((TR_tenTraceChan)127) /*for Nissan LCN2Kai, jov1kor, <26-Feb-2013>*/
#define TR_TTFIS_MEDIAMANAGER     ((TR_tenTraceChan)128) /* merged from tri_types.h for VWTRTN, dhd3kor, <11-Jan-2012>*/
#define TR_TTFIS_CCATRACE_P0      ((TR_tenTraceChan)130) /* Channels for CCA traces for processes  */ 
#define TR_TTFIS_CCATRACE_P1      ((TR_tenTraceChan)131)
#define TR_TTFIS_DATAPOOL         ((TR_tenTraceChan)132) /* New component for G3G[MIB2_ENTRY], jov1kor, <22-Aug-2013>*/
#define TR_TTFIS_FC_LANGUAGE_CHANGE ((TR_tenTraceChan)133) /* New component for G3G[MIB2_ENTRY], jov1kor, <13-Aug-2013>*/
#define TR_TTFIS_CCA_INC_ADAPTER  ((TR_tenTraceChan)134) /* New component for G3G, jov1kor, <18-Jun-2013>*/
#define TR_TTFIS_FC_TPEG          ((TR_tenTraceChan)135) /* for Nissan LCN2Kai, jov1kor, <21-Mar-2013>*/
#define TR_TTFIS_SDREFRESH        ((TR_tenTraceChan)136) /* for Nissan LCN2Kai, dhd3kor, <11-Jan-2013>*/
#define TR_TTFIS_AUDIOMANAGER     ((TR_tenTraceChan)137) /* for Nissan LCN2Kai, dhd3kor, <26-Sep-2012>*/
#define TR_TTFIS_FC_USB_TCU       ((TR_tenTraceChan)138)  /*for Gen3 , pik5kor, <23-SEP-2015>, Requested by Mani Arjunan (RBEI/ECV1) */
#define TR_TTFIS_FC_AUX           ((TR_tenTraceChan)139) /* For Gen3, jov1kor, <09-Apr-2014> */
#define TR_TTFIS_CSFWTRACE_P0     ((TR_tenTraceChan)140) /* Channels for CSFW traces for processes  */ 
#define TR_TTFIS_CSFWTRACE_P1     ((TR_tenTraceChan)141)
#define TR_TTFIS_FCSWUPDATE       ((TR_tenTraceChan)142) /* FC SW update for G3G, jov1kor, <08-Nov-2013>,Requested by Hassman Peter*/
#define TR_TTFIS_KDS              ((TR_tenTraceChan)143) /* KDS for G3G, jov1kor, <24-Sep-2013>,Requested by Buter Andrea */
#define TR_TTFIS_PROFILER         ((TR_tenTraceChan)144) /* FC Profiler for G3G, jov1kor, <24-Sep-2013>,Requested by Stresing Christian */
#define TR_TTFIS_DEV_WDG          ((TR_tenTraceChan)145) /* Dev WDG for G3G, jov1kor, <03-Sep-2013>,Requested by KalmsMartin*/
#define TR_TTFIS_DEV_WUP          ((TR_tenTraceChan)146) /* Dev WUP for G3G, jov1kor, <03-Sep-2013>,Requested by KalmsMartin*/
#define TR_TTFIS_PDD              ((TR_tenTraceChan)147) /* New component for G3G[MIB2_ENTRY], jov1kor, <22-Aug-2013>*/
#define TR_TTFIS_SBX              ((TR_tenTraceChan)148)
#define TR_TTFIS_FC_TCU           ((TR_tenTraceChan)149) /* TelematicControlUnit for Nissan LCN2Kai, dhd3kor, <22-Aug-2012>*/
#define TR_TTFIS_BROWSER          ((TR_tenTraceChan)150)
#define TR_TTFIS_DEV_DIAGEOL      ((TR_tenTraceChan)151)
#define TR_TTFIS_DEV_VOLT         ((TR_tenTraceChan)152) /* Dev volt for G3G, jov1kor, <03-Sep-2013>,Changed comp. confirmed by KalmsMartin*/
#define TR_TTFIS_RVC              ((TR_tenTraceChan)154) /* merged from tri_types.h for VWTRTN, dhd3kor, <11-Jan-2012>*/
#define TR_TTFIS_THERMALMANAGEMENT  ((TR_tenTraceChan)157)
#define TR_TTFIS_DEMO_SERVER      ((TR_tenTraceChan)158) /* 15.03.08, hem2hi, added for ccademoserver */
#define TR_TTFIS_ADASIS           ((TR_tenTraceChan)159) /* vd adasis  */ 
#define TR_TTFIS_PERFANALYSIS     ((TR_tenTraceChan)160)
#define TR_TTFIS_RADIODATA        ((TR_tenTraceChan)161) /*As per MMS 174845 */ 
#define TR_TTFIS_LOGBOOK          ((TR_tenTraceChan)162)
#define TR_TTFIS_PDIM             ((TR_tenTraceChan)163) /* personal device */    
#define TR_TTFIS_LVDS             ((TR_tenTraceChan)164) /* LVDS */
#define TR_TTFIS_FLASHFX          ((TR_tenTraceChan)165) /* LFX */
#define TR_TTFIS_REMOTE_DEVICES   ((TR_tenTraceChan)166) /* channel name = "Remote Devices ", sam5hi, <03.11.2010>*/
#define TR_TTFIS_CARD_CRYPT       ((TR_tenTraceChan)168) /* channel name = "fd crypt ",sam5hi, <19.01.2011>*/
#define TR_TTFIS_PHONE_VAG        ((TR_tenTraceChan)170) /* merged from tri_types.h for VWTRTN, dhd3kor, <11-Jan-2012>*/
#define TR_TTFIS_FC_LBS           ((TR_tenTraceChan)171) /* channel name = "fC LBS ",sam5hi, <27.04.2011>*/
#define TR_TTFIS_SDS_TRANS_SERV   ((TR_tenTraceChan)172) /* channel name = "SDS_TRANS_SERV ",sam5hi, <09.05.2011>*/
#define TR_TTFIS_RESIDENT_UNIT    ((TR_tenTraceChan)173) /* channel name = "Resident Unit ", sam5hi, <25.05.2011>*/
#define TR_TTFIS_DEV_INFO         ((TR_tenTraceChan)174) /* channel name = "Device info", dhd3kor, <28-Mar-2012>*/
#define TR_TTFIS_FC_APP           ((TR_tenTraceChan)175) /* merged from tri_types.h for VWTRTN, dhd3kor, <11-Jan-2012>*/
#define TR_TTFIS_FFD_LI           ((TR_tenTraceChan)176) /* channel name = "FFD Linux", dhd3kor, <28-Mar-2012>*/
#define TR_TTFIS_FC_UPS           ((TR_tenTraceChan)177) /* merged from tri_types.h for VWTRTN, dhd3kor, <02-Apr-2012>*/
#define TR_TTFIS_SMARTPHONE_INT   ((TR_tenTraceChan)178) /* for Nissan LCN2Kai, dhd3kor, <24-May-2012>*/
#define TR_TTFIS_MC_MEDIAPLAYER   ((TR_tenTraceChan)179) /* for Nissan LCN2Kai, dhd3kor, <12-Jun-2012>*/

#define TR_TTFIS_CSFW_RX          ((TR_tenTraceChan)204)
#define TR_TTFIS_CSFW_TX          ((TR_tenTraceChan)205)

#define TR_TTFIS_ACL_MEDIA        ((TR_tenTraceChan)256) /* for Gen3 PSA, jov1kor, <29-Oct-2013> + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
#define TR_TTFIS_ACL_TUNER        ((TR_tenTraceChan)257) /* for Gen3 PSA, jov1kor, <29-Oct-2013> + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
#define TR_TTFIS_AUDIO_MASTER     ((TR_tenTraceChan)258) /* for Gen3 PSA, jov1kor, <29-Oct-2013> + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
#define TR_TTFIS_ACL_CONNECTIVITY  ((TR_tenTraceChan)259) /* for Gen3 PSA, jov1kor, <29-Oct-2013> + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
#define TR_TTFIS_FC_PARKASSIST    ((TR_tenTraceChan)260) /* for Gen3 VW-MIB, jov1kor, <29-Nov-2013> + Edited the name on <24-Mar-2015>*/
#define TR_TTFIS_AUDIOSTACK       ((TR_tenTraceChan)261) /* for Gen3 VW-MIB, jov1kor, <29-Nov-2013>*/
#define TR_TTFIS_MOST_K2LD        ((TR_tenTraceChan)262) /* for Gen3 VW-MIB, jov1kor, <12-Dec-2013>*/
#define TR_TTFIS_MOST_MCS         ((TR_tenTraceChan)263) /* for Gen3 VW-MIB, jov1kor, <12-Dec-2013>*/
#define TR_TTFIS_SECURITYCTRL     ((TR_tenTraceChan)264) /* for Gen3 VW-MIB, jov1kor, <11-Mar-2014>,Requested by Medinas Luis (CM-AI/EPB1)*/
#define TR_TTFIS_PASC_CONNMAN     ((TR_tenTraceChan)265) /* for Gen3 PSA, jov1kor, <24-Mar-2014>,Requested by Seema S R (RBEI/ECP3)*/
#define TR_TFFIS_FC_CAREASYAPPS	  ((TR_tenTraceChan)266) /* for Gen3 PSA CEA, kap3kor, <14-Sep-2016>, PSARCC-13503 Requested by Bhatt Sonam (RBEI/ECP42) */
#define TR_TFFIS_FC_ONSTAR		  ((TR_tenTraceChan)267) /* for Gen3 PSA OnStar, kap3kor, <20-Sep-2016>, PSARCC-13514 Requested by Siva Sankaran Arivarasan (RBEI/ECP41) */
#define TR_TTFIS_FC_ATB           ((TR_tenTraceChan)268) /* for Gen3 PSA, jov1kor, <24-Mar-2014>,Requested by Seema S R (RBEI/ECP3) + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
#define TR_TTFIS_CDAUDIO          ((TR_tenTraceChan)269) /* for Gen3 LSIM, jov1kor, <22-Apr-2014>,Requested by Gopinathan Subu (RBEI/ECF5)*/
#define TR_TTFIS_PASC_SMARTPHONEINT ((TR_tenTraceChan)270) /* for Gen3 PSA, jov1kor, <22-Apr-2014>,Manishi Kumari (RBEI/ECP3)*/
#define TR_TTFIS_FC_DATASERVICES  ((TR_tenTraceChan)271) /* for Gen3, jov1kor, <29-Apr-2014>,Requested by Nagaraj Gopalakrishna (RBEI/ECV2)*/
#define TR_TTFIS_FC_CDPLAYER      ((TR_tenTraceChan)272) /* for Gen3 PSA, jov1kor, <29-Apr-2014>,Requested by Panduranga Pai Ballambettu (RBEI/ECP3) */
#define TR_TTFIS_FLYIN_APP_LIBBRARY  ((TR_tenTraceChan)273) /* for Gen3 SLN, jov1kor, <21-May-2014>,Requested by Shrikant Ramappa Idli (RBEI/ECV1)*/
#define TR_TTFIS_FC_VPAS      	  ((TR_tenTraceChan)274) /* for Gen3 PSA, pik5kor, <17-JUN-2014>,Requested by Panduranga Pai Ballambettu (RBEI/ECP3) + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
#define TR_TTFIS_EAMP_PLUGIN      ((TR_tenTraceChan)275) /* for Gen3 PSA, pik5kor, <20-JUN-2014>,Requested by Sandip Kumar Munda (RBEI/ECP3) */

/* 
 * Below channel IDs are unique for HMI components in Gen3 PSA&SUZUKI
 * jov1kor, <26-Jun-2014>,Requested by Hessling Matthias (CM-AI/ECB4) 
 */
#define TR_TTFIS_UI_APP_HMI_MASTER     ((TR_tenTraceChan)276)
#define TR_TTFIS_UI_APP_HMI_PLAYGROUND ((TR_tenTraceChan)277)
#define TR_TTFIS_UI_APP_HMI_01         ((TR_tenTraceChan)278)
#define TR_TTFIS_UI_APP_HMI_02         ((TR_tenTraceChan)279)
#define TR_TTFIS_UI_APP_HMI_03         ((TR_tenTraceChan)280)
#define TR_TTFIS_UI_APP_HMI_04         ((TR_tenTraceChan)281)
#define TR_TTFIS_UI_APP_HMI_05         ((TR_tenTraceChan)282)
#define TR_TTFIS_UI_APP_HMI_06         ((TR_tenTraceChan)283)
#define TR_TTFIS_UI_APP_HMI_07         ((TR_tenTraceChan)284)
#define TR_TTFIS_UI_APP_HMI_08         ((TR_tenTraceChan)285)
#define TR_TTFIS_UI_APP_HMI_09         ((TR_tenTraceChan)286)
#define TR_TTFIS_UI_APP_HMI_10         ((TR_tenTraceChan)287)
#define TR_TTFIS_UI_APP_HMI_11         ((TR_tenTraceChan)288)
#define TR_TTFIS_UI_APP_HMI_12         ((TR_tenTraceChan)289)
#define TR_TTFIS_UI_APP_HMI_13         ((TR_tenTraceChan)290)
#define TR_TTFIS_UI_APP_HMI_14         ((TR_tenTraceChan)291)
#define TR_TTFIS_UI_APP_HMI_15         ((TR_tenTraceChan)292)
#define TR_TTFIS_UI_APP_HMI_16         ((TR_tenTraceChan)293)
#define TR_TTFIS_UI_APP_HMI_17         ((TR_tenTraceChan)294)
#define TR_TTFIS_UI_APP_HMI_18         ((TR_tenTraceChan)295)

#define TR_TTFIS_FC_CLIMATE            ((TR_tenTraceChan)296) /* for Gen3 , pik5kor, <15-JUL-2014>,Requested by Jagdish Thanaram Jojawar (RBEI/ECP3)  */
#define TR_TTFIS_FC_ENG_MODE           ((TR_tenTraceChan)297) /* for Gen3 PSA, pik5kor, <25-JUL-2014>,Requested by Liju Prasanth Nivas (RBEI/ECA1)  */
#define TR_TTFIS_ACL_PARKING           ((TR_tenTraceChan)298) /* for Gen3 PSA, pik5kor, <01-SEP-2014>,Requested by Pankaj Dubey (RBEI/ECP4)  , + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
#define TR_TTFIS_FC_HIT                ((TR_tenTraceChan)299) /* for Gen3, pik5kor, <16-SEP-2014>, Requested by Priju K Padiyath (RBEI/ECP2,CM/EST-NA)  */
#define TR_TTFIS_ACL_AUDIO             ((TR_tenTraceChan)300) /* for Gen3, pik5kor, <19-SEP-2014>, Requested by Naveen Kumar Haalenahalli Narasimhamurthy (RBEI/ECP4)  */
#define TR_TTFIS_FC_VP1                ((TR_tenTraceChan)301)  /* for Gen3, pik5kor, <27-SEP-2014>, Requested by Saravana Raja (RBEI/ECP4).  */
#define TR_TTFIS_SWUPDATE_CTRL         ((TR_tenTraceChan)302)  /* for Gen3, pik5kor, <27-OCT-2014>, Requested by Landsvogt Torsten (CM-AI/ECA2)  */
#define TR_TTFIS_ACL_OBC               ((TR_tenTraceChan)303)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_ACL_SETTINGS          ((TR_tenTraceChan)304)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_ACL_DRIVE             ((TR_tenTraceChan)305)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_ACL_SPEED             ((TR_tenTraceChan)306)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_FC_VEHICLEFUNCMASTER  ((TR_tenTraceChan)307)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_FC_OBC                ((TR_tenTraceChan)308)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_FC_EXPORTBROKER       ((TR_tenTraceChan)309)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_ACL_ALERT             ((TR_tenTraceChan)310)  /* for Gen3, pik5kor, <04-DEC-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_ACL_MULTIDRIVE        ((TR_tenTraceChan)311)  /* for Gen3, pik5kor, <04-DEC-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_ACL_HYBRID            ((TR_tenTraceChan)312)  /* for Gen3, pik5kor, <04-DEC-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
#define TR_TTFIS_ACL_PHONE             ((TR_tenTraceChan)313)   /* for Gen3 ACL, pik5kor, <23-Jan-2015>, Requested by Sayantani Khan (RBEI/ECP4)*/
#define TR_TTFIS_ACL_UPDATE            ((TR_tenTraceChan)314)   /* for Gen3 PSA RCC, pik5kor, <9-Jan-2014>,Requested by Shaju Paul Paulson (RBEI/ECA1) */
#define TR_TTFIS_GENIVIAUDIO     	   ((TR_tenTraceChan)315)   /* for Gen3 FB_AVManager, pik5kor, <14-Jan-2014>,Requested by EXTERNAL Rey Patrick (Fa. ESE, CM-AI/ECB3) */
#define TR_TTFIS_FC_HYBRID      	   ((TR_tenTraceChan)316)   /* for Gen3 PSA RCC, pik5kor, <28-Jan-2015>,Requested by Ankur Gadre(RBEI/ECP4) */
#define TR_TTFIS_FCOTA          	   ((TR_tenTraceChan)317)   /* for Gen3 FC, pik5kor, <02-Feb-2015>, Requested by Hassmann Peter (CM-AI/EPB1) */
#define TR_TTFIS_ACL_BRIGHTNESS    	   ((TR_tenTraceChan)318)   /* for Gen3 ACL, pik5kor, <17-Feb-2015>, Requested by Sugumaran Balasubramanian (RBEI/ECA3) */
#define TR_TTFIS_APPINFRA_KEYHANDLER   ((TR_tenTraceChan)319)   /* for Gen3 PSA RCC, jov1kor, <25-Feb-2015>, Requested by Alekhya Govindu (RBEI/ECP4) */
#define TR_TTFIS_APPINFRA_USERMGMT     ((TR_tenTraceChan)320)   /* for Gen3 , pik5kor, <03-Mar-2015>, Requested by Flemming Reiner (CM-AI/EPD1) */
#define TR_TTFIS_APPINFRA_APPCTRL      ((TR_tenTraceChan)321)   /* for Gen3 , pik5kor, <03-Mar-2015>, Requested by Flemming Reiner (CM-AI/EPD1) */
#define TR_TTFIS_FC_ALERT              ((TR_tenTraceChan)322)   /* for Gen3 , pik5kor, <23-Mar-2015>, Requested by Sripooja Sivadanam (RBEI/ECP4) */
#define TR_TTFIS_APPINFRA_SCREENTEST   ((TR_tenTraceChan)323)   /* for Gen3 PSA-RCC, pik5kor, <26-Mar-2015>, Requested by Pravalli Sure (RBEI/ECP4)  */
#define TR_TTFIS_FC_RSECTRL            ((TR_tenTraceChan)324)   /* for Gen3 NCG3, pik5kor, <27-Mar-2015>, Requested by EXTERNAL Schurig Thomas */
#define TR_TTFIS_FC_USB                ((TR_tenTraceChan)325)   /* for Gen3 PSA-RCC , pik5kor, <30-Mar-2015>, Requested by Vinod M Subrahmanian (RBEI/ECA3) */
#define TR_TTFIS_FC_SERVICEMGR         ((TR_tenTraceChan)326)   /* for Gen3 PSA-RCC, pik5kor, <31-Mar-2015>, Requested by Seema S R (RBEI/ECP4) */
#define TR_TTFIS_DATACOLLECTOR         ((TR_tenTraceChan)327)   /* for Gen3 Middleware, pik5kor, <31-Mar-2015>, Praveen Ashok Kumar (RBEI/ECV1) */
#define TR_TTFIS_FC_CONNECT            ((TR_tenTraceChan)328)   /* for Gen3 , pik5kor, <29-May-2015>, Seema S R (RBEI/ECP4).*/
#define TR_TTFIS_VD_EARLY              ((TR_tenTraceChan)329)   /* for Gen3 , pik5kor, <06-JUL-2015>, Lisney Ian (CM-AI/EPB2) */
#define TR_TTFIS_VIDEOPLAYER           ((TR_tenTraceChan)330)   /* for Gen3 , pik5kor, <06-JUL-2015>, Lisney Ian (CM-AI/EPB2) */
#define TR_TTFIS_APPINFRA_DISPLAYMANAGER    ((TR_tenTraceChan)331)   /* for Gen3 , pik5kor, <06-JUL-2015>, George Jose Mattamana (RBEI/ECP1). */
#define TR_TTFIS_FC_LOCINPUT           ((TR_tenTraceChan)332)   /* for Gen3 FC, pik5kor, <11-JUL-2015>, Andres Sascha (CM/ESN2). */
#define TR_TTFIS_FC_EXT_DRV            ((TR_tenTraceChan)333)   /* for Gen3 FC, pik5kor, <23-JUL-2015>, EXTERNAL Schurig Thomas (Fa. Brunel, CM-AI/EPB2) . */
#define TR_TTFIS_VD_PV                 ((TR_tenTraceChan)334)   /* for Gen3, pik5kor, <28-JUL-2015>, Sanjay Gurugubelli (RBEI/ECF5). */
#define TR_TTFIS_FC_SMARTAPP           ((TR_tenTraceChan)335)   /* for Gen3 FC, pik5kor, <05-AUG-2015>, Sonam Bhatt (RBEI/ECP4). */
#define TR_TTFIS_FC_SPEECH_AUDIO       ((TR_tenTraceChan)336)   /* for Gen3 FC, pik5kor, <12-AUG-2015>, EXTERNAL Schurig Thomas (Fa. Brunel, CM-AI/EPB2). */
#define TR_TTFIS_TRACE_ADR 		       ((TR_tenTraceChan)337)   /* for Gen3 , pik5kor, <08-OCT-2015>, Raghavendra Nannuru Vutkuru (RBEI/ECV2). */
#define TR_TTFIS_FC_MAPUPDATE          ((TR_tenTraceChan)338)   /* for Gen3 FC, pik5kor, <15-OCT-2015>, Wiedemann Frank (CM/ESN1). */
#define TR_TTFIS_SWITCHCTRL            ((TR_tenTraceChan)340)   /* for Gen3, pik5kor, <24-NOV-2015>, Gupta Sandeep (RBEI/ECV3) . */
#define TR_TTFIS_FD_POWERMASTER        ((TR_tenTraceChan)341)   /* for Gen3, pik5kor, <10-DEC-2015>, Pooja Krishnan (RBEI/ECF5). */
#define TR_TTFIS_FD_SUPPLYMGMT         ((TR_tenTraceChan)342)   /* for Gen3, pik5kor, <10-DEC-2015>, Pooja Krishnan (RBEI/ECF5). */
#define TR_TTFIS_FD_THERMALMGMT        ((TR_tenTraceChan)343)   /* for Gen3, pik5kor, <10-DEC-2015>, Pooja Krishnan (RBEI/ECF5). */
#define TR_TTFIS_EXTNAV                ((TR_tenTraceChan)344)   /* for Gen3, pik5kor, <02-FEB-2016>, Ugesh Munirajulu (RBEI/ECA4) */
#define TR_TTFIS_FC_HOSTAPD            ((TR_tenTraceChan)345)   /* for Gen3 PSA RCC, pik5kor, <15-MAR-2016>, Mommed Rafiq (RBEI/ECP4). */
#define TR_TTFIS_MEDIAENGINE           ((TR_tenTraceChan)346)   /* for Gen3 generic, pik5kor,haa7kor, <07-JUL-2016>, Pieper Stephan (CM/ESC2). */
#define TR_TTFIS_LCMDBUSCLIENT         ((TR_tenTraceChan)347)   /* for Gen3 LCM DBus client, kap3kor, <06-Oct-2016>, CFG3-2064 Requested by Rossner Michael (CM/ESO3) */
#define TR_TTFIS_FC_DTV                ((TR_tenTraceChan)348)   /* for Gen3 FC_DTV, kap3kor, <25-Oct-2016>, AIVI-34183 Requested by Behera Naba Kishore (RBEI/ECV2) */
#define TR_TTFIS_FC_DUMM               ((TR_tenTraceChan)349)   /* for Gen3 FC_DUMM, kap3kor, <02-Nov-2016>, AIVI-34673 Requested by Praveen Surendra Badsheshi (RBEI/ECV13) */
#define TR_TTFIS_FC_AUTO_DRIVE         ((TR_tenTraceChan)350)   /* for Gen3 FC_AUTO_DRIVE, haa7kor, <05-Dec-2016>, AIVI-38614 Requested by Schurig Thomas (CM-CI1/ERN4-E) */
#define TR_TTFIS_FC_GATEWAY            ((TR_tenTraceChan)351)   /* for Gen3 FC_GATEWAY, haa7kor, <05-Dec-2016>, PSARCC-14503 Requested by Akruthi Krishna (RBEI/ECP42) */
#define TR_TTFIS_ACL_WEBPORTAL         ((TR_tenTraceChan)352)   /* for Gen3 ACL_WEBPORTAL, haa7kor, <05-Dec-2016>, PSARCC-14504 Requested by Akruthi Krishna (RBEI/ECP42) */
#define TR_TTFIS_FC_VIDMGR             ((TR_tenTraceChan)353)   /* for Gen3 FC_VIDMGR, haa7kor, <07-Dec-2016>, AIVI-38790 Requested by Schurig Thomas (CM-CI1/ERN4-E)*/
/*****************************************************************************
 *****************************************************************************
 *                    Please do not add any new Channels                       
 *   Please contact Trenkler Lutz(CM-AI/PJ-CF33) for adding new channels  
 *****************************************************************************
 *****************************************************************************/
    



/*****************************************************************************
 * Definitions for trace class
 * =============================
 *  
 * A class definition describes a 16 bit value, which has two parts
 *
 * [  Component - ID   ][   Trace-Class-ID  ]
 * [15                9][8                 0]
 *
 *
 * The component describes for which SW-parts the trace classes are defined for. 
 * For each component 2^8 = 256 classes are possible. 
 *
 *   +---------------+---------------+-------------------------------+
 *   | Component ID  | Purpose       | Comments                      |
 *   +---------------+---------------+-------------------------------+
 *   | 0-12          | Trace / System| historical reasons            |
 *   +---------------+---------------+-------------------------------+
 *   | 13-179        | Mother        | Reserved Component IDs for MCs|
 *   |               | Companies     |                               |
 *   +---------------+---------------+-------------------------------+
 *   | 180-255       | ADIT          | product relevant              | 
 *   +---------------+---------------+-------------------------------+
 *
 *   Please comment each update with the following template:
 *   <packagename>, <class id range>,<date of entry>
 ******************************************************************************/

/* +----------------+
   | TRACE / System |
   +----------------+ */ 


 #define TR_COMP_COUNT                (40)
 #define TR_MAX_COMP_COUNT            (256)
 #define TR_COMP_MAX_CLASS_COUNT      (256)

 #define TR_COMP_GLOBAL               (256 * 4)      /* componetname = "TRACE" pas2hi, 16.06.03 (new) */
 #define TR_COMP_LIB                  (256 * 8)      /* componentname = "TESTER" for MST testing */
 #define TR_COMP_TRACE                (256 * 12)     /* componetname = "TRACE" pas2hi, 16.06.03 (new) */
 #define TR_COMP_RBCM_GLOBAL          (256 * 13)     /* componentname = "RBCM_GLOBAL", ram2hi, 30.03.2010 */
 #define TR_COMP_PROF                 (256 * 17)     /* componentname = "PROFILER"  sec2hi, 16.06.03*/
 #define TR_COMP_PERF                 (256 * 18)     /* componentname = "PERF"       */
/*
 * Below component IDs are unique for HMI components in Gen3 PSA&SUZUKI
 * jov1kor, <26-Jun-2014>,Requested by Hessling Matthias (CM-AI/ECB4) 
 */ 
 #define TR_COMP_UI_APP_HMI_MASTER     ((256 * 21) +   0)
 #define TR_COMP_UI_APP_HMI_PLAYGROUND ((256 * 21) +  64)
 #define TR_COMP_UI_APP_HMI_01         ((256 * 21) + 128) 
 #define TR_COMP_UI_APP_HMI_02         ((256 * 21) + 192)
 #define TR_COMP_UI_APP_HMI_03         ((256 * 22) +   0)
 #define TR_COMP_UI_APP_HMI_04         ((256 * 22) +  64)
 #define TR_COMP_UI_APP_HMI_05         ((256 * 22) + 128) 
 #define TR_COMP_UI_APP_HMI_06         ((256 * 22) + 192)
 #define TR_COMP_UI_APP_HMI_07         ((256 * 23) +   0)
 #define TR_COMP_UI_APP_HMI_08         ((256 * 23) +  64)
 #define TR_COMP_UI_APP_HMI_09         ((256 * 23) + 128) 
 #define TR_COMP_UI_APP_HMI_10         ((256 * 23) + 192)

 /*OSAL*/
 #define TR_COMP_OSALCORE             (256 * 24)     /* componentname = "OSALCORE   */
 #define TR_COMP_OSALIO               (256 * 25)     /* componentname = "OSALIO     */
 #define TR_COMP_OSALTEST             (256 * 26)     /* componentname = "OSALTEST   */

 #define TR_COMP_UI_APP_HMI_11        ((256 * 27) +   0)
 #define TR_COMP_UI_APP_HMI_12        ((256 * 27) +  64)
 #define TR_COMP_UI_APP_HMI_13        ((256 * 27) + 128) 
 #define TR_COMP_UI_APP_HMI_14        ((256 * 27) + 192)

 /* CSM */
 #define TR_COMP_CSM                  (256 * 28)     /* componentname = "CSM       */
 #define TR_COMP_ANIMATION            (256 * 29)     /* for VWTRTN, dhd3kor, <23-Apr-2012>*/
 
 
 /* ELeNa */
 #define TR_COMP_SPM                  (256 * 30)	   /* componentname = "SPM"       */
 #define TR_COMP_SPI                  (256 * 31)     /* componentname = "SPI"       */
 #define TR_COMP_GATEWAY              (256 * 32)     /* componentname = "GATEWAY"   */
 #define TR_COMP_DIAGNOSIS            (256 * 33)     /* componentname = "DIAGNOSIS" */
 #define TR_COMP_DOWNLOAD             (256 * 34)     /* componentname = "DOWNLOAD"  */
 #define TR_COMP_DRVFUNC              (256 * 35)     /* componentname = "DRVFUNC"   */
 #define TR_COMP_CDVD                 (256 * 36)     /* componentname = "CDVD"      */
 #define TR_COMP_BOSCH_AUDIO          (256 * 37)     /* componentname = "AUDIO"     */
 #define TR_COMP_TUNER                (256 * 38)     /* componentname = "TUNER"     */
 #define TR_COMP_NAVIFUNC             (256 * 39)     /* componentname = "NAVIFUNC"  */
 #define TR_COMP_UI                   (256 * 40)     /* componentname = "UI"        */
 #define TR_COMP_SENSOR               (256 * 41)     /* componentname = "SENSOR"    */
  #define TR_COMP_SENSOR_SIMULATION	  ((256 * 41) + 128) /* Component Sensor Simulation application for LSIM, rmm1kor, 23-Sep-2015 */
 #define TR_COMP_VOICE                (256 * 42)     /* componentname = "VOICE"     */
 #define TR_COMP_CMG                  (256 * 43)     /* componentname = "CMG"       */
 #define TR_COMP_SDREFRESH            (256 * 44)     /* for Nissan LCN2kai, dhd3kor<11.Jan.2013>*/    
 #define TR_COMP_DISPLAY              (256 * 45)     /* componentname = "DISPLAY"   */
 #define TR_COMP_KEYBOARD             (256 * 46)     /* componentname = "KEYBOARD"  */
 #define TR_COMP_MCU                  (256 * 47)     /* componentname = "MCU"	   */
 #define TR_COMP_UMM                  (256 * 49)     /* componentname = "UMM"	   */
 #define TR_COMP_MP3D                 (256 * 50)     /* componentname = "MP3-VD"	   */
 #define TR_COMP_EMBEDDEDRADIOSW      (256 * 51)     /* componentname = "EMBEDDEDRADIOSW"*/
 #define TR_COMP_UC                   (256 * 53)     /* componentname = "UC"        */
 #define TR_COMP_DMM                  (256 * 54)     /* componentname = "DMM"      */
 #define TR_COMP_MOST_NSL             (256 * 55)     /* componentname = "MOST NSL"*/
 #define TR_COMP_NETWORKING           (256 * 55)     /* componentname = "NETWORKING" */
 /* As request from Detlef Hasenwinkel for the FLR/VCC/PAG Project 
   Networking should be a future comp ID for summarize networking components and functionality 
   like TR_COMP_MOST_NSL, TR_COMP_CSM, TR_COMP_CSM, TR_COMP_SPI, TR_COMP_DRVFUNC, TR_COMP_FD_CSM, TR_COMP_ODI, TR_COMP_MOST_NSL
   same ID as MOST_NSL which are not used in GMGE
   jhr2hi: 14.04.2008
 */
 
 #define TR_COMP_SCANSOFT_LIBRARY     (256 * 56)     /* component name = SCANSOFT_LIBRARY         */
 #define TR_COMP_AUDIOPLAYER          (256 * 57)     /* component name = AUDIOPLAYER        */
 #define TR_COMP_CDC                  (256 * 58)     /* component name = CDC       */
 #define TR_COMP_PHONE                (256 * 59)     /* component name = PHONE        */
 #define TR_COMP_CONNECTIVITY         (256 * 59)     /* component name = CONNECTIVITY        */ 
 /* As request from Detlef Hasenwinkel for the FLR/VCC/PAG Project 
   Connectivity should be a future comp ID for summarize Phone, IPOD and Bluetooth connected 
   components and functionality 
   same ID as PHONE which are not used in GMGE
   jhr2hi: 14.04.2008
 */
 #define TR_COMP_CLIMATECTRL          (256 * 60)  /* component name = CLIMATECTRL        */
 #define TR_COMP_REMOTEDISPLAY        (256 * 61)  /* component name = REMOTEDISPLAY         */
 #define TR_COMP_DRIVER_ASSIST_VIDEO  (256 * 62)	/* component name= "DRIVER ASSIST VIDEO", dhd2hi, <10-Jun-2010>*/
 #define TR_COMP_COMPRESSED_AUDIO     (256 * 63)  /* component name = COMPRESSED_AUDIO, kbm2hi 25-06-2007 */
 #define TR_COMP_MIDDLEWARE           (256 * 64)  /*component name = MIDDLEWARE */
 #define TR_COMP_ENTERTAINMENT        (256 * 64)  /*component name = ENTERTAINMENT */
 /* As request from Detlef Hasenwinkel for the FLR/VCC/PAG Project 
   Entertainment should be a future comp ID for summarize Entertainment/Middleware functionality 
   trace classes
   AmFm Tuner, Mediaplayer (iPod, USB, DVD, HDD, etc), TV, SDARS, DAB, AudioAux, AVAux 
   same ID as middleware which are not used in GMGE
   jhr2hi: 14.04.2008
 */
 
 #define TR_COMP_AMFM_AUD_PROXY       (256 * 65)     /* component name = AMFM_AUD_PROXY        */
 #define TR_COMP_TOUCH                (256 * 66)     /* component name = TOUCH        */
 #define TR_COMP_DIMMING              (256 * 67)     /* component name = DIMMING         */
 #define TR_COMP_CLOCK                (256 * 68)     /* component name = CLOCK        */
 #define TR_COMP_MEDIAMANAGER         (256 * 69)     /* component name = MEDIAMANAGER       */
 #define TR_COMP_RESOURCEMANAGEMENT   (256 * 69)     /*component name = ENTERTAINMENT */
 /* As request from Detlef Hasenwinkel for the FLR/VCC/PAG Project 
   Resource Management should be a future comp ID for summarize resource management functionality 
   trace classes
   Audio-, Video-, Connection Verwaltung, Applications, Error Management, Network und Power Master,     ContentProtection Manager
   same ID as mediamanager which are not used in GMGE
   jhr2hi: 14.04.2008
 */
 
 
 /* FGS components. Names to be changed sep 12, 2005*/
 #define TR_COMP_SAAL                 (256 * 70)     /* component name = SALL         */
 #define TR_COMP_FGS_MAP              (256 * 71)     /* component name = FGS_MAP         */
 #define TR_COMP_FGS_GRAPHIC          (256 * 72)     /* component name = FGS_GRAPHIC        */
 #define TR_COMP_FGS_SYSTEM           (256 * 73)     /* component name = FGS_SYSTEM         */
 #define TR_COMP_FGS_VIRT_DEVS        (256 * 74)     /* component name = FGS_VIRT_DEVS         */
 #define TR_COMP_FGS_DRIVER           (256 * 75)     /* component name = FGS_DRIVER         */

 /*NAVI*/
 #define TR_COMP_OBN                  (256 * 76)     /* componentname = "OBN"   */
 /*
 * Below component IDs are unique for HMI components in Gen3 PSA&SUZUKI
 * jov1kor, <26-Jun-2014>,Requested by Hessling Matthias (CM-AI/ECB4) 
 */ 
 #define TR_COMP_UI_APP_HMI_15        ((256 * 78) +   0)
 #define TR_COMP_UI_APP_HMI_16        ((256 * 78) +  64)
 #define TR_COMP_UI_APP_HMI_17        ((256 * 78) + 128)
 #define TR_COMP_UI_APP_HMI_18        ((256 * 78) + 192)
 
 #define TR_COMP_NAVI                 (256 * 80)     /* componentname = "NAVI"      */
 #define TR_COMP_FCVD_PREDDRIV        (256 * 81)     /* component name= "FCVD PREDDRIV", dhd2hi, <17-Feb-2010>*/
 #define TR_COMP_VD_EHPROV            (256 * 82)     /* component name= "VD EHPROV", dhd2hi, <17-Feb-2010>*/
 #define TR_COMP_FC_RSECTRL           ((256 * 83) +  0)    /* for Gen3 NCG3, pik5kor, <27-Mar-2015>, Requested by EXTERNAL Schurig Thomas */
 #define TR_COMP_FC_USB               ((256 * 83) +  64)    /* for Gen3 PSA-RCC , pik5kor, <30-Mar-2015>, Requested by Vinod M Subrahmanian (RBEI/ECA3) */
 #define TR_COMP_FC_SERVICEMGR        ((256 * 83) +  96)     /* for Gen3 PSA-RCC, pik5kor, <31-Mar-2015>, Requested by Seema S R (RBEI/ECP4) */
 #define TR_COMP_DATACOLLECTOR        ((256 * 83) +  128)     /* for Gen3 Middleware, pik5kor, <31-Mar-2015>, Praveen Ashok Kumar (RBEI/ECV1) */
 #define TR_COMP_DAPI                 (256 * 84)		 /* componentname = "DAPI"      */
 #define TR_COMP_MC_MEDIAPLAYER       (256 * 87)     /* for LCN2Kai, dhd3kor, <11-Jun-2012>*/
 #define TR_COMP_TIMA                 ((256 * 88) +    0)  /* componentname = "TIMA"      */
 #define TR_COMP_FC_TRAFFIC           ((256 * 88) +   64)  /* for Nissan LCN2kai, jov1kor, <19-Mar-2013>*/
 #define TR_COMP_FC_CONNECT           ((256 * 88) +  128)  /* For Gen3, jov1kor, <25-Feb-2015>, Wunderlich Wolfgang (CM-AI/ECN2)*/
 #define TR_COMP_DATAPROVIDER         (256 * 89)		 /* componentname = "DATA PROVIDER", dhd2hi, <02-Aug-2010> */

 #define TR_COMP_WDB                  (256 * 90)         /* componentname = "WDB",for G3G, jov1kor, <29-Jul-2013> */ 
 #define TR_COMP_FCADASIS             (256 * 91)		 /* component name= "FC ADASIS", dhd2hi, <17-Feb-2010>*/
 #define TR_COMP_SINA                 (256 * 92)		 /* componentname = "SINA"      */
 #define TR_COMP_NAVIUTIL             ((256 * 93) +  0)		 /* component name= "NAVI UTIL", dhd2hi, <10-Jun-2010>*/
 #define TR_COMP_EXTNAV               ((256 * 93) + 32)		 /* for Gen3 PSA, pik5kor, <02-FEB-2016>, component name = extnavi, requested by Ugesh Munirajulu (RBEI/ECA4) */
 #define TR_COMP_STAT                 (256 * 94)     /* componentname = "STAT" */
 #define TR_COMP_TEA                  (256 * 95)     /* componentname = "TEA" */
 #define TR_COMP_NAVLIB               (256 * 96)		 /* componentname = "NAVLIB"      */
 #define TR_COMP_FACIA                ((256 * 97) + 0) 	 /* for AID, pik5kor, <09-Apr-2015>, Requested by Hessling Matthias (CM-AI/ECB4)  */
 #define TR_COMP_FC_MAP3D             (256 * 98)		 /* component name = "FC_MAP3D"	*/
 #define TR_COMP_MAPENGINE            (256 * 99)		 /* component name = "MidMAP_Engine"	*/

 // components NAVRES1 - NAVRES5 for future use (update of navigation for current system)
 #define TR_COMP_NAVRES1              (256 * 100)		  /* componentname = "NAVRES1"  sec2hi, 13.06.03    */
 #define TR_COMP_NAVRES2              (256 * 101)		  /* componentname = "NAVRES2"  sec2hi, 13.06.03    */
 #define TR_COMP_NAVRES3              (256 * 102)		  /* componentname = "NAVRES3"  sec2hi, 13.06.03    */
 #define TR_COMP_NAVRES4              (256 * 103)		  /* componentname = "NAVRES4"  sec2hi, 13.06.03    */
 #define TR_COMP_NAVRES5              (256 * 104)		  /* componentname = "NAVRES5"  sec2hi, 13.06.03    */
 #define TR_COMP_NAVRES6              (256 * 105)		  /* componentname = "NAVRES6"  sec2hi, 13.06.03    */

 //#define TR_COMP_SDS        (1024 * 6)      /* componentname = "SDS"(speech dialogue system)  */
 #define TR_COMP_VOICECONTROL         (256 * 106)    /* componentname = "VOICECONTROL"(speech dialogue system)   */
 #define TR_COMP_TTS                  (256 * 107)    /* componentname = "TTS"(text to speech)          */

//added by gar2hi on sep 7, 2005
 #define TR_COMP_RECOGNIZER           (256 * 108)    /* componentname = "RECOGNIZER    */
 #define TR_COMP_PROMPTPLAYER         (256 * 109)    /*component name = "PROMPTPLAYER"  */
 #define TR_COMP_SPEECHDATAPROVIDER   (256 * 110)    /*component name = "SPEECH DATA PROVIDER"  */
 #define TR_COMP_VOICERECORDER        (256 * 111)    /* component name = "VOICE RECORDER"  */
 #define TR_COMP_SDS_HELPCLASSES      (256 * 112)	 /* SDS Help Class G3G, jov1kor, <06-Mar-2014>,Requested by Schneider Joachim (BSOT/ENG)*/
 #define TR_COMP_SECURITYCTRL         (256 * 113)    /* component name = "FC Security" */
/* ket2hi 05.11.2008: add TR_COMP_VEHICLEFUNCTIONS with same ID as TR_COMP_VEHICLEDATACTRL for VC program
   This is wanted no mistake*/
 #define TR_COMP_VEHICLEDATACTRL      (256 * 114)
 #define TR_COMP_VEHICLEFUNCTIONS     (256 * 114)
 
 #define TR_COMP_AUDARBITMA           (256 * 115)     
 #define TR_COMP_ANALYSIS             ((256 * 116) + 0)     /* component name = "Analysis" */
 #define TR_COMP_ACL_ALERT            ((256 * 116) + 32)    /* for Gen3 , pik5kor, <04-DEC-2014>,Requested by Jagdish Thanaram Jojawar (RBEI/ECP3)  */
 #define TR_COMP_ACL_MULTIDRIVE       ((256 * 116) + 64)    /* for Gen3 , pik5kor, <04-DEC-2014>,Requested by Jagdish Thanaram Jojawar (RBEI/ECP3)  */
 #define TR_COMP_ACL_HYBRID           ((256 * 116) + 96)    /* for Gen3 , pik5kor, <04-DEC-2014>,Requested by Jagdish Thanaram Jojawar (RBEI/ECP3)  */
 #define TR_COMP_ACL_PHONE            ((256 * 116) + 128)    /* for Gen3 , pik5kor, <23-JAN-2014>, Requested by Sayantani Khan (RBEI/ECP4)*/
	
 #define TR_COMP_FC_CLIMATE           ((256 * 117) + 0)    /* for Gen3 , pik5kor, <15-JUL-2014>,Requested by Jagdish Thanaram Jojawar (RBEI/ECP3)  */
 #define TR_COMP_FC_ENG_MODE          ((256 * 117) + 64)    /* for Gen3 PSA, pik5kor, <25-JUL-2014>,Requested by Liju Prasanth Nivas (RBEI/ECA1) */
 #define TR_COMP_FC_HIT               ((256 * 117) + 128)   /* New component for G3G, pik5kor, <16-SEP-2014> Requested by Priju K Padiyath (RBEI/ECP2,CM/EST-NA) */
 #define TR_COMP_ACL_AUDIO            ((256 * 117) + 192)   /* New component for G3G, pik5kor, <19-Sep-2014>*/ 
 #define TR_COMP_BROWSER              (256 * 118)    		/* component name = "DCM_Browser"  added by ket2hi 19.11.2007*/
 #define TR_COMP_BROWSER_MEDIA        ((256 * 118) + 128)  /* Volvo MCA project, added by dhd3kor, <05.10.2011>*/
 #define TR_COMP_BROWSER_OPERAMINI    ((256 * 118) + 192)  /* Volvo MCA project, added by dhd3kor, <05.10.2011>*/

 #define TR_COMP_INTERACTIONLOGGER    (256 * 119)    /* component name = "INTERACTIONLOGGER"  added by Sam5hi 16.03.2011*/
 #define TR_COMP_AV_TEST_SERVER       (256 * 120)    /* component name = "AV_TEST_SERVER"  added by Sam5hi 16.03.2011*/
 #define TR_COMP_FC_DABTUNER          (256 * 121)	 /* component name = "DABTUNER LCN2kai" added by jov1kor 09.07.2012*/
 #define TR_COMP_ACL_PARKING          ((256 * 122) + 0)   /* for Gen3 PSA, pik5kor, <01-SEP-2014>, Pankaj Dubey (RBEI/ECP4) , + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
 #define TR_COMP_FC_VP1               ((256 * 122) + 64)   /* for Gen3 PSA, pik5kor, <27-SEP-2014>, Saravana Raja (RBEI/ECP4) */
 #define TR_COMP_ACL_BASECLASS        ((256 * 122) + 128)  /* for Gen3 PSA RCC, jov1kor, <23-Feb-2015>, Flemming Reiner (CM-AI/EPD1)*/
 #define TR_COMP_APPINFRA_USERMGMT    ((256 * 122) + 160)  /* for Gen3 PSA RCC, jov1kor, <23-Feb-2015>, Flemming Reiner (CM-AI/EPD1)*/
 #define TR_COMP_APPINFRA_APPCTRL     ((256 * 122) + 192)  /* for Gen3 PSA RCC, jov1kor, <23-Feb-2015>, Flemming Reiner (CM-AI/EPD1)*/
 #define TR_COMP_APPINFRA_KEYHANDLER  ((256 * 122) + 224)  /* for Gen3 PSA, jov1kor, <25-Feb-2015>, Alekhya Govindu (RBEI/ECP4)*/
 #define TR_COMP_DCM_AMFMTUNER        (256 * 123)    /* componentname = "DCM_AMFMTUNER" */
 #define TR_COMP_TUNERMASTER          (256 * 125)

 #define TR_COMP_AUDIOMANAGEMENT      (256 * 127)		 /* componentname = "AudioManagement", 
                                                       prepare for change AudioManagement to 127, 
                                                       if all it ok, then change the name of this entry to
                                                       TR_COMP_AUDIOMGMT and delete old with number 112
                                                       */
 #define TR_COMP_AUDIO_ROUTER_MGMT    ((256*127) + 192) /* for LCN2Kai, dhd3kor, <22-Aug-2012>*/
 
 #define TR_COMP_RADIODATA            ((256 * 128) +   0)     /*As per MMS 174845*/
 #define TR_COMP_FCSWUPDATE           ((256 * 128) + 128)  /* FC SW update for G3G, jov1kor, <08-Nov-2013>,Requested by Hassman Peter*/
 #define TR_COMP_ODI                  (256 * 129)
 #define TR_COMP_GM_CUSTOMER          (256 * 130)		  /* component name= "GM Customer", dhd2hi, <28.07.2010>*/ 
 #define TR_COMP_SRVREC               (256 * 131)     /* component name = "ServiceRecorder", sam5hi, <11.05.2011>*/  
 #define TR_COMP_SRVREC_2             (256 * 132)     /* second byte to extend max. number of services from 256 => 512 */  
 
/* temporary addition needs to be discussed with Kalms, martin */
 #define TR_COMP_APPREC               (256 * 133)     /* componentname = "ApplicationRecorder" */  
 #define TR_COMP_APPREC_2             (256 * 134)     /* second byte to extend max. number of application from 256 => 512 */  
  
 #define TR_COMP_APP_ACT_SAFETY_DEMO  ((256 * 135) +   0) /* Volvo MCA project, added by dhd3kor, <05.10.2011>*/
 #define TR_COMP_FC_BTAUDIO           ((256 * 135) +  64) /* Volvo MCA project, added by dhd3kor, <05.10.2011>*/
 #define TR_COMP_APP_SPOTIFY          ((256 * 135) + 128) /* Volvo MCA project, added by dhd3kor, <05.10.2011>*/
 #define TR_COMP_TRAVEL_LINK          ((256 * 135) + 192) /* Volvo MCA project, added by dhd3kor, <05.10.2011>*/

 #define TR_COMP_VD_TEM               ((256 * 136) +  0)  /* Volvo MCA project, added by dhd3kor, <05.10.2011>*/
 #define TR_COMP_APP_CSB              ((256 * 136) + 64)  /* Volvo MCA project, added by dhd3kor, <05.10.2011>*/ 
 
 #define TR_COMP_FC_DIVA_SERVER       (256 * 137)     /*@dhd3kor<28.02.2012>: New component for NAVI*/
 #define TR_COMP_FC_USB_TCU			  ((256 * 138) + 160)     /* for Gen3 , pik5kor, <23-SEP-2015>, Requested by Mani Arjunan (RBEI/ECV1) */
 #define TR_COMP_FC_TCU               ((256 * 138) + 192)  /* TelematicControlUnit for LCN2Kai, dhd3kor, <22-Aug-2012>*/
 
 #define TR_COMP_CCA                  ((256 * 139) +   0)     /* for CCA framework in platform, dhd3kor, <11-Jun-2012>*/
 #define TR_COMP_FC_TPEG              ((256 * 139) + 128)     /* for Nissan LCN2kai, jov1kor, <21-Mar-2013>*/
 
 #define TR_COMP_PHONE_VAG            (256 * 140)     /* merged from tri_types.h for VWTRTN, dhd3kor, <11-Jan-2012>*/
 
 #define TR_COMP_CCA_INC_ADAPTER      (256 * 141)     /* New trace component for G3G, jov1kor, <18-Jun-2013>*/
 #define TR_COMP_FC_LANGUAGE_CHANGE   (256 * 142)     /* New component for G3G[MIB2_ENTRY], jov1kor, <13-Aug-2013>*/
 
 #define TR_COMP_RVC                  (256 * 146)     /* merged from tri_types.h for VWTRTN, dhd3kor, <11-Jan-2012>*/
 
 #define TR_COMP_SBX                  (256 * 148)
 
 #define TR_COMP_ACL_OBC              ((256 * 150) +   0)  /* for Gen3, Buisness logic, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
 #define TR_COMP_ACL_SETTINGS         ((256 * 150) +  64)  /* for Gen3, Buisness logic, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
 #define TR_COMP_ACL_DRIVE            ((256 * 150) + 128)  /* for Gen3, Buisness logic, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
 #define TR_COMP_ACL_SPEED            ((256 * 150) + 192)  /* for Gen3, Buisness logic, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
 
 #define TR_COMP_PDD                  ((256 * 151) +   0)  /* New component for G3G[MIB2_ENTRY], jov1kor, <22-Aug-2013>*/
 #define TR_COMP_DATAPOOL             ((256 * 151) +  64)  /* New component for G3G[MIB2_ENTRY], jov1kor, <22-Aug-2013>*/ 
 #define TR_COMP_AUDIOSTACK           ((256 * 151) + 128)  /* New component for G3G[MIB2_ENTRY], jov1kor, <29-Nov-2013>*/
 #define TR_COMP_STDOUT_TO_TTFIS      ((256 * 151) + 192)  /* New component for G3G[MIB2_ENTRY], jov1kor, <12-Dec-2013>*/ 
 #define TR_COMP_FC_PARKASSIST        (256 * 152)          /* New component for G3G[MIB2_ENTRY], jov1kor, <29-Nov-2013> + Edited the name on <24-Mar-2015>, pik5kor*/ 
 #define TR_COMP_FD_POWERMASTER       ((256 * 153) +   0)    /* for Gen3 , pik5kor, <10-DEC-2015>, Pooja Krishnan (RBEI/ECF5) */ 
 #define TR_COMP_FD_SUPPLYMGMT        ((256 * 153) +  32)   /* for Gen3 , pik5kor, <10-DEC-2015>, Pooja Krishnan (RBEI/ECF5) */ 
 #define TR_COMP_FD_THERMALMGMT       ((256 * 153) +  64)   /* for Gen3 , pik5kor, <10-DEC-2015>, Pooja Krishnan (RBEI/ECF5) */ 
 #define TR_COMP_THERMALMANAGEMENT    (256 * 157)
 #define TR_COMP_DEMO                 (256 * 158)		  /* component name= "DEMO SERVER", dhd2hi, <11.06.2010>*/
 #define TR_COMP_ADASIS               (256 * 159)		  /* vd adasis */
 #define TR_COMP_PASC_CONNMAN         ((256 * 160) +   0) /* for Gen3 PSA, jov1kor, <28-May-2014>,Requested by Seema S R (RBEI/ECP3)*/
 #define TR_COMP_VERIFIC              (256 * 161)		  /*  verification\CSI_stub, 03/10/05 */
 #define TR_COMP_LOGBOOK              (256 * 162)
 #define TR_COMP_PDIM                 (256 * 163)
 #define TR_COMP_ACL_UPDATE           ((256 * 164) +   0)     /* for Gen3 PSA RCC, pik5kor, <9-Jan-2014>,Requested by Shaju Paul Paulson (RBEI/ECA1) */
 #define TR_COMP_FC_HYBRID            ((256 * 164) +   64)     /* for Gen3 PSA RCC, pik5kor, <28-Jan-2015>,Requested by Ankur Gadre(RBEI/ECP4) */
 #define TR_COMP_FCOTA	              ((256 * 164) +   128)     /* for Gen3 FC, pik5kor, <02-Feb-2015>, Requested by Hassmann Peter (CM-AI/EPB1) */
 #define TR_COMP_ACL_BRIGHTNESS       ((256 * 164) +   192)      /* for Gen3 ACL, pik5kor, <17-Feb-2015>, Requested by Sugumaran Balasubramanian (RBEI/ECA3) */
 #define TR_COMP_SBR				  ((256 * 165) +   0)	/* for Gen4 SBR, kap3kor, <12-Jul-2016>, Requested by Mike Scholz(SBR) / Meyer Klaus-Hinrich (CM/ESO2) */
 #define TR_COMP_FC_CAREASYAPPS		  ((256 * 165) +   32)	/* for Gen3 PSA CEA, kap3kor, <14-Sep-2016>, PSARCC-13503 Requested by Bhatt Sonam (RBEI/ECP42) */
 #define TR_COMP_FC_ONSTAR			  ((256 * 165) +   64)	/* for Gen3 PSA OnStar, kap3kor, <20-Sep-2016>, PSARCC-13514 Requested by Siva Sankaran Arivarasan (RBEI/ECP41) */
 #define TR_COMP_LCMDBUSCLIENT        ((256 * 165) +   80)	/* for Gen3 LCM DBus client, kap3kor, <06-Oct-2016>, CFG3-2064 Requested by Rossner Michael (CM/ESO3) */
 #define TR_COMP_FC_DTV               ((256 * 165) +   96)  /* for Gen3 FC_DTV, kap3kor, <25-Oct-2016>, AIVI-34183 Requested by Behera Naba Kishore (RBEI/ECV2) */
 #define TR_COMP_FC_DUMM              ((256 * 165) +   112) /* for Gen3 FC_DUMM, kap3kor, <02-Nov-2016>, AIVI-34673 Requested by Praveen Surendra Badsheshi (RBEI/ECV13) */
 #define TR_COMP_CCA_VPS              ((256 * 165) +   144) /* for Gen3 CCA Video Position Support, haa7kor, <05-Dec-2016>, AIVI-34925 Requested by Peter Engel (CM/ESN2)*/
 #define TR_COMP_FC_AUTO_DRIVE        ((256 * 165) +   160) /* for Gen3 FC_AUTO_DRIVE, haa7kor, <05-Dec-2016>, AIVI-38614 Requested by Schurig Thomas (CM-CI1/ERN4-E) */
 #define TR_COMP_FC_GATEWAY           ((256 * 165) +   176) /* for Gen3 FC_GATEWAY, haa7kor, <05-Dec-2016>, PSARCC-14503 Requested by Akruthi Krishna (RBEI/ECP42) */
 #define TR_COMP_ACL_WEBPORTAL        ((256 * 165) +   208) /* for Gen3 ACL_WEBPORTAL, haa7kor, <05-Dec-2016>, PSARCC-14504 Requested by Akruthi Krishna (RBEI/ECP42) */
 #define TR_COMP_FC_VIDMGR            ((256 * 165) +   224) /* for Gen3 FC_VIDMGR, haa7kor, <07-Dec-2016>, AIVI-38790 Requested by Schurig Thomas (CM-CI1/ERN4-E)*/
 #define TR_COMP_REMOTE_DEVICES       (256 * 166)     /* Component name = "Remote Devices ", sam5hi, <03.11.2010>*/
 #define TR_COMP_V850_TRACE           (256 * 168)     /* For G3G, jov1kor, <06-Mar-2014>,Added for V850 non verbose messages*/
 #define TR_COMP_FC_DATASERVICES      ((256 * 169) +   0)     /* For G3G, jov1kor, <29-Apr-2014>,Requested by Nagaraj Gopalakrishna (RBEI/ECV2)*/
 #define TR_COMP_FC_CDPLAYER          ((256 * 169) +  64)     /* for Gen3 PSA, jov1kor, <29-Apr-2014>,Requested by Panduranga Pai Ballambettu (RBEI/ECP3)*/
 #define TR_COMP_FLYIN_APP_LIBBRARY   ((256 * 169) + 128)     /* for Gen3 SLN, jov1kor, <21-May-2014>,Requested by Shrikant Ramappa Idli (RBEI/ECV1)*/
 #define TR_COMP_FC_ATB               ((256 * 169) + 192)   /* for Gen3 PSA, jov1kor, <28-May-2014>,Requested by Seema S R (RBEI/ECP3) + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
 #define TR_COMP_APPINFRA_SCREENTEST  ((256 * 170) +   0)   /* for Gen3 PSA-RCC, pik5kor, <26-Mar-2015>, Requested by Pravalli Sure (RBEI/ECP4)  */
 #define TR_COMP_VD_EARLY			  ((256 * 170) +   64)   /* for Gen3 PSA-RCC, pik5kor, <06-JUL-2015>, Requested by Lisney Ian (CM-AI/EPB2)  */
 #define TR_COMP_VIDEOPLAYER		  ((256 * 170) +   128)   /* for Gen3 PSA-RCC, pik5kor, <06-JUL-2015>, Requested by Lisney Ian (CM-AI/EPB2)  */
 #define TR_COMP_APPINFRA_DISPLAYMANAGER  ((256 * 170) +   192)   /* for Gen3 PSA-RCC, pik5kor, <06-JUL-2015>, Requested by George Jose Mattamana (RBEI/ECP1)  */
 #define TR_COMP_FC_LBS               ((256 * 171) +   0)  /* Component name = "FC_LBS", sam5hi, <27.04.11>*/
 #define TR_COMP_FC_VEHICLEFUNCMASTER ((256 * 171) +  64)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
 #define TR_COMP_FC_OBC               ((256 * 171) + 128)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
 #define TR_COMP_FC_EXPORTBROKER      ((256 * 171) + 192)  /* for Gen3, pik5kor, <05-NOV-2014>, Jagdish Thanaram Jojawar (RBEI/ECP4) */
 #define TR_COMP_SDS_TRANS_SERV       (256 * 172)		  /* Component name = "SDS_TRANS_SERV ", sam5hi, <09.05.11>*/
 #define TR_COMP_RESIDENT_UNIT        (256 * 173)		  /* Component name = "Resident Unit ", sam5hi, <25.05.2011>*/
 #define TR_COMP_VSDR                 (256 * 174)     /* 05.07.11,sam5hi, Added Compoent VSDR*/
 #define TR_COMP_FC_APP               ((256 * 175)  + 0)     /* merged from tri_types.h for VWTRTN, dhd3kor, <11-Jan-2012>*/
 #define TR_COMP_FC_ALERT             ((256 * 175) + 64)      /* for Gen3 , pik5kor, <23-Mar-2015>, Requested by Sripooja Sivadanam (RBEI/ECP4) */
 #define TR_COMP_FC_EXT_DRV           ((256 * 175) + 96)      /* for Gen3 , pik5kor, <23-JUL-2015>, Requested by EXTERNAL Schurig Thomas (Fa. Brunel, CM-AI/EPB2) . */
 #define TR_COMP_VD_PV                ((256 * 175) + 128)      /* for Gen3 , pik5kor, <28-Jul-2015>, Requested by Sanjay Gurugubelli (RBEI/ECF5) */
 #define TR_COMP_FC_SMARTAPP          ((256 * 175) + 160)      /* for Gen3 FC, pik5kor, <05-AUG-2015>, Requested by Sonam Bhatt (RBEI/ECP4) */
 #define TR_COMP_FC_SPEECH_AUDIO      ((256 * 175) + 192)      /* for Gen3 FC, pik5kor, <12-AUG-2015>, Requested by EXTERNAL Schurig Thomas (Fa. Brunel, CM-AI/EPB2) */
 #define TR_COMP_FC_HOSTAPD           ((256 * 175) + 224)      /* for Gen3 PSA RCC - FC, pik5kor, <15-MAR-2016>, Requested by Mommed Rafiq (RBEI/ECP4) */
 #define TR_COMP_PROFILER             (256 * 176)     /* FC Profiler for G3G, jov1kor, <24-Sep-2013>,Requested by Stresing Christian */
 #define TR_COMP_FC_UPS               ((256 * 177) +   0)     /* merged from tri_types.h for VWTRTN, dhd3kor, <02-Apr-2012>*/
 #define TR_COMP_FC_AUX               ((256 * 177) + 128)     /* New component for G3G, jov1kor, <09-Apr-2014>*/
 #define TR_COMP_FC_VPAS              ((256 * 177) + 192)     /* New component for G3G, pik5kor, <17-JUN-2014>  , + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
 #define TR_COMP_FC_LOCINPUT          ((256 * 177) + 224)   /* for Gen3 FC, pik5kor, <11-JUL-2015>, Requested by Andres Sascha (CM/ESN2)  */
 #define TR_COMP_SMART_PHONE_INT      ((256 * 178) +   0)     /* for LCN2Kai, dhd3kor, <24-May-2012>*/
 #define TR_COMP_PASC_SMARTPHONE_INT  ((256 * 178) + 128)     /* for Gen3 PSA, jov1kor, <22-Apr-2014>,Manishi Kumari (RBEI/ECP3)*/
 #define TR_COMP_EAMP_PLUGIN          ((256 * 178) + 192)     /* for Gen3 PSA, pik5kor, <20-JUN-2014>,Sandip Kumar Munda (RBEI/ECP3)*/
 #define TR_COMP_TRACE_ADR 	          ((256 * 178) + 224)     /* for Gen3 , pik5kor, <08-OCT-2015>, Raghavendra Nannuru Vutkuru (RBEI/ECV2)*/
 #define TR_COMP_ACL_MEDIA            ((256 * 179) +   0)  /* New component for G3G[JAC-RCC], jov1kor, <29-Oct-2013> + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
 #define TR_COMP_FC_MAPUPDATE		  ((256 * 179) +  32)  /* for Gen3 PSA, pik5kor, <15-OCT-2015>, Wiedemann Frank (CM/ESN1)*/
 #define TR_COMP_ACL_TUNER            ((256 * 179) +  64)  /* New component for G3G[JAC-RCC], jov1kor, <29-Oct-2013> + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
 #define TR_COMP_AUDIO_MASTER         ((256 * 179) + 128)  /* New component for G3G[JAC-RCC], jov1kor, <29-Oct-2013> + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
 #define TR_COMP_ACL_CONNECTIVITY     ((256 * 179) + 192)  /* New component for G3G[JAC-RCC], jov1kor, <29-Oct-2013> + edited on <31-Mar-2015> requested by Seema S R (RBEI/ECP4)*/
 #define TR_COMP_SWITCHCTRL           ((256 * 179) + 224)  /* New component for G3G, pik5kor, <24-Nov-2015> requested by Gupta Sandeep (RBEI/ECV3)  */

 /*Below component IDs are in ADIT range, but used in Bosch. New IDs should be assigned*/
 #define TR_COMP_DABTUNER             (256 * 201)
 #define TR_COMP_ODI_CSM              (256 * 203)
 #define TR_COMP_CSFW                 (256 * 204)		  /* componentname = "CSFW"       */
  
/*****************************************************************************
 *****************************************************************************
 *                    Please do not add any new Trace component               
 * Please contact Trenkler Lutz(CM-AI/PJ-CF33) for adding new Components
 *****************************************************************************
 *****************************************************************************/

/* Definition of maximum class a user can use in a particular component
 * Below defines are requested for HMI common applications in Gen3 PSA&SUZUKI
 * jov1kor, <26-Jun-2014>,Requested by Hessling Matthias (CM-AI/ECB4) 
 */
#define TR_COMP_UI_APP_HMI_MASTER_RANGE     (64)
#define TR_COMP_UI_APP_HMI_PLAYGROUND_RANGE (64)
#define TR_COMP_UI_APP_HMI_01_RANGE         (64)
#define TR_COMP_UI_APP_HMI_02_RANGE         (64)
#define TR_COMP_UI_APP_HMI_03_RANGE         (64)
#define TR_COMP_UI_APP_HMI_04_RANGE         (64)
#define TR_COMP_UI_APP_HMI_05_RANGE         (64)
#define TR_COMP_UI_APP_HMI_06_RANGE         (64)
#define TR_COMP_UI_APP_HMI_07_RANGE         (64)
#define TR_COMP_UI_APP_HMI_08_RANGE         (64)
#define TR_COMP_UI_APP_HMI_09_RANGE         (64)
#define TR_COMP_UI_APP_HMI_10_RANGE         (64)
#define TR_COMP_UI_APP_HMI_11_RANGE         (64)
#define TR_COMP_UI_APP_HMI_12_RANGE         (64)
#define TR_COMP_UI_APP_HMI_13_RANGE         (64)
#define TR_COMP_UI_APP_HMI_14_RANGE         (64)
#define TR_COMP_UI_APP_HMI_15_RANGE         (64)
#define TR_COMP_UI_APP_HMI_16_RANGE         (64)
#define TR_COMP_UI_APP_HMI_17_RANGE         (64)
#define TR_COMP_UI_APP_HMI_18_RANGE         (64)
#define TR_COMP_FC_CLIMATE_RANGE            (64)
#define TR_COMP_FC_ENG_MODE_RANGE           (64)
#define TR_COMP_ACL_PARKING_RANGE           (32)
#define TR_COMP_FC_HIT_RANGE                (64)
#define TR_COMP_ACL_AUDIO_RANGE             (64)
#define TR_COMP_FC_VP1_RANGE                (64)
#define TR_COMP_ANALYSIS_RANGE              (32)
#define TR_COMP_ACL_ALERT_RANGE             (32)
#define TR_COMP_ACL_MULTIDRIVE_RANGE        (32)
#define TR_COMP_ACL_HYBRID_RANGE            (32)
#define TR_COMP_ACL_UPDATE_RANGE            (64)
#define TR_COMP_ACL_PHONE_RANGE             (64) 
#define TR_COMP_FC_UPDATE_RANGE             (64)
#define TR_COMP_FCOTA_RANGE                 (64)
#define TR_COMP_ACL_BRIGHTNESS_RANGE        (64)
#define TR_COMP_ACL_BASECLASS_RANGE         (32)
#define TR_COMP_APPINFRA_USERMGMT_RANGE     (32)
#define TR_COMP_APPINFRA_APPCTRL_RANGE      (32)
#define TR_COMP_APPINFRA_KEYHANDLER_RANGE   (32)
#define TR_COMP_FACIA_RANGE                 (32)
#define TR_COMP_FC_ALERT_RANGE				(32)
#define TR_COMP_FC_SERVICEMGR_RANGE			(32)
#define TR_COMP_FC_CONNECT_RANGE			(32)
#define TR_COMP_FC_VPAS_RANGE 				(32)
#define TR_COMP_FC_ATB_RANGE   				(32)
#define TR_COMP_AUDIO_MASTER_RANGE 			(32)
#define TR_COMP_ACL_CONNECTIVITY_RANGE   	(32)
#define TR_COMP_ACL_MEDIA_RANGE  			(32)
#define TR_COMP_ACL_TUNER_RANGE 			(32)
#define TR_COMP_DATACOLLECTOR_RANGE   		(32)
#define TR_COMP_VD_EARLY_RANGE   		    (64)
#define TR_COMP_VIDEOPLAYER_RANGE   		(64)
#define TR_COMP_APPINFRA_DISPLAYMANAGER_RANGE  	(64)
#define TR_COMP_FC_LOCINPUT_RANGE   		(32)
#define TR_COMP_FC_EXT_DRV_RANGE            (32)
#define TR_COMP_VD_PV_RANGE					(32)
#define TR_COMP_FC_SMARTAPP_RANGE			(32)
#define TR_COMP_FC_SPEECH_AUDIO_RANGE		(32)
#define TR_COMP_FC_USB_TCU_RANGE			(32)
#define TR_COMP_EAMP_PLUGIN_RANGE			(32)
#define TR_COMP_TRACE_ADR_RANGE				(32)
#define TR_COMP_FC_MAPUPDATE_RANGE			(32)
#define TR_COMP_SWITCHCTRL_RANGE			(32)
#define TR_COMP_FD_POWERMASTER_RANGE		(32)
#define TR_COMP_FD_SUPPLYMGMT_RANGE 		(32)
#define TR_COMP_FD_THERMALMGMT_RANGE		(32)
#define TR_COMP_EXTNAV_RANGE				(32)
#define TR_COMP_FC_HOSTAPD_RANGE			(32)
#define TR_COMP_SBR_RANGE					(32)
#define TR_COMP_FC_CAREASYAPPS_RANGE		(32)
#define TR_COMP_FC_ONSTAR_RANGE				(16)
#define TR_COMP_LCMDBUSCLIENT_RANGE         (16)
#define TR_COMP_FC_DTV_RANGE                (16)
#define TR_COMP_FC_DUMM_RANGE               (32)
#define TR_COMP_CCA_VPS_RANGE               (16)
#define TR_COMP_FC_AUTO_DRIVE_RANGE         (16)
#define TR_COMP_FC_GATEWAY_RANGE            (32)
#define TR_COMP_ACL_WEBPORTAL_RANGE         (16)
#define TR_COMP_FC_VIDMGR_RANGE             (32)

/* Definition of maximum class a user can have in each component
 * do not modify the name, only modify the value
 * The first 24 classes are predefined for global issues */
#define TR_MAX_CLSCNT_COMP013			(256)
#define TR_MAX_CLSCNT_COMP014			(256)
#define TR_MAX_CLSCNT_COMP015			(256)
#define TR_MAX_CLSCNT_COMP016			(256)
#define TR_MAX_CLSCNT_COMP017			(256)
#define TR_MAX_CLSCNT_COMP018			(256)
#define TR_MAX_CLSCNT_COMP019			(256)
#define TR_MAX_CLSCNT_COMP020			(256)
#define TR_MAX_CLSCNT_COMP021			(256)
#define TR_MAX_CLSCNT_COMP022			(256)
#define TR_MAX_CLSCNT_COMP023			(256)
#define TR_MAX_CLSCNT_COMP024			(256)
#define TR_MAX_CLSCNT_COMP025			(256)
#define TR_MAX_CLSCNT_COMP026			(256)
#define TR_MAX_CLSCNT_COMP027			(256)
#define TR_MAX_CLSCNT_COMP028			(256)
#define TR_MAX_CLSCNT_COMP029			(256)
#define TR_MAX_CLSCNT_COMP030			(256)
#define TR_MAX_CLSCNT_COMP031			(256)
#define TR_MAX_CLSCNT_COMP032			(256)
#define TR_MAX_CLSCNT_COMP033			(256)
#define TR_MAX_CLSCNT_COMP034			(256)
#define TR_MAX_CLSCNT_COMP035			(256)
#define TR_MAX_CLSCNT_COMP036			(256)
#define TR_MAX_CLSCNT_COMP037			(256)
#define TR_MAX_CLSCNT_COMP038			(256)
#define TR_MAX_CLSCNT_COMP039			(256)
#define TR_MAX_CLSCNT_COMP040			(256)
#define TR_MAX_CLSCNT_COMP041			(256)
#define TR_MAX_CLSCNT_COMP042			(256)
#define TR_MAX_CLSCNT_COMP043			(256)
#define TR_MAX_CLSCNT_COMP044			(256)
#define TR_MAX_CLSCNT_COMP045			(256)
#define TR_MAX_CLSCNT_COMP046			(256)
#define TR_MAX_CLSCNT_COMP047			(256)
#define TR_MAX_CLSCNT_COMP048			(256)
#define TR_MAX_CLSCNT_COMP049			(256)
#define TR_MAX_CLSCNT_COMP050			(256)
#define TR_MAX_CLSCNT_COMP051			(256)
#define TR_MAX_CLSCNT_COMP052			(256)
#define TR_MAX_CLSCNT_COMP053			(256)
#define TR_MAX_CLSCNT_COMP054			(256)
#define TR_MAX_CLSCNT_COMP055			(256)
#define TR_MAX_CLSCNT_COMP056			(256)
#define TR_MAX_CLSCNT_COMP057			(256)
#define TR_MAX_CLSCNT_COMP058			(256)
#define TR_MAX_CLSCNT_COMP059			(256)
#define TR_MAX_CLSCNT_COMP060			(256)
#define TR_MAX_CLSCNT_COMP061			(256)
#define TR_MAX_CLSCNT_COMP062			(256)
#define TR_MAX_CLSCNT_COMP063			(256)
#define TR_MAX_CLSCNT_COMP064			(256)
#define TR_MAX_CLSCNT_COMP065			(256)
#define TR_MAX_CLSCNT_COMP066			(256)
#define TR_MAX_CLSCNT_COMP067			(256)
#define TR_MAX_CLSCNT_COMP068			(256)
#define TR_MAX_CLSCNT_COMP069			(256)
#define TR_MAX_CLSCNT_COMP070			(256)
#define TR_MAX_CLSCNT_COMP071			(256)
#define TR_MAX_CLSCNT_COMP072			(256)
#define TR_MAX_CLSCNT_COMP073			(256)
#define TR_MAX_CLSCNT_COMP074			(256)
#define TR_MAX_CLSCNT_COMP075			(256)
#define TR_MAX_CLSCNT_COMP076			(256)
#define TR_MAX_CLSCNT_COMP077			(256)
#define TR_MAX_CLSCNT_COMP078			(256)
#define TR_MAX_CLSCNT_COMP079			(256)
#define TR_MAX_CLSCNT_COMP080			(256)
#define TR_MAX_CLSCNT_COMP081			(256)
#define TR_MAX_CLSCNT_COMP082			(256)
#define TR_MAX_CLSCNT_COMP083			(256)
#define TR_MAX_CLSCNT_COMP084			(256)
#define TR_MAX_CLSCNT_COMP085			(256)
#define TR_MAX_CLSCNT_COMP086			(256)
#define TR_MAX_CLSCNT_COMP087			(256)
#define TR_MAX_CLSCNT_COMP088			(256)
#define TR_MAX_CLSCNT_COMP089			(256)
#define TR_MAX_CLSCNT_COMP090			(256)
#define TR_MAX_CLSCNT_COMP091			(256)
#define TR_MAX_CLSCNT_COMP092			(256)
#define TR_MAX_CLSCNT_COMP093			(256)
#define TR_MAX_CLSCNT_COMP094			(256)
#define TR_MAX_CLSCNT_COMP095			(256)
#define TR_MAX_CLSCNT_COMP096			(256)
#define TR_MAX_CLSCNT_COMP097			(256)
#define TR_MAX_CLSCNT_COMP098			(256)
#define TR_MAX_CLSCNT_COMP099			(256)
#define TR_MAX_CLSCNT_COMP100			(256)
#define TR_MAX_CLSCNT_COMP101			(256)
#define TR_MAX_CLSCNT_COMP102			(256)
#define TR_MAX_CLSCNT_COMP103			(256)
#define TR_MAX_CLSCNT_COMP104			(256)
#define TR_MAX_CLSCNT_COMP105			(256)
#define TR_MAX_CLSCNT_COMP106			(256)
#define TR_MAX_CLSCNT_COMP107			(256)
#define TR_MAX_CLSCNT_COMP108			(256)
#define TR_MAX_CLSCNT_COMP109			(256)
#define TR_MAX_CLSCNT_COMP110			(256)
#define TR_MAX_CLSCNT_COMP111			(256)
#define TR_MAX_CLSCNT_COMP112			(256)
#define TR_MAX_CLSCNT_COMP113			(256)
#define TR_MAX_CLSCNT_COMP114			(256)
#define TR_MAX_CLSCNT_COMP115			(256)
#define TR_MAX_CLSCNT_COMP116			(256)
#define TR_MAX_CLSCNT_COMP117			(256)
#define TR_MAX_CLSCNT_COMP118			(256)
#define TR_MAX_CLSCNT_COMP119			(256)
#define TR_MAX_CLSCNT_COMP120			(256)
#define TR_MAX_CLSCNT_COMP121			(256)
#define TR_MAX_CLSCNT_COMP122			(256)
#define TR_MAX_CLSCNT_COMP123			(256)
#define TR_MAX_CLSCNT_COMP124			(256)
#define TR_MAX_CLSCNT_COMP125			(256)
#define TR_MAX_CLSCNT_COMP126			(256)
#define TR_MAX_CLSCNT_COMP127			(256)
#define TR_MAX_CLSCNT_COMP128			(256)
#define TR_MAX_CLSCNT_COMP129			(256)
#define TR_MAX_CLSCNT_COMP130			(256)
#define TR_MAX_CLSCNT_COMP131			(256)
#define TR_MAX_CLSCNT_COMP132			(256)
#define TR_MAX_CLSCNT_COMP133			(256)
#define TR_MAX_CLSCNT_COMP134			(256)
#define TR_MAX_CLSCNT_COMP135			(256)
#define TR_MAX_CLSCNT_COMP136			(256)
#define TR_MAX_CLSCNT_COMP137			(256)
#define TR_MAX_CLSCNT_COMP138			(256)
#define TR_MAX_CLSCNT_COMP139			(256)
#define TR_MAX_CLSCNT_COMP140			(256)
#define TR_MAX_CLSCNT_COMP141			(256)
#define TR_MAX_CLSCNT_COMP142			(256)
#define TR_MAX_CLSCNT_COMP143			(256)
#define TR_MAX_CLSCNT_COMP144			(256)
#define TR_MAX_CLSCNT_COMP145			(256)
#define TR_MAX_CLSCNT_COMP146			(256)
#define TR_MAX_CLSCNT_COMP147			(256)
#define TR_MAX_CLSCNT_COMP148			(256)
#define TR_MAX_CLSCNT_COMP149			(256)
#define TR_MAX_CLSCNT_COMP150			(256)
#define TR_MAX_CLSCNT_COMP151			(256)
#define TR_MAX_CLSCNT_COMP152			(256)
#define TR_MAX_CLSCNT_COMP153			(256)
#define TR_MAX_CLSCNT_COMP154			(256)
#define TR_MAX_CLSCNT_COMP155			(256)
#define TR_MAX_CLSCNT_COMP156			(256)
#define TR_MAX_CLSCNT_COMP157			(256)
#define TR_MAX_CLSCNT_COMP158			(256)
#define TR_MAX_CLSCNT_COMP159			(256)
#define TR_MAX_CLSCNT_COMP160			(256)
#define TR_MAX_CLSCNT_COMP161			(256)
#define TR_MAX_CLSCNT_COMP162			(256)
#define TR_MAX_CLSCNT_COMP163			(256)
#define TR_MAX_CLSCNT_COMP164			(256)
#define TR_MAX_CLSCNT_COMP165			(256)
#define TR_MAX_CLSCNT_COMP166			(256)
#define TR_MAX_CLSCNT_COMP167			(256)
#define TR_MAX_CLSCNT_COMP168			(256)
#define TR_MAX_CLSCNT_COMP169			(256)
#define TR_MAX_CLSCNT_COMP170			(256)
#define TR_MAX_CLSCNT_COMP171			(256)
#define TR_MAX_CLSCNT_COMP172			(256)
#define TR_MAX_CLSCNT_COMP173			(256)
#define TR_MAX_CLSCNT_COMP174			(256)
#define TR_MAX_CLSCNT_COMP175			(256)
#define TR_MAX_CLSCNT_COMP176			(256)
#define TR_MAX_CLSCNT_COMP177			(256)
#define TR_MAX_CLSCNT_COMP178			(256)
#define TR_MAX_CLSCNT_COMP179			(256)

#endif /* BOSCH_TRACE_H */
