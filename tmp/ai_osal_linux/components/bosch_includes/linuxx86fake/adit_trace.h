/**
 * \file: adit_trace.h
 *
 * \version: $Id: adit_trace.h,v 1.52 2012/02/16 13:49:11 jlorenz Exp $
 *
 * This header file declares constants for trace
 *
 * \author: T.Rodenbach <trodenbach@de.adit-jv.com>
 *          S.Maleyka   <smaleyka@de.adit-jv.com>
 *
 * \copyright: (c) 2003 - 2011 Adit Corporation
 *
 */

#ifndef ADIT_TRACE_H
#define ADIT_TRACE_H

#include "../trace/mc_trace.h"

typedef enum
{
    /* Old Definition */
   TR_LEVEL_CUSTOMER             = 0,
   TR_LEVEL_EEZ                  = 1,
   TR_LEVEL_FATAL_ERROR          = 2,
   TR_LEVEL_ERROR                = 3,
   TR_LEVEL_WARNING              = 4,
   TR_LEVEL_STATE_TRANSITION     = 5,
   TR_LEVEL_EVENT                = 6,
   TR_LEVEL_MESSAGE              = 7,
   TR_LEVEL_DATA                 = 8,

   /* New Definition */
   TR_LEVEL_FATAL                = 0,
   TR_LEVEL_ERRORS               = 1,
   TR_LEVEL_SYSTEM_MIN           = 2,
   TR_LEVEL_SYSTEM               = 3,
   TR_LEVEL_COMPONENT            = 4,
   TR_LEVEL_USER_1               = 5,
   TR_LEVEL_USER_2               = 6,
   TR_LEVEL_USER_3               = 7,
   TR_LEVEL_USER_4               = 8

}TR_tenTraceLevel;

/*****************************************************************************
 * Definitions for trace callback channels
 * =======================================
 *
 * A callback channel is identified by a unsigned 1-Byte number. The ComponentID
 * should match to the callback channel ID.
 *
 *
 *   +---------------+---------------+-------------------------------+
 *   | Component ID  | Purpose       | Comments                      |
 *   +---------------+---------------+-------------------------------+
 *   | 0-12          | Trace / System| historical reasons            |
 *   +---------------+---------------+-------------------------------+
 *   | 13-179        | Mother        | Reserved Component IDs for MCs|
 *   |               | Companies     |                               |
 *   +---------------+---------------+-------------------------------+
 *   | 180-255       | ADIT          | product relevant              |
 *   +---------------+---------------+-------------------------------+
 *
 *   Please comment each update with the following template:
 *   <packagename>, <date of entry>
 ******************************************************************************/
typedef enum
{
    NO_CHAN_SELECTED            = 0,
    TR_TTFIS_CACHE              = 1,     /* CX_TTFIS_DATA_CMD */
    TR_TTFIS_DVDTEST            = 2,
    TR_TTFIS_SYSMANAGER         = 3,     /* /platform/middleware/system_power */
    TR_TTFIS_FANCONTROL         = 4,
    TR_TTFIS_VKBOARD            = 5,
    TR_TTFIS_MTRACERUN          = 6,
    TR_TTFIS_NAVITRACE          = 7,
    TR_TTFIS_TMC                = 8,
    TR_TTFIS_VOICE              = 9,
    TR_TTFIS_PLAYGROUND         = 10,
    TR_TTFIS_LPM1               = 11,
    TR_TTFIS_LPM2               = 12,

#ifdef MC_TRACE_TR_TENTRACECHAN
    MC_TRACE_TR_TENTRACECHAN
#endif /*#ifdef MC_TRACE_TR_TENTRACECHAN*/

    TR_TTFIS_SED                = 181, /* platform/driver/sed                    */
    TR_TTFIS_TFW                = 182, /* test/testapps/test_framework/
                                          test/testapps/tfw/                     */
    TR_TTFIS_GRAPHIC            = 183, /* platform/svg_* (tkernel os             */
    TR_TTFIS_PROF               = 184, /* platform/middleware/trace_ttfis        */
    TR_TTFIS_ROUTER             = 185, /* platform/middleware/trace_ttfis        */

    TR_TTFIS_TSIM               = 186, /* test/cal_testenvironment               */
                                       /* test/cal_testenvironment_prc/          */
    TR_TTFIS_CAL_DAEMON         = 187, /* test/calgen/calgen_daemon              */
    TR_TTFIS_TAN                = 188, /* platform/driver/storage/tanashin_block */
    TR_TTFIS_EDAR               = 189, /* platform/edar/edar_*,                  */
    TR_TTFIS_DMA_ED             = 190, /* platform/edar/edar_dma*                */
    
    TR_TTFIS_MP3                = 193, /* application/demo/mpmw_demo             */
    TR_TTFIS_CHANNEL_DCMOD      = 194, /* platform/middleware/dcm_optical_disc   */
    TR_TTFIS_CAL                = 195, /* platform/middleware/cal_subsystem      */
    TR_TTFIS_DCF                = 196, /* platform/dcf                           */
    TR_TTFIS_GRAPHIC_LX         = 197, /* platform/svg_* (linux os)              */

    TR_TTFIS_DIAG_MCNET         = 200, /* platform/middleware/diag/              */
    TR_TTFIS_AUDIO              = 201, /* platform/fg, platform/stm              */
    TR_TTFIS_GNSS               = 202, /* platform/gnss                          */
    TR_TTFIS_TFAT               = 203, /* test/fat                               */
    TR_TTFIS_MEMTOOL            = 204, /* platform/mem_tool                      */
    TR_TTFIS_MOST               = 205, /* platform/most_netservices              */
    TR_TTFIS_VFS                = 206, /* platform/vfs                           */
    TR_TTFIS_TRACE_DUALOS       = 207, /* platform/trace                         */

    /* PRQA: Initialization of the last enum element intentionally left */
    /* open, because it shall be set to the last component id+1 automatically */
    /* PRQA S 0723 L1 */
    TR_LAST_LAUNCH_CHAN         = 511/* changed to 511 always maximum           */
    /* PRQA L:L1 */

}TR_tenTraceChan;


/**
 * Definitions for trace class
 *
 * A class definition describes a 16 bit value, which has two parts
 *
 * [  Component - ID   ][   Trace-Class-ID  ]
 * [15                9][8                 0]
 *
 *
 * The component describes for which SW-parts the trace classes are defined for.
 * For each component 2^8 = 256 classes are possible.
 *
 *   +---------------+---------------+-------------------------------+
 *   | Component ID  | Purpose       | Comments                      |
 *   +---------------+---------------+-------------------------------+
 *   | 0-12          | Trace / System| historical reasons            |
 *   +---------------+---------------+-------------------------------+
 *   | 13-179        | Mother        | Reserved Component IDs for MCs|
 *   |               | Companies     |                               |
 *   +---------------+---------------+-------------------------------+
 *   | 180-255       | ADIT          | product relevant              |
 *   +---------------+---------------+-------------------------------+
 *
 *   Please comment each update with the following template:
 *   <packagename>, <class id range>,<date of entry>
 */

/* TRACE / System */
 #define TR_COMP_SYSTEM         (256 * 0)       /* componentname = "SYSTEM"                      */
                                                /* system/mem_tool,            0-9,  18-Oct-2010 */

 #define TR_COMP_TRACE          (256 * 12)      /* componetname = "TRACE" pas2hi, 16.06.03 (new) */
                                                /* used for Profiler                             */
 #define TR_COMP_TESTER         (256 * 16)      /* componentname = "TESTER" for MST testing      */

/* ADIT platform  */
 #define TR_COMP_DRV_FM_DAB     (256 * 181)     /* platform/driver/fm_rds,                 00-09, 10-Oct-2006 */
                                                /* platform/sed,                           10-19, 15-Jul-2007 */
                                                /* platform/utility,                       20-29, 15-Jul-2007 */
                                                /* platform/xml,                           30-34, 03-Sep-2009 */

 #define TR_COMP_TFW            (256 * 182)     /* componentname = "TFW" TestApplications                     */

 #define TR_COMP_TEST           (256 * 183)     /* (?) test/testapps_adit/dcmod,           00-09, 18-Sep-2007 */
                                                /* (?) test/calgen/app,                    10-19, 04-Apr-2008 */
                                                /* (?) test/edar,                          20-24, 02-Jul-2009 */
                                                /* (?) test/gnss,                          25-29, 11-Mar-2010 */
                                                /* (?) test/tvdec,                         30-39, 29-Mar-2011 */ 
                                                /* (?) test/most,                          40-49, 04-May-2010 */
                                                /* (?) test/svg_bitmap_dec,                50-51, 19-Jul-2010 */
                                                /* (?) test/svg_common,                    52-52, 19-Jul-2010 */
                                                /*     test/mlb,                           53-53, 10-Feb-2011 */
 
 #define TR_COMP_DRV_AUDIO      (256 * 184)     /* platform/edar/edar,                      0-17, 30-Apr-2009 */ 
                                                /* platform/edar/edar_impl,                 0-17, 30-Apr-2009 */
                                                /* platform/edar/edar_fpga,                 0-17, 30-Apr-2009 */
                                                /* platform/edar/edar_dma                  20-37, 30-Apr-2009 */
                                                /* platform/edar/edar_dma_impl,            20-37, 30-Apr-2009 */
 
 #define TR_COMP_DRV_STORAGE    (256 * 185)     /* platform\driver\storage\ramdisk,        00-16, 03-Oct-2005 */
                                                /* platform/driver/storage/tanashin_block, 16-48, 14-Feb-2007 */
                                                /* platform/driver/storage/ide,            49-64, 23-May-2007 */
                                                /* platform/driver/filesystem/reliance,       65, 07-Nov-2007 */
                                                /* platform/driver/filesystem/fat_dn,         66, 07-Nov-2007 */
                                                /* platform/driver/filesystem/iso_dn,         67, 07-Nov-2007 */
                                                /* platform/driver/nec_sdmmc,              68-69, 01-Oct-2007 */
                                                /* platform/driver/storage/flashfx,           70, 07-Nov-2007 */
                                                /* platform/vfs/vfs,                       71-74, 18-Jan-2011 */


 #define TR_COMP_DRV_SCREEN     (256 * 186)     /* platform\driver\graphics\opengl_tinygl,        03-Oct-2005 */
                                                /* platform\driver\graphics\rgl,                  03-Oct-2005 */
                                                /* platform\driver\graphics\screen\vr5500,        03-Oct-2005 */
                                                /* platform\driver\graphics\screen\core2,         03-Oct-2005 */

 #define TR_COMP_DRV_INTERFACE  (256 * 187)     /* platform/driver/pcicom,                00-009, 03-Oct-2005 */
                                                /* platform/driver/sensor/vehicle_sensor, 10-019, 07-Oct-2005 */
                                                /* platform/driver/nec_uart,              20-029, 10-Oct-2006 */
                                                /* platform/driver/nec_csi,               30-039, 10-Oct-2006 */
                                                /* platform/iic/iic,                      40-049, 24-Feb-2009 */
                                                /* platform/hw_ctrl_lib,                  50-059, 17-Jun-2008 */
                                                /* platform/rtc/rtc,                      60-064, 19-Dec-2008 */
                                                /* platform/adac/adac,                    65-069, 19-Dec-2008 */
                                                /* platform/gadc/gadc,                    70-074, 28-Jan-2009 */
                                                /* platform/codec_cs42l51/codec,          75-084, 04-Feb-2009 */
                                                /* platform/iis/iis_ne,                   85-094, 23-Feb-2009 */
                                                /* platform/usbf/usbf,                    95-104, 02-Dec-2009 */
                                                /* platform/usbh/ *,                     105-120, 04-May-2010 */
                                                /* platform/rfs/ *,                      121-129, 23-Nov-2010 */
                                                /* platform/masca/ *,                    130-140, 16-Feb-2012 */
                                                  

 #define TR_COMP_SM             (256 * 188)     /* platform\middleware\system_power,     000-009, 03-Oct-2005 */
                                                /* platform\driver\drivermanager,        010-019, 03-Oct-2005 */
                                                /* platform\middleware/StartupMgr,       020-024, 02-Oct-2007 */
                                                /* platform\middleware\dnmgr,            224-255, 02-Oct-2007 */

 #define TR_COMP_SETE           (256 * 189)

 #define TR_COMP_APP_FW         (256 * 190)     /* platform\middleware\afw,                       03-Oct-2005 */
                                                /* platform\middleware\cal_wrapper,               03-Oct-2005 */

 #define TR_COMP_APP_MNGR       (256 * 191)     /* platform\middleware\applicationmanager,        03-Oct-2005 */

 #define TR_COMP_GRAPHIC        (256 * 192)     /* platform\graphics\grl_common,                  03-Oct-2005 */
                                                /* platform\graphics\grl_cpu_drawlib,             03-Oct-2005 */
                                                /* platform\graphics\grl_freetype,                03-Oct-2005 */
                                                /* platform\graphics\svg_font,                    03-Oct-2005 */
                                                /* platform\graphics\svg_layer,                   03-Oct-2005 */
                                                /* platform\graphics\svg_rasterizer,              03-Oct-2005 */
                                                /* platform\graphics\svg_resource,                03-Oct-2005 */

 #define TR_COMP_APP_MM_PLAYER  (256 * 193)     /* platform/middleware/mpmw,               00-19, 19-Sep-2006 */
                                                /* platform/multimedia/dvd,                20-39, 19-Sep-2006 */
                                                /* platform/ipod_ctrl/,                    40-59, 03-Mar-2009 */

 #define TR_COMP_MW_DCM         (256 * 194)     /* platform/middleware/dcm_optical_disc,   00-09, 22-Sep-2006 */
                                                /* platform/dcf/dcf,                       10-15, 18-Jan-2011 */

 #define TR_COMP_CAL            (256 * 195)     /* platform\middleware\cal,                00-09, 03-Oct-2005 */
                                                /* platform\middleware\tam_pcie,           10-14, 03-Oct-2005 */
                                                /* platform\middleware\tam_udp,            14-19, 26-Sep-2006 */
                                                /* platform\middleware\cal_daemon,         20-24, 26-Sep-2006 */
                                                /* platform\middleware\cal_resourcemanager,   25, 08-Nov-2006 */
                                                /* platform\middleware\od_rsm_fi,             26, 08-Nov-2006 */
                                                /* platform\middleware\streammanager,         27, 06-Jun-2007 */
                                                /* test\performancetest\performancelib,      255, 26-Sep-2006 */

 #define TR_COMP_DRV_SENSOR     (256 * 196)     /* platform/driver/gps,                      0-9, 23-Nov-2005 */

 #define TR_COMP_NETWORK        (256 * 197)     /* platform\driver\most\netservices           l1, 10-Mar-2005 */
                                                /* platform\driver\most\netserviceswrapper, 0-19, 10-Mar-2005 */
                                                /* platform/driver/ieee1394,               20-39, 05-Oct-2005 */

 #define TR_COMP_MEDIAPLAYER    (256 * 198)     /* application/demo/bfdemo/mediaplayer,           08-Jul-2005 */

 #define TR_COMP_CONFDRV        (256 * 199)     /* platform/driver/conf_driver,                   09-Aug-2005 */

 #define TR_COMP_DIAG           (256 * 200)     /* trace class ID for diagnosis,                  20-Sep-2005 */

 #define TR_COMP_AUDIO          (256 * 201)     /* platform/streammanager,                     0, 02-Jul-2009 */
                                                /* platform/filtergraph,                       1, 02-Jul-2009 */

 #define TR_COMP_VIDEO          (256 * 202)     /* platform/vmp,                         000-001, 29-Mar-2011 */
                                                /* platform/vdec,                        002-009, 29-Mar-2011 */
                                                /* reserved                              010-255, 29-Mar-2011 */

/* Verification */
 #define TR_COMP_VERIFIC        (256 * 161)     /* verification\CSI_stub,                        03-Oct-2005 */
                                                /* verification\DAB,                             03-Oct-2005 */
                                                /* verification\dma,                             03-Oct-2005 */
                                                /* verification\driver_stubs\cfcard_stub,        03-Oct-2005 */
                                                /* verification\driver_stubs\mmccard_stub,       03-Oct-2005 */
                                                /* verification\driver_stubs\mscard_stub,        03-Oct-2005 */
                                                /* verification\driver_stubs\sd_card,            03-Oct-2005 */
                                                /* verification\edar,                            03-Oct-2005 */
                                                /* verification\fm_rds,                          03-Oct-2005 */
                                                /* verification\GPS,                             03-Oct-2005 */
                                                /* verification\I2C_stub,                        03-Oct-2005 */
                                                /* verification\selftest,                        03-Oct-2005 */
                                                /* verification\system_level\test_application,   03-Oct-2005 */
                                                /* verification\system_level\test_manager,       03-Oct-2005 */
                                                /* verification\VDD_Applay,                      03-Oct-2005 */
                                                /* verification\VDD_Cards,                       03-Oct-2005 */
                                                /* verification\VDD_CSI,                         03-Oct-2005 */
                                                /* verification\VDD_I2C,                         03-Oct-2005 */
                                                /* verification\VDD_Utils,                       03-Oct-2005 */

/* END: definitions for trace class */

 #define TR_MAX_COMP_COUNT                  (256)
 #define TR_COMP_MAX_CLASS_COUNT            (256)

 #define TR_FIRST_COMPONENT_CLASS_ID        (24)    /* see below */

/* definition of old trace classes (may be removed by time)*/

typedef enum
{
  /* component-internal class defenitions  for TR_COMP_TRACE
   * Modified the CompID - 20/10/2005 <Sakthivelu.S>
   */
  TR_CLASS_TRACE = (TR_COMP_TRACE),
  TR_CLASS_POSTMORTEM,                   /* Added for PMM -19/08/2005 <Sakthivelu.S> */
  TR_CLASS_SYNC = (TR_CLASS_TRACE+0x25), /* Added for clock sync -02/02/2007 <Sakthivelu.S> */

#ifdef MC_TRACE_TR_TENTRACECLASS
  MC_TRACE_TR_TENTRACECLASS
#endif /*#ifdef MC_TRACE_TR_TENTRACECLASS*/

  TR_LAST_CLASS

}TR_tenTraceClass;

/* Definition of maximum class a user can have in each component
 * do not modify the name, only modify the value
 * The first 24 classes are predefined for global issues */

#define TR_MAX_CLSCNT_COMP000            (256)
#define TR_MAX_CLSCNT_COMP001            (  0)
#define TR_MAX_CLSCNT_COMP002            (  0)
#define TR_MAX_CLSCNT_COMP003            (  0)
#define TR_MAX_CLSCNT_COMP004            (  0)
#define TR_MAX_CLSCNT_COMP005            (  0)
#define TR_MAX_CLSCNT_COMP006            (  0)
#define TR_MAX_CLSCNT_COMP007            (  0)
#define TR_MAX_CLSCNT_COMP008            (  0)
#define TR_MAX_CLSCNT_COMP009            (  0)
#define TR_MAX_CLSCNT_COMP010            (  0)
#define TR_MAX_CLSCNT_COMP011            (  0)
#define TR_MAX_CLSCNT_COMP012            ( 64)
#ifndef TR_MAX_CLSCNT_COMP013
#define TR_MAX_CLSCNT_COMP013            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP014
#define TR_MAX_CLSCNT_COMP014            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP015
#define TR_MAX_CLSCNT_COMP015            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP016
#define TR_MAX_CLSCNT_COMP016            (256)  /* MC comp - was 32 */
#endif
#ifndef TR_MAX_CLSCNT_COMP017
#define TR_MAX_CLSCNT_COMP017            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP018
#define TR_MAX_CLSCNT_COMP018            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP019
#define TR_MAX_CLSCNT_COMP019            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP020
#define TR_MAX_CLSCNT_COMP020            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP021
#define TR_MAX_CLSCNT_COMP021            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP022
#define TR_MAX_CLSCNT_COMP022            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP023
#define TR_MAX_CLSCNT_COMP023            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP024
#define TR_MAX_CLSCNT_COMP024            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP025
#define TR_MAX_CLSCNT_COMP025            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP026
#define TR_MAX_CLSCNT_COMP026            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP027
#define TR_MAX_CLSCNT_COMP027            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP028
#define TR_MAX_CLSCNT_COMP028            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP029
#define TR_MAX_CLSCNT_COMP029            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP030
#define TR_MAX_CLSCNT_COMP030            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP031
#define TR_MAX_CLSCNT_COMP031            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP032
#define TR_MAX_CLSCNT_COMP032            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP033
#define TR_MAX_CLSCNT_COMP033            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP034
#define TR_MAX_CLSCNT_COMP034            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP035
#define TR_MAX_CLSCNT_COMP035            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP036
#define TR_MAX_CLSCNT_COMP036            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP037
#define TR_MAX_CLSCNT_COMP037            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP038
#define TR_MAX_CLSCNT_COMP038            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP039
#define TR_MAX_CLSCNT_COMP039            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP040
#define TR_MAX_CLSCNT_COMP040            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP041
#define TR_MAX_CLSCNT_COMP041            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP042
#define TR_MAX_CLSCNT_COMP042            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP043
#define TR_MAX_CLSCNT_COMP043            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP044
#define TR_MAX_CLSCNT_COMP044            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP045
#define TR_MAX_CLSCNT_COMP045            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP046
#define TR_MAX_CLSCNT_COMP046            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP047
#define TR_MAX_CLSCNT_COMP047            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP048
#define TR_MAX_CLSCNT_COMP048            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP049
#define TR_MAX_CLSCNT_COMP049            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP050
#define TR_MAX_CLSCNT_COMP050            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP051
#define TR_MAX_CLSCNT_COMP051            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP052
#define TR_MAX_CLSCNT_COMP052            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP053
#define TR_MAX_CLSCNT_COMP053            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP054
#define TR_MAX_CLSCNT_COMP054            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP055
#define TR_MAX_CLSCNT_COMP055            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP056
#define TR_MAX_CLSCNT_COMP056            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP057
#define TR_MAX_CLSCNT_COMP057            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP058
#define TR_MAX_CLSCNT_COMP058            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP059
#define TR_MAX_CLSCNT_COMP059            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP060
#define TR_MAX_CLSCNT_COMP060            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP061
#define TR_MAX_CLSCNT_COMP061            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP062
#define TR_MAX_CLSCNT_COMP062            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP063
#define TR_MAX_CLSCNT_COMP063            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP064
#define TR_MAX_CLSCNT_COMP064            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP065
#define TR_MAX_CLSCNT_COMP065            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP066
#define TR_MAX_CLSCNT_COMP066            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP067
#define TR_MAX_CLSCNT_COMP067            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP068
#define TR_MAX_CLSCNT_COMP068            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP069
#define TR_MAX_CLSCNT_COMP069            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP070
#define TR_MAX_CLSCNT_COMP070            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP071
#define TR_MAX_CLSCNT_COMP071            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP072
#define TR_MAX_CLSCNT_COMP072            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP073
#define TR_MAX_CLSCNT_COMP073            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP074
#define TR_MAX_CLSCNT_COMP074            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP075
#define TR_MAX_CLSCNT_COMP075            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP076
#define TR_MAX_CLSCNT_COMP076            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP077
#define TR_MAX_CLSCNT_COMP077            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP078
#define TR_MAX_CLSCNT_COMP078            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP079
#define TR_MAX_CLSCNT_COMP079            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP080
#define TR_MAX_CLSCNT_COMP080            (256)  /* MC comp - was 255 */
#endif
#ifndef TR_MAX_CLSCNT_COMP081
#define TR_MAX_CLSCNT_COMP081            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP082
#define TR_MAX_CLSCNT_COMP082            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP083
#define TR_MAX_CLSCNT_COMP083            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP084
#define TR_MAX_CLSCNT_COMP084            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP085
#define TR_MAX_CLSCNT_COMP085            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP086
#define TR_MAX_CLSCNT_COMP086            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP087
#define TR_MAX_CLSCNT_COMP087            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP088
#define TR_MAX_CLSCNT_COMP088            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP089
#define TR_MAX_CLSCNT_COMP089            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP090
#define TR_MAX_CLSCNT_COMP090            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP091
#define TR_MAX_CLSCNT_COMP091            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP092
#define TR_MAX_CLSCNT_COMP092            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP093
#define TR_MAX_CLSCNT_COMP093            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP094
#define TR_MAX_CLSCNT_COMP094            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP095
#define TR_MAX_CLSCNT_COMP095            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP096
#define TR_MAX_CLSCNT_COMP096            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP097
#define TR_MAX_CLSCNT_COMP097            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP098
#define TR_MAX_CLSCNT_COMP098            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP099
#define TR_MAX_CLSCNT_COMP099            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP100
#define TR_MAX_CLSCNT_COMP100            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP101
#define TR_MAX_CLSCNT_COMP101            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP102
#define TR_MAX_CLSCNT_COMP102            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP103
#define TR_MAX_CLSCNT_COMP103            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP104
#define TR_MAX_CLSCNT_COMP104            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP105
#define TR_MAX_CLSCNT_COMP105            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP106
#define TR_MAX_CLSCNT_COMP106            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP107
#define TR_MAX_CLSCNT_COMP107            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP108
#define TR_MAX_CLSCNT_COMP108            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP109
#define TR_MAX_CLSCNT_COMP109            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP110
#define TR_MAX_CLSCNT_COMP110            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP111
#define TR_MAX_CLSCNT_COMP111            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP112
#define TR_MAX_CLSCNT_COMP112            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP113
#define TR_MAX_CLSCNT_COMP113            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP114
#define TR_MAX_CLSCNT_COMP114            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP115
#define TR_MAX_CLSCNT_COMP115            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP116
#define TR_MAX_CLSCNT_COMP116            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP117
#define TR_MAX_CLSCNT_COMP117            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP118
#define TR_MAX_CLSCNT_COMP118            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP119
#define TR_MAX_CLSCNT_COMP119            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP120
#define TR_MAX_CLSCNT_COMP120            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP121
#define TR_MAX_CLSCNT_COMP121            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP122
#define TR_MAX_CLSCNT_COMP122            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP123
#define TR_MAX_CLSCNT_COMP123            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP124
#define TR_MAX_CLSCNT_COMP124            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP125
#define TR_MAX_CLSCNT_COMP125            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP126
#define TR_MAX_CLSCNT_COMP126            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP127
#define TR_MAX_CLSCNT_COMP127            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP128
#define TR_MAX_CLSCNT_COMP128            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP129
#define TR_MAX_CLSCNT_COMP129            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP130
#define TR_MAX_CLSCNT_COMP130            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP131
#define TR_MAX_CLSCNT_COMP131            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP132
#define TR_MAX_CLSCNT_COMP132            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP133
#define TR_MAX_CLSCNT_COMP133            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP134
#define TR_MAX_CLSCNT_COMP134            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP135
#define TR_MAX_CLSCNT_COMP135            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP136
#define TR_MAX_CLSCNT_COMP136            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP137
#define TR_MAX_CLSCNT_COMP137            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP138
#define TR_MAX_CLSCNT_COMP138            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP139
#define TR_MAX_CLSCNT_COMP139            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP140
#define TR_MAX_CLSCNT_COMP140            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP141
#define TR_MAX_CLSCNT_COMP141            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP142
#define TR_MAX_CLSCNT_COMP142            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP143
#define TR_MAX_CLSCNT_COMP143            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP144
#define TR_MAX_CLSCNT_COMP144            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP145
#define TR_MAX_CLSCNT_COMP145            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP146
#define TR_MAX_CLSCNT_COMP146            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP147
#define TR_MAX_CLSCNT_COMP147            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP148
#define TR_MAX_CLSCNT_COMP148            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP149
#define TR_MAX_CLSCNT_COMP149            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP150
#define TR_MAX_CLSCNT_COMP150            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP151
#define TR_MAX_CLSCNT_COMP151            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP152
#define TR_MAX_CLSCNT_COMP152            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP153
#define TR_MAX_CLSCNT_COMP153            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP154
#define TR_MAX_CLSCNT_COMP154            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP155
#define TR_MAX_CLSCNT_COMP155            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP156
#define TR_MAX_CLSCNT_COMP156            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP157
#define TR_MAX_CLSCNT_COMP157            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP158
#define TR_MAX_CLSCNT_COMP158            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP159
#define TR_MAX_CLSCNT_COMP159            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP160
#define TR_MAX_CLSCNT_COMP160            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP161
#define TR_MAX_CLSCNT_COMP161            (256)  /* MC comp - was 32 */
#endif
#ifndef TR_MAX_CLSCNT_COMP162
#define TR_MAX_CLSCNT_COMP162            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP163
#define TR_MAX_CLSCNT_COMP163            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP164
#define TR_MAX_CLSCNT_COMP164            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP165
#define TR_MAX_CLSCNT_COMP165            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP166
#define TR_MAX_CLSCNT_COMP166            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP167
#define TR_MAX_CLSCNT_COMP167            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP168
#define TR_MAX_CLSCNT_COMP168            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP169
#define TR_MAX_CLSCNT_COMP169            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP170
#define TR_MAX_CLSCNT_COMP170            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP171
#define TR_MAX_CLSCNT_COMP171            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP172
#define TR_MAX_CLSCNT_COMP172            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP173
#define TR_MAX_CLSCNT_COMP173            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP174
#define TR_MAX_CLSCNT_COMP174            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP175
#define TR_MAX_CLSCNT_COMP175            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP176
#define TR_MAX_CLSCNT_COMP176            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP177
#define TR_MAX_CLSCNT_COMP177            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP178
#define TR_MAX_CLSCNT_COMP178            (256)  /* MC comp - was 0 */
#endif
#ifndef TR_MAX_CLSCNT_COMP179
#define TR_MAX_CLSCNT_COMP179            (256)  /* MC comp - was 0 */
#endif
#define TR_MAX_CLSCNT_COMP180            (256)
#define TR_MAX_CLSCNT_COMP181            (256)
#define TR_MAX_CLSCNT_COMP182            (256)
#define TR_MAX_CLSCNT_COMP183            (256)
#define TR_MAX_CLSCNT_COMP184            (256)
#define TR_MAX_CLSCNT_COMP185            (256)
#define TR_MAX_CLSCNT_COMP186            (256)
#define TR_MAX_CLSCNT_COMP187            (256)
#define TR_MAX_CLSCNT_COMP188            (256)
#define TR_MAX_CLSCNT_COMP189            (256)
#define TR_MAX_CLSCNT_COMP190            (256)
#define TR_MAX_CLSCNT_COMP191            (256)
#define TR_MAX_CLSCNT_COMP192            (256)
#define TR_MAX_CLSCNT_COMP193            (256)
#define TR_MAX_CLSCNT_COMP194            (256)
#define TR_MAX_CLSCNT_COMP195            (256)
#define TR_MAX_CLSCNT_COMP196            (256)
#define TR_MAX_CLSCNT_COMP197            (256)
#define TR_MAX_CLSCNT_COMP198            (256)
#define TR_MAX_CLSCNT_COMP199            (256)
#define TR_MAX_CLSCNT_COMP200            (256)
#define TR_MAX_CLSCNT_COMP201            (256)
#define TR_MAX_CLSCNT_COMP202            (256)
#define TR_MAX_CLSCNT_COMP203            (  0)
#define TR_MAX_CLSCNT_COMP204            (  0)
#define TR_MAX_CLSCNT_COMP205            (  0)
#define TR_MAX_CLSCNT_COMP206            (  0)
#define TR_MAX_CLSCNT_COMP207            (  0)
#define TR_MAX_CLSCNT_COMP208            (  0)
#define TR_MAX_CLSCNT_COMP209            (  0)
#define TR_MAX_CLSCNT_COMP210            (  0)
#define TR_MAX_CLSCNT_COMP211            (  0)
#define TR_MAX_CLSCNT_COMP212            (  0)
#define TR_MAX_CLSCNT_COMP213            (  0)
#define TR_MAX_CLSCNT_COMP214            (  0)
#define TR_MAX_CLSCNT_COMP215            (  0)
#define TR_MAX_CLSCNT_COMP216            (  0)
#define TR_MAX_CLSCNT_COMP217            (  0)
#define TR_MAX_CLSCNT_COMP218            (  0)
#define TR_MAX_CLSCNT_COMP219            (  0)
#define TR_MAX_CLSCNT_COMP220            (  0)
#define TR_MAX_CLSCNT_COMP221            (  0)
#define TR_MAX_CLSCNT_COMP222            (  0)
#define TR_MAX_CLSCNT_COMP223            (  0)
#define TR_MAX_CLSCNT_COMP224            (  0)
#define TR_MAX_CLSCNT_COMP225            (  0)
#define TR_MAX_CLSCNT_COMP226            (  0)
#define TR_MAX_CLSCNT_COMP227            (  0)
#define TR_MAX_CLSCNT_COMP228            (  0)
#define TR_MAX_CLSCNT_COMP229            (  0)
#define TR_MAX_CLSCNT_COMP230            (  0)
#define TR_MAX_CLSCNT_COMP231            (  0)
#define TR_MAX_CLSCNT_COMP232            (  0)
#define TR_MAX_CLSCNT_COMP233            (  0)
#define TR_MAX_CLSCNT_COMP234            (  0)
#define TR_MAX_CLSCNT_COMP235            (  0)
#define TR_MAX_CLSCNT_COMP236            (  0)
#define TR_MAX_CLSCNT_COMP237            (  0)
#define TR_MAX_CLSCNT_COMP238            (  0)
#define TR_MAX_CLSCNT_COMP239            (  0)
#define TR_MAX_CLSCNT_COMP240            (  0)
#define TR_MAX_CLSCNT_COMP241            (  0)
#define TR_MAX_CLSCNT_COMP242            (  0)
#define TR_MAX_CLSCNT_COMP243            (  0)
#define TR_MAX_CLSCNT_COMP244            (  0)
#define TR_MAX_CLSCNT_COMP245            (  0)
#define TR_MAX_CLSCNT_COMP246            (  0)
#define TR_MAX_CLSCNT_COMP247            (  0)
#define TR_MAX_CLSCNT_COMP248            (  0)
#define TR_MAX_CLSCNT_COMP249            (  0)
#define TR_MAX_CLSCNT_COMP250            (  0)
#define TR_MAX_CLSCNT_COMP251            (  0)
#define TR_MAX_CLSCNT_COMP252            (  0)
#define TR_MAX_CLSCNT_COMP253            (  0)
#define TR_MAX_CLSCNT_COMP254            (  0)
#define TR_MAX_CLSCNT_COMP255            (  0)

#endif /* ADIT_TRACE_H */
