/* ******************************************************FileHeaderBegin** *//**
 *
 * @file        flashblocksize.h
 *
 *  header for the size of the flashblock cluster  
 *
 * @date        2010-06-22
 *
 * @note
 *
 *  &copy; Copyright TMS GmbH Hildesheim. All Rights reserved!
 *
 *//* ***************************************************FileHeaderEnd******* */
#ifndef FLASHBLOCK_SIZE_H
#define FLASHBLOCK_SIZE_H

/************************************************************************ 
|typedefs and struct defs
|-----------------------------------------------------------------------*/

/************************************************************************ 
|defines and macros 
|-----------------------------------------------------------------------*/
/* size of one cluster for FFD and KDS*/
#define FLASHBLOCK_CLUSTER_SIZE_FFD 0x8000   /*32k cluster*/
#define FLASHBLOCK_CLUSTER_SIZE_KDS 0x8000   /*32k cluster*/

#else
#error flashblocksize.h included several times
#endif 

