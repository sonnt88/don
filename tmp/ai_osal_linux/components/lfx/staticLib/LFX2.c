#include "LFX2.h"
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <sys/msg.h>
#include <signal.h>
#include <errno.h>
#include "../daemon/LFXdaemon.h"



struct LFXinst
{
	LFXpart usr;
	int d_mq_h;		// handle of daemon's message queue.
	int r_mq_h;		// handle for feedback message queue.
	char hand;		// daemon handle.
	unsigned int MQnum;		// ID of meedback message queue.
};

// helper function for sending message to MQ.
static int sendAndWait( int mqS , int mqR , char *buffer , unsigned int msgSize , unsigned int bufferSize , int replyCode );

// #define DUMMY_INIT_HEAP_BUFFERS		// valgrind wants to see the WHOLE heap buffer memset-ed before use.... Enable this for valgrind only.


static LFXhandle LFX_internalOpen( char *buffer , unsigned int bufferSize );

LFXhandle LFX_open(const char *name)
{
  struct LFXinst *res;
  unsigned int para1,para2,para3;
  char mbuf[256];
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)mbuf;
  int rv;
	memset( mbuf , 0xFE , sizeof(mbuf) );		// valgrind likes to see this.
	res = (struct LFXinst*)LFX_internalOpen(mbuf,sizeof(mbuf));
	if(!res)
		return 0;	// leave errno from LFX_internalOpen().
	// send open
	msg_b->cmd = CMD_OPENDEV;
	msg_b->byt = res->hand;
	strcpy( mbuf+sizeof(*msg_b) , name );
	rv = sendAndWait( res->d_mq_h , res->r_mq_h , mbuf , sizeof(*msg_b)+strlen(name) , sizeof(mbuf) , REP_OPENDEV );
	if( rv<0 )
		{LFX_close(&(res->usr));errno=rv;return 0;}
	if( (unsigned int)rv<sizeof(*msg_b) || msg_b->byt!=0 )
		{LFX_close(&(res->usr));errno=LFX_ERROR_INVALID_PART_NAME;return 0;}
	// send info query
	msg_b->cmd = CMD_INFO;
	msg_b->byt = res->hand;
	rv = sendAndWait( res->d_mq_h , res->r_mq_h , mbuf , sizeof(*msg_b) , sizeof(mbuf) , REP_INFO );
	if( rv<0 )
		{LFX_close(&(res->usr));errno=rv;return 0;}
	{
	  struct LFXmsg_b_iii *msg = (struct LFXmsg_b_iii*)mbuf;
		para1 = msg->para1;
		para2 = msg->para2;
		para3 = msg->para3;
		if( (unsigned int)rv<sizeof(*msg) || msg->byt!=0 ) // || (para1&0x80000000u) || (para2&0x80000000u) )
			{LFX_close(&(res->usr));errno=LFX_ERROR_DEVICE_OTHER_ERR;return 0;}
	}
	// done. We are connected.
	res->usr.blockSize = para1;
	res->usr.chunkSize = para2;
	res->usr.numBlocks = para3;
	res->usr.hasECC = (para2>1);
	return (LFXhandle)res;
}

void LFX_close( LFXhandle part )
{
  struct LFXinst *self = (struct LFXinst*)part;
  char mbuf[128];
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)mbuf;
	memset( mbuf , 0xFE , sizeof(mbuf) );		// valgrind likes to see this.
	if(!self)return;
	if(self->hand>=0)
	{
		// send disconnect
		memset( msg_b , 0x55 , sizeof(*msg_b) );
		msg_b->cmd = CMD_DISCONNECT;
		msg_b->byt = self->hand;
		sendAndWait( self->d_mq_h , self->r_mq_h , mbuf , sizeof(*msg_b) , sizeof(mbuf) , -1 );
		// ignore result.
	}
	if(self->r_mq_h)
		msgctl( self->r_mq_h , IPC_RMID , 0 );
	free(self);
}

int LFX_read( LFXhandle part , void *buffer , unsigned int size , unsigned int offset )
{
  struct LFXinst *self = (struct LFXinst*)part;
  char mbuf[4096+16+16];
  int rv;
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)mbuf;
  unsigned int rdSize;
	if(!self)return LFX_ERROR_OTHER;
#ifdef DUMMY_INIT_HEAP_BUFFERS
	memset( mbuf , 0xFE , sizeof(mbuf) );
#endif
	while(size>0)
	{
	  struct LFXmsg_b_ii *msg1 = (struct LFXmsg_b_ii*)mbuf;
		rdSize = size;
		if( rdSize>4096 )
			{rdSize = 4096-(offset&511);}
		msg1->cmd = CMD_READ;
		msg1->byt = self->hand;
		msg1->para1 = offset;
		msg1->para2 = rdSize;
		rv = sendAndWait( self->d_mq_h , self->r_mq_h , mbuf , sizeof(*msg1) , sizeof(mbuf) , REP_READ );
		if( rv<0 )return rv;
		if( (unsigned int)rv<sizeof(*msg_b)+rdSize || msg_b->byt!=0 )
			return LFX_ERROR_DEVICE_READ_ERR;
		memcpy( buffer , mbuf+sizeof(*msg_b) , rdSize );
		size-=rdSize;
		offset+=rdSize;
		buffer=((char*)buffer)+rdSize;
	}
	return 0;
}

int LFX_write( LFXhandle part , const void *buffer , unsigned int size , unsigned int offset )
{
  struct LFXinst *self = (struct LFXinst*)part;
  char mbuf[4096+16+16];
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)mbuf;
  int rv;
  unsigned int wrSize;
	if(!self)return LFX_ERROR_OTHER;
#ifdef DUMMY_INIT_HEAP_BUFFERS
	memset( mbuf , 0xFE , sizeof(mbuf) );
#endif
	while(size>0)
	{
	  struct LFXmsg_b_i *msg1 = (struct LFXmsg_b_i*)mbuf;
		wrSize = size;
		if( wrSize>4096 )
			{wrSize = 4096-(offset&511);}
		msg1->cmd = CMD_WRITE;
		msg1->byt = self->hand;
		msg1->para = offset;
		memcpy( mbuf+sizeof(*msg1) , buffer , wrSize );
		rv = sendAndWait( self->d_mq_h , self->r_mq_h , mbuf , sizeof(*msg1)+wrSize , sizeof(mbuf) , REP_WRITE );
		if( rv<0 )return rv;
		if( (unsigned int)rv<sizeof(*msg_b) || msg_b->byt!=0 )
			return LFX_ERROR_DEVICE_WRITE_ERR;
		size-=wrSize;
		offset+=wrSize;
		buffer=((const char*)buffer)+wrSize;
	}
	return 0;
}

int LFX_erase( LFXhandle part , unsigned size , unsigned int offset )
{
  struct LFXinst *self = (struct LFXinst*)part;
  char mbuf[128];
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)mbuf;
  struct LFXmsg_b_ii *msg1 = (struct LFXmsg_b_ii*)mbuf;
  int rv;
	if(!self)return LFX_ERROR_OTHER;
	memset( mbuf , 0xFE , sizeof(mbuf) );		// valgrind likes to see this.
	msg1->cmd = CMD_ERASE;
	msg1->byt = self->hand;
	msg1->para1 = offset;
	msg1->para2 = size;
	rv = sendAndWait( self->d_mq_h , self->r_mq_h , mbuf , sizeof(*msg1) , sizeof(mbuf) , REP_ERASE );
	if( rv<0 )return rv;
	if( (unsigned int)rv<sizeof(*msg_b) || msg_b->byt!=0 )
		return LFX_ERROR_DEVICE_ERASE_ERR;
	return 0;
}

int LFX_testFullOperation( LFXhandle part )
{
  struct LFXinst *self = (struct LFXinst*)part;
	if(!self)return LFX_ERROR_OTHER;
	return 0;
}

const char *LFX_queryNames()
{
  struct LFXinst *self;
  char mbuf[512];
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)mbuf;
  char *res;
  int rv;
  int i;
	memset( mbuf , 0xFE , sizeof(mbuf) );		// valgrind likes to see this.
	self = (struct LFXinst*)LFX_internalOpen(mbuf,sizeof(mbuf));
	if(!self)
		return 0;	// leave errno from LFX_internalOpen().
	// call query
	memset( msg_b , 0x55 , sizeof(*msg_b) );
	msg_b->cmd = CMD_LIST_DEVS;
	msg_b->byt = self->hand;
	rv = sendAndWait( self->d_mq_h , self->r_mq_h , mbuf , sizeof(*msg_b) , sizeof(mbuf)-2 , REP_LIST_DEVS );
	LFX_close(&(self->usr));
	if( rv<0 )
		{errno=rv;return 0;}
	mbuf[rv++] = 0;
	mbuf[rv++] = 0;
	// count length
	for( i=sizeof(struct LFXmsg_none)+1 ; i<rv ; i++ )
	{
		if( mbuf[i]==0 && mbuf[i-1]==0 )
			break;
	}
	i++;
	// rv now points to first byte not in include in result.
	res = (char*)malloc(i-sizeof(struct LFXmsg_none));
	if(!res)
		{errno=LFX_ERROR_OTHER;return 0;}
	memcpy( res , mbuf+sizeof(struct LFXmsg_none) , i-sizeof(struct LFXmsg_none) );
	return (const char*)res;
}

// internal open opens, but does not do CMD_OPENDEV.
static LFXhandle LFX_internalOpen( char *buffer , unsigned int bufferSize )
{
  struct LFXinst *res;
  struct LFXmsg_b_i *msg1 = (struct LFXmsg_b_i*)buffer;
  struct LFXmsg_bb *msg2 = (struct LFXmsg_bb*)buffer;
  int rv;
  unsigned int tryNum;
	res = (struct LFXinst*)malloc(sizeof(*res));
	if(!res)
		{errno=LFX_ERROR_OTHER;return 0;}
	memset( res , 0 , sizeof(*res) );
	res->d_mq_h = -1;
	res->r_mq_h = -1;
	res->hand = -1;
	// open daemon's queue
	res->d_mq_h = msgget( LFXDAEMON_MSG_Q , 0 );
	if( res->d_mq_h<0 )
		{LFX_close(&(res->usr));errno=LFX_ERROR_NOT_AVAILABLE;return 0;}
	// create our queue
	for( tryNum = 0x400000 ; tryNum<0x500000 ; tryNum++ )
	{
		res->r_mq_h = msgget( tryNum ,  IPC_CREAT|IPC_EXCL|0x1B6 );
		if( res->r_mq_h>=0 )break;
		if( errno!=EEXIST )break;
	}
	if( res->r_mq_h<0 )
		{LFX_close(&(res->usr));errno=LFX_ERROR_CANT_CREATE_MQ;return 0;}
	res->MQnum = tryNum;
	// send connect packet
	msg1->cmd = CMD_CONNECT;
	msg1->byt = 0x2A;
	msg1->para = res->MQnum ;
	rv = sendAndWait( res->d_mq_h , res->r_mq_h , buffer , sizeof(*msg1) , bufferSize , REP_CONNECT );
	if( rv<0 )
		{LFX_close(&(res->usr));errno=rv;return 0;}
	if( (unsigned int)rv<sizeof(*msg2) || msg2->byt1!=0 )
		{LFX_close(&(res->usr));errno=LFX_ERROR_DEVICE_OTHER_ERR;return 0;}
	res->hand = msg2->byt2;
	return (LFXhandle)res;
}


static void dummySigFunc(int sig)
{}

typedef void (*sigHdlFptr)(int);

static int sendAndWait( int mqS , int mqR , char *buffer , unsigned int msgSize , unsigned int bufferSize , int replyCode )
{
  int rv;
  sigHdlFptr orgh;
  struct LFXmsg_none *msg = (struct LFXmsg_none*)buffer;
	// send msg
	//printf("mq_send\n");
	rv = msgsnd( mqS , buffer , msgSize , 0 );
	if(rv!=0)
		return LFX_ERROR_OTHER;
	// wait for reply
	orgh = signal(SIGALRM,dummySigFunc);
	alarm(6);
	while(1)
	{
		//printf("mq_recv ");
		rv = msgrcv( mqR , buffer , bufferSize , 0 , 0 );
		//printf("rv=%d\n",rv);
		if( rv>=0 && (unsigned int)rv>=sizeof(*msg) && msg->cmd==REP_STOPPING )
		{	// daemon is shutting down.
			rv = LFX_ERROR_NOT_AVAILABLE;
			break;
		}
		if( rv>=0 && replyCode>=0 && ((unsigned int)rv<sizeof(*msg)||msg->cmd!=replyCode) )
		{
			continue;	// want another message
		}
		if( rv<0 )
		{
			rv = LFX_ERROR_OTHER;
			if( errno==EINTR )
				rv = LFX_ERROR_DAEMON_TIMEOUT;
		}
		break;
	}
	alarm(0);
	signal(SIGALRM,orgh);
	return rv;
}
