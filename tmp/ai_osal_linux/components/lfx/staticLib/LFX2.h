#ifndef LFX2_H
#define LFX2_H

// 'LFX' Interface.
// used to allow PDD and errmem to access the NOR areas.
// 
// CF-32  Dahlhoff



// Structure for LFX. Returned by LFX_open(). Use 'LFXhandle' type.  DO NOT MODIFY any fields in it. read only.
typedef struct _LFXpart
{
	const char *name;			// name used to open
	unsigned int intID;			// internal ID
	unsigned int blockSize;		// size of one erase-block
	unsigned int chunkSize;		// minimum recommended size to write
	unsigned int numBlocks;		// number of blocks.
	char hasECC;				// does have internal ECC
} LFXpart;

// LFX handle datatype. Use with all LFX functions.
typedef const LFXpart* LFXhandle;

// Error codes for all functions. In case of LFX_open(), it is returned in 'errno'.
#define LFX_ERROR_NOT_AVAILABLE     -1
#define LFX_ERROR_INVALID_PART_NAME -2
#define LFX_ERROR_DEVICE_READ_ERR   -3
#define LFX_ERROR_DEVICE_WRITE_ERR  -4
#define LFX_ERROR_DEVICE_ERASE_ERR  -5
#define LFX_ERROR_DEVICE_OTHER_ERR  -6
#define LFX_ERROR_DEVICE_NOT_READY  -7
#define LFX_ERROR_DEVICE_BAD_ARGS   -8
#define LFX_ERROR_ALREADY_OPEN      -9
#define LFX_ERROR_DAEMON_TIMEOUT   -10
#define LFX_ERROR_CANT_CREATE_MQ   -11
#define LFX_ERROR_OTHER            -12




// interface functions
#ifdef __cplusplus
extern "C" {
#endif

// access a partition.
// use string name for partition choice such as 'KDS' or 'errmem'.
// This is exclusive. Will not allow multiple opens. TODO: should be exclusive?
LFXhandle LFX_open(const char *name);		// if this fails, check errno for reason.

// close a partition (DO NOT FORGET THIS! posix message-queues are very limited in linux.).
void LFX_close( LFXhandle part );

// read data. No alignment requirements.
// returns 0 for OK.
int LFX_read( LFXhandle part , void *buffer , unsigned int size , unsigned int offset );

// write data. Should be aligned to chunkSize.
// returns 0 for OK.
int LFX_write( LFXhandle part , const void *buffer , unsigned int size , unsigned int offset );

// erase a block. Offset is in bytes. Must be multiple of blockSize.
// returns 0 for OK.
int LFX_erase( LFXhandle part , unsigned size , unsigned int offset );

// test if out of early-startup phase.
// Returns LFX_ERROR_DEVICE_NOT_READY if sill in early startup.
// Returns zero if fully available.
int LFX_testFullOperation( LFXhandle part );

// quers the list of all available devices.
// This function returns a buffer with strings in a sequence.
// strings are null-terminated. Second nullbyte after last name.
// free the returnvalue with free().
const char *LFX_queryNames();


#ifdef __cplusplus
}
#endif



#endif
