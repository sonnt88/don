#ifndef LFXDAEMON__MTD_CLIENT_H
#define LFXDAEMON__MTD_CLIENT_H

#include <unistd.h>


// types for file open, close, pwrite64, pread64, ioctl. Provided to allow unit-tests to override them.
typedef int (*type_open)(const char*,int,...);
typedef int (*type_close)(int);
typedef int (*type_ioctl)(int,long unsigned int,...);
typedef ssize_t (*type_read)(int,void*,size_t);
typedef ssize_t (*type_write)(int,const void*,size_t);
typedef off_t (*type_lseek)(int,off_t,int);


// struct for the mtd client.
struct mtdClient
{
	const char *devName;			// name of mtd device
	int fh;							// filehandle (from open())
	unsigned int eraseSize;			// eraseSize reported from MTD
	unsigned int writeSize;			// writeSize reported from MTD
	unsigned int numEraseBlocks;	// number of eraseblocks to use.
	unsigned int skipEraseBlocks;	// number of eraseblocks to skip.

	type_open     open;
	type_close    close;
	type_ioctl    ioctl;
	type_read     read;
	type_write    write;
	type_lseek    lseek;
};

// struct to pass parameters to the mtdClient_init function
struct mtdClient_init
{
	// Name of MTD device to use (e.g. 'mtd2', the '/dev/' will be prepended)
	const char *devName;
	unsigned long startOffset;
	unsigned long endOffset;		// use null to use all up to end.
	// functions to overload for unit-tests. Pass Null for normal operation. This will use the default system functinos.
	type_open     open;
	type_close    close;
	type_ioctl    ioctl;
	type_read     read;
	type_write    write;
	type_lseek    lseek;
};


#ifdef __cplusplus
extern "C" {
#endif

// init. devname is name without '/dev/' part.
struct mtdClient *mtdClient_init( struct mtdClient_init *para );

// uninit. close and deallocate.
void mtdClient_uninit( struct mtdClient *self );

// erase block
int mtdClient_erase( struct mtdClient *self , unsigned int offset , unsigned int length );

// write block
int mtdClient_write( struct mtdClient *self , unsigned int offset , unsigned int length , const char *buffer );

// read block
int mtdClient_read( struct mtdClient *self , unsigned int offset , unsigned int length , char *buffer );

#ifdef __cplusplus
}
#endif





#endif
