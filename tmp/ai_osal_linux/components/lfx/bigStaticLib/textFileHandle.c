#include "textFileHandle.h"
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>
#include <errno.h>


struct textFileHandle *filOpen( const char *name )
{
  struct textFileHandle *res;
  int i;
	res = (struct textFileHandle *)malloc(sizeof(struct textFileHandle));
	if(!res)
		{errno=ENOMEM;return 0;}
	memset( res , 0 , sizeof(*res) );
	res->fd = open( name , O_RDONLY );
	if( res->fd<0 )
		{free(res);return 0;}
	i = read( res->fd , res->rdbuf , sizeof(res->rdbuf) );
	if( i<0 )
		{close(res->fd);free(res);return 0;}
	res->bytes = (unsigned int)i;
	if(!i)res->eof=1;
	return res;
}

const char *filRead( struct textFileHandle *h )
{
  unsigned int i;
  int rv;
	// find a line-end or end-of-buffer or end-of-file.
	for( i=1 ; i<=h->bytes ; i++ )
	{
		if( (h->rdbuf[i-1]==10) || (i==sizeof(h->rdbuf)) || ((h->eof)&&(i==h->bytes)) )
		{
		  unsigned int ri;
			ri = i;
			if( ri >= sizeof(h->linbuf) )
				ri = sizeof(h->linbuf)-1;
			memcpy( h->linbuf , h->rdbuf , ri );
			h->linbuf[ri]=0;
			while( ri>0 && ( h->linbuf[ri-1]=='\r' || h->linbuf[ri-1]=='\n' ) )
				h->linbuf[--ri]=0;
			h->bytes -= i;
			if(h->bytes>0)
				memmove( h->rdbuf , h->rdbuf+i , h->bytes );
			return h->linbuf;
		}
	}
	// no hit. read more data.
	if( h->eof )return 0;
	rv = read( h->fd , h->rdbuf+h->bytes , sizeof(h->rdbuf)-h->bytes );
	i = ( rv>=0 ? (unsigned int)rv : 0 );	// Treat read-error like EOF.
	h->bytes += i;
	if(!i)
		h->eof=1;
	return filRead(h);
}

int filClose( struct textFileHandle *h )
{
	if(h)
	{
		close(h->fd);
		free(h);
	}
	return 0;
}
