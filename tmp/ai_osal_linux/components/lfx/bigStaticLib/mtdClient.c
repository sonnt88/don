#include "mtdClient.h"
#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include <fcntl.h>
#include <sys/ioctl.h>    // for ioctl command.
#include <mtd/mtd-abi.h>  // for MTD ioctl commands and structs.


struct mtdClient *mtdClient_init( struct mtdClient_init *para )
{
  struct mtdClient *res;
  int tmp;
  int rv;
  struct mtd_info_user mtdinf;
  char *dnam;
  unsigned long start,end;
	if( para->endOffset && para->endOffset<para->startOffset )
		return 0;	// bad parameter.
	// alloc struct and name
	tmp = strlen(para->devName);
	res = (struct mtdClient *)malloc(sizeof(*res)+5+4+tmp);
	if(!res)return 0;
	memset( res , 0 , sizeof(*res) );
	res->fh = -1;
	// set functions pointers
	res->open = open;
	res->close = close;
	res->ioctl = ioctl;
	res->read = read;
	res->write = write;
	res->lseek = lseek;
	if( para->open )res->open=para->open;
	if( para->close )res->close=para->close;
	if( para->ioctl )res->ioctl=para->ioctl;
	if( para->open )res->open=para->open;
	if( para->read )res->read=para->read;
	if( para->write )res->write=para->write;
	if( para->lseek )res->lseek=para->lseek;
	// build device name (prepend "/dev/")
	dnam = (char*)(res+1);
	res->devName = dnam;
	sprintf( dnam , "/dev/%s" , para->devName );
	// open device
	res->fh = res->open( res->devName , O_RDWR );
	if( res->fh < 0 )
		{mtdClient_uninit(res);return 0;}
	// get info
	memset( &mtdinf , 0 , sizeof(mtdinf) );
	rv = res->ioctl( res->fh , MEMGETINFO , &mtdinf );
	if(rv)
		{mtdClient_uninit(res);return 0;}
	res->eraseSize = (unsigned int)mtdinf.erasesize;
	res->writeSize = (unsigned int)mtdinf.writesize;
	res->numEraseBlocks = (unsigned int)(mtdinf.size/mtdinf.erasesize);
	// if end is zero, up to end.
	start = para->startOffset;
	end   = para->endOffset;
	if(!end)
		end=((unsigned long)res->numEraseBlocks)*res->eraseSize;
	// check start and end.
	if( (start%res->writeSize) || (start%res->eraseSize) || (end%res->writeSize) || (end%res->eraseSize) || ((end/res->eraseSize)>res->numEraseBlocks) )
		{mtdClient_uninit(res);return 0;}		// invalid requested start and end.
	// set start and end.
	res->skipEraseBlocks = start/res->eraseSize;
	res->numEraseBlocks = (end-start)/res->eraseSize;
	// ok.
	return res;
}

void mtdClient_uninit( struct mtdClient *self )
{
	if(self)
	{
		if( self->fh >= 0 )
			self->close( self->fh );
		free(self);
	}
}

int mtdClient_erase( struct mtdClient *self , unsigned int offset , unsigned int length )
{
  struct erase_info_user64 einfo;
  int rv;
	if(!self)return -1;
	if(length==0)return 0;
	if( (offset%self->eraseSize) || (length%self->eraseSize) )
		return -1;
	memset( &einfo , 0 , sizeof(einfo) );
	einfo.start = offset + ((unsigned long)self->skipEraseBlocks)*self->eraseSize;
	einfo.length = length;
	// check
	offset /= self->eraseSize;
	length /= self->eraseSize;
	if( offset+length > self->numEraseBlocks )
		return -1;	// too high.
	rv = self->ioctl( self->fh , MEMERASE64 , &einfo );
	if(rv)return -1;
	return 0;
}

int mtdClient_write( struct mtdClient *self , unsigned int offset , unsigned int length , const char *buffer )
{
  long rv;
  unsigned long maxSize;
	if(!self)return -1;
	if(length==0)return 0;
	if( self->writeSize && ( (offset%self->writeSize) || (length%self->writeSize) ) )
		return -1;	// bad alignment
	maxSize = self->numEraseBlocks * self->eraseSize;
	if( offset>maxSize || length>maxSize || (offset+length)>maxSize )
		return -1;	// bad offset. too high.
  unsigned long totalOffset;
	totalOffset = offset + ((unsigned long)self->skipEraseBlocks)*self->eraseSize;
	rv=-1;
	if(totalOffset >= 0xFFFFFFFFul )
		return -1;		// does not support long address.
	rv = self->lseek( self->fh , (off_t)totalOffset , SEEK_SET );
	if( rv!=-1 )
		rv = self->write( self->fh , buffer , length );
	if( rv<0 || ((unsigned long)rv)!=length )
		return -1;
	return 0;
}

int mtdClient_read( struct mtdClient *self , unsigned int offset , unsigned int length , char *buffer )
{
  long rv;
  unsigned long maxSize;
	if(!self)return -1;
	if(length==0)return 0;
	maxSize = self->numEraseBlocks * self->eraseSize;
	if( offset>maxSize || length>maxSize || (offset+length)>maxSize )
		return -1;	// bad offset. too high.
  unsigned long totalOffset;
	totalOffset = offset + ((unsigned long)self->skipEraseBlocks)*self->eraseSize;
	rv=-1;
	if(totalOffset >= 0xFFFFFFFFul )
		return -1;		// does not support long address.
	rv = self->lseek( self->fh , (off_t)totalOffset , SEEK_SET );
	if( rv!=-1 )
		rv = self->read( self->fh , buffer , length );
	if( rv<0 || ((unsigned long)rv)!=length )
		return -1;
	return 0;
}
