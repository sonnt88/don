#include "partConfig.h"
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <syslog.h>
#include <errno.h>
#include <fcntl.h>		// for open/close
#include <sys/stat.h>
#include "textFileHandle.h"



static unsigned int skipWhitespace( const char **linPtr );
static char parseString( const char **linPtr , char *out_string , unsigned int outSize );
static char parseInt( const char **linPtr , int *out_result );
static char parseUIntHex( const char **linPtr , unsigned int *out_result );

static void createDefaultLfxConfigFile(const char *filename);


// read and parse the  /proc/mtd  file.
// returns an array with the data structs.
// free with free() (from <malloc.h>)
struct LFXmtdpart_info *parseMtdProcFile(const char *filename,unsigned int *out_numparts)
{
  struct textFileHandle *fh;
  const char *lin;
  unsigned int lineNo;
  struct LFXmtdpart_info resTemp[32];
  unsigned int resNumber;
  struct LFXmtdpart_info *res;
	fh = filOpen( filename );
	if(!fh)return 0;
	resNumber = 0;

	lineNo = 0;

	// first line is the head line.
	lin = filRead( fh );
	lineNo++;
	if(!lin){filClose(fh);return 0;}		// bad. empty file.
	// just check that is starts with 'dev:'
	if( lin[0]!='d' || lin[1]!='e' || lin[2]!='v' || lin[3]!=':' )
		{filClose(fh);return 0;}

	memset( resTemp , 0xFE , sizeof(resTemp) );	// not needed, but LINT misinterprets the code and nags. Valgrind does not need it. Is rarely called, so does not hurt.

	// loop lines
	while(1)
	{
	  struct LFXmtdpart_info part;
	  int i;
		lin = filRead( fh );
		if(!lin)break;
		lineNo++;
		skipWhitespace(&lin);	// skip leading whitespace
		if( *lin==0 || *lin==10 || *lin==13 )
			{lin=0;break;}		// proc file has no empty lines. If empty line, exit loop. done.
		// line is mtdname, colon, two hex values, one string name.
		memset( &part , 0 , sizeof(part) );
		for( i=0 ; i+1<MAX_DEV_MTDNAME ; i++ )
		{
			if( !(*lin) || *lin==':' )break;
			part.DEVmtdname[i] = *(lin++);
		}
		if( *lin != ':' )break;
		lin++;
		if(!skipWhitespace(&lin))break;
		if(!parseUIntHex( &lin , &(part.size) ))break;
		if(!skipWhitespace(&lin))break;
		if(!parseUIntHex( &lin , &(part.eraseSize) ))break;
		if(!skipWhitespace(&lin))break;
		if(!parseString( &lin , part.DEVnicename , MAX_DEV_NICENAME ))break;
		skipWhitespace(&lin);
		// line must end here. If not, error.
		if( *lin && *lin!=10 && *lin!=13 )
			break;
		// line is good.
		if( resNumber >= sizeof(resTemp)/sizeof(struct LFXmtdpart_info) )
			break;	// too many items
		resTemp[resNumber++] = part;
	}
	// done. If 'lin' pointer is not null, there was an error.
	filClose(fh);
	if( lin != 0 )
	{
		syslog( 3 , "error parsing '/proc/mtd', line %u. bad line or too many items. Line must have mtdname, colon, 2 hex values and a string." , lineNo );
		return 0;
	}
	res = (struct LFXmtdpart_info*)malloc(sizeof(struct LFXmtdpart_info)*resNumber);
	memcpy( res , resTemp , sizeof(struct LFXmtdpart_info)*resNumber );
	*out_numparts = resNumber;
	return res;
}

struct LFXsubpart_info *parseLfxConfigFile(const char *filename,unsigned int *out_numparts)
{
  struct textFileHandle *fh;
  const char *lin;
  unsigned int lineNo;
  struct LFXsubpart_info resTemp[32];
  unsigned int resNumber;
  struct LFXsubpart_info *res;
	fh = filOpen( filename );
	if( (!fh) && errno==ENOENT )
	{
		// Error is file-not-found. Try to create it?
		createDefaultLfxConfigFile(filename);
		fh = filOpen( filename );
	}
	if(!fh)
	{
		return 0;
	}
	resNumber = 0;

	memset( resTemp , 0xFE , sizeof(resTemp) );	// not needed, but LINT misinterprets the code and nags. Valgrind does not need it. Is rarely called, so does not hurt.
	lineNo = 0;
	// loop lines
	while(1)
	{
	  struct LFXsubpart_info part;
		lin = filRead( fh );
		if(!lin)break;
		lineNo++;
		skipWhitespace(&lin);	// skip leading whitespace
		if( *lin=='#' )continue;		// comment line
		if( *lin==0 || *lin==10 || *lin==13 )continue;
		memset( &part , 0 , sizeof(part) );
		// expect two strings and two integers, with whitespace inbetween. Exit loop on any mismatch.
		if(!parseString( &lin , part.LFXname , sizeof(part.LFXname) ))
			break;
		if(!skipWhitespace(&lin))
			break;
		if(!parseString( &lin , part.DEVnicename , sizeof(part.DEVnicename) ))
			break;
		if(!skipWhitespace(&lin))
			break;
		if(!parseInt( &lin , &part.start ))
			break;
		if(!skipWhitespace(&lin))
			break;
		if(!parseInt( &lin , &part.end ))
			break;
		skipWhitespace(&lin);
		// line must end here or have comment sign. If not, error.
		if( *lin && *lin!='#' && *lin!=10 && *lin!=13 )
			break;
		// line is good.
		if( resNumber >= sizeof(resTemp)/sizeof(struct LFXsubpart_info) )
			break;	// too many items
		resTemp[resNumber++] = part;
	}
	// done. If 'lin' pointer is not null, there was a read error or parse error.
	filClose(fh);
	if( lin != 0 )
	{
		syslog( 3 , "error reading/parsing LFX configfile '%s', line %u. bad line or too many items. Line must have 2 strings and 2 ints." , filename , lineNo );
		return 0;
	}
	res = (struct LFXsubpart_info*)malloc(sizeof(struct LFXsubpart_info)*resNumber+1);		//lint -e826 PQM_authorized_460
	memcpy( res , resTemp , sizeof(struct LFXsubpart_info)*resNumber );
	*out_numparts = resNumber;
	return res;
}

// advance the linPtr over any whitespace.
static unsigned int skipWhitespace( const char **linPtr )
{
  const char *rd = *linPtr;
  unsigned int res=0;
	while( *rd==' ' || *rd=='\t' ){rd++;res++;}
	*linPtr = rd;
	return res;
}

// get a string from a line.
// linPtr is pointer to read-pointer. It will be advanced to the point behind the string.
// out_string is pointer to result. It will receive a null-terminated string.
// outSize is the maximum size of the output. (including the null-byte)
// result is 1 if good or 0 if something was bad.
static char parseString( const char **linPtr , char *out_string , unsigned int outSize )
{
  char quot=0;
  const char *rd = *linPtr;
	if( outSize<2 )return 0;
	skipWhitespace(&rd);
	if( *rd=='\"' || *rd=='\'' )
		{quot = *rd;rd++;}
	while( *rd && *rd!=quot )
	{
		if( (!quot) && ( *rd==' ' || *rd=='\t' ) )
			break;
		if(outSize<2)return 0;
		*out_string = *rd;out_string++;outSize--;
		rd++;
	}
	if(quot)
	{
		if( quot!=*rd )return 0;	// if start with quote, need end-quote.
		rd++;
	}
	while(outSize>0)
		{*out_string=0;out_string++;outSize--;}
	*linPtr = rd;
	return 1;
}

static char parseInt( const char **linPtr , int *out_result )
{
  const char *rd = *linPtr;
  char isNeg;
  unsigned long res;
	skipWhitespace(&rd);
	isNeg=0;
	if( *rd=='-' ){isNeg=1;rd++;}
	skipWhitespace(&rd);
	while( *rd=='0' )rd++;	// skip leading zeros.
	res = 0;
	// is a 'x' indicating Hex-number?
	if( *rd=='x' || *rd=='X' )
	{
	  unsigned int nv;
		rd++;
		if(!parseUIntHex( &rd , &nv ))return 0;
		res = nv;
	}else{
	  unsigned long nv;
		while(*rd)
		{
			if( *rd>='0' && *rd<='9' )
				nv = (res*10)+(*rd-'0');
			else
				break;
			if( (nv/10)<res )return 0;	// overflow
			res = nv;rd++;
		}
	}

	if( res>0x7FFFFFFF )return 0;	// overflow

	if( isNeg )
		*out_result = -(int)res;
	else
		*out_result = (int)res;

	*linPtr = rd;
	return 1;
}

static char parseUIntHex( const char **linPtr , unsigned int *out_result )
{
  const char *rd = *linPtr;
  unsigned long res;
  unsigned long nv;
	skipWhitespace(&rd);
	res = 0;
	while(*rd)
	{
		if( *rd>='0' && *rd<='9' )
			nv = (res<<4)+(*rd-'0');
		else if( *rd>='a' && *rd<='f' )
			nv = (res<<4)+(*rd-('a'-10));
		else if( *rd>='A' && *rd<='F' )
			nv = (res<<4)+(*rd-('A'-10));
		else
			break;
		if( (nv>>4)<res )return 0;	// overflow
		res = nv;rd++;
	}

	if( res>0x7FFFFFFF )return 0;	// overflow

	*out_result = (unsigned int)res;

	*linPtr = rd;
	return 1;
}

static void createDefaultLfxConfigFile(const char *filename)
{
  char *mp;
  int i;
	if( !filename || !filename[0] )return;
	// create path
	mp = (char*)malloc(strlen(filename+2));
	strcpy( mp , filename );
	for( i=1 ; mp[i] ; i++ )
	{
		if( mp[i]=='/' )
		{
		  int rv;
			mp[i]=0;
			rv = mkdir(mp,0x1ED);
			if( rv!=0 && errno!=EEXIST )
				{free(mp);return;}
			mp[i]='/';
		}
	}
	free(mp);
	// now create file
  int fh;
	fh = open(filename,O_WRONLY|O_CREAT|O_EXCL,0x1A4);
	if(fh<0)
		return;	// cannot create.
	// write dummy content
  const char *data =
		"# config file for the LFX subpartitions \n"
		"# \n"
		"# LFX users do not use the mtd names. An mtd partition is \n"
		"# further subdivided into 'LFX partitions'. These are \n"
		"# defined in here.\n"
		"# columns below:\n"
		"# LFX name:  Name to be used by the LFX clients. For example 'errmem'.\n"
		"# mtdname:   friendly-name assigned in the MTD. Can be found be looking at /proc/mtd .\n"
		"# start:     beginning of LFX subpartition, in bytes.\n"
		"# end:       end of LFX subpartition, in bytes\n"
		"# \n"
		"# LFX name    mtdname       start       end\n"
		"\n";
	i=write(fh,data,strlen(data));
	close(fh);
	if(i<=0)
		unlink(filename);
}
