#include "LFX2.h"
#include <unistd.h>
#include <fcntl.h>              // for open/close
#include <sys/ioctl.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include "partConfig.h"
#include "mtdClient.h"


//#define LFX_BIGLIB_NOWRITE


struct LFXinst
{
	LFXpart usr;		// part visible to user

	struct LFXsubpart_info *subParts;
	unsigned int subParts_num;
	struct LFXmtdpart_info *mtdParts;
	unsigned int mtdParts_num;

	struct mtdClient *dev;
};



LFXhandle LFX_open(const char *LFXname)
{
  struct LFXinst *self;
  const char *mtd_nicename;
  const char *mtd_devName;
  unsigned int sp,mp;
  struct mtdClient_init cliData;
  char *scratch_nameBuffer=0;
	self = (struct LFXinst*)malloc(sizeof(*self));
	if(!self)
		{errno=LFX_ERROR_OTHER;return 0;}
	memset( self , 0 , sizeof(*self) );
	// get / parse the LFX config file.
	self->subParts = parseLfxConfigFile( LFX_SUBPART_CONFIG_FILENAME , &(self->subParts_num) );
	if( (!self->subParts) || self->subParts_num<1 )
	{
		printf( "no LFX subpartitions defined in '%s'." , LFX_SUBPART_CONFIG_FILENAME );
		errno=LFX_ERROR_NOT_AVAILABLE;goto fail;	//lint -e801 PQM_authorized_462
	}else{
		//printf( "LFX has %u subpartitions defined." , (unsigned int)self->subParts_num );
	}
	// get / parse /proc/mtd
	self->mtdParts = parseMtdProcFile( MTD_PROC_FILENAME , &(self->mtdParts_num) );
	if( (!self->mtdParts) || self->mtdParts_num<1 )
	{
		printf( "no MTD partitions defined in '%s'." , MTD_PROC_FILENAME );
		errno=LFX_ERROR_NOT_AVAILABLE;goto fail;	//lint -e801 PQM_authorized_462
	}

	// find in list of subparts
	for( sp=0 ; sp<self->subParts_num ; sp++ )
	{
		if(!strcmp(self->subParts[sp].LFXname,LFXname))
			break;
	}
	if( sp >= self->subParts_num )
		{errno=LFX_ERROR_INVALID_PART_NAME;goto fail;}	//lint -e801 PQM_authorized_462
	mtd_nicename = self->subParts[sp].DEVnicename;

	// find in list of MTD partitions
	for( mp=0 ; mp<self->mtdParts_num ; mp++ )
	{
		if(!strcmp( self->mtdParts[mp].DEVnicename , mtd_nicename ))
			break;
	}
	if( mp >= self->mtdParts_num )
		{errno=LFX_ERROR_INVALID_PART_NAME;goto fail;}	//lint -e801 PQM_authorized_462
	mtd_devName = self->mtdParts[mp].DEVmtdname;

	// prepare struct to start mtdClient
	cliData.open = open;
	cliData.close = close;
	cliData.ioctl = ioctl;
	cliData.read = read;
	cliData.write = write;
	cliData.lseek = lseek;

	scratch_nameBuffer = (char*)malloc(1+strlen(mtd_devName));
	cliData.devName = scratch_nameBuffer;

	memcpy( scratch_nameBuffer , mtd_devName , strlen(mtd_devName)+1 );
	cliData.startOffset = self->subParts[sp].start;
	cliData.endOffset = self->subParts[sp].end;


	// now open
	self->dev = mtdClient_init( &cliData );
	if(!self->dev)
	{
		errno = LFX_ERROR_NOT_AVAILABLE;
		goto fail;			//lint -e801 PQM_authorized_462
	}

	self->usr.name = (char*)malloc(strlen(LFXname)+1);
	strcpy( (char*)(self->usr.name) , LFXname );
	self->usr.intID = 1;
	self->usr.blockSize = self->dev->eraseSize;
	self->usr.chunkSize = self->dev->writeSize;
	self->usr.numBlocks = self->dev->numEraseBlocks;

	// can drop the MTD configuration array
	free(self->mtdParts);
	self->mtdParts = 0;
	self->mtdParts_num = 0;

	free(scratch_nameBuffer);

	return (LFXhandle)(void*)self;  // for LINT nagging. was   return &(self->usr);
fail:
	LFX_close((LFXhandle)(void*)self);	// This closes/deallocates the struct.
	if(scratch_nameBuffer)
		free(scratch_nameBuffer);
	return 0;			//lint -e429 PQM_authorized_461
}

void LFX_close( LFXhandle part )
{
  struct LFXinst *self = (struct LFXinst*)part;		//lint -e826 PQM_authorized_459

	if( self->dev )
		mtdClient_uninit( self->dev );
	if( self->subParts )
		free( self->subParts );
	if( self->mtdParts )
		free( self->mtdParts );
	if( self->usr.name )
		free( (void*)(self->usr.name) );


	memset( self , 0xFE , sizeof(*self) );
	free( self );
}

int LFX_read( LFXhandle part , void *buffer , unsigned int size , unsigned int offset )
{
  struct LFXinst *self = (struct LFXinst*)part;
  int ret;
	ret = mtdClient_read( self->dev , offset , size , (char*)buffer );
	if(ret)
		return LFX_ERROR_DEVICE_READ_ERR;
	return 0;
}

int LFX_write( LFXhandle part , const void *buffer , unsigned int size , unsigned int offset )
{
#ifdef LFX_BIGLIB_NOWRITE
	return LFX_ERROR_OTHER;	// LFX big lib should only be used to read. No modifications. Use daemon process and smaller LFX lib for this.
#else
  struct LFXinst *self = (struct LFXinst*)part;
  int ret;
	ret = mtdClient_write( self->dev , offset , size , (const char*)buffer );
	if(ret)
		return LFX_ERROR_DEVICE_WRITE_ERR;
	return 0;
#endif
}

int LFX_erase( LFXhandle part ,unsigned size, unsigned int offset )
{
#ifdef LFX_BIGLIB_NOWRITE
	return LFX_ERROR_OTHER;	// LFX big lib should only be used to read. No modifications. Use daemon process and smaller LFX lib for this.
#else
  struct LFXinst *self = (struct LFXinst*)part;
  int ret;
	ret = mtdClient_erase( self->dev , offset , size );
	if(ret)
		return LFX_ERROR_DEVICE_ERASE_ERR;
	return 0;
#endif
}

int LFX_testFullOperation( LFXhandle part )
{
	part=part;	//LINT don't nag.
	return LFX_ERROR_DEVICE_NOT_READY;
}

const char *LFX_queryNames()
{
  struct LFXsubpart_info *subParts;
  unsigned int subParts_num;
  unsigned int t,num,space;
  char *res,*wr;
	// scan config file (again)
	subParts = parseLfxConfigFile( LFX_SUBPART_CONFIG_FILENAME , &subParts_num );
	if( (!subParts) || subParts_num<1 )
		return 0;

	// count space to allocate
	num=0;space=0;
	for( t=0 ; t<subParts_num ; t++ )
	{
		num++;
		space += strlen(subParts[t].LFXname)+1;
	}
	space++;
	res = (char*)malloc(space);
	if(!res){free(subParts);return res;}
	// copy in
	wr = res;
	for( t=0 ; t<subParts_num ; t++ )
	{
		strcpy( wr , subParts[t].LFXname );
		wr += strlen(subParts[t].LFXname)+1;
	}
	// add array terminator
	*wr = 0;
	// drop parts array
	free(subParts);
	// done.
	return res;
}
