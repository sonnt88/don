#ifndef TEXTFILE_HANDLE_H
#define TEXTFILE_HANDLE_H



struct textFileHandle
{
	int fd;
	char linbuf[256];
	char rdbuf[1024];
	unsigned int bytes;
	char eof;
};


#ifdef __cplusplus
extern "C" {
#endif


struct textFileHandle *filOpen( const char *name );
const char *filRead( struct textFileHandle *h );
int filClose( struct textFileHandle *h );

#ifdef __cplusplus
}
#endif



#endif
