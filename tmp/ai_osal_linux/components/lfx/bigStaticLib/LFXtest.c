#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <string.h>
#include "LFX2.h"

// Small test program to try and demonstrate the LFX component on linux-PC. Not part of the product build.


#define DEV_NAME   "errmem"

int main(int numargs,char * const *args)
{
  const char *errtxt;
  char *buffer;
  const char *names;
  const char *devName;
  LFXhandle hdl;
	hdl = 0;
	buffer = 0;
	names = 0;
	errtxt=0;
	devName = "errmem";

	buffer = (char*)malloc(0x100000);
	if(!buffer)
		{errtxt="malloc fail";goto leave;}

	names = LFX_queryNames();
	if(names)
	{
	  const char *rd;
		printf("names:\n");
		rd=names;
		while(*rd)
		{
			printf("  %s\n",rd);
			if( strcasestr( rd , "mtdram" ) )
				devName = rd;
			if( strcasestr( rd , "errm" ) )
				devName = rd;
			while(*rd)rd++;
			rd++;
		}
	}

	printf("opening LFX device '%s'\n",devName);
	hdl = LFX_open(devName);

	if(!hdl)
	{
		fprintf(stderr,"errno=%d\n",errno);
		errtxt="cannot open LFX\n";
		goto leave;
	}

	printf("opened LFX.\n");
	printf(" blockSize = 0x%08X\n",hdl->blockSize);
	printf(" chunkSize = 0x%08X\n",hdl->chunkSize);
	printf(" numBlocks = 0x%08X\n",hdl->numBlocks);

	if(LFX_read( hdl , buffer , 0x20000 , 0 ))
		{errtxt="MTD read failed.";goto leave;}

	if(LFX_erase( hdl , 0 ))
		{errtxt="MTD erase failed.";goto leave;}

	if(LFX_erase( hdl , hdl->blockSize ))
		{errtxt="MTD erase failed.";goto leave;}

	memcpy( buffer , "....ABCDEFGH...." , 16 );
	if(LFX_write( hdl , buffer , 16 , hdl->blockSize-8 ))
		{errtxt="MTD write failed.";goto leave;}

	if(LFX_read( hdl , buffer , 16 , hdl->blockSize-8 ))
		{errtxt="MTD read failed.";goto leave;}

	if( memcmp( buffer , "....ABCDEFGH...." , 16 ) )
		{errtxt="data error 1";}

	if(LFX_erase( hdl , 0 ))
		{errtxt="MTD erase failed.";goto leave;}

	if(LFX_read( hdl , buffer , 16 , hdl->blockSize-8 ))
		{errtxt="MTD read failed.";goto leave;}

	if( memcmp( buffer , "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF" "EFGH...." , 16 ) )
		{errtxt="data error 2";}



leave:
	// cleanup and exit.
	if(hdl)
		LFX_close(hdl);
	if(errtxt)
		fprintf(stderr,"Error: %s\n",errtxt);
	if(buffer)free(buffer);
	if(names)free((char*)names);
	return (errtxt?5:0);
}
