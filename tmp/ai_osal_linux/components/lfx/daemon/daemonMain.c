#include "daemonMain.h"
#include <unistd.h>		// for setsid, getpid
#include <fcntl.h>		// for open/close
#include <sys/ioctl.h>
#include <string.h>
#include <sys/msg.h>
#include <malloc.h>
#include <syslog.h>
#include <errno.h>
#include "LFXdaemon.h"		// message-queue interface definition.
#include "mtdClient.h"
#include "partConfig.h"

// LFX daemon
// 
// The LFX daemon is a daemon to allow non-root usermode programs to access the MTD devices.
// When the MTD is not yet available (flash still starting up), it can allow read-only access 
// by bypassing the MTD (NOR can be accessed directly).
// It also maps the device names so the user program accesses the partitions using the descriptive names.
// 
// It is an extra layer over the MTD to allow serializing accesses and to have a point to add an 
// additional ECC layer later.
// 



static char _isProtectedName(const char *name);
static int _sendNack( struct daemon *self , int feedback_mq , const char *errtxt );
#ifdef LOG_COMMANDS
static int dbg_msgsnd( int q , const void *b , size_t s , int f );
#endif

// protected names. These will not be accessible. compared against using strcasestr().
static const char *protectedNames[] =
{
	"u-boot",
	"kernel",
	"device tree",
	0
};


// Daemon main function.
// This function is called right after daemonizing the process.
// (that is, after the double-fork magic)
// Note that it is not possible to debug using printf().
// The standard daemonizing method includes closing the three standard 
// file handles stdin, stdout and stderr. Use syslog().
// The parameter is the handle to the creator's message queue.
// The daemon will send a reply to here to indicate success or 
// failure of the daemon startup.
int daemon_main(int feedback_mq,char bIsDebugMode)
{
  struct daemon self;
  char buffer[4096+16+16];
  unsigned int cn;
	// here we are.
	// set up self-struct.
	init_daemon( &self );

	setsid();

	// set up syslog. Note, that there is not stderr or stdout available here.
	openlog( "LFXdaemon" , 0 , LOG_DAEMON );

	sprintf(buffer,"starting (pid=%d)",getpid());
	syslog( 6 , "starting (pid=%u)" , (unsigned int)getpid() );

	memset( buffer , 0xFE , sizeof(buffer) );			// valgrind likes to see this. pretty pointless though. remove?

	// scan MTD devices by looking into /proc/mtd
	self.mtdParts = parseMtdProcFile( MTD_PROC_FILENAME , &(self.mtdParts_num) );
	if(!self.mtdParts)
	{
		uninit_daemon( &self );
		return _sendNack( &self , feedback_mq , "cannot find available MTD devices" );
	}

	if( self.mtdParts_num < 1 )
	{
		uninit_daemon( &self );
		return _sendNack( &self , feedback_mq , "no MTD devices/partitions found in /proc/mtd" );
	}

	// try to access the last device (basically, test if we are root).
	{
	  struct mtdClient *_try;
	  struct mtdClient_init initData;
	  const struct LFXmtdpart_info *pi;
		pi = self.mtdParts + (self.mtdParts_num-1);
		memcpy( &initData , &self.cli_proto , sizeof(initData) );
		initData.startOffset = 0;
		initData.endOffset = pi->size;
		initData.devName = pi->DEVmtdname;
		_try = mtdClient_init( &initData );
		if(_try)
			mtdClient_uninit(_try);	// is ok.
		else
		{
			uninit_daemon( &self );
			return _sendNack( &self , feedback_mq , "cannot open an MTD device (not root?)" );
		}
	}

	// get list of subpartitions
	self.subParts = parseLfxConfigFile( LFX_SUBPART_CONFIG_FILENAME , &(self.subParts_num) );
	if( (!self.subParts) || self.subParts_num<1 )
	{
		syslog( 2 , "no LFX subpartitions defined in '%s'." , LFX_SUBPART_CONFIG_FILENAME );
	}else{
		syslog( 6 , "LFX has %u subpartitions defined." , (unsigned int)self.subParts_num );
	}

	// create our message queue
	self.dm_mq_h = self.msgget( LFXDAEMON_MSG_Q , IPC_CREAT|IPC_EXCL|0x1B6 );
	if( self.dm_mq_h<0 )
	{
		syslog( 2 , "Create Q failed. retval=%d. Exiting." , (int)self.dm_mq_h );
		uninit_daemon( &self );
		return _sendNack( &self , feedback_mq , "cannot create main message queue" );
	}

//#ifdef LOG_COMMANDS
//	msgctl( self.dm_mq_h , IPC_INFO , (struct msqid_ds*)buffer );
//	syslog(7,"msgmap = 0x%08X\n",((struct msginfo*)buffer)->msgmap);
//	syslog(7,"msgmax = 0x%08X\n",((struct msginfo*)buffer)->msgmax);
//	syslog(7,"msgmnb = 0x%08X\n",((struct msginfo*)buffer)->msgmnb);
//#endif
	// all done.

	// log devices which we found to syslog.
	for( cn=0 ; cn<self.mtdParts_num ; cn++ )
		syslog( 7 , "MTD device/partition '%s' \"%s\"" , self.mtdParts[cn].DEVmtdname , self.mtdParts[cn].DEVnicename );

	// send positive ack

	{
	  struct LFXmsg_b *msg = (struct LFXmsg_b*)buffer;
		msg->cmd = REP_STARTUP;
		msg->byt = 0;
		self.msgsnd( feedback_mq , buffer , sizeof(*msg) , 0 );
	}
	feedback_mq = -1;	// no longer need the queue.

	// main loop here.
	self.loopCount = 0;
#ifdef LOG_COMMANDS
	syslog( 7 , "begin loop" );
#endif
	while(1)
	{
	  int l;
		self.loopCount++;

		// block and wait for a message here.
		l = self.msgrcv( self.dm_mq_h , buffer , sizeof(buffer)-1 , 0 , 0 );

		if( l<0 )
		{
			if(errno==EINTR)continue;	// signal. ignore. Just keep waiting.
			// if not signal, this is bad. Die.
			syslog( 2 , "error in mq_receive (errno=%d)" , (int)errno );break;
		}
		buffer[l]=0;

		if( process_msg( &self , buffer , l , sizeof(buffer) ) )
			break;

	}

	// leaving the loop. send signal to all clients.
	syslog( 7 , "stopping (pid=%u)" , (unsigned int)getpid() );

	msgctl( self.dm_mq_h , IPC_RMID , 0 );
	self.dm_mq_h = -1;

	closelog();	// close logfile here. If restarting, daemon might restart right after sending the stop-ack.

	for( cn=0 ; cn<MAX_CLIENTS ; cn++ )
	{
		if( self.clients[cn].dev )
		{
			mtdClient_uninit( self.clients[cn].dev );
			self.clients[cn].dev = 0;
		}
		if( self.clients[cn].cl_mq_h>=0 )
		{
		  struct LFXmsg_none *msg = (struct LFXmsg_none*)buffer;
			msg->cmd = REP_STOPPING;
			self.msgsnd( self.clients[cn].cl_mq_h , buffer , sizeof(*msg) , 0 );
		}
	}

	uninit_daemon(&self);

	return 0;
}


void init_daemon(struct daemon *self)
{
  unsigned int cn;
	memset( self , 0 , sizeof(*self) );
	self->dm_mq_h = -1;
	for( cn=0 ; cn<MAX_CLIENTS ; cn++ )
	{
		self->clients[cn].cl_mq_h = -1;
		self->clients[cn].dev = 0;
	}
	self->mtdParts = 0;
	self->mtdParts_num = 0;
	self->subParts = 0;
	self->subParts_num = 0;
	self->msgget = msgget;
#ifndef LOG_COMMANDS
	self->msgsnd = msgsnd;
#else
	self->msgsnd = dbg_msgsnd;
#endif
	self->msgrcv = msgrcv;
	self->cli_proto.open = open;
	self->cli_proto.close = close;
	self->cli_proto.ioctl = ioctl;
	self->cli_proto.read = read;
	self->cli_proto.write = write;
	self->cli_proto.lseek = lseek;
}

void uninit_daemon(struct daemon *self)
{
  unsigned int cn;
	if(!self)return;
	for( cn=0 ; cn<MAX_CLIENTS ; cn++ )
	{
		if( self->clients[cn].dev )
			mtdClient_uninit( self->clients[cn].dev );
	}
	if( self->mtdParts )
		free( self->mtdParts );
	if( self->subParts )
		free( self->subParts );
	if( self->dm_mq_h>=0 )
		msgctl( self->dm_mq_h , IPC_RMID , 0 );
  type_msgget k1 = self->msgget;
  type_msgsnd k2 = self->msgsnd;
  type_msgrcv k3 = self->msgrcv;
	memset( self , 0xFE , sizeof(*self) );
	self->msgget = k1;
	self->msgsnd = k2;
	self->msgrcv = k3;
}

// Process any message.
// Handles any results-sending and changes to client and daemon structs.
// returns 0 to indicate that message-loop should be continued. 1 to indicate exit.
char process_msg(struct daemon *self,char *msgBuffer,unsigned int msgLen,unsigned int bufferSize)
{
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)msgBuffer;
  struct client *cli;
  unsigned int cn;
	if( msgLen<sizeof(msg_b->cmd) )
		return 0;	// not even ID in message?

#ifdef LOG_COMMANDS
	if( msgLen>sizeof(msg_b->cmd) )
		syslog( 7 , "cmd: %d %d" , (int)msg_b->cmd , (int)msg_b->byt );
	else
		syslog( 7 , "cmd: %d" , (int)msg_b->cmd );
#endif

	if( msg_b->cmd == CMD_NONE )return 0;		// idle. Do nothing.
	if( msg_b->cmd == CMD_STOP )return 1;			// exit main loop.
	if( msg_b->cmd == CMD_CONNECT )
	{
		if( msgLen>=sizeof(struct LFXmsg_b_i) )
			process_connect( self , msgBuffer );
		return 0;
	}
	// all other packet types have a handle in msg_b->byt.
	cn = (unsigned char)msg_b->byt;
	if( msgLen<sizeof(*msg_b) || cn<0 || cn>=MAX_CLIENTS )
		return 0;	// bad packet. Packet too small or invalid client number
	cli = self->clients + cn ;	// get client struct
	if( cli->cl_mq_h<0 )
		return 0;	// bad client number. (This slot is not opened)
	cli->lastTouch = self->loopCount;
	// switch command
	switch( msg_b->cmd )
	{
	case CMD_OPENDEV:
		process_openDev( self , msgBuffer , cli );
		break;
	case CMD_DISCONNECT:
		// client requests to disconnect from daemon.
		process_disconnect( self , msgBuffer , cli );
		break;
	case CMD_INFO:
		// client requests device info (erasesize, total size, writesize)
		process_info( self , msgBuffer , cli );
		break;
	case CMD_ERASE:
		// command to erase one block
		process_erase( self , msgBuffer , msgLen , cli );
		break;
	case CMD_WRITE:
		// command to write data
		process_write( self , msgBuffer , msgLen , cli );
		break;
	case CMD_READ:
		// read request.
		process_read( self , msgBuffer , msgLen , cli );
		break;
	case CMD_LIST_DEVS:
		process_listDevs( self , msgBuffer , bufferSize , cli );
		break;
	default:
		syslog( 3 , "unknown message 0x%02X (len=%u)" , (unsigned int)(msg_b->cmd) , msgLen );
		//return 1;
	}
	return 0;
}

void process_connect(struct daemon *self,char *msgBuffer)
{
  int mq_r;
  unsigned int cn;
  unsigned int oldest,oldest_cn;
  struct client *cli;
  unsigned int cli_Q_num;
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)msgBuffer;
  struct LFXmsg_b_i *msg = (struct LFXmsg_b_i*)msgBuffer;
	// open client's mq
	cli_Q_num = msg->para;
	if( msg->byt != 0x2A )
		{syslog( 3 , "bad connect packet. Some other service trying to connect with a diffrent protocol?" );return;}
	// open the client's reply queue using the ID in the request packet.
	mq_r = self->msgget( cli_Q_num , 0 );
	if( mq_r<0 )
		{syslog( 3 , "error opening client's message queue 0x%08X." , cli_Q_num );return;}
	// find a slot for this request.
	oldest_cn=0;oldest=0;
	for( cn=0 ; cn<MAX_CLIENTS ; cn++ )
	{
	  unsigned int since;
		cli = self->clients + cn;
		if( cli->cl_mq_h < 0 )
			break;
		since = self->loopCount-cli->lastTouch;
		if( since>oldest )
			{oldest_cn = cn;oldest=since;}
	}
	// if we didn't find any free slot, take the one which was not used the longest. probably dead...(oohhh)
	if( cn>=MAX_CLIENTS && oldest>0 )
	{
		syslog( 4 , "Warning: dropping slot #%u to accept a new connection." ,(unsigned int)oldest_cn );
		cn = oldest_cn;
		cli = self->clients + cn;
		// stealing oldest slot.
		if( cli->dev )
			mtdClient_uninit( cli->dev );
		cli->dev = 0;
		msg_b->cmd = REP_DISCONNECT;
		msg_b->byt = 0;
		self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
		cli->cl_mq_h = -1;
	}

	if( cn>=MAX_CLIENTS )
	{
		// No slot found. reject.
		msg_b->cmd = REP_CONNECT;
		msg_b->byt = -1;
		self->msgsnd( mq_r , msgBuffer , sizeof(*msg_b) , 0 );
		return;
	}
	// is good.
	cli = self->clients + cn;
	cli->cl_mq_h = mq_r;
	cli->lastTouch = self->loopCount;
	{
	  struct LFXmsg_bb *msg = (struct LFXmsg_bb*)msgBuffer;
		msg->cmd = REP_CONNECT;
		msg->byt1 = 0;
		msg->byt2 = (char)cn;
		self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg) , 0 );
	}
}

void process_disconnect(struct daemon *self,char *msgBuffer,struct client *cli)
{
  struct LFXmsg_none *msg = (struct LFXmsg_none*)msgBuffer;
	// client requests to disconnect from daemon.
	if( cli->dev )
	{
		// close MTD.
		mtdClient_uninit( cli->dev );
		cli->dev=0;
	}
	msg->cmd = REP_DISCONNECT;
	self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg) , 0 );
	cli->cl_mq_h = -1;
}

void process_openDev(struct daemon *self,char *msgBuffer,struct client *cli)
{
  const char *devName;
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)msgBuffer;
  struct mtdClient_init initData;
	// get name requested
	devName = msgBuffer+sizeof(*msg_b);

	// we want LFX subpart names, not dev names. find and change pointer
	if(find_client_init_data( self , devName , &initData , msgBuffer+0x0800 ))
	{
		// failed to resolve name.
		syslog( 2 , "LFX cannot find requested subpart '%s'." , devName );
		msg_b->cmd = REP_OPENDEV;
		msg_b->byt = -1;
		self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
		return;
	}

	// try to open the MTD device. initData struct was filled in by find_client_init_data.
	cli->dev = mtdClient_init( &initData );
	if( ! cli->dev )
	{
		// open fail.
		syslog( 2 , "error opening MTD device '%s'" , initData.devName );
		msg_b->cmd = REP_OPENDEV;
		msg_b->byt = -1;
		self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
		return;
	}
	// good.
	msg_b->cmd = REP_OPENDEV;
	msg_b->byt = 0;
	self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
}

void process_info(struct daemon *self,char *msgBuffer,struct client *cli)
{
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)msgBuffer;
  struct LFXmsg_b_iii *msg;
	// client requests device info (erasesize, total size, writesize)
	if( !cli->dev )
	{
		msg_b->cmd = REP_INFO;
		msg_b->byt = -1;
		self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
		return;
	}
	msg = (struct LFXmsg_b_iii*)msgBuffer;
	msg->cmd = REP_INFO;
	msg->byt = 0;
	msg->para1 = cli->dev->eraseSize;
	msg->para2 = cli->dev->writeSize;
	msg->para3 = cli->dev->numEraseBlocks;
	// syslog( 7 , "erS=0x%08X wrS=0x%08X" , self.clients[cn].dev->eraseSize , self.clients[cn].dev->writeSize );
	self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg) , 0 );
}

void process_listDevs(struct daemon *self,char *msgBuffer,unsigned int bufferSize,struct client *cli)
{
  unsigned int j,l;
  struct LFXmsg_none *msg = (struct LFXmsg_none*)msgBuffer;
	msg->cmd = REP_LIST_DEVS;
	l=sizeof(*msg);

	for( j=0 ; j<self->subParts_num && (unsigned int)l+2<bufferSize ; j++ )
	{
	  unsigned int ln;
		ln = strlen( self->subParts[j].LFXname );
		if( l+ln+2 < bufferSize )
		{
			memcpy( msgBuffer+l , self->subParts[j].LFXname , ln );l+=ln;
			msgBuffer[l++]=0;
		}
	}


/*
	for( j=0 ; j<self->mtdParts_num && (unsigned int)l+2<bufferSize ; j++ )
	{
	  unsigned int ln;
		ln = strlen( self->mtdParts[j].DEVnicename );
		if( l+ln+2 < bufferSize )
		{
			memcpy( msgBuffer+l , self->mtdParts[j].DEVnicename , ln );l+=ln;
			msgBuffer[l++]=0;
		}
	}
*/
	msgBuffer[l++]=0;
	self->msgsnd( cli->cl_mq_h , msgBuffer , l , 0 );
}

void process_read(struct daemon *self,char *msgBuffer,unsigned int msgLen,struct client *cli)
{
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)msgBuffer;
  unsigned int pos0,length;
  int rv;
	// read request.
	{
	  struct LFXmsg_b_ii *msg = (struct LFXmsg_b_ii*)msgBuffer;
		pos0   = msg->para1;
		length = msg->para2;
	}
#ifdef LOG_COMMANDS
			syslog( 2 , "have read cmd  pos=%u len=%u" , pos0 , length );
#endif
	if( msgLen<sizeof(struct LFXmsg_b_ii) || (!cli->dev) || length>4096 )
	{
		msg_b->cmd = REP_READ;
		msg_b->byt = -1;
		self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
		return;
	}
	rv = mtdClient_read( cli->dev , pos0 , length , msgBuffer+sizeof(*msg_b) );
	msg_b->cmd = REP_READ;
	msg_b->byt = (char)( (rv&0xFFFFFF00)==0 ? rv : -1 );
	msgLen = sizeof(*msg_b);
	msgLen += (rv?0:length);
	rv = self->msgsnd( cli->cl_mq_h , msgBuffer , msgLen , 0 );
}

void process_write(struct daemon *self,char *msgBuffer,unsigned int msgLen,struct client *cli)
{
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)msgBuffer;
  unsigned int pos0;
  int rv;
	// command to write data
	// check size and handle
	if( msgLen<sizeof(struct LFXmsg_b_i) || (!cli->dev) )
	{
		msg_b->cmd = REP_WRITE;
		msg_b->byt = -1;
		self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
		return;
	}
	// Get parameters and do write command.
	{
	  struct LFXmsg_b_i *msg = (struct LFXmsg_b_i*)msgBuffer;
		pos0 = msg->para;
		rv = mtdClient_write( cli->dev , pos0 , msgLen-sizeof(*msg) , msgBuffer+sizeof(*msg) );
	}
	// send reply.
	msg_b->cmd = REP_WRITE;
	msg_b->byt = (char)( (rv&0xFFFFFF00)==0 ? rv : -1 );
	self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
}

void process_erase(struct daemon *self,char *msgBuffer,unsigned int msgLen,struct client *cli)
{
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)msgBuffer;
  unsigned int pos0,length;
  int rv;
	// command to erase one block
  	// check msg length and handle
	if( msgLen<sizeof(struct LFXmsg_b_ii) || (!cli->dev) )
	{
		msg_b->cmd = REP_ERASE;
		msg_b->byt = -1;
		self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
		return;
	}
	// Get parameters and do erase command.
	{
	  struct LFXmsg_b_ii *msg = (struct LFXmsg_b_ii*)msgBuffer;
		pos0   = msg->para1;
		length = msg->para2;
		rv = mtdClient_erase( cli->dev , pos0 , length );
	}
	// send reply.
	msg_b->cmd = REP_ERASE;
	msg_b->byt = (char)( (rv&0xFFFFFF00)==0 ? rv : -1 );
	self->msgsnd( cli->cl_mq_h , msgBuffer , sizeof(*msg_b) , 0 );
}

int find_client_init_data(struct daemon *self,const char *LFXname,struct mtdClient_init *out_data,char *scratch_nameBuffer)
{
  unsigned int sp,mp;
  const char *mtd_nicename;
  const char *mtd_devName;
	if( _isProtectedName(LFXname) )
	{
		syslog(5,"not allowing access to protected LFX partition '%s'",LFXname);
		return -1;
	}
	// find in list of subparts
	for( sp=0 ; sp<self->subParts_num ; sp++ )
	{
		if(!strcmp(self->subParts[sp].LFXname,LFXname))
			break;
	}
	if( sp >= self->subParts_num )return -2;
	mtd_nicename = self->subParts[sp].DEVnicename;

	for( mp=0 ; mp<self->mtdParts_num ; mp++ )
	{
		if(!strcmp( self->mtdParts[mp].DEVnicename , mtd_nicename ))
			break;
	}
	if( mp >= self->mtdParts_num )return -3;
	mtd_devName = self->mtdParts[mp].DEVmtdname;

	*out_data = self->cli_proto;
	if(scratch_nameBuffer==0)
		scratch_nameBuffer = (char*)malloc(1+strlen(mtd_devName));
	out_data->devName = scratch_nameBuffer;

	memcpy( scratch_nameBuffer , mtd_devName , strlen(mtd_devName)+1 );
	out_data->startOffset = self->subParts[sp].start;
	out_data->endOffset = self->subParts[sp].end;

	return 0;
}






static char _isProtectedName(const char *name)
{
  unsigned int i;
	for( i=0 ; protectedNames[i] ; i++ )
	{
		if( 0==strcasecmp( name , protectedNames[i] ) )
			return 1;	// is a protected name
	}
	return 0;
}

static int _sendNack( struct daemon *self , int feedback_mq , const char *errtxt )
{
  int ln = strlen(errtxt);
  char *buf = (char*)malloc(9+ln+1);
  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)buf;
	syslog( 3 , "%s" , errtxt );
	msg_b->cmd = REP_STARTUP;
	msg_b->byt = -1;
	memcpy( buf+sizeof(*msg_b) , errtxt , ln );
	buf[sizeof(*msg_b)+ln]=0;
	self->msgsnd( feedback_mq , buf , sizeof(*msg_b)+ln+1 , 0 );
	free(buf);
	return 1;
}


#ifdef LOG_COMMANDS
static int dbg_msgsnd( int q , const void *b , size_t s , int f )
{
  const struct LFXmsg_none *msg1 = (const struct LFXmsg_none*)b;
  const struct LFXmsg_b *msg2 = (const struct LFXmsg_b*)b;
	if( s>=sizeof(*msg2) )
		syslog( 7 , "rep: %d %d" , (int)msg2->cmd , (int)msg2->byt );
	else
		syslog( 7 , "rep: %d" , (int)msg1->cmd );
	return msgsnd( q , b , s , f );
}
#endif
