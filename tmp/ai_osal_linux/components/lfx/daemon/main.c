#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "LFXdaemon.h"




struct state
{
	int fb_mq_h;		// startup MQ handle
	int dm_mq_h;		// daemon MQ handle
	unsigned int fb_mq_num;	// startup MQ name
};


int daemon_main(int feedback_mq,char bIsDebugMode);

enum opMode
{
	op_none = 0,
	op_start = 1,
	op_stop = 2,
	op_restart = 3
};

static int msgrcv_w_timeout( int msqid , void *msgp , size_t msgsz , long msgtyp , int msgflg );

int main( int numargs , char * const *args )
{
  const char *errtxt;
  struct state self;
  int i;
  enum opMode mode;
  const char *mainArg;
  char debugMode;

	errtxt=0;
	memset( &self , 0 , sizeof(self) );
	self.fb_mq_h = -1;
	self.dm_mq_h = -1;

	mainArg=0;
	debugMode=0;
	for( i=1 ; i<numargs ; i++ )
	{
		if( args[i][0]=='-' )
		{
			// is option.
			if( !strcmp( args[i]+1 , "d" ) )
				debugMode=1;
			else
				{errtxt="unknown option.";goto leave;}
		}else{
			if(mainArg)
				{errtxt="need only one argument (plus options).";goto leave;}
			mainArg = args[i];
		}
	}


	// check argument.
	mode=op_none;
	if(!mainArg)mainArg="";

	if( !strcasecmp( mainArg , "start" ) )
		mode = op_start;
	else if( !strcasecmp( mainArg , "stop" ) )
		mode = op_stop;
	else if( !strcasecmp( mainArg , "restart" ) )
		mode = op_restart;
	else
		{errtxt="command must be one of start, stop or restart";goto leave;}

	// check if already running. Looking for the specially named message-queue
	self.dm_mq_h = msgget( LFXDAEMON_MSG_Q , 0 );		// if this exists, the daemon is already running. (check not mutex-locked)

	// make feedback-messagequeue (this will be inherited to child process)
	// for-loop to try some IDs until finding one which is free.
	for( i=0x12345 ; i<0x23456 ; i++ )
	{
		self.fb_mq_h = msgget( i , IPC_CREAT|IPC_EXCL|0x1B6 );
		if( self.fb_mq_h>=0 )
			break;	// have queue
		if( errno!=EEXIST )
			{errtxt="error creating message queue.";goto leave;}	// cound not create, and reason is not that ID already exists.
	}
	if( self.fb_mq_h<0 )
		{errtxt="error creating message queue (all IDs in use\?\?\?).";goto leave;}	// cound not create, and reason is not that ID already exists.
	self.fb_mq_num = i;

	if( mode==op_none )
		{errtxt=0;goto leave;}

	if( mode==op_stop || mode==op_restart )
	{
	  char buffer[288];
	  struct LFXmsg_b_i *msg = (struct LFXmsg_b_i*)buffer;
	  struct LFXmsg_b *msg_b = (struct LFXmsg_b*)buffer;
	  struct LFXmsg_none *msg_0 = (struct LFXmsg_none*)buffer;
	  int l;
		memset( buffer , 0xFE , sizeof(buffer) );		// valgrind wants to see this.
		// Stop selected.
		if( self.dm_mq_h < 0 )
			{errtxt="Cannot stop daemon. It does not seem to be running.";goto leave;}
		// connect to daemon.
		msg->cmd = CMD_CONNECT;
		msg->byt = 0x2A;
		msg->para = self.fb_mq_num;
		msgsnd( self.dm_mq_h , buffer , sizeof(*msg) , 0 );
		// wait for connect
		l = msgrcv_w_timeout( self.fb_mq_h , buffer , sizeof(buffer) , 0 , 0 );
		if( l<0 )
		{
			if( errno == EINTR )
			{
				printf("Warning: timeout connecting to daemon. crashed?\n");
				msgctl( self.dm_mq_h , IPC_RMID , 0 );
				errtxt="Q closed.";
				goto leave;
			}
			printf("errno = %d\n",(int)errno);
			{errtxt="mq_receive failed.";goto leave;}
		}
		if( (unsigned int)l<sizeof(*msg_b) || msg_b->cmd!=REP_CONNECT )
			{errtxt="unexpected result from CMD_CONNECT to daemon process. Cannot send stop command.";goto leave;}
		if( msg_b->byt!=0 )
			{errtxt="error connecting to daemon. Cannot send stop command.";goto leave;}
		// send stop
		msg_0->cmd = CMD_STOP;
		msgsnd( self.dm_mq_h , buffer , sizeof(*msg_0) , 0 );
		self.dm_mq_h = -1;
		// wait for result.
		l = msgrcv_w_timeout( self.fb_mq_h , buffer , sizeof(buffer) , 0 , 0 );
		if( (unsigned int)l<sizeof(*msg_0) || msg_0->cmd!=REP_STOPPING )
			{errtxt="unexpected result from CMD_STOP to daemon process. Cannot send stop command.";goto leave;}
		// ok done. daemon is gone.
		if( mode!=op_restart )
			goto leave;
		// if restart, fall through to start.
	}

	// have start or restart command.
	if( self.dm_mq_h >= 0 )
		{errtxt="Cannot start. daemon seems to be already running. Its message queue exists.";goto leave;}
	// fork process
	i = fork();
	if( i<0 )
		{errtxt="Error creating new process with fork()";goto leave;}

	if( i==0 )
	{
		// this is first child.
		if(!debugMode)
		{
			// daemonize. Close handles, fork again, call umaks and setsid.
			// close handles, then fork again.
			chdir("/");
			close(STDIN_FILENO);
			close(STDOUT_FILENO);
			close(STDERR_FILENO);
			setsid();
			umask(0);
			// now fork again
			if( fork() )
			{
				// is first child. Just drop.
				return 0;
			}
			// is second child
			umask(0);
		}
		// with the unlinked process, run the daemin main-loop.
		return daemon_main(self.fb_mq_h,debugMode);	// pass over control. Also for cleanup.
	}

	// Child is branched off. This is main.
	// wait for result in fb_mq
	{
	  char buffer[288];
	  int l;
	  struct LFXmsg_b *msg = (struct LFXmsg_b*)buffer;
		// first message after creating the daemon must be the status feedback.
		l = msgrcv( self.fb_mq_h , buffer , sizeof(buffer)-1 , 0 , 0 );
		if( l<0 )
			{errtxt="error in msgrcv. cannot get start ack.";goto leave;}
		if( (unsigned int)l<sizeof(*msg) || msg->cmd!=REP_STARTUP )
			{errtxt="unexpected answer from daemon process. Start possibly failed.";goto leave;}
		// check result.
		if( msg->byt != 0 )
		{
			if((unsigned int)l>sizeof(*msg))
			{
				// have error message from daemon process
				buffer[l] = 0;
				fprintf(stderr,"daemon start fail: %s\n",buffer+sizeof(*msg));
			}
			errtxt="daemon process failed to start up.";goto leave;
		}
		// is ok.
		errtxt=0;
	}


leave:
	if(self.fb_mq_h)
		msgctl( self.fb_mq_h , IPC_RMID , 0 );
	if(errtxt)
		fprintf(stderr,"Error: %s\n",errtxt);
	return (errtxt?1:0);
}

static void dummySigFunc(int sig)
{}

typedef void (*sigHdlFptr)(int);


static int msgrcv_w_timeout( int msqid , void *msgp , size_t msgsz , long msgtyp , int msgflg )
{
  sigHdlFptr orgh;
  int rv;
	orgh = signal(SIGALRM,dummySigFunc);
	alarm(6);
	rv = msgrcv( msqid , msgp , msgsz , msgtyp , msgflg );
	alarm(0);
	signal(SIGALRM,orgh);
	return rv;
}



