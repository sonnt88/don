#include "test.h"
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "textFileHandle.h"


static int setup();
static int run1();
static int teardown();

#define TMP_FILENAME "/tmp/test_textFileHandle.tmp"

static struct testdef tst1 = { "textFileHandle1" , setup , run1 , teardown };
static int dummy1 = defineTest( &tst1 );


static int fh=-1;
static struct textFileHandle *th=0;

static int setup()
{
	if(fh>=0)return -1;
	fh = open( TMP_FILENAME , O_CREAT|O_TRUNC|O_WRONLY , 0x01B6 );
	if(fh<0)return -1;
	return 0;
}

static int run1()
{
  const char *lin;
  int dummy=0;
	dummy += write( fh , "line1\n" , 6 );
	dummy += write( fh , "line2 with some appended words\r\n" , 32 );
	dummy += write( fh , " line3\n" , 7 );
	dummy += write( fh , "\n" , 1 );
	dummy += write( fh , "line5\n" , 6 );
	close(fh);
	if(dummy<=0)return -1;
	th = filOpen( "/tmp/thisFileShouldNotExist.txt" );
	if(th!=0)
		{filClose(th);return -1;}
	th = filOpen( TMP_FILENAME );
	if(!th)return -2;
	lin = filRead( th );
	if( (!lin) || strcmp( lin , "line1" ) )return -3;
	lin = filRead( th );
	if( (!lin) || strcmp( lin , "line2 with some appended words" ) )return -4;
	lin = filRead( th );
	if( (!lin) || strcmp( lin , " line3" ) )return -5;
	lin = filRead( th );
	if( (!lin) || strcmp( lin , "" ) )return -6;
	lin = filRead( th );
	if( (!lin) || strcmp( lin , "line5" ) )return -7;
	lin = filRead( th );
	if( lin )return -8;
	filClose(th);th=0;
	return 0;
}

static int teardown()
{
	if(th)
		filClose(th);
	if(fh>=0)
		close(fh);
	fh=-1;
	unlink(TMP_FILENAME);
	return 0;
}

