#ifndef LFXDAEMON__LFXDAEMON_H
#define LFXDAEMON__LFXDAEMON_H


#define LFXDAEMON_MSG_Q 0x1FFDAE9F		// magic ID of SystemV messageQ. Hopefully, it is free.

#define LFXDAEMON_SUBPART_NAMES_FILE "./LFXparts"
// format of file: lines, '#' is comment, each line has 4 part:  MTDname  ,  LFXname  ,  start  ,  end
// negative values indicate relative-to-end, a zero as end means up-to-end (there is no -0)

// each message consists of a LFXmsg struct type as listed below, then the parameters listed.

// commands to daemon
#define CMD_NONE       0		// LFXmsg_none
#define CMD_CONNECT    1		// LFXmsg_b_i     0x2A, mq for replies
#define CMD_OPENDEV    2		// LFXmsg_b       handle  +  string name partition to access.
#define CMD_DISCONNECT 3		// LFXmsg_b       handle
#define CMD_INFO       4		// LFXmsg_b       handle
#define CMD_ERASE      5		// LFXmsg_b_ii    handle, start, offset
#define CMD_WRITE      6		// LFXmsg_b_i     handle, start  +  data
#define CMD_READ       7		// LFXmsg_b_ii    handle, start, length
#define CMD_STOP       8		// LFXmsg_none
#define CMD_LIST_DEVS  9		// LFXmsg_b       handle


// replys from daemon
#define REP_STARTUP    0x10		// LFXmsg_b       errflag  +  [errorstring]
#define REP_CONNECT    0x11		// LFXmsg_bb      errflag, handle
#define REP_OPENDEV    0x12		// LFXmsg_b       errflag
#define REP_DISCONNECT 0x13		// LFXmsg_none
#define REP_INFO       0x14		// LFXmsg_b_iii   errflag ,erasesize, writesize, eraseBlocks
#define REP_ERASE      0x15		// LFXmsg_b       errflag
#define REP_WRITE      0x16		// LFXmsg_b       errflag
#define REP_READ       0x17		// LFXmsg_b       errflag  +  data
#define REP_STOPPING   0x18		// LFXmsg_none  (this notice arrives when the daemon shuts down)
#define REP_LIST_DEVS  0x19		// LFXmsg_none  +  strings, 0-seperated, double 0 at end.



// structs used to carry data for messages above.

struct LFXmsg_none
{
	long cmd;
};
struct LFXmsg_b
{
	long cmd;
	char byt;
};
struct LFXmsg_bb
{
	long cmd;
	char byt1,byt2;
};
struct LFXmsg_b_i
{
	long cmd;
	char byt;
	int para;
};
struct LFXmsg_b_ii
{
	long cmd;
	char byt;
	int para1,para2;
};
struct LFXmsg_b_iii
{
	long cmd;
	char byt;
	int para1,para2,para3;
};



#endif
