#include "test.h"
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <malloc.h>
#include <stdarg.h>
#include <sys/ioctl.h>    // for ioctl command.
#include <mtd/mtd-abi.h>  // for MTD ioctl commands and structs.
#include "partConfig.h"
#include "daemonMain.h"
#include "mtdClient.h"
#include "LFXdaemon.h"


static int setup();
static int run_init();
static int run_findCli();
static int run_open();
static int run_write();
static int run_read();
static int run_writefail();
static int teardown();
static int mock_msgget(key_t key,int msgflg);
static int mock_msgsnd(int msqid,const void *msgp,size_t msgsz,int msgflg);
static ssize_t mock_msgrcv(int msqid,void *msgp,size_t msgsz,long msgtyp,int msgflg);
static int mock_open( const char *name , int flags , ... );
static int mock_close( int fh );
static int mock_ioctl(int fh,long unsigned int cod,...);
static ssize_t mock_read(int fh,void *buffer,size_t);
static ssize_t mock_write(int fh,const void *buffer,size_t);
static off_t mock_lseek(int fh,off_t offset,int whence);

#define MOCK_DMN_Q_ID  4567
#define MOCK_REPL_Q_ID  4568
#define MOCK_DMN_Q_HDL  14567
#define MOCK_REPL_Q_HDL  14568
#define MOCK_FH_DEVICE_A 100000
#define MOCK_FH_DEVICE_B 100001


static struct testdef tst[] = {
		{ "daemonMain_init" , setup , run_init , teardown },
		{ "daemonMain_findCli" , setup , run_findCli , teardown },
		{ "daemonMain_open" , setup , run_open , teardown },
		{ "daemonMain_write" , setup , run_write , teardown },
		{ "daemonMain_read" , setup , run_read , teardown },
		{ "daemonMain_writefail" , setup , run_writefail , teardown },
};
static int dummy1 = defineTest( tst ) && defineTest( tst+1 ) && defineTest( tst+2 ) && defineTest( tst+3 ) && defineTest( tst+4 );


static struct daemon dm;
static char *msgBuffer=0;
static char *out_message=0;
static unsigned int out_message_size;
static LFXmsg_b *out_message_b;
static char handle;
static char mock_io_oCnt[2]={0,0};
static unsigned int mock_io_last_offset;
static unsigned int mock_io_last_len;
static char mock_io_failNext=0;

static struct LFXmtdpart_info dbg_parts[] = {{"mtd0","part A",0x00100000,0x00020000},{"mtd0","part B",0x00700000,0x00020000}};
static struct LFXsubpart_info dbg_subs[] = {{"LFX part 01","part A",0,0x80000},{"LFX part 02","part A",0x80000,0x100000},{"LFX part 03","part B",0x6C0000,0x040000},{"LFX part 04","part B",0x660000,0x060000}};

#define BUFFERSIZE (4096+32)

static int setup()
{
	if(msgBuffer==0)
		msgBuffer = (char*)malloc(BUFFERSIZE);
	if(out_message==0)
		out_message = (char*)malloc(BUFFERSIZE);
	out_message_b = (LFXmsg_b*)out_message;
	memset( msgBuffer , 0xBB , BUFFERSIZE );
	memset( &dm , 0xBB , sizeof(dm) );
	init_daemon( &dm );
	if( dm.msgget != msgget )return -1;
	dm.msgget = mock_msgget;
	dm.msgsnd = mock_msgsnd;
	dm.msgrcv = mock_msgrcv;
	dm.cli_proto.open = mock_open;
	dm.cli_proto.close = mock_close;
	dm.cli_proto.ioctl = mock_ioctl;
	dm.cli_proto.read = mock_read;
	dm.cli_proto.write = mock_write;
	dm.cli_proto.lseek = mock_lseek;
	dm.mtdParts = dbg_parts;
	dm.mtdParts_num = 2;
	dm.subParts = dbg_subs;
	dm.subParts_num = 4;
	dm.subParts_num = 2;
	mock_io_oCnt[0] = 0;
	mock_io_oCnt[1] = 0;
	handle = -1;
	return 0;
}

static int run_init()
{
  LFXmsg_b_i cmsg;
	out_message_size=0;

	cmsg.cmd = CMD_CONNECT;
	cmsg.byt = 0x2A;
	cmsg.para = MOCK_REPL_Q_ID;
	process_connect( &dm , (char*)(&cmsg) );

	// was ack sent?
	if( out_message_size<sizeof(LFXmsg_b) )
		return -1;		// no reply message (or too small)
	if( out_message_b->cmd!=REP_CONNECT || ((const LFXmsg_bb*)out_message)->byt1!=0 )
		return -2;		// not positive ack

	// was connect processed?
	if( dm.clients[0].cl_mq_h != MOCK_REPL_Q_HDL ) return -1;

	handle = ((const LFXmsg_bb*)out_message)->byt2;

	return 0;
}

static int run_findCli()
{
  int rv;
  char dummy[64];
  struct mtdClient_init d;
	// start like first test
	rv = run_init();if(rv)return rv;

	// try to resolve "LFX part 02"
	rv = find_client_init_data( &dm , "LFX part 02" , &d , dummy );
	if( rv )return -11;
	if( d.startOffset != 0x80000 )return -12;
	if( d.endOffset != 0x100000 )return -12;

	return 0;
}

static int run_open()
{
  LFXmsg_b *omsg = (LFXmsg_b*)msgBuffer;
  const char *name_request = "LFX part 02";
  int rv;
	// start like first test
	rv = run_init();if(rv)return rv;
	// try to open dev.
	out_message_size=0;
	omsg->cmd = CMD_OPENDEV;
	omsg->byt = handle;
	strcpy( (char*)(omsg+1) , name_request );
	process_openDev( &dm , msgBuffer , dm.clients+0 );

	// was ack sent?
	if( out_message_size<sizeof(LFXmsg_b) )
		return -11;		// no reply message (or too small)
	if( out_message_b->cmd!=REP_OPENDEV || out_message_b->byt!=0 )
		return -12;		// not positive ack

	if( dm.clients[0].dev == 0 )return -13;

	return 0;
}

static int run_write()
{
  int rv;
  LFXmsg_b_i *wcmd = (LFXmsg_b_i*)msgBuffer;
	// start like second test
	rv = run_open();if(rv)return rv;

	// send write packet.
	out_message_size=0;
	wcmd->cmd = CMD_WRITE;
	wcmd->byt = handle;
	wcmd->para = 0x1240;
	memset( wcmd+1 , 0xAC , 0x01000 );
	process_write( &dm , msgBuffer , sizeof(LFXmsg_b_i)+0x1000 , dm.clients+0 );

	// was ack sent?
	if( out_message_size<sizeof(LFXmsg_b) )
		return -21;		// no reply message (or too small)
	if( out_message_b->cmd!=REP_WRITE || out_message_b->byt!=0 )
		return -22;		// not positive ack

	if( mock_io_last_offset != 0x81240 )return -23;
	if( mock_io_last_len != 0x01000 )return -24;


	return 0;
}

static int run_read()
{
  int rv;
  int i;
  LFXmsg_b_ii *wcmd = (LFXmsg_b_ii*)msgBuffer;
  const unsigned char *rd;

	// start like second test
	rv = run_open();if(rv)return rv;

	// send read packet.
	out_message_size=0;
	memset( msgBuffer , 0xFE , BUFFERSIZE );
	wcmd->cmd = CMD_READ;
	wcmd->byt = handle;
	wcmd->para1 = 0x4210;
	wcmd->para2 = 0x0C00;
	process_read( &dm , msgBuffer , sizeof(LFXmsg_b_ii) , dm.clients+0 );

	// was reply sent?
	if( out_message_size<sizeof(LFXmsg_b) )
		return -21;		// no reply message (or too small)
	if( out_message_b->cmd!=REP_READ || out_message_b->byt!=0 )
		return -22;		// not positive ack
	if( out_message_size!=sizeof(LFXmsg_b)+0x0C00 )
		return -23;		// too small to include returned data.

	if( mock_io_last_offset != 0x84210 )return -23;
	if( mock_io_last_len != 0x0C00 )return -24;

	rd = (const unsigned char*)(out_message+sizeof(LFXmsg_b));
	for( i=0 ; i<0x0C00 ; i++ )
		if( rd[i] != 0xABu )return -25;

	return 0;
}

static int run_writefail()
{
  int rv;
  LFXmsg_b_i *wcmd = (LFXmsg_b_i*)msgBuffer;
	// start like second test
	rv = run_open();if(rv)return rv;

	// send write packet.
	out_message_size=0;
	wcmd->cmd = CMD_WRITE;
	wcmd->byt = handle;
	wcmd->para = 0x1240;
	memset( wcmd+1 , 0xAC , 0x01000 );
	mock_io_failNext = 1;
	process_write( &dm , msgBuffer , sizeof(LFXmsg_b_i)+0x1000 , dm.clients+0 );

	// was ack sent?
	if( out_message_size<sizeof(LFXmsg_b) )
		return -21;		// no reply message (or too small)
	// expect negative ack.
	if( out_message_b->cmd!=REP_WRITE || out_message_b->byt==0 )
		return -22;		// not negative ack

	return 0;
}


static int teardown()
{
	dm.mtdParts = 0;
	dm.subParts = 0;
	uninit_daemon( &dm );
	memset( &dm , 0xFE , sizeof(dm) );
	if(msgBuffer)
		free(msgBuffer);
	msgBuffer = 0;
	if(out_message)
		free(out_message);
	out_message = 0;
	out_message_b = 0;
	handle=0;
	return 0;
}

// mock the message functions

static int mock_msgget(key_t key,int msgflg)
{
	if( key == MOCK_REPL_Q_ID )
		return MOCK_REPL_Q_HDL;
	return -1;
}

static int mock_msgsnd(int msqid,const void *msgp,size_t msgsz,int msgflg)
{
	if( msqid!=MOCK_REPL_Q_HDL )return -1;
	if( out_message )free(out_message);
	testAssert( msgflg==0 , "want msgflg to be zero" );
	testAssert( msgsz>0 && msgsz<0x100000 , "bad size to msgsnd" );
	out_message = (char*)malloc(msgsz+1);
	out_message_b = (LFXmsg_b*)out_message;
	memcpy( out_message , msgp , msgsz );
	out_message_size = msgsz;
	return 0;
}

static ssize_t mock_msgrcv(int msqid,void *msgp,size_t msgsz,long msgtyp,int msgflg)
{
	if( msqid!=MOCK_REPL_Q_HDL )return -1;
	testAssert( 0 , "test should not run into msgrcv" );
	return -1;
}

// mock the device (file) I/O functions   open/close/ioctl/read/write/lseek

static off_t mOff[2];

static int mock_open( const char *name , int flags , ... )
{
  unsigned char devNo=0;
	if( mock_io_failNext ){mock_io_failNext=0;return -1;}
	// only the two mock-devs.
	if( !strcmp( name , "/dev/mtd0" ) )
		devNo=0;
	else if( !strcmp( name , "/dev/mtd1" ) )
		devNo=1;
	else
		return -1;
	// openflag does not allow O_CREAT or O_TRUNC
	if( flags&(O_CREAT|O_TRUNC) )return -1;

	mock_io_oCnt[devNo]++;
	mOff[devNo]=0;

	return ( devNo ? MOCK_FH_DEVICE_B : MOCK_FH_DEVICE_A );
}

static int mock_close( int fh )
{
	testAssert( fh==MOCK_FH_DEVICE_A || fh==MOCK_FH_DEVICE_B , "can only close the dummy handles." );
	testAssert( mock_io_oCnt[fh-MOCK_FH_DEVICE_A]>0 , "mock file was not opened (or already closed)" );
	mock_io_oCnt[fh-MOCK_FH_DEVICE_A]--;
	return 0;
}

static int mock_ioctl(int fh,long unsigned int cod,...)
{
	if( mock_io_failNext ){mock_io_failNext=0;return -1;}
	switch( cod )
	{
	case MEMGETINFO:
		{
		  va_list args;
		  struct mtd_info_user *mtdinf;
			va_start(args,cod);
			mtdinf = (struct mtd_info_user*)(va_arg(args,void*));
			va_end(args);
			memset( mtdinf , 0 , sizeof(*mtdinf) );
			mtdinf->type = MTD_NORFLASH;
			mtdinf->flags = 0x2C00;
			mtdinf->erasesize = 0x020000;
			mtdinf->writesize = 0x000020;
			mtdinf->size = 0x04000000;
		}
		return 0;
	}
	return -1;
}

static ssize_t mock_read(int fh,void *buffer,size_t len)
{
	testAssert( fh==MOCK_FH_DEVICE_A || fh==MOCK_FH_DEVICE_B , "can only close the dummy handles." );
	if( mock_io_failNext ){mock_io_failNext=0;return -1;}
  off_t offset = mOff[fh-MOCK_FH_DEVICE_A];
	mock_io_last_offset = (unsigned int)offset;
	mock_io_last_len = (unsigned int)len;
	testAssert( len<=0x04000000 && offset<=0x04000000 && (len+offset)<=0x04000000 , "bad len/offset for read" );
	memset( buffer , 0xAB , len );
	return (int)len;
}

static ssize_t mock_write(int fh,const void *buffer,size_t len)
{
	testAssert( fh==MOCK_FH_DEVICE_A || fh==MOCK_FH_DEVICE_B , "can only close the dummy handles." );
	if( mock_io_failNext ){mock_io_failNext=0;return -1;}
  off_t offset = mOff[fh-MOCK_FH_DEVICE_A];
	mock_io_last_offset = (unsigned int)offset;
	mock_io_last_len = (unsigned int)len;
	testAssert( len<=0x04000000 && offset<=0x04000000 && (len+offset)<=0x04000000 , "bad len/offset for write" );
	return (int)len;
}

static off_t mock_lseek(int fh,off_t offset,int whence)
{
	testAssert( fh==MOCK_FH_DEVICE_A || fh==MOCK_FH_DEVICE_B , "can only close the dummy handles." );
  off_t *p;
	p = mOff + (fh-MOCK_FH_DEVICE_A);
	switch(whence)
	{
	case SEEK_SET: *p = offset;break;
	case SEEK_CUR: *p += offset;break;
	default:
		testAssert( 0 , "lseek mock only supports SET and CUR." );
	}
	return *p;
}
