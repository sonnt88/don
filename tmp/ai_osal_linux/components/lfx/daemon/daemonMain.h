#ifndef DAEMONMAIN_H
#define DAEMONMAIN_H


// header for daemonMain.c . Only to be used by that. Available only as header for unit-tests to access this.


#include "mtdClient.h"
#include <sys/msg.h>		// to get types like key_t


#define MAX_CLIENTS 16

//#define LOG_COMMANDS		// comment out if not want to log each access.


// Data structure about one potential client.
// A fixed number of these are preallocated in the daemon's main struct.
struct client
{
	int cl_mq_h;			// client's return message queue. -1 if this slot is free.
	struct mtdClient *dev;	// null if not connected.
	unsigned int lastTouch;	// copy of daemon's loopCount to see which client was touched last.
};

// types for message send and recv functions. For unit-testing. Want to be able to replace them.
typedef int (*type_msgget)(key_t,int);
typedef int (*type_msgsnd)(int,const void *,size_t,int);
typedef ssize_t (*type_msgrcv)(int,void *,size_t,long,int);

// The daemon's main structure. Allocated statically on stack in daemon_main().
struct daemon
{
	int dm_mq_h;							// daemon MQ handle
	struct client clients[MAX_CLIENTS];		// preallocated client-structs.
	unsigned int loopCount;
	struct LFXmtdpart_info  *mtdParts;		// list of MTD partitions (from /proc/mtd)
	unsigned int            mtdParts_num;	// number of array above
	struct LFXsubpart_info  *subParts;		// list of MTD subpartitions (LFX special)
	unsigned int            subParts_num;	// number of array above
	type_msgget            msgget;			// pointer to normal system msgget() function.
	type_msgsnd            msgsnd;			// pointer to normal system msgsnd() function.
	type_msgrcv            msgrcv;			// pointer to normal system msgrcv() function.
	struct mtdClient_init  cli_proto;		// prototype struct to init clients. Can be used to override I/O functions.
};


// Prototypes of local functions
#ifdef __cplusplus
extern "C" {
#endif
void init_daemon(struct daemon *self);
void uninit_daemon(struct daemon *self);
char process_msg(struct daemon *self,char *msgBuffer,unsigned int msgLen,unsigned int bufferSize);
void process_connect(struct daemon *self,char *msgBuffer);
void process_disconnect(struct daemon *self,char *msgBuffer,struct client *cli);
void process_openDev(struct daemon *self,char *msgBuffer,struct client *cli);
void process_info(struct daemon *self,char *msgBuffer,struct client *cli);
void process_listDevs(struct daemon *self,char *msgBuffer,unsigned int bufferSize,struct client *cli);
void process_read(struct daemon *self,char *msgBuffer,unsigned int msgLen,struct client *cli);
void process_write(struct daemon *self,char *msgBuffer,unsigned int msgLen,struct client *cli);
void process_erase(struct daemon *self,char *msgBuffer,unsigned int msgLen,struct client *cli);
int find_client_init_data(struct daemon *self,const char *LFXname,struct mtdClient_init *out_data,char *scratch_nameBuffer);
#ifdef __cplusplus
}
#endif



#endif
