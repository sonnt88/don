#ifndef PARTCONFIG_H
#define PARTCONFIG_H


#define MAX_DEV_MTDNAME 24
#define MAX_DEV_NICENAME 24
#define MAX_LFX_NAME  24

#define MTD_PROC_FILENAME   "/proc/mtd"
#define LFX_SUBPART_CONFIG_FILENAME "/etc/default/LFXsubpartitions.conf"

/* sample for config file:

# Config file for LFX subpartitions
#  start and end values hex or decimal, negative values count off the back end.
#
# LFX name    mtdname       start       end
errmem      bootdata         0x00100000  0x00160000		# errmem. 768k
KDS         bootdata         0x00160000  0x00180000		# KDS. 256k
PDD         bootdata         0x00180000      0			# PDD. to end of mtd device. (0 as end is end-of-device)

*/

struct LFXmtdpart_info
{
	char DEVmtdname[MAX_DEV_MTDNAME];
	char DEVnicename[MAX_DEV_NICENAME];
	unsigned int size;
	unsigned int eraseSize;
};

struct LFXsubpart_info
{
	char LFXname[MAX_LFX_NAME];
	char DEVnicename[MAX_DEV_NICENAME];
	int start,end;
};


#ifdef __cplusplus
extern "C" {
#endif

struct LFXmtdpart_info *parseMtdProcFile(const char *filename,unsigned int *out_numparts);		// filename should be MTD_PROC_FILENAME
struct LFXsubpart_info *parseLfxConfigFile(const char *filename,unsigned int *out_numparts);	// filename should be LFX_SUBPART_CONFIG_FILENAME

#ifdef __cplusplus
}
#endif


#endif
