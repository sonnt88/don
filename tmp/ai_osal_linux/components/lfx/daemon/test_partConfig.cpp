#include "test.h"
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <malloc.h>
#include "partConfig.h"


static int setup();
static int run1();
static int run2();
static int run3();
static int teardown();

#define TMP_FILENAME "/tmp/test_partConfig.tmp"

static struct testdef tst1[] = {
		{ "partConfig1" , setup , run1 , teardown },
		{ "partConfig2" , setup , run2 , teardown },
		{ "partConfig3" , setup , run3 , teardown },
};
static int dummy1 = defineTest( tst1 ) && defineTest( tst1+1 ) && defineTest( tst1+2 );


static int fh=-1;
static struct LFXsubpart_info *spList=0;
static struct LFXmtdpart_info *mpList=0;

static int setup()
{
	if(fh>=0)return -1;
	fh = open( TMP_FILENAME , O_CREAT|O_TRUNC|O_WRONLY , 0x01B6 );
	if(fh<0)return -1;
	return 0;
}

static int run1()
{
  int dummy=0;
  unsigned int mpList_count;
  static const char *filData =
		"dev:    size   erasesize  name\n"
		"mtd0: 00060000 00020000 \"u-boot\"\n"
		"mtd1: 00020000 00020000 \"u-boot env\"\n"
		"mtd2: 00080000 00020000 \"device trees\"\n"
		"mtd3: 00700000 00020000 \"kernel\"\n"
		"mtd4: 00040000 00020000 \"KDS\"\n"
		"mtd5: 000c0000 00020000 \"errmem\"\n"
		"mtd6: 07700000 00020000 \"data\"\n"
  ;
	// create test file
	dummy += write( fh , filData , strlen(filData) );
	close(fh);
	if(dummy<=0)return -1;

	// call parse function
	mpList = parseMtdProcFile( TMP_FILENAME , &mpList_count );		// filename should be MTD_PROC_FILENAME
	if(!mpList)return -2;

	// check results
	if(mpList_count!=7)return -3;
	if(mpList[6].size!=0x07700000)return -4;
	if(mpList[6].eraseSize!=0x00020000)return -5;
	if(strcmp(mpList[6].DEVmtdname,"mtd6"))return -6;
	if(strcmp(mpList[6].DEVnicename,"data"))return -7;
	if(strcmp(mpList[1].DEVnicename,"u-boot env"))return -8;

	return 0;
}

static int run2()
{
  int dummy=0;
  unsigned int mpList_count;
  static const char *filData =
		"dev:    size   erasesize  name\n"
		"mtd0: 00020000 \"u-boot\"\n"	// purposely one field missing. Want to see null-return without crash.
  ;
	dummy += write( fh , filData , strlen(filData) );
	close(fh);
	if(dummy<=0)return -1;

	mpList = parseMtdProcFile( TMP_FILENAME , &mpList_count );		// filename should be MTD_PROC_FILENAME
	return ( mpList ? -1 : 0 );
}

static int run3()
{
  int dummy=0;
  unsigned int spList_count;
  static const char *filData =
		"# Some useless comments in \"header\" of this dummyfile.\n"
		"# LFX name    mtdname       start       end\n"
		"\n"
		"errmem      bootdata       0x00100000  0x00160000		# errmem. 768k\n"
		"KDS         bootdata       0x00160000  -131072		# KDS. 256k\n"
		"\n"
		"something   \"u-boot env\"   0x00000000  0x00180000		# KDS. 256k\n"
		"\n"
		"\n"
  ;
	// create test file
	dummy += write( fh , filData , strlen(filData) );
	close(fh);
	if(dummy<=0)return -1;

	// call parse function
	spList = parseLfxConfigFile( TMP_FILENAME , &spList_count );		// filename should be MTD_PROC_FILENAME
	if(!spList)return -2;
	if(spList_count!=3)return -3;

	// check results
	if(strcmp( spList[0].LFXname , "errmem" ))return -4;
	if(strcmp( spList[1].LFXname , "KDS" ))return -5;
	if(strcmp( spList[2].LFXname , "something" ))return -6;
	if(strcmp( spList[2].LFXname , "something" ))return -7;
	if(strcmp( spList[0].DEVnicename , "bootdata" ))return -8;
	if(strcmp( spList[1].DEVnicename , "bootdata" ))return -9;
	if(strcmp( spList[2].DEVnicename , "u-boot env" ))return -10;
	if( spList[0].start != 0x0100000 )return -11;
	if( spList[0].end != 0x0160000 )return -12;
	if( spList[1].end != -0x020000 )return -13;
	if( spList[2].end != 0x0180000 )return -14;


	return 0;
}

static int teardown()
{
	if(spList)
		free(spList);
	if(mpList)
		free(mpList);
	if(fh>=0)
		close(fh);
	fh=-1;
	unlink(TMP_FILENAME);
	return 0;
}

