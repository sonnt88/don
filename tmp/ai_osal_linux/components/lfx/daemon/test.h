#ifndef C_TEST_H
#define C_TEST_H



// Very simple 'unit' testing framework.
// Does not count/log errors, but exit on the first.

// typedefs for a test. functions.

typedef int (*test_setup_func)();
typedef int (*test_run_func)();
typedef int (*test_teardown_func)();

struct testdef
{
	const char          *name;
	test_setup_func     setup;
	test_run_func       run;
	test_teardown_func  teardown;
};

#ifdef __cplusplus
extern "C" {
#endif

int defineTest( const struct testdef *test );
void testAssert( char bCondition , const char *descString );
int dummyTestfunc();


#ifdef __cplusplus
}
#endif




#endif
