#include "test.h"
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>


static const struct testdef **tests=0;
static unsigned int tests_num=0;
static unsigned int tests_max=0;


int dummy_tf(){return 0;}
struct testdef muellTest = {"ABC",dummy_tf,dummy_tf,dummy_tf};

static int dummy = defineTest( &muellTest );

int main()
{
   unsigned int i;
   char bGood=0;
	//printf("tests_num = %u\n",tests_num);
	for( i=0 ; i<tests_num ; i++ )
	{
	  const struct testdef *td;
	  int rv;
		td = tests[i];
		printf("test %u: %s\n",i,td->name);
		rv = td->setup();
		if(rv)
		{
			fprintf(stderr,"FAIL: setup returned %d.\n",rv);
			break;
		}
		rv = td->run();
		if(rv)
		{
			fprintf(stderr,"FAIL: run returned %d.\n",rv);
			td->teardown();
			break;
		}
		rv = td->teardown();
		if(rv)
		{
			fprintf(stderr,"FAIL: run returned %d.\n",rv);
			break;
		}
	}
	if( i>=tests_num )
		bGood=1;
	if(bGood)
		printf("all passed.\n");
	return ( bGood ? 0 : 1 );
}

int defineTest( const struct testdef *test )
{
	if(!tests)
	{
		tests_max = 16;
		tests = (const struct testdef**)malloc(sizeof(void*)*tests_max);
	}
	if( tests_num >= tests_max )
	{
		tests_max = (tests_max*2+4);
		tests = (const struct testdef**)realloc( tests , sizeof(void*)*tests_max );
	}
	tests[tests_num++] = test;
	return 42;
}

void testAssert( char bCondition , const char *descString )
{
	if(!bCondition)
	{
		if(descString)
			fprintf( stderr , "ASSERT FAIL: %s\n" , descString );
		else
			fprintf( stderr , "ASSERT FAIL\n" );
		exit(10);
	}
}

int dummyTestfunc()
{
	return 0;
}
