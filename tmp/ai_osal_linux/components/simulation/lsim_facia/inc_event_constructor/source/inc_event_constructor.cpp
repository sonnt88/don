/************************************************************************
| FILE:         inc_evconstructor.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Program to transform LSIM System Events to INC messages.
|
|               Setup for test with INPUT_INC kernel module and fake device:
|
|               modprobe dev-fake
|               /opt/bosch/base/bin/inc_evconstructor
|               modprobe input_inc
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 11.03.13  | Initial Version            | tma2hi
| 19.07.13  | Revision for LSIM InputDev | kgr1kor
| 20.08.13  | InputDev INC Protocol V2   | kgr1kor
| 18.09.13  | Review update              | kgr1kor
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <mqueue.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <linux/input.h>

#include "inc.h"
#include "inc_ports.h"
#include "inc_scc_input_device.h"
#include "facia_xml_parser.h"



#include "dgram_service.h"

#ifdef __cplusplus
extern "C" {
#endif

#define INPUT_INC_STATUS_ACTIVE   		0x01

#define INPUT_INC_PROTOCOL_VERSION_CUR 2
#define INPUT_INC_PROTOCOL_VERSION_MIN 1

#define TRUE 1
#define FALSE 0



#define MSQ_PRIORITY 0x33
#define MSQ_NAME "/LSIMINCEVE"
#define MSQ_MAXMESG 100
#define MSQ_MSGSIZE 256

/*GLOBAL DATA*/
static int input_is_active = 0;
/*GLOBAL DATA*/

static void data_send(void *arg, char *sendbuf)
{
	dbgprintf( "INC_DataSend_raw:\tMSGID & DEVID:%x COUNT:%x\t\n",
	(int)*(unsigned char *)sendbuf,
	(int)*(unsigned char *)(sendbuf+1));
	int status;
	int headersize = 2;
	sk_dgram *dgram = (sk_dgram *) arg;
	if ((status = dgram_send(dgram, sendbuf, (headersize+(*(sendbuf + headersize -1)*8)))) == -1)
	{
		dbgprintf( "INC_sendto failed: %d\n", status);
	}
	else
	{
		int i=0;
		dbgprintf( "--------------------\n");
		dbgprintf( "<- R_EVENT: DeviceID: %x\n",  *(uint8_t*)(sendbuf+ headersize -2)); /*DEVID:1B*/
		dbgprintf( "<- R_EVENT: NumEvents: %d\n", *(uint8_t*)(sendbuf+ headersize -1)); /*EVCOUNT:1B*/
		for(i=0;i<(int)(*(sendbuf + headersize -1));i++)
		{
			dbgprintf("<- R_EVENT: Type: %d\tCode: %d\tValue: %d\n",
			*(unsigned short *) (sendbuf + headersize + (i*8)),
			*(unsigned short *) (sendbuf + headersize + (i*8) +
			sizeof(unsigned short)),
			*(signed int *)     (sendbuf + headersize + (i*8) +
			sizeof(unsigned short)+sizeof(unsigned short))); /*TYPE:2B CODE:2B VALUE:4B*/
		}
		dbgprintf( "--------------------\n");
	}
}

void hardkeys_remapping(__u16 *hardkeyval) {
#ifdef VARIANT_S_FTR_ENABLE_CANDERAADAPTER
	switch (*hardkeyval) {
	/* g3g */
	case 102:   *hardkeyval = KEY_H; break; /* KEY_HOME */
	case 762:   *hardkeyval = KEY_S; break; /* KEY_TEST */
	case 0:     *hardkeyval = 0x18;  break; /* ENC_LEFT */
	case 500:   *hardkeyval = 0x10;  break; /* ENC_RIGHT */
	/* rnaivi */
	/*    ncg3-p42r-nar,ncg3-p42r-eur */
	case 0x30b: *hardkeyval = KEY_I; break; /* BTN_ILLUM */
	case 0x304: *hardkeyval = KEY_N; break; /* BTN_PREV */
	case 0x305: *hardkeyval = 0x66;  break; /* BTN_NEXT */
	case 0x303: *hardkeyval = KEY_A; break; /* BTN_AUX */
	case 0x312: *hardkeyval = KEY_U; break; /* HK_MENU */
	case 0x309: *hardkeyval = KEY_M; break; /* BTN_MAP */
	case 0x302: *hardkeyval = KEY_C; break; /* BTN_CAM */
	case 0x30a: *hardkeyval = KEY_B; break; /* BTN_BACK */
	case 0x30f: *hardkeyval = KEY_Y; break; /* ENC_LEFT */
	case 0x30d: *hardkeyval = KEY_X; break; /* ENC_RIGHT */
	case 301:   *hardkeyval = KEY_1; break; /* BTN_IGN_OFF */
	case 302:   *hardkeyval = KEY_2; break; /* BTN_IGN_ACC */
	case 303:   *hardkeyval = KEY_3; break; /* BTN_IGN_IGN */
	case 0x35a: *hardkeyval = KEY_5; break; /* BTN_SWC_ENTER */
	case 0x358: *hardkeyval = KEY_4; break; /* BTN_SWC_UP */
	case 0x359: *hardkeyval = KEY_6; break; /* BTN_SWC_DOWN */
	case 0x67:  *hardkeyval = 0x67;  break; /* BTN_SWC_PREV */
	case 0x68:  *hardkeyval = KEY_7; break; /* BTN_SWC_TEL_ON */
	case 0x6C:  *hardkeyval = KEY_8; break; /* BTN_SWC_TEL_OFF */
	case 0x6B:  *hardkeyval = KEY_9; break; /* BTN_SWC_VOL_UP */
	case 0x6A:  *hardkeyval = KEY_0; break; /* BTN_SWC_VOL_DOWN */
	case 0x350: *hardkeyval = 0x68;  break; /* BTN_SWC_SOURCE */
	/*    ncg3-p32r-nar */
	case 0x310: *hardkeyval = KEY_E; break; /* BTN_EJECT */
	case 0x30e: *hardkeyval = 0x69;  break; /* BTN_SXM */
	case 0x311: *hardkeyval = KEY_F; break; /* BTN_AM_FM */
	case 0x301: *hardkeyval = KEY_D; break; /* BTN_CD */
	case 0x30c: *hardkeyval = 0x6a;  break; /* BTN_NAV */
	case 0x313: *hardkeyval = 0x6b;  break; /* BTN_APPS */
	case 0x300: *hardkeyval = KEY_T; break; /* BTN_PHONE */
	/*    ncg3-p32r-eur */
	case 0x306: *hardkeyval = KEY_R; break; /* BTN_RADIO */
	case 0x307: *hardkeyval = 0x6c;  break; /* BTN_INFO */
	case 0x308: *hardkeyval = KEY_P; break; /* BTN_SETUP */
	/*    ncg3-p32r */
	case 0x65:  *hardkeyval = 0x6d;  break; /* BTN_SWC_SOURCE */
	/*    ncg3-b02e-eur */
	case 0x315: *hardkeyval = 0x6e;  break; /* BTN_DISP */
	/*    rnaivi-dev */
	/*    pivi-nar */
	case 0x3CB: *hardkeyval = 0x6f;  break; /* BTN_JOY_MENU */
	case 0x314: *hardkeyval = 0x70;  break; /* BTN_JOY_AUDIO */
	case 0x3CC: *hardkeyval = 0x71;  break; /* BTN_JOY_BACK */
	case 0x3C8: *hardkeyval = 0x72;  break; /* BTN_JOY_OK */
	case 0x3C0: *hardkeyval = 0x73;  break; /* BTN_JOY_UP */
	case 0x3C1: *hardkeyval = 0x74;  break; /* BTN_JOY_DOWN */
	case 0x3C2: *hardkeyval = 0x75;  break; /* BTN_JOY_LEFT */
	case 0x3C3: *hardkeyval = 0x76;  break; /* BTN_JOY_RIGHT */
	case 0x3C4: *hardkeyval = 0x77;  break; /* BTN_JOY_LEFTUP */
	case 0x3C6: *hardkeyval = 0x78;  break; /* BTN_JOY_LEFTDOWN */
	case 0x3C5: *hardkeyval = 0x79;  break; /* BTN_JOY_RIGHTUP */
	case 0x3C7: *hardkeyval = 0x7a;  break; /* BTN_JOY_RIGHTDOWN */
	/*    150213 */
	case 0x35c: *hardkeyval = 0x7b;  break; /* HK_MUTE */
	case 0x23:  *hardkeyval = 0x7c;  break; /* HK_ADAS */
	case 0x25:  *hardkeyval = 0x7d;  break; /* HK_OPTION */
	case 0x356: *hardkeyval = 0x7e;  break; /* HK_ENC_LEFT_LEFT */
	case 0x355: *hardkeyval = 0x7f;  break; /* HK_ENC_LEFT_RIGHT */
	case 0x32:  *hardkeyval = 0x80;  break; /* HK_ENC_RIGHT_LEFT */
	case 0x33:  *hardkeyval = 0x81;  break; /* HK_ENC_RIGHT_RIGHT */
	/*    150214 */
	/* chery */
	case 806:   *hardkeyval = 0x82;  break; /* KEY_POWER */
	case 790:   *hardkeyval = KEY_V; break; /* KEY_Navi */
	case 791:   *hardkeyval = 0x83;  break; /* KEY_Media */
	case 792:   *hardkeyval = 0x84;  break; /* KEY_Phone */
	case 793:   *hardkeyval = 0x85;  break; /* KEY_Apps */
	case 795:   *hardkeyval = 0x86;  break; /* KEY_Back */
	case 807:   *hardkeyval = 0x87;  break; /* KEY_HOME */
	case 794:   *hardkeyval = 0x88;  break; /* KEY_SETTINGS */
	case 796:   *hardkeyval = 0x89;  break; /* KEY_VOLUMEUP */
	case 797:   *hardkeyval = 0x8a;  break; /* KEY_VOLUMEDOWN */
	case 798:   *hardkeyval = 0x8b;  break; /* KEY_Mute */
	/* suzsln */
	case 116:   *hardkeyval = 0x8c;  break; /* KEY_POWER */
	case 115:   *hardkeyval = 0x8d;  break; /* KEY_VOLUMEUP */
	case 114:   *hardkeyval = 0x8e;  break; /* KEY_VOLUMEDOWN */
	/* psarcc */
	case 0x39:  *hardkeyval = 0x8f;  break; /* HK_DRIVE */
	case 0x3A:  *hardkeyval = 0x90;  break; /* HK_CLIMATE */
	case 0x20:  *hardkeyval = 0x91;  break; /* HK_NAV */
	case 0x1e:  *hardkeyval = 0x92;  break; /* HK_MEDIA */
	case 0x1a:  *hardkeyval = 0x93;  break; /* HK_PHONE */
	case 0x38:  *hardkeyval = KEY_W; break; /* HK_WEB */
	}
#endif
	return;
}

void *tx_thread(void *arg)
{
	int  cycle = 0;
	int res			= 0;
	int RefCount 	= 0;
	int ref_abs_x 	= 0;
	int ref_abs_y 	= 0;

	char sendbuf_0[1024];
	char sendbuf_1[1024];
	char sendbuf_2[1024];

	char* pbuff0 = sendbuf_0;
	char* pbuff1 = sendbuf_1;
	char* pbuff2 = sendbuf_2;

	struct mq_attr eventqueueattr = {0,MSQ_MAXMESG,MSQ_MSGSIZE,0} ;
	struct input_event * pevent	= NULL;
	mqd_t eventmq_handle 		= 0;
	size_t size 				= 1024*1024*sizeof(char);
	char *evreceivebuff 		= (char *)malloc(size);

	dbgprintf("MQREAD:Init Start..\n");

	if((-1)==(eventmq_handle = mq_open(MSQ_NAME, O_CREAT| O_RDONLY ,
					S_IRUSR|S_IWUSR, &eventqueueattr)))
	{
		dbgprintf("MsgQueue:Create Failed with result 0x%X\n",eventmq_handle);
		return (void *)1;
	}

	if ( (-1) != mq_getattr(eventmq_handle,&eventqueueattr))
	{
		dbgprintf("MQREAD:Attr:0x%lX | %ld | %ld | %ld \n",
		eventqueueattr.mq_flags,
		eventqueueattr.mq_maxmsg,
		eventqueueattr.mq_msgsize,
		eventqueueattr.mq_curmsgs);
	}
	else
	dbgprintf("MsgQueue GetAttr Failed");

	dbgprintf("\nINC_SEND: Waiting for Initial Configuration of devices\n");

	while(!input_is_active)
	{
		usleep(100000);
	}

	dbgprintf("\nINPUT IS ACTIVATED\n");
	do
	{
		memset(evreceivebuff,'\0',size);
		if ((-1) != (res=mq_receive(eventmq_handle,evreceivebuff,size,NULL)))
		{
			pevent = (struct input_event *)evreceivebuff;
			hardkeys_remapping(&pevent->code);
			dbgprintf("MQRECEIVED: @%ld:%ld TYPE=%u CODE=%u VAL=%d\n",
			pevent->time.tv_sec,
			pevent->time.tv_usec,
			pevent->type,
			pevent->code,
			pevent->value);

			switch(pevent->type)
			{
			case EV_SYN:
				dbgprintf("\t===SYNC===\n");
				break;
			case EV_KEY: /*DEV_ID = 0*/
#ifdef BTN_TOUCH_SUPPORT
				if(pevent->code != BTN_TOUCH)
#endif
				{
					dbgprintf("\t===Key%s ===\n",
					(pevent->value)?"DOWN":"UP");

					pbuff0 = sendbuf_0;

					sendbuf_0[0] = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID;
					sendbuf_0[1] = 1; 					/*NumEvents*/
					*(unsigned short *)(pbuff0 += 2) = EV_KEY;  	/*Type*/
					*(unsigned short *)(pbuff0 += sizeof(unsigned short)) = pevent->code;  	/*Code*/
					*(signed int *)    (pbuff0 += sizeof(unsigned short)) = pevent->value; 		/*Value*/
				}
#ifdef BTN_TOUCH_SUPPORT
				else
				{
					dbgprintf("\t===TouchPEN %s ===\n",
					(pevent->value)?"DOWN":"UP");
					pbuff0 = sendbuf_0;
					sendbuf_0[0] = SCC_INPUT_DEVICE_R_EVENT_DEV0_MSGID;
					sendbuf_0[1] = 3; 					/*NumEvents*/
					*(unsigned short *)(pbuff0 += 2) = EV_ABS;  	/*Type*/
					*(unsigned short *)(pbuff0 += sizeof(unsigned short)) = ABS_X;  	/*Code*/
					*(signed int *)    (pbuff0 += sizeof(unsigned short)) = ref_abs_x; 		/*Value*/
					*(unsigned short *)(pbuff0 += sizeof(signed int)) 	  = EV_ABS;  	/*Type*/
					*(unsigned short *)(pbuff0 += sizeof(unsigned short)) = ABS_Y;	/*Code*/
					*(signed int *)    (pbuff0 += sizeof(unsigned short)) = ref_abs_y; 	/*Value*/
					*(unsigned short *)(pbuff0 += sizeof(signed int)) 	  = EV_KEY;  	/*Type*/
					*(unsigned short *)(pbuff0 += sizeof(unsigned short)) = pevent->code;	/*Code:BTN_TOUCH*/
					*(signed int *)    (pbuff0 += sizeof(unsigned short)) = pevent->value; 		/*Value*/
				}
#endif

				data_send(arg,sendbuf_0);
				break;
			case EV_REL:
				RefCount = RefCount + pevent->value;
				dbgprintf("\t===Scroll Move: %d === RefCount: %d\n",
				pevent->value,RefCount);

				pbuff1 = sendbuf_1;
				sendbuf_1[0] = SCC_INPUT_DEVICE_R_EVENT_DEV1_MSGID;
				sendbuf_1[1] = 1; 					/*NumEvents*/
				*(unsigned short *) (pbuff1 += 2) = EV_REL;  	/*Type*/
				*(unsigned short *) (pbuff1 += sizeof(unsigned short)) = pevent->code;  	/*Code*/
				*(signed int *)     (pbuff1 += sizeof(unsigned short)) = pevent->value; 		/*Value*/
				data_send(arg,sendbuf_1);
				break;
			case EV_ABS:
				dbgprintf("\t===TouchDrag===\n");
				if(pevent->code == 0 )
				ref_abs_x = pevent->value;
				else
				ref_abs_y = pevent->value;

				pbuff2 = sendbuf_2;
				sendbuf_2[0] = SCC_INPUT_DEVICE_R_EVENT_DEV2_MSGID;
				sendbuf_2[1] = 1; 					/*NumEvents*/
				*(unsigned short *) (pbuff2 += 2) = EV_ABS;  	/*Type*/
				*(unsigned short *) (pbuff2 += sizeof(unsigned short)) = pevent->code;  	/*Code*/
				*(signed int *)     (pbuff2 += sizeof(unsigned short)) = pevent->value; 		/*Value*/
				data_send(arg,sendbuf_2);
				break;
			default:
				dbgprintf("\n\t!!!Unknown event received!!!\n");
				break;
			}
		}
		else
		dbgprintf("MQREAD:FAILED mq_receive(%d,%d,%d,NULL)",
		eventmq_handle,(int)&evreceivebuff,(int)size);
		cycle++;
	}while(input_is_active);

	free(evreceivebuff);
	dbgprintf("MQREAD:Init Done!\n\n");

	return 0;
}

int main(int Argc,char* Argv[])
{
	int m, n, fd, ret;
	char sendbuf[1024];
	char recvbuf[1024];
	pthread_t tid;

	Keymap Keyinfo[40];
	int    Key[40] = {0};

	int NumerOfKeys;
	int cnt=0;
	int blocks;
	bool flag=0;
	int  keycnt=0;

	int i=0;

	int max_x = 0;
	int max_y = 0;
	if(Argc > 1)
	{
           if(LoadXML(Argv[1]) == SUCCESS)
           {
	          errprintf("load xml file success\n");
           }
           else
           {
              errprintf("ERROR: load xml file not successful\n");
              return ERROR;
           }
	}
	else
	{
           int xmlval = LoadXML((char*)"/opt/bosch/base/lsim/facia_configuration.xml");
           if(xmlval != SUCCESS)
           {
              errprintf("load xml file failed\n");
              return ERROR;
           }
	}
	if(Argc > 2)
	{ /* actual LSIM resolution given as program parameters */
	   max_x = atoi(Argv[2]);
	   max_y = atoi(Argv[3]);
	}
	if((max_x <= 0) || (max_y <= 0))
	{ /* set default resolution to 1024x768 if values are 0 or negative */
	   max_x = 1024;
	   max_y = 768;
	}

	NumerOfKeys = GetNumberOfBlocks();
	dbgprintf( "Number of blocks: %d\n", NumerOfKeys);
	GetKeymapInfo((Keymap*)&Keyinfo);



	for(cnt=0;cnt<NumerOfKeys;cnt++)
	{
		for(blocks=0;blocks<cnt;blocks++)
		{
			if(Key[blocks]==Keyinfo[cnt].KeyCode)
			{
				flag= TRUE;
				//dbgprintf("same key code:%d block:%d",Keyinfo[cnt].KeyCode,blocks);
				break;
			}
			else
			{
				//dbgprintf("Diff block:%d\n",blocks);
			}
		}
		if(flag == FALSE)
		{
			Key[keycnt]= Keyinfo[cnt].KeyCode;
			hardkeys_remapping((__u16*)&Key[keycnt]);
			dbgprintf("key#%i cnt %i Keyinfo[] %3i Key[] %3i\n", keycnt+1, cnt, Keyinfo[cnt].KeyCode, Key[keycnt]);
			keycnt++;
		}
		else
		{
			flag = FALSE;
		}
	}

	dbgprintf("total valid keys: %d\n", keycnt);



	sk_dgram *dgram;
	struct hostent *local, *remote;
	struct sockaddr_in local_addr, remote_addr;

	local = gethostbyname("fake1-local"); /*read IP address from /etc/hosts */
	if (local == NULL)
	{
		int errsv = errno;
		errprintf( "gethostbyname(fake1-local) failed: %d\n", errsv);
		return ERROR;
	}
	local_addr.sin_family = AF_INET;
	memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);
	local_addr.sin_port = htons(INPUT_DEVICE_PORT); /* from inc_ports.h (50964) */

	remote = gethostbyname("fake1");
	if (remote == NULL)
	{
		int errsv = errno;
		errprintf( "gethostbyname(fake1) failed: %d\n", errsv);
		return ERROR;
	}
	remote_addr.sin_family = AF_INET;
	memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
	remote_addr.sin_port = htons(INPUT_DEVICE_PORT); /* from inc_ports.h (50964) */

	dbgprintf( "local %s:%d\n", inet_ntoa(local_addr.sin_addr), ntohs(local_addr.sin_port));
	dbgprintf( "remote %s:%d\n", inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));


	fd = socket(AF_BOSCH_INC_AUTOSAR, SOCK_STREAM, 0);
	if (fd == ERROR)
	{
		int errsv = errno;
		errprintf( "create socket failed: %d\n", errsv);
		return ERROR;
	}

	dgram = dgram_init(fd, DGRAM_MAX, NULL);
	if (dgram == NULL)
	{
		errprintf( "dgram_init failed\n");
		return ERROR;
	}

	ret = bind(fd, (struct sockaddr *) &local_addr, sizeof(local_addr));
	if (ret < 0)
	{
		int errsv = errno;
		errprintf( "bind failed: %d\n", errsv);
		return ERROR;
	}

	ret = connect(fd, (struct sockaddr *) &remote_addr, sizeof(remote_addr));
	if (ret < 0)
	{
		int errsv = errno;
		errprintf( "connect failed: %d\n", errsv);
		return ERROR;
	}

	if (pthread_create(&tid, NULL, tx_thread, (void *) dgram) == -1)
	dbgprintf( "pthread_create failed\n");

	do {
		do {
			n = dgram_recv(dgram, recvbuf, sizeof(recvbuf));
		} while(n < 0 && errno == EAGAIN);
		if (n < 0)
		{
			int errsv = errno;
			errprintf( "recv failed: %d\n", errsv);
			return ERROR;
		}

		dbgprintf( "recv: %d\n", n);

		if (n == 0)
		{
			errprintf( "invalid msg size: %d\n", n);
			return ERROR;
		}

		switch (recvbuf[0])
		{
		case SCC_INPUT_DEVICE_C_COMPONENT_STATUS_MSGID:
			if (n != 3) 
			{
				errprintf( "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
				return ERROR;
			}
			if (recvbuf[1] != INPUT_INC_STATUS_ACTIVE) 
			{
				errprintf( "invalid status: %d\n", recvbuf[1]);
				return ERROR;
			}
			if (recvbuf[2] > INPUT_INC_PROTOCOL_VERSION_CUR ||
					recvbuf[2] < INPUT_INC_PROTOCOL_VERSION_MIN)
			{
				errprintf( "invalid protocol version: %d\n", recvbuf[2]);
				return ERROR;
			}
			dbgprintf( "-> C_COMPONENT_STATUS.HostAppStatus: %d\n", recvbuf[1]);
			dbgprintf( "-> C_COMPONENT_STATUS.HostAppVersion: %d\n", recvbuf[2]);
			dbgprintf( "--------------------\n");

			sendbuf[0] = SCC_INPUT_DEVICE_R_COMPONENT_STATUS_MSGID;
			sendbuf[1] = INPUT_INC_STATUS_ACTIVE;
			sendbuf[2] = INPUT_INC_PROTOCOL_VERSION_CUR;

			m = dgram_send(dgram, sendbuf, 3);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_COMPONENT_STATUS.SCCAppStatus: %d\n", sendbuf[1]);
			dbgprintf( "<- R_COMPONENT_STATUS.SCCAppVersion: %d\n", sendbuf[2]);
			dbgprintf( "--------------------\n");


			dbgprintf( "_________________________\n");
			input_is_active = 1;
			dbgprintf( "___input_inc_is_active___\n");
			dbgprintf( "_________________________\n");
			break;
		case SCC_INPUT_DEVICE_C_CONFIG_START_MSGID:
			if (n != 1) 
			{
				errprintf( "invalid msg size: 0x%02X %d\n", recvbuf[0], n);
				return ERROR;
			}
			dbgprintf( "-> C_CONFIG_START\n");
			dbgprintf( "--------------------\n");

			/********************** Config Start *************************/
			sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_START_MSGID;
			sendbuf[1] = 3;  					/*NumDevices*/
			m = dgram_send(dgram, sendbuf, 2);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_CONFIG_START.NumDevices: %d\n", sendbuf[1]);
			dbgprintf( "--------------------\n");
			
			/**************************** Key device, Dev.ID = 0 START*********************************/
			sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
			sendbuf[1] = 0; 					/*Dev.ID*/
			*(unsigned short *) (sendbuf + 2) = EV_KEY; 		/*Type*/
			sendbuf[4] = keycnt; 					/*NumCodes*/
			
			for(cnt=5;cnt<(5+(keycnt*2));cnt=cnt+2)
			{
				*(unsigned short *) (sendbuf + cnt)  = Key[i];
				dbgprintf( "Count:%d,Index:%d,Keycode:%d\n", cnt, i, Key[i]);
				i++;
			}
#ifndef VARIANT_S_FTR_ENABLE_CANDERAADAPTER
			*(unsigned short *) (sendbuf + cnt) = BTN_LEFT;
			cnt=cnt+2;
			keycnt=keycnt+1;					/* keys from config + BTN_LEFT(272) */
			sendbuf[4] = keycnt; 					/*NumCodes*/
#endif
			m = dgram_send(dgram, sendbuf, cnt);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
			dbgprintf( "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
			for(cnt=5; cnt<(5+(keycnt*2)); cnt=cnt+2)
			{
				dbgprintf( "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + cnt));
			}

			dbgprintf( "--------------------\n");

			/**************************** Key device, Dev.ID = 0 END*********************************/

			/**************************** 3 Encoder, Dev.ID = 1 START*********************************/
			sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
			sendbuf[1] = 1; 					/*Dev.ID*/
			*(unsigned short *) (sendbuf + 2) = EV_REL; 		/*Type*/
			sendbuf[4] = 4; 					/*NumCodes*/
			*(unsigned short *) (sendbuf + 5)  = REL_HWHEEL; 		/*Code*/
			*(unsigned short *) (sendbuf + 7)  = REL_DIAL; 		/*Code*/
			*(unsigned short *) (sendbuf + 9)  = REL_WHEEL; 		/*Code*/
			*(unsigned short *) (sendbuf + 11)  = REL_MISC; 		/*Code*/
			m = dgram_send(dgram, sendbuf, 13);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
			dbgprintf( "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 5));
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 7));
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 9));
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 11));
			dbgprintf( "--------------------\n");

			/**************************** 3 Encoder, Dev.ID = 1 END*********************************/

			/************************* Touch device Key, Dev.ID = 2 START****************************/
			sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
			sendbuf[1] = 2; 					/*Dev.ID*/
			*(unsigned short *) (sendbuf + 2) = EV_KEY; 		/*Type*/
			sendbuf[4] = 1; 					/*NumCodes*/
			*(unsigned short *) (sendbuf + 5)  = BTN_TOUCH; 	/*Code*/
			m = dgram_send(dgram, sendbuf, 7);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
			dbgprintf( "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 5));
			dbgprintf( "--------------------\n");

			/************************* Touch device Abs, Dev.ID = 2 ****************************/
			sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_TYPE_MSGID;
			sendbuf[1] = 2; 					/*Dev.ID*/
			*(unsigned short *) (sendbuf + 2) = EV_ABS; 		/*Type*/
			sendbuf[4] = 2; 					/*NumCodes*/
			*(unsigned short *) (sendbuf + 5)  = ABS_X; 		/*Code*/
			*(unsigned short *) (sendbuf + 7)  = ABS_Y; 		/*Code*/
			m = dgram_send(dgram, sendbuf, 9);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_CONFIG_TYPE.Dev.ID: %d\n", sendbuf[1]);
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Type: %d\n", *(unsigned short *) (sendbuf + 2));
			dbgprintf( "<- R_CONFIG_TYPE.Dev.NumCodes: %d\n", sendbuf[4]);
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 5));
			dbgprintf( "<- R_CONFIG_TYPE.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 7));
			dbgprintf( "--------------------\n");

			/********************** Touch device Abs, ABS_X Dev.ID = 2 *************************/
			sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_ABS_MSGID;
			sendbuf[1] = 2; 					/*Dev.ID*/
			*(unsigned short *) (sendbuf + 2)  = ABS_X; 		/*Type*/
			*(signed int *)     (sendbuf + 4)  = 0; 		/*Min*/
			*(signed int *)     (sendbuf + 8)  = max_x;		/*Max*/
			*(signed int *)     (sendbuf + 12) = 0; 		/*Fuzz*/
			*(signed int *)     (sendbuf + 16) = 0; 		/*Flat*/
			m = dgram_send(dgram, sendbuf, 20);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_CONFIG_ABS.Dev.ID: %d\n", sendbuf[1]);
			dbgprintf( "<- R_CONFIG_ABS.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 2));
			dbgprintf( "<- R_CONFIG_ABS.Dev.Min: %d\n", *(signed int *) (sendbuf + 4));
			dbgprintf( "<- R_CONFIG_ABS.Dev.Max: %d\n", *(signed int *) (sendbuf + 8));
			dbgprintf( "<- R_CONFIG_ABS.Dev.Fuzz: %d\n", *(signed int *) (sendbuf + 12));
			dbgprintf( "<- R_CONFIG_ABS.Dev.Flat: %d\n", *(signed int *) (sendbuf + 16));
			dbgprintf( "--------------------\n");

			/********************** Touch device Abs, ABS_Y Dev.ID = 2 *************************/
			sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_ABS_MSGID;
			sendbuf[1] = 2; 					/*Dev.ID*/
			*(unsigned short *) (sendbuf + 2)  = ABS_Y; 		/*Type*/
			*(signed int *)     (sendbuf + 4)  = 0; 	/*Min*/
			*(signed int *)     (sendbuf + 8)  = max_y; 		/*Max*/
			*(signed int *)     (sendbuf + 12) = 0; 		/*Fuzz*/
			*(signed int *)     (sendbuf + 16) = 0; 		/*Flat*/
			m = dgram_send(dgram, sendbuf, 20);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_CONFIG_ABS.Dev.ID: %d\n", sendbuf[1]);
			dbgprintf( "<- R_CONFIG_ABS.Dev.Code: %d\n", *(unsigned short *) (sendbuf + 2));
			dbgprintf( "<- R_CONFIG_ABS.Dev.Min: %d\n", *(signed int *) (sendbuf + 4));
			dbgprintf( "<- R_CONFIG_ABS.Dev.Max: %d\n", *(signed int *) (sendbuf + 8));
			dbgprintf( "<- R_CONFIG_ABS.Dev.Fuzz: %d\n", *(signed int *) (sendbuf + 12));
			dbgprintf( "<- R_CONFIG_ABS.Dev.Flat: %d\n", *(signed int *) (sendbuf + 16));
			dbgprintf( "--------------------\n");
			/************************* Dev.ID = 2 END****************************/
			/********************** Config End *************************/
			sendbuf[0] = SCC_INPUT_DEVICE_R_CONFIG_END_MSGID;
			m = dgram_send(dgram, sendbuf, 1);
			if (m == -1)
			{
				int errsv = errno;
				errprintf( "send failed: %d\n", errsv);
				return ERROR;
			}
			dbgprintf( "<- R_CONFIG_END\n");
			dbgprintf( "--------------------\n\n");

			break;

		default:
			errprintf( "invalid msgid: 0x%02X\n", recvbuf[0]);
			return ERROR;
		}

	} while (n > 0);

	ret = dgram_exit(dgram);
	if (ret < 0)
	{
		int errsv = errno;
		errprintf( "dgram_exit failed: %d\n", errsv);
		return ERROR;
	}

	close(fd);

	return SUCCESS;
}

#ifdef __cplusplus
}
#endif
