/************************************************************************
| FILE:         dgram_service.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Datagram service to be used on TCP stream socket.
|
|               Written data is prepended with a header which is used to find
|               message boundaries on receive.
|
|               Each call to dgram_send will result in one call to dgram_recv
|               returning.
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 15.01.13  | Initial revision           | Andreas Pape
| 21.02.13  | Fix cpp compiler issues    | Matthias Thomae
| 06.02.13  | Move functions to .c file  | Matthias Thomae
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>

#include "dgram_service.h"

#ifdef __cplusplus
extern "C" {
#endif

#define DEBUG_PRINT

#ifdef DEBUG_PRINT
	#define dbgprintf(fmt,...) printf("dgram: "fmt,##__VA_ARGS__)
#else
	#define dbgprintf(fmt,...)
#endif

#define errprintf(fmt,...) printf("dgram: "fmt,##__VA_ARGS__)


#ifndef AF_INC
#define AF_INC 41
#endif

#define UNUSED_VARIABLE(VAR) VAR

/*
init datagram servive on given socket.
options: optional - not yet used.
*/
sk_dgram* dgram_init(int sk, int dgram_max, void *options)
{
   sk_dgram *skd;
   struct sockaddr_in addr;
   socklen_t len = sizeof(struct sockaddr);
   if(getsockname(sk, (struct sockaddr *)&addr, &len)<0)
      return NULL;
   dbgprintf("dgram init on socket af %d\n",addr.sin_family);

   if(dgram_max > (int) DGRAM_MAX) { /* satisfy lint */
      errprintf("exceed maxsize (%d>%ld)\n",dgram_max, DGRAM_MAX);
      return NULL;
   }
   switch(addr.sin_family) {
      case AF_INET:
      case AF_INC:
         break;
      default:
         errprintf("not supported on AF %d\n",addr.sin_family);
         return NULL;
   }

   skd = (sk_dgram *) malloc(sizeof(sk_dgram));
   if(!skd) {
      errprintf("failed allocating memory\n");
      return NULL;
   }
   memset(skd, 0,sizeof(*skd));
   skd->proto = addr.sin_family;
   skd->hlen = sizeof(dgram_header_std);
   skd->len = dgram_max;
   skd->buf = (char *) malloc(skd->len+skd->hlen);

   if(!skd->buf) {
      errprintf("failed allocating rcv buffer\n");
      free(skd);
      return NULL;
   }
   skd->sk = sk;
   /*ignore options*/
   UNUSED_VARIABLE(options);

   return skd;
}


int dgram_exit(sk_dgram *skd)
{
   if(!skd) {
      errprintf("invalid handle\n");
      errno = EINVAL;
      return -1;
   }
   free(skd->buf);
   skd->buf = NULL;
   free(skd);
   return 0;
}


/*send message as datagram
-just prepend header
*/
int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen)
{
   struct msghdr msg;
   struct iovec iov[2];
   dgram_header_std h;
   int ret;

   if(skd->proto != AF_INET)
      return send(skd->sk, ubuf, ulen, 0);

   if((int) ulen > skd->len) { /* satisfy lint */
      errprintf("send: dgram exceeds bufsize (%ld>%d)\n", ulen, skd->len);
      errno = EMSGSIZE;
      return -1;
   }
   memset(&msg, 0, sizeof(msg));
   msg.msg_iovlen = 2;
   msg.msg_iov = iov;
   h.dglen = htons(ulen);
   iov[0].iov_base = &h;
   iov[0].iov_len = sizeof(h);
   iov[1].iov_base = ubuf;
   iov[1].iov_len = ulen;
   ret = sendmsg(skd->sk, &msg, 0);
   if(ret >= (int) sizeof(h)) /* satisfy lint */
      ret -= sizeof(h);
   return ret;
}


/*receive datagram*/
int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen)
{
   int err;

   if(skd->proto != AF_INET)
      return recv(skd->sk, ubuf, ulen, 0);


   if((skd->received < skd->hlen) || ((skd->received >= skd->hlen) && (skd->received < (skd->h.dglen+skd->hlen)))){
      err = recv(skd->sk, skd->buf+skd->received, (skd->len + skd->hlen)-skd->received, 0);
      if(err <= 0) {
         dbgprintf("receive failed with err %d %d \n",err, errno);
         return err;
      }
      skd->received += err;
   }

   if(skd->received < skd->hlen){
      errno = EAGAIN;
      return -1;
   }

   /*complete header available*/
   memcpy((char*)&skd->h, skd->buf, skd->hlen);
   skd->h.dglen = ntohs(skd->h.dglen);

   if(skd->received < (skd->h.dglen+skd->hlen )) {
      errno = EAGAIN;
      return -1;
   }

   /*full dgram available*/
   if(ulen < skd->h.dglen) {
      errprintf("recv: dgram exceeds bufsize (%d>%ld)\n", skd->h.dglen, ulen);
      errno = EMSGSIZE;
      return -1;
   }

   memcpy(ubuf, skd->buf+skd->hlen, skd->h.dglen);
   skd->received-= (skd->h.dglen+skd->hlen);
   err = skd->h.dglen;
   if(skd->received > 0 ) {
      memmove(skd->buf, skd->buf+skd->hlen+skd->h.dglen, skd->received);
      if(skd->received >= skd->hlen){
         memcpy((char*)&skd->h, skd->buf, skd->hlen);
         skd->h.dglen = ntohs(skd->h.dglen);
      }
   }
   dbgprintf("finished DGRAM of len %d\n", err);
   return err;
}

#ifdef __cplusplus
}
#endif
