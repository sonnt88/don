/******************************************************************************
 * FILE              : facia_xml_parser.cpp
 *
 * SW-COMPONENT      : LSIM
 *
 * DESCRIPTION       : XML parser for facia
 *
 * AUTHOR(s)         : Sudharsanan Sivagnanam (RBEI/ECF5)
 *
 * HISTORY      :
 *-----------------------------------------------------------------------------
 * Date           |       Version        | Author & comments
 *-------- --|---------------|-------------------------------------------------
 * 04.Oct.2013 |  Initialversion 1.0 | Sudharsanan Sivagnanam (RBEI/ECF5)
 * -----------------------------------------------------------------------------
 *******************************************************************************/

/************************************************************************
 | Header file declaration
 |-----------------------------------------------------------------------*/

#ifdef TIXML_USE_STL
#include <iostream>
#include <sstream>
using namespace std;
#else
#include <stdio.h>
#endif

#if defined( WIN32 ) && defined( TUNE )
#include <crtdbg.h>
_CrtMemState startMemState;
_CrtMemState endMemState;
#endif

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <tinyxml.h>
#include "facia_xml_parser.h"

TiXmlDocument doc;
TiXmlElement* root;

/*************************************************************************************
 * FUNCTION      : GetAttribute()

 * PARAMETER     : TiXmlElement* Element,const char* Name

 * RETURNVALUE   : value of the attribute

 * DESCRIPTION   :This function gets value of the attribute

 * HISTORY       : 19.Sep.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
 ***************************************************************************************/

const char* GetAttribute(TiXmlElement* Element, const char* Name) {

	const char* charptr;

	charptr = Element->Attribute(Name);

	if (charptr == NULL) {
		dbgprintf("%s info not found\n", Name);
		//charptr="No";
	} else {
		//dbgprintf("%s\n",charptr);
	}

	return charptr;
}

/*************************************************************************************
 * FUNCTION      : GetFaciaTouchRegion()

 * PARAMETER     : Keymap* Region, Resolution* size

 * RETURNVALUE   : -1 on error, 0 on success

 * DESCRIPTION   : parse touch screen region info

 * HISTORY       : 19.Sep.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
 ***************************************************************************************/
int GetFaciaTouchRegion(Keymap* Region, Resolution* size) {

	TiXmlElement* Bmp;
	const char* charptr;
	TiXmlElement* NextElement = 0;

	Bmp = root->FirstChildElement("Bitmap");

	if (Bmp == NULL) {
		dbgprintf("Error: No Bitmap child element. Exiting!\n");
		return ERROR;
	} else {
		charptr = Bmp->Value();
		dbgprintf("%s\n", charptr);
	}

	NextElement = Bmp->FirstChildElement("Region");

	Region->ButtonName = "TOUCH_SCREEN";
	Region->Block = 1;
	Region->Axis = -1;

	Region->ButtonPos.Left = (float) atoi(GetAttribute(NextElement, "Left"));
	Region->ButtonPos.Top = (float) atoi(GetAttribute(NextElement, "Top"));
	Region->ButtonPos.Right = (float) atoi(GetAttribute(NextElement, "Right"));
	Region->ButtonPos.Bottom = (float) atoi(
			GetAttribute(NextElement, "Bottom"));

	NextElement = Bmp->FirstChildElement("DisplayResolution");

	size->width = (unsigned int) atoi(NextElement->Attribute("Width"));
	size->height = atoi(NextElement->Attribute("Height"));

	return SUCCESS;

}

/*************************************************************************************
 * FUNCTION      : GetNumberOfBlocks()

 * PARAMETER     : None

 * RETURNVALUE   : Number of blocks

 * DESCRIPTION   :check number of block available in facia xml file

 * HISTORY       : 19.Sep.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
 ***************************************************************************************/

int GetNumberOfBlocks() {

	TiXmlElement* KeymapElement = 0;
	TiXmlElement* FaciaKeymap;
	int BlockCnt = 0;

	FaciaKeymap = root->FirstChildElement("Keymap");

	if (FaciaKeymap == NULL) {
		dbgprintf("Error: No Keymap child element. Exiting!\n");
		return ERROR;
	}

	for (KeymapElement = FaciaKeymap->FirstChildElement(); KeymapElement;
			KeymapElement = KeymapElement->NextSiblingElement()) {

		BlockCnt++;

	}

	dbgprintf("Number of blocks:%d\n", BlockCnt);

	return BlockCnt;

}

/*************************************************************************************
 * FUNCTION      : GetKeymapInfo()

 * PARAMETER     : Keymap*

 * RETURNVALUE   : value of the attribute

 * DESCRIPTION   :parse keymap info

 * HISTORY       : 19.Sep.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
 ***************************************************************************************/
int GetKeymapInfo(Keymap* KeyInfoPtr) {

	TiXmlElement* KeymapElement = 0;
	TiXmlElement* NextElement = 0;
	const char* charptr;
	Keypos* Buttposptr;
	TiXmlElement* FaciaKeymap;
	int BlockCnt = 2;

	FaciaKeymap = root->FirstChildElement("Keymap");

	if (FaciaKeymap == NULL) {
		dbgprintf("Error: No Keymap child element found. Exiting!\n");
		return ERROR;
	}

	for (KeymapElement = FaciaKeymap->FirstChildElement(); KeymapElement;
			KeymapElement = KeymapElement->NextSiblingElement()) {

		charptr = KeymapElement->Attribute("Name");

		if (charptr) {
			dbgprintf("Working on button name: %s\n", charptr);
		} else {
			dbgprintf("Error: Key Name not found. Exiting!\n");
			return ERROR;
		}

		KeyInfoPtr->ButtonName = charptr;

		charptr = GetAttribute(KeymapElement, "Axis");

		if (charptr != NULL) {
			KeyInfoPtr->Axis = atoi(charptr);
		} else {
			KeyInfoPtr->Axis = -1;
		}

		if ((charptr = KeymapElement->Attribute("KeyCode")) != NULL) {
			if (*(charptr + 1) == 'x') {
				KeyInfoPtr->KeyCode = (int) strtol(charptr, NULL, 16);
			} else {
				KeyInfoPtr->KeyCode = atoi(charptr);
			}
			if (KeyInfoPtr->KeyCode > 0xffffu) {
				dbgprintf(
						"Error: KeyCode is bigger as 16 bit value! This can't be handled by facia yet. Exiting!\n");
				return ERROR;
			}
		} else {
			dbgprintf(
					"Error: Attribute KeyCode is not available for:%s. Exiting!\n",
					KeyInfoPtr->ButtonName);
			//KeyInfoPtr->KeyCode = 0;
			return ERROR;

		}
		if((charptr = GetAttribute(KeymapElement, "IsPower")) != NULL){ 
			KeyInfoPtr->IsPower = (int) strtol(charptr, NULL, 16);
		}

		else if ((charptr = GetAttribute(KeymapElement, "Key")) != NULL) {
			KeyInfoPtr->Key = (int) strtol(charptr, NULL, 16);
		} else {
			dbgprintf("Error: Key is not available for:%s. Exiting!\n",
					KeyInfoPtr->ButtonName);
			//KeyInfoPtr->Key="\0";
			return ERROR;
		}

		Buttposptr = (Keypos*) &KeyInfoPtr->ButtonPos;
		NextElement = KeymapElement->FirstChildElement();

		Buttposptr->Left = (float) atoi(GetAttribute(NextElement, "Left"));
		Buttposptr->Top = (float) atoi(GetAttribute(NextElement, "Top"));

		charptr = NextElement->Attribute("Right");
		if (charptr != 0) {
			Buttposptr->Right = (float) atoi(charptr);
		} else {
			charptr = NextElement->Attribute("Width");
			if (charptr != 0) {
				Buttposptr->Right = ((float) atoi(charptr) + Buttposptr->Left);
				//dbgprintf("***Right:%f***\n",Buttposptr->Right);
			} else {
				dbgprintf(
						"Error: Neither attribute Right nor Width was found. Exiting!\n");
				return ERROR;
			}
		}
		charptr = NextElement->Attribute("Bottom");
		if (charptr != 0) {
			Buttposptr->Bottom = (float) atoi(charptr);
		} else {
			charptr = NextElement->Attribute("Height");
			if (charptr) {
				Buttposptr->Bottom = ((float) atoi(charptr) + Buttposptr->Top);
				//dbgprintf("***Bottom:%f***\n",Buttposptr->Bottom);
			} else {
				dbgprintf(
						"Error: Missing attribute Height to calculate Bottom. Exiting!");
				return ERROR;
			}
		}
		//Buttposptr->Right = (float)atoi(GetAttribute(NextElement,"Right"));
		//Buttposptr->Bottom = (float)atoi(GetAttribute(NextElement,"Bottom"));

		dbgprintf(
				"Name:%s,KeyCode:%d,Axis=%d,Key:%d,left:%f, top:%f, Right:%f, bottom:%f\n",
				KeyInfoPtr->ButtonName, KeyInfoPtr->KeyCode, KeyInfoPtr->Axis, KeyInfoPtr->Key, Buttposptr->Left, Buttposptr->Top, Buttposptr->Right, Buttposptr->Bottom);

		KeyInfoPtr->Block = BlockCnt;

		KeyInfoPtr++;
		BlockCnt++;

	}

	return SUCCESS;
}

/*************************************************************************************
 * FUNCTION      : GetBitMapInfo()

 * PARAMETER     : Bitmap* BitmapData

 * RETURNVALUE   : value of the attribute

 * DESCRIPTION   :This function gets value of the attribute

 * HISTORY       : 19.Sep.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
 ***************************************************************************************/
int GetBitMapInfo(Bitmap* BitmapData) {
	TiXmlElement* BitmapElem = 0;
	const char* charptr;
	TiXmlElement* Bmp;
	const char* colorinfo[] = { "TransparentColor", "ButtonMouseOverColor",
			"ButtonPressedColor", "ButtonLockedDownColor" };

	Bmp = root->FirstChildElement("Bitmap");
	if (Bmp == NULL) {
		dbgprintf("Error: Missing Element Bitmap. Exiting!\n");
		return ERROR;
	} else {
		charptr = Bmp->Value();
		dbgprintf("%s\n", charptr);
	}

	charptr = GetAttribute(Bmp, "Name");
	if (charptr == NULL) {
		dbgprintf("Error: Facia image file doesn't exist. Exiting!\n");
		return ERROR;
	}

	BitmapData->Name = charptr;
	dbgprintf("%s\n", charptr);

	for (BitmapElem = Bmp->FirstChildElement(); BitmapElem; BitmapElem =
			BitmapElem->NextSiblingElement()) {

		charptr = BitmapElem->Value();
		Colors* RgbPtr = &(BitmapData->transColor.Rgb);

		if (!strcmp(charptr, colorinfo[0])) {

			RgbPtr->Red = atoi(GetAttribute(BitmapElem, "Red"));
			RgbPtr->Green = atoi(GetAttribute(BitmapElem, "Green"));
			RgbPtr->Blue = atoi(GetAttribute(BitmapElem, "Blue"));
			BitmapData->transColor.Active = GetAttribute(BitmapElem, "Active");
			dbgprintf("transColor:Red:%d,green:%d,blue:%d,active:%s\n",
					RgbPtr->Red, RgbPtr->Green, RgbPtr->Blue, BitmapData->transColor.Active);
		}

		RgbPtr = &(BitmapData->ButtonMouseOverColor.Rgb);
		if (!strcmp(charptr, colorinfo[1])) {
			RgbPtr->Red = atoi(GetAttribute(BitmapElem, "Red"));
			RgbPtr->Green = atoi(GetAttribute(BitmapElem, "Green"));
			RgbPtr->Blue = atoi(GetAttribute(BitmapElem, "Blue"));
			BitmapData->ButtonMouseOverColor.Alpha = atoi(
					GetAttribute(BitmapElem, "Alpha"));
			dbgprintf(
					"ButtonMouseOverColor:Red:%d,green:%d,blue:%d,active:%d\n",
					RgbPtr->Red, RgbPtr->Green, RgbPtr->Blue, BitmapData->ButtonMouseOverColor.Alpha);
		}

		RgbPtr = &(BitmapData->ButtonPressedColor.Rgb);
		if (!strcmp(charptr, colorinfo[2])) {
			RgbPtr->Red = atoi(GetAttribute(BitmapElem, "Red"));
			RgbPtr->Green = atoi(GetAttribute(BitmapElem, "Green"));
			RgbPtr->Blue = atoi(GetAttribute(BitmapElem, "Blue"));
			BitmapData->ButtonPressedColor.Alpha = atoi(
					GetAttribute(BitmapElem, "Alpha"));
			dbgprintf(
					"ButtonMousePressedColor:Red:%d,green:%d,blue:%d,active:%d\n",
					RgbPtr->Red, RgbPtr->Green, RgbPtr->Blue, BitmapData->ButtonPressedColor.Alpha);
		}

		RgbPtr = &(BitmapData->ButtonLockedDownColor.Rgb);
		if (!strcmp(charptr, colorinfo[3])) {
			RgbPtr->Red = atoi(GetAttribute(BitmapElem, "Red"));
			RgbPtr->Green = atoi(GetAttribute(BitmapElem, "Green"));
			RgbPtr->Blue = atoi(GetAttribute(BitmapElem, "Blue"));
			BitmapData->ButtonLockedDownColor.Alpha = atoi(
					GetAttribute(BitmapElem, "Alpha"));
			dbgprintf(
					"ButtonMouseLockedDownColor:Red:%d,green:%d,blue:%d,active:%d\n",
					RgbPtr->Red, RgbPtr->Green, RgbPtr->Blue, BitmapData->ButtonLockedDownColor.Alpha);
		}

	}

	return SUCCESS;
}
/*************************************************************************************
 * FUNCTION      : LoadXML()

 * PARAMETER     : XMLFileName

 * RETURNVALUE   : -1 on error , 0 on success

 * DESCRIPTION   :Load the xml file

 * HISTORY       : 19.Sep.2012| Initial Version |Sudharsanan Sivagnanam (RBEI/ECF5)
 ***************************************************************************************/
int LoadXML(char* XMLFileName) {
	const char* Element;
	struct stat buffer;

	if (stat(XMLFileName, &buffer) != 0) {
		dbgprintf("Error: file does not exist. Exiting!\n");
		return ERROR;
	}

	if (!doc.LoadFile(XMLFileName)) {
		dbgprintf("Error: loading xml failed. Exiting!\n");
		return ERROR;
	}

	root = doc.FirstChildElement();
	if (root == NULL) {
		dbgprintf("Error: No root element. Exiting!\n");
		return ERROR;
	} else {
		Element = root->Value();
		dbgprintf("%s\n", Element);
	}

	return SUCCESS;
}

