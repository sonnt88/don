/******************************************************************************
* FILE              : facia_xml_parser.h
*
* SW-COMPONENT      : 
*
* DESCRIPTION       : Header file for LSIM xml parser
*                                      
* AUTHOR(s)         : Sudharsanan Sivagnanam (RBEI/ECF5)
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date          |       Version        | Author & comments
*-------- --|---------------|-------------------------------------------------
*                  |  Initialversion 1.0        | Sudharsanan Sivagnanam (RBEI/ECF5)
* -----------------------------------------------------------------------------
*******************************************************************************/
#ifndef _XMLPARSER_H_
#define _XMLPARSER_H_


#define ERROR -1
#define SUCCESS 0

#define DEBUG_PRINT

#ifdef DEBUG_PRINT
#define dbgprintf(fmt,...) printf("%s:"fmt,__func__,##__VA_ARGS__);fflush(stdout)
#else
#define dbgprintf(fmt,...)
#endif

#define errprintf(fmt,...) printf("%s:"fmt,__func__,##__VA_ARGS__);fflush(stdout)



typedef struct
{

	float Left;
	float Top;
	float Right;
	float Bottom;
}Keypos;

typedef struct
{

	int Red;
	int Green;
	int Blue;
}Colors;

typedef struct
{
	Colors Rgb;
	int    Alpha;
}Rgba;

typedef struct
{
	unsigned int    width;
	unsigned int    height;
}Resolution;


typedef struct
{
	Colors 		Rgb;
	const char* 	Active;
}TransparentColor;



typedef struct
{
	const char* 		Name;
	TransparentColor 	transColor;
	Rgba 			ButtonMouseOverColor;
	Rgba 			ButtonPressedColor;
	Rgba 			ButtonLockedDownColor;
}Bitmap;

typedef struct
{
	const char* 	ButtonName;
	int 	        Key;
	int 	        KeyCode;
	int             Axis;
	int             Block;
	Keypos 		ButtonPos;
	int 			IsPower; 
}Keymap;



int LoadXML(char* XMLFileName);
int GetBitMapInfo(Bitmap* BitmapData);
int GetKeymapInfo(Keymap* KeyInfoPtr);
int GetNumberOfBlocks();
int GetFaciaTouchRegion(Keymap* Region, Resolution* size);




#endif






