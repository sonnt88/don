#!/bin/sh
# this script prepares the LSIM environment

LOGFILE="/tmp/start_facia.log"

echo "$0 begins" > $LOGFILE

#Start Screenbroker/Layermanager
#systemctl start screenbroker.service

sleep 1

#Disable console blink to have a clean Facia startup
echo 0 > /sys/class/graphics/fbcon/cursor_blink
echo "$0 Starting input_event_filter_out ..." >> $LOGFILE
/opt/bosch/base/lsim/input_event_filter_out.out 2>&1 | /opt/bosch/base/bin/stdout_to_ttfis_out.out -c97d1 &
sleep 1

echo "$0 Modprobe dev-fake ..." >> $LOGFILE
modprobe dev-fake
sleep 1

ifup inc-fake0
ifup inc-fake1

echo "$0 Modprobe inc-proto-ssi ..." >> $LOGFILE
modprobe inc-proto-ssi

echo "add much stuff to inc/allowed_connections" >> $LOGFILE
for i in {50944..51195}
do 
    echo 192.168.203.002:$i 000.000.000.000:00000 >> /proc/sys/net/inc/allowed_connections
done

for i in {50944..51195}
do 
    echo 192.168.204.002:$i 000.000.000.000:00000 >> /proc/sys/net/inc/allowed_connections
done

for i in {50944..51195}
do 
    echo 192.168.223.002:$i 000.000.000.000:00000 >> /proc/sys/net/inc/allowed_connections
done

for i in {50944..51195}
do 
    echo 192.168.224.002:$i 000.000.000.000:00000 >> /proc/sys/net/inc/allowed_connections
done

echo "$0 Starting inc_event_constructor_out.out /opt/bosch/base/lsim/$xml ..." >> $LOGFILE
/opt/bosch/base/lsim/inc_event_constructor_out.out /opt/bosch/base/lsim/$xml 2>&1 | /opt/bosch/base/bin/stdout_to_ttfis_out.out -c97d2 &

sleep 1

echo "$0 Modprobe input_inc ..." >> $LOGFILE
modprobe input_inc

export XDG_RUNTIME_DIR=/tmp

echo "$0 Starting facia with \"$xml\" configuration" >> $LOGFILE
cd /opt/bosch/base/lsim/
/opt/bosch/base/lsim/facia_wl_egl_gui_out.out /opt/bosch/base/lsim/$xml /opt/bosch/base/lsim/facia-"$VARIANT".bmp 2>&1 | /opt/bosch/base/bin/stdout_to_ttfis_out.out -c97d3 &

# hem2hi now done via systemd
#echo "$0 Starting procbase ..." >> $LOGFILE
#systemctl start rbcm-procbase.service

echo "$0 Stopping TTY console..."
systemctl stop tty-console.service

echo "$0 Finished." >> $LOGFILE

#send log to ttfis
cat $LOGFILE | /opt/bosch/base/bin/stdout_to_ttfis_out.out -c97d0
