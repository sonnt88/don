/******************************************************************************
* FILE              : facia_main.cpp
*
* SW-COMPONENT      : lsim
*
* DESCRIPTION      : This implementation provides the GUI support for LSIM Facia.
*
* HISTORY      :
*-----------------------------------------------------------------------------
* Date           |       Version        | Author & comments
*-------- --|---------------|-------------------------------------------------
* 02.09.2013|   Initial version    |Sudharsanan Sivagnanam (RBEI/ECF5)
* -----------------------------------------------------------------------------
* 19.07.2016|   Version 1.0 | Changes required w.r.t candera adaptor | sja3kor
*******************************************************************************/

#include "ilm_client.h"
#include "facia_egl_helper.h"
#include "facia_gles2_app.h"
#include "facia_xml_parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "facia_fake_wl_input_event.h"
#include <signal.h>
#include <semaphore.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <tinyxml.h>
#define CLOCKID CLOCK_REALTIME
#define SIG SIGRTMIN
#define FACIA_CONTEXT  0
#define CURSOR_CONTEXT 1
#define MAX_LAYER_IDS 100
#define FACIA_LAYER_ID 10000
#define MOUSE_CURSOR_LAYER_ID 4

extern pthread_t curserTid;
extern pthread_t faciaDrowTid;
//CMG3GB-1756 : Flags and Semaphores for protecting critical sections
extern bool CursorReadyFlag;
extern bool FaciaReadyFlag;
extern sem_t CursorReadyMutex;
extern sem_t FaciaReadyMutex;
pthread_t faciaPositionTid;
/********************* DEFINES **********************************************/

t_ilm_uint screenwidth;
t_ilm_uint screenheight;

/********************************************************************************
* FUNCTION        : vSetPositionThread()

* PARAMETER       :   None

* RETURNVALUE   : 0 on success


* DESCRIPTION   : Set the offset position
**********************************************************************************/
#ifdef  VARIANT_S_FTR_ENABLE_SINGLE_SCREENLAYOUTS
int GetTotalLayersFromScreenLayouts(char* XMLFileName)
{
	dbgprintf("===== ScreenLayouts File: %s\n", XMLFileName);

	TiXmlDocument *docScreenLayout = new TiXmlDocument();
	if (!docScreenLayout->LoadFile(XMLFileName))
	{
		dbgprintf("Error: loading ScreenLayouts xml failed. Exiting!\n");
		return 0;
	}

	int counter = 0;
	TiXmlElement* rootScreenLayout = docScreenLayout->FirstChildElement();
	TiXmlElement* displayEle = rootScreenLayout->FirstChildElement("ScreenLayout")->FirstChildElement("Displays")->FirstChildElement("Display");
	TiXmlElement* KeymapElement = 0;

	if (displayEle == NULL) {
		dbgprintf("Error: No Keymap child element found. Exiting!\n");
		return 0;
	}
	for (KeymapElement = displayEle->FirstChildElement(); KeymapElement;
		KeymapElement = KeymapElement->NextSiblingElement())
	{
		counter++;
	}

	return counter;
}

int updateLayersPosition(t_ilm_layer* pLayerID, t_ilm_int NumberLayerIDs)
{
	// greater than 2 because facia(10000) and mouse(4) applications are created before this
	Keymap TouchRegionOffset;
	Resolution TouchRegionSize;
	t_ilm_uint pos[2];
	dbgprintf("ilm_getLayerIDs succeeded \n");
	GetFaciaTouchRegion(&TouchRegionOffset,&TouchRegionSize);
	//pos[0] = (int)TouchRegionOffset.ButtonPos.Left;
	//pos[1] = (int)TouchRegionOffset.ButtonPos.Top;
	dbgprintf("TouchRegionOffset:(%d,%d)",pos[0],pos[1]);
	int changedLayerNumber = 0;

	for(int i = 0 ; i<NumberLayerIDs; ++i)
	{
		dbgprintf("ilm_surfaceSetPosition LayerID[%d] = %d \n",i,*pLayerID);
		if(*pLayerID != FACIA_LAYER_ID && *pLayerID != MOUSE_CURSOR_LAYER_ID)   // Layer repositioning not required for FACIA and CURSOR layers
		{
			//ilmErrorTypes GetLayerInfoError =  ilm_layerGetPosition(*pLayerID,pos);
			pos[0] = (int)TouchRegionOffset.ButtonPos.Left;
			pos[1] = (int)TouchRegionOffset.ButtonPos.Top;
			ilmLayerProperties pLayerProperties;
			ilmErrorTypes GetLayerInfoError = ilm_getPropertiesOfLayer(*pLayerID,&pLayerProperties);
			bool needUpdate = true;
			if(GetLayerInfoError == 0)
			{
				//dbgprintf("ilm_getPropertiesOfLayer: LayerID=%d - (%d,%d) \n",*pLayerID, pLayerProperties.destX,pLayerProperties.destX);
				pos[0] += pLayerProperties.destX;
				pos[1] += pLayerProperties.destY;
			}
			if(ilm_layerSetPosition(*pLayerID,pos) == 0)
			{
				changedLayerNumber++;
				//dbgprintf("ilm_layerSetPosition Layer ID = %d \n",*pLayerID);
			}
		}
		(pLayerID)++;
	}
	ilm_commitChanges();
	dbgprintf("Finished Repositioning Layers");
	return changedLayerNumber;
}

void* vSetPositionThread(void *vArg)
{
	dbgprintf("vSetPositionThread starting......\n");
	bool bIsFinishSetPos = false;
	t_ilm_int NumberLayerIDs=0;
	t_ilm_layer LayerID[MAX_LAYER_IDS];
	t_ilm_layer* pLayerID = LayerID;
	ilmErrorTypes Error;
	int changedLayers = 0;

	int totalLayers = GetTotalLayersFromScreenLayouts((char*)"/opt/bosch/hmibase/ScreenLayouts.xml");

	dbgprintf("Total Layers From ScreenLayouts: %d\n", totalLayers);

	if(totalLayers > 0)
	{
		bool isFinished = false;
		while(!isFinished)
		{
			Error = ilm_getLayerIDs(&NumberLayerIDs,&pLayerID);
			if(Error == 0 && NumberLayerIDs > 0)
			{
				dbgprintf(" Loop SetPosition 1\n");
				if(NumberLayerIDs >= totalLayers)
				{
					dbgprintf(" Loop SetPosition 2\n");
					changedLayers += updateLayersPosition(pLayerID+changedLayers,NumberLayerIDs-changedLayers);
					isFinished = true;
				}
				else
				{
					if(changedLayers != NumberLayerIDs)
					{
						dbgprintf(" Loop SetPosition 3 --- changedLayers=%d vs NumberLayerIDs=%d\n",changedLayers,NumberLayerIDs );
						changedLayers += updateLayersPosition(pLayerID+changedLayers,NumberLayerIDs-changedLayers);
					}
					else
					{
						dbgprintf(" Loop SetPosition 4 --- NumberLayerIDs=%d\n", NumberLayerIDs);
						isFinished = true;
					}
				}
			}
			sleep(1);
		}
	}
	else {
		dbgprintf("Waiting 5s because of failed getting number of layers\n", totalLayers);
		sleep(5); // for waiting 5s in case error from screenlayouts
		Error = ilm_getLayerIDs(&NumberLayerIDs,&pLayerID);
		if(Error == 0)
		{
			updateLayersPosition(pLayerID,NumberLayerIDs);
		}
	}
	dbgprintf("Facia drawn,setting position of Layers");


	return NULL;
}
#endif
/********************************************************************************
* FUNCTION        : main() 

* PARAMETER       :   None
																		
* RETURNVALUE   : 0 on success
*                 -1 on failure 

* DESCRIPTION   :Main function for facia_wl_egl_gui
**********************************************************************************/
void* Cursor_gui_thread(void *arg)
{

	dbgprintf("cursor_gui_thread\n");
	//CMG3GB-1756 : Initialize the semaphores, release of the semaphore will happen in Draw_cursor()
	sem_init(&CursorReadyMutex,0,1);
#ifndef VARIANT_S_FTR_ENABLE_CANDERAADAPTER	
	sem_wait(&CursorReadyMutex);
#endif	

    curserTid = pthread_self();
	if (!createWLContext(screenwidth, screenheight,CURSOR_CONTEXT))
	{
		errprintf("Can't Create Wayland Context\n");
		 exit(1);
		
	}

	if (!createEGLContext(screenwidth, screenheight,CURSOR_CONTEXT))
	{
		errprintf("Can't Create EGL Context\n");
		 exit(1);
	}

	
	InitCursorGlApplication();
	swapBuffers(CURSOR_CONTEXT);
	Draw_cursor();

	destroyEglContext(CURSOR_CONTEXT);
	destroyWLContext(CURSOR_CONTEXT);
	DestroyCursorGlApplication();

	 exit(0);

}


/********************************************************************************
* FUNCTION        : main() 

* PARAMETER       :   None
																		
* RETURNVALUE   : 0 on success
*                          -1 on failure 

* DESCRIPTION   :Main function for facia_wl_egl_gui
**********************************************************************************/
int main(int Argc,char* Argv[])
{
	//CMG3GB-1756 : Initialize FaciaReadyMutex and the flags
	CursorReadyFlag = false;
	FaciaReadyFlag = false;
	sem_init(&FaciaReadyMutex,0,1);
	pthread_t thread;
	Resolution Bmpsize;
	Bitmap bmpname;

	if(Argc > 1)
	{
		if(LoadXML(Argv[1]) == SUCCESS)
		{
			dbgprintf("load xml file success\n");
		}
		else
		{
			errprintf("ERROR: load xml file not successful\n");
			return ERROR;
		}
	}
	else
	{
		if(LoadXML((char*)"/opt/bosch/base/lsim/facia_configuration.xml"))
		{
			errprintf("load xml file failed\n");
			return ERROR;
		}
	}
	
	if (Argv[2] == NULL)
	{
		if (GetBMPinfo(NULL)== ERROR)
		{
			dbgprintf("GetBMPinfo without path failed\n");
			return ERROR;
		}
	}
	else
	{
		if (GetBMPinfo(Argv[2])== ERROR)
		{
			dbgprintf("GetBMPinfo with path failed\n");
			return ERROR;
		}
	}

	GetImagesize((Resolution*)&Bmpsize);

	dbgprintf("Image resolution w:%d,H:%d\n",Bmpsize.width,Bmpsize.height);
	
	faciaDrowTid = pthread_self();

	if (ilm_init() == ILM_FAILED)
	{
		errprintf("Can't Init LayerManagement Communication\n");
		return ERROR;
	}

	ilm_getScreenResolution (0, &screenwidth, &screenheight);
	dbgprintf("LSIM resolution w:%d,H:%d\n",screenwidth,screenheight);

	if((screenwidth < Bmpsize.width) || (screenheight < Bmpsize.height))
	{
		errprintf("Error Image size is bigger than the LSIM resolution\n");
		return ERROR;
	}


	if (!createWLContext(Bmpsize.width,Bmpsize.height,FACIA_CONTEXT))
	{
		errprintf("Can't Create Wayland Context\n");
		return ERROR;
	}

	if (!createEGLContext(Bmpsize.width,Bmpsize.height,FACIA_CONTEXT))
	{
		errprintf("Can't Create EGL Context\n");
		return ERROR;
	}


	if (lsim_inc_filter_init(screenwidth,screenheight)) {
		errprintf("Inc filter initialization failed!\n");
		return ERROR;
	}

	if (fake_wl_input_event_init())
	{
		errprintf("fake_wl_input_event_init failed\n");
		return ERROR;
	}

#ifndef VARIANT_S_FTR_ENABLE_CANDERAADAPTER
	if (pthread_create(&thread, NULL, Cursor_gui_thread, NULL)) {
		errprintf("fake gui thread creation failed!");
		return ERROR;
	}
	else
	{

		dbgprintf("cursor thread success\n");

	}


	//CMG3GB-1756 : Facia thread needs to wait till facia is initialized, so guarding it with critical section
	if(FaciaReadyFlag == false)
		sem_wait(&FaciaReadyMutex);
#endif

	InitFaciaGlApplication();
	Draw_facia_image();
	
//Ticket AIVI-4311 
#ifdef  VARIANT_S_FTR_ENABLE_SINGLE_SCREENLAYOUTS
	dbgprintf(" In RNAIVI we create a thread to set offset separately \n");
	if (pthread_create(&faciaPositionTid, NULL, vSetPositionThread, NULL)) {
		errprintf("Set Position thread creation failed!");
		return ERROR;
	}
	else
	{

		dbgprintf("Set Position thread success\n");
	}
#endif

	Draw_facia();
#ifndef VARIANT_S_FTR_ENABLE_CANDERAADAPTER	
	if(FaciaReadyFlag == false)
		sem_post(&FaciaReadyMutex);	
#endif	
	destroyEglContext(FACIA_CONTEXT);
	destroyWLContext(FACIA_CONTEXT);
	DestroyFaciaGlApplication();

	ilm_destroy();
	sem_destroy(&FaciaReadyMutex);
	sem_destroy(&CursorReadyMutex);

	return SUCCESS;
}


