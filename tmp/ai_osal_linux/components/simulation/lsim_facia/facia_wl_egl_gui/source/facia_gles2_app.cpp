/******************************************************************************
* FILE               : facia_gles2_app.cpp
*
* SW-COMPONENT       : lsim
*
* DESCRIPTION        : This file contains implementation of facia GUI part.
*
* HISTORY            :
*-----------------------------------------------------------------------------
* Date           |       Version        | Author & comments
*-----------|---------------|--------------------------------------------
*  02.09.2013 |   Initial version    |Sudharsanan Sivagnanam (RBEI/ECF5)
*  18.09.2013    | 0.1                  | kgr1kor:Review Update-perf improvement
*  20.03.2014   | 0.2                   |pub5kor: The bmp image width is checked to see if it is 
							divisible by 64. If not,an error message is displayed.														
* ----------------------------------------------------------------------------
*  26.09.2013   |multykeypress support aded  | rai5cob
* ----------------------------------------------------------------------------
* 19.07.2016|   Version 1.0 | Changes required w.r.t candera adaptor | sja3kor
*******************************************************************************/

/************************************************************************
| Header file declaration
|-----------------------------------------------------------------------*/ 

#include "facia_egl_helper.h"
#include "facia_gles2_app.h"
#include "facia_xml_parser.h"



#include "IlmMatrix.h"
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <sys/types.h>
#include <png.h>
#include <semaphore.h>


/************************************************************************
| Macro declaration (scope: global)
|-----------------------------------------------------------------------*/

#define OFFSET(_STRUCT, _MEMBER)    ((unsigned int)&(_MEMBER) - (unsigned int)&(_STRUCT))
#define BMPFILEHEADERSIZE   (2+4+2+2+4)
#define BMPINFOHEADERSIZE   (4+4+4+2+2+4+4+4+4+4+4)

#define LSIM_FACIA_GUI_SIG SIGRTMIN
#define LSIM_FACIA_CURSOR_SIG (SIGRTMIN+1)

#define NO_OF_CURSOR_INDICES 9
#define NO_OF_CURSOR_LINE_INDICES 3
#define CURSOR_BUFFER_SIZE 27
#define CURSOR_COMPONENTS 3

#define FACIA_ELEMENTS 6
#define BUTTON_ELEMENTS 6

#define VERTEX_SIZE 4
#define INDEX_SIZE 2

#define MAX_KEY   50

#define PNG_BYTES_TO_CHECK 8
#define MAP_LAYER_ID 1000
/************************************************************************
| Variable declaration (scope: global)
|-----------------------------------------------------------------------*/

pthread_mutex_t facia_layer_arg_lock = PTHREAD_MUTEX_INITIALIZER;
//CMG3GB-1756 : For facia crash resolution, use the below semaphores to guard critical sections during startup alone
sem_t CursorReadyMutex;
sem_t FaciaReadyMutex;
bool CursorReadyFlag;
bool FaciaReadyFlag;

typedef struct
{
	GLuint  fragmentShaderId;
	GLuint  vertexShaderId;
	GLuint  shaderProgramId;
	GLint   a_positionLocation;
	GLint   a_colorLocation;
	GLint   a_texcoordLocation;
	GLint   u_matrixLocation;
	GLint   u_colorLocation;
	GLint   u_textureLocation;
} FaciaGles2Shader;

typedef struct
{
	GLuint  vtxbo;
	GLuint  idxbo;
} FaciaGles2VertexBuffer;

typedef struct
{
	GLfloat     x, y, z;
	GLfloat     u, v;
} vertexData;



typedef struct
{
	unsigned short  Type;
	unsigned long   Size;
	unsigned short  Reserved1;
	unsigned short  Reserved2;
	unsigned long   OffsetData;
} BMPFILEHEADER;

typedef struct
{
	unsigned long   Size;
	signed long     Width;
	signed long     Height;
	unsigned short  Planes;
	unsigned short  BitCount;
	unsigned long   Compression;
	unsigned long   SizeImage;
	signed long     XPelsPerMeter;
	signed long     YPelsPerMeter;
	unsigned long   ClrUsed;
	unsigned long   ClrImportant;
} BMPINFOHEADER;


typedef struct t_shaderObject
{
	GLuint fragmentShaderId;
	GLuint vertexShaderId;
	GLuint shaderProgramId;
	GLint matrixLocation;
	GLint colorLocation;
} CursorGles2Shader;

typedef struct vertexBufferObject
{
	GLuint vbo;
} CursorGles2VertexBuffer;

typedef struct
{
	GLuint      width, height;
	GLubyte     *pixel;
	GLfloat     maxu, maxv;
	GLuint      textureId;
   	GLint       internformat;
} textureData;

typedef struct
{
	Keypos position;
	int   Keycode;
	int   State;  

}buttoninfo;

buttoninfo g_buttonstate[MAX_KEY];

textureData triangleTextureData;


static CursorGles2Shader CursorShader;
static CursorGles2VertexBuffer CursorVertexBuffer;

static FaciaGles2Shader          FaciaShader;
static FaciaGles2VertexBuffer    FaciaVertexBuffer;
static FaciaGles2VertexBuffer    ButtonVertexBuffer;


GLuint      whiteTextureId;
GLuint      ImageWidth;
GLuint      ImageHeight;

bool g_bmultykeypressactive =false;

Bitmap BMPinfo;

// Coordinates for the corners of a quad
vertexData ButtontriangleVertexData[4] =
{
	// Position XYZ         Tex UV
	{ 0.681774, 0.114120, 0.0f,   1.0f, 0.0f, },   // lower left corner
	{ 0.849566, 0.114120, 0.0f,   0.0f, 1.0f, },    // lower right corner
	{ 0.681774, 0.504836, 0.0f,   1.0f, 1.0f, },    // upper left corner
	{ 0.849566, 0.504836, 0.0f,   0.0f, 0.0f, },  // upper right corner
};

GLubyte ButtontriangleIndexData[] =
{
	0, 1, 2,
	1, 3, 2,
};

// Coordinates for the corners of a quad
vertexData FaciatriangleVertexData[] =
{
	// Position XYZ         Tex UV
	{ -1.0f, -1.0f, 0.0f,   0.0f, 0.0f, },  // lower left corner
	{  1.0f, -1.0f, 0.0f,   1.0f, 0.0f, },  // lower right corner
	{ -1.0f,  1.0f, 0.0f,   0.0f, 1.0f, },  // upper left corner
	{  1.0f,  1.0f, 0.0f,   1.0f, 1.0f, },  // upper right corner
};

GLubyte FaciatriangleIndexData[] =
{
	0, 1, 2,
	1, 3, 2,
};


//GLfloat CursorTriangleVertexData[9] ={0.0000};

GLfloat CursorTriangleVertexData[] =
{
	-0.4f, -0.4f, 0.0f, -0.3f, -0.4f, 0.0f, -0.35f, -0.3f, 0.0f,
};


static volatile float g_cursor_x,g_cursor_y;
static sigset_t   signal_mask;  /* signals to block         */
int g_ButNo;
float cursor_x,cursor_y;
pthread_t curserTid=0;
pthread_t faciaDrowTid = 0;

/************************************************************************
| Shader Code
|-----------------------------------------------------------------------*/

const char * FaciaSourceFragShader = "\
		uniform lowp vec4       u_color;\
		uniform sampler2D       u_texture;\
		varying mediump vec2    v_texcoord;\
		void main (void)\
		{\
				gl_FragColor = texture2D(u_texture, v_texcoord) * u_color;\
		}";

const char * FaciaSourceVertShader = "\
		attribute highp vec4    a_position;\
		attribute mediump vec2  a_texcoord;\
		uniform mediump mat4    u_matrix;\
		varying mediump vec2    v_texcoord;\
		void main(void)\
		{\
				gl_Position = u_matrix*a_position;\
				v_texcoord = a_texcoord;\
		}";

const char * CursorSourceFragShader = "\
		uniform mediump vec4	u_color;\
		void main (void)\
		{\
				gl_FragColor = u_color;\
		}";

const char * CursorSourceVertShader = "\
		attribute highp vec4	a_vertex;\
		uniform mediump mat4	u_matrix;\
		void main(void)\
		{\
				gl_Position = u_matrix*a_vertex;\
		}";


/************************************************************************
| Inline functions definition
|-----------------------------------------------------------------------*/

inline short GetShort(const unsigned char *Buffer)              { return (short)Buffer[0] | ((short)Buffer[1]) << 8; }
inline unsigned short GetUShort(const unsigned char *Buffer)    { return (unsigned short)Buffer[0] | ((unsigned short)Buffer[1]) << 8; }
inline long GetLong(const unsigned char *Buffer)                { return (long)Buffer[0] | ((long)Buffer[1]) << 8 | ((long)Buffer[2]) << 16 | ((long)Buffer[3]) << 24; }
inline unsigned long GetULong(const unsigned char *Buffer)      { return (unsigned long)Buffer[0] | ((unsigned long)Buffer[1]) << 8 | ((unsigned long)Buffer[2]) << 16 | ((unsigned long)Buffer[3]) << 24; }


t_ilm_bool CursorInitShader(void);
t_ilm_bool InitCursorVertexBuffer(void);


/************************************************************************
| Function definition 
|-----------------------------------------------------------------------*/

/********************************************************************************
* FUNCTION      : InitCursorGlApplication()

* PARAMETER     :  None
																		
* RETURNVALUE   : None

* DESCRIPTION   :This funciton initializes GL app for cursor
**********************************************************************************/
t_ilm_bool InitCursorGlApplication()
{
	dbgprintf("InitCursorGlApplication\n");

	if (!InitCursorShader())
	{
		return ILM_FALSE;
	}

	if (!InitCursorVertexBuffer())
	{
		return ILM_FALSE;
	}

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	return ILM_TRUE;
}
/********************************************************************************
* FUNCTION      : InitCursorShader()

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :This function initializes the cursor shader
**********************************************************************************/
t_ilm_bool InitCursorShader()
{
	dbgprintf("InitCursorShader\n");

	t_ilm_bool result = ILM_TRUE;

	// Create the fragment shader object
	CursorShader.fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

	// Load Fragment Source
	glShaderSource(CursorShader.fragmentShaderId, 1, (const char**) &CursorSourceFragShader, NULL);

	// Compile the source code of fragment shader
	glCompileShader(CursorShader.fragmentShaderId);

	glGetShaderiv(CursorShader.fragmentShaderId, GL_COMPILE_STATUS, (GLint*) &result);

	if (!result)
	{
		t_ilm_int infoLength, numberChars;
		glGetShaderiv(CursorShader.fragmentShaderId, GL_INFO_LOG_LENGTH, &infoLength);

		// Allocate Log Space
		char* info = (char*) malloc(sizeof(char) * infoLength);
		glGetShaderInfoLog(CursorShader.fragmentShaderId, infoLength, &numberChars, info);

		// Print the error
		dbgprintf("Failed to compile fragment shader: %s\n", info);
		free(info);
		return ILM_FALSE;
	}

	// Create the fragment shader object
	CursorShader.vertexShaderId = glCreateShader(GL_VERTEX_SHADER);

	// Load Fragment Source
	glShaderSource(CursorShader.vertexShaderId, 1, (const char**) &CursorSourceVertShader, NULL);

	// Compile the source code of fragment shader
	glCompileShader(CursorShader.vertexShaderId);

	glGetShaderiv(CursorShader.vertexShaderId, GL_COMPILE_STATUS, (GLint*) &result);

	if (!result)
	{
		t_ilm_int infoLength, numberChars;
		glGetShaderiv(CursorShader.vertexShaderId, GL_INFO_LOG_LENGTH, &infoLength);

		// Allocate Log Space
		char* info = (char*) malloc(sizeof(char) * infoLength);
		glGetShaderInfoLog(CursorShader.vertexShaderId, infoLength, &numberChars, info);

		// Print the error
		dbgprintf("Failed to compile vertex shader: %s\n", info);
		free(info);
		return ILM_FALSE;
	}

	CursorShader.shaderProgramId = glCreateProgram();

	glAttachShader(CursorShader.shaderProgramId, CursorShader.fragmentShaderId);
	glAttachShader(CursorShader.shaderProgramId, CursorShader.vertexShaderId);

	glBindAttribLocation(CursorShader.shaderProgramId, 0, "a_vertex");

	glLinkProgram(CursorShader.shaderProgramId);

	glGetProgramiv(CursorShader.shaderProgramId, GL_LINK_STATUS, (GLint*) &result);

	if (!result)
	{
		t_ilm_int infoLength, numberChars;
		glGetShaderiv(CursorShader.shaderProgramId, GL_INFO_LOG_LENGTH, &infoLength);

		// Allocate Log Space
		char* info = (char*) malloc(sizeof(char) * infoLength);
		glGetShaderInfoLog(CursorShader.shaderProgramId, infoLength, &numberChars,
		info);

		// Print the error
		dbgprintf("Failed to link program: %s\n", info);
		free(info);
		return ILM_FALSE;
	}
	glUseProgram(CursorShader.shaderProgramId);
	CursorShader.matrixLocation = glGetUniformLocation(CursorShader.shaderProgramId, "u_matrix");
	CursorShader.colorLocation = glGetUniformLocation(CursorShader.shaderProgramId, "u_color");
	return result;
}
/********************************************************************************
* FUNCTION      : DestroyCursorShader()

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :This function destroys the cursor shader
**********************************************************************************/
t_ilm_bool DestroyCursorShader()
{
	t_ilm_bool result = ILM_TRUE;
	glDeleteProgram(CursorShader.shaderProgramId);
	glDeleteShader(CursorShader.fragmentShaderId);
	glDeleteShader(CursorShader.vertexShaderId);
	return result;
}

/********************************************************************************
* FUNCTION      : InitCursorVertexBuffer()

* PARAMETER     :  None
																		
* RETURNVALUE   : None

* DESCRIPTION   :This function initializes the cursor vertex buffer
**********************************************************************************/
t_ilm_bool InitCursorVertexBuffer()
{
	t_ilm_bool result = ILM_TRUE;

	glGenBuffers(1, &CursorVertexBuffer.vbo);
	glBindBuffer(GL_ARRAY_BUFFER, CursorVertexBuffer.vbo);
	
	unsigned int uiSize = CURSOR_BUFFER_SIZE * (sizeof(GLfloat) * 3); 
	glBufferData(GL_ARRAY_BUFFER, uiSize, &CursorTriangleVertexData[0], GL_STATIC_DRAW);

	return result;
}
/********************************************************************************
* FUNCTION      : AttachCursorVertexBuffer()

* PARAMETER     :  None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Attach cursor vertex buffer
**********************************************************************************/
void AttachCursorVertexBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, CursorVertexBuffer.vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, CURSOR_COMPONENTS, GL_FLOAT, GL_FALSE, 0, 0);

}
/********************************************************************************
* FUNCTION      : DetachCursorVertexBuffer()

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Detach vertex buffer
**********************************************************************************/
void DetachCursorVertexBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
/********************************************************************************
* FUNCTION      : DestroyCursorVertexBuffer()

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Destroy cursor vertex buffer
**********************************************************************************/
void DestroyCursorVertexBuffer()
{
	glDeleteBuffers(1, &CursorVertexBuffer.vbo);
}


/********************************************************************************
* FUNCTION      : Cursor()

* PARAMETER     :  int abs_x, int abs_y, int pendown
																		
* RETURNVALUE   : None

* DESCRIPTION   :Draw cursor
**********************************************************************************/
void Cursor()
{
	IlmMatrix matrix;
	
	dbgprintf("## Cursor: enter\n");
	
	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(CursorShader.shaderProgramId);
	InitCursorVertexBuffer();
	AttachCursorVertexBuffer();
	IlmMatrixIdentity(matrix);
	float color[4] = { 0.0f, 1.0f, 1.0f, 1.0f };
	float lineColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
	
	glUniformMatrix4fv(CursorShader.matrixLocation, 1, GL_FALSE, &matrix.f[0]);
	glUniform4fv(CursorShader.colorLocation, 1, &color[0]);
	glDrawArrays(GL_TRIANGLES, 0, NO_OF_CURSOR_INDICES); 
	
	glUniform4fv(CursorShader.colorLocation, 1, &lineColor[0]);
	glDrawArrays(GL_LINE_LOOP,0, NO_OF_CURSOR_LINE_INDICES);
	
	DetachCursorVertexBuffer();
	
	swapBuffers(1);

	dbgprintf("## Cursor: exit\n");
}
/********************************************************************************
* FUNCTION      : CursorDestroyGlApplication()

* PARAMETER     :  None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Destroy cursor GL app
**********************************************************************************/
void DestroyCursorGlApplication()
{
	DestroyCursorShader();
	DestroyCursorVertexBuffer();
}

/********************************************************************************
* FUNCTION      : WLcbCursorPos()

* PARAMETER     :  int abs_x, int abs_y, int pendown
																		
* RETURNVALUE   : None

* DESCRIPTION   :This is callback function which is called from INC Filter to notify cursor movement
**********************************************************************************/
void Calculate_Cursor_Co_ordinates(float x,float y)
{

	float temp1 =0;
	float temp2=0;
	float co_ord[6]={0};

	temp1 = x;
	temp2 = y;
	/*temp1 = ((2*(float)x)/(float)TOTAL_WIDTH)-1.0;
	temp2 = -(((2*(float)y)/(float)TOTAL_HEIGHT)-1.0);*/

	co_ord[0]= temp1;
	co_ord[1]= temp2;

	co_ord[2]= (temp1);
	co_ord[3]= (temp2-0.04);

	co_ord[4]= (temp1+0.02);
	co_ord[5]= (temp2-0.03);


	CursorTriangleVertexData[0]= co_ord[0];
	CursorTriangleVertexData[1]= co_ord[1];
	CursorTriangleVertexData[2]= 0.0;


	CursorTriangleVertexData[3]= co_ord[2];
	CursorTriangleVertexData[4]= co_ord[3];
	CursorTriangleVertexData[5]= 0.0;


	CursorTriangleVertexData[6]= co_ord[4];
	CursorTriangleVertexData[7]= co_ord[5];
	CursorTriangleVertexData[8]= 0.0;

#ifdef LOCAL_CALC
	dbgprintf("CursorTriangleVertexData[0]:%f\n",CursorTriangleVertexData[0]);
	dbgprintf("CursorTriangleVertexData[1]:%f\n",CursorTriangleVertexData[1]);

	dbgprintf("CursorTriangleVertexData[2]:%f\n",CursorTriangleVertexData[2]);
	dbgprintf("CursorTriangleVertexData[3]:%f\n",CursorTriangleVertexData[3]);

	dbgprintf("CursorTriangleVertexData[4]:%f\n",CursorTriangleVertexData[4]);
	dbgprintf("CursorTriangleVertexData[5]:%f\n",CursorTriangleVertexData[5]);

	dbgprintf("CursorTriangleVertexData[6]:%f\n",CursorTriangleVertexData[6]);
	dbgprintf("CursorTriangleVertexData[7]:%f\n",CursorTriangleVertexData[7]);

	dbgprintf("CursorTriangleVertexData[8]:%f\n",CursorTriangleVertexData[8]);
#endif

}

/********************************************************************************
* FUNCTION      : WLcbCursorPos()

* PARAMETER     :  int abs_x, int abs_y, int pendown
																		
* RETURNVALUE   : None

* DESCRIPTION   :This is callback function which is called from INC Filter to notify cursor movement
**********************************************************************************/

void WLcbCursorPos(float abs_x, float abs_y, int pendown)
{
	//CMG3GB-1756 : For facia crash during mouse pointer movement, guard below section till Cursor thread finishes drawing/initialization,
	//              else pthread_kill crashes the facia app
#ifdef VARIANT_S_FTR_ENABLE_CANDERAADAPTER
	return;
#endif

	if(CursorReadyFlag == false)
		sem_wait(&CursorReadyMutex);

	dbgprintf("$$ Cursor Update Signal Sent @ %f x %f >>%s\n",abs_x,abs_y,(pendown)?"PENDOWN":"PENUP");
	g_cursor_x= abs_x;
	g_cursor_y= abs_y;
	if(pthread_kill(curserTid, LSIM_FACIA_CURSOR_SIG) <0)
	{ 
		dbgprintf("Unable to send a signal\n");
	}
	else
	{
		dbgprintf("signal send success\n");
	}
	if(CursorReadyFlag == false)
		sem_post(&CursorReadyMutex);
}

/********************************************************************************
* FUNCTION      : Draw_cursor()

* PARAMETER     :  int abs_x, int abs_y, int pendown
																		
* RETURNVALUE   : None

* DESCRIPTION   :Draw cursor
**********************************************************************************/
void Draw_cursor()
{
   int sig;
   sigset_t set;
   siginfo_t info;
   struct	sigaction Sigact, old;
   dbgprintf("Draw_cursor\n");
   sigemptyset(&set);
   sigaddset(&set, LSIM_FACIA_CURSOR_SIG);
   sigprocmask(SIG_BLOCK, &set, NULL);

   
   Sigact.sa_handler = SIG_IGN;
   sigemptyset(&Sigact.sa_mask);
   Sigact.sa_flags = 0;
   sigaction(LSIM_FACIA_CURSOR_SIG, &Sigact, &old);

   dbgprintf("Draw_cursor_init\n");

   float old_cur_x = 0;
   float old_cur_y = 0;
   while(1)
   {
      //CMG3GB-1756 : release semaphore
#ifndef VARIANT_S_FTR_ENABLE_CANDERAADAPTER	  
      if(CursorReadyFlag == false)
         sem_post(&CursorReadyMutex);
#endif	 
      /*when a cursor movement is there signal will be recieved*/
      sig = sigwaitinfo(&set, &info);
      
      if(sig == LSIM_FACIA_CURSOR_SIG)
      {
         if (fabs(old_cur_x - g_cursor_x) > 0.001f  || fabs(old_cur_y - g_cursor_y) > 0.001f)
         {
            old_cur_x = g_cursor_x;
            old_cur_y = g_cursor_y;
            Calculate_Cursor_Co_ordinates(g_cursor_x,g_cursor_y);

            dbgprintf("## Draw_cursor activecursor_x:%f cursor_y:%f \n", g_cursor_x, g_cursor_y);

            Cursor();
            //CMG3GB-1756 : After this, Cursor is initialized, we need not use sempahore, and release semaphore
            CursorReadyFlag = true;
            dbgprintf("## Highlight_button: 2\n");
         }
      }
  
   }
}

/********************************************************************************
* FUNCTION      : ReadPNG2RGBATexture() 

* PARAMETER     :   TextureData, FileName
																		
* RETURNVALUE   : pixel data

* DESCRIPTION   : This function decodes png image and get texture data

* HISTORY            :
*----------------------------------------------------------------------------
* Date           |       Version        | Author & comments
*-----------|---------------|------------------------------------------------
*  27.05.2014 |   Initial version    | Luis Medinas (CM-AI/PJ-EPB1)
**********************************************************************************/

int ReadPNG2RGBATexture(textureData *TextureData, const char *FileName)
{
   FILE *file = NULL;
   png_structp png_ptr;
   png_infop info_ptr;
   png_bytepp row_pointers;
   png_uint_32 width, height;
   unsigned int sig_read = 0;
   unsigned int row_bytes;
   int color_type, interlace_type;
   int outWidth;
   int outHeight;
   int bit_depth;
   GLubyte *outData;

   file = fopen(FileName, "rb");
   if(file == NULL)
   {
      dbgprintf("Can't open png File\n");
      goto EXIT;
   }

   png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
                                        NULL, NULL, NULL);

   if (png_ptr == NULL)
   {
      dbgprintf("ReadPNG2RGBATexture: ERROR png pointer null\n");
      goto EXIT;
   }

   info_ptr = png_create_info_struct(png_ptr);

   if (info_ptr == NULL)
   {
      dbgprintf("ReadPNG2RGBATexture: png problem on create info struct\n");
      png_destroy_read_struct(&png_ptr, NULL, NULL);
      goto EXIT;
   }

   if (setjmp(png_jmpbuf(png_ptr)))
   {
       // Free all of the memory associated
       // with the png_ptr and info_ptr
       dbgprintf("ReadPNG2RGBATexture: png setup exception handling failed\n");
       png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
       goto EXIT;
   }

   // Set up the output control if
   png_init_io(png_ptr, file);

   // read some of the signature
   png_set_sig_bytes(png_ptr, sig_read);

   // read png
   png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND, NULL);

   png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
                &interlace_type, NULL, NULL);

   outWidth = width;
   outHeight = height;

   // Workaround to images with NPOTs
   row_bytes = png_get_rowbytes(png_ptr, info_ptr);
   row_bytes += 3 - ((row_bytes-1) % 4);
   outData = (unsigned char*) malloc(row_bytes * outHeight);

   row_pointers = png_get_rows(png_ptr, info_ptr);

   for (int i = 0; i < outHeight; i++)
   {
      // note that png is ordered top to
      // bottom, but OpenGL expect it bottom to top
      // so the order or swapped
      if (outData != NULL)
         memcpy(outData+(row_bytes * (outHeight-1-i)), row_pointers[i], row_bytes);
   }

   dbgprintf("ReadPNG2RGBATexture: png assign main data\n");

   // Assign Data to the OpenGLES struct
   ImageWidth = (GLuint)outWidth;
   ImageHeight = (GLuint)outHeight;
   triangleTextureData.width = outWidth;
   triangleTextureData.height = outHeight;
   triangleTextureData.pixel = outData;
   TextureData->maxu = (GLfloat)outWidth / (GLfloat)TextureData->width;
   TextureData->maxv = (GLfloat)outHeight / (GLfloat)TextureData->height;
   TextureData->internformat = GL_RGB;

   // Clean up after the read
   // and free any memory allocated
   png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

   return SUCCESS;

EXIT:
   // Yes the ugly goto
   if (file)
      fclose(file);
   png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
   dbgprintf("Failed to read %s\n", FileName);

   return ERROR;
}

/********************************************************************************
* FUNCTION      : ReadBMP2RGBATexture() 

* PARAMETER     :   TextureData, FileName
																		
* RETURNVALUE   : pixel data

* DESCRIPTION   : This function decodes bitmap image and get texture data

* HISTORY            :
*----------------------------------------------------------------------------
* Date           |       Version        | Author & comments
*-----------|---------------|------------------------------------------------
*  02.09.2013 |   Initial version    |Sudharsanan Sivagnanam (RBEI/ECF5)
*  20.03.2014 |   0.1			|pub5kor: The bmp image width is checked to see if it is 
								divisible by 64. If not,an error message is displayed.
**********************************************************************************/
int ReadBMP2RGBATexture(textureData *TextureData, const char *FileName)
{
	FILE            *file=NULL;
	BMPFILEHEADER   bmpfileheader;
	BMPINFOHEADER   bmpinfoheader;
	unsigned char   tmpbuff[(BMPFILEHEADERSIZE>BMPINFOHEADERSIZE)?BMPFILEHEADERSIZE:BMPINFOHEADERSIZE];
	size_t          palettesize;
	unsigned char   *palette=NULL;
	size_t          filebufflinesize, filebuffsize;
	unsigned char   *filebuff=NULL;
	//unsigned char   *pixel=NULL;
	size_t          cnt;
        
	file = fopen(FileName, "r");
	if(file == NULL)
	{
		dbgprintf("Can't open File\n");
		goto EXIT;
	}

	if(fread(&tmpbuff, 1, BMPFILEHEADERSIZE, file) != BMPFILEHEADERSIZE)
	{
		dbgprintf("Can't read fileheader\n");
		goto EXIT;
	}
	if(tmpbuff[0] != 'B' || tmpbuff[1] != 'M')
	{
		dbgprintf("File is not a BMP\n");
		goto EXIT;
	}
	// Use the GetXXX functions to extract the data. This prevents problems with endianess and structure alignment.
	bmpfileheader.Type          = GetUShort(&tmpbuff[0]);   cnt=2;
	bmpfileheader.Size          = GetULong(&tmpbuff[cnt]);  cnt+=4;
	bmpfileheader.Reserved1     = GetUShort(&tmpbuff[cnt]); cnt+=2;
	bmpfileheader.Reserved2     = GetUShort(&tmpbuff[cnt]); cnt+=2;
	bmpfileheader.OffsetData    = GetULong(&tmpbuff[cnt]);  cnt+=2;

	if(fread(&tmpbuff, 1, BMPINFOHEADERSIZE, file) != BMPINFOHEADERSIZE)
	{
		dbgprintf("Can't read infoheader\n");
		goto EXIT;
	}
	// Use the GetXXX functions to extract the data. This prevents problems with endianess and structure alignment.
	bmpinfoheader.Size          = GetULong(&tmpbuff[0]);    cnt=4;
	bmpinfoheader.Width         = GetLong(&tmpbuff[cnt]);   cnt+=4;
	bmpinfoheader.Height        = GetLong(&tmpbuff[cnt]);   cnt+=4;
	bmpinfoheader.Planes        = GetUShort(&tmpbuff[cnt]); cnt+=2;
	bmpinfoheader.BitCount      = GetUShort(&tmpbuff[cnt]); cnt+=2;
	bmpinfoheader.Compression   = GetULong(&tmpbuff[cnt]);  cnt+=4;
	bmpinfoheader.SizeImage     = GetULong(&tmpbuff[cnt]);  cnt+=4;
	bmpinfoheader.XPelsPerMeter = GetLong(&tmpbuff[cnt]);   cnt+=4;
	bmpinfoheader.YPelsPerMeter = GetLong(&tmpbuff[cnt]);   cnt+=4;
	bmpinfoheader.ClrUsed       = GetULong(&tmpbuff[cnt]);  cnt+=4;
	bmpinfoheader.ClrImportant  = GetULong(&tmpbuff[cnt]);  cnt+=4;

	if(bmpinfoheader.Width % 2 != 0)
	{
		int i;
		dbgprintf("WARNING: The image width should be in POT (Power of Two)!! Please consider converting\n");

		for (i = 0; i < 1024; i++)
		{
			if (i > bmpinfoheader.Width && (i % 2 == 0))
			{
				bmpinfoheader.Width = i;
				break;
			}
		}
		dbgprintf("Resize image to new width of %d\n", bmpinfoheader.Width);
	}


	ImageWidth = (GLuint)bmpinfoheader.Width;
	ImageHeight = (GLuint)bmpinfoheader.Height;

	dbgprintf("bmpinfoheader.Size:%lu \n",bmpinfoheader.Size);
	dbgprintf("bmpinfoheader.Width:%ld \n",bmpinfoheader.Width);
	dbgprintf("bmpinfoheader.Height:%ld \n",bmpinfoheader.Height);
	dbgprintf("bmpinfoheader.Planes:%u \n",bmpinfoheader.Planes);
	dbgprintf("bmpinfoheader.BitCount:%u \n",bmpinfoheader.BitCount);
	dbgprintf("bmpinfoheader.Compression:%lu \n",bmpinfoheader.Compression);
	dbgprintf("bmpinfoheader.SizeImage:%lu \n",bmpinfoheader.SizeImage);
	dbgprintf("bmpinfoheader.XPelsPerMeter:%ld \n",bmpinfoheader.XPelsPerMeter);
	
	dbgprintf("bmpinfoheader.YPelsPerMeter:%ld \n",bmpinfoheader.YPelsPerMeter);
	dbgprintf("bmpinfoheader.ClrUsed:%lu \n",bmpinfoheader.ClrUsed);
	dbgprintf("bmpinfoheader.ClrImportant:%lu \n",bmpinfoheader.ClrImportant);

	
	if(bmpinfoheader.Size < BMPINFOHEADERSIZE) // Only Windows BMPs are supported for version 3.x and up.
	{
		dbgprintf("Invalid infoheader\n");
		goto EXIT;
	}
	// FIXME: We could support masks and stuff but for now we just skip that
	fseek(file, bmpinfoheader.Size - BMPINFOHEADERSIZE, SEEK_CUR);

	if(bmpinfoheader.Compression && bmpinfoheader.Compression!=3)   // Compression is currently not supported. Usually BMPs become bigger when compressed anyways
	{
		dbgprintf("Compressed BMPs are not supported\n");
		goto EXIT;
	}

	//    palettesize = bmpfileheader.OffsetData - BMPFILEHEADERSIZE - bmpinfoheader.Size;
	palettesize = (bmpinfoheader.ClrUsed * 4);
	palette = (unsigned char *)malloc(palettesize);

	if(palettesize && !palette)
	{
		dbgprintf("Can't allocate memory for palette\n");
		goto EXIT;
	}

	if(fread(palette, 1, palettesize, file) != palettesize)
	{
		dbgprintf("Can't read palette\n");
		goto EXIT;
	}

	filebufflinesize = (bmpinfoheader.Width * bmpinfoheader.BitCount + 7) / 8;
	filebuffsize = (filebufflinesize * bmpinfoheader.Height);
	filebuff = (unsigned char *)malloc(filebuffsize);

	if(!filebuff)
	{
		dbgprintf("Can't allocate memory for filebuffer\n");
		goto EXIT;
	}

	if(fread(filebuff, filebufflinesize, bmpinfoheader.Height, file) != bmpinfoheader.Height)
	{
		dbgprintf("Can't read pixels to filebuffer\n");
		goto EXIT;
	}

	for(TextureData->width = 1; TextureData->width < bmpinfoheader.Width; TextureData->width <<= 1)
	{
	}

	for(TextureData->height = 1; TextureData->height < bmpinfoheader.Height; TextureData->height <<= 1)
	{
	}

        TextureData->pixel = (GLubyte *)malloc(TextureData->width * TextureData->height * 4);

	if(!TextureData->pixel)
	{
		dbgprintf("Can't allocate memory for pixel\n");
		goto EXIT;
	}

	bzero(TextureData->pixel, TextureData->width * TextureData->height * 4);
	TextureData->maxu = (GLfloat)bmpinfoheader.Width / (GLfloat)TextureData->width;
	TextureData->maxv = (GLfloat)bmpinfoheader.Height / (GLfloat)TextureData->height;
	TextureData->internformat = GL_RGBA;

	for(int y = 0; y < TextureData->height; y++)
	for(int x = 0; x < TextureData->width; x++)
	{
		TextureData->pixel[(y * TextureData->width + x) * 4 + 0] = 255;
		TextureData->pixel[(y * TextureData->width + x) * 4 + 1] = 0;
		TextureData->pixel[(y * TextureData->width + x) * 4 + 2] = 255;
		TextureData->pixel[(y * TextureData->width + x) * 4 + 3] = 255;
	}
	if(bmpinfoheader.BitCount == 8)
	{

		dbgprintf("Bit count is 8 \n");
		// Uncompressed BMP in 8Bit palette format, e.g. each pixel is an offset
		for(int y = 0; y < bmpinfoheader.Height; y++)
		{
			unsigned char   *src = filebuff + y * bmpinfoheader.Width;
			GLubyte         *dst = TextureData->pixel + y * TextureData->width * 4;
			for(int x = 0; x < bmpinfoheader.Width; x++)
			{
				dst[0] = palette[src[0] * 4 + 2];
				dst[1] = palette[src[0] * 4 + 1];
				dst[2] = palette[src[0] * 4 + 0];
				dst[3] = 255;

				src ++;
				dst += 4;
			}
		}
	}
	else if(bmpinfoheader.BitCount == 24)
	{

		dbgprintf("Bit count is 24 \n");
		// Uncompressed BMP in RGB888 format
		for(int y = 0; y < bmpinfoheader.Height; y++)
		{
			unsigned char   *src = filebuff + y * bmpinfoheader.Width * 3;
			GLubyte         *dst = TextureData->pixel + y * TextureData->width * 4;
			for(int x = 0; x < bmpinfoheader.Width; x++)
			{
				dst[0] = src[2];
				dst[1] = src[1];
				dst[2] = src[0];
				dst[3] = 255;

				src += 3;
				dst += 4;
			}
		}
	}
	else if(bmpinfoheader.BitCount == 32)
	{

		dbgprintf("Bit count is 32 \n");
		// Uncompressed BMP in RGBA8888 format
		// Paint programs usualy create these with compression format 3, so we should evaluate the entries for masks etc.
		// On the other hand I have never seen a single 32Bit BMP that was not RGBA8888
		for(int y = 0; y < bmpinfoheader.Height; y++)
		{
			unsigned char   *src = filebuff + y * bmpinfoheader.Width * 4;
			GLubyte         *dst = TextureData->pixel + y * TextureData->width * 4;
			for(int x = 0; x < bmpinfoheader.Width; x++)
			{
				dst[0] = src[3];
				dst[1] = src[2];
				dst[2] = src[1];
				dst[3] = src[0];

				src += 4;
				dst += 4;
			}
		}
	}
	else
	{
		// 1, 2, 4 bit not supported right now
		dbgprintf("Unsupported bits per pixel\n");
		goto EXIT;
	}

	
	return SUCCESS;

EXIT:
	if(file)
        	fclose(file);
	free(palette);
	free(filebuff);
	//free(pixel);

	dbgprintf("Failed to read %s\n", FileName);
	return ERROR;
}
/********************************************************************************
* FUNCTION      : WLcbHighlightRegion() 

* PARAMETER     :  int left, int top, int right, int bottom,int state
																		
* RETURNVALUE   : None

* DESCRIPTION   :This is callback function which is called from INC dispatcher to highlight the buttons
**********************************************************************************/
void WLcbHighlightRegion(Keypos* position,int state,int keycode)
{

   union sigval sigvalue = {0};
   static volatile int button_state = 0;
   static volatile int  LastKeycode = 0;
   static Keypos* KeppadPosition = 0;
   
   if(g_bmultykeypressactive == false ) 
   {
      g_ButNo=0;         
   }

   if(keycode==CNTRL_KEY && state==KEY_RELEASE)
   {
      /*this condition will remove complete dehilight issue when control key released*/
      Save_Button_info(KeppadPosition,button_state,LastKeycode);
   }
   else
   {

      /*save state in static temperary variable for multy key release condition*/
      KeppadPosition=position;
      button_state=state;
      LastKeycode=keycode;
      Save_Button_info(position,state,keycode);
   }    
   //CMG3GB-1756 : Facia is not yet ready, so do not send signal, wait for semaphore to be free
#ifndef VARIANT_S_FTR_ENABLE_CANDERAADAPTER   
   if(FaciaReadyFlag == false)
      sem_wait(&FaciaReadyMutex);
#endif

   if(pthread_sigqueue(faciaDrowTid, LSIM_FACIA_GUI_SIG, sigvalue) <0)
   { 
      perror("Unable to send a signal\n");
   }
#ifndef VARIANT_S_FTR_ENABLE_CANDERAADAPTER   
   if(FaciaReadyFlag == false)
      sem_post(&FaciaReadyMutex);
#endif
}

void Signal_handler()
{

	//Do nothing
}

/********************************************************************************
* FUNCTION      : layerNotificationCallback

* PARAMETER     : LayerID, Layer Properties, mask

* RETURNVALUE   : None

* DESCRIPTION   : Callback function for Changed Layer Notification
**********************************************************************************/
#ifdef  VARIANT_S_FTR_ENABLE_SINGLE_SCREENLAYOUTS
void layerNotificationCallback(t_ilm_layer layer, struct ilmLayerProperties* properties, t_ilm_notification_mask mask)
{
	if(layer == MAP_LAYER_ID)
	{
		if (ILM_NOTIFICATION_DEST_RECT & mask)
		{
			/// set it to right position
			Keymap TouchRegionOffset;
			Resolution TouchRegionSize;
			GetFaciaTouchRegion(&TouchRegionOffset,&TouchRegionSize);

			t_ilm_uint changedToPos[2];
			ilmErrorTypes isError = ilm_layerGetPosition(MAP_LAYER_ID,changedToPos);
			if(isError == 0)
			{
				if(changedToPos[0] != (int)TouchRegionOffset.ButtonPos.Left && changedToPos[1] != (int)TouchRegionOffset.ButtonPos.Top)
				{
					dbgprintf("ilm_getLayerIDs succeeded \n");
					t_ilm_uint correctPos[2];
					correctPos[0] = (int)TouchRegionOffset.ButtonPos.Left;
					correctPos[1] = (int)TouchRegionOffset.ButtonPos.Top;
					ilmErrorTypes LayerSetPositionError =  ilm_layerSetPosition(MAP_LAYER_ID,correctPos);
					ilm_commitChanges();
					ilm_layerRemoveNotification(MAP_LAYER_ID);
				}
			}
		}
	}
}
#endif

/********************************************************************************
* FUNCTION      : draw_facia() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Draw the facia image on the screen whenever button is pressed/released
**********************************************************************************/
void Draw_facia()
{

   int sig;
   sigset_t set;
   siginfo_t info;
   struct	sigaction Sigact, old;

   sigemptyset(&set);
   sigaddset(&set, LSIM_FACIA_GUI_SIG);
   sigprocmask( SIG_BLOCK, &set, NULL );

   Sigact.sa_handler = SIG_IGN;
   sigemptyset(&Sigact.sa_mask);
   Sigact.sa_flags = 0;
   sigaction(LSIM_FACIA_GUI_SIG, &Sigact, &old);
   printf("signal installed\n");
   
   // register the Map Layer Changed Notification
#ifdef  VARIANT_S_FTR_ENABLE_SINGLE_SCREENLAYOUTS
   ilm_layerAddNotification(MAP_LAYER_ID,layerNotificationCallback);
#endif
   dbgprintf("Draw_facia : Init \n");
   while(ILM_TRUE)
   {
      //CMG3GB-1756 : free the semaphore here
#ifndef VARIANT_S_FTR_ENABLE_CANDERAADAPTER	  
      if(FaciaReadyFlag == false)
         sem_post(&FaciaReadyMutex);
#endif
      sig = sigwaitinfo(&set, &info);
      
      if(sig == LSIM_FACIA_GUI_SIG)
      {
         dbgprintf("Draw_facia : Active \n");
         
         if(g_ButNo)
         {
            
            Button_Highlight();         
            
         }
         else
         {
            dbgprintf("Draw_facia: Button dehighlight\n");
            Draw_facia_image();
         }
         //CMG3GB-1756 : After this, facia is ready, so no need to use sempahore anymore
         FaciaReadyFlag = true;
      }
      dbgprintf("Draw_facia: Done\n");
      dbgprintf("Draw_facia : Inactive \n");
   }

}
/********************************************************************************
* FUNCTION      : Save_Button_info(Keypos* position,int button_state,int keycode)

* PARAMETER     : Keypos* position,int button_state,int keycode
																		
* RETURNVALUE   : None

* DESCRIPTION   :Save or remove  the location Info before drawing
 **********************************************************************************/
int Save_Button_info(Keypos* position,int button_state,int keycode)
{
   int retval=0;  
   int numkeys=0;
   pthread_mutex_lock(&facia_layer_arg_lock);

   /*logic for dehighlight the already pressed key*/

   if(button_state == MOUSE_HOVER_BUTTONPRESS && g_ButNo>0)
   {
      for(numkeys=0; numkeys<g_ButNo; numkeys++)
      {        /* We shall consider only pressed keys here. i.e ignore the mouse hover highlighted key */
         if(keycode == g_buttonstate[numkeys].Keycode)
         {
            /* Checking if current key is already in pressed state */
            if(g_buttonstate[numkeys].State== MOUSE_HOVER_BUTTONPRESS)
            {
               /* Yes it is already pressed key. Lets remove it from the pressed key list */
               for(int i=numkeys;i<g_ButNo;i++)
               {
                  g_buttonstate[i]=g_buttonstate[i+1];
               }
               g_ButNo--;
               pthread_mutex_unlock(&facia_layer_arg_lock);
               return retval;				
            }

         }
      }
   }
   /* If the last key is hover highlight key lets remove it */
   if (g_ButNo > 0 && g_buttonstate[g_ButNo-1].State == MOUSE_HOVER_BUTTONREGION) 
   {
      g_ButNo--;
   }
   /* logic for highligh the key*/
   g_buttonstate[g_ButNo].position.Left = position->Left;
   g_buttonstate[g_ButNo].position.Top = position->Top;
   g_buttonstate[g_ButNo].position.Right= position->Right;
   g_buttonstate[g_ButNo].position.Bottom= position->Bottom;
   g_buttonstate[g_ButNo].State= button_state;

   if(button_state==MOUSE_HOVER_BUTTONPRESS)
   {      
      g_buttonstate[g_ButNo].Keycode= keycode;
      retval=MOUSE_HOVER_BUTTONPRESS;
   }
   else if(button_state==MOUSE_HOVER_BUTTONREGION)
   {
      retval=MOUSE_HOVER_BUTTONREGION;
   }
   if(g_ButNo<MAX_KEY)
   {
      g_ButNo++;
   }
   pthread_mutex_unlock(&facia_layer_arg_lock);
   return retval;
}

int ImageDetector(const char *imgfile)
{
   FILE *fp = NULL;
   unsigned char buf[PNG_BYTES_TO_CHECK];
   unsigned char   tmpbuff[(BMPFILEHEADERSIZE>BMPINFOHEADERSIZE)?BMPFILEHEADERSIZE:BMPINFOHEADERSIZE];
   int retval = IMG_INVALID;

   fp = fopen(imgfile, "rb");
   if(fp == NULL)
   {
      dbgprintf("imagedetector: Can't open File\n");
      return IMG_INVALID;
   }

   if(fread(&tmpbuff, 1, BMPFILEHEADERSIZE, fp) != BMPFILEHEADERSIZE)
   {
      dbgprintf("imagedetector: Can't read bmp fileheader\n");
      retval = IMG_INVALID;
   }
   if(tmpbuff[0] != 'B' || tmpbuff[1] != 'M')
   {
      dbgprintf("imagedetector: File is not a BMP\n");
      retval = IMG_INVALID;
   }
   else
   {
      dbgprintf("imagedetector: File is a BMP\n");
      retval = IMG_BMP;

   }
   dbgprintf("imagedetector: 1\n");

   if (fseek(fp, 0, SEEK_SET) != 0)
   {
      dbgprintf("imagedetector: fseek to beginning of the file failed\n");
   }

   if (fread(buf, 1, PNG_BYTES_TO_CHECK, fp) != PNG_BYTES_TO_CHECK)
   {
      dbgprintf("imagedetector: Can't read png fileheader\n");
      return IMG_INVALID;
   }

   if (png_sig_cmp(buf, 0, PNG_BYTES_TO_CHECK) == 0)
   {
      dbgprintf("imagedetector: File is a PNG\n");
      retval = IMG_PNG;
   }

   dbgprintf("imagedetector: 2\n");

   fclose(fp);

   return retval;
}

int GetBMPinfo(char * Path)
{
   const char *realpath;
   int img_type;

   if(GetBitMapInfo((Bitmap*)&BMPinfo)== ERROR)
   {
      dbgprintf("GetBMPinfo failed\n");
      return ERROR;
   }

   if (Path == NULL)
   {
      realpath = BMPinfo.Name;
      dbgprintf("GetBMPinfo load image by xml %s\n", BMPinfo.Name);
   }
   else
   {
      realpath = Path;
      dbgprintf("GetBMPinfo load image by path\n");
   }

   img_type = ImageDetector(realpath);

   dbgprintf("GetBMPinfo file image is type %d\n", img_type);

   if (img_type == IMG_PNG)
   {
      if (ReadPNG2RGBATexture(&triangleTextureData, realpath)== SUCCESS)
      {
         dbgprintf("GetBMPinfo load png success %d\n", img_type);
         return SUCCESS;
      }
   }

   if (img_type == IMG_BMP)
   {
      if(ReadBMP2RGBATexture(&triangleTextureData, realpath)== SUCCESS)
      {
         dbgprintf("GetBMPinfo load bmp success %d\n", img_type);
         return SUCCESS;
      }
   }

   return ERROR;
}

void GetImagesize(Resolution* size)
{

	size->width=ImageWidth;
	size->height=ImageHeight;

}

/********************************************************************************
* FUNCTION      : InitGlApplication() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :create image and white texture, create shader object, Initialize vertex buffer 
**********************************************************************************/
t_ilm_bool InitFaciaGlApplication()
{

	dbgprintf("InitFaciaGlApplication\n");


	glGenTextures(1, &triangleTextureData.textureId);
	glBindTexture(GL_TEXTURE_2D, triangleTextureData.textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE  );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE  );
	glTexImage2D(GL_TEXTURE_2D, 0, triangleTextureData.internformat, triangleTextureData.width, 
                     triangleTextureData.height, 0, triangleTextureData.internformat, GL_UNSIGNED_BYTE, triangleTextureData.pixel);

	FaciatriangleVertexData[0].u = 0.0f;                     FaciatriangleVertexData[0].v = 0.0f;
	FaciatriangleVertexData[1].u = triangleTextureData.maxu; FaciatriangleVertexData[1].v = 0.0f;
	FaciatriangleVertexData[2].u = 0.0f;                     FaciatriangleVertexData[2].v = triangleTextureData.maxv;
	FaciatriangleVertexData[3].u = triangleTextureData.maxu; FaciatriangleVertexData[3].v = triangleTextureData.maxv;

	unsigned long   whitetexel = 0xffffffff;
	glGenTextures(1, &whiteTextureId);
	glBindTexture(GL_TEXTURE_2D, whiteTextureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, &whitetexel);

	if (!InitFaciaShader())
	{
		return ILM_FALSE;
	}

	if (!InitFaciaVertexBuffer())
	{
		dbgprintf("InitFaciaVertexBuffer failed");
		return ILM_FALSE;
	}

	if (!InitButtonVertexBuffer())
	{

		dbgprintf("InitButtonVertexBuffer failed");
		return ILM_FALSE;
	}
	
	glClearColor(0.0f,0.0f,0.0f,1.0f); 
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);    // Use premultiplied alpha! It's way better in any way you can think of.
	glEnable(GL_BLEND);

	return ILM_TRUE;
}

/********************************************************************************
* FUNCTION      : InitShader() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Init shader
**********************************************************************************/

t_ilm_bool InitFaciaShader()
{

	dbgprintf("InitShader");
	t_ilm_bool result = ILM_TRUE;

	// Create the fragment shader object
	FaciaShader.fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

	// Load Fragment Source
	glShaderSource(FaciaShader.fragmentShaderId, 1, (const char**) &FaciaSourceFragShader, NULL);

	// Compile the source code of fragment shader
	glCompileShader(FaciaShader.fragmentShaderId);

	glGetShaderiv(FaciaShader.fragmentShaderId, GL_COMPILE_STATUS, (GLint*) &result);

	if (!result)
	{
		t_ilm_int infoLength, numberChars;
		glGetShaderiv(FaciaShader.fragmentShaderId, GL_INFO_LOG_LENGTH, &infoLength);

		// Allocate Log Space
		char* info = (char*) malloc(sizeof(char) * infoLength);
		glGetShaderInfoLog(FaciaShader.fragmentShaderId, infoLength, &numberChars, info);

		// Print the error
		dbgprintf("Failed to compile fragment shader: %s\n", info);
		free(info);
		return ILM_FALSE;
	}

	// Create the fragment shader object
	FaciaShader.vertexShaderId = glCreateShader(GL_VERTEX_SHADER);

	// Load Fragment Source
	glShaderSource(FaciaShader.vertexShaderId, 1, (const char**) &FaciaSourceVertShader, NULL);

	// Compile the source code of fragment shader
	glCompileShader(FaciaShader.vertexShaderId);

	glGetShaderiv(FaciaShader.vertexShaderId, GL_COMPILE_STATUS, (GLint*) &result);

	if (!result)
	{
		t_ilm_int infoLength, numberChars;
		glGetShaderiv(FaciaShader.vertexShaderId, GL_INFO_LOG_LENGTH, &infoLength);

		// Allocate Log Space
		char* info = (char*) malloc(sizeof(char) * infoLength);
		glGetShaderInfoLog(FaciaShader.vertexShaderId, infoLength, &numberChars, info);

		// Print the error
		dbgprintf("Failed to compile vertex shader: %s\n", info);
		free(info);
		return ILM_FALSE;
	}

	FaciaShader.shaderProgramId = glCreateProgram();

	glAttachShader(FaciaShader.shaderProgramId, FaciaShader.fragmentShaderId);
	glAttachShader(FaciaShader.shaderProgramId, FaciaShader.vertexShaderId);

	glLinkProgram(FaciaShader.shaderProgramId);

	glGetProgramiv(FaciaShader.shaderProgramId, GL_LINK_STATUS, (GLint*) &result);

	if (!result)
	{
		t_ilm_int infoLength, numberChars;
		glGetShaderiv(FaciaShader.shaderProgramId, GL_INFO_LOG_LENGTH, &infoLength);

		// Allocate Log Space
		char* info = (char*) malloc(sizeof(char) * infoLength);
		glGetShaderInfoLog(FaciaShader.shaderProgramId, infoLength, &numberChars, info);

		// Print the error
		dbgprintf("Failed to link program: %s\n", info);
		free(info);
		return ILM_FALSE;
	}
	glUseProgram(FaciaShader.shaderProgramId);
	FaciaShader.a_positionLocation = glGetAttribLocation(FaciaShader.shaderProgramId, "a_position");
	//FaciaShader.a_colorLocation = glGetAttribLocation(FaciaShader.shaderProgramId, "a_color");
	FaciaShader.a_texcoordLocation = glGetAttribLocation(FaciaShader.shaderProgramId, "a_texcoord");
	FaciaShader.u_matrixLocation = glGetUniformLocation(FaciaShader.shaderProgramId, "u_matrix");
	FaciaShader.u_colorLocation = glGetUniformLocation(FaciaShader.shaderProgramId, "u_color");
	FaciaShader.u_textureLocation = glGetUniformLocation(FaciaShader.shaderProgramId, "u_texture");
	return result;
}

/********************************************************************************
* FUNCTION      : DestroyShader() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :destroy Shader
**********************************************************************************/
t_ilm_bool DestroyFaciaShader()
{
	t_ilm_bool result = ILM_TRUE;
	glDeleteProgram(FaciaShader.shaderProgramId);
	glDeleteShader(FaciaShader.fragmentShaderId);
	glDeleteShader(FaciaShader.vertexShaderId);
	return result;
}

/********************************************************************************
* FUNCTION      : InitFaciaVertexBuffer() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Init vertex buffer for facia image
**********************************************************************************/

t_ilm_bool InitFaciaVertexBuffer()
{
	t_ilm_bool      result = ILM_TRUE;
	unsigned int    uiSize;

	glGenBuffers(1, &FaciaVertexBuffer.vtxbo);
	glBindBuffer(GL_ARRAY_BUFFER, FaciaVertexBuffer.vtxbo);
	uiSize = VERTEX_SIZE * sizeof(vertexData); 
	glBufferData(GL_ARRAY_BUFFER, uiSize, &FaciatriangleVertexData[0], GL_STATIC_DRAW);

	glGenBuffers(1, &FaciaVertexBuffer.idxbo);
	glBindBuffer(GL_ARRAY_BUFFER, FaciaVertexBuffer.idxbo);
	uiSize = INDEX_SIZE * (sizeof(char) * 3);
	glBufferData(GL_ARRAY_BUFFER, uiSize, &FaciatriangleIndexData[0], GL_STATIC_DRAW);

	return result;
}
/********************************************************************************
* FUNCTION      : InitButtonVertexBuffer() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Init vertex buffer for button to be highlighted
**********************************************************************************/
t_ilm_bool InitButtonVertexBuffer()
{
	t_ilm_bool      result = ILM_TRUE;
	unsigned int    uiSize;

	glGenBuffers(1, &ButtonVertexBuffer.vtxbo);
	glBindBuffer(GL_ARRAY_BUFFER, ButtonVertexBuffer.vtxbo);
	uiSize = VERTEX_SIZE * sizeof(vertexData); 
	glBufferData(GL_ARRAY_BUFFER, uiSize, &ButtontriangleVertexData[0], GL_STATIC_DRAW);

	glGenBuffers(1, &ButtonVertexBuffer.idxbo);
	glBindBuffer(GL_ARRAY_BUFFER, ButtonVertexBuffer.idxbo);
	uiSize = INDEX_SIZE * (sizeof(char) * 3);
	glBufferData(GL_ARRAY_BUFFER, uiSize, &ButtontriangleIndexData[0], GL_STATIC_DRAW);

	return result;
}

/********************************************************************************
* FUNCTION      : AttachFaciaVertexBuffer() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Attach vertex buffer for facia image
**********************************************************************************/
void AttachFaciaVertexBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, FaciaVertexBuffer.vtxbo);
	glEnableVertexAttribArray(FaciaShader.a_positionLocation);
	glVertexAttribPointer(FaciaShader.a_positionLocation, 3, GL_FLOAT, GL_FALSE, sizeof(vertexData),
	(GLvoid *)OFFSET(FaciatriangleVertexData[0], FaciatriangleVertexData[0].x));
	glEnableVertexAttribArray(FaciaShader.a_texcoordLocation);
	glVertexAttribPointer(FaciaShader.a_texcoordLocation, 2, GL_FLOAT, GL_FALSE, sizeof(vertexData), 
	(GLvoid *)OFFSET(FaciatriangleVertexData[0], FaciatriangleVertexData[0].u));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, FaciaVertexBuffer.idxbo);
}

/********************************************************************************
* FUNCTION      : AttachButtonVertexBuffer() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Attach vertex buffer for button to be highlighted
**********************************************************************************/
void AttachButtonVertexBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, ButtonVertexBuffer.vtxbo);
	glEnableVertexAttribArray(FaciaShader.a_positionLocation);
	glVertexAttribPointer(FaciaShader.a_positionLocation, 3, GL_FLOAT, GL_FALSE, sizeof(vertexData), 
	(GLvoid *)OFFSET(ButtontriangleVertexData[0], ButtontriangleVertexData[0].x));
	glEnableVertexAttribArray(FaciaShader.a_texcoordLocation);
	glVertexAttribPointer(FaciaShader.a_texcoordLocation, 2, GL_FLOAT, GL_FALSE, sizeof(vertexData), 
	(GLvoid *)OFFSET(ButtontriangleVertexData[0], ButtontriangleVertexData[0].u));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ButtonVertexBuffer.idxbo);
}

/********************************************************************************
* FUNCTION      : DetachFaciaVertexBuffer() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :detachVertexBuffer
**********************************************************************************/
void DetachFaciaVertexBuffer()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


/********************************************************************************
* FUNCTION      : DestroyVertexBuffer() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :destroyVertexBuffer
**********************************************************************************/

void DestroyVertexBuffer()
{
	glDeleteBuffers(1, &FaciaVertexBuffer.vtxbo);
	glDeleteBuffers(1, &FaciaVertexBuffer.idxbo);
	glDeleteBuffers(1, &ButtonVertexBuffer.vtxbo);
	glDeleteBuffers(1, &ButtonVertexBuffer.idxbo);
}

/********************************************************************************
* FUNCTION      : Highlight_button() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Highlight a perticular button 
**********************************************************************************/
/********************************************************************************
* FUNCTION      : Button_Highlight() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Highlight a perticular button 
**********************************************************************************/
void Button_Highlight()
{
   int j,temp=g_ButNo;
   
   dbgprintf("## Highlight_button: enter\n");

   IlmMatrix           matrix,matrix_1;
   float color[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
   float color2[4] = { 0.0f, 0.0f, 0.5f, 0.5f };//green
   float color3[4] = { 0.0f, 0.5f, 0.0f, 0.5f };//blue

   glClear(GL_COLOR_BUFFER_BIT);
   glUseProgram(FaciaShader.shaderProgramId);

   AttachFaciaVertexBuffer();
   IlmMatrixIdentity(matrix);
   glUniformMatrix4fv(FaciaShader.u_matrixLocation, 1, GL_FALSE,&matrix.f[0]);
   glUniform4fv(FaciaShader.u_colorLocation, 1, &color[0]);
   glBindTexture(GL_TEXTURE_2D, triangleTextureData.textureId);
   glUniform1i(FaciaShader.u_textureLocation, 0);
   glDrawElements(GL_TRIANGLES, FACIA_ELEMENTS, GL_UNSIGNED_BYTE, 0); 
   DetachFaciaVertexBuffer();
   pthread_mutex_lock(&facia_layer_arg_lock);
   for(j=0;j<temp;j++)
   {
      
      
      ButtontriangleVertexData[0].x =   g_buttonstate[j].position.Left;
      ButtontriangleVertexData[0].y =   g_buttonstate[j].position.Top;
      
      ButtontriangleVertexData[1].x =   g_buttonstate[j].position.Right;
      ButtontriangleVertexData[1].y =   g_buttonstate[j].position.Top;
      
      ButtontriangleVertexData[2].x =   g_buttonstate[j].position.Left;
      ButtontriangleVertexData[2].y =   g_buttonstate[j].position.Bottom;
      
      ButtontriangleVertexData[3].x =   g_buttonstate[j].position.Right;;
      ButtontriangleVertexData[3].y =   g_buttonstate[j].position.Bottom;;
      

      InitButtonVertexBuffer();
      AttachButtonVertexBuffer();
      
      
      IlmMatrixIdentity(matrix_1);		
      glUniformMatrix4fv(FaciaShader.u_matrixLocation, 1, GL_FALSE,&matrix_1.f[0]);
      if(g_buttonstate[j].State==MOUSE_HOVER_BUTTONREGION)
      {
         glUniform4fv(FaciaShader.u_colorLocation, 1, &color3[0]);
         
      }
      else
      {
         glUniform4fv(FaciaShader.u_colorLocation, 1, &color2[0]);
         
      }
      
      glBindTexture(GL_TEXTURE_2D, whiteTextureId);
      glUniform1i(FaciaShader.u_textureLocation, 0);

      glDrawElements(GL_TRIANGLES, BUTTON_ELEMENTS, GL_UNSIGNED_BYTE, 0); 
      //glDrawArrays(GL_TRIANGLE_STRIP, matrix_1.f[0], 16); 
      
      DetachFaciaVertexBuffer();
      
   }
   pthread_mutex_unlock(&facia_layer_arg_lock);
   swapBuffers(0);
   dbgprintf("## Highlight_buttons: \n");
}
/********************************************************************************
* FUNCTION      : Draw_facia_image() 

* PARAMETER     : None
                                                      
* RETURNVALUE   : None

* DESCRIPTION   :Draw facia image
**********************************************************************************/
void Draw_facia_image()
{

   dbgprintf("## Draw_facia_image: Enter\n");
   
   IlmMatrix           matrix;
   float color[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

   glClear(GL_COLOR_BUFFER_BIT);
   glUseProgram(FaciaShader.shaderProgramId);
   AttachFaciaVertexBuffer();
   IlmMatrixIdentity(matrix);

   glUniformMatrix4fv(FaciaShader.u_matrixLocation, 1, GL_FALSE,&matrix.f[0]);
   glUniform4fv(FaciaShader.u_colorLocation, 1, &color[0]);
   glBindTexture(GL_TEXTURE_2D, triangleTextureData.textureId); 
   glUniform1i(FaciaShader.u_textureLocation, 0);
   glDrawElements(GL_TRIANGLES, FACIA_ELEMENTS, GL_UNSIGNED_BYTE, 0); 

   DetachFaciaVertexBuffer();
   swapBuffers(0);
   dbgprintf("## Draw_facia_image: Exit\n");
}


/********************************************************************************
* FUNCTION      : destroyGlApplication() 

* PARAMETER     : None
																		
* RETURNVALUE   : None

* DESCRIPTION   :Draw facia image
**********************************************************************************/

void DestroyFaciaGlApplication()
{
	DestroyFaciaShader();
	DestroyVertexBuffer();
}
