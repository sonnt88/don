/************************************************************************
| FILE:         facia_inc_filter.cpp
| PROJECT:      platform
| SW-COMPONENT: FACIA-INC
|------------------------------------------------------------------------------
| DESCRIPTION:  This part of the code is used to handle the input events
|				from wayland and send to INC constructor module and for
|				Highlighting Facia HMI regions based on on the available
|               XML data
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 19.07.13  | Initial version            | kgr1kor
| 18.09.13  | Review Update, Perf Improve| kgr1kor
|           | multiple resolution support|
|           | kbd & xml parser FW support|
| --.--.--  | ----------------           | -------, -----
|26.09.13   |multykeypress support aded  | rai5cob
|*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <linux/input.h>
#include <stdbool.h>
#include <unistd.h>

#include "facia_fake_wl_input_event.h"
//#include "facia_xmldata.h"
#include "facia_gles2_app.h"
#include "facia_xml_parser.h"
#include "trace_interface.h"

#ifdef __cplusplus
extern "C" {
#endif

#define RESIZE_BMP

#define REMAP(x,max_x) (((2*((float)x))/(float)(max_x))-1.0)
#ifdef RESIZE_BMP
#define RESIZE(x,old_max_x,new_max_x) (((float)x/(float)old_max_x)*((float)(new_max_x)))
#else
#define RESIZE(x,old_max_x,new_max_x)  (x)

#endif

#define MSQ_PRIORITY 0x33
#define MSQ_NAME "/LSIMINCEVE"
#define MSQ_MAXMESG 100
#define MSQ_MSGSIZE 256
pthread_t accThreadId;

//#define OUT_OF_SCREEN  -1, /*Not a valid facia block*/
//#define FACIA_SCREEN   0
//#define TOUCH_SCREEN   1

enum
{
	OUT_OF_SCREEN=-1,FACIA_SCREEN,TOUCH_SCREEN
}screenregion;

	//#define XMLFILE "/opt/bosch/base/faciaconfig.xml"

mqd_t mqh = 0;
bool gpendown=false;
bool gtouchpendown=false;
int gPointerX = 0;
int gPointerY = 0;
int  gfacia_blk_count = 0; 

#define FACIABLK_XMLDATA_COUNT 40

Keymap faciablk_xmldata[FACIABLK_XMLDATA_COUNT] = {{"OUT_OF_SCREEN",0,0,-1,-1,{0,0,0,0},0},
	{"FACIA_SCREEN",0,0,-1,0,{0,0,0,0},0}};

extern bool g_bmultykeypressactive;

Resolution currentimage;
Resolution Touchsize;
unsigned int LSIMwidth;
unsigned int LSIMheight;
extern int 	g_ButNo;
int g_KeyNo=0;

input_event g_inpEvent[50];
pthread_mutex_t accState_mutex;
bool g_bACCActiveState = true;


int Save_key_info(input_event* ev );
void Send_current_Location(void);

/********************************************************************************
	* FUNCTION		: preparefacia_info() 
	
	* PARAMETER 	:	None
																			
	* RETURNVALUE	: None
	
	* DESCRIPTION	: prepare the facia button co-ordinates 
**********************************************************************************/
static int preparefacia_info()
{
   int i=0;
   
   Keypos* position;


   faciablk_xmldata[1].ButtonPos.Right = LSIMwidth;
   faciablk_xmldata[1].ButtonPos.Bottom = LSIMheight ;
   
   GetImagesize((Resolution*)&currentimage);
   dbgprintf("Image w:%d,H:%d\n",currentimage.width,currentimage.height);

   if(GetFaciaTouchRegion((Keymap*)&faciablk_xmldata[2],(Resolution*)&Touchsize)== ERROR)
   {
      errprintf("Get touch region failed\n");
      return ERROR;
   }
   dbgprintf("Touch W:%d,H:%d\n",Touchsize.width,Touchsize.height);

   gfacia_blk_count = GetNumberOfBlocks();
   if ((gfacia_blk_count+3) > FACIABLK_XMLDATA_COUNT) {
      errprintf("GetKeymapInfo ERROR: Number of XML key entries are higher as current MAX %d > %d\n", gfacia_blk_count, (FACIABLK_XMLDATA_COUNT-3));
      return ERROR;
   }
   gfacia_blk_count +=3;
   dbgprintf("Number of blocks:%d\n",gfacia_blk_count);
   
   if (GetKeymapInfo((Keymap*)&faciablk_xmldata[3])== ERROR)
   {
      errprintf("GetKeymapInfo failed\n");
      return ERROR;
   }

   dbgprintf("blocks:%d imgwidth:%d, imgHeight:%d\n",gfacia_blk_count,currentimage.width,currentimage.height);

   for ( i=2; i< gfacia_blk_count ;i++)
   {
      position=(Keypos*)&faciablk_xmldata[i].ButtonPos;

      position->Left = REMAP(RESIZE(position->Left,currentimage.width,LSIMwidth),LSIMwidth);
      position->Top = -(REMAP(RESIZE(position->Top,currentimage.height,LSIMheight),LSIMheight));
      position->Right = REMAP(RESIZE(position->Right,currentimage.width,LSIMwidth),LSIMwidth);
      position->Bottom = -(REMAP(RESIZE(position->Bottom,currentimage.height,LSIMheight),LSIMheight));
      
      dbgprintf(" Block:%d Name:%s Key:%d Keycode:%d Axis:%d Left:%f Top:%f Right:%f Bottom:%f \n",
      faciablk_xmldata[i].Block,
      faciablk_xmldata[i].ButtonName,
      faciablk_xmldata[i].Key,
      faciablk_xmldata[i].KeyCode,
      faciablk_xmldata[i].Axis,
      position->Left,
      position->Top,
      position->Right,
      position->Bottom);
      
   }
   return 0;
}
void* executeSPMCommand(void *vArg)
{
   dbgprintf("FakePointerHandleButton: HardKey Message , ACC found \n");
   pthread_mutex_lock(&accState_mutex);
   if(g_bACCActiveState)
   {
        dbgprintf("FakePointerHandleButton: HardKey Message , ACC found  OFF \n");
        g_bACCActiveState = false;
        U8 Buf1[15] = {0x0F ,0xFF ,0x02 ,0x03 ,0 ,0x2B ,0,0,0 ,0x09 ,0 ,0 ,0 ,0}; //SpmSubState IGNITION off
        U8 Buf2[15] = {0x0F ,0xFF ,0x02 ,0x03 ,0 ,0x2B ,0,0,0 ,0x08 ,0 ,0 ,0 ,0}; //SpmSubState ACCESSORY off
        TR_core_s32SendCmd(Buf1,14);
        sleep(1);
        TR_core_s32SendCmd(Buf2,14);
   }
   else
 {
      dbgprintf("FakePointerHandleButton: HardKey Message , ACC found  ON \n");
      g_bACCActiveState = true;
      U8 Buf1[15] = {0x0F ,0xFF ,0x02 ,0x03 ,0 ,0x2B ,0,0,0 ,0x09 ,0 ,0 ,0 ,1}; //SpmSubState IGNITION on
      U8 Buf2[15] = {0x0F ,0xFF ,0x02 ,0x03 ,0 ,0x2B ,0,0,0 ,0x08 ,0 ,0 ,0 ,1}; //SpmSubState ACCESSORY on
	   TR_core_s32SendCmd(Buf1,14);
	   sleep(1);
	   TR_core_s32SendCmd(Buf2,14);
    }
   pthread_mutex_unlock(&accState_mutex);
   return NULL;
}

/********************************************************************************
   * FUNCTION		: lsim_inc_filter_init() 
   
   * PARAMETER 	:screenWd, screenHt
                                                         
   * RETURNVALUE	: -1 on error, 0 on success. 
   
   * DESCRIPTION	: Init LSIM INC filter
**********************************************************************************/
int lsim_inc_filter_init(unsigned int screenWd, unsigned int screenHt)
{

   LSIMwidth=screenWd;
   LSIMheight=screenHt;

   struct mq_attr mqa = {0,MSQ_MAXMESG,MSQ_MSGSIZE,0} ; ;

   dbgprintf("%s MsgQueue:Init Start..\n",__func__);
   if((ERROR)==(mqh = mq_open(MSQ_NAME, O_CREAT|O_WRONLY, S_IRUSR|S_IWUSR, &mqa)))
   {
      errprintf("MsgQueue:Create Failed with 0x%X\n",mqh);
      return ERROR;
   }
   dbgprintf("MsgQueue:Create result 0x%X\n",mqh);
   if ( (ERROR) != mq_getattr(mqh,&mqa))
   dbgprintf("MQUEUE:Attr:0x%lX | %ld | %ld | %ld \n",
   mqa.mq_flags,mqa.mq_maxmsg,mqa.mq_msgsize,mqa.mq_curmsgs);

   dbgprintf(" %s MsgQueue:Init Done!\n\n",__func__);

   if (preparefacia_info()== ERROR)
   {
      errprintf("preparefacia_info failed\n");
      return ERROR;
   }
   dbgprintf("lsim_inc_filter_init Success!!!!\n");
   return 0;
}
/********************************************************************************
   * FUNCTION		: SendToMQ() 
   
   * PARAMETER 	:input_event* ev
                                                         
   * RETURNVALUE	:None
   
   * DESCRIPTION	: Send message to message queue
**********************************************************************************/


static void SendToMQ(input_event* ev)
{
	mq_attr attr;
if (mq_getattr(mqh,&attr)==0 && attr.mq_curmsgs < attr.mq_maxmsg)
{
   if( (ERROR) == mq_send(mqh,(const char *)ev,sizeof(struct input_event),
            MSQ_PRIORITY))
   {
      errprintf("MQSEND:Error Posting Msq @ 0x%X\n",mqh);
   }
   else
   {
      dbgprintf("MQSENT:@%ld:%ld TYPE=%u CODE=%u VAL=%d\n",
      ev->time.tv_sec,ev->time.tv_usec,ev->type,ev->code,ev->value);

      char ttt[200];
      char ccc[200];
      char vvv[200];






	switch (ev->type) {
	case EV_KEY:
		strcpy(ttt,"EV_KEY");
		break;
	case EV_ABS:
			strcpy(ttt,"EV_ABS");
			break;
	case EV_REL:
			strcpy(ttt,"EV_REL");
			break;
	case EV_SYN:
			strcpy(ttt,"EV_SYN");
			break;
	default:
		sprintf(ttt,"%u",ev->type);
		break;
	}

	switch (ev->code) {
//		case EV_REL_AXIS_CODE:
//			strcpy(ccc, "EV_REL_AXIS_CODE");
//			break;
		case ABS_Y:
			strcpy(ccc, "ABS_Y");
			break;
		case ABS_X:
			strcpy(ccc, "ABS_X");
			break;
		case BTN_LEFT:
			strcpy(ccc, "BTN_LEFT");
			break;
		case BTN_TOUCH:
			strcpy(ccc, "BTN_TOUCH");
			break;
		default:
			sprintf(ccc, "%u", ev->code);
			break;
		}

	switch (ev->value) {
//		case EV_KEY_REPEAT_CODE:
//			strcpy(ccc,"EV_KEY_REPEAT_CODE");
//			break;
		default:
			sprintf(vvv,"%d",ev->value);
			break;
	}






	dbgprintf("MQSENTC:@%ld:%ld TYPE=%s CODE=%s VAL=%s\n",		ev->time.tv_sec,ev->time.tv_usec,ttt,ccc,vvv);
	}
	}

}



	/*
* static int cursor_faciablock()
*
* This function calculates the current facia
* block based on the current X Y co-ordinates.
*
* Mapping: ( X,Y ) co-ordinates to layer co-ordinates
*
* (X,Y) = (0,0)
*   (-1,1) ________________________ (1,1)
*         |           |            |
*         |           |            |
*         |   (-,+)   |   (+,+)    |
*         |           |            |
*         |___________|____________|
*         |           |            |
*         |           |            |
*         |   (-,-)   |    (+,-)   |
*         |           |            |
*  (-1,-1)|___________|____________|(1,-1)
*                             (X,Y) = ( MAX_WIDTH,MAX_HEIGHT)
*
*
*/


/********************************************************************************
	* FUNCTION		: cursor_faciablock() 
	
	* PARAMETER 	:	None
																			
	* RETURNVALUE	: Block number , -1 on error
	
	* DESCRIPTION	: Find out the current block of the facia
**********************************************************************************/
static int cursor_faciablock()
{

	int fblock=gfacia_blk_count;
	Keypos* position;
	/*float fgPointerX = ((float)(2*(gPointerX))/(currentimage.width))-1;
	float fgPointerY = -(((float)(2*(gPointerY))/(currentimage.height))-1);

	float fgPointerX = ((float)(2*(gPointerX))/(1024))-1;
	float fgPointerY = -(((float)(2*(gPointerY))/(768))-1);*/
	float fgPointerX = REMAP(gPointerX,currentimage.width);
	float fgPointerY = -(REMAP(gPointerY,currentimage.height));

	dbgprintf("cursor_faciablock @\t %f x %f \n",fgPointerX,fgPointerY);
	
	for( fblock= gfacia_blk_count ; fblock >= 0;fblock-- )
	{

		position=(Keypos*)&faciablk_xmldata[fblock].ButtonPos;
		
		if(((position->Left < fgPointerX)&&(fgPointerX < position->Right))
				&& ((position->Top > fgPointerY)&&(fgPointerY > position->Bottom)))
		{
			dbgprintf("FACIABLOCK:%d @ [ %f, %f, %f, %f]\n",faciablk_xmldata[fblock].Block,
			position->Left,position->Top,position->Right,position->Bottom);
			return fblock;
		}
#ifdef VERBOSEDBG
		else
		{
			dbgprintf("NOT-IN FACIABLOCK:%d @ ![ %f < [%f] < %f | %f > [%f] > %f]\n",fblock,
			position->Left,
			fgPointerX,
			position->Right,
			position->Top,
			fgPointerY,
			position->Bottom);
		}
#endif
	}

	return 0;

}

/********************************************************************************
	* FUNCTION		: keymap_faciablock() 
	
	* PARAMETER 	:input_event* ev
																			
	* RETURNVALUE	: current block or -1 on error
	
	* DESCRIPTION	: find out the current block of perticular key
**********************************************************************************/
static int keymap_faciablock(input_event* ev)
{

	int fblock=gfacia_blk_count;
	for( fblock=gfacia_blk_count; fblock>=0;fblock-- )
	{
		if(faciablk_xmldata[fblock].Key == ev->code)
		{
			dbgprintf("FACIABLOCK: KBD BLK %d, Keycode:%d\n ",fblock,faciablk_xmldata[fblock].KeyCode);
			return fblock;
		}
	}

	return ERROR;

}
/********************************************************************************
	* FUNCTION		: FakeUpdateTouch() 
	
	* PARAMETER 	:bool bstate
																			
	* RETURNVALUE	: None
	
	* DESCRIPTION	: send message for touch
**********************************************************************************/
void FakeUpdateTouch(bool bstate)
{
    /*TODO: call    FakePointerHandleButton(NULL, NULL, 0,0, BTN_TOUCH,state) */
    struct input_event ev = {{0,0},0,0,0};

    ev.type=EV_ABS;
    ev.code=ABS_X;
    ev.value=gPointerX;
    SendToMQ(&ev);

    ev.type=EV_ABS;
    ev.code=ABS_Y;
    ev.value=gPointerY;
    SendToMQ(&ev);

    //mel1hi: Set btn after position to avoid send the updates for wrong positions
    ev.type=EV_KEY; /*Till Touch support in Wayland*/
    //ev.code=BTN_TOUCH;
    ev.code=BTN_LEFT;
    ev.value=bstate;

    SendToMQ(&ev);

    gtouchpendown=bstate;
    dbgprintf("%s:>>Fake Update Touch Pen %s\n",__func__,(bstate)?"DOWN":"UP");
}

/********************************************************************************
	* FUNCTION		: FakePointerHandleMotion() 
	
	* PARAMETER 	:void* data, struct wl_pointer* wlPointer, uint32_t time,
									wl_fixed_t sx, wl_fixed_t sy
																			
	* RETURNVALUE	: None
	
	* DESCRIPTION	: callback to handle the motion
**********************************************************************************/
void FakePointerHandleMotion(void* data, struct wl_pointer* wlPointer, uint32_t time,
wl_fixed_t sx, wl_fixed_t sy)
{
	WL_UNUSED(data);
	WL_UNUSED(wlPointer);
	WL_UNUSED(time);
	gPointerX = (int)wl_fixed_to_double(sx);
	gPointerY = (int)wl_fixed_to_double(sy);
	struct input_event ev = {{0,0},0,0,0};
	static int savedBlock = 0;
	Keypos* position;
	static int previousblock;

	//dbgprintf("ENTER PointerHandleMotion: data(%d) wlPointer(%d) time(%d) x(%d), y(%d)\n",
	//(int)data, (int)wlPointer, time, PointerX, PointerY);

	//
	/*	float fPointerX = ((float)(2*(gPointerX))/(1024))-1;
		float fPointerY = -(((float)(2*(gPointerY))/(768))-1);*/

	float fPointerX = REMAP(gPointerX,LSIMwidth);
	float fPointerY = -(REMAP(gPointerY,LSIMheight));
	dbgprintf("Remapped size x:%f, y:%f \n",fPointerX,fPointerY);

	WLcbCursorPos(fPointerX, fPointerY, gpendown);

  
	int cblock = cursor_faciablock();
  
	dbgprintf("ENTER PointerHandleMotion:\tx(%8d)\ty(%8d) \t @BLOCK: %d:PEN:%s:TOUCHPEN:%s\n",
	gPointerX, gPointerY,cblock,(gpendown)?"DOWN":"UP",(gtouchpendown)?"DOWN":"UP");





	if (faciablk_xmldata[cblock].Block == OUT_OF_SCREEN && savedBlock==1 &&
			faciablk_xmldata[cblock].Block != FACIA_SCREEN && faciablk_xmldata[savedBlock].Block != TOUCH_SCREEN )
	{
		position=(Keypos*)&faciablk_xmldata[cblock].ButtonPos;
		if(g_ButNo==0)
		{
			WLcbHighlightRegion(position,MOUSE_HOVER_OUTOFREGION,faciablk_xmldata[cblock].KeyCode);
		}
		else
		{
			WLcbHighlightRegion(position,MOUSE_HOVER_BUTTONREGION,0);
		}
		savedBlock =0;
		dbgprintf("CLEAR SCREEN BLOCK:%d FLAG:%d \n",faciablk_xmldata[cblock].Block,savedBlock);
	}



   if(faciablk_xmldata[cblock].Block != OUT_OF_SCREEN && 
         faciablk_xmldata[cblock].Block != FACIA_SCREEN && 
         faciablk_xmldata[cblock].Block != TOUCH_SCREEN &&
         faciablk_xmldata[cblock].Block != previousblock)
   {
      //dbgprintf("current block\n");
      position=(Keypos*)&faciablk_xmldata[cblock].ButtonPos;
      
      WLcbHighlightRegion(position,1,faciablk_xmldata[cblock].KeyCode);
      
      savedBlock =1;
      dbgprintf("DRAW SCREEN BLOCK:%d FLAG:%d \n",faciablk_xmldata[cblock].Block,savedBlock);
   }

   if(faciablk_xmldata[cblock].Block == TOUCH_SCREEN)
   {
      dbgprintf("touch block\n");

      
      if(gpendown)
      {
         dbgprintf("pendown block\n");
         
         if(!gtouchpendown)
         {
            dbgprintf(" touchpendown pendown block\n");
            /*Report BTN_TOUCH pendown Event first touchpendown=true*/
            FakeUpdateTouch(true);
         }

         /*For X*/
         ev.type=EV_ABS;
         ev.code=ABS_X;
         //ev.value=gPointerX-faciablkdata[TOUCH_SCREEN].left;
         ev.value=gPointerX;
         SendToMQ(&ev);
         /*For Y*/
         ev.type=EV_ABS;
         ev.code=ABS_Y;
         //ev.value=gPointerY-faciablkdata[TOUCH_SCREEN].top;
         ev.value=gPointerY;
         SendToMQ(&ev);
      }
   }
   else if(gtouchpendown)
   {
      dbgprintf("touchpendown block\n");
      /*Report BTN_TOUCH penup Event touchpendown=false*/
      FakeUpdateTouch(false);
   }

   previousblock = faciablk_xmldata[cblock].Block;
   dbgprintf("Exit %s\n",__func__);
}

/********************************************************************************
	* FUNCTION		: FakePointerHandleButton() 
	
	* PARAMETER 	:void* data, struct wl_pointer* wlPointer, uint32_t serial,
									uint32_t time, uint32_t button, uint32_t state
																			
	* RETURNVALUE	: None
	
	* DESCRIPTION	: callback to handle the button press
**********************************************************************************/
void FakePointerHandleButton(void* data, struct wl_pointer* wlPointer, uint32_t serial,
uint32_t time, uint32_t button, uint32_t state)
{
   WL_UNUSED(data);
   WL_UNUSED(wlPointer);
   WL_UNUSED(serial);
   WL_UNUSED(time);
   WL_UNUSED(button);
   WL_UNUSED(state);
   struct input_event ev = {{0,0},0,0,0};
   Keypos* position;

   //dbgprintf("ENTER PointerHandleButton: data(%d) wlPointer(%d) serial(%d) time(%d) button(%d), state(%d)\n",
   //(int)data, (int)wlPointer, serial, time, button, state);
   ev.type=EV_KEY;
   ev.code=button;
   ev.value=state;
   dbgprintf("FakePointerHandleButton:\tev.type(%d)\tev.code(%d)\tev.value(%d)\n", ev.type, ev.code, ev.value);

   int currfblock=OUT_OF_SCREEN;
   bool sendkey=true;

   if(ev.code==BTN_LEFT)
   {
      gpendown=!!ev.value;
      
      currfblock 	= cursor_faciablock();

      if(faciablk_xmldata[currfblock].Block == TOUCH_SCREEN)
      {
         dbgprintf("FakePointerHandleButton: On TouchScreen\n");

         FakeUpdateTouch(ev.value);
      }

      if(faciablk_xmldata[currfblock].Block != OUT_OF_SCREEN &&
            faciablk_xmldata[currfblock].Block != FACIA_SCREEN &&
            faciablk_xmldata[currfblock].Block != TOUCH_SCREEN)
      {
         position=(Keypos*)&faciablk_xmldata[currfblock].ButtonPos;
         ev.code=faciablk_xmldata[currfblock].KeyCode;
    	 dbgprintf("FakePointerHandleButton: HardKey Message KeyCode (%d, 0x%X)\n", ev.code, ev.code);
   if(faciablk_xmldata[currfblock].IsPower && ev.value==0)
         {
        if (pthread_mutex_init(&accState_mutex, NULL) == 0)
      {
           dbgprintf("\n mutex init successfull for ACC button\n");
		   pthread_create(&accThreadId, NULL, executeSPMCommand, NULL);
    	   pthread_mutex_destroy(&accState_mutex);
    	}
		else{
				    dbgprintf("\n Mutex initialisation is not successful for ACC button");
			     }
         }
         if(g_bmultykeypressactive == true)
         {
            if( ev.value==KEY_PRESS)
            {
               if(!Save_key_info(&ev))
               {
                  ev.value=0;
               }

               SendToMQ(&ev);
            }

         }
         else
         {
            SendToMQ(&ev);

         }
         if(state == KEY_PRESS)
         {
            // Save_Button_info(position,KEY_PRESS_DETECT,ev.code);
            WLcbHighlightRegion(position,MOUSE_HOVER_BUTTONPRESS,ev.code);
         }
         else
         {
            //Save_Button_info(position,KEY_REGION_DETECT,ev.code);
            WLcbHighlightRegion(position,MOUSE_HOVER_BUTTONREGION,ev.code);
         }
         
         
      }
      
   }
   //dbgprintf("Exit %s\n",__func__);
}

/********************************************************************************
	* FUNCTION		: FakePointerHandleAxis() 
	
	* PARAMETER 	:void* data, struct wl_pointer* wlPointer, uint32_t time,
									uint32_t axis, wl_fixed_t value
																			
	* RETURNVALUE	: None
	
	* DESCRIPTION	: callback to handle axis
**********************************************************************************/
void FakePointerHandleAxis(void* data, struct wl_pointer* wlPointer, uint32_t time,
uint32_t axis, wl_fixed_t value)
{
	WL_UNUSED(data);
	WL_UNUSED(wlPointer);
	WL_UNUSED(time);

	struct input_event ev = {{0,0},0,0,0};
	int currfblock;

	dbgprintf("ENTER PointerHandleAxis:\taxis(%d)\tvalue(%d)\n", axis, wl_fixed_to_int(value));

	ev.type=EV_REL;
	ev.value=wl_fixed_to_int(value);

	currfblock = cursor_faciablock();

	if(faciablk_xmldata[currfblock].Axis != -1)
	{
		switch(faciablk_xmldata[currfblock].Axis)
		{
         case REL_HWHEEL:
         case REL_DIAL:
         case REL_WHEEL:
         case REL_MISC:
            ev.code = faciablk_xmldata[currfblock].Axis;
            break;
         default:
            errprintf(" PointerHandleAxis: Wheel Axis %d not supported \n", faciablk_xmldata[currfblock].Axis);
            return;
		}

		SendToMQ(&ev);

	} 
   else 
   {
		dbgprintf(" PointerHandleAxis: ignore axis motion at facia block %d\n", currfblock);
	}

	dbgprintf("Exit %s\n",__func__);
}

/********************************************************************************
	* FUNCTION		: FakeKeyboardHandleKey() 
	
	* PARAMETER 	:void* data, struct wl_keyboard* keyboard, uint32_t serial,
									uint32_t time, uint32_t key, uint32_t state_w
																			
	* RETURNVALUE	:None
	
	* DESCRIPTION	: callback to handle the key board keys
**********************************************************************************/
void FakeKeyboardHandleKey(void* data, struct wl_keyboard* keyboard, uint32_t serial,
uint32_t time, uint32_t key, uint32_t state_w)
{
   WL_UNUSED(data);
   WL_UNUSED(keyboard);
   WL_UNUSED(serial);
   WL_UNUSED(time);
   WL_UNUSED(key);
   WL_UNUSED(state_w);

   struct input_event ev = {{0,0},0,0,0};
   int currfblock = OUT_OF_SCREEN;
   bool sendkey = true;
   Keypos* position = 0;


   // dbgprintf("ENTER KeyboardHandleKey: data(%d) wl_keyboard(%d) serial(%d), time(%d), key(%d), state_w(%d)\n",
   //     (int)data, (int)keyboard, serial, time, key, state_w);
   dbgprintf("ENTER KeyboardHandleKey:\tkey(%8d)\tstate_w(%8d)\n",
   key, state_w);


   ev.type=EV_KEY;
   ev.code=key;
   ev.value=state_w;
   
   if(key == CNTRL_KEY && state_w == KEY_PRESS)
   {
      g_bmultykeypressactive=true;
      return;
   }
   else if(key == CNTRL_KEY && state_w == KEY_RELEASE)
   {
      g_bmultykeypressactive=false;
      
      for(int i=0;i<g_KeyNo;i++)
      {

         ev.type  =g_inpEvent[i].type ;
         ev.code  =g_inpEvent[i].code;
         ev.value =0;
         SendToMQ(&ev);
         
      }
      g_KeyNo=0;
      /*Send the Control key release info to GUI for facia updation*/
      WLcbHighlightRegion(position,state_w,CNTRL_KEY);
      return;
   }
   currfblock 	= keymap_faciablock(&ev);

   if(faciablk_xmldata[currfblock].Block != OUT_OF_SCREEN &&
         faciablk_xmldata[currfblock].Block != FACIA_SCREEN && 
         faciablk_xmldata[currfblock].Block != TOUCH_SCREEN)
   {
      position=(Keypos*)&faciablk_xmldata[currfblock].ButtonPos;
      ev.code=faciablk_xmldata[currfblock].KeyCode;
      
      if(g_bmultykeypressactive == true)
      {
         if( ev.value==KEY_PRESS)
         {
            if(!Save_key_info(&ev))
            {
               /*if the key is already pressed second press will consider as a key release*/ 
               ev.value=0;
            }
            /*only press information is send while multykey scenario*/
            SendToMQ(&ev);
         }
         
      }
      else
      {
         SendToMQ(&ev);
         
      }
      if(state_w == KEY_PRESS)
      {
         // Save_Button_info(position,KEY_PRESS_DETECT,ev.code);
         WLcbHighlightRegion(position,MOUSE_HOVER_BUTTONPRESS,ev.code);
      }
      else
      {
         //Save_Button_info(position,KEY_REGION_DETECT,ev.code);
         WLcbHighlightRegion(position,MOUSE_HOVER_BUTTONREGION,ev.code);
      }
   }

   dbgprintf("Exit %s\n",__func__);
}
/********************************************************************************
	* FUNCTION		: Save_key_info() 
	
	* PARAMETER 	:input_event* ev
																			
	* RETURNVALUE	:None
	
	* DESCRIPTION	: Save or Remove the pressed key info
**********************************************************************************/
int Save_key_info( input_event* ev)
{
   int Numkeys;

   if(g_KeyNo>0)
   {
      /*remove already pressed key from structure*/
      for(Numkeys=0;Numkeys<g_KeyNo;Numkeys++)
      {
         if(ev->code == g_inpEvent[Numkeys].code)
         {
            
            for(int i=Numkeys;i<g_KeyNo;i++)
            {
               g_inpEvent[i] = g_inpEvent[i+1];

            }
            g_KeyNo--;
            
            return 0;
         }
      }

   }
   /*save the key info*/
   if(ev->value)
   {
      g_inpEvent[g_KeyNo].type= ev->type;
      g_inpEvent[g_KeyNo].code = ev->code;
      g_inpEvent[g_KeyNo].value = ev->value;
      if(g_KeyNo<50)
      {
         g_KeyNo++;
      }
   }


   return 1;

}

#ifdef __cplusplus
}
#endif
