
//#include <string.h>
//#include <fcntl.h>
//#include <unistd.h>
#include <stdio.h>

//#include <sys/select.h>
//#include <sys/time.h>
//#include <sys/types.h>

//#ifndef EV_SYN
//#define EV_SYN 0
//#endif

//#include <sys/stat.h>
#include <mqueue.h>
#include <time.h>
//#include <sys/time.h>
//#include <sys/wait.h>
#include <linux/input.h>
#include <stdlib.h>
#include <libudev.h>
#include <pthread.h>
#include "ilm_client.h"
#include "facia_fake_wl_input_event.h"
#include "facia_xml_parser.h"


#ifdef __cplusplus
extern "C" {
#endif 

	//********************************************************************
struct input_event_data{
    struct input_event ev;
    int abs_max ;
};

#define EV_KEY_REPEAT_CODE 2
#define EV_KEY_MOUSE_CODE_START 272 /* 272:Left, 273:Right, 274:Middle, 275:Side, 276:Extra */
#define EV_KEY_MOUSE_CODE_END 276
#define EV_REL_AXIS_CODE 8
#define LSIM_FILTER_MQ_NAME "/lsim_filter"
#define LSIM_FILTER_MQ_MESSAGE_PRIORITY 0
#define LSIM_FILTER_MQ_MAX_NUM_OF_MESSAGES 1000

enum {
	EV_KEYBOARD_KEY, EV_MOUSE_KEY, EV_MOUSE_POSITION, EV_MOUSE_WHEEL
};

void send_event_to_wayland(struct input_event_data *ev_data);
mqd_t message_queue_open(void);
int message_queue_receive(mqd_t mqd, struct input_event_data *ev);
void* fake_gui_thread(void *arg);

	
//********************************************************************


int fake_wl_input_event_init()
{
	pthread_t thread;

	if (pthread_create(&thread, NULL, fake_gui_thread, NULL)) {
		errprintf("fake gui thread creation failed!");
		return -1;
	}
	
	return 0;
}

void* fake_gui_thread(void *arg)
{	
	mqd_t mqd;
	struct input_event_data ev;
	int ret;

	mqd = message_queue_open();
	if ((int)mqd < 0)
	{
		errprintf("Messgae queue open failed...you might not have started filter \n");
		exit(-1);
	}

	while(1) {
		ret = message_queue_receive(mqd, &ev);
		if (!ret)
		{
			send_event_to_wayland(&ev);
		}
	}
	return NULL;
}

int message_queue_receive(mqd_t mqd, struct input_event_data *ev)
{	
	int msg_len;
	msg_len = mq_receive(mqd, (char *)ev, sizeof(struct input_event_data), NULL);
	if(msg_len < 0)
	{
		errprintf("Message queue Failed");
		return -1;
	}
	else if (msg_len < sizeof(struct input_event_data))
	{
		errprintf("Error: Message size is too less");
		return -1;
	}

	return 0;
}

mqd_t message_queue_open()
{
	mqd_t mqd;
	/* Open message queue */
	mqd = mq_open(LSIM_FILTER_MQ_NAME, O_RDONLY);

	if( mqd != (mqd_t)-1 )
	{
		errprintf(" Message Queue Opened %d  \n", (int)mqd);
		return(mqd);
	}
	else
	{
		errprintf(" Message queue open failed ");
		return (mqd_t)-1;
	}
}

void send_event_to_wayland(struct input_event_data *ev_data)
{
	static uint32_t key_serial = 0;
	static uint32_t mouse_key_serial = 0;
	static int send_x=0;
	static int send_y=0;
	static int send_axis_motion=0;
	static int send_type = -1;
	static uint32_t send_key;
	static uint32_t send_value;
	uint32_t time;
	unsigned int screenWidth;
    unsigned int screenHeight;

	ilm_getScreenResolution (0, &screenWidth, &screenHeight);	
	errprintf("Screen Resolution Width : %d Height : %d \n", screenWidth, screenHeight);

	if (ev_data->ev.type == EV_KEY)	{
		if( ev_data->ev.value != EV_KEY_REPEAT_CODE)
		{
			send_key = ev_data->ev.code;
			send_value = ev_data->ev.value;
			if ( send_key >= EV_KEY_MOUSE_CODE_START && send_key <= EV_KEY_MOUSE_CODE_END)
			send_type = EV_MOUSE_KEY;
			else
			send_type = EV_KEYBOARD_KEY;
		}
	} else if (ev_data->ev.type == EV_ABS) {
		send_type = EV_MOUSE_POSITION;
		if((ev_data->ev.code == 0)&&(ev_data->abs_max)) //x
		send_x = (ev_data->ev.value)*screenWidth/ev_data->abs_max; 
		else if (( ev_data->ev.code == 1)&&(ev_data->abs_max)) //y
		send_y = (ev_data->ev.value)*screenHeight/ev_data->abs_max;
		
	} else if (ev_data->ev.type == EV_REL) {
		if( ev_data->ev.code == EV_REL_AXIS_CODE) {
			send_axis_motion = ev_data->ev.value;
			send_type = EV_MOUSE_WHEEL;
		} else {
			errprintf("------ Ignoring EV_REL code %d --------\n", ev_data->ev.code);
		}
	} else if (ev_data->ev.type == EV_SYN) {
		time = ev_data->ev.time.tv_sec + ev_data->ev.time.tv_usec/1000;
		switch (send_type) {
		case EV_KEYBOARD_KEY:
			FakeKeyboardHandleKey(NULL, NULL, ++key_serial, time, send_key, send_value);
			break;
		case EV_MOUSE_POSITION:
			FakePointerHandleMotion(NULL, NULL, time, wl_fixed_from_int(send_x),  wl_fixed_from_int(send_y));
			break;
		case EV_MOUSE_KEY:
			FakePointerHandleButton(NULL, NULL, ++mouse_key_serial, time, send_key, send_value);
			break;
		case EV_MOUSE_WHEEL:
			FakePointerHandleAxis(NULL, NULL, time, 0, wl_fixed_from_int(send_axis_motion));
			break;
		default:
			//errprintf("------ Ignoring sync --------\n");
			break;
		}
		send_type = -1;
	} else {
		//errprintf("------ Ignoring event type %d --------\n", ev->type);
	}	
	return;
}

#ifdef __cplusplus
}
#endif

