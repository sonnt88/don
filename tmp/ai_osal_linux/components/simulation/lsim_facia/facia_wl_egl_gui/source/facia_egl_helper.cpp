/**************************************************************************
*
* Copyright 2010, 2011 BMW Car IT GmbH 
* Copyright (C) 2011 DENSO CORPORATION and Robert Bosch Car Multimedia Gmbh
*
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
****************************************************************************/
#include "facia_egl_helper.h"
#include "ExampleAppIds.h" /* changed by gs77hi, original file "LayerScene.h"
						was replaced by "ExampleAppIds.h" when 
						SceneProvider was moved to LM Plugin Directory
						in ILM version 0.99 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <stdint.h>
#include <signal.h>
#include <sys/stat.h>
#include <linux/fb.h>

#include "WaylandServerinfoClientProtocol.h"
#include "facia_xml_parser.h"

/* #define USE_PIXMAP */
typedef struct t_eglContextStruct
{
	EGLNativeDisplayType nativeDisplay;
	EGLNativeWindowType  nativeWindow;
	EGLDisplay eglDisplay;
	EGLConfig eglConfig;
	EGLSurface eglSurface;
	EGLContext eglContext;
} EglContextStruct;

typedef struct t_wlContextStruct
{
	struct wl_display* wlDisplay;
	struct wl_registry* wlRegistry;
	struct wl_compositor* wlCompositor;

	struct wl_surface* wlSurface;
	struct wl_egl_window* wlNativeWindow;
	int width;
	int height;

	struct serverinfo* wlExtServerinfo;
	uint32_t connect_id;
} WLContextStruct;

static EglContextStruct g_eglContextStruct[2]={0};
static WLContextStruct g_wlContextStruct[2]={0};

static void serverinfoListener(void *data, struct serverinfo *pServerinfo, uint32_t client_handle)
{
	pServerinfo = pServerinfo; // TODO:to avoid warning
	WLContextStruct* p_wlCtx = (WLContextStruct*)data;
	p_wlCtx->connect_id = client_handle;
	fprintf( stderr, "notified wayland connection : id=%d\n", p_wlCtx->connect_id);
}

struct serverinfo_listener serverinfo_listener_list = {
	serverinfoListener
};

static void registry_handle_global(void* data, struct wl_registry* registry, uint32_t name, const char* interface, uint32_t version)
{
	version = version; // TODO: to avoid warning
	WLContextStruct* p_wlCtx = (WLContextStruct*)data;
	int ans_strcmp = 0;

	do
	{
		ans_strcmp = strcmp(interface, "wl_compositor");
		if (0 == ans_strcmp)
		{
			p_wlCtx->wlCompositor = (wl_compositor*)wl_registry_bind(registry, name, &wl_compositor_interface, 1);
			break;
		}

		ans_strcmp = strcmp(interface, "serverinfo");
		if (0 == ans_strcmp)
		{
			p_wlCtx->wlExtServerinfo = (struct serverinfo*)wl_registry_bind(registry, name, &serverinfo_interface, 1);
			serverinfo_add_listener(p_wlCtx->wlExtServerinfo, &serverinfo_listener_list, data);
			serverinfo_get_connection_id(p_wlCtx->wlExtServerinfo);
		}
	} while(0);
}

static const struct wl_registry_listener registry_listener = {
	registry_handle_global,
	NULL
};

t_ilm_bool createWLContext(t_ilm_int width, t_ilm_int height, t_ilm_int CntxNo)
{
	t_ilm_bool result = ILM_TRUE;

	memset(&g_wlContextStruct[CntxNo], 0, sizeof(g_wlContextStruct[CntxNo]));

	g_wlContextStruct[CntxNo].width = width;
	g_wlContextStruct[CntxNo].height = height;
	g_wlContextStruct[CntxNo].wlDisplay = wl_display_connect(NULL);
	if (NULL == g_wlContextStruct[CntxNo].wlDisplay)
	{
		dbgprintf("Error: wl_display_connect() failed.\n");
	}

	g_wlContextStruct[CntxNo].wlRegistry = wl_display_get_registry(g_wlContextStruct[CntxNo].wlDisplay);
	wl_registry_add_listener(g_wlContextStruct[CntxNo].wlRegistry, &registry_listener, &g_wlContextStruct[CntxNo]);
	wl_display_dispatch(g_wlContextStruct[CntxNo].wlDisplay);
	wl_display_roundtrip(g_wlContextStruct[CntxNo].wlDisplay);

	g_wlContextStruct[CntxNo].wlSurface = wl_compositor_create_surface(g_wlContextStruct[CntxNo].wlCompositor);
	if (NULL == g_wlContextStruct[CntxNo].wlSurface)
	{
		dbgprintf("Error: wl_compositor_create_surface() failed.\n");
		destroyWLContext(CntxNo);
	}

	g_wlContextStruct[CntxNo].wlNativeWindow = wl_egl_window_create(g_wlContextStruct[CntxNo].wlSurface, width, height);
	if (NULL == g_wlContextStruct[CntxNo].wlNativeWindow)
	{
		dbgprintf("Error: wl_egl_window_create() failed.\n");
		destroyWLContext(CntxNo);
	}

	return result;
}

EGLint contextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };

t_ilm_bool createEGLContext(t_ilm_int width, t_ilm_int height, t_ilm_int CntxNo)
{
	t_ilm_bool result = ILM_TRUE;
	g_eglContextStruct[CntxNo].eglDisplay = NULL;
	g_eglContextStruct[CntxNo].eglSurface = NULL;
	g_eglContextStruct[CntxNo].eglContext = NULL;
	ilmErrorTypes error = ILM_FAILED;
	uint32_t native_ilm_handle = 0;
	EGLint eglstatus = EGL_SUCCESS;
	t_ilm_layer *ilmLayers;
	t_ilm_int layerCount;
	t_ilm_int count;
	EGLint iMajorVersion, iMinorVersion;

	g_eglContextStruct[CntxNo].eglDisplay = eglGetDisplay(g_wlContextStruct[CntxNo].wlDisplay);
	eglstatus = eglGetError();
	if (!g_eglContextStruct[CntxNo].eglDisplay)
	{
		dbgprintf("Error: eglGetDisplay() failed.\n");
		return ILM_FAILED;
	}

	if (!eglInitialize(g_eglContextStruct[CntxNo].eglDisplay, &iMajorVersion,
				&iMinorVersion))
	{
		dbgprintf("Error: eglInitialize() failed.\n");
		return ILM_FAILED;
	}

	eglBindAPI(EGL_OPENGL_ES_API);
	eglstatus = eglGetError();
	if (eglstatus != EGL_SUCCESS)
	{
		dbgprintf("Error: eglBindAPI() failed.\n");
		return ILM_FAILED;
	}
	EGLint pi32ConfigAttribs[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_RED_SIZE,   8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE,  8,
		EGL_ALPHA_SIZE, 8,
		EGL_NONE
	};

	int iConfigs;

	if (!eglChooseConfig(g_eglContextStruct[CntxNo].eglDisplay, pi32ConfigAttribs, &g_eglContextStruct[CntxNo].eglConfig, 1, &iConfigs) || (iConfigs != 1))
	{
		dbgprintf("Error: eglChooseConfig() failed.\n");
		return ILM_FAILED;
	}

	eglstatus = eglGetError();
	if (eglstatus != EGL_SUCCESS)
	{
		dbgprintf("Error: eglChooseConfig() failed. EGLStatus:0x%x\n", (unsigned int)eglstatus); // TODO: do not use printf directly here
		return ILM_FAILED;
	}

	g_eglContextStruct[CntxNo].eglSurface = eglCreateWindowSurface(
	g_eglContextStruct[CntxNo].eglDisplay, g_eglContextStruct[CntxNo].eglConfig,
	g_wlContextStruct[CntxNo].wlNativeWindow, NULL);
	eglstatus = eglGetError();

	if (eglstatus != EGL_SUCCESS)
	{
		dbgprintf("Error: eglCreateWindowSurface() failed. EGLStatus:0x%u\n", (unsigned int)eglstatus); // TODO: do not use printf directly here
		return ILM_FAILED;
	}

	g_eglContextStruct[CntxNo].eglContext = eglCreateContext(
	g_eglContextStruct[CntxNo].eglDisplay, g_eglContextStruct[CntxNo].eglConfig, NULL,
	contextAttribs);

	eglstatus = eglGetError();
	if (eglstatus != EGL_SUCCESS)
	{
		dbgprintf("Error: eglCreateContext() failed.\n");
		return ILM_FAILED;
	}

	eglMakeCurrent(g_eglContextStruct[CntxNo].eglDisplay,
	g_eglContextStruct[CntxNo].eglSurface, g_eglContextStruct[CntxNo].eglSurface,
	g_eglContextStruct[CntxNo].eglContext);

	eglstatus = eglGetError();

	if (eglstatus != EGL_SUCCESS)
	{
		dbgprintf("Error: eglMakeCurrent() failed.\n");
		return ILM_FAILED;
	}

	eglSwapInterval(g_eglContextStruct[CntxNo].eglDisplay, 1);
	eglstatus = eglGetError();

	if (eglstatus != EGL_SUCCESS)
	{
		dbgprintf("Error: eglSwapInterval() failed.\n");
		return ILM_FAILED;
	}

	t_ilm_layer layerId = 0;
	t_ilm_surface surfaceid = 0;

	// register surfaces to layermanager

	if (CntxNo == 0)
	{
		layerId = (t_ilm_layer)LAYER_LSIM_FACIA;
		surfaceid = (t_ilm_surface)SURFACE_LSIM_FACIA;
	}
	else if (CntxNo == 1)
	{
		layerId = (t_ilm_layer)LAYER_LSIM_MOUSE_CURSOR;
		surfaceid = (t_ilm_surface)SURFACE_LSIM_MOUSE_CURSOR;
	}

	if(layerId != 1)//FIXME instead check if the layer already created
	{
		error = ilm_layerCreateWithDimension(&layerId, width, height);

		if (error == ILM_FAILED)
		{
			dbgprintf("ERROR:Create layer\n");
			return ILM_FAILED;
		}
		else
		{
			dbgprintf("Layer created with id %d width %d height %d \n", layerId, width, height);
		}

		error = ilm_layerSetVisibility (layerId, ILM_TRUE);

		if (error == ILM_FAILED)
		{
			dbgprintf("ERROR: Can't make layer visible\n");
			return ILM_FAILED;
		}
		else
		{
			dbgprintf("Layer %d is made visible \n", layerId);
		}

		error = ilm_getLayerIDsOnScreen(DISPLAY_LSIM_FACIA, &layerCount, &ilmLayers);

		if (error == ILM_FAILED)
		{
			dbgprintf("ERROR: ilm_getLayerIDsOnScreen failed \n");
			return ILM_FAILED;
		}
		else
		{
			t_ilm_layer ilmLayersNew[1000];
			/* Mouse layer should be always on the top */
			if (layerId == LAYER_LSIM_MOUSE_CURSOR)
			{
				for(count=0; count<layerCount; count++)
				{
					dbgprintf("### Layers %d \n", (int)ilmLayers[count]);
					ilmLayersNew[count] = ilmLayers[count];
				}
				ilmLayersNew[count] = layerId;
			}
			else
			{ /* Facia layer should be always at bottom */
				for(count=0; count<layerCount; count++)
				{
					dbgprintf("### Layers %d \n", (int)ilmLayers[count]);
					ilmLayersNew[count+1] = ilmLayers[count];
				}
				ilmLayersNew[0] = layerId;
			}
			ilm_displaySetRenderOrder(DISPLAY_LSIM_FACIA, ilmLayersNew, layerCount+1);
		}
	}

	// TODO: auto generate surface id
	
	struct wl_proxy* p_proxy = (struct wl_proxy*)g_wlContextStruct[CntxNo].wlSurface;
	uint32_t id = wl_proxy_get_id(p_proxy);   
	native_ilm_handle = (g_wlContextStruct[CntxNo].connect_id << 16) | (uint32_t)id;

	
	error = ilm_surfaceCreate( (t_ilm_nativehandle) native_ilm_handle, width, height,
	ILM_PIXELFORMAT_RGBA_8888, &surfaceid);

	if (error == ILM_FAILED)
	{
		dbgprintf("ERROR: create a surface\n");
		return ILM_FAILED;

	}

	dbgprintf("set surface dest region\n");
	error = ilm_surfaceSetDestinationRectangle(surfaceid, 0, 0, width, height);
	if (error == ILM_FAILED)
	{
		dbgprintf("ERROR: set surface destination rectangle\n");
		return ILM_FAILED;
	}

	dbgprintf("set surface src region\n");

	error = ilm_surfaceSetSourceRectangle(surfaceid, 0, 0, width, height);

	if (error == ILM_FAILED)
	{
		dbgprintf("ilm_commitChanges failed\n");
		return ILM_FALSE;
	}

	dbgprintf("add surface to layer\n");
	error = ilm_layerAddSurface(layerId, surfaceid);

	if (error == ILM_FAILED)
	{
		dbgprintf("ilm_commitChanges failed\n");
		return ILM_FALSE;
	}

	dbgprintf("Set surface visible\n");
	error = ilm_surfaceSetVisibility(surfaceid, ILM_TRUE);

	if (error == ILM_FAILED)
	{
		dbgprintf("ilm_commitChanges failed\n");
		return ILM_FALSE;
	}

	dbgprintf("Set surface opacity\n");
	error = ilm_surfaceSetOpacity(surfaceid, 0.75f);

	if (error == ILM_FAILED)
	{
		dbgprintf("ilm_commitChanges failed\n");
		return ILM_FALSE;
	}

	dbgprintf("Set surface to not accept wayland events\n");
	error = ilm_UpdateInputEventAcceptanceOn(surfaceid,
                                                 ILM_INPUT_DEVICE_POINTER |
                                                 ILM_INPUT_DEVICE_TOUCH |
                                                 ILM_INPUT_DEVICE_KEYBOARD,
                                                 ILM_FALSE);

	if (error == ILM_FAILED)
	{
		dbgprintf("ERROR: set surface to not accept wayland events \n");
		return ILM_FAILED;
	}

	dbgprintf("ilm commit changes\n");
	error = ilm_commitChanges();

	if (error == ILM_FAILED) 
	{
		dbgprintf("ERROR: ilm_commitChanges failed\n");
		return ILM_FAILED;
	}

	return result;
}

void destroyEglContext( int CntxNo)
{
	if (g_eglContextStruct[CntxNo].eglDisplay != NULL)
	{
		eglMakeCurrent(g_eglContextStruct[CntxNo].eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		eglTerminate(g_eglContextStruct[CntxNo].eglDisplay);
	}
}

void destroyWLContext(int CntxNo)
{
	if (g_wlContextStruct[CntxNo].wlNativeWindow)
	{
		wl_egl_window_destroy(g_wlContextStruct[CntxNo].wlNativeWindow);
	}
	if (g_wlContextStruct[CntxNo].wlSurface)
	{
		wl_surface_destroy(g_wlContextStruct[CntxNo].wlSurface);
	}
	if (g_wlContextStruct[CntxNo].wlCompositor)
	{
		wl_compositor_destroy(g_wlContextStruct[CntxNo].wlCompositor);
	}
}

t_ilm_uint GetTickCount()
{
	struct timeval ts;
	gettimeofday(&ts, 0);
	return (t_ilm_uint) (ts.tv_sec * 1000 + (ts.tv_usec / 1000));
}

static void frame_listener_func(void *data, struct wl_callback *callback, uint32_t time)
{
	dbgprintf("** frame_listener_func: wl_callback:%x Enter \n", (unsigned int)callback);
	data = data; // TODO:to avoid warning
	time = time; // TODO:to avoid warining
	if (callback)
	{
		wl_callback_destroy(callback);
	}
	dbgprintf("** frame_listener_func: wl_callback:%x Exit \n", (unsigned int)callback);
}

static const struct wl_callback_listener frame_listener = {
	frame_listener_func	
};

void swapBuffers(int CntxNo)
{
	dbgprintf("** swapBuffers: Context %d Enter\n", CntxNo);
	eglSwapBuffers(g_eglContextStruct[CntxNo].eglDisplay, g_eglContextStruct[CntxNo].eglSurface);
#if 0//not required with new wayland
	struct wl_callback* callback = wl_surface_frame(g_wlContextStruct[CntxNo].wlSurface);
	dbgprintf("** swapBuffers: wl_callback:%x \n", (unsigned int)callback);
	wl_callback_add_listener(callback, &frame_listener, NULL);
	wl_surface_commit(g_wlContextStruct[CntxNo].wlSurface);
#endif
	wl_display_flush(g_wlContextStruct[CntxNo].wlDisplay);
	dbgprintf("** swapBuffers: Context %d Exit \n", CntxNo);
}
