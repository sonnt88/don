/******************************************************************************
* FILE               : facia_gles2_app.h
*
* SW-COMPONENT       : lsim
*
* DESCRIPTION        : Header file for facia GUI part.
*
* HISTORY            :
*-----------------------------------------------------------------------------
* Date           |       Version        | Author & comments
*-----------|---------------|--------------------------------------------
*  02.09.2013 |   Initial version    |Sudharsanan Sivagnanam (RBEI/ECF5)
* -----------------------------------------------------------------------------
*******************************************************************************/

#ifndef _GLES2APPLICATION_H_
#define _GLES2APPLICATION_H_

#include "ilm_client.h"
#include <GLES2/gl2.h>
#include "facia_xml_parser.h"



t_ilm_bool InitFaciaGlApplication();
t_ilm_bool InitFaciaShader();
t_ilm_bool DestroyFaciaShader();
t_ilm_bool InitFaciaVertexBuffer();
t_ilm_bool InitButtonVertexBuffer();
void AttachFaciaVertexBuffer();
void AttachButtonVertexBuffer();
void DetachFaciaVertexBuffer();
void DestroyFaciaVertexBuffer();
void DestroyFaciaGlApplication();
void Draw_facia_image();
void Draw_facia();

void WLcbHighlightRegion(Keypos* position,int state,int keycode);


t_ilm_bool InitCursorGlApplication();
t_ilm_bool InitCursorShader();
t_ilm_bool DestroyCursorShader();
t_ilm_bool InitCursorVertexBuffer();
void AttachCursorVertexBuffer();
void DetachCursorVertexBuffer();
void DestroyCursorVertexBuffer();
void DestroyCursorGlApplication();

void Calculate_Cursor_Co_ordinates(float x,float y);
void WLcbCursorPos(float abs_x, float abs_y, int pendown);
void Draw_cursor();
void Cursor();
int GetBMPinfo(char * Path);
void GetImagesize(Resolution* size);
void Button_Highlight();
int Save_Button_info(Keypos* position,int button_state,int keycode);

typedef enum
{
	MOUSE_HOVER_OUTOFREGION=0,MOUSE_HOVER_BUTTONREGION,MOUSE_HOVER_BUTTONPRESS

}buttonregion;

enum IMAGETYPE
{
   IMG_INVALID = 0,
   IMG_BMP = 1,
   IMG_PNG = 2
};

#define CNTRL_KEY 29
#define ALT_KEY 56
#define KEY_PRESS 1
#define KEY_RELEASE 0




#endif /* _GLES2APPLICATION_H_ */

