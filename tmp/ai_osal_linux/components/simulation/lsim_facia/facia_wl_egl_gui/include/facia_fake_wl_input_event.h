
#ifndef FACIA_FAKE_WL_INPUT_EVENT
#define FACIA_FAKE_WL_INPUT_EVENT

#include <stdint.h>
#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif

#include <wayland-client.h>
#include <wayland-client-protocol.h>

#define WL_UNUSED(arg) ((void)(arg))

int fake_wl_input_event_init();

void
FakePointerHandleMotion(void* data, struct wl_pointer* wlPointer, uint32_t time,
                    wl_fixed_t sx, wl_fixed_t sy);
void
FakePointerHandleButton(void* data, struct wl_pointer* wlPointer, uint32_t serial,
                    uint32_t time, uint32_t button, uint32_t state);
void
FakePointerHandleAxis(void* data, struct wl_pointer* wlPointer, uint32_t time,
                  uint32_t axis, wl_fixed_t value);
void
FakeKeyboardHandleKey(void* data, struct wl_keyboard* keyboard, uint32_t serial,
                  uint32_t time, uint32_t key, uint32_t state_w);

int lsim_inc_filter_init(unsigned int screenWd, unsigned int screenHt);


//int faciablock(void);

void FakeUpdateTouch(bool bstate);

//void WLcbHighlightRegion(int left, int top, int right, int bottom, int state);

#ifdef __cplusplus
}
#endif

#endif
