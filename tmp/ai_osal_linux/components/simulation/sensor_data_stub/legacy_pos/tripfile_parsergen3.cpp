/******************************************************************************
* FILE         : tripfile_parsergen3.c
*             
* DESCRIPTION  : This file implements an application that simulates v850 for sensor proxy.
*                In GEN3 gyro, odometer and accelerometer sensor hardwares are present on V850.
*                Sensor data will be sent from V850 to imx via INC. It is the job of sensor
*                data proxy module to collect this data, and deliver this to VD-Sensor.
*                As we dont have v850 and INC for LSIM, this application takes trip file as the input 
*                parses it and sends the sensor data to LSIM sensor drivers . 
*                It communicates with sensor proxy on imx via socket using TCP/IP.
*                IP Address of imx:  172.17.0.1
*                IP addredd of ubuntu in which test stub is executed shall be : 172.17.0.6
* 
* AUTHOR(s)    : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 15.MAR.2013|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* 31.Mar.2015| Modified to fit Gen3 LSIM |Padmashri Kudari(RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<limits.h>
#include<errno.h>
#include<sys/types.h>

/* inet_pton() */
#include <arpa/inet.h>
/* htons() */
#include <netinet/in.h>
#include<sys/socket.h>
#include <semaphore.h>

#include <time.h>

#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_LEGACY_POS
#include "trcGenProj/Header/tripfile_parsergen3.cpp.trc.h"
#endif


#include "sensor_stub_main_start.h"
#include "basic_types.h"
#include "tripfile_parsergen3.h"

/* Buffer to send data */
static tU8 u8Buff[BUFFER_SIZE];
/* Handle to socket */
tS32 s32ConnFD;

/*semaphores for synchronization*/
sem_t hParserSem;
sem_t hSendSem;

/*************************************************************
*****************Function declarations*****************************
*************************************************************/

extern tPVoid LsimOdoParserThread (tPVoid pvTripFilePath);
extern tPVoid LsimGyroParserThread(tPVoid pvTripFilePath);
extern tPVoid LsimAccParserThread(tPVoid pvTripFilePath);
static tPVoid vCommunicateToAPP(char *file_path);

static tPVoid Start_GnssParser(tPVoid pvTripFilePath);
static tPVoid Start_PosParser(tPVoid pvTripFilePath);

/************************************************************
****************Function definitions******************************
***********************************************************/

/* Start of program execution  */
tS32 main(tS32 argc, char *argv[])
{
    tS32 s32RetVal = OSAL_ERROR;
	 pthread_t GnssParserID, PosParserID;

    vInitPlatformEtg(); // initialize the trace backend no cleanup required
	 
    if(argv[1]!=NULL)
    {
	 
		if( 0 != pthread_create(&GnssParserID,NULL, Start_GnssParser,(tPVoid)argv[1]) )
		{
			ETG_TRACE_FATAL(("GNSS thread create failed errno %d",errno));
		}
		
		if( 0 != pthread_create(&PosParserID,NULL, Start_PosParser,(tPVoid)argv[1]) )
		{
			ETG_TRACE_FATAL(("POS thread create failed errno %d",errno));
		}
		
		while(1)
		{
			sleep (20);
			ETG_TRACE_COMP(("TRIP Parser running"));
		}
	
    }
    else
    {
        ETG_TRACE_FATAL(("\n\t\t!!!!!NO TRIP FILE AVAILABLE  \t\t USAGE: sudo ./sensor_sb_main_start.out ABSOLUTE_PATH_TO_TRIP_FILE\n"));
        exit(1);
    }
    return s32RetVal;
}


static tPVoid Start_GnssParser(tPVoid pvTripFilePath)
{
	Gnss_start_main((char *)pvTripFilePath);

}
static tPVoid Start_PosParser(tPVoid pvTripFilePath)
{
    struct sockaddr_in rSocAttr;
    tS32 s32RetVal = OSAL_ERROR;
    tS32 s32OptState = 1;
    tS32 s32SocketFD= 0;
    char *newstr = (char *) pvTripFilePath;

	if( NULL != newstr )
	{
        memset(&rSocAttr,0, sizeof(rSocAttr));
        rSocAttr.sin_family      = AF_INET; //IPv4 Internet protocol
        rSocAttr.sin_port        = htons(6); // Todo: this has to be changed
        rSocAttr.sin_addr.s_addr = htonl(INADDR_ANY);

        /* create a socket
        SOCK_STREAM: sequenced, reliable, two-way, connection-based
        byte streams with an OOB data transmission mechanism.*/

        s32SocketFD = socket(AF_INET, SOCK_STREAM, 0);
        if(s32SocketFD == -1)
        {
            ETG_TRACE_FATAL(("socket() failed"));
        }
        else
        {
            /* This option informs us to reuse the socket address even if it is busy*/
            if(setsockopt(s32SocketFD, SOL_SOCKET, SO_REUSEADDR,
            &s32OptState, sizeof(s32OptState)) == -1)
            {
                ETG_TRACE_FATAL(( "!!!setsockopt() Failed"));
            }
            /* Bind the socket created to a address */

            s32RetVal = bind(s32SocketFD, (struct sockaddr *)&rSocAttr, sizeof(rSocAttr));
            if(s32RetVal == -1)
            {
                ETG_TRACE_FATAL(("Bind Failed"));
            }
            else
            {
                /*Listen system call makes the socket as a passive socket. 
                i.e socket will be used to accept incoming connections*/

                s32RetVal = listen(s32SocketFD, 10);
                if(s32RetVal == -1)
                {
                    ETG_TRACE_FATAL(("Listen Failed"));
                    s32RetVal = OSAL_ERROR;
                }
                else
                {
                    s32RetVal = OSAL_OK;
                    s32ConnFD = accept(s32SocketFD, NULL, NULL);
                    if(s32ConnFD == -1)
                    {
                        /* This should never happen. Only reason for this is something went
                        wrong while configuring the socket*/
                        ETG_TRACE_FATAL(( "accept Failed !!!"));
                        s32RetVal = OSAL_ERROR;
                    }
                    else
                    {
                        ETG_TRACE_COMP(( "Socket creation and Setup completed successfully"));
                        vCommunicateToAPP(newstr);
                    }
                }
            }
        }
	}

}

/* All communications to APP (IMX) is done from here */
tPVoid vCommunicateToAPP(char *file_path)
{

    tS32 s32ErrChk = OSAL_ERROR;
    tU32 u32ConfigSent = 0;
    tU32 u32Offset = 0;
    pthread_t Odotid,Gyrotid,Acctid;
    tU32 u32ActiveParserCnt=0;
    tS32 s32RetVal = OSAL_OK;

    /* First IMX is expected to send status. So call recv from socket */
    s32ErrChk = recv(s32ConnFD, &u8Buff, 2,0);
    if(s32ErrChk == 2)
    {
        /* Just tracce it out */
        ETG_TRACE_COMP(("Msg ID %d  APP STATUS %d",u8Buff[0], u8Buff[1]));

        /* Next its v850 turn to send status */
        u8Buff[0] = 0x02; /* In every message sent to IMX, (First_byte) = (Total bytes transmitted -1)*/
        u8Buff[1] = MSGID_STATUS;/* Message ID */
        u8Buff[2] = STATUS_ACTIVE;/*  Status as active */
        s32ErrChk = write( s32ConnFD, &u8Buff, 3);
        if(s32ErrChk == 3)
        {
            ETG_TRACE_COMP(("status sent sucessfully "));

            u8Buff[0]=  11;// (bytes_transmitted -1)
            u8Buff[1]=  MSGID_CONFIG;// Msg ID
            u8Buff[2]=  ODO_CONFIG_INTERVAL;// Odo Config Interval
            u8Buff[3]=  0;// ----||----
            u8Buff[4]=  GYRO_CONFIG_INTERVAL;// gyro Config intervall
            u8Buff[5]=  0;
            u8Buff[6]=  GYRO_TYPE; // gyro type
            u8Buff[7]=  ACC_CONFIG_INTERVAL;// ACC interval 50
            u8Buff[8]=  0;
            u8Buff[9]=  ACC_TYPE;//ACC type
            u8Buff[10]= 0;//ABS interval
            u8Buff[11]= 0;//----||----   

            /* Send config message. */
            s32ErrChk = write(s32ConnFD, &u8Buff,12);
            if(s32ErrChk == 12)
            {
                ETG_TRACE_COMP(("Config sent "));
                u32ConfigSent = 1;
            }
            else
            {
                ETG_TRACE_FATAL(("!!! config Failed "));
                s32RetVal = FAILED;
            }
        }
        else
        {
            ETG_TRACE_FATAL((" write to socket failed Sending status from v850 failed  "));
            s32RetVal = FAILED;
        }
    }
    else
    {

        ETG_TRACE_FATAL(("recv failed %d",s32ErrChk));
        s32RetVal = FAILED;
    }

    if(u32ConfigSent == 1)
    {
        //initialize both the semaphores
        if( PASSED == sem_init( &hSendSem, 0, 1))
        {
            if( PASSED == sem_init( &hParserSem, 0, 0) )
            {
                /* Spwan  threads to parse odometer gyro and acc data from tripfile
                Path to trip file is argument to the thread*/
                if(PASSED == pthread_create(&Odotid,NULL, LsimOdoParserThread,(tPVoid)file_path))
                {
                    /*If thread spwan passed, update active thread count*/
                    u32ActiveParserCnt++; 
                }
                else
                {
                    ETG_TRACE_FATAL(("pthread_create failed !!!. unable to parse odometer data"));
                    s32RetVal = FAILED;
                }
                if(PASSED == pthread_create(&Gyrotid,NULL,LsimGyroParserThread ,(tPVoid)file_path))
                {
                    /*If thread spwan passed, update active thread count*/
                    u32ActiveParserCnt++;
                }
                else
                {
                    ETG_TRACE_FATAL(("pthread_create failed !!!. unable to parse Acc data"));
                    s32RetVal = FAILED;
                }
                if(PASSED == pthread_create(&Acctid,NULL,LsimAccParserThread ,(tPVoid)file_path))
                {
                    /*If thread spwan passed, update active thread count*/
                    u32ActiveParserCnt++;   
                }
                else
                {
                    ETG_TRACE_FATAL(("pthread_create failed !!!. unable to parse Gyro data"));
                    s32RetVal = FAILED;
                }
            }
            else
            {
                ETG_TRACE_FATAL((" !!!sem_init Failed Unable to parse tripfile"));
                s32RetVal = FAILED;
            }
        }
        else
        {
            ETG_TRACE_FATAL((" !!!sem_init hSendSem Failed :("));
            s32RetVal = FAILED;
        }
        /* Wait for all threads to die*/
        while(u32ActiveParserCnt != 0)
        {
            if(PASSED != sem_wait(&hParserSem))
            {
                ETG_TRACE_FATAL(("!!!! sem_wait returned error"));
                s32RetVal = FAILED;
            }

            u32ActiveParserCnt--;
            if(u32ActiveParserCnt == 0)
            {
                if(close(s32ConnFD)==PASSED)
                {
                    ETG_TRACE_COMP(("close successful:"));
                }
                else
                {
                    ETG_TRACE_FATAL(("close FAILED:"));
                    s32RetVal = FAILED;
                }
            }
            ETG_TRACE_COMP((" One Thread is terminated. Remaining threads %lu  ",u32ActiveParserCnt));
        }
        ETG_TRACE_COMP(("Exiting parser s32RetVal =%d ",s32RetVal));
    }

}

tS32 SendData(tU8 *pu8Buff, tU32 u32Bytes)
{
    tS32 s32ErrChk = OSAL_ERROR;
    tS32 s32retval = FAILED;

    if(PASSED == sem_wait(&hSendSem))
    {
        /*send data*/
        s32ErrChk = write(s32ConnFD, pu8Buff, u32Bytes);
        if(s32ErrChk == u32Bytes)
        {
            ETG_TRACE_COMP((" u32Bytes %lu Sent ",u32Bytes));
            s32retval = PASSED;
        }
        else
        {
            ETG_TRACE_FATAL(("Send failed Failed"));
            s32retval = FAILED;
        }

        if(FAILED == sem_post(&hSendSem))
        {
            ETG_TRACE_FATAL(("SendData:Oops!hSendSem Semaphore Post Failed"));
            s32retval = FAILED;
        }
    }
    else
    {
        ETG_TRACE_FATAL(("!!!! sem_wait hSendSem returned error"));
    }

    return s32retval;
}

