/*------------------------------------------------------------------------------------------|
| FILE           : gyro_parser.h                                                            |
|                                                                                           |
| SW-COMPONENT   : Linux native application                                                 |
|                                                                                           |
| DESCRIPTION    :This Is The Header File Of gyro_parser.c                                  |
|                                                                                           |
|-------------------------------------------------------------------------------------------|
| Date           |       Version        | Author & comments                                 |
|----------------|----------------------|---------------------------------------------------|
| 31.Mar.2015    |  Initial Version 1.0 | Padmashri Kudari(RBEI/ECF5)                       |
-------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------*
|                           Macro Definitions                                               |
*------------------------------------------------------------------------------------------*/
#define BUFFER_SIZE 1024
#define MSGID_STATUS 0x21
#define STATUS_ACTIVE 0x01
#define MSGID_CONFIG 0x31
#define ODO_CONFIG_INTERVAL 70
#define GYRO_TYPE 1
#define GYRO_CONFIG_INTERVAL 50
#define ACC_CONFIG_INTERVAL 50
#define ACC_TYPE 1

