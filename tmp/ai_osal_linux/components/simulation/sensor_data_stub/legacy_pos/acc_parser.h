/*------------------------------------------------------------------------------------------|
| FILE           : acc_parser.h                                                            |
|                                                                                           |
| SW-COMPONENT   : Linux native application                                                 |
|                                                                                           |
| DESCRIPTION    :This Is The Header File Of gyro_parser.c                                  |
|                                                                                           |
|-------------------------------------------------------------------------------------------|
| Date           |       Version        | Author & comments                                 |
|----------------|----------------------|---------------------------------------------------|
| 31.Mar.2015    |  Initial Version 1.0 | Padmashri kudari (RBEI/ECF5)                      |
-------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------*
|                           Macro Definitions                                                |
*--------------------------------------------------------------------------------------------*/
#define BUFF_SIZE 4096
#define BUFFER_SIZE 100
#define MSGID_DATA 0x41
#define ACC_ID 0x04
#define ACC_MAX_ELEMENTS  7

#define ACC_TIME_STAMP "Acc3dTimestamp = "
#define ACC_R_AXIS_VAL "Acc3dRVal = "
#define ACC_S_AXIS_VAL "Acc3dSVal = "
#define ACC_T_AXIS_VAL "Acc3dTVal = "
#define ACC_R_AXIS_STATUS "Acc3dRStatus = "
#define ACC_S_AXIS_STATUS "Acc3dSStatus = "
#define ACC_T_AXIS_STATUS "Acc3dTStatus = "

#define ACC_TIMESTAMP_INDEX      0
#define ACC_R_AXIS_VAL_INDEX     1
#define ACC_S_AXIS_VAL_INDEX     2
#define ACC_T_AXIS_VAL_INDEX     3
#define ACC_R_AXIS_STATUS_INDEX  4
#define ACC_S_AXIS_STATUS_INDEX  5
#define ACC_T_AXIS_STATUS_INDEX  6

#define ACC_FSEEK_RETRY_COUNT 3
#define ACC_FSEEK_RETRY_WAIT_TIME_MS 1000
#define MAX_SENSOR_DEVICES       3

#define MS_MULTIPLIER 1000


#define ODO_TIME_STAMP "OdometerTimestamp = "
#define ACC3D_TIME_STAMP  "Acc3dTimestamp ="
#define GYRO3D_TIME_STAMP "Gyro3dTimestamp = "

#define LSIM_SENSOR_DEVICE_GYRO       0
#define LSIM_SENSOR_DEVICE_ODOMETER   1
#define LSIM_SENSOR_DEVICE_ACC        2

#define TRUE  1
#define FALSE 0

#define STRING_NOT_FOUND  -4
#define END_OF_BUFFER     -2
#define DATA_BEYOND_LIMIT -5

