/*------------------------------------------------------------------------------------------|
| FILE           : gyro_parser.h                                                            |
|                                                                                           |
| SW-COMPONENT   : Linux native application                                                 |
|                                                                                           |
| DESCRIPTION    :This Is The Header File Of gyro_parser.c                                  |
|                                                                                           |
|-------------------------------------------------------------------------------------------|
| Date           |       Version        | Author & comments                                 |
|----------------|----------------------|---------------------------------------------------|
| 31.Mar.2015    |  Initial Version 1.0 | Padmashri Kudari(RBEI/ECF5)                       |
-------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------*
|                               Macro Definitions                                           |
*--------------------------------------------------------------------------------------------*/
#define BUFF_SIZE 4096
#define BUFFER_SIZE 100
#define MSGID_DATA 0x41
#define GYROMETER_ID 0x02

#define GYRO_MAX_ELEMENTS  7
#define MAX_SENSOR_DEVICES 3

#define LSIM_SENSOR_DEVICE_GYRO 0
#define LSIM_SENSOR_DEVICE_ODOMETER 1
#define LSIM_SENSOR_DEVICE_ACC 2

#define GYRO_TIME_STAMP "Gyro3dTimestamp = "
#define GYRO_R_AXIS_VAL "Gyro3dRVal = "
#define GYRO_S_AXIS_VAL "Gyro3dSVal = "
#define GYRO_T_AXIS_VAL "Gyro3dTVal = "
#define GYRO_R_AXIS_STATUS "Gyro3dRStatus = "
#define GYRO_S_AXIS_STATUS "Gyro3dSStatus = "
#define GYRO_T_AXIS_STATUS "Gyro3dTStatus = "

#define GYRO_TIMESTAMP_INDEX  0
#define GYRO_R_AXIS_VAL_INDEX 1
#define GYRO_S_AXIS_VAL_INDEX 2
#define GYRO_T_AXIS_VAL_INDEX 3
#define GYRO_R_AXIS_STATUS_INDEX 4
#define GYRO_S_AXIS_STATUS_INDEX 5
#define GYRO_T_AXIS_STATUS_INDEX 6

//#define GPS_TIME_STAMP "GnssTimestamp = "
#define ODO_TIME_STAMP "OdometerTimestamp = "
#define GYRO3D_TIME_STAMP "Gyro3dTimestamp = "
#define ACC3D_TIME_STAMP  "Acc3dTimestamp ="

#define TRUE  1
#define FALSE 0

#define STRING_NOT_FOUND -4
#define END_OF_BUFFER -2
#define DATA_BEYOND_LIMIT -5
#define MS_MULTIPLIER 1000

#define GYRO_FSEEK_RETRY_COUNT 3
#define GYRO_FSEEK_RETRY_WAIT_TIME_MS 1000

//Gyro structures to be filled and sent
typedef struct
{
    tU32 u32TimeStamp;
    tU32 u32RaxisVal;
    tU32 u32SaxisVal;
    tU32 u32TaxisVal;
    tU16 u16Rstatus;
    tU16 u16Sstatus;
    tU16 u16Tstatus;
}trGyroData;

typedef struct
{
    trGyroData rGyroData;

}trGyroMeterData;


