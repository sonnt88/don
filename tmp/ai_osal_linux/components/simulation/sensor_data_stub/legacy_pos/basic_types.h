/******************************************************************************
* FILE           : basic_types.h
*
* SW-COMPONENT   : Linux native application
*
* DESCRIPTION    :This file has the basic data types and header files.
*
* AUTHOR(s)      : Padmashri Kudari (RBEI/ECF5)
*
* HISTORY        :
*-----------------------------------------------------------------------------
* Date           |       Version      | Author & comments
*----------------|--------------------|-------------------------------------------------
* 31.Mar.2015    |  Initialversion 1.0| Padmashri Kudari (RBEI/ECF5)
* -----------------------------------------------------------------------------
*******************************************************************************/
#ifndef BASIC_TYPES

#define BASIC_TYPES

   #include<stdio.h>
   #include<stdlib.h>
   #include<string.h>
   #include<limits.h>
   #include<errno.h>
   #include<sys/types.h>
	#include <unistd.h>
	#include <pthread.h>

   #define PASSED 0
   #define FAILED -1



   typedef unsigned char tU8;
   typedef signed char tS8;
   typedef unsigned long tU32;
   typedef const char * tCString;
   typedef unsigned short tU16;
   typedef int tS32;
#if 0
   /* OUR error codes. */
   #define OSAL_OK 0
   #define OSAL_ERROR -1
   typedef void * tPVoid;
   typedef void tVoid;
#endif
#endif

