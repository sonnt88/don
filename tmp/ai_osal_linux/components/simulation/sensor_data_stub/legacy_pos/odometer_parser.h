/******************************************************************************
* FILE          : odometer_parser.h
*
* SW-COMPONENT  : Linux native application
*
* DESCRIPTION   : Header file for odometer parser.
*
* AUTHOR(s)     : Padmashri Kudari (RBEI/ECF5)
*
* HISTORY       :
*-----------------------------------------------------------------------------
* Date           |       Version      | Author & comments
*----------------|--------------------|---------------------------------------
* 31.Mar.2015   |  Initialversion 1.0| Padmashri Kudari (RBEI/ECF5)
* -----------------------------------------------------------------------------
*******************************************************************************/
#define BUFF_SIZE 4096
#define BUFFER_SIZE 100	
#define END_OF_BUFFER -2
#define STRING_NOT_FOUND -4
#define DATA_BEYOND_LIMIT -5
#define MSGID_DATA 0x41	
#define ODOMETER_ID 0x01

#define ODOMETER_TIME_STAMP 0
#define ODOMETER_COUNT      1
#define ODOMETER_STATUS     2
#define ODOMETER_DIRECTION  3

#define ODOMETER_MAX_ENTRIES 4
#define MAX_SENSOR_DEVICES   3
#define READONLY "r"


#define LSIM_SENSOR_DEVICE_GYRO 0
#define LSIM_SENSOR_DEVICE_ODOMETER 1
#define LSIM_SENSOR_DEVICE_ACC 2

#define GYRO3D_TIME_STAMP "Gyro3dTimestamp = "
#define ACC3D_TIME_STAMP  "Acc3dTimestamp ="

#define ODO_TIME_STAMP "OdometerTimestamp = "
#define ODO_COUNT "OdometerCount = "
#define ODO_STATUS "OdometerStatus = "
#define ODO_DIRECTION "OdometerCountDirection = "

#define MS_MULTIPLIER 1000
#define ODO_FSEEK_RETRY_COUNT 3
#define ODO_FSEEK_RETRY_WAIT_TIME_MS 1000

#if 0
/* Actual odometer structures  */
typedef enum {
    OSAL_EN_RFS_UNKNOWN,
    OSAL_EN_RFS_FORWARD,
    OSAL_EN_RFS_REVERSE
}OSAL_tenIOCtrlOdometerRFS;

typedef struct
{
    tU32 u32TimeStamp;
    tU32 u32WheelCounter;
    OSAL_tenIOCtrlOdometerRFS enDirection;
    tU16 u16ErrorCounter;
    /* Only for Testing of odometer via CAN */
    //   tU16 u16OICRecv;        //#MaTmp.n
    //   tU16 u16OICSent;        //#MaTmp.n
    //   tU32 u32TuaregTimestamp;//#MaTmp.n
    tU8 u8OdomStatus;      //#MaF70.001.n
    tU8 u8FillByte1;    /* Alignment auf 32 Byte Zugriffe */
    tU8 u8FillByte2;    /* Alignment auf 32 Byte Zugriffe *///#MaV01A.n
    tU8 u8FillByte3;    /* Alignment auf 32 Byte Zugriffe *///#MaV01A.n
    tU16 u16OdoMsgCounter;                                                //#MaOdo01.n
    tU8 u8FillByte4;    /* Alignment auf 32 Byte Zugriffe *///#MaV01A.n   //#MaOdo01.n
    tU8 u8FillByte5;    /* Alignment auf 32 Byte Zugriffe *///#MaV01A.n   //#MaOdo01.n
}OSAL_trIOCtrlOdometerData;

#endif

