/*------------------------------------------------------------------------------------------|
| FILE           : gyro_parser.c                                                            |
|                                                                                           |
| SW-COMPONENT   : Linux native application                                                 |
|                                                                                           |
| DESCRIPTION    :1: This parses the trip file passed as a first parameter to it.          |
|                 2: Trip file has to be in ascii readable format (.txt).                  |
|                 3: If Trip file is modified manually after converting(using              |
|                    trip_conv.exe) it to.txt format , parser may behave abnormally.       |
|                 4: This will operate as a separate process in LSIM environment.          |
|-------------------------------------------------------------------------------------------|
| Date           |       Version        | Author & comments                                 |
|----------------|----------------------|---------------------------------------------------|
| 24.Dec.2012    |  Initial Version 1.0 | Sanjay G(RBEI/ECF5)                               |
| 31.Mar.2015    |Modified to fit Gen3 LSIM|Padmashri K(RBEI/ECF5)
-------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------*
|                               Header File Declaration                                     |
*-------------------------------------------------------------------------------------------*/

/*This piece of code may be executed in LSIM environment or in any other flavor of linux.
Thus common osal header files cant be used. Hence some piece of code from osal header
files is reproduced in the basic_types.h*/

#include"basic_types.h"
#include<unistd.h>
#include <semaphore.h>
#include"gyro_parser.h"

#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_LEGACY_POS
#include "trcGenProj/Header/gyro_parser.cpp.trc.h"
#endif

/*--------------------------------------------------------------------------------------------*
|                                Function Prototypes                                          |
*---------------------------------------------------------------------------------------------*/
extern tS32 SendData(tU8 *pu8Buff, tU32 u32Bytes);

static tS32 s32GyroGetTimeOffset(FILE *fhandle);

static tS32 s32GyroGetval(const char *SearchStr,tU32*u32Value,tU32 u32Base);

static tS32 s32GyroReadFromPrevNewLine(FILE *fhandle);

static tS32 s32StartParsingGyrodata(FILE *fHandle,tS32 s32InitialOffset);

tPVoid LsimGyroParserThread (tPVoid pvTripFilePath);

/*--------------------------------------------------------------------------------------------*
|                                Variable Declaration                                         |
*---------------------------------------------------------------------------------------------*/

const char *pCcGyroDataElements[GYRO_MAX_ELEMENTS]={GYRO_TIME_STAMP,
                                                    GYRO_R_AXIS_VAL,
                                                    GYRO_S_AXIS_VAL,
                                                    GYRO_T_AXIS_VAL,
                                                    GYRO_R_AXIS_STATUS,
                                                    GYRO_S_AXIS_STATUS,
                                                    GYRO_T_AXIS_STATUS};

/*Semaphore handle which is used to inform parser process regarding
thread termination event. This is opened in tripfile_parsergen3.c*/
extern sem_t hParserSem;

/* This is used to store data read from trip file*/
static char buf[BUFF_SIZE];

/*this is a pointer to the buffer holding data from trip file. This is used to traverse
through the trip file*/
char *parser2=buf;

/*used to store the time  interval*/
tU32 u32Gyrowait_time;


/********************************************************************************
* FUNCTION    : s32GyroGetTimeOffset 

* PARAMETER   : fhandle  -File handle to trip file
*                                                      
* RETURNVALUE : Least of four timestamps of Odometer, Gyro, GPS and Acc
*               -1 on Failure
*
* DESCRIPTION :1: Get the first records of Odometer, GPS ,Gyro and Acc.
*              2: Compare their time stamps to find the least time stamp.
* HISTORY     : 30.DEC.2012| Initial Version      |Madhu Kiran Ramachandra (RBEI/ECF5)
*             : 31.Mar.2015|Modified to fit Gen3 LSIM |Padmashri Kudari(RBEI/ECF5)
**********************************************************************************/
tS32 s32GyroGetTimeOffset(FILE *fhandle)
{

    tS32 s32RetVal = FAILED;
    tU32 u32Value=0;
    tU32 u32LoopCnt = 0;
    const char *pcs8_SensorDeviceTypes[MAX_SENSOR_DEVICES]={GYRO3D_TIME_STAMP,ODO_TIME_STAMP,ACC3D_TIME_STAMP};
    tU32 u32OdoTS = 0;
    tU32 u32GpsTS = 0;
    tU32 u32GyroTS = 0;
    tU32 u32AccTS = 0;
    tU32 u32med;


    //Read from trip file
    s32RetVal = fread((void *)buf,1,BUFF_SIZE,fhandle);
    if(s32RetVal > 0)
    {
        for(u32LoopCnt=0;u32LoopCnt < MAX_SENSOR_DEVICES;)
        {
            /*This parses the buffer for the string passed and updates u32Value with the 
            value assosiated with the string.*/

            s32RetVal = s32GyroGetval(pcs8_SensorDeviceTypes[u32LoopCnt],&u32Value,0);
            switch (s32RetVal)
            {
                // Required string is not available in the current page so do another read
                case STRING_NOT_FOUND:
                {
                    if(feof(fhandle) == 0)
                    {
                        /*If required string is partially read in buffer, it will be 
                        read partially in next read aslo. so before any read, 
                        fseek to previous '' character */
                        s32GyroReadFromPrevNewLine(fhandle);
                    }
                    else
                    {
                        /*We have reached end of file but we did not get the desired data
                        So exit from loop and return failure.*/
                        ETG_TRACE_COMP(("!!!! END OF TRIP FILE !!!! "));
                        u32LoopCnt = MAX_SENSOR_DEVICES;
                        s32RetVal = FAILED;
                    }
                    break;
                }
                case END_OF_BUFFER:
                {
                    s32GyroReadFromPrevNewLine(fhandle);
                    break;
                }
                case PASSED:
                {
                    switch (u32LoopCnt)
                    {
                        /*GYRO time stamp*/
                        case LSIM_SENSOR_DEVICE_GYRO:
                        {
                            u32GyroTS = u32Value;
                            ETG_TRACE_COMP(("Gyro Parser Gyro %lu", u32GyroTS));
                            rewind(fhandle);
                            s32RetVal = fread((void *)buf,1,BUFF_SIZE,fhandle);
                            break;
                        }
                        /*Odometer time stamp*/
                        case LSIM_SENSOR_DEVICE_ODOMETER :
                        {
                            u32OdoTS = u32Value;
                            ETG_TRACE_COMP(("Gyro Parser odo %lu", u32OdoTS));
                            rewind(fhandle);
                            s32RetVal = fread((void *)buf,1,BUFF_SIZE,fhandle);
                            break;
                        }
                        /*Accelerometer time stamp*/
                        case LSIM_SENSOR_DEVICE_ACC :
                        {
                            u32AccTS = u32Value;
                            ETG_TRACE_COMP(( "Gyro Parser Acc %lu", u32AccTS));
                            /*Get the least sensor timestamp*/
                             if(u32AccTS < u32GyroTS)
                            {
                                if(u32AccTS < u32OdoTS)
                                {
                                    s32RetVal = u32AccTS;
                                }
                                else
                                {
                                    s32RetVal = u32OdoTS;
                                }
                            }
                            else if(u32GyroTS < u32OdoTS)
                            {
                                s32RetVal = u32GyroTS;
                            }
                            else
                            {
                                s32RetVal = u32OdoTS;
                            }
                            ETG_TRACE_COMP(("Gyro parser Least Time stamp : %d ", s32RetVal));
                            rewind(fhandle);
                            break;
                        }
                        default:
                        {
                            ETG_TRACE_COMP((" !!!!!! This should never happen "));
                            break;
                        }
                    }
                    /*We got a sensor time stamp.Update the loop count*/
                    u32LoopCnt=(u32LoopCnt+1);
                    break;
                }
                default:
                {
                    ETG_TRACE_COMP(("!!!! This should never occour "));
                }
            }
        }
    }
    else
    {
        ETG_TRACE_COMP(("Read Failed Errorval = %d", s32RetVal));
        s32RetVal = FAILED;
    }
    return s32RetVal;
}

/********************************************************************************
* FUNCTION    : s32OdoReadFromPrevNewLine 
* PARAMETER   : fhandle      - Handle to tripfile
* RETURNVALUE :0 on sucess
*             -1 on Failure
* DESCRIPTION :1: This will search for new line character () in a loop.
*              2: then does a fseek to  -(loop counter) from current position.
*              3: Then it reads BUFF_SIZE-1 bytes of data from tripfile.
*              4: Append a \0 at the end of buffer.
* HISTORY     : 20.DEC.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 s32GyroReadFromPrevNewLine(FILE *fhandle)
{
    tS32 s32SeekPos = 0;
    tS32 s32RetVal= FAILED;
    tU32 u32DataSize = BUFF_SIZE-1;
    tU32 u32Fseekretrycnt;

    for(;buf[u32DataSize-s32SeekPos] != '\n';s32SeekPos++)
    {
        //ETG_TRACE_COMP((" %lu ",(tU32)buf[u32DataSize-s32SeekPos-1]));
    }

   
    for ( u32Fseekretrycnt = 0;
        ((u32Fseekretrycnt < GYRO_FSEEK_RETRY_COUNT)&&(OSAL_ERROR == s32RetVal));
        u32Fseekretrycnt++ )
    {
        if ( OSAL_ERROR == (s32RetVal = fseek(fhandle ,(-(s32SeekPos)),SEEK_CUR)) )
        {
            ETG_TRACE_COMP(("!!!! fseek failed"));
            usleep( GYRO_FSEEK_RETRY_WAIT_TIME_MS );
        }
    }

    s32RetVal = fread((void *)buf,1,BUFF_SIZE-1,fhandle);
    if (s32RetVal <= 0)
    {
        ETG_TRACE_COMP((" !!!!!!! READ FAILED after seek  "));
    }
    else
    {
        buf[s32RetVal]='\0';
        parser2=buf;
        s32RetVal  = PASSED;
    }
    return s32RetVal;
}

/*---------------------------------------------------------------------------------------------|
| FUNCTION      : LsimGyroParserThread                                                         |
| DESCRIPTION   : Entry Point For Gyro Parser Thread.                                          |
| PARAMETER     : pvTripFilePath - Pointer To The Trip File Path.                              |
| RETURNVALUE   : void                                                                         |
|----------------------------------------------------------------------------------------------*/
tPVoid LsimGyroParserThread(tPVoid pvTripFilePath)
{
    tS32 s32RetVal = FAILED;
    tS32 s32InitialOffset=0;
    FILE *fHandle;

    if(pvTripFilePath != NULL)
    {
        ETG_TRACE_COMP(("LsimGyroParserThread:Gyro Parser Invoked"));

        fHandle=fopen((tCString)pvTripFilePath, "r");
        if(fHandle == NULL)
        {
            ETG_TRACE_COMP(("GyroParserThread:Oops!Opening Trip File Failed"));
        }
        else
        {  
            /*Get Least Time Stamp Among GPS,ODO,GYRO.This Is
            Used To Synchronize All Sensors Data With Each Other*/
            s32InitialOffset = s32GyroGetTimeOffset(fHandle);
            if(s32InitialOffset < 0)
            {
                s32InitialOffset = 0;
            }

            /* Now Start Reading Gyro Data From Trip File And Send It To LSIM. 
            Returns From This Function Only If Complete File Parsing Is  Done  */

            s32RetVal=s32StartParsingGyrodata(fHandle,s32InitialOffset);

            if(PASSED == s32RetVal)
            {
                ETG_TRACE_COMP(("GyroParserThread:Cheers!Parsing Trip File %s Is Successful",(tCString)pvTripFilePath));
            }
            else
            {
                ETG_TRACE_COMP(("GyroParserThread:Oops!Reading From Trip File %s FAILED ",(tCString)pvTripFilePath));
            }

            if(fclose(fHandle))
            {
                ETG_TRACE_COMP(("Closing Trip File FAILED"));
            }
        }
    }
    else
    {
        ETG_TRACE_COMP(("GyroParserThread:Please Enter TripFile(path) As Command Line ArgumentUSAGE:./Executable  TripFilePath"));
    }
    if(FAILED == sem_post(&hParserSem))
    {
        ETG_TRACE_COMP(("GyroParserThread:Oops!Semaphore Post Failed"));
    }
}
/********************************************************************************
* FUNCTION      : SendGyroData 
* PARAMETER     :structure  trGyroMeterData
* RETURNVALUE   : 0 on sucess
* DESCRIPTION   :1: copies the structure values obtained by parsing 
*                and send it to LSIM with appropriate msg ID
* HISTORY       : 31.Mar.2015 |Padmashri Kudari(RBEI/ECF5)
**********************************************************************************/
tS32 SendGyroData(trGyroMeterData * prGyroMeterData)
{
    tU8 buff[BUFFER_SIZE];
    unsigned int uitemp;
    tS32 s32val = FAILED  ;

    memset(buff,0,sizeof(buff));
    buff[0] = 13; //number of bytes sent -1
    buff[1] = MSGID_DATA; //msg id for data
    buff[2] = 1;
    buff[4] = GYROMETER_ID;//Gyro id
    
    uitemp  = prGyroMeterData->rGyroData.u32TimeStamp & 0xffffff;
    buff[5] = uitemp & 0xff;
    uitemp  = uitemp >> 8;
    buff[6] = uitemp & 0xff;
    buff[7] = uitemp >> 8;

    uitemp  =  prGyroMeterData->rGyroData.u32RaxisVal;
    uitemp  =  uitemp & 0xffff;
    buff[8] = uitemp & 0xff;
    buff[9] = uitemp >> 8;

    uitemp  =  prGyroMeterData->rGyroData.u32SaxisVal;
    uitemp  =  uitemp & 0xffff;
    buff[10] = uitemp & 0xff;
    buff[11] = uitemp >> 8;

    uitemp  =  prGyroMeterData->rGyroData.u32TaxisVal;
    uitemp  =  uitemp & 0xffff;
    buff[12] = uitemp & 0xff;
    buff[13] = uitemp >> 8;

    s32val = SendData(buff,14);
    if(PASSED == s32val )
    {
        ETG_TRACE_COMP(("SendGyroData successful :)"));

    }
    else
    {
        ETG_TRACE_COMP(("SendGyroData failed :("));
    }
    ETG_TRACE_COMP(("u32Gyrowait_time = %lu ",u32Gyrowait_time));
    usleep(u32Gyrowait_time*MS_MULTIPLIER); //wait for gyro interval
    return s32val;
}

/********************************************************************************
* FUNCTION : s32GyroGetval 
* PARAMETER  : Searchstr   - string to be searched
*              u32Value    - pointer to a tU32 variable to store value associated with Searchstr
*              u32Base  - In trip file value associated with OdometerStatus string is stored
*                         in hex format. Presently this is not used. This may be needed in future
* RETURNVALUE: 0 on sucess
*              -1 on Failure
*              -2 END_OF_BUFFER
*              -4 STRING_NOT_FOUND
*              -5 DATA_BEYOND_LIMIT
*
* DESCRIPTION :1: This will search in buffer (pointed by parser1) for required string
*              2: This reads from trip file
*              3: Search for required strings in buffer.
*              4: Send data to LSIM.
* HISTORY     :03.oct.2012| Initial Version |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 s32GyroGetval(const char *SearchStr,tU32 *u32Value,tU32 u32Base)
{

    tS32 s32RetVal = FAILED;
    char *First_InvalChar = NULL;

    parser2=strstr(parser2,SearchStr);
    if(parser2 != NULL)
    {
        *u32Value = strtol(parser2+strlen(SearchStr),&First_InvalChar,u32Base);
        if(((*u32Value==LONG_MIN)||(*u32Value==LONG_MAX))&& (errno == ERANGE))
        {
            ETG_TRACE_COMP(("!!!!!!! Data read from trip file is beyond limit"));
            s32RetVal = DATA_BEYOND_LIMIT;
        }
        else if(*First_InvalChar == '\0')
        {
            /*We found our string but value associated with it is not in the buffer.*/	 
            ETG_TRACE_COMP((" End of BUFFER "));
            s32RetVal = END_OF_BUFFER;
        }
        else 
        {
            /* we found our string and value associated with it.*/
            s32RetVal = PASSED;
        }
    }
    else
    {
        /*Buffer is parsed fully but string is not found.*/
        s32RetVal =  STRING_NOT_FOUND;

    }
    return  s32RetVal;

}

/********************************************************************************
* FUNCTION      : s32StartParsing 

* PARAMETER     :   fhandle    -File handle to trip file                                                    
* RETURNVALUE   : 0 on sucess
*                -1 on Failure
* DESCRIPTION   :1: This is the core of parsing algorithm.
*                          2: This reads from trip file
*                          3: Search for required strings in buffer.
*                          4: Send data to Dispatcher via interfaces of lsimconnect.h
*                          5: If Trip file is parsed fully, Send disconnect message to dispatcher.
* HISTORY       : 03.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 s32StartParsingGyrodata(FILE *fhandle, tS32 s32InitialOffset)
{

    tS32 s32RetVal = FAILED;
    tU32 u32Value=0;
    tU32 u32LoopCnt = 0;
    tU32 u32RecordCnt=0;
    trGyroMeterData rGyroMeterData;
    tU32 u32PrevTimeStamp = s32InitialOffset;
    tU32 u32WaitTime;

    ETG_TRACE_COMP(("s32StartParsingGyrodata called with initial offset %d",s32InitialOffset));

    (tVoid)memset(&rGyroMeterData,0,sizeof(rGyroMeterData));

    //Read from trip file
    s32RetVal = fread((void *)buf,1,BUFF_SIZE,fhandle);
    if(s32RetVal > 0)
    {
        for(u32LoopCnt=0;u32LoopCnt < GYRO_MAX_ELEMENTS;)
        {
            /*This parses the buffer for the string passed and updates u32Value with the 
            value assosiated with the string.*/
          s32RetVal = s32GyroGetval(pCcGyroDataElements[u32LoopCnt],&u32Value,0);
            switch (s32RetVal)
            {
                // GYROMETER data is not available in the current page so do another read
                case STRING_NOT_FOUND:
                {
                    if(feof(fhandle) == 0)
                    {
                        /*If required string is partially read in buffer, it will be 
                        read partially in next read aslo. so before any read, 
                        fseek to previous '' character */
                        s32GyroReadFromPrevNewLine(fhandle);
                    }
                    else
                    {
                        /*We have reached end of file*/
                        ETG_TRACE_COMP(("!!! END OF TRIP FILE "));
                        ETG_TRACE_COMP(("Sending static records as end of file reached: "));
                        while(1)
                            while(SendGyroData(&rGyroMeterData)!= PASSED)
                                ETG_TRACE_COMP(("Send failed "));
                    }
                    break;
                }
                case END_OF_BUFFER:
                {
                    s32GyroReadFromPrevNewLine(fhandle);
                    break;
                }
                case PASSED:
                {
                    switch (u32LoopCnt)
                    {
                        case GYRO_TIMESTAMP_INDEX :
                        {
                            u32WaitTime = u32Value - u32PrevTimeStamp;
                            u32PrevTimeStamp = u32Value;
                            rGyroMeterData.rGyroData.u32TimeStamp = u32WaitTime;
                            u32Gyrowait_time = u32WaitTime;
                            break;
                        }
                        case GYRO_R_AXIS_VAL_INDEX :
                        {
                            rGyroMeterData.rGyroData.u32RaxisVal = u32Value;
                            break;
                        }
                        case GYRO_S_AXIS_VAL_INDEX:
                        {
                            rGyroMeterData.rGyroData.u32SaxisVal =  u32Value;
                            break;
                        }
                        case GYRO_T_AXIS_VAL_INDEX:
                        {
                            rGyroMeterData.rGyroData.u32TaxisVal = u32Value;
                            break;
                        }
                        case GYRO_R_AXIS_STATUS_INDEX:
                        {
                            rGyroMeterData.rGyroData.u16Rstatus = (tU16) u32Value;
                            break;
                        }
                        case GYRO_S_AXIS_STATUS_INDEX:
                        {
                            rGyroMeterData.rGyroData.u16Sstatus = (tU16) u32Value;
                            break;
                        }
                        case GYRO_T_AXIS_STATUS_INDEX:
                        {
                            rGyroMeterData.rGyroData.u16Tstatus = (tU16) u32Value ;
                            u32RecordCnt++;
                            /*GYRO_STATUS is the last entry of every record in trip file
                            Thus now you have one full record. Send it to LSIM*/
                            while(SendGyroData(&rGyroMeterData)!= PASSED)
                            ETG_TRACE_COMP(("s32SendToDispatcher FAILED "));
                            break;
                        }
                        default:
                        {
                            ETG_TRACE_COMP(("!!! Switch Hits Default"));
                            break;
                        }

                    }
                    u32LoopCnt=(u32LoopCnt+1)%GYRO_MAX_ELEMENTS;
                    break;
                }
                default:
                {
                    ETG_TRACE_COMP(("!!! Switch Hits Default"));
                }
            }
        }
    }
    else
    {
        ETG_TRACE_COMP(("Read Failed Errorval = %d", s32RetVal));
        ETG_TRACE_COMP(("fread error = %s",strerror(errno)));
        s32RetVal = FAILED;
    }

    return s32RetVal;
}
