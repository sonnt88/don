/******************************************************************************
* FILE         : odometer_parser.c
*
* DESCRIPTION  : 1: This parses the trip file passed as a first parameter to it.
*                2: Trip file has to be in ascii readable format (.txt).
*                3: IF Trip file is modified manually after converting it using to
*                   .txt format using trip_conv.exe, parser may behave abnormally.
*                4: This will operate as a separate process in LSIM environment.
* AUTHOR(s)   : Madhu Kiran Ramachandra (RBEI/ECF5)
* HISTORY     : 
*-----------------------------------------------------------------------------
* Date        |       Version      | Author & comments
*-------------|--------------------|-------------------------------------------------
* 03.oct.2012 |  Initialversion 1.0| Madhu Kiran Ramachandra (RBEI/ECF5)
* 31.Mar.2015 | Modified the code so as fit Gen3 LSIM|Padmashri Kudari(RBEI/ECF5)
* -----------------------------------------------------------------------------
*******************************************************************************/

/************************************************************************
| Header file declaration
|-----------------------------------------------------------------------*/    

/*This piece of code may be executed in LSIM environment or in any other flavor of linux.
Thus common osal header files cant be used. Hence some piece of code from osal header files is reproduced in
the basic_types.h*/
#include"basic_types.h"
#include <semaphore.h>
#include <errno.h>
#include"odometer_parser.h"

#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_LEGACY_POS
#include "trcGenProj/Header/odometer_parser.cpp.trc.h"
#endif

typedef struct
{
    OSAL_trIOCtrlOdometerData rOdoData;
}trOdoMeterData;


/************************************************************************
|function prototypes (scope: local)
|-----------------------------------------------------------------------*/
static tS32 s32OdoGetval(const char *SearchStr,tU32*u32Value,tU32 u32Base);
static tS32 s32OdoStartParsing(FILE *fhandle, tS32 s32InitialOffset);
static tS32 s32OdoReadFromPrevNewLine(FILE *fhandle);
static tS32 s32OdoGetTimeOffset(FILE *fhandle);
static tS32 SendOdoData(trOdoMeterData * prOdoMeterData);
extern tS32 SendData(tU8 *pu8Buff, tU32 u32Bytes);

/************************************************************************
| Variable declaration (scope: global)
|-----------------------------------------------------------------------*/

const char *pcs8_OdoDataTypes[ODOMETER_MAX_ENTRIES]={ODO_TIME_STAMP,ODO_COUNT,ODO_STATUS,ODO_DIRECTION};

/* This is used to store data read from trip file*/
static char buf[BUFF_SIZE];

/*this is a pointer to the buffer holding data from trip file. This is used to traverse
through the trip file*/
char *parser1=buf;

/*used to store the time  interval*/
tU32 u32Odowait_time;


/*Semaphore handle which is used to inform parser process regarding
 thread termination event. This is opened in tripfile_parsergen3.c*/
extern sem_t hParserSem;


/********************************************************************************
* FUNCTION       : SendOdoData 
* PARAMETER      :structure  trOdoMeterData
* RETURNVALUE    : 0 on sucess
* DESCRIPTION    :1: copies the structure values obtained by parsing 
*                           and send it to LSIM with appropriate msg ID
* HISTORY        : 31.Mar.2015 |Padmashri Kudari(RBEI/ECF5)
**********************************************************************************/
tS32 SendOdoData(trOdoMeterData * prOdoMeterData)
{
    tU8 buff[BUFFER_SIZE];
    unsigned int uitemp;
    tS32 s32val;
    memset(buff,0,sizeof(buff));

    //fill the buffer with data to be sent
    buff[0] = 10;//number of bytes to be sent-1
    buff[1] = MSGID_DATA;//msg id for data
    buff[2] = 1;
    buff[4] = ODOMETER_ID;

    uitemp = prOdoMeterData->rOdoData.u32TimeStamp & 0xffffff;
    buff[5]= uitemp & 0xff;
    uitemp = uitemp >> 8;
    buff[6] = uitemp &0xff;
    buff[7] = uitemp >> 8;

    uitemp =  prOdoMeterData->rOdoData.u32WheelCounter;
    uitemp = uitemp & 0xffff;
    buff[8] = uitemp & 0xff;
    buff[9] = uitemp >> 8;

    buff[10] = (tU8)prOdoMeterData->rOdoData.enDirection | (tU8)prOdoMeterData->rOdoData.enOdometerStatus;
    s32val = SendData(buff,11);
    if(PASSED == s32val)
    {
        ETG_TRACE_COMP(("SendOdoData successful "));
    }
    else
    {
        ETG_TRACE_COMP(("SendOdoData failed "));
    }
    ETG_TRACE_COMP((" Odo u32Odowait_time = %lu", u32Odowait_time));
    usleep(u32Odowait_time*MS_MULTIPLIER); /*wait till odo interval*/
    return s32val;
}

/********************************************************************************
* FUNCTION      : LsimOdoParserThread 
* PARAMETER     : pvTripFilePath : path to trip file
* RETURNVALUE   : None
* DESCRIPTION   :1: This is the entry point for the parser.
*                2: opens trip file and calls parsing algorithm.
* HISTORY        : 20.DEC.2012| Initial Version         |Madhu Kiran Ramachandra (RBEI/ECF5)
*                :31.Mar.2015 |Modified to fit Gen3 Lsim|Padmashri Kudari(RBEI/ECF5)
**********************************************************************************/
tPVoid LsimOdoParserThread (tPVoid pvTripFilePath)
{
    tS32 s32RetVal=FAILED;
    tS32 s32InitialOffset = 0;
    FILE *fhandle; // To access trip file 

    ETG_TRACE_COMP(("odometer parser is invoked "));
    if(pvTripFilePath != NULL)
    {
        //open Trip file
        fhandle=fopen((tCString)pvTripFilePath, READONLY);
        if(fhandle == NULL)
        {
            ETG_TRACE_COMP(("!!!!!open failed"));
        }
        else
        {
            s32RetVal = s32OdoGetTimeOffset(fhandle);
            if(s32RetVal > 0)
            {
                s32InitialOffset = s32RetVal;
            }
            else
            {
                s32InitialOffset = 0;
            }
            //Call parsing algorithm
            s32RetVal=s32OdoStartParsing(fhandle,s32InitialOffset);
            if(PASSED == s32RetVal)
            {
                ETG_TRACE_COMP((" LsimOdoParserThread:Cheers!Parsing Trip File %s completed sucessfully ",(tCString)pvTripFilePath));
            }
            else
            {
                ETG_TRACE_COMP(("!!!!Function s32OdoStartParsing() failed. Unable to parse Trip file %s",(tCString)pvTripFilePath));
            }
            //close trip file
            s32RetVal=fclose(fhandle);
            if(s32RetVal != PASSED)
            {
                ETG_TRACE_COMP(("!!!!!Close failed "));
            }
            else
            {
                ETG_TRACE_COMP(("close passed"));
            }
        }
    }
    else
    {
        ETG_TRACE_COMP(("!!! Invalid Trip file path "));
        s32RetVal = FAILED;
    }

    if(FAILED == sem_post(&hParserSem))
    {
        ETG_TRACE_COMP(("sem_post failed"));
    }
}

/********************************************************************************
* FUNCTION      : s32OdoStartParsing 
* PARAMETER     : fhandle - File handle to trip file
*                 s32InitialOffset -Least of all the timestamps
* RETURNVALUE   : 0 on sucess
*                -1 on Failure
* DESCRIPTION   :1: This is the core of parsing algorithm.
*                2: This reads from trip file
*                3: Search for required strings in buffer.
*                4: Send data to LSIM.
* HISTORY       : 03.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
*               : 31.Mar.2015 | Modified the code so as fit Gen3 LSIM | Padmashri Kudari(RBEI/ECF5)
**********************************************************************************/
tS32 s32OdoStartParsing(FILE *fhandle, tS32 s32InitialOffset)
{

    tS32 s32RetVal = FAILED;
    tU32 u32Value=0;
    tU32 u32LoopCnt = 0;
    tU32 u32RecordCnt=0;
    trOdoMeterData rOdoMeterData;
    tU32 u32PrevTimeStamp = s32InitialOffset;
    tU32 u32WaitTime;

    ETG_TRACE_COMP(("s32OdoStartParsing called with initial offset %d",s32InitialOffset));

    (tVoid)memset(&rOdoMeterData,0,sizeof(rOdoMeterData));
    //Read from trip file
    s32RetVal = fread((void *)buf,1,BUFF_SIZE,fhandle);
    if(s32RetVal > 0)
    {

        for(u32LoopCnt=0;u32LoopCnt < ODOMETER_MAX_ENTRIES;)
        {
            /*This parses the buffer for the string passed and updates u32Value with the 
            value assosiated with the string.*/
            s32RetVal = s32OdoGetval(pcs8_OdoDataTypes[u32LoopCnt],&u32Value,0);
            switch (s32RetVal)
            {
                // ODOMETER data is not available in the current page so do another read
                case STRING_NOT_FOUND:
                {
                    if(feof(fhandle) == 0)
                    {
                        /*If required string is partially read in buffer, it will be 
                        read partially in next read also. so before any read, 
                        fseek to previous '' character */
                        s32OdoReadFromPrevNewLine(fhandle);
                    }
                    else
                    {
                        /*We have reached end of file. so send the last record continuously*/
                        ETG_TRACE_COMP(("!!!! END OF TRIP FILE !!!!  %lu",u32RecordCnt));
                        ETG_TRACE_COMP(("!Sending Static data as the trip file end is reached"));
                        while(1)
                            while(SendOdoData(&rOdoMeterData)!= PASSED)
                                ETG_TRACE_COMP(("Send failed "));

                    }
                    break;
                }
                case END_OF_BUFFER:
                {
                    s32OdoReadFromPrevNewLine(fhandle);
                    break;
                }
                case PASSED:
                {
                    //Fill odometer structure with the parsed data from tripfile
                    switch (u32LoopCnt)
                    {
                        case ODOMETER_TIME_STAMP :
                        {
                            u32WaitTime = u32Value - u32PrevTimeStamp;
                            u32PrevTimeStamp = u32Value;
                            rOdoMeterData.rOdoData.u32TimeStamp = u32WaitTime;
                            u32Odowait_time = u32WaitTime;
                            break;
                        }
                        case ODOMETER_COUNT :
                        {
                            rOdoMeterData.rOdoData.u32WheelCounter = u32Value;
                            break;
                        }
                        case ODOMETER_STATUS:
                        {
                            rOdoMeterData.rOdoData.enOdometerStatus = (OSAL_tenOdometerStatus) u32Value;
                            break;
                        }
                        case ODOMETER_DIRECTION:
                        {
                            rOdoMeterData.rOdoData.enDirection = (OSAL_tenIOCtrlOdometerRFS) u32Value ;
                            u32RecordCnt++;
                            /*ODOMETER_DIRECTION is the last entry of every record in trip file
                            Thus now you have one full record. Send it to Dispatcher*/
                            while(SendOdoData(&rOdoMeterData)!= PASSED)
                            ETG_TRACE_COMP(("s32SendToDispatcher FAILED "));
                            break;
                        }
                        default:
                        {
                            ETG_TRACE_COMP(("!!! Switch Hits Default"));
                            break;
                        }
                    }
                    u32LoopCnt=(u32LoopCnt+1)%ODOMETER_MAX_ENTRIES;
                    break;
                }
                default:
                {
                    ETG_TRACE_COMP(("!!! Switch Hits Default"));
                }
            }
        }
    }
    else
    {
        ETG_TRACE_COMP(("Read Failed Errorval = %d", s32RetVal));
        s32RetVal = FAILED;
    }
    return s32RetVal;
}

/********************************************************************************
* FUNCTION   : s32OdoGetval 
            
* PARAMETER  : Searchstr  - string to be searched
*              u32Value   - pointer to a tU32 variable to store value associated with Searchstr
*              u32Base    - In trip file value associated with OdometerStatus string is stored
*                           in hex format. Presently this is not used. This may be needed in future.
* RETURNVALUE: 0 on sucess
*              -1 on Failure
*              -2 END_OF_BUFFER
*              -4 STRING_NOT_FOUND
*              -5 DATA_BEYOND_LIMIT
* DESCRIPTION :1: This will search in buffer (pointed by parser1) for required string
*              2: This reads from trip file
*              3: Search for required strings in buffer.
*              4: Send data to LSIM
* HISTORY    :03.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
*            :31.Mar.2015| modified to fit Gen3 LSIM|Padmashri Kudari(RBEI/ECF5)
**********************************************************************************/
tS32 s32OdoGetval(const char *SearchStr,tU32 *u32Value,tU32 u32Base)
{

    tS32 s32RetVal = FAILED;
    char *First_InvalChar = NULL;

    parser1=strstr(parser1,SearchStr);
    if(parser1 != NULL)
    {
        *u32Value = strtol(parser1+strlen(SearchStr),&First_InvalChar,u32Base);
        if(((*u32Value==LONG_MIN)||(*u32Value==LONG_MAX))&& (errno == ERANGE))
        {
            ETG_TRACE_COMP(("!!!!!!! Data read from trip file is beyond limit"));
            s32RetVal = DATA_BEYOND_LIMIT;
        }
        else if(*First_InvalChar == '\0')
        {
            /*We found our string but value associated 
            with it is not in the buffer.*/
            ETG_TRACE_COMP((" End of BUFFER "));
            s32RetVal = END_OF_BUFFER;
        }
        else 
        {
            /* we found our string and value associated with it.*/
            s32RetVal = PASSED;
        }
    }
    else 
    {
        /*Buffer is parsed fully but string is not found.*/
        s32RetVal =  STRING_NOT_FOUND;
    }
    return  s32RetVal;

}

/********************************************************************************
* FUNCTION    : s32OdoReadFromPrevNewLine 

* PARAMETER   : fhandle      - Handle to tripfile
*
*                                                      
* RETURNVALUE :0 on sucess
*              -1 on Failure
*
* DESCRIPTION :1: This will search for new line character () in a loop.
*              2: then does a fseek to  -(loop counter) from current position.
*              3: Then it reads BUFF_SIZE-1 bytes of data from tripfile.
*              4: Append a \0 at the end of buffer.
*
* HISTORY   : 20.DEC.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 s32OdoReadFromPrevNewLine(FILE *fhandle)
{
    tS32 s32SeekPos = 0;
    tS32 s32RetVal= FAILED;
    tU32 u32DataSize = BUFF_SIZE-1;
    tU32 u32Fseekretrycnt;

    for(;buf[u32DataSize-s32SeekPos] != '\n';s32SeekPos++)
    {
        // ETG_TRACE_COMP((" %lu ",(tU32)buf[u32DataSize-s32SeekPos-1]));
    }
    for ( u32Fseekretrycnt = 0;
        ((u32Fseekretrycnt < ODO_FSEEK_RETRY_COUNT)&&(OSAL_ERROR == s32RetVal));
        u32Fseekretrycnt++ )
    {
        if ( OSAL_ERROR == (s32RetVal = fseek(fhandle ,(-(s32SeekPos)),SEEK_CUR)) )
        {
            ETG_TRACE_COMP(("!!!! fseek failed"));
            usleep( ODO_FSEEK_RETRY_WAIT_TIME_MS );
        }
    }

    s32RetVal = fread((void *)buf,1,BUFF_SIZE-1,fhandle);
    if(s32RetVal <= 0)
    {
        ETG_TRACE_COMP((" !!!!!!! READ FAILED after seek  "));
    }
    else
    {
        buf[s32RetVal]='\0';
        parser1=buf;
        s32RetVal  = PASSED;
    }

    return s32RetVal;
}

/********************************************************************************
* FUNCTION    : s32OdoGetTimeOffset 

* PARAMETER   : fhandle  -File handle to trip file
*                                                      
* RETURNVALUE : Least of four timestamps of Odometer, Gyro, GPS, Acc.
*               -1 on Failure
* DESCRIPTION :1: Get the first records of Odometer, GPS , Gyro and Acc.
*              2: Compare their time stamps to find the least time stamp.
* HISTORY     : 30.DEC.2012| Initial Version      |Madhu Kiran Ramachandra (RBEI/ECF5)
*             :31.Mar.2015 |Modified the code so as fit Gen3 Lsim|Padmashri Kudari(RBEI/ECF5)
**********************************************************************************/
tS32 s32OdoGetTimeOffset(FILE *fhandle)
{

    tS32 s32RetVal = FAILED;
    tU32 u32Value=0;
    tU32 u32LoopCnt = 0;
    const char *pcs8_SensorDeviceTypes[MAX_SENSOR_DEVICES]={GYRO3D_TIME_STAMP,ODO_TIME_STAMP,ACC3D_TIME_STAMP};
    tU32 u32OdoTS = 0;
    tU32 u32GpsTS = 0;
    tU32 u32GyroTS = 0;
    tU32 u32AccTS = 0;
    tU32 u32med;

    //Read from trip file
    s32RetVal = fread((void *)buf,1,BUFF_SIZE,fhandle);
    if(s32RetVal > 0)
    {
        for(u32LoopCnt=0;u32LoopCnt < MAX_SENSOR_DEVICES;)
        {
            /*This parses the buffer for the string passed and updates u32Value with the 
            value assosiated with the string.*/
            s32RetVal = s32OdoGetval(pcs8_SensorDeviceTypes[u32LoopCnt],&u32Value,0);
            switch (s32RetVal)
            {
                // Required string is not available in the current page so do another read
                case STRING_NOT_FOUND:
                {
                    if(feof(fhandle) == 0)
                    {
                        /*If required string is partially read in buffer, it will be 
                        read partially in next read aslo. so before any read, 
                        fseek to previous '' character */
                        s32OdoReadFromPrevNewLine(fhandle);
                    }
                    else
                    {
                        /*We have reached end of file but we did not get the desired data
                        So exit from loop and return failure.*/
                        ETG_TRACE_COMP(("!!!! END OF TRIP FILE !!!! "));
                        u32LoopCnt = MAX_SENSOR_DEVICES;
                        s32RetVal = FAILED;
                    }
                    break;
                }
                case END_OF_BUFFER:
                {
                    s32OdoReadFromPrevNewLine(fhandle);
                    break;
                }
                case PASSED:
                {
                    switch (u32LoopCnt)
                    { 
                        /*GYRO time stamp*/
                        case LSIM_SENSOR_DEVICE_GYRO:
                        {
                            u32GyroTS = u32Value;
                            ETG_TRACE_COMP(("Odometer Parser: Gyro %lu", u32GyroTS));
                            rewind(fhandle);
                            s32RetVal = fread((void *)buf,1,BUFF_SIZE,fhandle);
                            break;
                        }
                        /*Odometer time stamp*/
                        case LSIM_SENSOR_DEVICE_ODOMETER :
                        {
                            u32OdoTS = u32Value;
                            ETG_TRACE_COMP(("Odometer Parser: odo %lu", u32OdoTS));
                            rewind(fhandle);
                            s32RetVal = fread((void *)buf,1,BUFF_SIZE,fhandle);
                            break;
                        }
                        /*Accelerometer time stamp*/
                        case LSIM_SENSOR_DEVICE_ACC :
                        {
                            u32AccTS = u32Value;
                            ETG_TRACE_COMP(( "Odometer Parser: Acc %lu", u32AccTS));
                            if(u32AccTS < u32GyroTS)
                            {
                                if(u32AccTS < u32OdoTS)
                                {
                                    s32RetVal = u32AccTS;
                                }
                                else
                                {
                                    s32RetVal = u32OdoTS;
                                }
                            }
                            else if(u32GyroTS < u32OdoTS)
                            {
                                s32RetVal = u32GyroTS;
                            }
                            else
                            {
                                s32RetVal = u32OdoTS;
                            }
                            ETG_TRACE_COMP(("Odometer Parser Least Time stamp : %d ", s32RetVal));
                            rewind(fhandle);
                            break;
                        }
                        default:
                        {
                            ETG_TRACE_COMP((" !!!!!! This should never happen "));
                            break;
                        }
                    }
                    /*We got a sensor time stamp. Update the loop count*/
                    u32LoopCnt=(u32LoopCnt+1);
                    break;
                }
                default:
                {
                    ETG_TRACE_COMP(("!!!! This should never occour "));
                }
            }
        }
    }
    else
    {
        ETG_TRACE_COMP(("Read Failed Errorval = %d", s32RetVal));
        s32RetVal = FAILED;
    }
    return s32RetVal;
}

/************************************************************************
|end of file
|-----------------------------------------------------------------------*/
