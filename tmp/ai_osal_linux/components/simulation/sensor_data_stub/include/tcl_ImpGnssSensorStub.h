///////////////////////////////////////////////////////////
//  tcl_ImpGnssSensorStub.h
//  Implementation of the Class tcl_ImpGnssSensorStub
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_F3384BE4_26EB_4a71_8C07_A570E79AE82D__INCLUDED_)
#define EA_F3384BE4_26EB_4a71_8C07_A570E79AE82D__INCLUDED_

#include "tcl_ImpIncGnssMsgBuilder.h"
#include "tcl_ImpTcpPxyGnss.h"
#include "tcl_ImpTripParserGnss.h"

class tcl_ImpGnssSensorStub
{

public:
	tcl_ImpGnssSensorStub();
	virtual ~tcl_ImpGnssSensorStub();
   tcl_InfAppCommMsgBuilder *poInfAppCommMsgBuilder;
	tcl_InfAppCommPxy *poInfAppCommPxy;
   tcl_InfSensorDataSource *poInfSensorDataSource;
   tcl_InfSensorData *poInfSensorData;


	bool SenStb_bDeInit();
	bool SenStb_bInit(char *TripFilePath);
	bool SenStb_bStart();
	bool SenStb_bStop();
   void SenStb_bWaitforGnssIntervall(tcl_InfSensorData *poInfSensorData);


};
#endif // !defined(EA_F3384BE4_26EB_4a71_8C07_A570E79AE82D__INCLUDED_)
