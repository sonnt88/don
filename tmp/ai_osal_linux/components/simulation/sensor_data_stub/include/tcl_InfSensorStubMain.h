///////////////////////////////////////////////////////////
//  tcl_InfSensorStubMain.h
//  Implementation of the Interface tcl_InfSensorStubMain
//  Created on:      15-Jul-2015 8:53:16 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_0379C936_6BE7_4456_AFE1_36B77C0A92EE__INCLUDED_)
#define EA_0379C936_6BE7_4456_AFE1_36B77C0A92EE__INCLUDED_

class tcl_InfSensorStubMain
{

public:
	tcl_InfSensorStubMain() {

	}

	virtual ~tcl_InfSensorStubMain() {

	}

	virtual bool SenStb_bDeInit() =0;
	virtual bool SenStb_bInit(char *TripFilePath) =0;
	virtual bool SenStb_bPauseStub() =0;
	virtual bool SenStb_bStartStub() =0;
	virtual bool SenStb_bStopStub() =0;

};
#endif // !defined(EA_0379C936_6BE7_4456_AFE1_36B77C0A92EE__INCLUDED_)
