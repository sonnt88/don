///////////////////////////////////////////////////////////
//  tcl_ImpSensorData.h
//  Implementation of the Class tcl_ImpSensorData
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_48FBD05B_D023_463f_9680_F0C21713088E__INCLUDED_)
#define EA_48FBD05B_D023_463f_9680_F0C21713088E__INCLUDED_

#include "tcl_InfSensorData.h"
#include "sensor_stub_types.h"

class tcl_ImpSensorData : public tcl_InfSensorData
{

public:
	tcl_ImpSensorData();
	virtual ~tcl_ImpSensorData();

	virtual map_GnssPvtInfo SenStb_mapGetGnssPvtData();
	virtual void SenStb_vmapSetGnssPvtData(map_GnssPvtInfo &mParGnssPvtInfo) = 0;
	virtual vec_GnsssatInfo SenStb_vecGetGnssSatData();
	virtual void SenStb_vSetGnssSatData(vec_GnsssatInfo &vGnsssatInfo)= 0;
   virtual void SenStb_vTraceRecord() =0;

};
#endif // !defined(EA_48FBD05B_D023_463f_9680_F0C21713088E__INCLUDED_)
