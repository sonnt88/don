///////////////////////////////////////////////////////////
//  tcl_ImpTripParserGyro.h
//  Implementation of the Class tcl_ImpTripParserGyro
//  Created on:      15-Jul-2015 8:53:16 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_19DD047D_0417_41a0_A189_E92EEE4386DB__INCLUDED_)
#define EA_19DD047D_0417_41a0_A189_E92EEE4386DB__INCLUDED_

#include "tcl_InfSensorData.h"
#include "tcl_ImpTripParser.h"

class tcl_ImpTripParserGyro : public tcl_ImpTripParser
{

public:
	tcl_ImpTripParserGyro();
	virtual ~tcl_ImpTripParserGyro();

	bool bHasGyro() const;
	bool SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData);
	bool SenStb_bDeInit();
	bool SenStb_bInit(char *TripFilePath);

};
#endif // !defined(EA_19DD047D_0417_41a0_A189_E92EEE4386DB__INCLUDED_)
