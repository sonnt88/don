///////////////////////////////////////////////////////////
//  tcl_ImpGnssNmeaMsgBuilder.h
//  Implementation of the Class tcl_ImpGnssNmeaMsgBuilder
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_30B5CF22_3709_44e8_9E48_88947B464D0B__INCLUDED_)
#define EA_30B5CF22_3709_44e8_9E48_88947B464D0B__INCLUDED_

#include "tcl_ImpGnssDataBuilder.h"
#include <sstream>

#define SEN_STB_GPGGA_HEADER ("$GPGGA")
#define SEN_STB_GPRMC_HEADER ("$GPRMC")
#define SEN_STB_GPGSA_HEADER ("$GPGSA")
#define SEN_STB_GPGSV_HEADER ("$GPGSV")
#define SEN_STB_PSTMPV_HEADER ("$PSTMPV")
#define SEN_STB_PSTMCPU_MSG ("$PSTMCPU,58.32,0,52*65\r\n")


#define SEN_STB_NMEA_FIELD_DELILITER (',')
#define SEN_STB_NMEA_CHECKSUM_DELILITER ('*')
#define SEN_STB_NMEA_MSG_TERMINATOR ("\r\n")

#define SEN_STB_NMEA_GPRMC_TIME_VALID ('A')
#define SEN_STB_NMEA_GPRMC_TIME_INVALID ('V')

#define SEN_STB_NMEA_LATITUDE_DIRECTION_NORTH ('N')
#define SEN_STB_NMEA_LATITUDE_DIRECTION_SOUTH ('S')
#define SEN_STB_NMEA_LONGITUDE_DIRECTION_EAST ('E')
#define SEN_STB_NMEA_LONGITUDE_DIRECTION_WEST ('W')

#define SEN_STB_NMEA_ALTITUDE_UNIT ('M')

#define SEN_STB_GPGSA_FIX_MODE_AUTOMATIC ('A')


#define SEN_STB_GSA_NUM_SATS_PER_MSG (12.0)
#define SEN_STB_NUM_OF_GSV_SAT_INFO_IN_SINGLE_MSG (4.0)

#define SEN_STB_SAT_USED_FOR_POSCALC (0x20)

typedef enum
{
   SENSTB_OSSPERTIMESTAMP = 0,
   SENSTB_OSSPERLAT = 1,
   SENSTB_OSSPERLON = 2,
   SENSTB_OSSPERDATE = 3,
   SENSTB_OSSPERFIXQUAL = 4,
   SENSTB_OSSPERSATUSED = 5,
   SENSTB_OSSPERHDOP = 6,
   SENSTB_OSSPERPDOP = 7,
   SENSTB_OSSPERVDOP = 8,
   SENSTB_OSSPERALT = 9,
   SENSTB_OSSPERGEOSEP = 10,
   SENSTB_OSSPERFIXTYPE = 11,
   SENSTB_OSSPERSATINVIEW = 12,
   SENSTB_OSSPERVELNORTH = 13,
   SENSTB_OSSPERVELEAST = 14,
   SENSTB_OSSPERVELUP = 15,
   SENSTB_OSSPERPOSCOVNORTH = 16,
   SENSTB_OSSPERPOSCOVNOREAST = 17,
   SENSTB_OSSPERPOSCOVNORUP = 18,
   SENSTB_OSSPERPOSCOVEAST = 19,
   SENSTB_OSSPERPOSCOVEASTUP = 20,
   SENSTB_OSSPERPOSCOVUP = 21,
   SENSTB_OSSPERVELCOVNORTH = 22,
   SENSTB_OSSPERVELCOVNORTHEAST = 23,
   SENSTB_OSSPERVELCOVNORUP = 24,
   SENSTB_OSSPERVELCOVEAST = 25,
   SENSTB_OSSPERVELCOVEASTUP = 26,
   SENSTB_OSSPERVELCOVUP = 27,
   SENSTB_OSSMAXLIMIT = 28
}SenStb_tEnOss;


class tcl_ImpGnssNmeaMsgBuilder : public tcl_ImpGnssDataBuilder
{

private:

   string SenStb_strGprmcMsg(map_GnssPvtInfo &mapGnssPvtInfo,vec_GnsssatInfo &vecGnsssatInfo);
   string SenStb_strGpggaMsg(map_GnssPvtInfo &mapGnssPvtInfo,vec_GnsssatInfo &vecGnsssatInfo);
   string SenStb_strGpgsaMsg(map_GnssPvtInfo &mapGnssPvtInfo,vec_GnsssatInfo &vecGnsssatInfo);
   string SenStb_strSingleGpgsaMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                             vec_GnsssatInfo &vecGnsssatInfo,
                                             int MsgCnt);
   string SenStb_strGpgsvMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                      vec_GnsssatInfo &vecGnsssatInfo );
   string SenStb_strSingleGpgsvMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                             vec_GnsssatInfo &vecGnsssatInfo,
                                             int CurrMsgNum,
                                             int TotalNumMsgs);

   string SenStb_strPstmpvMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                                                  vec_GnsssatInfo &vecGnsssatInfo );


   string SenStb_StrGetTimestamp(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetLat(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetLon(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetSatUsed(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetHDop(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetPDop(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVDop(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetAlt(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetGeoSep(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetFixtype(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetFixQuality(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetSatInView(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetDate(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelNorth(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelEast(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelUp(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetPosCovNorth(map_GnssPvtInfo &mapGnssPvtInfo);  
   string SenStb_StrGetPosCovNorEast(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetPosCovNorUp(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetPosCovEast(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetPosCovEastUp(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetPosCovUp(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelCovNorth(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelCovNorthEast(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelCovNorUp(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelCovEast(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelCovEastUp(map_GnssPvtInfo &mapGnssPvtInfo);
   string SenStb_StrGetVelCovUp(map_GnssPvtInfo &mapGnssPvtInfo);
   void SenStb_vClearSSObj();
   string SenStb_StrCalcNmeaChksum(string &OstrGprmc);
   inline double SenStb_dradToDeg(double angleRadians);
   stringstream ossPerArr[SENSTB_OSSMAXLIMIT];

public:
   tcl_ImpGnssNmeaMsgBuilder();
   virtual ~tcl_ImpGnssNmeaMsgBuilder();

   bool SenStb_bCreateGnssMsg(tcl_InfSensorData *poInfSensorData, string &oStrGnssAppMsg);
   void SenStb_vAddTimeStamp(map_GnssPvtInfo &mapGnssPvtInfo, string &oStrGnssAppMsg);
   void SenStb_vAddNmeaSeq(map_GnssPvtInfo &mapGnssPvtInfo, vec_GnsssatInfo &vecGnsssatInfo, string &oStrGnssAppMsg);  
   bool SenStb_bDeInit();
   bool SenStb_bInit();

};
#endif // !defined(EA_30B5CF22_3709_44e8_9E48_88947B464D0B__INCLUDED_)

