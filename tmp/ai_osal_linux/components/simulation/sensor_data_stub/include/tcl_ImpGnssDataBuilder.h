///////////////////////////////////////////////////////////
//  tcl_ImpGnssDataBuilder.h
//  Implementation of the Class tcl_ImpGnssDataBuilder
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_0C83C88E_3BDC_4f01_B76E_7BE5E39A5033__INCLUDED_)
#define EA_0C83C88E_3BDC_4f01_B76E_7BE5E39A5033__INCLUDED_

#include "tcl_InfGnssDataBuilder.h"

class tcl_ImpGnssDataBuilder : public tcl_InfGnssDataBuilder
{

public:
	tcl_ImpGnssDataBuilder();
	virtual ~tcl_ImpGnssDataBuilder();

	virtual bool SenStb_bCreateGnssMsg(tcl_InfSensorData *poInfSensorData, string &oStrGnssAppMsg) =0;
	virtual bool SenStb_bDeInit();
	virtual bool SenStb_bInit();

};
#endif // !defined(EA_0C83C88E_3BDC_4f01_B76E_7BE5E39A5033__INCLUDED_)
