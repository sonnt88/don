///////////////////////////////////////////////////////////
//  tcl_ImpOdoData.h
//  Implementation of the Class tcl_ImpOdoData
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_048ED2B6_693E_44ff_84D7_EDDC6CC190D8__INCLUDED_)
#define EA_048ED2B6_693E_44ff_84D7_EDDC6CC190D8__INCLUDED_

#include "tcl_ImpSensorData.h"

class tcl_ImpOdoData : public tcl_ImpSensorData
{

public:
	tcl_ImpOdoData();
	virtual ~tcl_ImpOdoData();

};
#endif // !defined(EA_048ED2B6_693E_44ff_84D7_EDDC6CC190D8__INCLUDED_)
