///////////////////////////////////////////////////////////
//  tcl_ImpSensorDataSource.h
//  Implementation of the Class tcl_ImpSensorDataSource
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_1E7F6E58_8787_412a_80A4_BADFB54C3796__INCLUDED_)
#define EA_1E7F6E58_8787_412a_80A4_BADFB54C3796__INCLUDED_

#include "tcl_InfSensorData.h"
#include "tcl_InfSensorDataSource.h"

class tcl_ImpSensorDataSource : public tcl_InfSensorDataSource
{

public:
	tcl_ImpSensorDataSource();
	virtual ~tcl_ImpSensorDataSource();

	virtual bool bHasAbs() const;
	bool bHasAcc() const;
	bool bHasGnss();
	bool bHasGyro() const;
	bool bHasOdo() const;
	virtual bool SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData) =0;
	virtual bool SenStb_bDeInit();
	virtual bool SenStb_bInit(char *TripFilePath);

};
#endif // !defined(EA_1E7F6E58_8787_412a_80A4_BADFB54C3796__INCLUDED_)
