///////////////////////////////////////////////////////////
//  tcl_ImpGnssData.h
//  Implementation of the Class tcl_ImpGnssData
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_76334F41_49C0_4387_A24F_1E04A2597BED__INCLUDED_)
#define EA_76334F41_49C0_4387_A24F_1E04A2597BED__INCLUDED_

#include "tcl_ImpSensorData.h"
//#include "tcl_ImpTripParserGnss.h"

class tcl_ImpGnssData : public tcl_ImpSensorData
{

private:
   map_GnssPvtInfo mGnssPvtInfo;
   vec_GnsssatInfo vGnssChanInfo;

public:
   tcl_ImpGnssData();
   virtual ~tcl_ImpGnssData();

   map_GnssPvtInfo SenStb_mapGetGnssPvtData();
   void SenStb_vmapSetGnssPvtData( map_GnssPvtInfo &mParGnssPvtInfo);
   vec_GnsssatInfo SenStb_vecGetGnssSatData();
   void SenStb_vSetGnssSatData(vec_GnsssatInfo &vGnsssatInfo);
   void SenStb_vTraceRecord();

};
#endif // !defined(EA_76334F41_49C0_4387_A24F_1E04A2597BED__INCLUDED_)
