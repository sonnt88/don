///////////////////////////////////////////////////////////
//  tcl_ImpGyroData.h
//  Implementation of the Class tcl_ImpGyroData
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_B621DBA7_FB9B_4cb7_BAA6_087F4A0DDA40__INCLUDED_)
#define EA_B621DBA7_FB9B_4cb7_BAA6_087F4A0DDA40__INCLUDED_

#include "tcl_ImpSensorData.h"

class tcl_ImpGyroData : public tcl_ImpSensorData
{

public:
	tcl_ImpGyroData();
	virtual ~tcl_ImpGyroData();

};
#endif // !defined(EA_B621DBA7_FB9B_4cb7_BAA6_087F4A0DDA40__INCLUDED_)
