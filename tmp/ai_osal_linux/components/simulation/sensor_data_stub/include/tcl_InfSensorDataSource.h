///////////////////////////////////////////////////////////
//  tcl_InfSensorDataSource.h
//  Implementation of the Interface tcl_InfSensorDataSource
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_BC669D08_6F02_463d_8D19_6A5C4FEEF106__INCLUDED_)
#define EA_BC669D08_6F02_463d_8D19_6A5C4FEEF106__INCLUDED_

#include "tcl_InfSensorData.h"
//#include "tcl_IfSenStubMasterIf.h"

class tcl_InfSensorDataSource// : public tcl_IfSenStubMasterIf
{

public:
	tcl_InfSensorDataSource() {

	}

	virtual ~tcl_InfSensorDataSource() {

	}

	virtual bool bHasAbs() const =0;
	virtual bool bHasAcc() const =0;
	virtual bool bHasGnss() =0;
	virtual bool bHasGyro() const =0;
	virtual bool bHasOdo() const =0;
	virtual bool SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData) =0;
	virtual bool SenStb_bDeInit() =0;
	virtual bool SenStb_bInit(char *TripFilePath) =0;

};
#endif // !defined(EA_BC669D08_6F02_463d_8D19_6A5C4FEEF106__INCLUDED_)
