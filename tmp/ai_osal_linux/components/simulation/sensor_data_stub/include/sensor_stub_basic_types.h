#ifndef SENSOR_STUB_BASIC_TYPES
#define  SENSOR_STUB_BASIC_TYPES
#if 1
   typedef unsigned char tU8;
   typedef signed char tS8;
   typedef signed char* tPS8;
   typedef unsigned long tU32;
   typedef const char * tCString;
   typedef unsigned char tBool;
   typedef unsigned short tU16;
   typedef int tS32;
   typedef void * tPVoid;
   typedef void tVoid;
   typedef double tF64;
   typedef float tF32;
#endif
#endif