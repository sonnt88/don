///////////////////////////////////////////////////////////
//  tcl_InfAppCommMsgBuilder.h
//  Implementation of the Interface tcl_InfAppCommMsgBuilder
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_A6C5DA82_158C_485b_86A2_F8CE65048C32__INCLUDED_)
#define EA_A6C5DA82_158C_485b_86A2_F8CE65048C32__INCLUDED_
#include "sensor_stub_types.h"
#include "tcl_InfSensorDataSource.h"

class tcl_InfAppCommMsgBuilder
{

protected:
   string ostrAppMsg;
public:
	tcl_InfAppCommMsgBuilder() {

	}

	virtual ~tcl_InfAppCommMsgBuilder() {

	}

   virtual bool SenStb_bCreateAppMsg(tcl_InfSensorData *poInfSensorData) =0;
   virtual bool SenStb_bCreateAppMsg(En_AppMsgType enAppMsgType) =0;
   virtual bool SenStb_bDeInit() =0;
   virtual bool SenStb_bInit() =0;
   virtual bool SenStb_ostrGetAppMsg( string &osrtAppMsg ) =0;
};
#endif // !defined(EA_A6C5DA82_158C_485b_86A2_F8CE65048C32__INCLUDED_)
