///////////////////////////////////////////////////////////
//  tcl_ImpAppCommIncMsgBuilder.h
//  Implementation of the Class tcl_ImpAppCommIncMsgBuilder
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_4B3E2A0A_0E06_4f37_BCF4_64A9D1ACF54D__INCLUDED_)
#define EA_4B3E2A0A_0E06_4f37_BCF4_64A9D1ACF54D__INCLUDED_

#include "tcl_ImpAppCommMsgBuilder.h"

class tcl_ImpAppCommIncMsgBuilder : public tcl_ImpAppCommMsgBuilder
{

   protected:

   public:
   	tcl_ImpAppCommIncMsgBuilder();
   	virtual ~tcl_ImpAppCommIncMsgBuilder();

   	virtual bool SenStb_bCreateAppMsg(tcl_InfSensorData *poInfSensorData) = 0;
      virtual bool SenStb_bCreateAppMsg(En_AppMsgType enAppMsgType) = 0;
   	virtual bool SenStb_bDeInit();
   	virtual bool SenStb_bInit();

};
#endif // !defined(EA_4B3E2A0A_0E06_4f37_BCF4_64A9D1ACF54D__INCLUDED_)
