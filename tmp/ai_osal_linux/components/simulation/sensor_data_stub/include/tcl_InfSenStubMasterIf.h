///////////////////////////////////////////////////////////
//  tcl_IfSenStubMasterIf.h
//  Implementation of the Interface tcl_IfSenStubMasterIf
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_2BCC002C_2A9D_401d_A634_341BCE619EF0__INCLUDED_)
#define EA_2BCC002C_2A9D_401d_A634_341BCE619EF0__INCLUDED_

#include "tcl_InfSensorData.h"
#include "sensor_stub_types.h"
#include "tcl_IfSensorDataSource.h"
#include "tcl_IfAppCommMsgBuilder.h"

class tcl_IfSenStubMasterIf
{

public:
	tcl_IfSenStubMasterIf() {

	}

	virtual ~tcl_IfSenStubMasterIf() {

	}

	virtual bool bHasAbs() const =0;
	virtual bool bHasAcc() const =0;
	virtual bool bHasGnss() const =0;
	virtual bool bHasGyro() const =0;
	virtual bool bHasOdo() const =0;
	virtual map_GnssPvtInfo SenStb_mapGetGnssPvtData() =0;
	virtual void SenStb_vmapSetGnssPvtData(map_GnssPvtInfo &mParGnssPvtInfo) =0;
	virtual bool SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData) =0;
	virtual tcl_InfSensorData* SenStb_oGetOneRecord(unsigned int Timecursor) =0;
	virtual bool SenStb_bCreateAppMsg(tcl_InfSensorData *poInfSensorData) =0;
	virtual bool SenStb_bCreateAppMsg(En_AppMsgType enAppMsgType) =0;
	virtual bool SenStb_bCreateGnssMsg(tcl_InfSensorData *poInfSensorData, string &oStrGnssAppMsg) =0;
	virtual bool SenStb_bDeInit() =0;
   virtual bool SenStb_ostrGetAppMsg( string &osrtAppMsg ) =0;
	virtual bool SenStb_bInit() =0;
	virtual bool SenStb_bPauseStub() =0;
	virtual int SenStb_s32SendDataToApp(string &ostrAppMsg) =0;
	virtual bool SenStb_bStartStub() =0;
	virtual bool SenStb_bStopStub() =0;
	virtual vec_GnsssatInfo SenStb_vecGetGnssSatData() =0;
	virtual void SenStb_vSetGnssSatData(vec_GnsssatInfo &vGnsssatInfo) =0;

};
#endif // !defined(EA_2BCC002C_2A9D_401d_A634_341BCE619EF0__INCLUDED_)
