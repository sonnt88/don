///////////////////////////////////////////////////////////
//  tcl_ImpIncGnssMsgBuilder.h
//  Implementation of the Class tcl_ImpIncGnssMsgBuilder
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_BB637925_14FA_496c_BAB8_C44314993342__INCLUDED_)
#define EA_BB637925_14FA_496c_BAB8_C44314993342__INCLUDED_

#include "tcl_ImpGnssNmeaMsgBuilder.h"
#include "tcl_ImpAppCommIncMsgBuilder.h"

class tcl_ImpIncGnssMsgBuilder : public tcl_ImpAppCommIncMsgBuilder
{

public:
	tcl_ImpIncGnssMsgBuilder();
	virtual ~tcl_ImpIncGnssMsgBuilder();
	tcl_ImpGnssNmeaMsgBuilder *m_tcl_ImpGnssNmeaMsgBuilder;

	bool SenStb_bCreateAppMsg(tcl_InfSensorData *poInfSensorData);
   bool SenStb_bCreateAppMsg(En_AppMsgType enAppMsgType);
	bool SenStb_bDeInit();
	bool SenStb_bInit();

};
#endif // !defined(EA_BB637925_14FA_496c_BAB8_C44314993342__INCLUDED_)
