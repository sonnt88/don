///////////////////////////////////////////////////////////
//  tcl_ImpSensorStubFactory.h
//  Implementation of the Class tcl_ImpSensorStubFactory
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_2D25A6AA_6494_484f_9DD9_FDC5D7F9F6DF__INCLUDED_)
#define EA_2D25A6AA_6494_484f_9DD9_FDC5D7F9F6DF__INCLUDED_
#include "En_StubFactObjType.h"

/* Interface headers*/
#include "tcl_InfAppCommPxy.h"
#include "tcl_InfAppCommMsgBuilder.h"
#include "tcl_InfGnssDataBuilder.h"
#include "tcl_InfSensorDataSource.h"
#include "tcl_InfSensorStubMain.h"


class tcl_ImpSensorStubFactory
{

private:
   static tcl_ImpSensorStubFactory *poImpSensorStubFactory;
   tcl_ImpSensorStubFactory();

public:
   virtual ~tcl_ImpSensorStubFactory();
   static tcl_ImpSensorStubFactory* SenStb_poGetFactoryObj();

   tcl_InfAppCommPxy* SenStb_poMakeAppCommPxyObj(En_SensorType enSensorType);
   tcl_InfAppCommMsgBuilder* SenStb_poAppCommMsgBuilderObj(En_SensorType enSensorType);
   tcl_InfGnssDataBuilder* SenStb_poGnssDataBuilderObj(En_SensorType enSensorType);
   tcl_InfSensorDataSource* SenStb_poSensorDataSourceObj(En_SensorType enSensorType);
   tcl_InfSensorStubMain* SenStb_poSensorStubMainObj(En_SensorType enSensorType);
   tcl_InfSensorData* SenStb_poSensorDataStorageObj(En_SensorType enSensorType);

};
#endif // !defined(EA_2D25A6AA_6494_484f_9DD9_FDC5D7F9F6DF__INCLUDED_)
