///////////////////////////////////////////////////////////
//  tcl_ImpAppCommMsgBuilder.h
//  Implementation of the Class tcl_ImpAppCommMsgBuilder
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_017E19E4_ABC4_4a29_B4F5_10E14ED27270__INCLUDED_)
#define EA_017E19E4_ABC4_4a29_B4F5_10E14ED27270__INCLUDED_

#include "tcl_InfAppCommMsgBuilder.h"

class tcl_ImpAppCommMsgBuilder : public tcl_InfAppCommMsgBuilder
{

public:
   tcl_ImpAppCommMsgBuilder();
   virtual ~tcl_ImpAppCommMsgBuilder();

   virtual bool SenStb_bCreateAppMsg(tcl_InfSensorData *poInfSensorData) =0;
   virtual bool SenStb_bCreateAppMsg(En_AppMsgType enAppMsgType) =0;
   virtual bool SenStb_bDeInit() =0;
   /* This can be over ridden based on requirement */
   virtual bool SenStb_ostrGetAppMsg( string &osrtAppMsg );
   virtual bool SenStb_bInit() =0;

};
#endif // !defined(EA_017E19E4_ABC4_4a29_B4F5_10E14ED27270__INCLUDED_)
