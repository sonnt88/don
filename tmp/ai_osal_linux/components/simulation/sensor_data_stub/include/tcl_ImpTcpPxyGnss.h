///////////////////////////////////////////////////////////
//  tcl_ImpTcpPxyGnss.h
//  Implementation of the Class tcl_ImpTcpPxyGnss
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_E87B30CC_96EF_492d_895C_4AAB5C3B00A9__INCLUDED_)
#define EA_E87B30CC_96EF_492d_895C_4AAB5C3B00A9__INCLUDED_

#include "tcl_ImpTcpPxy.h"

class tcl_ImpTcpPxyGnss : public tcl_ImpTcpPxy
{

public:
	tcl_ImpTcpPxyGnss();
	virtual ~tcl_ImpTcpPxyGnss();

	bool SenStb_bDeInit();
	bool SenStb_bInit();
	int SenStb_s32SendDataToApp(string &ostrAppMsg);

};
#endif // !defined(EA_E87B30CC_96EF_492d_895C_4AAB5C3B00A9__INCLUDED_)
