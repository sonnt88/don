///////////////////////////////////////////////////////////
//  tcl_ImpTcpPxyPos.h
//  Implementation of the Class tcl_ImpTcpPxyPos
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_63BD4FAC_33BB_4a56_ACD5_380DFCB7A74B__INCLUDED_)
#define EA_63BD4FAC_33BB_4a56_ACD5_380DFCB7A74B__INCLUDED_

#include "tcl_ImpTcpPxy.h"

class tcl_ImpTcpPxyPos : public tcl_ImpTcpPxy
{

public:
	tcl_ImpTcpPxyPos();
	virtual ~tcl_ImpTcpPxyPos();

	bool SenStb_bDeInit();
	bool SenStb_bInit();
	int SenStb_s32SendDataToApp(string &ostrAppMsg);

};
#endif // !defined(EA_63BD4FAC_33BB_4a56_ACD5_380DFCB7A74B__INCLUDED_)
