///////////////////////////////////////////////////////////
//  tcl_ImpTripParserAcc.h
//  Implementation of the Class tcl_ImpTripParserAcc
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_9F724994_2606_4828_B183_06089598249F__INCLUDED_)
#define EA_9F724994_2606_4828_B183_06089598249F__INCLUDED_

#include "tcl_InfSensorData.h"
#include "tcl_ImpTripParser.h"

class tcl_ImpTripParserAcc : public tcl_ImpTripParser
{

public:
	tcl_ImpTripParserAcc();
	virtual ~tcl_ImpTripParserAcc();

	bool bHasAcc() const;
	bool SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData);
	bool SenStb_bDeInit();
	bool SenStb_bInit(char *TripFilePath);

};
#endif // !defined(EA_9F724994_2606_4828_B183_06089598249F__INCLUDED_)
