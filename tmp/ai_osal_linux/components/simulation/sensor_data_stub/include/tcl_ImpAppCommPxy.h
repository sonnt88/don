///////////////////////////////////////////////////////////
//  tcl_ImpAppCommPxy.h
//  Implementation of the Class tcl_ImpAppCommPxy
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_A23DF1DE_B767_4dbe_8107_2B7D00EBBDC4__INCLUDED_)
#define EA_A23DF1DE_B767_4dbe_8107_2B7D00EBBDC4__INCLUDED_

#include "tcl_InfAppCommPxy.h"

class tcl_ImpAppCommPxy : public tcl_InfAppCommPxy
{

public:
	tcl_ImpAppCommPxy();
	virtual ~tcl_ImpAppCommPxy();

	virtual bool SenStb_bDeInit() =0;
	virtual bool SenStb_bInit() =0;
	virtual int SenStb_s32SendDataToApp(string &ostrAppMsg);

};
#endif // !defined(EA_A23DF1DE_B767_4dbe_8107_2B7D00EBBDC4__INCLUDED_)
