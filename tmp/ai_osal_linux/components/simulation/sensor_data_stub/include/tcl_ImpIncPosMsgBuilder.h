///////////////////////////////////////////////////////////
//  tcl_ImpIncPosMsgBuilder.h
//  Implementation of the Class tcl_ImpIncPosMsgBuilder
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_A3F31CF9_B255_4a73_864B_DF652CEE949F__INCLUDED_)
#define EA_A3F31CF9_B255_4a73_864B_DF652CEE949F__INCLUDED_

#include "tcl_ImpAppCommIncMsgBuilder.h"

class tcl_ImpIncPosMsgBuilder : public tcl_ImpAppCommIncMsgBuilder
{

public:
	tcl_ImpIncPosMsgBuilder();
	virtual ~tcl_ImpIncPosMsgBuilder();

	bool SenStb_bCreateAppMsg(tcl_InfSensorData *poInfSensorData);
	bool SenStb_bDeInit();
	bool SenStb_bInit();

};
#endif // !defined(EA_A3F31CF9_B255_4a73_864B_DF652CEE949F__INCLUDED_)
