///////////////////////////////////////////////////////////
//  SenStub_GnssPvtGnssPvtDataMapKey.h
//  Implementation of the Class SenStub_GnssPvtGnssPvtDataMapKey
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_88E62935_063C_48aa_8988_8DA028D877BF__INCLUDED_)
#define EA_88E62935_063C_48aa_8988_8DA028D877BF__INCLUDED_

/* explisitly assigning the values to all members is needed becaiuse C++ doesnt guarantee the
     numbering to be sequential*/

typedef enum
{
   GnssTimestamp = 0,
   GnssRecordCounter = 1,
   GnssNumOfChannels = 2,
   GnssYear = 3,
   GnssMonth = 4,
   GnssDay = 5,
   GnssHour = 6,
   GnssMinute = 7,
   GnssSecond = 8,
   GnssMilliSeconds = 9,
   GnssLatitude = 10,
   GnssLongitude = 11,
   GnssAltitudeWGS84 = 12,
   GnssGeoidalSeparation = 13,
   GnssVelocityNorth = 14,
   GnssVelocityEast = 15,
   GnssVelocityUp = 16,
   GnssPositionCovarianceMatrix_0 = 17,
   GnssPositionCovarianceMatrix_4 = 18,
   GnssPositionCovarianceMatrix_5 = 19,
   GnssPositionCovarianceMatrix_8 = 20,
   GnssPositionCovarianceMatrix_9 = 21,
   GnssPositionCovarianceMatrix_10 = 22,
   GnssVelocityCovarianceMatrix_0 = 23,
   GnssVelocityCovarianceMatrix_4 = 24,
   GnssVelocityCovarianceMatrix_5 = 25,
   GnssVelocityCovarianceMatrix_8 = 26,
   GnssVelocityCovarianceMatrix_9 = 27,
   GnssVelocityCovarianceMatrix_10 = 28,
   GnssQuality = 29,
   GnssMode = 30,
   GnssGDOP = 31,
   GnssPDOP = 32,
   GnssHDOP = 33,
   GnssTDOP = 34,
   GnssVDOP = 35,
   GnssSatSysUsed = 36,
   GnssSatellitesVisible = 37,
   GnssSatellitesReceived = 38,
   GnssSatellitesUsed = 39,
   GnssPositionResidualMax = 40,
   GnssVelocityResidualMax = 41,
   GnssConstellationValid = 42

}SenStub_GnssPvtGnssPvtDataMapKey;

/* explisitly assigning the values to all members is needed becaiuse C++ doesnt guarantee the
     numbering to be sequential*/
typedef enum
{
   GnssChnRecordCounter = 0,
   GnssChannel = 1,
   GnssSvId = 2,
   GnssSatStatus = 3,
   GnssCarrierToNoiseRatio = 4,
   GnssAzimuth = 5,
   GnssElevation = 6
}En_GnssChannelDataSeq;

#endif // !defined(EA_88E62935_063C_48aa_8988_8DA028D877BF__INCLUDED_)
