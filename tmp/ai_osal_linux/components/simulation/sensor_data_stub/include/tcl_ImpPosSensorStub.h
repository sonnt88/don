///////////////////////////////////////////////////////////
//  tcl_ImpPosSensorStub.h
//  Implementation of the Class tcl_ImpPosSensorStub
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_A43CB468_A252_404a_9BDC_1EAFCF50ED9C__INCLUDED_)
#define EA_A43CB468_A252_404a_9BDC_1EAFCF50ED9C__INCLUDED_

#include "tcl_ImpIncPosMsgBuilder.h"
#include "tcl_ImpTcpPxyPos.h"
#include "tcl_ImpTripParserOdo.h"
#include "tcl_ImpTripParserGyro.h"
#include "tcl_ImpTripParserAcc.h"

class tcl_ImpPosSensorStub
{

public:
	tcl_ImpPosSensorStub();
	virtual ~tcl_ImpPosSensorStub();
	tcl_ImpIncPosMsgBuilder *m_tcl_ImpIncPosMsgBuilder;
	tcl_ImpTcpPxyPos *m_tcl_ImpTcpPxyPos;
	tcl_ImpTripParserOdo *m_tcl_ImpTripParserOdo;
	tcl_ImpTripParserGyro *m_tcl_ImpTripParserGyro;
	tcl_ImpTripParserAcc *m_tcl_ImpTripParserAcc;

	bool SenStb_bDeInit();
	bool SenStb_bInit();
	bool SenStb_bStart();
	bool SenStb_bStop();

};
#endif // !defined(EA_A43CB468_A252_404a_9BDC_1EAFCF50ED9C__INCLUDED_)
