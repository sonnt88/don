///////////////////////////////////////////////////////////
//  tcl_ImpTripParser.h
//  Implementation of the Class tcl_ImpTripParser
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_30ECAF37_EA7D_4b9b_BB27_07C6E5E07E56__INCLUDED_)
#define EA_30ECAF37_EA7D_4b9b_BB27_07C6E5E07E56__INCLUDED_

#include "tcl_InfSensorData.h"
#include "tcl_ImpSensorDataSource.h"

class tcl_ImpTripParser : public tcl_ImpSensorDataSource
{

private:
   streampos TripResPos;

protected:
   ifstream oTripFile;
   char* SenStb_pcGetTripFilePath();
   

public:
	tcl_ImpTripParser();
	virtual ~tcl_ImpTripParser();

   void SenStb_vStoreFileRdPos();
   void SenStb_vRestoreFileRdPos();
	virtual bool bHasAbs() const;
	virtual bool bHasAcc() const;
	virtual bool bHasGnss();
	virtual bool bHasGyro() const;
	virtual bool bHasOdo() const;
	virtual bool SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData) =0;
	virtual bool SenStb_bDeInit();
	virtual bool SenStb_bInit(char *TripFilePath);

};
#endif // !defined(EA_30ECAF37_EA7D_4b9b_BB27_07C6E5E07E56__INCLUDED_)
