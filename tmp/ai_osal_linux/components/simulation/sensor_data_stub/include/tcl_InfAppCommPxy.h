///////////////////////////////////////////////////////////
//  tcl_InfAppCommPxy.h
//  Implementation of the Interface tcl_InfAppCommPxy
//  Created on:      15-Jul-2015 8:53:16 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_9A4F7E5B_5786_4776_83F3_01E66ED57D4A__INCLUDED_)
#define EA_9A4F7E5B_5786_4776_83F3_01E66ED57D4A__INCLUDED_
#include "tcl_InfAppCommMsgBuilder.h"
class tcl_InfAppCommPxy
{

public:
	tcl_InfAppCommPxy() {

	}

	virtual ~tcl_InfAppCommPxy() {

	}

	virtual bool SenStb_bDeInit() =0;
	virtual bool SenStb_bInit() =0;
	virtual int SenStb_s32SendDataToApp(string &ostrAppMsg) =0;

};
#endif // !defined(EA_9A4F7E5B_5786_4776_83F3_01E66ED57D4A__INCLUDED_)
