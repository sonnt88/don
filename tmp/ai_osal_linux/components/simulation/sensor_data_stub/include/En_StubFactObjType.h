///////////////////////////////////////////////////////////
//  En_StubFactObjType.h
//  Implementation of the Class En_StubFactObjType
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_46F32B60_7539_4df4_909D_C8449A9E35F4__INCLUDED_)
#define EA_46F32B60_7539_4df4_909D_C8449A9E35F4__INCLUDED_


typedef enum{

   SENSOR_CATEGORY_GNSS,
   SENSOR_CATEGORY_POS   

}En_SensorCategory;


typedef enum{
   SENSOR_TYPE_GNSS,
   SENSOR_TYPE_ODOMETER,
   SENSOR_TYPE_GYRO,
   SENSOR_TYPE_ACC,
   SENSOR_TYPE_ABS,
   SENSOR_TYPE_GENERIC

}En_SensorType;


enum En_StubFactObjType
{
	SENSOR_SIMULATION_STUB,
	SENSOR_DATA_SOURCE,
	SENSOR_APP_COMM_PXY,
	SEN_APP_COMM_MSG_BUILDER,
	SEN_GNSS_DATA_BUILDER
};
#endif // !defined(EA_46F32B60_7539_4df4_909D_C8449A9E35F4__INCLUDED_)
