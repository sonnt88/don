///////////////////////////////////////////////////////////
//  tcl_ImpTcpPxy.h
//  Implementation of the Class tcl_ImpTcpPxy
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_6BEAC0E4_DF89_4254_9123_B8F36696A5AB__INCLUDED_)
#define EA_6BEAC0E4_DF89_4254_9123_B8F36696A5AB__INCLUDED_

#include "tcl_ImpAppCommPxy.h"
/* inet_pton() */
#include <arpa/inet.h>
/* htons() */
#include <netinet/in.h>
#include<sys/socket.h>
#include <unistd.h>


class tcl_ImpTcpPxy : public tcl_ImpAppCommPxy
{
protected:
   int s32SocketFD;
   int s32ConnFD;

public:
	tcl_ImpTcpPxy();
	virtual ~tcl_ImpTcpPxy();

	virtual bool SenStb_bDeInit();
	virtual bool SenStb_bInit() =0;
	int SenStb_s32SendDataToApp(string &ostrAppMsg);

};
#endif // !defined(EA_6BEAC0E4_DF89_4254_9123_B8F36696A5AB__INCLUDED_)
