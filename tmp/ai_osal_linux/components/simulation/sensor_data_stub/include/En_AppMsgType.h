///////////////////////////////////////////////////////////
//  En_AppMsgType.h
//  Implementation of the Class En_AppMsgType
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_2D4AB65C_166C_463e_B8AC_42A5F1773C3D__INCLUDED_)
#define EA_2D4AB65C_166C_463e_B8AC_42A5F1773C3D__INCLUDED_

enum En_AppMsgType
{
	/**
	 * Both status and Config are sent currently
	 */
	SEN_STB_APP_MSG_TYPE_INC_STATUS,
	SEN_STB_APP_MSG_TYPE_INC_CONFIG
};
#endif // !defined(EA_2D4AB65C_166C_463e_B8AC_42A5F1773C3D__INCLUDED_)
