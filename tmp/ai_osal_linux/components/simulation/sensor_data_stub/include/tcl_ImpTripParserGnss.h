///////////////////////////////////////////////////////////
//  tcl_ImpTripParserGnss.h
//  Implementation of the Class tcl_ImpTripParserGnss
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_FBEDCB57_2337_47ef_9098_35660FBACF41__INCLUDED_)
#define EA_FBEDCB57_2337_47ef_9098_35660FBACF41__INCLUDED_

#include "tcl_InfSensorData.h"
#include "tcl_ImpTripParser.h"

#ifdef TEMP_INCLUDES

#include "tcl_ImpGnssData.h"

#endif


class tcl_ImpTripParserGnss : public tcl_ImpTripParser
{

private:

   void SenStb_vParseGnssPvtData();
   void SenStb_vParseGnssChannelData();
   void SenStb_vAddPvtData(SenStub_GnssPvtGnssPvtDataMapKey enMapKey, tS8 s8ValChar);
   void SenStb_vAddPvtData(SenStub_GnssPvtGnssPvtDataMapKey enMapKey,tF64 f64ValDouble);
   void SenStb_vAddPvtData(SenStub_GnssPvtGnssPvtDataMapKey enMapKey,tS32 s32ValInt);
   void SenStb_vAddPvtData(SenStub_GnssPvtGnssPvtDataMapKey enMapKey,tU32 u32ValUnsignedInt);
   void SenStb_vAddChannelData(En_GnssChannelDataSeq enMapKey,tU32 u32ValUnsignedInt);
   bool SenStb_bSeekToChanDataStart();
   bool SenStb_bStoreParsedGnssData(tcl_InfSensorData *poInfSensorData);

   /* Member Data */
   map_GnssPvtInfo map_ParGnssPvtInfo;
   map_GnssChannInfo map_ParGnssChannInfo;
   vec_GnsssatInfo vec_ParGnssChanInfo;
   tcl_ImpGnssData oImpGnssData;


public:
	tcl_ImpTripParserGnss();
	virtual ~tcl_ImpTripParserGnss();

	bool bHasGnss();
	bool SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData);
	bool SenStb_bDeInit();
	bool SenStb_bInit(char *TripFilePath);

};
#endif // !defined(EA_FBEDCB57_2337_47ef_9098_35660FBACF41__INCLUDED_)
