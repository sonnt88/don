///////////////////////////////////////////////////////////
//  tcl_InfSensorData.h
//  Implementation of the Interface tcl_InfSensorData
//  Created on:      15-Jul-2015 8:53:16 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_A6362F97_D55B_443e_8236_3E066FCC212C__INCLUDED_)
#define EA_A6362F97_D55B_443e_8236_3E066FCC212C__INCLUDED_
#include "sensor_stub_types.h"

class tcl_InfSensorData
{

public:
	tcl_InfSensorData() {

	}

	virtual ~tcl_InfSensorData() {

	}

   virtual map_GnssPvtInfo SenStb_mapGetGnssPvtData() =0;
   virtual void SenStb_vmapSetGnssPvtData(map_GnssPvtInfo &mParGnssPvtInfo) =0;
   virtual vec_GnsssatInfo SenStb_vecGetGnssSatData() =0;
   virtual void SenStb_vSetGnssSatData(vec_GnsssatInfo &vGnsssatInfo) =0;
   virtual void SenStb_vTraceRecord() =0;

};
#endif // !defined(EA_A6362F97_D55B_443e_8236_3E066FCC212C__INCLUDED_)
