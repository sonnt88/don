#ifndef SENSOR_STUB_TYPES
#define  SENSOR_STUB_TYPES


#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <stdio.h>
#include <cstdlib>
#include <string>

#include "sensor_stub_basic_types.h"
#include "En_StubFactObjType.h"
#include "En_AppMsgType.h"
#include "SenStub_GnssPvtGnssPvtDataMapKey.h"

#define TEMP_INCLUDES


using namespace std;

#define TRIP_FILE_PATH ("trip.txt")
#define GNSS_START_CHANNEL_DATA_KEY ("[GnssChanData]")
#define GNSS_START_PVT_DATA_KEY     ("[GnssPvtData]")


typedef struct {

tU16 u16GnssAzimuth;
tU16 u16GnssCarrierToNoiseRatio;
tU16 u16GnssElevation;
tU16 u16GnssSatStatus;
tU16 u16GnssSvId;

}tr_GnssSatInfo;

typedef union
{
   tS8 s8ValChar;
   tF64 f64ValDouble;
   tS32 s32ValInt;
   tU32 u32ValUnsignedInt;
}Un_KeyValue;

/* Map to hold PVT data */
typedef map<SenStub_GnssPvtGnssPvtDataMapKey,Un_KeyValue> map_GnssPvtInfo;

/* Map to hold channel data */
typedef map<En_GnssChannelDataSeq,Un_KeyValue> map_GnssChannInfo;

typedef vector<map_GnssChannInfo> vec_GnsssatInfo;


typedef struct
{
   tPS8 pcGnssMsgBuff;
   tU32 u32FilledGnssBuffsize;
   tU32 u32MsgBuffSize;

}tr_SensorMsgInfo;


#endif
