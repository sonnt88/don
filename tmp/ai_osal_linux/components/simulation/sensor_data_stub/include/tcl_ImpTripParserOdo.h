///////////////////////////////////////////////////////////
//  tcl_ImpTripParserOdo.h
//  Implementation of the Class tcl_ImpTripParserOdo
//  Created on:      15-Jul-2015 8:53:16 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_CCD5FC1E_77F3_4f41_9D7B_DEC354C5DB6E__INCLUDED_)
#define EA_CCD5FC1E_77F3_4f41_9D7B_DEC354C5DB6E__INCLUDED_

#include "tcl_InfSensorData.h"
#include "tcl_ImpTripParser.h"

class tcl_ImpTripParserOdo : public tcl_ImpTripParser
{

public:
	tcl_ImpTripParserOdo();
	virtual ~tcl_ImpTripParserOdo();

	bool bHasOdo() const;
	virtual bool SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData);
	bool SenStb_bDeInit();
	bool SenStb_bInit(char *TripFilePath);

};
#endif // !defined(EA_CCD5FC1E_77F3_4f41_9D7B_DEC354C5DB6E__INCLUDED_)
