///////////////////////////////////////////////////////////
//  tcl_ImpAccData.h
//  Implementation of the Class tcl_ImpAccData
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_C0175146_C943_4476_90A6_B88D0B53F1C2__INCLUDED_)
#define EA_C0175146_C943_4476_90A6_B88D0B53F1C2__INCLUDED_

#include "tcl_ImpSensorData.h"

class tcl_ImpAccData : public tcl_ImpSensorData
{

public:
	tcl_ImpAccData();
	virtual ~tcl_ImpAccData();

};
#endif // !defined(EA_C0175146_C943_4476_90A6_B88D0B53F1C2__INCLUDED_)
