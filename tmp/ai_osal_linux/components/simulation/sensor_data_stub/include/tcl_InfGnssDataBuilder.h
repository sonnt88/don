///////////////////////////////////////////////////////////
//  tcl_InfGnssDataBuilder.h
//  Implementation of the Interface tcl_InfGnssDataBuilder
//  Created on:      15-Jul-2015 8:53:16 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_0C919D54_757B_4dbb_B4F2_329CE9DE772A__INCLUDED_)
#define EA_0C919D54_757B_4dbb_B4F2_329CE9DE772A__INCLUDED_
#include "sensor_stub_types.h"
#include "tcl_InfSensorDataSource.h"

class tcl_InfGnssDataBuilder
{

public:
	tcl_InfGnssDataBuilder() {

	}

	virtual ~tcl_InfGnssDataBuilder() {

	}

	virtual bool SenStb_bCreateGnssMsg(tcl_InfSensorData *poInfSensorData, string &oStrGnssAppMsg) =0;
	virtual bool SenStb_bDeInit() =0;
	virtual bool SenStb_bInit() =0;

};
#endif // !defined(EA_0C919D54_757B_4dbb_B4F2_329CE9DE772A__INCLUDED_)
