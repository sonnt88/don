///////////////////////////////////////////////////////////
//  tcl_ImpSensorStubMain.h
//  Implementation of the Class tcl_ImpSensorStubMain
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#if !defined(EA_968F966A_F248_4328_A2C1_11040780E9A2__INCLUDED_)
#define EA_968F966A_F248_4328_A2C1_11040780E9A2__INCLUDED_

#include "tcl_InfSensorStubMain.h"
#include "tcl_ImpGnssSensorStub.h"
#include "tcl_ImpPosSensorStub.h"

class tcl_ImpSensorStubMain : public tcl_InfSensorStubMain
{

public:
	tcl_ImpSensorStubMain();
	virtual ~tcl_ImpSensorStubMain();
	tcl_ImpGnssSensorStub *poImpGnssSensorStub;
	tcl_ImpPosSensorStub *poImpPosSensorStub;

	virtual bool SenStb_bDeInit();
	virtual bool SenStb_bInit(char *TripFilePath);
	bool SenStb_bPauseStub();
	bool SenStb_bStartStub();
	bool SenStb_bStopStub();

};
#endif // !defined(EA_968F966A_F248_4328_A2C1_11040780E9A2__INCLUDED_)
