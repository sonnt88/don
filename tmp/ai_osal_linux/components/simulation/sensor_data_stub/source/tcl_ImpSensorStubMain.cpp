///////////////////////////////////////////////////////////
//  tcl_ImpSensorStubMain.cpp
//  Implementation of the Class tcl_ImpSensorStubMain
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpSensorStubMain.h"
#include "tcl_ImpSensorStubFactory.h"


#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_MAIN_CTRL
#include "trcGenProj/Header/tcl_ImpSensorStubMain.cpp.trc.h"
#endif

tcl_ImpSensorStubMain::tcl_ImpSensorStubMain(){

   poImpGnssSensorStub = NULL;
   poImpPosSensorStub = NULL;

}

tcl_ImpSensorStubMain::~tcl_ImpSensorStubMain(){

}

bool tcl_ImpSensorStubMain::SenStb_bDeInit(){

	return 0;
}


bool tcl_ImpSensorStubMain::SenStb_bInit(char *TripFilePath){

   bool bRetVal = false;
   if (NULL == TripFilePath)
   {
      ETG_TRACE_FATAL(("TRIPFILE path is NULL "));
   }
   else
   {
      /* Create GNSS Parser class */
      poImpGnssSensorStub = new tcl_ImpGnssSensorStub;

      if( NULL == poImpGnssSensorStub)
      {
         ETG_TRACE_FATAL(("New operator fail"));
      }
      else
      {
         bRetVal = poImpGnssSensorStub->SenStb_bInit(TripFilePath);         
      }
   }
   return bRetVal;
}


bool tcl_ImpSensorStubMain::SenStb_bPauseStub(){

	return 0;
}


bool tcl_ImpSensorStubMain::SenStb_bStartStub(){

	return poImpGnssSensorStub->SenStb_bStart();
}


bool tcl_ImpSensorStubMain::SenStb_bStopStub(){

	return 0;
}

