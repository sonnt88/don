///////////////////////////////////////////////////////////
//  tcl_ImpGnssData.cpp
//  Implementation of the Class tcl_ImpGnssData
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpGnssData.h"
#include "sensor_stub_trace.h"

// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_GNSS_DATA
#include "trcGenProj/Header/tcl_ImpGnssData.cpp.trc.h"
#endif
extern string GnssChannSeq[];
extern string GnssPvtSeq[];


tcl_ImpGnssData::tcl_ImpGnssData(){

}



tcl_ImpGnssData::~tcl_ImpGnssData(){

}





map_GnssPvtInfo tcl_ImpGnssData::SenStb_mapGetGnssPvtData(){

   return  mGnssPvtInfo;
}


void tcl_ImpGnssData::SenStb_vmapSetGnssPvtData( map_GnssPvtInfo &mParGnssPvtInfo){

   this->mGnssPvtInfo = mParGnssPvtInfo;
}


vec_GnsssatInfo tcl_ImpGnssData::SenStb_vecGetGnssSatData(){


   return  vGnssChanInfo;
}

void tcl_ImpGnssData::SenStb_vSetGnssSatData(vec_GnsssatInfo &vGnsssatInfo){

   this->vGnssChanInfo = vGnsssatInfo;
}

void  tcl_ImpGnssData::SenStb_vTraceRecord()
{


   /* Trace out channel data */
   vec_GnsssatInfo::reverse_iterator ritv = vGnssChanInfo.rbegin();
   map_GnssChannInfo::iterator itm;

   ETG_TRACE_USR4 (( "\n\n\t\tNEW RECORD\n\n"));
   ETG_TRACE_USR4 (( "num of channel elements %d",vGnssChanInfo.size() ));

   /* Loop through all map members in the vector*/
   for (;ritv < vGnssChanInfo.rend(); ritv++)
   {

      itm = (*ritv).begin();
      /* Loop through all members in the map */
      for (;itm != (*ritv).end(); itm++)
         ETG_TRACE_USR4 (( "%20s %d", (GnssChannSeq[itm->first]).c_str() , (itm->second).u32ValUnsignedInt ));
	  ETG_TRACE_USR4 (("\n"));

   }

   /* Trace out PVT data */
   map_GnssPvtInfo::iterator it;

   for(it=mGnssPvtInfo.begin();it!=mGnssPvtInfo.end();it++)
   {
      switch(it->first)
     {
        /* Extraction of decimal unsigned integer values */
        case GnssTimestamp:
        case GnssRecordCounter:
        case GnssNumOfChannels:
        case GnssYear:
        case GnssMonth:
        case GnssDay:
        case GnssHour:
        case GnssMinute:
        case GnssSecond:
        case GnssMilliSeconds:
        case GnssQuality:
        case GnssMode:
        case GnssSatellitesVisible:
        case GnssSatellitesReceived:
        case GnssSatellitesUsed:
        case GnssPositionResidualMax:
        case GnssVelocityResidualMax:
        case GnssConstellationValid:
        {
           ETG_TRACE_USR4 (( "%20s %d",GnssPvtSeq[it->first].c_str(), (it->second).u32ValUnsignedInt ));
           break;
        }
        /* Extraction of floating values */
        case GnssLatitude:
        case GnssLongitude:
        case GnssAltitudeWGS84:
        case GnssGeoidalSeparation:
        case GnssVelocityNorth:
        case GnssVelocityEast:
        case GnssVelocityUp:
        case GnssPositionCovarianceMatrix_0:
        case GnssPositionCovarianceMatrix_4:
        case GnssPositionCovarianceMatrix_5:
        case GnssPositionCovarianceMatrix_8:
        case GnssPositionCovarianceMatrix_9:
        case GnssPositionCovarianceMatrix_10:
        case GnssVelocityCovarianceMatrix_0:
        case GnssVelocityCovarianceMatrix_4:
        case GnssVelocityCovarianceMatrix_5:
        case GnssVelocityCovarianceMatrix_8:
        case GnssVelocityCovarianceMatrix_9:
        case GnssVelocityCovarianceMatrix_10:
        case GnssGDOP:
        case GnssPDOP:
        case GnssHDOP:
        case GnssTDOP:
        case GnssVDOP:
        {
           ETG_TRACE_USR4 (( "%20s %f", GnssPvtSeq[it->first].c_str() , (it->second).f64ValDouble ));
           break;
        }

        /* Extraction of hexadecimal unsigned integer values */
        case GnssSatSysUsed:
        {
		   ETG_TRACE_USR4 (( "%20s %x", GnssPvtSeq[it->first].c_str() , (it->second).u32ValUnsignedInt ));
           break;
        }
        default:
        {
           ETG_TRACE_ERR (("Switch hits deault"));
           break;
        }

      }
   }

}
