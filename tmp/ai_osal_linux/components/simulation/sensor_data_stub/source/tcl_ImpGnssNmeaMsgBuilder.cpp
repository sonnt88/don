///////////////////////////////////////////////////////////
//  tcl_ImpGnssNmeaMsgBuilder.cpp
//  Implementation of the Class tcl_ImpGnssNmeaMsgBuilder
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpGnssNmeaMsgBuilder.h"
#include <iomanip>
#include <cmath>
#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_NMEA_MSG_BLD
#include "trcGenProj/Header/tcl_ImpGnssNmeaMsgBuilder.cpp.trc.h"
#endif

const double PI = 3.14159265358979323846;

tcl_ImpGnssNmeaMsgBuilder::tcl_ImpGnssNmeaMsgBuilder(){

}

tcl_ImpGnssNmeaMsgBuilder::~tcl_ImpGnssNmeaMsgBuilder(){

}

inline double tcl_ImpGnssNmeaMsgBuilder::SenStb_dradToDeg(double angleRadians)
{

	return (angleRadians * 180.0 / PI);
}

bool tcl_ImpGnssNmeaMsgBuilder::SenStb_bCreateGnssMsg(tcl_InfSensorData *poInfSensorData, string &oStrGnssAppMsg){


   map_GnssPvtInfo mapGnssPvtInfo(poInfSensorData->SenStb_mapGetGnssPvtData());
   vec_GnsssatInfo vecGnsssatInfo (poInfSensorData->SenStb_vecGetGnssSatData());

//#ifdef TRACING_ON
   //poInfSensorData->SenStb_vTraceRecord();
//#endif
   
   /* clear the contents of the string */
   oStrGnssAppMsg.clear();
   /* Add time stamp to the Message */
   SenStb_vAddTimeStamp(mapGnssPvtInfo, oStrGnssAppMsg);
   SenStb_vAddNmeaSeq( mapGnssPvtInfo, vecGnsssatInfo, oStrGnssAppMsg);

   
   return false;
}


bool tcl_ImpGnssNmeaMsgBuilder::SenStb_bDeInit(){

   return 0;
}


bool tcl_ImpGnssNmeaMsgBuilder::SenStb_bInit(){

   return 0;
}

void tcl_ImpGnssNmeaMsgBuilder::SenStb_vAddTimeStamp(map_GnssPvtInfo &mapGnssPvtInfo, string &oStrGnssAppMsg)
{
   oStrGnssAppMsg += char ((mapGnssPvtInfo[GnssTimestamp]).u32ValUnsignedInt >> 0);
   oStrGnssAppMsg += char ((mapGnssPvtInfo[GnssTimestamp]).u32ValUnsignedInt >> 8);
   oStrGnssAppMsg += char ((mapGnssPvtInfo[GnssTimestamp]).u32ValUnsignedInt >> 16);
   oStrGnssAppMsg += char ((mapGnssPvtInfo[GnssTimestamp]).u32ValUnsignedInt >> 24);
   
}

/* Get All the GNSS nmea messages into a string object */
void tcl_ImpGnssNmeaMsgBuilder::SenStb_vAddNmeaSeq(map_GnssPvtInfo &mapGnssPvtInfo,vec_GnsssatInfo &vecGnsssatInfo, string &oStrGnssAppMsg)
{
   oStrGnssAppMsg += SenStb_strGprmcMsg(mapGnssPvtInfo, vecGnsssatInfo);
   oStrGnssAppMsg += SenStb_strGpggaMsg(mapGnssPvtInfo, vecGnsssatInfo);
   oStrGnssAppMsg += SenStb_strGpgsaMsg(mapGnssPvtInfo, vecGnsssatInfo);
   oStrGnssAppMsg += SenStb_strGpgsvMsg(mapGnssPvtInfo, vecGnsssatInfo);
   oStrGnssAppMsg += SenStb_strPstmpvMsg(mapGnssPvtInfo, vecGnsssatInfo);
   oStrGnssAppMsg += SEN_STB_PSTMCPU_MSG;

   SenStb_vClearSSObj();
   ETG_TRACE_USR4 (( "%s", oStrGnssAppMsg.c_str()));
}


/*$GPRMC,<Timestamp>,<Status>,<Lat>,<N/S>,<Long>,<E/W>,<Speed>, <Trackgood>,<Date>,<MagVar>,<MagVarDir> <checksum><cr><lf>*/

string tcl_ImpGnssNmeaMsgBuilder::SenStb_strGprmcMsg(map_GnssPvtInfo &mapGnssPvtInfo,vec_GnsssatInfo &vecGnsssatInfo)
{
   string OstrGprmc(SEN_STB_GPRMC_HEADER);
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;

   /* Add the time stamp field to GPRMC MSG */   
   OstrGprmc += SenStb_StrGetTimestamp(mapGnssPvtInfo);
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------Add Time validity Info---------------------------------*/
   if (0 !=  mapGnssPvtInfo[GnssYear].u32ValUnsignedInt)
   {
      OstrGprmc += SEN_STB_NMEA_GPRMC_TIME_VALID;
   }
   else
   {
      OstrGprmc += SEN_STB_NMEA_GPRMC_TIME_INVALID;
   }
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;
   

   /*------------------------------Get Latitude Magnitude---------------------------------*/
   OstrGprmc += SenStb_StrGetLat(mapGnssPvtInfo);
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;
   /* Compute Latitude Direction : North if positive,  south if negative*/
   if(0 > (mapGnssPvtInfo[GnssLatitude].f64ValDouble))
   {
      OstrGprmc += SEN_STB_NMEA_LATITUDE_DIRECTION_SOUTH;
   }
   else
   {
      OstrGprmc += SEN_STB_NMEA_LATITUDE_DIRECTION_NORTH;
   }
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;
   
   /*------------------------------Get Longitude Magnitude---------------------------------*/
   OstrGprmc += SenStb_StrGetLon(mapGnssPvtInfo);
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;
   /* Compute Longitude Direction : East if positive,  West if negative*/
   if(0 > (mapGnssPvtInfo[GnssLongitude].f64ValDouble))
   {
      OstrGprmc += SEN_STB_NMEA_LONGITUDE_DIRECTION_WEST;
   }
   else
   {
      OstrGprmc += SEN_STB_NMEA_LONGITUDE_DIRECTION_EAST;
   }
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;

   /*-----------------------------------Speed in Knots---------------------------------*/
   /*Not used by Driver currently*/
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;

   /*-------------------------------------Trackgood-------------------------------------*/
   /*Not used by Driver currently*/
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;

   /*-------------------------------------Date ddmmyy-------------------------------------*/
   OstrGprmc += SenStb_StrGetDate(mapGnssPvtInfo);
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;

   /*-------------------------------------MagVar-------------------------------------*/
   /*Not used by Driver currently*/
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;

   /*-------------------------------------MagVarDir-------------------------------------*/
   /*Not used by Driver currently*/
   /* This delimiter should not be present. This looks as a bug from teseo */
   OstrGprmc += SEN_STB_NMEA_FIELD_DELILITER;

   /*----------------------------- Add checksum to the message -----------------------*/
   OstrGprmc += SenStb_StrCalcNmeaChksum(OstrGprmc);

   /*----------------------------- Add Msg Delimiters -----------------------*/
   OstrGprmc += SEN_STB_NMEA_MSG_TERMINATOR;

   /*-------------------------------------The End Of GPRMC data--------------------------*/

   return OstrGprmc;

}

/*$GPGGA,<Timestamp>,<Lat>,<N/S>,<Long>,<E/W>,<GPSQual>,<Sats>,<HDOP>, <Alt>,<AltVal>,<GeoSep>,<GeoVal>,
               <DGPSAge>,<DGPSRef>, <checksum><cr><lf> */

string tcl_ImpGnssNmeaMsgBuilder::SenStb_strGpggaMsg(map_GnssPvtInfo &mapGnssPvtInfo,vec_GnsssatInfo &vecGnsssatInfo)
{
   string OstrGpgga(SEN_STB_GPGGA_HEADER);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;

   /* Add the time stamp field to GPGGA MSG */   
   OstrGpgga += SenStb_StrGetTimestamp(mapGnssPvtInfo);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------Get Latitude Magnitude---------------------------------*/
   OstrGpgga += SenStb_StrGetLat(mapGnssPvtInfo);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;
   /* Compute Latitude Direction : North if positive,  south if negative*/
   if(0 > (mapGnssPvtInfo[GnssLatitude].f64ValDouble))
   {
      OstrGpgga += SEN_STB_NMEA_LATITUDE_DIRECTION_SOUTH;
   }
   else
   {
      OstrGpgga += SEN_STB_NMEA_LATITUDE_DIRECTION_NORTH;
   }
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;
   
   /*------------------------------Get Longitude Magnitude---------------------------------*/
   OstrGpgga += SenStb_StrGetLon(mapGnssPvtInfo);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;
   /* Compute Longitude Direction : East if positive,  West if negative*/
   if(0 > (mapGnssPvtInfo[GnssLongitude].f64ValDouble))
   {
      OstrGpgga += SEN_STB_NMEA_LONGITUDE_DIRECTION_WEST;
   }
   else
   {
      OstrGpgga += SEN_STB_NMEA_LONGITUDE_DIRECTION_EAST;
   }
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------Get GNSS Quality---------------------------------*/
   OstrGpgga+= SenStb_StrGetFixQuality(mapGnssPvtInfo);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------Get GNSS Sats Used for Fix--------------------------*/
   OstrGpgga+= SenStb_StrGetSatUsed(mapGnssPvtInfo);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------HDOP--------------------------*/
   OstrGpgga+= SenStb_StrGetHDop(mapGnssPvtInfo);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------Altitude--------------------------*/
   OstrGpgga+= SenStb_StrGetAlt(mapGnssPvtInfo);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;
   /* AltVal: Unit for Altitude */
   OstrGpgga += SEN_STB_NMEA_ALTITUDE_UNIT;
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------Geoidal Sep--------------------------*/
   OstrGpgga+= SenStb_StrGetGeoSep(mapGnssPvtInfo);
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;
   /* GeoVal :Unit for Geodial Separation */
   OstrGpgga += SEN_STB_NMEA_ALTITUDE_UNIT;
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------DGPSAge--------------------------*/
   /*Not Used By driver*/
   OstrGpgga += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------DGPSRef--------------------------*/
   /*Not Used By driver*/

   /*----------------------------- Add checksum to the message -----------------------*/
   OstrGpgga += SenStb_StrCalcNmeaChksum(OstrGpgga);

   /*----------------------------- Add Msg Delimiters -----------------------*/
   OstrGpgga += SEN_STB_NMEA_MSG_TERMINATOR;

   /*-------------------------------------The End Of GPGGA data--------------------------*/

   return OstrGpgga;
}


string tcl_ImpGnssNmeaMsgBuilder::SenStb_strGpgsaMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                                              vec_GnsssatInfo &vecGnsssatInfo )
{
   string oStrGpgsa;
   int s32MsgRepCnt;

   s32MsgRepCnt = mapGnssPvtInfo[GnssSatellitesUsed].u32ValUnsignedInt;
   s32MsgRepCnt =  int(ceil(float(s32MsgRepCnt)/SEN_STB_GSA_NUM_SATS_PER_MSG));

   ETG_TRACE_USR4 (( "GSA Cnt %d", s32MsgRepCnt));
   
   for(int CurrMsgNum = 0;CurrMsgNum < s32MsgRepCnt; CurrMsgNum++ )
   {
      oStrGpgsa += SenStb_strSingleGpgsaMsg(mapGnssPvtInfo,vecGnsssatInfo, CurrMsgNum);
   }

   return oStrGpgsa;
}

/* $--GSA,<Mode>,<CurrentMode>,[<SatPRN1>],...,[<SatPRNN>], <PDOP>,<HDOP>,<VDOP>, <checksum><cr><lf> */
string tcl_ImpGnssNmeaMsgBuilder::SenStb_strSingleGpgsaMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                                            vec_GnsssatInfo &vecGnsssatInfo,
                                                            int MsgCnt)
{
   string oStrGpgsa(SEN_STB_GPGSA_HEADER);
   oStrGpgsa += SEN_STB_NMEA_FIELD_DELILITER;

   stringstream oss;
   vec_GnsssatInfo::iterator itv = vecGnsssatInfo.begin();
   int SatSkiped = 0;
   int SatToSkip = (int(SEN_STB_GSA_NUM_SATS_PER_MSG) * MsgCnt);
   int SatPicked = 0;

   /*----------------------------- FIX ACQ Mode -----------------------*/
   oStrGpgsa += SEN_STB_GPGSA_FIX_MODE_AUTOMATIC;
   oStrGpgsa += SEN_STB_NMEA_FIELD_DELILITER;

   /*----------------------------- FIX TYPE NO/2D/3D -----------------------*/
   oStrGpgsa += SenStb_StrGetFixtype(mapGnssPvtInfo);
   oStrGpgsa += SEN_STB_NMEA_FIELD_DELILITER;

   /*----------------------------- Loop Through to find sats used for fix -----------------------*/
   for (;(itv < vecGnsssatInfo.end()) && (SatPicked < (int(SEN_STB_GSA_NUM_SATS_PER_MSG))) ;itv++ )
   {
      if (SEN_STB_SAT_USED_FOR_POSCALC == (*itv)[GnssSatStatus].u32ValUnsignedInt )
      {
         /* Skip satellites as these were picked in previous Msg */
         if( SatSkiped < SatToSkip )
         {
            SatSkiped++;           
         }
         else
         {
            /* Pick This satellite */
            SatPicked++;
            oss << (*itv)[GnssSvId].u32ValUnsignedInt;
            oss << SEN_STB_NMEA_FIELD_DELILITER;
         }
      }
   }
   /* Add Collected satellites to GSA message */
   oStrGpgsa += oss.str();
   /* Fill remaining spaces */
   for (;SatPicked < (int(SEN_STB_GSA_NUM_SATS_PER_MSG)); SatPicked++)
   {
      oStrGpgsa += SEN_STB_NMEA_FIELD_DELILITER;
   }
   /*------------------------------HDOP--------------------------*/
   oStrGpgsa+= SenStb_StrGetHDop(mapGnssPvtInfo);
   oStrGpgsa += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------PDOP--------------------------*/
   oStrGpgsa+= SenStb_StrGetPDop(mapGnssPvtInfo);
   oStrGpgsa += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------VDOP--------------------------*/
   oStrGpgsa+= SenStb_StrGetVDop(mapGnssPvtInfo);

   /*----------------------------- Add checksum to the message -----------------------*/
   oStrGpgsa += SenStb_StrCalcNmeaChksum(oStrGpgsa);

   /*----------------------------- Add Msg Delimiters -----------------------*/
   oStrGpgsa += SEN_STB_NMEA_MSG_TERMINATOR;

   /*-------------------------------------The End Of GPGSA data--------------------------*/

   return oStrGpgsa;
}


string tcl_ImpGnssNmeaMsgBuilder::SenStb_strGpgsvMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                                              vec_GnsssatInfo &vecGnsssatInfo )
{
   string oStrGpgsv;
   int s32MsgRepCnt;
   int CurrMsgNum;

   s32MsgRepCnt = mapGnssPvtInfo[GnssSatellitesVisible].u32ValUnsignedInt;
   s32MsgRepCnt =  int(ceil(float(s32MsgRepCnt)/SEN_STB_NUM_OF_GSV_SAT_INFO_IN_SINGLE_MSG));

   ETG_TRACE_USR4 (( "GSV Cnt %d", s32MsgRepCnt));

   for(int CurrMsgNum = 0;CurrMsgNum < s32MsgRepCnt; CurrMsgNum++ )
   {
      oStrGpgsv += SenStb_strSingleGpgsvMsg(mapGnssPvtInfo,vecGnsssatInfo, CurrMsgNum, s32MsgRepCnt);
   }


   return oStrGpgsv;
}

/*$--GSV,<GSVAmount>,<GSVNumber>,<TotSats>, [<Sat1PRN>,<Sat1Elev>,<Sat1Azim>,<Sat1C/N0>], ... [<SatNPRN>,<SatNElev>,<SatNAzim>,<SatNC/N0>], <checksum><cr><lf>*/

string tcl_ImpGnssNmeaMsgBuilder::SenStb_strSingleGpgsvMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                                            vec_GnsssatInfo &vecGnsssatInfo,
                                                            int CurrMsgNum,
                                                            int TotalNumMsgs)
{
   string oStrGpgsv(SEN_STB_GPGSV_HEADER);
   oStrGpgsv += SEN_STB_NMEA_FIELD_DELILITER;
   int SatPicked = 0;

   stringstream oss;
   vec_GnsssatInfo::iterator itv = vecGnsssatInfo.begin();
   /*Jump All parsed Channel Info*/
   itv += (CurrMsgNum  * int(SEN_STB_NUM_OF_GSV_SAT_INFO_IN_SINGLE_MSG));


   /*----------------------------- Add Total number of GSV Messages -----------------------*/
   oss << TotalNumMsgs;
   oStrGpgsv += oss.str();
   /* Clear oss for next operation */
   oss.str("");
   oStrGpgsv += SEN_STB_NMEA_FIELD_DELILITER;

   /*----------------------------- Add Current GSV Message Number -----------------------*/
   oss << (CurrMsgNum +1);
   oStrGpgsv += oss.str();
   /* Clear oss for next operation */
   oss.str("");
   oStrGpgsv += SEN_STB_NMEA_FIELD_DELILITER;


   /*------------------------------Add Total Sats in View--------------------------*/
   oStrGpgsv+= SenStb_StrGetSatInView(mapGnssPvtInfo);
   oStrGpgsv += SEN_STB_NMEA_FIELD_DELILITER;

   /*----------------------------- Loop Through to find sats Visible -----------------------*/
   for (;(itv < vecGnsssatInfo.end()) && (SatPicked < (int(SEN_STB_NUM_OF_GSV_SAT_INFO_IN_SINGLE_MSG))) 
        ;itv++ )
   {
      /* Pick This satellite */
      SatPicked++;
      /* Satellite ID */
      oss << (*itv)[GnssSvId].u32ValUnsignedInt;
      oss << SEN_STB_NMEA_FIELD_DELILITER;
      /* Satellite Elevation */
      oss << (*itv)[GnssElevation].u32ValUnsignedInt;
      oss << SEN_STB_NMEA_FIELD_DELILITER;
      /* Satellite Azimuthal */
      oss << (*itv)[GnssAzimuth].u32ValUnsignedInt;
      oss << SEN_STB_NMEA_FIELD_DELILITER;
      /* Satellite C/NO */
      oss << (*itv)[GnssCarrierToNoiseRatio].u32ValUnsignedInt;
      oss << SEN_STB_NMEA_FIELD_DELILITER;
   }
   /* Add Collected satellites to GSV message */
   oStrGpgsv += oss.str();

   /* Fill remaining spaces */
   for (;SatPicked < (int(SEN_STB_NUM_OF_GSV_SAT_INFO_IN_SINGLE_MSG)); SatPicked++)
   {
      oStrGpgsv += SEN_STB_NMEA_FIELD_DELILITER;
      oStrGpgsv += SEN_STB_NMEA_FIELD_DELILITER;
      oStrGpgsv += SEN_STB_NMEA_FIELD_DELILITER;
      oStrGpgsv += SEN_STB_NMEA_FIELD_DELILITER;
   }

   /*----------------------------- Remove last delimiter added above-----------------------*/
   oStrGpgsv.erase(oStrGpgsv.size() - 1);

   /*----------------------------- Add checksum to the message -----------------------*/
   oStrGpgsv += SenStb_StrCalcNmeaChksum(oStrGpgsv);

   /*----------------------------- Add Msg Delimiters -----------------------*/
   oStrGpgsv += SEN_STB_NMEA_MSG_TERMINATOR;

   /*-------------------------------------The End Of GPGSV data--------------------------*/

   return oStrGpgsv;
}


/*$PSTMPV,<Timestamp>,<Lat>,<N/S>,<Long>,<E/W>,<Alt>,<AltVal>,<Vel_N>,<Vel_E>,<Vel_V>,<P_cov_N>,
   <P_cov_NE>,<P_cov_NV>,<P_cov_E>,<P_cov_EV>,<P_cov_V>,<V_cov_N>,<V_cov_NE>,<V_cov_NV>,
   <V_cov_E>,<V_cov_EV>,<V_cov_V>*<checksum><cr><lf>*/

string tcl_ImpGnssNmeaMsgBuilder::SenStb_strPstmpvMsg( map_GnssPvtInfo &mapGnssPvtInfo,
                                                              vec_GnsssatInfo &vecGnsssatInfo )
{

   string OstrPstmpv(SEN_STB_PSTMPV_HEADER);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;

   /* Add the time stamp field to PSTMPV MSG */   
   OstrPstmpv += SenStb_StrGetTimestamp(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------Get Latitude Magnitude---------------------------------*/
   OstrPstmpv += SenStb_StrGetLat(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /* Compute Latitude Direction : North if positive,  south if negative*/
   if(0 > (mapGnssPvtInfo[GnssLatitude].f64ValDouble))
   {
      OstrPstmpv += SEN_STB_NMEA_LATITUDE_DIRECTION_SOUTH;
   }
   else
   {
      OstrPstmpv += SEN_STB_NMEA_LATITUDE_DIRECTION_NORTH;
   }
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------Get Longitude Magnitude---------------------------------*/
   OstrPstmpv += SenStb_StrGetLon(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /* Compute Longitude Direction : East if positive,  West if negative*/
   if(0 > (mapGnssPvtInfo[GnssLongitude].f64ValDouble))
   {
      OstrPstmpv += SEN_STB_NMEA_LONGITUDE_DIRECTION_WEST;
   }
   else
   {
      OstrPstmpv += SEN_STB_NMEA_LONGITUDE_DIRECTION_EAST;
   }
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;


   /*------------------------------Altitude--------------------------*/
   OstrPstmpv+= SenStb_StrGetAlt(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /* AltVal: Unit for Altitude */
   OstrPstmpv += SEN_STB_NMEA_ALTITUDE_UNIT;
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;


   /*------------------------------VEL-NOR--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelNorth(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------VEL-East--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelEast(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------VEL-UP--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelUp(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;

   /*------------------------------POS_COV-NOR--------------------------*/
   OstrPstmpv+= SenStb_StrGetPosCovNorth(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------POS_COV-NOR_EAST--------------------------*/
   OstrPstmpv+= SenStb_StrGetPosCovNorEast(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------POS_COV-NOR_UP--------------------------*/
   OstrPstmpv+= SenStb_StrGetPosCovNorUp(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------POS_COV-EAST--------------------------*/
   OstrPstmpv+= SenStb_StrGetPosCovEast(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------POS_COV-EAST_UP--------------------------*/
   OstrPstmpv+= SenStb_StrGetPosCovEastUp(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;  
   /*------------------------------POS_COV-UP--------------------------*/
   OstrPstmpv+= SenStb_StrGetPosCovUp(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;


   /*------------------------------VEL_COV-NOR--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelCovNorth(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------VEL_COV-NOR_EAST--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelCovNorthEast(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------VEL_COV-NOR_UP--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelCovNorUp(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------VEL_COV-EAST--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelCovEast(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;
   /*------------------------------VEL_COV-EAST_UP--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelCovEastUp(mapGnssPvtInfo);
   OstrPstmpv += SEN_STB_NMEA_FIELD_DELILITER;  
   /*------------------------------VEL_COV-UP--------------------------*/
   OstrPstmpv+= SenStb_StrGetVelCovUp(mapGnssPvtInfo);

   /*----------------------------- Add checksum to the message -----------------------*/
   OstrPstmpv += SenStb_StrCalcNmeaChksum(OstrPstmpv);

   /*----------------------------- Add Msg Delimiters -----------------------*/
   OstrPstmpv += SEN_STB_NMEA_MSG_TERMINATOR;

   /*-------------------------------------The End Of PSTMPV data--------------------------*/

   return OstrPstmpv;
}

/* Getters for the NMEA fields */
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetTimestamp(map_GnssPvtInfo &mapGnssPvtInfo)
{

   if (0 == (ossPerArr[SENSTB_OSSPERTIMESTAMP].str()).size())
   {
   
      ossPerArr[SENSTB_OSSPERTIMESTAMP] << setfill('0'); /* Fill the prefix with '0' to get required field length*/
      /* Set width and store data */
      /*hour width 2 */
      ossPerArr[SENSTB_OSSPERTIMESTAMP] << setw(2)<< mapGnssPvtInfo[GnssHour].u32ValUnsignedInt;
      /*minute width 2 */
      ossPerArr[SENSTB_OSSPERTIMESTAMP] << setw(2)<< mapGnssPvtInfo[GnssMinute].u32ValUnsignedInt;
      /*second width 2 */
      ossPerArr[SENSTB_OSSPERTIMESTAMP] << setw(2) <<mapGnssPvtInfo[GnssSecond].u32ValUnsignedInt ;
      /*mili-second width 3 */
      ossPerArr[SENSTB_OSSPERTIMESTAMP] << '.' << setw(3) << mapGnssPvtInfo[GnssMilliSeconds].u32ValUnsignedInt;
     
   }
   return ossPerArr[SENSTB_OSSPERTIMESTAMP].str();

}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetLat(map_GnssPvtInfo &mapGnssPvtInfo)
{
   double dLatitude;
   double dMinutes;

   if (0 == (ossPerArr[SENSTB_OSSPERLAT].str()).size())
   {
         /*Get Degrees from radians*/
         dLatitude = abs(SenStb_dradToDeg(mapGnssPvtInfo[GnssLatitude].f64ValDouble));
         /* Get Decimal Degrees */
         ossPerArr[SENSTB_OSSPERLAT] << int(dLatitude);
         /* Extract minues from Degrees*/
         dMinutes = (dLatitude - int(dLatitude)) * 60;
         /* Add Minutes to Lat field */
         ossPerArr[SENSTB_OSSPERLAT] << dMinutes;

   }      
   return ossPerArr[SENSTB_OSSPERLAT].str();

}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetLon(map_GnssPvtInfo &mapGnssPvtInfo)
{
   double dLongitude;
   double dMinutes;

   if (0 == (ossPerArr[SENSTB_OSSPERLON].str()).size())
   {
         /*Get Degrees from radians*/
         dLongitude = abs(SenStb_dradToDeg(mapGnssPvtInfo[GnssLongitude].f64ValDouble));
         /* Get Decimal Degrees */
         ossPerArr[SENSTB_OSSPERLON] << int(dLongitude);
         /* Extract minues from Degrees*/
         dMinutes = (dLongitude - int(dLongitude)) * 60;
         /* Add Minutes to Lon field */
         ossPerArr[SENSTB_OSSPERLON] << dMinutes;

   }
   return ossPerArr[SENSTB_OSSPERLON].str();

}

string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetDate(map_GnssPvtInfo &mapGnssPvtInfo)
{
   
   if (0 == (ossPerArr[SENSTB_OSSPERDATE].str()).size())
   {
   
      ossPerArr[SENSTB_OSSPERDATE] << setfill('0'); /* Fill the prefix with '0' to get required field length*/
      /* Set width and store data */
      /*Day width 2 */
      ossPerArr[SENSTB_OSSPERDATE] << setw(2) << mapGnssPvtInfo[GnssDay].u32ValUnsignedInt;
      /*Month width 2 */
      ossPerArr[SENSTB_OSSPERDATE] << setw(2) << mapGnssPvtInfo[GnssMonth].u32ValUnsignedInt;
      /*Year width 2 and remove the cetury offset */
      ossPerArr[SENSTB_OSSPERDATE] << setw(2) << ((mapGnssPvtInfo[GnssYear].u32ValUnsignedInt) % 100 ) ;
   }

   return ossPerArr[SENSTB_OSSPERDATE].str();
}

string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetFixQuality(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERFIXQUAL].str().size()))
   {
      ossPerArr[SENSTB_OSSPERFIXQUAL] << mapGnssPvtInfo[GnssQuality].u32ValUnsignedInt;
   }
   
   return ossPerArr[SENSTB_OSSPERFIXQUAL].str();
}


string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetSatUsed(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERSATUSED].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERSATUSED] << mapGnssPvtInfo[GnssSatellitesUsed].u32ValUnsignedInt;
   }

   return ossPerArr[SENSTB_OSSPERSATUSED].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetHDop(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERHDOP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERHDOP] << mapGnssPvtInfo[GnssHDOP].f64ValDouble;
   }

   return ossPerArr[SENSTB_OSSPERHDOP].str();

}

string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetAlt(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERALT].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERALT] << (mapGnssPvtInfo[GnssAltitudeWGS84].f64ValDouble - 
                                  mapGnssPvtInfo[GnssGeoidalSeparation].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERALT].str();

}

string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetGeoSep(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERGEOSEP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERGEOSEP] << mapGnssPvtInfo[GnssGeoidalSeparation].f64ValDouble;
   }

   return ossPerArr[SENSTB_OSSPERGEOSEP].str();
}


string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetFixtype(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERFIXTYPE].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERFIXTYPE] << mapGnssPvtInfo[GnssMode].u32ValUnsignedInt;
   }

   return ossPerArr[SENSTB_OSSPERFIXTYPE].str();

}


string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetPDop(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERPDOP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERPDOP] << mapGnssPvtInfo[GnssPDOP].f64ValDouble;
   }

   return ossPerArr[SENSTB_OSSPERPDOP].str();

}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVDop(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVDOP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVDOP] << mapGnssPvtInfo[GnssVDOP].f64ValDouble;
   }

   return ossPerArr[SENSTB_OSSPERVDOP].str();

}


string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetSatInView(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERSATINVIEW].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERSATINVIEW] << mapGnssPvtInfo[GnssSatellitesVisible].u32ValUnsignedInt;
   }

   return ossPerArr[SENSTB_OSSPERSATINVIEW].str();

}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelNorth(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELNORTH].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELNORTH] << mapGnssPvtInfo[GnssVelocityNorth].f64ValDouble;
   }

   return ossPerArr[SENSTB_OSSPERVELNORTH].str();

}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelEast(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELEAST].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELEAST]<< mapGnssPvtInfo[GnssVelocityEast].f64ValDouble;
   }

   return ossPerArr[SENSTB_OSSPERVELEAST].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelUp(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELUP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELUP] << mapGnssPvtInfo[GnssVelocityUp].f64ValDouble;
   }

   return ossPerArr[SENSTB_OSSPERVELUP].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetPosCovNorth(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERPOSCOVNORTH].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERPOSCOVNORTH] << sqrt(mapGnssPvtInfo[GnssPositionCovarianceMatrix_0].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERPOSCOVNORTH].str();
}

string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetPosCovNorEast(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERPOSCOVNOREAST].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERPOSCOVNOREAST] << sqrt(mapGnssPvtInfo[GnssPositionCovarianceMatrix_4].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERPOSCOVNOREAST].str();
}

string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetPosCovNorUp(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERPOSCOVNORUP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERPOSCOVNORUP] << sqrt(mapGnssPvtInfo[GnssPositionCovarianceMatrix_8].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERPOSCOVNORUP].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetPosCovEast(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERPOSCOVEAST].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERPOSCOVEAST] << sqrt(mapGnssPvtInfo[GnssPositionCovarianceMatrix_5].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERPOSCOVEAST].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetPosCovEastUp(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERPOSCOVEASTUP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERPOSCOVEASTUP] << sqrt(mapGnssPvtInfo[GnssPositionCovarianceMatrix_9].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERPOSCOVEASTUP].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetPosCovUp(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERPOSCOVUP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERPOSCOVUP] << sqrt(mapGnssPvtInfo[GnssPositionCovarianceMatrix_10].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERPOSCOVUP].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelCovNorth(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELCOVNORTH].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELCOVNORTH] << sqrt(mapGnssPvtInfo[GnssVelocityCovarianceMatrix_0].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERVELCOVNORTH].str();
}

string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelCovNorthEast(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELCOVNORTHEAST].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELCOVNORTHEAST] << sqrt(mapGnssPvtInfo[GnssVelocityCovarianceMatrix_4].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERVELCOVNORTHEAST].str();
}

string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelCovNorUp(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELCOVNORUP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELCOVNORUP] << sqrt(mapGnssPvtInfo[GnssVelocityCovarianceMatrix_8].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERVELCOVNORUP].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelCovEast(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELCOVEAST].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELCOVEAST] << sqrt(mapGnssPvtInfo[GnssVelocityCovarianceMatrix_5].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERVELCOVEAST].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelCovEastUp(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELCOVEASTUP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELCOVEASTUP] << sqrt(mapGnssPvtInfo[GnssVelocityCovarianceMatrix_9].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERVELCOVEASTUP].str();
}
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrGetVelCovUp(map_GnssPvtInfo &mapGnssPvtInfo)
{
   if (0 == (ossPerArr[SENSTB_OSSPERVELCOVUP].str()).size() )
   {
      ossPerArr[SENSTB_OSSPERVELCOVUP] << sqrt(mapGnssPvtInfo[GnssVelocityCovarianceMatrix_10].f64ValDouble);
   }

   return ossPerArr[SENSTB_OSSPERVELCOVUP].str();
}
/* Clean all the string stream objects used for NMEA creation */
void tcl_ImpGnssNmeaMsgBuilder::SenStb_vClearSSObj()
{
	for( int i=0; i < SENSTB_OSSMAXLIMIT; i++ )
		ossPerArr[i].str("");
}


/* Returns the checksum of the NMEA message in OstrGprmc*/
string tcl_ImpGnssNmeaMsgBuilder::SenStb_StrCalcNmeaChksum(string &OstrGprmc)
{
   tU8 u8Chksum = 0;
   stringstream oss;
   std::string::iterator it=OstrGprmc.begin();
   /* $ is not a part of message. No Chksum for dollar*/
   it++;
   for ( ;it!=OstrGprmc.end(); it++)
   {
      (u8Chksum) ^= (*it);
   }
   oss << setfill('0'); /* Fill the prefix with '0' to get required field length*/
   /* Set width as 2 */
   oss << SEN_STB_NMEA_CHECKSUM_DELILITER << setw(2) << hex << int(u8Chksum) << dec; 

   return oss.str();
}
