///////////////////////////////////////////////////////////
//  tcl_ImpTcpPxyGnss.cpp
//  Implementation of the Class tcl_ImpTcpPxyGnss
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpTcpPxyGnss.h"
#include <string.h>

#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_TCP_PXY_GNSS
#include "trcGenProj/Header/tcl_ImpTcpPxyGnss.cpp.trc.h"
#endif

tcl_ImpTcpPxyGnss::tcl_ImpTcpPxyGnss(){

}



tcl_ImpTcpPxyGnss::~tcl_ImpTcpPxyGnss(){

}





bool tcl_ImpTcpPxyGnss::SenStb_bDeInit(){

	return 0;
}


bool tcl_ImpTcpPxyGnss::SenStb_bInit(){

   struct sockaddr_in rSocAttr;
   bool bRetVal = false;
   int s32OptState = 1;
   int s32ErrChk;

   memset(&rSocAttr,0, sizeof(rSocAttr));
   rSocAttr.sin_family      = AF_INET;
   rSocAttr.sin_port        = htons(0x08); // Todo: this has to be changed
   rSocAttr.sin_addr.s_addr = htonl(INADDR_ANY);

   /* create a socket
      SOCK_STREAM: sequenced, reliable, two-way, connection-based
      byte streams with an OOB data transmission mechanism.*/
   s32SocketFD = socket(AF_INET, SOCK_STREAM, 0);
   if(s32SocketFD == -1)
   {
      perror("socket() failed");
   }
   else
   {
      /* This option informs os to reuse the socket address even if it is busy*/
      if(setsockopt(s32SocketFD, SOL_SOCKET, SO_REUSEADDR,
                    &s32OptState, sizeof(s32OptState)) == -1)
      {
         perror( "!!!setsockopt() Failed");
      }
      /* Bind the socket created to a address */
      s32ErrChk = bind(s32SocketFD, (struct sockaddr *)&rSocAttr, sizeof(rSocAttr));
      if(s32ErrChk == -1)
      {
         perror("Bind Failed");
      }
      else
      {
         /*Listen system call makes the socket as a passive socket. 
                  i.e socket will be used to accept incoming connections*/
         s32ErrChk = listen(s32SocketFD, 10);
         if(s32ErrChk == -1)
         {
            perror("Listen Failed");
         }
         else
         {
            s32ConnFD = accept(s32SocketFD, NULL, NULL);
            if(s32ConnFD == -1)
            {
               perror( "accept Failed !!!");
               /* This should never happen. Only reason for this is something went
                          wrong while configuring the socket*/
            }
      	   else
      	   {
               ETG_TRACE_COMP(( "Socket creation and Setup completed successfully"));
               bRetVal = true;
      	   }
         }
      }
   }

   return bRetVal;

}


int tcl_ImpTcpPxyGnss::SenStb_s32SendDataToApp(string &ostrAppMsg){

   /* Send Msg Headre .i.e size of message in 2 bytes */
   unsigned int msgHdr = ostrAppMsg.size();
   ETG_TRACE_USR4(("Msg Header size : %d",msgHdr ));

   if (2 != write( s32ConnFD, (const void *)&msgHdr, (size_t)2))
   {
      ETG_TRACE_FATAL (("Socket write failed for Msg Header : %d", msgHdr ));
   }
   else if (msgHdr != write( s32ConnFD, (const void *)ostrAppMsg.c_str(), (size_t)msgHdr))
   {
      ETG_TRACE_FATAL(("Socket write failed for Msg data : %d", msgHdr ));
   }

	return 0;
}