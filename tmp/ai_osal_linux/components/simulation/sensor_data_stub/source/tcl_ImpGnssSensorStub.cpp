///////////////////////////////////////////////////////////
//  tcl_ImpGnssSensorStub.cpp
//  Implementation of the Class tcl_ImpGnssSensorStub
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpGnssSensorStub.h"
#include "tcl_ImpSensorStubFactory.h"
#include <unistd.h>

#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_GNSS_SEN_STB
#include "trcGenProj/Header/tcl_ImpGnssSensorStub.cpp.trc.h"
#endif

#define SEN_STB_CONV_MILLI_SEC_TO_MICRO_SEC (1000)


tcl_ImpGnssSensorStub::tcl_ImpGnssSensorStub(){
   poInfSensorData = NULL;
   poInfAppCommMsgBuilder = NULL;
   poInfSensorDataSource = NULL;
   poInfAppCommPxy = NULL;
}



tcl_ImpGnssSensorStub::~tcl_ImpGnssSensorStub(){

}





bool tcl_ImpGnssSensorStub::SenStb_bDeInit(){

	return false;
}


bool tcl_ImpGnssSensorStub::SenStb_bInit(char *TripFilePath)
{
   bool bRetValue = false;

   tcl_ImpSensorStubFactory* poImpSensorStubFactory;

   poImpSensorStubFactory = tcl_ImpSensorStubFactory::SenStb_poGetFactoryObj();
   if( NULL == poImpSensorStubFactory )
   {
      ETG_TRACE_FATAL(("SenStb_poGetFactoryObj failed function "));
   }
   else
   {
      poInfSensorData = poImpSensorStubFactory->SenStb_poSensorDataStorageObj(SENSOR_TYPE_GNSS);
      poInfAppCommMsgBuilder = poImpSensorStubFactory->SenStb_poAppCommMsgBuilderObj(SENSOR_TYPE_GNSS);
      poInfSensorDataSource = poImpSensorStubFactory->SenStb_poSensorDataSourceObj(SENSOR_TYPE_GNSS);
      poInfAppCommPxy = poImpSensorStubFactory->SenStb_poMakeAppCommPxyObj(SENSOR_TYPE_GNSS);

      if( (NULL == poInfSensorData)||
          (NULL == poInfAppCommMsgBuilder)||
          (NULL == poInfSensorDataSource)||
          (NULL == poInfAppCommPxy) )
      {
         ETG_TRACE_FATAL(("Factory Allocation failed"));
      }
      else if ( false == poInfSensorDataSource->SenStb_bInit(TripFilePath) )
      {
         ETG_TRACE_FATAL((" Gnss data source parser Initialization failed"));
      }
      else if (  false == poInfSensorDataSource->bHasGnss())
      {
         ETG_TRACE_FATAL(("GNSS info Not available"));
      }
      else if (false == poInfAppCommPxy->SenStb_bInit())
      {
         ETG_TRACE_FATAL(("App CommPxy failed"));
      }
      else
      {
         ETG_TRACE_COMP(("GNSS Parser init Sussess"));
         bRetValue = true;
      }      
   }

   return bRetValue;
}

/* Ideal behavior of this function is
      create a thread and return the control back to calling function imeediately.
      the thread created is responsible for the parsing. Since currently we dont have POS parser, 
      single-MAIN thread is used to for GNSS parsing.*/
bool tcl_ImpGnssSensorStub::SenStb_bStart()
{
   string ostrAppMsg;

   /* Create and Send APP Message for status */
   poInfAppCommMsgBuilder->SenStb_bCreateAppMsg(SEN_STB_APP_MSG_TYPE_INC_STATUS);
   poInfAppCommMsgBuilder->SenStb_ostrGetAppMsg(ostrAppMsg);
   poInfAppCommPxy->SenStb_s32SendDataToApp(ostrAppMsg);



   /* Create APP Message for Config */
   poInfAppCommMsgBuilder->SenStb_bCreateAppMsg(SEN_STB_APP_MSG_TYPE_INC_CONFIG);
   poInfAppCommMsgBuilder->SenStb_ostrGetAppMsg(ostrAppMsg);
   poInfAppCommPxy->SenStb_s32SendDataToApp(ostrAppMsg);

   while(1)
   {

      poInfSensorDataSource->SenStb_bGetOneRecord(poInfSensorData);
      poInfSensorData->SenStb_vTraceRecord();

      poInfAppCommMsgBuilder->SenStb_bCreateAppMsg(poInfSensorData);
      poInfAppCommMsgBuilder->SenStb_ostrGetAppMsg(ostrAppMsg);
      SenStb_bWaitforGnssIntervall(poInfSensorData);
      poInfAppCommPxy->SenStb_s32SendDataToApp(ostrAppMsg);
   }

   if ( false == poInfSensorDataSource->SenStb_bDeInit() )
   {
      ETG_TRACE_FATAL((" Gnss Trip file parser DE-Init failed"));
   }
   return false;
}
bool tcl_ImpGnssSensorStub::SenStb_bStop()
{

   return false;
}

void tcl_ImpGnssSensorStub::SenStb_bWaitforGnssIntervall(tcl_InfSensorData *poInfSensorData)
{
   static unsigned int u32PrevTimeStamp = 0;
   unsigned int u32WaitTimems =0;
   map_GnssPvtInfo mapGnssPvtInfo = poInfSensorData->SenStb_mapGetGnssPvtData();

   /* Check for first record. No wait time. Send immediately */
   if (0 != u32PrevTimeStamp)
   {  
      /* Normal case */
      if ( (mapGnssPvtInfo[GnssTimestamp].u32ValUnsignedInt >= u32PrevTimeStamp) )
      {
         u32WaitTimems = mapGnssPvtInfo[GnssTimestamp].u32ValUnsignedInt - u32PrevTimeStamp;
      }
      /* Restart parsing from begining */
      else
      {
         u32WaitTimems = 1000;
      }
   }
   
   u32PrevTimeStamp = mapGnssPvtInfo[GnssTimestamp].u32ValUnsignedInt;
   /* Sleep for wait time */
   usleep((useconds_t)  (u32WaitTimems * SEN_STB_CONV_MILLI_SEC_TO_MICRO_SEC) );
}

