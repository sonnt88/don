///////////////////////////////////////////////////////////
//  tcl_ImpIncGnssMsgBuilder.cpp
//  Implementation of the Class tcl_ImpIncGnssMsgBuilder
//  Created on:      15-Jul-2015 8:53:14 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpIncGnssMsgBuilder.h"
#include "tcl_ImpSensorStubFactory.h"

#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_GNSS_INC_MSG_BLD
#include "trcGenProj/Header/tcl_ImpIncGnssMsgBuilder.cpp.trc.h"
#endif

#define GNSS_INC_STATUS_MSG_ID       char(0x21)
#define GNSS_INC_STATUS_SCC_ACTIVE   char(0x01)
#define GNSS_INC_STATUS_SCC_VERSION  char(0x01)


#define GNSS_INC_CONFIG_MSG_ID           char(0x31);
#define GNSS_INC_CONFIG_MSG_TYPE_CONFIG  char(0x00);
#define GNSS_INC_CONFIG_INT_LOW_BYTE     char(0xE8);// Data intervall : 0x3E8 = 1000 in decimal
#define GNSS_INC_CONFIG_INT_HIGH_BYTE    char(0x03);
#define GNSS_INC_CONFIG_CHIP_TYPE_USED   char(0x01); // TESEO-ST8088

tcl_ImpIncGnssMsgBuilder::tcl_ImpIncGnssMsgBuilder(){

}



tcl_ImpIncGnssMsgBuilder::~tcl_ImpIncGnssMsgBuilder(){

}





bool tcl_ImpIncGnssMsgBuilder::SenStb_bCreateAppMsg(tcl_InfSensorData *poInfSensorData){

   
   tcl_ImpSensorStubFactory* poImpSensorStubFactory;

   //poInfSensorData->SenStb_vTraceRecord();


   poImpSensorStubFactory = tcl_ImpSensorStubFactory::SenStb_poGetFactoryObj();
   if( NULL == poImpSensorStubFactory )
   {
      ETG_TRACE_FATAL(("SenStb_poGetFactoryObj failed"));
   }
   else
   {
      tcl_InfGnssDataBuilder *poInfGnssDataBuilder = poImpSensorStubFactory->SenStb_poGnssDataBuilderObj(SENSOR_TYPE_GNSS);;
      poInfGnssDataBuilder->SenStb_bCreateGnssMsg(poInfSensorData,ostrAppMsg);
      ostrAppMsg = 'A' + ostrAppMsg;

   }
   return false;
}


bool tcl_ImpIncGnssMsgBuilder::SenStb_bCreateAppMsg(En_AppMsgType enAppMsgType)
{
   switch (enAppMsgType)
   {
      /* Create status message */
      case SEN_STB_APP_MSG_TYPE_INC_STATUS:
         /* Erase the existing message */
         ostrAppMsg.clear();
         /* Fill in the status message */
         ostrAppMsg += GNSS_INC_STATUS_MSG_ID;
         ostrAppMsg += GNSS_INC_STATUS_SCC_ACTIVE;
         ostrAppMsg += GNSS_INC_STATUS_SCC_VERSION;
         break;
         /* create config message */
      case SEN_STB_APP_MSG_TYPE_INC_CONFIG:
         /* Erase the existing message */
         ostrAppMsg.clear();
         /* Fill in the status message */
         ostrAppMsg += GNSS_INC_CONFIG_MSG_ID;
         ostrAppMsg += GNSS_INC_CONFIG_MSG_TYPE_CONFIG;
         ostrAppMsg += GNSS_INC_CONFIG_INT_LOW_BYTE;
         ostrAppMsg += GNSS_INC_CONFIG_INT_HIGH_BYTE;
         ostrAppMsg += GNSS_INC_CONFIG_CHIP_TYPE_USED;
         break;

      default:
         ETG_TRACE_FATAL(("switch Hits default"));
   }

}

bool tcl_ImpIncGnssMsgBuilder::SenStb_bDeInit(){

	return 0;
}


bool tcl_ImpIncGnssMsgBuilder::SenStb_bInit(){

	return 0;
}
