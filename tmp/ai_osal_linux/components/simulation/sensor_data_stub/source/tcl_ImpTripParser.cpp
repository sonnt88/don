///////////////////////////////////////////////////////////
//  tcl_ImpTripParser.cpp
//  Implementation of the Class tcl_ImpTripParser
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpTripParser.h"

#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_TRIP_PARSER
#include "trcGenProj/Header/tcl_ImpTripParser.cpp.trc.h"
#endif

tcl_ImpTripParser::tcl_ImpTripParser(){

}



tcl_ImpTripParser::~tcl_ImpTripParser(){

}





bool tcl_ImpTripParser::bHasAbs() const {

	return false;
}


bool tcl_ImpTripParser::bHasAcc() const {

	return false;
}


bool tcl_ImpTripParser::bHasGnss() {

	return false;
}


bool tcl_ImpTripParser::bHasGyro() const {

	return false;
}


bool tcl_ImpTripParser::bHasOdo() const {

	return false;
}


bool tcl_ImpTripParser::SenStb_bDeInit(){

	return 0;
}


bool tcl_ImpTripParser::SenStb_bInit(char *TripFilePath){

   return 0;
}

char* tcl_ImpTripParser::SenStb_pcGetTripFilePath()
{

   return (char*) TRIP_FILE_PATH;

}

void tcl_ImpTripParser::SenStb_vStoreFileRdPos(){

   if (oTripFile.is_open())
   {
      TripResPos = oTripFile.tellg();
   }
   else
   {
      ETG_TRACE_FATAL(("In-Correct File obj "));
   }
}
void tcl_ImpTripParser::SenStb_vRestoreFileRdPos(){

   if (oTripFile.is_open())
   {
      oTripFile.seekg(TripResPos);
   }
   else
   {
      ETG_TRACE_FATAL(("In-Correct File obj"));
   }

}