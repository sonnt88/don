///////////////////////////////////////////////////////////
//  tcl_ImpAppCommMsgBuilder.cpp
//  Implementation of the Class tcl_ImpAppCommMsgBuilder
//  Created on:      15-Jul-2015 8:53:13 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpAppCommMsgBuilder.h"


tcl_ImpAppCommMsgBuilder::tcl_ImpAppCommMsgBuilder(){

}



tcl_ImpAppCommMsgBuilder::~tcl_ImpAppCommMsgBuilder(){

}



bool tcl_ImpAppCommMsgBuilder::SenStb_ostrGetAppMsg( string &osrtAppMsg )
{
   bool bRetVal = true;
   if( 0 != ostrAppMsg.size() )
   {
      osrtAppMsg = this->ostrAppMsg;
   }
   else
   {
      bRetVal = false;
   }
   
   return bRetVal;
}

