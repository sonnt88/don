///////////////////////////////////////////////////////////
//  tcl_ImpSensorDataSource.cpp
//  Implementation of the Class tcl_ImpSensorDataSource
//  Created on:      15-Jul-2015 8:53:15 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpSensorDataSource.h"


tcl_ImpSensorDataSource::tcl_ImpSensorDataSource(){

}



tcl_ImpSensorDataSource::~tcl_ImpSensorDataSource(){

}





bool tcl_ImpSensorDataSource::bHasAbs() const {

	return false;
}


bool tcl_ImpSensorDataSource::bHasAcc() const {

	return false;
}


bool tcl_ImpSensorDataSource::bHasGnss() {

	return false;
}


bool tcl_ImpSensorDataSource::bHasGyro() const {

	return false;
}


bool tcl_ImpSensorDataSource::bHasOdo() const {

	return false;
}


bool  tcl_ImpSensorDataSource::SenStb_bDeInit(){

	return 0;
}


bool tcl_ImpSensorDataSource::SenStb_bInit(char *TripFilePath){

	return 0;
}