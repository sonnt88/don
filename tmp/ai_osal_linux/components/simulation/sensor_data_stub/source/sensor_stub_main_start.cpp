#include <linux/input.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>

//#define VARIANT_S_FTR_ENABLE_TRC_GEN

#include "tcl_InfSensorStubMain.h"
#include "sensor_stub_types.h"
#include "tcl_ImpSensorStubFactory.h"
#include "sensor_stub_main_start.h"
#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_MAIN
#include "trcGenProj/Header/sensor_stub_main_start.cpp.trc.h"
#endif


/* This is supposed to be the main() of the parser once pos leagacy code is replaced with cmplete c++ implementation. 
     char *TripFilePath: trip file patch should not be passed as a prameter down the line as the design of the module has to work even if trip
     file is replaced by some other sensor source later. Rather the path of the trip file should be read from a config file like registry or from rfs.
     In the current implementation, only GNSS data specific implementation is done. POS information is left blank. Legacy POS parser has t be
     replaced after the implementation of this module.
     */
int Gnss_start_main(char *TripFilePath)
{
   tS32 s32RetVal = -1;

//   vInitPlatformEtg(); // initialize the trace backend no cleanup required

   ETG_TRACE_COMP(( "ETG data traced" ));

   tcl_ImpSensorStubFactory* poImpSensorStubFactory;
   /* Factory is implemented as a singleton paterren. Use the static method  SenStb_poGetFactoryObj to get the factory object*/
   poImpSensorStubFactory = tcl_ImpSensorStubFactory::SenStb_poGetFactoryObj();
   if( NULL == poImpSensorStubFactory )
   {
      ETG_TRACE_COMP(("SenStb_poGetFactoryObj failed"));
   }
   else
   {

      tcl_InfSensorStubMain * poInfSensorStubMain;
      /* Get the parser main entry object from the factory method. */
      poInfSensorStubMain = poImpSensorStubFactory->SenStb_poSensorStubMainObj(SENSOR_TYPE_GENERIC);
      if ( NULL == poInfSensorStubMain)
      {
         ETG_TRACE_COMP(("SenStb_poSensorStubMainObj factory failed"));
      }
      else
      {
         ETG_TRACE_COMP(("SenStb_poSensorStubMain Obj created"));
         /* Initialize the main stub object.
            Trip file as a parameter is a temporary workaround. This has to be removed along with legasy POS parser.*/
         if (true != poInfSensorStubMain->SenStb_bInit(TripFilePath))
         {
            ETG_TRACE_COMP(("Stub Init failed %s", __FUNCTION__));
         }
         else if( true != poInfSensorStubMain->SenStb_bStartStub())
         {
            ETG_TRACE_COMP(("Stub start failed %s", __FUNCTION__));
         }
      }
   }

   return s32RetVal;
}
