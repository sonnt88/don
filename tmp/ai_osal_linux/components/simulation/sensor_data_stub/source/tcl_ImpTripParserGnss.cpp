///////////////////////////////////////////////////////////
//  tcl_ImpTripParserGnss.cpp
//  Implementation of the Class tcl_ImpTripParserGnss
//  Created on:      15-Jul-2015 8:53:16 AM
//  Original author: rmm1kor
///////////////////////////////////////////////////////////

#include "tcl_ImpTripParserGnss.h"

#include "sensor_stub_trace.h"


// ETG defines
#define ETG_S_IMPORT_INTERFACE_GENERIC
#include "etg_if.h"

// include for trace backend from ADIT
#define ETG_ENABLED
#include "trace_interface.h"

// defines and include of generated code
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SEN_STUB_TRIP_PARS_GNSS
#include "trcGenProj/Header/tcl_ImpTripParserGnss.cpp.trc.h"
#endif

/* The order of delcaration of strings here should match with the order in TRIP file.
     This is done to keep parsing logic simple. Also this should be in sync with enum 
     En_GnssChannelDataSeq */
string GnssChannSeq[]=
{
   "GnssRecordCounter = ",
   "GnssChannel = ",
   "GnssSvId = ",
   "GnssSatStatus = ",
   "GnssCarrierToNoiseRatio = ",
   "GnssAzimuth = ",
   "GnssElevation = "
};


/* The order of delcaration of strings here should match with the order in TRIP file.
     This is done to keep parsing logic simple. Also this should be in sync with enum 
     SenStub_GnssPvtGnssPvtDataMapKey */
string GnssPvtSeq[]=
{
   "GnssTimestamp = ",
   "GnssRecordCounter = ",
   "GnssNumOfChannels = ",
   "GnssYear = ",
   "GnssMonth = ",
   "GnssDay = ",
   "GnssHour = ",
   "GnssMinute = ",
   "GnssSecond = ",
   "GnssMilliSeconds = ",
   "GnssLatitude = ",
   "GnssLongitude = ",
   "GnssAltitudeWGS84 = ",
   "GnssGeoidalSeparation = ",
   "GnssVelocityNorth = ",
   "GnssVelocityEast = ",
   "GnssVelocityUp = ",
   "GnssPositionCovarianceMatrix(0) = ",
   "GnssPositionCovarianceMatrix(4) = ",
   "GnssPositionCovarianceMatrix(5) = ",
   "GnssPositionCovarianceMatrix(8) = ",
   "GnssPositionCovarianceMatrix(9) = ",
   "GnssPositionCovarianceMatrix(10) = ",
   "GnssVelocityCovarianceMatrix(0) = ",
   "GnssVelocityCovarianceMatrix(4) = ",
   "GnssVelocityCovarianceMatrix(5) = ",
   "GnssVelocityCovarianceMatrix(8) = ",
   "GnssVelocityCovarianceMatrix(9) = ",
   "GnssVelocityCovarianceMatrix(10) = ",
   "GnssQuality = ",
   "GnssMode = ",
   "GnssGDOP = ",
   "GnssPDOP = ",
   "GnssHDOP = ",
   "GnssTDOP = ",
   "GnssVDOP = ",
   "GnssSatSysUsed = ",
   "GnssSatellitesVisible = ",
   "GnssSatellitesReceived = ",
   "GnssSatellitesUsed = ",
   "GnssPositionResidualMax = ",
   "GnssVelocityResidualMax = ",
   "GnssConstellationValid = "
};

tcl_ImpTripParserGnss::tcl_ImpTripParserGnss(){

}



tcl_ImpTripParserGnss::~tcl_ImpTripParserGnss(){

}
/* Searches for the GNSS Header and returns true if header is available. */
bool tcl_ImpTripParserGnss::bHasGnss() {

   bool bStringFound = false;
   string line;
   size_t pos;

   SenStb_vStoreFileRdPos();

   while(oTripFile.good() && (false == bStringFound) )
   {
      getline(oTripFile, line);
      if (string::npos != line.find(GNSS_START_PVT_DATA_KEY)) // search
      {
         bStringFound = true;
         ETG_TRACE_COMP(("GNSS Info available in Trip file"));
      }
   }

   SenStb_vRestoreFileRdPos();
   
   return bStringFound;
}

/* Returns on GNSS record */
bool  tcl_ImpTripParserGnss::SenStb_bGetOneRecord(tcl_InfSensorData *poInfSensorData){

   /* Seek to Gnss channel data */
   /* Extract elements in a loop and add the channel data to the vector*/
   /* repeat till end of channel data */
   SenStb_vParseGnssChannelData();


   /*Seek to the PVT data header*/
   /*Extract and add PVT data to the Map*/
   SenStb_vParseGnssPvtData();

   /*Store parsed data*/
   SenStb_bStoreParsedGnssData(poInfSensorData);
   
   return  NULL;
}

bool tcl_ImpTripParserGnss::SenStb_bDeInit(){

   bool bRetVal = false;

   if (oTripFile.is_open())
   {
      oTripFile.close();
   
      if (!oTripFile.is_open())
      {
         bRetVal = true;
      }
   }
   else
   {
      ETG_TRACE_ERR(("ERR: TRIP File already closed"));
   }

   return bRetVal;
}


/* OPEN the trip file */
bool tcl_ImpTripParserGnss::SenStb_bInit(char *TripFilePath){

   bool bRetVal = false;
   /* Open TRip file */
   /*
      This needs to be used to get path from cfg file
      oTripFile.open(SenStb_pcGetTripFilePath(), ios::in); */
   if (NULL != TripFilePath)
   {
      oTripFile.open(TripFilePath, ios::in);
      if (oTripFile.is_open())
      {
         bRetVal =  true;
         ETG_TRACE_COMP (( " TRIP FILE OPEN Passed" ));
      }
      else
      {
         ETG_TRACE_FATAL ((" TRIP FILE OPEN Failed" ));
      }
   }
   else
   {
      ETG_TRACE_FATAL(("TRIP FILE param is NULL"));
   }

   return bRetVal;
}

/* Extract PVT data from trip file into the PVT map  */
void tcl_ImpTripParserGnss::SenStb_vParseGnssPvtData(){

   bool bEndLoop = false;
   string line;
   size_t pos;
   SenStub_GnssPvtGnssPvtDataMapKey enGnsspvtLoopInx = GnssTimestamp;

/* Seek to the header of Gnss data */
   while(oTripFile.good() && (false == bEndLoop) )
   {
      getline(oTripFile, line);
      if (string::npos != line.find(GNSS_START_PVT_DATA_KEY)) // search
      {
         bEndLoop = true;
      }
   }

/* Extract all the elements in a row and fill the map GNSS sensor data*/
   bEndLoop = false;
   while(oTripFile.good() && (false == bEndLoop) )
   {
      getline(oTripFile, line);

      switch(enGnsspvtLoopInx)
      {
         /* Extraction of decimal unsigned integer values */
         case GnssTimestamp:
         case GnssRecordCounter:
         case GnssNumOfChannels:
         case GnssYear:
         case GnssMonth:
         case GnssDay:
         case GnssHour:
         case GnssMinute:
         case GnssSecond:
         case GnssMilliSeconds:
         case GnssQuality:
         case GnssMode:
         case GnssSatellitesVisible:
         case GnssSatellitesReceived:
         case GnssSatellitesUsed:
         case GnssPositionResidualMax:
         case GnssVelocityResidualMax:
         case GnssConstellationValid:
         {
            if (string::npos == line.find(GnssPvtSeq[enGnsspvtLoopInx])) // search
            {
               bEndLoop = true;
               ETG_TRACE_ERR(("Error: PVT Key mismatch: %d", (int)enGnsspvtLoopInx));
            }
            else
            {          
               line.erase(0,GnssPvtSeq[enGnsspvtLoopInx].length());
               SenStb_vAddPvtData(enGnsspvtLoopInx,  strtoul (line.c_str(), NULL,10));
            }
            break;
         }
         /* Extraction of floating values */
         case GnssLatitude:
         case GnssLongitude:
         case GnssAltitudeWGS84:
         case GnssGeoidalSeparation:
         case GnssVelocityNorth:
         case GnssVelocityEast:
         case GnssVelocityUp:
         case GnssPositionCovarianceMatrix_0:
         case GnssPositionCovarianceMatrix_4:
         case GnssPositionCovarianceMatrix_5:
         case GnssPositionCovarianceMatrix_8:
         case GnssPositionCovarianceMatrix_9:
         case GnssPositionCovarianceMatrix_10:
         case GnssVelocityCovarianceMatrix_0:
         case GnssVelocityCovarianceMatrix_4:
         case GnssVelocityCovarianceMatrix_5:
         case GnssVelocityCovarianceMatrix_8:
         case GnssVelocityCovarianceMatrix_9:
         case GnssVelocityCovarianceMatrix_10:
         case GnssGDOP:
         case GnssPDOP:
         case GnssHDOP:
         case GnssTDOP:
         case GnssVDOP:
         {
            if (string::npos == line.find(GnssPvtSeq[enGnsspvtLoopInx])) // search
            {
               bEndLoop = true;
               ETG_TRACE_ERR(("Error: PVT Key mismatch: %d", (int)enGnsspvtLoopInx));
            }
            else
            {          
               line.erase(0,GnssPvtSeq[enGnsspvtLoopInx].length());
               SenStb_vAddPvtData(enGnsspvtLoopInx,  strtod(line.c_str(), NULL));
            }
            break;
         }
         /* Extraction of hexadecimal unsigned integer values */
         case GnssSatSysUsed:
         {
            if (string::npos == line.find(GnssPvtSeq[enGnsspvtLoopInx])) // search
            {
               bEndLoop = true;
               ETG_TRACE_ERR(("Error: PVT Key mismatch: %d", (int)enGnsspvtLoopInx));
            }
            else
            {          
               line.erase(0,GnssPvtSeq[enGnsspvtLoopInx].length());
               SenStb_vAddPvtData(enGnsspvtLoopInx,  strtoul (line.c_str(), NULL,16));
            }
            break;
         }
         default:
         {
            bEndLoop = true;
            ETG_TRACE_ERR(("Switch hits deault in"));
            break;
         }
      }
      /* GnssConstellationValid is the last entry of PVT data in TRIP file*/
      if (GnssConstellationValid == enGnsspvtLoopInx )
      {
         bEndLoop = true;
      }
      enGnsspvtLoopInx = 
         static_cast<SenStub_GnssPvtGnssPvtDataMapKey>(static_cast<int>(enGnsspvtLoopInx) + 1);
   }
}

/*ADD PVT data of type char to MAP: Not needed now*/
void tcl_ImpTripParserGnss::SenStb_vAddPvtData(SenStub_GnssPvtGnssPvtDataMapKey enMapKey, tS8 s8ValChar){


}
/*ADD PVT data of type double to MAP*/
void tcl_ImpTripParserGnss::SenStb_vAddPvtData(SenStub_GnssPvtGnssPvtDataMapKey enMapKey,tF64 f64ValDouble){

   Un_KeyValue uKeyValue;
   uKeyValue.f64ValDouble = f64ValDouble;
   map_ParGnssPvtInfo[enMapKey]=uKeyValue;
   
   ETG_TRACE_USR4 (( "%10s %f",GnssPvtSeq[enMapKey].c_str(), (map_ParGnssPvtInfo[enMapKey]).f64ValDouble ))

}

/*ADD PVT data of type int to MAP: Not needed now*/
void tcl_ImpTripParserGnss::SenStb_vAddPvtData(SenStub_GnssPvtGnssPvtDataMapKey enMapKey,tS32 s32ValInt){
}

/*ADD PVT data of type unsigned int to MAP*/
void tcl_ImpTripParserGnss::SenStb_vAddPvtData(SenStub_GnssPvtGnssPvtDataMapKey enMapKey,tU32 u32ValUnsignedInt){

   Un_KeyValue uKeyValue;
   uKeyValue.u32ValUnsignedInt = u32ValUnsignedInt;
   map_ParGnssPvtInfo[enMapKey]=uKeyValue;

   ETG_TRACE_USR4 (( "%10s %d",GnssPvtSeq[enMapKey].c_str(), (map_ParGnssPvtInfo[enMapKey]).u32ValUnsignedInt ))

}

/*ADD Channel data of type unsigned int to MAP*/
void tcl_ImpTripParserGnss::SenStb_vAddChannelData(En_GnssChannelDataSeq enMapKey,tU32 u32ValUnsignedInt)
{

   Un_KeyValue uKeyValue;
   uKeyValue.u32ValUnsignedInt = u32ValUnsignedInt;
   map_ParGnssChannInfo[enMapKey]=uKeyValue;

   ETG_TRACE_USR4 (( "%10s %d",GnssChannSeq[enMapKey].c_str(), (map_ParGnssChannInfo[enMapKey]).u32ValUnsignedInt ))

}

/* Extract PVT data from trip file into the vector of Channel data maps  */
void tcl_ImpTripParserGnss::SenStb_vParseGnssChannelData(){

   bool bEndLoop = false;
   string line;
   En_GnssChannelDataSeq enChannDataLoopInx = GnssChnRecordCounter;

   /* clear the previous stored info from vector */
   vec_ParGnssChanInfo.clear();
   vec_GnsssatInfo::iterator it = vec_ParGnssChanInfo.begin();

/* Seek to the header of Gnss channel data */
   if (!SenStb_bSeekToChanDataStart())
   {
      /* This will not parse further. Ideally exception is needed here */
      bEndLoop = true;
      /* Check if the reason is because we reached end of file. */
      if( true == oTripFile.eof() )
      {
         /* Start parsing from begining. Adjust File oblect to begining */
         oTripFile.clear();
         oTripFile.seekg(0);


         /* This shiould work now.*/
         if ( false == SenStb_bSeekToChanDataStart())
         {
            /*Trip file is not a valid one */
            ETG_TRACE_ERR((" NO GNSS Found in Trip file provided :%s",TRIP_FILE_PATH));
         }
         else
         {
            ETG_TRACE_COMP(("Reached END of Trip file. started Parsing  same file from begining again"));
            bEndLoop = false;
         }
      }
   }

   

/* Extract all the elements in a row and fill the vector of GNSS channel data*/
   while( oTripFile.good() && (false == bEndLoop) )
   {
      getline(oTripFile, line);
      switch (enChannDataLoopInx)
      {
         case GnssChnRecordCounter:
         case GnssChannel:
         case GnssSvId:
         case GnssCarrierToNoiseRatio:
         case GnssAzimuth:
         case GnssElevation:
         {
            if (string::npos == line.find(GnssChannSeq[enChannDataLoopInx])) // search
            {
               bEndLoop = true;
               ETG_TRACE_ERR(("Error: Chann Data Key mismatch: %d", (int)enChannDataLoopInx));
            }
            else
            {          
               line.erase(0,GnssChannSeq[enChannDataLoopInx].length());
               SenStb_vAddChannelData(enChannDataLoopInx,  strtoul (line.c_str(), NULL,10));
            }
            break;
         }
         case GnssSatStatus:
         {
            if (string::npos == line.find(GnssChannSeq[enChannDataLoopInx])) // search
            {
               bEndLoop = true;
               ETG_TRACE_ERR(("Error: Chann Data Key mismatch: %d", (int)enChannDataLoopInx));
            }
            else
            {          
               line.erase(0,GnssChannSeq[enChannDataLoopInx].length());
               SenStb_vAddChannelData(enChannDataLoopInx,  strtoul (line.c_str(), NULL,16));
            }
            break;
        }
        default:
        {
           bEndLoop = true;
           ETG_TRACE_ERR(("Switch hits default in "));
           break;
        }
      }

      if ( GnssElevation == enChannDataLoopInx )
      {
         /* Add Map element to the vector */
         it = vec_ParGnssChanInfo.insert(it,map_ParGnssChannInfo);

         /* Update the loop index to search from begining again*/
         enChannDataLoopInx = GnssChnRecordCounter;

         ETG_TRACE_USR4 (( "size = %d", vec_ParGnssChanInfo.size()));

         /* Check for end of channel data and end loop */
         if( 1 == (map_ParGnssChannInfo[GnssChannel]).u32ValUnsignedInt )
         {
            bEndLoop = true;
         }
         /* Seek to the next header of Gnss channel data */
         else if (!SenStb_bSeekToChanDataStart())
         {
            /* This will not parse further. Ideally exception is needed here */
            bEndLoop = true;
         }
      }
      else
      {

         enChannDataLoopInx = 
            static_cast<En_GnssChannelDataSeq>(static_cast<int>(enChannDataLoopInx) + 1);
      }
   }
}

/* Adjust the get position of Trip file object to start of Next GNSS data in trip file*/
bool tcl_ImpTripParserGnss::SenStb_bSeekToChanDataStart()
{

   bool bFound = false;
   string line;

   /* Seek to the header of Gnss data */
   while(oTripFile.good() && (false == bFound) )
   {
      getline(oTripFile, line);
      if (string::npos != line.find(GNSS_START_CHANNEL_DATA_KEY)) // search
      {
         bFound = true;
      }
   }
   return bFound;
}

/* Store the extrated GNSS data into Sensor data object */
bool tcl_ImpTripParserGnss::SenStb_bStoreParsedGnssData(tcl_InfSensorData *poInfSensorData){
   poInfSensorData->SenStb_vmapSetGnssPvtData(map_ParGnssPvtInfo);
   poInfSensorData->SenStb_vSetGnssSatData(vec_ParGnssChanInfo);
   return true;
}


