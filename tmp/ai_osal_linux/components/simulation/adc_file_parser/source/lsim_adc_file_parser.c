/************************************************************************
| FILE:         inc_sim_adc.c
| PROJECT:      platform
| SW-COMPONENT: INC
|------------------------------------------------------------------------------
| DESCRIPTION:  Test program for simulating a remote ADC port extender for INC.
|
|               Setup for test with ADC_INC kernel module and fake device:
|
|               modprobe dev-fake
|               /opt/bosch/base/bin/inc_sim_adc_out.out
|               modprobe adc_inc
|
|------------------------------------------------------------------------------
| COPYRIGHT:    (c) 2013 Robert Bosch GmbH
| HISTORY:
| Date      | Modification               | Author
| 10.02.13  | Initial revision           | ant3kor
| 11.03.13  | Use datagram service	     | ant3kor
| 01.04.13  | Use host name, update desc | ant3kor
| --.--.--  | ----------------           | -------, -----
|
|*****************************************************************************/

#include "lsim_adc_file_parser.h"

#ifdef DG_ENABLE_DEBUG
#define DG_DBG(a, ...) fprintf(stdout,a, ## __VA_ARGS__)
#else
#define DG_DBG(a,...)
#endif

#define DG_ERR(a, ...) fprintf(stderr,a, ## __VA_ARGS__)
#ifndef AF_INC
#define AF_INC 41
#endif

#define UNUSED_VARIABLE(VAR) VAR

static void parse_from_datafile(FILE **p_fp, file_data *filedata);
static void get_numchannels_fromfile(FILE *, unsigned int *);

static void parse_from_datafile(FILE **p_fp, file_data *filedata)
{
	FILE *fp = *p_fp;
	short int value;
	int interval;
   int i = 0; char c;
	fscanf(fp, "%x %d", &value, &interval);
	/* Check if interval is less than 20ms */
	if(interval < 20)
	{
		interval = 20;
		printf("\n The time interval given is very less for the simulator to respond\n");
		printf("\n Making interval = 20 ms\n");
	}
	filedata->interval = interval;
	filedata->value = value;
	if(feof(fp))
		rewind(fp);
	*p_fp = fp;
}

static void get_numchannels_fromfile(FILE *fp, unsigned int *p_numchannels)
{

	if(NULL == fp)
	{
		*p_numchannels = NUM_CHANNELS_DEFAULT;
	}
	else
	{
		fscanf(fp, "%d", p_numchannels);
		/* Check for 1 <= numchannels <= 255 */
		if(*p_numchannels > ADC_INC_MAX_CHANNELS)
		{
			*p_numchannels = ADC_INC_MAX_CHANNELS;
		}
		else if(p_numchannels == 0)
		{
			*p_numchannels = 1;
		}
	}
}

void *tx_thread(void *arg)
{
   int n, fd;
   char sendbuf[1024] = {0};
   int sendflag = 0;
	thread_argument * _arg = (thread_argument *) arg;
	int channel_index = _arg->channel_index;
	FILE *fp = NULL;
	char file_path[1000]="/tmp/ADC_Data_files/channel_";
	short int value = 0;
	file_data ChannelDataFromFile;
	int NeedParsing = 0;
	unsigned char higher_byte = 0, lower_byte= 0;
	unsigned int i;

    /*To make lint happy*/
    fd = 0;

#ifdef USE_DGRAM_SERVICE
	sk_dgram *dgram = _arg->dgram;
#else
	fd = _arg->fd;
#endif

	/* Open the appropriate file */
	sprintf(file_path, "%s%d", file_path, channel_index);
	do
	{
      fp = fopen(file_path, "r");
      if(NULL == fp)
      {
         /* If file not found send default value */
         ChannelDataFromFile.value = 0;
         ChannelDataFromFile.interval = 0;
         sample_buffer[2*channel_index] = 0;
         sample_buffer[2*channel_index + 1] = 0;
      }
      else
      {
         /* Data to be read from file */
         NeedParsing = 1;
      }	
      if(NeedParsing)
      {
         parse_from_datafile(&fp, &ChannelDataFromFile);
         lower_byte = (unsigned char)(ChannelDataFromFile.value & 0x00ff);
         higher_byte = (unsigned char)(ChannelDataFromFile.value >> 8);
         /* Copy values into Send buf */
         if(sample_buffer)
         {
            sample_buffer[2*channel_index] = lower_byte;
            sample_buffer[2*channel_index + 1] = higher_byte;
         }
         /* Detect threshold crossing  */
         /* Check for upper limit and send */
         if(1 == adcsetTh[channel_index].upper_compare) 
         {
            if(higher_byte > adcsetTh[channel_index].upper_thval.limit)
            {
               sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_THRESHOLD_HIT_MSGID;				
               sendbuf[1] = channel_index;
               sendbuf[2] = 0x01;
               sendbuf[3] = 0x00;
#ifdef USE_DGRAM_SERVICE
               n = dgram_send(dgram, sendbuf, 4);
#else 
               n = send(fd, sendbuf, 4, 0);
#endif   
               if (n == -1)
               {
                  fprintf(stderr, "INC_sendto failed: %d\n", n);
                  break;
               }
            }
         }
         /* Check for lower limit and send*/
         if(1 == adcsetTh[channel_index].lower_compare) 
         {
            if(lower_byte < adcsetTh[channel_index].lower_thval.limit)
            {
               sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_THRESHOLD_HIT_MSGID;				
               sendbuf[1] = channel_index;
               sendbuf[3] = 0x01;
               sendbuf[2] = 0x00;
#ifdef USE_DGRAM_SERVICE
               n = dgram_send(dgram, sendbuf, 4);
#else 
               n = send(fd, sendbuf, 4, 0);
#endif   
               if (n == -1)
               {
                  fprintf(stderr, "INC_sendto failed: %d\n", n);
                  break;
               }
            }
         }
         /* wait till the sample value changes */
         usleep(ChannelDataFromFile.interval * 1000);
      }
      if(fp)
         fclose(fp);
   }while(1);
   return 0;
}

int main()
{
   int m, n, fd, ret, adcid, n_fd, clilen;
   unsigned char sendbuf[1024];
   unsigned char recvbuf[1024];
   char HOSTNAME[1000];
   pthread_t tid;
   unsigned int num_channels=0, sendbuf_idx=0;
   FILE *fp = NULL;
   char file_path[1000]="/tmp/ADC_Data_files/numchannels";
   int i;
   thread_argument *_arg = NULL, *_arg1 = NULL;
   /* struct sockaddr_in local, remote; */
   struct hostent *local, *remote;
   struct sockaddr_in local_addr, remote_addr;
#ifdef USE_DGRAM_SERVICE
   sk_dgram *dgram;
#endif
   /* Get the number of adc channels */
   fp = fopen(file_path, "r");
   get_numchannels_fromfile(fp, &num_channels);
   /* Allocate the buffer for samples on ADC channels */
   sample_buffer = malloc(num_channels * 2);
   if(NULL == sample_buffer)
   {
      ret = -1;
      goto fail;
   }
   /* Allocate the buffer to pass the arguments to threads */
   _arg = malloc(num_channels * sizeof(thread_argument));	
   if(NULL == _arg)
   {
      ret = -1;
      goto fail;
   }
   _arg1 = _arg; // Back up to be used for freeing   
   /* Prepare the socket for send and receive */
   local = gethostbyname("fake1-local");
   if (local == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1-local) failed: %d\n", errsv);
      ret = -1;
      goto fail;
   }
   local_addr.sin_family = AF_INET;
   memcpy((char *) &local_addr.sin_addr.s_addr, (char *) local->h_addr, local->h_length);	
   /*local.sin_addr.s_addr = inet_addr("192.168.4.1");*/
   local_addr.sin_port = htons(PORT_EXTENDER_ADC_PORT); /*From inc_ports.h*/
   remote = gethostbyname("fake1");
   if (remote == NULL)
   {
      int errsv = errno;
      fprintf(stderr, "gethostbyname(fake1) failed: %d\n", errsv);
      ret = -1;
      goto fail;
   }
   remote_addr.sin_family = AF_INET;
   memcpy((char *) &remote_addr.sin_addr.s_addr, (char *) remote->h_addr, remote->h_length);
   /*remote.sin_addr.s_addr = inet_addr("192.168.4.2");*/
   remote_addr.sin_port = htons(PORT_EXTENDER_ADC_PORT); /* from inc_ports.h */
   fd = socket(AF_BOSCH_INC_LINUX, SOCK_STREAM, 0);
   if (fd == -1)
   {
      int errsv = errno;
      fprintf(stderr, "create socket failed: %d\n", errsv);
      ret = -1;
      goto fail;
   }
#ifdef USE_DGRAM_SERVICE
   dgram = dgram_init(fd, DGRAM_MAX, NULL);
   if (dgram == NULL)
   {
   fprintf(stderr, "dgram_init failed\n");
   return 1;
   }
#endif
   ret = bind(fd, (struct sockaddr *) &local_addr, sizeof(local_addr));
   if (ret < 0)
   {
      int errsv = errno;
      perror("Bind Failed: ");
      fprintf(stderr, "bind failed: %d\n", errsv);
      ret = -1;
      goto fail;
   }
   ret = listen(fd,1);
   if(ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "listen failed: %d\n", errsv);
      ret = -1;
      goto fail;
   }
   clilen = sizeof(remote_addr);
   /* Accept actual connection from the client */
   n_fd = accept(fd, (struct sockaddr *)&remote_addr, 
      &clilen);
   if (n_fd < 0) 
   {
      int errsv = errno;
      fprintf(stderr, "accept failed: %d\n", errsv);
      ret = -1;
      goto fail;
   }
   
#ifdef USE_DGRAM_SERVICE
   dgram->sk = n_fd;
#endif
   /* Spawn the threads - 1 thread for 1 adc channel */
   for(i=0;i<num_channels;i++)
   {
#ifdef USE_DGRAM_SERVICE
      _arg->dgram = dgram;
      _arg->channel_index = i;
      if (pthread_create(&tid, NULL, tx_thread, (void *) _arg) == -1)
      {
         fprintf(stderr, "pthread_create failed\n");
      }
#else
      _arg->fd = n_fd;
      _arg->channel_index = i;
      if (pthread_create(&tid, NULL, tx_thread, (void *) _arg) == -1)
      {
         fprintf(stderr, "pthread_create failed\n");
      }
#endif
      _arg++;
   }
   /* Start receiving and sending messages */
   do {
      do {
#ifdef USE_DGRAM_SERVICE
         n = dgram_recv(dgram, recvbuf, sizeof(recvbuf));
#else
         n = recv(n_fd, recvbuf, sizeof(recvbuf), 0);
#endif
      } while(n < 0 && errno == EAGAIN);
      if (n < 0)
      {
         int errsv = errno;
         fprintf(stderr, "recv failed: %d\n", errsv);
         ret = -1;
         goto fail;
      }
      if (n == 0)
      {
         fprintf(stderr, "invalid msg size: %d\n", n);
         ret = -1;
         goto fail;
      }
      
      switch (recvbuf[0])
      {
      case SCC_PORT_EXTENDER_ADC_C_COMPONENT_STATUS_MSGID:
         if (recvbuf[1] != ADC_INC_STATUS_ACTIVE) {
            fprintf(stderr, "invalid status: %d\n", recvbuf[1]);
            ret = -1;
            goto fail;
         }
         fprintf(stderr, "-> C_COMPONENT_STATUS: %d\n", recvbuf[1]);
         sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_COMPONENT_STATUS_MSGID;
         sendbuf[1] = ADC_INC_STATUS_ACTIVE;
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 2);
#else
         m = send(n_fd, sendbuf, 2, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            ret = -1;
            goto fail;
         }
         fprintf(stderr, "<- R_COMPONENT_STATUS: %d\n", sendbuf[1]);
         break;
		case SCC_PORT_EXTENDER_ADC_C_GET_SAMPLE_MSGID:
         fprintf(stderr, "-> C_GET_SAMPLE: %d\n", recvbuf[1]);
         sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_GET_SAMPLE_MSGID;
         sendbuf[1] = (char)num_channels;
         /* fill the sendbuf  with samples*/
         sendbuf_idx = 2;
         for(i=0; i<num_channels; i++)
         {
            sendbuf[sendbuf_idx] = sample_buffer[2*i];
            sendbuf_idx++;
            sendbuf[sendbuf_idx] = sample_buffer[2*i + 1];
            sendbuf_idx++;
         }
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, (2*num_channels + 2));
#else
         m = send(n_fd, sendbuf, (2*num_channels + 2), 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            ret = -1;
            goto fail;
         }
         fprintf(stderr, "<- R_GET_SAMPLE: %d\n", sendbuf[1]);
         break;
      case SCC_PORT_EXTENDER_ADC_C_SET_THRESHOLD_MSGID:
         fprintf(stderr, "-> C_SET_THRESHOLD channelid: %d\n", recvbuf[1]);
         adcid = recvbuf[1];
         if(recvbuf[2] == 1)
         {
            /* Set the upper limit threshold */
            fprintf(stderr, " Set the upper limit threshold\n");
            adcsetTh[adcid].upper_compare = (encomparision)(1);
            adcsetTh[adcid].upper_thval.lpart.lowbyte = recvbuf[3]; 
            adcsetTh[adcid].upper_thval.lpart.highbyte = recvbuf[4];
            sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_SET_THRESHOLD_MSGID;
            sendbuf[1] = adcid;
            sendbuf[2] = adcsetTh[adcid].upper_compare ;
            sendbuf[3] = adcsetTh[adcid].upper_thval.lpart.lowbyte;
            sendbuf[4] = adcsetTh[adcid].upper_thval.lpart.highbyte;
         }
         else if(recvbuf[2] == 2)
         {
            /* Set the lower limit threshold */
            fprintf(stderr, " Set the lower limit threshold\n");
            adcsetTh[adcid].lower_compare = (encomparision)(1);
            adcsetTh[adcid].lower_thval.lpart.lowbyte = recvbuf[3]; 
            adcsetTh[adcid].lower_thval.lpart.highbyte = recvbuf[4];
            sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_SET_THRESHOLD_MSGID;
            sendbuf[1] = adcid;
            sendbuf[2] = adcsetTh[adcid].lower_compare ;
            sendbuf[3] = adcsetTh[adcid].lower_thval.lpart.lowbyte;
            sendbuf[4] = adcsetTh[adcid].lower_thval.lpart.highbyte;
         }			
         else 
         {
            fprintf(stderr,"Wrong enum for Threshold comparision\n");
            adcsetTh[adcid].upper_compare = 0;
            adcsetTh[adcid].lower_compare = 0;
            sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_SET_THRESHOLD_MSGID;
            sendbuf[1] = adcid;
            sendbuf[2] = (char)(recvbuf[2]);
            sendbuf[3] = 0;
            sendbuf[4] = 0;
         }
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 5);
#else
         m = send(n_fd, sendbuf, 5, 0);
#endif
         if (m == -1)
         {
            int errsv = errno;
            fprintf(stderr, "send failed: %d\n", errsv);
            ret = -1;
            goto fail;
         }
         fprintf(stderr, "<- R_SET_THRESHOLD: channelid %d\n", sendbuf[1]);
         break;
      case SCC_PORT_EXTENDER_ADC_C_GET_CONFIG_MSGID:
         fprintf(stderr, "-> C_GET_CONFIG: %d\n", recvbuf[1]);
         sendbuf[0] = SCC_PORT_EXTENDER_ADC_R_GET_CONFIG_MSGID;
         sendbuf[1] = ADC_INC_RESOLUTION_10BIT;
#ifdef USE_DGRAM_SERVICE
         m = dgram_send(dgram, sendbuf, 2);
#else
         m = send(n_fd, sendbuf, 2, 0);
#endif
         if(-1 == m)
         {
            fprintf(stderr, "INC_sendto failed: %d\n", m);	  
            ret = -1;
            goto fail;
         }
         fprintf(stderr, "<- R_GET_CONFIG: %d\n", sendbuf[1]);
         break;
      default:
         fprintf(stderr, "invalid msgid: 0x%02X\n", recvbuf[0]);
         ret = -1;
         goto fail;
      }
	} while (n >= 0);
#ifdef USE_DGRAM_SERVICE
   ret = dgram_exit(dgram);
   if (ret < 0)
   {
      int errsv = errno;
      fprintf(stderr, "dgram_exit failed: %d\n", errsv);
      ret = -1;
      goto fail;
   }
#endif

fail:
   if(fd)
      close(fd);
   /* free sample_buffer and thread argument */
   if(sample_buffer)
      free(sample_buffer);
   if(_arg1)
      free(_arg1);
   return ret;
}
/*
init datagram servive on given socket.
options: optional - not yet used.
*/
sk_dgram* dgram_init(int sk, int dgram_max, void *options)
{
   sk_dgram *skd;
   struct sockaddr_in addr;
   socklen_t len = sizeof(struct sockaddr);
   if(getsockname(sk, (struct sockaddr *)&addr, &len)<0)
      return NULL;
   DG_DBG("dgram init on socket af %d\n",addr.sin_family);

   if(dgram_max > (int) DGRAM_MAX) { /* satisfy lint */
      DG_ERR("exceed maxsize (%d>%ld)\n",dgram_max, DGRAM_MAX);
      return NULL;
   }
   switch(addr.sin_family) {
      case AF_INET:
      case AF_INC:
         break;
      default:
         DG_ERR("not supported on AF %d\n",addr.sin_family);
         return NULL;
   }

   skd = (sk_dgram *) malloc(sizeof(sk_dgram));
   if(!skd) {
      DG_ERR("failed allocating memory\n");
      return NULL;
   }
   memset(skd, 0,sizeof(*skd));
   skd->proto = addr.sin_family;
   skd->hlen = sizeof(dgram_header_std);
   skd->len = dgram_max;
   skd->buf = (char *) malloc(skd->len+skd->hlen);

   if(!skd->buf) {
      DG_ERR("failed allocating rcv buffer\n");
      free(skd);
      return NULL;
   }
   skd->sk = sk;
   /*ignore options*/
   UNUSED_VARIABLE(options);

   return skd;
}

int dgram_exit(sk_dgram *skd)
{
   if(!skd) {
      DG_ERR("invalid handle\n");
      errno = EINVAL;
      return -1;
   }
   free(skd->buf);
   skd->buf = NULL;
   free(skd);
   return 0;
}


/*send message as datagram
-just prepend header
*/
int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen)
{
   struct msghdr msg;
   struct iovec iov[2];
   dgram_header_std h;
   int ret;
   if(skd->proto != AF_INET)
      return send(skd->sk, ubuf, ulen, 0);
   if((int) ulen > skd->len) { /* satisfy lint */
      DG_ERR("send: dgram exceeds bufsize (%ld>%d)\n", ulen, skd->len);
      errno = EMSGSIZE;
      return -1;
   }
   memset(&msg, 0, sizeof(msg));
   msg.msg_iovlen = 2;
   msg.msg_iov = iov;
   h.dglen = htons(ulen);
   iov[0].iov_base = &h;
   iov[0].iov_len = sizeof(h);
   iov[1].iov_base = ubuf;
   iov[1].iov_len = ulen;
   ret = sendmsg(skd->sk, &msg, 0);
   if(ret >= (int) sizeof(h)) /* satisfy lint */
      ret -= sizeof(h);
   return ret;
}

/*receive datagram*/
int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen)
{
   int err;
   if(skd->proto != AF_INET)
      return recv(skd->sk, ubuf, ulen, 0);
   if((skd->received < skd->hlen) || ((skd->received >= skd->hlen) && (skd->received < (skd->h.dglen+skd->hlen)))){
      err = recv(skd->sk, skd->buf+skd->received, (skd->len + skd->hlen)-skd->received, 0);
      if(err <= 0) {
         DG_DBG("receive failed with err %d %d \n",err, errno);
         return err;
      }
      skd->received += err;
   }
   if(skd->received < skd->hlen){
      errno = EAGAIN;
      return -1;
   }
   /*complete header available*/
   memcpy((char*)&skd->h, skd->buf, skd->hlen);
   skd->h.dglen = ntohs(skd->h.dglen);
   if(skd->received < (skd->h.dglen+skd->hlen )) {
      errno = EAGAIN;
      return -1;
   }
   /*full dgram available*/
   if(ulen < skd->h.dglen) {
      DG_ERR("recv: dgram exceeds bufsize (%d>%ld)\n", skd->h.dglen, ulen);
      errno = EMSGSIZE;
      return -1;
   }
   memcpy(ubuf, skd->buf+skd->hlen, skd->h.dglen);
   skd->received-= (skd->h.dglen+skd->hlen);
   err = skd->h.dglen;
   if(skd->received > 0 ) {
      memmove(skd->buf, skd->buf+skd->hlen+skd->h.dglen, skd->received);
      if(skd->received >= skd->hlen){
         memcpy((char*)&skd->h, skd->buf, skd->hlen);
         skd->h.dglen = ntohs(skd->h.dglen);
      }
   }
   DG_DBG("finished DGRAM of len %d\n", err);
   return err;
}