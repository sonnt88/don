/******************************************************************************
* FILE            : lsim_adc_file_parser.h
*
*
* DESCRIPTION     : This is the header file for lsim_adc_file_parser.c
*
* AUTHOR(s)       : Suryachand Yellamraju (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 17.OCT.2012|  Initialversion 1.0  | Suryachand Yellamraju (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#ifndef LSIM_ADC_FILE_PARSER_HEADER
#define LSIM_ADC_FILE_PARSER_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>

/*Reject reasons not added*/

#define AF_BOSCH_INC_ADR      41 /* AF_INC */
#define AF_BOSCH_INC_AUTOSAR  41 /* AF_INC */
#define AF_BOSCH_INC_LINUX    AF_INET
#define PORT_OFFSET        0xC700
#define SPM_PORT        (0x01 | PORT_OFFSET)
#define PORT_EXTENDER_GPIO_PORT     (0x02 | PORT_OFFSET)
#define PORT_EXTENDER_ADC_PORT      (0x03 | PORT_OFFSET)
#define PD_NET_PORT        (0x04 | PORT_OFFSET)
#define DIA_UDD_PORT       (0x05 | PORT_OFFSET)
#define SENSORS_PORT       (0x06 | PORT_OFFSET)
#define DLT_PORT        (0x07 | PORT_OFFSET)
#define GNSS_PORT       (0x08 | PORT_OFFSET)
#define WDG_PORT        (0x09 | PORT_OFFSET)
#define PORT_EXTENDER_PWM_PORT      (0x0A | PORT_OFFSET)
#define DOWNLOAD_PORT         (0x0B | PORT_OFFSET)
#define THERMAL_MANAGEMENT_PORT     (0x0C | PORT_OFFSET)
#define SUPPLY_MANGEMENT_PORT    (0x0D | PORT_OFFSET)
#define NET_CTRL_PORT         (0x0E | PORT_OFFSET)
#define NET_BROADCAST_PORT    (0x0F | PORT_OFFSET)
#define NET_TP0_PORT       (0x10 | PORT_OFFSET)
#define NET_TP1_PORT       (0x11 | PORT_OFFSET)
#define NET_TP2_PORT       (0x12 | PORT_OFFSET)
#define NET_TP3_PORT       (0x13 | PORT_OFFSET)
#define INPUT_DEVICE_PORT     (0x14 | PORT_OFFSET)
#define DIA_ERRMEM_PORT       (0x15 | PORT_OFFSET)
#define DIA_APP_PORT       (0x16 | PORT_OFFSET)
#define AUDIO_ROUTER_PORT     (0x17 | PORT_OFFSET)
#define CONNECTION_MASTER_PROXY_PORT   (0x18 | PORT_OFFSET)
#define CONNECTION_MASTER_PLUGIN_PORT  (0x19 | PORT_OFFSET)
#define STREAM_MANAGER_PORT      (0x1A | PORT_OFFSET)
#define ILLUMINATION_PORT     (0x1B | PORT_OFFSET)
#define VEHICLE_DATA_SERVER_PORT (0x1C | PORT_OFFSET)

#define SCC_PORT_EXTENDER_ADC_C_COMPONENT_STATUS_MSGID	 0x20
#define SCC_PORT_EXTENDER_ADC_R_COMPONENT_STATUS_MSGID   0x21
#define SCC_PORT_EXTENDER_ADC_R_REJECT_MSGID		 0x0B
#define SCC_PORT_EXTENDER_ADC_C_GET_SAMPLE_MSGID 	 0x40
#define SCC_PORT_EXTENDER_ADC_R_GET_SAMPLE_MSGID         0x41
#define SCC_PORT_EXTENDER_ADC_C_SET_THRESHOLD_MSGID	 0x42
#define SCC_PORT_EXTENDER_ADC_R_SET_THRESHOLD_MSGID	 0x43
#define SCC_PORT_EXTENDER_ADC_R_THRESHOLD_HIT_MSGID	 0x45
#define SCC_PORT_EXTENDER_ADC_C_GET_CONFIG_MSGID	 0x46
#define SCC_PORT_EXTENDER_ADC_R_GET_CONFIG_MSGID	 0x47

#define ADC_INC_STATUS_ACTIVE         0x01
#define ADC_INC_RESOLUTION_10BIT      0x0a
#define ADC_INC_MAX_CHANNELS          255
#define NUM_CHANNELS_DEFAULT          10

/*As of now below defines are not used*/
/*#define ADC_INC_STATUS_INACTIVE       0x02 */
/* #define ADC_INC_RESOLUTION_12BIT      0x0c */

#define false 0
#define true 1
#define DGRAM_MAX (0xffff-sizeof(dgram_header_std))

/*standard header for datagram service*/
typedef struct {
   unsigned short dglen;   /*2byte-length of dgram*/
}dgram_header_std;

/*maximum size of datagram*/

typedef struct {
   int sk;     /*socket filedescriptor*/
   char* buf;  /*receive buffer*/
   int len;    /*len of receive buffer*/
   int proto;  /*used protocol*/
   int hlen;   /*size of used dgram header*/
   int received;  /*received bytes*/
   dgram_header_std h; /*copy of received header*/
}sk_dgram;

sk_dgram* dgram_init(int sk, int dgram_max, void *options);
int dgram_exit(sk_dgram *skd);
int dgram_send(sk_dgram *skd, void *ubuf, size_t ulen);
int dgram_recv(sk_dgram *skd, void* ubuf, size_t ulen);

struct bytewidelimit
{
    uint8_t lowbyte;
    uint8_t highbyte;
};
typedef union stlimit
{
    uint16_t limit;
    struct bytewidelimit lpart;
} threshold;

typedef enum
{
    en_adc_upperthreshold = 1,
    en_adc_lowerthreshold
}encomparision;

struct adcsetThreshold
{
    encomparision lower_compare;
    encomparision upper_compare;
    threshold lower_thval;
	threshold upper_thval;
};
struct adcsetThreshold adcsetTh[ADC_INC_MAX_CHANNELS];

typedef struct thread_argument
{
#ifdef USE_DGRAM_SERVICE
	sk_dgram *dgram;
#else
	int fd;
#endif
	int channel_index;
}thread_argument;

typedef struct file_data
{
	int interval;
	unsigned short int value;
}file_data;

unsigned char *sample_buffer= NULL;

#endif

/*End of file*/
