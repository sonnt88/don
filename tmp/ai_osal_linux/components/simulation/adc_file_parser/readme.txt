Usage

The following sequence has to be followed while using the simulation software:

1. Configure the number of ADC channels used by adding a file with the name �numchannels� @ /tmp/ADC_data_files/ in the LSIM machine and then adding an entry with the number of channels. 
   If the file is not found then the default number of channels is taken as 10.

2. Create the data files for the required channels. This can be done by adding files with names �channel_<channel_number>� in /tmp/ADC_data_files in the LSIM machine. 
   For example, if the channel 1 is to be used for ADC, the name of the file should be channel_1. 
   a. The numbering of channels starts from 0.
   b. This file has two fields:
   c. Sample value: This is the value of the sample in decimal format.
   d. Time interval: This is the time interval for which the sample value remains unchanged. The units of time are milli seconds. This value should be greater than 20ms.
   e. If the file is not found then the default value will be taken as zero for both the sample value and the time interval.

3. a. Start rbcm-inc-scc-config.service file using the below command
      <systemctl start rbcm-inc-scc-config.service>
                     (or)
   b. Run the script start_inc.sh in LSIM machine using the below command.
      </opt/bosch/start_inc.sh>

4.	Run the process lsim_adc_file_parser_out.out in the LSIM machine using the below command.
   </opt/bosch/base/bin/adc_file_parser_out.out>
   This adc_file_parser_out.out is obtained through build.pl --lsim adc_file_parser --info
   
5. Run the desired application.