/******************************************************************************
* FILE         : network_data_dispatcher.c
*             
*             
* DESCRIPTION  : This file implements Network data dispatcher module.
*                This module is used by LSIM drivers to communicate with linux simulation applications via TCP/IP interface.
*             
* AUTHOR(s)    : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 17.OCT.2012|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

/************************************************************************
* Header file declaration
*-----------------------------------------------------------------------*/

#include<errno.h>

/*All Below listed header files are needed for socket programming */

#include <arpa/inet.h> /* for inet_pton() */
#include <netinet/in.h> /* for htons() */
#include<sys/socket.h>
#include<sys/types.h>


#include "OsalConf.h"
#define OSAL_S_IMPORT_INTERFACE_TYPES
#define OSAL_S_IMPORT_INTERFACE_THREADING
#include "osal_if.h"
#include "osansi.h"
#include "ostrace.h"


#include "network_data_dispatcher.h"

/************************************************************************
* Macro declaration (scope: Global)
*-----------------------------------------------------------------------*/
#define DATA_DISPATCHER_PORT (13) // todo:  This should be stored in registry.
#define DISP_MAX_CONN_REQ_IN_QUE (10)
#define DISPATCHER_MAX_CONNECTIONS (10)
#define MAX_RECONNECT_TRIALS (5) /* Maximum number of retries for network errors*/
#define DISP_MSG_PRIO_NORMAL (6) /* This is used is DISP_RESPONSE messages.*/

#define LSIM_NET_DISP_CONN_MANAGER_THREAD_NAME "NET_DISP_CONN_MANAGER_THREAD"
#define DISP_CONN_THREAD_RAW_NAME "NET_DISP_CONN_THREAD_XXX"
#define DISP_CONN_CNT_SEMAPHORE "DISP_CONN_CNT_SEMAPHORE"
#define DISP_DRIVER_RESPONSE_SEMAPHORE "DISP_DRIVER_RESPONSE_SEMAPHORE"
#define DISP_CONN_THREAD_RENAME_CHARRACTER_COUNT 3

#define CONN_CNT_SEMAPHORE_TIMEOUT (1000)
#define CONN_DRV_RES_SEMAPHORE_TIMEOUT (1000)
#define DISP_THREAD_RENAME_INDEX (20) /* This is the index to replace "X" in "NET_DISP_CONN_THREAD_XXX" */
#define LSIM_NET_DISP_CONN_MANAGER_THREAD_STACKSIZE (2048)
#define LSIM_NET_DISP_CONN_THREAD_STACKSIZE (2048)
#define LSIM_MAX_DATA_FIELD_SIZE_IN_PACKET (2048)
#define LSIM_BYTES_IN_DATA_FIELD_INTERNAL_USE (2*sizeof(tU32))

/*These macros are used to respond to simulation applications*/
#define LSIM_NET_DISP_SUCCESS (0) /* on success*/
#define LSIM_NET_DISP_ERROR_UNKNOWN (-1) /* In case of unknown reason for error*/
#define LSIM_NET_DISP_ERROR_MAX_CONNECTIONS_REACHED (-4); /* This will be the Error response for connect from 
                                                             Simulation applications*/

/* These macros are used to release the resources acquired.*/
#define LSIM_DISP_MSG_QUEUE (0x01)
#define LSIM_DISP_CONN_CNT_SEMAPHORE (0x02)
#define LSIM_DISP_DRV_RES_SEMAPHORE (0x03)
#define LSIM_DISP_NET_CONNECTIONS (0x04)

/************************************************************************
* Driver specific structure definition
*-----------------------------------------------------------------------*/

/* This is the packet header. Every packet that is sent to any simulation application
   or received by network dispatcher should have this header.



Packet Format
____________________________________________________________________
|driver   | Message   |Message    | Data Field|                     |
| ID      | PRIO      |Type       |   size    |    Data Field       |
|32bytes  | 32bytes   |32bytes    |   32bytes |    2048 bytes(MAX)  |
|_________|___________|___________|___________|_____________________|

*/


typedef struct
{
   tU32 u32DrvID;     /* Driver ID: To which driver the packet is refered to  */
   tU32 u32MsgPrio;   /* Message priority: This priority is used while posting into message queue*/
   tU32 u32MsgType;   /* one of the message types explained in tEnDispMsgType data type */
   tU32 u32DataSize;  /* Data field size: Size of the data field following the header*/
}trDispPacketHeader;

/* Message types used to communication between Driver <-> Dispatcher <-> Simulation application */

typedef enum
{
   INVALID_MESSAGE = 0,

   MSG_QUE_STATUS = 1,  /*  This message is addressed to dispatcher and not driver.
                            Dispatcher returns the driver message queue status.*/
   DRIVER_DATA = 2,     /*  This Message is addressed to driver. So dispatcher 
                            will forward this to proper driver */
   DISP_RESPONSE = 3,   /*  This is a response from dispatcher for some request by 
                            simulation application */
   DRIVER_RESPONSE= 4   /*   If driver wants to pass some data to simulation application
                            this type of message should be used. */
}tEnDispMsgType;

/*This is used to respond to MSG_QUE_STATUS type of message.*/

typedef struct 
{
   tU32 u32MaxMessages;       /* Maximum number of messages that queue can hold  */
   tU32 u32MaxMsgLength;      /* Maximum size of each message in the queue*/
   tU32 u32AvailableMsgCnt;   /* Number of messages presently existing in the queue. 
                                (u32MaxMessages - u32AvailableMsgCnt) gives number of 
                                free slots in the queue*/
} trMsgQueStatus;


typedef struct
{
   OSAL_tSemHandle hConnCntSem;
   OSAL_tSemHandle hDrvResSem;
   tS32 s32SocketFD;
   tU32 u32ConnCnt; /* This counter will be used to limit the number of connections and
                         to close all connections during shutdown*/
   tS32 s32SocConnectionID[DISPATCHER_MAX_CONNECTIONS];
   tBool bShutdownFlag;
}trNetDispInfo;



/* When a new driver is added and if that driver needs Network dispatcher support,
   an entry should be added in tenLsimDriverID for that driver */
typedef enum
{
   LSIM_INVALID_DRIVER_ID = 0x0FFFF,
   LSIM_ODOMETER_DRIVER_ID = 0,
   LSIM_GYRO_DRIVER_ID = 1,
   LSIM_USBPWR_DRIVER_ID = 2,
   MAX_DRIVER_ID,
}tenLsimDriverID;

/* This structure holds the message queue details which network dispatcher 
   uses to communicate with osal driver */
typedef struct
{
   tCString csDrvMsgQueName; /* Message queue name */
   tenLsimDriverID enDrvID;  /* Driver ID */
   OSAL_tMQueueHandle hMsgQ; /* Handle to message queue */
}trDrvMsgQueList;


trDrvMsgQueList rDrvMsgQueList[]=
{
   /* Message queue name created byDriver ---Unique ID given to driver---OSAL_NULL */
   {"ODOMETER_MSG_QUEUE_LSIM",           LSIM_ODOMETER_DRIVER_ID,     OSAL_C_INVALID_HANDLE}, /* #1 */
   {"LsimGyroMessageQueue",              LSIM_GYRO_DRIVER_ID,         OSAL_C_INVALID_HANDLE}, /* #2 */
   {"USBPWR_MSGQUE_LSIM",                LSIM_USBPWR_DRIVER_ID,       OSAL_C_INVALID_HANDLE}, /* #3 */
   

   /*This should always be the last entry*/
   {(tCString)OSAL_NULL,                 LSIM_INVALID_DRIVER_ID,      OSAL_C_INVALID_HANDLE},

};

/************************************************************************
* Variables declaration (scope: Global)
*-----------------------------------------------------------------------*/

trNetDispInfo rNetDispInfo = {0};


/************************************************************************
* Function declaration (scope: Local to file)
*-----------------------------------------------------------------------*/
static tS32 NetDisp_s32SocketSetup(tVoid);
static tVoid NetDisp_pvConnectionManagerThread(tPVoid pvDummyarg);
static tVoid NetDisp_pvDispConnThread(tPVoid pvArg);
static tVoid NetDisp_vReleaseResource(tU32 u32ResourceID);
static tS32 NetDisp_s32HandleNewConn(tS32 s32ConnFD);
static tS32 NetDisp_s32WriteToSocket(tS32 s32ConFD, trDispPacketHeader *prPktHeader, tPVoid pvDataBuff);
static tS32 NetDisp_s32ReadFromSocket(tS32 s32ConFD, trDispPacketHeader *prPktHeader, tPVoid pvDataBuff);
static tVoid NetDisp_vDispatchMsgQueStatus(trDispPacketHeader *prDispPktHeader, tS32 s32ConnFD);
static tVoid NetDisp_vHandleDriverData(trDispPacketHeader *prDispPktHeader, tS32 s32ConnFD, tU32 *pu32PktData);
static tVoid vLSIM_NET_DISP_TraceOut( tU32 u32Level, const tChar *pcFormatString,... );

/********************************************************************************
* FUNCTION        : s32NetDataDiapatcher_Init 

* PARAMETER       :   NONE
*                                                      
* RETURNVALUE     : OSAL_OK  on sucess
*                   OSAL_ERROR on Failure
*
* DESCRIPTION     : This is initialization function for Network Dispatcher. 
*                   This is called from devinit.c at startup.
*                   1:It opens all message queues to communicate with driver.
*                   2: Configure socket connections.
*                   3: Spawn a thread to handle further requests from simulation applications.
*
* HISTORY         : 10.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 s32NetDataDiapatcher_Init(tVoid)
{
   tS32 s32RetVal = OSAL_ERROR;
   OSAL_trThreadAttribute rThreadAttr;
   tU32 u32LoopCnt=0;

   rNetDispInfo.bShutdownFlag = FALSE;

   /*Try to open all message queues which are used to communicate to driver. If open fails, 
     trace out a error message and try next one.
     These message queues should be created by driver prior to calling this function.
     If they are not created, open will fail */
   while((rDrvMsgQueList[u32LoopCnt].csDrvMsgQueName != OSAL_NULL) &&
         (rDrvMsgQueList[u32LoopCnt].enDrvID != LSIM_INVALID_DRIVER_ID))
   {
      if(OSAL_OK != OSAL_s32MessageQueueOpen(rDrvMsgQueList[u32LoopCnt].csDrvMsgQueName,
                                            (OSAL_tenAccess)OSAL_EN_READWRITE, 
                                            &(rDrvMsgQueList[u32LoopCnt].hMsgQ)))
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,"%s Message Queue open FAILED driver ID %d ",
         rDrvMsgQueList[u32LoopCnt].csDrvMsgQueName, (tS32)rDrvMsgQueList[u32LoopCnt].enDrvID);
      }
      u32LoopCnt++;
   }

   /*This semaphore is used to protect the global variables : 
     rDrvMsgQueList , rNetDispInfo.s32SocketFD and rNetDispInfo.u32ConnCnt*/
   if((OSAL_s32SemaphoreCreate((tCString)DISP_CONN_CNT_SEMAPHORE, &rNetDispInfo.hConnCntSem, 1))!= OSAL_OK)
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "%s creation failed", DISP_CONN_CNT_SEMAPHORE);
      NetDisp_vReleaseResource(LSIM_DISP_MSG_QUEUE);
   }
   else if((OSAL_s32SemaphoreCreate((tCString)DISP_DRIVER_RESPONSE_SEMAPHORE,
                                    &rNetDispInfo.hDrvResSem, 1))!= OSAL_OK)
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "%s creation failed", DISP_DRIVER_RESPONSE_SEMAPHORE);
      NetDisp_vReleaseResource(LSIM_DISP_MSG_QUEUE);
      NetDisp_vReleaseResource(LSIM_DISP_CONN_CNT_SEMAPHORE);
   }
   else
   {
      /*This will Create a socket an configure it*/
      s32RetVal = NetDisp_s32SocketSetup();
      if(s32RetVal == OSAL_ERROR)
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "Socket Setup Failed");
         NetDisp_vReleaseResource(LSIM_DISP_MSG_QUEUE);
         NetDisp_vReleaseResource(LSIM_DISP_CONN_CNT_SEMAPHORE);
         NetDisp_vReleaseResource(LSIM_DISP_DRV_RES_SEMAPHORE);
      }
      else
      {
         /*Update thread attributes*/
         rThreadAttr.szName         = LSIM_NET_DISP_CONN_MANAGER_THREAD_NAME;
         rThreadAttr.u32Priority    = OSAL_C_U32_THREAD_PRIORITY_NORMAL;      
         rThreadAttr.s32StackSize   = LSIM_NET_DISP_CONN_MANAGER_THREAD_STACKSIZE;   
         rThreadAttr.pfEntry        = NetDisp_pvConnectionManagerThread;
         rThreadAttr.pvArg          = OSAL_NULL;

         /* Spawn connection Manager Thread */
         s32RetVal = OSAL_ThreadSpawn(&rThreadAttr);
         if(OSAL_ERROR == s32RetVal)
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                    "!!!ThreadSpawn Failed:NetDisp_pvConnectionManagerThread Err Code %d",
                                    s32RetVal);
            NetDisp_vReleaseResource(LSIM_DISP_MSG_QUEUE);
            NetDisp_vReleaseResource(LSIM_DISP_CONN_CNT_SEMAPHORE);
            NetDisp_vReleaseResource(LSIM_DISP_DRV_RES_SEMAPHORE);
         }
         else
         {
            s32RetVal = OSAL_OK;
         }
      }
   }
   return s32RetVal;
}

/********************************************************************************
* FUNCTION         : NetDisp_s32SocketSetup 

* PARAMETER        :  NONE
*                                                      
* RETURNVALUE      : OSAL_OK  on sucess
*                    OSAL_ERROR on Failure
*
* DESCRIPTION      : This does all the setup required to create a socket.
*                    1: It creates a socket.
*                    2: Binds to a address.
*                    3: Mark the socket created as a Passive socket.
*
* HISTORY          : 10.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

static tS32 NetDisp_s32SocketSetup(tVoid)
{
   struct sockaddr_in rSocAttr;
   tS32 s32RetVal = OSAL_ERROR;
   tS32 s32OptState = 1;

   OSAL_pvMemorySet(&rSocAttr,0, sizeof(rSocAttr));
   rSocAttr.sin_family      = AF_INET;
   rSocAttr.sin_port        = htons(DATA_DISPATCHER_PORT); // Todo: this has to be changed
   rSocAttr.sin_addr.s_addr = htonl(INADDR_ANY);

   /* create a socket
      SOCK_STREAM: sequenced, reliable, two-way, connection-based
      byte streams with an OOB data transmission mechanism.*/
   rNetDispInfo.s32SocketFD = socket(AF_INET, SOCK_STREAM, 0);
   if(rNetDispInfo.s32SocketFD == -1)
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "socket() failed");
   }
   else
   {
      /* This option informs os to reuse the socket address even if it is busy*/
      if(setsockopt(rNetDispInfo.s32SocketFD, SOL_SOCKET, SO_REUSEADDR,
                    &s32OptState, sizeof(s32OptState)) == -1)
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "!!!setsockopt() Failed");
      }
      /* Bind the socket created to a address */
      s32RetVal = bind(rNetDispInfo.s32SocketFD, (struct sockaddr *)&rSocAttr, sizeof(rSocAttr));
      if(s32RetVal == -1)
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "Bind Failed");
      }
      else
      {
         /*Listen system call makes the socket as a passive socket. 
                  i.e socket will be used to accept incoming connections*/
         s32RetVal = listen(rNetDispInfo.s32SocketFD, DISP_MAX_CONN_REQ_IN_QUE);
         if(s32RetVal == -1)
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "Listen Failed");
         }
         else
         {
            s32RetVal = OSAL_OK;
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_USER_4,
                                    "Socket creation and Setup completed successfully");
         }
      }
   }
   return s32RetVal;
}


/********************************************************************************
* FUNCTION         : NetDisp_pvConnectionManagerThread 

* PARAMETER        : NONE
*                                                      
* RETURNVALUE      : None
*
* DESCRIPTION      : This handles all the Connection requests on Dispatcher port.
*                  1: It will be blocking on accept call for connect request.
*                  2: Check for MAX connection limits.
*                  3: Spawn a Thread thread to handle the client.
*                  4: Respond to client with the success packet in case of no error. 
*                     Else respond with a Error message.
*
* HISTORY         : 10.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid NetDisp_pvConnectionManagerThread(tPVoid pvDummyarg)
{
   tS32 s32ErrChk = OSAL_ERROR;
   tS32 s32ConnFD = 0; 
   tU32 u32ConnErrCnt = 0;
   tU32 u32LoopCnt;
   tS32 s32ErrResponse;
   /*These are used to send Error response to Simulation Applicationin case of connectoion failure*/
   trDispPacketHeader rPktHdr = {(tU32)LSIM_INVALID_DRIVER_ID, 
                                 /* Response is not related to any driver */
                                 (tU32)DISP_MSG_PRIO_NORMAL,
                                 /* Most Dispatcher responses use this priotity*/
                                 (tU32)DISP_RESPONSE,
                                 /*This is a response from dispatcher*/
                                 (tU32)sizeof(tS32)};
                                 /*Size of error is always sizeof(tS32)*/

   /*We will exit this loop only when accept fails repeatedly */
   while((u32ConnErrCnt < MAX_RECONNECT_TRIALS) && (!rNetDispInfo.bShutdownFlag))
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_USER_4,"Blocking on accept in connection manager");
      /* Wait for connect request from simulation applications */
      s32ConnFD = accept(rNetDispInfo.s32SocketFD, NULL, NULL);
      if(s32ConnFD == -1)
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "accept Failed !!!");
         /* This should never happen. Only reason for this is something went
         wrong while configuring the socket*/
         u32ConnErrCnt++;
      }
      else
      {
         /* Clear Error counter*/
         u32ConnErrCnt = 0;
         if(OSAL_s32SemaphoreWait(rNetDispInfo.hConnCntSem,
                                 (OSAL_tMSecond)CONN_CNT_SEMAPHORE_TIMEOUT) == OSAL_OK)
         {
            if(rNetDispInfo.u32ConnCnt < DISPATCHER_MAX_CONNECTIONS)
            {
               /* Increment the number of active connections count.*/
               rNetDispInfo.u32ConnCnt++;
               /*Update the handles in the list of active connection handles.*/
               for(u32LoopCnt =0; u32LoopCnt < DISPATCHER_MAX_CONNECTIONS; u32LoopCnt++)
               {
                  if(rNetDispInfo.s32SocConnectionID[u32LoopCnt] == NULL)
                  {
                     rNetDispInfo.s32SocConnectionID[u32LoopCnt] = s32ConnFD;
                     /* List is updated with  s32ConnFD so exit from loop*/
                     u32LoopCnt = DISPATCHER_MAX_CONNECTIONS;
                  }
               }
               OSAL_s32SemaphorePost(rNetDispInfo.hConnCntSem);
               /* This will spawn a new thread to handle the new connection with simulation client.
                  Also it responds to the client about connection establishment event */  
               s32ErrChk = NetDisp_s32HandleNewConn(s32ConnFD);
               if(s32ErrChk != OSAL_OK)
               {
                  /* If NetDisp_s32HandleNewConn fails, decrement the number of active connections count.
                  Remove the handle from the list of active connection handles. */
                  if(OSAL_OK == OSAL_s32SemaphoreWait(rNetDispInfo.hConnCntSem,
                                                      (OSAL_tMSecond)CONN_CNT_SEMAPHORE_TIMEOUT))
                  {
                     rNetDispInfo.u32ConnCnt--;
                     for(u32LoopCnt =0; u32LoopCnt < DISPATCHER_MAX_CONNECTIONS; u32LoopCnt++)
                     {
                        if(rNetDispInfo.s32SocConnectionID[u32LoopCnt] == s32ConnFD)
                        {
                           rNetDispInfo.s32SocConnectionID[u32LoopCnt] = NULL;
                           /* List is updated with  s32ConnFD so exit from loop*/
                           u32LoopCnt = DISPATCHER_MAX_CONNECTIONS;
                        }
                     }
                     OSAL_s32SemaphorePost(rNetDispInfo.hConnCntSem);
                  }
                  else
                  {
                     vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                             "OSAL_s32SemaphoreWait failed: errorcode : %lu Line: %d",
                                             OSAL_u32ErrorCode(),__LINE__);                  
                  }
               }
            }
            else
            {
               OSAL_s32SemaphorePost(rNetDispInfo.hConnCntSem);
               /* Send connection Rejection Packet to simulation application
                  as MAX connections limit is reached*/
               s32ErrResponse = LSIM_NET_DISP_ERROR_MAX_CONNECTIONS_REACHED;
               vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                       "Request Rejected. MAX connections limit is reached");
               if(NetDisp_s32WriteToSocket(s32ConnFD, &rPktHdr, &s32ErrResponse)!= OSAL_OK)
               {
                  vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                          "NetDisp_s32WriteToSocket failed in Line: %d",__LINE__);
               }
            }
         }
         else
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                    "Sem wait failed: errorcode : %lu Line: %d",
                                    OSAL_u32ErrorCode(),__LINE__);
         }
      }
   }
   
   /*If system is not shutting down, close socket.
      Else if system is shutting down, this job will be done by 
      s32NetDataDiapatcher_Deinit function.*/
   if(rNetDispInfo.bShutdownFlag == FALSE)
   {
      if (OSAL_s32SemaphoreWait(rNetDispInfo.hConnCntSem, 
                               (OSAL_tMSecond)CONN_CNT_SEMAPHORE_TIMEOUT) == OSAL_OK)
      {
         if(rNetDispInfo.s32SocketFD != OSAL_NULL)
         {
            close(rNetDispInfo.s32SocketFD);
         }
         OSAL_s32SemaphorePost(rNetDispInfo.hConnCntSem);
      }
      else
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "Sem wait failed: errorcode : %lu Line: %d",
                                                OSAL_u32ErrorCode(), __LINE__);
      }
   }
}

/********************************************************************************
* FUNCTION      : NetDisp_s32HandleNewConn 

* PARAMETER     :  s32ConnFD: Handle to network connection.
*                                                      
* RETURNVALUE   : OSAL_OK or OSAL_ERROR.
*
* DESCRIPTION   : 1: This is called for every connection request.
*                 2: It  spawns a thread to handle the client.
*                 3: Respond to client with the success message in case of no error.
                     Else respond with a Error message.
*
* HISTORY       : 17.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 NetDisp_s32HandleNewConn(tS32 s32ConnFD)
{
   tS32 s32RetVal = OSAL_ERROR;
   OSAL_trThreadAttribute rThreadAttr;
   /* u32ThreadConnCnt is used to count the number of threads spawned.
     This count is not decremented if a thread terminates.*/
   static tU32 u32ThreadConnCnt=0;
   tU32 u32TmpThreadCnt =u32ThreadConnCnt;
   tS32 s32DispResponse = LSIM_NET_DISP_ERROR_UNKNOWN;
   tS32 s32LoopCnt;

   /* A unique name for thread will be calculated using DISP_CONN_THREAD_RAW_NAME.*/
   tChar cDispConnThreadName[] = DISP_CONN_THREAD_RAW_NAME;
   /*These are used to send Dispatcher Response to Simulation Application*/
   trDispPacketHeader rPktHdr = {(tU32)LSIM_INVALID_DRIVER_ID,
                                 /* Response is not related to any driver */
                                 (tU32)DISP_MSG_PRIO_NORMAL,
                                 /* Most Dispatcher responses use this priotity*/
                                 (tU32)DISP_RESPONSE,
                                 /*This is a response from dispatcher*/
                                 (tU32)sizeof(tS32)};
                                 /*Size of connection response is always sizeof(tS32)*/

   /* Calculate the thread name based on connection number.*/
   for(s32LoopCnt = DISP_CONN_THREAD_RENAME_CHARRACTER_COUNT; s32LoopCnt > 0; s32LoopCnt--)
   {
      cDispConnThreadName[DISP_THREAD_RENAME_INDEX+s32LoopCnt] = ((u32TmpThreadCnt%10)+0x30);
      u32TmpThreadCnt = u32TmpThreadCnt/10;
   }
   vLSIM_NET_DISP_TraceOut(TR_LEVEL_USER_4, "New thread name calucated: %s", cDispConnThreadName);

   /*Fill thread attributes to the struct*/
   rThreadAttr.pfEntry = NetDisp_pvDispConnThread;
   rThreadAttr.szName = (tString)cDispConnThreadName;
   rThreadAttr.pvArg = s32ConnFD; /* Connection handle */
   rThreadAttr.s32StackSize = LSIM_NET_DISP_CONN_THREAD_STACKSIZE;
   rThreadAttr.u32Priority = OSAL_C_U32_THREAD_PRIORITY_NORMAL;

   /* Spawn new instance of  NetDisp_pvDispConnThread for every connection */
   s32RetVal = OSAL_ThreadSpawn(&rThreadAttr);
   if(OSAL_ERROR == s32RetVal)
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                              "!!!ThreadSpawn Failed:NetDisp_pvDispConnThread. Err code %d",
                              OSAL_u32ErrorCode());
      s32DispResponse = LSIM_NET_DISP_ERROR_UNKNOWN;
   }
   else
   {
      s32DispResponse= LSIM_NET_DISP_SUCCESS;
      u32ThreadConnCnt++;
	  s32RetVal = OSAL_OK;
   }

   /* Respond the status of the thread spwan to simulation application */
   if(NetDisp_s32WriteToSocket(s32ConnFD, &rPktHdr, &s32RetVal) != OSAL_OK)
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "NetDisp_s32WriteToSocket failed in Line: %d", __LINE__);
   }
   else if(s32RetVal == OSAL_OK)
   {
      /* Return value is OSAL_OK only is thread spwan and  NetDisp_s32WriteToSocket are sucessfull*/
      s32RetVal = OSAL_OK;
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_COMPONENT,
                              "New connection Established.Connection Number : %d",
                              (tS32)rNetDispInfo.u32ConnCnt);
   }
   else
   {
      s32RetVal = OSAL_ERROR;
   }
   return s32RetVal;
}

/********************************************************************************
* FUNCTION         : NetDisp_pvDispConnThread 

* PARAMETER        :  pvArg : Handle to connection with simulation application.
*                                                      
* RETURNVALUE      : None
*
* DESCRIPTION      : A new instance of this thread will 
*                    be spawned for every new connection request.
*                    This receives requestes from simulation application and process them.
*                    1: This will be blocking on recv call.
*                    2: It responds to MSG_QUE_STATUS and DRIVER_DATA
*                       requests from simulation application.
*
* HISTORY         : 16.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid NetDisp_pvDispConnThread(tPVoid pvArg)
{

   tPU32 pu32PktData = OSAL_NULL;
   tS32 s32ConnFD = (tS32)pvArg;
   trDispPacketHeader rDispPktHeader;
   tU32 u32LoopCnt;
   tBool bResetConn = FALSE;

   /* This is used to read data feild of the packet*/
   pu32PktData = (tPU32)OSAL_pvMemoryAllocate(LSIM_MAX_DATA_FIELD_SIZE_IN_PACKET);
   if(pu32PktData != OSAL_NULL)
   {
      /*Exit from loop if read fails.*/
      while( (bResetConn != TRUE) && (!rNetDispInfo.bShutdownFlag) )
      {
         if(OSAL_ERROR == ( NetDisp_s32ReadFromSocket (s32ConnFD, &rDispPktHeader,
                                                      (tPVoid)(pu32PktData+2))))
         {
            /* Dont print this trace during shtudown.
               Read is expected to fail during shutdown as connection is closed */
            if(rNetDispInfo.bShutdownFlag == FALSE)
            {
               vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                       "NetDisp_s32ReadFromSocket failed in Line: %d", __LINE__);
            }
            bResetConn = TRUE;
         }
         else
         {
            switch(rDispPktHeader.u32MsgType)
            {
            case MSG_QUE_STATUS:
               {
                  NetDisp_vDispatchMsgQueStatus(&rDispPktHeader, s32ConnFD);
                  break;
               }
            case DRIVER_DATA:
               {
                  /* Update first 8 bytes of message received from client with
                     connection handle and message size. Then post it to message queue */
                  pu32PktData[0]=s32ConnFD;
                  pu32PktData[1]=rDispPktHeader.u32DataSize;
                  NetDisp_vHandleDriverData(&rDispPktHeader, s32ConnFD, pu32PktData);
                  break;
               }
            default:
               {
                  vLSIM_NET_DISP_TraceOut(TR_LEVEL_ERRORS,
                                          "!!!Invalid message type: %u with Driver ID : %u",
                                          rDispPktHeader.u32MsgType,rDispPktHeader.u32DrvID);
                  break;
               }
            }
            
         }
      }
      /*If system is not shutting down, close connection and update active connection list.
        Else if system is shutting down, this job will be done by
        s32NetDataDiapatcher_Deinit function.*/
      if(rNetDispInfo.bShutdownFlag == FALSE)
      {
         if (OSAL_s32SemaphoreWait(rNetDispInfo.hConnCntSem,
                                  (OSAL_tMSecond)CONN_CNT_SEMAPHORE_TIMEOUT) == OSAL_OK)
         {
            rNetDispInfo.u32ConnCnt--;
            for(u32LoopCnt =0; u32LoopCnt<DISPATCHER_MAX_CONNECTIONS; u32LoopCnt++)
            {
               if(rNetDispInfo.s32SocConnectionID[u32LoopCnt] == s32ConnFD)
               {
                  rNetDispInfo.s32SocConnectionID[u32LoopCnt] = NULL;
                  u32LoopCnt = DISPATCHER_MAX_CONNECTIONS;
               }
            }
            OSAL_s32SemaphorePost(rNetDispInfo.hConnCntSem);
         }
         else
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "!!!Sem wait failed errcode: %lu line: %d",
                                                     OSAL_u32ErrorCode(), __LINE__);
         }
      }
      OSAL_vMemoryFree((tPVoid) pu32PktData);
   }
   else
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "OSAL_pvMemoryAllocate failed");
   }
}

/********************************************************************************
* FUNCTION       : s32NetDataDiapatcher_Deinit 

* PARAMETER      :   NONE
*                                                      
* RETURNVALUE    : OSAL_OK  on sucess
*                  OSAL_ERROR on Failure
*
* DESCRIPTION    : This is De-initialization function for Network Dispatcher.
*                  This will be called from devinit.c .
*                  1:Close all active socket connections.
*                  2: Close all opened driver message queues.
*                  3: Close/Delete semaphores.
*
* HISTORY         : 10.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/

tS32 s32NetDataDiapatcher_Deinit(tVoid)
{

   vLSIM_NET_DISP_TraceOut(TR_LEVEL_USER_4, "s32NetDataDiapatcher_Deinit called");
   rNetDispInfo.bShutdownFlag = TRUE;

   NetDisp_vReleaseResource(LSIM_DISP_NET_CONNECTIONS);
   NetDisp_vReleaseResource(LSIM_DISP_MSG_QUEUE);
   NetDisp_vReleaseResource(LSIM_DISP_CONN_CNT_SEMAPHORE);
   NetDisp_vReleaseResource(LSIM_DISP_DRV_RES_SEMAPHORE);

   return OSAL_OK;
}
/********************************************************************************
* FUNCTION      : NetDisp_vReleaseResource 

* PARAMETER     : u32ResourceID : Which resource has to be released.
*
* RETURNVALUE   : NONE.
*
* DESCRIPTION   : This Funcrion releases the resources specified.
*
* HISTORY       : 17.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tVoid NetDisp_vReleaseResource(tU32 u32ResourceID)
{
   tU32 u32LoopCnt = 0;
   
   switch(u32ResourceID)
   {

      /* Closes all opened message queues. */
      case LSIM_DISP_MSG_QUEUE:
      {
         while(rDrvMsgQueList[u32LoopCnt].csDrvMsgQueName != OSAL_NULL)
         {
            if(rDrvMsgQueList[u32LoopCnt].hMsgQ != OSAL_NULL &&
               rDrvMsgQueList[u32LoopCnt].hMsgQ != OSAL_C_INVALID_HANDLE)
            {
               if((OSAL_s32MessageQueueClose(rDrvMsgQueList[u32LoopCnt].hMsgQ)) != OSAL_OK)
               {
                  vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                          "MessageQueueClose failed errcode: %lu For driver ID:%d",
                                          OSAL_u32ErrorCode(),rDrvMsgQueList[u32LoopCnt].enDrvID);
               }
            }
            u32LoopCnt++;
         }
         break;
      }

      /* Closes and delete connection count semaphore. */
      case LSIM_DISP_CONN_CNT_SEMAPHORE:
      {
         if((OSAL_s32SemaphoreClose(rNetDispInfo.hConnCntSem)) != OSAL_OK)
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,"OSAL_s32SemaphoreClose failed errcode: %lu",
                                                    OSAL_u32ErrorCode());
         }
         else
         {
            if((OSAL_s32SemaphoreDelete((tCString)DISP_CONN_CNT_SEMAPHORE)) != OSAL_OK)
            {
               vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,"SemaphoreDelete failed errcode: %lu",
                                                       OSAL_u32ErrorCode());
            }
         }
         break;
      }
      /* Closes and delete Driver response semaphore. */
      case LSIM_DISP_DRV_RES_SEMAPHORE:
      {
         if((OSAL_s32SemaphoreClose(rNetDispInfo.hDrvResSem)) != OSAL_OK)
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,"SemaphoreClose failed errcode: %lu",
                                                    OSAL_u32ErrorCode());
         }
         else
         {
            if((OSAL_s32SemaphoreDelete((tCString)DISP_DRIVER_RESPONSE_SEMAPHORE)) != OSAL_OK)
            {
               vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "SemaphoreDelete failed errcode: %lu",
                                                       OSAL_u32ErrorCode());
            }
         }
         break;
      }

      /* Closes all active network connections. */
      case LSIM_DISP_NET_CONNECTIONS:
      {
         for(u32LoopCnt =0;u32LoopCnt<DISPATCHER_MAX_CONNECTIONS;u32LoopCnt++)
         {
            if(rNetDispInfo.s32SocConnectionID[u32LoopCnt] != OSAL_NULL)
            {
               close(rNetDispInfo.s32SocConnectionID[u32LoopCnt]);
               rNetDispInfo.s32SocConnectionID[u32LoopCnt] = OSAL_NULL;
            }
         }
         if(rNetDispInfo.s32SocketFD != OSAL_NULL)
         {
            close(rNetDispInfo.s32SocketFD);
            rNetDispInfo.s32SocketFD = OSAL_NULL;
         }
         break;
      }
      default:
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                 " %s this line should never be executed", __LINE__);
         break;
      }
   }

}

/********************************************************************************
* FUNCTION     : NetDisp_s32WriteToSocket 

* PARAMETER    :  s32ConFD    : Connection Handle to which data has to be written
*                 prPktHeader : POinter to trDispPacketHeader structure filled with
*                               valid header information
*                 pvDataBuff  : Pointer to data buffer whose size is mentioned in 
*                               u32DataSize field of prPktHeader.
*                                                      
* RETURNVALUE  : OSAL_OK on sucess , OSAL_ERROR in case of error.
*
* DESCRIPTION  : This is a wrapper over write system call of linux.
*                This operates in two stages:
*                1: Writes header of the packet into the network stack
*                2: Then it writes data field of packet into network stack
*
* HISTORY      : 10.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tS32 NetDisp_s32WriteToSocket(tS32 s32ConFD, trDispPacketHeader * prPktHeader, tPVoid pvDataBuff)
{

   tS32 s32RetVal=OSAL_ERROR;
   if((s32ConFD <= 0) || (!prPktHeader) || (!pvDataBuff))
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                              "!!!some pointers passed to NetDisp_s32WriteToSocket are NULL POINTERS");
      s32RetVal = OSAL_ERROR;
   }
   /* This semaphore lock is needed to avoid interleaving of data written from dispatcher
      and driver into same connection*/
   else if(OSAL_s32SemaphoreWait(rNetDispInfo.hDrvResSem,
                                (OSAL_tMSecond)CONN_DRV_RES_SEMAPHORE_TIMEOUT) == OSAL_OK)
   {
      /*write system call will return number of bytes of data written into network stack.
         If this is not same as what you wanted to send, synchronisation issues will arise.
         This case is not handled presently. This has to be fixed.*/

      /* Write header into network stack */
      s32RetVal = write(s32ConFD, prPktHeader, sizeof(trDispPacketHeader));
      if(s32RetVal != sizeof(trDispPacketHeader))
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                 "!!! write FAIL for HDR.This may block client permanently.");
      }
      else
      {
         /* Write data field of packet into network stack */
         s32RetVal = write(s32ConFD, pvDataBuff, prPktHeader->u32DataSize);
         if(s32RetVal != prPktHeader->u32DataSize)
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                        "!!! write FAIL for Data.This may block client permanently.");
         }
         else
         {
            s32RetVal= OSAL_OK;
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_USER_4,
                                    "Data of size %lu written to network stack",
                                    prPktHeader->u32DataSize);
         }
      }

      OSAL_s32SemaphorePost(rNetDispInfo.hDrvResSem);
   }
   else
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "!!!Sem Wait call failed errcode: %lu line: %d",
                                             OSAL_u32ErrorCode(),__LINE__);
   }
   return s32RetVal;
}

/********************************************************************************
* FUNCTION        : NetDisp_s32ReadFromSocket 

* PARAMETER       : s32ConFD    : Connection Handle to which data has to be written
*                   prPktHeader : POinter to trDispPacketHeader structure which will be
*                                 filled with header information
*                   pvDataBuff  : Pointer to data buffer of size 
*                                 LSIM_MAX_DATA_FIELD_SIZE_IN_PACKET.
*      
* RETURNVALUE     : OSAL_OK on sucess , OSAL_ERROR in case of error.
*
* DESCRIPTION     : This is a wrapper over read system call of linux.
*                   This function is not thread safe.
*                   This operates in two stages:
*                   1:Reads header of the packet from network stack to prPktHeader
*                   2:Then based on data field of header, if reads data from network stack to 
*                     memory pointed by pvDataBuff.
*
* HISTORY         : 12.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tS32 NetDisp_s32ReadFromSocket(tS32 s32ConFD,trDispPacketHeader * prPktHeader,tPVoid pvDataBuff)
{
   tS32 s32RetVal=OSAL_ERROR;
   if((s32ConFD==OSAL_NULL) || (prPktHeader==OSAL_NULL) || (pvDataBuff==OSAL_NULL))
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                              "some pointers passed to NetDisp_s32ReadFromSocket are NULL POINTERS");
   }
   else
   {
      /*read system call will return number of bytes of data read from network stack.
         If this is not same as what you wanted to read, synchronization issues will araise.
         This case is not handled presently. This has to be fixed.*/

      /* read header from network stack */
      s32RetVal = recv(s32ConFD, prPktHeader, sizeof(trDispPacketHeader), 0);
      if(s32RetVal != sizeof(trDispPacketHeader))
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                        "!!! recv FAIL for HDR.This may block client permanently.");
         s32RetVal = OSAL_ERROR;
      }
      else if((LSIM_MAX_DATA_FIELD_SIZE_IN_PACKET - LSIM_BYTES_IN_DATA_FIELD_INTERNAL_USE) < prPktHeader->u32DataSize)
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                 "Data size of packet from client is %lu. Max allowed is %lu",
                                  prPktHeader->u32DataSize,
                                  (LSIM_MAX_DATA_FIELD_SIZE_IN_PACKET - LSIM_BYTES_IN_DATA_FIELD_INTERNAL_USE));
         s32RetVal = OSAL_ERROR;
      }
      else if(0 != prPktHeader->u32DataSize)
      {
         /* read data field of packet from network stack */
         s32RetVal = recv(s32ConFD, pvDataBuff, prPktHeader->u32DataSize, 0);
         if(s32RetVal != prPktHeader->u32DataSize)
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,
                                    "!!! recv FAIL for Data.This may block client permanently.");
            s32RetVal = OSAL_ERROR;
         }
         else
         {
            s32RetVal= OSAL_OK;
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_USER_4,"Data of size %lu read from network stack",
                                                     prPktHeader->u32DataSize);
         }
      }
   }
   return s32RetVal;
}
/********************************************************************************
* FUNCTION     : NetDisp_vDispatchMsgQueStatus 

* PARAMETER    :  prDispPktHeader  : Pointer to header received from simulation application
*                 s32ConnFD        : Handle to connection.
*      
* RETURNVALUE  : None.
*
* DESCRIPTION  : This function gets the status of the message queue and send it to simulation application.
*                This function is not thread safe.
*                1: This Iterates through message queue list rDrvMsgQueList for required driver ID.
*                2: Then it will get the message queue status for the driver identified.
*                3: Subtract the size of connection handle (s32ConnFD) and u32DataSize of 
*                   trDispPacketHeader from u32MaxMsgLength.
*                4: Respond to simulation application with rMsgQueStatus in case of no error.
*                5: Else respond with Error message.
*
* HISTORY      : 16.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid NetDisp_vDispatchMsgQueStatus(trDispPacketHeader *prDispPktHeader, tS32 s32ConnFD)
{
   tS32 s32ErrChk = OSAL_ERROR;
   trMsgQueStatus rMsgQueStatus; /*To respond to simulation application*/

   /* Iterate through the mesage queue list to find an entry with diver ID passed in  prDispPktHeader*/
   if(rDrvMsgQueList[prDispPktHeader->u32DrvID].csDrvMsgQueName != OSAL_NULL)
   {
         /*rDrvMsgQueList[u32LoopCnt].hMsgQ will be null if message queue open has failed at startup.*/
         if(rDrvMsgQueList[prDispPktHeader->u32DrvID].hMsgQ != OSAL_C_INVALID_HANDLE)
         {
            s32ErrChk = OSAL_s32MessageQueueStatus (rDrvMsgQueList[prDispPktHeader->u32DrvID].hMsgQ,
                                                    &rMsgQueStatus.u32MaxMessages,
                                                    &rMsgQueStatus.u32MaxMsgLength,
                                                    &rMsgQueStatus.u32AvailableMsgCnt);
         }
         else
         {
            vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL, "!!!Message queue not opened for driver ID %lu,%s",
            prDispPktHeader->u32DrvID,
            rDrvMsgQueList[prDispPktHeader->u32DrvID].csDrvMsgQueName);
         }
   }
   /*No Entry is found with the driver ID in prDispPktHeader*/
   else
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL," !!! Unknown Driver ID %lu",prDispPktHeader->u32DrvID);
   }
   
   prDispPktHeader->u32MsgType = DISP_RESPONSE;
   /*s32ErrChk will be OSAL_OK only if OSAL_s32MessageQueueStatus has succeeded*/
   if(s32ErrChk == OSAL_OK)
   {
      /* Message posted by dispatcher to message queue of any driver should have below fields :
         1: handle to socket connection. 2:  Size of message received from simulation application.
         First 8 bytes of every message posted to message queue is reserved for this. Thus simulation
         application can send a message with maximum size of (u32MaxMsgLength - 8) bytes*/
      rMsgQueStatus.u32MaxMsgLength = rMsgQueStatus.u32MaxMsgLength - (LSIM_BYTES_IN_DATA_FIELD_INTERNAL_USE);
      prDispPktHeader->u32DataSize = sizeof(rMsgQueStatus);
      if(( NetDisp_s32WriteToSocket(s32ConnFD, prDispPktHeader, (tPVoid)&rMsgQueStatus)) == OSAL_ERROR)
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,"!!! Response to MSG_QUE_STATUS Failed");
      }
   }
   /* If something goes wrong, respond with a error status to message queue */
   else
   {
      prDispPktHeader->u32DataSize = sizeof(tS32);
      s32ErrChk = LSIM_NET_DISP_ERROR_UNKNOWN;
      if(( NetDisp_s32WriteToSocket(s32ConnFD, prDispPktHeader, (tPVoid)&s32ErrChk)) == OSAL_ERROR)
      {
         vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,"!!! Error Response to MSG_QUE_STATUS failed.");
      }
   }
}
/********************************************************************************
* FUNCTION     : NetDisp_vHandleDriverData 

* PARAMETER    :  prDispPktHeader  : Pointer to header received from simulation application
*                 s32ConnFD        : Handle to connection.
*                 pu32PktData      : pointer to data received from simulation application.
*      
* RETURNVALUE : None.
*
* DESCRIPTION : This function posts the message from simulation application to driver message queue.
*               This function is not thread safe.
*               1: This Iterates through message queue list rDrvMsgQueList for required driver ID.
*               2: Then it will post the message to message queue of the driver identified.
*               3: Respond to simulation application with sucess in case of no error.
*               4: Else respond with Error message.
*
* HISTORY    : 16.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
static tVoid NetDisp_vHandleDriverData(trDispPacketHeader *prDispPktHeader,
                                       tS32 s32ConnFD, tU32 *pu32PktData)
{
   tS32 s32ErrChk = OSAL_ERROR;
   tS32 s32DispResponse;

   /* Iterate through the mesage queue list to find 
      an entry with diver ID passed in  prDispPktHeader*/
   if(rDrvMsgQueList[prDispPktHeader->u32DrvID].csDrvMsgQueName != OSAL_NULL)
   {
		/*rDrvMsgQueList[u32LoopCnt].hMsgQ will be null 
		       if message queue open has failed at startup.*/
		if(rDrvMsgQueList[prDispPktHeader->u32DrvID].hMsgQ != OSAL_C_INVALID_HANDLE)
		{

			s32ErrChk=OSAL_s32MessageQueuePost(
						rDrvMsgQueList[prDispPktHeader->u32DrvID].hMsgQ,
						(tPCU8)pu32PktData,
						(prDispPktHeader->u32DataSize + LSIM_BYTES_IN_DATA_FIELD_INTERNAL_USE),
						prDispPktHeader->u32MsgPrio);
		}
		else
		{
			vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL,"!!!Msg queue open failed for driver ID %lu,%s ",
			prDispPktHeader->u32DrvID, rDrvMsgQueList[prDispPktHeader->u32DrvID].csDrvMsgQueName);
		}
   }
   /*No Entry is found with the driver ID in  prDispPktHeader*/
   else
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL," !!!Unknown Driver ID %lu",
                                                prDispPktHeader->u32DrvID);
   }
   
   prDispPktHeader->u32MsgType = DISP_RESPONSE;
   prDispPktHeader->u32DataSize = sizeof(tS32);
   if(s32ErrChk == OSAL_OK)
   {
      s32DispResponse = LSIM_NET_DISP_SUCCESS;
   }
   else
   {
      s32DispResponse = LSIM_NET_DISP_ERROR_UNKNOWN;
   }
   if(( NetDisp_s32WriteToSocket(s32ConnFD,prDispPktHeader, (tPVoid)&s32DispResponse)) == OSAL_ERROR)
   {
      vLSIM_NET_DISP_TraceOut(TR_LEVEL_FATAL," !!! Error Response to Handle Driver Data failed.");
   }
}

/********************************************************************************
* FUNCTION     : s32RespondToSimApp 

* PARAMETER    :  s32ConnFD        : Handle to connection.
*                 s32DriverID      : Unique ID assigned to driver
*                 pvBuff           : pointer to data to be sent to simulation application.
*                 u32DataSize      : Size of pvBuff
*                 u32MsgPrio       : Priority with which the message has to be sent.
*      
* RETURNVALUE : OSAL_OK on SUCESS
*               OSAL_ERROR in case of error.
*
* DESCRIPTION : This function is used by driver to send data to simulation application.
*
* HISTORY    : 16.oct.2012| Initial Version             |Madhu Kiran Ramachandra (RBEI/ECF5)
**********************************************************************************/
tS32 s32RespondToSimApp(tS32 s32ConFD,
                        tS32 s32DriverID,
                        tPVoid pvBuff,
                        tU32 u32DataSize,
                        tU32 u32MsgPrio)
{
   tS32 s32RetVal = OSAL_ERROR;
   trDispPacketHeader rPktHdr;

   /* Form Dispatcher packet*/
   rPktHdr.u32DataSize = u32DataSize;
   rPktHdr.u32DrvID = s32DriverID;
   rPktHdr.u32MsgPrio = u32MsgPrio;
   rPktHdr.u32MsgType = DRIVER_RESPONSE;
   
   /*Write to socket*/
   s32RetVal = NetDisp_s32WriteToSocket(s32ConFD, &rPktHdr, pvBuff);

   return s32RetVal;

}


/********************************************************************************
* FUNCTION     : vLSIM_NET_DISP_TraceOut

* PARAMETER    : u32Level - trace level, pcFormatString - Trace string

* RETURNVALUE  : tU32 error codes

* DESCRIPTION  : Trace out function for LSIM gpio.

* HISTORY      : 26.SEP.2012| Initial Version    |Madhu Kiran Ramachandra (RBEI/ECF5)      
**********************************************************************************/
static tVoid vLSIM_NET_DISP_TraceOut(  tU32 u32Level,const tChar *pcFormatString,... )
{
   if(LLD_bIsTraceActive(OSAL_C_TR_CLASS_LSIM_NET_DATA_DISP, u32Level) != FALSE)
   {
      /*
      Parameter to hold the argument for a function, specified the format
      string in pcFormatString
      defined as:
      typedef char* va_list in stdarg.h
      */
      va_list argList = {0};
      /*
      vsnprintf Returns Number of bytes Written to buffer or a negative
      value in case of failure
      */
      tS32 s32Size ;
      /*
      Buffer to hold the string to trace out
      */
      tS8 u8Buffer[MAX_TRACE_SIZE];
      /*
      Position in buffer from where the format string is to be
      concatenated
      */
      tS8* ps8Buffer = (tS8*)&u8Buffer[0];

      /*
      Flush the String
      */
      (tVoid)OSAL_pvMemorySet( u8Buffer,( tChar )'\0',MAX_TRACE_SIZE );   // To satisfy lint

      /*
      Copy the String to indicate the trace is from the RTC device
      */

      /*
      Initialize the argList pointer to the beginning of the variable
      arguement list
      */
      va_start( argList, pcFormatString ); /*lint !e718 */

      /*
      Collect the format String's content into the remaining part of
      the Buffer
      */
      if( 0 > ( s32Size = vsnprintf( (tString) ps8Buffer,
                  sizeof(u8Buffer),
                  pcFormatString,
                  argList ) ) )
      {
         return;
      }

      /*
      Trace out the Message to TTFis
      */
      LLD_vTrace( OSAL_C_TR_CLASS_LSIM_NET_DATA_DISP,
      u32Level,
      u8Buffer,
      (tU32)s32Size );   /* Send string to Trace*/
      /*
      Performs the appropiate actions to facilitate a normal return by a
      function that has used the va_list object
      */
      va_end(argList);
   }
}

/*End of file*/

