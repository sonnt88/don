/******************************************************************************
* FILE            : network_data_dispatcher.h
*
*
* DESCRIPTION     : This is the header file for network_data_dispatcher.c
*
* AUTHOR(s)       : Madhu Kiran Ramachandra (RBEI/ECF5)
*
* HISTORY      :
*------------------------------------------------------------------------
* Date       |       Version        | Author & comments
*------------|----------------------|------------------------------------
* 17.OCT.2012|  Initialversion 1.0  | Madhu Kiran Ramachandra (RBEI/ECF5)
* -----------------------------------------------------------------------
***************************************************************************/

#ifndef NETWORK_DATA_DISPATCHER_HEADER

#define NETWORK_DATA_DISPATCHER_HEADER



tS32 s32NetDataDiapatcher_Init(tVoid);
tS32 s32NetDataDiapatcher_Deinit(tVoid);
tS32 s32RespondToSimApp(tS32 s32ConFD, tS32 s32DriverID, tPVoid pvBuff, tU32 u32DataSize,tU32 u32MsgPrio);

#endif

/*End of file*/
