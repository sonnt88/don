#ifndef __ERRORLOG_IF_H__
#define __ERRORLOG_IF_H__

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

class IELog
{
public:
   typedef enum {
      E_FATAL     = 1,  //Fatal error messages
      E_ERROR     = 2,  //General error messages
      E_WARNING   = 3,  //Warning messages
      E_MESSAGE   = 4,   //General messages
      E_DETAILED  = 5  //All error messages
   } ten_ErrorLevel;

public:
   virtual tVoid vLog( tPCChar error_file,
                       tInt error_line,
                       ten_ErrorLevel level,
                       tCString coszMessage,
                       ... ) = 0;
};

#endif //__ERRORLOG_IF_H__
