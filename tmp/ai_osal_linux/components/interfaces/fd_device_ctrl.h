/*******************************************************************************
 *
 * FILE:
 *     fd_device_ctrl.h
 *
 * REVISION:
 *	   1.1
 *
 * AUTHOR:
 *     (c) 2010, Robert Bosch Gmbh., Ravindran P (RBEI CM-AI/PJ-CF31)
*                                                   
 *
 * CREATED:
 *     23/12/2010 - Ravindran P 
 *
 * DESCRIPTION:
 *     This file contains the exported API functions & data elements 
 *     for the module FD_device_ctrl
 *
 * NOTES:
 *
 * MODIFIED:
 *   DATE        |  AUTHOR        |         MODIFICATION
 *  23/12/2010   |  Ravindran P   |	Initial version
 *  05/04/2011   | Anooj Gopi     |  Added wrapper function to read CID
 *  13/03/2012   | Anooj Gopi     | Added support to read CID from multiple devices.
 *  19/11/2012   | Ravikiran G    |  Added fix for nikai-435 and added new functions
 *                                |  to differenciate whether the SD Card is through
 *                                |  USB hub interface or direct Sd card interface
 * 20/06/2014    |Padmashri K     | Fix for ticket CFG3-742
 *                                | Added an argument to functions 
 *                                | fd_device_ctrl_u32SDCardInfo and 
 *                                | fd_device_ctrl_u32ReadCid
 *                                | Removed argument s32DeviceId
 ******************************************************************************/

#ifndef _FD_DEVICE_CTRL_H
#define _FD_DEVICE_CTRL_H

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|----------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/******************************************************************************
 * FUNCTION:      fd_device_ctrl_u32UsbSdCardInfo
 *
 * DESCRIPTION:   Reads the cared related data from via the GM USB hub
 *
 * PARAMETER:     fd - file descripter
 *                prIOCtrlCardState - Card State structure
 *
 * RETURNVALUE:   tU32 Error Code
 *
 * HISTORY:
 * Date        |   Modification                          | Authors
 * 23.12.10    |   Initial revision                      | Ravindran P
 * 12.03.12    |   Support added to read CID from -      |
 *                 multiple devices                      | Anooj Gopi (agn2cob)
 * 19.11.2012  |   fix for nikai-435                     | Ravikiran Gundeti
 ******************************************************************************/
tU32 fd_device_ctrl_u32UsbSdCardInfo( tS32 fd, OSAL_trIOCtrlCardState* prIOCtrlCardState );

/******************************************************************************
 * FUNCTION:      fd_device_ctrl_vCIDPatternAdapt
 *
 * DESCRIPTION:   Adapts the read CID as per the signature generated.
 *
 * PARAMETER: tU8* pu8CIDRegister - pointer for CID register
                       Ported from Gen1 Implementation
 *
 * RETURNVALUE:   tVoid
 *
 * HISTORY:
 * Date        |   Modification                         | Authors
 * 23.12.10  |   Initial revision                      | Ravindran P
 ******************************************************************************/
tVoid fd_device_ctrl_vCIDPatternAdapt(tU8* pu8CIDRegister);

/*****************************************************************************
 * FUNCTION:
 *     fd_device_ctrl_u32ReadCid
 *
 * DESCRIPTION:
 *     Reads the CID from SD card connected to GM Hub.
 *
 * PARAMETERS:
 *      pu8Cid:    (Out)  Pointer to CID data (tU8 au8CID[CID_LEN])
 *      s32DeviceIndx: (In) Device Index returned by the PRM.
 *
 * RETURNVALUE:
 *     OSAL_E_NOERROR     - Success
 *     OSAL_E_IOERROR     - Error while reading CID
 *
 * HISTORY:
 *   DATE      |  AUTHOR     |         MODIFICATION
 *   24/02/11  | Anooj Gopi  |          Created
 *   12/03/12  | Anooj Gopi  |  Added support for multiple devices
 *   20/06/14  | Padmashri K | Added one more argument to the function 
 *             |               Removed an argument s32DeviceId
 *****************************************************************************/
tU32 fd_device_ctrl_u32ReadCid( tPU8 pu8Cid,tS32 s32DeviceIndx);

/******************************************************************************
 * FUNCTION:      fd_device_ctrl_u32SDCardInfo
 *
 * DESCRIPTION:   Reads the cared related data from SD card
 *
 * PARAMETER:
 *                prIOCtrlCardState - Card State structure
 *                s32DeviceIndx - Device Index
 *
 * RETURNVALUE:   tU32 Error Code
 *
 * HISTORY:
 * Date        |   Modification                          | Authors
 * 23.12.10    |   Initial revision                      | Ravindran P
 * 12.03.12    |   Support added to read CID from -      |
 *                 multiple devices                      | Anooj Gopi (agn2cob)
 * 19.11.2012  |   fix for nikai-435                     | Ravikiran Gundeti
 * 20.06.2014  | Fix for Ticket CFG3-742                 | 
 *             | Added one more argument to the function |
 *             | Removed an argument s32DeviceId         |Padmashri M K    
 ******************************************************************************/
tU32 fd_device_ctrl_u32SDCardInfo( OSAL_trIOCtrlCardState* prIOCtrlCardState, tS32 s32DeviceIndx );

/******************************************************************************
 * FUNCTION:      fd_device_ctrl_u32DirectSDCardInfo
 *
 * DESCRIPTION:   Reads the cared related data from direct SD card (Nissan LCN2 Kai).
 *
 * PARAMETER:     prIOCtrlCardState - Card State structure
 *
 * RETURNVALUE:   tU32 Error Code
 *
 * HISTORY:
 * Date        |   Modification                          | Authors
 * 19.11.2012  |   Initial revision                      | Ravikiran Gundeti
 * 19.11.2012  |   fix for nikai-435                     | Ravikiran Gundeti
 ******************************************************************************/

tU32 fd_device_ctrl_u32DirectSDCardInfo( OSAL_trIOCtrlCardState* prIOCtrlCardState );

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/


   
/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/





#ifdef __cplusplus
}
#endif


#endif /* #ifndef _FD_DEVICE_CTRL_H */
