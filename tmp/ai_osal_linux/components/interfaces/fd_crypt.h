/*******************************************************************************
 *
 * FILE:
 *     fd_crypt.h
 *
 * REVISION:
 *	   1.1
 *
 * AUTHOR:
 *     (c) 2007, Robert Bosch India Ltd., ECM/ECM-1, Divya H, 
 *                                                   divya.h@in.bosch.com
 *
 * CREATED:
 *     24/01/2007 - Divya H
 *
 * DESCRIPTION:
 *     This file contains the exported API functions & data elements 
 *     for the module FD_CRYPT
 *
 * NOTES:
 *
 * MODIFIED:
 *    DATE      |  AUTHOR    |         MODIFICATION
 *	 25/09/07  |  Divya H   |      Modified for function_code:error_stage traces
 *    23/02/09  | Ravindran P|      API change for fd_crypt_IOControl
 *    27/10/10  | Gokulkrishnan N|  VIN Feature porting for MFD
 *    05/04/11  | Anooj Gopi |      Added support for multiple crypt devices
 *
 ******************************************************************************/

#ifndef _FD_CRYPT_H
#define _FD_CRYPT_H

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************
| includes of component-internal interfaces
| (scope: component-local)
|----------------------------------------------------------------*/
			
/* Nucleus OS Interface * /
#define NUCLEUS_CORE_S_IMPORT_INTERFACE_NUCLEUS
#include "nucleus_core_pif.h"

#include "../../../../nucleus_osal/osal_io/include/dispatcher.h" */
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

/*****************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------*/

/*****************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------*/

typedef enum
{
	FD_CRYPT_SUCCESS=OSAL_E_NOERROR,
	FD_CRYPT_SF_NOT_PRESENT=1,
	ERROR_FD_CRYPT_INVALID_PARAM= -57,
	ERROR_FD_CRYPT_ENCRYPTION_FAILED=-58,
	ERROR_FD_CRYPT_DECRYPTION_FAILED=-59,
	ERROR_FD_CRYPT_GETKDSKEYFAIL=-60,
	ERROR_FD_CRYPT_DATEFAIL=-61,
	ERROR_FD_CRYPT_NOTSUPPORTED=-62,
	ERROR_FD_CRYPT_LENGTHMISMATCH=-63,
	ERROR_FD_CRYPT_MAGICMISMATCH=-64,
	ERROR_FD_CRYPT_SIGNVERI_FAILED=-65,
	ERROR_FD_CRYPT_MAX_FILES_OPENED=-66,
	ERROR_FD_CRYPT_MEDIUM_ATTRIB_FAIL=-67,
	ERROR_FD_CRYPT_FILE_ATTRIB_FAIL=-68,
	ERROR_FD_CRYPT_FILE_HANDLE_INVALID=-69,
	ERROR_FD_CRYPT_FILE_ALREADY_EXISTS=-70,
	ERROR_FD_CRYPT_KDS_DEVICE_FAIL=-71,
	ERROR_FD_CRYPT_CID_READ_FAIL=-72,
	ERROR_FD_CRYPT_UNKNOWN=-73
}fd_crypt_tenError;

/* Enumeration for function names */
typedef enum 			
{  
   FD_CRYPT_FILE_CREATE=1,
   FD_CRYPT_FILE_REMOVE,
   FD_CRYPT_FILE_OPEN,
   FD_CRYPT_FILE_CLOSE,
   FD_CRYPT_FILE_READ,
   FD_CRYPT_FILE_WRITE,
   FD_CRYPT_IOCTRL,
   FD_CRYPT_VERIFY_SF,
   FD_CRYPT_HANDLE_ENC_WRITE,
   FD_CRYPT_HANDLE_ENC_READ
} fd_crypt_tenFunction;


typedef enum 			
{  
     FD_CRYPT_VERIFY_DONE = 0,
     FD_CRYPT_VERIFY_IN_PROGRESS
} fd_crypt_tenVerifyState;

   
/*****************************************************************
| variable declaration (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: component-local)
|----------------------------------------------------------------*/

/*****************************************************************
| function prototypes (scope: global)
|----------------------------------------------------------------*/

/*****************************************************************************
 *
 * FUNCTION:
 * 		fd_crypt_create
 *
 * DESCRIPTION:
 *     	This function is called by devinit to create the fd_crypt module.
 *	   	This function gets the keys ECC keys from KDS. 
 *
 *
 * PARAMETERS:
 *     None
 *
 * RETURNVALUE:
 *     OSAL_E_NOERROR - Success
 *     OSAL_E_UNKNOWN - failure
 *
 * HISTORY:
 *  24/10/08  | Ravindran| Ported to ADIT platform
 *  02/02/09  | Ravindran| Removed code of AES encryption/decryption
 *  05/04/11  | Anooj Gopi | Added Multi device support
 *
 *****************************************************************************/
tU32 fd_crypt_create(tVoid);

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_destory
 *
 * DESCRIPTION:
 *     This function is called by devinit.c to remove the fd_crypt module.
 *
 * PARAMETERS:
 *    None
 *
 * RETURNVALUE:
 *    OSAL_E_NOERROR - Success
 *
 *
 * HISTORY:
 *    24/10/08  | Ravindran| Ported to ADIT platform
 *    05/04/11  | Anooj Gopi | Added Multi device support
 *****************************************************************************/
tU32 fd_crypt_destroy(tVoid);

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_file_create
 *
 * DESCRIPTION:
 *     This function is called by drv_card to create a file. 
 *
 *
 * PARAMETERS:
 *     coszName:  file name
 *     enAccess:  access options
 *	   coszDrive: drive name
 *	   pu32Addr:  pointer to the file descriptor structure 
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS						- Success
 *     ERROR_FD_CRYPT_UNKNOWN				- Unknown
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED      	- Signature file verification failed
 *
 * HISTORY:
 *   DATE      |  AUTHOR  |         MODIFICATION
 *	 25/09/07  |  Divya H |	Modified for function_code:error_stage traces 
 *   07/12/07  |  Divya H | Modified for MMS 160814 fix
 *	 28/04/08  |  Divya H |	Modified for performing navigation copy in VW (MMS 187997)   
 *	 30/05/08  |  Divya H |	Modified by allowing all files to be created by default
 *							with OSAL_EN_READWRITE access parameter (MMS 187874)
    24/10/08  | Ravindran| Ported to ADIT platform 
     02/02/09  | Ravindran| Removed code of AES encryption/decryption							  
 *****************************************************************************/
fd_crypt_tenError fd_crypt_file_create(tCString coszName,OSAL_tenAccess enAccess,
					tCString coszDrive,	tU32 *pu32Addr);

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_file_remove
 *
 * DESCRIPTION:
 *     This function is used to remove a file 
 *
 * PARAMETERS:
 *     coszName:  file name
 *	   coszDrive: drive name
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS						- Success
 *     ERROR_FD_CRYPT_NOTSUPPORTED			- Not Supported
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED      	- Signature file verification failed
 *	   
 * HISTORY:
 *   DATE      |  AUTHOR  |         MODIFICATION
 *	 25/09/07  |  Divya H |	Modified for function_code:error_stage traces 
 *	 31/03/08  |  Divya H |	Modified for removing hidden and write protected files 
     24/10/08  | Ravindran| Ported to ADIT platform 
      02/02/09  | Ravindran| Removed code of AES encryption/decryption							  
 *		
 *****************************************************************************/

fd_crypt_tenError fd_crypt_file_remove(tCString coszName, tCString coszDrive );

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_file_open
 *
 * DESCRIPTION:
 *     This function is called by drv_card to open a file. 
 *
 *
 * PARAMETERS:
 *     coszName:  file name
 *     enAccess:  access options
 *	   coszDrive: drive name
 *	   pu32Addr:  pointer to the file descriptor structure 
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS						- Success
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED		- Signature file verification failed
       ERROR_FD_CRYPT_UNKNOWN               - NU call fails  
 *
 * HISTORY:
 * 11/09/07  |  Divya H | Modified code to work without NU_Set_Current_Dir calls
 * 25/09/07  |  Divya H | Modified for function_code:error_stage traces 
 * 07/12/07	 |  Divya H | Modified for MMS 160814 fix
 * 24/01/08  |  Vijay D | Modified for MMS 174264 fix
 * 28/04/08  |  Divya H | Modified for performing navigation copy in VW (MMS 187997)   	   
 * 30/05/08  |  Divya H | Modified by allowing all files to be opened by default
 *							with OSAL_EN_READWRITE access parameter (MMS 187874)
 24/10/08  | Ravindran| Ported to ADIT platform 
  02/02/09  | Ravindran| Removed code of AES encryption/decryption							  
 *****************************************************************************/
fd_crypt_tenError fd_crypt_file_open( tCString coszName,OSAL_tenAccess enAccess,
                       tCString coszDrive, tU32 *u32Addr );


/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_file_close
 *
 * DESCRIPTION:
 *     This function is called by drv_card to close a file. 
 *
 *
 * PARAMETERS:
 *     u32fd:  		descriptor of file
 *	   coszDrive: 	drive name 
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS						- Success
     
 * HISTORY:
 * 11/09/07  |  Divya H | Modified code to work without NU_Set_Current_Dir calls
 * 25/09/07  |  Divya H | Modified for function_code:error_stage traces 
 * 28/04/08  |  Divya H | Modified for performing navigation copy in VW (MMS 187997)   
 * 27/05/08  |  Divya H | Modified for performing navigation copy in VW (MMS 187997)   
 * 30/05/08  |  Divya H | Set the file attribute as hidden (MMS 187874)
   24/10/08  | Ravindran| Ported to ADIT platform 
    02/02/09  | Ravindran| Removed code of AES encryption/decryption							     
 *
 *****************************************************************************/

fd_crypt_tenError fd_crypt_file_close(tU32 u32fd,tCString coszDrive);

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_file_read
 *
 * DESCRIPTION:
 *     This function is called by drv_card to read a file. 
 *
 *
 * PARAMETERS:
 *     u32fd:  		file descriptor
 *     pBuffer:  	input buffer
 *	   u32MaxLength:maximum size of the input buffer in bytes
 *	   ps32Read:  	pointer to number of bytes read 
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS						- Success
 *     ERROR_FD_CRYPT_NOTSUPPORTED			- Not Supported
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED      	- Signature file verification failed 
 *          
 *
 * HISTORY:
 *   DATE      |  AUTHOR  |         MODIFICATION
 *	 25/09/07  |  Divya H |	Modified for function_code:error_stage traces 
     24/10/08  | Ravindran| Ported to ADIT platform 
      02/02/09  | Ravindran| Removed code of AES encryption/decryption							  
 *
 *****************************************************************************/
fd_crypt_tenError fd_crypt_file_read(tU32 u32fd, tU32 pBuffer, tU32 u32MaxLength, tS32 *ps32Read);

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_file_write
 *
 * DESCRIPTION:
 *     This function is called by drv_card to write a file. 
 *
 *
 * PARAMETERS:
 *     u32fd:  		file descriptor
 *     pBuffer:  	input buffer
 *	   u32Length:	size of the input buffer in bytes
 *	   ps32Written: pointer to number of bytes written
 *	   coszDrive: 	drive name
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS						- Success
 *     ERROR_FD_CRYPT_NOTSUPPORTED			- Not Supported
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED      	- Signature file verification failed 
 *     ERROR_FD_CRYPT_FILE_HANDLE_INVALID	- File Handle Invalid
 *     
 *
 * HISTORY:
 *   DATE      |  AUTHOR  |         MODIFICATION
 *	 25/09/07  |  Divya H |	Modified for function_code:error_stage traces 
 *	 28/04/08  |  Divya H |	Modified for performing navigation copy in VW (MMS 187997)   
 *	 19/05/08  |  Divya H |	Modified for appending huge file sizes 
 *							(greater than drv_card split size for write) (MMS 187997)   
 * 	 27/05/08  |  Divya H | Modified for performing navigation copy in VW (MMS 187997)   
 * 	 30/05/08  |  Divya H | Solved the data corruption problem seen for files with 
 *						  |	size less than 20 bytes (MMS 187874)
     24/10/08  | Ravindran| Ported to ADIT platform
      02/02/09  | Ravindran| Removed code of AES encryption/decryption 							     
 *
 *****************************************************************************/
fd_crypt_tenError fd_crypt_file_write( tU32 u32fd, tU32 pBuffer, tU32 u32Length,
                        tS32 *ps32Written,tCString coszDrive);
/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_IOControl
 *
 * DESCRIPTION:
 *     This function is called by drv_card to perform IO Control functions
 *
 *
 * PARAMETERS:
 *     u32fd:  		file descriptor
 *	   fun: 		function to be executed
 *	   arg:			argument to the function requested		
 *	   coszDrive: 	drive name
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS						- Success
 *     ERROR_FD_CRYPT_NOTSUPPORTED			- Not Supported
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED      	- Signature file verification failed 
 *     ERROR_FD_CRYPT_UNKNOWN				- Unknown
 *     
 *
 * HISTORY:
 *   DATE      |  AUTHOR  |         MODIFICATION
 *	 25/09/07  |  Divya H |	Modified for function_code:error_stage traces 
 *	 28/04/08  |  Divya H |	Modified for performing navigation copy in VW (MMS 187997)   
 * 	 27/05/08  |  Divya H | Modified for performing navigation copy in VW (MMS 187997)   
 * 	 30/05/08  |  Divya H | Seek beyond file size sets file pointer to EOF and returns error.
 *							This is the behaviour seen with /dev/card
     24/10/08  | Ravindran| Ported to ADIT platform 
      02/02/09  | Ravindran| Removed code of AES encryption/decryption							  
 *
 *****************************************************************************/
fd_crypt_tenError fd_crypt_IOControl(tU32 u32fd, tS32 s32Fun, tS32 s32Arg, tCString coszDrive);

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_verify_signaturefile
 *
 * DESCRIPTION:
 *     This function verifies the signature file for the device
 *     (The device name is extracted from the file path)
 *
 * PARAMETERS:
 *     coszCertPath - Absolute path of the certificate.
 *
 * RETURNVALUE:
 *     FD_CRYPT_SUCCESS                   - Success
 *     FD_CRYPT_SF_NOT_PRESENT            - Signature File not present
 *     ERROR_FD_CRYPT_SIGNVERI_FAILED     - Signature file verification failed 
 *     ERROR_FD_CRYPT_NOTSUPPORTED        - Not Supported
 *     ERROR_FD_CRYPT_UNKNOWN             - Unknown
 *     
 *
 * HISTORY:
 *  DATE      |  AUTHOR  |         MODIFICATION
 *  25/09/07  |  Divya H | Modified for function_code:error_stage traces 
 *  24/03/08  |  Divya H | Modified for Crypt extension signature file 
 *            |          | handling in VW 
    24/10/08  | Ravindran| Ported to ADIT platform
    02/02/09  | Ravindran| Removed code of AES encryption/decryption 
 *  13/12/10  | kgr1kor  | Re-organised the function and issue regarding critical section.     
 *  05/04/11  | AnoojGopi| Added multiple crypt device support
 *****************************************************************************/
fd_crypt_tenError fd_crypt_verify_signaturefile( tCString coszCertPath );

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_get_signaturefile_type
 *
 * DESCRIPTION                                                          
 *     Gets the signature file type
 *
 * PARAMETERS:
 *	  coszCertPath - Absolute path of the certificate.
 *
 * RETURNVALUE:
 *     OSAL_EN_SIGN_TYPE_UNKNOWN
 *     OSAL_EN_SIGN_XML_VIN
 *     OSAL_EN_SIGN_XML_CID
 *
 * HISTORY:
 *   DATE         |  AUTHOR     |         MODIFICATION
 *   23/12/10     | Ravindran P |	   Created 1st version
 *   05/04/11     | AnoojGopi   | Added multiple crypt device support
************************************************************************/
OSAL_tenSignFileType fd_crypt_get_signaturefile_type( tCString coszCertPath );

/*****************************************************************************
 *
 * FUNCTION:
 *     fd_crypt_get_signature_verify_status
 *
 * DESCRIPTION                                                          
 *      Gets the signature verify status for the device specified
 *
 * PARAMETERS:
 *	   coszDevName - Device name
 *
 * RETURNVALUE:
 *      OSAL_EN_SIGN_VERIFY_PASSED - Verification passed for this device
 *      OSAL_EN_SIGN_VERIFY_INPROGRESS  - Verification is in progress for the device
 *      OSAL_EN_SIGN_VERIFY_FAILED - Verification failed for the device
 *      OSAL_EN_SIGN_STATUS_UNKNOWN - Device in unknown error state
 *
 * HISTORY:
 *   DATE       |  AUTHOR     |         MODIFICATION
 *   23/12/10   | Ravindran P |	   Created 1st version 
 *   05/04/11   | Anooj Gopi  |      Added multi device support
************************************************************************/
OSAL_tenSignVerifyStatus fd_crypt_get_signature_verify_status( tCString coszDevName );


#ifdef __cplusplus
}
#endif


#endif /* #ifndef _FD_CRYPT_H */
