#!/usr/bin/python
# ---------------------------------------------------------------------------
# Copyright (C) Robert Bosch Car Multimedia GmbH, 2014
# This software is property of Robert Bosch GmbH. Unauthorized
# duplication and disclosure to third parties is prohibited.
# ---------------------------------------------------------------------------
# \file     CreateOSALPackage.py
# \brief    This python module generates .opk file
#            Resulting files are placed in:
#
# \author   CM-AI/ECN3 Bjoern Hornburg
#           bjoern.hornburg@de.bosch.com
#
# \par Copyright:
# (c) 2014-2014 Robert Bosch Car Multimedia GmbH
# ---------------------------------------------------------------------------

import argparse
import cmBuildTools
import opkg
import stat

#global variables
SwName                  = "osal-linux-dev"
TargetEnv               = "linuxx86make"


#Read command line arguments. Either 'release' or 'debug' is possible only.
def parseCommandParams():
    parser = argparse.ArgumentParser(description="Creates " + SwName + " package")
    parser.add_argument('build_mode', choices=['release', 'debug'])
    args = parser.parse_args()

    global Mode
    Mode = args.build_mode


def main():
    parseCommandParams()
    naviSuperRepoDir = cmBuildTools.getNaviSuperRepoDir()
    naviBuildDir = cmBuildTools.getNaviBuildDir()
    swVersion = cmBuildTools.getRepoVersion(naviSuperRepoDir)

    print "Creating " + SwName + " package with version: " + swVersion

    #package description
    opkg.init(SwName, swVersion)
    opkg.setArchitecture("i686")
    opkg.setMaintainer("Robert Bosch Car Multimedia GmbH (CM-AI/ECF) <CM-AINav3.0MiddlewareTeam@bcn.bosch.com>")
    opkg.setDescription("OSAL package for LinuxX86")
    #resulting .opkg files will be created here
    opkg.setPackageDestinationFolder(naviBuildDir + "/bin/" + TargetEnv + "/" + Mode + "/packages")


    #package content
    access_rights = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH
    opkg.addFile(naviSuperRepoDir + "/ai_osal_common/components/osal/osal_if.h", "/usr/include/osal/ai_osal_common/components/osal/osal_if.h", access_rights)
    opkg.addFile(naviSuperRepoDir + "/ai_osal_common/components/osal/system_pif.h", "/usr/include/osal/ai_osal_common/components/osal/system_pif.h", access_rights)
    opkg.addFolder(naviSuperRepoDir + "/ai_osal_common/components/osal/include", "/usr/include/osal/ai_osal_common/components/osal/include")
    opkg.addFolder(naviSuperRepoDir + "/ai_osal_linux/components/system", "/usr/include/osal/ai_osal_linux/components/system")

    opkg.addFile(naviSuperRepoDir + "/di_trace/components/trace/trace_class_def_navapp.h", "/usr/include/trace/trace_class_def_navapp.h", access_rights)
    opkg.addFile(naviSuperRepoDir + "/di_trace/components/trace/trace_comp_check_navapp.h", "/usr/include/trace/trace_comp_check_navapp.h", access_rights)
    opkg.addFile(naviSuperRepoDir + "/di_trace/components/trace/trace_if.h", "/usr/include/trace/trace_if.h", access_rights)
    opkg.addFile(naviSuperRepoDir + "/di_trace/components/trace/trace_pif.h", "/usr/include/trace/trace_pif.h", access_rights)
    opkg.addFile(naviSuperRepoDir + "/di_trace/components/trace/trc_cl.h", "/usr/include/trace/trc_cl.h", access_rights)
    opkg.addFile(naviSuperRepoDir + "/di_trace/components/trace/trc_def.h", "/usr/include/trace/trc_def.h", access_rights)
    opkg.addFile(naviSuperRepoDir + "/di_trace/components/trace/trc_macro.h", "/usr/include/trace/trc_macro.h", access_rights)
    opkg.addFile(naviSuperRepoDir + "/di_trace/components/trace/trc_stdtext.h", "/usr/include/trace/trc_stdtext.h", access_rights)
    opkg.addFolder(naviSuperRepoDir + "/di_trace/components/trace/include", "/usr/include/trace/include")

    opkg.addFile(naviBuildDir + "/bin/" + TargetEnv + "/" + Mode + "/libosal_linux_so.so", "/opt/bosch/base/lib/libosal_linux_so.so", access_rights)
    opkg.addFile(naviBuildDir + "/bin/" + TargetEnv + "/" + Mode + "/libosalcppwrapper_a.a", "/opt/bosch/base/lib/libosalcppwrapper_a.a", access_rights)

    opkg.create()


if __name__ == "__main__":

    main()
