/*!
 *******************************************************************************
 * \file             spi_tclDipoDataService.cpp
 * \brief            DiPO Data Service class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    DiPO Data Service class implements Data Service Info Management for
 CarPlay capable devices. This class must be derived from base Data Service class.
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      		| Modifications
 27.03.2014 |  Ramya Murthy                		| Initial Version
 14.04.2014 |  Ramya Murthy                		| Implemented sending GPS data to MediaPlayer client.
 13.06.2014 |  Ramya Murthy                		| Implementation for:
                                             	 (1) MPlay FI extn to start/stop loc info
                                             	 (2) VDSensor data integration
                                             	 (3) NMEA-PASCD sentence for DiPO
 13.10.2014 |  Hari Priya E R (RBEI/ECP2)  		| Added interface to get Vehicle Data for PASCD.

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLDIPOLOCATION_H_
#define SPI_TCLDIPOLOCATION_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclDataServiceDevBase.h"
#include "spi_tclDataServiceTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */


/*!
 * \class spi_tclDipoDataService
 * \brief Mirrorlink Connection class implements Data Service Info Management for
 *        Mirrorlink capable devices. This class must be derived from base
 *        Data Service class.
 */

class spi_tclDipoDataService :
   public spi_tclDataServiceDevBase  //! Base Connection Class
{
public:
   /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclDipoDataService::spi_tclDipoDataService(const trDataServiceCb...)
    ***************************************************************************/
   /*!
    * \fn     spi_tclDipoDataService(const trDataServiceCb& rfcorDataServiceCb)
    * \brief  Parameterised Constructor
    * \param  rfcorDataServiceCb: [IN] Structure containing callbacks to
    *            DataService Manager.
    * \sa      ~spi_tclDipoDataService()
    **************************************************************************/
   spi_tclDipoDataService(const trDataServiceCb& rfcorDataServiceCb);

   /***************************************************************************
    ** FUNCTION:  spi_tclDipoDataService::~spi_tclDipoDataService
    ***************************************************************************/
   /*!
    * \fn     ~spi_tclDipoDataService()
    * \brief  Destructor
    * \sa     spi_tclDipoDataService()
    **************************************************************************/
   virtual ~spi_tclDipoDataService();


   /***** Start of Methods overridden from spi_tclDataServiceDevBase *********/

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDipoDataService::bInitialise();
   ***************************************************************************/
   /*!
   * \fn      bInitialise()
   * \brief   Method to initialises the service handler. (Performs initialisations which
   *          are not device specific.)
   *          Mandatory interface to be implemented.
   * \retval  t_Bool: TRUE - If ServiceHandler is initialised successfully, else FALSE.
   * \sa      bUninitialise()
   ***************************************************************************/
   virtual t_Bool bInitialise();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDipoDataService::bUninitialise();
   ***************************************************************************/
   /*!
   * \fn      bUninitialise()
   * \brief   Method to uninitialise the service handler.
   *          Mandatory interface to be implemented.
   * \retval  t_Bool: TRUE - If ServiceHandler is uninitialised successfully, else FALSE.
   * \sa      bInitialise()
   ***************************************************************************/
   virtual t_Bool bUninitialise();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDipoDataService::vOnSelectDeviceResult(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vOnSelectDeviceResult(t_U32 u32DeviceHandle)
    * \brief   Called when a device is selected.
    *          Mandatory interface to be implemented.
    * \param   [IN] u32DeviceHandle: Unique handle of selected device
    * \retval  None
    * \sa      vOnDeselectDevice()
    **************************************************************************/
   virtual t_Void vOnSelectDeviceResult(t_U32 u32DeviceHandle);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDipoDataService::vOnDeselectDeviceResult(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vOnDeselectDeviceResult(t_U32 u32DeviceHandle)
    * \brief   Called when currently selected device is de-selected.
    *          Mandatory interface to be implemented.
    * \param   [IN] u32DeviceHandle: Unique handle of selected device
    * \retval  None
    * \sa      vOnSelectDevice()
    **************************************************************************/
   virtual t_Void vOnDeselectDeviceResult(t_U32 u32DeviceHandle);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDipoDataService::vStartLocationData(...)
    ***************************************************************************/
   /*!
    * \fn      vStartLocationData(
    *             const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList)
    * \brief   Called to start sending LocationData to selected device.
    *          Mandatory interface to be implemented.
    * \param   [IN] rfcoNmeaSentencesList: List of NMEA sentences to be sent in
    *             location data.
    * \retval  None
    **************************************************************************/
   virtual t_Void vStartLocationData(
         const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDipoDataService::vStopLocationData(...)
    ***************************************************************************/
   /*!
    * \fn      vStopLocationData()
    * \brief   Called to stop sending LocationData to selected device.
    *          Mandatory interface to be implemented.
    * \param   [IN] rfcoNmeaSentencesList: List of NMEA sentences which should
    *             be stopped in location data.
    * \retval  None
    **************************************************************************/
   virtual t_Void vStopLocationData(
         const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDipoDataService::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   *          Optional interface to be implemented.
   * \param   rfcorGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDipoDataService::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDipoDataService::vOnData(const trVehicleData& rfcoVehicleData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trVehicleData& rfcoVehicleData)
   * \brief   Method to receive Vehicle data.
   * \param   rfcoVehicleData: [IN] Vehicle data
   * \param   bSolicited: [IN] True if the data update is for changed vehicle data, else False
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trVehicleData& rfcoVehicleData, t_Bool bSolicited);


   /******* End of Methods overridden from spi_tclDataServiceDevBase *********/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDipoDataService::vSetLocDataSubscription();
   ***************************************************************************/
   /*!
   * \fn      vSetLocDataSubscription()
   * \brief   Subscribes/Unsubscribes for location data information.
   * \param   bSubscriptionOn: [IN] Indicates if location data should be
   *             subscribed/unsubscribed.
   * \retval  t_Void
   ***************************************************************************/
   virtual t_Void vSetLocDataSubscription(t_Bool bSubscriptionOn);

   /***************************************************************************
    ** Selected device's information
    ***************************************************************************/
   t_U32             m_u32SelDevHandle;

   /***************************************************************************
    ** LocationData transfer enabled/disabled flag
    ***************************************************************************/
   t_Bool            m_bIsLocDataTransferEnabled;

   /***************************************************************************
    ** List of NMEA sentences to be transferred in LocationData
    ***************************************************************************/
   std::vector<tenNmeaSentenceType>   m_NmeaSentencesList;

   /***************************************************************************
    ** DataService callbacks structure
    ***************************************************************************/
   trDataServiceCb   m_rDataServiceCb;

   /***************************************************************************
    ** Structure containing GPS data
    ***************************************************************************/
    trGPSData      m_rGPSData;

   /***************************************************************************
    ** Structure containing Sensor data
    ***************************************************************************/
   trSensorData      m_rSensorData;

   /***************************************************************************
    ** Structure containing Vehicle data
    ***************************************************************************/
   trVehicleData      m_rVehicleData;


};
#endif // SPI_TCLDIPOLOCATION_H_
