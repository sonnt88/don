/*!
 *******************************************************************************
 * \file             spi_tclDipoDataService.cpp
 * \brief            DiPO Data Service class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    DiPO Data Service class implements Data Service Info Management for
 CarPlay capable devices. This class must be derived from base Data Service class.
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      		| Modifications
 27.03.2014 |  Ramya Murthy                		| Initial Version
 14.04.2014 |  Ramya Murthy                		| Implemented sending GPS data to MediaPlayer client.
 13.06.2014 |  Ramya Murthy                		| Implementation for:
                                             	 (1) MPlay FI extn to start/stop loc info
                                             	 (2) VDSensor data integration
                                             	 (3) NMEA-PASCD sentence for DiPO
 13.10.2014 |  Hari Priya E R (RBEI/ECP2)  		| Added interface to get Vehicle Data for PASCD.
 23.04.2015 |  Ramya Murthy (RBEI/ECP2)    | Implementation to provide PASCD & PAGCD sentences (for China region)
 26.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors



 \endverbatim
 ******************************************************************************/
#include <algorithm>

#include "spi_tclFactory.h"
#include "IPCMessageQueue.h"
#include "spi_tclMPlayClientHandler.h"
#include "NmeaEncoder.h"
#include "spi_tclDipoDataService.h"
#include "spi_tclConfigReader.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DATASERVICE
      #include "trcGenProj/Header/spi_tclDipoDataService.cpp.trc.h"
   #endif
#endif


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
//typedef std::vector<tenNmeaSentenceType>::iterator tNmeaSentenceLstItr;            //Commented to suppress Lint

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclDipoDataService::spi_tclDipoDataService
 ***************************************************************************/
spi_tclDipoDataService::spi_tclDipoDataService(const trDataServiceCb& rfcorDataServiceCb)
   : m_u32SelDevHandle(0),
     m_bIsLocDataTransferEnabled(false),
     m_rDataServiceCb(rfcorDataServiceCb)
{
   ETG_TRACE_USR1(("spi_tclDipoDataService() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDipoDataService::spi_tclDipoDataService
 ***************************************************************************/
spi_tclDipoDataService::~spi_tclDipoDataService()
{
   ETG_TRACE_USR1(("~spi_tclDipoDataService() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclDipoDataService::bInitialise()
 ***************************************************************************/
t_Bool spi_tclDipoDataService::bInitialise()
{
   ETG_TRACE_USR1(("spi_tclDipoDataService::bInitialise() entered \n"));
   return true;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclDipoDataService::bUninitialise()
 ***************************************************************************/
t_Bool spi_tclDipoDataService::bUninitialise()
{
   ETG_TRACE_USR1(("spi_tclDipoDataService::bUninitialise() entered \n"));
   return true;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDipoDataService::vOnSelectDeviceResult(t_U32...)
 ***************************************************************************/
t_Void spi_tclDipoDataService::vOnSelectDeviceResult(t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclDipoDataService::vOnSelectDeviceResult() entered: u32DeviceHandle = %u \n", u32DeviceHandle));

   //! Store selected device's handle
   m_u32SelDevHandle = u32DeviceHandle;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDipoDataService::vOnDeselectDeviceResult(t_U32...)
 ***************************************************************************/
t_Void spi_tclDipoDataService::vOnDeselectDeviceResult(t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclDipoDataService::vOnDeselectDeviceResult() entered: u32DeviceHandle = %u \n", u32DeviceHandle));

   //! Clear data
   if (u32DeviceHandle == m_u32SelDevHandle)
   {
      //Unregister for Location data
      vSetLocDataSubscription(false);

      m_bIsLocDataTransferEnabled = false;
      m_NmeaSentencesList.clear();

      //! Clear selected device handle
      m_u32SelDevHandle = 0;

   } //if (u32DeviceHandle == m_u32SelDevHandle)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDipoDataService::vStartLocationData(...)
 ***************************************************************************/
t_Void spi_tclDipoDataService::vStartLocationData(
      const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList)
{
   ETG_TRACE_USR1(("spi_tclDipoDataService::vStartLocationData() entered: rfcoNmeaSentencesList size = %u \n",
         rfcoNmeaSentencesList.size()));

   m_bIsLocDataTransferEnabled = true;

   //Store complete Nmea sentences list. Only the requested sentences (if supported)
   //will be transferred. (In future, may need to support individual sentences transfer).
   m_NmeaSentencesList = rfcoNmeaSentencesList;

   //Register for Location data
   vSetLocDataSubscription(true);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDipoDataService::bStopLocationData(...)
 ***************************************************************************/
t_Void spi_tclDipoDataService::vStopLocationData(
      const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList)
{
   ETG_TRACE_USR1(("spi_tclDipoDataService::vStopLocationData() entered: rfcoNmeaSentencesList size = %u \n",
         rfcoNmeaSentencesList.size()));

   m_bIsLocDataTransferEnabled = false;

   //Clear Nmea sentences list since currently all sentences transfer is stopped.
   //(In future, may need to support stopping transfer of individual sentences).
   m_NmeaSentencesList.clear();

   //Unregister for Location data
   vSetLocDataSubscription(false);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDipoDataService::vOnData(const trGPSData& rfcorGpsData)
***************************************************************************/
t_Void spi_tclDipoDataService::vOnData(const trGPSData& rfcorGpsData)
{
   //ETG_TRACE_USR1(("spi_tclDipoDataService::vOnData() entered \n"));

   //Store the GPS Data
   m_rGPSData =  rfcorGpsData;

   //! If a device is active & LocationData start request is received,
   //! send NMEA-formatted GPS data to Mediaplayer client handler.
   if (
      (0 != m_u32SelDevHandle)
      &&
      (true == m_bIsLocDataTransferEnabled)
      &&
      (false == m_NmeaSentencesList.empty())
      )
   {
      NmeaEncoder oNmeaEncoder(rfcorGpsData, m_rSensorData,m_rVehicleData);
      t_String szNmeaGGASentence;
      t_String szNmeaRMCSentence;
      t_String szNmeaPASCDSentence;
      t_String szNmeaPAGCDSentence;

      //! @Note:
      //! Send GPS data if GPGGA, GPRMC, GPGSV and/or GPHT sentences are requested
      //! Send GPS data if PACSD, PAGCD and/or PAACD sentences are requested
      t_Bool bTransferGpsData = false;
      t_Bool bTransferDrData = false;

      //! Check and get GPGGA sentence if required.
      if (m_NmeaSentencesList.end() !=
            std::find(m_NmeaSentencesList.begin(), m_NmeaSentencesList.end(), e8NMEA_GPGGA))
      {
         szNmeaGGASentence = oNmeaEncoder.szGetNmeaGGASentence(e8NMEA_SOURCE_GPS);
         bTransferGpsData = true;
      }
      //! Check and get GPRMC sentence if required.
      if (m_NmeaSentencesList.end() !=
            std::find(m_NmeaSentencesList.begin(), m_NmeaSentencesList.end(), e8NMEA_GPRMC))
      {
         szNmeaRMCSentence = oNmeaEncoder.szGetNmeaRMCSentence(e8NMEA_SOURCE_GPS);
         bTransferGpsData = true;
      }
      //! Check and get PASCD sentence if required.
      if (m_NmeaSentencesList.end() !=
         std::find(m_NmeaSentencesList.begin(), m_NmeaSentencesList.end(), e8NMEA_PASCD))
      {
         szNmeaPASCDSentence = oNmeaEncoder.szGetNmeaPASCDSentence(true);
         bTransferDrData = true;
      }
      //! Check and get PAGCD sentence if required.
      if (m_NmeaSentencesList.end() !=
         std::find(m_NmeaSentencesList.begin(), m_NmeaSentencesList.end(), e8NMEA_PAGCD))
      {
         szNmeaPAGCDSentence = oNmeaEncoder.szGetNmeaPAGCDSentence(true);
         bTransferDrData = true;
      }

      spi_tclFactory* poFactory = spi_tclFactory::getInstance();
      ahl_tclBaseOneThreadApp* poMainApp = (NULL != poFactory) ? (poFactory->poGetMainAppInstance()) : (NULL);
      spi_tclMPlayClientHandler* poMplayClient = (NULL != poMainApp) ?
            (spi_tclMPlayClientHandler::getInstance(poMainApp)) : (NULL);
      if (NULL != poMplayClient)
      {
         if (true == bTransferGpsData)
         {
            poMplayClient->bTransferGPSData(m_u32SelDevHandle,szNmeaGGASentence, szNmeaRMCSentence, "", "");
         }
         if (true == bTransferDrData)
         {
            poMplayClient->bTransferDrData(m_u32SelDevHandle,szNmeaPASCDSentence, szNmeaPAGCDSentence, "");
         }
      } //if (NULL != poMplayClient)
   } //if ((0 != m_u32SelDevHandle)&&...)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDipoDataService::vOnData(const trSensorData&...)
***************************************************************************/
t_Void spi_tclDipoDataService::vOnData(const trSensorData& rfcorSensorData)
{
   //ETG_TRACE_USR1(("spi_tclDipoDataService::vOnData() entered \n"));

   //! Store Sensor Data
   //@Note: It will be used along with Location data to provode NMEA data
   //once Location data is received.
   m_rSensorData = rfcorSensorData;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDipoDataService::vOnData(const trVehicleData&...)
***************************************************************************/
t_Void spi_tclDipoDataService::vOnData(const trVehicleData& rfcoVehicleData, t_Bool bSolicited)
{
   //ETG_TRACE_USR1(("spi_tclDipoDataService::vOnData() entered \n"));

   //! Store Vehicle Data
   m_rVehicleData = rfcoVehicleData;

   //! Send PASCD sentence update here only for Sensor mode variants
   if (
      (0 != m_u32SelDevHandle)
      &&
      (true == bSolicited)
      &&
      (true == m_bIsLocDataTransferEnabled)
      &&
      (false == m_NmeaSentencesList.empty())
      )
   {
      NmeaEncoder oNmeaEncoder(m_rGPSData, m_rSensorData, rfcoVehicleData);
      t_String szNmeaPASCDSentence;

      //! Check and get PASCD sentence if required.
      if (m_NmeaSentencesList.end() !=
         std::find(m_NmeaSentencesList.begin(), m_NmeaSentencesList.end(), e8NMEA_PASCD))
      {
         /*Set the Posix time in the NMEA Encoder and format this appropriately and
         populate the timestamp parameter in PASCD sentence*/
         szNmeaPASCDSentence = oNmeaEncoder.szGetNmeaPASCDSentence(false);
      }
      spi_tclFactory* poFactory = spi_tclFactory::getInstance();
      ahl_tclBaseOneThreadApp* poMainApp = (NULL != poFactory) ? (poFactory->poGetMainAppInstance()) : (NULL);
      spi_tclMPlayClientHandler* poMplayClient = (NULL != poMainApp) ?
            (spi_tclMPlayClientHandler::getInstance(poMainApp)) : (NULL);
      if (NULL != poMplayClient)
      {
         if (false == szNmeaPASCDSentence.empty())
         {
            poMplayClient->bTransferDrData(m_u32SelDevHandle,szNmeaPASCDSentence, "", "");
         }
      }
   }//if ((0 != m_u32SelDevHandle)&&...)
}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclDipoDataService::vSetLocDataSubscription();
***************************************************************************/
t_Void spi_tclDipoDataService::vSetLocDataSubscription(t_Bool bSubscribe)
{
   ETG_TRACE_USR1(("spi_tclDipoDataService::vSetLocDataSubscription() entered: bSubscribe = %u \n",
         ETG_ENUM(BOOL, bSubscribe)));
   /*lint -esym(40,fvSubscribeForLocationData)fvSubscribeForLocationData Undeclared identifier */
   //! Forward LocationData subscription/unsubscription request to DataService manager
   if (NULL != m_rDataServiceCb.fvSubscribeForLocationData)
   {
      m_rDataServiceCb.fvSubscribeForLocationData(bSubscribe, e8GPS_DATA);
      //@Note: Currently only GPS data is being used. In future if DeadReckoning
      //data is required (for PAACD, PAGCD), need to subscribe for DR data.
   }
}


//lint –restore

///////////////////////////////////////////////////////////////////////////////
// <EOF>
