/***********************************************************************/
/*!
* \file  spi_tclDiPoVideo.cpp
* \brief DiPo Video Implementation
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPo Video Implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
21.01.2014  | Shiva Kumar Gurija    | Updated with Video Response Interface
API's in CmdInterface
26.05.2015 |  Tejaswini H B(RBEI/ECP2)  | Added Lint comments to suppress C++11 Errors
17.07.2015 | Sameer Chandra         | Memory leak fix



\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Trace.h"
#include "IPCMessageQueue.h"
#include "spi_tclVideoTypedefs.h"
#include "spi_tclDiPoVideo.h"
#include "DiPOTypes.h"


#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VIDEO
#include "trcGenProj/Header/spi_tclDiPoVideo.cpp.trc.h"
#endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclDiPoVideo::spi_tclDiPoVideo()
***************************************************************************/
spi_tclDiPoVideo::spi_tclDiPoVideo(void):spi_tclVideoDevBase()
{
   //constructor
   ETG_TRACE_USR1(("spi_tclDiPoVideo()"));
}

/***************************************************************************
** FUNCTION:  spi_tclDiPoVideo::~spi_tclDiPoVideo()
***************************************************************************/
spi_tclDiPoVideo::~spi_tclDiPoVideo(void)
{
   //destructor
   ETG_TRACE_USR1(("~spi_tclDiPoVideo() "));
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDiPoVideo::bInitialize()
***************************************************************************/
t_Bool spi_tclDiPoVideo::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclDiPoVideo:bInitialize"));
   //Add code
   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoVideo::vUninitialize()
***************************************************************************/
t_Void spi_tclDiPoVideo::vUninitialize()
{
   ETG_TRACE_USR1(("spi_tclDiPoVideo:vUninitialize"));
   //add code
}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclDiPoVideo::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclDiPoVideo::vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
{
   ETG_TRACE_USR1(("spi_tclDiPoVideo:vRegisterCallbacks()"));
   //Copy
   m_rVideoCallbacks = corfrVideoCallbacks;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoVideo::vSelectDevice()
***************************************************************************/
t_Void spi_tclDiPoVideo::vSelectDevice(const t_U32 cou32DevId,
                                       const tenDeviceConnectionReq coenConnReq)
{

	/*lint -esym(40,fpvSelectDeviceCb)fpvSelectDeviceCb Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDiPoVideo:vSelectDevice:Device ID-0x%x ",cou32DevId));
   SPI_INTENTIONALLY_UNUSED(coenConnReq);
   if(NULL != m_rVideoCallbacks.fpvSelectDeviceCb)
   {
      (m_rVideoCallbacks.fpvSelectDeviceCb)(cou32DevId,e8NO_ERRORS);
   }//if(NULL != m_rVideoCallbacks.fpvSelectDeviceCb)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDiPoVideo::bLaunchVideo()
***************************************************************************/
t_Bool spi_tclDiPoVideo::bLaunchVideo(const t_U32 cou32DevId,
                                      const t_U32 cou32AppId,
                                      const tenEnabledInfo coenSelection)
{
   ETG_TRACE_USR1(("spi_tclDiPoVideo:bLaunchVideo:Device ID-0x%x AppID-0x%x ",
      cou32DevId,cou32AppId));
   SPI_INTENTIONALLY_UNUSED(coenSelection);
   // add code 

   return true;
}

/***************************************************************************
** FUNCTION:  t_U32  spi_tclDiPoVideo::vStartVideoRendering()
***************************************************************************/
t_Void spi_tclDiPoVideo::vStartVideoRendering(t_Bool bStartVideoRendering)
{

   /*lint -esym(40,fpvVideoRenderStatusCb)fpvVideoRenderStatusCb Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDiPoVideo:vStartVideoRendering() "));
   //Currently unused - just send the response
   if(NULL != m_rVideoCallbacks.fpvVideoRenderStatusCb )
   {
      (m_rVideoCallbacks.fpvVideoRenderStatusCb)(bStartVideoRendering,e8DEV_TYPE_DIPO);
   }//if(NULL != m_rVideoCallbacks.fpvCbUpdateVideoRenderStatus )
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoVideo::vGetVideoSettings()
***************************************************************************/
t_Void spi_tclDiPoVideo::vGetVideoSettings(const t_U32 cou32DevId,
                                           trVideoAttributes& rfrVideoAttributes)
{
   ETG_TRACE_USR1(("spi_tclDiPoVideo:vGetVideoSettings() "));
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   SPI_INTENTIONALLY_UNUSED(rfrVideoAttributes);
   //Add code
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoVideo::vSetServerAspectRatio()
***************************************************************************/
t_Void spi_tclDiPoVideo::vSetServerAspectRatio(const tenScreenAspectRatio& corfenScrAspRatio)
{
   ETG_TRACE_USR1(("spi_tclDiPoVideo:vSetServerAspectRatio()"));
   SPI_INTENTIONALLY_UNUSED(corfenScrAspRatio);
   //Add code
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoVideo::vSetOrientationMode()
***************************************************************************/
t_Void spi_tclDiPoVideo::vSetOrientationMode(const t_U32 cou32DevId,
                                             const tenOrientationMode coenOrientationMode,
                                             const trUserContext& corfrUsrCntxt)
{

	/*lint -esym(40,fpvSetOrientationModeCb)fpvSetOrientationModeCb Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDiPoVideo:vSetOrientationMode() "));

   SPI_INTENTIONALLY_UNUSED(coenOrientationMode);

   //send the success as a response
   if(NULL != m_rVideoCallbacks.fpvSetOrientationModeCb)
   {
      (m_rVideoCallbacks.fpvSetOrientationModeCb)(cou32DevId,
         e8UNSUPPORTED_OPERATION,corfrUsrCntxt,e8DEV_TYPE_DIPO);
   }// if(NULL != m_rVideoCallbacks.fpvCbSetVideoBlockingMode)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoVideo::vSetScreenAttr()
***************************************************************************/
t_Void spi_tclDiPoVideo::vSetScreenAttr(const trVideoConfigData& corfrVideoConfig)
{
   IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
   trDiPOVideoConfigMsg *poVideoConfigMsg = (trDiPOVideoConfigMsg *)oMessageQueue.vpCreateBuffer(sizeof(trDiPOVideoConfigMsg));
   if(NULL != poVideoConfigMsg)
   {
      poVideoConfigMsg->enMsgType = e8VIDEO_CONFIG_MESSAGE;
      poVideoConfigMsg->rVideoConfigData = corfrVideoConfig;
      t_Bool bStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*)poVideoConfigMsg, DIPO_MESSAGE_PRIORITY,(eMessageType) TCL_DATA_MESSAGE));
      ETG_TRACE_USR4(("IPC Message send status from spi_tclDiPoVideo::vSetScreenAttr: %d", bStatus));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poVideoConfigMsg);
   }//if(NULL != poVideoConfigMsg)
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>


