/***********************************************************************/
/*!
* \file  spi_tclDiPoVideo.h
* \brief DiPo Video Implementation
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPo Video Implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
21.01.2014  | Shiva Kumar Gurija    | Updated with Video Response Interface
API's in CmdInterface

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLDIPOVIDEO_H_
#define _SPI_TCLDIPOVIDEO_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclVideoDevBase.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclDiPoVideo
* \brief 
****************************************************************************/
class spi_tclDiPoVideo:public spi_tclVideoDevBase 
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoVideo::spi_tclDiPoVideo()
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoVideo()
   * \brief   Default Constructor
   * \sa      ~spi_tclDiPoVideo()
   **************************************************************************/
   spi_tclDiPoVideo();

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoVideo::~spi_tclDiPoVideo()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclDiPoVideo()
   * \brief   Destructor
   * \sa      spi_tclDiPoVideo()
   **************************************************************************/
   ~spi_tclDiPoVideo();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoVideo::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the ML Video related things
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoVideo::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief   To Uninitialize all the ML Video related things
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vUninitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclDiPoVideo::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
   * \brief   To Register for the asynchronous responses that are required from
   *          DiPo Video
   * \param   corfrVideoCallbacks : [IN] Video callabcks structure
   * \retval  t_Void 
   **************************************************************************/
   t_Void vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoVideo::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSelectDevice(const t_U32 cou32DevId,
   *          const tenDeviceConnectionReq coenConnReq)
   * \brief   To Initialize/UnInitialize Video setup for the currently selected device
   * \pram    cou32DevId : [IN] Unique Device Id
   * \param   coenConnReq : [IN] connected/disconnected
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq);


   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoVideo::bLaunchVideo()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bLaunchVideo(const t_U32 cou32DevId,
   *                 const t_U32 cou32AppId,
   *                 const tenEnabledInfo coenSelection)
   * \brief   To Launch the Video for the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    cou32AppId  : [IN] Application Id
   * \pram    coenSelection  : [IN] Enable/disable the video
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bLaunchVideo(const t_U32 cou32DevId,
      const t_U32 cou32AppId,
      const tenEnabledInfo coenSelection);

   /***************************************************************************
   ** FUNCTION:  t_U32  spi_tclDiPoVideo::vStartVideoRendering()
   ***************************************************************************/
   /*!
   * \fn      t_Void vStartVideoRendering(t_Bool bStartVideoRendering)
   * \brief   Method send request to ML/DiPo Video eithr to start or stop
   *          Video Rendering
   * \pram    bStartVideoRendering : [IN] True - Start Video rendering
   *                                      False - Stop Video rendering
   * \retval  t_Void 
   **************************************************************************/
   t_Void vStartVideoRendering(t_Bool bStartVideoRendering);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPoVideo::vGetVideoSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vGetVideoSettings(const t_U32 cou32DevId,
   *                                  trVideoAttributes& rfrVideoAttributes
   * \brief  To get the current Video Settings.
   * \param  u32DeviceHandle    : [IN] Uniquely identifies the target Device.
   * \param  rfrVideoAttributes : [OUT]includes screen size & orientation.
   * \retval t_Void
   * \sa
   **************************************************************************/
   t_Void vGetVideoSettings(const t_U32 cou32DevId,
      trVideoAttributes& rfrVideoAttributes);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPoVideo::vSetServerAspectRatio()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetServerAspectRatio(const tenScreenAspectRatio& corfenScrAspRatio)
   * \brief  Interface to set the aspect ratio of Head Unit.
   * \param  corfenScrAspRatio : [IN] Server Aspect Ratio.
   * \retval t_Void
   **************************************************************************/
   t_Void vSetServerAspectRatio(const tenScreenAspectRatio& corfenScrAspRatio);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPoVideo::vSetOrientationMode()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetOrientationMode(const t_U32 cou32DevId,
   *                                    const tenOrientationMode coenOrientationMode,
   *                                    const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  cou32DevId          : [IN] Uniquely identifies the target Device.
   * \param  coenOrientationMode : [IN] Orientation Mode Value.
   * \param  corfrUsrCntxt       : [IN] User Context 
   * \retval t_Void
   **************************************************************************/
   t_Void vSetOrientationMode(const t_U32 cou32DevId,
      const tenOrientationMode coenOrientationMode,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPoVideo::vSetScreenAttr()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vSetScreenAttr(const trVideoConfigData& corfrVideoConfig)
   * \brief  Interface to set the screen attributes of Head Unit.
   * \param  corfrVideoConfig   : [IN] Screen Setting attributes.
   * \retval t_Void
   **************************************************************************/
   t_Void vSetScreenAttr(const trVideoConfigData& corfrVideoConfig);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoVideo& spi_tclDiPoVideo::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoVideo& operator= (const spi_tclDiPoVideo &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDiPoVideo& operator= (const spi_tclDiPoVideo &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoVideo::spi_tclDiPoVideo(const spi_tclDiPoVideo..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoVideo(const spi_tclDiPoVideo &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDiPoVideo(const spi_tclDiPoVideo &corfrSrc);

   //! Video Callbacks structure
   trVideoCallbacks m_rVideoCallbacks;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclDiPoVideo

#endif //_SPI_TCLDIPOVIDEO_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>


