/*!
 *******************************************************************************
 * \file             spi_tclDiPoConnection.cpp
 * \brief            DiPo Connection class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    DiPo Connection class to handle ios devices capable of DiPo
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.01.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 13.02.2013 |  Shihabudheen P M            | Added 1. vOnDiPODeviceConnect
                                                   2. vOnDiPODeviceDisConnect
 31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()
 05.11.2014 |  Ramya Murthy                | Added response for Application metadata.
 26.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors

 06.05.2015  |Tejaswini HB                 |Lint Fix

 \endverbatim
 ******************************************************************************/

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#include <unistd.h>
#include <cstdlib>
#include "spi_tclFactory.h"
#include "spi_tclMPlayClientHandler.h"
#include "spi_tclDiPoConnection.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
      #include "trcGenProj/Header/spi_tclDiPoConnection.cpp.trc.h"
   #endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::spi_tclDiPoConnection
 ***************************************************************************/
spi_tclDiPoConnection::spi_tclDiPoConnection():m_poMplayClientHandler(NULL), m_enCurrDiPOState(e8DIPO_NOT_ACTIVE), 
   m_enDeviceSelReq(e8DEVCONNREQ_SELECT), m_u32CurrSelectedDevice(0), m_enCarplaySettingStatus(e8USAGE_ENABLED)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::~spi_tclDiPoConnection
 ***************************************************************************/
spi_tclDiPoConnection::~spi_tclDiPoConnection()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_poMplayClientHandler = NULL;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::bInitializeConnection
 ***************************************************************************/
t_Bool spi_tclDiPoConnection::bInitializeConnection()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   return true;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::vOnSaveSettings
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vOnSaveSettings()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_enCurrDiPOState = e8DIPO_NOT_ACTIVE;
   m_u32CurrSelectedDevice = 0;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::bStartDeviceDetection
 ***************************************************************************/
t_Bool spi_tclDiPoConnection::bStartDeviceDetection()
{
   t_Bool bRetVal = false;
   ahl_tclBaseOneThreadApp *poMainApp = spi_tclFactory::getInstance()->poGetMainAppInstance();
   if (NULL != poMainApp)
   {
      m_poMplayClientHandler = spi_tclMPlayClientHandler::getInstance(poMainApp, this);
      SPI_NORMAL_ASSERT(NULL == m_poMplayClientHandler);
      if (NULL != m_poMplayClientHandler)
      {
         m_poMplayClientHandler->vRegisterForProperties();
         bRetVal = true;
      }
   }
   ETG_TRACE_USR1(("spi_tclDiPoConnection::bStartDeviceDetection bRetVal = %d \n", bRetVal));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::vUnInitializeConnection
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vUnInitializeConnection()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_poMplayClientHandler = NULL;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vOnAddDevicetoList
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vOnAddDevicetoList(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::bSetDevProjUsage
 ***************************************************************************/
t_Bool spi_tclDiPoConnection::bSetDevProjUsage(tenEnabledInfo enServiceStatus)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_enCarplaySettingStatus = enServiceStatus;
   return true; //! TODO
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vRegisterCallbacks
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vRegisterCallbacks(trConnCallbacks &ConnCallbacks)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_rDiPoConnCallbacks = ConnCallbacks;
}


/* Interface for interact with mediaplayer*/
/***************************************************************************
** FUNCTION:  spi_tclDiPoConnection::vOnDiPODeviceConnect()
***************************************************************************/
t_Void spi_tclDiPoConnection::vOnDiPODeviceConnect(const trDeviceInfo &corfrDeviceInfo)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   vOnDeviceConnection(corfrDeviceInfo.u32DeviceHandle,corfrDeviceInfo);  
}

/***************************************************************************
** FUNCTION:  spi_tclDiPoConnection::vOnDiPODeviceDisConnect()
***************************************************************************/
t_Void spi_tclDiPoConnection::vOnDiPODeviceDisConnect(const t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   vOnDeviceDisconnection(u32DeviceHandle);
}

/***************************************************************************
** FUNCTION:  spi_tclDiPoConnection::vOnSelectDevice()
***************************************************************************/
t_Void spi_tclDiPoConnection::vOnSelectDevice(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceConnectionReq enDevConnReq, tenEnabledInfo enDAPUsage,
               tenEnabledInfo enCDBUsage, t_Bool bIsHMITrigger,
               const trUserContext corUsrCntxt)
{

	SPI_INTENTIONALLY_UNUSED(bIsHMITrigger);
	SPI_INTENTIONALLY_UNUSED(corUsrCntxt);
	SPI_INTENTIONALLY_UNUSED(enDAPUsage);
	SPI_INTENTIONALLY_UNUSED(enCDBUsage);
	SPI_INTENTIONALLY_UNUSED(enDevConnType);


   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_u32CurrSelectedDevice = cou32DeviceHandle;
   m_enDeviceSelReq = enDevConnReq;
   tenTriggerType enTriggerType = e8DIPO_TRIGGER_SELECT_DEVICE;
   tenSessionStatus enSessionStatus = e8_SESSION_UNKNOWN; //Dummy value. will not process it
   tenDeviceConnectionStatus enDeviceConnectionStatus = e8DEV_NOT_CONNECTED;//Dummy value . will not process
   // Handle to change the state change.
   vHandleDiPOState(cou32DeviceHandle, enDevConnReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType);
}

/***************************************************************************
** FUNCTION:  spi_tclDiPoConnection::vPostSelctDeviceResult()
***************************************************************************/
t_Void spi_tclDiPoConnection::vPostSelectDeviceResult(tenResponseCode enResponse, 
    tenErrorCode enErrorType)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   if((e8SUCCESS != enResponse) && (e8DEVCONNREQ_SELECT == m_enDeviceSelReq))
   {
      m_enCurrDiPOState = e8DIPO_NOT_ACTIVE;
      
   } //if((e8SUCCESS != enResponse) && (e8DEVCONNREQ_SELECT == m_enDeviceSelReq))
   else if((e8SUCCESS != enResponse) && (e8DEVCONNREQ_SELECT != m_enDeviceSelReq) )
   {
      if(e8DIPO_NOT_ACTIVE != m_enCurrDiPOState)
      {
         m_enCurrDiPOState = e8DIPO_ACTIVE;
      }//if(e8DIPO_NOT_ACTIVE != m_enCurrDiPOState)
   }//else if((e8SUCCESS != enResponse) && (e8DEVCONNREQ_SELECT != m_enDeviceSelReq) )
   vHandleStateResponse(enErrorType);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationMediaMetaData(...
***************************************************************************/
t_Void spi_tclDiPoConnection::vPostApplicationMediaMetaData(
   const trAppMediaMetaData& rfcorApplicationMediaMetaData,
   const trUserContext& rfcorUsrCntxt)
{
	/*lint -esym(40,fvAppMediaMetaData)fvAppMediaMetaData Undeclared identifier */
   //! Forward metadata to Connection Manager
   if (NULL != (m_rDiPoConnCallbacks.fvAppMediaMetaData))
   {
      (m_rDiPoConnCallbacks.fvAppMediaMetaData)(rfcorApplicationMediaMetaData, rfcorUsrCntxt);
   }//if (NULL != (m_rDiPoConnCallbacks.fvAppMediaMetaData))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationPhoneData(...
***************************************************************************/
t_Void spi_tclDiPoConnection::vPostApplicationPhoneData(
   const trAppPhoneData& rfcorApplicationPhoneData,
   const trUserContext& rfcorUsrCntxt)
{
   /*lint -esym(40,fvAppPhoneData)fvAppPhoneData Undeclared identifier */
   //! Forward metadata to Connection Manager
   if (NULL != (m_rDiPoConnCallbacks.fvAppPhoneData))
   {
      (m_rDiPoConnCallbacks.fvAppPhoneData)(rfcorApplicationPhoneData, rfcorUsrCntxt);
   }//if (NULL != (m_rDiPoConnCallbacks.fvAppPhoneData))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationMediaPlaytime(...
***************************************************************************/
t_Void spi_tclDiPoConnection::vPostApplicationMediaPlaytime(
   const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
   const trUserContext& rfcorUsrCntxt)
{
   /*lint -esym(40,fvAppMediaPlaytime)fvAppMediaPlaytime Undeclared identifier */
   //! Forward metadata to Connection Manager
   if (NULL != (m_rDiPoConnCallbacks.fvAppMediaPlaytime))
   {
      (m_rDiPoConnCallbacks.fvAppMediaPlaytime)(rfcorApplicationMediaPlaytime, rfcorUsrCntxt);
   }//if (NULL != (m_rDiPoConnCallbacks.fvAppMediaPlaytime))
}

/***************************************************************************
** FUNCTION:  spi_tclDiPoConnection::vSetRoleSwitchRequestedInfo()
***************************************************************************/
t_Void spi_tclDiPoConnection::vSetRoleSwitchRequestedInfo(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_u32CurrSelectedDevice = cou32DeviceHandle;
}


/***************************************************************************
** FUNCTION:  spi_tclDiPoConnection::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclDiPoConnection::vOnSelectDeviceResult(const t_U32 cou32DeviceHandle,
                                                    const tenDeviceConnectionReq coenConnReq,
                                                    const tenResponseCode coenRespCode,
                                                    tenDeviceCategory enDevCat, t_Bool bIsHMITrigger)
{
	/*lint -esym(40,fvDeviceConnection)fvDeviceConnection Undeclared identifier */

	 SPI_INTENTIONALLY_UNUSED(enDevCat);
	 SPI_INTENTIONALLY_UNUSED(bIsHMITrigger);
   
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_enDeviceSelReq = coenConnReq;
   // If the response is a failure, assume that any of the internal 
   // component is not ready to accept the latest state chanege and handle it accordingly.
   if(e8FAILURE == coenRespCode)
   {
      tenSessionStatus enSessionStatus = e8_SESSION_UNKNOWN; //Dummy value. will not process it
      tenDeviceConnectionStatus enDeviceConnectStatus = e8DEV_NOT_CONNECTED;//Dummy value . will not process
      vHandleDiPOState(cou32DeviceHandle, coenConnReq, enSessionStatus, enDeviceConnectStatus, 
         e8DIPO_TRIGGER_SELECT_DEVICE_RESULT);
   }
}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vOnDeviceConnection
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vOnDeviceConnection(const t_U32 cou32DeviceHandle, const trDeviceInfo &corfrDeviceInfo)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   tenTriggerType enTriggerType = e8DIPO_TRIGGER_NO_SELECT_DEVICE;
   tenDeviceConnectionReq enDevSelReq = e8DEVCONNREQ_SELECT; //Dummy value. will not be processed further
   if(cou32DeviceHandle == m_u32CurrSelectedDevice)
   {
      //! Handle state only if the device is already selected device
      vHandleDiPOState(cou32DeviceHandle, enDevSelReq, corfrDeviceInfo.enSessionStatus, 
         corfrDeviceInfo.enDeviceConnectionStatus, enTriggerType);
   }
   if(NULL != (m_rDiPoConnCallbacks.fvDeviceConnection))
   {
      (m_rDiPoConnCallbacks.fvDeviceConnection)(cou32DeviceHandle, corfrDeviceInfo,e8DEV_TYPE_DIPO);
   }
} 

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vOnDeviceDisconnection
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vOnDeviceDisconnection(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   /*lint -esym(40,fvDeviceDisconnection)fvDeviceDisconnection Undeclared identifier */
   tenTriggerType enTriggerType = e8DIPO_TRIGGER_NO_SELECT_DEVICE;
   tenDeviceConnectionReq enDevSelReq = e8DEVCONNREQ_SELECT; //Dummy value. will not be processed further
   tenDeviceConnectionStatus enDeviceConnectStatus = e8DEV_NOT_CONNECTED;
   tenSessionStatus enSessionStatus = e8_SESSION_INACTIVE; 
   if(m_u32CurrSelectedDevice == cou32DeviceHandle)
   {
      //Handle the state only if the device ID is currently selected device
      vHandleDiPOState(cou32DeviceHandle, enDevSelReq, enSessionStatus, 
         enDeviceConnectStatus, enTriggerType);
   }

   if(NULL != (m_rDiPoConnCallbacks.fvDeviceDisconnection))
   {
      (m_rDiPoConnCallbacks.fvDeviceDisconnection)(cou32DeviceHandle,e8DEV_TYPE_DIPO);
   }//if(NULL != (m_rDiPoConnCallbacks.fvDeviceDisconnection))
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vHandleDiPOState
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vHandleDiPOState(const t_U32 cou32DeviceHandle, 
                        tenDeviceConnectionReq enDevSelReq, 
                        tenSessionStatus enSessionStatus, 
                        tenDeviceConnectionStatus enDeviceConnectionStatus, 
                        tenTriggerType enTriggerType)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   tenDiPOState enDiPOState = m_enCurrDiPOState;
   switch(m_enCurrDiPOState)
   {
   case e8DIPO_NOT_ACTIVE:
      {
         vTriggerNotActiveState(cou32DeviceHandle, enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType);
      }
      break;
   case e8DIPO_ACTIVE:
      {
         vTriggerActiveState(cou32DeviceHandle, enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType);
      }
      break;
   case e8SIPO_SPI_ACTIVATING:
      {
         vTriggerSPIActivatingState(cou32DeviceHandle, enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType);
      }
      break;
   case e8DIPO_HMI_ACTIVATING:
      {
         vTriggerHMIActivatingState(cou32DeviceHandle, enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType);
      }
      break; 
   case e8DIPO_SPI_DEACTIVATING:
      {
         vTriggerSPIDeActivatingState(cou32DeviceHandle, enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType);
      }
      break;
   case e8DIPO_HMI_DEACTIVATING:
      {
        vTriggerHMIDeActivatingState(cou32DeviceHandle, enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType);
      }
      break;
   default:
      ETG_TRACE_USR1((" INVALID DIPO STATE"));
   } //switch(m_rCurrDiPOState)
   if(enDiPOState != m_enCurrDiPOState)
   {
      //Function gets invoked only if a state change
      //! The error code is set to e8NO_ERRORS always, since the role switch failure cases are handled in 
      // vPostSelectDeviceResult()
      vHandleStateResponse(e8NO_ERRORS);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vTriggerActiveState
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vTriggerActiveState(const t_U32 cou32DeviceHandle, 
                    tenDeviceConnectionReq enDevSelReq, 
                    tenSessionStatus enSessionStatus, 
                    tenDeviceConnectionStatus enDeviceConnectionStatus, 
                    tenTriggerType enTriggerType)
{
   ETG_TRACE_USR4(("SELECTION REQ: %d, SESSION STATUS:%d, CONNECTION STATUS:%d, TRIGGER TYPE : %d", 
      enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType));
   switch(enTriggerType)
   {
   case e8DIPO_TRIGGER_SELECT_DEVICE:
      {
         if(NULL != m_poMplayClientHandler)
         {
            m_poMplayClientHandler->bDiPORoleSwitchRequest(cou32DeviceHandle, enDevSelReq);
         }//if(nullptr != m_poMplayClientHandler)
         m_enCurrDiPOState = (e8DEVCONNREQ_SELECT==enDevSelReq) ? m_enCurrDiPOState : e8DIPO_HMI_DEACTIVATING;

      }
      break;
   case e8DIPO_TRIGGER_NO_SELECT_DEVICE:
      {
         if((e8_SESSION_ACTIVE != enSessionStatus) && (e8DEV_CONNECTED != enDeviceConnectionStatus))
         {
            m_enCurrDiPOState = e8DIPO_SPI_DEACTIVATING;
         }
      }
      break;
   } //switch(rTriggerType)
   ETG_TRACE_USR1(("vTriggerActiveState; returned with state: %d", ETG_ENUM(DIPO_STATE,m_enCurrDiPOState)));
} 


/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vTriggerNotActiveState
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vTriggerNotActiveState(const t_U32 cou32DeviceHandle, 
                                                     tenDeviceConnectionReq enDevSelReq, 
                                                     tenSessionStatus enSessionStatus, 
                                                     tenDeviceConnectionStatus enDeviceConnectionStatus, 
                                                     tenTriggerType enTriggerType)
{
  ETG_TRACE_USR4(("SELECTION REQ: %d, SESSION STATUS:%d, CONNECTION STATUS:%d, TRIGGER TYPE : %d", 
     enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType));
  switch(enTriggerType)
  {
  case e8DIPO_TRIGGER_SELECT_DEVICE:
     {
        if((NULL != m_poMplayClientHandler) && ((e8USAGE_ENABLED == m_enCarplaySettingStatus) || (e8USAGE_CONF_REQD == m_enCarplaySettingStatus)))
        {
           // Roleswitch request only if the projection status is true.
           m_poMplayClientHandler->bDiPORoleSwitchRequest(cou32DeviceHandle, enDevSelReq);
           m_enCurrDiPOState = (e8DEVCONNREQ_SELECT==enDevSelReq) ? e8DIPO_HMI_ACTIVATING : e8DIPO_NOT_ACTIVE;

        }//if(nullptr != m_poMplayClientHandler)      
     }
     break;
  case e8DIPO_TRIGGER_NO_SELECT_DEVICE:
     {
        if((e8_SESSION_ACTIVE == enSessionStatus) && (e8DEV_CONNECTED == enDeviceConnectionStatus))
        {
            m_enCurrDiPOState = e8SIPO_SPI_ACTIVATING;
        }
     }
     break;

  }//switch(rTriggerType)
  ETG_TRACE_USR1(("vTriggerNotActiveState; returned with state: %d", ETG_ENUM(DIPO_STATE,m_enCurrDiPOState)));
}


/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vTriggerSPIActivatingState
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vTriggerSPIActivatingState(const t_U32 cou32DeviceHandle, 
                                                         tenDeviceConnectionReq enDevSelReq, 
                                                         tenSessionStatus enSessionStatus, 
                                                         tenDeviceConnectionStatus enDeviceConnectionStatus, 
                                                         tenTriggerType enTriggerType)
{
   ETG_TRACE_USR4(("SELECTION REQ: %d, SESSION STATUS:%d, CONNECTION STATUS:%d, TRIGGER TYPE : %d", 
      enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType));
   switch(enTriggerType)
   {
   case e8DIPO_TRIGGER_SELECT_DEVICE:
      {        
         m_enCurrDiPOState = (e8DEVCONNREQ_SELECT == enDevSelReq) ? e8DIPO_ACTIVE : e8DIPO_NOT_ACTIVE;
         if((e8USAGE_DISABLED == m_enCarplaySettingStatus) && (NULL != m_poMplayClientHandler))
         {
            // In the case of automatic roleswitch from mediaplayer on reconnection, the roleswitch could be completed 
            // and it reaches in this state. So If the projection is disabled in the target, there must be reverse 
            // roleswitch to contineue the device in normal iAP2 mode.
            m_poMplayClientHandler->bDiPORoleSwitchRequest(cou32DeviceHandle, e8DEVCONNREQ_DESELECT);
            m_enCurrDiPOState = e8DIPO_HMI_DEACTIVATING;
         }
      }
      break;
   case e8DIPO_TRIGGER_SELECT_DEVICE_RESULT:
      {
         if(NULL != m_poMplayClientHandler)
         {
            m_poMplayClientHandler->bDiPORoleSwitchRequest(cou32DeviceHandle, e8DEVCONNREQ_DESELECT);
         }//if(nullptr != m_poMplayClientHandler)
         m_enCurrDiPOState = e8DIPO_HMI_DEACTIVATING;
      }
      break;
   }//switch(rTriggerType)
   ETG_TRACE_USR1(("vTriggerSPIActivatingState; returned with state: %d", ETG_ENUM(DIPO_STATE,m_enCurrDiPOState)));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vTriggerHMIActivatingState
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vTriggerHMIActivatingState(const t_U32 cou32DeviceHandle, 
                                                         tenDeviceConnectionReq enDevSelReq, 
                                                         tenSessionStatus enSessionStatus, 
                                                         tenDeviceConnectionStatus enDeviceConnectionStatus, 
                                                         tenTriggerType enTriggerType)
{

	SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR4(("SELECTION REQ: %d, SESSION STATUS:%d, CONNECTION STATUS:%d, TRIGGER TYPE : %d", 
      enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType));
   switch(enTriggerType)
   {
   case e8DIPO_TRIGGER_SELECT_DEVICE:
      {
        
      }
      break;
   case e8DIPO_TRIGGER_NO_SELECT_DEVICE:
      {
         if((e8_SESSION_ACTIVE == enSessionStatus) && (e8DEV_CONNECTED == enDeviceConnectionStatus))
         {
            m_enCurrDiPOState = e8DIPO_ACTIVE;
         }
         else if(e8DEV_CONNECTED != enDeviceConnectionStatus)
         {
            m_enCurrDiPOState = e8DIPO_NOT_ACTIVE;
         }
      }//case e8DIPO_TRIGGER_NO_SELECT_DEVICE:
      break;
   } // switch(rTriggerType)
   ETG_TRACE_USR1(("vTriggerHMIActivatingState; returned with state: %d", ETG_ENUM(DIPO_STATE,m_enCurrDiPOState)));
}


/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vTriggerSPIDeActivatingState
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vTriggerSPIDeActivatingState(const t_U32 cou32DeviceHandle, 
                                                           tenDeviceConnectionReq enDevSelReq, 
                                                           tenSessionStatus enSessionStatus, 
                                                           tenDeviceConnectionStatus enDeviceConnectionStatus, 
                                                           tenTriggerType enTriggerType)
{
	SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   ETG_TRACE_USR4(("SELECTION REQ: %d, SESSION STATUS:%d, CONNECTION STATUS:%d, TRIGGER TYPE : %d", 
      enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType));
   switch(enTriggerType)
   {
   case e8DIPO_TRIGGER_SELECT_DEVICE:
      {
         //! state is moved to not active state irrespective of the selection request type.
         //  This is used to handle the timimg issues in device selection. 
         //  At this stage state machine is expected to go back to not active state.
         //  Change added for GMMY16-21958.
         m_enCurrDiPOState = e8DIPO_NOT_ACTIVE;
      }
      break;
   case e8DIPO_TRIGGER_NO_SELECT_DEVICE:
      {
         //Nothing to do
      }
      break;
   } //switch(enTriggerType)
   ETG_TRACE_USR1(("vTriggerSPIDeActivatingState; returned with state: %d", ETG_ENUM(DIPO_STATE,m_enCurrDiPOState)));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vTriggerHMIDeActivatingState
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vTriggerHMIDeActivatingState(const t_U32 cou32DeviceHandle, 
                                    tenDeviceConnectionReq enDevSelReq, 
                                    tenSessionStatus enSessionStatus, 
                                    tenDeviceConnectionStatus enDeviceConnectionStatus, 
                                    tenTriggerType enTriggerType)
{
SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   ETG_TRACE_USR4(("SELECTION REQ: %d, SESSION STATUS:%d, CONNECTION STATUS:%d, TRIGGER TYPE : %d", 
      enDevSelReq, enSessionStatus, enDeviceConnectionStatus, enTriggerType));
   switch(enTriggerType)
   {
   case e8DIPO_TRIGGER_SELECT_DEVICE:
      {
      }
      break;
   case e8DIPO_TRIGGER_NO_SELECT_DEVICE:
      {
         if((e8_SESSION_ACTIVE != enSessionStatus) || (e8DEV_CONNECTED != enDeviceConnectionStatus))
         {
            m_enCurrDiPOState = e8DIPO_NOT_ACTIVE;
         }
      }
      break;
   } //switch(enTriggerType)
   ETG_TRACE_USR1(("vTriggerHMIDeActivatingState; returned with state: %d", ETG_ENUM(DIPO_STATE,m_enCurrDiPOState)));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoConnection::vHandleStateResponse
 ***************************************************************************/
t_Void spi_tclDiPoConnection::vHandleStateResponse(tenErrorCode enErrorCode)
{

	/*lint -esym(40,fvSelectDeviceResult)fvSelectDeviceResult Undeclared identifier */
   ETG_TRACE_USR1(("vHandleStateResponse with state: %d, Error Code : %d", ETG_ENUM(DIPO_STATE,m_enCurrDiPOState), enErrorCode));
   switch(m_enCurrDiPOState)
   {
   case e8DIPO_ACTIVE:
      {
		#ifndef VARIANT_S_FTR_ENABLE_GM
			#ifndef VARIANT_S_FTR_ENABLE_PSA
				system("ip link set usb0 up");
			#endif
		#endif
         tenErrorCode enSelErrorCode = (e8DEVCONNREQ_SELECT == m_enDeviceSelReq) ? e8NO_ERRORS : enErrorCode;
         if(NULL != (m_rDiPoConnCallbacks.fvSelectDeviceResult))
         {
            (m_rDiPoConnCallbacks.fvSelectDeviceResult)(enSelErrorCode);
         }
         if(NULL != m_poMplayClientHandler)
         {
        	 m_poMplayClientHandler->vRegisterForMetadataProperties();
         }
      }
      break;
   case e8DIPO_NOT_ACTIVE:
      {
         tenErrorCode enSelErrorCode = (e8DEVCONNREQ_SELECT != m_enDeviceSelReq)? e8NO_ERRORS : enErrorCode;
         if(NULL != (m_rDiPoConnCallbacks.fvSelectDeviceResult))
         {
            (m_rDiPoConnCallbacks.fvSelectDeviceResult)(enSelErrorCode);
         }     
         if(NULL != m_poMplayClientHandler)
         {
        	 m_poMplayClientHandler->vUnregisterForMetadataProperties();
         }
      }
      break;
   }//switch(m_rCurrDiPOState)  
}
//lint –restore
