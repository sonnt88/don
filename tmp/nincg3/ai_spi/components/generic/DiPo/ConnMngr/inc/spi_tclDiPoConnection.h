/*!
 *******************************************************************************
 * \file             spi_tclDiPoConnection.h
 * \brief            DiPo Connection class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    DiPo Connection class to handle ios devices capable of DiPo
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.01.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 13.02.2014 |  Shihabudheen P M            | added functions
                                             1.vOnDiPODeviceConnect()
                                             2.vOnDiPODeviceDisConnect()
 05.11.2014 |  Ramya Murthy                | Added response for Application metadata.

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLDIPOCONNECTION_H_
#define SPI_TCLDIPOCONNECTION_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "DiPOTypes.h"
#include "spi_tclConnection.h"
#include "spi_tclDiPOConnectionIntf.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

typedef enum
{
   //! DiPO activated.
   e8DIPO_ACTIVE = 0,

   //! Dipo not active
   e8DIPO_NOT_ACTIVE = 1,

   //! DiPO Activating automatically(Automatic device selection)
   e8SIPO_SPI_ACTIVATING = 2,

   //! DiPO activating by the user
   e8DIPO_HMI_ACTIVATING = 3,

   //! DiPO deactivating by SPI internally
   e8DIPO_SPI_DEACTIVATING = 4,

   //! DiPO deactivating by HMI internally
   e8DIPO_HMI_DEACTIVATING = 5
}tenDiPOState;


typedef enum 
{
   //! trigger is the select device response
   e8DIPO_TRIGGER_SELECT_DEVICE_RESULT = 0,

   //! Trigger is the select device
   e8DIPO_TRIGGER_SELECT_DEVICE = 1,

   //! Trigger other than select device
   e8DIPO_TRIGGER_NO_SELECT_DEVICE = 2
}tenTriggerType;

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
class spi_tclMPlayClientHandler;

/*!
 * \class spi_tclDiPoConnection
 * \brief DiPo Connection class to handle ios devices capable of DiPo
 */

class spi_tclDiPoConnection: public spi_tclConnection, //! Base Connection Class
   public spi_tclDiPOConnectionIntf  //!DiPO connection manager interface.
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::spi_tclDiPoConnection
       ***************************************************************************/
      /*!
       * \fn     spi_tclDiPoConnection()
       * \brief  Default Constructor
       * \sa      ~spi_tclDiPoConnection()
       **************************************************************************/
      spi_tclDiPoConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::~spi_tclDiPoConnection
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclDiPoConnection()
       * \brief  Destructor
       * \sa     spi_tclDiPoConnection()
       **************************************************************************/
      ~spi_tclDiPoConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::bInitializeConnection
       ***************************************************************************/
      /*!
       * \fn     bInitializeConnection()
       * \brief  Initialization of device detection and
       *         any other required initializations
       **************************************************************************/
      t_Bool bInitializeConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::vOnSaveSettings
       ***************************************************************************/
      /*!
       * \fn     vOnSaveSettings()
       * \brief  Called on savesettings
       *         Optional interface
       * \retval none
       **************************************************************************/
      virtual t_Void vOnSaveSettings();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::bStartDeviceDetection
       ***************************************************************************/
      /*!
       * \fn     bStartDeviceDetection()
       * \brief  Starts device detection
       * \retval returns true on successful initialization and false on failure
       **************************************************************************/
       t_Bool bStartDeviceDetection();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::vUnInitializeConnection
       ***************************************************************************/
      /*!
       * \fn     vUnInitializeConnection()
       * \brief  Uninitialization of sdk's etc
       **************************************************************************/
      t_Void vUnInitializeConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::vOnAddDevicetoList
       ***************************************************************************/
      /*!
       * \fn     vOnAddDevicetoList()
       * \brief  To be called when a new device has to be added to device list
       * \param  cou32DeviceHandle: Device handle of the device to be added to
       *         the device list
       **************************************************************************/
      t_Void vOnAddDevicetoList(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::bSetDevProjUsage
       ***************************************************************************/
      /*!
       * \fn     bSetDevProjUsage()
       * \brief  Called when the SPI featured is turned ON or OFF by the user
       **************************************************************************/
      t_Bool bSetDevProjUsage(tenEnabledInfo enServiceStatus);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  interface for the creator class to register for the required
       *        callbacks.
       * \param rfrConnCallbacks : reference to the callback structure
       *        populated by the caller
       **************************************************************************/
      t_Void vRegisterCallbacks(trConnCallbacks &ConnCallbacks);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclDiPoConnection::vOnDiPODeviceConnect()
      ***************************************************************************/
      /*!
      * \fn     vOnDiPODeviceConnect()
      * \brief  Interface for the mediaplayer clinet hander to report a device 
      *         connection.
      * \param corfrDeviceInfo : [IN] connected device info
      **************************************************************************/
      virtual t_Void vOnDiPODeviceConnect(const trDeviceInfo &corfrDeviceInfo);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclDiPoConnection::vOnDiPODeviceDisConnect()
      ***************************************************************************/
      /*!
      * \fn     vOnDiPODeviceDisConnect()
      * \brief  interface for the mediaplyer to report a DiPO device disconnection.
      * \param  corfrDeviceInfo : [IN] disconnecetd device info
      **************************************************************************/
      virtual t_Void vOnDiPODeviceDisConnect(const t_U32 u32DeviceHandle);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclDiPoConnection::vOnSelectDevice()
      ***************************************************************************/
      /*!
      * \fn     t_Void vOnSelectDevice()
      * \brief  Function to get the deviec selection.
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevConnReq : [IN] Selection status
      **************************************************************************/
      virtual t_Void vOnSelectDevice(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceConnectionReq enDevSelReq, tenEnabledInfo enDAPUsage,
               tenEnabledInfo enCDBUsage, t_Bool bIsHMITrigger,
               const trUserContext corUsrCntxt);

     /***************************************************************************
      ** FUNCTION:  t_Void spi_tclDiPoConnection::vPostSelctDeviceResult()
      ***************************************************************************/
      /*!
      * \fn     t_Void vPostSelctDeviceResult()
      * \brief  Function to post the device selection statu.
      * \param  enResponse : [IN] Operation status.
      * \param  enErrorType : [IN] error value.
      **************************************************************************/
      virtual t_Void vPostSelectDeviceResult(tenResponseCode enResponse, 
         tenErrorCode enErrorType);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationMediaMetaData(t_U32& ...
      ***************************************************************************/
      /*!
      * \fn     vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationMediaMetaData : Contains the media metadata information
      *              related to an application.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      virtual t_Void vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
         const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationPhoneData(...
      ***************************************************************************/
      /*!
      * \fn     vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationPhoneData : Contains the phone related information.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      virtual t_Void vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
         const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationMediaPlaytime(...
      ***************************************************************************/
      /*!
      * \fn     vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationMediaPlaytime : Contains the media play time of current playing track.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      virtual t_Void vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
         const trUserContext& rfcorUsrCntxt);

     /***************************************************************************
      ** FUNCTION:  t_Void spi_tclDiPoConnection::vSetRoleSwitchRequestedInfo()
      ***************************************************************************/
      /*!
      * \fn     t_Void vSetRoleSwitchRequestedInfo()
      * \brief  Function to post the device selection statu.
      * \param  cou32DeviceHandle : [IN] Roleswitch requested device ID
      * \retVal NONE
      **************************************************************************/
      virtual t_Void vSetRoleSwitchRequestedInfo(const t_U32 cou32DeviceHandle);

     /***************************************************************************
      ** FUNCTION:  t_Void spi_tclDiPoConnection::vOnSelectDeviceResult()
      ***************************************************************************/
      /*!
      * \fn      t_Void vOnSelectDeviceResult
      * \brief   To perform the actions that are required, after the select device is
      *           successful/failed
      * \pram    cou32DeviceHandle  : [IN] Uniquely identifies the target Device.
      * \pram    enDevSelReq : [IN] Identifies the Connection Request.
      * \pram    coenRespCode: [IN] Response code. Success/Failure
      * \pram    enDevCat    : [IN] Device Category. ML/DiPo
      * \retval  t_Void
      **************************************************************************/
      t_Void vOnSelectDeviceResult(const t_U32 cou32DeviceHandle,
         const tenDeviceConnectionReq coenConnReq,
         const tenResponseCode coenRespCode,
         tenDeviceCategory enDevCat, t_Bool bIsHMITrigger);

   private:
      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      //! Callbacks for ConnMngr to register. These callbacks will be used to
      //! inform device detection and device disconnection to connection manager
      trConnCallbacks m_rDiPoConnCallbacks;

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::vOnDeviceConnection
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceConnection()
       * \brief
       **************************************************************************/
      t_Void vOnDeviceConnection(const t_U32 cou32DeviceHandle,
               const trDeviceInfo &corfrDeviceInfo);

      /***************************************************************************
      ** FUNCTION:  spi_tclDiPoConnection::vHandleDiPOState
      ***************************************************************************/
      /*!
      * \fn     vHandleDiPOState()
      * \brief  Function which handle the state transition of DiPO
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevSelReq : [IN] Selection request type
      * \param  enSessionStatus : [IN] Session ststus info.(DiPO Capable or not)
      * \param  enDeviceConnectionStatus : [IN] Connection status.
      * \param  enTriggerType : [IN] Type of the trigger for state handling.
      **************************************************************************/
      t_Void vHandleDiPOState(const t_U32 cou32DeviceHandle, 
         tenDeviceConnectionReq enDevSelReq, 
         tenSessionStatus enSessionStatus, 
         tenDeviceConnectionStatus enDeviceConnectionStatus, 
         tenTriggerType enTriggerType);

      /***************************************************************************
      ** FUNCTION:  spi_tclDiPoConnection::vTriggerActiveState
      ***************************************************************************/
      /*!
      * \fn     vHandleDiPOState()
      * \brief  Function which handle the state transition of DiPO
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevSelReq : [IN] Selection request type
      * \param  enSessionStatus : [IN] Session ststus info.(DiPO Capable or not)
      * \param  enDeviceConnectionStatus : [IN] Connection status.
      * \param  enTriggerType : [IN] Type of the trigger for state handling.
      **************************************************************************/
      t_Void vTriggerActiveState(const t_U32 cou32DeviceHandle, 
         tenDeviceConnectionReq enDevSelReq, 
         tenSessionStatus enSessionStatus, 
         tenDeviceConnectionStatus enDeviceConnectionStatus, 
         tenTriggerType enTriggerType);

      /***************************************************************************
      ** FUNCTION:  spi_tclDiPoConnection::vTriggerNotActiveState
      ***************************************************************************/
      /*!
      * \fn     vTriggerNotActiveState()
      * \brief  Function which handle the state transition of DiPO
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevSelReq : [IN] Selection request type
      * \param  enSessionStatus : [IN] Session ststus info.(DiPO Capable or not)
      * \param  enDeviceConnectionStatus : [IN] Connection status.
      * \param  enTriggerType : [IN] Type of the trigger for state handling.
      **************************************************************************/
      t_Void vTriggerNotActiveState(const t_U32 cou32DeviceHandle, 
         tenDeviceConnectionReq enDevSelReq, 
         tenSessionStatus enSessionStatus, 
         tenDeviceConnectionStatus enDeviceConnectionStatus, 
         tenTriggerType enTriggerType);

      /***************************************************************************
      ** FUNCTION:  spi_tclDiPoConnection::vTriggerSPIActivatingState
      ***************************************************************************/
      /*!
      * \fn     vTriggerSPIActivatingState()
      * \brief  Function which handle the state transition of DiPO
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevSelReq : [IN] Selection request type
      * \param  enSessionStatus : [IN] Session ststus info.(DiPO Capable or not)
      * \param  enDeviceConnectionStatus : [IN] Connection status.
      * \param  enTriggerType : [IN] Type of the trigger for state handling.
      **************************************************************************/
      t_Void vTriggerSPIActivatingState(const t_U32 cou32DeviceHandle, 
         tenDeviceConnectionReq enDevSelReq, 
         tenSessionStatus enSessionStatus, 
         tenDeviceConnectionStatus enDeviceConnectionStatus, 
         tenTriggerType enTriggerType);

      /***************************************************************************
      ** FUNCTION:  spi_tclDiPoConnection::vTriggerHMIActivatingState
      ***************************************************************************/
      /*!
      * \fn     vTriggerHMIActivatingState()
      * \brief  Function which handle the state transition of DiPO
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevSelReq : [IN] Selection request type
      * \param  enSessionStatus : [IN] Session ststus info.(DiPO Capable or not)
      * \param  enDeviceConnectionStatus : [IN] Connection status.
      * \param  enTriggerType : [IN] Type of the trigger for state handling.
      **************************************************************************/
      t_Void vTriggerHMIActivatingState(const t_U32 cou32DeviceHandle, 
         tenDeviceConnectionReq enDevSelReq, 
         tenSessionStatus enSessionStatus, 
         tenDeviceConnectionStatus enDeviceConnectionStatus, 
         tenTriggerType enTriggerType);

     /***************************************************************************
      ** FUNCTION:  spi_tclDiPoConnection::vTriggerSPIDeActivatingState
      ***************************************************************************/
      /*!
      * \fn     vTriggerSPIDeActivatingState()
      * \brief  Function which handle the state transition of DiPO
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevSelReq : [IN] Selection request type
      * \param  enSessionStatus : [IN] Session ststus info.(DiPO Capable or not)
      * \param  enDeviceConnectionStatus : [IN] Connection status.
      * \param  enTriggerType : [IN] Type of the trigger for state handling.
      **************************************************************************/
      t_Void vTriggerSPIDeActivatingState(const t_U32 cou32DeviceHandle, 
         tenDeviceConnectionReq enDevSelReq, 
         tenSessionStatus enSessionStatus, 
         tenDeviceConnectionStatus enDeviceConnectionStatus, 
         tenTriggerType enTriggerType);

     /***************************************************************************
      ** FUNCTION:  spi_tclDiPoConnection::vTriggerHMIDeActivatingState
      ***************************************************************************/
      /*!
      * \fn     vTriggerHMIDeActivatingState()
      * \brief  Function which handle the state transition of DiPO
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevSelReq : [IN] Selection request type
      * \param  enSessionStatus : [IN] Session ststus info.(DiPO Capable or not)
      * \param  enDeviceConnectionStatus : [IN] Connection status.
      * \param  enTriggerType : [IN] Type of the trigger for state handling.
      **************************************************************************/
      t_Void vTriggerHMIDeActivatingState(const t_U32 cou32DeviceHandle, 
         tenDeviceConnectionReq enDevSelReq, 
         tenSessionStatus enSessionStatus, 
         tenDeviceConnectionStatus enDeviceConnectionStatus, 
         tenTriggerType enTriggerType);

     /***************************************************************************
      ** FUNCTION:  spi_tclDiPoConnection::vHandleStateResponse
      ***************************************************************************/
      /*!
      * \fn     vHandleStateResponse()
      * \brief  Function which handle the state entry
      * \param  enErrorCode : [IN] Error Code of the operation.
      **************************************************************************/
      t_Void vHandleStateResponse(tenErrorCode enErrorCode);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoConnection::vOnDeviceDisconnection
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceDisconnection()
       * \brief
       **************************************************************************/
      t_Void vOnDeviceDisconnection(const t_U32 cou32DeviceHandle);

      //! Media player client handler
      spi_tclMPlayClientHandler* m_poMplayClientHandler;

      //! Current DiPO state
      tenDiPOState m_enCurrDiPOState;

      //! Device selection status
      tenDeviceConnectionReq m_enDeviceSelReq; 

      //! Current selected device
      t_U32 m_u32CurrSelectedDevice; 

      //! Status of carplay service status.
      tenEnabledInfo m_enCarplaySettingStatus;
};
#endif // SPI_TCLDIPOCONNECTION_H_
