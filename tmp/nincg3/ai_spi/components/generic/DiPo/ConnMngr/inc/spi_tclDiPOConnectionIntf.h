/*!
 *******************************************************************************
 * \file             spi_tclDiPOConnectionIntf.h
 * \brief            DiPo Connection interface 
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    DiPo Connection manager interface class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 12.02.2013 |  Shihabudheen P M            | Initial Version
 05.11.2014 |  Ramya Murthy                | Added response for Application metadata.

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#ifndef SPI_TCLDIPOCONNECTIONINTF_H
#define SPI_TCLDIPOCONNECTIONINTF_H

#include "SPITypes.h"
#include "BaseTypes.h"

/*!
 * \class spi_tclDiPOConnectionIntf
 * \brief DiPo connection interface class for handling device connections.
 */
class spi_tclDiPOConnectionIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION: spi_tclDiPOConnectionIntf::~spi_tclDiPOConnectionIntf()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOConnectionIntf()
   * \brief  Constructor
   * \sa     
   **************************************************************************/
   spi_tclDiPOConnectionIntf() {};

   /***************************************************************************
   ** FUNCTION: spi_tclDiPOConnectionIntf::~spi_tclDiPOConnectionIntf()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOConnectionIntf()
   * \brief  Destructor
   * \sa     
   **************************************************************************/
   virtual ~spi_tclDiPOConnectionIntf() {};

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOConnectionIntf::vOnDiPODeviceConnect()...
   ***************************************************************************/
   /*!
   * \fn     vOnDiPODeviceConnect(const trDeviceInfo &corfrDeviceInfo)
   * \brief  DiPO capable device connection.
   * \param  corfrDeviceInfo : [IN] Connected device info.
   * \sa     
   **************************************************************************/
   virtual t_Void vOnDiPODeviceConnect(const trDeviceInfo &corfrDeviceInfo) = 0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOConnectionIntf::vOnDiPODeviceDisConnect()...
   ***************************************************************************/
   /*!
   * \fn     vOnDiPODeviceDisConnect(const trDeviceInfo &corfrDeviceInfo)
   * \brief  DiPO capable device disconnection.
   * \param  corfrDeviceInfo : [IN] disconnected device info.
   * \sa     
   **************************************************************************/
   virtual t_Void vOnDiPODeviceDisConnect(const t_U32 u32DeviceHandle) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOConnectionIntf::vPostSelctDeviceResult()
   ***************************************************************************/
   /*!
   * \fn     t_Void vPostSelctDeviceResult()
   * \brief  Function to post the device selection statu.
   * \param  enResponse : [IN] Operation status.
   * \param  enErrorType : [IN] error value.
   **************************************************************************/
   virtual t_Void vPostSelectDeviceResult(tenResponseCode enResponse, 
      tenErrorCode enErrorType) = 0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationMediaMetaData(...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationMediaMetaData : Contains the media metadata information
   *              related to an application.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
      const trUserContext& rfcorUsrCntxt) =0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationPhoneData(...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationPhoneData : Contains the phone related information.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
      const trUserContext& rfcorUsrCntxt) =0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPoConnection::vPostApplicationMediaPlaytime(...
   ***************************************************************************/
   /*!
   * \fn     vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
   *            const trUserContext& rfcorUsrCntxt)
   * \brief  Interface to notify application media metadata to the client.
   * \param  [IN] rfcorApplicationMediaPlaytime : Contains the media play time of current playing track.
   * \param  [IN] rfcorUsrCntxt    : User Context Details.
   * \sa
   **************************************************************************/
   virtual t_Void vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
      const trUserContext& rfcorUsrCntxt) =0;

   /***************************************************************************
   *********************************END OFPUBLIC*******************************
   ***************************************************************************/

private:
  /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/


  /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

};
#endif
