/***********************************************************************/
/*!
* \file  spi_tclDiPOTraceCommands.cpp
* \brief DiPo Trace Commands
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPo trace commands
AUTHOR:         Shihabudheen P M
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
05.02.2014  | Shihabudheen P M      | Initial Version
27.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors


\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "SPITypes.h"
#include "DiPOTypes.h"
#include "spi_tclDiPOTraceCommands.h"
#include "IPCMessageQueue.h"


#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOTraceCommands.cpp.trc.h"
#endif 

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 ** FUNCTION:  spi_tclDiPOTraceCommands::spi_tclDiPOTraceCommands()
 ***************************************************************************/
spi_tclDiPOTraceCommands::spi_tclDiPOTraceCommands()
{
   ETG_TRACE_USR1(("spi_tclDiPOTraceCommands() \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOTraceCommands::spi_tclDiPOTraceCommands()
 ***************************************************************************/
spi_tclDiPOTraceCommands::~spi_tclDiPOTraceCommands()
{
   ETG_TRACE_USR1(("~spi_tclDiPOTraceCommands() \n"));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOTraceCommands::vDiPOExecCommands()
 ***************************************************************************/
t_Void spi_tclDiPOTraceCommands::vDiPOExecCommands() const
{
   ETG_TRACE_FATAL(("vDiPOExecCommands() \n"));
   // start the Dipo
   system("export XDG_RUNTIME_DIR=/tmp");
   system("export LM_PLUGIN_PATH=/usr/lib/layermanager");
   system("export LD_LIBRARY_PATH=/usr/lib/:$LM_PLUGIN_PATH:/usr/lib/gstreamer-0.10/");
   system("sleep(3)");

   system("ulimit �q unlimited");
   system("sleep(3)");
   // running the bonjure service
   system("start-stop-daemon -S -x mdnsd");
   system("sleep(3)");
   //// Running the dipo service
   system("dipo-service");  

}

#if 0
/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOTraceCommands::vSetDiPOLayerVisibility()
 ***************************************************************************/
t_Void spi_tclDiPOTraceCommands::vSetDiPOLayerVisibility(t_U32 u32LayerID) const
{
   ETG_TRACE_FATAL(("vSetDiPOLayerVisibility() \n"));
   char myname[] = "DiPOMessageTest";
   IPCMessageQueue* poMessageQueue = new IPCMessageQueue("/SPIDiPOMsgQueue");
   if (NULL != poMessageQueue)
   {
      void *buf = poMessageQueue->vpCreateBuffer(20);
      memcpy(buf, myname, strlen(myname) + 1);

      if (0 <= poMessageQueue->iSendBuffer(buf, 0, (eMessageType) u32LayerID))
      {
         ETG_TRACE_FATAL(("vSetDiPOLayerVisibility() bu send success "));
      }
      else
      {
         ETG_TRACE_FATAL(("vSetDiPOLayerVisibility() bu send fail "));
      }
   }
   else
   {
      ETG_TRACE_FATAL(("vSetDiPOLayerVisibility() poMessageQueue is null"));
   }
}
#endif


/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOTraceCommands::vResetDiPOLayerVisibility()
 ***************************************************************************/
t_Void spi_tclDiPOTraceCommands::vResetDiPOLayerVisibility(t_U32 u32LayerID) const
{
   ETG_TRACE_USR1(("vResetDiPOLayerVisibility() \n"));
   char buf[256];
   sprintf(buf, "LayerManagerControl set layer %d visibility 0", u32LayerID);
   system(buf);
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOTraceCommands::vInitializeDiPOAudio()
 ***************************************************************************/
t_Void spi_tclDiPOTraceCommands::vInitializeDiPOAudio() const
{
   ETG_TRACE_USR1(("vResetDiPOLayerVisibility() \n"));
}
//lint �restore