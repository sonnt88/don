/***********************************************************************/
/*!
* \file  spi_tclDiPOTraceCommands.h
* \brief DiPo Trace Commands
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPo trace commands
AUTHOR:         Shihabudheen P M
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
05.02.2014  | Shihabudheen P M      | Initial Version


\endverbatim
*************************************************************************/
#ifndef SPI_TCLDIPOTRACECOMMANDS_H
#define SPI_TCLDIPOTRACECOMMANDS_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include <stdio.h>

/****************************************************************************/
/*!
* \class spi_tclDiPOTraceCommands
* \brief DiPO Trace Commands
*
* spi_tclDiPOTraceCommands is used to implement the DiPO trace commands.
****************************************************************************/
class spi_tclDiPOTraceCommands
{
public:

  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOTraceCommands::~spi_tclDiPOTraceCommands()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOTraceCommands()
   * \brief  Constructor
   * \sa     ~spi_tclDiPOTraceCommands()
   **************************************************************************/
   spi_tclDiPOTraceCommands();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOTraceCommands::~spi_tclDiPOTraceCommands()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOTraceCommands()
   * \brief  Destructor
   * \sa     spi_tclDiPOTraceCommands()
   **************************************************************************/
   virtual ~spi_tclDiPOTraceCommands();

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOTraceCommands::vDiPOExecCommands()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDiPOExecCommands()
   * \brief   Method to run DiPO video through TTFIS.
   * 
   **************************************************************************/
   t_Void vDiPOExecCommands() const;

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOTraceCommands::vSetDiPOLayerVisibility()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetDiPOLayerVisibility()
   * \brief   Method to set layer visibility through TTFIS.
   * \param   u32LayerID : [IN] Layer ID
   * 
   **************************************************************************/
   //t_Void vSetDiPOLayerVisibility(t_U32 u32LayerID) const;

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOTraceCommands::vResetDiPOLayerVisibility()
   ***************************************************************************/
   /*!
   * \fn      t_Void vResetDiPOLayerVisibility()
   * \brief   Method to set layer visibility through TTFIS.
   * \param   u32LayerID : [IN] Layer ID
   * 
   **************************************************************************/
   t_Void vResetDiPOLayerVisibility(t_U32 u32LayerID) const;

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOTraceCommands::vInitializeDiPOAudio()
   ***************************************************************************/
   /*!
   * \fn      t_Void vInitializeDiPOAudio()
   * \brief   Method to test DiPO audio
   * 
   **************************************************************************/
   t_Void vInitializeDiPOAudio() const;

   /*************************************************************************
   ****************************END OF PUBLIC*********************************
   *************************************************************************/
};


#endif

