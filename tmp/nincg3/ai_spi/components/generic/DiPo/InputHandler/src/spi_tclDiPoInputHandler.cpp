/*!
 *******************************************************************************
 * \file              spi_tclDiPoInputHandler.cpp
 * \brief             SPI input handler for DiPo devices
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   provides input handler to send input events from DiPo client to DiPo server
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.11.2013 |  Hari Priya E R              | Initial Version
 10.03.2014 |  Hari Priya E R              | Touch handling Implementation
 04.04.2014 |  Hari Priya E R              | Key handling Implementation
 03.07.2014 |  Hari Priya E R              | Added changes related to Input Response interface
 26.05.2015 |  Tejaswini H B(RBEI/ECP2)    | Added Lint comments to suppress C++11 Errors
 15.07.2015 |  Sameer Chandra              | Added Knob key Support Implementation

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "Trace.h"
#include "IPCMessageQueue.h"

#include "spi_tclInputRespIntf.h"
#include "spi_tclDiPoInputHandler.h"

#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_INPUTHANDLER
#include "trcGenProj/Header/spi_tclDiPoInputHandler.cpp.trc.h"
#endif
#endif




//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	

/***************************************************************************
** FUNCTION:  spi_tclDiPoInputHandler::spi_tclDiPoInputHandler()
***************************************************************************/
spi_tclDiPoInputHandler::spi_tclDiPoInputHandler(spi_tclInputRespIntf* poInputRespIntf)
{
   SPI_INTENTIONALLY_UNUSED(poInputRespIntf);
   ETG_TRACE_USR1(("spi_tclDiPoInputHandler::spi_tclDiPoInputHandler entered \n"));
}

/***************************************************************************
** FUNCTION:  spi_tclDiPoInputHandler::~spi_tclDiPoInputHandler()
***************************************************************************/
spi_tclDiPoInputHandler::~spi_tclDiPoInputHandler()
{
   ETG_TRACE_USR1(("spi_tclDiPoInputHandler::~spi_tclDiPoInputHandler entered \n"));


}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoInputHandler::vProcessTouchEvent
***************************************************************************/
t_Void spi_tclDiPoInputHandler::vProcessTouchEvent(
   t_U32 u32DeviceHandle,trTouchData &rfrTouchData)const
{

   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   std::vector<trTouchInfo>::iterator itTouchInfo;
   std::vector<trTouchCoordinates> vecTouchCoord;
   std::vector<trTouchCoordinates>::iterator itTouchCoord;

   t_U8 u8TouchMode = 0;
   t_Bool bMsgSendStatus = false;

   trTouchMsg rTouchMsg;

   //Iterate through the list and populate the touch co-ordinates
   for (itTouchInfo = rfrTouchData.tvecTouchInfoList.begin(); itTouchInfo
      != rfrTouchData.tvecTouchInfoList.end(); ++itTouchInfo)
   {
      vecTouchCoord = itTouchInfo->tvecTouchCoordinatesList;

      for (itTouchCoord = vecTouchCoord.begin(); itTouchCoord != vecTouchCoord.end();
         ++itTouchCoord)
      {
         u8TouchMode = static_cast<tenTouchMode>(itTouchCoord->enTouchMode);

         t_S32 xCoordinate = (itTouchCoord->s32XCoordinate);
         t_S32 yCoordinate = (itTouchCoord->s32YCoordinate);

         ETG_TRACE_USR4(("vProcessTouchEvent() X Co-ordinate  : %d \n",xCoordinate));
         ETG_TRACE_USR4(("vProcessTouchEvent() Y Co-ordinate  : %d \n",yCoordinate));
         ETG_TRACE_USR4(("vProcessTouchEvent() Touch Mode : %d \n",u8TouchMode));


         // Populate the Touch IPC message
         rTouchMsg.enMsgType = e8TOUCH_MESSAGE;
         rTouchMsg.rTouchCoordinates.s32XCoordinate = xCoordinate;
         rTouchMsg.rTouchCoordinates.s32YCoordinate = yCoordinate;

         rTouchMsg.rTouchCoordinates.enTouchMode = static_cast<tenTouchMode>(u8TouchMode);

         bMsgSendStatus = bSendIPCMessage<trTouchMsg>(rTouchMsg);
      }
   }
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoInputHandler::vProcessKeyEvents
***************************************************************************/
t_Void spi_tclDiPoInputHandler::vProcessKeyEvents(t_U32 u32DeviceHandle,
                                                  tenKeyMode enKeyMode, tenKeyCode enKeyCode)const
{

   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   t_Bool bMsgSendStatus = false;
   trKeyMsg rKeyMsg;

   rKeyMsg.enMsgType = e8KEY_MESSAGE;
   rKeyMsg.enKeyMode = enKeyMode;
   rKeyMsg.enKeyCode = enKeyCode;
   rKeyMsg.enKeyType = e8_BUTTON_TYPE;

   ETG_TRACE_USR4(("vProcessKeyEvents() Key Mode  : %d \n",rKeyMsg.enKeyMode));
   ETG_TRACE_USR4(("vProcessKeyEvents() Key Code  : %d \n",rKeyMsg.enKeyCode));

   bMsgSendStatus = bSendIPCMessage<trKeyMsg>(rKeyMsg);

}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPoInputHandler::bSendIPCMessage()
 ***************************************************************************/
template<typename trMessage>
t_Bool spi_tclDiPoInputHandler::bSendIPCMessage(trMessage rMessage)
{
   t_Bool bRetVal = false;
   IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);

   trMessage *poBuffer = (trMessage *)oMessageQueue.vpCreateBuffer(sizeof(trMessage));

   if(NULL != poBuffer)
   {
      memset(poBuffer, 0, sizeof(trMessage));
      memcpy(poBuffer, &rMessage, sizeof(trMessage));
      bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poBuffer, DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poBuffer);
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclMLInputHandler::vSelectDevice
***************************************************************************/
t_Void  spi_tclDiPoInputHandler::vSelectDevice(const t_U32 cou32DevId,
   const tenDeviceConnectionReq coenConnReq)
{
  ETG_TRACE_USR1(("spi_tclDiPoInputHandler:vSelectDevice:Device-0x%x \n",cou32DevId));
  SPI_INTENTIONALLY_UNUSED(coenConnReq);
  /*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
  if(NULL != m_rInputCallbacks.fvSelectDeviceResp)
  {
     (m_rInputCallbacks.fvSelectDeviceResp)(cou32DevId,e8NO_ERRORS);
  }//if(NULL != m_rVideoCallbacks.fpvSelectDeviceCb
}
/***************************************************************************
** FUNCTION:  t_Void  spi_tclMLInputHandler::vRegisterVideoCallbacks
***************************************************************************/
t_Void  spi_tclDiPoInputHandler::vRegisterInputCallbacks(const trInputCallbacks& corfrInputCallbacks)
{
  ETG_TRACE_USR1(("spi_tclDiPoInputHandler:vRegisterInputCallbacks() \n"));
   //Copy
   m_rInputCallbacks = corfrInputCallbacks;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoInputHandler::vProcessKnobKeyEvents
 ***************************************************************************/
t_Void spi_tclDiPoInputHandler::vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt) const
{
   ETG_TRACE_USR1(("spi_tclDiPoInputHandler:vProcessKnobKeyEvents() entered"));
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);

   t_Bool bMsgSendStatus = false;

   trKeyMsg rKeyMsg;
   //Populate the message with Knob type.
   rKeyMsg.enMsgType = e8KEY_MESSAGE;
   rKeyMsg.enKeyType = e8_KNOB_TYPE;
   rKeyMsg.s8EncoderDeltaCnt = s8EncoderDeltaCnt;

   bMsgSendStatus = bSendIPCMessage<trKeyMsg> (rKeyMsg);
   ETG_TRACE_USR4(("vProcessKnobKeyEvents IPC Send state : %d",bMsgSendStatus));
}


