/*!
 *******************************************************************************
 * \file              spi_tclDiPoInputHandler.h
 * \brief             SPI input handler for DiPo devices
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Input handler class to send input events from DiPo client to DiPo server
 AUTHOR:        Hari Priya E R (RBEI/ECP2)
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.11.2013 |  Hari Priya E R              | Initial Version
 10.03.2014 |  Hari Priya E R              | Touch handling Implementation
 04.04.2014 |  Hari Priya E R              | Key handling Implementation
 03.07.2014 |  Hari Priya E R              | Added changes related to Input Response interface
 17.07.2015 |  Sameer Chandra              | Added knob key support.

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLDIPOINPUTHANDLER_H_
#define SPI_TCLDIPOINPUTHANDLER_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "Timer.h"
#include "DiPOTypes.h"
#include "spi_tclInputDevBase.h"


/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/
 class spi_tclInputRespIntf;

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/


/*!
 * \class spi_tclDiPoInputHandler
 * \brief provides input handler to send input events from DiPo client to DiPo server
 *
 */
class spi_tclDiPoInputHandler: public spi_tclInputDevBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoInputHandler::spi_tclDiPoInputHandler
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoInputHandler(spi_tclInputRespIntf* poInputRespIntf)
   * \brief   Parameterised constructor
   * \param   poInputRespIntf: [IN]Pointer to Input Resp Interface
   * \sa      ~spi_tclDiPoInputHandler()
   **************************************************************************/
   spi_tclDiPoInputHandler(spi_tclInputRespIntf* poInputRespIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoInputHandler::~spi_tclDiPoInputHandler()
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoInputHandler()
   * \brief   destructor
   * \sa      spi_tclDiPoInputHandler()
   **************************************************************************/
   ~spi_tclDiPoInputHandler();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoInputHandler::vProcessTouchEvent
   ***************************************************************************/
   /*!
   * \fn      vProcessTouchEvent()
   * \brief   Receives the Touch events and forwards it through IPC to DiPO Server 
   * \param   u32DeviceHandle  :[IN] unique identifier to ML Server
   * \param   rfrTouchData     :[IN] reference to touch data structure which contains
   *          touch details received /ref trTouchData
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessTouchEvent(t_U32 u32DeviceHandle,trTouchData &rfrTouchData)const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoInputHandler::vProcessKeyEvents
   ***************************************************************************/
   /*!
   * \fn      vProcessKeyEvents()
   * \brief   Receives hard key events and forwards it through IPC to DiPO Server
   * \param   u32DeviceHandle :[IN] unique identifier to ML Server
   * \param   enKeyMode       :[IN] indicates keypress or keyrelease
   * \param   enKeyCode       :[IN] unique key code identifier
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessKeyEvents(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
      tenKeyCode enKeyCode)const;

   /***************************************************************************
   ** FUNCTION: virtual t_Void spi_tclMLInputHandler::vSelectDevice()
   ***************************************************************************/
  /*!
   * \fn      t_Void vSelectDevice(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq)
   * \brief   To setup Video related info when a device is selected or
   *          de selected.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Type.
   * \retval  t_Void
   **************************************************************************/
   t_Void  vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq);
  /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclMLInputHandler::vRegisterVideoCallbacks()
   ***************************************************************************/
  /*!
   * \fn      t_Void vRegisterInputCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo Video
   * \param   corfrVideoCallbacks : [IN] Video callabcks structure
   * \retval  t_Void
   **************************************************************************/
   t_Void  vRegisterInputCallbacks(const trInputCallbacks& corfrInputCallbacks);
   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoInputHandler::vProcessKnobKeyEvents
   ***************************************************************************/
   /*!
   * \fn      vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt
   * \brief   Receives Knob key enocder change and forwards it to
   *          further handlers for processing
   * \param   u32DeviceHandle : [IN] unique identifier to CP Server
   * \param   s8EncoderDeltaCount : [IN] encoder delta count
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt)
      const;

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE***********************************
   ***************************************************************************/
   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoInputHandler::spi_tclDiPoInputHandler
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoInputHandler(const spi_tclMLInputHandler &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDiPoInputHandler(const spi_tclDiPoInputHandler& corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoInputHandler& spi_tclDiPoInputHandler::operator= 
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoInputHandler& operator= (const spi_tclDiPoInputHandler &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDiPoInputHandler& operator=(const spi_tclDiPoInputHandler& corfrSrc);


   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPoInputHandler::bSendIPCMessage()
   ***************************************************************************/
   /*!
   * \fn     bSendIPCMessage(trMsgQBase &rfoMsgQBase)
   * \brief  Send the IPC message to SPI component.
   * \param  rMessage : [IN]Message data
   * \retVal  t_Bool : True if message send success, false otherwise
   * \sa     
   **************************************************************************/
   template<typename trMessage>
   static t_Bool bSendIPCMessage(trMessage rMessage);

   //! Input Callbacks structure
   trInputCallbacks m_rInputCallbacks;
   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif /* SPI_TCLDIPOINPUTHANDLER_H_ */
