/*!
*******************************************************************************
* \file              spi_tclDiPOContextHandler.cpp
* \brief             DiPO VideoContext reader 
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Video Context reader implementation
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
28.03.2014 |  Shihabudheen P M            | Initial Version
21.04.2014 |  Shihabudheen P M            | Modified for audio contexct handling
02.07.2014 |  Shihabudheen P M            | Remove AudioContext mapping
16.12.2014 |  Shihabudheen P M            | Added info context handling

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "spi_tclDiPOContextHandler.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOContextHandler.cpp.trc.h"
#endif

/******************************************************************************
| defines:
|----------------------------------------------------------------------------*/
// To populate the video context information for requesting the ownership of a resource
#define DIPO_VIDEO_REQ_CONTEXT
//! Array of video context data
static trDiPOVideoContext rDiPOVideoContextReq[] =
#include "spi_tclDiPOContext.cfg"  
#undef DIPO_VIDEO_REQ_CONTEXT

//! To populate the video context information for releasing the ownership of a resource
#define DIPO_VIDEO_REL_CONTEXT
//! Array of video context data for release.
static trDiPOVideoContext rDiPOVideoContextRel[] =
#include "spi_tclDiPOContext.cfg"
#undef DIPO_VIDEO_REL_CONTEXT

//! To populate the audio context information for requesting the audio ownership
#define DIPO_AUDIO_REQ_CONTEXT
//!Array of audio context data
static trDiPOAudioContext rDiPOAudioContextReq[] =
#include "spi_tclDiPOContext.cfg"
#undef DIPO_AUDIO_REQ_CONTEXT

//! To populate the audio context information for relaesing the audio ownership
#define DIPO_AUDIO_REL_CONTEXT
//!Array of audio context data
static trDiPOAudioContext rDiPOAudioContextRel[] =
#include "spi_tclDiPOContext.cfg"
#undef DIPO_AUDIO_REL_CONTEXT

//! Audio/Video info context are used to inform the iPhone about 
// itermediate transfer of states. i.e.This transfer requests will not yield for any resource transfer.
#define DIPO_VIDEO_INFO_CONTEXT
static trDiPOVideoContext rDiPOVideoInfoContext[] =
#include "spi_tclDiPOContext.cfg"
#undef DIPO_VIDEO_INFO_CONTEXT

#define DIPO_AUDIO_INFO_CONTEXT
static trDiPOAudioContext rDiPOAudioInfoContext[] =
#include "spi_tclDiPOContext.cfg"
#undef DIPO_AUDIO_INFO_CONTEXT



/***************************************************************************
** FUNCTION: spi_tclDiPOContextHandler::spi_tclDiPOContextHandler()
***************************************************************************/
spi_tclDiPOContextHandler::spi_tclDiPOContextHandler()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
** FUNCTION: spi_tclDiPOContextHandler::~spi_tclDiPOContextHandler()
***************************************************************************/
spi_tclDiPOContextHandler::~spi_tclDiPOContextHandler()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}


/***************************************************************************
** FUNCTION: spi_tclDiPOContextHandler::bGetVideoContextInfo()
***************************************************************************/
t_Bool spi_tclDiPOContextHandler::bGetVideoContextInfo(tenDisplayContext enDisplayContext, t_Bool bReqStatus, 
                                                       trDiPOVideoContext &rfoDiPOVideoContext)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal =false;
   

   if(true == bReqStatus)
   {
      t_U32 u32ContainerSize = (sizeof(rDiPOVideoContextReq))/(sizeof(trDiPOVideoContext));
      //This part of the code will execute if the accessory request to get the ownership of a resource
      for(t_U8 u8index =0; u8index < u32ContainerSize; u8index++)
      {
         // Check for the matching Accessory context entry
         if(enDisplayContext == rDiPOVideoContextReq[u8index].enDisplayContext)
         {
            bRetVal = true;
            rfoDiPOVideoContext = rDiPOVideoContextReq[u8index];
            // stop the process one search hits at the desired value
            break;
         } //if(enDisplayContext == rDiPOVideoContext[u8index].enDisplayContext)
      } //for(t_U8 u8index =0; u8index < CONTEXT_INFO_MAX_SIZE; u8index++)
   } //if(true == bReqStatus)
   else
   {
   	t_U32 u32ContainerSize = (sizeof(rDiPOVideoContextRel))/(sizeof(trDiPOVideoContext));
      //This part of the code will execute if the accessory request to release the ownership of a resource
      for(t_U8 u8index =0; u8index < u32ContainerSize; u8index++)
      {
         // Check for the matching Accessory context entry
         if(enDisplayContext == rDiPOVideoContextRel[u8index].enDisplayContext)
         {
            bRetVal = true;
            rfoDiPOVideoContext = rDiPOVideoContextRel[u8index];
            // stop the process one search hits at the desired value
            break;
         } //if(enDisplayContext == rDiPOVideoContext[u8index].enDisplayContext)
      }//for(t_U8 u8index =0; u8index < CONTEXT_INFO_MAX_SIZE; u8index++)
   } // if(true == bReqStatus)
   return bRetVal;

}

/***************************************************************************
** FUNCTION: spi_tclDiPOContextHandler::bGetAudioContextInfo()
***************************************************************************/
t_Bool spi_tclDiPOContextHandler::bGetAudioContextInfo(tenAudioContext enAudioCntxt, t_Bool bReqStatus,
                                                       trDiPOAudioContext &rfoDiPOAudioContext)
{
   t_Bool bRetVal = false;

   if(true == bReqStatus)
   {
      t_U32 u32ContainerSize = (sizeof(rDiPOAudioContextReq))/(sizeof(trDiPOAudioContext));
      //This part of the code will execute if the accessory request to get the ownership of the audio resource
      for(t_U8 u8index =0; u8index < u32ContainerSize; u8index++)
      {
         // Check for the matching Accessory context entry
         if(enAudioCntxt == rDiPOAudioContextReq[u8index].enAudioContext)
         {
            bRetVal = true;

            rfoDiPOAudioContext = rDiPOAudioContextReq[u8index];
            break; // stop the process one search hits at the desired value

         } //if(enDisplayContext == rDiPOAudioContextReq[u8index].enAudioContext)
      } //for(t_U8 u8index =0; u8index < CONTEXT_INFO_MAX_SIZE; u8index++)

   }
   else //if(true == bReqStatus)
   {
      t_U32 u32ContainerSize = (sizeof(rDiPOAudioContextRel))/(sizeof(trDiPOAudioContext));
      //This part of the code will execute if the accessory request to release the ownership of the audio resource
      for(t_U8 u8index =0; u8index < u32ContainerSize; u8index++)
      {
         // Check for the matching Accessory context entry
         if(enAudioCntxt == rDiPOAudioContextRel[u8index].enAudioContext)
         {
            bRetVal = true;
            rfoDiPOAudioContext = rDiPOAudioContextRel[u8index];
            break;  // stop the process one search hits at the desired value

         } //if(enDisplayContext == rDiPOAudioContextRel[u8index].enAudioContext)
      } //for(t_U8 u8index =0; u8index < CONTEXT_INFO_MAX_SIZE; u8index++)
   } //if(true == bReqStatus)
   ETG_TRACE_USR1(("spi_tclDiPOContextHandler::bGetAudioContextInfo enAudioCntxt = %d bReqStatus = %d bRetVal = %d ",
         ETG_ENUM(AUDIO_CONTEXT,enAudioCntxt),ETG_ENUM(BOOL,bReqStatus), ETG_ENUM(BOOL,bRetVal)));
   return bRetVal;
}


/***************************************************************************
** FUNCTION: spi_tclDiPOContextHandler::bGetVideoDummyContext()
***************************************************************************/
t_Bool spi_tclDiPOContextHandler::bGetVideoDummyContext(tenDisplayContext enDisplayContext,
                                                       trDiPOVideoContext &rfoDiPOVideoContext)
{
   t_Bool bRetVal = false;
   t_U32 u32ContainerSize = (sizeof(rDiPOVideoInfoContext))/(sizeof(trDiPOVideoContext));
   for(t_U8 u8index =0; u8index < u32ContainerSize; u8index++)
   {
      // Check for the matching Accessory context entry
      if(enDisplayContext == rDiPOVideoInfoContext[u8index].enDisplayContext)
      {
         bRetVal = true;
         rfoDiPOVideoContext = rDiPOVideoInfoContext[u8index];
         // stop the process one search hits at the desired value
         break;
      } //if(enDisplayContext == rDiPOVideoInfoContext[u8index].enDisplayContext)
   } //for(t_U8 u8index =0; u8index < u32ContainerSize; u8index++)
   return bRetVal;
}

/***************************************************************************
** FUNCTION: spi_tclDiPOContextHandler::bGetAudioDummyContext()
***************************************************************************/
t_Bool spi_tclDiPOContextHandler::bGetAudioDummyContext(tenAudioContext enAudioCntxt,
      trDiPOAudioContext &rfoDiPOAudioContext)
{
   t_Bool bRetVal = false;
   t_U32 u32ContainerSize = (sizeof(rDiPOAudioInfoContext))/(sizeof(trDiPOAudioContext));
   for(t_U8 u8index =0; u8index < u32ContainerSize; u8index++)
   {
      // Check for the matching Accessory context entry
      if(enAudioCntxt == rDiPOAudioInfoContext[u8index].enAudioContext)
      {
         bRetVal = true;

         rfoDiPOAudioContext = rDiPOAudioInfoContext[u8index];
         break; // stop the process one search hits at the desired value

      } //if(enDisplayContext == rDiPOAudioContextReq[u8index].enAudioContext)
   } //for(t_U8 u8index =0; u8index < CONTEXT_INFO_MAX_SIZE; u8index++)
   ETG_TRACE_USR1(("spi_tclDiPOContextHandler::bGetAudioDummyContext enAudioCntxt = %d  bRetVal = %d ",
      ETG_ENUM(AUDIO_CONTEXT,enAudioCntxt),ETG_ENUM(BOOL,bRetVal)));
   return bRetVal;
}

