/*!
*******************************************************************************
* \file              spi_tclDiPOContextHandler.h
* \brief             DiPO Context reader
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Video Context reader
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
28.03.2014 |  Shihabudheen P M            | Initial Version
21.04.2014 |  Shihabudheen P M            | Modified for audio contexct handling
02.07.2014 |  Shihabudheen P M            | Remove AudioContext mapping
16.12.2014 |  Shihabudheen P M            | Added info context handling

\endverbatim
******************************************************************************/

#ifndef SPI_TCLDIPCONTEXTHANDLER_H
#define SPI_TCLDIPCONTEXTHANDLER_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "DiPOTypes.h"
#include "GenericSingleton.h"


/******************************************************************************
| defines:
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class spi_tclDiPOContextHandler
* \brief DiPO Context reader
*
* spi_tclDiPORMMsgHandler is used to populate the DiPO video context information 
* along with the priority, constraints etc. This class will read the data from
* spi_tclVideoContext.cfg file and keep it in a specified format
****************************************************************************/
class spi_tclDiPOContextHandler : public GenericSingleton<spi_tclDiPOContextHandler>
{
public:
   /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOContextHandler::~spi_tclDiPOContextHandler()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOContextHandler()
   * \brief  Destructor
   * \sa     spi_tclDiPOContextHandler()
   **************************************************************************/ 
   ~spi_tclDiPOContextHandler();

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOContextHandler::bGetVideoContextInfo()
   ***************************************************************************/
   /*!
   * \fn     bGetVideoContextInfo()
   * \brief  Returns the video context info based on the accessory display context
   * \param  enDisplayContext : [IN] Accessory display context
   * \param  bReqStatus : [IN] request status
   * \param  trDiPOVideoContext : [OUT]Mode change info.
   * \sa     
   **************************************************************************/ 
   t_Bool bGetVideoContextInfo(tenDisplayContext enDisplayContext, t_Bool bReqStatus, 
      trDiPOVideoContext &rfoDiPOVideoContext);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOContextHandler::bGetAudioContextInfo()
   ***************************************************************************/
   /*!
   * \fn     bGetAudioContextInfo()
   * \brief  Returns the audio context info based on the accessory audio context.
   *         This function will fetch the data based on the SPI internal audio context
   *         mapping irrespective of project specific mapping of context.
   * \param  enAudioCntxt : [IN] Accessory display context(SPI internal context)
   * \param  bReqStatus : [IN] request status
   * \param  rfoDiPOAudioContext : [OUT]Mode change info.
   * \sa     
   **************************************************************************/ 
   t_Bool bGetAudioContextInfo(tenAudioContext enAudioCntxt, t_Bool bReqStatus,
      trDiPOAudioContext &rfoDiPOAudioContext);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOContextHandler::bGetVideoDummyContext()
   ***************************************************************************/
   /*!
   * \fn     bGetVideoDummyContext()
   * \brief  Returns the video context info based on the accessory display context.
   * \       This is to handle the intermediate state transfer requests.
   * \param  enDisplayContext : [IN] Accessory display context(SPI internal context)
   * \param  rfoDiPOVideoContext : [OUT]Mode change info.
   * \sa     
   **************************************************************************/ 
   t_Bool bGetVideoDummyContext(tenDisplayContext enDisplayContext,
      trDiPOVideoContext &rfoDiPOVideoContext);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOContextHandler::bGetAudioDummyContext()
   ***************************************************************************/
   /*!
   * \fn     bGetAudioDummyContext()
   * \brief  Returns the audio context info based on the accessory audio context.
   *         This function will fetch the data based on the SPI internal audio context
   *         mapping irrespective of project specific mapping of context.
   * \param  enAudioCntxt : [IN] Accessory display context(SPI internal context)
   * \param  rfoDiPOAudioContext : [OUT]Mode change info.
   * \sa     
   **************************************************************************/ 
   t_Bool bGetAudioDummyContext(tenAudioContext enAudioCntxt,
      trDiPOAudioContext &rfoDiPOAudioContext);

   //! Fried class declarartion to use singleton utility.
   friend class GenericSingleton<spi_tclDiPOContextHandler>;

   /***************************************************************************
   ****************************END OF PUBLIC**********************************
   ***************************************************************************/
private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOContextHandler::spi_tclDiPOContextHandler()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOContextHandler()
   * \brief  Constructor
   * \sa     ~spi_tclDiPOContextHandler()
   **************************************************************************/ 
   spi_tclDiPOContextHandler();

   /***************************************************************************
   *****************************END OF PRIVATE********************************
   ***************************************************************************/

}; // class spi_tclDiPOVideoContext

#endif
