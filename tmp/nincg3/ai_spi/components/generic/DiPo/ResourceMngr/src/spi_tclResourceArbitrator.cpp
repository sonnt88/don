/*!
*******************************************************************************
* \file              spi_tclResourceArbitrator.cpp
* \brief             DiPO Resource Arbitrator
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Resource arbitrator
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
01.04.2014 |  Shihabudheen P M            | Initial Version
21.04.2014 |  Shihabudheen P M            | Added audio handling
22.05.2014 |  Shihabudheen P M            | Updated with App State handling
16.12.2014 |  Shihabudheen P M            | Changed resource transfer requests.

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "spi_tclDiPOContextHandler.h"
#include "spi_tclResourceArbitrator.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclResourceArbitrator.cpp.trc.h"
#endif

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceArbitrator::spi_tclResourceArbitrator()
***************************************************************************/
spi_tclResourceArbitrator::spi_tclResourceArbitrator()
:m_enPhoneAppState(e8SPI_PHONE_NOT_ACTIVE), m_enNavAppState(e8SPI_NAV_NOT_ACTIVE), 
m_enSpeechAppState(e8SPI_SPEECH_END)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceArbitrator::~spi_tclResourceArbitrator()
***************************************************************************/
spi_tclResourceArbitrator::~spi_tclResourceArbitrator()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceArbitrator::vRequestVideoModeChange()
***************************************************************************/
t_Bool spi_tclResourceArbitrator::bGetVideoModeChangeMsg(tenDisplayContext enAccDisplayContext, 
                                                         t_Bool bReqStatus,
                                                         trAccVideoContextMsg &rfoAccDispContextMsg,
                                                         tenModeChangeReqType enModeChangeReqType)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = false;
   trDiPOVideoContext rDiPOVideoContext;

   //DiPOContext handler 
   spi_tclDiPOContextHandler *poContextHandler = spi_tclDiPOContextHandler::getInstance();

   if(NULL != poContextHandler)
   {
      bRetVal = (e8ACTUAL_MODE_CHANGE_REQUEST == enModeChangeReqType) ?
         poContextHandler->bGetVideoContextInfo(enAccDisplayContext, bReqStatus, rDiPOVideoContext) :
         poContextHandler->bGetVideoDummyContext(enAccDisplayContext, rDiPOVideoContext);

      if(true == bRetVal)
      {
         rfoAccDispContextMsg.enMsgType = e8RM_VIDEO_RQST_MESSAGE;
         rfoAccDispContextMsg.rDiPOVideoContext = rDiPOVideoContext;    
      }
      else
      {
         ETG_TRACE_ERR((" Search element not found"));
      } //if(true == bRetVal)
   }
   else
   {
      ETG_TRACE_ERR((" Context handler or Receiver is NULL"));
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceArbitrator ::bGetAudioModechangeMsg()
***************************************************************************/
t_Bool spi_tclResourceArbitrator::bGetAudioModechangeMsg(const tenAudioContext coenAudioCntxt,
                                                         t_Bool bReqStatus,
                                                         trAccAudioContextMsg &rfoAccAudioContextMsg,
                                                         tenModeChangeReqType enModeChangeReqType)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = false;
   trDiPOAudioContext rDiPOAudioContext;

   //DiPOContext handler 
   spi_tclDiPOContextHandler *poContextHandler = spi_tclDiPOContextHandler::getInstance();
   if(NULL != poContextHandler)
   {
      // Fetch the audio context info 
      bRetVal = (e8ACTUAL_MODE_CHANGE_REQUEST == enModeChangeReqType)?
         poContextHandler->bGetAudioContextInfo(coenAudioCntxt, bReqStatus,rDiPOAudioContext):
         poContextHandler->bGetAudioDummyContext(coenAudioCntxt, rDiPOAudioContext);

      if(true == bRetVal)
      {
         rfoAccAudioContextMsg.enMsgType = e8RM_AUDIO_RQST_MESSAGE;
         rfoAccAudioContextMsg.rDiPOAudioContext = rDiPOAudioContext;
      }
      else
      {
         ETG_TRACE_ERR((" Search element not found"));
      }//if(true == bRetVal)
   }
   else //if(NULL != poContextHandler)
   {
       ETG_TRACE_ERR((" Context handler or Receiver is NULL"));
   }//if(NULL != poContextHandler)

   return bRetVal;
}


/***************************************************************************
** FUNCTION: t_Void spi_tclResourceArbitrator ::bGetDeviceSpeechState()
***************************************************************************/
t_Bool spi_tclResourceArbitrator::bGetDeviceSpeechState(const trDiPOModeState &crfoModeState, 
                                                        const trDiPOModeState &crfoCurrModeState,
                                                        tenSpeechAppState &rfoSpeechAppState)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = true;
   if(crfoModeState.rSpeechState.enSpeechMode == crfoCurrModeState.rSpeechState.enSpeechMode)
   {
      bRetVal = false;
      rfoSpeechAppState = m_enSpeechAppState;
   }
   else if(e8DIPO_ENTITY_MOBILE == crfoModeState.rSpeechState.enEntity)
   {
      //Update only if the mode change related to device App state.
      switch((t_U8)crfoModeState.rSpeechState.enSpeechMode)
      {
      case e8DIPO_SPEECHMODE_NA:
      case e8DIPO_SPEECHMODE_NONE:
         {
            rfoSpeechAppState = e8SPI_SPEECH_END;
         }
         break;
      case e8DIPO_SPEECHMODE_REC:
         {
            rfoSpeechAppState = e8SPI_SPEECH_RECOGNIZING;
         }
         break;
      case e8DIPO_SPEECHMODE_SPEAKING:
         {
            rfoSpeechAppState = e8SPI_SPEECH_SPEAKING;
         }
         break;
      default:
         {
            bRetVal = false;
         }
      }//switch((t_U8)crfoModeState.rSpeechState.enSpeechMode)
   }//if(crfoModeState.rSpeechState.enSpeechMode == crfoCurrModeState.rSpeechState.enSpeechMode) 
   else if((e8DIPO_ENTITY_CAR == crfoModeState.rSpeechState.enEntity) ||(e8DIPO_ENTITY_NA == crfoModeState.rSpeechState.enEntity))
   {
       rfoSpeechAppState = e8SPI_SPEECH_END;
   }
   m_enSpeechAppState = rfoSpeechAppState;
   ETG_TRACE_USR1(("bGetDeviceSpeechState %d\n", ETG_ENUM( BOOL, bRetVal)));
   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceArbitrator ::bGetDevicePhoneState()
***************************************************************************/
t_Bool spi_tclResourceArbitrator::bGetDevicePhoneState(const trDiPOModeState &crfoModeState, 
                                                       const trDiPOModeState &crfoCurrModeState,
                                                       tenPhoneAppState &rfoPhoneAppState)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = true;

   ETG_TRACE_USR1(("Current Phone State %d", crfoCurrModeState.enPhone));
   ETG_TRACE_USR1(("New Phone State %d", crfoModeState.enPhone));
   if(crfoModeState.enPhone == crfoCurrModeState.enPhone)
   {
      bRetVal = false;
      rfoPhoneAppState = m_enPhoneAppState;
   }
   else if(e8DIPO_ENTITY_MOBILE == crfoModeState.enPhone)
   {
      rfoPhoneAppState = e8SPI_PHONE_ACTIVE;
   }
   else if((e8DIPO_ENTITY_NA == crfoModeState.enPhone) ||(e8DIPO_ENTITY_CAR == crfoModeState.enPhone))
   {
      rfoPhoneAppState = e8SPI_PHONE_NOT_ACTIVE;
   }//if(crfoModeState.enPhone == crfoCurrModeState.enPhone)
   m_enPhoneAppState = rfoPhoneAppState;
   ETG_TRACE_USR1(("bGetDevicePhoneState %d\n", ETG_ENUM( BOOL, bRetVal)));
   return bRetVal;
}


/***************************************************************************
** FUNCTION: t_Void spi_tclResourceArbitrator ::bGetDeviceNavState()
***************************************************************************/
t_Bool spi_tclResourceArbitrator::bGetDeviceNavState(const trDiPOModeState &crfoModeState, 
                                                     const trDiPOModeState &crfoCurrModeState,
                                                     tenNavAppState &rfoNavAppState)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = true;

   ETG_TRACE_USR1(("Current Navigation State %d", crfoCurrModeState.enNavigation));
   ETG_TRACE_USR1(("New Navigation State %d", crfoModeState.enNavigation));
   if(crfoModeState.enNavigation == crfoCurrModeState.enNavigation)
   {
      bRetVal = false;
      rfoNavAppState = m_enNavAppState;
   }
   else if(e8DIPO_ENTITY_MOBILE == crfoModeState.enNavigation)
   {
      rfoNavAppState = e8SPI_NAV_ACTIVE;
   }
   else if((e8DIPO_ENTITY_NA == crfoModeState.enNavigation) ||(e8DIPO_ENTITY_CAR == crfoModeState.enNavigation))
   {
      rfoNavAppState = e8SPI_NAV_NOT_ACTIVE;
   }
   m_enNavAppState = rfoNavAppState;
   ETG_TRACE_USR1(("bGetDeviceNavState %d\n", ETG_ENUM( BOOL, bRetVal)));
   return bRetVal;
}
