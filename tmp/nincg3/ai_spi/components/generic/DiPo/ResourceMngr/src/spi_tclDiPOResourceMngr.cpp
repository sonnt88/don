/*!
*******************************************************************************
* \file              spi_tclDiPOResourceMngr.cpp
* \brief             DiPO Resource manager
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Resource manager implementation
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
24.03.2014 |  Shihabudheen P M            | Initial Version
05.04.2014 |  Priju K Padiyath            | Modified to handle display & audio context
14.04.2014 |  Shihabudheen P M            | Modified vSetAccessoryDisplayContext()
19.04.2014 |  Shihabudheen P M            | Implemented CarPlay resource arbitration
25.06.2014 |  Shihabudheen P M            | Adapted to the CarPlay design changes
27.08.2014 |  Shihabudheen P M            | Changes to reset all resource arbitration parameters.
16.12.2014 |  Shihabudheen P M            | Changed resource transfer requests.
11.02.2015 |  Shihabudheen P M            | Added timer implementation
06.05.2015  |Tejaswini HB                 |Lint Fix
26.05.2015 |  Tejaswini H B(RBEI/ECP2)    | Added Lint comments to suppress C++11 Errors
15.06.2015 |  Shihabudheen P M            | added vSetVehicleBTAddress.

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "spi_tclResorceMngrDefines.h"
#include "spi_tclDiPODeviceMsgRcvr.h"
#include "spi_tclResourceArbitrator.h"
#include "spi_tclFactory.h"
#include "spi_tclAudio.h"
#include "spi_tclMediator.h"
#include "spi_tclDiPoAudioResourceMngr.h"
#include "spi_tclDiPOResourceMngr.h"
#include "spi_tclBluetooth.h"
#include "spi_tclConfigReader.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
#include "trcGenProj/Header/spi_tclDiPOResourceMngr.cpp.trc.h"
#endif

static t_U32 su32TimerInterval=0;
static timer_t srTimerIndex = 0;
static t_String szBtMacAddress = "";
t_U32 spi_tclDiPOResourceMngr::m_U32DevId = 0;
tenDiPOSessionState spi_tclDiPOResourceMngr::m_enDiPOSessionState = e8DIPO_SESSION_UNKNOWN; 
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/***************************************************************************
** FUNCTION: spi_tclDiPOResourceMngr::spi_tclDiPOResourceMngr()
***************************************************************************/
spi_tclDiPOResourceMngr::spi_tclDiPOResourceMngr()
   :m_poResArb(NULL),
    m_enCurDispCntxt(e8DISPLAY_CONTEXT_UNKNOWN), 
    m_u8CurAudioSource(e8SPI_AUDIO_MAIN), 
    m_enAudioDir(e8AUD_INVALID), 
    m_poAudioResourceMngr(NULL),
    m_MasterVideoContext(e8DISPLAY_CONTEXT_UNKNOWN), 
    m_MasterVideoTransfer(e8DIPO_TRANSFERTYPE_NA),
    m_MasterAudioContext(e8SPI_AUDIO_MAIN), 
    m_MasterAudioTransfer(e8DIPO_TRANSFERTYPE_NA),
    m_bCurDispFlag(false), 
    m_bCurAudioFlag(false),
    m_u8DriveModeRestrictionInfo(0),
	m_enCPlayAutoLaunch(e8AUTOLAUNCH_DISABLED)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_poResArb = new spi_tclResourceArbitrator();
   SPI_NORMAL_ASSERT( NULL == m_poResArb);

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if(NULL != poSPIFactory)
   {
      spi_tclAudio *poAudio = poSPIFactory->poGetAudioInstance();
      m_poAudioResourceMngr = new spi_tclDiPoAudioResourceMngr(poAudio);
      SPI_NORMAL_ASSERT( NULL == m_poAudioResourceMngr);
   }//if(NULL != poSPIFactory)
}

/***************************************************************************
** FUNCTION: spi_tclDiPOResourceMngr::~spi_tclDiPOResourceMngr()
***************************************************************************/
spi_tclDiPOResourceMngr::~spi_tclDiPOResourceMngr()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   RELEASE_MEM(m_poResArb);
   RELEASE_MEM(m_poAudioResourceMngr);
}


/***************************************************************************
** FUNCTION: spi_tclDiPOResourceMngr::vOnSPISelectDeviceResult()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vOnSPISelectDeviceResult(t_U32 u32DevID,
      tenDeviceConnectionReq enDeviceConnReq, tenResponseCode enRespCode,
      tenErrorCode enErrorCode)
{
	SPI_INTENTIONALLY_UNUSED(enErrorCode);

   if ((e8DEVCONNREQ_SELECT == enDeviceConnReq) && (e8SUCCESS == enRespCode))
   {
      m_U32DevId = u32DevID;
   }
   else if (e8DEVCONNREQ_DESELECT == enDeviceConnReq)
   {
      m_U32DevId = 0;
      vHandleSessionEnd();
   }

   //@Note: Start the timer for handling the session startup.
   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
   {
      if ((e8DEVCONNREQ_SELECT == enDeviceConnReq) && (e8SUCCESS == enRespCode) && (e8DIPO_SESSION_START
               != m_enDiPOSessionState))
      {
         //! Start the timer as soon role switch is successfull.
         poTimer->StartTimer(srTimerIndex, su32TimerInterval, 0, this, bSessionStartTimerCb, NULL);

      }
      else if ((e8DEVCONNREQ_DESELECT == enDeviceConnReq) && (0 != srTimerIndex))
      {
         //! Stop timer
         poTimer->CancelTimer(srTimerIndex);
         srTimerIndex = 0;
      }
   }//if (NULL != poTimer)

   if (NULL != m_poAudioResourceMngr)
   {
      m_poAudioResourceMngr->vOnSPISelectDeviceResult(u32DevID,
            enDeviceConnReq, enRespCode, enErrorCode);
   }
}

/***************************************************************************
** FUNCTION: spi_tclDiPOResourceMngr::vCbDeviceStateUpdate()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vOnModeChanged(const t_U32 cou32DeviceHandle, trDiPOModeState &rfoDiPOModeState)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   vUpdateDisplayContext(rfoDiPOModeState);
   vUpdateAudioContext(rfoDiPOModeState);
   vUpdateAppState(rfoDiPOModeState);
   m_CurrModeState = rfoDiPOModeState; // To keep the current mode info

   if(NULL != m_poAudioResourceMngr)
   {
      m_poAudioResourceMngr->vOnAudioModeChange(cou32DeviceHandle, rfoDiPOModeState.enAudio);
   }//if(NULL != m_poAudioResourceMngr)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceMngr::vOnAudioMsg()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioAllocMsg &rfrAudioAllocMsg)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != m_poAudioResourceMngr)
   {
      m_poAudioResourceMngr->vOnAudioMsg(cou32DeviceHandle, rfrAudioAllocMsg);
   }//if(NULL != m_poAudioResourceMngr)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceMngr::vOnAudioMsg()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioDuckMsg &rfrAudioDuckMsg)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);

   if(NULL != m_poAudioResourceMngr)
   {
      m_poAudioResourceMngr->vOnAudioDuckMsg(rfrAudioDuckMsg);
   }//End of if(NULL != m_poAudioResourceMngr)
}

/***************************************************************************
** FUNCTION: spi_tclDiPOResourceMngr::vCbOnRequestUI()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vOnRequestUI(t_Bool bRenderStatus)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   // Request a mode change to the device, to indicate that the accessory is
   // ready to render native HMI and it want the ownership of the resource.
   vSetAccessoryDisplayContext(m_U32DevId, bRenderStatus, m_enCurDispCntxt, m_rcUsrCntxt);
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDiPOResourceMngr::vCbOnSessionUpdate()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vOnSessionMsg(tenDiPOSessionState enDiPOSessionState)
{
   /*lint -esym(40,fvUpdateSessionStatus)fvUpdateSessionStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDiPOResourceMngr::vOnSessionMsg entered with state: %d",
      ETG_ENUM(DIPO_SESSION_STATE, enDiPOSessionState)));

   m_enDiPOSessionState = enDiPOSessionState;
   tenSessionStatus enSessionStatus = e8_SESSION_UNKNOWN;
   if(e8DIPO_SESSION_END == m_enDiPOSessionState)
   {      
      vHandleSessionEnd();
      enSessionStatus = e8_SESSION_INACTIVE;
      if(NULL != m_poAudioResourceMngr)
      {
         m_poAudioResourceMngr->vHandleSessionEnd();
      }
   }//if(e8DIPO_SESSION_END == m_enDiPOSessionState)
   else if (e8DIPO_SESSION_START == m_enDiPOSessionState)
   {
      enSessionStatus = e8_SESSION_ACTIVE;
      //!Cancel the start up timer if it still running
      Timer* poTimer = Timer::getInstance();
      if ((NULL != poTimer) && (0 != srTimerIndex))
      {
         poTimer->CancelTimer(srTimerIndex);
         srTimerIndex =0;
      }
   }//else if (e8DIPO_SESSION_START == m_enDiPOSessionState)
   else if(e8DIPO_PLUGIN_LOADED == m_enDiPOSessionState)
   {
      
      spi_tclBluetooth *poBluetooth = spi_tclFactory::getInstance()->poGetBluetoothInstance();
      spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();

      if ((NULL != poBluetooth) && (NULL != poConfigReader))
      {
         trVehicleConfigData rVehicleConfigData;
         poConfigReader->vGetVideoConfigData(e8DEV_TYPE_DIPO,
                  rVehicleConfigData.rVideoConfigData);

         rVehicleConfigData.enDriveSideInfo
                  = poConfigReader->enGetDriveSideInfo();
         poConfigReader->vGetOemIconData(rVehicleConfigData.rVehicleBrandInfo);

         /*//! Check if rotary controllers are supported.
         rVehicleConfigData.rVehicleBrandInfo.bIsRotarySupported
                  = poConfigReader->bGetRotaryCtrlSupport();*/


         //! Read the drive restriction value from configuration
         t_U8 u8DriveRestrictionInfo = poConfigReader->u8GeDiPODriveRestrictionInfo();

         t_String szBtId = "";
         poBluetooth->vGetVehicleBTAddress(szBtId);
         vFormatBTMacAddress(szBtId);
         IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
         trDiPOInfoMessage *poInfoMessage =
                           (trDiPOInfoMessage *) oMessageQueue.vpCreateBuffer(sizeof(trDiPOInfoMessage));
         if(NULL != poInfoMessage)
         {
            poInfoMessage->enMsgType = e8INFO_MESSAGE;
            strncpy(poInfoMessage->szBtMacAdress, szBtId.c_str(), MAX_STR_LEN);
            poInfoMessage->rVehicleConfigData = rVehicleConfigData;
            strncpy((poInfoMessage->rVehicleConfigData.rVehicleBrandInfo).szOemIconPath,
                     rVehicleConfigData.rVehicleBrandInfo.szOemIconPath, MAX_STR_LEN);
            strncpy((poInfoMessage->rVehicleConfigData.rVehicleBrandInfo).szOemName,
                     rVehicleConfigData.rVehicleBrandInfo.szOemName, MAX_STR_LEN);

            poConfigReader->vGetDriveModeInfo(poInfoMessage->enDriveModeInfo);
            poConfigReader->vGetNightModeInfo(poInfoMessage->enNightModeInfo);
            poConfigReader->vGetDisplayInputParam(poInfoMessage->u8DisplayInput);
            poInfoMessage->enAutoLaucnhFlag = m_enCPlayAutoLaunch;

            ETG_TRACE_USR1(("CarPlay Audo Launch vOnCarPlayPluginLoaded: %d", m_enCPlayAutoLaunch));
            poInfoMessage->u8DriveRestrictionInfo = u8DriveRestrictionInfo;

            t_Bool bStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*) poInfoMessage,
                              DIPO_MESSAGE_PRIORITY, (eMessageType) TCL_DATA_MESSAGE));
            ETG_TRACE_USR1(("IPC Message send status from vOnCarPlayPluginLoaded: %d", bStatus));
         }// if(NULL != poInfoMessage)

         //Destroy the buffer
         oMessageQueue.vDropBuffer(poInfoMessage);

         trUserContext rfrcUsrCntxt;
         while(!m_rAudioContextInfo.empty())
         {
        	 trAudioContextInfo rAudioContextInfo = m_rAudioContextInfo.front();
        	 m_rAudioContextInfo.pop();
        	 vSetAccessoryAudioContext(m_U32DevId, rAudioContextInfo.enAudioContext, rAudioContextInfo.bAudioFlag, rfrcUsrCntxt);
         }

         vSetAccessoryAppState(m_rDiPOAppState.enSpeechAppState,
        		 m_rDiPOAppState.enPhoneAppState,
        		 m_rDiPOAppState.enNavAppState, rfrcUsrCntxt);

      }//if((NULL != poBluetooth) && (NULL != poConfigReader))
   }
   if((e8_SESSION_UNKNOWN != enSessionStatus) && (NULL != (m_rRsrcMngrCallback.fvUpdateSessionStatus)))
   {
      (m_rRsrcMngrCallback.fvUpdateSessionStatus)(m_U32DevId, e8DEV_TYPE_DIPO, enSessionStatus);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vRegRsrcMngrCallBack()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vRegRsrcMngrCallBack(trRsrcMngrCallback rRsrcMngrCallback)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_rRsrcMngrCallback = rRsrcMngrCallback;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetAccessoryDisplayContext()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vSetAccessoryDisplayContext(const t_U32 cou32DevId,
                                                            t_Bool bDisplayFlag, 
                                                            tenDisplayContext enDisplayContext,
                                                            const trUserContext& rfrcUsrCntxt)
{
   // This informations are keep to utilize for asynchronous responses
   m_rcUsrCntxt = rfrcUsrCntxt; // To keep the user context 
   t_Bool bStatus = false;
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   
   //!@ Glossary
   // Master Transfer : A transfer which cuases the display resource to transfer from the iPhone to Car or vice versa.
   //                   Possible values : Take, Borrow, NA
   // Master video context: Display context which causes a display resource transfer from iPhone to car or vice versa.
   // Please refer spi_tclDiPOContext.cfg for display context values.
   // Intermediate transfer: A transfer to update the resource access constraint and do no perform a resource transfer.

   //! Resource transfer requested only in any of these cases.
   // 1. The resource is with the mobile(controller).
   // 2. The last requested context is not same with the current one.
   if((NULL != m_poResArb) && 
      ((enDisplayContext != m_enCurDispCntxt) || 
      (bDisplayFlag != m_bCurDispFlag) ||
      (e8DIPO_ENTITY_MOBILE == m_CurrModeState.enScreen)))
   {
      trAccVideoContextMsg rAccDispContMsg;
      IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
      t_Bool bMasterTransfer = false;
      tenModeChangeReqType enModeChangeReqType = e8ACTUAL_MODE_CHANGE_REQUEST;
      tenDisplayContext enVideoContext  = enDisplayContext;

      if((e8DIPO_ENTITY_CAR == m_CurrModeState.enScreen) &&
         (e8DIPO_TRANSFERTYPE_TAKE == m_MasterVideoTransfer) &&
         (true == bDisplayFlag))
      {
         //! if it is an intermediate transfer send the info message request.
         //! Only take. No borrow, untake, unborrow.
         enModeChangeReqType = e8INFO_MODE_CHANGE_REQUEST;
         ETG_TRACE_USR1(("Within first e8INFO_MODE_CHANGE_REQUEST clause"));
      }

      else if ((e8DIPO_ENTITY_CAR == m_CurrModeState.enScreen) &&
         (m_MasterVideoContext != enDisplayContext) &&
         (e8DIPO_TRANSFERTYPE_TAKE == m_MasterVideoTransfer) &&
         (false == bDisplayFlag))
      {
         //! If it is with flag = false, and is not with the  master context,
         //! update the same with master context.
         enVideoContext = m_MasterVideoContext;
         bDisplayFlag = true;
         ETG_TRACE_USR1(("Within Revert back to master context clause"));
      }
      else
      {
         //! If the transfer is a master transfer, then forward it to the CarPlay plugin.
         bMasterTransfer = true;
         ETG_TRACE_USR1(("Within Master transfer clause"));
      }
     
      bStatus = m_poResArb->bGetVideoModeChangeMsg(enVideoContext, bDisplayFlag, rAccDispContMsg, 
         enModeChangeReqType);

      //! Update the master video context parameter value.
      if((true == bMasterTransfer) && (true == bDisplayFlag))
      {
         m_MasterVideoContext = enVideoContext;
      }
      else if ((true == bMasterTransfer) && (false == bDisplayFlag))
      {
         m_MasterVideoContext = e8DISPLAY_CONTEXT_UNKNOWN;
      }

      // ! Updating the master video transfer parameter value.
      if((true == bMasterTransfer) && (true == bDisplayFlag) &&(true == bStatus))
      {
         m_MasterVideoTransfer = rAccDispContMsg.rDiPOVideoContext.enTransferType;
      }
      else if ((true == bMasterTransfer) && (false == bDisplayFlag) &&(true == bStatus))
      {
         m_MasterVideoTransfer = e8DIPO_TRANSFERTYPE_NA;
      }

      ETG_TRACE_USR1(("Master Video Context: %d, Master Video Transfer %d", m_MasterVideoContext, m_MasterVideoTransfer));
      
      //! Send IPC message to CarPlay plugin to do the corresponding screen resource transfer request.
      trAccVideoContextMsg *pBuffer = (trAccVideoContextMsg *)oMessageQueue.vpCreateBuffer(sizeof(rAccDispContMsg));
      if((true == bStatus) && (NULL != pBuffer))
      {      
         
         ETG_TRACE_USR4(("Video message: enVideoContext=%d  enBorrowConstraint = %d  "
            "enTakeConstraint = %d  enTransferPriority = %d  enTransferType =%d \n ",
            rAccDispContMsg.rDiPOVideoContext.enDisplayContext,
            rAccDispContMsg.rDiPOVideoContext.enBorrowConstraint,
            rAccDispContMsg.rDiPOVideoContext.enTakeConstraint,
            rAccDispContMsg.rDiPOVideoContext.enTransferPriority,
            rAccDispContMsg.rDiPOVideoContext.enTransferType));
           
         memcpy(pBuffer, &rAccDispContMsg, sizeof(rAccDispContMsg));
         bStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*)pBuffer, DIPO_MESSAGE_PRIORITY,(eMessageType) TCL_DATA_MESSAGE));
         ETG_TRACE_USR1(("IPC Message send status from vSetAccessoryDisplayContext: %d", bStatus));

         if(false == bStatus)
         {
            //! Reset the Current display context information, 
            // if it failed to send the IPC message to CarPlay plugin.
            m_bCurDispFlag = false;
            m_enCurDispCntxt = e8DISPLAY_CONTEXT_UNKNOWN;
         }
         else //if(false == bStatus)
         {
            //! Update the latest sent display context to current context.
            m_enCurDispCntxt = enDisplayContext; 
            m_bCurDispFlag = bDisplayFlag;
         }// if(false == bStatus)
      }
      else //if((true == bStatus) && (NULL != pBuffer))
      {
         ETG_TRACE_ERR(("Mode change request failed\n"));
      }
      //Destroy the buffer
      oMessageQueue.vDropBuffer(pBuffer);
   } //if(NULL != m_poResArb)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPOResourceMngr::vSetAccessoryDisplayMode(t_U32...
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vSetAccessoryDisplayMode(const t_U32 cu32DeviceHandle,
                                                     const trDisplayContext corDisplayContext,
                                                     const trDisplayConstraint corDisplayConstraint,
                                                     const tenDisplayInfo coenDisplayInfo)
{
   ETG_TRACE_USR1(("spi_tclDiPOResourceMngr::vSetAccessoryDisplayMode() entered \n"));
   if(e8_DISPLAY_CONTEXT == coenDisplayInfo)
   {
      trUserContext rfrcUsrCntxt; // Dummy user context to have backward compatability.
      vSetAccessoryDisplayContext(cu32DeviceHandle, 
         corDisplayContext.bDisplayFlag, 
         corDisplayContext.enDisplayContext,
         rfrcUsrCntxt);
   }
   else if (e8_DISPLAY_CONSTRAINT == coenDisplayInfo)
   {
      trAccVideoContextMsg rfoAccDispContextMsg;
      rfoAccDispContextMsg.enMsgType = e8RM_VIDEO_RQST_MESSAGE;
      rfoAccDispContextMsg.rDiPOVideoContext.enDisplayContext = e8DISPLAY_CONTEXT_UNKNOWN; // Dummy value keep it for backward compatability.
      rfoAccDispContextMsg.rDiPOVideoContext.enTransferType = corDisplayConstraint.enTransferType;
      rfoAccDispContextMsg.rDiPOVideoContext.enTransferPriority = corDisplayConstraint.enTransferPriority;
      rfoAccDispContextMsg.rDiPOVideoContext.enTakeConstraint = corDisplayConstraint.enTakeConstraint;
      rfoAccDispContextMsg.rDiPOVideoContext.enBorrowConstraint = corDisplayConstraint.enBorrowConstraint;

      t_Bool bStatus = bSendIPCMessage<trAccVideoContextMsg>(rfoAccDispContextMsg);
      ETG_TRACE_USR1(("IPC Message send status from vSetAccessoryDisplayMode: %d",  ETG_ENUM(BOOL,bStatus)));
   }
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetAccessoryAudioContext()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vSetAccessoryAudioContext(const t_U32 cou32DevId, 
                                                          const tenAudioContext coenAudioCntxt,
                                                          t_Bool bReqFlag, 
                                                          const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclDiPOResourceMngr::vSetAccessoryAudioContext: Audio context --- flag : %d,context : %d",
         ETG_ENUM(BOOL,bReqFlag), ETG_ENUM(AUDIO_CONTEXT,coenAudioCntxt)));

   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   SPI_INTENTIONALLY_UNUSED(rfrcUsrCntxt);

   // This informations are keep to utilize for asynchronous responses
   m_rcUsrCntxt = rfrcUsrCntxt; // To keep the user context 
   t_Bool bStatus =false;

   //! Resource transfer requested only in any of these cases.
   // 1. The resource is with the mobile(controller).
   // 2. The last requested context is not same with the current one.
   if((NULL != m_poResArb) && 
      (NULL!= m_poAudioResourceMngr) &&
      ((m_u8CurAudioSource != coenAudioCntxt) ||
      (m_bCurAudioFlag != bReqFlag) ||
      (e8DIPO_ENTITY_MOBILE == m_CurrModeState.enAudio)))
   {
      trAccAudioContextMsg rAccAudioContextMsg;
      IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
      t_Bool bMasterTransfer = false;
      tenModeChangeReqType enModeChangeReqType = e8ACTUAL_MODE_CHANGE_REQUEST;
      tenAudioContext enDiPOAudioCntxt = coenAudioCntxt;

      if((e8DIPO_ENTITY_CAR == m_CurrModeState.enAudio) &&
         (e8DIPO_TRANSFERTYPE_TAKE == m_MasterAudioTransfer) &&
         (true == bReqFlag))
      {
         //! if it is an intermediate transfer send the info message request.
         //! Only take. No borrow, untake, unborrow.
         enModeChangeReqType = e8INFO_MODE_CHANGE_REQUEST;
      }
      else if ((e8DIPO_ENTITY_CAR == m_CurrModeState.enAudio) &&
         (m_MasterAudioContext != coenAudioCntxt) &&
		 (e8DIPO_TRANSFERTYPE_TAKE == m_MasterAudioTransfer) &&
         (false == bReqFlag))
      {
         //! If it is with flag = false, and is not with the  master context,
         //! update the same with master context.
         enDiPOAudioCntxt = m_MasterAudioContext;
         bReqFlag = true;
      }
      else
      {
         //! If the transfer is a master transfer, then forward it to the CarPlay plugin.
         bMasterTransfer = true;
      }
     
      bStatus = m_poResArb->bGetAudioModechangeMsg(enDiPOAudioCntxt, bReqFlag, rAccAudioContextMsg, enModeChangeReqType);

      //! Update the master video context parameter value.
      if((true == bMasterTransfer) && (true == bReqFlag))
      {
         m_MasterAudioContext = enDiPOAudioCntxt;
      }
      else if ((true == bMasterTransfer) && (false == bReqFlag))
      {
         m_MasterAudioContext = e8SPI_AUDIO_MAIN;
      }

      // ! Updating the master video transfer parameter value.
      if((true == bMasterTransfer) && (true == bReqFlag) &&(true == bStatus))
      {
         m_MasterAudioTransfer = rAccAudioContextMsg.rDiPOAudioContext.enTransferType;
      }
      else if ((true == bMasterTransfer) && (false == bReqFlag) &&(true == bStatus))
      {
         m_MasterAudioTransfer = e8DIPO_TRANSFERTYPE_NA;
      }

      //! update the transfer request info to the audio resource manager.
      m_poAudioResourceMngr->vSetDiPOTransferType(rAccAudioContextMsg.rDiPOAudioContext.enTransferType);

      ETG_TRACE_USR4(("AudioContext message: enAudioContext=%d  enBorrowConstraint = %d  "
            "enTakeConstraint = %d  enTransferPriority = %d  enTransferType =%d \n ",
            ETG_ENUM(AUDIO_CONTEXT, rAccAudioContextMsg.rDiPOAudioContext.enAudioContext),
            ETG_ENUM(DIPO_CONSTRAINT, rAccAudioContextMsg.rDiPOAudioContext.enBorrowConstraint),
            ETG_ENUM(DIPO_CONSTRAINT, rAccAudioContextMsg.rDiPOAudioContext.enTakeConstraint),
            ETG_ENUM(DIPO_TRANSFERPRIO,rAccAudioContextMsg.rDiPOAudioContext.enTransferPriority),
            ETG_ENUM(DIPO_TRANSFERTYPE,rAccAudioContextMsg.rDiPOAudioContext.enTransferType)));

      //! Send IPC message to CarPlay plugin adapter to request for audio resource transfer.
      trAccAudioContextMsg *pBuffer = (trAccAudioContextMsg *)oMessageQueue.vpCreateBuffer(sizeof(rAccAudioContextMsg));
      if((true == bStatus) && (NULL != pBuffer))
      {
         memcpy(pBuffer, &rAccAudioContextMsg, sizeof(rAccAudioContextMsg));
         bStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*)pBuffer, DIPO_MESSAGE_PRIORITY,(eMessageType) TCL_DATA_MESSAGE));
         ETG_TRACE_USR1(("IPC Message send status from vSetAccessoryAudioContext: %d",  ETG_ENUM(BOOL,bStatus)));

         if(false == bStatus)
         {
            //!The missed updates during startup time will keep in a queue
            // and sent it to the carplay deamon later. This is important
            // to process the subsequent requests in proper way.
            trAudioContextInfo rAudioContextInfo;
            rAudioContextInfo.bAudioFlag = bReqFlag;
            rAudioContextInfo.enAudioContext = coenAudioCntxt;
            m_rAudioContextInfo.push(rAudioContextInfo);

            //! Reset the Current audio context information,
            // if it failed to send the IPC message to CarPlay plugin.
            m_bCurAudioFlag = false;
            m_u8CurAudioSource = e8SPI_AUDIO_MAIN;
         }
         else //if(false == bStatus)
         {
            //! Update the latest sent display context to current context.
            m_u8CurAudioSource = coenAudioCntxt; 
            m_bCurAudioFlag = bReqFlag;
         }
      }
      else //if((true == bStatus) && (NULL != pBuffer))
      {
         ETG_TRACE_ERR(("Mode change request failed\n"));
      }

      //Destroy the buffer
      oMessageQueue.vDropBuffer(pBuffer);
   } //  if(NULL != m_poResArb)  

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetAccessoryAppState()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vSetAccessoryAppState(const tenSpeechAppState enSpeechAppState, 
                                                      const tenPhoneAppState enPhoneAppState,
                                                      const tenNavAppState enNavAppState, 
                                                      const trUserContext& rfrcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rfrcUsrCntxt);

   ETG_TRACE_USR4(("vSetAccessoryAppState with NAV = %d, PHONE = %d, SPEECH = %d", ETG_ENUM(NAV_APP_STATE, enNavAppState),
      ETG_ENUM(PHONE_APP_STATE, enPhoneAppState),  ETG_ENUM(SPEECH_APP_STATE, enSpeechAppState)));

   t_Bool bStatus =false;
   trAccAppStateMsg rAccAppStateMsg;
   rAccAppStateMsg.rDiPOAppState.enSpeechAppState = enSpeechAppState;
   rAccAppStateMsg.rDiPOAppState.enPhoneAppState = enPhoneAppState;
   rAccAppStateMsg.rDiPOAppState.enNavAppState = enNavAppState;
   rAccAppStateMsg.enMsgType = e8RM_APP_STATE_MESSAGE;

   m_rDiPOAppState =  rAccAppStateMsg.rDiPOAppState;

   // Send the data to the CarPlay adapter plugin
   IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
   trAccAppStateMsg *poAccAppStateMsg = (trAccAppStateMsg *)oMessageQueue.vpCreateBuffer(sizeof(rAccAppStateMsg));
   if(NULL != poAccAppStateMsg)
   {
      memcpy(poAccAppStateMsg, &rAccAppStateMsg, sizeof(rAccAppStateMsg));
      bStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*)poAccAppStateMsg, DIPO_MESSAGE_PRIORITY,(eMessageType) TCL_DATA_MESSAGE));
      ETG_TRACE_USR1(("IPC Message send status from vSetAccessoryAppState: %d", bStatus));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poAccAppStateMsg);
   }//if(NULL != poAccAppStateMsg)
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vUpdateInitialSettings()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vUpdateInitialSettings(trRsrcSettings rRsrcSettings)
{
   su32TimerInterval = rRsrcSettings.u32StartTimeInterval;
   m_enCPlayAutoLaunch = rRsrcSettings.enCPlayAutoLaunchFlag;

}

/* Private functions */

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vHandleSessionEnd()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vHandleSessionEnd()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   trDiPOModeState rDiPOModeState;
   spi_tclMediator *poMediator = spi_tclMediator::getInstance();
   if(NULL != poMediator)
   {
      //Trigger a device deselection to switch back to the host mode.
      poMediator->vPostAutoDeviceSelection(m_U32DevId, e8DEVCONNREQ_DESELECT);

   }
   rDiPOModeState.enScreen = e8DIPO_ENTITY_NA;
   rDiPOModeState.enAudio = e8DIPO_ENTITY_NA;
   rDiPOModeState.rSpeechState.enEntity = e8DIPO_ENTITY_NA;
   rDiPOModeState.rSpeechState.enSpeechMode = e8DIPO_SPEECHMODE_NA;
   rDiPOModeState.enPhone = e8DIPO_ENTITY_NA;
   rDiPOModeState.enNavigation = e8DIPO_ENTITY_NA;
   //! Update the display context
   vUpdateDisplayContext(rDiPOModeState);
   //! Update the audio context.
   vUpdateAudioContext(rDiPOModeState);
   //! Update the app states.(Clear all states)
   vUpdateAppState(rDiPOModeState);
   //Reset the falgs to default
   m_CurrModeState = rDiPOModeState;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vUpdateDisplayContext()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vUpdateDisplayContext(trDiPOModeState & rfoDiPOModeState)
{
   /*lint -esym(40,fvPostDeviceDisplayContext)fvPostDeviceDisplayContext Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRenderStatus = true;// True to render CarPlay, False otherwise
   tenDisplayContext enDispCntxt = e8DISPLAY_CONTEXT_UNKNOWN;

   if(m_CurrModeState.enScreen != rfoDiPOModeState.enScreen) // For Video response handling
   {
      if((e8DIPO_ENTITY_CAR == rfoDiPOModeState.enScreen) || 
         (e8DIPO_ENTITY_NA == rfoDiPOModeState.enScreen))
      {
         bRenderStatus = false; // To show native HMI send false to HMI
      }
      else if(e8DIPO_ENTITY_MOBILE == rfoDiPOModeState.enScreen)
      {
         bRenderStatus = true; // To show CarPlay screen send true to HMI
      }
      if(NULL != (m_rRsrcMngrCallback.fvPostDeviceDisplayContext))
      {
         (m_rRsrcMngrCallback.fvPostDeviceDisplayContext)(bRenderStatus, enDispCntxt, m_rcUsrCntxt);
         ETG_TRACE_USR4(("Device Display Context Update; Status: %d, Context: %d ", bRenderStatus, enDispCntxt));
      }
   } //if(m_CurrModeState.screen != rModeState.screen)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vUpdateAudioContext()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vUpdateAudioContext(trDiPOModeState & rfoDiPOModeState)
{

   /*lint -esym(40,fvPostDeviceAudioContext)fvPostDeviceAudioContext Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bPlayStatus = true;// True to render CarPlay, False otherwise

   if(m_CurrModeState.enAudio != rfoDiPOModeState.enAudio) // For Audio response handling
   {
      if((e8DIPO_ENTITY_CAR == rfoDiPOModeState.enAudio) ||
         (e8DIPO_ENTITY_NA == rfoDiPOModeState.enAudio))
      {
         bPlayStatus = false; //To play native audio send false to HMI
      }
      else if(e8DIPO_ENTITY_MOBILE == rfoDiPOModeState.enAudio)
      {
         bPlayStatus = true; // To play CarPlay audio send true to HMI
      }
      if(NULL != (m_rRsrcMngrCallback.fvPostDeviceAudioContext))
      {
        (m_rRsrcMngrCallback.fvPostDeviceAudioContext)(bPlayStatus, e8SPI_AUDIO_MAIN, m_rcUsrCntxt);
        ETG_TRACE_USR4(("Device Audio Context Update; Status: %d, Context: %d ", bPlayStatus, e8SPI_AUDIO_MAIN));
      } 
   } //if(m_CurrModeState.enAudio != rDiPOModeState.enAudio)
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vUpdateAppState()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vUpdateAppState(trDiPOModeState & rfoDiPOModeState)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   /*lint -esym(40,fvPostDeviceAppState)fvPostDeviceAppState Undeclared identifier */
   /*lint -esym(40,fvSetDeviceAppState)fvSetDeviceAppState Undeclared identifier */

   t_Bool bAppState = false;
   tenSpeechAppState enSpeechAppState = e8SPI_SPEECH_UNKNOWN;
   tenPhoneAppState enPhoneAppState   = e8SPI_PHONE_UNKNOWN;
   tenNavAppState enNavAppState       = e8SPI_NAV_UNKNOWN;

   if(NULL != m_poResArb) // For App state handling
   {
      vAcquireDevAppStateLock();

      bAppState = m_poResArb->bGetDeviceSpeechState( rfoDiPOModeState, m_CurrModeState, enSpeechAppState);
      bAppState = m_poResArb->bGetDevicePhoneState( rfoDiPOModeState, m_CurrModeState, enPhoneAppState) || bAppState;
      bAppState = m_poResArb->bGetDeviceNavState( rfoDiPOModeState, m_CurrModeState, enNavAppState) || bAppState;

      if((true == bAppState) && (NULL != m_rRsrcMngrCallback.fvSetDeviceAppState))
      {
         (m_rRsrcMngrCallback.fvSetDeviceAppState)(enSpeechAppState, enPhoneAppState, enNavAppState);
      }

      vReleaseDevAppStateLock();

      if((true == bAppState) && (NULL != (m_rRsrcMngrCallback.fvPostDeviceAppState)))
      {
         (m_rRsrcMngrCallback.fvPostDeviceAppState)(enSpeechAppState, enPhoneAppState, enNavAppState, m_rcUsrCntxt);
      }
   }//if(NULL != m_poResArb)
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngr::vSetVehicleBTAddress()
***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vSetVehicleBTAddress(t_String szBtAddress)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   trBtAddrUpdateMsg rBtUpdateMsg;

   if ((0 != strcmp(szBtAddress.c_str(), szBtMacAddress.c_str())) && (true
            == vFormatBTMacAddress(szBtAddress)))
   {
      strcpy(rBtUpdateMsg.cBTAddress, szBtAddress.c_str());
      szBtMacAddress = szBtAddress;
      t_Bool bStatus = bSendIPCMessage<trBtAddrUpdateMsg> (rBtUpdateMsg);
      ETG_TRACE_USR4((" BT address updates status = %d\n", bStatus));
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetFeatureRestrictions(...)
 ***************************************************************************/
t_Void spi_tclDiPOResourceMngr::vSetFeatureRestrictions(
      const t_U8 cou8ParkModeRestrictionInfo, const t_U8 cou8DriveModeRestrictionInfo)
{
   SPI_INTENTIONALLY_UNUSED(cou8ParkModeRestrictionInfo);

   //! Send the drive mode restriction data to CarPlay adapter if data has changed
   if (cou8DriveModeRestrictionInfo != m_u8DriveModeRestrictionInfo)
   {
      //! Store the latest drive mode restriction data
      m_u8DriveModeRestrictionInfo = cou8DriveModeRestrictionInfo;

      trRestrictionsUpdateMsg rRestrictionsUpdateMsg;
      rRestrictionsUpdateMsg.enMsgType = e8RESTRICTIONS_UPDATE_MESSAGE;
      rRestrictionsUpdateMsg.u8DriveRestrictionInfo = cou8DriveModeRestrictionInfo;

      //! Send the data to the CarPlay adapter plugin
      t_Bool bStatus = bSendIPCMessage<trRestrictionsUpdateMsg>(rRestrictionsUpdateMsg);
      ETG_TRACE_USR1(("IPC Message send status from vSetFeatureRestrictions: %d with Drive Restriction = %d",
            ETG_ENUM(BOOL,bStatus), rRestrictionsUpdateMsg.u8DriveRestrictionInfo));
   }//if (cou8DriveModeRestrictionInfo != m_u8DriveModeRestrictionInfo)
}

//Static
/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPOResourceMngr::bSessionStartTimerCb()
***************************************************************************/
t_Bool spi_tclDiPOResourceMngr::bSessionStartTimerCb(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData)
{
    ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
    SPI_INTENTIONALLY_UNUSED(timerID);
    SPI_INTENTIONALLY_UNUSED(pObject);
    SPI_INTENTIONALLY_UNUSED(pcoUserData);

    //!Cancel the start up timer if it still running. 
    // Note  that the cancel  will destroy the timer from the context.
    Timer* poTimer = Timer::getInstance();
    if ((NULL != poTimer) && (0 != srTimerIndex))
    {
       poTimer->CancelTimer(srTimerIndex);
       srTimerIndex =0;
    }//if ((NULL != poTimer) && (0 != srTimerIndex))

    if(e8DIPO_SESSION_START != m_enDiPOSessionState)
    {
       spi_tclMediator *poMediator = spi_tclMediator::getInstance();
       if(NULL != poMediator)
       {
          //Trigger a device deselection to switch back to the host mode.
          poMediator->vPostAutoDeviceSelection(m_U32DevId, e8DEVCONNREQ_DESELECT);
       }// if(NULL != poMediator)
    }//if(e8DIPO_SESSION_START != m_enDiPOSessionState)
    return true;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOResourceMngr::vFormatBTMacAddress()
 ***************************************************************************/
t_Bool spi_tclDiPOResourceMngr::vFormatBTMacAddress(t_String &szMACAddress)
{
   // Insert the ":" charecter in the address string begin from second position and after the two charecters.
   const t_U8 u8InitPos = 2;
   const t_U8 u8Offset = 3;
   t_Bool bStatus = false;

   //! If string is empty return false.
   if(! szMACAddress.empty())
   {
      bStatus = true;
   }

   for(t_U8 u8Index = u8InitPos; u8Index < szMACAddress.length(); u8Index+=u8Offset)
   {
      szMACAddress.insert(u8Index,":");
   }

   ETG_TRACE_USR1(("Formatted MAC address : %s", szMACAddress.c_str()));
   return bStatus;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::bSendIPCMessage()
 ***************************************************************************/
template<typename trMessage>
t_Bool spi_tclDiPOResourceMngr::bSendIPCMessage(trMessage rMessage)
{
   t_Bool bRetVal = false;
   IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);

   trMessage *poBuffer = (trMessage *)oMessageQueue.vpCreateBuffer(sizeof(trMessage));
   if(NULL != poBuffer)
   {
      memset(poBuffer, 0, sizeof(trMessage));
      memcpy(poBuffer, &rMessage, sizeof(trMessage));
      bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poBuffer, 
         DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poBuffer);
   }
   return bRetVal;
}
//lint –restore
