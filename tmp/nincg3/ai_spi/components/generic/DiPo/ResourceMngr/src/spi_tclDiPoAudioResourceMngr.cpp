/***********************************************************************/
/*!
 * \file  spi_tclDiPoAudioResourceMngr.cpp
 * \brief Manages DiPo Audio resource
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Manages DiPo Audio resource
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 20.08.2014  | Pruthvi Thej Nagaraju | Initial Version 
 02.03.2015  |  Ramya Murthy         | Added trigger for media release on Siri/Phone/Alert prepare 
                                       (Fix for GMMY16-25651)
06.05.2015  |Tejaswini HB                 |Lint Fix				
 02.12.2016  |  Ramya Murthy         | Changes to cleanup audio on SessionEnd or DeviceDeselection 
                                       (Fix for NCG3D-8442)	
 24.02.2016  |  Ramya Murthy         | Implementation of default audio type
									   

 \endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
#include "trcGenProj/Header/spi_tclDiPoAudioResourceMngr.cpp.trc.h"
#endif

#include "IPCMessageQueue.h"
#include "spi_tclDiPoAudioResourceMngr.h"
#include "spi_tclAudio.h"

static const t_Double scodAudioUnduck      = 1.0;
//static const t_Double scodAudioDuckMaxdB   = 120.0;
//static const t_Double scodAudioDuckDefdB   = 41.0;
static const t_Double scodAudioDuckMindB   = 0.0;
static const t_U16 scou16AudioRampDefDuration  = 1000;  //In msec

static const t_U8 scou8AudioDuckMaxdB = 120;
static const t_U8 scou8AudioDuckDefdB = 41;

//! Timer ID and flag of timer for restoration of last main audio source after Phone/Siri
static timer_t srAudRestoreTimerID;
static t_Bool bAudRestoreTimerRunning = false;

//! Timer ID and flag of timer for dellocation of Phone/Siri/Alert sources when resource is with CarPlay
static timer_t srAudReleaseTimerID;
static t_Bool bAudRelTimerRunning = false;

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef std::map<t_U16, tenAudioSamplingRate>::iterator tAudSampleRateMapItr;

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::spi_tclDiPoAudioResourceMngr
 ***************************************************************************/
spi_tclDiPoAudioResourceMngr::spi_tclDiPoAudioResourceMngr(
         spi_tclAudio* poAudio) : m_enCurrentAudioMode(e8DIPO_ENTITY_NA),
         m_enCurrAudioMainSrc(e8AUD_INVALID), m_enCurrAudioAltSrc(e8AUD_INVALID),
         m_poAudio(poAudio), m_enTransferType(e8DIPO_TRANSFERTYPE_NA),
         m_u32DeviceHandle(0), m_enAudioSamplingRate(e8AUD_SAMPLERATE_DEFAULT),
         m_bAudDuckEnabled(false)
{
   //! populate the sample rates map
   m_mapAudioSamplingRate[0]     = e8AUD_SAMPLERATE_DEFAULT;
   m_mapAudioSamplingRate[8000]  = e8AUD_SAMPLERATE_8KHZ;
   m_mapAudioSamplingRate[16000] = e8AUD_SAMPLERATE_16KHZ;
   m_mapAudioSamplingRate[24000] = e8AUD_SAMPLERATE_24KHZ;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::~spi_tclDiPoAudioResourceMngr
 ***************************************************************************/
spi_tclDiPoAudioResourceMngr::~spi_tclDiPoAudioResourceMngr()
{
   m_poAudio = NULL;
   m_bAudDuckEnabled    = false;
   m_enCurrAudioMainSrc = e8AUD_INVALID;
   m_enCurrAudioAltSrc  = e8AUD_INVALID;
   m_enCurrentAudioMode = e8DIPO_ENTITY_NA;
   m_enTransferType     = e8DIPO_TRANSFERTYPE_NA;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vOnAudioModeChange
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vOnAudioModeChange(const t_U32 cou32DeviceHandle, tenDiPOEntity enRequestedAudioMode)
{
   ETG_TRACE_USR4(("spi_tclDiPoAudioResourceMngr::vOnAudioModeChange: DeviceHandle = %d "
      "CurrentAudioMode = %d, RequestedAudioMode = %d, CurrAudioMainSrc = %d\n",
      cou32DeviceHandle,
      ETG_ENUM(DIPO_ENTITY_MODE, m_enCurrentAudioMode),
      ETG_ENUM(DIPO_ENTITY_MODE, enRequestedAudioMode),
      ETG_ENUM(AUDIO_DIRECTION, m_enCurrAudioMainSrc)));

   //! check if the audio mode has changed
   if ((m_enCurrentAudioMode != enRequestedAudioMode) && (NULL != m_poAudio))
   {
      Timer* poTimer = Timer::getInstance();
      if ((true == bAudRelTimerRunning) && (NULL != poTimer))
      {
         poTimer->CancelTimer(srAudReleaseTimerID);
         ETG_TRACE_USR4(("Aud Release Timer Stopped\n"));
         bAudRelTimerRunning = false;
      }//End of if ((true == bAudRelTimerRunning) && (NULL != poTimer))

      //! Requested Mode is MOBILE and current mode is NA/CAR.
      if(e8DIPO_ENTITY_MOBILE == enRequestedAudioMode)
      {
         m_enTransferType = e8DIPO_TRANSFERTYPE_BORROW;
         ETG_TRACE_USR4(("Mode Change: TransferType changed to %d\n", ETG_ENUM(DIPO_TRANSFERTYPE, m_enTransferType)));
      }//End of if(e8DIPO_ENTITY_MOBILE == enRequestedAudioMode)

      //! Requested Mode is NA/CAR and current mode is MOBILE.
      else
      {
         m_enTransferType = e8DIPO_TRANSFERTYPE_NA;
         ETG_TRACE_USR4(("Mode Change: TransferType changed to %d\n", ETG_ENUM(DIPO_TRANSFERTYPE, m_enTransferType)));

         if ((true == bAudRestoreTimerRunning) && (NULL != poTimer))
         {
            poTimer->CancelTimer(srAudRestoreTimerID);
            ETG_TRACE_USR4(("Aud Restore Timer Stopped\n"));
            bAudRestoreTimerRunning = false;
            m_poAudio->vRestoreLastMediaAudSrc();
         }//End of if ((true == bAudRestoreTimerRunning) && (NULL != poTimer))

         //! If the audio mode has changed from NA/CAR to MOBILE
         //! Do nothing
         //! Terminate audio  for current audio source(other than e8AUD_MAIN_OUT) if audio mode has changed from MOBILE to NA/CAR
         if (e8AUD_MAIN_OUT != m_enCurrAudioMainSrc)
         {
            m_poAudio->vTerminateAudio(cou32DeviceHandle, m_enCurrAudioMainSrc);
         }//End of if ((e8DIPO_ENTITY_MOBILE == m_enCurrentAudioMode) && ...)
         else
         {
            m_poAudio->vSendAudioStatusChange(e8AUDIO_STATUS_RELEASE_AUDIO_DEVICE);
         }

         m_enCurrAudioMainSrc = e8AUD_INVALID;
         m_poAudio->vSendAudioStatusChange(e8AUDIO_STATUS_MEDIA_RELEASE);
      }//End of Else

      m_enCurrentAudioMode = enRequestedAudioMode;
   }//End of if ((m_enCurrentAudioMode != enRequestedAudioMode) ..)

   ETG_TRACE_USR4(("spi_tclDiPoAudioResourceMngr::vOnAudioModeChange left with CurrentAudioMode = %d, CurrAudioMainSrc = %d \n",
         ETG_ENUM(DIPO_ENTITY_MODE, m_enCurrentAudioMode),ETG_ENUM(AUDIO_DIRECTION, m_enCurrAudioMainSrc)));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vOnAudioMsg
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioAllocMsg &rfrAudioAllocMsg)
{
   ETG_TRACE_USR4(("Received request %d with Audio channel %d and type %d  Current source: m_enCurrAudioMainSrc = %d\n",
            ETG_ENUM(AUDIO_REQUEST_TYPE,rfrAudioAllocMsg.enAudioReqType), rfrAudioAllocMsg.enAudioChannelType,
            ETG_ENUM(DIPO_AUDIO_TYPE, rfrAudioAllocMsg.enAudioType), ETG_ENUM(AUDIO_DIRECTION, m_enCurrAudioMainSrc)));

   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);

   if (e8ALLOCATION_REQUEST == rfrAudioAllocMsg.enAudioReqType)
   {
      vHandleAllocRequest(rfrAudioAllocMsg);
   }//End of if (e8ALLOCATION_REQUEST == (t_U8) rfrAudioAllocMsg.enAudioReqType)

   else if ((e8DEALLOCATION_REQUEST == rfrAudioAllocMsg.enAudioReqType))
   {
      vHandleDeallocRequest(rfrAudioAllocMsg.enAudioChannelType);
   } //else if(e8DEALLOCATION_REQUEST == (t_U8)rfrAudioAllocMsg.enAudioReqType)
}//End of t_Void spi_tclDiPoAudioResourceMngr::vOnAudioMsg(...)

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vHandleAllocRequest()
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vHandleAllocRequest(trAudioAllocMsg &rfrAudioAllocMsg)
{
   ETG_TRACE_USR1(("vHandleAllocRequest \n"));

   if(NULL != m_poAudio)
   {
      tenAudioSamplingRate enAudioSamplingRate = e8AUD_SAMPLERATE_DEFAULT;
      tenAudioDir enRequestedAudioSrc = e8AUD_INVALID;

      //!If ALTERNATE channel is requested from the device
      if (AudioChannelType_Alternate == (t_U8) rfrAudioAllocMsg.enAudioChannelType)
      {
         m_poAudio->vLaunchAudio(m_u32DeviceHandle, e8DEV_TYPE_DIPO, e8AUD_MIX_OUT,
                  enAudioSamplingRate);
         m_enCurrAudioAltSrc = e8AUD_MIX_OUT;
      }
      else if (AudioChannelType_Main == (t_U8) rfrAudioAllocMsg.enAudioChannelType)
      {
         //!If MAIN channel is requested from the device
         switch (rfrAudioAllocMsg.enAudioType)
         {
            case e8AUDIO_MEDIA:
            case e8AUDIO_ALERT:
            {
               enRequestedAudioSrc = (e8AUDIO_ALERT == rfrAudioAllocMsg.enAudioType)
                                         ? (e8AUD_ALERT_OUT) : (e8AUD_MAIN_OUT);
            }
               break;
            case e8AUDIO_DEFAULT:
            case e8AUDIO_TELEPHONY:
            case e8AUDIO_SPEECHREC:
            case e8AUDIO_SPOKEN:
            {
               if (e8AUDIO_DEFAULT == rfrAudioAllocMsg.enAudioType)
               {
                  enRequestedAudioSrc = e8AUD_DEFAULT;
               }
               else
               {
                  enRequestedAudioSrc = (e8AUDIO_TELEPHONY == rfrAudioAllocMsg.enAudioType)
                                           ? (e8AUD_PHONE_IN) : (e8AUD_VR_IN);
               }

               tAudSampleRateMapItr
                        itSampleRate = m_mapAudioSamplingRate.find(rfrAudioAllocMsg.rDiPOAudioFormat.u16SampleRate);

               enAudioSamplingRate = (m_mapAudioSamplingRate.end()!= itSampleRate)
                                         ? (itSampleRate->second) : (e8AUD_SAMPLERATE_DEFAULT);
            }
               break;
            default:
            {
               enRequestedAudioSrc = e8AUD_INVALID;
            }
            break;
         } //  End of switch((t_U8)rfrAudioAllocMsg.enAudioType)
         
         Timer* poTimer = Timer::getInstance();
         if ((true == bAudRelTimerRunning) && (NULL != poTimer))
         {
            poTimer->CancelTimer(srAudReleaseTimerID);
            ETG_TRACE_USR4(("Aud Release Timer Stopped\n"));
            bAudRelTimerRunning = false;
         }//End of if ((true == bAudRelTimerRunning) && (NULL != poTimer))

         if (enRequestedAudioSrc != m_enCurrAudioMainSrc)
         {
            //!If requested audio source is differs with the current active source.
            vProcessChangedMainAudSrc(enRequestedAudioSrc, enAudioSamplingRate);
         }//End of if (enRequestedAudioSrc != m_enCurrAudioMainSrc)
         else
         {
            //!If requested audio source is same as the current active source.
            vProcessUnchangedMainAudSrc(enAudioSamplingRate);
         }//End of else

         if (e8AUD_MAIN_OUT == enRequestedAudioSrc)
         {
            m_poAudio->vSendAudioStatusChange(e8AUDIO_STATUS_MEDIA_SETUP);
         }
         else if ((e8AUD_ALERT_OUT == enRequestedAudioSrc) ||
               (e8AUD_PHONE_IN == enRequestedAudioSrc) ||
               (e8AUD_VR_IN == enRequestedAudioSrc) ||
               (e8AUD_DEFAULT == enRequestedAudioSrc))
         {
            //@Note: This is to ensure cleanup is done for main audio if Siri/Phone/Alert prepare comes
            //after a dummy media prepare
            m_poAudio->vSendAudioStatusChange(e8AUDIO_STATUS_MEDIA_RELEASE);
         }
      } // End of else if (AudioChannelType_Main ==... )
   }//End of if(NULL != m_poAudio)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vHandleDeallocRequest()
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vHandleDeallocRequest(const AudioChannelType coenAudioChannelType)
{
   ETG_TRACE_USR1(("vHandleDeallocRequest\n"));

   if(NULL != m_poAudio)
   {
      if (AudioChannelType_Main == (t_U8) coenAudioChannelType)
      {
         m_poAudio->vStopAudioIn(m_enCurrAudioMainSrc);
         if ((e8AUD_MAIN_OUT == m_enCurrAudioMainSrc) || (e8AUD_INVALID == m_enCurrAudioMainSrc))
         {
            m_poAudio->vSendAudioStatusChange(e8AUDIO_STATUS_MEDIA_TEARDOWN);
         }
         else if (false == bAudRelTimerRunning)
         {
            Timer* poTimer = Timer::getInstance();
            if (NULL != poTimer)
            {
               poTimer->StartTimer(srAudReleaseTimerID, 5000, 0, this,
                                 &spi_tclDiPoAudioResourceMngr::bAudReleaseTimerCb, NULL);
               ETG_TRACE_USR4(("Aud Release Timer started\n"));
               bAudRelTimerRunning = true;
            }//End of if (NULL != poTimer)
         }//End of else if ((e8AUD_MAIN_OUT != m_enCurrAudioMainSrc) && (false == bAudRelTimerRunning))
      }//End of if (AudioChannelType_Main == (t_U8) coenAudioChannelType)

      //! If request is received to deallocate alternate channel,
      //! send terminate audio request for e8AUD_MIX_OUT
      if(AudioChannelType_Alternate == (t_U8)coenAudioChannelType)
      {
         m_poAudio->vTerminateAudio( m_u32DeviceHandle, e8AUD_MIX_OUT);
         m_enCurrAudioAltSrc = e8AUD_INVALID;
      }//End of if(AudioChannelType_Alternate == (t_U8)coenAudioChannelType)
   }//End of if(NULL != m_poAudio)
}

//!Static
/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::bAudRestoreTimerCb
 ***************************************************************************/
t_Bool spi_tclDiPoAudioResourceMngr::bAudRestoreTimerCb(
         timer_t rTimerID, t_Void *pvObject, const t_Void *pvUserData)
{
   SPI_INTENTIONALLY_UNUSED(pvUserData);
   SPI_INTENTIONALLY_UNUSED(rTimerID);
   ETG_TRACE_USR1((" spi_tclDiPoAudioResourceMngr::bAudRestoreTimerCb entered \n"));

   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
   {
      poTimer->CancelTimer(srAudRestoreTimerID);
      ETG_TRACE_USR4(("Aud Restore Timer stopped\n"));

      bAudRestoreTimerRunning = false;
   }//End of if (NULL != poTimer)

   spi_tclDiPoAudioResourceMngr* poAudResrcMngr =
            static_cast<spi_tclDiPoAudioResourceMngr*> (pvObject);

   if (NULL != poAudResrcMngr)
   {
      poAudResrcMngr->m_enTransferType = e8DIPO_TRANSFERTYPE_TAKE;

      /*t_U32 u32DevHandle = poAudResrcMngr->m_u32DeviceHandle;
      tenAudioDir enAudSrc = poAudResrcMngr->m_enCurrAudioMainSrc;

      //! If the current active source is not Entertainment source, then
      //! de-activate the current active source and activate the new requested source.
      if(e8AUD_MAIN_OUT != enAudSrc)
      {
         poAudResrcMngr->m_poAudio->vTerminateAudio(u32DevHandle, enAudSrc);
      }//End of if(e8AUD_MAIN_OUT != poAudResrcMngr->enAudSrc)

      poAudResrcMngr->m_poAudio->vLaunchAudio(u32DevHandle,
               e8DEV_TYPE_DIPO,
               e8AUD_MAIN_OUT,
               e8AUD_SAMPLERATE_DEFAULT);

      poAudResrcMngr->m_enCurrAudioMainSrc = e8AUD_MAIN_OUT;
      poAudResrcMngr->m_enAudioSamplingRate = e8AUD_SAMPLERATE_DEFAULT;*/
   }//End of if (NULL != poAudResrcMngr)

   return true;
}

//!Static
/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::bAudReleaseTimerCb
 ***************************************************************************/
t_Bool spi_tclDiPoAudioResourceMngr::bAudReleaseTimerCb(
         timer_t rTimerID, t_Void *pvObject, const t_Void *pvUserData)
{
   SPI_INTENTIONALLY_UNUSED(pvUserData);
   SPI_INTENTIONALLY_UNUSED(rTimerID);
   ETG_TRACE_USR1((" spi_tclDiPoAudioResourceMngr::bAudReleaseTimerCb entered \n"));

   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
   {
      poTimer->CancelTimer(srAudReleaseTimerID);
      ETG_TRACE_USR4(("Aud Release Timer expired\n"));

      bAudRelTimerRunning = false;
   }//End of if (NULL != poTimer)

   spi_tclDiPoAudioResourceMngr* poAudResrcMngr =
            static_cast<spi_tclDiPoAudioResourceMngr*> (pvObject);

   if ((NULL != poAudResrcMngr) && (NULL != poAudResrcMngr->m_poAudio))
   {
      poAudResrcMngr->m_poAudio->vTerminateAudio(poAudResrcMngr->m_u32DeviceHandle,
            poAudResrcMngr->m_enCurrAudioMainSrc);
      poAudResrcMngr->m_enCurrAudioMainSrc = e8AUD_INVALID;
   }//End of if ((NULL != poAudResrcMngr) && (NULL != poAudResrcMngr->m_poAudio))

   return true;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vProcessChangedMainAudSrc()
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vProcessChangedMainAudSrc(
         const tenAudioDir coenRequestedAudSrc,
         const tenAudioSamplingRate coenRequestedSamplingRate)
{
   ETG_TRACE_USR1(("vProcessChangedMainAudSrc TransferType %d\n", ETG_ENUM(DIPO_TRANSFERTYPE, m_enTransferType)));

   if (NULL != m_poAudio)
   {
      //! No Carplay Source was playing. Media Prepare received after Audio Transfer to Mobile
      //! INVALID -> MEDIA
      if ((e8AUD_MAIN_OUT == coenRequestedAudSrc) && (e8AUD_INVALID == m_enCurrAudioMainSrc) &&
               (e8DIPO_TRANSFERTYPE_BORROW == m_enTransferType))
      {
         m_enTransferType = e8DIPO_TRANSFERTYPE_TAKE;
         ETG_TRACE_USR4(("TransferType changed to %d\n", ETG_ENUM(DIPO_TRANSFERTYPE, m_enTransferType)));
         m_poAudio->vLaunchAudio(m_u32DeviceHandle, e8DEV_TYPE_DIPO, coenRequestedAudSrc,
                  coenRequestedSamplingRate);
      }//End of ((e8AUD_MAIN_OUT == coenRequestedAudSrc) && (e8AUD_INVALID == m_enCurrAudioMainSrc)..

      //! PHONE/SIRI/ALERT -> MEDIA
      else if ((e8AUD_MAIN_OUT == coenRequestedAudSrc) && (e8AUD_INVALID != m_enCurrAudioMainSrc) &&
               (e8DIPO_TRANSFERTYPE_BORROW == m_enTransferType))
      {
         m_poAudio->vTerminateAudio(m_u32DeviceHandle, m_enCurrAudioMainSrc);

         m_poAudio->vLaunchAudio(m_u32DeviceHandle, e8DEV_TYPE_DIPO, coenRequestedAudSrc, coenRequestedSamplingRate);

         Timer* poTimer = Timer::getInstance();
         if (NULL != poTimer)
         {
            poTimer->StartTimer(srAudRestoreTimerID, 3000, 0, this,
                  &spi_tclDiPoAudioResourceMngr::bAudRestoreTimerCb, NULL);

            ETG_TRACE_USR4(("Aud Restore Timer started\n"));

            bAudRestoreTimerRunning = true;
         }//End of if (NULL != poTimer)
      }//End of else if ((e8AUD_MAIN_OUT == coenRequestedAudSrc) && (e8AUD_INVALID != m_enCurrAudioMainSrc)...

      //! No Carplay Source was playing. Alert/Phone/Siri Audio Prepare received
      //! INVALID -> ALERT/PHONE/SIRI
      //! SIRI->PHONE, ALERT->PHONE
      else
      {
         ETG_TRACE_USR4(("Transfer Type Unchanged. Aud Restore Timer not started\n"));

         if (e8AUD_MAIN_OUT != m_enCurrAudioMainSrc)
         {
            m_poAudio->vTerminateAudio(m_u32DeviceHandle, m_enCurrAudioMainSrc);
         }

         m_poAudio->vLaunchAudio(m_u32DeviceHandle, e8DEV_TYPE_DIPO,
                  coenRequestedAudSrc, coenRequestedSamplingRate);
      }//End of Else

      m_enCurrAudioMainSrc  = coenRequestedAudSrc;
      m_enAudioSamplingRate = coenRequestedSamplingRate;
   }//End of if(NULL != m_poAudio)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vProcessUnchangedMainAudSrc()
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vProcessUnchangedMainAudSrc( const tenAudioSamplingRate coenRequestedSamplingRate)
{
   ETG_TRACE_USR1(("spi_tclDiPoAudioResourceMngr::vProcessUnchangedMainAudSrc\n"));

   //!If current source is Phone/Siri.
   if(((e8AUD_PHONE_IN == m_enCurrAudioMainSrc) || (e8AUD_VR_IN == m_enCurrAudioMainSrc) || (e8AUD_DEFAULT == m_enCurrAudioMainSrc)) &&
            (NULL != m_poAudio))
   {
      m_enAudioSamplingRate = coenRequestedSamplingRate;
      //! Reset the audio information
      m_poAudio->vLaunchAudio(m_u32DeviceHandle, e8DEV_TYPE_DIPO,
               m_enCurrAudioMainSrc, coenRequestedSamplingRate);
      //! Restart ecnr service with new sampling rate
      m_poAudio->vSetAudioInConfig(m_enCurrAudioMainSrc, coenRequestedSamplingRate);
      m_poAudio->vSetAlsaDevice(m_enCurrAudioMainSrc);
      m_poAudio->vStartAudioIn(m_enCurrAudioMainSrc);
   }//End of if (e8AUD_PHONE_IN == m_enCurrAudioMainSrc) ...)

   //! For all sources signal Audio Out adapter to unlock from 2 sec signaled wait
   IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
   trAudioSrcActiveMsg *poAudioActiveMsg =
            (trAudioSrcActiveMsg *)oMessageQueue.vpCreateBuffer(sizeof(trAudioSrcActiveMsg));
   if(NULL != poAudioActiveMsg)
   {
      poAudioActiveMsg->enMsgType = e8AUDIO_ACTIVE_MESSAGE;
      t_Bool bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poAudioActiveMsg,
               DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
      ETG_TRACE_USR4(("IPC Message send status from vProcessUnchangedAudSrc: %d", bRetVal));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poAudioActiveMsg);
   }//End of if(NULL != poAudioActiveMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vOnAudioDuckMsg
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vOnAudioDuckMsg(trAudioDuckMsg &rfrAudioDuckMsg)
{
   ETG_TRACE_USR1(("Received Audio Duck request with Final volume: %f ,Duration = %f\n",
            rfrAudioDuckMsg.dFinalVolume, rfrAudioDuckMsg.dDurationInMs));

   t_U8 u8RampVolIndB = 0;
   t_Bool bRetVal = false;

   if (NULL != m_poAudio)
   {
      if (scodAudioUnduck !=  rfrAudioDuckMsg.dFinalVolume)
      {
         //! Convert Relative value to Decibels.Info from ADIT
         //! rfrAudioDuckMsg.dFinalVolume = 1.0 -> Unduck
         //! <= -144 translates to 0.0
         //! > -144 .. < 0 translates to 0.2
         //!   >= 0 translates to 1.0 */
         u8RampVolIndB = (t_U8)((rfrAudioDuckMsg.dFinalVolume > scodAudioDuckMindB)
                           && (rfrAudioDuckMsg.dFinalVolume < scodAudioUnduck))
                                   ? (scou8AudioDuckDefdB) : (scou8AudioDuckMaxdB);

         bRetVal = m_poAudio->bSetAudioDucking((t_U16) rfrAudioDuckMsg.dDurationInMs,
                  u8RampVolIndB, e8_DUCKINGTYPE_DUCK,e8DEV_TYPE_DIPO);

         m_bAudDuckEnabled = true;
      }//End of if (scodAudioUnduck !=  rfrAudioDuckMsg.dFinalVolume)
      else
      {
         bRetVal = m_poAudio->bSetAudioDucking((t_U16) rfrAudioDuckMsg.dDurationInMs,
                  u8RampVolIndB, e8_DUCKINGTYPE_UNDUCK,e8DEV_TYPE_DIPO);

         m_bAudDuckEnabled = false;
      }//End of else
   } //if(NULL != m_poAudio)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vHandleSessionEnd
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vHandleSessionEnd()
{
   ETG_TRACE_USR4(("spi_tclDiPoAudioResourceMngr::vHandleSessionEnd: \n"));

   //@Note: DeviceHandle check ensures vHandleSessionEnd() is executed only once -
   //On Device deselection or Session end, whichever arrives first
   if (0 != m_u32DeviceHandle)
   {
      Timer* poTimer = Timer::getInstance();
      if (NULL != poTimer)
      {
         if (true == bAudRestoreTimerRunning)
         {
            poTimer->CancelTimer(srAudRestoreTimerID);
            bAudRestoreTimerRunning = false;
            ETG_TRACE_USR4(("Session Ended. Aud Restore Timer stopped\n"));
         }
         if (true == bAudRelTimerRunning)
         {
            poTimer->CancelTimer(srAudReleaseTimerID);
            ETG_TRACE_USR4(("Session Ended. Aud Release Timer stopped\n"));
            bAudRelTimerRunning = false;
         }
      }//End of if (NULL != poTimer)

      if(NULL != m_poAudio)
      {
         //! Set audio status to STATUS_MEDIA_RELEASE, on session termination.
         m_poAudio->vSendAudioStatusChange(e8AUDIO_STATUS_MEDIA_RELEASE);

         if (e8AUD_MAIN_OUT == m_enCurrAudioMainSrc)
         {
            m_poAudio->vSendAudioStatusChange(e8AUDIO_STATUS_RELEASE_AUDIO_DEVICE);
         }

         //! Terminate the currently active Main Audio source on session end
         m_poAudio->vTerminateAudio(m_u32DeviceHandle, m_enCurrAudioMainSrc);

         //! Terminate the currently active Alternate Audio source on session end
         m_poAudio->vTerminateAudio(m_u32DeviceHandle, m_enCurrAudioAltSrc);

         if(true == m_bAudDuckEnabled)
         {
            m_poAudio->bSetAudioDucking(scou16AudioRampDefDuration,
                     scou8AudioDuckDefdB, e8_DUCKINGTYPE_UNDUCK, e8DEV_TYPE_DIPO);
            m_bAudDuckEnabled = false;
         }//End of if(true = m_bAudDuckEnabled)
      }//End of if(NULL != m_poAudio)

      m_u32DeviceHandle = 0;
      m_enCurrAudioMainSrc  = e8AUD_INVALID;
      m_enCurrAudioAltSrc   = e8AUD_INVALID;
      m_enCurrentAudioMode  = e8DIPO_ENTITY_NA;
      m_enAudioSamplingRate = e8AUD_SAMPLERATE_DEFAULT;
      m_enTransferType      = e8DIPO_TRANSFERTYPE_NA;
   }//if (0 != m_u32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vSetDiPOTransferType
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vSetDiPOTransferType(const tenDiPOTransferType coenDiPOTransferType)
{
	 SPI_INTENTIONALLY_UNUSED(coenDiPOTransferType);
   /*m_enDiPOTransferType = coenDiPOTransferType;

   //! If the audio resource is taken by CAR set the current audio source to invalid
   m_enCurrAudioMainSrc =
         (e8DIPO_TRANSFERTYPE_TAKE == coenDiPOTransferType)?(e8AUD_INVALID):(m_enCurrAudioMainSrc);*/
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vOnSPISelectDeviceResult()
 ***************************************************************************/
t_Void spi_tclDiPoAudioResourceMngr::vOnSPISelectDeviceResult(
      t_U32 u32DevID, tenDeviceConnectionReq enDeviceConnReq,
      tenResponseCode enRespCode, tenErrorCode enErrorCode)
{
   SPI_INTENTIONALLY_UNUSED(enErrorCode);

   if ((e8DEVCONNREQ_SELECT == enDeviceConnReq) && (e8SUCCESS == enRespCode))
   {
      m_u32DeviceHandle = u32DevID;
   }
   else if ((e8DEVCONNREQ_DESELECT == enDeviceConnReq) && (u32DevID == m_u32DeviceHandle))
   {
      //@Note: DeviceHandle check ensures vHandleSessionEnd() is invoked here
      //only if Session End msg has not yet arrived
      vHandleSessionEnd();
   }
}//! end of spi_tclDiPoAudioResourceMngr::vOnSPISelectDeviceResult()

