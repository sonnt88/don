/*!
*******************************************************************************
* \file              Event.cpp
* \brief             Event handler
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Event  handler
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
02.04.2014 |  Shihabudheen P M            | Initial Version

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <sys/time.h>
#include "Event.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/Event.cpp.trc.h"
#endif 

/***************************************************************************
 ** FUNCTION:  Event::Event()
 ***************************************************************************/
Event::Event()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   pthread_mutex_init(&m_Mutext, NULL);
   pthread_cond_init(&m_CondVar, NULL);
}

/***************************************************************************
 ** FUNCTION:  Event::Event()
 ***************************************************************************/
Event::Event(const pthread_mutexattr_t MutexAttr, const pthread_condattr_t CondAttr)
 {
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   pthread_mutex_init(&m_Mutext, &MutexAttr);
   pthread_cond_init(&m_CondVar, &CondAttr);
 }

/***************************************************************************
 ** FUNCTION:  Event::~Event()
 ***************************************************************************/
Event::~Event()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   pthread_mutex_destroy(&m_Mutext);
   pthread_cond_destroy(&m_CondVar);
}

/***************************************************************************
 ** FUNCTION:  Event::vLock()
 ***************************************************************************/
void Event::vLock()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   pthread_mutex_lock(&m_Mutext);
}

/***************************************************************************
 ** FUNCTION:  Event::vUnLock()
 ***************************************************************************/
void Event::vUnLock()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   pthread_mutex_unlock(&m_Mutext);
}

/***************************************************************************
 ** FUNCTION:  Event::vWait()
 ***************************************************************************/
void Event::vWait()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   pthread_cond_wait(&m_CondVar,&m_Mutext);
}

/***************************************************************************
 ** FUNCTION:  Event::vTimedWait()
 ***************************************************************************/
void Event::vTimedWait(unsigned int u32WaitTime)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   if(0 != u32WaitTime)
   {
      struct timespec rTimeToWait;
      struct timeval rCurrentTime;

      // get current time
      gettimeofday(&rCurrentTime, NULL);

      // convert timeval to timespec
      rTimeToWait.tv_sec = rCurrentTime.tv_sec;
      rTimeToWait.tv_nsec = rCurrentTime.tv_usec * 1000;

      // update timeToWait with wait duration
      rTimeToWait.tv_sec += u32WaitTime / 1000;
      rTimeToWait.tv_nsec += ((u32WaitTime % 1000) * 1000000);
      if (rTimeToWait.tv_nsec > 1000000000)
      {
         rTimeToWait.tv_sec += rTimeToWait.tv_nsec / 1000000000;
         rTimeToWait.tv_sec += rTimeToWait.tv_nsec % 1000000000;
      }
      //condition wait will get releases when timeout occurs or if a signal get called
      pthread_cond_timedwait(&m_CondVar, &m_Mutext, &rTimeToWait);
   }
   else
   {
      // Wait until get a signal
      pthread_cond_wait(&m_CondVar,&m_Mutext);
   }
}

/***************************************************************************
 ** FUNCTION:  Event::vSignal()
 ***************************************************************************/
void Event::vSignal()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   pthread_cond_signal(&m_CondVar);
}