/*!
*******************************************************************************
* \file              spi_tclResourceArbitrator.h
* \brief             DiPO Resource arbitrator
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Resource arbitrator
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
29.03.2014 |  Shihabudheen P M            | Initial Version
19.06.2014 |  Shihabudheen P M            | Updated for App state handling
16.12.2014 |  Shihabudheen P M            | Changed resource transfer requests.

\endverbatim
******************************************************************************/

#ifndef SPI_TCLRESOURCEARBITRATOR_H
#define SPI_TCLRESOURCEARBITRATOR_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/

#include "DiPOTypes.h"

// Forward declaraions
class spi_tclDiPOContextHandler;

/****************************************************************************/
/*!
* \class spi_tclResourceArbitrator
* \brief DiPO Resource Arbitrator
*
* spi_tclResourceArbitrator  is the core of the DiPO resource arbitration. It 
* identify the resource transfer action, constraints and , priority based on
* audio/ video context input.
****************************************************************************/
class spi_tclResourceArbitrator
{
public:
  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceArbitrator::spi_tclResourceArbitrator()
   ***************************************************************************/
   /*!
   * \fn     spi_tclResourceArbitrator()
   * \brief  Constructor
   * \sa     ~spi_tclResourceArbitrator()
   **************************************************************************/ 
   spi_tclResourceArbitrator();

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceArbitrator::~spi_tclResourceArbitrator()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclResourceArbitrator()
   * \brief  Destructor
   * \sa     spi_tclResourceArbitrator()
   **************************************************************************/ 
   ~spi_tclResourceArbitrator();

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceArbitrator::bGetVideoModeChangeMsg()
   ***************************************************************************/
   /*!
   * \fn     bGetVideoModeChangeMsg()
   * \brief  Request for video video mode change. This will trigger a 
   * \       ModeChangeRequest to the device, because of the change in accessory
   * \       display context.
   * \param  enAccDisplayContext : [IN] Accessory display context 
   * \param  cfoAccDispContextMsg : [OUT] Video mode change message 
   * \param  enModeChangeReqType : [IN] Type of the mode change[info/actual]. 
   * \retVal  Bool, True if successfully create the context change request,
   *         False otherwise.
   * \sa    
   **************************************************************************/ 
   t_Bool bGetVideoModeChangeMsg(tenDisplayContext enAccDisplayContext, 
      t_Bool bReqStatus,
     trAccVideoContextMsg &cfoAccDispContextMsg, 
     tenModeChangeReqType enModeChangeReqType);

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceArbitrator::bGetAudioModechangeMsg()
   ***************************************************************************/
   /*!
   * \fn     bGetAudioModechangeMsg()
   * \brief  function to get a resource mode change message for audio context change  
   * \param  bReqStatus : [IN] Request status, True for request and False for release
   * \param  rfoAccDispContextMsg : [OUT] Message
   * \param  enModeChangeReqType : [IN] Type of the mode change[info/actual].
   * \retVal t_Bool : True if successfull, false otherwise
   * \sa    
   **************************************************************************/ 
   t_Bool bGetAudioModechangeMsg(const tenAudioContext coenAudioCntxt,
      t_Bool bReqStatus,
      trAccAudioContextMsg &rfoAccAudioContextMsg, 
      tenModeChangeReqType enModeChangeReqType);

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceArbitrator::bGetDeviceSpeechState()
   ***************************************************************************/
   /*!
   * \fn     bGetDeviceSpeechState()
   * \brief  Function to determine the Device Speech State
   * \param  crfoModeState : [IN] DiPO mode state info(New Mode).
   * \param  crfoCurrModeState : [IN] Current Mode State
   * \param  rfoSpeechAppState : [OUT] Speech state
   * \retVal t_Bool : True if there is a state change, false otherwise
   * \sa    
   **************************************************************************/ 
   t_Bool bGetDeviceSpeechState(const trDiPOModeState &crfoModeState, 
      const trDiPOModeState &crfoCurrModeState,
      tenSpeechAppState &rfoSpeechAppState);

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceArbitrator::bGetDevicePhoneState()
   ***************************************************************************/
   /*!
   * \fn     bGetDevicePhoneState()
   * \brief  Function to determine the Device Speech State
   * \param  crfoModeState : [IN] DiPO mode state info(New Mode).
   * \param  crfoCurrModeState : [IN] Current Mode State
   * \param  rfoPhoneAppState : [OUT] Phone App state
   * \retVal t_Bool : True if there is a state change, false otherwise
   * \sa    
   **************************************************************************/ 
   t_Bool bGetDevicePhoneState(const trDiPOModeState &crfoModeState,
      const trDiPOModeState &crfoCurrModeState,
      tenPhoneAppState &rfoPhoneAppState);

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceArbitrator::bGetDeviceNavState()
   ***************************************************************************/
   /*!
   * \fn     bGetDeviceNavState()
   * \brief  Function to determine the Device Speech State
   * \param  crfoModeState : [IN] DiPO mode state info(New Mode).
   * \param  crfoCurrModeState : [IN] Current Mode State
   * \param  rfoNavAppState : [OUT] Navigation App state
   * \retVal t_Bool : True if there is a state change, false otherwise
   * \sa    
   **************************************************************************/ 
   t_Bool bGetDeviceNavState(const trDiPOModeState &crfoModeState,
      const trDiPOModeState &crfoCurrModeState,
      tenNavAppState &rfoNavAppState);
 
  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/
private:

   //!Speech state
   tenSpeechAppState m_enSpeechAppState;

   //! Phone state
   tenPhoneAppState m_enPhoneAppState;

   //! Navigation state.
   tenNavAppState m_enNavAppState;

};

#endif
