/***********************************************************************/
/*!
* \file  spi_tclDiPoAudioResourceMngr.h
* \brief Manages DiPo Audio resource
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Manages DiPo Audio resource
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.08.2014  | Pruthvi Thej Nagaraju | Initial Version
\endverbatim
*************************************************************************/

#ifndef SPI_TCLDIPOAUDIORESOURCEMNGR_H_
#define SPI_TCLDIPOAUDIORESOURCEMNGR_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "DiPOTypes.h"
#include "Timer.h"

class spi_tclAudio;

/*!
 * \class spi_tclDiPoAudioResourceMngr
 * \brief Manages DiPo Audio resource
 */
class spi_tclDiPoAudioResourceMngr
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::spi_tclDiPoAudioResourceMngr
       ***************************************************************************/
      /*!
       * \fn     spi_tclDiPoAudioResourceMngr()
       * \brief  Default Constructor
       * \sa      ~spi_tclDiPoAudioResourceMngr()
       **************************************************************************/
      spi_tclDiPoAudioResourceMngr(spi_tclAudio* poAudio);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::~spi_tclDiPoAudioResourceMngr
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclDiPoAudioResourceMngr()
       * \brief  destructor
       * \sa     spi_tclDiPoAudioResourceMngr()
       **************************************************************************/
      ~spi_tclDiPoAudioResourceMngr();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vOnAudioModeChange
       ***************************************************************************/
      /*!
       * \fn     vOnAudioModeChange()
       * \brief  Informs when audio mode changes on the carplay device
       **************************************************************************/
      t_Void vOnAudioModeChange(const t_U32 cou32DeviceHandle, tenDiPOEntity enAudioMode);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vOnAudioDuckMsg
       ***************************************************************************/
      /*!
       * \fn     vOnAudioDuckMsg()
       * \brief  Informs when audio ducking request is received from phone
       **************************************************************************/
      t_Void vOnAudioDuckMsg(trAudioDuckMsg &rfrAudioDuckMsg);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vOnAudioMsg
       ***************************************************************************/
      /*!
       * \fn     vOnAudioMsg()
       * \brief  Informs when audiop allcation or deallocation request is received from phone
       **************************************************************************/
      t_Void vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioAllocMsg &rfrAudioAllocMsg);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vHandleSessionEnd
       ***************************************************************************/
      /*!
       * \fn     vHandleSessionEnd()
       * \brief  Informs when dipo session ends
       **************************************************************************/
      t_Void vHandleSessionEnd();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vSetDiPOTransferType
       ***************************************************************************/
      /*!
       * \fn     vSetDiPOTransferType()
       * \brief  Sets DiPO transfer type
       **************************************************************************/
      t_Void vSetDiPOTransferType(const tenDiPOTransferType coenDiPOTransferType);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vOnSPISelectDeviceResult()
       ***************************************************************************/
       /*!
       * \fn     vOnSPISelectDeviceResult()
        * \brief   Interface to receive result of SPI device selection/deselection
       * \param   u32DevID : [IN] Resource Manager callbacks structure.
       * \param   enDeviceConnReq : [IN] Select/ deselect.
       * \param   enRespCode : [IN] Response code (success/failure)
       * \param   enErrorCode : [IN] Error
       * \retval  t_Void
       **************************************************************************/
       t_Void vOnSPISelectDeviceResult(t_U32 u32DevID,
             tenDeviceConnectionReq enDeviceConnReq,
             tenResponseCode enRespCode,
             tenErrorCode enErrorCode);

   private:

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vProcessChangedAudSrc()
       ***************************************************************************/
      /*!
       * \fn     vProcessChangedMainAudSrc()
       * \brief  Processes the incoming Audio message if the source has changed
       **************************************************************************/
      t_Void vProcessChangedMainAudSrc(const tenAudioDir enRequestedAudSrc,
               const tenAudioSamplingRate enRequestedSamplingRate);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vProcessUnchangedAudSrc()
       ***************************************************************************/
      /*!
       * \fn     vProcessUnchangedMainAudSrc()
       * \brief  Processes the incoming Audio message if the source has not changed
       **************************************************************************/
      t_Void vProcessUnchangedMainAudSrc( const tenAudioSamplingRate enRequestedSamplingRate);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vHandleAllocRequest
       ***************************************************************************/
      /*!
       * \fn     vHandleAllocRequest()
       * \brief  Processes audio allocation request from the device.
       **************************************************************************/
      t_Void vHandleAllocRequest(trAudioAllocMsg &rfrAudioAllocMsg);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::vHandleDeallocRequest
       ***************************************************************************/
      /*!
       * \fn     vHandleDeallocRequest()
       * \brief  Processes audio deallocation request from the device.
       **************************************************************************/
      t_Void vHandleDeallocRequest(const AudioChannelType coenAudioChannelType);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::bAudRestoreTimerCb
       ***************************************************************************/
      /*!
       * \fn     bAudRestoreTimerCb
       * \brief  called on expiry of selection timer
       * \param  rTimerID: ID of the timer which has expired
       * \param  pvObject: pointer to object passed while starting the timer
       * \param  pvUserData: data passed during start of the timer
       **************************************************************************/
      static t_Bool bAudRestoreTimerCb(timer_t rTimerID, t_Void *pvObject,
               const t_Void *pvUserData);

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPoAudioResourceMngr::bAudReleaseTimerCb
       ***************************************************************************/
      /*!
       * \fn     bAudReleaseTimerCb
       * \brief  called on expiry of audio release timer
       * \param  rTimerID: ID of the timer which has expired
       * \param  pvObject: pointer to object passed while starting the timer
       * \param  pvUserData: data passed during start of the timer
       **************************************************************************/
      static t_Bool bAudReleaseTimerCb(timer_t rTimerID, t_Void *pvObject,
               const t_Void *pvUserData);

      //! Map to keep the Audio sampling rate info
      std::map<t_U16, tenAudioSamplingRate> m_mapAudioSamplingRate;

      //! Stores the currently active audio source
      tenDiPOEntity m_enCurrentAudioMode;

      //! Stores the currently active Main audio direction
      tenAudioDir m_enCurrAudioMainSrc;

      //! Stores the currently active Alternate audio direction
      tenAudioDir m_enCurrAudioAltSrc;

      //! pointer to audio class
      spi_tclAudio* m_poAudio;

      //! Sets dipo transfer type
      tenDiPOTransferType m_enTransferType;

      //! Stores current device used in carplay session
      t_U32 m_u32DeviceHandle;

      //! Stores the current sampling rate of the main channel
      tenAudioSamplingRate m_enAudioSamplingRate;

      //! Stores the enabled/disabled status of Audio Ducking feature.
      t_Bool m_bAudDuckEnabled;
	  
};


#endif /* SPI_TCLDIPOAUDIORESOURCEMNGR_H_ */
