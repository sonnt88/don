/*!
*******************************************************************************
* \file              spi_tclDiPOResourceMngr.h
* \brief             DiPO Resource manager
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Resource manager
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
24.03.2014 |  Shihabudheen P M            | Initial Version
05.04.2014 |  Priju K Padiyath            | Modified to handle display & audio context
19.06.2014 |  Shihabudheen P M            | Modified foe App state resource arbitration,
                                            session end handling
25.06.2014 |  Shihabudheen P M            | Adapted to the CarPlay design changes
16.12.2014 |  Shihabudheen P M            | Changed resource transfer requests.
11.02.2015 |  Shihabudheen P M            | Added timer implementation

\endverbatim
******************************************************************************/
#ifndef SPI_TCLDIPORESOURCEMNGR_H
#define SPI_TCLDIPORESOURCEMNGR_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <queue>
#include "Timer.h"
#include "BaseTypes.h"
#include "DiPOTypes.h"
#include "spi_tclResourceMngrBase.h"

class spi_tclDiPODeviceMsgRcvr;
class spi_tclResourceArbitrator;
class spi_tclDiPoAudioResourceMngr;

struct trAudioContextInfo
{
	tenAudioContext enAudioContext;
	t_Bool bAudioFlag;

	trAudioContextInfo()
	{
		enAudioContext = e8SPI_AUDIO_MAIN;
		bAudioFlag = false;
	}
};
/****************************************************************************/
/*!
* \class spi_tclDiPOResourceMngr
* \brief DiPO Resource manager class
*
* spi_tclDiPOResourceMngr is the resource manager class which consists of the 
* the logics to handle  DiPO resource arbitration related messages between HMI and device.
****************************************************************************/
class spi_tclDiPOResourceMngr: public spi_tclResourceMngrBase
{
public:
  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/
   
   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOResourceMngr::spi_tclDiPOResourceMngr()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOResourceMngr()
   * \brief  Constructor
   * \sa     ~spi_tclDiPOResourceMngr()
   **************************************************************************/
   spi_tclDiPOResourceMngr();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOResourceMngr::~spi_tclDiPOResourceMngr()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOResourceMngr()
   * \brief  Destructor
   * \sa     spi_tclDiPOResourceMngr()
   **************************************************************************/
   ~spi_tclDiPOResourceMngr();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOResourceMngr::vOnSPISelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn     vOnSPISelectDeviceResult()
    * \brief   Interface to receive result of SPI device selection/deselection
   * \param   u32DevID : [IN] Resource Manager callbacks structure.
   * \param   enDeviceConnReq : [IN] Select/ deselect.
   * \param   enRespCode : [IN] Response code (success/failure)
   * \param   enErrorCode : [IN] Error
   * \retval  t_Void 
   **************************************************************************/
   t_Void vOnSPISelectDeviceResult(t_U32 u32DevID, tenDeviceConnectionReq enDeviceConnReq,
         tenResponseCode enRespCode, tenErrorCode enErrorCode);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOResourceMngr::vOnModeChanged()
   ***************************************************************************/
   /*!
   * \fn     vOnModeChanged()
   * \brief  Function to update the device state 
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \param  rDiPOModeState : [IN] Device context data
   **************************************************************************/
   t_Void vOnModeChanged(const t_U32 cou32DeviceHandle,trDiPOModeState &rDiPOModeState);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vOnAudioMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnAudioMsg()
   * \brief  Function used to get post audio information from device
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \param  [IN] rfrAudioAllocMsg  : Audio info message
   * \sa
   **************************************************************************/
   t_Void vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioAllocMsg &rfrAudioAllocMsg);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vOnAudioMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnAudioMsg()
   * \brief  Function used to post audio duck message from device
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \param  [IN] rfrAudioDuckMsg  : Audio duck message
   * \sa
   **************************************************************************/
   t_Void vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioDuckMsg &rfrAudioDuckMsg);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOResourceMngr::vOnRequestUI()
   ***************************************************************************/
   /*!
   * \fn     vOnRequestUI()
   * \brief  Function to handle the requestUI message from the device
   * \param  bRenderStatus: [IN] UI redering request status
   **************************************************************************/
   t_Void vOnRequestUI(t_Bool bRenderStatus);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOResourceMngr::vOnSessionMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnSessionMsg()
   * \brief  Function to handle session updates from CarPlay plugin
   * \param  enDiPOSessionState : [IN] Session state
   * \
   **************************************************************************/
   t_Void vOnSessionMsg(tenDiPOSessionState enDiPOSessionState);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vRegRsrcMngrCallBack()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegRsrcMngrCallBack()
   * \brief   To register call back to the resource manager
   * \param   rRsrcMngrCallback : [IN] Callback functions holder
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vRegRsrcMngrCallBack(trRsrcMngrCallback rRsrcMngrCallback);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetAccessoryDisplayContext()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
   *        t_Bool bDisplayFlag, tenDisplayContext enDisplayContext)
   * \brief   To send accessory display context related info .
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   bDisplayFlag : [IN] Display flag
   * \pram    enDisplayContext : [IN] Display context
   * \pram    rfrcUsrCntxt: [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
      t_Bool bDisplayFlag, tenDisplayContext enDisplayContext,
      const trUserContext& rfrcUsrCntxt);

      /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOResourceMngr::vSetAccessoryDisplayMode(t_U32...
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryDisplayMode()
   * \brief  Accessory display mode update request.
   * \param  [IN] cu32DeviceHandle      : Uniquely identifies the Device.
   * \param  [IN] corDisplayContext : Display context info
   * \param  [IN] corDisplayConstraint : DiDisplay constraint info
   * \param  [IN] coenDisplayInfo       : Display info flag
   * \sa
   **************************************************************************/
   t_Void vSetAccessoryDisplayMode(const t_U32 cu32DeviceHandle,
      const trDisplayContext corDisplayContext,
      const trDisplayConstraint corDisplayConstraint,
      const tenDisplayInfo coenDisplayInfo);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetAccessoryAudioContext()
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryAudioContext(const t_U32 cou32DevId, const t_U8 cu8AudioCntxt
   *       t_Bool bDisplayFlag, const trUserContext& rfrcUsrCntxt)
   * \brief   To send accessory audio context related info .
   * \pram    cou32DevId   : [IN] Uniquely identifies the target Device.
   * \param   cu8AudioCntxt: [IN] Audio Context
   * \pram    bReqFlag     : [IN] Request/ Release flag
   * \pram    rfrcUsrCntxt : [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryAudioContext(const t_U32 cou32DevId, 
      const tenAudioContext coenAudioCntxt,
      t_Bool bReqFlag, 
      const trUserContext& rfrcUsrCntxt);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetAccessoryAppState()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetAccessoryAppState(tenSpeechAppState enSpeechAppState, tenPhoneAppState enPhoneAppState,
   *         tenNavAppState enNavAppState
   *
   * \brief   To set accessory app state realated info.
   * \pram    enSpeechAppState: [IN] Accessory speech state.
   * \param   enPhoneAppState : [IN] Accessory phone state
   * \pram    enNavAppState   : [IN] Accessory navigation app state
   * \pram    rfrcUsrCntxt    : [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryAppState(const tenSpeechAppState enSpeechAppState, 
      const tenPhoneAppState enPhoneAppState,
      const tenNavAppState enNavAppState, 
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOResourceMngr::vUpdateInitialSettings()
   ***************************************************************************/
   /*!
   * \fn     vUpdateInitialSettings()
   * \brief  Function to set the initial settings
   * \param  trRsrcSettings : [IN] Settings
   * \
   **************************************************************************/
   t_Void vUpdateInitialSettings(trRsrcSettings rRsrcSettings);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetVehicleBTAddress()
   ***************************************************************************/
   /*!
   * \fn      vSetVehicleBTAddress(t_Bool bLocDataAvailable)
   * \brief   Interface to update the vehicle BT address info update.
   * \param   szBtAddress: [IN] BT address.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetVehicleBTAddress(t_String szBtAddress);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vSetFeatureRestrictions()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSetFeatureRestrictions(const t_U8 cou8ParkModeRestrictionInfo,
    *          const t_U8 cou8DriveModeRestrictionInfo)
    * \brief   Method to set Vehicle Park/Drive Mode Restriction
    *          cou8ParkModeRestrictionInfo : Park mode restriction
    *          cou8DriveModeRestrictionInfo : Drive mode restriction
    * \retval  t_Void
    **************************************************************************/
   virtual t_Void vSetFeatureRestrictions(const t_U8 cou8ParkModeRestrictionInfo,
         const t_U8 cou8DriveModeRestrictionInfo);

   /***************************************************************************
   **************************END OF PUBLIC************************************
   ***************************************************************************/

private:

  /***************************************************************************
   ***************************PRIVATE*****************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vHandleSessionEnd()
   ***************************************************************************/
   /*!
   * \fn      t_Void vHandleSessionEnd()
   * \brief   Function to handle the session end update from CarPlay. This is
   *          used to trigger an automatic device deselection and thereby 
   *          deallocate all the allocated resources.
   * \param   rRsrcMngrCallback : [IN] Callback functions holder
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vHandleSessionEnd();

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vUpdateDisplayContext()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUpdateDisplayContext()
   * \brief   Function to examine the display resource ownership changes and,
   * \        update the HMI if required. 
   * \param   rfoDiPOModeState : [IN] CarPlay mode state
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vUpdateDisplayContext(trDiPOModeState & rfoDiPOModeState);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vUpdateAudioContext()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUpdateAudioContext()
   * \brief   Function to examine the audio resource ownership changes and,
   * \        update the HMI if required.     
   * \param   rfoDiPOModeState : [IN] CarPlay mode state
   * \retval  t_Void
   **************************************************************************/
   t_Void vUpdateAudioContext(trDiPOModeState & rfoDiPOModeState);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOResourceMngr::vUpdateAppState()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUpdateAppState()
   * \brief   Function to determine the CarPlay device app state changes and,
   * \        update it to the HMI if required 
   * \param   rfoDiPOModeState : [IN] mode state 
   * \retval  t_Void
   **************************************************************************/
   t_Void vUpdateAppState(trDiPOModeState & rfoDiPOModeState);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOResourceMngr::vFormatBTMacAddress
   ***************************************************************************/
   /*!
   * \fn     vFormatBTMacAddress()
   * \brief  This function is used to format the BT address to MAC address
   * \param  szMACAddress: [IN] MAC address
   * \retVal Bool : True if successfully transferred.
   **************************************************************************/
   t_Bool vFormatBTMacAddress(t_String &szMACAddress);

   /***************************************************************************
   ** FUNCTION: tBool spi_tclDiPOResourceMngr::bSessionStartTimerCb(
                       timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData);
   ***************************************************************************/
   /*!
   * \fn     bSessionStartTimerCb(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData)
   * \brief  Callback function that is called upon session start timer expiry
   * \param  timerID : [IN]Timer ID
   * \param  pObject : [IN]Object context for timer callback call
   * \param  pcoUserData:[IN] additional pointer to user supplied data which 
   * \       will be send back via user callback function when the timer expires 
   * \retVal  tBool
   * \sa     
   **************************************************************************/
   static t_Bool bSessionStartTimerCb(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData);
   
  /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOResourceMngr::bSendIPCMessage()
   ***************************************************************************/
   /*!
   * \fn     bSendIPCMessage(trMsgQBase &rfoMsgQBase)
   * \brief  Send the IPC message to SPI component.
   * \param  rMessage : [IN]Message data
   * \retVal  t_Bool : True if message send success, false otherwise
   * \sa     
   **************************************************************************/
   template<typename trMessage>
   t_Bool bSendIPCMessage(trMessage rMessage);

   //! Resourec arbitrator.
   spi_tclResourceArbitrator *m_poResArb;

   //! Callback functions to Resource manager
   trRsrcMngrCallback m_rRsrcMngrCallback;

   //! Current Display Context
   tenDisplayContext m_enCurDispCntxt;

   //! Current display flag
   t_Bool m_bCurDispFlag;

   //! Current audio flag
   t_Bool m_bCurAudioFlag;

   //! current audio source for resource arbitration
   tenAudioContext m_u8CurAudioSource;

   //! Current mode state of the CarPlay
   trDiPOModeState m_CurrModeState;

   //! Selected device ID
   static t_U32 m_U32DevId;

   //! User cntxt
   trUserContext m_rcUsrCntxt;

   //! Last allocated audio direction
   tenAudioDir m_enAudioDir;

   //! Current CarPlay session state
   static tenDiPOSessionState m_enDiPOSessionState;

   //! audio resource manager instance
   spi_tclDiPoAudioResourceMngr* m_poAudioResourceMngr;

   //!Master video context. It keeps the first video resource
   // transfer context to the accessory.
   tenDisplayContext m_MasterVideoContext;

   //!Master video transfer. The type of video transfer
   // happend at the first instance(borrow/take)
   tenDiPOTransferType m_MasterVideoTransfer;

   //! Master audio context. It keeps the first audio resource
   // transfer context to the accessory.
   tenAudioContext m_MasterAudioContext;

   //!Master audio transfer. The type of audio transfer
   // happend at the first instance(borrow/take)
   tenDiPOTransferType m_MasterAudioTransfer;

   //! Latest app state updated.
   trDiPOAppState m_rDiPOAppState;

   //! Latest Drive mode restriction
   t_U8 m_u8DriveModeRestrictionInfo;

   //! queue of audio context updates failed
   std::queue<trAudioContextInfo> m_rAudioContextInfo;

   tenAutoLaucnhFlag m_enCPlayAutoLaunch;

   /***************************************************************************
   **************************END OF PRIVATE************************************
   ***************************************************************************/
};


#endif
