/*!
*******************************************************************************
* \file              spi_tclDiPOKeyInputAdapterImpl.cpp
* \brief             DiPO Key Input Adapter implementation
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO key input adapter implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                       | Modifications
03.04.2014 |  Hari Priya E R               | Initial Version
26.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
15.07.2015 |  Sameer Chandra               | knob encoder implementation
\endverbatim
******************************************************************************/


/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "spi_tclDiPOKeyInputAdapterImpl.h"
#include "spi_tclDiPOAdapterConfig.cfg"
#include "Lock.h" 

#define SPI_ENABLE_DLT //enable DLT
#define SPI_LOG_CLASS Spi_CarPlay

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOKeyInputAdapterImpl.cpp.trc.h"
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//import the DLT context
LOG_IMPORT_CONTEXT(Spi_CarPlay);

//! Defines for Apple specific HID descriptor to readable
#define HID_USAGE_PAGE(a)                  0x05, (a)

#ifdef USING_HID_8BIT
#define HID_USAGE(a)                       0x09, (a)
#endif

#ifdef USING_HID_16BIT
#define HID_USAGE_16BIT(a, b)              0x0A, (a), (b) // Don't remove this directly for Lint Sake. Changes needs to be done here& in config files carefully to avoid Lint
#endif

#define HID_COLLECTION(a)                  0xA1, (a)
#define HID_END_COLLECTION()               0xC0
#define HID_REPORT_SIZE(size)              0x75, (size)
#define HID_REPORT_COUNT(count)            0x95, (count)
#define HID_INPUT(a)                       0x81, (a)
#define HID_LOGICAL_MINMAX(min, max)       0x15, (min), 0x25, (max)

/*! 
  * \static const t_U8 coConsumerKeyDescriptor
  * \Brief : This formulates the initial report about the consumer input device.
  */
#ifdef USING_CONSUMER_KEY
#define DIPO_CONSUMER_KEY_DESCRIPTOR
static const t_U8 coConsumerKeyDescriptor[] =
#include "spi_tclDiPOAdapterConfig.cfg"
#undef DIPO_CONSUMER_KEY_DESCRIPTOR
#endif

/*! 
  * \static const t_U8 TelephoneKeyInfo
  * \Brief : This formulates the initial report about the Telephone Key input device.
  */
#ifdef USING_TELEPHONY_KEY
#define DIPO_TELEPHONY_KEY_DESCRIPTOR
static const t_U8 coTelephonyKeyDescriptor[] =
#include "spi_tclDiPOAdapterConfig.cfg"
#undef DIPO_TELEPHONY_KEY_DESCRIPTOR
#endif

#ifdef USING_KNOBENCODER_KEY
#define KNOB_PRODUCT_ID  832;
#define KNOB_VENDOR_ID  52;
#define DIPO_KNOBENCODER_DESCRIPTOR
static const t_U8 coKnobKeyDescriptor[] =
#include "spi_tclDiPOAdapterConfig.cfg"
#undef DIPO_KNOBENCODER_DESCRIPTOR
#endif

spi_tclDiPOKeyInputAdapterImpl* spi_tclDiPOKeyInputAdapterImpl::m_poDiPOKeyInputAdapter = NULL;
t_Bool spi_tclDiPOKeyInputAdapterImpl::m_bKnobKeySupported = true;

/*@Note: The array consists of bitmap codes for consumer key
         in the order given below.
 u8KeyMapBitValues[0] = Key map for button play/pause toggle.
 u8KeyMapBitValues[1] = Key map for button next.
 u8KeyMapBitValues[2] = Key map for button previous.
 u8KeyMapBitValues[3] = Key map for button play.
 u8KeyMapBitValues[4] = Key map for button pause.
 u8KeyMapBitValues[5] = Key map for button AC Back
*/

#define DIPO_CONSUMER_KEYCODE_BITMAP
static t_U8 au8KeyMapBitValues [] = 
#include "spi_tclDiPOAdapterConfig.cfg"
#undef DIPO_CONSUMER_KEYCODE_BITMAP


/*@Note: The arry consists of bitmap codes for telephony keys
         in the order given below.
 au8TelKeyMapBitValues[0] = Key map for button Hook.
 au8TelKeyMapBitValues[1] = Key map for button Flash.
 au8TelKeyMapBitValues[2] = Key map for button Drop.
 */
#define DIPO_TELEPHONY_KEYCODE_BITMAP
static t_U8 au8TelKeyMapBitValues [] =
#include "spi_tclDiPOAdapterConfig.cfg"
#undef DIPO_TELEPHONY_KEYCODE_BITMAP

/*@Note The array consists of bitmap codes for Knob keys
 *      in the order given below
 * au8KnobKeyMapBitValues[0] = Key map for button 1
 * Note: Above button is used to denote a selection when
 *       knob is used.
 */
#define DIPO_KNOB_KEYCODE_BITMAP
static t_U8 au8KnobKeyMapBitValues [] =
#include "spi_tclDiPOAdapterConfig.cfg"
#undef DIPO_KNOB_KEYCODE_BITMAP

static Lock rKeyAdapterSyncLock;

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOKeyInputAdapterImpl::spi_tclDiPOKeyInputAdapterImpl()
 ***************************************************************************/
spi_tclDiPOKeyInputAdapterImpl::spi_tclDiPOKeyInputAdapterImpl() :
   m_poIInputReceiver(NULL), m_u8ConsumerKeydata(0), m_u8TelephoneKeydata(0)
{
   ETG_TRACE_USR1(("spi_tclDiPOKeyInputAdapterImpl::spi_tclDiPOKeyInputAdapterImpl entered"));
   
   rKeyAdapterSyncLock.s16Lock();
   m_poDiPOKeyInputAdapter = this;
   rKeyAdapterSyncLock.vUnlock();
   
   memset(&m_rHIDConsumerKeyDeviceInfo, 0, sizeof(trHIDDeviceInfo));
   memset(&m_rHIDTelKeyDeviceInfo, 0, sizeof(trHIDDeviceInfo));
   memset(&m_rHIDKnobKeyDeviceInfo,0,sizeof(trHIDDeviceInfo));
   memset(m_u8KnobKeydata,0,KNOB_DATA_SIZE);
}


/***************************************************************************
 ** FUNCTION:  spi_tclDiPOKeyInputAdapterImpl::~spi_tclDiPOKeyInputAdapterImpl()
 ***************************************************************************/
spi_tclDiPOKeyInputAdapterImpl::~spi_tclDiPOKeyInputAdapterImpl()
{
   ETG_TRACE_USR1(("spi_tclDiPOKeyInputAdapterImpl::~spi_tclDiPOKeyInputAdapterImpl entered"));
   
   rKeyAdapterSyncLock.s16Lock();
   m_poIInputReceiver = NULL;
   m_poDiPOKeyInputAdapter = NULL;
   m_u8ConsumerKeydata = 0;
   m_u8TelephoneKeydata = 0;
   rKeyAdapterSyncLock.vUnlock();
   
   memset(&m_rHIDConsumerKeyDeviceInfo, 0, sizeof(trHIDDeviceInfo));
   memset(&m_rHIDTelKeyDeviceInfo, 0, sizeof(trHIDDeviceInfo));
   memset(&m_rHIDKnobKeyDeviceInfo,0,sizeof(trHIDDeviceInfo));
   memset(m_u8KnobKeydata,0,KNOB_DATA_SIZE);


}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOKeyInputAdapterImpl* spi_tclDiPOKeyInputAdapterImpl::
 **            poGetDiPOKeyInputAdapterInstance()
 ***************************************************************************/
spi_tclDiPOKeyInputAdapterImpl* spi_tclDiPOKeyInputAdapterImpl::poGetDiPOKeyInputAdapterInstance()
{
      ETG_TRACE_USR1(("spi_tclDiPOKeyInputAdapterImpl::poGetDiPOKeyInputAdapterInstance entered"));
   rKeyAdapterSyncLock.s16Lock();
   spi_tclDiPOKeyInputAdapterImpl *poKeyInputAdapter = m_poDiPOKeyInputAdapter;
   rKeyAdapterSyncLock.vUnlock();
   return poKeyInputAdapter;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOKeyInputAdapterImpl::Initialize()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Bool spi_tclDiPOKeyInputAdapterImpl::Initialize(
         const IConfiguration& rfConfig, IInputReceiver& rfReceiver)
{
   ETG_TRACE_USR1(("spi_tclDiPOKeyInputAdapterImpl::Initialize entered"));
   t_Bool bRetVal = false;
	
   rKeyAdapterSyncLock.s16Lock();	
   m_poIInputReceiver = &rfReceiver;
   SPI_NORMAL_ASSERT(NULL == m_poIInputReceiver);

#ifdef USING_CONSUMER_KEY
   {
      // populate the Consumer Key device info.
      m_rHIDConsumerKeyDeviceInfo.HIDProductId
         = rfConfig.GetNumber("key-input-hid-consumer-product-id", 0LL);
      m_rHIDConsumerKeyDeviceInfo.HIDVendorId
         = rfConfig.GetNumber("key-input-hid-consumer-vendor-id", 0LL);
      m_rHIDConsumerKeyDeviceInfo.Name
         = rfConfig.GetItem("key-input-consumer-device-name", "consumer-keys").c_str();
      m_rHIDConsumerKeyDeviceInfo.UUID
         = rfConfig.GetItem("key-input-hid-consumer-uuid", "").c_str();
      m_rHIDConsumerKeyDeviceInfo.HIDCountryCode
         = rfConfig.GetNumber("key-input-hid-country-code", 0LL);
      m_rHIDConsumerKeyDeviceInfo.HIDDescriptor = coConsumerKeyDescriptor;
      m_rHIDConsumerKeyDeviceInfo.HIDDescriptorLen = sizeof(coConsumerKeyDescriptor);
      // Attach the Consumer Input Device Info
      bRetVal = m_poIInputReceiver->AttachInput(m_rHIDConsumerKeyDeviceInfo);
      ETG_TRACE_USR2(("[DESC]: HID consumer device attach status  = %d", ETG_ENUM(STATUS, bRetVal)));
   }
#endif


#ifdef USING_TELEPHONY_KEY
   {
      // populate the Telephone Key device info.
      m_rHIDTelKeyDeviceInfo.HIDProductId =
         rfConfig.GetNumber("key-input-hid-telephony-product-id", 0LL);
      m_rHIDTelKeyDeviceInfo.HIDVendorId =
         rfConfig.GetNumber("key-input-hid-telephony-vendor-id", 0LL);
      m_rHIDTelKeyDeviceInfo.Name =
         rfConfig.GetItem("key-input-telephony-device-name","telephony-keys").c_str();
      //Since the UUID value for telephony key input in dipo.cfg is invalidly formatted,use another UUID
      m_rHIDTelKeyDeviceInfo.UUID =
         rfConfig.GetItem("key-input-hid-telephony-uuid ", "e3a8f0b8-d7c1-4d94-bdde-40bbfdcf6c05").c_str();
      m_rHIDTelKeyDeviceInfo.HIDCountryCode
         = rfConfig.GetNumber("key-input-hid-country-code", 0LL);

      m_rHIDTelKeyDeviceInfo.HIDDescriptor = coTelephonyKeyDescriptor;
      m_rHIDTelKeyDeviceInfo.HIDDescriptorLen = sizeof(coTelephonyKeyDescriptor);
      bRetVal = m_poIInputReceiver->AttachInput(m_rHIDTelKeyDeviceInfo);
      ETG_TRACE_USR2(("[DESC]: HID telephony device attach status  = %d", ETG_ENUM(STATUS, bRetVal)));
   }
#endif

#ifdef USING_KNOBENCODER_KEY
   if(true == m_bKnobKeySupported)
   {
      //populate the Knob Key device info.
      m_rHIDKnobKeyDeviceInfo.HIDProductId = KNOB_PRODUCT_ID;

      m_rHIDKnobKeyDeviceInfo.HIDVendorId = KNOB_VENDOR_ID;

      m_rHIDKnobKeyDeviceInfo.Name =
         rfConfig.GetItem("key-input-knob-device-name","knob-key").c_str();
      //UUID for Knob key has been randomly generated.
      m_rHIDKnobKeyDeviceInfo.UUID =
         rfConfig.GetItem("key-input-hid-knob-uuid", "dc68c8c0-5d31-4c81-b184-a1529959ce8d").c_str();
      m_rHIDKnobKeyDeviceInfo.HIDCountryCode
         = rfConfig.GetNumber("key-input-hid-country-code", 0LL);
      m_rHIDKnobKeyDeviceInfo.HIDDescriptor = coKnobKeyDescriptor;

      m_rHIDKnobKeyDeviceInfo.HIDDescriptorLen = sizeof(coKnobKeyDescriptor);

      bRetVal = m_poIInputReceiver->AttachInput(m_rHIDKnobKeyDeviceInfo);
      ETG_TRACE_USR2(("[DESC]: Knob encoder attach status  = %d", ETG_ENUM(STATUS, bRetVal)));
   }
#endif
   rKeyAdapterSyncLock.vUnlock();

   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclDiPOKeyInputAdapterImpl::bSendKeyEvent()
***************************************************************************/
t_Bool spi_tclDiPOKeyInputAdapterImpl::bSendKeyEvent(tenKeyMode enKeyMode,tenKeyCode enKeyCode)
{
   ETG_TRACE_USR1(("spi_tclDiPOKeyInputAdapterImpl::bSendKeyEvent entered"));
   ETG_TRACE_USR4(("[PARAM]:bSendKeyEvent - Key Mode  = %d", enKeyMode));
   ETG_TRACE_USR4(("[PARAM]:bSendKeyEvent - Key code  = %d", enKeyCode));
   // HID device consumer Key Input report
   trHIDInputReport rHIDConsumerKeyInputReport;
   
   // HID device Telephone Key Input report
   trHIDInputReport rHIDTelKeyInputReport;

   t_Bool bIsValidKey = false;
   t_Bool bRetVal = false;

   //!Flag that checks if the key received is a consumer key
   t_Bool bIsConsumerKey = true;

   /*While populating the key data descriptor,set the bit corresponding to the key to 1,if the key is pressed.
   If the key is released,set the corresponding bit to 0 */
   switch(enKeyCode)
   {

   case e32DEV_BACKWARD:
      {
    	 if(0 != au8KeyMapBitValues[5])
    	 {
    		 m_u8ConsumerKeydata = (e8KEY_PRESS==enKeyMode)?(m_u8ConsumerKeydata|au8KeyMapBitValues[5]):(m_u8ConsumerKeydata^au8KeyMapBitValues[5]);
    		 bIsValidKey = true;
    	 }
         
      }
      break;
   case e32MULTIMEDIA_NEXT:
      {
         if(0 != au8KeyMapBitValues[1])
         {
            m_u8ConsumerKeydata = (e8KEY_PRESS==enKeyMode)?(m_u8ConsumerKeydata|au8KeyMapBitValues[1]):(m_u8ConsumerKeydata^au8KeyMapBitValues[1]);
            bIsValidKey = true;
         }
         
      }
      break;

   case e32MULTIMEDIA_PREVIOUS:
      {
         if(0 != au8KeyMapBitValues[2])
         {
            m_u8ConsumerKeydata = (e8KEY_PRESS==enKeyMode)?(m_u8ConsumerKeydata|au8KeyMapBitValues[2]):(m_u8ConsumerKeydata^au8KeyMapBitValues[2]);
            bIsValidKey = true;
         }
         
      }
      break;

   case e32MULTIMEDIA_PLAY:
      {
         if(0 != au8KeyMapBitValues[0])
         {
            m_u8ConsumerKeydata = (e8KEY_PRESS==enKeyMode)?(m_u8ConsumerKeydata|au8KeyMapBitValues[0]):(m_u8ConsumerKeydata^au8KeyMapBitValues[0]);
            bIsValidKey = true;
         }
         else if (0 != au8KeyMapBitValues[3])
         {
        	 m_u8ConsumerKeydata = (e8KEY_PRESS==enKeyMode)?(m_u8ConsumerKeydata|au8KeyMapBitValues[3]):(m_u8ConsumerKeydata^au8KeyMapBitValues[3]);
        	 bIsValidKey = true;
         }
      }
      break;

   case e32DEV_PHONE_CALL:
      {
         if(0 != au8TelKeyMapBitValues[0])
         {
            m_u8TelephoneKeydata = (e8KEY_PRESS==enKeyMode)?(m_u8TelephoneKeydata|au8TelKeyMapBitValues[0]):(m_u8TelephoneKeydata^au8TelKeyMapBitValues[0]);
            bIsValidKey = true;
         }
         bIsConsumerKey = false;
      }
      break;

   case e32DEV_PHONE_END:
      {
         if (0 != au8TelKeyMapBitValues[2])
         {
            m_u8TelephoneKeydata = (e8KEY_PRESS==enKeyMode)?(m_u8TelephoneKeydata|au8TelKeyMapBitValues[2]):(m_u8TelephoneKeydata^au8TelKeyMapBitValues[2]);
            bIsValidKey = true;
         }

         bIsConsumerKey = false;
      }
      break;
   case e32DEV_PHONE_FLASH:
      {
         if(0 != au8TelKeyMapBitValues[1])
         {
            m_u8TelephoneKeydata = (e8KEY_PRESS == enKeyMode) ? (m_u8TelephoneKeydata|au8TelKeyMapBitValues[1]) : (m_u8TelephoneKeydata^ au8TelKeyMapBitValues[1]);
            bIsValidKey = true;

         }
         bIsConsumerKey = false;
      }
      break;
   case e32DEV_MENU:
      {
         if (0!= au8KnobKeyMapBitValues[0])
         {
            m_u8KnobKeydata[0] = (e8KEY_PRESS==enKeyMode)?(m_u8KnobKeydata[0]|au8KnobKeyMapBitValues[0]):(m_u8KnobKeydata[0]^au8KnobKeyMapBitValues[0]);
            //Send the selection action via knob key HID descriptor only.
            bSendKnobKeyEvent();
         }
      }
      break;

   default:
      {
         ETG_TRACE_ERR(("[ERR]: No valid key found to Send to iPhone"));
      }
      break;
   }
	
   rKeyAdapterSyncLock.s16Lock();
   if((NULL!= m_poIInputReceiver) && (true == bIsValidKey))
   {
      if(true == bIsConsumerKey)
      {
         //Populate the HID input report for Consumer Key data
         rHIDConsumerKeyInputReport.HIDReportData = &m_u8ConsumerKeydata;
         rHIDConsumerKeyInputReport.HIDReportLen = sizeof(m_u8ConsumerKeydata);
         rHIDConsumerKeyInputReport.UUID = m_rHIDConsumerKeyDeviceInfo.UUID;

         bRetVal = m_poIInputReceiver->SendInput(rHIDConsumerKeyInputReport);
      }
      else
      {
         //Populate the HID input report for Telephone Key data
         rHIDTelKeyInputReport.HIDReportData = &m_u8TelephoneKeydata;
         rHIDTelKeyInputReport.HIDReportLen = sizeof(m_u8TelephoneKeydata);
         rHIDTelKeyInputReport.UUID = m_rHIDTelKeyDeviceInfo.UUID;

         bRetVal = m_poIInputReceiver-> SendInput(rHIDTelKeyInputReport);
      }
   }
   rKeyAdapterSyncLock.vUnlock();
   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclDiPOKeyInputAdapterImpl::bSendKnobKeyEvent()
***************************************************************************/
t_Bool spi_tclDiPOKeyInputAdapterImpl::bSendKnobKeyEvent(t_S8 s8EncoderDeltaCounts)
{
   ETG_TRACE_USR1(("spi_tclDiPOKeyInputAdapterImpl::bSendKnobKeyEvent entered"));

   ETG_TRACE_USR4(("[PARAM]:bSendKeyEvent - Delta counts  = %d", s8EncoderDeltaCounts));
   /**
    * @Note : The HID descriptor for Knob key actually looks like
    *
    *      Byte 1           Byte 2
    *  _ _ _ _ _ _ _ _   _ _ _ _ _ _ _ _
    * |_|_|_|_|_|_|_|x| |x|x|x|x|x|x|x|x|
    *
    * $Byte 1
    *  0th position - Button 1 (Used for selection)
    * $Byte 2
    *  8 bits for encoder change in delta counts(signed).
    *
    * @Note
    * To denote the same in HID descriptor and maintain bit alignment
    * added Inverse Report Count and Report Size.
    */

   t_Bool bRetVal = false;
   // HID device Telephone Key Input report
   trHIDInputReport rHIDKnobKeyInputReport;
   
    rKeyAdapterSyncLock.s16Lock();  
   //Assign the deltaCounts to the second byte of the HID array.
   if ((true == m_bKnobKeySupported) && (NULL != m_poIInputReceiver))
   {
      m_u8KnobKeydata[1] = s8EncoderDeltaCounts;

      rHIDKnobKeyInputReport.HIDReportData = (uint8_t*)(m_u8KnobKeydata);
      rHIDKnobKeyInputReport.HIDReportLen = sizeof(m_u8KnobKeydata);
      rHIDKnobKeyInputReport.UUID = m_rHIDKnobKeyDeviceInfo.UUID;

      bRetVal = m_poIInputReceiver-> SendInput(rHIDKnobKeyInputReport);

      ETG_TRACE_USR2(("[DESC]: Knob key event sent status = %d", ETG_ENUM(STATUS, bRetVal)));
   }
   rKeyAdapterSyncLock.vUnlock();

   return bRetVal;
}


/***************************************************************************
** FUNCTION: t_Void spi_tclDiPOKeyInputAdapterImpl::vSetDisplayInputParam()
***************************************************************************/
t_Void spi_tclDiPOKeyInputAdapterImpl::vSetDisplayInputParam(t_U8 u8DisplayInput)
{
   ETG_TRACE_USR1(("spi_tclDiPOKeyInputAdapterImpl::vSetDisplayInputParam entered"));
   //@Note: expand the switch case for any other input capability handling
   switch (u8DisplayInput)
   {
      case LOW_FIDELITY_TOUCH_WITH_KNOB:
      case HIGH_FIDELITY_TOUCH_WITH_KNOB:
      {
         m_bKnobKeySupported = true;
      }
         break;
      case LOW_FIDELITY_TOUCH:
      case HIGH_FIDELITY_TOUCH:
      {
         m_bKnobKeySupported = false;
      }
         break;
      default:
      {
         ETG_TRACE_ERR(("[ERR]:Invalid display input entry received"));
      }
         break;
   }
}
