/*!
*******************************************************************************
* \file              spi_tclInputReceiverHelper.h
* \brief             DiPO Input receiver helper class
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO input receiver helper implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
5.3.2014  |  Shihabudheen P M            | Initial Version
04.04.2014|  Hari Priya E R              | Included Changes for hard key handling
26.05.2014| Hari Priya E R               | Included changes to read Screen size values
                                           from EOL Handling class

\endverbatim
******************************************************************************/


/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/

#include <unistd.h>
#include "spi_tclInputReceiverHelper.h"
#include "spi_tclDiPOReceiverHelper.h"
#include "spi_tclConfigReader.h"

//using namespace spi::io;
#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclInputReceiverHelper.cpp.trc.h"
#endif





static const t_U16 u16MaxSixteenBitvalue = 32767;
static const t_U8  u8BitMaskValue = 255;
static const t_U8 u8BackKeySetValue = 32;
static const t_U8 u8NextKeySetValue = 2;
static const t_U8 u8PrevKeySetValue = 4;
static const t_U8 u8PlayKeySetValue = 1;

/***************************************************************************
 ** FUNCTION:  spi_tclInputReceiverHelper::spi_tclInputReceiverHelper()
 ***************************************************************************/
spi_tclInputReceiverHelper::spi_tclInputReceiverHelper()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   m_u8ConsumerKeydata =0;
   memset(&m_u8TelephoneKeydata,0,TELEPHONE_KEYDATA_SIZE);
}

/***************************************************************************
 ** FUNCTION:  spi_tclInputReceiverHelper::spi_tclInputReceiverHelper()
 ***************************************************************************/
spi_tclInputReceiverHelper::~spi_tclInputReceiverHelper()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   m_u8ConsumerKeydata =0;
   memset(&m_u8TelephoneKeydata,0,TELEPHONE_KEYDATA_SIZE);

}


/***************************************************************************
 ** FUNCTION:  spi_tclInputReceiverHelper::vSetInputReceiverHandler()
 ***************************************************************************/
t_Void spi_tclInputReceiverHelper::vSetInputReceiverHandler(IInputReceiver *poInputReceiver)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   m_poInputReceiver = poInputReceiver;
}

/***************************************************************************
 ** FUNCTION:  spi_tclInputReceiverHelper::poGetControlReceiverHandler()
 ***************************************************************************/
IInputReceiver* spi_tclInputReceiverHelper::poGetControlReceiverHandler()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   
}


/***************************************************************************
 ** FUNCTION:  spi_tclInputReceiverHelper::bAttchInputDevice()
 ***************************************************************************/
t_Bool spi_tclInputReceiverHelper::bAttchInputDevice(tenInputType enInputType,
                                                     const trHIDDeviceInfo rHIDDeviceInfo)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   t_Bool bRetVal = false;

   switch(enInputType)
   {
   case e8TOUCH:
      {
         // Populate the HID descriptor for single touch
         m_rHIDTouchDeviceInfo = rHIDDeviceInfo;
         m_rHIDTouchDeviceInfo.HIDDescriptor = coSingleTouchDescriptor;
         m_rHIDTouchDeviceInfo.HIDDescriptorLen = sizeof(coSingleTouchDescriptor);
         if(NULL != m_poInputReceiver)
         {
            bRetVal = m_poInputReceiver->AttachInput(m_rHIDTouchDeviceInfo);
         }
      }
      break;
   case e8CONSUMER_KEY:
      {
         // Populate the HID descriptor for consumer key
         m_rHIDConsumerKeyDeviceInfo = rHIDDeviceInfo;
         m_rHIDConsumerKeyDeviceInfo.HIDDescriptor = coConsumerKeyDescriptor;
         m_rHIDConsumerKeyDeviceInfo.HIDDescriptorLen = sizeof(coConsumerKeyDescriptor);
         if(NULL != m_poInputReceiver)
         {
            bRetVal = m_poInputReceiver->AttachInput(m_rHIDConsumerKeyDeviceInfo);
         }
      }
      break;

   case e8TELEPHONE_KEY:
      {
         // Populate the HID descriptor for telephone key
         m_rHIDTelKeyDeviceInfo = rHIDDeviceInfo;
         m_rHIDTelKeyDeviceInfo.HIDDescriptor = coTelephonyKeyDescriptor;
         m_rHIDTelKeyDeviceInfo.HIDDescriptorLen = sizeof(coTelephonyKeyDescriptor);
         if(NULL != m_poInputReceiver)
         {
            bRetVal = m_poInputReceiver->AttachInput(m_rHIDTelKeyDeviceInfo);
         }
      }
      break;
   default:
      {
         ETG_TRACE_ERR((" No match found for the key group \n"));
      }
      break;
   }


   return bRetVal;
}


/***************************************************************************
 ** FUNCTION:  spi_tclInputReceiverHelper::bSendTouchInputData()
 ***************************************************************************/
t_Bool spi_tclInputReceiverHelper::bSendTouchInputData(const trTouchCoordinates &rfoTouchCoordinates)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));

   spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();

   // HID touch device report
   trHIDInputReport rHIDTouchInputReport;

   //! Touch data
   t_U8 u8Touchdata[SINGLETOUCH_DATA_SIZE];

   if(NULL!= poConfigReader)
   {
      trVideoConfigData rVideoConfigData;
      poConfigReader->vGetVideoConfigData(e8DEV_TYPE_DIPO,rVideoConfigData);


      t_Float fxCoordinate = (t_Float)rfoTouchCoordinates.s32XCoordinate/rVideoConfigData.u32Screen_Width;
      t_Float fyCoordinate = (t_Float)rfoTouchCoordinates.s32YCoordinate/rVideoConfigData.u32Screen_Height;

      /*To convert the float value into a 16 bit integer value*/ 
      t_U16 u16xCoordinate = (t_U16)(fxCoordinate * (t_Float)u16MaxSixteenBitvalue);
      t_U16 u16yCoordinate = (t_U16)(fyCoordinate * (t_Float)u16MaxSixteenBitvalue);


      //First byte is populated with the touch mode
      u8Touchdata[0] = (t_U8)rfoTouchCoordinates.enTouchMode;//Touch press or release
      //Second and third bytes are populated with the x co-ordinate values
      u8Touchdata[1] = (t_U8)( u16xCoordinate       & u8BitMaskValue);
      u8Touchdata[2] = (t_U8)((u16xCoordinate >> 8) & u8BitMaskValue);

      //Third and fourth bytes are populated with the y co-ordinate values
      u8Touchdata[3] = (t_U8)( u16yCoordinate       & u8BitMaskValue);
      u8Touchdata[4] = (t_U8)((u16yCoordinate >> 8) & u8BitMaskValue);

      //Populate the HID input report for touch
      rHIDTouchInputReport.HIDReportData = u8Touchdata;
      rHIDTouchInputReport.HIDReportLen = sizeof(u8Touchdata);
      printf("bSendTouchInputData:HID Report Length = %d",rHIDTouchInputReport.HIDReportLen);
      rHIDTouchInputReport.UUID = m_rHIDTouchDeviceInfo.UUID;

      return bSendInputReport(rHIDTouchInputReport);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclInputReceiverHelper::bSendInputReport()
 ***************************************************************************/
t_Bool spi_tclInputReceiverHelper::bSendInputReport(const trHIDInputReport &rHIDInputReport)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   if (NULL != m_poInputReceiver)
   {
   return m_poInputReceiver->SendInput(rHIDInputReport);
   }
}


/***************************************************************************
** FUNCTION: t_Bool spi_tclInputReceiverHelper::bSendKeyData()
***************************************************************************/
t_Bool spi_tclInputReceiverHelper::bSendKeyData(tenKeyMode enKeyMode,tenKeyCode enKeyCode)
{ 
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));

   t_Bool bRetVal = false;

   //!Flag that checks for a consumer key
   t_Bool bIsConsumerKey = true;

   // HID device consumer Key Input report
   trHIDInputReport rHIDConsumerKeyInputReport;

   trHIDInputReport rHIDTelKeyInputReport;

   /*While populating the key data descriptor,set the bit corresponding to the key to 1,if the key is pressed.
   If the key is released,set the corresponding bit to 0 */
   switch(enKeyCode)
   {

   case e32DEV_BACKWARD:
      {
         /*Back button appears in the 7th position as per the Consumer key report.
         Hence set the 6th bit in the byte to 1.if key press,0 if key release*/ 
         m_u8ConsumerKeydata = (enKeyMode == e8KEY_PRESS)?(m_u8ConsumerKeydata|u8BackKeySetValue):(m_u8ConsumerKeydata^u8BackKeySetValue);
         printf("bSendKeyData :  Consumer key data = %x \n", m_u8ConsumerKeydata);
      }
      break;
   case e32MULTIMEDIA_NEXT:
      {
         /*Multimedia Next button appears in the 1st position as per the Consumer key report.
         Hence set the 0th bit in the byte to 1 if key press,0 if key release.*/  
         m_u8ConsumerKeydata = (enKeyMode == e8KEY_PRESS)?(m_u8ConsumerKeydata|u8NextKeySetValue):(m_u8ConsumerKeydata^u8NextKeySetValue);
         printf("bSendKeyData : Consumer key data = %x \n", m_u8ConsumerKeydata);
      }
      break;

   case e32MULTIMEDIA_PREVIOUS:
      {
         /*Multimedia Previous button appears in the 2nd position as per the Consumer key report.
         Hence set the 1st bit in the byte to 1 if key press,0 if key release.*/
         m_u8ConsumerKeydata = (enKeyMode == e8KEY_PRESS)?(m_u8ConsumerKeydata|u8PrevKeySetValue):(m_u8ConsumerKeydata^u8PrevKeySetValue);
         printf("bSendKeyData :  Consumer key data = %x \n", m_u8ConsumerKeydata);
      }
      break;

   case e32MULTIMEDIA_PLAY:
      {
         /*Multimedia play/pause button appears in the 3rd position as per the Consumer key report.
         Hence set the 2nd bit in the byte to 1 if key press,0 if key release.*/
         m_u8ConsumerKeydata = (enKeyMode == e8KEY_PRESS)?(m_u8ConsumerKeydata|u8PlayKeySetValue):(m_u8ConsumerKeydata^u8PlayKeySetValue);
         printf("bSendKeyData :  Consumer key data = %x \n", m_u8ConsumerKeydata);
      }
      break;

   case e32DEV_PHONE_CALL:
      {
         //Key data for Phone Call Accept button
         //Phone Call Accept Button appears in the bit 0 position in the Telephone Key Input report.
         //Hence set the bit 0 in the least significant byte to 1 if key pressed,else 0 if key released
         m_u8TelephoneKeydata[0] = (enKeyMode == e8KEY_PRESS)?(m_u8TelephoneKeydata[0]|0x01):(m_u8TelephoneKeydata[0]^0x01);
         m_u8TelephoneKeydata[1] = (enKeyMode == e8KEY_PRESS)?0x00:0x00;
         printf("bSendKeyData :  m_u8TelephoneKeydata[0] = %x \n", m_u8TelephoneKeydata[0]);
         printf("bSendKeyData :  m_u8TelephoneKeydata[1] = %x \n", m_u8TelephoneKeydata[1]);
         bIsConsumerKey = false;

      }
      break;

   case e32DEV_PHONE_END:
      {
         //Key data for Phone Call End button
         //Phone Call Accept Button appears in the bit 2 position in the Telephone Key Input report.
         //Hence set the bit 2 in the least significant byte to 1 if key pressed,else 0 if key released
         m_u8TelephoneKeydata[0] = (enKeyMode == e8KEY_PRESS)?(m_u8TelephoneKeydata[0]|0x04):(m_u8TelephoneKeydata[0]^0x04);
         m_u8TelephoneKeydata[1] = (enKeyMode == e8KEY_PRESS)?0x00:0x00;
         printf("bSendKeyData :  TelephoneKeydata[0] = %x \n", m_u8TelephoneKeydata[0]);
         printf("bSendKeyData :  TelephoneKeydata[1] = %x \n", m_u8TelephoneKeydata[1]);
         bIsConsumerKey = false;
      }
      break;

   default:
      {
         ETG_TRACE_ERR((" No match found for the key \n"));
      }
      break;
   }

   if(true == bIsConsumerKey)
   {
      //Populate the HID input report for Consumer Key data
      rHIDConsumerKeyInputReport.HIDReportData = &m_u8ConsumerKeydata;
      rHIDConsumerKeyInputReport.HIDReportLen = sizeof(m_u8ConsumerKeydata);
      rHIDConsumerKeyInputReport.UUID = m_rHIDConsumerKeyDeviceInfo.UUID;

      bRetVal = bSendInputReport(rHIDConsumerKeyInputReport);
   }
   else
   {
      //Populate the HID input report for Telephone Key data
      rHIDTelKeyInputReport.HIDReportData = m_u8TelephoneKeydata;
      rHIDTelKeyInputReport.HIDReportLen = sizeof(m_u8TelephoneKeydata);
      rHIDTelKeyInputReport.UUID = m_rHIDTelKeyDeviceInfo.UUID;

      bRetVal =  bSendInputReport(rHIDTelKeyInputReport);
   }
   return bRetVal;
}
