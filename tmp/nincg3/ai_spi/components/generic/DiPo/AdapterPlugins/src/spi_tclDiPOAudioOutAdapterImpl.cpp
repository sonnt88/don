/*!
*******************************************************************************
* \file              spi_tclDiPOAudioOutAdapterImpl.cpp
* \brief             CarPlay audio out adapter extended implementation
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    CarPlay audio out adapter extended implementation
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
14.07.2014 |  Shihabudheen P M            | Initial Version
05.06.2015 |  Tejaswini HB                | Added lint comments to suppress C++11 errors
17.07.2015 |  Sameer Chandra              | Memory leak fix for IPC createBuffer

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "Lock.h"
#include "Event.h"
#include "IPCMessageQueue.h"
#include "spi_tclDiPOAudioOutAdapterImpl.h"

#define SPI_ENABLE_DLT //enable DLT
#define SPI_LOG_CLASS Spi_CarPlay

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOAudioOutAdapterImpl.cpp.trc.h"
#endif 

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	

//Declare DiPO context
LOG_IMPORT_CONTEXT(Spi_CarPlay);

/******************************************************************************
| defines:
|----------------------------------------------------------------------------*/
spi_tclDiPOAudioOutAdapterImpl* spi_tclDiPOAudioOutAdapterImpl::m_poAudioOutAdapter = NULL;
std::map<t_String, tenDiPOMainAudioType> spi_tclDiPOAudioOutAdapterImpl::m_mapAudioType =
		spi_tclDiPOAudioOutAdapterImpl::vInitializeAudioTypeMap();
static Lock rAudioAdapterSyncLock;

/***************************************************************************
** FUNCTION: spi_tclDiPOAudioOutAdapterImpl::spi_tclDiPOAudioOutAdapterImpl()
***************************************************************************/
spi_tclDiPOAudioOutAdapterImpl::spi_tclDiPOAudioOutAdapterImpl(): m_enAudioType(e8AUDIO_NA)
{
   ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::spi_tclDiPOAudioOutAdapterImpl entered"));
   
   rAudioAdapterSyncLock.s16Lock();
   m_poAudioOutAdapter = this;
   rAudioAdapterSyncLock.vUnlock();
}

/***************************************************************************
** FUNCTION: spi_tclDiPOAudioOutAdapterImpl::~spi_tclDiPOAudioOutAdapterImpl()
***************************************************************************/
spi_tclDiPOAudioOutAdapterImpl::~spi_tclDiPOAudioOutAdapterImpl()
{
   ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::~spi_tclDiPOAudioOutAdapterImpl entered"));
   
   rAudioAdapterSyncLock.s16Lock();
   m_enAudioType = e8AUDIO_NA;
   m_poAudioOutAdapter = NULL;
   rAudioAdapterSyncLock.vUnlock();
}

/***************************************************************************
** FUNCTION: spi_tclDiPOAudioOutAdapterImpl::poGetInstance()
***************************************************************************/
/*Static*/
spi_tclDiPOAudioOutAdapterImpl* spi_tclDiPOAudioOutAdapterImpl::poGetInstance()
{
   ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::poGetInstance entered"));
   
   rAudioAdapterSyncLock.s16Lock();
   spi_tclDiPOAudioOutAdapterImpl *poAudioAdapter = m_poAudioOutAdapter;
   rAudioAdapterSyncLock.vUnlock();
   return poAudioAdapter;
}

/***************************************************************************
** FUNCTION: spi_tclDiPOAudioOutAdapterImpl::Prepare()
***************************************************************************/
t_Bool spi_tclDiPOAudioOutAdapterImpl::Prepare(AudioFormatStruct rAudioFormat, 
                                               AudioChannelType enChannel,
                                               const std::string& szAudioType)
{
   ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::Prepare entered"));
   ETG_TRACE_USR4(("[PARAM]:Prepare - Sample rate = %d", rAudioFormat.SampleRate));
   ETG_TRACE_USR4(("[PARAM]:Prepare - Number of Channels = %d", rAudioFormat.Channels));
   ETG_TRACE_USR4(("[PARAM]:Prepare - Bits per channel  = %d", rAudioFormat.BitsPerChannel));
   ETG_TRACE_USR4(("[PARAM]:Prepare - Type of the audio  = %s", szAudioType.c_str()));

   // Keeping the requested audio information for future use
   m_rCurrAudioFormat.u16SampleRate = rAudioFormat.SampleRate;
   m_rCurrAudioFormat.u8BitsPerChannel = rAudioFormat.BitsPerChannel;
   m_rCurrAudioFormat.u8Channels = rAudioFormat.Channels;
   m_enCurrAllocatedChannel = enChannel;
   
   Event *poEvent = Event::getInstance();
   // Populate the message
   trAudioAllocMsg rAudioAllocMsg(e8AUDIO_ALLOC_MESSAGE, enChannel, e8AUDIO_NA, e8ALLOCATION_REQUEST, m_rCurrAudioFormat);

   /*For alternate Audio, the AudioType is ignored.*/
   if(AudioChannelType_Main == rAudioAllocMsg.enAudioChannelType)
   {
      vGetAudioType(szAudioType, rAudioAllocMsg.enAudioType);
   }

   m_enAudioType = rAudioAllocMsg.enAudioType;

   t_Bool bStatus = bSendIPCMessage<trAudioAllocMsg>(rAudioAllocMsg);
   ETG_TRACE_USR2(("[DESC]: Audio prepare message sent to SPI with status = %d", ETG_ENUM(STATUS, bStatus)));
   if((true == bStatus) && (NULL != poEvent))
   {
      poEvent->vLock();
      poEvent->vTimedWait(2000);
      poEvent->vUnLock();
   } // if((true == bStatus) && (NULL != poEvent))

   // Invoking the base class prepare function to create the audio pipeline and start audio playback.
   bStatus = AlsaAudioOut::Prepare(rAudioFormat, enChannel, szAudioType);
   return bStatus;
}

/***************************************************************************
** FUNCTION: spi_tclDiPOAudioOutAdapterImpl::poGetInstance()
***************************************************************************/
t_Void spi_tclDiPOAudioOutAdapterImpl::Stop()
{
   ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::Stop entered"));
  // Reset the audio format values;
   m_rCurrAudioFormat.u16SampleRate = 0;
   m_rCurrAudioFormat.u8BitsPerChannel = 0;
   m_rCurrAudioFormat.u8Channels = 0;

   // Populate the audio message
   trAudioAllocMsg rAudioAllocMsg(e8AUDIO_ALLOC_MESSAGE, m_enCurrAllocatedChannel, e8AUDIO_NA, e8DEALLOCATION_REQUEST, m_rCurrAudioFormat);
   AlsaAudioOut::Stop();
   t_Bool bStatus = bSendIPCMessage<trAudioAllocMsg>(rAudioAllocMsg);
   ETG_TRACE_USR2(("[DESC]: Audio tear down message sent to SPI with status = %d", ETG_ENUM(STATUS, bStatus)));

   m_enAudioType = e8AUDIO_NA;
   //GstreamerAudioOut::Stop();
}

/***************************************************************************
** FUNCTION: spi_tclDiPOAudioOutAdapterImpl::vStopAudioPlayback()
***************************************************************************/
t_Void spi_tclDiPOAudioOutAdapterImpl::vStopAudioPlayback()
{
	ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::vStopAudioPlayback entered"));

   //Invoking the base class stop function to destroy the pipeline and stop audio playback.
   //Todo: This function is currently not used.
   //GstreamerAudioOut::Stop();
}

/***************************************************************************
** FUNCTION: spi_tclDiPOAudioOutAdapterImpl::vGetCurrAudioFormat()
***************************************************************************/
t_Void spi_tclDiPOAudioOutAdapterImpl::vGetCurrAudioFormat(trDiPOAudioFormat &rfoAudioFormat)
{
   ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::vGetCurrAudioFormat entered"));
   ETG_TRACE_USR4(("[PARAM]:vGetCurrAudioFormat - Sample rate = %d", m_rCurrAudioFormat.u16SampleRate));
   ETG_TRACE_USR4(("[PARAM]:vGetCurrAudioFormat - Audio format = %d", m_rCurrAudioFormat.u8Channels));
   ETG_TRACE_USR4(("[PARAM]:vGetCurrAudioFormat - Bits per channel  = %d", m_rCurrAudioFormat.u8BitsPerChannel));
   rfoAudioFormat = m_rCurrAudioFormat;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::enGetCurrAudioType()
***************************************************************************/
tenDiPOMainAudioType spi_tclDiPOAudioOutAdapterImpl::enGetCurrAudioType()
{
   ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::enGetCurrAudioType entered"));
   return m_enAudioType;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOAudioOutAdapterImpl::vGetAudioType()
 ***************************************************************************/
t_Void spi_tclDiPOAudioOutAdapterImpl::vGetAudioType(const t_String szAudioType, 
                                                    tenDiPOMainAudioType &coenAudioType)
 {
	ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::vGetAudioType entered"));
    //Mapping the Apple specific audio types to the SPI internal types
	std::map<t_String, tenDiPOMainAudioType>::iterator it = m_mapAudioType.find(szAudioType.c_str());
	if(it != m_mapAudioType.end())
	{
		coenAudioType = it->second;
	}
	ETG_TRACE_USR2(("[DESC]: vGetAudioType returned with audio type = %d", ETG_ENUM(DIPO_AUDIO_TYPE, coenAudioType)));
 }

//static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vInitializeAudioTypeMap
 ***************************************************************************/
std::map<t_String, tenDiPOMainAudioType> spi_tclDiPOAudioOutAdapterImpl::vInitializeAudioTypeMap()
{
	ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::vInitializeAudioTypeMap entered"));
	std::map<t_String, tenDiPOMainAudioType> mTempAudioMap;
	mTempAudioMap["media"] = e8AUDIO_MEDIA;
	mTempAudioMap["speechRecognition"] = e8AUDIO_SPEECHREC;
	mTempAudioMap["spokenAudio"] = e8AUDIO_SPOKEN;
	mTempAudioMap["telephony"] = e8AUDIO_TELEPHONY;
	mTempAudioMap["alert"] = e8AUDIO_ALERT;
	mTempAudioMap["default"] = e8AUDIO_DEFAULT;
	return mTempAudioMap;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOAudioOutAdapterImpl::bSendIPCMessage()
 ***************************************************************************/
template<typename trMessage>
t_Bool spi_tclDiPOAudioOutAdapterImpl::bSendIPCMessage(trMessage rMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOAudioOutAdapterImpl::bSendIPCMessage entered"));
   t_Bool bRetVal = false;
   IPCMessageQueue oMessageQueue(DIPO_SPI_MSGQ_IPC_THREAD);

   trMessage *poBuffer = (trMessage *)oMessageQueue.vpCreateBuffer(sizeof(trMessage));
   if(NULL != poBuffer)
   {
      memset(poBuffer, 0, sizeof(trMessage));
      memcpy(poBuffer, &rMessage, sizeof(trMessage));
      bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poBuffer, DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));

      //Destroy the buffer
      oMessageQueue.vDropBuffer(poBuffer);
      //oMessageQueue.
   }
   return bRetVal;
}
//lint –restore