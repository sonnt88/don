/*!
*******************************************************************************
* \file              spi_tclDiPOTouchInputAdapterImpl.cpp
* \brief             DiPO Input Adapter implementation
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO input adapter implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
5.3.2014  |  Shihabudheen P M            | Initial Version
03.04.2014  |  Hari Priya E R            | Removed vSetHIDDeviceInfo()function
06.05.2015  |Tejaswini HB                 |Lint Fix
26.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
\endverbatim
******************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "Lock.h" 
#include "spi_tclDiPOTouchInputAdapterImpl.h"

#define SPI_ENABLE_DLT //enable DLT
#define SPI_LOG_CLASS Spi_CarPlay

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOTouchInputAdapterImpl.cpp.trc.h"
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported


//import the DLT context
LOG_IMPORT_CONTEXT(Spi_CarPlay);

spi_tclDiPOTouchInputAdapterImpl* spi_tclDiPOTouchInputAdapterImpl::m_poDiPOTouchInputAdapter = NULL;

t_U32 spi_tclDiPOTouchInputAdapterImpl::m_u32Height = 0;
t_U32 spi_tclDiPOTouchInputAdapterImpl::m_u32Width = 0;

//! Size of the user input Data for single touch
#define SINGLETOUCH_DATA_SIZE 5

//! Defines for Apple specific HID descriptor to readable
#define HID_USAGE_PAGE(a)                  0x05, (a)
#define HID_USAGE(a)                       0x09, (a)
#define HID_COLLECTION(a)                  0xA1, (a)
#define HID_END_COLLECTION()               0xC0
#define HID_REPORT_SIZE(size)              0x75, (size)
#define HID_REPORT_COUNT(count)            0x95, (count)
#define HID_INPUT(a)                       0x81, (a)
#define HID_LOGICAL_MINMAX(min, max)       0x15, (min), 0x25, (max)
#define HID_LOGICAL_MINMAX16_WITH_LSB_MSB(min_MSB_byte, min_LSB_byte,  max_MSB_byte, max_LSB_byte)     0x16, min_LSB_byte , min_MSB_byte, \
  0x26, max_LSB_byte, max_MSB_byte


 /*!
  * \static const t_U8 coSingleTouchDescriptor
  * \Brief : This formulate the initial report about the input device which is
  *          used to send the user input later. This report sends to the device
  *          on startup to inform(Attach Input Device) about the usage of the
  *          input device. The report includes the type of the input device,
  *          type of the user input data, length of the user input data etc.
  *          For more information, see section 15.3.5 of Apple accessory specification.
  */

static const t_U8 coSingleTouchDescriptor[] =
{
    HID_USAGE_PAGE (0x0D),
    HID_USAGE (0x04),
    HID_COLLECTION (0x01),
        // Digitizer
        HID_USAGE_PAGE (0x0D),
        HID_USAGE (0x22),
        HID_COLLECTION (0x02),
            // Digitizer
            HID_USAGE_PAGE (0x0D),
            HID_USAGE (0x33),

            // Overall logical info.
            HID_LOGICAL_MINMAX(0x00, 0x01),
            HID_REPORT_SIZE(0x01),
            HID_REPORT_COUNT(0x01),
            HID_INPUT(0x02),

            // Report data info
            HID_REPORT_SIZE(0x07),
            HID_REPORT_COUNT(0x01),
            HID_INPUT(0x01),

            HID_USAGE_PAGE (0x01),

            // X Cordinate value info
            HID_USAGE (0x30),
            HID_LOGICAL_MINMAX16_WITH_LSB_MSB (0x00,0x00, 0x7f,0xff),
            HID_REPORT_SIZE(0x10),
            HID_REPORT_COUNT(0x01),
            HID_INPUT(0x02),

            // Y cordinate value info
            HID_USAGE (0x31),
            HID_LOGICAL_MINMAX16_WITH_LSB_MSB (0x00,0x00, 0x7f,0xff),
            HID_REPORT_SIZE(0x10),
            HID_REPORT_COUNT(0x01),
            HID_INPUT(0x02),
            HID_END_COLLECTION(),
    HID_END_COLLECTION()
};

static const t_U16 u16MaxSixteenBitvalue = 32767;
static const t_U8  u8BitMaskValue = 255;
static Lock rTouchAdapterSyncLock; 


/***************************************************************************
 ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl::spi_tclDiPOTouchInputAdapterImpl()
 ***************************************************************************/
spi_tclDiPOTouchInputAdapterImpl::spi_tclDiPOTouchInputAdapterImpl():m_poIInputReceiver(NULL)
{
   ETG_TRACE_USR1(("spi_tclDiPOTouchInputAdapterImpl::spi_tclDiPOTouchInputAdapterImpl entered"));
   
   rTouchAdapterSyncLock.s16Lock();
   m_poDiPOTouchInputAdapter = this;
   rTouchAdapterSyncLock.vUnlock();
   
   memset(&m_rHIDTouchDeviceInfo, 0, sizeof(trHIDDeviceInfo));
}


/***************************************************************************
 ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl::~spi_tclDiPOTouchInputAdapterImpl()
 ***************************************************************************/
spi_tclDiPOTouchInputAdapterImpl::~spi_tclDiPOTouchInputAdapterImpl()
{
   ETG_TRACE_USR1(("spi_tclDiPOTouchInputAdapterImpl::~spi_tclDiPOTouchInputAdapterImpl entered"));
   
   rTouchAdapterSyncLock.s16Lock();
   m_poIInputReceiver = NULL;
   m_poDiPOTouchInputAdapter = NULL;
   memset(&m_rHIDTouchDeviceInfo, 0, sizeof(trHIDDeviceInfo));
   rTouchAdapterSyncLock.vUnlock();  
}


/***************************************************************************
 ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl* spi_tclDiPOTouchInputAdapterImpl::
 **            poGetDiPOTouchInputAdapterInstance()
 ***************************************************************************/
spi_tclDiPOTouchInputAdapterImpl* spi_tclDiPOTouchInputAdapterImpl::poGetDiPOTouchInputAdapterInstance()
{
   ETG_TRACE_USR1(("spi_tclDiPOTouchInputAdapterImpl::poGetDiPOTouchInputAdapterInstance entered"));
   rTouchAdapterSyncLock.s16Lock();
   spi_tclDiPOTouchInputAdapterImpl *poTouchAdapter = m_poDiPOTouchInputAdapter;
   rTouchAdapterSyncLock.vUnlock(); 
   return poTouchAdapter;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl::vSetTouchProperty (...)

 ***************************************************************************/
t_Void spi_tclDiPOTouchInputAdapterImpl :: vSetTouchProperty(const t_U32 u32Screen_Height, const t_U32 u32Screen_Width)
{
   ETG_TRACE_USR1(("spi_tclDiPOTouchInputAdapterImpl::vSetTouchProperty entered"));
   m_u32Height = u32Screen_Height;
   ETG_TRACE_USR4(("[PARAM]::vSetTouchProperty - Display height = %d, Display width = %d", m_u32Height, m_u32Width));
   m_u32Width = u32Screen_Width;
}
/***************************************************************************
 ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl::Initialize()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Bool spi_tclDiPOTouchInputAdapterImpl::Initialize(const IConfiguration& rfConfig, IInputReceiver& rfReceiver)
{
   ETG_TRACE_USR1(("spi_tclDiPOTouchInputAdapterImpl::Initialize entered"));
   t_Bool bRetVal = false;

	rTouchAdapterSyncLock.s16Lock();
	
    m_poIInputReceiver = &rfReceiver;
    SPI_NORMAL_ASSERT(NULL == m_poIInputReceiver);
	
    // populate the touch device info.
    m_rHIDTouchDeviceInfo.DisplayUUID = rfConfig.GetItem("display-uuid", "").c_str();
    m_rHIDTouchDeviceInfo.HIDCountryCode = rfConfig.GetNumber("wl-touch-hid-country-code", (long long)0);
    m_rHIDTouchDeviceInfo.HIDProductId = rfConfig.GetNumber("wl-touch-hid-product-id",(long long) 0);
    m_rHIDTouchDeviceInfo.HIDVendorId = rfConfig.GetNumber("wl-touch-hid-vendor-id",(long long) 0);
    m_rHIDTouchDeviceInfo.Name = "DiPOTouchDevice";
    m_rHIDTouchDeviceInfo.UUID =  rfConfig.GetItem("wl-touch-uuid", "").c_str();
    m_rHIDTouchDeviceInfo.HIDDescriptor = coSingleTouchDescriptor;
    m_rHIDTouchDeviceInfo.HIDDescriptorLen = sizeof(coSingleTouchDescriptor);

   // Attach the Input Device Info
    bRetVal = m_poIInputReceiver->AttachInput(m_rHIDTouchDeviceInfo);
  ETG_TRACE_USR2(("[DESC]: Touch adapter registration for carplay  completed with status = %d", ETG_ENUM( STATUS, bRetVal)));
  
    rTouchAdapterSyncLock.vUnlock(); 
	
    return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl::bSendTouchEvent()
 ***************************************************************************/
t_Bool spi_tclDiPOTouchInputAdapterImpl::bSendTouchEvent(const trTouchCoordinates &rfcorTouchCoordinates)
{
   ETG_TRACE_USR1(("spi_tclDiPOTouchInputAdapterImpl::bSendTouchEvent entered"));
   t_Bool bRetVal = false;
   // HID touch device report
   trHIDInputReport rHIDTouchInputReport;

   //! Touch data
   t_U8 u8Touchdata[SINGLETOUCH_DATA_SIZE];

      t_Float fxCoordinate = (t_Float)rfcorTouchCoordinates.s32XCoordinate/m_u32Width;
      t_Float fyCoordinate = (t_Float)rfcorTouchCoordinates.s32YCoordinate/m_u32Height;

      /*To convert the float value into a 16 bit integer value*/
      t_U16 u16xCoordinate = (t_U16)(fxCoordinate * (t_Float)u16MaxSixteenBitvalue);
      t_U16 u16yCoordinate = (t_U16)(fyCoordinate * (t_Float)u16MaxSixteenBitvalue);

      ETG_TRACE_USR4(("[PARAM]::bSendTouchEvent - X-Coordinate  = %d", u16xCoordinate));
      ETG_TRACE_USR4(("[PARAM]::bSendTouchEvent - Y-Coordinate  = %d", u16yCoordinate));

      //First byte is populated with the touch mode
      u8Touchdata[0] = (t_U8)rfcorTouchCoordinates.enTouchMode;//Touch press or release
      //Second and third bytes are populated with the x co-ordinate values
      u8Touchdata[1] = (t_U8)( u16xCoordinate       & u8BitMaskValue);
      u8Touchdata[2] = (t_U8)((u16xCoordinate >> 8) & u8BitMaskValue);

      //Third and fourth bytes are populated with the y co-ordinate values
      u8Touchdata[3] = (t_U8)( u16yCoordinate       & u8BitMaskValue);
      u8Touchdata[4] = (t_U8)((u16yCoordinate >> 8) & u8BitMaskValue);

      //Populate the HID input report for touch
      rHIDTouchInputReport.HIDReportData = u8Touchdata;
      rHIDTouchInputReport.HIDReportLen = sizeof(u8Touchdata);
      rHIDTouchInputReport.UUID = m_rHIDTouchDeviceInfo.UUID;

	  rTouchAdapterSyncLock.s16Lock();
      if (NULL != m_poIInputReceiver)
      {
         bRetVal = m_poIInputReceiver->SendInput(rHIDTouchInputReport);
      }//End of if (NULL != m_poIInputReceiver)
	  rTouchAdapterSyncLock.vUnlock(); 
	  
   return bRetVal;
}
//lint –restore



