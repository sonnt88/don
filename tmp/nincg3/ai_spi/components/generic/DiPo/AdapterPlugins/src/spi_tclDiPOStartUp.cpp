/*!
*******************************************************************************
* \file              spi_tclDiPOStartUp.cpp
* \brief             DiPO StartUP 
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO StartUP code. This class is get called from ADIT process
                once the DiPO session is active. This class is used to register
                the customized adapter implementation classes.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
5.2.2014   |  Shihabudheen P M            | Initial Version
18.03.2014 |  Shihabudheen P M            | Modified 1.vExecute
04.04.2014 |  Hari Priya E R              | Included message for hard key handling
03.09.2014 |  Shihabudheen P M            | Changes for set up the core-DeviceId
13.10.2014 | Vinoop U					  | Changes to set OEM icon Image And OEM icon label
17.03.2015 | Shihabudheen P M             | Changes for setting display parameters on start up.
27.05.2015 |  Tejaswini H B(RBEI/ECP2)    | Added Lint comments to suppress C++11 Errors
15.07.2015 |  Sameer Chandra              | Knob Key Implementation
\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <algorithm>

#include "Event.h"
#include "spi_tclDiPOStartUp.h"
#include "spi_tclDiPOControlAdapterImpl.h"
#include "spi_tclDiPOTouchInputAdapterImpl.h"
#include "spi_tclDiPOKeyInputAdapterImpl.h"
#include "spi_tclDiPOAudioOutAdapterImpl.h"


#define SPI_ENABLE_DLT //enable DLT
#define SPI_LOG_CLASS Spi_CarPlay
#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOStartUp.cpp.trc.h"
#endif

static spi_tclDiPOStartUp* poDiPOStartUp  = NULL;
static MsgQIPCThreader* poMsgQIPCThreader = NULL;

#define DIPO_CONTROL_ADAPTER     "spi_tclDiPOControlAdapterImpl"
#define DIPO_KEY_INPUT_ADAPTER   "spi_tclDiPOKeyInputAdapterImpl"
#define DIPO_AUDIO_OUT_ADAPTER   "spi_tclDiPOAudioOutAdapterImpl"

#ifdef VARIANT_S_FTR_ENABLE_GM_TOUCHENABLE
#define DIPO_TOUCH_INPUT_ADAPTER "spi_tclDiPOTouchInputAdapterImpl"
#endif

#define DIPO_MESSAGE_WAIT_TIME 3000
#define DIPO_RETRY_WAIT_TIME 10

#define BUFFER_MAX_LENGTH 10

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/*! 
* \extern "C" void carplay_LibraryEntryPoint()
* \brief  Function which act as a entry point to the DiPO implementations. 
*         This function is used by ADIT to look up SPI DiPO implementation
*/

//Declare DiPO context
LOG_DECLARE_CONTEXT(Spi_CarPlay);

extern "C" void carplay_LibraryEntryPoint()
{
    /* Register Spi_CarPlay with DLT */
    LOG_REGISTER_APP("SPICP", "SPI CarPlay");
    LOG_REGISTER_CONTEXT(Spi_CarPlay,"INFO", "SPI CarPlay Info Msg");

    ETG_TRACE_USR1(("carplay_LibraryEntryPoint entered"));
   
   DipoFactory_Register<IControlAdapter, spi_tclDiPOControlAdapterImpl>(DIPO_CONTROL_ADAPTER);

#ifdef VARIANT_S_FTR_ENABLE_GM_TOUCHENABLE
  // Touch Input adapter registration for GM
   DipoFactory_Register<IInputAdapter, spi_tclDiPOTouchInputAdapterImpl>(DIPO_TOUCH_INPUT_ADAPTER);
#endif

   //Key input adapter registration
   DipoFactory_Register<IInputAdapter, spi_tclDiPOKeyInputAdapterImpl>(DIPO_KEY_INPUT_ADAPTER);
   // Audio out adapter registration
   DipoFactory_Register<IAudioOutAdapter, spi_tclDiPOAudioOutAdapterImpl>(DIPO_AUDIO_OUT_ADAPTER);

   // Startup the Msq to get the messages
   poDiPOStartUp = new spi_tclDiPOStartUp();
   SPI_NORMAL_ASSERT(NULL == poDiPOStartUp);
   
   trDiPOSessionMsg rDiPOSessionMsg;
   rDiPOSessionMsg.enMsgType = e8SESSION_MESSAGE;
   rDiPOSessionMsg.enDiPOSessionState = e8DIPO_PLUGIN_LOADED;

   Event *poEvent = Event::getInstance();
   if((NULL != poDiPOStartUp) && (NULL != poEvent))
   {
      poEvent->vLock();
      t_Bool bStatus = false;
      while(false == bStatus)
      {
         bStatus = poDiPOStartUp->bSendIPCMessage<trDiPOSessionMsg>(rDiPOSessionMsg);
         ETG_TRACE_USR1(("IPC Message send status from Entry Point: %d \n", bStatus));
         // Wait to retry the message send to SPI
         poEvent->vTimedWait(DIPO_RETRY_WAIT_TIME);
      }
      //Wait the function till it receives the device MAC address from SPI
      poEvent->vTimedWait(DIPO_MESSAGE_WAIT_TIME);
      poEvent->vUnLock();
   } //if((NULL != poDiPOStartUp) && (NULL != poEvent))
}


/*!
* \extern "C" void carplay_InitConfiguration()
* \brief  Function which act as a initialization of all CarPlay configuration.
*/
extern "C" void carplay_InitConfiguration(adit::carplay::IDynamicConfiguration* inConfig)
{
   ETG_TRACE_USR1(("carplay_InitConfiguration entered"));
   if((NULL != poDiPOStartUp) && (NULL != inConfig))
   {
      trInitConfigParameters rInitConfigParam;
      poDiPOStartUp->vGetInitConfigParam(rInitConfigParam);
      t_Char szWidthInMM[BUFFER_MAX_LENGTH], szHeightInMM[BUFFER_MAX_LENGTH],
         szWidthInPixel[BUFFER_MAX_LENGTH], szHeightInPixel[BUFFER_MAX_LENGTH],
         szDisplayInput[BUFFER_MAX_LENGTH];

      memset(szWidthInMM, 0, BUFFER_MAX_LENGTH);
      memset(szHeightInMM, 0, BUFFER_MAX_LENGTH);
      memset(szWidthInPixel, 0, BUFFER_MAX_LENGTH);
      memset(szHeightInPixel, 0, BUFFER_MAX_LENGTH);
      memset(szDisplayInput, 0, BUFFER_MAX_LENGTH);

      sprintf(szWidthInMM, "%d", rInitConfigParam.u32DisplayWidthMM);
      sprintf(szHeightInMM, "%d", rInitConfigParam.u32DisplayHeightMM);
      sprintf(szWidthInPixel, "%d", rInitConfigParam.u32DisplayWidthPixels);
      sprintf(szHeightInPixel, "%d", rInitConfigParam.u32DisplayHeightPixels);
      sprintf(szDisplayInput,"%d",rInitConfigParam.u8DisplayInput);

      //!@Note: core-deviceID, and core-model is used for bonjour service.
      // Hence this value must set on system startup.
      inConfig->SetItem("core-deviceID", rInitConfigParam.szBluetoothId);
      inConfig->SetItem("display-width-millimeter", szWidthInMM);
      inConfig->SetItem("display-height-millimeter", szHeightInMM);
      inConfig->SetItem("display-width", szWidthInPixel);
      inConfig->SetItem("display-height", szHeightInPixel);
      inConfig->SetItem("display-input",szDisplayInput);
      inConfig->SetItem("core-input",szDisplayInput);
      inConfig->SetItem("core-model", rInitConfigParam.szCoreModelName);
   }
}

/*!
* \extern "C" void carplay_LibraryExitPoint()
* \brief  Function which act as a exit point to the DiPO implementations.
*         This function is used by ADIT to look up SPI DiPO implementation
*/
extern "C" void carplay_LibraryExitPoint()
{
   ETG_TRACE_USR1(("carplay_LibraryExitPoint entered"));

   RELEASE_MEM(poDiPOStartUp);
   
    /* Unregister Spi_CarPlay from DLT */
    LOG_UNREGISTER_CONTEXT(Spi_CarPlay);
    LOG_UNREGISTER_APP();
}


/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::spi_tclDiPOStartUp()
***************************************************************************/
spi_tclDiPOStartUp::spi_tclDiPOStartUp()
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::spi_tclDiPOStartUp entered"));

   poMsgQIPCThreader = new MsgQIPCThreader(SPI_DIPO_MSGQ_IPC_THREAD, this, true);
   SPI_NORMAL_ASSERT(NULL == poMsgQIPCThreader);
   if(NULL != poMsgQIPCThreader)
   {
      poMsgQIPCThreader->vSetThreadName("DiPOMsgQ");
   }
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::~spi_tclDiPOStartUp()
***************************************************************************/
spi_tclDiPOStartUp::~spi_tclDiPOStartUp()
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::~spi_tclDiPOStartUp entered"));
   //Terminate the IPC MsgQ thread
   //Get the message Queue
   IPCMessageQueue* poMessageQueue = poMsgQIPCThreader->poGetMessageQueu();

   //Create Buffer
   trMsgQBase * poBuffer = (trMsgQBase*)poMessageQueue->vpCreateBuffer(sizeof(trMsgQBase));
   memset(poBuffer, 0, sizeof(trMsgQBase));
   //Send the Terminate message
   poMessageQueue->iSendBuffer((void*)poBuffer,DIPO_MESSAGE_PRIORITY,TCL_THREAD_TERMINATE_MESSAGE);
   //Destroy the buffer
   poMessageQueue->vDropBuffer(poBuffer);
   //Wait for the message queue thread to join.
   Threader::vWaitForTermination(poMsgQIPCThreader->pGetThreadID());

   RELEASE_MEM(poMsgQIPCThreader);
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::vExecute
***************************************************************************/

t_Void spi_tclDiPOStartUp::vExecute(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::vExecute entered"));
   t_Bool bStatus = false;

   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trMsgQBase *poBaseMsg =  NULL;
      poBaseMsg = (trMsgQBase*)poMessage->pvBuffer;
      switch((t_U8)poBaseMsg->enMsgType)
      {
      case e8AUDIO_MESSAGE:
         {
            Event *poEvent = Event::getInstance();
            if (NULL != poEvent)
            {
               poEvent->vLock();
               // This function is used to set audio configuration. 
               bStatus = bProcessAudioMsg(poMessage);
               poEvent->vSignal();
               poEvent->vUnLock();
            } // if(NULL != poEvent)
         }
         break;
         case e8AUDIO_IN_MESSAGE:
         case e8AUDIO_ACTIVE_MESSAGE:
         {
            Event *poEvent = Event::getInstance();
            if (NULL != poEvent)
            {
               poEvent->vLock();
               // This function was used to set audio configuration. Currently not used. Keep it for future use.
               //bStatus = bProcessAudioMsg(poMessage);
               poEvent->vSignal();
               poEvent->vUnLock();
            } // if(NULL != poEvent)
         }
         break;
      case e8AUDIO_STOP_MESSAGE:
         {
            bStatus = bProcessAudioStopMsg(poMessage);
         }
         break;
      case e8TOUCH_MESSAGE:
         {
               bStatus = bProcessTouchMsg(poMessage);
         }
         break;
      case e8KEY_MESSAGE:
         {
               bStatus = bProcessKeyMsg(poMessage);
         }
         break;
      case e8SIRIACTION_MESSAGE:
         {
            bStatus = bProcessSiriActionMsg(poMessage);
            
         }
         break;
      case e8REQUEST_UI_MESSAGE:
         {
            bStatus = bProcessLaunchAppMsg(poMessage);
         }
         break;
      case e8RM_VIDEO_RQST_MESSAGE:
         {
            bStatus = bProcessAccessoryVideoContextMsg(poMessage);
         }
         break;
      case e8RM_AUDIO_RQST_MESSAGE:
         {
            bStatus = bProcessAccessoryAudioContextMsg(poMessage);
         }
         break;
      case e8RM_APP_STATE_MESSAGE:
         {
            bStatus = bProcessAccessoryAppStateMsg(poMessage);
         }
         break;
      case e8VEHICLE_MODE_MESSAGE:
         {
            bStatus = bProcessVehicleModeMsg(poMessage);
         }
         break;
      case e8INFO_MESSAGE:
         {
            bStatus = bProcessInfoMessage(poMessage);
         }
         break;
      case e8AUDIO_ERROR_MESSAGE:
         {
            bStatus = bProcessAudioErrorMsg(poMessage);
         }
         break;
      case e8VIDEO_CONFIG_MESSAGE:
         {
            bStatus = bProcessVideoConfigMsg(poMessage);
         }
         break;
      case e8BTADDRESS_UPDATE_MESSAGE:
      	 {
			bStatus = bProcessBtAddressUpdateMsg(poMessage);
      	 }
		 break;
      case e8RESTRICTIONS_UPDATE_MESSAGE:
         {
            bStatus = bProcessRestrictionsUpdateMsg(poMessage);
         }
       break;
      case e8UNKNOWN_MESSAGE:
      default:
         {
        	 ETG_TRACE_USR2(("[DESC]: Message from SPI is invalid"));
         }
         break;
      }//End of Switch

      // Release the allocated memory
      t_Char* pczBuffer = static_cast<t_Char*> (poMessage->pvBuffer);
      RELEASE_ARRAY_MEM(pczBuffer);
   }//End of if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   RELEASE_MEM(poMessage); // Moved out the if state to make Lint happy. NULL point check is part of RELEASE_MEM
   ETG_TRACE_USR2(("[DESC]: Message from SPI is processed with status = %d", ETG_ENUM( STATUS, bStatus)));
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessAudioMsg(tShlMessage *poMessage)
***************************************************************************/ 
t_Bool spi_tclDiPOStartUp::bProcessAudioMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessAudioMsg entered"));
   t_Bool bRetVal = false;
   trMsgQBase *poBaseMsg =  NULL;
   spi_tclDiPOControlAdapterImpl* poDiPOControlAdapter =
            spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance();

   if((NULL != poMessage) && (NULL != poDiPOControlAdapter))
   {
     poBaseMsg = (trMsgQBase*)poMessage->pvBuffer;
	  if((NULL != poBaseMsg) && (e8AUDIO_MESSAGE == poBaseMsg->enMsgType))
	  {
	      trAudioMsg *poAudioMsg = static_cast<trAudioMsg*>(poBaseMsg);
	      // Note: NULL check of poAudioMsg is not needed, as already NULL check is done for poBaseMsg.
		  if(e8ALTERNATE_AUDIO == poAudioMsg->enAudioStreamType)
		  {
			  bRetVal =  poDiPOControlAdapter->bSetAudioOutConfig(poAudioMsg->szALSADeviceName,
                                                                   poAudioMsg->enAudioStreamType, e8AUDIO_MESSAGE);
		  }
	  }//if((NULL != poBaseMsg) && (e8AUDIO_MESSAGE == poBaseMsg->enMsgType))
   } //if((NULL != poMessage) && (NULL != poDiPOControlAdapter))

   return true;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessTouchMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessTouchMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessTouchMsg entered"));
   trTouchMsg *poTouchInputMsg = NULL;
   t_Bool bRetVal = false;

   if((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      poTouchInputMsg = static_cast<trTouchMsg*>(poMessage->pvBuffer);
      spi_tclDiPOTouchInputAdapterImpl* poDiPOTouchInputAdapter =
               spi_tclDiPOTouchInputAdapterImpl::poGetDiPOTouchInputAdapterInstance();

      if((NULL != poTouchInputMsg) && (NULL != poDiPOTouchInputAdapter))
      {
         // Send the input data report to the Device
         bRetVal = (poDiPOTouchInputAdapter->bSendTouchEvent(poTouchInputMsg->rTouchCoordinates));
      }//End of if((NULL != poTouchInputMsg) && (NULL != poReceiverHelper))
   }//End of if((NULL != poMessage) && (NULL != poMessage->pvBuffer))

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOStartUp::bProcessKeyMsg(tShlMessage *poMessage )
 ***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessKeyMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessKeyMsg entered"));
   trKeyMsg *poKeyInputMsg = NULL;
   t_Bool bRetVal = false;

   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      poKeyInputMsg = static_cast<trKeyMsg*> (poMessage->pvBuffer);
      spi_tclDiPOKeyInputAdapterImpl* poDiPOKeyInputAdapter =
               spi_tclDiPOKeyInputAdapterImpl::poGetDiPOKeyInputAdapterInstance();

      if ((NULL != poKeyInputMsg) && (NULL != poDiPOKeyInputAdapter))
      {
         // Send the input data report to the Device

         bRetVal = (e8_BUTTON_TYPE == poKeyInputMsg->enKeyType) ?
                    (poDiPOKeyInputAdapter->bSendKeyEvent(poKeyInputMsg->enKeyMode, poKeyInputMsg->enKeyCode)):
                    (poDiPOKeyInputAdapter->bSendKnobKeyEvent(poKeyInputMsg->s8EncoderDeltaCnt));
      }//End of if ((NULL != poKeyInputMsg) && (NULL != poDiPOKeyInputAdapter))
   }//End of if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))

   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessLaunchAppMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessLaunchAppMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessLaunchAppMsg entered"));

   t_Bool bRetVal = false;
   spi_tclDiPOControlAdapterImpl* poDiPOControlAdapter =
            spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance();

   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trRequestUIMsg *poReqUIMsg = NULL;
      poReqUIMsg = static_cast<trRequestUIMsg*>(poMessage->pvBuffer);

      if ((NULL != poReqUIMsg) && (NULL != poDiPOControlAdapter))
      {
         // Send the Application URL to launch an application
         bRetVal =  poDiPOControlAdapter->bLaunchApp(poReqUIMsg->szAppUrl);
      }//End of if ((NULL != poReqUIMsg)...
   }//End of if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))

   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessAccessoryAudioContextMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessAccessoryAudioContextMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessAccessoryAudioContextMsg entered"));

   t_Bool bRetVal = false;
   spi_tclDiPOControlAdapterImpl* poDiPOControlAdapter =
            spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance();

   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trModeChange rModeChange;
      trAccAudioContextMsg *poAccAudioContextMsg = NULL;
      poAccAudioContextMsg = static_cast<trAccAudioContextMsg*>(poMessage->pvBuffer);
      if(NULL != poAccAudioContextMsg)
      {
         vSetAudioMode(poAccAudioContextMsg->rDiPOAudioContext, rModeChange);
         if(NULL != poDiPOControlAdapter)
         {
            bRetVal = poDiPOControlAdapter->bAccessoryModeChange(rModeChange, e8MODE_CHANGE_AUDIO);
         } //if(NULL != poDiPOControlAdapter)
         else
         {
             //function to set the mode change info. this helps to keep track of all the changes
             //in audio context in HMI side. This will get called even if session is not started.
             spi_tclDiPOControlAdapterImpl::vSetCurrentAccessoryMode(rModeChange, e8MODE_CHANGE_AUDIO);
         }
      }// if(NULL != poAccAudioContextMsg)
   }//End of if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))

   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessAccessoryVideoContextMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessAccessoryVideoContextMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessAccessoryVideoContextMsg entered"));
   t_Bool bRetVal = false;
   spi_tclDiPOControlAdapterImpl* poDiPOControlAdapter =
            spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance();

   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trModeChange rModeChange;
      trAccVideoContextMsg *poAccVideoContextMsg = NULL;
      poAccVideoContextMsg = static_cast<trAccVideoContextMsg*>(poMessage->pvBuffer);

      if(NULL != poAccVideoContextMsg)
      {
         vSetVideoMode(poAccVideoContextMsg->rDiPOVideoContext, rModeChange);
         
         if(NULL != poDiPOControlAdapter)
         {
            bRetVal = poDiPOControlAdapter->bAccessoryModeChange(rModeChange, e8MODE_CHANGE_DISPLAY);
         }//if(NULL != poDiPOControlAdapter)
         else
         {
             //function to set the mode change info. this helps to keep track of all the changes
             //in video context in HMI side. This will get called even if session is not started.
             spi_tclDiPOControlAdapterImpl::vSetCurrentAccessoryMode(rModeChange, e8MODE_CHANGE_DISPLAY);
         }
      }// if(NULL != poAccVideoContextMsg)
   }//End of if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))

   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessAccessoryAppStateMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessAccessoryAppStateMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessAccessoryAppStateMsg entered"));

   t_Bool bRetVal = false;
   spi_tclDiPOControlAdapterImpl* poDiPOControlAdapter =
            spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance();

   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trModeChange rModeChange;
      trAccAppStateMsg *poAccAppStateMsg  = NULL;
      poAccAppStateMsg = static_cast<trAccAppStateMsg*>(poMessage->pvBuffer);

      if(NULL != poAccAppStateMsg)
      {
         vSetAppState(poAccAppStateMsg->rDiPOAppState, rModeChange);

         if(NULL != poDiPOControlAdapter)
         {
             bRetVal = poDiPOControlAdapter->bAccessoryModeChange(rModeChange, e8MODE_CHANGE_APP);
         }//if(NULL != poDiPOControlAdapter)
         else
         {
             //function to set the mode change info. this helps to keep track of all the changes
             //in App context in HMI side. This will get called even if session is not started.
             spi_tclDiPOControlAdapterImpl::vSetCurrentAccessoryMode(rModeChange, e8MODE_CHANGE_APP);
         }
      }//if(NULL != poAccAppStateMsg)
   }//End of if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessSiriActionMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessSiriActionMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessSiriActionMsg entered"));
   t_Bool bRetVal = false;
   spi_tclDiPOControlAdapterImpl* poDiPOControlAdapter =
            spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance();
   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trSiriActionMsg *poSiriActionMsg = NULL;
      poSiriActionMsg = static_cast<trSiriActionMsg*>(poMessage->pvBuffer);

      if((NULL != poSiriActionMsg) && (NULL != poDiPOControlAdapter))
      {
         // Send the input data report to the Device
         bRetVal= poDiPOControlAdapter->bSiriAction(poSiriActionMsg->enSiriAction);
      }
   }
   return bRetVal;
}


/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessAudioStopMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessAudioStopMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessAudioStopMsg entered"));

   SPI_INTENTIONALLY_UNUSED(poMessage);
   return true;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessAudioErrorMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessAudioErrorMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessAudioErrorMsg entered"));
   t_Bool bRetVal = false;
   trMsgQBase *poBaseMsg = NULL;
   spi_tclDiPOControlAdapterImpl* poDiPOControlAdapter =
         spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance();

   if (NULL != poMessage)
   {
      poBaseMsg = (trMsgQBase*) poMessage->pvBuffer;
   }

   if ((NULL != poBaseMsg) && (e8AUDIO_ERROR_MESSAGE == poBaseMsg->enMsgType))
   {
      trAudioErrorMsg *poAudioErrMsg = NULL;
      poAudioErrMsg = static_cast<trAudioErrorMsg*> (poBaseMsg);
      // Note: NULL check of poAudioErrMsg is not needed, as already NULL check is done for poBaseMsg.
      if (NULL != poDiPOControlAdapter)
      {
    	  ETG_TRACE_USR4(("[PARAM]:bProcessAudioErrorMsg - enAudioStreamType = %d",
    	  		   ETG_ENUM(DIPO_AUDIO_STREAMTYPE , poAudioErrMsg->enAudioStreamType)));
    	  ETG_TRACE_USR4(("[PARAM]:bProcessAudioErrorMsg - szALSADummyDeviceName = %s",
    			  poAudioErrMsg->szALSADummyDeviceName));
         bRetVal
               = poDiPOControlAdapter->bSetAudioOutConfig(poAudioErrMsg->szALSADummyDeviceName, poAudioErrMsg->enAudioStreamType, 
               e8AUDIO_ERROR_MESSAGE);
      }//End of if((NULL != poAudioMsg)&&...
   }//End of if((NULL != poBaseMsg) &&
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessVehicleModeMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessVehicleModeMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessVehicleModeMsg entered"));
   t_Bool bRetVal = false;
   if((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trVehicleModeInfoMsg *poVehicleModeMsg = NULL;
      poVehicleModeMsg = static_cast<trVehicleModeInfoMsg*>(poMessage->pvBuffer);
      spi_tclDiPOControlAdapterImpl* poDiPOControlAdapter =
         spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance();
      if(NULL != poVehicleModeMsg)
      {
    	  switch(poVehicleModeMsg->enVehicleConfig)
    	  {
    		  case e8_NIGHT_MODE:
    		  case e8_DAY_MODE:
    		  {
    			  t_Bool bNightMode =(e8_NIGHT_MODE == poVehicleModeMsg->enVehicleConfig) ? true : false;
    	    	  if(NULL != poDiPOControlAdapter)
    	    	  {
    	    	 	 poDiPOControlAdapter->vSetNightMode(bNightMode);
    	    	 	 bRetVal = true;
    	    	  }
    	    	  spi_tclDiPOControlAdapterImpl::vSetNightModeInfo(bNightMode);
    		  }
    		  break;
    		  case e8PARK_MODE:
    		  case e8DRIVE_MODE:
    		  {
    			  t_Bool bDriveMode = (e8DRIVE_MODE==poVehicleModeMsg->enVehicleConfig) ? true : false;
    	    	  if(NULL != poDiPOControlAdapter)
    	    	  {
    	    		 poDiPOControlAdapter->vSetLimitedUI(bDriveMode);
    	    		 bRetVal = true;
    	    	  }
    	    	  spi_tclDiPOControlAdapterImpl::vSetDriveModeInfo(bDriveMode);
    		  }
    		  break;
			  case e8_RIGHT_HAND_DRIVE:
			  case e8_LEFT_HAND_DRIVE:
			  {
				  tenDriveSideInfo enDriveSideInfo = (e8_RIGHT_HAND_DRIVE == poVehicleModeMsg->enVehicleConfig) ?
						  e8RIGHT_HAND_DRIVE :  e8LEFT_HAND_DRIVE;
				  spi_tclDiPOControlAdapterImpl::vSetDriveSideInfo(enDriveSideInfo);
				  bRetVal = true;
			  }
			  break;
    		  default:
    		  {
				  ETG_TRACE_ERR(("[ERR]:Vehicle configuration parameter is not valid/ not used for CarPlay."));
    		  }
    	  }//switch(poVehicle
      }//if(NULL != poVehicleModeMsg)
   }//if((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessInfoMessage(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessInfoMessage(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessInfoMessage entered"));
   t_Bool bStatus = false;
   
   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      Event *poEvent = Event::getInstance();
      trDiPOInfoMessage *poInfoMsg = NULL;
      poInfoMsg = static_cast<trDiPOInfoMessage*>(poMessage->pvBuffer);
      if((NULL != poInfoMsg) && (NULL != poEvent))
      {
         bStatus = true;
         poEvent->vLock();
          
         //! Set up the init config parameter which required on Plugin loaded

         t_String szBTAddress = poInfoMsg->szBtMacAdress;
         std::transform(szBTAddress.begin(), szBTAddress.end(), szBTAddress.begin(), ::toupper);

         trInitConfigParameters rInitConfigParameters( poInfoMsg->rVehicleConfigData.rVideoConfigData.u32ProjScreen_Height_Mm,
            poInfoMsg->rVehicleConfigData.rVideoConfigData.u32ProjScreen_Width_Mm,
            poInfoMsg->rVehicleConfigData.rVideoConfigData.u32ProjScreen_Height,
            poInfoMsg->rVehicleConfigData.rVideoConfigData.u32ProjScreen_Width,
            szBTAddress,coU8KnobHighFidelityTouch,
            poInfoMsg->rVehicleConfigData.rVehicleBrandInfo.szModel);

         rInitConfigParameters.u8DisplayInput = poInfoMsg->u8DisplayInput;

         vSetInitConfigParam(rInitConfigParameters);
         spi_tclDiPOKeyInputAdapterImpl::vSetDisplayInputParam(poInfoMsg->u8DisplayInput);

         // Set the parameters to control adapter to share the same with iPhone on session initialization
         spi_tclDiPOControlAdapterImpl::vSetBluetoothMacAddr(szBTAddress);
         spi_tclDiPOControlAdapterImpl::vSetVideoConfigInfo(poInfoMsg->rVehicleConfigData.rVideoConfigData);
         spi_tclDiPOControlAdapterImpl::vSetDriveSideInfo(poInfoMsg->rVehicleConfigData.enDriveSideInfo);
         spi_tclDiPOControlAdapterImpl::vSetDriveRestrictionInfo(poInfoMsg->u8DriveRestrictionInfo);

         spi_tclDiPOTouchInputAdapterImpl::vSetTouchProperty (poInfoMsg->rVehicleConfigData.rVideoConfigData.u32Screen_Height,poInfoMsg->rVehicleConfigData.rVideoConfigData.u32Screen_Width);
         spi_tclDiPOControlAdapterImpl::vSetVehicleBrandInfo(poInfoMsg->rVehicleConfigData.rVehicleBrandInfo);

         t_Bool bNightMode =(e8_NIGHT_MODE == poInfoMsg->enNightModeInfo) ? true : false;
         spi_tclDiPOControlAdapterImpl::vSetNightModeInfo(bNightMode);

         t_Bool bDriveMode = (e8DRIVE_MODE==poInfoMsg->enDriveModeInfo) ? true : false;
         spi_tclDiPOControlAdapterImpl::vSetDriveModeInfo(bDriveMode);
		 spi_tclDiPOControlAdapterImpl::vSetCPlayAutoLaunchFlag((tenAutoLaucnhFlag)poInfoMsg->enAutoLaucnhFlag);

         poEvent->vSignal();
         poEvent->vUnLock();
      }
   } // if(NULL != poEvent)
   return bStatus;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessVideoConfigMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessVideoConfigMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessVideoConfigMsg entered"));
   t_Bool bStatus = false;
   if((NULL != poMessage) && (poMessage->pvBuffer))
   {
      trDiPOVideoConfigMsg *poVideoConfigMsg = NULL;
      poVideoConfigMsg = static_cast<trDiPOVideoConfigMsg*>(poMessage->pvBuffer);
      if(NULL != poVideoConfigMsg)
      {
         spi_tclDiPOControlAdapterImpl::vSetVideoConfigInfo(poVideoConfigMsg->rVideoConfigData);
         bStatus=true;
      }//if(NULL != poVideoConfigMsg)
   }//if((NULL != poMessage) && (poMessage->pvBuffer))
   return bStatus;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessBtAddressUpdateMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessBtAddressUpdateMsg(tShlMessage *poMessage)
{
	ETG_TRACE_USR1(("spi_tclDiPOStartUp::bProcessBtAddressUpdateMsg entered"));
	t_Bool bStatus = false;

	if((NULL != poMessage) && (NULL != poMessage->pvBuffer))
	{
		trBtAddrUpdateMsg *BtAddrMsg = NULL;
		BtAddrMsg = static_cast<trBtAddrUpdateMsg *>(poMessage->pvBuffer);
		t_String szBTAddress = BtAddrMsg->cBTAddress;
        std::transform(szBTAddress.begin(), szBTAddress.end(), szBTAddress.begin(), ::toupper);
		spi_tclDiPOControlAdapterImpl::vSetBluetoothMacAddr(szBTAddress);
		bStatus = true;
	}//if((NULL != poMessage) && (NULL != poMessage->pvBuffer))
	return bStatus;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::bProcessRestrictionsUpdateMsg(tShlMessage *poMessage )
***************************************************************************/
t_Bool spi_tclDiPOStartUp::bProcessRestrictionsUpdateMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bStatus = false;

   if((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trRestrictionsUpdateMsg* prRestrictionsMsg =
            static_cast<trRestrictionsUpdateMsg*>(poMessage->pvBuffer);
      spi_tclDiPOControlAdapterImpl::vSetDriveRestrictionInfo(
            prRestrictionsMsg->u8DriveRestrictionInfo);
      bStatus = true;
   }//if((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   return bStatus;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::poGetMsgBuffer()
***************************************************************************/
tShlMessage* spi_tclDiPOStartUp::poGetMsgBuffer(size_t siBuffer)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::poGetMsgBuffer entered"));
   tShlMessage* poMessage = new tShlMessage;

   if (NULL != poMessage)
   {
      if(0 < siBuffer)
      {
         //! Allocate the requested memory
         poMessage->pvBuffer = new t_Char[siBuffer];
      }
      else
      {
         poMessage->pvBuffer = NULL;
      } // if(0 < siBuffer)

      if (NULL != poMessage->pvBuffer)
      {
         poMessage->size = siBuffer;
      }
      else
      {
         //! Free the message as internal allocation failed.
         delete poMessage;
         poMessage = NULL;
      } //   if (NULL != poMessage->pvBuffer)
   } // if (NULL != poMessage)

   return poMessage;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::vSetVideoMode()
***************************************************************************/
t_Void spi_tclDiPOStartUp::vSetVideoMode(trDiPOVideoContext &rfoDiPOVideoContext,
                                                    trModeChange &rfoModeChange)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::vSetVideoMode entered"));
   rfoModeChange.screen.type =(TransferType)rfoDiPOVideoContext.enTransferType;
   rfoModeChange.screen.priority = (TransferPriority)rfoDiPOVideoContext.enTransferPriority;
   rfoModeChange.screen.takeConstraint = (Constraint)rfoDiPOVideoContext.enTakeConstraint;
   rfoModeChange.screen.borrowOrUnborrowConstraint = (Constraint)rfoDiPOVideoContext.enBorrowConstraint; 
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::vSetAudioMode()
***************************************************************************/
t_Void spi_tclDiPOStartUp::vSetAudioMode(trDiPOAudioContext &rfoDiPOAudioContext,
                                                    trModeChange &rfoModeChange)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::vSetAudioMode entered"));
   rfoModeChange.audio.type = (TransferType)rfoDiPOAudioContext.enTransferType;
   rfoModeChange.audio.priority = (TransferPriority)rfoDiPOAudioContext.enTransferPriority;
   rfoModeChange.audio.takeConstraint = (Constraint)rfoDiPOAudioContext.enTakeConstraint;
   rfoModeChange.audio.borrowOrUnborrowConstraint = (Constraint)rfoDiPOAudioContext.enBorrowConstraint;
}

/***************************************************************************
** FUNCTION:  spi_tclDiPOStartUp::vSetAppState()
***************************************************************************/
t_Void spi_tclDiPOStartUp::vSetAppState(const trDiPOAppState &rfcorDiPOAppState,
                                                   trModeChange &rfoModeChange)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::vSetAppState entered"));

   rfoModeChange.speech = (SpeechMode)rfcorDiPOAppState.enSpeechAppState;
   rfoModeChange.phone = (AppState)rfcorDiPOAppState.enPhoneAppState;
   rfoModeChange.navigation = (AppState)rfcorDiPOAppState.enNavAppState;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOStartUp::bSendIPCMessage()
 ***************************************************************************/
template<typename trMessage>
t_Bool spi_tclDiPOStartUp::bSendIPCMessage(trMessage rMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::bSendIPCMessage entered"));
   t_Bool bRetVal = false;
   IPCMessageQueue oMessageQueue(DIPO_SPI_MSGQ_IPC_THREAD);

   trMessage *poBuffer = (trMessage *)oMessageQueue.vpCreateBuffer(sizeof(trMessage));

   if(NULL != poBuffer)
   {
      memset(poBuffer, 0, sizeof(trMessage));
      memcpy(poBuffer, &rMessage, sizeof(trMessage));
      bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poBuffer, DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poBuffer);
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOStartUp::vSetInitConfigParam()
 ***************************************************************************/
t_Void spi_tclDiPOStartUp::vSetInitConfigParam(trInitConfigParameters 
                                               rInitConfigParameters)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::vSetInitConfigParam entered"));
   m_rInitConfigParam = rInitConfigParameters;  
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOStartUp::vGetInitConfigParam()
 ***************************************************************************/
t_Void spi_tclDiPOStartUp::vGetInitConfigParam(trInitConfigParameters 
                                               &rfoInitConfigParameters)
{
   ETG_TRACE_USR1(("spi_tclDiPOStartUp::vGetInitConfigParam entered"));
   rfoInitConfigParameters = m_rInitConfigParam;
}
//lint –restore
