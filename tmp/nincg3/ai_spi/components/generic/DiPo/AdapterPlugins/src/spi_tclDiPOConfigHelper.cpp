/*!
*******************************************************************************
* \file              spi_tclDiPOConfigHelper.cpp
* \brief             DiPO Config helper class.
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Config helper class to setup configurations
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
21.1.2014 |  Shihabudheen P M            | Initial Version
26.05.2014| Hari Priya E R               | Included changes to read Screen size values
                                           from EOL Handling class


\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <stdio.h>
//#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include <utility>
#include "spi_tclDiPOConfigHelper.h"
#include "spi_tclConfigReader.h"

using namespace std;

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOConfigHelper.cpp.trc.h"
#endif 
/***************************************************************************
 ** FUNCTION:  spi_tclDiPOConfigHelper::spi_tclDiPOConfigHelper()
 ***************************************************************************/
spi_tclDiPOConfigHelper::spi_tclDiPOConfigHelper(): m_poConfig(NULL) 
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
}


/***************************************************************************
 ** FUNCTION:  spi_tclDiPOConfigHelper::~spi_tclDiPOConfigHelper()
 ***************************************************************************/
spi_tclDiPOConfigHelper::~spi_tclDiPOConfigHelper()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
}


/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOConfigHelper::vSetConfigurationHandler()
 ***************************************************************************/
t_Void spi_tclDiPOConfigHelper::vSetConfigurationHandler(IDynamicConfiguration *poConfig)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   m_poConfig = poConfig;
}

/***************************************************************************
 ** FUNCTION: IDynamicConfiguration* spi_tclDiPOConfigHelper::poGetConfigurationHandler()
 ***************************************************************************/
IDynamicConfiguration* spi_tclDiPOConfigHelper::poGetConfigurationHandler()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   return m_poConfig;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOConfigHelper::vSetVideoConfiguration()
 ***************************************************************************/
t_Bool spi_tclDiPOConfigHelper::vSetVideoConfiguration()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   t_Bool bRetVal = false;
   char buffer[2080];
   if(nullptr != m_poConfig)
   {
      bRetVal = true;

      spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
      if(NULL!= poConfigReader)
      {
         poConfigReader->vGetVideoConfigData(e8DEV_TYPE_DIPO,m_rVideoConfigData);

         m_poConfig->SetItem("display-width", to_string(m_rVideoConfigData.u32Screen_Width));

         m_poConfig->SetItem("display-heigh", to_string(m_rVideoConfigData.u32Screen_Height));

         m_poConfig->SetItem("wl-touch-layer-id", to_string(m_rVideoConfigData.u32TouchLayerId));

         m_poConfig->SetItem("wl-touch-surface-id", to_string(m_rVideoConfigData.u32TouchSurfaceId));

         // set up the video pipeline
         sprintf(buffer, "vpudec low-latency=true ! " \
            "gst_apx_sink display-width=%d display-height=%d " \
            "layer-id=%d surface-id=%d sync=false",
            m_rVideoConfigData.u32Screen_Width,
            m_rVideoConfigData.u32Screen_Height,
            m_rVideoConfigData.u32LayerId,
            m_rVideoConfigData.u32SurfaceId);

         // set the video pipeline to the config file
         m_poConfig->SetItem("gstreamer-video-pipeline", buffer);     
      }
   }
   return bRetVal;
}



/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOConfigHelper::vSetAudioConfiguration()
 ***************************************************************************/
t_Bool spi_tclDiPOConfigHelper::vSetAudioOutConfig(t_String szAudioDevice,  tenAudioStreamType enStreamType)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   printf("szAudioDevice: %s \n", szAudioDevice.c_str());
   t_Bool bRetVal = false;
   t_Char buffer[1024];
   if(nullptr != m_poConfig)
   {
      printf(" ");

      bRetVal = true;   
      if(e8MAIN_AUDIO == (t_U8)enStreamType)
      {
         // Set main audio configuration
         sprintf(buffer, "alsasink device=%s", szAudioDevice.c_str());
         m_poConfig->SetItem("gstreamer-main-audio-pipeline", buffer); 
         printf("Inside e8MAIN_AUDIO \n");
         fflush(stdout);
      }
      else if(e8ALTERNATE_AUDIO == (t_U8)enStreamType)
      {
         // Set Alternate audio pipeline
         sprintf(buffer, "alsasink device=%s", szAudioDevice.c_str());
         m_poConfig->SetItem("gstreamer-alternate-audio-pipeline", buffer);
         printf("Inside e8ALTERNATE_AUDIO \n");
         fflush(stdout);
      } //  if(enStreamType == e8MAIN_AUDIO)

      t_String szMainAudio = m_poConfig->GetItem("gstreamer-main-audio-pipeline", "").c_str();
      printf("Main Audio : %s \n", szMainAudio.c_str());
      fflush(stdout);
   }
   else
   {
      ETG_TRACE_ERR(("m_poConfig is NULL"));
   }

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOConfigHelper::vSetAudioInConfig()
 ***************************************************************************/
t_Void spi_tclDiPOConfigHelper::vSetAudioInConfig(t_String szAudioOutDev, t_String szAudioInDev)
{
    ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
    t_Char buffer[1024];
    printf("szAudioOutDev: %s \n", szAudioOutDev.c_str());
    printf("szAudioInDev: %s \n", szAudioInDev.c_str());
    if(NULL != m_poConfig)
    {
       // Set the main audio out pipeline
       sprintf(buffer, "alsasink device=%s", szAudioOutDev.c_str());
       m_poConfig->SetItem("gstreamer-main-audio-pipeline", buffer); 

       // set the audio in pipeline
       sprintf(buffer, "alsasrc device=%s", szAudioInDev.c_str());
       m_poConfig->SetItem("gstreamer-audio-in-pipeline", buffer);

    }
    else
    {
      ETG_TRACE_ERR(("m_poConfig is NULL"));
    }
}