/*!
*******************************************************************************
* \file              spi_tclDiPORecieverHelper.cpp
* \brief             DiPO IControlReceiver helper
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO helper class. This is a helper class to invoke the 
                IControlReciver functions from SPI. This class forward the 
                request to the IControlReceiver implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
21.1.2014 |  Shihabudheen P M            | Initial Version
30.04.2014 |  Hari Priya E R             | Included function for Siri Action
12.06.2014 |  Shihabudheen P M            | Included function for Blutooth ID

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclDiPOReceiverHelper.h"

#define SPI_ENABLE_DLT //enable DLT
#define SPI_LOG_CLASS Spi_CarPlay

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOReceiverHelper.cpp.trc.h"
#endif

//import the DLT context 
LOG_IMPORT_CONTEXT(Spi_CarPlay);
/***************************************************************************
 ** FUNCTION:  spi_tclDiPOReceiverHelper::spi_tclDiPOReceiverHelper()
 ***************************************************************************/
spi_tclDiPOReceiverHelper::spi_tclDiPOReceiverHelper(): m_poReciever(NULL)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOReceiverHelper::~spi_tclDiPOReceiverHelper()
 ***************************************************************************/
spi_tclDiPOReceiverHelper::~spi_tclDiPOReceiverHelper()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOReceiverHelper::bRequestDeviceUI(t_String szAppUrl)
 ***************************************************************************/
t_Void spi_tclDiPOReceiverHelper::vRequestDeviceUI(t_String szAppUrl)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   if(nullptr != m_poReciever)
   {
      // Invoke the IControlReciever function to request device UI.
	   m_poReciever->RequestUI(szAppUrl);
   }
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOReceiverHelper::vRequestResourceModeChange(..
 ***************************************************************************/
t_Void spi_tclDiPOReceiverHelper::vRequestResourceModeChange(const trModeChange rModeChange)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   ETG_TRACE_USR4(("[CarPlay]:vRequestResourceModeChange entered \n"));
   ETG_TRACE_USR4(("[CarPlay]:Screen,  Type: %d", rModeChange.screen.type));
   ETG_TRACE_USR4(("[CarPlay]:Screen,  Priority: %d \n", rModeChange.screen.priority));
   ETG_TRACE_USR4(("[CarPlay]:Screen,  Constraint: %d \n", rModeChange.screen.takeConstraint));
   ETG_TRACE_USR4(("[CarPlay]:Screen,  Borrow const: %d \n", rModeChange.screen.borrowOrUnborrowConstraint));

   ETG_TRACE_USR4(("[CarPlay]:Audio,  Type: %d \n", rModeChange.audio.type));
   ETG_TRACE_USR4(("[CarPlay]:Audio,  Priority: %d \n", rModeChange.audio.priority));
   ETG_TRACE_USR4(("[CarPlay]:Audio,  Constraint: %d \n", rModeChange.audio.takeConstraint));
   ETG_TRACE_USR4(("[CarPlay]:Audio,  Borrow const: %d \n", rModeChange.audio.borrowOrUnborrowConstraint));
   if(nullptr != m_poReciever)
   {
      // Invoke the IControlReciever function to mode change.
	   m_poReciever->ChangeResourceMode(rModeChange);
   }
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOReceiverHelper::vRequestSiriAction(..
 ***************************************************************************/
t_Void spi_tclDiPOReceiverHelper::vRequestSiriAction(SiriAction enSiriAction)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   if(nullptr != m_poReciever)
   {
      // Invoke the IControlReciever function to initiate Siri Action.
	   m_poReciever->RequestSiriAction(enSiriAction);
   }
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOReceiverHelper::vSetControlReceiverHandler(..
 ***************************************************************************/
t_Void spi_tclDiPOReceiverHelper::vSetControlReceiverHandler(IControlReceiver *poReceiver)
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   m_poReciever = poReceiver;
}

/***************************************************************************
 ** FUNCTION: IControlReceiver spi_tclDiPOReceiverHelper::poGetConrolReceiverHandler
 ***************************************************************************/
IControlReceiver* spi_tclDiPOReceiverHelper::poGetControlReceiverHandler()
{
   ETG_TRACE_USR1((" %s entered \n", __FUNCTION__));
   return m_poReciever;
}

/***************************************************************************
 ** FUNCTION: IControlReceiver spi_tclDiPOReceiverHelper::poGetConrolReceiverHandler
 ***************************************************************************/
t_Void spi_tclDiPOReceiverHelper::vSetBlutoothID(const std::list<std::string>& rfDeviceIDList) const
{
   printf("[CarPlay] : vSetBlutoothID called ");
   if(NULL != m_poReciever)
   {
      m_poReciever->SetBluetoothIDs(rfDeviceIDList);
   }    
}