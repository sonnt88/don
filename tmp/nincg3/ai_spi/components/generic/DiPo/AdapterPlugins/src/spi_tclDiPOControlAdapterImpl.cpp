/*!
*******************************************************************************
* \file              spi_tclDiPOControlAdapterImpl.cpp
* \brief             DiPO IControlAdapter impmentation
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO IControlAdapter implementation. This class implement all the 
                functions of the IControlAdapter interface of ADIT. This class
                is used to handle the callbacks from iOS device.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
21.1.2014 |  Shihabudheen P M            | Initial Version
18.4.2014 |  Shihabudheen P M            | Modified for resource arbitration
07.05.2014|  Shihabudheen P M            | Modified for resource arbitration 
13.10.2014| Vinoop U					 | Changes to set OEM icon Image And OEM icon label
25.10.2014 |  Shihabudheen P M           | updated video pipeline with dis-reorder=true.
15.12.2014 | Tejaswini HB                | Lint fix
27.05.2015 |  Tejaswini H B(RBEI/ECP2)   | Added Lint comments to suppress C++11 Errors
20.08.2015 |  Shihabudheen P M           | Added methods to dynamically set the limited UI elements.
08.01.2015 | Shihabudheen P M           | Code improvements.

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "IPCMessageQueue.h"
#include "spi_tclDiPOControlAdapterImpl.h"
#include "Timer.h"

using namespace std;

#define SPI_ENABLE_DLT //enable DLT
#define SPI_LOG_CLASS Spi_CarPlay

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DIPO
#include "trcGenProj/Header/spi_tclDiPOControlAdapterImpl.cpp.trc.h"
#endif 
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//! import the DLT context 
LOG_IMPORT_CONTEXT(Spi_CarPlay);

#define MAX_BUF_SIZE   256
trModeChange g_rModeChange;
spi_tclDiPOControlAdapterImpl* spi_tclDiPOControlAdapterImpl::m_poDiPOControlAdapter = NULL;
trModeChange spi_tclDiPOControlAdapterImpl::m_AccessoryMode = g_rModeChange;
t_String spi_tclDiPOControlAdapterImpl::m_szBlutoothMac = "";
t_Bool spi_tclDiPOControlAdapterImpl::m_bCurNightModeInfo = false;
t_Bool spi_tclDiPOControlAdapterImpl::m_bCurDriveModeInfo = false;
tenAutoLaucnhFlag spi_tclDiPOControlAdapterImpl::m_enCPlayAutoLaunchFlag = e8AUTOLAUNCH_DISABLED;
trDiPODisplayAttributes spi_tclDiPOControlAdapterImpl::m_rDiPODisplayAttr;
trDiPOTouchInputAttributes spi_tclDiPOControlAdapterImpl::m_rDiPOTouchInputAttr;
trDiPOInfoRespParam spi_tclDiPOControlAdapterImpl::m_rDiPOInfoRespParam;
std::map<t_String, tenDiPOMainAudioType> spi_tclDiPOControlAdapterImpl::m_mAudioTypeMap =
		spi_tclDiPOControlAdapterImpl::vInitializeAudioTypeMap();
t_U8 spi_tclDiPOControlAdapterImpl::m_u8LimitedUIElementCount = 0;

static t_U32 su32TimerInterval=1000;
static timer_t srTimerID  = 0;
static Lock rAdaperSyncLock;

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOControlAdapterImpl::spi_tclDiPOControlAdapterImpl()
 ***************************************************************************/
spi_tclDiPOControlAdapterImpl::spi_tclDiPOControlAdapterImpl():
 m_poIDynamicConfig(NULL), 
 m_poIControlReceiver(NULL), 
 m_CurrAudioMessage(e8AUDIO_MESSAGE),
 m_bIsSessionStarted(false),
 m_bRequestUIStatus(false)
{
   ETG_TRACE_USR1((" spi_tclDiPOControlAdapterImpl::spi_tclDiPOControlAdapterImpl entered"));
   rAdaperSyncLock.s16Lock();
   m_poDiPOControlAdapter = this;
   rAdaperSyncLock.vUnlock();
   
   if(e8AUTOLAUNCH_DISABLED == m_enCPlayAutoLaunchFlag)
   {
	   // Need to explicitly set the display resource parameters, to avoid the taking of display
	   // resource  by device upon startup. For audio and App state, the current mode of the vehicle
	   // is enough.
       m_AccessoryMode.screen.type = TransferType_Take;
   	   m_AccessoryMode.screen.priority = TransferPriority_UserInitiated;
   	   m_AccessoryMode.screen.takeConstraint = Constraint_Never;
	   m_AccessoryMode.screen.borrowOrUnborrowConstraint = Constraint_UserInitiated;
   } // if(e8AUTOLAUNCH_DISABLED == m_enCPlayAutoLaunchFlag)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOControlAdapterImpl::~spi_tclDiPOControlAdapterImpl()
 ***************************************************************************/
spi_tclDiPOControlAdapterImpl::~spi_tclDiPOControlAdapterImpl()
{
   ETG_TRACE_USR1((" spi_tclDiPOControlAdapterImpl::~spi_tclDiPOControlAdapterImpl entered"));
   rAdaperSyncLock.s16Lock();
   m_bIsSessionStarted = false;
   m_poIDynamicConfig = NULL;
   m_poIControlReceiver = NULL;
   m_poDiPOControlAdapter = NULL;
   rAdaperSyncLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclDiPOControlAdapterImpl::Initialize(IDynamicConfiguration ..)
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Bool spi_tclDiPOControlAdapterImpl::Initialize(IDynamicConfiguration& rfoConfig, 
                                                 IControlReceiver& rfoControl)
{ 
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::Initialize entered"));

   rAdaperSyncLock.s16Lock();
   m_poIDynamicConfig   = &rfoConfig;
   SPI_NORMAL_ASSERT(NULL == m_poIDynamicConfig);
   m_poIControlReceiver = &rfoControl;
   SPI_NORMAL_ASSERT(NULL == m_poIControlReceiver);
   rAdaperSyncLock.vUnlock();

   vSetVideoConfiguration();
   vSetBluetoothIds();
   vSetNightMode(m_bCurNightModeInfo);
   vConfigLimitedUIElements();
   vSetLimitedUI(m_bCurDriveModeInfo);

   return ((NULL != m_poIDynamicConfig) && (NULL != m_poIControlReceiver));
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclDiPOControlAdapterImpl::Initialize(IDynamicConfiguration ..)
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnSessionStart()
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnSessionStart entered"));
   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
   {
      poTimer->StartTimer( srTimerID, su32TimerInterval,
         0, this,bSessionStartTimerCb,NULL );
   }//if (NULL != poTimer) 
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclDiPOControlAdapterImpl::OnSessionEnd()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnSessionEnd()
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnSessionEnd entered"));
   trDiPOSessionMsg rDiPOSessionMsg;
   rDiPOSessionMsg.enMsgType = e8SESSION_MESSAGE;
   rDiPOSessionMsg.enDiPOSessionState = e8DIPO_SESSION_END;

   rAdaperSyncLock.s16Lock();
   m_poIDynamicConfig = NULL;
   m_poIControlReceiver = NULL;
   rAdaperSyncLock.vUnlock();

   t_Bool bStatus = bSendIPCMessage<trDiPOSessionMsg>(rDiPOSessionMsg);
   ETG_TRACE_USR2(("[DESC]: Session termination message sent to SPI with status = %d", ETG_ENUM( STATUS, bStatus)));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDiPOControlAdapterImpl* spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance()
 ***************************************************************************/
spi_tclDiPOControlAdapterImpl* spi_tclDiPOControlAdapterImpl::poGetDiPOControlAdapterInstance()
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnSessionEnd entered"));
   rAdaperSyncLock.s16Lock();
   spi_tclDiPOControlAdapterImpl *poAdapterHandler = m_poDiPOControlAdapter;
   rAdaperSyncLock.vUnlock();
   return poAdapterHandler;
}

/***************************************************************************
 ** FUNCTION:  bool spi_tclDiPOControlAdapterImpl::OnModesChanged()...
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnModesChanged(trModeState rState)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnModesChanged entered"));

   if(false == m_bIsSessionStarted)
   {
      Timer* poTimer = Timer::getInstance();
  	  if ((NULL != poTimer) && (0 != srTimerID))
   	  {
      	poTimer->CancelTimer(srTimerID);
      	srTimerID  =0;
	  }

      //! Inform SPI about the session startup during the first mode change message
      trDiPOSessionMsg rDiPOSessionMsg;
      rDiPOSessionMsg.enMsgType = e8SESSION_MESSAGE;
      rDiPOSessionMsg.enDiPOSessionState = e8DIPO_SESSION_START;
      t_Bool bStatus = bSendIPCMessage<trDiPOSessionMsg>(rDiPOSessionMsg); 
      ETG_TRACE_USR2(("[DESC]: Session start message sent to SPI with status = %d", ETG_ENUM( STATUS, bStatus)));
      m_bIsSessionStarted = true;
   }

   // Populate message data
   trDiPORMMsgResp rDiPORMMsgResp;
   rDiPORMMsgResp.enMsgType = e8RM_RESP_MESSAGE;
   vConvertModeState(rState, rDiPORMMsgResp.rDiPOModeState);
   
   // Keep track of the latest mode changed message from iPhone.
   m_rCurrentModeState = rDiPORMMsgResp.rDiPOModeState;

   ETG_TRACE_USR4(("[DESC]: modesChanged message received from iPhone with following States :: "
		   "Display State = %d, Audio State = %d, Phone State = %d, Navigation state = %d, Speech State = %d",
		   ETG_ENUM( CARPLAY_RESOURCE_STATE, rState.screen),
		   ETG_ENUM( CARPLAY_RESOURCE_STATE, rState.audio),
		   ETG_ENUM( CARPLAY_APP_STATE, rState.phone),
		   ETG_ENUM( CARPLAY_APP_STATE, rState.navigation),
		   ETG_ENUM( CARPLAY_APP_STATE, rState.speech.entity)));

   t_Bool bStatus = bSendIPCMessage<trDiPORMMsgResp>(rDiPORMMsgResp);
   ETG_TRACE_USR2(("[DESC]: modesChanged message with new modes sent to SPI with status = %d", ETG_ENUM( STATUS, bStatus)));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::OnRequestUI()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnRequestUI()
{
   // switch to the native HMI
	ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnRequestUI entered"));

   trOnRequestUIMsg rOnRequestUIMsg;
   rOnRequestUIMsg.enMsgType =e8ON_REQUEST_UI_MESSAGE;
   rOnRequestUIMsg.bRequestUIStatus = true; //Always true.

   t_Bool bStatus = bSendIPCMessage<trOnRequestUIMsg>(rOnRequestUIMsg);
   ETG_TRACE_USR2(("[DESC]: onRequestUI call received from iPhone and the same is passed to SPI with status: %d", ETG_ENUM( STATUS, bStatus)));
}


/***************************************************************************
 ** FUNCTION:  bool spi_tclDiPOControlAdapterImpl::InitializeInternal()...
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnAudioPrepare(AudioChannelType channel, 
                                                     const t_String& audioType)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnAudioPrepare entered"));
   SPI_INTENTIONALLY_UNUSED(channel);
   SPI_INTENTIONALLY_UNUSED(audioType);
   //@Note : Nothing to do here. AudioPrepare is handled from AudioOutAdapterImpl.
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::OnAudioStop()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnAudioStop(AudioChannelType enChannel)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnAudioStop entered"));
   SPI_INTENTIONALLY_UNUSED(enChannel);
  //@Note : Nothing to do here. AudioStop is handled from AudioOutAdapterImpl.
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::OnGetBluetoothIDs()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnGetBluetoothIDs(std::list<std::string>& deviceIDs)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnGetBluetoothIDs entered"));
   deviceIDs.push_back(m_szBlutoothMac);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::OnGetNightMode()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Bool spi_tclDiPOControlAdapterImpl::OnGetNightMode()
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnGetNightMode entered"));
   return true;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::OnRampVolume()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnRampVolume
(
 t_Double d64FinalVolume, 
 t_Double d64Duration
)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnRampVolume entered"));
   trAudioDuckMsg rDuckMsg;

   rDuckMsg.enMsgType = e8AUDIO_DUCK_MESSAGE;
   rDuckMsg.dFinalVolume = d64FinalVolume;
   rDuckMsg.dDurationInMs = d64Duration;

   t_Bool bStatus = bSendIPCMessage<trAudioDuckMsg>(rDuckMsg);
   ETG_TRACE_USR2(("[DESC]: Audio duck request received from iPhone with finalVolume = %f ,duration = %f and request forwarded to SPI with status = %d",
		   d64FinalVolume, d64Duration,  ETG_ENUM( STATUS, bStatus)));

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::OnDisableBluetooth()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnDisableBluetooth(const t_String& szDeviceId)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnDisableBluetooth entered"));
   t_Bool bStatus = false;
   ETG_TRACE_USR2(("[DESC]: Disable Bluetooth command received from iPhone with MAC address : %s", szDeviceId.c_str()));
   trOnDisBluetoothMsg rOnDisBluetoothMsg;
   rOnDisBluetoothMsg.enMsgType = e8DISABLE_BLUETOOTH_MESSAGE;
   strncpy(rOnDisBluetoothMsg.cBluetoothID, szDeviceId.c_str(), MAX_STR_LEN);
   bStatus = bSendIPCMessage<trOnDisBluetoothMsg>(rOnDisBluetoothMsg);
   ETG_TRACE_USR2(("[DESC]: Disable bluetooth command sent to SPI with status = %d", ETG_ENUM( STATUS, bStatus)));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::OnGetCurrentResourceMode()
 ***************************************************************************/
/* ADIT Interface Implementation.*/
t_Void spi_tclDiPOControlAdapterImpl::OnGetCurrentResourceMode(trModeChange& rfoModeChange)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::OnGetCurrentResourceMode entered"));
   rfoModeChange = m_AccessoryMode;
   //The Checking below is required to avoid sending empty valuse to iPhone
   // while the current values are not yet available. Need for initial handshaking.
   if(TransferType_NA == rfoModeChange.screen.type)
   {
	   rfoModeChange.screen.type = TransferType_Take;
	   rfoModeChange.screen.priority = TransferPriority_UserInitiated;
	   rfoModeChange.screen.takeConstraint = Constraint_UserInitiated;
	   rfoModeChange.screen.borrowOrUnborrowConstraint = Constraint_UserInitiated;
   }
   if(TransferType_NA == rfoModeChange.audio.type)
   {
	   rfoModeChange.audio.type = TransferType_Take;
	   rfoModeChange.audio.priority = TransferPriority_UserInitiated;
	   rfoModeChange.audio.takeConstraint = Constraint_UserInitiated;
	   rfoModeChange.audio.borrowOrUnborrowConstraint = Constraint_UserInitiated;
   }
   if(SpeechMode_NA  == rfoModeChange.speech)
   {
	   rfoModeChange.speech = SpeechMode_None;
   }
   if(AppState_NA == rfoModeChange.phone)
   {
	   rfoModeChange.phone = AppState_False;
   }
   if(AppState_NA == rfoModeChange.navigation)
   {
	   rfoModeChange.navigation = AppState_False;
   }
}


/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::vSetVideoConfiguration()
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetVideoConfiguration()
{
   /*lint -esym(746,to_string) function to_string not made in the presence of a prototype */

   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetVideoConfiguration entered"));
   if(NULL != m_poIDynamicConfig)
   {

	  rAdaperSyncLock.s16Lock();

      t_Char szVideoPipeline[MAX_BUF_SIZE] = {0};

      m_poIDynamicConfig->SetItem("display-width", to_string(m_rDiPODisplayAttr.u32Width));

      m_poIDynamicConfig->SetItem("display-height", to_string(m_rDiPODisplayAttr.u32Height));

      m_poIDynamicConfig->SetItem("wl-touch-layer-id", to_string(m_rDiPOTouchInputAttr.u32TouchLayerId));

      m_poIDynamicConfig->SetItem("wl-touch-surface-id", to_string(m_rDiPOTouchInputAttr.u32TouchSurfaceId));

      t_String szDriveSideInfo = (e8RIGHT_HAND_DRIVE == m_rDiPOInfoRespParam.enDriveSideInfo) ? "1" : "0";
      m_poIDynamicConfig->SetItem("core-rightHandDrive", szDriveSideInfo);

      m_poIDynamicConfig->SetItem("display-width-millimeter", to_string(m_rDiPODisplayAttr.u32Width_Millimeter));
      m_poIDynamicConfig->SetItem("display-height-millimeter", to_string(m_rDiPODisplayAttr.u32Height_Millimeter));

      // set up the video pipeline
      sprintf(szVideoPipeline, "vpudec low-latency=true frame-plus=2 framedrop=false framerate-nu=60 dis-reorder=true ! "
      "gst_apx_sink display-width=%d display-height=%d "
      "layer-id=%d surface-id=%d sync=false qos=false max-lateness=3000000000",
      m_rDiPODisplayAttr.u32Width,
      m_rDiPODisplayAttr.u32Height,
      m_rDiPODisplayAttr.u32LayerId,
      m_rDiPODisplayAttr.u32SurfaceId);

      ETG_TRACE_USR2(("[DESC]: CarPlay video pipeline configured = %s", szVideoPipeline));

      // set the video pipeline to the config file
      m_poIDynamicConfig->SetItem("gstreamer-video-pipeline", szVideoPipeline);
      m_poIDynamicConfig->SetItem("core-oemIconPath", m_rDiPOInfoRespParam.szOemIconPath);
      m_poIDynamicConfig->SetItem("core-oemIconLabel", m_rDiPOInfoRespParam.szOemIcon);
      // Core model is same as oem icon name.
      m_poIDynamicConfig->SetItem("core-model",m_rDiPOInfoRespParam.szModelName);
      m_poIDynamicConfig->SetItem("core-manufacturer",m_rDiPOInfoRespParam.szManufacturer);
	  rAdaperSyncLock.vUnlock();
   }//End of if(NULL != poConfigReader) && (NULL != m_poIDynamicConfig))
}


/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::vConfigLimitedUIElements()
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vConfigLimitedUIElements()
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vConfigLimitedUIElements entered"));
   t_U8 u8BitMask = 1;
   t_U8 u8itemCount = 0;

   for (t_U8 u8Iterator = 0; (u8Iterator < 8) && (u8BitMask != 0); u8Iterator++)
   {
      t_U8 u8TempValue = m_rDiPOInfoRespParam.u8DriveRestrictionInfo & u8BitMask;
      t_Char szKeyValueName[100];
      sprintf(szKeyValueName, "limited-ui-elements-%d", u8itemCount);
      switch (u8TempValue)
      {
         case e8SOFT_KEYBOARD:
         {
            u8itemCount++;
            if (NULL != m_poIDynamicConfig)
            {
               rAdaperSyncLock.s16Lock();
               m_poIDynamicConfig->SetItem(szKeyValueName, "softKeyboard");
               rAdaperSyncLock.vUnlock();
            }//if(NULL != m_poIDynamicConfig)
         }
            break;
         case e8SOFT_PHONEKEYPAD:
         {
            u8itemCount++;
            if (NULL != m_poIDynamicConfig)
            {
               rAdaperSyncLock.s16Lock();
               m_poIDynamicConfig->SetItem(szKeyValueName, "softPhoneKeypad");
               rAdaperSyncLock.vUnlock();
            }//if(NULL != m_poIDynamicConfig)
         }
            break;
         case e8NON_MUSIC_LIST:
         {
            u8itemCount++;
            if (NULL != m_poIDynamicConfig)
            {
               rAdaperSyncLock.s16Lock();
               m_poIDynamicConfig->SetItem(szKeyValueName, "nonMusicLists");
               rAdaperSyncLock.vUnlock();
            }//if(NULL != m_poIDynamicConfig)

         }
            break;
         case e8MUSIC_LIST:
         {
            u8itemCount++;
            if (NULL != m_poIDynamicConfig)
            {
               rAdaperSyncLock.s16Lock();
               m_poIDynamicConfig->SetItem(szKeyValueName, "musicLists");
               rAdaperSyncLock.vUnlock();
            }//if(NULL != m_poIDynamicConfig)
         }
            break;
         case e8JAPAN_MAPS:
         {
            u8itemCount++;
            if (NULL != m_poIDynamicConfig)
            {
               rAdaperSyncLock.s16Lock();
               m_poIDynamicConfig->SetItem(szKeyValueName, "japanMaps");
               rAdaperSyncLock.vUnlock();
            }//if(NULL != m_poIDynamicConfig)
         }
            break;
         default:
         {
            //do nothing
         }
            break;
      } //switch(u8TemoValue)
      u8BitMask = u8BitMask << 1;
   }//for(t_U8 u8Iterator = 0; u8Iterator <8; u8Iterator++)

   m_u8LimitedUIElementCount = u8itemCount;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bSetAudioConfiguration()
 ***************************************************************************/
t_Bool spi_tclDiPOControlAdapterImpl::bSetAudioOutConfig(const t_String szAudioDevice,
		const tenAudioStreamType enStreamType,
		const tenMsgTypes enMsgTypes)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::bSetAudioOutConfig entered"));
   /*ETG_TRACE_USR4(("[PARAM]:bSetAudioOutConfig received with streamType = %d, deviceName = %s, messageType = %d",
		   ETG_ENUM(DIPO_AUDIO_STREAMTYPE , enStreamType), szAudioDevice.c_str(), enMsgTypes));*/
   t_Bool bRetVal = false;
   t_Char szAudPipeline[MAX_BUF_SIZE] = {0};
   SPI_INTENTIONALLY_UNUSED(szAudPipeline);
    rAdaperSyncLock.s16Lock();
   if((NULL != m_poIDynamicConfig) && (false == szAudioDevice.empty()))
   {
      if((e8ALTERNATE_AUDIO == enStreamType) && (e8AUDIO_MESSAGE == enMsgTypes))
      {
         if(e8AUDIO_MESSAGE != m_CurrAudioMessage)
         {
            t_String szDummyAudioPipe = m_poIDynamicConfig->GetItem("alsa-alternate-audio-dummy","");
            t_String szActualAudioPipe = m_poIDynamicConfig->GetItem("alsa-alternate-audio-0","");
            m_poIDynamicConfig->SetItem("alsa-alternate-audio-0", szDummyAudioPipe);
            m_poIDynamicConfig->SetItem("alsa-alternate-audio-dummy", szActualAudioPipe);
           
         }//if(e8AUDIO_MESSAGE!= m_CurrAudioMessage)
         m_CurrAudioMessage = e8AUDIO_MESSAGE;  
      } //  if(enStreamType == e8MAIN_AUDIO)
      else if((e8ALTERNATE_AUDIO == enStreamType) && (e8AUDIO_ERROR_MESSAGE == enMsgTypes))
      {
         if( e8AUDIO_ERROR_MESSAGE!= m_CurrAudioMessage)
         {
            t_String szDummyAudioPipe = m_poIDynamicConfig->GetItem("alsa-alternate-audio-dummy","");
            t_String szActualAudioPipe = m_poIDynamicConfig->GetItem("alsa-alternate-audio-0","");
            m_poIDynamicConfig->SetItem("alsa-alternate-audio-0", szDummyAudioPipe);
            m_poIDynamicConfig->SetItem("alsa-alternate-audio-dummy", szActualAudioPipe);
         }
         m_CurrAudioMessage = e8AUDIO_ERROR_MESSAGE;
      }//else if((e8ALTERNATE_AUDIO == enStreamType) && (e8AUDIO_ERROR_MESSAGE == enMsgTypes))
	   bRetVal = true;
   }
   rAdaperSyncLock.vUnlock();
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bSetAudioInConfig()
 ***************************************************************************/
 //This function is not used currently. As per the new audio design, all the configurations are statically
 //set in the config file. The function kept for future use.
t_Bool spi_tclDiPOControlAdapterImpl::bSetAudioInConfig(const t_String szAudioOutDev, const t_String szAudioInDev)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::bSetAudioInConfig entered"));
   SPI_INTENTIONALLY_UNUSED(szAudioOutDev);
   SPI_INTENTIONALLY_UNUSED(szAudioInDev);
   return true;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bLaunchApp(t_String szAppUrl)
 ***************************************************************************/
t_Bool spi_tclDiPOControlAdapterImpl::bLaunchApp(const t_String szAppUrl)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::bLaunchApp entered"));
   ETG_TRACE_USR4(("[PARAM]:: bLaunchApp - url = %s\n", szAppUrl.c_str()));
   t_Bool bRetVal = false;
   
   rAdaperSyncLock.s16Lock();
   if(NULL != m_poIControlReceiver)
   {
      if((Constraint_Never == m_AccessoryMode.screen.takeConstraint) || (false == m_bRequestUIStatus))
      {
         // It required to provide a untake of the display resource before calling LaunchApp.
         // This is because on startup the take constraint for display resource is set to 
         // NEVER to avoid the taking of display resource on session startup. So unless,
         // explicitly given a untake command with LaunchApp request will not succeeded.
         trModeChange rModeChange;
         rModeChange.screen.type = TransferType_Untake;
         rModeChange.screen.priority = TransferPriority_UserInitiated;
         rModeChange.screen.takeConstraint = Constraint_NA;
         rModeChange.screen.borrowOrUnborrowConstraint = Constraint_NA;
         vSetCurrentAccessoryMode(rModeChange, e8MODE_CHANGE_DISPLAY);
         m_poIControlReceiver->ChangeResourceMode(rModeChange);
      }// if(Constraint_Never == m_AccessoryMode.screen.takeConstraint)

      // Set the request UI status to true since to indicate that the application launch is happened
      // at least once from the accessory side.
      m_bRequestUIStatus = true;

      // Invoke the IControlReciever function to request device UI.
      m_poIControlReceiver->RequestUI(szAppUrl);
      bRetVal = true;
   }
   rAdaperSyncLock.vUnlock();
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bAccessoryModeChange(..)
 ***************************************************************************/
t_Bool spi_tclDiPOControlAdapterImpl::bAccessoryModeChange(const trModeChange& rfcorModeChange,
		const tenAccModeChangeType enAccModeChangeType)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::bAccessoryModeChange entered"));

   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Display transfer type  = %d", ETG_ENUM( DIPO_TRANSFERTYPE, rfcorModeChange.screen.type)));
   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Display transfer priority  = %d", ETG_ENUM( DIPO_TRANSFERPRIO, rfcorModeChange.screen.priority)));
   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Display take constraint  = %d", ETG_ENUM( DIPO_CONSTRAINT, rfcorModeChange.screen.takeConstraint)));
   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Display borrow constraint  = %d", ETG_ENUM( DIPO_CONSTRAINT, rfcorModeChange.screen.borrowOrUnborrowConstraint)));

   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Audio transfer type  = %d", ETG_ENUM( DIPO_TRANSFERTYPE, rfcorModeChange.audio.type)));
   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Audio transfer priority  = %d", ETG_ENUM( DIPO_TRANSFERPRIO, rfcorModeChange.audio.priority)));
   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Audio take constraint = %d", ETG_ENUM( DIPO_CONSTRAINT, rfcorModeChange.audio.takeConstraint)));
   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Audio borrow constraint  = %d", ETG_ENUM( DIPO_CONSTRAINT, rfcorModeChange.audio.borrowOrUnborrowConstraint)));

   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Speech state  = %d", ETG_ENUM(SPEECH_APP_STATE, rfcorModeChange.speech)));
   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Phone state  = %d", ETG_ENUM(PHONE_APP_STATE, rfcorModeChange.phone)));
   ETG_TRACE_USR4(("[PARAM]:: bAccessoryModeChange - Navigation state = %d", ETG_ENUM(NAV_APP_STATE, rfcorModeChange.navigation)));

   t_Bool bUpdateRequired = false;
   rAdaperSyncLock.s16Lock();

   if(NULL != m_poIControlReceiver)
   {
	   //@Note: The following condition needs to check before passing the resource transfer request to iPhone.
	   // 1. RequestUI is not called for atleast once.
	   // 2. Automatic launching of CarPlay is disabled.
	   // 3. If the mode change request is for display resource.
	   // 4. The display resource is with CAR.
	   if((false == m_bRequestUIStatus) && 
	   (e8AUTOLAUNCH_DISABLED == m_enCPlayAutoLaunchFlag) && 
	   (e8MODE_CHANGE_DISPLAY == enAccModeChangeType) &&
	   (e8DIPO_ENTITY_CAR == m_rCurrentModeState.enScreen))
	   {
	       //! If there is transfer request with borrow constraint as Never, then the same must be forwarded
		   if((TransferType_Take == rfcorModeChange.screen.type ||
				   TransferType_Borrow == rfcorModeChange.screen.type ) &&
				   (Constraint_Never == rfcorModeChange.screen.borrowOrUnborrowConstraint))
		   {
			   bUpdateRequired = true;
		   }
		   else if(TransferType_Untake == rfcorModeChange.screen.type ||
				   TransferType_Unborrow == rfcorModeChange.screen.type)
		   {
		       //! If there are any untake or unborrow request to clear the never constraint for borrow the same must be allowed.
			   bUpdateRequired = true;
		   }
	   }
	   else
	   {
		   bUpdateRequired = true;
	   }

	   if(true == bUpdateRequired)
	   {
		   m_poIControlReceiver->ChangeResourceMode(rfcorModeChange);
		   vSetCurrentAccessoryMode(rfcorModeChange, enAccModeChangeType);

	   }
   }
   rAdaperSyncLock.vUnlock();
   return bUpdateRequired;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bSiriAction(..
 ***************************************************************************/
t_Bool spi_tclDiPOControlAdapterImpl::bSiriAction(const SiriAction enSiriAction)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::bSiriAction entered"));
   t_Bool bRetVal = false;

   ETG_TRACE_USR4(("[PARAM]:: bSiriAction - Siri Action  = %d", ETG_ENUM( CARPLAY_SIRI_EVENT, enSiriAction)));
   rAdaperSyncLock.s16Lock();
   if(NULL != m_poIControlReceiver)
   {
      // Invoke the IControlReciever function to initiate Siri Action.
      m_poIControlReceiver->RequestSiriAction(enSiriAction);
      bRetVal = true;
   }
   rAdaperSyncLock.vUnlock();
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::bSetNightMode(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetNightMode(const t_Bool bIsNightMode)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetNightMode entered"));
   ETG_TRACE_USR4(("[PARAM]:: vSetNightMode - NightMode  = %d", ETG_ENUM( BOOL, bIsNightMode)));
   rAdaperSyncLock.s16Lock();
   if(NULL != m_poIControlReceiver)
   {
      m_poIControlReceiver->SetNightMode(bIsNightMode);
   }
   rAdaperSyncLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::vSetLimitedUI(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetLimitedUI(const t_Bool bLimitedUIStatus)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetLimitedUI entered"));
   ETG_TRACE_USR4(("[PARAM]:: vSetLimitedUI - Limited UI = %d", ETG_ENUM( BOOL, bLimitedUIStatus)));
   rAdaperSyncLock.s16Lock();
   if((NULL != m_poIControlReceiver) && (0 != m_u8LimitedUIElementCount))
   {
      m_poIControlReceiver->SetLimitedUI(bLimitedUIStatus);
   }
   rAdaperSyncLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::vSetBluetoothIds(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetBluetoothIds()
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetBluetoothIds entered"));
   ETG_TRACE_USR4(("[PARAM]:: vSetBluetoothIds - Bluetooth Id = %s", m_szBlutoothMac.c_str()));
   
   rAdaperSyncLock.s16Lock();
   if(NULL != m_poIControlReceiver)
   {
      std::list<std::string> deviceIDs;
      deviceIDs.push_back(m_szBlutoothMac);
      m_poIControlReceiver->SetBluetoothIDs(deviceIDs);
   }
   rAdaperSyncLock.vUnlock();
}

//!static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vSetCurrentAccessoryMode(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetCurrentAccessoryMode(const trModeChange &corModeChange, 
      const tenAccModeChangeType enAccModeChangeType)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetCurrentAccessoryMode entered"));
   ETG_TRACE_USR4(("[PARAM]:: vSetCurrentAccessoryMode - Mode Change Type Id = %d", ETG_ENUM(MODE_UPDATE, enAccModeChangeType)));
   switch(enAccModeChangeType)
   {
   case e8MODE_CHANGE_DISPLAY:
      {
    	 m_AccessoryMode.screen = corModeChange.screen;
      }
      break;
   case e8MODE_CHANGE_AUDIO:
      {
    	 m_AccessoryMode.audio = corModeChange.audio;
      }
      break;
   case e8MODE_CHANGE_APP:
      {
         m_AccessoryMode.speech = corModeChange.speech;
         m_AccessoryMode.phone = corModeChange.phone;
         m_AccessoryMode.navigation = corModeChange.navigation;
      }
      break;
   default:
      {
      }
      break;
   }
}

//!static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vSetBluetoothMacAddr(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetBluetoothMacAddr(const t_String szMacAddress)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetBluetoothMacAddr entered"));
   m_szBlutoothMac = szMacAddress;
}

//!static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vSetOemIconData(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetVehicleBrandInfo(const trVehicleBrandInfo &rfoVehicleBrandInfo)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetVehicleBrandInfo entered"));
   m_rDiPOInfoRespParam.szOemIconPath = rfoVehicleBrandInfo.szOemIconPath;
   ETG_TRACE_USR4(("[PARAM]::vSetVehicleBrandInfo - Icon Path = %s", m_rDiPOInfoRespParam.szOemIconPath.c_str()));
   m_rDiPOInfoRespParam.szOemIcon = rfoVehicleBrandInfo.szOemName;
   ETG_TRACE_USR4(("[PARAM]::vSetVehicleBrandInfo - Icon Label = %s", m_rDiPOInfoRespParam.szOemIcon.c_str()));
   m_rDiPOInfoRespParam.szModelName = rfoVehicleBrandInfo.szModel;
   ETG_TRACE_USR4(("[PARAM]::vSetVehicleBrandInfo - Model Name = %s", m_rDiPOInfoRespParam.szModelName.c_str()));
   m_rDiPOInfoRespParam.szManufacturer = rfoVehicleBrandInfo.szManufacturer;
   ETG_TRACE_USR4(("[PARAM]::vSetVehicleBrandInfo - Manufacturer = %s", m_rDiPOInfoRespParam.szManufacturer.c_str()));
}

//!static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vSetDriveSideInfo(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetDriveSideInfo(const tenDriveSideInfo enDriveSideInfo)
{
	ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetDriveSideInfo entered"));
	m_rDiPOInfoRespParam.enDriveSideInfo = enDriveSideInfo;
	ETG_TRACE_USR4(("[PARAM]::vSetDriveSideInfo - Drive Side = %d", ETG_ENUM(DRIVE_SIDE_TYPE, enDriveSideInfo)));
}

/***************************************************************************
** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::vSetVideoConfigInfo()
***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetVideoConfigInfo(trVideoConfigData rVideoConfigData)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetVideoConfigInfo entered"));
   m_rDiPODisplayAttr.u32Height = rVideoConfigData.u32ProjScreen_Height;
   m_rDiPODisplayAttr.u32Width = rVideoConfigData.u32ProjScreen_Width;
   m_rDiPODisplayAttr.u32LayerId = rVideoConfigData.u32LayerId;
   m_rDiPODisplayAttr.u32SurfaceId = rVideoConfigData.u32SurfaceId;
   m_rDiPODisplayAttr.u32Width_Millimeter = rVideoConfigData.u32ProjScreen_Width_Mm;
   m_rDiPODisplayAttr.u32Height_Millimeter = rVideoConfigData.u32ProjScreen_Height_Mm;
   m_rDiPOTouchInputAttr.u32TouchLayerId = rVideoConfigData.u32TouchLayerId;
   m_rDiPOTouchInputAttr.u32TouchSurfaceId = rVideoConfigData.u32TouchSurfaceId;

}

//!static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bSessionStartTimerCb(..
 ***************************************************************************/
t_Bool spi_tclDiPOControlAdapterImpl::bSessionStartTimerCb(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::bSessionStartTimerCb entered"));
   SPI_INTENTIONALLY_UNUSED(pObject);
   SPI_INTENTIONALLY_UNUSED(pcoUserData);
   SPI_INTENTIONALLY_UNUSED(timerID);

   //!Cancel the start up timer if it still running. 
   // Note  that the cancel  will destroy the timer from the context.
   Timer* poTimer = Timer::getInstance();
   if ((NULL != poTimer) && (0 != srTimerID ))
   {
      poTimer->CancelTimer(srTimerID );
      srTimerID  =0;
   }//if ((NULL != poTimer) && (0 != srTimerID))

   IPCMessageQueue oMessageQueue(DIPO_SPI_MSGQ_IPC_THREAD);
   trDiPOSessionMsg *poBuffer = (trDiPOSessionMsg *)oMessageQueue.vpCreateBuffer(sizeof(trDiPOSessionMsg));
   if(NULL != poBuffer)
   {
      poBuffer->enMsgType = e8SESSION_MESSAGE;
      poBuffer->enDiPOSessionState = e8DIPO_SESSION_START;

      t_Bool bStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*)poBuffer, DIPO_MESSAGE_PRIORITY,(eMessageType) TCL_DATA_MESSAGE));
      ETG_TRACE_USR2(("[DESC]: Session start message sent to SPI with status = %d", ETG_ENUM(STATUS, bStatus)));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poBuffer);
   }
   return true; // Lint fix.
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vSetNightModeInfo(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetNightModeInfo(const t_Bool bIsNightMode)
{
	ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetNightModeInfo entered"));
	m_bCurNightModeInfo = bIsNightMode;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vSetDriveModeInfo(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetDriveModeInfo(const t_Bool bIsDriveMode)
{
	ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetDriveModeInfo entered"));
	m_bCurDriveModeInfo = bIsDriveMode;
}

//static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vSetDriveRestrictionInfo(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetDriveRestrictionInfo(const t_U8 u8DriveRest)
{
	ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetDriveRestrictionInfo entered"));
	m_rDiPOInfoRespParam.u8DriveRestrictionInfo = u8DriveRest;
	ETG_TRACE_USR4(("[PARAM]::vSetDriveRestrictionInfo - Driver Restriction Bitmap = %d", m_rDiPOInfoRespParam.u8DriveRestrictionInfo));
}

//static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vInitializeAudioTypeMap
 ***************************************************************************/
std::map<t_String, tenDiPOMainAudioType> spi_tclDiPOControlAdapterImpl::vInitializeAudioTypeMap()
{
	ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vInitializeAudioTypeMap entered"));
	std::map<t_String, tenDiPOMainAudioType> mTempAudioMap;
	mTempAudioMap["media"] = e8AUDIO_MEDIA;
	mTempAudioMap["speechRecognition"] = e8AUDIO_SPEECHREC;
	mTempAudioMap["spokenAudio"] = e8AUDIO_SPOKEN;
	mTempAudioMap["telephony"] = e8AUDIO_TELEPHONY;
	mTempAudioMap["alert"] = e8AUDIO_ALERT;
	mTempAudioMap["default"] = e8AUDIO_DEFAULT;
	return mTempAudioMap;
}

//static
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vSetCPlayAutoLaunchFlag(..
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vSetCPlayAutoLaunchFlag(tenAutoLaucnhFlag enAutoLaunchFlag)
{
	ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vSetCPlayAutoLaunchFlag entered"));
	m_enCPlayAutoLaunchFlag = enAutoLaunchFlag;
}

/* Private methods*/

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vGetAudioType()
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vGetAudioType(const t_String szAudioType, 
                                                    tenDiPOMainAudioType &enAudioType)
 {
	ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vGetAudioType entered"));
    //Mapping the Apple specific audio types to the SPI internal types
	std::map<t_String, tenDiPOMainAudioType>::iterator it = m_mAudioTypeMap.find(szAudioType.c_str());
	if(it != m_mAudioTypeMap.end())
	{
		enAudioType = it->second;
	}
	ETG_TRACE_USR2(("[DESC]: vGetAudioType returned with audio type = %d", ETG_ENUM(DIPO_AUDIO_TYPE, enAudioType)));
 }

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vConvertModeState()
 ***************************************************************************/
t_Void spi_tclDiPOControlAdapterImpl::vConvertModeState(const trModeState& rfcorModestate,
                                                    trDiPOModeState &rfoDiPOModestate)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::vConvertModeState entered"));
   rfoDiPOModestate.enScreen = (tenDiPOEntity)rfcorModestate.screen;
   rfoDiPOModestate.enAudio = (tenDiPOEntity)rfcorModestate.audio;
   rfoDiPOModestate.rSpeechState.enEntity = (tenDiPOEntity)rfcorModestate.speech.entity;
   rfoDiPOModestate.rSpeechState.enSpeechMode = (tenDiPOSpeechMode)rfcorModestate.speech.mode;
   rfoDiPOModestate.enPhone = (tenDiPOEntity)rfcorModestate.phone;
   rfoDiPOModestate.enNavigation = (tenDiPOEntity)rfcorModestate.navigation;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::bSendIPCMessage()
 ***************************************************************************/
template<typename trMessage>
t_Bool spi_tclDiPOControlAdapterImpl::bSendIPCMessage(trMessage rMessage)
{
   ETG_TRACE_USR1(("spi_tclDiPOControlAdapterImpl::bSendIPCMessage entered"));
   t_Bool bRetVal = false;
   IPCMessageQueue oMessageQueue(DIPO_SPI_MSGQ_IPC_THREAD);

   trMessage *poBuffer = (trMessage *)oMessageQueue.vpCreateBuffer(sizeof(trMessage));
   if(NULL != poBuffer)
   {
      memset(poBuffer, 0, sizeof(trMessage));
      memcpy(poBuffer, &rMessage, sizeof(trMessage));
      bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poBuffer, 
         DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poBuffer);
   }
   return bRetVal;
}

//lint –restore