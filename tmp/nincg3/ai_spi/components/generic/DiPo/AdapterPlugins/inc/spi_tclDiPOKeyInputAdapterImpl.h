/*!
*******************************************************************************
* \file              spi_tclDiPOKeyInputAdapterImpl.h
* \brief             DiPO Key Input Adapter implementation
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO key input adapter implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
03.04.2014 |  Hari Priya E R              | Initial Version
17.07.2015 |  Sameer Chandra              | Added Knob Key Implementation.

\endverbatim
******************************************************************************/

#ifndef SPI_TCLDIPOKEYINPUTADAPTERIMPL_H
#define SPI_TCLDIPOKEYINPUTADAPTERIMPL_H

#include "DiPOTypes.h"

static const t_U8 KNOB_DATA_SIZE = 2;

/****************************************************************************/
/*!
 * \class spi_tclDiPOKeyInputAdapterImpl
 * \brief DiPO Input adapter implementation
 *
 * spi_tclDiPOKeyInputAdapterImpl is a realization of IInputAdapter
 ****************************************************************************/
class spi_tclDiPOKeyInputAdapterImpl: public IInputAdapter
{
   public:
      /***************************************************************************
       *********************************PUBLIC************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPOKeyInputAdapterImpl::spi_tclDiPOKeyInputAdapterImpl()
       ***************************************************************************/
      /*!
       * \fn     spi_tclDiPOKeyInputAdapterImpl()
       * \brief  Constructor
       * \sa     ~spi_tclDiPOKeyInputAdapterImpl()
       **************************************************************************/
      spi_tclDiPOKeyInputAdapterImpl();

      /***************************************************************************
       ** FUNCTION:  spi_tclDiPOKeyInputAdapterImpl::~spi_tclDiPOKeyInputAdapterImpl()
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclDiPOKeyInputAdapterImpl()
       * \brief  Desstructor
       * \sa     spi_tclDiPOKeyInputAdapterImpl()
       **************************************************************************/
      virtual ~spi_tclDiPOKeyInputAdapterImpl();

      /***************************************************************************
       ** FUNCTION:  virtual t_Bool spi_tclDiPOKeyInputAdapterImpl::bInitialize()...
       ***************************************************************************/
      /*!
       * \fn     bInitialize(const IConfiguration& rfConfig, IInputReceiver& rfReceiver)
       * \brief  Control adapter initialization. This is called when a dipo session
       *         is initialized.
       * \param  rfConfig : [IN] IConfiguration handler
       * \param  rfReceiver : [IN] IControlReceiver handler
       * \retVal bool : true if initializatio success, false otherwise
       * \sa
       **************************************************************************/
      virtual t_Bool Initialize(const IConfiguration& rfConfig,
               IInputReceiver& rfReceiver);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclDiPOKeyInputAdapterImpl::bSendKeyData()
       ***************************************************************************/
      /*!
       * \fn     bSendKeyData()
       * \brief  function to send the key input data
       * \param  enKeyMode : [IN] Key Mode-Pressed or Released
       * \param  enKeyCode : [IN] Key Code-Unique identifier of the Key
       * \retVal bool : true if success , false otherwise
       * \sa
       **************************************************************************/
      t_Bool bSendKeyEvent(tenKeyMode enKeyMode, tenKeyCode enKeyCode);
	  
      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclDiPOKeyInputAdapterImpl::bSendKeyData()
       ***************************************************************************/
      /*!
       * \fn     bSendKnobKeyData()
       * \brief  function to send the key input data
       * \param  u8EncoderDeltaCounts : [IN] Change in the encoder count
       * \retVal bool : true if success , false otherwise
       * \sa
       **************************************************************************/
      t_Bool bSendKnobKeyEvent(t_S8 u8EncoderDeltaCounts =0);

      /***************************************************************************
       ** FUNCTION:  static spi_tclDiPOKeyInputAdapterImpl*
       **            poGetDiPOKeyInputAdapterInstance();
       ***************************************************************************/
      /*!
       * \fn     poGetDiPOKeyInputAdapterInstance()
       * \brief  Method to get a pointer to spi_tclDiPOKeyInputAdapterImpl class
       * \param  None
       * \param  None
       * \sa
       **************************************************************************/
      static spi_tclDiPOKeyInputAdapterImpl* poGetDiPOKeyInputAdapterInstance();

      /***************************************************************************
      ** FUNCTION:  virtual t_Void spi_tclDiPOKeyInputAdapterImpl::vSetDisplayInputParam()
      ***************************************************************************/
      /*!
      * \fn     vSetDisplayInputParam(t_U8 u8DisplayInput)
      * \brief  Set the drive restriction info
      * \param  u8DisplayInput : [IN] Display input param
      * \retVal Void
      * \sa
      **************************************************************************/
      static t_Void vSetDisplayInputParam(t_U8 u8DisplayInput);

      /***************************************************************************
       ************************END OF PUBLIC *************************************
       ***************************************************************************/

protected:
   /***************************************************************************
   *********************************PROTECTED************************************
   ***************************************************************************/

   /***************************************************************************
   *********************************END OF PROTECTED**************************
   ***************************************************************************/

private:

      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/
      /*!
       * \IInputReceiver* m_poInputReceiver
       * \brief IInputReceiver handler
       */
      IInputReceiver* m_poIInputReceiver;

      /*!
       * \static spi_tclDiPOKeyInputAdapterImpl* m_poDiPOKeyInputAdapter;
       * \brief Pointer to spi_tclDiPOKeyInputAdapterImpl class type
       */
      static spi_tclDiPOKeyInputAdapterImpl* m_poDiPOKeyInputAdapter;

      /*! 
      * \trHIDDeviceInfo m_rHIDConsumerKeyDeviceInfo
      * \brief HID Consumer Key device Info
      */
      trHIDDeviceInfo m_rHIDConsumerKeyDeviceInfo;

      /*! 
      * \trHIDDeviceInfo m_rHIDTelKeyDeviceInfo
      * \brief HID Telephony Key device Info
      */
      trHIDDeviceInfo m_rHIDTelKeyDeviceInfo;

      /*!
      * \trHIDDeviceInfo m_rHIDKnobKeyDeviceInfo
      * \brief HID Knob Key device Info
      */
      trHIDDeviceInfo m_rHIDKnobKeyDeviceInfo;

      //!Consumer Key data
      t_U8 m_u8ConsumerKeydata;

      //!Telephony Key data
      t_U8 m_u8TelephoneKeydata;

      //!Knob Key data
      t_S8 m_u8KnobKeydata[KNOB_DATA_SIZE];

      //!Static Flag to check if Knob keys are supported.
      static t_Bool m_bKnobKeySupported;

   /***************************************************************************
       ** FUNCTION:  spi_tclDiPOKeyInputAdapterImpl::spi_tclDiPOKeyInputAdapterImpl
       ***************************************************************************/
      /*!
       * \fn      spi_tclDiPOKeyInputAdapterImpl(const spi_tclDiPOKeyInputAdapterImpl &corfrSrc)
       * \brief   Copy constructor, will not be implemented.
       * \note    This is a technique to disable the Copy constructor for this class.
       * \param   corfrSrc : [IN] Source Object
       **************************************************************************/
      spi_tclDiPOKeyInputAdapterImpl(
               const spi_tclDiPOKeyInputAdapterImpl& corfrSrc);

      /***************************************************************************
       ** FUNCTION:spi_tclDiPOKeyInputAdapterImpl& spi_tclDiPOKeyInputAdapterImpl::operator= .
       ***************************************************************************/
      /*!
       * \fn      spi_tclDiPOKeyInputAdapterImpl& operator= (const
       spi_tclDiPOKeyInputAdapterImpl &orfrSrc)
       * \brief   Assignment Operator, will not be implemented.
       * \note    This is a technique to disable the assignment operator for this class.
       *          So if an attempt for the assignment is made linker complains.
       * \param   corfrSrc : [IN] Source Object
       **************************************************************************/
      spi_tclDiPOKeyInputAdapterImpl& operator =(
               const spi_tclDiPOKeyInputAdapterImpl& corfrSrc);

      /***************************************************************************
       *********************************END OF PRIVATE*****************************
       ***************************************************************************/

};

#endif 
