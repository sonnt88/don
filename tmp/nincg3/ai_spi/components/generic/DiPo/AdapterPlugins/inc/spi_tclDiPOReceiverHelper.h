/*!
*******************************************************************************
* \file              spi_tclDiPORecieverHelper.h
* \brief             DiPO IControlReceiver helper
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO helper class. This is a helper class to invoke the 
                IControlReciver functions from SPI. This class forward the 
                request to the IControlReceiver implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
21.1.2014  |  Shihabudheen P M            | Initial Version
30.04.2014 |  Hari Priya E R              | Included function for Siri Action
12.06.2014 |  Shihabudheen P M            | Included function for Blutooth ID

\endverbatim
******************************************************************************/

#ifndef SPI_TCLDIPORECIEVERWRAPPER_H
#define SPI_TCLDIPORECIEVERWRAPPER_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <list>
#include "BaseTypes.h"
#include "DiPOTypes.h"
#include "GenericSingleton.h"

using namespace std;

/****************************************************************************/
/*!
* \class spi_tclDiPOConfigHelper
* \brief DiPO Config helper class implementation
*
* spi_tclDiPOConfigHelper is a singletone class which holds the configuration
* handler and act as an interface to perform the Dynamic configurations.
****************************************************************************/
class spi_tclDiPOReceiverHelper: public GenericSingleton<spi_tclDiPOReceiverHelper> 
{
public:
  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPORecieverHelper::~spi_tclDiPORecieverHelper()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPORecieverHelper()
   * \brief  Destructor 
   * \sa     spi_tclDiPORecieverHelper()
   **************************************************************************/
	virtual ~spi_tclDiPOReceiverHelper();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPORecieverHelper::bRequestDeviceUI(t_String szAppUrl)
   ***************************************************************************/
   /*!
   * \fn     bRequestDeviceUI(const string)
   * \brief  Request to render projected device UI
   * \param  szAppUrl : [IN] string indicating the application to launch
   * \sa     
   **************************************************************************/
	t_Void vRequestDeviceUI(t_String szAppUrl);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPORecieverHelper::vRequestResourceModeChange
   ***************************************************************************/
   /*!
   * \fn     vRequestResourceModeChange(const trModeChange rModeChange)
   * \brief  Request to change the resource mode
   * \param  rModeChange : [IN] Mode change request data. 
   * \sa     
   **************************************************************************/
   t_Void vRequestResourceModeChange(const trModeChange rModeChange);

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPORecieverHelper::vSetControlReceiverHandler()
   ***************************************************************************/
   /*!
   * \fn     vSetControlReceiverHandler(IControlReceiver *poReceiver)
   * \brief  function to set the IControlReciever handler
   * \param  poReceiver : [IN] IControl reciever handler
   * \sa     
   **************************************************************************/
   t_Void vSetControlReceiverHandler(IControlReceiver *poReceiver);

     /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPORecieverHelper::vRequestSiriAction()
   ***************************************************************************/
   /*!
   * \fn     vREquestSiriAction(tenSiriAction enSiriAction)
   * \brief  function to request the SiriAction
   * \param  enSiriAction : [IN] Siri Action
   * \sa     
   **************************************************************************/
   t_Void vRequestSiriAction(SiriAction enSiriAction);

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPORecieverHelper::poGetConrolReceiverHandler()
   ***************************************************************************/
   /*!
   * \fn     poGetConrolReceiverHandler(IControlReceiver *poReceiver)
   * \brief  function to get the IControlReciever handler
   * \retVal  IControlReceiver* :IControl reciever handler
   * \sa     
   **************************************************************************/
   IControlReceiver* poGetControlReceiverHandler();

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPORecieverHelper::vSetBlutoothID()
   ***************************************************************************/
   /*!
   * \fn      vSetBlutoothID(IControlReceiver *poReceiver)
   * \brief   rfDeviceIDList : [IN] List of the Blutooth MAC adress
   * \retVal  IControlReceiver* :IControl reciever handler
   * \sa     
   **************************************************************************/
   t_Void vSetBlutoothID(const std::list<std::string>& rfDeviceIDList) const;

   /*! 
   * \friend class GenericSingleton<spi_tclDiPOReceiverHelper>
   * \brief friend class declaration
   */
   friend class GenericSingleton<spi_tclDiPOReceiverHelper>;

  /***************************************************************************
   *********************************END OF PUBLIC*****************************
   ***************************************************************************/

protected:

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPORecieverHelper::spi_tclDiPORecieverHelper()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPORecieverHelper()
   * \brief  Constructor 
   * \sa     ~spi_tclDiPORecieverHelper()
   **************************************************************************/
   spi_tclDiPOReceiverHelper();

   /*! 
   * \IControlReceiver* m_poReciever
   * \brief IControlReceiver handler
   */
   IControlReceiver* m_poReciever;
};

#endif