/*!
*******************************************************************************
* \file              spi_tclDiPOConfigHelper.h
* \brief             DiPO Config helper class.
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO Config helper class to perform configurations
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
21.1.2014 |  Shihabudheen P M            | Initial Version

\endverbatim
******************************************************************************/
#ifndef SPI_TCLDIPOCONFIGHELPER_H
#define SPI_TCLDIPOCONFIGHELPER_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "GenericSingleton.h"
#include "DiPOTypes.h"

/******************************************************************************
| Defines:
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclDiPOConfigHelper
* \brief DiPO Config helper class implementation
*
* spi_tclDiPOConfigHelper is a singletone class which holds the configuration
* handler and act as an interface to perform the Dynamic configurations.
****************************************************************************/
class spi_tclDiPOConfigHelper : public GenericSingleton<spi_tclDiPOConfigHelper>
{
public:

   /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/
  
   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOConfigHelper::~spi_tclDiPOConfigHelper()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOConfigHelper()
   * \brief  Destructor
   * \sa     spi_tclDiPOConfigHelper()
   **************************************************************************/
   virtual ~spi_tclDiPOConfigHelper();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOConfigHelper::vSetConfigurationHandler(..)
   ***************************************************************************/
   /*!
   * \fn      vSetConfigurationHandler(IDynamicConfiguration *poConfig)
   * \brief   Function to set the IConfiguration handle
   * \param   poConfig: [IN] IConfiguration handle
   * \sa
   **************************************************************************/
   t_Void vSetConfigurationHandler(IDynamicConfiguration *poConfig);

   /***************************************************************************
   ** FUNCTION:  IConfiguration* spi_tclDiPOConfigHelper::poGetConfigurationHandler()
   ***************************************************************************/
   /*!
   * \fn      poGetConfigurationHandler()
   * \brief   Function to get the IConfiguration handle
   * \retVal  IConfiguration *: [OUT] IDynamicConfiguration handle
   * \sa
   **************************************************************************/
   IDynamicConfiguration* poGetConfigurationHandler();

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOConfigHelper::vSetVideoConfiguration()
   ***************************************************************************/
   /*!
   * \fn     vSetVideoConfiguration() const
   * \brief  Function to set the video configuration to dipo.cfg
   * \retVal bool : true if success, false otherwise  
   * \sa
   **************************************************************************/
   t_Bool vSetVideoConfiguration();

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOConfigHelper::vSetAudioConfiguration()
   ***************************************************************************/
   /*!
   * \fn     vSetAudioConfiguration() const
   * \brief  Function to set the audio configuration to dipo.cfg
   * \param  szAudioDevice : [IN] Audio device name
   * \retVal bool : true if success, false otherwise
   * \sa
   **************************************************************************/
   t_Bool vSetAudioOutConfig(t_String szAudioDevice, tenAudioStreamType enStreamType);


  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOConfigHelper::vSetAudioInConfig()
   ***************************************************************************/
   /*!
   * \fn     vSetAudioInConfig() const
   * \brief  Function to set the audio configuration to dipo.cfg
   * \param  szAudioOutDev : [IN] Audio Out device name
   * \param  szAudioInDev : [IN] Audio In device name
   * \retVal bool : true if success, false otherwise
   * \sa
   **************************************************************************/
   t_Void vSetAudioInConfig(t_String szAudioOutDev, t_String szAudioInDev);


   //! Fried class declarartion to use singleton utility.
   friend class GenericSingleton<spi_tclDiPOConfigHelper>;

   /*************************************************************************
   ****************************END OF PUBLIC*********************************
   *************************************************************************/

private:

   /*************************************************************************
   ****************************START OF PRIVATE******************************
   *************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOConfigHelper::spi_tclDiPOConfigHelper()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOConfigHelper()
   * \brief  Default constructor
   * \sa     ~spi_tclDiPOConfigHelper()
   **************************************************************************/
   spi_tclDiPOConfigHelper();

   //! IConfiguration handle
   IDynamicConfiguration *m_poConfig;
   
   //! Video config data
   trVideoConfigData m_rVideoConfigData; 

   /*************************************************************************
   ****************************END OF PRIVATE*********************************
   *************************************************************************/
};

#endif
