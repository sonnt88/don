/*!
*******************************************************************************
* \file              spi_tclDiPOControlAdapterImpl.h
* \brief             DiPO IControlAdapter impmentation.
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO IControlAdapter implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
21.1.2014 |  Shihabudheen P M            | Initial Version
07.05.2014|  Shihabudheen P M            | Modeified for resource arbitration 
20.08.2015 |  Shihabudheen P M           | Added methods to dynamically set the limited UI elements.
08.01.2016 | Shihabudheen P M            | Improvements.

\endverbatim
******************************************************************************/
#ifndef SPI_TCL_DIPOCONTROLADAPTERIMPL_H_
#define SPI_TCL_DIPOCONTROLADAPTERIMPL_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <list>
#include <map>
#include "DiPOTypes.h"

class IPCMessageQueue;

struct trDiPODisplayAttributes
{
	t_U32 u32Height;
	t_U32 u32Width;
	t_U32 u32LayerId;
	t_U32 u32SurfaceId;
	t_U32 u32Height_Millimeter;
	t_U32 u32Width_Millimeter;

	trDiPODisplayAttributes()
	{
		u32Height = 0;
		u32Width = 0;
		u32LayerId = 0;
		u32SurfaceId = 0;
		u32Height_Millimeter = 0;
		u32Width_Millimeter = 0;
	}
};

struct trDiPOTouchInputAttributes
{
    t_U32 u32TouchLayerId;
    t_U32 u32TouchSurfaceId;

    trDiPOTouchInputAttributes()
    {
    	u32TouchLayerId = 0;
    	u32TouchSurfaceId = 0;
    }
};


struct trDiPOInfoRespParam
{
	t_U8 u8DriveRestrictionInfo;
	tenDriveSideInfo enDriveSideInfo;
	t_String szOemIcon;
	t_String szOemIconPath;
	t_String szModelName;
	t_String szManufacturer;

	trDiPOInfoRespParam()
	{
		u8DriveRestrictionInfo = 0;
		enDriveSideInfo = e8LEFT_HAND_DRIVE;
		szOemIcon = "";
		szOemIconPath = "";
		szModelName = "";
		szManufacturer = "";
	}
};

/****************************************************************************/
/*!
* \class spi_tclDiPOControlAdapterImpl
* \brief DiPO IControlAdapter realization 
*
* spi_tclDiPOControlAdapterImpl is the realization of IControlAdapter interface.
* 
****************************************************************************/
class spi_tclDiPOControlAdapterImpl: public IControlAdapter
{
public:

  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOControlAdapterImpl::spi_tclDiPOControlAdapterImpl()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOControlAdapterImpl()
   * \brief  Constructor
   * \sa     ~spi_tclDiPOControlAdapterImpl()
   **************************************************************************/
   spi_tclDiPOControlAdapterImpl();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOControlAdapterImpl::~spi_tclDiPOControlAdapterImpl()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOControlAdapterImpl()
   * \brief  Destructor
   * \sa     spi_tclDiPOControlAdapterImpl()
   **************************************************************************/
   virtual ~spi_tclDiPOControlAdapterImpl();
  
  /***************************************************************************
   ** FUNCTION:  virtual bool spi_tclDiPOControlAdapterImpl::Initialize()...
   ***************************************************************************/
   /*!
   * \fn     Initialize(IDynamicConfiguration& config, IControlReceiver& control)
   * \brief  Control adapter initialization. This is called when a dipo session 
   *         is initialized. 
   * \param  rfConfig : [IN] IConfiguration handler
   * \param  rfReceiver : [IN] IControlReceiver handler
   * \retVal bool : true if initializatio success, false otherwise
   * \sa     
   **************************************************************************/
   virtual t_Bool Initialize(IDynamicConfiguration& rfConfig, IControlReceiver& rfReceiver);

  /***************************************************************************
   ** FUNCTION:  virtual bool spi_tclDiPOControlAdapterImpl::OnSessionStart()...
   ***************************************************************************/
   /*!
   * \fn     OnSessionStart()
   * \brief  Method which gets called after the session initailization process.
   *         This gets called after the "RECORD" message called from Apple.
   * \retVal NONE
   * \sa     
   **************************************************************************/
   virtual t_Void OnSessionStart();

   /***************************************************************************
   ** FUNCTION:  virtual bool spi_tclDiPOControlAdapterImpl::OnSessionEnd()...
   ***************************************************************************/
   /*!
   * \fn     OnSessionEnd()
   * \brief  Method which gets called on the session terminated.
   * \retVal NONE
   * \sa     
   **************************************************************************/
   virtual t_Void OnSessionEnd();
   
  /***************************************************************************
   ** FUNCTION:  virtual void spi_tclDiPOControlAdapterImpl::OnModesChanged(trModeState rModeState)
   ***************************************************************************/
   /*!
   * \fn     OnModesChanged(trModeState rModeState)
   * \brief  Mode changed request handling implementation. This function get called 
   *         when device sends a mode changed confirmation to the accessory 
   * \param  rModeState : [IN] current mode of the DiPo resources and App.
   * \sa     
   **************************************************************************/
   virtual t_Void OnModesChanged(trModeState rModeState);

  /***************************************************************************
   ** FUNCTION:  virtual void spi_tclDiPOControlAdapterImpl::OnRequestUI()
   ***************************************************************************/
   /*!
   * \fn     OnRequestUI()
   * \brief  RequestUI handler. This function get called when device sends
   *         a command to accessory to display the native HMI. 
   * \sa     
   **************************************************************************/
   virtual t_Void OnRequestUI();

  /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::OnRampVolume()
   ***************************************************************************/
   /*!
   * \fn     OnRampVolume(t_Double d64FinalVolume, t_Double d64Duration)
   * \brief  RampVolume handler. This function get called when device sends
   *         a command to accessory ramp down the accessory volume. 
   * \param  d64FinalVolume : [IN] Level to which volume has ti reduce.
   * \param  d64Duration : [IN] Time interval to keep in the same state.
   * \sa     
   **************************************************************************/
   virtual t_Void OnRampVolume(t_Double d64FinalVolume, t_Double d64Duration);

  /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::OnDisableBluetooth()
   ***************************************************************************/
   /*!
   * \fn     OnDisableBluetooth(const t_String& szDeviceId)
   * \brief  DisableBluetooth handler. This function get called when device sends
   *         a command to accessory to disable the bluetooth. 
   * \param  szDeviceId : [IN] Device ID
   * \sa     
   **************************************************************************/
   virtual t_Void OnDisableBluetooth(const t_String& szDeviceId);

  /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::OnAudioPrepare()
   ***************************************************************************/
   /*!
   * \fn     OnAudioPrepare(AudioChannelType channel, const t_String& audioType)
   * \brief  Call to prepare for auddio rendering
   * \param  channel : [IN] Type of the audio channel to be allocated
   * \param  audioType : [IN] Type of the audio stream.
   * \sa     
   **************************************************************************/
   virtual t_Void OnAudioPrepare(AudioChannelType enChannel, const t_String& szAudioType);
   
   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::OnAudioStop()
   ***************************************************************************/
   /*!
   * \fn     OnAudioStop(AudioChannelType channel)
   * \brief  Call to stop the audio playback
   * \param  channel : [IN] Type of the audio channel to be allocated
   * \param  audioType : [IN] Type of the audio stream.
   * \sa     
   **************************************************************************/
   virtual t_Void OnAudioStop(AudioChannelType channel);
   
   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::OnGetBluetoothIDs()
   ***************************************************************************/
   /*!
   * \fn     OnGetBluetoothIDs(std::list<std::string>& deviceIDs)
   * \brief  Call to get the Bluetooth device ID
   * \param  channel : [IN] Type of the audio channel to be allocated
   * \param  audioType : [IN] Type of the audio stream.
   * \sa     
   **************************************************************************/
   virtual t_Void OnGetBluetoothIDs(std::list<std::string>& deviceIDs);
   
  /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::OnGetBluetoothIDs()
   ***************************************************************************/
   /*!
   * \fn     OnGetNightMode()
   * \brief  Call to get the night mode info from accessory
   * \param  channel : [IN] Type of the audio channel to be allocated
   * \param  audioType : [IN] Type of the audio stream.
   * \sa     
   **************************************************************************/
   virtual t_Bool OnGetNightMode();

   /***************************************************************************
    ** FUNCTION:  static spi_tclDiPOControlAdapterImpl* poGetDiPOControlAdapterInstance();
    ***************************************************************************/
    /*!
    * \fn     poGetDiPOControlAdapterInstance()
    * \brief  Method to get a pointer to spi_tclDiPOControlAdapterImpl class
    * \param  None
    * \param  None
    * \sa
    **************************************************************************/
   static spi_tclDiPOControlAdapterImpl* poGetDiPOControlAdapterInstance();

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bSetAudioOutConfig()
   ***************************************************************************/
   /*!
   * \fn     bSetAudioOutConfig() const
   * \brief  Function to set the audio configuration to dipo.cfg
   * \param  szAudioDevice : [IN] Audio Out device name
   * \param  enStreamType : [IN] Audio Stream Type
   * \retVal bool : true if success, false otherwise
   * \sa
   **************************************************************************/
   t_Bool bSetAudioOutConfig(const t_String szAudioDevice,  const tenAudioStreamType enStreamType,
		   const tenMsgTypes enMsgTypes);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bSetAudioInConfig()
   ***************************************************************************/
   /*!
   * \fn     bSetAudioInConfig() const
   * \brief  Function to set the audio configuration to dipo.cfg
   * \param  szAudioDevice : [IN] Audio Out device name
   * \param  szAudioInDev : [IN] Audio In Device Name
   * \retVal bool : true if success, false otherwise
   * \sa
   **************************************************************************/
   t_Bool bSetAudioInConfig(const t_String szAudioOutDev, const t_String szAudioInDev);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclDiPOControlAdapterImpl::bLaunchApp(t_String szAppUrl)
    ***************************************************************************/
    /*!
    * \fn     bLaunchApp(const string)
    * \brief  Request to render projected device UI
    * \param  szAppUrl : [IN] string indicating the application to launch
    * \sa
    **************************************************************************/
   t_Bool bLaunchApp(const t_String szAppUrl);

   /***************************************************************************
    ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bAccessoryModeChange
    ***************************************************************************/
    /*!
    * \fn     bAccessoryModeChange(const trModeChange& rfcorModeChange)
    * \brief  Request to change the resource mode
    * \param  rfcorModeChange : [IN] Mode change request data.
    * \param  enAccModeChangeType : [IN] ModeChange request type(audio, display, Appstate)
    * \sa
    **************************************************************************/
   t_Bool bAccessoryModeChange(const trModeChange& rfcorModeChange,
		   const tenAccModeChangeType enAccModeChangeType);

   /***************************************************************************
   ** FUNCTION: t_Bool t_Void spi_tclDiPOControlAdapterImpl::vRequestSiriAction()
   ***************************************************************************/
   /*!
   * \fn     vREquestSiriAction(tenSiriAction enSiriAction)
   * \brief  function to request the SiriAction
   * \param  enSiriAction : [IN] Siri Action
   * \sa
   **************************************************************************/
   t_Bool bSiriAction(const SiriAction enSiriAction);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::bSetNightMode()
   ***************************************************************************/
   /*!
   * \fn     bSetNightMode(t_Bool bIsNightMode)
   * \brief  function to set the night mode info
   * \param  bIsNightMode : [IN] True if night mode, false otherwise
   * \retVal None
   * \sa
   **************************************************************************/
   t_Void vSetNightMode(const t_Bool bIsNightMode);

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::vSetLimitedUI()
   ***************************************************************************/
   /*!
   * \fn     vSetLimitedUI(t_Bool bLimitedUIStatus)
   * \brief  function to set the status of LimitedUI(To limit someUI elements from display)
   * \param  bLimitedUIStatus : [IN] True for limiting UI elements, false otherwise
   * \sa
   **************************************************************************/
   t_Void vSetLimitedUI(const t_Bool bLimitedUIStatus);
   
  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::vSetBluetoothIds()
   ***************************************************************************/
   /*!
   * \fn     vSetBluetoothIds(t_Bool bLimitedUIStatus)
   * \brief  function to set the BluetoothIds
   * \
   * \retVal : bool, true if succes, false otherwise.
   **************************************************************************/
   t_Void vSetBluetoothIds();

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::OnGetCurrentResourceMode()
   ***************************************************************************/
   /*!
   * \fn     OnGetCurrentResourceMode()
   * \brief  Call to set the current mode of the accessory upon request from iOS.
   * \       The same interface is used to transfer the accessory mode as a part 
   * \       of info message.
   * \param  rfoModeChange : [OUT] Current mode of the accessory.
   * \sa     
   **************************************************************************/
   virtual t_Void OnGetCurrentResourceMode(trModeChange& rfoModeChange); 

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vSetCurrentAccessoryMode()
   ***************************************************************************/
   /*!
   * \fn     vSetCurrentModeChangeRequest()
   * \brief  Function to set the latset mode change request which reflects 
   * \param  corModeChange : [IN] Current mode change request.
   * \param  enAccModeChangeType : [IN] Mode change type to identify the change is related to
   *         which resource(display, audio or appstate).
   * \sa     
   **************************************************************************/
   static t_Void vSetCurrentAccessoryMode(const trModeChange &corModeChange, 
      const tenAccModeChangeType enAccModeChangeType);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vSetBluetoothMacAddr()
   ***************************************************************************/
   /*!
   * \fn     vSetBluetoothMacAddr(const t_String szMacAddress)
   * \brief  Function to set the Bluetooth MAC address.
   * \param  szMacAddress : [IN] Bluetooth MAc address
   * \retVal None
   * \sa     
   **************************************************************************/
   static t_Void vSetBluetoothMacAddr(const t_String szMacAddress);
   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vSetBluetoothMacAddr()
   ***************************************************************************/
   /*!
   * \fn     vSetVehicleBrandInfo(const trVehicleBrandInfo &rfoVehicleBrandInfo)
   * \brief  Function to set Oem Icon Data.
   * \param  rfoVehicleBrandInfo : [IN] Vehicle brand info
   * \retVal None
   * \sa     
   **************************************************************************/
   static t_Void vSetVehicleBrandInfo(const trVehicleBrandInfo &rfoVehicleBrandInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vSetDriveSideInfo()
   ***************************************************************************/
   /*!
   * \fn     vSetDriveSideInfo(t_Bool bIsNightMode, t_Bool bIsDriveMode)
   * \brief  Function to update the drive side info
   * \param  enDriveSideInfo : Drive side
   * \retVal None
   * \sa
   **************************************************************************/
   static t_Void vSetDriveSideInfo(const tenDriveSideInfo enDriveSideInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vSetNightModeInfo()
   ***************************************************************************/
   /*!
   * \fn     vSetNightModeInfo(t_Bool bIsNightMode, t_Bool bIsDriveMode)
   * \brief  Function to update vehicle info.
   * \param  bIsNightMode : [IN] True if night mode, False otherwise
   * \retVal None
   * \sa
   **************************************************************************/
   static t_Void vSetNightModeInfo(const t_Bool bIsNightMode);

   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::vSetDriveModeInfo()
   ***************************************************************************/
   /*!
   * \fn     vSetDriveModeInfo( t_Bool bIsDriveMode)
   * \brief  Function to update vehicle info.
   * \param  bIsDriveMode : [IN] True if drive mode, False otherwise
   * \retVal None
   * \sa
   **************************************************************************/
   static t_Void vSetDriveModeInfo(const t_Bool bIsDriveMode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::bSessionStartTimerCb()
   ***************************************************************************/
   /*!
   * \fn     bSessionStartTimerCb( t_Bool bIsDriveMode)
   * \brief  Timer callback function
   * \param  timerID : [IN] Timer Id.
   * \param  pObject : [IN] Object pointer.
   * \param  pcoUserData : [IN] User data pointer.
   * \retVal Bool
   * \sa
   **************************************************************************/
   static t_Bool bSessionStartTimerCb(timer_t timerID , tVoid *pObject, 
                         const tVoid *pcoUserData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vSetVideoConfigInfo()
   ***************************************************************************/
   /*!
   * \fn     vSetVideoConfigInfo(trVideoConfigData rDiPOVideoConfigMsg)
   * \brief  Set the video config info
   * \param  rDiPOVideoConfigMsg : [IN] Timer Id.
   * \retVal Void
   * \sa
   **************************************************************************/
   static t_Void vSetVideoConfigInfo(trVideoConfigData rDiPOVideoConfigMsg);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPOControlAdapterImpl::vSetDriveRestrictionInfo()
   ***************************************************************************/
   /*!
   * \fn     vSetDriveRestrictionInfo(trVideoConfigData rDiPOVideoConfigMsg)
   * \brief  Set the drive restriction info
   * \param  u8DriveRest : [IN] Drive restriction bitmask
   * \retVal Void
   * \sa
   **************************************************************************/
   static t_Void vSetDriveRestrictionInfo(t_U8 u8DriveRest);


   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOControlAdapterImpl::vInitializeAudioTypeMap()
   ***************************************************************************/
   /*!
   * \fn     vInitializeAudioTypeMap()
   * \brief  Map the CarPlay specific audio type to SPI specific audio type.
   * \param  None
   * \retVal Void
   * \sa
   **************************************************************************/
   static std::map<t_String, tenDiPOMainAudioType> vInitializeAudioTypeMap();

   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclDiPOControlAdapterImpl::vSetCPlayAutoLaunchFlag()
   ***************************************************************************/
   /*!
   * \fn     vSetCPlayAutoLaunchFlag(t_Bool bAutoLaunchFlag)
   * \brief  Set the drive restriction info
   * \param  bAutoLaunchFlag : [IN] Auto launch flag.
   * \retVal Void
   * \sa
   **************************************************************************/
   static t_Void vSetCPlayAutoLaunchFlag(tenAutoLaucnhFlag enAutoLaunchFlag);


  /*************************************************************************
   ****************************END OF PUBLIC*********************************
   *************************************************************************/

private:
   
   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::vSetVideoConfiguration();
   ***************************************************************************/
   /*!
   * \fn     vSetVideoConfiguration()
   * \brief  Function to set the video configuration to dipo configuration list
   * \retVal None
   * \sa     
   **************************************************************************/
   t_Void vSetVideoConfiguration();

  /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vGetAudioType()
   ***************************************************************************/
   /*!
   * \fn     vGetAudioType(trMsgQBase &rfoMsgQBase)
   * \brief  Convert the audio stream type to enum valuse to use in SPI
   * \param  azAudioType : [IN] Audio type in string
   * \param  enAudioType : [OUT] Audio type
   * \sa     
   **************************************************************************/
   t_Void vGetAudioType(const t_String azAudioType, tenDiPOMainAudioType &enAudioType);
   
  /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::vConvertModeState()
   ***************************************************************************/
   /*!
   * \fn     vConvertModeState(const trModeState& rfcorModestate, trDiPOModeState &rfoDiPOModestate)
   * \brief  Convert the Apple specific mode state to SPI mode state
   * \param  rfcorModestate : [IN]Apple specific mode state
   * \param  rfoDiPOModestate : [OUT] SPI specific mode state
   * \sa     
   **************************************************************************/
   t_Void vConvertModeState(const trModeState& rfcorModestate, trDiPOModeState &rfoDiPOModestate);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclDiPOControlAdapterImpl::vConfigLimitedUIElements()
    ***************************************************************************/
    /*!
    * \fn     vConfigLimitedUIElements()
    * \brief  Update the drive restriction configuration.
    * \retVal None.
    * \sa
    **************************************************************************/
   t_Void vConfigLimitedUIElements();

  /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOControlAdapterImpl::bSendIPCMessage()
   ***************************************************************************/
   /*!
   * \fn     bSendIPCMessage(trMsgQBase &rfoMsgQBase)
   * \brief  Send the IPC message to SPI component.
   * \param  rMessage : [IN]Message data
   * \retVal  t_Bool : True if message send success, false otherwise
   * \sa     
   **************************************************************************/
   template<typename trMessage>
   t_Bool bSendIPCMessage(trMessage rMessage);

   //! IConfiguration handle
   IDynamicConfiguration *m_poIDynamicConfig;

   //! IControlReceiver handle
   IControlReceiver *m_poIControlReceiver;

   //! Current recieved audio message.
   // Values can be e8AUDIO_ERROR_MESSAGE, and e8AUDIO_MESSAGE
   tenMsgTypes m_CurrAudioMessage;

   //! Pointer to spi_tclDiPOControlAdapterImpl class instance
   static spi_tclDiPOControlAdapterImpl* m_poDiPOControlAdapter;

   //! Current Accessory Mode
   static trModeChange m_AccessoryMode;

   //! MAC address of the bluetooth protocol stack
   static t_String m_szBlutoothMac;

   // ! Session start status
   t_Bool m_bIsSessionStarted;

   //! Status of the request UI command.
   t_Bool m_bRequestUIStatus;
   
   trDiPOModeState m_rCurrentModeState;

   static t_Bool m_bCurNightModeInfo;
   
   static t_Bool m_bCurDriveModeInfo;

   static t_U8 m_u8LimitedUIElementCount;
   
   static trDiPODisplayAttributes m_rDiPODisplayAttr;

   static trDiPOTouchInputAttributes m_rDiPOTouchInputAttr;

   static trDiPOInfoRespParam m_rDiPOInfoRespParam;

   static std::map<t_String, tenDiPOMainAudioType> m_mAudioTypeMap;
   
   static tenAutoLaucnhFlag m_enCPlayAutoLaunchFlag;
};

#endif
