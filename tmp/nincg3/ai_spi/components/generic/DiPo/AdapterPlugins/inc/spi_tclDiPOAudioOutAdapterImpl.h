/*!
*******************************************************************************
* \file              spi_tclDiPOAudioOutAdapterImpl.h
* \brief             CarPlay audio out adapter extended implementation
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    CarPlay audio out adapter extended implementation
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
14.07.2014 |  Shihabudheen P M            | Initial Version

\endverbatim
******************************************************************************/
#ifndef SPI_TCLDIPOAUDIOOUTADAPTERIMPL_H
#define SPI_TCLDIPOAUDIOOUTADAPTERIMPL_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "DiPOTypes.h"
#include "AlsaAudioOut.h"

/****************************************************************************/
/*!
* \class spi_tclDiPOAudioOutAdapterImpl
* \brief CarPlay audio out adapter extended.
*
* spi_tclDiPOAudioOutAdapterImpl is the adapter implementation of IAudioOutAdapter.
* IAudioOutAdapter is implemented in GstreamerAudioOut, and the spi_tclDiPOAudioOutAdapterImpl
* is extending the implementation for adding additional funcionality.
* @ Note : GstreamerAudioOut is a part of libcarplay_gstreamer_so.co
****************************************************************************/
class spi_tclDiPOAudioOutAdapterImpl: public AlsaAudioOut
{
public:

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::spi_tclDiPOAudioOutAdapterImpl()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOAudioOutAdapterImpl()
   * \brief  Constructor
   * \sa     ~spi_tclDiPOAudioOutAdapterImpl()
   **************************************************************************/ 
   spi_tclDiPOAudioOutAdapterImpl();

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::spi_tclDiPOAudioOutAdapterImpl()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOAudioOutAdapterImpl()
   * \brief  Destructor
   * \sa     spi_tclDiPOAudioOutAdapterImpl()
   **************************************************************************/ 
   virtual ~spi_tclDiPOAudioOutAdapterImpl();

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::poGetInstance()
   ***************************************************************************/
   /*!
   * \fn     poGetInstance()
   * \brief  Return the handle of CarPlay audio out adapter implementation
   * \retVal spi_tclDiPOAudioOutAdapterImpl* : handle to the audio out adapter
   **************************************************************************/
   static spi_tclDiPOAudioOutAdapterImpl* poGetInstance();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::Prepare()
   ***************************************************************************/
   /*!
   * \fn     Prepare()
   * \brief  To prespare and set up the pipeline for audio playback
   * \param  rFormat: [IN] Audio format requested
   * \param  enChannel : [IN] Audio channel type 
   * \param  szAudioType : [IN] AudioType
   * \retVal Bool : true if success, false otherwise.
   **************************************************************************/
   t_Bool Prepare(AudioFormatStruct rFormat, AudioChannelType enChannel,
            const std::string& szAudioType);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::Stop()
   ***************************************************************************/
   /*!
   * \fn     Stop()
   * \brief  To stop the audio playback through the audio output channel
   * \retVal NONE
   **************************************************************************/
   t_Void Stop();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::vStopAudioPlayback()
   ***************************************************************************/
   /*!
   * \fn     vStopAudioPlayback()
   * \brief  Function to stop the audio playback and destroy the audio pipeline
   * \retVal NONE
   **************************************************************************/
   t_Void vStopAudioPlayback();

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::vGetCurrAudioFormat()
   ***************************************************************************/
   /*!
   * \fn     vGetCurrAudioFormat()
   * \brief  Function which reqtuen the audio format of the current allocated channel.
   * \retVal NONE
   **************************************************************************/
   t_Void vGetCurrAudioFormat(trDiPOAudioFormat &rfoAudioFormat);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::enGetCurrAudioType()
   ***************************************************************************/
   /*!
   * \fn     enGetCurrAudioType()
   * \brief  Function to get the audio Type of the current allocated channel.
   * \retVal tenDiPOMainAudioType
   **************************************************************************/
   tenDiPOMainAudioType enGetCurrAudioType();

private:

   //!Static
   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOAudioOutAdapterImpl::vInitializeAudioTypeMap()
   ***************************************************************************/
   /*!
   * \fn     vInitializeAudioTypeMap()
   * \brief  Map the CarPlay specific audio type to SPI specific audio type.
   * \param  None
   * \retVal Void
   * \sa
   **************************************************************************/
   static std::map<t_String, tenDiPOMainAudioType> vInitializeAudioTypeMap();


   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOAudioOutAdapterImpl::vGetAudioType()
   ***************************************************************************/
   /*!
   * \fn     vGetAudioType(trMsgQBase &rfoMsgQBase)
   * \brief  Convert the audio stream type to enum valuse to use in SPI
   * \param  azAudioType : [IN] Audio type in string
   * \param  enAudioType : [OUT] Audio type
   * \sa     
   **************************************************************************/
   t_Void vGetAudioType(const t_String azAudioType, tenDiPOMainAudioType &coenAudioType);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOAudioOutAdapterImpl::bSendIPCMessage()
   ***************************************************************************/
   /*!
   * \fn     bSendIPCMessage(trMsgQBase &rfoMsgQBase)
   * \brief  Send the IPC message to SPI component.
   * \param  rMessage : [IN]Message data
   * \retVal  t_Bool : True if message send success, false otherwise
   * \sa     
   **************************************************************************/
   template<typename trMessage>
   t_Bool bSendIPCMessage(trMessage rMessage);

   //! Audio out adapter handle
   static spi_tclDiPOAudioOutAdapterImpl * m_poAudioOutAdapter;

   //! Current DiPO Audio Format
   trDiPOAudioFormat m_rCurrAudioFormat;

   //! Current allocated audio channel [M ain audio or Alternate audio] 
   AudioChannelType m_enCurrAllocatedChannel;

   //! Current DiPo main Audio Type
   tenDiPOMainAudioType m_enAudioType;

   //!Audio map device
   static std::map<t_String, tenDiPOMainAudioType> m_mapAudioType;
};

#endif
