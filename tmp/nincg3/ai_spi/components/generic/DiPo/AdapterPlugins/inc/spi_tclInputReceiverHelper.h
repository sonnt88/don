/*!
*******************************************************************************
* \file              spi_tclInputReceiverHelper.h
* \brief             DiPO Input receiver helper class
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO input receiver helper implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
5.3.2014  |  Shihabudheen P M            | Initial Version
03.4.2014 |  Hari Priya E R              | Included changes for hard key handling

\endverbatim
******************************************************************************/
#ifndef SPI_TCLINPUTRECEIVERHELPER_H
#define SPI_TCLINPUTRECEIVERHELPER_H

#include "GenericSingleton.h"
#include "DiPOTypes.h"

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
//! Size of the user input Data for single touch
#define SINGLETOUCH_DATA_SIZE 5

//! Size of the user input Data for telephony keys
#define TELEPHONE_KEYDATA_SIZE 2

//! Defines for Apple specific HID descriptor to readable
#define HID_USAGE_PAGE(a)                  0x05, (a)
#define HID_USAGE(a)                       0x09, (a)
#define HID_COLLECTION(a)                  0xA1, (a)
#define HID_END_COLLECITON()               0xC0
#define HID_REPORT_SIZE(size)              0x75, (size)
#define HID_REPORT_COUNT(count)            0x95, (count)
#define HID_INPUT(a)                       0x81, (a)
#define HID_LOGICAL_MINMAX(min, max)       0x15, (min), 0x25, (max)
#define HID_USAGE_MINMAX(min, max)         0x19, (min), 0x29, (max)
#define HID_LOGICAL_MINMAX16(min, max)     0x16, ((min) & 0xff), (((min) >> 8) & 0xff), \
    0x26, ((max) & 0xff), (((max) >> 8) & 0xff)
#define HID_PHYSICAL_MINMAX16(min, max)    0x36, ((min) & 0xff), (((min) >> 8) & 0xff), \
    0x46, ((max) & 0xff), (((max) >> 8) & 0xff)
#define HID_UNIT(a)                        0x65, (a)
#define HID_EXPONENT(a)                    0x55, (a)



 /*! 
  * \static const t_U8 coSingleTouchDescriptor
  * \Brief : This formulate the initial report about the input device which is
  *          used to send the user input later. This report sends to the device
  *          on startup to inform(Attach Input Device) about the usage of the 
  *          input device. The report includes the type of the input device, 
  *          type of the user input data, length of the user input data etc.
  *          For more information, see section 15.3.5 of Apple accessory specification.
  */

static const t_U8 coSingleTouchDescriptor[] =
{
    HID_USAGE_PAGE (0x0D),
    HID_USAGE (0x04),
    HID_COLLECTION (0x01),
        // Digitizer
        HID_USAGE_PAGE (0x0D),
        HID_USAGE (0x22),
        HID_COLLECTION (0x02),
            // Digitizer
            HID_USAGE_PAGE (0x0D),
            HID_USAGE (0x33),

            // Overall logical info.
            HID_LOGICAL_MINMAX(0x00, 0x01),
            HID_REPORT_SIZE(0x01),
            HID_REPORT_COUNT(0x01),
            HID_INPUT(0x02),

            // Report data info
            HID_REPORT_SIZE(0x07),
            HID_REPORT_COUNT(0x01),
            HID_INPUT(0x01),

            HID_USAGE_PAGE (0x01),

            // X Cordinate value info
            HID_USAGE (0x30),
            HID_LOGICAL_MINMAX16(0x0000, 0x7fff),
            HID_REPORT_SIZE(0x10),
            HID_REPORT_COUNT(0x01),
            HID_INPUT(0x02),

            // Y cordinate value info
            HID_USAGE (0x31),
            HID_LOGICAL_MINMAX16(0x0000, 0x7fff),
            HID_REPORT_SIZE(0x10),
            HID_REPORT_COUNT(0x01),
            HID_INPUT(0x02),
        HID_END_COLLECITON(),
    HID_END_COLLECITON()
};

/*! 
  * \static const t_U8 ConsumerKeyInfo
  * \Brief : This formulates the initial report about the Consumer Key input device.
  */
static const t_U8 coConsumerKeyDescriptor[] =
{
   HID_USAGE_PAGE (0x0C), //USAGE PAGE (CONSUMER Device)
   HID_USAGE (0x01), //USAGE (Consumer Control)
   HID_COLLECTION(0x01), //COLLECTION (Application)
   HID_LOGICAL_MINMAX(0x00, 0x01),
   HID_REPORT_SIZE(0x01),//  REPORT SIZE (1)
   HID_REPORT_COUNT(0x06),//  REPORT COUNT (6)
   HID_USAGE (0xCD),//  USAGE (Play/Pause)
   HID_USAGE (0xB5),//  USAGE (Scan Next Track)
   HID_USAGE (0xB6),//  USAGE (Scan Previous Track)
   HID_USAGE (0xB0), //  USAGE (Play)
   HID_USAGE (0xB1), //  USAGE (Pause)
   0x0a, 0x24, 0x02, // AC Back
   HID_INPUT(0x02), //  INPUT (Date,Var,Abs)
   HID_REPORT_SIZE(0x02), //  REPORT SIZE (2)
   HID_REPORT_COUNT(0x01), //  REPORT COUNT (1)
   HID_INPUT(0x03), //  INPUT (Cnst,Var,Abs)
   HID_END_COLLECITON()//END COLLECTION

};


/*! 
  * \static const t_U8 TelephoneKeyInfo
  * \Brief : This formulates the initial report about the Telephone Key input device.
  */
static const t_U8 coTelephonyKeyDescriptor[] =
{
   HID_USAGE_PAGE(0x0B), // usage page
   HID_USAGE(0x01), // usage
   HID_COLLECTION(0x01), // collection
   // Telephony
   HID_USAGE_PAGE(0x0B), // usage page
   HID_USAGE(0x20), // Hook
   HID_USAGE(0x21), // flash
   HID_USAGE(0x26), // Drop
   HID_USAGE(0xB0), // Key 0
   HID_USAGE(0xB1), // Key 1
   HID_USAGE(0xB2), // Key 2
   HID_USAGE(0xB3), // Key 3
   HID_USAGE(0xB4), // Key 4
   HID_USAGE(0xB5), // Key 5
   HID_USAGE(0xB6), // Key 6
   HID_USAGE(0xB7), // Key 7
   HID_USAGE(0xB8), // Key 8
   HID_USAGE(0xB9), // Key 9
   HID_USAGE(0xBA), // Key Star
   HID_USAGE(0xBB), // Key Pound
   HID_LOGICAL_MINMAX(0x00, 0x01),
   HID_REPORT_SIZE(0x01), // report size
   HID_REPORT_COUNT(0x0F), // report count
   HID_INPUT(0x02), // array
   HID_REPORT_SIZE(0x01), // report size
   HID_REPORT_COUNT(0x01), // report count
   HID_INPUT(0x01), // array
   HID_END_COLLECITON() // end collection
};




/****************************************************************************/
/*!
* \class spi_tclInputReceiverHelper
* \brief DiPO Input receiver helper class implementation
*
* spi_tclDiPOConfigHelper is a singletone class which holds the configuration
* handler and act as an interface to perform the Dynamic configurations.
****************************************************************************/
class spi_tclInputReceiverHelper : public GenericSingleton<spi_tclInputReceiverHelper> 
{
public:
   
  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclInputReceiverHelper::~spi_tclInputReceiverHelper()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclInputReceiverHelper()
   * \brief  Destructor 
   * \sa     spi_tclInputReceiverHelper()
   **************************************************************************/
   virtual ~spi_tclInputReceiverHelper();

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputReceiverHelper::vSetInputReceiverHandler()
   ***************************************************************************/
   /*!
   * \fn     vSetInputReceiverHandler(IInputReceiver *poReceiver)
   * \brief  function to set the IInputReceiver handler
   * \param  poReceiver : [IN] IControl reciever handler
   * \sa     
   **************************************************************************/
   t_Void vSetInputReceiverHandler(IInputReceiver *poInputReceiver);

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputReceiverHelper::poGetControlReceiverHandler()
   ***************************************************************************/
   /*!
   * \fn     poGetControlReceiverHandler()
   * \brief  function to get the IInputReciever handler
   * \retVal  IInputReceiver* :IInput reciever handler
   * \sa     
   **************************************************************************/
   IInputReceiver* poGetControlReceiverHandler();

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputReceiverHelper::bAttchInputDevice()
   ***************************************************************************/
   /*!
   * \fn     bAttchInputDevice()
   * \brief  function to attach an Input Device.
   * \param  poInputReceiver: [IN]Input Receiver Instance
   * \param  enInputType: [IN]Input type-touch or key
   * \parm   rHIDDeviceInfo: [IN] Device Info
   * \sa     
   **************************************************************************/
   t_Bool bAttchInputDevice(
      tenInputType enInputType,const trHIDDeviceInfo rHIDDeviceInfo);

  /***************************************************************************
   ** FUNCTION: t_Bool spi_tclInputReceiverHelper::bSendTouchInputData()
   ***************************************************************************/
   /*!
   * \fn     bSendTouchInputData()
   * \brief  function to send the touch input data
   * \rfoTouchCoordinates : [IN] Touch cordinate data
   * \ retVal bool : true if success , false otherwise
   * \sa     
   **************************************************************************/
   t_Bool bSendTouchInputData(const trTouchCoordinates &rfoTouchCoordinates);

     /***************************************************************************
   ** FUNCTION: t_Bool spi_tclInputReceiverHelper::bSendKeyData()
   ***************************************************************************/
   /*!
   * \fn     bSendKeyData()
   * \brief  function to send the key input data
   * \param  enKeyMode : [IN] Key Mode-Pressed or Released
   * \param  enKeyCode : [IN] Key Code-Unique identifier of the Key 
   * \retVal bool : true if success , false otherwise
   * \sa     
   **************************************************************************/
   t_Bool bSendKeyData(tenKeyMode enKeyMode,tenKeyCode enKeyCode);


   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclInputReceiverHelper::bSendInputReport()
   ***************************************************************************/
   /*!
   * \fn     bSendInputReport()
   * \brief  function to send the input data to the device
   * \rHIDInputReport : [IN]HID Input Report
   * \ retVal bool : true if success , false otherwise
   * \sa     
   **************************************************************************/
   t_Bool bSendInputReport(const trHIDInputReport &rHIDInputReport);

  /*! 
   * \friend class GenericSingleton<spi_tclInputReceiverHelper>
   * \brief friend class declaration
   */
   friend class GenericSingleton<spi_tclInputReceiverHelper>;
private:

  /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclInputReceiverHelper::spi_tclInputReceiverHelper()
   ***************************************************************************/
   /*!
   * \fn     spi_tclInputReceiverHelper()
   * \brief  Constructor 
   * \sa     ~spi_tclInputReceiverHelper()
   **************************************************************************/
   spi_tclInputReceiverHelper();

    /***************************************************************************
   ** FUNCTION:  spi_tclInputReceiverHelper::spi_tclInputReceiverHelper
   ***************************************************************************/
   /*!
   * \fn      spi_tclInputReceiverHelper(const spi_tclInputReceiverHelper &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclInputReceiverHelper(const spi_tclInputReceiverHelper& corfrSrc);

   /***************************************************************************
   ** FUNCTION:spi_tclInputReceiverHelper& spi_tclInputReceiverHelper::operator= .
   ***************************************************************************/
   /*!
   * \fn      spi_tclInputReceiverHelper& operator= (const 
                        spi_tclInputReceiverHelper &orfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclInputReceiverHelper& operator =
      (const spi_tclInputReceiverHelper& corfrSrc);



  /*! 
   * \IInputReceiver* m_poInputReceiver
   * \brief IInputReceiver handler
   */
   IInputReceiver* m_poInputReceiver;

  /*! 
   * \trHIDDeviceInfo m_rHIDTouchDeviceInfo
   * \brief HID touch device Info
   */
   trHIDDeviceInfo m_rHIDTouchDeviceInfo;

     /*! 
   * \trHIDDeviceInfo m_rHIDConsumerKeyDeviceInfo
   * \brief HID Consumer Key device Info
   */
   trHIDDeviceInfo m_rHIDConsumerKeyDeviceInfo;

        /*! 
   * \trHIDDeviceInfo m_rHIDTelKeyDeviceInfo
   * \brief HID Telephony Key device Info
   */
   trHIDDeviceInfo m_rHIDTelKeyDeviceInfo;


   //!Consumer Key data
   t_U8 m_u8ConsumerKeydata;

   //!Telephony Key data
   t_U8 m_u8TelephoneKeydata[TELEPHONE_KEYDATA_SIZE];
   






};

#endif