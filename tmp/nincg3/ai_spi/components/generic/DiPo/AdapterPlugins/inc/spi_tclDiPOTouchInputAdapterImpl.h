/*!
*******************************************************************************
* \file              spi_tclDiPOTouchInputAdapterImpl.h
* \brief             DiPO Input Adapter implementation
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO input adapter implementation.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
5.3.2014  |  Shihabudheen P M            | Initial Version

\endverbatim
******************************************************************************/

#ifndef SPI_TCLDIPOTOUCHINPUTADAPTERIMPL_H
#define SPI_TCLDIPOTOUCHINPUTADAPTERIMPL_H

#include "DiPOTypes.h"

/****************************************************************************/
/*!
* \class spi_tclDiPOTouchInputAdapterImpl
* \brief DiPO Input adapter implementation
*
* spi_tclDiPOTouchInputAdapterImpl is a realization of IInputAdapter
****************************************************************************/
class spi_tclDiPOTouchInputAdapterImpl : public IInputAdapter
{
public:
  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl::spi_tclDiPOTouchInputAdapterImpl()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOTouchInputAdapterImpl()
   * \brief  Constructor
   * \sa     ~spi_tclDiPOTouchInputAdapterImpl()
   **************************************************************************/
   spi_tclDiPOTouchInputAdapterImpl();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl::~spi_tclDiPOTouchInputAdapterImpl()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPOTouchInputAdapterImpl()
   * \brief  Constructor
   * \sa     spi_tclDiPOTouchInputAdapterImpl()
   **************************************************************************/
   virtual ~spi_tclDiPOTouchInputAdapterImpl();
   
  /***************************************************************************
   ** FUNCTION:  virtual t_Bool spi_tclDiPOTouchInputAdapterImpl::Initialize()...
   ***************************************************************************/
   /*!
   * \fn     Initialize(const IConfiguration& rfConfig, IInputReceiver& rfReceiver)
   * \brief  Control adapter initialization. This is called when a dipo session 
   *         is initialized. 
   * \param  rfConfig : [IN] IConfiguration handler
   * \param  rfReceiver : [IN] IControlReceiver handler
   * \retVal bool : true if initializatio success, false otherwise
   * \sa     
   **************************************************************************/
   virtual t_Bool Initialize(const IConfiguration& rfConfig, IInputReceiver& rfReceiver);

   /***************************************************************************
    ** FUNCTION:  static spi_tclDiPOTouchInputAdapterImpl*
    **            poGetDiPOTouchInputAdapterInstance();
    ***************************************************************************/
    /*!
    * \fn     poGetDiPOTouchInputAdapterInstance()
    * \brief  Method to get a pointer to spi_tclDiPOTouchInputAdapterImpl class
    * \param  None
    * \param  None
    * \sa
    **************************************************************************/
   static spi_tclDiPOTouchInputAdapterImpl* poGetDiPOTouchInputAdapterInstance();

   /***************************************************************************
    ** FUNCTION: t_Bool spi_tclDiPOTouchInputAdapterImpl::bSendTouchEvent()
    ***************************************************************************/
    /*!
    * \fn     bSendTouchEvent()
    * \brief  function to send the touch input data
    * \param  rfcorTouchCoordinates : [IN] Touch cordinate data
    * \ retVal bool : true if success , false otherwise
    * \sa
    **************************************************************************/
	t_Bool bSendTouchEvent(const trTouchCoordinates &rfcorTouchCoordinates);

   /***************************************************************************
    ** FUNCTION:  static spi_tclDiPOTouchInputAdapterImpl
    **            vSetTouchProperty();
    ***************************************************************************/
    /*!
    * \fn     vSetTouchProperty()
    * \brief  Method to get a Displayheight & Displaywidth
    * \param  
    * \param  
    * \sa
    **************************************************************************/
   static t_Void vSetTouchProperty(const t_U32 u32Displayheight, const t_U32 u32Displaywidth);

  /***************************************************************************
   ************************END OF PUBLIC *************************************
   ***************************************************************************/

private:

  /***************************************************************************
   ************************PRIVATE********************************************
   ***************************************************************************/

   /*!
    * \IInputReceiver* m_poInputReceiver
    * \brief IInputReceiver handler
    */
    IInputReceiver* m_poIInputReceiver;

    /*!
     * \static spi_tclDiPOTouchInputAdapterImpl* m_poDiPOTouchInputAdapter;
     * \brief Pointer to spi_tclDiPOTouchInputAdapterImpl class type
     */
    static spi_tclDiPOTouchInputAdapterImpl* m_poDiPOTouchInputAdapter;

   /*!
    * \trHIDDeviceInfo m_rHIDTouchDeviceInfo
    * \brief HID touch device Info
    */
    trHIDDeviceInfo m_rHIDTouchDeviceInfo;

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl::spi_tclDiPOTouchInputAdapterImpl
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPOTouchInputAdapterImpl(const spi_tclDiPOTouchInputAdapterImpl &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDiPOTouchInputAdapterImpl(const spi_tclDiPOTouchInputAdapterImpl& corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOTouchInputAdapterImpl& spi_tclDiPOTouchInputAdapterImpl::operator= .
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPOTouchInputAdapterImpl& operator= (const spi_tclDiPOTouchInputAdapterImpl &orfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDiPOTouchInputAdapterImpl& operator =
      (const spi_tclDiPOTouchInputAdapterImpl& corfrSrc);

	//! Display parameters
   static t_U32 m_u32Height;
   static t_U32 m_u32Width;
  /***************************************************************************
   ************************END OF PRIVATE*************************************
   ***************************************************************************/
};

#endif 
