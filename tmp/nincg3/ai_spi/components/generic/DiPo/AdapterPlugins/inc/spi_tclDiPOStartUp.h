/*!
*******************************************************************************
* \file              spi_tclDiPOStartUp.h
* \brief             DiPO StartUP
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO StartUP code. This class is get called from ADIT process
                once the DiPO session is active. This class is ussed to register
                the customized adapter implementation classes
COPYRIGHT:      &copy; RBEI

HISTORY:
Date      |  Author                      | Modifications
5.2.2014  |  Shihabudheen P M            | Initial Version
10.5.2014 |  Hari Priya E R              | Included separate functions for handling touch and key messages
03.09.2014 |  Shihabudheen P M           | Changes for set up the core-DeviceId
17.03.2015 | Shihabudheen P M            | Changes for setting display parameters on start up.
15.06.2015 | Shihabudheen P M            | added bProcessBtAddressUpdateMsg().
 
\endverbatim
******************************************************************************/

#ifndef SPI_TCLDIPOSTRATUP_H
#define SPI_TCLDIPOSTRATUP_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <stdio.h>
#include "dipo_plugin.h"
#include "DiPOTypes.h"
#include "MsgQIPCThreader.h"
#include "MsgQThreadable.h"

using namespace shl::thread;
using namespace dipo;

static const t_U8 coU8LowFidelityTouch = 0x02;
static const t_U8 coU8HighFidelityTouch = 0x04;
static const t_U8 coU8KnobLowFidelityTouch = 0x08;
static const t_U8 coU8KnobHighFidelityTouch = 0x10;
static const t_String sczManfacturedByGM = "GM";

struct trInitConfigParameters
{
   t_U32 u32DisplayHeightMM;

   t_U32 u32DisplayWidthMM;

   t_U32 u32DisplayHeightPixels;

   t_U32 u32DisplayWidthPixels;

   t_String szBluetoothId;

   t_U8 u8DisplayInput;

   t_String szCoreModelName;

   trInitConfigParameters()
   {
      u32DisplayHeightMM = 0;
      u32DisplayWidthMM =0;
      u32DisplayHeightPixels =0;
      u32DisplayWidthPixels =0;
      szBluetoothId = "";
      u8DisplayInput = coU8KnobHighFidelityTouch;
      szCoreModelName = "";
   }//trInitConfigParametrs()

   trInitConfigParameters(t_U32 u32Height, t_U32 u32Width, 
      t_U32 u32HeightPixel, t_U32 u32widthPixel ,t_String BTId ,t_U8 DispInput, t_String modelName)
   {
      u32DisplayHeightMM = u32Height;
      u32DisplayWidthMM = u32Width;
      u32DisplayHeightPixels = u32HeightPixel;
      u32DisplayWidthPixels = u32widthPixel;
      szBluetoothId = BTId;
      u8DisplayInput = DispInput;
      szCoreModelName = modelName;
   }//trInitConfigParametrs()
};

/****************************************************************************/
/*!
* \class spi_tclDiPOStartUp
* \brief DiPO Startup class
*
* spi_tclDiPOStartUp is used to initialize and set up all the configurations
* required for DiPO. It includes the registation of Adapter implementations
* (spi_tclDiPOControlAdapterImpl, etc.)
****************************************************************************/
class spi_tclDiPOStartUp : public MsgQThreadable
{
public:
  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::spi_tclDiPOStartUp()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOStartUp()
   * \brief  Constructor 
   * \sa     ~spi_tclDiPOStartUp()
   **************************************************************************/
   spi_tclDiPOStartUp();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::~spi_tclDiPOStartUp()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPOStartUp()
   * \brief  Destructor 
   * \sa     spi_tclDiPOStartUp()
   **************************************************************************/
	virtual ~spi_tclDiPOStartUp();

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::vExecute
   ***************************************************************************/
   /*!
   * \fn      t_Void vExecute(tShlMessage *poMessage)
   * \brief   Responsible for posting the message to respective dispatchers
   * \param   poMessage : message received from MsgQ for dispatching
   **************************************************************************/
   virtual t_Void vExecute(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::tShlMessage* poGetMsgBuffer(size_t )
   ***************************************************************************/
   /*!
   * \fn      tShlMessage* poGetMsgBuffer(size_t )
   * \brief  This function will be called for requesting the storage allocation for received
   *           message
   * \param siBuffer: size of the buffer to be allocated for the received message
   **************************************************************************/
   virtual tShlMessage* poGetMsgBuffer(size_t siBuffer);

  /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPOStartUp::bSendIPCMessage()
   ***************************************************************************/
   /*!
   * \fn     bSendIPCMessage(trMsgQBase &rfoMsgQBase)
   * \brief  Send the IPC message to SPI component.
   * \param  rMessage : [IN]Message data
   * \retVal  t_Bool : True if message send success, false otherwise
   * \sa     
   **************************************************************************/
   template<typename trMessage>
   t_Bool bSendIPCMessage(trMessage rMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::vSetInitConfigParam()
   ***************************************************************************/
   /*!
   * \fn      vSetInitConfigParam()
   * \brief  Setter function for member.
   * \param rInitConfigParameters: [IN]Init configurations
   **************************************************************************/
   t_Void vSetInitConfigParam(trInitConfigParameters rInitConfigParameters);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::vGetInitConfigParam()
   ***************************************************************************/
   /*!
   * \fn     vGetInitConfigParam()
   * \brief  Getter function for member.
   * \param  rfoInitConfigParameters: [OUT]Init configurations
   **************************************************************************/
   t_Void vGetInitConfigParam(trInitConfigParameters &rfoInitConfigParameters);

   /*************************************************************************
   ****************************END OF PUBLIC*********************************
   *************************************************************************/
	
private: 

  /*************************************************************************
   ****************************PRIVATE**************************************
   *************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp:bProcessAudioMsg( )
   ***************************************************************************/
   /*!
   * \fn     bProcessAudioMsg(tShlMessage *poMessage)
   * \brief  To handle the audio message from SPI and to update the corresponding
   *         CarPlay configurations
   * \param  poMessage: [IN] Audio message from SPI
   **************************************************************************/
   t_Bool bProcessAudioMsg(tShlMessage *poMessage);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessTouchMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn      bProcessTouchMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received touch message
   * \param poMessage: [IN]Received touch message
   **************************************************************************/
   t_Bool bProcessTouchMsg(tShlMessage *poMessage);
   
   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessKeyMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessKeyMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received key message
   * \param poMessage: [IN]Received key message
   **************************************************************************/
   t_Bool bProcessKeyMsg(tShlMessage *poMessage);
   
   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessLaunchAppMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessLaunchAppMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received request UI message
   * \param poMessage: [IN]Received Request UI message
   **************************************************************************/
   t_Bool bProcessLaunchAppMsg(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessAccessoryAudioContextMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessAccessoryAudioContextMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received accessory Audio context message
   * \param poMessage: [IN]Received Audio context message
   **************************************************************************/
   t_Bool bProcessAccessoryAudioContextMsg(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessAccessoryVideoContextMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessAccessoryVideoContextMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received accessory Video context message
   * \param poMessage: [IN]Received Video context message
   **************************************************************************/
   t_Bool bProcessAccessoryVideoContextMsg(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessAccessoryAppStateMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessAccessoryAppStateMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received accessory App State message
   * \param poMessage: [IN]Received App State message
   **************************************************************************/
   t_Bool bProcessAccessoryAppStateMsg(tShlMessage *poMessage);

    /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessSiriActionMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessSiriActionMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received Siri Action message
   * \param poMessage: [IN]Received Siri Action  message
   **************************************************************************/
   t_Bool bProcessSiriActionMsg(tShlMessage *poMessage);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessAudioStopMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessAudioStopMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received audio stop message
   * \param poMessage: [IN]Received message
   **************************************************************************/
   t_Bool bProcessAudioStopMsg(tShlMessage *poMessage);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessAudioErrorMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessAudioErrorMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the received audio error message
   * \param poMessage: [IN]Received message
   **************************************************************************/
   t_Bool bProcessAudioErrorMsg(tShlMessage *poMessage);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessVehicleModeMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessVehicleModeMsg(tShlMessage *poMessage )
   * \brief  This function is used to handle the vehicle mode info message.
   * \param poMessage: [IN]Received message
   **************************************************************************/
   t_Bool bProcessVehicleModeMsg(tShlMessage *poMessage);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessInfoMessage(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessInfoMessage(tShlMessage *poMessage )
   * \brief  This function process the info message
   * \param  poMessage: [IN]Received message
   **************************************************************************/
   t_Bool bProcessInfoMessage(tShlMessage *poMessage);

     /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp::bProcessVideoConfigMsg(tShlMessage *poMessage )
   ***************************************************************************/
   /*!
   * \fn     bProcessVideoConfigMsg(tShlMessage *poMessage )
   * \brief  This function process the info message
   * \param  poMessage: [IN]Received message
   **************************************************************************/
   t_Bool bProcessVideoConfigMsg(tShlMessage *poMessage);
   
   /***************************************************************************
    ** FUNCTION:  spi_tclDiPOStartUp::bProcessBtAddressUpdateMsg(tShlMessage *poMessage )
    ***************************************************************************/
    /*!
    * \fn     bProcessBtAddressUpdateMsg(tShlMessage *poMessage )
    * \brief  This function process BT address update message
    * \param  poMessage: [IN]Received message
    **************************************************************************/
    t_Bool bProcessBtAddressUpdateMsg(tShlMessage *poMessage);

   /***************************************************************************
    ** FUNCTION:  spi_tclDiPOStartUp::bProcessRestrictionsUpdateMsg(tShlMessage *poMessage )
    ***************************************************************************/
    /*!
    * \fn     bProcessRestrictionsUpdateMsg(tShlMessage *poMessage )
    * \brief  This function process Restrictions update message
    * \param  poMessage: [IN]Received message
    **************************************************************************/
    t_Bool bProcessRestrictionsUpdateMsg(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp:vSetVideoMode( )
   ***************************************************************************/
   /*!
   * \fn      vSetVideoMode(trDiPOVideoContext &rfoDiPOVideoContext,
   *          trModeChange &rfoModeChange)
   * \brief  used to populate tha video resource handler parameteres
   * \param  rfoDiPOVideoContext: [IN] Video context data
   * \param  rfoModeChange : [OUT] request data 
   **************************************************************************/
   t_Void vSetVideoMode(trDiPOVideoContext &rfoDiPOVideoContext,
      trModeChange &rfoModeChange);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp:vSetAudioMode( )
   ***************************************************************************/
   /*!
   * \fn      vSetAudioMode(trDiPOAudioContext &rfoDiPOAudioContexts,
   *          trModeChange &rfoModeChange)
   * \brief  used to populate tha audio resource handling parameteres
   * \param  rfoDiPOAudioContext: [IN] Audio context data
   * \param  rfoModeChange : [OUT] request data 
   **************************************************************************/
   t_Void vSetAudioMode(trDiPOAudioContext &rfoDiPOAudioContext,
      trModeChange &rfoModeChange);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPOStartUp:vSetAppState( )
   ***************************************************************************/
   /*!
   * \fn      vSetAppState(const trDiPOAppState &rfcorDiPOAppState,
   *           trModeChange &rfoModeChange)
   * \brief  used to populate tha app state handling parameters
   * \param  rfcorDiPOAppState: [IN] App data
   * \param  rfoModeChange : [OUT] request data 
   **************************************************************************/
   t_Void vSetAppState(const trDiPOAppState &rfcorDiPOAppState, trModeChange &rfoModeChange);

   //! Initialization config parameters.
   trInitConfigParameters m_rInitConfigParam;
   /*************************************************************************
   ****************************END OF PRIVATE*******************************
   *************************************************************************/

};

#endif
