/***********************************************************************/
/*!
* \file  spi_tclDiPoAppMngr.h
* \brief DiPo App Mngr Implementation
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPo App Mngr Implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version
20.03.2014  | Shihabudheen P M      | Modified 1.vLaunchApp
27.06.2014  | Hari Priya E R        | Included Siri Action handling in LaunchApp
18.07.2014  | Shihabudheen P M      | Added vSetVehicleConfig
05.06.2015  | Tejaswini HB          | Added lint comments to suppress C++11 errors
06.05.2015  | Tejaswini HB          | Lint Fix
17.07.2015  | Sameer Chandra        | Memory leak fix
15.10.2015  | Shihabudheen P M      | Added vUpdateConfigInfo

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//#define VNC_USE_STDINT_H                   //Commented to Fix Lint errors,May be required for future use
#include <string.h>
#include "DiPOTypes.h"
#include "IPCMessageQueue.h"
#include "spi_tclAppMngrDefines.h"
#include "spi_tclDiPOResourceMngr.h"
#include "spi_tclDiPoAppMngr.h"
#include "spi_tclConfigReader.h"

using namespace std;

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclDiPoAppMngr.cpp.trc.h"
#endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/***************************************************************************
** FUNCTION:  spi_tclDiPoAppMngr::spi_tclDiPoAppMngr()
***************************************************************************/
spi_tclDiPoAppMngr::spi_tclDiPoAppMngr()
{
   ETG_TRACE_USR1(("spi_tclDiPoAppMngr::spi_tclDiPoAppMngr()\n"));
}

/***************************************************************************
** FUNCTION:  spi_tclDiPoAppMngr::~spi_tclDiPoAppMngr()
***************************************************************************/
spi_tclDiPoAppMngr::~spi_tclDiPoAppMngr()
{
   ETG_TRACE_USR1((" spi_tclDiPoAppMngr::~spi_tclDiPoAppMngr() \n"));
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDiPoAppMngr::bInitialize()
***************************************************************************/
t_Bool spi_tclDiPoAppMngr::bInitialize()
{
   ETG_TRACE_USR1((" spi_tclDiPoAppMngr::bInitialize() \n" ));
   //Add code
   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAppMngr::vUnInitialize()
***************************************************************************/
t_Void spi_tclDiPoAppMngr::vUnInitialize()
{
   ETG_TRACE_USR1((" spi_tclDiPoAppMngr::vUnInitialize() \n"));
   //Add code
}
   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclDiPoAppMngr::vRegisterAppMngrCallbacks()
   ***************************************************************************/
t_Void spi_tclDiPoAppMngr::vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs)
{
   //Add code
   m_rAppMngrCallbacks = corfrAppMngrCbs;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAppMngr::vSelectDevice()
***************************************************************************/
t_Void spi_tclDiPoAppMngr::vSelectDevice(const t_U32 cou32DevId,
                                         const tenDeviceConnectionReq coenConnReq)
{
	/*lint -esym(40,fpvSelectDeviceResult)fpvSelectDeviceResult Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclDiPoAppMngr::vSelectDevice:Dev-0x%x",cou32DevId));
   SPI_INTENTIONALLY_UNUSED(coenConnReq);

   if(NULL != m_rAppMngrCallbacks.fpvSelectDeviceResult)
   {
      (m_rAppMngrCallbacks.fpvSelectDeviceResult)(true);
   }//if(NULL != m_rAppMngrCallbacks.fpvSelectDeviceResult)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDiPoAppMngr::bLaunchApp()
***************************************************************************/
t_Void spi_tclDiPoAppMngr::vLaunchApp(const t_U32 cou32DevId, 
                                      t_U32 u32AppHandle,
                                      const trUserContext& rfrcUsrCntxt, 
                                      tenDiPOAppType enDiPOAppType, 
                                      t_String szTelephoneNumber, 
                                      tenEcnrSetting enEcnrSetting)
{
	/*lint -esym(40,fpvLaunchAppResult)fpvLaunchAppResult Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclDiPoAppMngr::vLaunchApp:Dev-0x%x App-0c%x \n",
      cou32DevId,u32AppHandle));
   SPI_INTENTIONALLY_UNUSED(enEcnrSetting);

   t_Bool bIsSiriAction = false;
   t_Bool bMessageSendStatus = false;

   //! URL of the application to launch
   t_Char szAppUrl[MAX_STR_LEN]={0};
   SiriAction enSiriAction=SiriAction_NA;
   // Open the IPC message queue
   IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);

   // Assigning the URL values for different Apps as per the Apple Specification
   switch ((t_U8) enDiPOAppType)
   {
      case e8DIPO_NOT_USED:
      case e8DIPO_NO_URL:
      {
         // Message to launch the home screen
         strncpy(szAppUrl, "", MAX_STR_LEN);
      }
         break;
      case e8DIPO_MAPS:
      {
         // Message to launch the Maps
         strncpy(szAppUrl, "maps:", MAX_STR_LEN);
      }
         break;
      case e8DIPO_MOBILEPHONE:
      {
         // Message to launch the Mobilephone application
         strncpy(szAppUrl, "mobilephone:", MAX_STR_LEN);
      }
         break;
      case e8DIPO_TEL_NUMBER:
      {
         // Message to launch the phone call with given number
         strncpy(szAppUrl, "tel:", MAX_STR_LEN);
         strncat(szAppUrl, szTelephoneNumber.c_str(), szTelephoneNumber.size());
      }
         break;
      case e8DIPO_SIRI_PREWARN:
      {
         enSiriAction = SiriAction_Prewarm;
         bIsSiriAction = true;
      }
         break;

      case e8DIPO_SIRI_BUTTONDOWN:
      {
         enSiriAction = SiriAction_ButtonDown;
         bIsSiriAction = true;
      }
         break;
      case e8DIPO_SIRI_BUTTONUP:
      {
         enSiriAction = SiriAction_ButtonUp;
         bIsSiriAction = true;
      }
         break;
      case e8DIPO_MUSIC:
      {
         strncpy(szAppUrl, "music:", MAX_STR_LEN);
      }
         break;
      case e8DIPO_NOWPLAYING:
      {
         strncpy(szAppUrl, "nowplaying:", MAX_STR_LEN);
      }
         break;
      default:
         bIsSiriAction = false;
         break;
   }//End of switch


   if (true == bIsSiriAction)
   {
      //Create the Siri Action message
      trSiriActionMsg *poSiriActionMsg = (trSiriActionMsg *)oMessageQueue.vpCreateBuffer(sizeof(trSiriActionMsg));
      if(NULL!= poSiriActionMsg)
      {

         poSiriActionMsg->enMsgType = e8SIRIACTION_MESSAGE;
         poSiriActionMsg->enSiriAction = enSiriAction;
         ETG_TRACE_USR4(("Siri Action: %d", poSiriActionMsg->enSiriAction));
         bMessageSendStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*)poSiriActionMsg,DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
         //Destroy the buffer
         oMessageQueue.vDropBuffer(poSiriActionMsg);
      }
   }
   else
   {
      // Create the UI message
      trRequestUIMsg *poRequestUIMsg = (trRequestUIMsg *)oMessageQueue.vpCreateBuffer(sizeof(trRequestUIMsg));
      if(NULL != poRequestUIMsg)
      {
         poRequestUIMsg->enMsgType = e8REQUEST_UI_MESSAGE;
         strncpy(poRequestUIMsg->szAppUrl, szAppUrl, MAX_STR_LEN);

         // Send the IPC message
         ETG_TRACE_USR4(("poRequestUIMsg->szAppUrl: %s", poRequestUIMsg->szAppUrl));
         bMessageSendStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*)poRequestUIMsg,DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
         //Destroy the buffer
         oMessageQueue.vDropBuffer(poRequestUIMsg);
      }
   }
   ETG_TRACE_USR4(("IPC Message send status from vLaunchApp : %d", bMessageSendStatus));


   if( NULL != m_rAppMngrCallbacks.fpvLaunchAppResult)
   {
      (m_rAppMngrCallbacks.fpvLaunchAppResult)(cou32DevId,u32AppHandle,enDiPOAppType,
         e8NO_ERRORS,rfrcUsrCntxt);
   }//if( NULL != m_rAppMngrCallbacks.fpvLaunchAppResult)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAppMngr::vTerminateApp()
***************************************************************************/
t_Void spi_tclDiPoAppMngr::vTerminateApp(const t_U32 cou32DevId,
                                         const t_U32 cou32AppId,
                                         const trUserContext& rfrcUsrCntxt)
{
	/*lint -esym(40,fpvTerminateAppResult)fpvTerminateAppResult Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //Post the success as a Result to tcl App Mngr
   if( NULL != m_rAppMngrCallbacks.fpvTerminateAppResult )
   {
      (m_rAppMngrCallbacks.fpvTerminateAppResult)(cou32DevId,cou32AppId,
         e8NO_ERRORS,rfrcUsrCntxt);
   }//if( NULL != m_rAppMngrCallbacks.fpvTerminateAppResult )
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAppMngr::vGetAppIconData()
***************************************************************************/
t_Void spi_tclDiPoAppMngr::vGetAppIconData(t_String szAppIconUrl, 
                                           const trUserContext& rfrcUsrCntxt)
{
	/*lint -esym(40,fpvCbAppIconDataResult)fpvCbAppIconDataResult Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(szAppIconUrl);
   //Post rsponse to HMI
   if( NULL != m_rAppMngrCallbacks.fpvCbAppIconDataResult)
   {
      (m_rAppMngrCallbacks.fpvCbAppIconDataResult)(e8ICON_INVALID,NULL,0,rfrcUsrCntxt);
   }//if( NULL != m_rAppMngrCallbacks.fpvCbAppIconDataResult)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDiPoAppMngr::bCheckAppValidity()
***************************************************************************/
t_Bool spi_tclDiPoAppMngr::bCheckAppValidity(const t_U32 cou32DevId, 
                                             const t_U32 cou32AppId)
{
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   SPI_INTENTIONALLY_UNUSED(cou32AppId);
   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAppMngr::vSetVehicleConfig()
***************************************************************************/
t_Void spi_tclDiPoAppMngr::vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
                         t_Bool bSetConfig)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(bSetConfig);

   vUpdateConfigInfo(enVehicleConfig);

   IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
   trVehicleModeInfoMsg *poVehicleModeMsg = 
      (trVehicleModeInfoMsg *)oMessageQueue.vpCreateBuffer(sizeof(trVehicleModeInfoMsg));
   if(NULL != poVehicleModeMsg)
   {
      poVehicleModeMsg->enMsgType = e8VEHICLE_MODE_MESSAGE;
      poVehicleModeMsg->enVehicleConfig = enVehicleConfig;
      t_Bool bStatus = (0 <= oMessageQueue.iSendBuffer((t_Void*)poVehicleModeMsg,DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
      ETG_TRACE_USR4(("IPC Message send status from vSetVehicleConfig : %d", bStatus));
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poVehicleModeMsg);
   }//if(NULL != poVehicleModeMsg)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAppMngr::vUpdateConfigInfo()
***************************************************************************/
t_Void spi_tclDiPoAppMngr::vUpdateConfigInfo(const tenVehicleConfiguration enVehicleConfig)
{
	spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();
	switch(enVehicleConfig)
	{
	case e8PARK_MODE:
	case e8DRIVE_MODE:
		{
			if(NULL != poConfigReader)
			{
				poConfigReader->vSetDriveModeInfo(enVehicleConfig);
			}
		}
		break;
	case e8_DAY_MODE:
	case e8_NIGHT_MODE:
		{
			if(NULL != poConfigReader)
			{
				poConfigReader->vSetNightModeInfo(enVehicleConfig);
			}
		}
	break;
	}
}

///////////////////////////////////////////////////////////////////////////////

//lint –restore
// <EOF>
