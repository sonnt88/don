/***********************************************************************/
/*!
* \file  spi_tclDiPoAppMngr.h
* \brief DiPo App Mngr Implementation
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPo App Mngr Implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/


#ifndef _SPI_TCLDIPOAPPMNGR_H_
#define _SPI_TCLDIPOAPPMNGR_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclAppMngrDev.h"

class spi_tclDiPORMMsgHandler;
class spi_tclDiPOResourceMngr;
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclDiPoAppMngr
* \brief DiPo App Mngr Implementation
****************************************************************************/
class spi_tclDiPoAppMngr : public spi_tclAppMngrDev
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoAppMngr::spi_tclDiPoAppMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoAppMngr()
   * \brief   Default Constructor
   * \sa      ~spi_tclDiPoAppMngr()
   **************************************************************************/
   spi_tclDiPoAppMngr();

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPoAppMngr::~spi_tclDiPoAppMngr()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclDiPoAppMngr()
   * \brief   Destructor
   * \sa      spi_tclDiPoAppMngr()
   **************************************************************************/
   ~spi_tclDiPoAppMngr();
 
   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoAppMngr::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all theDipo App Mngr related things
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoAppMngr::vUnInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUnInitialize()
   * \brief   To Uninitialize all the Dipo App Mngr related things
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vUnInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclDiPoAppMngr::vRegisterAppMngrCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo App Mngr
   * \param   corfrAppMngrCbs : [IN] Application Manager callabcks structure
   * \retval  t_Void 
   **************************************************************************/
   t_Void vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoAppMngr::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSelectDevice(const t_U32 cou32DevId,
   *          const tenDeviceConnectionReq coenConnReq)
   * \brief   To Subscribe/unsubscribe for events of the currently selected device
   * \pram    cou32DevId : [IN] Unique Device Id
   * \param   coenConnReq : [IN] connected/disconnected
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoAppMngr::bLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bLaunchApp(const t_U32 cou32DevId, 
   *           t_U32 u32AppHandle, 
   *           tenDiPOAppType enDiPOAppType, 
   *           t_String szTelephoneNumber, 
   *           tenEcnrSetting enEcnrSetting)
   * \brief   To Launch the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param  [IN] enDevCat : Device Type Information(Mirror Link/DiPO).
   * \param  [IN] u32AppHandle : Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface. 
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
   *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] szTelephoneNumber : Number to be dialed if the DiPO application to be launched 
   *              is a phone application. If not valid to be used, this will be set to NULL, 
   *              zero length string. Will not be used if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] enEcnrSetting : Sets voice or server echo cancellation and noise reduction 
   *              settings if the DiPO application to be launched is a phone application. 
   *              If not valid to be used, this will be set to ECNR_NOCHANGE.
   * \retval  t_Bool
   * \sa      vTerminateApp()
   **************************************************************************/
   t_Void vLaunchApp(const t_U32 cou32DevId, 
      t_U32 u32AppHandle,
      const trUserContext& rfrcUsrCntxt, 
      tenDiPOAppType enDiPOAppType, 
      t_String szTelephoneNumber, 
      tenEcnrSetting enEcnrSetting);

     /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoAppMngr::vTerminateApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vTerminateApp(trUserContext rUserContext,
   *          t_U32 u32DeviceId, t_U32 u32AppId)
   * \brief   To Terminate an Application asynchronously.
   * \param   rUserContext : [IN] Context Message
   * \param   u32DeviceId  : [IN] Device Id
   * \param   u32AppId     : [IN] Application Id
   * \retval  t_Void
   * \sa      t_Bool bLaunchApp(t_U32 u32DeviceId, t_U32 u32AppId)
   **************************************************************************/
   t_Void vTerminateApp(const t_U32 cou32devId, 
      const t_U32 cou32AppId,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoAppMngr::vGetAppIconData()
   ***************************************************************************/
   /*!
   * \fn    virtual t_Void vGetAppIconData(t_String szAppIconUrl, 
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  szAppIconUrl  : [IN] Application Icon data
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppIconData(t_String szAppIconUrl, 
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoAppMngr::bCheckAppValidity()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bCheckAppValidity(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId)
   * \brief   To check whether the application exists or not
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId  : [IN] Application Id
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bCheckAppValidity(const t_U32 cou32DevId, 
      const t_U32 cou32AppId);

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclDiPoAppMngr::vSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
   *          t_Bool bSetConfig,const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the Vehicle configurations.
   * \param  [IN] enVehicleConfig :  Identifies the Vehicle Configuration.
   * \param  [IN] bSetConfig      : Enable/Disable config
   **************************************************************************/
   virtual t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoAppMngr& spi_tclDiPoAppMngr::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoAppMngr& operator= (const spi_tclDiPoAppMngr &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDiPoAppMngr& operator= (const spi_tclDiPoAppMngr &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoAppMngr::spi_tclDiPoAppMngr(const spi_tclDiPoAppMngr..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoAppMngr(const spi_tclDiPoAppMngr &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDiPoAppMngr(const spi_tclDiPoAppMngr &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoAppMngr::vUpdateConfigInfo()
   ***************************************************************************/
   /*!
   * \fn      vUpdateConfigInfo(const tenVehicleConfiguration enVehicleConfig)
   * \brief   Copy constructor, will not be implemented.
   * \note    Function to update the values in config reader.
   * \param   enVehicleConfig : [IN] latest config value.
   **************************************************************************/
   t_Void vUpdateConfigInfo(const tenVehicleConfiguration enVehicleConfig);

   //! call back structure tos end response to Main App lmgr
   trAppMngrCallbacks m_rAppMngrCallbacks;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};//spi_tclDiPoAppMngr


#endif //_SPI_TCLDIPOAPPMNGR_H_
