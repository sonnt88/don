  /*!
 *******************************************************************************
 * \file         spi_tclDiPoBluetooth.h
 * \brief        DiPO Bluetooth class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    DiPO Bluetooth handler class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 24.11.2014 |  Ramya Murthy (RBEI/ECP2)         | Implemented BT block/unblock for GM
 
 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLDIPOBLUETOOTH_H
#define _SPI_TCLDIPOBLUETOOTH_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_BluetoothTypedefs.h"
#include "spi_tclBluetoothDevBase.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/* Forward Declarations. */
class spi_tclBluetoothIntf;
class spi_tclBluetoothClient;

/*!
* \class spi_tclDiPoBluetooth
* \brief This is the DiPO Bluetooth class that handles the Bluetooth
*        connection logic during a DiPO device session
*/
class spi_tclDiPoBluetooth : public spi_tclBluetoothDevBase
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoBluetooth::spi_tclDiPoBluetooth(...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclDiPoBluetooth(spi_tclBluetoothIntf* poBTInterface,
   *             spi_tclBluetoothPolicyBase* poBTPolicyBase,
   *             tenBTDisconnectStrategy enBTDisconnStrategy)
   * \brief   Parameterized Constructor
   * \param   [IN] poBTInterface: Pointer to Bluetooth manager interface
   * \param   [IN] poBTPolicyBase: Pointer to Bluetooth policy
   * \param   [IN] enBTDisconnStrategy: BT disconnection strategy for DiPO devices
   ***************************************************************************/
   spi_tclDiPoBluetooth(spi_tclBluetoothIntf* poBTInterface,
         spi_tclBluetoothPolicyBase* poBTPolicyBase,
         tenBTDisconnectStrategy enBTDisconnStrategy);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPoBluetooth::~spi_tclDiPoBluetooth();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclDiPoBluetooth()
   * \brief   Virtual Destructor
   ***************************************************************************/
   virtual ~spi_tclDiPoBluetooth();

   /*********Start of functions overridden from spi_tclBluetoothDevBase*******/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when SelectDevice request is received by SPI.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \retval  None
   **************************************************************************/
   t_Void vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenResponseCode enRespCode)
   * \brief   Called when SelectDevice operation is complete, with the result
   *          of the operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enRespCode: Response code enumeration
   * \retval  None
   **************************************************************************/
   t_Void vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenResponseCode enRespCode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when DeselectDevice request is received by SPI.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \retval  None
   **************************************************************************/
   t_Void vOnSPIDeselectDeviceRequest(t_U32 u32ProjectionDevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenResponseCode enRespCode)
   * \brief   Called when DeselectDevice operation is complete, with the result
   *          of the operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enRespCode: Response code enumeration
   * \param   [IN] bIsDeviceSwitch: True - if a projection device switch is in progress
   * \retval  None
   **************************************************************************/
   t_Void vOnSPIDeselectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenResponseCode enRespCode,
         t_Bool bIsDeviceSwitch);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnBTDeviceSwitchAction(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
   *             t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange)
   * \brief   Called with user action when there is a device switch occurring
   *          between a Projection and a BT device.
   *          Mandatory interface to be implemented.
   * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
   * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
   * \param  [IN] enBTChange  : Indicates user's device change action
   * \retval  None
   **************************************************************************/
   t_Void vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enInitialBTChange,
         tenBTChangeInfo enFinalBTChange);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnDisableBluetooth(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnDisableBluetooth(t_String szProjectionDevBTAddr)
   * \brief   Called when Bluetooth connection should be disabled during
   *          projection session.
   *          Mandatory interface to be implemented.
   * \param   [IN] szProjectionDevBTAddr : BT address of active projection device
   * \retval  None
   **************************************************************************/
   t_Void vOnDisableBluetooth(t_String szProjectionDevBTAddr);

   /*********End of functions overridden from spi_tclBluetoothDevBase*********/

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /*********Start of functions overridden from spi_tclBluetoothDevBase*******/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnBTConnectionResult(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTConnectionResult(t_String szBTDeviceAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Interface to receive result of a BT device connection/disconnection request.
   * \param   [IN] szBTDeviceAddress: Bluetooth address of device
   *          Mandatory interface to be implemented.
   * \param   [IN] enBTConnResult: Connection/Disconnection result.
   * \retval  None
   **************************************************************************/
   t_Void vOnBTConnectionResult(t_String szBTDeviceAddress,
         tenBTConnectionResult enBTConnResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnBTConnectionChanged(t_String...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclBluetoothDevBase(t_String szBTDeviceAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Interface to receive notification on a BT device connection/disconnection.
   *          Mandatory interface to be implemented.
   * \param   [IN] szBTDeviceAddress: Bluetooth address of device
   * \param   [IN] enBTConnResult: Connection/Disconnection result.
   * \retval  None
   **************************************************************************/
   t_Void vOnBTConnectionChanged(t_String szBTDeviceAddress,
         tenBTConnectionResult enBTConnResult);

   /*********End of functions overridden from spi_tclBluetoothDevBase*********/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/
   /***************************************************************************
    ** FUNCTION: spi_tclDiPoBluetooth(const spi_tclDiPoBluetooth &rfcoBluetooth)
    ***************************************************************************/
   /*!
    * \fn      spi_tclDiPoBluetooth(const spi_tclDiPoBluetooth &rfcoBluetooth)
    * \brief   Copy constructor not implemented hence made private
    **************************************************************************/
   spi_tclDiPoBluetooth(const spi_tclDiPoBluetooth &rfcoBluetooth);

   /***************************************************************************
    ** FUNCTION: const spi_tclDiPoBluetooth & operator=(
    **                                 const spi_tclDiPoBluetooth &rfcoBluetooth);
    ***************************************************************************/
   /*!
    * \fn      const spi_tclDiPoBluetooth & operator=(const spi_tclDiPoBluetooth &rfcoBluetooth);
    * \brief   assignment operator not implemented hence made private
    **************************************************************************/
   const spi_tclDiPoBluetooth & operator=(
            const spi_tclDiPoBluetooth &rfcoBluetooth);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vRegisterBTCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterBTCallbacks()
   * \brief   Registers callbacks to Bluetooth client.
   * \retval  None
   **************************************************************************/
   t_Void vRegisterBTCallbacks();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoBluetooth::bValidateBTDisconnectionRequired()
   ***************************************************************************/
   /*!
   * \fn      bValidateBTDisconnectionRequired()
   * \brief   Determines if BT disconnection is required
   * \retval  Bool: TRUE - if BT device disconnection is required, else FALSE
   **************************************************************************/
   t_Bool bValidateBTDisconnectionRequired();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoBluetooth::bTriggerBTDeviceDisconnection()
   ***************************************************************************/
   /*!
   * \fn      bTriggerBTDeviceDisconnection()
   * \brief   Triggers disconnection of active BT device.
   * \retval  Bool: TRUE - if BT device disconnection is triggered, else FALSE
   **************************************************************************/
   t_Bool bTriggerBTDeviceDisconnection();

   /***************************************************************************
   ** FUNCTION:   t_Bool spi_tclDiPoBluetooth::bTriggerBTDevicesBlocking()
   ***************************************************************************/
   /*!
   * \fn      bTriggerBTDevicesBlocking()
   * \brief   Triggers request to block single/all BT devices.
   * \retval  Bool: TRUE - if blocking is triggered successfully, else FALSE
   **************************************************************************/
   t_Bool bTriggerBTDevicesBlocking(tenBTDeviceBlockType enBTBlockType);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vHandleBTtoDiPOSwitch(tenBTChangeInfo...)
   ***************************************************************************/
   /*!
   * \fn      vHandleBTtoDiPOSwitch(tenBTChangeInfo enBTChangeAction)
   * \brief   Implements logic to switch from BT to DiPO device
   * \retval  None
   **************************************************************************/
   t_Void vHandleBTtoDiPOSwitch(tenBTChangeInfo enBTChangeAction);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vHandleDiPOtoBTSwitch(tenBTChangeInfo...)
   ***************************************************************************/
   /*!
   * \fn      vHandleDiPOtoBTSwitch(tenBTChangeInfo enBTChangeAction)
   * \brief   Implements logic to switch from DiPO to BT device
   * \retval  None
   **************************************************************************/
   t_Void vHandleDiPOtoBTSwitch(tenBTChangeInfo enBTChangeAction);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDiPoBluetooth::vApplyBTDisconnectionStrategy()
   ***************************************************************************/
   /*!
   * \fn      vApplyBTDisconnectionStrategy()
   * \brief   Implements logic to disconnect BT devices based on project requirement
   * \retval  None
   **************************************************************************/
   t_Void vApplyBTDisconnectionStrategy();

   /***************************************************************************
   ** FUNCTION:   t_Void spi_tclDiPoBluetooth::vDeselectDipoDevice()
   ***************************************************************************/
   /*!
   * \fn      vDeselectDipoDevice()
   * \brief   Requests for deselection of current active DiPO device.
   * \retval  None
   **************************************************************************/
   t_Void vDeselectDipoDevice();

   /***************************************************************************
   ** FUNCTION:   t_Bool spi_tclDiPoBluetooth::bIsBlockingStrategyActive()
   ***************************************************************************/
   /*!
   * \fn      bIsBlockingStrategyActive()
   * \brief   Validates if active BT disconnection strategy is of blocking type
   * \retval  t_Bool: TRUE - If blocking strategy is active, else FALSE
   **************************************************************************/
   t_Bool bIsBlockingStrategyActive();

   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   /***************************************************************************
   ** BT Manager interface pointer
   ***************************************************************************/
   spi_tclBluetoothIntf* const   m_cpoBTInterface;

   /***************************************************************************
   ** BT PolicyBase pointer
   ***************************************************************************/
   spi_tclBluetoothPolicyBase* const   m_cpoBTPolicyBase;

   /***************************************************************************
   ** BT disconnection mode
   ***************************************************************************/
   const tenBTDisconnectStrategy   m_coenBTDisconnStrategy;

   /***************************************************************************
   ** Indicates whether BT Blocking is triggered on device selection
   ***************************************************************************/
   t_Bool   m_bBTBlockedOnSelect;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

};

#endif // _SPI_TCLDIPOBLUETOOTH_H

