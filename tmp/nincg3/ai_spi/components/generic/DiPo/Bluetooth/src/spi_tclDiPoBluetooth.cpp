  /*!
 *******************************************************************************
 * \file         spi_tclDiPoBluetooth.cpp
 * \brief        DiPo Bluetooth class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    DiPo Bluetooth handler class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 24.11.2014 |  Ramya Murthy (RBEI/ECP2)         | Implemented BT block/unblock for GM
 26.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors


 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclBluetoothIntf.h"
#include "spi_tclBluetoothPolicyBase.h"
#include "spi_tclDiPoBluetooth.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_BLUETOOTH
      #include "trcGenProj/Header/spi_tclDiPoBluetooth.cpp.trc.h"
   #endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclDiPoBluetooth::spi_tclDiPoBluetooth();
***************************************************************************/
spi_tclDiPoBluetooth::spi_tclDiPoBluetooth(spi_tclBluetoothIntf* poBTInterface,
      spi_tclBluetoothPolicyBase* poBTPolicyBase,
      tenBTDisconnectStrategy enBTDisconnStrategy)
      : m_cpoBTInterface(poBTInterface),
        m_cpoBTPolicyBase(poBTPolicyBase),
        m_coenBTDisconnStrategy(enBTDisconnStrategy),
        m_bBTBlockedOnSelect(false)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth() entered: enBTDisconnStrategy = %d ",
         ETG_ENUM(BT_DISCONN_MODE, m_coenBTDisconnStrategy)));
   SPI_NORMAL_ASSERT(NULL == m_cpoBTInterface);
   SPI_NORMAL_ASSERT(NULL == m_cpoBTPolicyBase);
} //!end of spi_tclDiPoBluetooth()

/***************************************************************************
** FUNCTION:  spi_tclDiPoBluetooth::~spi_tclDiPoBluetooth();
***************************************************************************/
spi_tclDiPoBluetooth::~spi_tclDiPoBluetooth()
{
   ETG_TRACE_USR1(("~spi_tclDiPoBluetooth() entered "));
} //!end of ~spi_tclDiPoBluetooth()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceRequest(t_U32...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vOnSPISelectDeviceRequest() entered "));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);

   vRegisterBTCallbacks();

   //! Block all devices if there is no BT device active. Else wait for DisableBT msg to validate active BT device.
   if (
      (e8BT_STRATEGY_BLOCK_ALL == m_coenBTDisconnStrategy)
      &&
      (NULL != m_cpoBTPolicyBase)
      &&
      (false == IS_VALID_BT_ADDRESS(m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress()))
      )
   {
      m_bBTBlockedOnSelect = bTriggerBTDevicesBlocking(e8BLOCK_ALL_DEVICES);
   }

   if (NULL != m_cpoBTInterface)
   {
      //! Set Device status - this is in order to prevent SwitchDevice during ongoing selection
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SELECTION_IN_PROGRESS);

      //! Proceed with DiPO selection (wait for DisableBluetooth msg to disconnect BT if block is not triggered).
      m_cpoBTInterface->vSendSelectDeviceResult(true);
   }
} //!end of vOnSPISelectDeviceRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceResponse(t_U32...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
      tenResponseCode enRespCode)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vOnSPISelectDeviceResponse() entered "));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);
   
   //@Note: If BT device is connected, we wait for DisableBluetooth msg to disconnect BT.
   
   if (NULL != m_cpoBTInterface)
   {
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
   }

   //! If Selection has failed, unblock BT devices if previously blocked.
   if (
      (e8FAILURE == enRespCode)
      &&
      (true == bIsBlockingStrategyActive())
      &&
      (NULL != m_cpoBTPolicyBase)
      &&
      (false == m_cpoBTPolicyBase->bUnblockBTDevice(e8UNBLOCK_ALL_DEVICES, ""))
      )
   {
      ETG_TRACE_ERR((" vOnSPISelectDeviceResponse: Unblocking BT devices failed! "));
   }
   //! If Selection is successful, process DisableBT cmd if it was received during Selection
   //! (observed when CarPlay session is started before selection process is completed)
   else if (
      (e8SUCCESS == enRespCode)
      &&
      (NULL != m_cpoBTInterface)
      )
   {
      t_String szSelDevBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();
      if (IS_VALID_BT_ADDRESS(szSelDevBTAddress))
      {
         ETG_TRACE_ERR((" vOnSPISelectDeviceResponse: Processing DisableBT command now.. "));
         vOnDisableBluetooth(szSelDevBTAddress);
      }
   }
} //!end of vOnSPISelectDeviceResponse()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceRequest(t_U32...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vOnSPIDeselectDeviceRequest(t_U32 u32ProjectionDevHandle)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vOnSPIDeselectDeviceRequest() entered"));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);
   if (NULL != m_cpoBTInterface)
   {
      // Set Device status - this is in order to prevent SwitchDevice during ongoing deselection
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_DESELECTION_IN_PROGRESS);
      // Nothing else to be done. Simply send success result.
      m_cpoBTInterface->vSendSelectDeviceResult(true);
   }
} //!end of vOnSPIDeselectDeviceRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnSPISelectDeviceResponse(t_U32...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vOnSPIDeselectDeviceResponse(t_U32 u32ProjectionDevHandle,
      tenResponseCode enRespCode,
      t_Bool bIsDeviceSwitch)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vOnSPIDeselectDeviceResponse() entered"));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);
   SPI_INTENTIONALLY_UNUSED(enRespCode);

   tenBTDeviceUnblockType enUnblockType = (bIsDeviceSwitch || m_bBTBlockedOnSelect) ?
         e8UNBLOCK_ALL_DEVICES : e8UNBLOCK_ALL_DEVICES_AUTO_CONNECT;

   //! Clear flag
   m_bBTBlockedOnSelect = false;

   //! Unblock BT devices, if blocked
   if (
      /*(e8SUCCESS == enRespCode)
      &&*/         //TODO - required?
      (true == bIsBlockingStrategyActive())
      &&
      (NULL != m_cpoBTPolicyBase)
      &&
      (false == m_cpoBTPolicyBase->bUnblockBTDevice(enUnblockType, ""))
      )
   {
      ETG_TRACE_ERR((" vOnSPIDeselectDeviceResponse: Unblocking BT devices failed! "));
   }
   //! Clear device status
   if (NULL != m_cpoBTInterface)
   {
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
   }
} //!end of vOnSPIDeselectDeviceResponse()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnBTDeviceSwitchAction(t_U32...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
      t_U32 u32ProjectionDevHandle,
      tenBTChangeInfo enInitialBTChange,
      tenBTChangeInfo enFinalBTChange)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vOnInvokeBTDeviceAction() entered: "));
   ETG_TRACE_USR4((" vOnInvokeBTDeviceAction: BT DevHandle = 0x%x, Projection DevHandle = 0x%x ",
         u32BluetoothDevHandle, u32ProjectionDevHandle));
   ETG_TRACE_USR4((" vOnInvokeBTDeviceAction: InitialBTChange = %d, enFinalBTChange = %d ",
         ETG_ENUM(BT_CHANGE_INFO, enInitialBTChange), ETG_ENUM(BT_CHANGE_INFO, enFinalBTChange)));

   //! Handle requested device switch action only if device change is in progress
   //! (i.e. if SPI had previously sent a device switch event)
   if (
      (NULL != m_cpoBTInterface)
      &&
      (e8DEVICE_SWITCH_IN_PROGRESS == m_cpoBTInterface->enGetSelectedDevStatus())
      )
   {
      //! Handle switch from BT device to DiPo device.
      //! @Note: When switch from BT to DiPo device is triggered, if user selects to:
      //! a) Switch to DiPo device - BT device should be disconnected in order to
      //!    continue with DiPo device activation.
      //! b) Cancel switch operation - DiPo device selection should be canceled, and
      //!    BT connection should remain unchanged.

      if (e8SWITCH_BT_TO_DIPO == enInitialBTChange)
      {
         vHandleBTtoDiPOSwitch(enFinalBTChange);
      }//if (e8SWITCH_BT_TO_DIPO == enInitialBTChange)
      else if (e8SWITCH_DIPO_TO_BT == enInitialBTChange)
      {
         vHandleDiPOtoBTSwitch(enFinalBTChange);
      }//else if (e8SWITCH_BT_TO_DIPO == enInitialBTChange)
   } //if ((NULL != m_cpoBTInterface)&&...)
   else
   {
      ETG_TRACE_ERR((" vOnInvokeBTDeviceAction: Null ptr/Device Switch not in progress. "));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnDisableBluetooth(t_String...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vOnDisableBluetooth(t_String szDiPODevBTAddress)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vOnDisableBluetooth() entered: DiPO device BTAddress = %s ",
         szDiPODevBTAddress.c_str()));

   if (NULL != m_cpoBTInterface)
   {
      //! Store BT address of DiPO device (since BTAddress is only received with DisableBT msg)
      if (IS_VALID_BT_ADDRESS(szDiPODevBTAddress))
      {
         m_cpoBTInterface->vSetSelectedDevBTAddress(szDiPODevBTAddress);
      }//if (IS_VALID_BT_ADDRESS(szDiPODevBTAddress))

      //! If a BT device is connected, and:
      //! 1. Devices are different - send device switch event.
      //! 2. Devices are same - disconnect BT
      if (
         (e8DEVICE_CHANGE_COMPLETE == m_cpoBTInterface->enGetSelectedDevStatus())
         &&
         (NULL != m_cpoBTPolicyBase)
         )
      {
         t_String szConnDevBTAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();

         //! If a BT device is active, trigger DeviceSwitch
         if (IS_VALID_BT_ADDRESS(szConnDevBTAddr))
         {
            ETG_TRACE_USR3((" vOnDisableBluetooth: Requesting BT to DiPO device switch "));
            t_U32 u32DiPODevHandle = m_cpoBTInterface->u32GetSelectedDevHandle();
            t_U32 u32ConnBTDevHandle = m_cpoBTPolicyBase->u32GetBTDeviceHandle(szConnDevBTAddr);
            t_Bool bIsSameDevice = (szDiPODevBTAddress == szConnDevBTAddr);
            t_Bool bCallActive = m_cpoBTInterface->bGetCallStatus();

            m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SWITCH_IN_PROGRESS);
            m_cpoBTInterface->vSendDeviceSwitchEvent(u32ConnBTDevHandle, u32DiPODevHandle,
                  e8SWITCH_BT_TO_DIPO, bIsSameDevice, bCallActive);
         }//if (IS_VALID_BT_ADDRESS(szConnDevBTAddr))
         //! Else apply BT disconnection strategy ONLY if blocking is required.
         else if (true == bIsBlockingStrategyActive())
         {
            vApplyBTDisconnectionStrategy();
         }
      }//if (e8DEVICE_CHANGE_COMPLETE ==...)
      else
      {
         ETG_TRACE_USR3((" vOnDisableBluetooth: No action taken since there is an ongoing device change. "));
      }
   }//if (NULL != m_cpoBTInterface)
}//!end of vOnDisableBluetooth()

/***************************************************************************
*********************************PROTECTED**********************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnBTConnectionResult(...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vOnBTConnectionResult(t_String szBTDeviceAddress,
      tenBTConnectionResult enBTConnResult)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vOnBTConnectionResult() entered: BTConnectionResult = %d, BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult), szBTDeviceAddress.c_str()));

   //! @Note:
   //! (1) BT Device connection/disconnection result will be received only for
   //!     an SPI-triggered BT device connection/disconnection.
   //! (2) Unblocking result need not be handled since result will be received
   //!     after DiPO device is deselected.

   if (NULL != m_cpoBTInterface)
   {
      //! Handle BT connection/disconnection result only if a DiPO device is active
      if (
         (IS_VALID_DEVHANDLE(m_cpoBTInterface->u32GetSelectedDevHandle()))
         &&
         (e8DEVICE_CHANGE_IN_PROGRESS == m_cpoBTInterface->enGetSelectedDevStatus())
         )
      {
         //! Disconnection/Blocking success result can be received:
         //! 1. After DisableBluetooth(DiPO), OR
         //! 2. When user cancels switch from DiPo to BT device.

         switch (enBTConnResult)
         {
            case e8BT_RESULT_DISCONNECTED:
            {
               ETG_TRACE_USR1((" BT device successfully disconnected "));
               m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
            }
               break;
            case e8BT_RESULT_BLOCKED:
            {
               ETG_TRACE_USR1((" BT device(s) successfully blocked "));
               if (e8BT_STRATEGY_BLOCK_ALL == m_coenBTDisconnStrategy)
               {
                  m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
               }
               //! Disconnect active BT device(if any), since blocking of DiPO device is complete
               else if (e8BT_STRATEGY_BLOCK_SINGLE == m_coenBTDisconnStrategy)
               {
                  if (false == bValidateBTDisconnectionRequired())
                  {
                     m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
                  }
                  else if (false == bTriggerBTDeviceDisconnection())
                  {
                     ETG_TRACE_ERR((" Sending BT disconnect request failed! Hence deactivating DiPo device. "));
                     vDeselectDipoDevice();
                  }
                  //@Note: If BT disconnection is triggered, device status should not be cleared!
               }//else if (e8BT_STRATEGY_BLOCK_SINGLE == m_coenBTDisconnStrategy)
            }
               break;
            case e8BT_RESULT_DISCONN_FAILED:
            case e8BT_RESULT_BLOCK_FAILED:
            {
               ETG_TRACE_USR1((" BT device disconnection/blocking failed! "));
               vDeselectDipoDevice();
            }
               break;
            default:
               ETG_TRACE_ERR((" vOnBTConnectionResult: Invalid enum encountered = %u ",
                     ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult)));
               break;
         } //switch (enBTConnResult)
      } //if (IS_VALID_BT_ADDRESS(m_cpoBTInterface->u32GetSelectedDevHandle()))
      else
      {
         ETG_TRACE_ERR((" vOnBTConnectionResult: No device active/Ongoing BT device change ! "));
      }
   }//if (NULL != m_cpoBTInterface)
} //!end of vOnBTConnectionResponse()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vOnBTConnectionChanged(t_String...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vOnBTConnectionChanged(t_String szBTDeviceAddress,
      tenBTConnectionResult enBTConnResult)
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vOnBTConnectionChanged() entered: BTConnectionResult = %d, BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult), szBTDeviceAddress.c_str()));

   if (NULL != m_cpoBTInterface)
   {
      t_U32 u32DiPoDevHandle = m_cpoBTInterface->u32GetSelectedDevHandle();

      if (
         (IS_VALID_DEVHANDLE(u32DiPoDevHandle)) /*If DiPo device is active*/
         &&
         (e8BT_RESULT_CONNECTED == enBTConnResult) /*If a BT device has been connected*/
         &&
         (IS_VALID_BT_ADDRESS(szBTDeviceAddress)) /*If BT device address is valid*/
         &&
         (NULL != m_cpoBTPolicyBase)
         &&
         (e8DEVICE_CHANGE_COMPLETE == m_cpoBTInterface->enGetSelectedDevStatus())
            //@Note: This is checked to prevent a SwitchDevice (DiPO to BT) event during an ongoing blocking
         )
      {
         //Fetch DeviceHandle of connected BT device
         t_U32 u32ConnBTDevHandle = m_cpoBTPolicyBase->u32GetBTDeviceHandle(szBTDeviceAddress);
         t_Bool bIsSameDevice = (szBTDeviceAddress == m_cpoBTInterface->szGetSelectedDevBTAddress());

         //@Note: Call Status is not required to be sent for this scenario,
         //since BT is the newly connected device.
         m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SWITCH_IN_PROGRESS);
         m_cpoBTInterface->vSendDeviceSwitchEvent(u32ConnBTDevHandle, u32DiPoDevHandle,
               e8SWITCH_DIPO_TO_BT, bIsSameDevice, false);
      }//if ((IS_VALID_DEVHANDLE(u32DiPoDevHandle) &&...)
   }//if (NULL != m_cpoBTInterface)

} //!end of vOnBTConnectionChanged()


/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vRegisterBTCallbacks()
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vRegisterBTCallbacks()
{

   /*lint -esym(40,fvOnBTConnectionResult)fvOnBTConnectionResult Undeclared identifier */
   /*lint -esym(40,fvOnBTConnectionChanged)fvOnBTConnectionChanged Undeclared identifier */
   /*lint -esym(40,_1)_1 Undeclared identifier */
   /*lint -esym(40,_2)_2 Undeclared identifier */
   if (NULL != m_cpoBTPolicyBase)
   {
      //!Initialize BT response callbacks structure
      trBluetoothCallbacks rBluetoothCb;
      rBluetoothCb.fvOnBTConnectionResult =
         std::bind(&spi_tclDiPoBluetooth::vOnBTConnectionResult,
               this,
               SPI_FUNC_PLACEHOLDERS_2);
      rBluetoothCb.fvOnBTConnectionChanged =
         std::bind(&spi_tclDiPoBluetooth::vOnBTConnectionChanged,
               this,
               SPI_FUNC_PLACEHOLDERS_2);
      m_cpoBTPolicyBase->vRegisterCallbacks(rBluetoothCb);
   }//if (NULL != m_cpoBTPolicyBase)
}

/***************************************************************************
** FUNCTION:   t_Bool spi_tclDiPoBluetooth::bValidateBTDisconnectionRequired()
***************************************************************************/
t_Bool spi_tclDiPoBluetooth::bValidateBTDisconnectionRequired()
{
   t_Bool bDisconnectRequired = false;

   if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))
   {
      t_String szConnBTAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();
      t_String szDipoBTAddr = m_cpoBTInterface->szGetSelectedDevBTAddress();

      switch (m_coenBTDisconnStrategy)
      {
         //! BT disconnection will be required if there is any BT device connected
         case e8BT_STRATEGY_DISCONNECT_ALL:
         case e8BT_STRATEGY_BLOCK_ALL:
            bDisconnectRequired = (IS_VALID_BT_ADDRESS(szConnBTAddr));
            break;
         //! BT disconnection will be required if there is a BT device connected other
         //! than active DiPO device (since DiPO device will be blocked)
         case e8BT_STRATEGY_BLOCK_SINGLE:
            bDisconnectRequired = ((IS_VALID_BT_ADDRESS(szConnBTAddr)) && (szConnBTAddr != szDipoBTAddr));
            break;
         default:
            ETG_TRACE_ERR(("bValidateBTDisconnectionRequired: Invalid enum! "));
            break;
      }//switch (m_coenBTDisconnStrategy)
   }//if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))

   ETG_TRACE_USR2(("spi_tclDiPoBluetooth::bValidateBTDisconnectionRequired() left with DisconnectRequired = %u ",
         ETG_ENUM(BOOL, bDisconnectRequired)));
   return bDisconnectRequired;
} //!end of bValidateBTDisconnectionRequired()

/***************************************************************************
** FUNCTION:   t_Bool spi_tclDiPoBluetooth::bTriggerBTDeviceDisconnection()
***************************************************************************/
t_Bool spi_tclDiPoBluetooth::bTriggerBTDeviceDisconnection()
{
   //! Request disconnection of currently connected BT device (if any connection exists)
   t_Bool bDisconnectTriggered = false;

   if (NULL != m_cpoBTPolicyBase)
   {
      //! Fetch BT Address of connected device
      t_String szConnBTDevAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();
      ETG_TRACE_USR4((" bTriggerBTDeviceDisconnection: Retrieved connected device BTAddress = %s ",
            szConnBTDevAddr.c_str()));

      if (IS_VALID_BT_ADDRESS(szConnBTDevAddr)) /*If a BT device is connected*/
      {
         bDisconnectTriggered = m_cpoBTPolicyBase->bDisconnectBTDevice(szConnBTDevAddr);
      }
   } //if (NULL != m_cpoBTPolicyBase)

   ETG_TRACE_USR2(("spi_tclDiPoBluetooth::bTriggerBTDeviceDisconnection() left with DisconnectTriggered = %u ",
         ETG_ENUM(BOOL, bDisconnectTriggered)));
   return bDisconnectTriggered;
} //!end of bTriggerBTDeviceDisconnection()

/***************************************************************************
** FUNCTION:   t_Bool spi_tclDiPoBluetooth::bTriggerBTDevicesBlocking()
***************************************************************************/
t_Bool spi_tclDiPoBluetooth::bTriggerBTDevicesBlocking(tenBTDeviceBlockType enBTBlockType)
{
   //! Request disconnection of currently connected BT device (if any connection exists)
   t_Bool bBlockTriggered = false;

   if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))
   {
      t_String szSelDipoBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();
      bBlockTriggered = m_cpoBTPolicyBase->bBlockBTDevice(enBTBlockType, szSelDipoBTAddress);
   } //if (NULL != m_cpoBTPolicyBase)

   ETG_TRACE_USR2(("spi_tclDiPoBluetooth::bTriggerBTDevicesBlocking() left with BlockTriggered = %u ",
         ETG_ENUM(BOOL, bBlockTriggered)));
   return bBlockTriggered;
} //!end of bTriggerBTDevicesBlocking()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vHandleBTtoDiPOSwitch(tenBTChangeInfo...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vHandleBTtoDiPOSwitch(tenBTChangeInfo enBTChangeAction)
{
   switch (enBTChangeAction)
   {
      case e8SWITCH_BT_TO_DIPO:
      {
          if (NULL != m_cpoBTPolicyBase)
         {
            //! Disconnect BT device (if any connected)
            if (true == IS_VALID_BT_ADDRESS(m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress()))
            {
               ETG_TRACE_USR1((" BT to DiPo switch - Attempting to disconnect BT device "));
               vApplyBTDisconnectionStrategy();
            }
            //! No further action required if BT is disconnected when this event is received.
            else
            {
               ETG_TRACE_USR4((" BT to DiPo switch - No action taken since there is no BT device connected "));

               if (NULL != m_cpoBTInterface)
               {
                  m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
               }//if (NULL != m_cpoBTInterface)
            }
         }//if (NULL != m_cpoBTPolicyBase)
      }
         break;
      case e8NO_CHANGE:
      {
         //! Send SelectDevice error
         ETG_TRACE_USR1((" Cancel BT to DiPo switch - Deactivating DiPo device "));
         vDeselectDipoDevice();
      }
         break;
      default:
         ETG_TRACE_ERR((" vHandleBTtoDiPOSwitch: Invalid user action for BT to DiPo switch. "));
         break;
   }//switch (enBTChangeAction)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vHandleDiPOtoBTSwitch(tenBTChangeInfo...)
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vHandleDiPOtoBTSwitch(tenBTChangeInfo enBTChangeAction)
{
   switch (enBTChangeAction)
   {
      case e8SWITCH_DIPO_TO_BT:
      {
         //! Deselect DiPo device
         ETG_TRACE_USR1((" DiPo to BT switch - Deactivating DiPo device "));
         vDeselectDipoDevice();
      }
         break;
      case e8NO_CHANGE:
      {
         //! Disconnect BT device
         if(NULL != m_cpoBTPolicyBase)
         {
            if (true == IS_VALID_BT_ADDRESS(m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress()))
            {
               ETG_TRACE_USR1((" Cancel DiPo to BT switch - Attempting to disconnect BT device "));
               vApplyBTDisconnectionStrategy();
            }
            //! No further action required if BT is disconnected when this event is received.
            else
            {
               ETG_TRACE_USR1((" Cancel DiPo to BT switch - No action taken since there is no BT device connected "));
            }
         }
      }
         break;
      default:
         ETG_TRACE_ERR((" vHandleDiPOtoBTSwitch: Invalid user action for DiPo to BT switch. "));
         break;
   }//switch (enBTChangeAction)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoBluetooth::vApplyBTDisconnectionStrategy()
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vApplyBTDisconnectionStrategy()
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vApplyBTDisconnectionStrategy() entered "));
   ETG_TRACE_USR1(("Applying disconnection strategy type %d ", ETG_ENUM(BT_DISCONN_MODE, m_coenBTDisconnStrategy)));

   if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))
   {
      //Set status to indicate BT disconnection is in progress
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_IN_PROGRESS);

      t_Bool bSuccess = false;

      switch (m_coenBTDisconnStrategy)
      {
         case e8BT_STRATEGY_BLOCK_SINGLE:
         {
            t_String szSelDipoBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();
            //! If received DiPO BT address is invalid, block (and disconnect) all BT devices
            if (false == (IS_VALID_BT_ADDRESS(szSelDipoBTAddress)))
            {
               bSuccess = bTriggerBTDevicesBlocking(e8BLOCK_ALL_DEVICES);
            }
            //! If received DiPO BT address is valid, and device is BT paired, block only the active DiPO device
            //@Note: Once block result is received, disconnect other BT device (if any device is connected)
            else if (true == m_cpoBTPolicyBase->bGetPairingStatus(szSelDipoBTAddress))
            {
               bSuccess = bTriggerBTDevicesBlocking(e8BLOCK_SINGLE_DEVICE);
            }
            //! If received DiPO BT address is valid, but device is not BT paired, simply disconnect other BT device
            else
            {
               bSuccess = bTriggerBTDeviceDisconnection();
            }
         }
            break;
         case e8BT_STRATEGY_BLOCK_ALL:
         {
            bSuccess = bTriggerBTDevicesBlocking(e8BLOCK_ALL_DEVICES);
         }//case e8BT_STRATEGY_BLOCK_ALL:
            break;
         case e8BT_STRATEGY_DISCONNECT_ALL:
         default:
         {
            bSuccess = bTriggerBTDeviceDisconnection();
         }//default
            break;
      }//switch (m_coenBTDisconnStrategy)

      if (false == bSuccess)
      {
         ETG_TRACE_ERR((" Sending BT request failed! Hence deactivating DiPo device. "));
         vDeselectDipoDevice();
      }//if (false == bSuccess)
   }//if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))
} //!end of vApplyBTDisconnectionStrategy()

/***************************************************************************
** FUNCTION:   t_Void spi_tclDiPoBluetooth::vDeselectDipoDevice()
***************************************************************************/
t_Void spi_tclDiPoBluetooth::vDeselectDipoDevice()
{
   ETG_TRACE_USR1(("spi_tclDiPoBluetooth::vDeselectDipoDevice() entered "));
   if (NULL != m_cpoBTInterface)
   {
      //Clear device status
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
      m_cpoBTInterface->vSendDeselectDeviceRequest();
   }
} //!end of vDeselectDipoDevice()

/***************************************************************************
** FUNCTION:   t_Bool spi_tclDiPoBluetooth::bIsBlockingStrategyActive()
***************************************************************************/
t_Bool spi_tclDiPoBluetooth::bIsBlockingStrategyActive()
{
   return ((e8BT_STRATEGY_BLOCK_ALL == m_coenBTDisconnStrategy) ||
         (e8BT_STRATEGY_BLOCK_SINGLE == m_coenBTDisconnStrategy));
}

//lint –restore

///////////////////////////////////////////////////////////////////////////////
// <EOF>
