/*!
 *******************************************************************************
 * \file             spi_tclDiPoAudio.h
 * \brief            Implements the Audio functionality for Digital IPOd Out using 
                     interface to Real VNC SDK through VNC Wrapper.
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Implementation for Digital IPOd Out
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 29.10.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
 04.04.2014 |  Shihabudheen P M(RBEI/ECP2)     | Added 1.bStartAudio()

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLDIPOAUDIO_H
#define SPI_TCLDIPOAUDIO_H

#include "BaseTypes.h"
#include "spi_tclAudioDevBase.h"

/**
 *  class definitions.
 */

typedef void (*fp)(void);


/**
 * This class implements the Audio functionality for Digital IPOd Out using 
    interface to Real VNC SDK through VNC Wrapper..
 */
class spi_tclDiPoAudio : public spi_tclAudioDevBase
{
public:
	 /***************************************************************************
     *********************************PUBLIC*************************************
     ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclDiPoAudio::spi_tclDiPoAudio();
    ***************************************************************************/
    /*!
    * \fn      spi_tclDiPoAudio()
    * \brief   Default Constructor
    **************************************************************************/
	spi_tclDiPoAudio();

	/***************************************************************************
    ** FUNCTION:  spi_tclDiPoAudio::~spi_tclDiPoAudio();
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclDiPoAudio()
    * \brief   Virtual Destructor
    **************************************************************************/
	virtual ~spi_tclDiPoAudio();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDiPoAudio::vInitAudio(t_U16 u16DeviceId,
	fp fpInitAudioResp)
    ***************************************************************************/
    /*!
    * \fn      vInitAudio(t_U16 u16DeviceId)
    * \brief   Method to handle actions specific to a selected device 
    * \param   u16DeviceId: [IN]Device ID
	* \param   fpInitAudioResp: [IN]Function pointer for init audio
    * \retval  NONE
    **************************************************************************/
	t_Void vInitAudio(t_U16 u16DeviceId,fp fpInitAudioResp);

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclDiPoAudio::vPrepareStartAudioStreaming(t_Char* pcDeviceName)
    ***************************************************************************/
    /*!
    * \fn      vPrepareStartAudioStreaming(t_Char* pcDeviceName)
    * \brief   Method that sets some parameters required for starting audio streaming 
	* \param   pcDeviceName: [IN]Device Name
    * \retval  NONE
    **************************************************************************/
	t_Void vPrepareStartAudioStreaming(t_Char* pcDeviceName);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDiPoAudio::vStartAudioStreaming(fp fpStartAudioStreamingResp)
    ***************************************************************************/
    /*!
    * \fn      vStartAudioStreaming()
    * \brief   Method that starts the audio streaming
	* \param   fpStartAudioStreamingResp: [IN]Function pointer for audio streaming
    * \retval  NONE
    **************************************************************************/
	t_Void vStartAudioStreaming(fp fpStartAudioStreamingResp);

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclDiPoAudio::vPrepareStopAudioStreaming(t_Char* pcDeviceName)
    ***************************************************************************/
    /*!
    * \fn      vPrepareStopAudioStreaming(t_Char* pcDeviceName)
    * \brief   Method that sets some parameters required for stopping audio streaming
	* \param   pcDeviceName: [IN]Device Name
    * \retval  NONE
    **************************************************************************/
	t_Void vPrepareStopAudioStreaming(t_Char* pcDeviceName);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDiPoAudio::vStopAudioStreaming()
    ***************************************************************************/
    /*!
    * \fn      vStopAudioStreaming()
    * \brief   Method that stops audio streaming
	* \param   NONE
    * \retval  NONE
    **************************************************************************/
	t_Void vStopAudioStreaming();
	
	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclDiPoAudio::vDeInitAudio(fp fpDeInitAudioResp)
    ***************************************************************************/
    /*!
    * \fn      vDeInitAudio()
    * \brief   Method that removes the audio links and destroys the audio router
	* \param   fpDeInitAudioResp : [IN]Function pointer for deinitialise audio
    * \retval  NONE
    **************************************************************************/
	t_Void vDeInitAudio(fp fpDeInitAudioResp);

  /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoAudio::bStartAudio(t_U32,tString,CbAudioResp)
   ***************************************************************************/
   /*!
   * \fn      bStartAudio(t_U32 u32DeviceId, tString szAudioDev,
   * \brief   Start Streaming of Audio from the CE Device to the Audio Output
	*          Device assigned by the Audio Manager for the Source.
	*          Mandatory Interface to be implemented.
	* \param   [u32DeviceId]: Unique Identifier for the Connected Device.
	*		     [szAudioDev]: ALSA Audio Device
   *          [enAudDir]    :Specify the Audio Direction(Alternate or Main Audio).
	* \retval  Bool value
    **************************************************************************/
	virtual t_Bool bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
              tenAudioDir enAudDir);

  /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoAudio::bStartAudio(t_U32,tString,t_String, 
   **				tenAudioDir)
   ***************************************************************************/
   /*!
   * \fn      bStartAudio(t_U32 u32DeviceId, tString szAudioDev,
   * \brief   Start Streaming of Audio from the CE Device to the Audio Output
	*          Device assigned by the Audio Manager for the Source.
	*          Mandatory Interface to be implemented.
	* \param  [u32DeviceId]: Unique Identifier for the Connected Device.
	*		  [szAudioDev]: ALSA Audio Out Device 
   *          [szInputAudioDev]: Audio In Device name
   *		  [enAudDir]    :Specify the Audio Direction(Phone or VR Audio).
	* \retval  Bool value
    **************************************************************************/
   virtual t_Bool bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
              t_String szInputAudioDev, tenAudioDir enAudDir);

  /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoAudio::bStopAudio(t_U32, CbAudioResp)
   ***************************************************************************/
   /*!
   * \fn      vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
   * \brief   Stop Streaming of Audio from the CE Device to the Audio Output
	*          Device assigned by the Audio Manager for the Source.
	*          Mandatory Interface to be implemented.
	* \param   [u32DeviceId]: Unique Identifier for the Connected Device.
   *          [enAudDir]  :Specify the Audio Direction.
   * \retval  None
   **************************************************************************/
   virtual t_Void vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir);

  /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoAudio::bSelectAudioDevice(t_U32,
   ***************************************************************************/
   /*!
   * \fn      bSelectAudioDevice(t_U32 u32DeviceId CbAudioResp cbSelectDevResp)
   * \brief   Perfom necessary actions specific to a device selection like
	*          obtaining audio capabilities of device, supported modes etc
	* \param   [u32DeviceId]: Unique Identifier for the Connected Device.
	*		   [cbSelectDevResp]: Callback function provided to notify response
	*          for Select Device.
	* \retval  Bool value
    **************************************************************************/
	virtual t_Bool bSelectAudioDevice(t_U32 u32DeviceId);

  /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDiPoAudio::bIsAudioLinkSupported(t_U32,
	*                                            tenAudioLink)
   ***************************************************************************/
   /*!
   * \fn      bIsAudioLinkSupported(t_U32 u32DeviceId)
   * \brief   Perfom necessary actions specific to a device on deselection.
	*          Optional Interface for implementation.
	* \param   [u32DeviceId]: Unique Identifier for the Connected Device.
	*          [enLink]: Specify the Audio Link Type for which Capability is
	*          requested. Mandatory interface to be implemented.
	* \retval  Bool value, TRUE if Supported, FALSE otherwise
    **************************************************************************/
	virtual t_Bool bIsAudioLinkSupported(t_U32 u32DeviceId, tenAudioLink enLink);

  /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclDiPoAudio::vDeselectAudioDevice()
   ***************************************************************************/
   /*!
   * \fn      vDeselectAudioDevice(t_U32 u32DeviceId)
   * \brief   Perfom necessary actions specific to a device on deselection.
	*          Optional Interface for implementation.
	* \param   [u32DeviceId]: Unique Identifier for the Connected Device.
	* \retval  Bool value
    **************************************************************************/
	virtual t_Void vDeselectAudioDevice(t_U32 u32DeviceId);


  /***************************************************************************
    ** FUNCTION:  spi_tclDiPoAudio::vRegisterCallbacks
    ***************************************************************************/
   /*!
    * \fn     vRegisterCallbacks()
    * \brief  interface for the creator class to register for the required
    *        callbacks.
    * \param rfrAudCallbacks : reference to the callback structure
    *        populated by the caller
    **************************************************************************/
   t_Void vRegisterCallbacks(trAudioCallbacks &rfrAudCallbacks);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclDiPoAudio::bSetAudioBlockingMode()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bSetAudioBlockingMode(const t_U32 cou32DevId,
   *         const tenBlockingMode coenBlockingMode)
   * \brief  Interface to set the audio blocking mode.
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenBlockingMode : [IN] Identifies the Blocking Mode.
   * \retval t_Bool
   **************************************************************************/
   t_Bool bSetAudioBlockingMode(const t_U32 cou32DevId,
      const tenBlockingMode coenBlockingMode);

   /***************************************************************************
    ** FUNCTION: t_Bool spi_tclDiPoAudio::vOnAudioError()
    ***************************************************************************/
   /*!
    * \fn    t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError)
    * \brief  Interface to set the audio error.
    * \param  enAudDir       : [IN] Uniquely identifies the target Device.
    * \param  enAudioError : [IN] Audio Error
    **************************************************************************/
   t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError);

private:

	/***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/
   //! Structure object for Function pointers . 
   //! This will be used by Audio Manager to register for response callbacks from ML and DiPo Audio
   trAudioCallbacks m_rAudCallbacks;
};
#endif // SPI_TCLDIPOAUDIO_H
