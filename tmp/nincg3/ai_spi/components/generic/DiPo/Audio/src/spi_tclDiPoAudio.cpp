/*!
*******************************************************************************
* \file             spi_tclDiPoAudio.h
* \brief            Implements the Audio functionality for Digital IPOd Out using 
interface to Real VNC SDK through VNC Wrapper.
*******************************************************************************
\verbatim
PROJECT:        Gen3 Projects
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Audio Implementation for Digital IPOd Out
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                          | Modifications
29.10.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
04.04.2014 |  Shihabudheen P M(RBEI/ECP2)     | Added bStartAudio()
26.05.2015 |  Tejaswini H B(RBEI/ECP2)        | Added Lint comments to suppress C++11 Errors
17.07.2015 |  Sameer Chandra                   | Memory leak fix




\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclDiPoAudio.cpp.trc.h"
#endif

#include "DiPOTypes.h"
#include "IPCMessageQueue.h"
#include "spi_tclDiPoAudio.h"
#include "spi_tclAudioSettings.h"


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	


/***************************************************************************
** FUNCTION:  spi_tclDiPoAudio::spi_tclDiPoAudio()
***************************************************************************/
spi_tclDiPoAudio::spi_tclDiPoAudio():spi_tclAudioDevBase()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   // Initializations to be done which are not specific to device connected/selected
}//spi_tclDiPoAudio::spi_tclDiPoAudio()


/***************************************************************************
** FUNCTION:  spi_tclDiPoAudio::~spi_tclDiPoAudio()
***************************************************************************/
spi_tclDiPoAudio::~spi_tclDiPoAudio()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}//spi_tclDiPoAudio::~spi_tclDiPoAudio()

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoAudio::vInitAudio(t_U16 u16DeviceId,fp fpInitAudioResp)
***************************************************************************/
t_Void spi_tclDiPoAudio::vInitAudio(t_U16 u16DeviceId,fp fpInitAudioResp)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(u16DeviceId);
   SPI_INTENTIONALLY_UNUSED(fpInitAudioResp);

   //Invoked on SelectDevice Method as defined in SPI API Document.
   //Method to handle actions specific to a selected device like

}//void spi_tclDiPoAudio::vInitAudio(/*DeviceName*/)

/***************************************************************************
** FUNCTION: t_Void spi_tclDiPoAudio::vPrepareStartAudioStreaming(t_Char* pcDeviceName)
***************************************************************************/
t_Void spi_tclDiPoAudio::vPrepareStartAudioStreaming(t_Char* pcDeviceName)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(pcDeviceName);
}//void spi_tclDiPoAudio::vPrepareStartAudioStreaming(t_Char* pcDeviceName)

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::vStartAudioStreaming(
fp fpStartAudioStreamingResp)
***************************************************************************/
t_Void spi_tclDiPoAudio::vStartAudioStreaming(fp fpStartAudioStreamingResp)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(fpStartAudioStreamingResp);
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::vPrepareStartAudioStreaming
(t_Char* pcDeviceName)
***************************************************************************/
t_Void spi_tclDiPoAudio::vPrepareStopAudioStreaming(t_Char* pcDeviceName)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(pcDeviceName);

}//void spi_tclDiPoAudio::vPrepareStopAudioStreaming

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::vStopAudioStreaming()
***************************************************************************/
t_Void spi_tclDiPoAudio::vStopAudioStreaming()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

}//spi_tclDiPoAudio::vStopAudioStreaming()


/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::vDeInitAudio()
***************************************************************************/
t_Void spi_tclDiPoAudio::vDeInitAudio(fp fpDeInitAudioResp)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(fpDeInitAudioResp);

}//spi_tclDiPoAudio::vDeInitAudio()

t_Void spi_tclDiPoAudio::vRegisterCallbacks(trAudioCallbacks &rfrAudCallbacks)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_rAudCallbacks = rfrAudCallbacks;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::bStartAudio()
***************************************************************************/
t_Bool spi_tclDiPoAudio::bStartAudio(t_U32 u32DeviceId, t_String szAudioDev,
                                     tenAudioDir enAudDir)
{

	/*lint -esym(40,fvStartAudioResp)fvStartAudioResp Undeclared identifier */

   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   t_Bool bRetVal = false;

   if (e8AUD_DUCK != enAudDir)
   {
      IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
      trAudioMsg *poAudioMsg = (trAudioMsg *)oMessageQueue.vpCreateBuffer(sizeof(trAudioMsg));

      if(NULL != poAudioMsg)
      {
         poAudioMsg->enMsgType = e8AUDIO_MESSAGE;
         poAudioMsg->enAudioStreamType = (e8AUD_MAIN_OUT == enAudDir || e8AUD_ALERT_OUT == enAudDir)? e8MAIN_AUDIO : e8ALTERNATE_AUDIO;
         ETG_TRACE_USR4(("poAudioMsg->enAudioStreamType: %d", (t_U8)poAudioMsg->enAudioStreamType));
         strncpy(poAudioMsg->szALSADeviceName, szAudioDev.c_str(), MAX_STR_LEN);
         ETG_TRACE_USR4(("poAudioMsg->szALSADeviceName: %s", poAudioMsg->szALSADeviceName));
         bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poAudioMsg, DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
         ETG_TRACE_USR4(("IPC Message send status from bStartAudio: %d", bRetVal));
         //Destroy the buffer
         oMessageQueue.vDropBuffer(poAudioMsg);
      }//End of if(NULL != poAudioMsg)
   }
   else
   {
      bRetVal = true;
   }

   if(NULL != m_rAudCallbacks.fvStartAudioResp)
   {
      (m_rAudCallbacks.fvStartAudioResp)(enAudDir, bRetVal);
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::bStartAudio()
***************************************************************************/
t_Bool spi_tclDiPoAudio::bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
                                     t_String szInputAudioDev, tenAudioDir enAudDir)
{

   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   t_Bool bRetVal = false;

   IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
   trAudioInMsg *poAudioInMsg =  (trAudioInMsg *)oMessageQueue.vpCreateBuffer(sizeof(trAudioInMsg));
   
   if(NULL != poAudioInMsg)
   {
      poAudioInMsg->enMsgType = e8AUDIO_IN_MESSAGE;
      strncpy(poAudioInMsg->szAudioOutDevice, szOutputAudioDev.c_str(), MAX_STR_LEN);
      ETG_TRACE_USR4(("poAudioInMsg->szAudioOutDevice: %s", poAudioInMsg->szAudioOutDevice));
      strncpy(poAudioInMsg->szAudioInDevice, szInputAudioDev.c_str(), MAX_STR_LEN);
      ETG_TRACE_USR4(("poAudioInMsg->szAudioInDevice: %s", poAudioInMsg->szAudioInDevice));
      // Send the IPC message
      bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poAudioInMsg, DIPO_MESSAGE_PRIORITY,(eMessageType) TCL_DATA_MESSAGE));
      ETG_TRACE_USR1(("IPC Message send status from bStartAudio: %d", bRetVal)); 
      //Destroy the buffer
      oMessageQueue.vDropBuffer(poAudioInMsg);
   }

   if(NULL != m_rAudCallbacks.fvStartAudioResp)
   {
      (m_rAudCallbacks.fvStartAudioResp)(enAudDir, bRetVal);
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::bStopAudio()
***************************************************************************/
t_Void spi_tclDiPoAudio::vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
{

	/*lint -esym(40,fvStopAudioResp)fvStopAudioResp Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);

   t_Bool bRetVal = false;

   if (e8AUD_MAIN_OUT == enAudDir)
   {
      IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
      trAudioStopMsg *poAudioStopMsg = (trAudioStopMsg *) oMessageQueue.vpCreateBuffer(sizeof(trAudioStopMsg));

      if (NULL != poAudioStopMsg)
      {
         poAudioStopMsg->enMsgType = e8AUDIO_STOP_MESSAGE;
         poAudioStopMsg->enAudioReqType = e8DEALLOCATION_REQUEST;
         ETG_TRACE_USR4(("poAudioStopMsg->enAudioReqType: %d", poAudioStopMsg->enAudioReqType));
         // Send the IPC message
         bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*) poAudioStopMsg, DIPO_MESSAGE_PRIORITY,
               (eMessageType) TCL_DATA_MESSAGE));
         ETG_TRACE_USR1(("IPC Message send status from bStopAudio: %d", bRetVal));
         //Destroy the buffer
         oMessageQueue.vDropBuffer(poAudioStopMsg);

      }
   }
   else
   {
      bRetVal = true;
   }

   if(NULL != m_rAudCallbacks.fvStopAudioResp)
   {
      (m_rAudCallbacks.fvStopAudioResp)(enAudDir, bRetVal);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::bSelectAudioDevice()
***************************************************************************/
t_Bool spi_tclDiPoAudio::bSelectAudioDevice(t_U32 u32DeviceId)
{
	/*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != (m_rAudCallbacks.fvSelectDeviceResp))
   {
      (m_rAudCallbacks.fvSelectDeviceResp)(u32DeviceId, e8DEVCONNREQ_SELECT, true);
   }
   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::vDeselectAudioDevice()
***************************************************************************/
t_Void spi_tclDiPoAudio::vDeselectAudioDevice(t_U32 u32DeviceId)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != (m_rAudCallbacks.fvSelectDeviceResp))
   {
      (m_rAudCallbacks.fvSelectDeviceResp)(u32DeviceId, e8DEVCONNREQ_DESELECT, true);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDiPoAudio::bIsAudioLinkSupported()
***************************************************************************/
t_Bool spi_tclDiPoAudio::bIsAudioLinkSupported(t_U32 u32DeviceId, tenAudioLink enLink)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   SPI_INTENTIONALLY_UNUSED(enLink);
   return true;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclDiPoAudio::bSetAudioBlockingMode()
***************************************************************************/
t_Bool spi_tclDiPoAudio::bSetAudioBlockingMode(const t_U32 cou32DevId,
                                               const tenBlockingMode coenBlockingMode)
{
   //always return an error, if the audio blocking request comes for DiPo device.
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   SPI_INTENTIONALLY_UNUSED(coenBlockingMode);
   return false;
}
/***************************************************************************
** FUNCTION: t_Bool spi_tclDiPoAudio::vOnAudioError()
***************************************************************************/
t_Void spi_tclDiPoAudio::vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError)
{
   ETG_TRACE_USR1((" spi_tclDiPoAudio::vOnAudioError: enAudDir = %d enAudioError =%d \n",
         ETG_ENUM(AUDIO_DIRECTION,enAudDir), ETG_ENUM(AUDIO_ERROR,enAudioError)));
   switch(enAudioError)
   {
      case e8_AUDIOERROR_AVACTIVATION:
      case e8_AUDIOERROR_ALLOCATE:
      case e8_AUDIOERROR_STARTSOURCEACT:
      {
         IPCMessageQueue oMessageQueue(SPI_DIPO_MSGQ_IPC_THREAD);
         trAudioErrorMsg *poAudioErrMsg = (trAudioErrorMsg *)oMessageQueue.vpCreateBuffer(sizeof(trAudioErrorMsg));

         if(NULL != poAudioErrMsg)
         {
            poAudioErrMsg->enMsgType = e8AUDIO_ERROR_MESSAGE;
            poAudioErrMsg->enAudioStreamType = (e8AUD_MAIN_OUT == enAudDir || e8AUD_ALERT_OUT == enAudDir)? e8MAIN_AUDIO : e8ALTERNATE_AUDIO;
            strncpy(poAudioErrMsg->szALSADummyDeviceName, "AdevSpiDummy", MAX_STR_LEN);
            ETG_TRACE_USR4(("poAudioErrMsg->enAudioStreamType: %d", (t_U8)poAudioErrMsg->enAudioStreamType));
            ETG_TRACE_USR4(("poAudioErrMsg->szALSADummyDeviceName: %s", poAudioErrMsg->szALSADummyDeviceName));
            t_Bool bRetVal = (0 <= oMessageQueue.iSendBuffer((t_Void*)poAudioErrMsg, DIPO_MESSAGE_PRIORITY,(eMessageType)TCL_DATA_MESSAGE));
            ETG_TRACE_USR4(("IPC Message send status from vOnAudioError: %d", bRetVal));
            //Destroy the buffer
            oMessageQueue.vDropBuffer(poAudioErrMsg);
         }//End of if(NULL != poAudioErrMsg)
         break;
      }
      default:
      {
         ETG_TRACE_USR4(("Audio Error Not Handled\n"));
      }
   }
}

//lint –restore

