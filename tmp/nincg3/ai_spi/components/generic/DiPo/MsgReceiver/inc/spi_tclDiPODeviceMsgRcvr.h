/*!
*******************************************************************************
* \file              spi_tclDiPODeviceMsgRcvr.h
* \brief             DiPO Msg handler for Resource Management
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO IPC message handler for resource management
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
22.03.2014 |  Shihabudheen P M            | Initial Version
05.04.2014 |  Priju K Padiyath            | Updating mesage receive functionality
23.06.2014 |  Shihabudheen P M            | Adapted to the latest CarPlay design
21.07.2014 |  Shihabudheen P M            | Added 1. vInitAudioMap() 
08.10.2014 |  Ramya Murthy                | Implementation for DisableBluetooth msg.

\endverbatim
******************************************************************************/
#ifndef SPI_TCLDIPOMSGRCVR_H
#define SPI_TCLDIPOMSGRCVR_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/

#include "DiPOTypes.h"
#include "MsgQIPCThreader.h"
#include "MsgQThreadable.h"
#include "spi_tclResorceMngrDefines.h"
using namespace shl::thread;

class spi_tclResourceMngrResp;
class spi_tclResourceMngr;
/****************************************************************************/
/*!
* \class spi_tclDiPODeviceMsgRcvr
* \brief DiPO IPC message handler for resource management
*
* spi_tclDiPORMMsgHandler is used to recieve the IPC message from the DiPOAdapter
* plugin named IControlAdapter, about the context change information from the 
* device and to trigger the resource management accordingly
****************************************************************************/
class spi_tclDiPODeviceMsgRcvr: public MsgQThreadable
{
public:
   /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPORMMsgHandler::spi_tclDiPORMMsgHandler()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDiPODeviceMsgRcvr()
   * \brief  Constructor
   * \sa     ~spi_tclDiPODeviceMsgRcvr()
   **************************************************************************/
   spi_tclDiPODeviceMsgRcvr(spi_tclResourceMngrResp *poRespIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::~spi_tclDiPODeviceMsgRcvr()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDiPODeviceMsgRcvr()
   * \brief  Destructor
   * \sa     spi_tclDiPODeviceMsgRcvr()
   **************************************************************************/
   virtual ~spi_tclDiPODeviceMsgRcvr();

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::vExecute
   ***************************************************************************/
   /*!
   * \fn      t_Void vExecute(tShlMessage *poMessage)
   * \brief   Responsible for posting the message to respective dispatchers
   * \param   poMessage : message received from MsgQ for dispatching
   **************************************************************************/
   virtual t_Void vExecute(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::tShlMessage* poGetMsgBuffer(size_t )
   ***************************************************************************/
   /*!
   * \fn      tShlMessage* poGetMsgBuffer(size_t )
   * \brief  This function will be called for requesting the storage allocation for received
   *           message
   * \param siBuffer: size of the buffer to be allocated for the received message
   **************************************************************************/
   virtual tShlMessage* poGetMsgBuffer(size_t siBuffer);

   /***************************************************************************
   *********************************END OF PUBLIC*****************************
   ***************************************************************************/
private:
   /***************************************************************************
   ******************************PRIVATE**************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::bHandleModeChangeMsg
   ***************************************************************************/
   /*!
   * \fn      bHandleModeChangeMsg()
   * \brief  This function is used to distribute the  mode change message from CarPlay plugin
   * \param poMessage: [IN] Pointer to the message data
   **************************************************************************/
   t_Bool bHandleModeChangeMsg(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::bHandleRequestUIMsg
   ***************************************************************************/
   /*!
   * \fn     bHandleRequestUIMsg()
   * \brief  This function is used to distribute the OnRequestUI msg from the device
   * \param poMessage: [IN] Pointer to the message data
   **************************************************************************/
   t_Bool bHandleRequestUIMsg(tShlMessage *poMessage);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::bHandleSessionMsg
   ***************************************************************************/
   /*!
   * \fn     bHandleSessionMsg()
   * \brief  This function is used to distribute the Sessionmsg from the device
   * \param poMessage: [IN] Pointer to the message data
   **************************************************************************/
   t_Bool bHandleSessionMsg(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::bHandleAudioMsg
   ***************************************************************************/
   /*!
   * \fn     bHandleAudioMsg()
   * \brief  This function is used to distribute the audio allocate/deallocate 
   * \       request from the CarPlay device
   * \param poMessage: [IN] Pointer to the message data
   **************************************************************************/
   t_Bool bHandleAudioMsg(tShlMessage *poMessage);

  /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::bHandleAudioDuckMsg
   ***************************************************************************/
   /*!
   * \fn     bHandleAudioDuckMsg()
   * \brief  This function is used to distribute the audio duck request from  
   * \       CarPlay device to the audio manager
   * \param poMessage: [IN] Pointer to the message data
   **************************************************************************/
   t_Bool bHandleAudioDuckMsg(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION:  spi_tclDiPODeviceMsgRcvr::bHandleDisableBluetoothMsg
   ***************************************************************************/
   /*!
   * \fn     bHandleDisableBluetoothMsg()
   * \brief  This function is used to distribute the DisableBluetooth msg from the device
   * \param poMessage: [IN] Pointer to the message data
   **************************************************************************/
   t_Bool bHandleDisableBluetoothMsg(tShlMessage *poMessage);

   /***************************************************************************
   ** FUNCTION: t_String spi_tclDiPODeviceMsgRcvr::szGetBTAddress(const t_String...)
   ***************************************************************************/
   /*!
   * \fn     szGetBTAddress(const t_String& rfcoszBTMACAddress)
   * \brief  Converts a BT MAC address string to BT Address formatted string
   * \param  rfcoszBTMACAddress: [IN] BT MAC address
   **************************************************************************/
   t_String szGetBTAddress(const t_String& rfcoszBTMACAddress) const;

   //! IPC message queue threader handler
   MsgQIPCThreader *m_poMsgQIPCThreader;

   //! Response interface
   spi_tclResourceMngrResp *m_poRespIntf;

   //! Audio direction
   tenAudioDir m_enAudioDir;

   //! Map to keep the Audio sampling rate info
   std::map<t_U16, tenAudioSamplingRate> m_mapAudioSamplingRate;

   /***************************************************************************
   **************************END OF PRIVATE***********************************
   ***************************************************************************/
};


#endif
