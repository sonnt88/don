/*!
*******************************************************************************
* \file              spi_tclDiPODeviceMsgRcvr.cpp
* \brief             DiPO Msg handler for RM
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO IPC message handler for resource management
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
22.03.2014 |  Shihabudheen P M            | Initial Version
05.04.2014 |  Priju K Padiyath            | Updating mesage receive functionality
17.04.2014 |  Shihabudheen P M            | Update the message handling and triggering
                                            as per the resource manager design.
23.06.2014 |  Shihabudheen P M            | Adapted to the latest CarPlay design
21.07.2014 |  Shihabudheen P M            | Added vInitAudioMap() 
                                            Modified bHandleAudioMsg() for handling sampling rates 
08.10.2014 |  Ramya Murthy                | Implementation for DisableBluetooth msg.
28.11.2014 | Hari Priya E R               | Added a separate trace to measure audio source switching timing
06.05.2015 |Tejaswini HB                  | Lint Fix
17.07.2015 | Sameer Chandra               | Memory leak fix
                                    
\endverbatim
******************************************************************************/
#include <algorithm>

#include "DiPOTypes.h"
#include "spi_tclFactory.h"
#include "spi_tclAudio.h"
#include "spi_tclResourceMngrResp.h"
#include "spi_tclResourceMngr.h"
#include "spi_tclDeviceSelector.h"
#include "spi_tclBluetooth.h"
#include "spi_tclDiPODeviceMsgRcvr.h"
#include "spi_tclConfigReader.h"


#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
#include "trcGenProj/Header/spi_tclDiPODeviceMsgRcvr.cpp.trc.h"
#endif


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
//typedef std::map<t_U16, tenAudioSamplingRate>::iterator tAudSampleRateMapItr;   //Commented to suppress Lint Warning

static const t_U8 scu8TraceArraySize = 10;

/***************************************************************************
 ** FUNCTION: spi_tclDiPODeviceMsgRcvr::spi_tclDiPODeviceMsgRcvr()
 ***************************************************************************/
spi_tclDiPODeviceMsgRcvr::spi_tclDiPODeviceMsgRcvr(spi_tclResourceMngrResp *poRespIntf)
   :m_poMsgQIPCThreader(NULL), m_poRespIntf(poRespIntf), m_enAudioDir(e8AUD_INVALID)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_mapAudioSamplingRate[0]     = e8AUD_SAMPLERATE_DEFAULT;
   m_mapAudioSamplingRate[8000]  = e8AUD_SAMPLERATE_8KHZ;
   m_mapAudioSamplingRate[16000] = e8AUD_SAMPLERATE_16KHZ;
   m_mapAudioSamplingRate[24000] = e8AUD_SAMPLERATE_24KHZ;
   m_poMsgQIPCThreader = new MsgQIPCThreader(DIPO_SPI_MSGQ_IPC_THREAD, this, true);
}

/***************************************************************************
 ** FUNCTION: spi_tclDiPODeviceMsgRcvr::~spi_tclDiPODeviceMsgRcvr()
 ***************************************************************************/
spi_tclDiPODeviceMsgRcvr::~spi_tclDiPODeviceMsgRcvr()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   m_mapAudioSamplingRate.clear();
   m_poRespIntf = NULL; // Lint fix.

   if(NULL != m_poMsgQIPCThreader)
   {
	   //Terminate the IPC MsgQ thread
	   //Get the message Queue
       IPCMessageQueue* poMessageQueue = m_poMsgQIPCThreader->poGetMessageQueu();

       //Create Buffer
       trMsgQBase * poBuffer = (trMsgQBase*)poMessageQueue->vpCreateBuffer(sizeof(trMsgQBase));
       memset(poBuffer, 0, sizeof(trMsgQBase));
       //Send the Terminate message
       poMessageQueue->iSendBuffer((void*)poBuffer,DIPO_MESSAGE_PRIORITY,TCL_THREAD_TERMINATE_MESSAGE);
       //Destroy the buffer
       poMessageQueue->vDropBuffer(poBuffer);

       //Wait for the message queue thread to join.
       Threader::vWaitForTermination(m_poMsgQIPCThreader->pGetThreadID());

       //Once the thread is terminated delete the memory allocated
       delete m_poMsgQIPCThreader;
   } //if(NULL != m_poMsgQIPCThreader)
}


/***************************************************************************
 ** FUNCTION: t_Void spi_tclDiPODeviceMsgRcvr::vExecute()
 ***************************************************************************/
t_Void spi_tclDiPODeviceMsgRcvr::vExecute(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   trMsgQBase *poBaseMsg =  NULL;
   t_Bool bStatus = false;
  // spi_tclAudio *poAudio = spi_tclFactory::getInstance()->poGetAudioInstance();
   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      poBaseMsg = (trMsgQBase*)poMessage->pvBuffer;
      switch((t_U8)poBaseMsg->enMsgType)
      {
      case e8RM_RESP_MESSAGE:
         {
            ETG_TRACE_USR1(("ModeChanged message received from [CarPlay] "));
            bStatus = bHandleModeChangeMsg(poMessage);
         }
         break;
      case e8AUDIO_ALLOC_MESSAGE:
         {
            ETG_TRACE_USR1(("AudioAllocation message received from [CarPlay] "));
            bStatus = bHandleAudioMsg(poMessage);
         }
         break;
      case e8ON_REQUEST_UI_MESSAGE:
         {
            ETG_TRACE_USR1(("RequestUI message received from [CarPlay] "));
            bStatus = bHandleRequestUIMsg(poMessage);
         }
         break;
	  case e8AUDIO_DUCK_MESSAGE:
         {
            ETG_TRACE_USR1(("Audio Duck message received from [CarPlay] "));
            bStatus = bHandleAudioDuckMsg(poMessage);
         }
         break;
     case e8SESSION_MESSAGE:
        {
           ETG_TRACE_USR1(("Session message received from [CarPlay]"));
           bStatus = bHandleSessionMsg(poMessage);
        }
        break;
     case e8DISABLE_BLUETOOTH_MESSAGE:
        {
           ETG_TRACE_USR1(("DisableBluetooth message received from [CarPlay]"));
           bStatus = bHandleDisableBluetoothMsg(poMessage);
        }
        break;
      default:
         {
            ETG_TRACE_USR1(("Invalid message "));
         }
      } // switch((t_U8)poBaseMsg->enMsgType)
      // Release the allocated memory
      t_Char* pczBuffer = static_cast<t_Char*> (poMessage->pvBuffer);
      RELEASE_ARRAY_MEM(pczBuffer);
   }// if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   RELEASE_MEM(poMessage); // Move out if condition to make Lint happy, NULL check is part of RELEASE_MEM.
   ETG_TRACE_USR1(("Message recieved from CarPlay Adapter plugin processesd with status: %d ", bStatus));
}

/***************************************************************************
 ** FUNCTION: tShlMessage spi_tclDiPODeviceMsgRcvr::poGetMsgBuffer()
 ***************************************************************************/
tShlMessage* spi_tclDiPODeviceMsgRcvr::poGetMsgBuffer(size_t siBuffer)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   tShlMessage* poMessage = new tShlMessage;

   if (NULL != poMessage)
   {
      if(0 < siBuffer)
      {
         //! Allocate the requested memory
         poMessage->pvBuffer = new t_Char[siBuffer];
      }
      else
      {
         poMessage->pvBuffer = NULL;
      } // if(0 < siBuffer)

      if (NULL != poMessage->pvBuffer)
      {
         poMessage->size = siBuffer;
      }
      else
      {
         //! Free the message as internal allocation failed.
         delete poMessage;
         poMessage = NULL;
      } //   if (NULL != poMessage->pvBuffer)
   } // if (NULL != poMessage)

   return poMessage;
}


/****Private methods******/
/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPODeviceMsgRcvr::bHandleModeChangeMsg()
 ***************************************************************************/
t_Bool spi_tclDiPODeviceMsgRcvr::bHandleModeChangeMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal= false;
   trDiPORMMsgResp *poRmMsgResp = NULL;
   poRmMsgResp = static_cast<trDiPORMMsgResp*>(poMessage->pvBuffer);


   //Get the selected device handle from device selector
   t_U32 u32DevId = spi_tclDeviceSelector::u32GetSelectedDeviceId();

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if(NULL != poSPIFactory)
   {
      spi_tclResourceMngr* poResourceMngr = poSPIFactory->poGetRsrcMngrInstance();
      if(NULL != poResourceMngr)
      {
         poResourceMngr->vOnModeChanged(u32DevId, poRmMsgResp->rDiPOModeState);
         bRetVal = true;
      } //if(NULL != m_poResourceMngr)
   }//if(NULL != poSPIFactory)
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPODeviceMsgRcvr::bHandleRequestUIMsg()
 ***************************************************************************/
t_Bool spi_tclDiPODeviceMsgRcvr::bHandleRequestUIMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = false;
   trOnRequestUIMsg *poOnRequestUIMsg = NULL;
   poOnRequestUIMsg = static_cast<trOnRequestUIMsg*>(poMessage->pvBuffer);
   
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if(NULL != poSPIFactory)
   {
      spi_tclResourceMngr* poResourceMngr = poSPIFactory->poGetRsrcMngrInstance();
      if(NULL != poResourceMngr)
      {
         poResourceMngr->vOnRequestUI((t_Bool)poOnRequestUIMsg->bRequestUIStatus);
         bRetVal = true;
      } //if(NULL != m_poResourceMngr)
   }//if(NULL != poSPIFactory)
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPODeviceMsgRcvr::bHandleSessionMsg()
 ***************************************************************************/
t_Bool spi_tclDiPODeviceMsgRcvr::bHandleSessionMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = false;
   trDiPOSessionMsg *poDiPOSessionMsg;
   poDiPOSessionMsg = static_cast<trDiPOSessionMsg*>(poMessage->pvBuffer);

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if(NULL != poSPIFactory)
   {
      spi_tclResourceMngr* poResourceMngr = poSPIFactory->poGetRsrcMngrInstance();
      if(NULL != poResourceMngr)
      {
         //Add code to get data from bluetooth component
         poResourceMngr->vOnSessionMsg((tenDiPOSessionState)poDiPOSessionMsg->enDiPOSessionState);
         bRetVal = true;
      } //if(NULL != m_poResourceMngr)
   }//if(NULL != poSPIFactory)
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPODeviceMsgRcvr::bHandleAudioMsg()
 ***************************************************************************/
t_Bool spi_tclDiPODeviceMsgRcvr::bHandleAudioMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   //tenAudioDir enAudioDir = e8AUD_INVALID;
   trAudioAllocMsg *poAudioAllocMsg = NULL;
   t_Bool bRetVal = false;
   poAudioAllocMsg =  static_cast<trAudioAllocMsg*>(poMessage->pvBuffer);

   //Get the selected device handle from device selector
   t_U32 u32DevId = spi_tclDeviceSelector::u32GetSelectedDeviceId();

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if ((NULL != poSPIFactory) && (NULL != poAudioAllocMsg))
   {
      /* Separate binary trace added,which will be manually decoded for every allocation/deallocation request.
      This trace will be used along with a trace in Audio component to determine the Source Switch timing */
      tU8 u8Data[scu8TraceArraySize] = {
         enModule_SPI,
         enFile_spi_tclDiPODeviceMsgRcvr,
         enFkt_bHandleAudioMsg,(poAudioAllocMsg->enAudioChannelType),
         (poAudioAllocMsg->enAudioType),(poAudioAllocMsg->enAudioReqType),0,0,0,0};
         tU32 u32Line = __LINE__;
         u8Data[6] = (tU8)(((u32Line+6) & 0xFF000000)>>24);
         u8Data[7] = (tU8)(((u32Line+6) & 0x00FF0000)>>16);
         u8Data[8] = (tU8)(((u32Line+6) & 0x0000FF00)>>8);
         u8Data[9] = (tU8)((u32Line+6) & 0x000000FF);
         et_vTraceBinary(TR_CLASS_AUDIO_SRC_CHANGE_MEAS, TR_LEVEL_ERRORS, ET_EN_T8LIST, scu8TraceArraySize, u8Data, ET_EN_DONE);

         spi_tclResourceMngr* poResourceMngr =
            poSPIFactory->poGetRsrcMngrInstance();
         if (NULL != poResourceMngr)
         {
            poResourceMngr->vOnAudioMsg(u32DevId, *poAudioAllocMsg);
            bRetVal = true;
         } //if(NULL != m_poResourceMngr)
   }//if(NULL != poSPIFactory)

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPODeviceMsgRcvr::bHandleAudioMsg()
 ***************************************************************************/
t_Bool spi_tclDiPODeviceMsgRcvr::bHandleAudioDuckMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = false;
   trAudioDuckMsg *poAudioDuckMsg = NULL;
   poAudioDuckMsg =  static_cast<trAudioDuckMsg*>(poMessage->pvBuffer);

   //Get the selected device handle from device selector
   t_U32 u32DevId = spi_tclDeviceSelector::u32GetSelectedDeviceId();

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if ((NULL != poSPIFactory) && (NULL != poAudioDuckMsg))
   {
      spi_tclResourceMngr* poResourceMngr = poSPIFactory->poGetRsrcMngrInstance();
      if (NULL != poResourceMngr)
      {
         poResourceMngr->vOnAudioMsg(u32DevId, *poAudioDuckMsg);
         bRetVal = true;
      } //if(NULL != m_poResourceMngr)
   }//if(NULL != poSPIFactory)

   return bRetVal;
}


/***************************************************************************
 ** FUNCTION: t_Bool spi_tclDiPODeviceMsgRcvr::bHandleDisableBluetoothMsg()
 ***************************************************************************/
t_Bool spi_tclDiPODeviceMsgRcvr::bHandleDisableBluetoothMsg(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = false;

   if (NULL != poMessage)
   {
      trOnDisBluetoothMsg *poOnDisBluetoothMsg = static_cast<trOnDisBluetoothMsg*>(poMessage->pvBuffer);
      spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
      if ((NULL != poOnDisBluetoothMsg) && (NULL != poSPIFactory))
      {
         spi_tclBluetooth *poBluetooth = poSPIFactory->poGetBluetoothInstance();
         if (NULL != poBluetooth)
         {
            t_String szBTAddress = szGetBTAddress(poOnDisBluetoothMsg->cBluetoothID);
            poBluetooth->vOnDisableBluetooth(szBTAddress.c_str());
            bRetVal = true;
         } //if(NULL != poBluetooth)
      }//if ((NULL != poOnDisBluetoothMsg) && (NULL != poSPIFactory))
   }//if (NULL != poMessage)
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_String spi_tclDiPODeviceMsgRcvr::szGetBTAddress(const t_String...)
 ***************************************************************************/
t_String spi_tclDiPODeviceMsgRcvr::szGetBTAddress(const t_String& rfcoszBTMACAddress) const
{
   //! Initialize string with the BT MAC address
   t_String szBTAddress(rfcoszBTMACAddress);

   //! Remove all instances of ':' character from string and convert to uppercase.
   //! (Example: If szBTMACAddress is "28:e1:4c:df:30:72", format string as "28E14CDF3072"
   if (false == szBTAddress.empty())
   {
      szBTAddress.erase(std::remove(szBTAddress.begin(), szBTAddress.end(), ':'), szBTAddress.end());
      std::transform(szBTAddress.begin(), szBTAddress.end(), szBTAddress.begin(), ::toupper);
   }

   ETG_TRACE_USR4(("spi_tclDiPODeviceMsgRcvr::szGetBTAddress() left with %s ", szBTAddress.c_str()));
   return szBTAddress;
}
