/*!
*******************************************************************************
* \file              dipo_plugin_wrapper.h
* \brief             Wrapper for dipo_plugin.h.
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Wrapper for dipo_plugin.h.
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
9.12.2014  |  Sameer Chandra              | Initial Version

\endverbatim
******************************************************************************/
#ifndef SPI_TCLDIPOPLUGIN_WRAPPER_H
#define SPI_TCLDIPOPLUGIN_WRAPPER_H

 #ifdef GEN3X86
// Defining dummy data types
// in order to remove dependency
// with non X86 libraries.
namespace dipo {
	typedef enum
	{
		AudioChannelType_Main = 0,
		AudioChannelType_Alternate = 1
	} AudioChannelType;

	typedef enum
	{
		SiriAction_NA           = 0,
		SiriAction_Prewarm      = 1,
		SiriAction_ButtonDown   = 2,
		SiriAction_ButtonUp     = 3
	}SiriAction; 

}
 struct ModeState
 {};
 struct ModeChanges
 {};
 struct HIDDevice
 {};
 struct HIDInputReport
 {};


 #else
// Include the actual plugin 
//if it is a GEN3ARMMAKE build
 #include "dipo_plugin.h"
 #endif // GEN3X86

#endif //SPI_TCLDIPOPLUGIN_WRAPPER_H
