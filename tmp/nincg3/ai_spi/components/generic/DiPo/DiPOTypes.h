/*!
*******************************************************************************
* \file              DiPoTypes.h
* \brief             DiPO type definitions.
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    DiPO type definitions
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
20.1.2014  |  Shihabudheen P M            | Initial Version
05.2.2014  |  Shihabudheen P M            | Added trModeState, trModeChange
03.4.2014  |  Hari Priya E R              | Added defines for key handling
26.05.2014 | Shihabudheen P M			  | Modified for resource arbitration defines

\endverbatim
******************************************************************************/
#ifndef SPI_TCLDIPOTYPES_H
#define SPI_TCLDIPOTYPES_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/

#include "BaseTypes.h"
#include "SPITypes.h"
#include "DipoPluginWrapper.h"
using namespace dipo;

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

#define SPI_DIPO_MSGQ_IPC_THREAD "/SPIDiPOMsgQueue"
#define DIPO_SPI_MSGQ_IPC_THREAD "/SPIDiPORMMsgQueue"
#define DIPO_MESSAGE_PRIORITY 0
//Invalid Audio db value
#define MAX_STR_LEN      100

#define LOW_FIDELITY_TOUCH 0x02
#define HIGH_FIDELITY_TOUCH 0x04
#define LOW_FIDELITY_TOUCH_WITH_KNOB 0x08
#define HIGH_FIDELITY_TOUCH_WITH_KNOB 0x10

/* Types for media player client handler*/
//! Identify DiPO capability of the device
typedef enum
{
   //! Idenify unsupported device
   e8DIPO_UNSUPPORTED = 0x00,

   //! Identify supported device
   e8DIPO_SUPPORTED = 0x01
}tenDiPOCapability;

//! Identify DiPO session status
typedef enum
{
   //! Session is inactive
   e8DIPO_SESSION_INACTIVE = 0x00,

   //! Session active
   e8DIPO_SESSION_ACTIVE = 0x01
}tenDiPOSessionActive;

// Identify the DiPO device role status
typedef enum
{
   //! Identify the client mode
   e8DIPO_CLIENTMODE = 0x00,

   //! Idenify the host mode
   e8DIPO_HOSTMODE = 0x01

}tenDiPORoleStatus;

/*Types for ADIT interface*/

/*! 
 * \typedef struct ModeState trModeState
 * \brief Represent the DiPO mode state. 
 */
typedef struct ModeState trModeState;

/*! 
 * \typedef struct ModeChanges trModeChange
 * \brief Represent the DiPO ModeChange request parameterts. 
 */
typedef struct ModeChanges trModeChange;

/*! 
 * \typedef struct rVideoConfigData
 * \brief DiPO video configuration data.
 */

//! Identify the IPC messages
typedef enum
{
   //! Identify an unrecognized message
   e8UNKNOWN_MESSAGE = 0,

   //! Identify the audio message
   e8AUDIO_MESSAGE = 1,

   //! Identify Audio in message
   e8AUDIO_IN_MESSAGE = 2,
   
   //! Identify Audio Active message
   e8AUDIO_ACTIVE_MESSAGE = 3,

   //! Identify the video messages
   e8VIDEO_CONFIG_MESSAGE = 4,
   
   //! Identify the touch messages
   e8TOUCH_MESSAGE = 5,

   //! Identify the RequestUI message
   e8REQUEST_UI_MESSAGE = 6,

   //! Identify the OnRequestUI message 
   e8ON_REQUEST_UI_MESSAGE = 7,

   //! Identify the Resource management message
   e8RM_RESP_MESSAGE = 8,

   //! Identify the Acc display context message
   e8ACC_DISPLAY_CONTEXT_MESSAGE = 9,

   //! Identify Audio allocation message from Device
   e8AUDIO_ALLOC_MESSAGE = 10,
   
   //! Identify Audio Duck-Unduck message from Device
   e8AUDIO_DUCK_MESSAGE = 11,

   //! Message to stop the audio out message. 
   e8AUDIO_STOP_MESSAGE = 12,
   
    //! Identify Night mode message
   e8NIGHT_MODE_MESSAGE = 13,
   
      //! Identify Key Message
   e8KEY_MESSAGE = 14,
   
      //! Identify the Resource management message
   e8RM_VIDEO_RQST_MESSAGE = 15,

   //! Identify the audio resource handling message
   e8RM_AUDIO_RQST_MESSAGE = 16,
   
      //! Identify the app state message
   e8RM_APP_STATE_MESSAGE = 17,

   //!Identify the CarPlay session message
   e8SESSION_MESSAGE = 18,
   
   //! Identify the carplay info message
   e8INFO_MESSAGE = 19,

   //!Message to inform the disable bluetooth request from device
   e8DISABLE_BLUETOOTH_MESSAGE = 20,

   //! Siri action message
   e8SIRIACTION_MESSAGE = 21,
   
   //! Message to transfer the vehilce mode(park, drive)
   e8VEHICLE_MODE_MESSAGE = 22,

   //! Sent  when an error is received for audio allocation
   e8AUDIO_ERROR_MESSAGE = 23,

   // BT address message to update the BT address on the fly.
   // SPI to CarPlay plugin
   e8BTADDRESS_UPDATE_MESSAGE = 24,

   // Restrictions message to update the Restrictions info on the fly.
   // SPI to CarPlay plugin
   e8RESTRICTIONS_UPDATE_MESSAGE = 25

   

}tenMsgTypes;

/*! 
 * \typedef struct trMsgQBase
 * \brief DiPO IPC message base
 */
struct trMsgQBase
{
   public:
      trMsgQBase():enMsgType(e8UNKNOWN_MESSAGE),u32DataSize(0)
      {
      }

      trMsgQBase(tenMsgTypes msgType):enMsgType(msgType)
      {
      }
      virtual ~trMsgQBase(){}

      virtual t_Void vAllocate() = 0;

      virtual t_Void vDeAllocate() = 0;

      //! Message type
      tenMsgTypes enMsgType;
      t_U32 u32DataSize;

};

/*! 
 * \typedef struct rMsgQBase
 * \brief DiPO IPC message base
 */
typedef enum
{
   //! Identify the main audio device
   e8MAIN_AUDIO = 0,

   //! Identify the alternate audio devie
   e8ALTERNATE_AUDIO = 1
}tenAudioStreamType;

/*! 
 * \typedef struct rAudioMsg
 * \brief DiPO audio message structure
 */
struct trAudioMsg: public trMsgQBase
{
   trAudioMsg():trMsgQBase(e8AUDIO_MESSAGE)
      {
         memset(szALSADeviceName, 0, MAX_STR_LEN);
      }

      virtual ~trAudioMsg() {}

      virtual t_Void vAllocate()
      {
      }

      virtual t_Void vDeAllocate()
      {
      }

      //! Audio Stream type
      tenAudioStreamType enAudioStreamType;

      //! ALSA device name
      t_Char szALSADeviceName[MAX_STR_LEN];
};

/*! 
 * \typedef struct rAudioErrorMsg
 * \brief DiPO audio message structure
 */
struct trAudioErrorMsg: public trMsgQBase
{
      trAudioErrorMsg():enAudioStreamType(e8ALTERNATE_AUDIO), 
         trMsgQBase(e8AUDIO_ERROR_MESSAGE)
      {
         memset(szALSADummyDeviceName, 0, MAX_STR_LEN);
      }

      virtual ~trAudioErrorMsg() {}

      virtual t_Void vAllocate()
      {
      }

      virtual t_Void vDeAllocate()
      {
      }

      tenAudioStreamType enAudioStreamType;
      //! ALSA device name
      t_Char szALSADummyDeviceName[MAX_STR_LEN];
};

/*!
 * \typedef struct rAudioInMsg
 * \brief DiPO audio Message for Audio IN
 */
struct trAudioInMsg: public trMsgQBase
{
   trAudioInMsg():trMsgQBase(e8AUDIO_IN_MESSAGE)
      {
         memset(szAudioOutDevice, 0, MAX_STR_LEN);
         memset(szAudioInDevice, 0, MAX_STR_LEN);
      }

      virtual ~trAudioInMsg() {}

      virtual t_Void vAllocate()
      {
      }

      virtual t_Void vDeAllocate()
      {
      }

      //! Audio Out device name
      t_Char szAudioOutDevice[MAX_STR_LEN];

      //! Audio In Device name
      t_Char szAudioInDevice[MAX_STR_LEN];

};

/*! 
 * \typedef struct trAudioSrcActiveMsg
 * \brief DiPO audio active message structure
 */
struct trAudioSrcActiveMsg: public trMsgQBase
{
   trAudioSrcActiveMsg(): trMsgQBase(e8AUDIO_ACTIVE_MESSAGE)
      {}

      virtual ~trAudioSrcActiveMsg() {}

      virtual t_Void vAllocate()
      {
      }

      virtual t_Void vDeAllocate()
      {
      }
};

/*! 
 * \struct trTouchMsg
 * \brief DiPO touch message
 */
struct trTouchMsg : public trMsgQBase
{
   trTouchMsg():trMsgQBase(e8TOUCH_MESSAGE)
      {
      }

      virtual ~trTouchMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! touch cordinate data
      trTouchCoordinates rTouchCoordinates;
};

/*! 
 * \struct trRequestUIMsg : public rMsgQBase
 * \brief DiPO message for RequestUI
 */
struct trRequestUIMsg : public trMsgQBase
{
   trRequestUIMsg():trMsgQBase(e8REQUEST_UI_MESSAGE)
      {
         memset(szAppUrl, 0, MAX_STR_LEN);
      }

      virtual ~trRequestUIMsg() {}

      virtual t_Void vAllocate()
      {
      }

      virtual t_Void vDeAllocate()
      {
      }

      //! URL of the application to launch
      t_Char szAppUrl[MAX_STR_LEN];
};


/*! 
 * \struct trAudioDuckMsg : public rMsgQBase
 * \brief DiPO message for Duck audio request from device(Response from ADIT)
 */
struct trAudioDuckMsg : public trMsgQBase
{
      trAudioDuckMsg():dFinalVolume(0), dDurationInMs(0),
         trMsgQBase(e8AUDIO_DUCK_MESSAGE)
      {
      }

      virtual ~trAudioDuckMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! Final volume after ducking the audio
      t_Double dFinalVolume;

      //! Duration in milli seconds
      t_Double dDurationInMs;
};


/*!
 * \struct trDiPONightModeMsg : public rMsgQBase
 * \brief DiPO message for Day Night Mode information to iPhone
 */
struct trDiPONightModeMsg : public trMsgQBase
{
   trDiPONightModeMsg(): trMsgQBase(e8NIGHT_MODE_MESSAGE)
      {
      }

      virtual ~trDiPONightModeMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! Represents Day or night mode (TRUE == Night ,FALSE == Day)
      t_Bool bIsNightMode;
};

/*!
* \struct trOnRequestUIMsg
* \brief OnRequestUI message from device(Carplayd response)
*/
struct trOnRequestUIMsg: public trMsgQBase
{
   trOnRequestUIMsg(): trMsgQBase(e8ON_REQUEST_UI_MESSAGE)
   {
   }

   virtual ~trOnRequestUIMsg() {}
   virtual t_Void vAllocate() {}
   virtual t_Void vDeAllocate() {}

   t_Bool bRequestUIStatus;
};

/*!
 * \struct trKeyMsg : public rMsgQBase
 * \brief DiPO message for Hard Keys & Knob Controller
 */
struct trKeyMsg : public trMsgQBase
{
   trKeyMsg(): trMsgQBase(e8KEY_MESSAGE),s8EncoderDeltaCnt(0)
      {
      }

      virtual ~trKeyMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! Key Mode-Pressed or Released
      tenKeyMode enKeyMode;
      //!Key Code
      tenKeyCode enKeyCode;
      //!Type of key i.e knob or button
      tenKeyType enKeyType;
      //! Knob encoder delta count
      t_S8 s8EncoderDeltaCnt;

};


/*!
 * \typedef struct HIDDevice trHIDDeviceInfo
 * \brief DiPO Input device Info
 */
typedef struct HIDDevice trHIDDeviceInfo;

/*!
 * \typedef struct HIDInputReport trHIDInputReport
 * \brief DiPO Input report (Touch data)
 */
typedef struct HIDInputReport trHIDInputReport;

/*! 
 * \typedef enum tenDiPOMainAudioType
 * \brief DiPO main audio types
 */
typedef enum
{
   //! Main audio type is not used(Alternate audio)
   e8AUDIO_NA = 0, 

   //! Defaut audio stream (UnKnown audio)
   e8AUDIO_DEFAULT = 1,

   //! Alert audio steream
   e8AUDIO_ALERT = 2,

   //! Media audio stream type
   e8AUDIO_MEDIA = 3,

   //! Telephony audio stream
   e8AUDIO_TELEPHONY = 4,

   //! Speech rec audio stream
   e8AUDIO_SPEECHREC = 5,

   //! Spoken audio stream
   e8AUDIO_SPOKEN = 6

}tenDiPOMainAudioType;


typedef enum
{
   //! allocation request
   e8ALLOCATION_REQUEST = 0,
 
   //! Deallocation request
   e8DEALLOCATION_REQUEST =1,

}tenAudioReqType;


typedef enum
{
    e8DIPO_ENTITY_NA     = 0,
    e8DIPO_ENTITY_CAR    = 1,
    e8DIPO_ENTITY_MOBILE = 2
} __attribute__ ((packed, aligned (1))) tenDiPOEntity;


typedef enum
{
    e8DIPO_SPEECHMODE_NA          = 0,
    e8DIPO_SPEECHMODE_NONE        = 1,
    e8DIPO_SPEECHMODE_REC         = 2,
    e8DIPO_SPEECHMODE_SPEAKING    = 3
} __attribute__ ((packed, aligned (1))) tenDiPOSpeechMode;


typedef struct rDiPOSpeechState
{
    tenDiPOEntity     enEntity /*= e8DIPO_ENTITY_NA*/;
    tenDiPOSpeechMode enSpeechMode   /*= e8DIPO_SPEECHMODE_NA*/;

    rDiPOSpeechState()
    {
       enEntity = e8DIPO_ENTITY_NA;
       enSpeechMode = e8DIPO_SPEECHMODE_NA;
    }
} __attribute__ ((packed)) trDiPOSpeechState;


struct trDiPOModeState
{
    tenDiPOEntity      enScreen     /*= e8DIPO_ENTITY_NA*/;
    tenDiPOEntity      enAudio      /*= e8DIPO_ENTITY_NA*/;
    trDiPOSpeechState  rSpeechState;
    tenDiPOEntity      enPhone      /*= e8DIPO_ENTITY_NA*/;
    tenDiPOEntity      enNavigation /*= e8DIPO_ENTITY_NA*/;

    trDiPOModeState()
    {
      enScreen = e8DIPO_ENTITY_NA;
      enAudio = e8DIPO_ENTITY_NA;
      enPhone = e8DIPO_ENTITY_NA;
      enNavigation = e8DIPO_ENTITY_NA;
    }
};


typedef enum
{
    e8DIPO_APPSTATE_NA    = 0,
    e8DIPO_APPSTATE_TRUE  = 1,
    e8DIPO_APPSTATE_FALSE = 2
} __attribute__ ((packed, aligned (1))) tenDiPOAppState;


typedef struct rDiPORsrcTransfer
{
    tenDiPOTransferType     enType;
    tenDiPOTransferPriority enPriority;
    tenDiPOConstraint       enTakeConstraint;
    tenDiPOConstraint       enBorrowConstraint;

    rDiPORsrcTransfer()
    {
        enType = e8DIPO_TRANSFERTYPE_NA;
        enPriority = e8DIPO_TRANSFERPRIO_NA;
    }
} __attribute__ ((packed, aligned (4))) trDiPORsrcTransfer;


struct trDiPOModeChanges
{
    trDiPORsrcTransfer rScreen;
    trDiPORsrcTransfer rAudio;
    tenDiPOSpeechMode  enSpeech     /*= e8DIPO_SPEECHMODE_NA*/;
    tenDiPOAppState    enPhone      /*= e8DIPO_APPSTATE_NA*/;
    tenDiPOAppState    enNavigation /*= e8DIPO_APPSTATE_NA*/;

    trDiPOModeChanges()
    {
       enSpeech = e8DIPO_SPEECHMODE_NA;
       enPhone = e8DIPO_APPSTATE_NA;
       enNavigation = e8DIPO_APPSTATE_NA;
    }
};

/*! 
* \struct rDiPOVideoContext
* \brief structure to hold video context data
*/
struct trDiPOVideoContext
{
   //! Display Context
   tenDisplayContext enDisplayContext;

   //! Transfer type
   tenDiPOTransferType enTransferType;

   //! Transfer priority
   tenDiPOTransferPriority enTransferPriority;

   //! Transfer constraint
   tenDiPOConstraint enTakeConstraint;

   //! Transfer borrow constraint
   tenDiPOConstraint enBorrowConstraint;

   trDiPOVideoContext& operator=(trDiPOVideoContext &DiPOVideoContext)
   {
      enDisplayContext = DiPOVideoContext.enDisplayContext;     
      enTransferType = DiPOVideoContext.enTransferType;
      enTransferPriority = DiPOVideoContext.enTransferPriority;
      enTakeConstraint = DiPOVideoContext.enTakeConstraint;
      enBorrowConstraint = DiPOVideoContext.enBorrowConstraint;
      return *this;
   }
};


/*! 
* \struct rDiPOAudioContext
* \brief structure to hold audio context data
*/
struct trDiPOAudioContext
{
   //! Audio context
   tenAudioContext enAudioContext;

   //! Transfer type
   tenDiPOTransferType enTransferType;

   //! Transfer priority
   tenDiPOTransferPriority enTransferPriority;

   //! Transfer constraint
   tenDiPOConstraint enTakeConstraint;

   //! Transfer borrow constraint
   tenDiPOConstraint enBorrowConstraint;
 
   trDiPOAudioContext& operator=(trDiPOAudioContext &rfoDiPOAudioContext)
   {
      enAudioContext = rfoDiPOAudioContext.enAudioContext;
      enTransferType = rfoDiPOAudioContext.enTransferType;
      enTransferPriority = rfoDiPOAudioContext.enTransferPriority;
      enTakeConstraint = rfoDiPOAudioContext.enTakeConstraint;
      enBorrowConstraint = rfoDiPOAudioContext.enBorrowConstraint;
      return *this;
   }

};


/*! 
* \struct rDiPOAppState
* \brief structure to hold app state data
*/
struct trDiPOAppState
{
   //! Speech state
   tenSpeechAppState enSpeechAppState;

   //! Phone state
   tenPhoneAppState enPhoneAppState;

   //! Nav app state
   tenNavAppState enNavAppState;

   trDiPOAppState& operator=(trDiPOAppState &rfoDiPOAppState)
   {
      enSpeechAppState = rfoDiPOAppState.enSpeechAppState;
      enPhoneAppState = rfoDiPOAppState.enPhoneAppState;
      enNavAppState = rfoDiPOAppState.enNavAppState;
      return *this;
   }
};

typedef enum
{
   //!Input Type Touch
   e8TOUCH = 0,

   //!Input Type Consumer Key
   e8CONSUMER_KEY,

   //!Input Type Telephone Key
   e8TELEPHONE_KEY,
}tenInputType;

/*!
 * \struct trAccVidContextMsg : public rMsgQBase
 * \brief DiPO message for Duck audio request from device(Response from ADIT)
 */
struct trAccVideoContextMsg : public trMsgQBase
{
   trAccVideoContextMsg():trMsgQBase(e8ACC_DISPLAY_CONTEXT_MESSAGE)
      {
      }

      virtual ~trAccVideoContextMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! Accessory display context
      trDiPOVideoContext rDiPOVideoContext;
};

/*! 
 * \struct trDiPOAudioFormat
 * \brief Audio format information container
 */

struct trDiPOAudioFormat
{
   //! Sampling rate of the audio stream
   t_U16 u16SampleRate;

   //! Bits per channel
   t_U8 u8BitsPerChannel;

   //! Number of channels in the stream
   t_U8 u8Channels;

   //!Drfault constructor
   trDiPOAudioFormat()
   {
      u16SampleRate = 0;
      u8BitsPerChannel = 0;
      u8Channels = 0;
   }

   //! Parametrized constructor
   trDiPOAudioFormat(t_U16 sampleRate, t_U8 bitsPerChannel, t_U8 channels)
   {
     u16SampleRate =  sampleRate;
     u8BitsPerChannel = bitsPerChannel;
     u8Channels = channels;
   } 

   //! Destructor
   virtual ~trDiPOAudioFormat(){}

}__attribute__ ((packed, aligned (4)));

/*!
 * \struct trAudioAllocMsg : public rMsgQBase
 * \brief DiPO message audio allocation (Response from ADIT)
 */
struct trAudioAllocMsg : public trMsgQBase
{
   trAudioAllocMsg():trMsgQBase(e8AUDIO_ALLOC_MESSAGE)
      {
      }

   /* Parametrized constructor*/
   	trAudioAllocMsg(tenMsgTypes enMsgType,
   			AudioChannelType enChannelType,
   			tenDiPOMainAudioType enDiPOMainAudioType,
   			tenAudioReqType enReqType,
   			trDiPOAudioFormat rAudioFormat
   			):trMsgQBase(enMsgType)
      {
   		enAudioChannelType = enChannelType;
   		enAudioType = enDiPOMainAudioType;
   		enAudioReqType = enReqType;
   		rDiPOAudioFormat = rAudioFormat;
      }

      virtual ~trAudioAllocMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! Identify the type of the audio.
      AudioChannelType enAudioChannelType;

      //! type of the audio stream
      tenDiPOMainAudioType enAudioType;

      //!type of audio allocation
      tenAudioReqType enAudioReqType;

      //! Format of the audio satream requested.
      trDiPOAudioFormat rDiPOAudioFormat;
};


struct trAccAudioContextMsg: public trMsgQBase
{
   trAccAudioContextMsg():trMsgQBase(e8RM_AUDIO_RQST_MESSAGE)
      {
      }

      virtual ~trAccAudioContextMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! parameters to request thr audio resource
      trDiPOAudioContext rDiPOAudioContext;

};

/*! 
* \struct trAccAppStateMsg
* \brief DiPO AccessoryApp state message
*/
struct trAccAppStateMsg : public trMsgQBase
{
   trAccAppStateMsg():trMsgQBase(e8RM_APP_STATE_MESSAGE)
      {
      }

      virtual ~trAccAppStateMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

   //! App state 
   trDiPOAppState rDiPOAppState;
};

/*! 
 * \typedef struct rDiPORMMsg : public rMsgQBase
 * \brief DiPO message for Resource Management(Response from ADIT)
 */
struct trDiPORMMsgResp : public trMsgQBase
{
   trDiPORMMsgResp(): trMsgQBase(e8RM_RESP_MESSAGE)
      {
      }

      virtual ~trDiPORMMsgResp() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! Represents the current state of the device
      trDiPOModeState rDiPOModeState;
};

/*!
* \struct rDiPOAudioMap
* \brief Audio map
*/
struct trDiPOAudioMap
{
   //! Audio context for project
   t_U8 u8ProjAudioCntxt;

   //! SPI internal audio context
   tenAudioContext enSPIAudioCntxt;
};

/*! 
 * \typedef enum tenDiPOSessionState
 * \brief Session states
 */
typedef enum
{
   //! Session state not known
   e8DIPO_SESSION_UNKNOWN = 0,

   //! session started
   e8DIPO_SESSION_START = 1,

   //! session ended
   e8DIPO_SESSION_END = 2,

   //! CarPlay library loaded
   e8DIPO_PLUGIN_LOADED = 3,

   //! CarPlay library unloaded
   e8DIPO_PLUGIN_UNLOADED = 4
}__attribute__ ((packed, aligned (1))) tenDiPOSessionState;


/*! 
 * \struct trSiriActionMsg : public trMsgQBase
 * \brief Siri Action Message
 */
struct trSiriActionMsg : public trMsgQBase
{
   trSiriActionMsg():trMsgQBase(e8SIRIACTION_MESSAGE)
   {
   }

   virtual ~trSiriActionMsg() {}
   virtual t_Void vAllocate() {}
   virtual t_Void vDeAllocate() {}

   //!Siri Action type-Prewarm,Button Down or Button Up
   SiriAction enSiriAction;
};



/*! 
 * \struct trDiPOSessionMsg : public trMsgQBase
 * \brief DiPO session related messages from CarPlay plugin
 */
struct trDiPOSessionMsg : public trMsgQBase
{
    trDiPOSessionMsg():enDiPOSessionState(e8DIPO_SESSION_UNKNOWN),
       trMsgQBase(e8SESSION_MESSAGE)
   {
   }

   virtual ~trDiPOSessionMsg() {}
   virtual t_Void vAllocate() {}
   virtual t_Void vDeAllocate() {}

   //! Session state
   tenDiPOSessionState enDiPOSessionState;
};

/*! 
 * \struct rVehicleConfigData
 * \brief DiPO info messages
 */
struct trVehicleConfigData
{

      trVideoConfigData rVideoConfigData;
      trVehicleBrandInfo rVehicleBrandInfo;
     // t_U8  u8Driveinfo;
      tenDriveSideInfo enDriveSideInfo;
      trVehicleConfigData():
      enDriveSideInfo(e8UNKNOWN_DRIVE_SIDE)
      {

      }

};

struct trDiPOVideoConfigMsg: public trMsgQBase
{
   trDiPOVideoConfigMsg(): trMsgQBase(e8VIDEO_CONFIG_MESSAGE)
   {

   }

   virtual ~trDiPOVideoConfigMsg()
   {

   }

   virtual t_Void vAllocate()
   {
   }
   virtual t_Void vDeAllocate()
   {
   }

   //!Video configuration data
   trVideoConfigData rVideoConfigData;
};


enum tenDiPORestrictionInfo
{
	//@Note : Bitmap
	// Bit 0- Soft keyboard
	// Bit 1- Soft phone keypad
	// Bit 2- non music list
	// Bit 3- music list
	// Bit 4- japan maps.
	// value 1 indicates that the restriction is required.
	e8SOFT_KEYBOARD = 1,
	e8SOFT_PHONEKEYPAD = 2,
	e8NON_MUSIC_LIST = 4,
	e8MUSIC_LIST = 8,
	e8JAPAN_MAPS = 16

};

typedef enum
{
     e8AUTOLAUNCH_DISABLED = 0,
	 e8AUTOLAUNCH_ENABLED = 1
}tenAutoLaucnhFlag;

/*! 
 * \struct trDiPOInfoMessage: public trMsgQBase
 * \brief DiPO info messages
 */
struct trDiPOInfoMessage: public trMsgQBase
{
   trDiPOInfoMessage():trMsgQBase(e8INFO_MESSAGE)
      {
          memset(szBtMacAdress, 0, MAX_STR_LEN);
      }

      virtual ~trDiPOInfoMessage()
      {
      }
      virtual t_Void vAllocate()
      {
      }
      virtual t_Void vDeAllocate()
      {
      }
      t_Char szBtMacAdress[MAX_STR_LEN]; 

      //! Vehicle config data
      trVehicleConfigData rVehicleConfigData;

	  //! Display restriction info.
      t_U8 u8DriveRestrictionInfo;

      //! Filed contains vehicle drive status
      tenVehicleConfiguration enDriveModeInfo;

      //! Filed contains the vehicle light info.
      tenVehicleConfiguration enNightModeInfo;

      //! Display input configuration.
      t_U8 u8DisplayInput;

      //! Indicates whether the automatic launch is required or not
      tenAutoLaucnhFlag enAutoLaucnhFlag;
};

/*! 
 * \typedef struct rOnDisBluetoothMsg
 * \brief DiPO Disable bluetooth request message
 */
struct trOnDisBluetoothMsg :  public trMsgQBase
{
   t_Char cBluetoothID[MAX_STR_LEN];

   //! Default constructor
   trOnDisBluetoothMsg():trMsgQBase(e8DISABLE_BLUETOOTH_MESSAGE)
   {
   }
   
   //! parametrized constructor 
   trOnDisBluetoothMsg(t_String szBlutoothID):trMsgQBase(e8DISABLE_BLUETOOTH_MESSAGE)
   {
      sprintf(cBluetoothID, szBlutoothID.c_str());
   }
   
   virtual ~trOnDisBluetoothMsg() {}
   virtual t_Void vAllocate() {}
   virtual t_Void vDeAllocate() {}
   
}__attribute__ ((packed));


/*! 
 * \struct trVehicleModeInfoMsg
 * \brief Vehicle mode info[SPI_TO_CarPlay]
 */
struct trVehicleModeInfoMsg : public trMsgQBase
{
   tenVehicleConfiguration enVehicleConfig;

   trVehicleModeInfoMsg():trMsgQBase(e8VEHICLE_MODE_MESSAGE)
   {
   }

   virtual ~trVehicleModeInfoMsg(){}
   virtual t_Void vAllocate() {}
   virtual t_Void vDeAllocate() {}
};

struct trAudioStopMsg:public trMsgQBase
{
   trAudioStopMsg(): trMsgQBase(e8AUDIO_ERROR_MESSAGE)
      {
      }

      virtual ~trAudioStopMsg() {}
      virtual t_Void vAllocate() {}
      virtual t_Void vDeAllocate() {}

      //! touch cordinate data
      tenAudioReqType enAudioReqType;
};

/*!
 * \typedef struct trBtAddrUpdateMsg
 * \brief BT address update msg.
 */
struct trBtAddrUpdateMsg: public trMsgQBase
{

	trBtAddrUpdateMsg():trMsgQBase(e8BTADDRESS_UPDATE_MESSAGE)
	{
		memset(cBTAddress, 0, MAX_STR_LEN);
	}

	virtual ~trBtAddrUpdateMsg(){}

    virtual t_Void vAllocate() {}

    virtual t_Void vDeAllocate() {}

    t_Char cBTAddress[MAX_STR_LEN];
};

/*!
 * \typedef struct trRestrictionsUpdateMsg
 * \brief Restrictions info update msg.
 */
struct trRestrictionsUpdateMsg:public trMsgQBase
{
   trRestrictionsUpdateMsg(): trMsgQBase(e8RESTRICTIONS_UPDATE_MESSAGE)
   {
   }

   virtual ~trRestrictionsUpdateMsg() {}
   virtual t_Void vAllocate() {}
   virtual t_Void vDeAllocate() {}

   //! Display restriction info.
    t_U8 u8DriveRestrictionInfo;
};


/*! 
 * \enum tenAccModeChangeType
 * \brief Mode change request type
 */
typedef enum
{
   //! Mode change unknown
   e8MODE_CHANGE_UNKNOWN = 0,

   //! Display mode change
   e8MODE_CHANGE_DISPLAY = 1,

   //!Audio mode change
   e8MODE_CHANGE_AUDIO = 2,

   //! App state change
   e8MODE_CHANGE_APP = 3
}tenAccModeChangeType;

/*! 
 * \enum tenModeChangeReqType
 * \brief Mode change request type
 */
typedef enum
{
   //! Indicates the actual mode change requests
   e8ACTUAL_MODE_CHANGE_REQUEST = 0,
   //! Indicates the informational mode change request.
   e8INFO_MODE_CHANGE_REQUEST = 1
}tenModeChangeReqType;

#endif
