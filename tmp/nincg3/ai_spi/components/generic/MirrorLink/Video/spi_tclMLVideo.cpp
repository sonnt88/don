/***********************************************************************/
/*!
* \file  spi_tclMLVideo.cpp
* \brief Mirror Link Video Implementation
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Mirror Link Video Implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
21.01.2014  | Shiva Kumar Gurija    | Updated with Video Response Interface
03.04.2013  | Shiva Kumar Gurija    | Device status messages
16.04.2013  | Shiva Kumar Gurija    | Content Attestation
08.07.2014  | Shiva Kumar Gurija    | FrameBufferBlocking changes to 
                                       fix CTS issues
16.07.2014  | Shiva Kumar Gurija    | Implemented SessionStatusInfo update
30.10.2013  | Shiva Kumar Gurija    | Fix for GMMY16-19383 
06.11.2014  |  Hari Priya E R       | Added changes to set Client key capabilities
21.05.2014  | Shiva Kumar Gurija    | Changes to use ML1.x or above Phones
25.06.2015  | Shiva kaumr G         | Content attestation

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include <map>
#include "SPITypes.h"
#include "RespBase.h"
#include "RespRegister.h"
#include "spi_tclMLVncManager.h"
#include "Timer.h"
#include "Lock.h"
#include "spi_tclConfigReader.h"
#include "spi_tclVideoTypedefs.h"
#include "spi_tclVideoSettings.h"
#include "spi_tclVideoDevBase.h"
#include "spi_tclMLVncCmdViewer.h"
#include "spi_tclMLVncCmdDiscoverer.h"
#include "spi_tclMLVncRespViewer.h"
#include "spi_tclMLVideo.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VIDEO
#include "trcGenProj/Header/spi_tclMLVideo.cpp.trc.h"
#endif
#endif
#if defined(_lint) && (_lint <= 912) // Versions up to and including 9.00L
//lint �e10   PQM_authorized_multi_492  Reason: Lack of C++11 support by Lint
//lint �e19   PQM_authorized_multi_493  Reason: Lack of C++11 support by Lint
//lint �e40   PQM_authorized_multi_494  Reason: Lack of C++11 support by Lint
//lint �e48   PQM_authorized_multi_495  Reason: Lack of C++11 support by Lint
//lint �e55   PQM_authorized_multi_496  Reason: Lack of C++11 support by Lint
//lint �e58   PQM_authorized_multi_497  Reason: Lack of C++11 support by Lint
//lint �e63   PQM_authorized_multi_498  Reason: Lack of C++11 support by Lint
//lint �e64   PQM_authorized_multi_499  Reason: Lack of C++11 support by Lint
//lint �e1013 PQM_authorized_multi_500  Reason: Lack of C++11 support by Lint
//lint �e1055 PQM_authorized_multi_501  Reason: Lack of C++11 support by Lint
//lint �e601  PQM_authorized_multi_502  Reason: Lack of C++11 support by Lint
//lint �e1401 PQM_authorized_multi_503  Reason: Lack of C++11 support by Lint
//lint �e808  PQM_authorized_multi_504  Reason: Lack of C++11 support by Lint
//lint �e515  PQM_authorized_multi_505  Reason: Lack of C++11 support by Lint
//lint �e516  PQM_authorized_multi_506  Reason: Lack of C++11 support by Lint
//lint �e746  PQM_authorized_multi_507  Reason: Lack of C++11 support by Lint
#elif defined(_lint)
#error Check if above problems still exist with new Lint version
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
//wl_pump_events method should be called fro every 25msec
#define TIMER_PUMP_WL_EVENTS_MSEC (tCU32)25

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static spi_tclMLVncManager* spoVncManager = NULL;
static spi_tclMLVncCmdViewer* spoMLViewerCmd = NULL;
static spi_tclMLVncCmdDiscoverer* spoMLDiscCmd = NULL;

//Member variable to maintain Timer Index of Wayland touch events timer
static timer_t sWLTimerIndex = 0;
static const t_String coszEmptyStr = "";
static const t_U32 cos32ContAttestationNormalError = 1 ;
static const t_U32 cos32ContAttestationFatalError = 0 ;
static const t_U32 scou32InvalidDevID = 0;

/***************************************************************************
** FUNCTION:  spi_tclMLVideo::spi_tclMLVideo()
***************************************************************************/
spi_tclMLVideo::spi_tclMLVideo():
m_u32SelectedDevice(0),
m_bWLInitialized(false),
m_bCmdStringProcessed(false),
m_bServerSupportsOrientationSwitch(false),
m_enOrientatiobMode(e8INVALID_MODE),
m_enBlockingMode(e8DISABLE_BLOCKING),
m_bMLSessionEstablished(false),
m_bNightModeEnabled(false),
m_bDriveModeEnabled(false),
m_bDeviceStatusUpdate(false),
m_bIsContAttEnabled(false)
{
   ETG_TRACE_USR1(("spi_tclMLVideo() "));
}

/***************************************************************************
** FUNCTION:  spi_tclMLVideo::~spi_tclMLVideo()
***************************************************************************/
spi_tclMLVideo::~spi_tclMLVideo()
{
   ETG_TRACE_USR1(("~spi_tclMLVideo() "));
   // release memory & set member variables to null to avoid Lint 
   m_bDeviceStatusUpdate=false;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVideo::bInitialize()
***************************************************************************/
t_Bool spi_tclMLVideo::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclMLVideo:bInitialize "));
   t_Bool bRet = false;

   spoVncManager = spi_tclMLVncManager::getInstance();
   SPI_NORMAL_ASSERT(NULL == spoVncManager);

   if(NULL != spoVncManager )
   {
      spoMLViewerCmd = const_cast<spi_tclMLVncCmdViewer*>(spoVncManager->poGetViewerInstance());
      SPI_NORMAL_ASSERT(NULL == spoMLViewerCmd);

      spoMLDiscCmd = const_cast<spi_tclMLVncCmdDiscoverer*>(spoVncManager->poGetDiscovererInstance());
      SPI_NORMAL_ASSERT(NULL == spoMLDiscCmd);

      if(( NULL != spoMLDiscCmd )&&( NULL != spoMLViewerCmd )&&
         ( true == spoMLViewerCmd -> bInitialiseViewerSDK()))
      {
         bRet = true;
      }//if(( NULL != spoMLViewerCmd )

      //Register for Viewer & DAP Responses
      spoVncManager->bRegisterObject((spi_tclMLVncRespViewer*)this) ;
      spoVncManager->bRegisterObject((spi_tclMLVncRespDAP*)this) ;
      spoVncManager->bRegisterObject((spi_tclMLVncRespDiscoverer*)this);
   }//if(NULL != spoVncManager )

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vUninitialize()
***************************************************************************/
t_Void spi_tclMLVideo::vUninitialize()
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vUninitialize "));
   if( NULL != spoMLViewerCmd )
   {
      spoMLViewerCmd -> vUnInitializeViewerSDK();
   }//if( NULL != spoMLViewerCmd )
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPopulateWLInitializeData()
***************************************************************************/
t_Void spi_tclMLVideo::vPopulateWLInitializeData()
{
   ETG_TRACE_USR1(("spi_tclMLVideo::vPopulateWLInitializeData"));
   spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();
   spi_tclVideoSettings* poVideoSettings = spi_tclVideoSettings::getInstance();
   if( (NULL != poVideoSettings) && (NULL != poConfigReader) )
   {
      trVideoConfigData rVideoConfigData;
      poConfigReader->vGetVideoConfigData(e8DEV_TYPE_MIRRORLINK,rVideoConfigData);

      m_rWLInitializeData.u32LayerId = rVideoConfigData.u32LayerId;
      m_rWLInitializeData.u32SurfaceId = rVideoConfigData.u32SurfaceId;

      m_rWLInitializeData.rMLScreenSize.u32Screen_Height = rVideoConfigData.u32ProjScreen_Height;
      m_rWLInitializeData.rMLScreenSize.u32Screen_Width = rVideoConfigData.u32ProjScreen_Width;
      m_rWLInitializeData.u32ScreenWidth_Mm = rVideoConfigData.u32ProjScreen_Width_Mm;
      m_rWLInitializeData.u32ScreenHeight_Mm = rVideoConfigData.u32ProjScreen_Height_Mm;

      poVideoSettings->vGetScreenOffset(m_rWLInitializeData.rMLScreenOffset);
      m_rWLInitializeData.bServerSideScalingRequired = poVideoSettings->bIsServerSideScalingRequired();
      m_rWLInitializeData.u8EnableTouchInputEvents = poVideoSettings->u8EnableTouchInputEvents();

      if( NULL != spoMLViewerCmd )
      {
         spoMLViewerCmd->vPopulateTouchEnableFlag(m_rWLInitializeData.u8EnableTouchInputEvents);
      }//if( OSAL_NULL != spoMLViewerCmd )

   }//if(OSAL_NULL != poVideoSettings)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVideo::bInitializeMLViewerSession()
***************************************************************************/
t_Bool spi_tclMLVideo::bInitializeMLViewerSession(const t_U32 cou32DevID)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:bInitializeMLViewerSession:Device ID-0x%x ",
      cou32DevID));

   m_u32SelectedDevice = cou32DevID ;
   //Initialize viewer & wayland only if it is not initialized earlier
   if( (false == m_bWLInitialized)&&( NULL != spoMLViewerCmd ) )
   {
      spoMLViewerCmd -> vCreateViewer(NULL);

      vPopulateWLInitializeData();

      ETG_TRACE_USR4(("LayerID-%d SurfaceID-%d Screen OffSet X-%d Y-%d ScreenWidth-%d ScreenHeight-%d Enable Touch-%d Scaling-%d",
         m_rWLInitializeData.u32LayerId,
         m_rWLInitializeData.u32SurfaceId,m_rWLInitializeData.rMLScreenOffset.u32Screen_X_Offset,
         m_rWLInitializeData.rMLScreenOffset.u32Screen_Y_Offset, m_rWLInitializeData.rMLScreenSize.u32Screen_Width,
         m_rWLInitializeData.rMLScreenSize.u32Screen_Height, m_rWLInitializeData.u8EnableTouchInputEvents,
         ETG_ENUM(BOOL,m_rWLInitializeData.bServerSideScalingRequired)));

      //Initialize Wayland
      m_bWLInitialized = spoMLViewerCmd -> bInitializeWayland(m_rWLInitializeData.u32LayerId, m_rWLInitializeData.u32SurfaceId,
         m_rWLInitializeData.rMLScreenOffset.u32Screen_X_Offset,m_rWLInitializeData.rMLScreenOffset.u32Screen_Y_Offset, 
         m_rWLInitializeData.rMLScreenSize.u32Screen_Width, m_rWLInitializeData.rMLScreenSize.u32Screen_Height, 
         m_rWLInitializeData.bServerSideScalingRequired, m_rWLInitializeData.u8EnableTouchInputEvents,
         m_rWLInitializeData.u32ScreenHeight_Mm,m_rWLInitializeData.u32ScreenWidth_Mm);

      //Added compiler switch to disable content attestation feature.
      if(true == m_bWLInitialized)
      {
         vEnableContentAttestation();
      }//if(true == m_bWLInitialized)
   } //if((false == m_bWLInitialized)

   return m_bWLInitialized;
}
/***************************************************************************
** FUNCTION:  t_Void  spi_tclMLVideo::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclMLVideo::vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vRegisterCallbacks "));
   //Copy all the function pointers 
   m_rVideoCallbacks = corfrVideoCallbacks;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vSelectDevice()
***************************************************************************/
t_Void spi_tclMLVideo::vSelectDevice(const t_U32 cou32DevID,
                                     const tenDeviceConnectionReq coenConnReq)
{

   /*lint -esym(40,fpvSelectDeviceCb)fpvSelectDeviceCb Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLVideo:vSelectDevice: Device ID-0x%x Selection Type - %d",
      cou32DevID,ETG_ENUM(CONNECTION_REQ,coenConnReq)));

   tenErrorCode enErrCode = e8INVALID_DEV_HANDLE;
   if(  e8DEVCONNREQ_SELECT == coenConnReq )
   {
      //If the new Select device request comes with the e8DEVCONNREQ_SELECT, with out
      //the select device e8_DEVDISCONNECT for the already selected device,
      //unInitialize the wayland, if it is already initialized.
      if( cou32DevID != m_u32SelectedDevice )
      {
         vUnInitializeMLViewerSession(m_u32SelectedDevice);
      }//if( cou32DevID != m_u32SelectedDevice )

       enErrCode = (true == bInitializeMLViewerSession(cou32DevID))?e8NO_ERRORS:e8SELECTION_FAILED;
   }//if( coenConnReq == e8DEVCONNREQ_SELECT )
   else
   {
      enErrCode = e8NO_ERRORS;
      vUnInitializeMLViewerSession(cou32DevID);
   }

   if(NULL != m_rVideoCallbacks.fpvSelectDeviceCb)
   {
      (m_rVideoCallbacks.fpvSelectDeviceCb)(cou32DevID,enErrCode);
   }//if(NULL != m_rVideoCallbacks.fpvSelectDeviceCb)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vUnInitializeMLViewerSession()
***************************************************************************/
t_Void spi_tclMLVideo::vUnInitializeMLViewerSession(const t_U32 cou32DevId)
{
   ETG_TRACE_USR1(("vUnInitializeMLViewerSession:Device ID:0x%x, Selected Device ID:0x%x Wayland already Initialized-%d",
      cou32DevId,m_u32SelectedDevice,ETG_ENUM(BOOL,m_bWLInitialized)));

   //If the device is not invalid and the de select request came for the selected device,
   //Uninitialize the wayland and reset flags
   //Selected device can be an invalid device(ID 0), when the a device is selected for the 
   //first time.or a device X is de selected and a new device Y is selected. When the device X 
   //is de selected, selected device will be set to zero. and when the new device Y is selected, 
   //selected device will be zero and the same will be passed here.
   if((scou32InvalidDevID != cou32DevId)&&(m_u32SelectedDevice == cou32DevId))
   {
      //Uninitialize viewer and then finalize wayland.
      //wl_pump_events timer must be stopped, before destroying viewer
      if( ( NULL != spoMLViewerCmd ) && (true == m_bWLInitialized) )
      {
         //Stop the timer 
         vStopTimerTouchEvents();

         m_bWLInitialized = false;
         spoMLViewerCmd -> vDestroyViewer();
         spoMLViewerCmd->vUnInitializeWayland();
      }//if( (NULL != spoMLViewerCmd )

      //Reset flags
      m_bCmdStringProcessed = false;
      m_u32SelectedDevice = scou32InvalidDevID;
      m_bServerSupportsOrientationSwitch = false;
      m_enOrientatiobMode = e8INVALID_MODE ;
      m_enBlockingMode = e8DISABLE_BLOCKING ;
      m_bMLSessionEstablished = false;
      m_bDeviceStatusUpdate = false;
      m_bIsContAttEnabled = false;

   }//if((scou32InvalidDevID != cou32DevId)

}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVideo::bLaunchVideo()
***************************************************************************/
t_Bool spi_tclMLVideo::bLaunchVideo(const t_U32 cou32DevId,
                                    const t_U32 cou32AppId,
                                    const tenEnabledInfo coenSelection)
{
   t_Bool bRet = false;
   //Currently Enabled/Disabled setting is not used
   SPI_INTENTIONALLY_UNUSED(coenSelection);

   ETG_TRACE_USR1(("spi_tclMLVideo:bLaunchVideo: Device ID -0x%x,App ID-0x%x,Selected Device ID-0x%x, ML Session already started-%d\n",
      cou32DevId,cou32AppId,m_u32SelectedDevice,m_bCmdStringProcessed));

   // If launch video is requested for some other device, other than the selected device
   //return false
   if(cou32DevId == m_u32SelectedDevice)
   {
      //Process cmd string should be called only once for a device.
      //System crashes if the cmd string is processed twice for the same device.
      //if the CmdString is already processed return true, else process it
      if( ( false == m_bCmdStringProcessed )&&( true == m_bWLInitialized )&&( NULL != spoMLViewerCmd )&&
         (true == spoMLViewerCmd->bTriggerProcessCmdString(cou32AppId,cou32DevId)) )
      {
         vStartTimerTouchEvents();
         m_bCmdStringProcessed = true;
      } //if(( false == bRet )
      bRet = m_bCmdStringProcessed;
   } //if(cou32DevId == m_u32SelectedDevice)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vStartTimerTouchEvents()
***************************************************************************/
t_Void spi_tclMLVideo::vStartTimerTouchEvents()
{
   //@Note: Timer is used to send wayland touch events to layer manager
   //StartTimer Creates a new Timer every Time & populates the Timer Index
   Timer* poTimer = Timer::getInstance();
   if ((NULL != poTimer) &&
      (false == poTimer->StartTimer( sWLTimerIndex, TIMER_PUMP_WL_EVENTS_MSEC,
      TIMER_PUMP_WL_EVENTS_MSEC, this,bOnSendWLTouchEventsCb,NULL ))
      )
   {
      ETG_TRACE_ERR(("vStartTimerTouchEvents: Error in starting Timer"));
   }//if(( NULL != poTimer)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vStopTimerTouchEvents()
***************************************************************************/
t_Void  spi_tclMLVideo::vStopTimerTouchEvents()
{
   //StartTimer creates a new Timer always & populates the Timer Index.
   // So reset the Timer, once the Timer is started
   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
   {
      poTimer->CancelTimer(sWLTimerIndex);
      //Reset the Timer Index
      sWLTimerIndex = 0; 
      ETG_TRACE_USR2(("vStopTimerTouchEvents: Timer stopped"));
   }//if( NULL != poTimer)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVideo::bOnSendWLTouchEventsCb()
***************************************************************************/
t_Bool spi_tclMLVideo::bOnSendWLTouchEventsCb(timer_t timerID , 
                                              t_Void *pObject, 
                                              const t_Void *pcoUserData)
{
   t_Bool bRet = false ;

   SPI_INTENTIONALLY_UNUSED(pcoUserData);
   SPI_INTENTIONALLY_UNUSED(pObject);

   if( ( NULL != spoMLViewerCmd )&&( sWLTimerIndex == timerID ) )
   {
      spoMLViewerCmd->vPumpWaylandEvents();
      bRet = true;
   }//if( NULL != spoMLViewerCmd )

   return bRet;

}

/***************************************************************************
** FUNCTION:  t_Void vPostWLInitialiseError()
***************************************************************************/
t_Void spi_tclMLVideo::vPostWLInitialiseError( t_String szMessage,
                                              const t_U32 cou32DevId)
{
   ETG_TRACE_ERR(("spi_tclMLVideo:vPostWLInitialiseError:Error Msg - %s",szMessage.c_str()));
   SPI_INTENTIONALLY_UNUSED(cou32DevId);

   /*lint -esym(40,fpvNotifySessionStatus)fpvNotifySessionStatus Undeclared identifier */
   //There is an error in wayland(video rendering). Inform HMI about the session error and
   //De select the device. This will terminate the session gracefully.
   if( ( scou32InvalidDevID != m_u32SelectedDevice) 
      && (NULL != m_rVideoCallbacks.fpvNotifySessionStatus ))
   {
      (m_rVideoCallbacks.fpvNotifySessionStatus)(m_u32SelectedDevice,
         e8DEV_TYPE_MIRRORLINK,e8_SESSION_ERROR);
   }//if(NULL != m_rVideoCallbacks.fpvNotifySessionStatus )

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostViewerError()
***************************************************************************/
t_Void spi_tclMLVideo::vPostViewerError(t_U32 u32ViewerError)
{
   /*lint -esym(40,fpvNotifySessionStatus)fpvNotifySessionStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLVideo:vPostViewerError: Error code - %d ",u32ViewerError));

   switch(u32ViewerError)
   {
   case cs32MLViewerErrorMirrorLinkMLServerPublicKeyRequired:
      {
         ETG_TRACE_USR4(("Session is terminated due to ML Server VNC Public Key was not set"));
      }//case cs32MLViewerErrorMirrorLinkMLServerPubli
      break;
   case cs32MLViewerErrorContentAttestationFailed:
      {
         ETG_TRACE_USR4(("Session is terminated due to Content Attestation Failure"));
      }//case cs32MLViewerErrorContentAttestationFailed
      break;
   default:
      {

      }//default:
   }//switch(u32ViewerError)

   //If there is any error in viewer, Inform HMI about the session error and
   //De select the device. This will terminate the session gracefully.

   //it is observed that, SDK is sending viewer error after the Viewer is destroyed.
   //So we would send the session status with SESSION_ERROR, after the device is deselected.
   //So, invalid device check is added to avoid this. after the Video resources are deallocated,
   //Selected device value will be set to invalid device
   if( ( scou32InvalidDevID != m_u32SelectedDevice) &&
      (NULL != m_rVideoCallbacks.fpvNotifySessionStatus ))
   {
      (m_rVideoCallbacks.fpvNotifySessionStatus)(m_u32SelectedDevice,
         e8DEV_TYPE_MIRRORLINK,e8_SESSION_ERROR);
   }//if(NULL != m_rVideoCallbacks.fpvNotifySessionStatus )

}

/***************************************************************************
** FUNCTION:  t_U32  spi_tclMLVideo::vStartVideoRendering()
***************************************************************************/
t_Void spi_tclMLVideo::vStartVideoRendering(t_Bool bStartVideoRendering)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vStartVideoRendering: Enable SPI Layer-%d ",ETG_ENUM(BOOL,bStartVideoRendering)));

   /*lint -esym(40,fpvVideoRenderStatusCb)fpvVideoRenderStatusCb Undeclared identifier */
#if 0
   //StartWriting frame buffers to layer request is equivalent to disable blocking/Enabling layer.
   tenBlockingMode enBlockingMode = (true == bStartVideoRendering)? e8DISABLE_BLOCKING : e8ENABLE_BLOCKING ;

   if(0 != m_u32SelectedDevice)
   {
      if( ( m_enBlockingMode != enBlockingMode) && (NULL != spoMLViewerCmd) )
      {
         //in case, if HMI sends enable blocking, before the session is established, SDK will return an 
         //an error. But we have to send request to stop writing frame buffers, once the session is established,
         //because SPI layer will be there on BG. So send response as success always. so that HMI can send 
         //disable blocking request, whenever SPI screen comes to FG and SPI can request Phone to start sending 
         //the frame buffer updates.
         m_enBlockingMode = enBlockingMode;

         if(e8ENABLE_BLOCKING == enBlockingMode)
         {
            //SPI is moved to background, inform device about this with a blocking notification
            m_oAppContextLock.s16Lock();
            if(true == spoMLViewerCmd->bTriggerFramebufferBlockingNotification(m_rAppCntxtInfo.u32AppUUID,
               m_rAppCntxtInfo.rMLRect,e16MLUI_NOTVISIBLE,m_u32SelectedDevice))
            {
               ETG_TRACE_USR4(("spi_tclMLVideo:enSetVideoBlockingMode:frame buffer blocking request is sent!"));
            }//if(true == spoMLViewerCmd->bTrigg
            m_oAppContextLock.vUnlock();

            //If the disable frame buffer update request is sent, before the first application is launched,
            //Device is not providing the session established callback.
            //So, send the request to stop sending frame buffer updates once the session established callback
            //is received, if the video blocking is enabled
            if(m_bMLSessionEstablished == true)
            {
               //Send request to the device to stop sending frame buffers , when the blocking is enabled.
               spoMLViewerCmd->vEnableFrameBufferUpdates(false);
            }//if(m_bMLSessionEstablished == true)

         }//if(e8ENABLE_BLOCKING == coenBlockingMode)
         else
         {
            //send request to start sending frame buffer updates , when the blocking is disabled.
            spoMLViewerCmd->vEnableFrameBufferUpdates(true);
         }
      }//if(e8ENABLE_BLOCKING == enBlockingMode)
   }
   else
   {
      //Let the SPI layer be on the BG, independent of the enable/disable layer request.
      //Currently there is no device is selected for session
      bStartVideoRendering = false;
   }
#endif

   // Send response to the set request
   if(NULL != m_rVideoCallbacks.fpvVideoRenderStatusCb )
   {
      (m_rVideoCallbacks.fpvVideoRenderStatusCb)(bStartVideoRendering,e8DEV_TYPE_MIRRORLINK);
   }//if(NULL != m_rVideoCallbacks.fpvVideoRenderStatusCb )

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vGetVideoSettings()
***************************************************************************/
t_Void spi_tclMLVideo::vGetVideoSettings(const t_U32 cou32DevId,
                                         trVideoAttributes& rfrVideoAttributes)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vGetVideoSettings() "));
   SPI_INTENTIONALLY_UNUSED(cou32DevId);

   //Currently only Landscape mode is supported by ADIT.
   rfrVideoAttributes.enOrientationMode = m_enOrientatiobMode;

   //Screen width and height values are set by HMI.
   //If HMI doesn't set screen width and height, default values will be taken from config file
   //Default screen size in the config file is 800X480
   rfrVideoAttributes.rScreenAttributes.u32ScreenHeight = m_rWLInitializeData.rMLScreenSize.u32Screen_Height;
   rfrVideoAttributes.rScreenAttributes.u32ScreenWidth = m_rWLInitializeData.rMLScreenSize.u32Screen_Width ;

   //Currently display aspect ratio is UnKnown
   //It could be either server aspect ratio or Screen size provided during the wayland initialization.
   //We don't have interface with ADIT to set the aspect ratio dynamically
   rfrVideoAttributes.rScreenAttributes.enScreenAspectRatio=e8ASPECT_UNKNOWN;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vSetServerAspectRatio()
***************************************************************************/
t_Void spi_tclMLVideo::vSetServerAspectRatio(const tenScreenAspectRatio& corfenScrAspRatio)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vSetServerAspectRatio()"));

   //corScreenAttributes.enScreenAspectRatio is unused currently.
   SPI_INTENTIONALLY_UNUSED(corfenScrAspRatio);
   //Display will be shown with the screen width and height

}

/***************************************************************************
** FUNCTION: tenErrorCode spi_tclMLVideo::enSetVideoBlockingMode()
***************************************************************************/
tenErrorCode spi_tclMLVideo::enSetVideoBlockingMode(const t_U32 cou32DevId,
                                                    const tenBlockingMode coenBlockingMode)
{
   /*lint -esym(40,fpvNotifySessionStatus)fpvNotifySessionStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLVideo:enSetVideoBlockingMode: Blocking Mode-%d ",ETG_ENUM(BLOCKING_MODE,coenBlockingMode) ));

   //tenErrorCode is used to return the invalid device handle errors, if the device is not selected.
   //it can be used to send the different error codes in future.
   tenErrorCode enErrorCode = e8INVALID_DEV_HANDLE;

   if( (cou32DevId == m_u32SelectedDevice) && (NULL != spoMLViewerCmd) )
   {
      //Always send the response as success to HMI. Because we send request to start/stop sending frame buffer 
      //updates to Phone, once the session is established. Here our main intention is to inform Phone about the
      //Layer change and the continue/stop writing frame buffers to SPi Layer. 
      enErrorCode = e8NO_ERRORS;

      if( m_enBlockingMode != coenBlockingMode) 
      {
         //in case, if HMI sends enable blocking, before the session is established, SDK will return an 
         //an error. But we have to send request to stop writing frame buffers, once the session is established,
         //because SPI layer will be there on BG. So send response as success always. so that HMI can send 
         //disable blocking request, whenever SPI screen comes to FG and SPI can request Phone to start sending 
         //the frame buffer updates.
         m_enBlockingMode = coenBlockingMode;

         //If the request is to disable blocking,send the session status update as Active.
         tenSessionStatus enSessionStatus = e8_SESSION_ACTIVE ;
         if(e8ENABLE_BLOCKING == coenBlockingMode)
         {
            //SPI is moved to background, inform device about this with a blocking notification
            m_oAppContextLock.s16Lock();
            if(true == spoMLViewerCmd->bTriggerFramebufferBlockingNotification(m_rAppCntxtInfo.u32AppUUID,
               m_rAppCntxtInfo.rMLRect,e16MLUI_NOTVISIBLE,m_u32SelectedDevice))
            {
               ETG_TRACE_USR4(("frame buffer blocking request is sent successfully"));
            }//if(true == spoMLViewerCmd->bTrigg
            m_oAppContextLock.vUnlock();

            //Still the session is active, but the SPI is on the background.
            //Inform  the status as suspended
            enSessionStatus = e8_SESSION_SUSPENDED ;

            //If the disable frame buffer update request is sent, before the first application is launched,
            //Device is not providing the session established callback.
            //So, send the request to stop sending frame buffer updates once the session established callback
            //is received, if the video blocking is enabled
            if(m_bMLSessionEstablished == true)
            {
               //Send request to the device to stop sending frame buffers , when the blocking is enabled.
               spoMLViewerCmd->vEnableFrameBufferUpdates(false);
            }//if(m_bMLSessionEstablished == true)

         }//if(e8ENABLE_BLOCKING == coenBlockingMode)
         else
         {
            //send request to start sending frame buffer updates , when the blocking is disabled.
            spoMLViewerCmd->vEnableFrameBufferUpdates(true);
         }

         //Update session status to HMI, only if the session is already established.

         //ML Session becomes active, only after the command string is processed(first application launch)
         //and license is valid.
         //HMI would send disable blocking request for each launch app request and we would send status as 
         //active for the first launch app request, before the session status is actually active.
         //to avoid that, send the session status, only after the session is established.

         //in case of any error during session initialization, we will get Viewer error and the session error
         //status will be reported to HMI.
         if( (true == m_bMLSessionEstablished)&&
            (NULL != m_rVideoCallbacks.fpvNotifySessionStatus ))
         {
            (m_rVideoCallbacks.fpvNotifySessionStatus)(m_u32SelectedDevice,
               e8DEV_TYPE_MIRRORLINK,enSessionStatus);
         }//if(NULL != m_rVideoCallbacks.fpvNotifySessionStatus )

      }//if( m_enBlockingMode != coenBlockingMode) 

   }//if( (cou32DevId == m_u32SelectedDevice) && (NULL != spoMLViewerCmd) )

   return enErrorCode;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vSetOrientationMode()
***************************************************************************/
t_Void spi_tclMLVideo::vSetOrientationMode(const t_U32 cou32DevId,
                                           const tenOrientationMode coenOrientationMode,
                                           const trUserContext& corfrUsrCntxt)
{

   /*lint -esym(40,fpvSetOrientationModeCb)fpvSetOrientationModeCb Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLVideo:vSetOrientationMode()"));

   tenErrorCode enErrorCode = e8INVALID_DEV_HANDLE;

   //If the request is for currently selected device, then only forward request to VNC
   if(cou32DevId == m_u32SelectedDevice)
   {
      enErrorCode = e8NO_ERRORS;
      //If server doesn't support orientation switch, report Unsupported operation error
      if( ( true == m_bServerSupportsOrientationSwitch )&&( NULL != spoMLViewerCmd ) )
      {
         //If server supports orientation switch & Viewer returns error
         //report UNKNOWN Error
         if(false == spoMLViewerCmd->bSetOrientation(coenOrientationMode))
         {
            enErrorCode = e8UNKNOWN_ERROR;
         }//if(false == spoMLViewerCmd->bSetOrientation(coenOrientationMode))
      }//if( ( true == m_bServerSupportsOrientationSwitch )&&(NULL != spoMLViewerCmd) )
      else
      {
         //If the mode is landscape request send success even if the device doesn't support orientation switch.
         //default orientation mode is Landscape. it is mandatory that ML server starts session in Landscape mode.
         if(coenOrientationMode != e8LANDSCAPE_MODE)
         {
            enErrorCode = e8UNSUPPORTED_OPERATION;
         }//if(coenOrientationMode != e8LANDSCAPE_MODE)
      }
   }//if(cou32DevId == m_u32SelectedDevice)

   //@todo - revisit response handling in the next phases
   if(NULL != m_rVideoCallbacks.fpvSetOrientationModeCb)
   {
      (m_rVideoCallbacks.fpvSetOrientationModeCb)(cou32DevId,
         enErrorCode,corfrUsrCntxt,e8DEV_TYPE_MIRRORLINK);
   }// if(NULL != m_rVideoCallbacks.fpvCbSetVideoBlockingMode)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vSetVehicleConfig()
***************************************************************************/
t_Void spi_tclMLVideo::vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
                                         t_Bool bSetConfig)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vSetVehicleConfig: Vehicle Configuration-%d ",enVehicleConfig));

   switch(enVehicleConfig)
   {
   case e8PARK_MODE:
      {
         vSetVehicleMode(false);
      }
      break;
   case e8DRIVE_MODE:
      {
         vSetVehicleMode(true);
      }
      break;
   case e8_DAY_MODE:
      {
         vSetDayNightMode(false);
      }
      break;
   case e8_NIGHT_MODE:
      {
         vSetDayNightMode(true);
      }
      break;
   case e8_KEY_LOCK:
      {
         //bSetConfig : TRUE-Enable FALSE - Disable
         vEnableKeyLock(bSetConfig);
      }
      break;
   case e8_DEVICE_LOCK:
      {
         //bSetConfig : TRUE-Enable FALSE - Disable
         vEnableDeviceLock(bSetConfig);
      }
      break;
   case e8_SCREEN_SAVER:
      {
         //bSetConfig : TRUE-Enable FALSE - Disable
         vEnableScreenSaver(bSetConfig);
      }
      break;
   case e8_VOICE_RECOG:
      {
         //bSetConfig : TRUE-Enable FALSE - Disable
         vEnableVoiceRecognition(bSetConfig);
      }
      break;
   case e8_MICROPHONE_IN:
      {
         //bSetConfig : TRUE-Enable FALSE - Disable
         vEnableMicroPhoneIn(bSetConfig);
      }
      break;
   default:
      {
         //Nothing to do
      }
      break;
   }

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vSetVehicleMode()
***************************************************************************/
t_Void spi_tclMLVideo::vSetVehicleMode(t_Bool bEnableDriveMode)
{

   ETG_TRACE_USR1(("spi_tclMLVideo::vSetVehicleMode: Enable Drive Mode- %d",ETG_ENUM(BOOL,bEnableDriveMode)));
   m_bDriveModeEnabled = bEnableDriveMode;
   //m_bDriveModeEnabled became unused currently. If not required, remove the member variable
   //and this method in future
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vSetPixelFormat()
***************************************************************************/
t_Void spi_tclMLVideo::vSetPixelFormat(const t_U32 cou32DevId,
         const tenDeviceCategory coenDevCat,
         const tenPixelFormat coenPixelFormat)
{ 
    SPI_INTENTIONALLY_UNUSED(coenDevCat); 
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vSetPixelFormat(cou32DevId, coenPixelFormat);
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vSetDayNightMode()
***************************************************************************/
t_Void spi_tclMLVideo::vSetDayNightMode(t_Bool bEnableNightMode)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vSetDayNightMode:EnableNightMode-%d ",
      ETG_ENUM(BOOL,bEnableNightMode)));

   if( (m_bNightModeEnabled != bEnableNightMode) &&(NULL != spoMLViewerCmd))
   {
      spoMLViewerCmd->vSetDayNightMode(bEnableNightMode);
      m_bNightModeEnabled = bEnableNightMode;
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vEnableKeyLock(t_Bool bEnable)
***************************************************************************/
t_Void spi_tclMLVideo::vEnableKeyLock(t_Bool bEnable) const
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vEnableKeyLock: Enable key Lock -%d ",ETG_ENUM(BOOL,bEnable)));
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vEnableKeyLock(bEnable);
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vEnableDeviceLock(t_Bool bEnable)
***************************************************************************/
t_Void spi_tclMLVideo::vEnableDeviceLock(t_Bool bEnable) const
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vEnableDeviceLock: Enable Device Lock-%d",
      ETG_ENUM(BOOL,bEnable)));
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vEnableDeviceLock(bEnable);
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vEnableScreenSaver(t_Bool bEnable)
***************************************************************************/
t_Void spi_tclMLVideo::vEnableScreenSaver(t_Bool bEnable) const
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vEnableScreenSaver: Enable Screen Saver-%d ",
      ETG_ENUM(BOOL,bEnable)));
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vEnableScreenSaver(bEnable);
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vEnableVoiceRecognition(t_Bool bEnable)
***************************************************************************/
t_Void spi_tclMLVideo::vEnableVoiceRecognition(t_Bool bEnable) const
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vEnableVoiceRecognition: Enable Voice Recognition-%d ",
      ETG_ENUM(BOOL,bEnable)));
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vEnableVoiceRecognition(bEnable);
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vEnableMicroPhoneIn(t_Bool bEnable)
***************************************************************************/
t_Void spi_tclMLVideo::vEnableMicroPhoneIn(t_Bool bEnable) const
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vEnableMicroPhoneIn-%d ",
      ETG_ENUM(BOOL,bEnable)));
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vEnableMicroPhoneInput(bEnable);
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostDriverDistractionAvoidanceResult
***************************************************************************/
t_Void spi_tclMLVideo::vPostDriverDistractionAvoidanceResult(t_Bool bDriverDistAvoided,
                                                             const t_U32 cou32DevId)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vPostDriverDistractionAvoidanceResult: Driver Distraction Avoided -%d ",
      bDriverDistAvoided));
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostAppContextInfo()
***************************************************************************/
t_Void spi_tclMLVideo::vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo)
{
   m_oAppContextLock.s16Lock();
   //Current App context info is required, to send frame buffer blocking notifications.
   m_rAppCntxtInfo = corfrAppCntxtInfo ;
   m_oAppContextLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostDayNightModeInfo()
***************************************************************************/
t_Void spi_tclMLVideo::vPostDayNightModeInfo(t_Bool bNightModeEnabled)
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vPostDayNightModeInfo: Noght mode enabled-%d ",bNightModeEnabled));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostDisplayCapabilities()
***************************************************************************/
t_Void spi_tclMLVideo::vPostDisplayCapabilities(trDisplayCapabilities* pDispCapabilities,
                                const t_U32 cou32DevId )
{
   ETG_TRACE_USR1(("spi_tclMLVideo:vPostDisplayCapabilities "));

   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   if(
      (NULL != pDispCapabilities)
      &&
      (MLFramebufferConfigurationServerSideOrientationSwitch == 
      (pDispCapabilities->u16FrameBufferConfiguration & 
      MLFramebufferConfigurationServerSideOrientationSwitch))
      )
   {
      ETG_TRACE_USR4(("ML Server supports orientation switch"));
      m_bServerSupportsOrientationSwitch = true;
   }//if((NULL != pDispCapabilities)
}


/***************************************************************************
** FUNCTION:t_Void spi_tclMLVideo::vPostOrientationMode
***************************************************************************/
t_Void spi_tclMLVideo::vPostOrientationMode(tenOrientationMode enOrientationMode,
                                            const t_U32 cou32DevId)
{
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Current OrientationMode-%d ",
      ETG_ENUM(ORIENTATION_MODE,enOrientationMode)));

   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   m_enOrientatiobMode = enOrientationMode;
}

/***************************************************************************
** FUNCTION:t_Void spi_tclMLVideo::vPostDAPAttestResp()
***************************************************************************/
t_Void spi_tclMLVideo::vPostDAPAttestResp(trUserContext rUserContext,
                                            tenMLAttestationResult enAttestationResult,
                                            t_String szVNCAppPublickey,
                                            t_String szUPnPAppPublickey,
                                            t_String szVNCUrl,
                                            const t_U32 cou32DevId,
                                            t_Bool bVNCAvailInDAPAttestResp)
{
   ETG_TRACE_USR1(("spi_tclMLVideo::vPostDAPAttestResp:Dev-0x%x Result-%d Attestation Key-%s",
      cou32DevId,enAttestationResult,szVNCAppPublickey.c_str()));

   SPI_INTENTIONALLY_UNUSED(rUserContext);
   SPI_INTENTIONALLY_UNUSED(szUPnPAppPublickey);
   SPI_INTENTIONALLY_UNUSED(szVNCUrl);
   SPI_INTENTIONALLY_UNUSED(bVNCAvailInDAPAttestResp);

   m_oDevPublicKeyLock.s16Lock();
   //Public Key can be empty, though the DAP Attestation is successful.
   //It's observed with reference device(ML1.1)
   //So avoid sending content attestation enable request to Device with the empty public key.
   //sending this cause, immediate VNC Session termination, as soon as a app is launched.
   if((coszEmptyStr != szVNCAppPublickey) && (enAttestationResult == e8MLATTESTATION_SUCCESSFUL))
   {
      m_mapDevicePublicKey[cou32DevId] = szVNCAppPublickey.c_str();
   }//if(("" != szVNCAppPublickey) && (enAttestationResult == e8MLATTESTA
   m_oDevPublicKeyLock.vUnlock();


   //Fix for GMMY16-19383
   //Device is selected for the session, before the DAP attestation response is posted to Video class.
   //since the UPnP public key will not be available at that point of time, Content attestation will not be enabled.
   //this is a work around to enable content attestation,in case if the select device comes before the attestation
   //response is received.
   if( (true == m_bWLInitialized) && (cou32DevId == m_u32SelectedDevice) )
   {
       vEnableContentAttestation();
   }//if(true == m_bWLInitialized)


}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostDeviceDisconnected()
***************************************************************************/
t_Void spi_tclMLVideo::vPostDeviceDisconnected(const t_U32 cou32DevId)
{
   ETG_TRACE_USR1(("spi_tclMLVideo::vPostDeviceDisconnected: Device ID-0x%x",cou32DevId));

   m_oDevPublicKeyLock.s16Lock();
   if( (SPI_MAP_NOT_EMPTY(m_mapDevicePublicKey))&&
      (m_mapDevicePublicKey.end() != m_mapDevicePublicKey.find(m_u32SelectedDevice)))
   {
      m_mapDevicePublicKey.erase(m_mapDevicePublicKey.find(m_u32SelectedDevice));
   }//if( (m_mapDevicePublicKey.end() != m_mapDevicePublicKey.find(m_u32SelectedDevice)))
   m_oDevPublicKeyLock.vUnlock();
}

/***************************************************************************
** FUNCTION:t_Void spi_tclMLVideo::vEnableContentAttestation()
***************************************************************************/
t_Void spi_tclMLVideo::vEnableContentAttestation()
{
   ETG_TRACE_USR1(("spi_tclMLVideo::vEnableContentAttestation for Device - 0x%x",m_u32SelectedDevice));

   spi_tclVideoSettings* poVideoSettings = spi_tclVideoSettings::getInstance();
   if( (NULL != poVideoSettings)&&
      (static_cast<t_U8>(e8ATTEST_DISABLE) != poVideoSettings->u8GetContAttestSignFlag()) )
   {
      m_oDevPublicKeyLock.s16Lock();
      m_oContAttstLock.s16Lock();

      if( (false == m_bIsContAttEnabled)&&(SPI_MAP_NOT_EMPTY(m_mapDevicePublicKey))&&
         (m_mapDevicePublicKey.end() != m_mapDevicePublicKey.find(m_u32SelectedDevice))&&
         (NULL != spoMLViewerCmd) )
      {
         t_Char czContentAttestationSignatureFlag[MAX_KEYSIZE] = { 0 };
         snprintf(czContentAttestationSignatureFlag,MAX_KEYSIZE,"%d",poVideoSettings->u8GetContAttestSignFlag());

         spoMLViewerCmd ->vTriggerSetSessionParameters(NULL,czContentAttestationSignatureFlag,
            m_mapDevicePublicKey[m_u32SelectedDevice].c_str());

         m_bIsContAttEnabled = true ;

      }// if( (m_mapDevicePublicKey.end() != m_mapDevicePublicKey.fin
      else
      {
         ETG_TRACE_ERR(("Device is not DAP attested/content attestation already enabled"));
      }

      m_oContAttstLock.vUnlock();
      m_oDevPublicKeyLock.vUnlock();
   }
   else
   {
      ETG_TRACE_USR4(("Content Attestation is disabled"));
   }

}

/***************************************************************************
** FUNCTION:t_Void spi_tclMLVideo::vDisableContentAttestation()
***************************************************************************/
t_Void spi_tclMLVideo::vDisableContentAttestation()
{
   ETG_TRACE_USR1(("spi_tclMLVideo::vDisableContentAttestation for Device - 0x%x",m_u32SelectedDevice));

   m_oContAttstLock.s16Lock();
   if((NULL != spoMLViewerCmd)&&(true == m_bIsContAttEnabled))
   {
      t_Char czContentAttestationSignatureFlag[MAX_KEYSIZE] = { 0 };
      snprintf(czContentAttestationSignatureFlag,MAX_KEYSIZE,"%d",e8ATTEST_DISABLE);
      spoMLViewerCmd ->vTriggerSetSessionParameters(NULL,czContentAttestationSignatureFlag,NULL);

      m_bIsContAttEnabled = false ;

   }//if(NULL != spoMLViewerCmd)
   m_oContAttstLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostContentAttestationFailureInfo()
***************************************************************************/
t_Void spi_tclMLVideo::vPostContentAttestationFailureInfo(
   tenContentAttestationFailureReason enFailureReason)
{
   ETG_TRACE_USR1(("spi_tclMLVideo::vPostContentAttestationFailureInfo: failure reason-0x%x",
      enFailureReason));

   t_U32 u32Reason = cos32ContAttestationNormalError;

   switch(enFailureReason)
   {
   case e32MLContentAttestationFailureNoSignature:
   case e32MLContentAttestationFailureFramebufferNotAttested:
   case e32MLContentAttestationFailureContextInformationNotAttested:
   case e32MLContentAttestationFailurePixelCountNotAttested:
      {
         //Currently ignoring all the errors, as the SDK takes care when fatal errors occurred like hash is not attested at all.
         //These error codes are given for the better control to VNC Client, but unfortunately many fields are not mandatory for server
         //in spec, so we can't really rely on this. so at the moment, we are not terminating the Session in case of any non fatal error.
         //Ignore all the errors.

         /*
         //fatal error's terminate the session
         u32Reason = cos32ContAttestationFatalError ;
         */
         //Remove this, when above code is uncommented
         SPI_INTENTIONALLY_UNUSED(cos32ContAttestationFatalError);
      }
      break;
   case e32MLContentAttestationFailureErrorNotImplemented:
   case e32MLContentAttestationFailureErrorNoSessionKey:
   case e32MLContentAttestationFailureErrorOther: //check whether we may need single window counter
   case e32MLContentAttestationFailureErrorUnknown: //check whether we may need single window counter
      {
         // Server doesn't support attestation or there are some errors on server in attesting, 
         // disable content attestation
         vDisableContentAttestation();
      }
      break;
   case e32MLContentAttestationFailureNoResponse:
   default:
      {
         //Nothing to do 
      }
      break;
   }//switch(enFailureReason)
   

   //send the ContentAttestationFailureResult request to SDK.
   //if u32Reason is 0, then the session will be terminated,
   //if u32Reason is not equal to 0, then the session continues and the same error won't be informed 
   //in this session.
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vSendContentAttestationFailureResult(u32Reason);
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVideo::vSetPixelResolution()
***************************************************************************/
t_Void spi_tclMLVideo::vSetPixelResolution(const tenPixelResolution coenPixResolution)
{
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vSetPixelResolution(coenPixResolution);
   }//if(NULL != spoMLViewerCmd)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostViewerSessionProgress
***************************************************************************/
t_Void spi_tclMLVideo::vPostViewerSessionProgress(tenSessionProgress enSessionProgress,
                                                  const t_U32 cou32DeviceHandle)
{
   /*lint -esym(40,fpvNotifySessionStatus)fpvNotifySessionStatus Undeclared identifier */
   
    SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   if(enSessionProgress == e16_SESSION_PROGRESS_SESSION_ESTABLISHED)
   {
      ETG_TRACE_USR3_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Viewer Session established successfully"));

      m_bMLSessionEstablished = true;

      //Session is established successfully. Inform HMI, with the session status update 
      if( ( scou32InvalidDevID != m_u32SelectedDevice)&&
         (NULL != m_rVideoCallbacks.fpvNotifySessionStatus) )
      {
         tenSessionStatus enSessionStatus = e8_SESSION_ACTIVE;

         if(e8ENABLE_BLOCKING == m_enBlockingMode)
         {
            //Video blocking is enabled, before the first application is launched,
            //Send stop frame buffer updates request to Phone and send the session status as suspended to HMI.
            if(NULL != spoMLViewerCmd)
            {
               spoMLViewerCmd->vEnableFrameBufferUpdates(false);
            }//if(NULL != spoMLViewerCmd)
            enSessionStatus = e8_SESSION_SUSPENDED ;
         }//if(e8ENABLE_BLOCKING == m_enBlockingMode)

         (m_rVideoCallbacks.fpvNotifySessionStatus)(m_u32SelectedDevice,
            e8DEV_TYPE_MIRRORLINK,enSessionStatus);

      }//if(NULL != m_rVideoCallbacks.fpvNotifySessionStatus )

      //By default SDK enables day mode, during the initial handshake
      //So if the current mode is night mode, send a device status request to phone
      if( (NULL != spoMLViewerCmd) && ( true == m_bNightModeEnabled) )
      {
         spoMLViewerCmd->vSetDayNightMode(m_bNightModeEnabled);
      }//if( (NULL != spoMLViewerCmd) && ( true == m_bNightModeEnabled) )

   }//if(enSessionProgress == tenSessionProgress)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclMLVideo::vOnSelectDeviceResult(const t_U32 cou32DevId,
                                             const tenDeviceConnectionReq coenConnReq,
                                             const tenResponseCode coenRespCode)
{
   /*lint -esym(40,fpvNotifySessionStatus)fpvNotifySessionStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLVideo::vOnSelectDeviceResult:Device ID-0x%x Selection Type-%d, RespCode-%d",
      cou32DevId,ETG_ENUM(CONNECTION_REQ,coenConnReq),coenRespCode));

   //Device selection is failed. Clear the video resources,if already allocated
   if ( (e8FAILURE == coenRespCode) && (e8DEVCONNREQ_SELECT == coenConnReq) )
   {
      vUnInitializeMLViewerSession(m_u32SelectedDevice);
   } //if ( (e8FAILURE == coenRespCode) && (e8DEVCONNREQ_SELECT == coenConnReq) )

   //When a device is selected/deselected, send the session status update as InActive.
   if( (e8SUCCESS == coenRespCode)&&(NULL != m_rVideoCallbacks.fpvNotifySessionStatus) )
   {
      (m_rVideoCallbacks.fpvNotifySessionStatus)(m_u32SelectedDevice,
         e8DEV_TYPE_MIRRORLINK,e8_SESSION_INACTIVE);
   }//if(NULL != m_rVideoCallbacks.fpvNotifySessionStatus )
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vPostDeviceStatusMessages()
***************************************************************************/
t_Void spi_tclMLVideo::vPostDeviceStatusMessages(t_U32 u32DeviceStatusFeature, 
   const t_U32 cou32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVideo:: vPostDeviceStatusMessages:Device Status Feature-0x%x",
      u32DeviceStatusFeature));

   if(m_bDeviceStatusUpdate == false)
   {
      if(NULL != spoMLViewerCmd)
      {
      spoMLViewerCmd->vSetDayNightMode(m_bNightModeEnabled);
      trUserContext corUserContext;
      spoMLViewerCmd-> vTriggerSetDriverDistractionAvoidance(
         m_bDriveModeEnabled,  m_u32SelectedDevice);
      
   }
   m_bDeviceStatusUpdate  = true;
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVideo::vSetClientCapabilities(const trClient...)
***************************************************************************/
t_Void spi_tclMLVideo::vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities)
{
   ETG_TRACE_USR1(("spi_tclMLVideo::vSetClientCapabilities entered "));
   if(NULL != spoMLViewerCmd)
   {
      spoMLViewerCmd->vSetClientCapabilities(corfrClientCapabilities);
   }
}



#if defined(_lint) && (_lint <= 912) // Versions up to and including 9.00L
//lint +e10   re-activate due to above deactivation.
//lint +e19   re-activate due to above deactivation.
//lint +e40   re-activate due to above deactivation.
//lint +e48   re-activate due to above deactivation.
//lint +e55   re-activate due to above deactivation.
//lint +e58   re-activate due to above deactivation.
//lint +e63   re-activate due to above deactivation.
//lint +e64   re-activate due to above deactivation.
//lint +e1013 re-activate due to above deactivation.
//lint +e1055 re-activate due to above deactivation.
//lint +e601  re-activate due to above deactivation.
//lint +e1401 re-activate due to above deactivation.
//lint +e808  re-activate due to above deactivation.
//lint +e515  re-activate due to above deactivation.
//lint +e516  re-activate due to above deactivation.
//lint +e746  re-activate due to above deactivation.
#elif defined(_lint)
#error Check if deactivation / re-activation is necessary with new Lint version
#endif
//lint �restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>

