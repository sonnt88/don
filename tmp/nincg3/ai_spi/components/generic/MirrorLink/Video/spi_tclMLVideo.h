/***********************************************************************/
/*!
* \file  spi_tclMLVideo.h
* \brief Mirror Link Video Implementation
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Mirror Link Video Implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
21.01.2014  | Shiva Kumar Gurija    | Updated with Video Response Interface
03.04.2013  | Shiva Kumar Gurija    | Device status messages
16.04.2013  | Shiva Kumar Gurija    | Content Attestation
16.07.2014  | Shiva Kumar Gurija    | Implemented SessionStatusInfo update
06.11.2014  |  Hari Priya E R       | Added changes to set Client key capabilities
\endverbatim
*************************************************************************/

#ifndef _SPI_TCLMLVIDEO_H_
#define _SPI_TCLMLVIDEO_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"
#include "BaseTypes.h"
#include "spi_tclMLVncRespViewer.h"
#include "spi_tclMLVncRespDAP.h"
#include "spi_tclMLVncRespDiscoverer.h"
#include "spi_tclVideoDevBase.h"
#include "Lock.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
class Timer;

typedef struct rWLInitializeData
{
   t_U32 u32LayerId;
   t_U32 u32SurfaceId;
   trScreenOffset rMLScreenOffset;
   trScreenSize rMLScreenSize;
   t_U8 u8EnableTouchInputEvents;
   t_Bool bServerSideScalingRequired;
   t_U32 u32ScreenWidth_Mm;
   t_U32 u32ScreenHeight_Mm;

   rWLInitializeData():
   u32LayerId(0),
      u32SurfaceId(0),
      u8EnableTouchInputEvents(0),
      bServerSideScalingRequired(false),
      u32ScreenWidth_Mm(0),
      u32ScreenHeight_Mm(0)
   {

   }
}trWLInitializeData;

/****************************************************************************/
/*!
* \class spi_tclMLVideo
* \brief Mirror Link Video Implementation
* This class interacts with ADIT & Real VNC SDK to render the ML video
****************************************************************************/
class spi_tclMLVideo:public spi_tclVideoDevBase,public spi_tclMLVncRespViewer,public spi_tclMLVncRespDAP,public spi_tclMLVncRespDiscoverer
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVideo::spi_tclMLVideo()
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVideo()
   * \brief   Default Constructor
   * \sa      ~spi_tclMLVideo()
   **************************************************************************/
   spi_tclMLVideo();

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVideo::~spi_tclMLVideo()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclMLVideo()
   * \brief   Destructor
   * \sa      spi_tclMLVideo()
   **************************************************************************/
   ~spi_tclMLVideo();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVideo::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the ML Video related things
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief   To Uninitialize all the ML Video related things
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vUninitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclMLVideo::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML Video
   * \param   corfrVideoCallbacks : [IN] Video callabcks structure
   * \retval  t_Void 
   **************************************************************************/
   t_Void vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSelectDevice(const t_U32 cou32DevId,
   *          const tenDeviceConnectionReq coenConnReq)
   * \brief   To Initialize/UnInitialize Video setup for the currently selected device
   * \pram    cou32DevId : [IN] Unique Device Id
   * \param   coenConnReq : [IN] connected/disconnected
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVideo::bLaunchVideo()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bLaunchVideo(const t_U32 cou32DevId,
   *                 const t_U32 cou32AppId,
   *                 const tenEnabledInfo coenSelection)
   * \brief   To Launch the Video for the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    cou32AppId  : [IN] Application Id
   * \pram    coenSelection  : [IN] Enable/disable the video
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bLaunchVideo(const t_U32 cou32DevId,
      const t_U32 cou32AppId,
      const tenEnabledInfo coenSelection);

   /***************************************************************************
   ** FUNCTION:  t_U32  spi_tclMLVideo::vStartVideoRendering()
   ***************************************************************************/
   /*!
   * \fn      t_Void vStartVideoRendering(t_Bool bStartVideoRendering)
   * \brief   Method send request to ML/DiPo Video eithr to start or stop
   *          Video Rendering
   * \pram    bStartVideoRendering : [IN] True - Start Video rendering
   *                                      False - Stop Video rendering
   * \retval  t_Void 
   **************************************************************************/
   t_Void vStartVideoRendering(t_Bool bStartVideoRendering);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vGetVideoSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vGetVideoSettings(const t_U32 cou32DevId,
   *                                  trVideoAttributes& rfrVideoAttributes
   * \brief  To get the current Video Settings.
   * \param  u32DeviceHandle    : [IN] Uniquely identifies the target Device.
   * \param  rfrVideoAttributes : [OUT]includes screen size & orientation.
   * \retval t_Void
   * \sa
   **************************************************************************/
   t_Void vGetVideoSettings(const t_U32 cou32DevId,
      trVideoAttributes& rfrVideoAttributes);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vSetServerAspectRatio()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetServerAspectRatio(const tenScreenAspectRatio& corfenScrAspRatio)
   * \brief  Interface to set the aspect ratio of Head Unit.
   * \param  corfenScrAspRatio : [IN] Screen Aspect Ratio
   * \retVal t_Void
   **************************************************************************/
   t_Void vSetServerAspectRatio(const tenScreenAspectRatio& corfenScrAspRatio);

   /***************************************************************************
   ** FUNCTION: tenErrorCode spi_tclMLVideo::enSetVideoBlockingMode()
   ***************************************************************************/
   /*!
   * \fn     tenErrorCode enSetVideoBlockingMode(const t_U32 cou32DevId,
   *         const tenBlockingMode coenBlockingMode)
   * \brief  Interface to set the display blocking mode.
   * \param  cou32DevId             : [IN] Uniquely identifies the target Device.
   * \param  coenBlockingMode       : [IN] Identifies the Blocking Mode.
   * \retval tenErrorCode
   **************************************************************************/
   tenErrorCode enSetVideoBlockingMode(const t_U32 cou32DevId,
      const tenBlockingMode coenBlockingMode);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vSetOrientationMode()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetOrientationMode(const t_U32 cou32DevId,
   *                                    const tenOrientationMode coenOrientationMode,
   *                                    const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  cou32DevId          : [IN] Uniquely identifies the target Device.
   * \param  coenOrientationMode : [IN] Orientation Mode Value.
   * \param  corfrUsrCntxt       : [IN] User Context
   * \retval t_Void
   **************************************************************************/
   t_Void vSetOrientationMode(const t_U32 cou32DevId,
      const tenOrientationMode coenOrientationMode,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
   *          t_Bool bSetConfig,
   *          const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the Vehicle configurations.
   * \param  [IN] enVehicleConfig : Identifies the Vehicle Configuration.
   * \param  [IN] bSetConfig      : Enable/Disable config
   **************************************************************************/
   t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vSetPixelFormat()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetPixelFormat
   * \brief  Method to set pixel format of the video dynamically
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  coenPixelFormat  : [IN] Pixel format to be set
   * \retval t_Void
   **************************************************************************/
   t_Void vSetPixelFormat(const t_U32 cou32DevId,
               const tenDeviceCategory coenDevCat,
               const tenPixelFormat coenPixelFormat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vSetPixelResolution()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution)
   * \brief  Method to set pixel resolution
   * \param  coenPixResolution: [IN] Client display resolution
   * \retval t_Void
   **************************************************************************/
   t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vSetClientCapabilities(const trClient...)
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetClientCapabilities(
                   const trClientCapabilities& corfrClientCapabilities)
   * \brief   To set the client capabilities
   * \param   corfrClientCapabilities: [IN]Client Capabilities
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities);
   
   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVideo& spi_tclMLVideo::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVideo& operator= (const spi_tclMLVideo &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclMLVideo& operator= (const spi_tclMLVideo &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVideo::spi_tclMLVideo(const spi_tclMLVideo..
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVideo(const spi_tclMLVideo &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclMLVideo(const spi_tclMLVideo &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPopulateWLInitializeData()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPopulateWLInitializeData()
   * \brief   To Fetch the Layer related details on startup
   * \retval  t_Void
   **************************************************************************/
   t_Void vPopulateWLInitializeData();


   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVideo::bInitializeMLViewerSession()
   ***************************************************************************/
   /*!
   * \fn      t_Void bInitializeMLViewerSession(const t_U32 cou32DevID)
   * \brief   To Initialize the Viewer & wayland
   * \retval  t_Bool
   * \sa      vUnInitializeMLViewerSession()
   **************************************************************************/
   t_Bool bInitializeMLViewerSession(const t_U32 cou32DevID);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vUnInitializeWayland()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUnInitializeMLViewerSession(const t_U32 cou32DevId)
   * \brief   To UnInitialize the wayland & Viewer
   * \param   cou32DevId : Unique Device Id
   * \retval  t_Void
   * \sa      vInitializeMLViewerSession()
   **************************************************************************/
   t_Void vUnInitializeMLViewerSession(const t_U32 cou32DevId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vStartTimerTouchEvents()
   ***************************************************************************/
   /*!
   * \fn      t_Void vStartTimerTouchEvents()
   * \brief   To start the timer
   * \retval  t_Void
   * \sa      vStopTimerTouchEvents()
   **************************************************************************/
   t_Void vStartTimerTouchEvents();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vStopTimerTouchEvents()
   ***************************************************************************/
   /*!
   * \fn      t_Void vStopTimerTouchEvents()
   * \brief   To stop the timer
   * \retval  t_Void
   * \sa      vStartTimerTouchEvents()
   **************************************************************************/
   t_Void vStopTimerTouchEvents();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVideo::bOnSendWLTouchEventsCb()
   ***************************************************************************/
   /*!
   * \fn      static t_Bool bOnSendWLTouchEventsCb(timer_t timerID , 
   *                                               t_Void *pObject, 
   *                                               const t_Void *pcoUserData)
   * \brief   Timer Callback Function to send wayland touch events
   * \param   timerID : [IN] Id of the timer for which the timer is expired
   * \param   pObject : [IN] pointer to the object for which the callback is related to
   * \param   pcoUserData : [IN] User context data
   * \retval  t_Bool
   * \sa      vStartPumpWLEventsTimer()
   **************************************************************************/
   static t_Bool bOnSendWLTouchEventsCb(timer_t timerID , 
      t_Void *pObject, 
      const t_Void *pcoUserData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostWLInitialiseError()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostWLInitialiseError()
   * \brief   Posts whether a particular Extension has been enabled or not
   * \param   pcMessage: [IN]Message indicating the failure in WL initialise
   * \param   cou32DevId : [IN]  Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostWLInitialiseError( t_String szMessage,
      const t_U32 cou32DevId = 0);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostViewerError()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostViewerError(t_U32 u32ViewerError)
   * \brief   Posts the Viewer Error message to SPI
   * \param   u32ViewerError : [IN]  Error code
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostViewerError(t_U32 u32ViewerError);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostDriverDistractionAvoidanceResult
   ***************************************************************************/
   /*!
   * \fn      vPostDriverDistractionAvoidanceResult()
   * \brief   Posts the device status callback after the driver distraction avoidance is
   *          enabled or disabled.
   * \param   bDriverDistAvoided : [IN] enabled/disabled
   * \param   cou32DevId         : [IN]  Device handle to identify the MLServer
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostDriverDistractionAvoidanceResult(t_Bool bDriverDistAvoided,
      const t_U32 cou32DevId = 0);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostAppContextInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo)
   * \brief   Posts the context info of the application on the FG
   * \param   corfrAppCntxtInfo : [IN]  Application context info
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vSetVehicleMode()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleMode(t_Bool bEnableDriveMode)
   * \brief  Interface to set the Vehicle mode of the projected display.
   * \param  bEnableDriveMode   : [IN] TRUE - Enable Drive Mode
   *                                   FALSE - Enable Park Mode
   * \retval t_Void
   **************************************************************************/
   t_Void vSetVehicleMode(t_Bool bEnableDriveMode);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vSetDayNightMode()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetDayNightMode(t_Bool bEnableNightMode)
   * \brief  Interface to set the DayNight mode of the projected display.
   * \param  bEnableNightMode   : [IN] TRUE - Enable Night Mode
   *                                   FALSE - Enable Day Mode
   * \retval t_Void
   **************************************************************************/
   t_Void vSetDayNightMode(t_Bool bEnableNightMode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostDayNightModeInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostDayNightModeInfo(t_Bool bNightModeEnabled)
   * \brief   Posts the day night mode info
   * \param   bNightModeEnabled : [IN]  True - If Night mode is enabled
   *                                    False - If Night Mode is disabled
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostDayNightModeInfo(t_Bool bNightModeEnabled);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vEnableKeyLock(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableKeyLock(t_Bool bEnable)
   * \brief  Interface to Enable/disable Key lock (pointer& touch events from Phone)
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable the Key Lock
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableKeyLock(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vEnableDeviceLock(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableDeviceLock(t_Bool bEnable)
   * \brief  Interface to Enable/disable Device lock 
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable 
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableDeviceLock(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostDisplayCapabilities()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostDisplayCapabilities(trDisplayCapabilities* pDispCapabilities,
   *                                              const t_U32 cou32DevId = 0)
   * \brief   Posts the server Display capabilities to the SPI
   * \param   pDispCapabilities: [IN]Display Capabilities
   * \param   cou32DevId       : [IN]  Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostDisplayCapabilities(
      trDisplayCapabilities* pDispCapabilities,
      const t_U32 cou32DevId = 0);

   /***************************************************************************
   ** FUNCTION:t_Void spi_tclMLVideo::vPostOrientationMode
   ***************************************************************************/
   /*!
   * \fn     vPostOrientationMode(
   *               tenOrientationMode enOrientationMode, const t_U32 cou32DevId = 0)
   * \brief   Posts the Orientation Mode to the SPI
   * \param   enOrientationMode: [IN]Orientation Mode
   * \param   cou32DevId        : [IN]  Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostOrientationMode(tenOrientationMode enOrientationMode,
      const t_U32 cou32DevId = 0);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vEnableScreenSaver(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableScreenSaver(t_Bool bEnable)
   * \brief  Interface to Enable/disable Screen saver
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable the Key Lock
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableScreenSaver(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vEnableVoiceRecognition(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableVoiceRecognition(t_Bool bEnable)
   * \brief  Interface to Enable/disable Voice Recognition
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable 
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableVoiceRecognition(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVideo::vEnableMicroPhoneIn(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableMicroPhoneIn(t_Bool bEnable)
   * \brief  Interface to Enable/disable MicroPhoneIn
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable 
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableMicroPhoneIn(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION:t_Void spi_tclMLVideo::vPostDAPAttestResp()
   ***************************************************************************/
   /*!
   * \fn    t_Void vPostDAPAttestResp(trUserContext rUsrCntext,
   *            tenMLAttestationResult enAttestationResult,
   *            t_String szVNCAppPublickey,
   *            t_String szUPnPAppPublickey,
   *            t_String szVNCUrl,
   *            const t_U32 cou32DeviceHandle,
   *            t_Bool bVNCAvailInDAPAttestResp)
   * \brief   posts result of DAP request is complete
   * \param   rUsrCntext             : [IN] Context information passed from the caller
   * \param   enAttestationResult    : [IN] DAP attestation response
   * \param   szVNCAppPublickey      :[IN] Only for the component TerminalMode:VNC-Server
   *                                  Application public key will be sent.
   * \param   szUPnPAppPublickey     :[IN] Only for the component TerminalMode:UPnP-Server
   *                                  Application public key will be sent.
   * \param   szVNCUrl               :[IN] VNC Server URL
   * \param   cou32DeviceHandle      :[IN] Device handle
   * \param  bVNCAvailInDAPAttestResp:[IN] VNC-Server is listed in DAP attestation response
   * \retval t_Void
   **************************************************************************/
   t_Void vPostDAPAttestResp(trUserContext rUsrCntext,
      tenMLAttestationResult enAttestationResult,
      t_String szVNCAppPublickey,
      t_String szUPnPAppPublickey,
      t_String szVNCUrl,
      const t_U32 cou32DeviceHandle,
      t_Bool bVNCAvailInDAPAttestResp);

   /***************************************************************************
   ** FUNCTION:t_Void spi_tclMLVideo::vEnableContentAttestation()
   ***************************************************************************/
   /*!
   * \fn       t_Void vEnableContentAttestation()
   * \brief    Enable the content attestation
   * \retval   t_Void
   **************************************************************************/
   t_Void vEnableContentAttestation();

   /***************************************************************************
   ** FUNCTION:t_Void spi_tclMLVideo::vDisableContentAttestation()
   ***************************************************************************/
   /*!
   * \fn       t_Void vDisableContentAttestation()
   * \brief    Disable the content attestation
   * \retval   t_Void
   **************************************************************************/
   t_Void vDisableContentAttestation();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostDeviceDisconnected()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostDeviceDisconnected(const t_U32 cou32DevId)
   * \brief   To Post the Device Id to SPI, when a device is disconnected
   * \param   cou32DevId    : [IN] Device Id
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostDeviceDisconnected(const t_U32 cou32DevId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostContentAttestationFailureInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostContentAttestationFailureInfo(
   *            tenContentAttestationFailureReason enFailureReason)
   * \brief   Posts the Content attestation failure info to SPI
   * \param   enFailureReason : [IN]  Failure reason
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostContentAttestationFailureInfo(
      tenContentAttestationFailureReason enFailureReason);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostViewerSessionProgress
   ***************************************************************************/
   /*!
   * \fn      vPostViewerSessionProgress( tenSessionProgress enSessionProgress, 
   *                       const t_U32 cou32DeviceHandle = 0);
   * \brief   Posts the VNC Viewer Session establishment Progress
   * \param   enSessionProgress; [IN]Session Progress status
   * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostViewerSessionProgress(tenSessionProgress enSessionProgress, 
      const t_U32 cou32DeviceHandle = 0);
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVideo::vPostDeviceStatusCallabck
   ***************************************************************************/
   /*!
   * \fn      vPostDeviceStatusCallabck( ...);
   * \brief   Posts the VNC Viewer Session establishment Progress
   * \param   enSessionProgress; [IN]Session Progress status
   * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostDeviceStatusMessages(t_U32 u32DeviceStatusFeature,
      const t_U32 cou32DeviceHandle=0);


   //Currently connected device 
   t_U32 m_u32SelectedDevice;

   //! Flag used to avoid multiple wayland initializations
   t_Bool m_bWLInitialized;

   //! Member variable to restrict Processing command string only once for a device
   t_Bool m_bCmdStringProcessed;

   //! Video Callbacks structure
   trVideoCallbacks m_rVideoCallbacks;

   //!Layer related data
   trWLInitializeData m_rWLInitializeData;

   //! Context info of the app in FG
   trAppContextInfo m_rAppCntxtInfo;

   //! Member variable to say whether the server supports orientation switch or not
   t_Bool m_bServerSupportsOrientationSwitch;

   //! member variable to maintain the current orientation mode
   tenOrientationMode m_enOrientatiobMode;

   /*!
   * \brief Map to store 64-bit encoded public keys of the device.
   * This is required for DAP attestation and we get this info from DAP Attestation response
   */
   std::map<t_U32,t_String> m_mapDevicePublicKey;

   //! Locks variable for thread synchronization
   //To protect application context info update
   Lock m_oAppContextLock;
   //to protect UPnP application public key data structure.
   Lock m_oDevPublicKeyLock;

   //To protect the m_bIsContAttEnabled flag. this is accessed from two threads
   Lock m_oContAttstLock;

   //! variable to maintain the current blocking mode
   tenBlockingMode m_enBlockingMode;

   //! Flag to maintain whether the ML session is established or not
   t_Bool m_bMLSessionEstablished;

   //! Flag to maintain whether the current mode is day mode or night mode
   t_Bool m_bNightModeEnabled;

   //! Flag to maintain whether the vehicle is Park mode or Drive mode
   t_Bool m_bDriveModeEnabled;

   //! variable to set device status
   t_Bool m_bDeviceStatusUpdate;

   ////! Flag to maintain the status of content attestation
   t_Bool m_bIsContAttEnabled;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclMLVideo

#endif //_SPI_TCLMLVIDEO_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>

