/*!
 *******************************************************************************
 * \file             spi_tclMLConnection.cpp
 * \brief            ML Connection class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Mirrorlink Connection class implements connection Management for
 Mirrorlink capable devices. This class must be derived from Connection base
 connection class.
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 24.04.2014 |  Shiva kumar Gurija          | XML Validation
 27.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors


 \endverbatim
 ******************************************************************************/
#define VNC_USE_STDINT_H

#include "RespBase.h"
#include "spi_tclMLVncManager.h"
#include "spi_tclMLConnection.h"
#include "spi_tclConnSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclMLConnection.cpp.trc.h"
#endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported


/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::spi_tclMLConnection
 ***************************************************************************/
spi_tclMLConnection::spi_tclMLConnection():m_bMLSwitchInProgress(false)
{
   ETG_TRACE_USR1((" spi_tclMLConnection::spi_tclMLConnection() entered \n"));
   spi_tclMLVncManager *pVncManager = spi_tclMLVncManager::getInstance();
   if (NULL != pVncManager)
   {
      pVncManager->bRegisterObject((spi_tclMLVncRespDiscoverer*) this);
      pVncManager->bRegisterObject((spi_tclMLVncRespDAP*) this);
   }// if (NULL != pVncManager)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::spi_tclMLConnection
 ***************************************************************************/
spi_tclMLConnection::~spi_tclMLConnection()
{
   ETG_TRACE_USR1((" spi_tclMLConnection::~spi_tclMLConnection() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::bInitializeConnection
 ***************************************************************************/
t_Bool spi_tclMLConnection::bInitializeConnection()
{
   t_Bool bDiscInit = false;
   spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();

   //! Initialize discovery SDK
   if (NULL != poVncManager)
   {
      spi_tclMLVncCmdDiscoverer *poDiscoverer =
               poVncManager->poGetDiscovererInstance();
      if (NULL != poDiscoverer)
      {
         bDiscInit = poDiscoverer->bInitializeDiscoverers();
      }
   }

   //! Initialize DAP SDK and create DAP client
   t_Bool bRetDAPInit = false;
   if (NULL != poVncManager)
   {
      spi_tclMLVncCmdDAP *poDapVncCmd = poVncManager->poGetDAPInstance();
      if (NULL != poDapVncCmd)
      {
         bRetDAPInit = poDapVncCmd->bIntializeDAPSDK();
         if (true == bRetDAPInit)
         {
            bRetDAPInit = poDapVncCmd->bCreateDAPClient();
         }
         else
         {
            ETG_TRACE_ERR((" Initialization of DAP SDK failed \n"));
         }
      }
   }
   ETG_TRACE_USR2(("spi_tclMLConnection::bInitializeConnection bDiscInit =%d "
            "bRetDAPInit = %d \n", ETG_ENUM(BOOL,bDiscInit), ETG_ENUM(BOOL, bRetDAPInit)));
   return bDiscInit && bRetDAPInit;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::bStartDeviceDetection
 ***************************************************************************/
t_Bool spi_tclMLConnection::bStartDeviceDetection()
{
   t_Bool bDiscStart = false;
   spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();

   //!  start device discovery
   if (NULL != poVncManager)
   {
      spi_tclMLVncCmdDiscoverer *poDiscoverer =
               poVncManager->poGetDiscovererInstance();
      if (NULL != poDiscoverer)
      {
            poDiscoverer->vStartDeviceDiscovery();
            bDiscStart = true;
      }
   }
   ETG_TRACE_USR2(("spi_tclMLConnection::bStartDeviceDetection bDiscStart =%d ",
            ETG_ENUM(BOOL,bDiscStart)));
   return bDiscStart;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vUnInitializeConnection
 ***************************************************************************/
t_Void spi_tclMLConnection::vUnInitializeConnection()
{
   ETG_TRACE_USR1((" spi_tclMLConnection::vUnInitializeConnection() entered \n"));
   spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();

   //! stop device discovery and  Uninitialize discovery SDK
   if (NULL != poVncManager)
   {
      spi_tclMLVncCmdDiscoverer *poDiscoverer =
               poVncManager->poGetDiscovererInstance();
      if (NULL != poDiscoverer)
      {
         poDiscoverer->vStopDeviceDiscovery();
         poDiscoverer->vUnInitializeDiscoverers();
      }
   }

   //! Destroy DAP clients and Uninitialize DAP SDK
   if (NULL != poVncManager)
   {
      spi_tclMLVncCmdDAP *poDapVncCmd = poVncManager->poGetDAPInstance();
      if (NULL != poDapVncCmd)
      {
         poDapVncCmd->vDestroyDAPClient();
         poDapVncCmd->vUnIntializeDAPSDK();
      }
   }
}


/***************************************************************************
 ** FUNCTION:  spi_tclConnection::vOnSelectDevice
 ***************************************************************************/
t_Void spi_tclMLConnection::vOnSelectDevice(const t_U32 cou32DeviceHandle,
         tenDeviceConnectionType enDevConnType,
         tenDeviceConnectionReq enDevSelReq, tenEnabledInfo enDAPUsage,
         tenEnabledInfo enCDBUsage, t_Bool bIsHMITrigger,
         const trUserContext corUsrCntxt)
{
   /*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,u32DAPAppID)u32DAPAppID Undeclared identifier */
	/*lint -esym(40,fvSelectDeviceResult)fvSelectDeviceResult Undeclared identifier */

   ETG_TRACE_USR1((" spi_tclMLConnection::vOnSelectDevice() entered \n"));
   SPI_INTENTIONALLY_UNUSED(bIsHMITrigger);
   SPI_INTENTIONALLY_UNUSED(corUsrCntxt);
   SPI_INTENTIONALLY_UNUSED(enCDBUsage);
   SPI_INTENTIONALLY_UNUSED(enDevConnType);

   tenErrorCode enErrorCode = e8NO_ERRORS;

   if (e8DEVCONNREQ_SELECT == enDevSelReq)
   {
      //! Fetch project preferred setting for DAP
      tenDAPPreference enDAPPref = e8DAPPREF_UNKNOWN;
      spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
      if (NULL != poConnSettings)
      {
         enDAPPref = poConnSettings->enGetDAPPreference();
      } // if (NULL != poConnSettings)

      //! If there is a request for DAP then send the request to SDK
      if ((e8USAGE_ENABLED == enDAPUsage)
               && (e8DAPPREF_ON_REQUEST == enDAPPref))
      {
         auto itMapInfo = m_mapDapCdbInfo.find(cou32DeviceHandle);
         t_U32 u32AppID = 0;
         if (m_mapDapCdbInfo.end() != itMapInfo)
         {
            u32AppID = itMapInfo->second.u32DAPAppID;
         } //  if (m_mapDapCdbInfo.end() != itMapInfo)

         if (0 != u32AppID)
         {
            vRequestDAPLaunch(cou32DeviceHandle, u32AppID);
         }
         else
         {
            ETG_TRACE_ERR((" DAP Not Supported by device %d \n", cou32DeviceHandle));
         } // if (0 != u32AppID)
      } // if ((e8USAGE_ENABLED == enDAPUsage)

      //! Switch to Mirrolink mode if the device is not already in mirrorlink mode
      m_bMLSwitchInProgress = false;
      spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();
      if (NULL != poVncManager)
      {
         spi_tclMLVncCmdDiscoverer *poDiscCmd = poVncManager->poGetDiscovererInstance();

         if ((NULL != poDiscCmd) && (false == poDiscCmd->bisDeviceInMirrorlinkMode(cou32DeviceHandle)))
         {
            m_bMLSwitchInProgress = bRequestDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_MIRRORLINK);
           //! Currently Realvnc sdk is sending error for mirrorlink command even though device switches to mirrolrink mode.
       	   //! TODO: To be removed once Realvnc fixes the issue
            m_bMLSwitchInProgress = true;
            enErrorCode = (false == m_bMLSwitchInProgress) ? e8SELECTION_FAILED : e8NO_ERRORS;
         }
      }

      //! If the switch fails or if device is already in mirrorlink mode then send the result
      if (false == m_bMLSwitchInProgress)
      {
         //! Send the response if the device is already in Mirrorlink mode
         if (NULL != (m_rMLConnCallbacks.fvSelectDeviceResult))
         {
            (m_rMLConnCallbacks.fvSelectDeviceResult)(enErrorCode);
         }
      }
   }
   else if(e8DEVCONNREQ_DESELECT == enDevSelReq)
   {
      //! Send the response if the device is already in Mirrorlink mode
      if (NULL != (m_rMLConnCallbacks.fvSelectDeviceResult))
      {
         (m_rMLConnCallbacks.fvSelectDeviceResult)(enErrorCode);
      }
   }

   //TODO: For PSA, if DAP fails false result has to be sent to Connection Manager

}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vRegisterCallbacks
 ***************************************************************************/
t_Void spi_tclMLConnection::vRegisterCallbacks(trConnCallbacks &ConnCallbacks)
{
   ETG_TRACE_USR1((" spi_tclMLConnection::vRegisterCallbacks() entered \n"));
   m_rMLConnCallbacks = ConnCallbacks;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::bSetDevProjUsage
 ***************************************************************************/
t_Bool spi_tclMLConnection::bSetDevProjUsage(tenEnabledInfo enServiceStatus)
{
   ETG_TRACE_USR1((" spi_tclMLConnection::bSetDevProjUsage() enServiceStatus = %d \n",
            ETG_ENUM(ENABLED_INFO,enServiceStatus)));
   spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();
   if (NULL != poVncManager)
   {
      spi_tclMLVncCmdDiscoverer *poDiscCmd = poVncManager->poGetDiscovererInstance();
      if (NULL != poDiscCmd)
      {
         //! Currently start stop of discoverer doesn't work
        /* bServiceStatus ?
                  poDiscCmd->vStartDeviceDiscovery() :
                  poDiscCmd->vStopDeviceDiscovery();*/
      }
   }
   //! TODO to be changed when ON/Off feature is supported
   return true;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::bRequestDeviceSwitch
 ***************************************************************************/
t_Bool spi_tclMLConnection::bRequestDeviceSwitch(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1((" spi_tclMLConnection::bRequestDeviceSwitch() cou32DeviceHandle = 0x%x enDevCat = %d\n",
            cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDevCat)));
   spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();
   t_Bool bRetval = false;
   if (NULL != poVncManager)
   {
      spi_tclMLVncCmdDiscoverer *poDiscCmd = poVncManager->poGetDiscovererInstance();
      if (NULL != poDiscCmd)
      {
         bRetval = poDiscCmd->bRequestMirrorlinkSwitch(cou32DeviceHandle);
      }
   }
   return bRetval;
}
/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vOnDeviceConnection
 ***************************************************************************/
t_Void spi_tclMLConnection::vOnDeviceConnection(const t_U32 cou32DeviceHandle,
         const trDeviceInfo &corfrDeviceInfo)
{

	/*lint -esym(40,fvDeviceConnection)fvDeviceConnection Undeclared identifier */
   /*lint -esym(40,fvSelectDeviceResult)fvSelectDeviceResult Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclMLConnection::vOnDeviceConnection() entered \n"));

   //! Call the registered callbacks
   if (NULL != (m_rMLConnCallbacks.fvDeviceConnection))
   {
      (m_rMLConnCallbacks.fvDeviceConnection)(cou32DeviceHandle, corfrDeviceInfo, e8DEV_TYPE_MIRRORLINK);
   }

   //! Send device selection result if mirrorlink switch is initiated as part of select device request
   if((true == m_bMLSwitchInProgress) && (e8DEV_TYPE_MIRRORLINK == corfrDeviceInfo.enDeviceCategory))
   {
      //! Send the response after device is switched to Mirrolink mode
      if (NULL != (m_rMLConnCallbacks.fvSelectDeviceResult))
      {
         (m_rMLConnCallbacks.fvSelectDeviceResult)(e8NO_ERRORS);
      }
      m_bMLSwitchInProgress = false;
   }

}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vOnDeviceDisconnection
 ***************************************************************************/
t_Void spi_tclMLConnection::vOnDeviceDisconnection(
         const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,fvDeviceDisconnection)fvDeviceDisconnection Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclMLConnection::vOnDeviceDisconnection() entered \n"));
   if (NULL != (m_rMLConnCallbacks.fvDeviceDisconnection))
   {
      (m_rMLConnCallbacks.fvDeviceDisconnection)(cou32DeviceHandle, e8DEV_TYPE_MIRRORLINK);
   }
}



/**** Methods overidden from spi_tclVncRespDiscoverer **********************/
/***************************************************************************
 ** FUNCTION:  vPostDeviceInfo::vPostDeviceInfo
 ***************************************************************************/
t_Void spi_tclMLConnection::vPostDeviceInfo(const t_U32 cou32DeviceHandle,
         const trMLDeviceInfo& corfrMLDeviceInfo)
{

	/*lint -esym(40,fvUpdateDAPStatus)fvUpdateDAPStatus Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclMLConnection::vPostDeviceInfo() cou32DeviceHandle = 0x%x \n", cou32DeviceHandle));
   ETG_TRACE_USR4(("********New Device Detected ************* \n"));
   ETG_TRACE_USR4(("cou32DeviceHandle= %u \n", cou32DeviceHandle));
   ETG_TRACE_USR4(("szDeviceUUID = %s \n", corfrMLDeviceInfo.szDeviceUUID.c_str()));
   ETG_TRACE_USR4(("szVnccmd = %s \n", corfrMLDeviceInfo.szVnccmd.c_str()));
   ETG_TRACE_USR4(("szFriendlyDeviceName = %s \n", corfrMLDeviceInfo.szFriendlyDeviceName.c_str()));
   ETG_TRACE_USR4(("szDeviceManufacturerName = %s \n", corfrMLDeviceInfo.szDeviceManufacturerName.c_str()));
   ETG_TRACE_USR4(("szDeviceModelName = %s \n", corfrMLDeviceInfo.szDeviceModelName.c_str()));
   ETG_TRACE_USR4(("szBTAddress = %s \n", corfrMLDeviceInfo.szBTAddress.c_str()));
   ETG_TRACE_USR4(("enDeviceCategory = %d \n", corfrMLDeviceInfo.enDeviceCategory));
   ETG_TRACE_USR4(("enDeviceConnectionStatus = %d \n", corfrMLDeviceInfo.enDeviceConnectionStatus));
   ETG_TRACE_USR4(("enDeviceConnectionType = %d \n", corfrMLDeviceInfo.enDeviceConnectionType));
   ETG_TRACE_USR4(("MLVersionInfo :  %d.%d \n \n", corfrMLDeviceInfo.rVersionInfo.u32MajorVersion, corfrMLDeviceInfo.rVersionInfo.u32MinorVersion));

   //! Copy device info to generic device info structure
   trDeviceInfo oDeviceInfo;
   vCopyDeviceInfo(cou32DeviceHandle, oDeviceInfo, corfrMLDeviceInfo);

   if(e8DEV_TYPE_UNKNOWN == corfrMLDeviceInfo.enDeviceCategory)
   {
      //! Post USB device for further processing
      vOnDeviceConnection(cou32DeviceHandle, oDeviceInfo);
   }
   else
   {
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   if (NULL != poConnSettings)
   {
      tenDAPPreference enDAPPref = poConnSettings->enGetDAPPreference();
      if (e8DAPPREF_ON_DEVICECONNECTION == enDAPPref)
      {
         m_mapMLDeviceInfo.insert(std::pair<t_U32, trDeviceInfo> (cou32DeviceHandle, oDeviceInfo));
         if (NULL != (m_rMLConnCallbacks.fvUpdateDAPStatus))
         {
            (m_rMLConnCallbacks.fvUpdateDAPStatus)(cou32DeviceHandle, e8DAP_UNKNOWN);
         }
      }
      else if(e8DAPPREF_ON_REQUEST == enDAPPref)
      {
         vOnDeviceConnection(cou32DeviceHandle, oDeviceInfo);
      }
   }

   //! Reset the DAP and CDB usage info if device is reconnected or new device is connected
   trDapCdbInfo rDapCdbInfo;
   m_mapDapCdbInfo[cou32DeviceHandle] = rDapCdbInfo;
   }

}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vPostDeviceDisconnected
 ***************************************************************************/
t_Void spi_tclMLConnection::vPostDeviceDisconnected(
         const t_U32 cou32DeviceHandle)
{
   vOnDeviceDisconnection(cou32DeviceHandle);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLConnection::vPostAppList(trUserContext..
***************************************************************************/
t_Void spi_tclMLConnection::vPostAppList(trUserContext rUserContext,
                                         const t_U32 cou32DeviceHandle,
                                         const std::vector<t_U32>& corfvecAppList )
{

	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,u32DAPAppID)u32DAPAppID Undeclared identifier */
	/*lint -esym(40,bIsDAPSupported)bIsDAPSupported Undeclared identifier */
	/*lint -esym(40,bIsCDBSupported)bIsCDBSupported Undeclared identifier */
	/*lint -esym(40,fvUpdateDAPStatus)fvUpdateDAPStatus	 Undeclared identifier */
   SPI_INTENTIONALLY_UNUSED(corfvecAppList);
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLConnection::vPostAppList:Dev-0x%x", cou32DeviceHandle));

   auto itMapCdbInfo = m_mapDapCdbInfo.find(cou32DeviceHandle);
   if ((m_mapDapCdbInfo.end() != itMapCdbInfo) && (0 == itMapCdbInfo->second.u32DAPAppID))
   {
      t_U32 u32DAPAppID = 0;
      t_U32 u32CDBAppID = 0;
      t_Bool bCDBSupported = false;
      t_Bool bDapSupported = false;
      spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();
      if (NULL != poVncManager)
      {
         spi_tclMLVncCmdDiscoverer *poVncDisc = poVncManager->poGetDiscovererInstance();
         if (NULL != poVncDisc)
         {
            bDapSupported = poVncDisc->bGetAppIDforProtocol(cou32DeviceHandle, "DAP", u32DAPAppID);
            bCDBSupported = poVncDisc->bGetAppIDforProtocol(cou32DeviceHandle, "CDB", u32CDBAppID);
         }
      }
      itMapCdbInfo->second.bIsDAPSupported = bDapSupported;
      if (true == bDapSupported)
      {
         itMapCdbInfo->second.u32DAPAppID = u32DAPAppID;
         spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
         if (NULL != poConnSettings)
         {
            tenDAPPreference enDAPPref = poConnSettings->enGetDAPPreference();
            if (e8DAPPREF_ON_DEVICECONNECTION == enDAPPref)
            {
               vRequestDAPLaunch(cou32DeviceHandle, u32DAPAppID);
            }
         }
      }

      itMapCdbInfo->second.bIsCDBSupported = bCDBSupported;
      itMapCdbInfo->second.u32CDBAppID = u32CDBAppID;

      if ((false == itMapCdbInfo->second.bIsDAPSupported) && NULL != (m_rMLConnCallbacks.fvUpdateDAPStatus))
      {
         (m_rMLConnCallbacks.fvUpdateDAPStatus)(cou32DeviceHandle, e8DAP_NOT_SUPPORTED);
         spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
         if (NULL != poConnSettings)
         {
            tenDAPPreference enDAPPref = poConnSettings->enGetDAPPreference();
            if (e8DAPPREF_ON_DEVICECONNECTION == enDAPPref)
            {
               auto itmapMLDevinfo = m_mapMLDeviceInfo.find(cou32DeviceHandle);
               if (m_mapMLDeviceInfo.end() != itmapMLDevinfo)
               {
                  vOnDeviceConnection(cou32DeviceHandle, itmapMLDevinfo->second);
               }
            }
         }
      }
   }
}


/****End of Methods overridden from spi_tclVncRespDiscoverer ***************/


/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vRequestDAPLaunch
 ***************************************************************************/
t_Void spi_tclMLConnection::vRequestDAPLaunch(const t_U32 cou32DeviceHandle,
         const t_U32 cou32DAPAppID) const
{

	/*lint -esym(40,fvUpdateDAPStatus)fvUpdateDAPStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLConnection::vRequestDAPLaunch cou32DeviceHandle= 0x%x cou32DAPAppID = %d \n",
            cou32DeviceHandle, cou32DAPAppID));
   if (NULL != (m_rMLConnCallbacks.fvUpdateDAPStatus))
   {
      (m_rMLConnCallbacks.fvUpdateDAPStatus)(cou32DeviceHandle,
               e8DAP_IN_PROGRESS);
   }

   spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();
   if (NULL != poVncManager)
   {
      spi_tclMLVncCmdDAP *poDapVncCmd = poVncManager->poGetDAPInstance();
      if (NULL != poDapVncCmd)
      {
         trUserContext rUserCtxt;
         poDapVncCmd->vLaunchDAP(rUserCtxt, cou32DAPAppID, cou32DeviceHandle);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vHandleAttestationResult
 ***************************************************************************/
t_Void spi_tclMLConnection::vHandleAttestationResult(const t_U32 cou32DeviceHandle,
         tenMLAttestationResult enAttestResult)
{

	/*lint -esym(40,fvUpdateDAPStatus)fvUpdateDAPStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLConnection::vHandleAttestationResult: Dev-0x%x enAttestResult = %d \n",
            cou32DeviceHandle, ETG_ENUM(DAP_ATTESTATION, enAttestResult)));

   if (NULL != (m_rMLConnCallbacks.fvUpdateDAPStatus))
   {

      tenDAPStatus enDAPStatus = e8DAP_FAILED;

      if((enAttestResult == e8MLATTESTATION_SUCCESSFUL) || (enAttestResult == e8MLXML_VALIDATION_SUCCESSFUL))
      {

         enDAPStatus = e8DAP_SUCCESS ;
         tenDAPPreference enDAPPref = e8DAPPREF_UNKNOWN;

         spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
         if (NULL != poConnSettings)
         {
            enDAPPref = poConnSettings->enGetDAPPreference();
         }

         //Request is to perform the DAP on device detection, update the device directly
         if ( (e8DAPPREF_ON_DEVICECONNECTION == enDAPPref) && (SPI_MAP_NOT_EMPTY(m_mapMLDeviceInfo)))
         {
            auto itmapMLDevinfo = m_mapMLDeviceInfo.find(cou32DeviceHandle);
            if(m_mapMLDeviceInfo.end() != itmapMLDevinfo)
            {
               vOnDeviceConnection(cou32DeviceHandle, itmapMLDevinfo->second);
            }//if(m_mapMLDeviceInfo.end() != itmapMLDevinfo)
         }//if ( (e8DAPPREF_ON_DEVICECONNECTION == enDAPPref) 

      }//if((enAttestResult == e8MLATTESTATION_SUCCESSFUL

      (m_rMLConnCallbacks.fvUpdateDAPStatus)(cou32DeviceHandle, enDAPStatus);
   }//if (NULL != (m_rMLConnCallbacks.fvUpdateDAPStatus))
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vCopyDeviceInfo
 ***************************************************************************/
t_Void spi_tclMLConnection::vCopyDeviceInfo(const t_U32 cou32DeviceHandle,
         trDeviceInfo &rfrDeviceInfo, const trMLDeviceInfo &corfrMLDeviceInfo) const
{
   ETG_TRACE_USR1((" spi_tclMLConnection::vCopyDeviceInfo() entered \n"));
   rfrDeviceInfo.u32DeviceHandle = cou32DeviceHandle;
   rfrDeviceInfo.rVersionInfo = corfrMLDeviceInfo.rVersionInfo;
   rfrDeviceInfo.szDeviceName = corfrMLDeviceInfo.szFriendlyDeviceName.c_str();
   rfrDeviceInfo.szDeviceManufacturerName = corfrMLDeviceInfo.szDeviceManufacturerName.c_str();
   rfrDeviceInfo.szDeviceModelName = corfrMLDeviceInfo.szDeviceModelName.c_str();
   rfrDeviceInfo.szBTAddress = corfrMLDeviceInfo.szBTAddress.c_str();
   rfrDeviceInfo.enDeviceCategory = corfrMLDeviceInfo.enDeviceCategory;
   rfrDeviceInfo.enDeviceConnectionStatus =
            corfrMLDeviceInfo.enDeviceConnectionStatus;
   rfrDeviceInfo.enDeviceConnectionType =
            corfrMLDeviceInfo.enDeviceConnectionType;
}

/********Inherited from spi_tclMLVncRespDAP*******************************/
/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vPostLaunchDAPRes
 ***************************************************************************/
t_Void spi_tclMLConnection::vPostLaunchDAPRes(trUserContext rUsrCntext,
                                              t_Bool bLaunchDAPRes, 
                                              const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR2(("spi_tclMLConnection::vPostLaunchDAPRes: cou32DeviceHandle= 0x%x bLaunchDAPRes =%d\n",
            cou32DeviceHandle, ETG_ENUM(BOOL,bLaunchDAPRes)));
   if (true == bLaunchDAPRes)
   {
      spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();
      if (NULL != poVncManager)
      {
         spi_tclMLVncCmdDAP *poDapVncCmd = poVncManager->poGetDAPInstance();
         if (NULL != poDapVncCmd)
         {

            poDapVncCmd->vRequestDAPAttestation(cou32DeviceHandle,rUsrCntext);
         }// if (NULL != poDapVncCmd)
      } //if (NULL != poVncManager)
   } //  if (true == bLaunchDAPRes)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLConnection::vPostDAPAttestResp
 ***************************************************************************/
t_Void spi_tclMLConnection::vPostDAPAttestResp(trUserContext rUsrCntext,
                                                 tenMLAttestationResult enAttestationResult,
                                                 t_String szVNCAppPublickey,
                                                 t_String szUPnPAppPublickey,
                                                 t_String szVNCUrl,
                                                 const t_U32 cou32DeviceHandle,
                                                 t_Bool bVNCAvailInDAPAttestResp)
{
   ETG_TRACE_USR1(("spi_tclMLConnection::vPostDAPAttestResp: cou32DeviceHandle = 0x%x enAttestationResult = %d \n",
            cou32DeviceHandle, ETG_ENUM(DAP_ATTESTATION,enAttestationResult)));
   SPI_INTENTIONALLY_UNUSED(rUsrCntext);
   SPI_INTENTIONALLY_UNUSED(szVNCAppPublickey);
   SPI_INTENTIONALLY_UNUSED(szVNCUrl);
   SPI_INTENTIONALLY_UNUSED(bVNCAvailInDAPAttestResp);

   t_Bool bXMLValidationEnabled = false;

   spi_tclConnSettings* poConnSettings = spi_tclConnSettings::getInstance();
   if(NULL != poConnSettings)
   {
      bXMLValidationEnabled = poConnSettings->bIsXMLValidationEnabled();
   }

   //Set the UPnP public Key, if the DAP attestation is successful and the XML validation is enabled.
   //else directly send the info to update the device list and HMI, if required
   if( (true == bXMLValidationEnabled)&&(e8MLATTESTATION_SUCCESSFUL == enAttestationResult) )
   {

      //@Note : Check what needs to be done, If the UPnP Public key is empty,when the XML validation is enabled?.
      // Currently allowing the user to use the device. (if the Key empty, XML validation wont be performed)
      //If it should not be allowed,send the XML validation failure as a reason to vHandleAttestationResult
      //SDK doesn't perform the XML validation, in case if the UPnP Public Key is not available.
      //if(false == szUPnPAppPublickey.empty())
      //{
      //   vHandleAttestationResult(cou32DeviceHandle,e8MLXML_VALIDATION_FAILED);
      //}

      spi_tclMLVncManager *poVncManager = spi_tclMLVncManager::getInstance();
      if (NULL != poVncManager)
      {
         spi_tclMLVncCmdDiscoverer *poDiscCmd = poVncManager->poGetDiscovererInstance();
         if (NULL != poDiscCmd)
         {
            poDiscCmd->vSetUPnPAppPublicKeyForXMLValidation(cou32DeviceHandle,
               szUPnPAppPublickey,rUsrCntext);
         }//if (NULL != poDiscCmd)
      }//if (NULL != poVncManager)
   }//if( (true == bXMLValidationEnabled)&&(e8ML
   else
   {
      //XML validation is not required or Device is not DAP attested, 
      //directly send the info to update the device list and HMI, if required
      vHandleAttestationResult(cou32DeviceHandle, enAttestationResult);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLConnection::vPostSetUPnPPublicKeyResp()
***************************************************************************/
t_Void spi_tclMLConnection::vPostSetUPnPPublicKeyResp(t_U32 u32DeviceHandle, 
                                                      t_Bool bXMLValidationSucceeded,
                                                      const trUserContext& corfrUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
   ETG_TRACE_USR1(("spi_tclMLConnection::vPostSetUPnPPublicKeyResp:Dev-0x%x,bResp-%d\n",
      u32DeviceHandle,ETG_ENUM(BOOL,bXMLValidationSucceeded)));

   //Check whether the UPnP key is set successfully or not
   tenMLAttestationResult enAttestResult = (true == bXMLValidationSucceeded)?
      e8MLXML_VALIDATION_SUCCESSFUL:e8MLXML_VALIDATION_FAILED ;

   vHandleAttestationResult(u32DeviceHandle,enAttestResult);
}
/******** end of Inherited from spi_tclMLVncRespDAP*******************************/
//lint –restore
