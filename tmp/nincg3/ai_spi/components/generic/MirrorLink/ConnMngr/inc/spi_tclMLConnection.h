/*!
 *******************************************************************************
 * \file             spi_tclMLConnection.h
 * \brief            ML Connection class
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Mirrorlink Connection class implements connection Management for
 Mirrorlink capable devices. This class must be derived from Connection base
 connection class.
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 24.04.2014 |  Shiva kumar Gurija          | XML Validation

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMLCONNECTION_H_
#define SPI_TCLMLCONNECTION_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <map>
#include "SPITypes.h"
#include "spi_tclConnection.h"
#include "spi_tclMLVncRespDiscoverer.h"
#include "spi_tclMLVncRespDAP.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
struct trDapCdbInfo
{
      t_U32 u32DAPAppID;
      t_U32 u32CDBAppID;
      t_Bool bIsDAPSupported;
      t_Bool bIsCDBSupported;

      trDapCdbInfo() :
                        u32DAPAppID(0),
                        u32CDBAppID(0),
                        bIsDAPSupported(false),
                        bIsCDBSupported(false)
      {

      }
};
/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLConnection
 * \brief Mirrorlink Connection class implements connection Management for
 *        Mirrorlink capable devices. This class must be derived from base
 *        connection class.
 */

class spi_tclMLConnection: public spi_tclConnection, //! Base Connection Class
         public spi_tclMLVncRespDiscoverer, //! Vnc Response class used for device detection and disconnection
         public spi_tclMLVncRespDAP //! For Attestation : To be removed after attestation class is in place
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::spi_tclMLConnection
       ***************************************************************************/
      /*!
       * \fn     spi_tclMLConnection()
       * \brief  Default Constructor
       * \sa      ~spi_tclMLConnection()
       **************************************************************************/
      spi_tclMLConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::~spi_tclMLConnection
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclMLConnection()
       * \brief  Destructor
       * \sa     spi_tclMLConnection()
       **************************************************************************/
      ~spi_tclMLConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::bInitializeConnection
       ***************************************************************************/
      /*!
       * \fn     bInitializeConnection()
       * \brief  Initialization of device detection and
       *         any other required initializations
       **************************************************************************/
      t_Bool bInitializeConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::bStartDeviceDetection
       ***************************************************************************/
      /*!
       * \fn     bStartDeviceDetection()
       * \brief  Starts device detection
       * \retval returns true on successful initialization and false on failure
       **************************************************************************/
      t_Bool bStartDeviceDetection();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::vUnInitializeConnection
       ***************************************************************************/
      /*!
       * \fn     vUnInitializeConnection()
       * \brief  Uninitialization of sdk's etc
       **************************************************************************/
      t_Void vUnInitializeConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  interface for the creator class to register for the required
       *        callbacks.
       * \param rfrConnCallbacks : reference to the callback structure
       *        populated by the caller
       **************************************************************************/
      t_Void vRegisterCallbacks(trConnCallbacks &ConnCallbacks);

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::bSetDevProjUsage
       ***************************************************************************/
      /*!
       * \fn     bSetDevProjUsage()
       * \brief  Called when the SPI featured is turned ON or OFF by the user
       * \param  enMirrorlinkStatus : Sets the particular SPI service ON or OFF
       **************************************************************************/
      t_Bool bSetDevProjUsage(tenEnabledInfo enServiceStatus);

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::bRequestDeviceSwitch
       ***************************************************************************/
      /*!
       * \fn     bRequestDeviceSwitch
       * \brief  Function to request device switch
       * \param  u32DeviceHandle : Uniquely identifies the target Device.
       **************************************************************************/
      t_Bool bRequestDeviceSwitch(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat);

   private:
      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      //! Callbacks for ConnMngr to register. These callbacks will be used to
      //! inform device detection and device disconnection to connection manager
      trConnCallbacks m_rMLConnCallbacks;

	  //! Stores CDB and DAP Info
      std::map<t_U32, trDapCdbInfo> m_mapDapCdbInfo;

      //! Stores Mirrorlink device info when required
      std::map<t_U32, trDeviceInfo> m_mapMLDeviceInfo;

      //! Indicates if mirrolink switch is in progress for a device
      t_Bool m_bMLSwitchInProgress;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::vOnDeviceConnection
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceConnection()
       * \brief  invoked on detection of new device
       **************************************************************************/
       t_Void vOnDeviceConnection(const t_U32 cou32DeviceHandle,
               const trDeviceInfo &corfrDeviceInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::vOnDeviceDisconnection
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceDisconnection()
       * \brief  invoked on disconnection of a connected device
       **************************************************************************/
       t_Void vOnDeviceDisconnection(const t_U32 cou32DeviceHandle);


       /***************************************************************************
        ** FUNCTION:  spi_tclConnection::vOnSelectDevice
        ***************************************************************************/
       /*!
        * \fn     vOnSelectDevice
        * \brief  Called when a device is selected by the user. This function evaluates
        *         DAP and CDB Usage and initiates DAP or CDB if required
        * \param  u32DeviceHandle : Uniquely identifies the target Device.
        * \param  enDevConnType   : Identifies the Connection Type.
        * \param  enDevSelReq    : Identifies the Connection Request.
        * \param  enDAPUsage      : Identifies Usage of DAP for the selected ML device.
        *              This value is not considered for de-selection of device.
        * \param  enCDBUsage      : Identifies Usage of CDB for the selected ML device.
        *              This value is not considered for de-selection of device
        * \param  bIsHMITrigger   : true if HMI has triggered select device
        * \param  corUsrCntxt      : User Context Details.
        **************************************************************************/
       t_Void vOnSelectDevice(const t_U32 cou32DeviceHandle,
                tenDeviceConnectionType enDevConnType,
                tenDeviceConnectionReq enDevSelReq,
                tenEnabledInfo enDAPUsage,
                tenEnabledInfo enCDBUsage,
                t_Bool bIsHMITrigger,
                const trUserContext corUsrCntxt);

      /**** Methods overridden from spi_tclVncRespDiscoverer **********************/
      /***************************************************************************
       ** FUNCTION:  vPostDeviceInfo::vPostDeviceInfo
       ***************************************************************************/
      /*!
       * \fn     vPostDeviceInfo()
       * \brief  Callback invoked on detection of new device
       * \sa     spi_tclVncRespDiscoverer::vPostDeviceInfo
       **************************************************************************/
      t_Void vPostDeviceInfo(const t_U32 cou32DeviceHandle,
               const trMLDeviceInfo& corfrMLDeviceInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::vPostDeviceDisconnected
       ***************************************************************************/
      /*!
       * \fn     vPostDeviceDisconnected()
       * \brief  callback invoked on disconnection of a connected device
       * \sa     spi_tclVncRespDiscoverer::vPostDeviceDisconnected
       **************************************************************************/
      t_Void vPostDeviceDisconnected(const t_U32 cou32DeviceHandle);

      /****End of Methods overridden from spi_tclVncRespDiscoverer ***************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::vRequestDAPLaunch
       ***************************************************************************/
      /*!
       * \fn     vRequestDAPLaunch()
       * \brief  Requests Device Attestation
       * \param  cou32DeviceHandle: Device Handle
       **************************************************************************/
      t_Void vRequestDAPLaunch(const t_U32 cou32DeviceHandle,const t_U32 cou32DAPAppID) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::vHandleAttestationResult
       ***************************************************************************/
      /*!
       * \fn     vHandleAttestationResult()
       * \brief  Receives DAP result with information of attestation
       **************************************************************************/
      t_Void vHandleAttestationResult(const t_U32 cou32DeviceHandle,
               tenMLAttestationResult enDAPResult);

      /***************************************************************************
       ** FUNCTION:  spi_tclMLConnection::vCopyDeviceInfo
       ***************************************************************************/
      /*!
       * \fn     vCopyDeviceInfo()
       * \brief  Copy Mirrorlink Device Info to Generic Device Info
       **************************************************************************/
      t_Void vCopyDeviceInfo(const t_U32 cou32DeviceHandle,
               trDeviceInfo &rfrDeviceInfo,
               const trMLDeviceInfo &corfrMLDeviceInfo) const;

      /********Inherited from spi_tclMLVncRespDAP*******************************/

      /***************************************************************************
      ** FUNCTION:t_Void spi_tclMLConnection::vPostLaunchDAPRes()
      ***************************************************************************/
      /*!
      * \fn    t_Void vPostLaunchDAPRes(trUserContext rUsrCntext, t_Bool bLaunchDAPRes,
      *        const t_U32 cou32DeviceHandle)
      * \brief   posts result of Launching DAP on MLServer
      * \param  rUsrCntext    :   [IN] Context information passed from the caller
      * \param   bLaunchDAPRes : true : Launch DAP on MLServer was successful
      *                          false : Launch DAP on MLServer failed
      * \param   cou32DeviceHandle :[IN] Device handle
      **************************************************************************/
      t_Void vPostLaunchDAPRes(trUserContext rUsrCntext,
         t_Bool bLaunchDAPRes, 
         const t_U32 cou32DeviceHandle);

      /***************************************************************************
      ** FUNCTION:t_Void spi_tclMLConnection::vPostDAPAttestResp()
      ***************************************************************************/
      /*!
      * \fn    t_Void vPostDAPAttestResp(trUserContext rUsrCntext,
      *            tenMLAttestationResult enAttestationResult,
      *            t_String szVNCAppPublickey,
      *            t_String szUPnPAppPublickey,
      *            t_String szVNCUrl,
      *            const t_U32 cou32DeviceHandle,
      *            t_Bool bVNCAvailInDAPAttestResp)
      * \brief   posts result of DAP request is complete
      * \param   rUsrCntext             : [IN] Context information passed from the caller
      * \param   enAttestationResult    : [IN] DAP attestation response
      * \param   szVNCAppPublickey      :[IN] Only for the component TerminalMode:VNC-Server
      *                                  Application public key will be sent.
      * \param   szUPnPAppPublickey     :[IN] Only for the component TerminalMode:UPnP-Server
      *                                  Application public key will be sent.
      * \param   szVNCUrl               :[IN] VNC Server URL
      * \param   cou32DeviceHandle      :[IN] Device handle
      * \param  bVNCAvailInDAPAttestResp:[IN] VNC-Server is listed in DAP attestation response
      * \retval t_Void
      **************************************************************************/
      t_Void vPostDAPAttestResp(trUserContext rUsrCntext,
         tenMLAttestationResult enAttestationResult,
         t_String szVNCAppPublickey,
         t_String szUPnPAppPublickey,
         t_String szVNCUrl,
         const t_U32 cou32DeviceHandle,
         t_Bool bVNCAvailInDAPAttestResp);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLConnection::vPostAppList(trUserContext..
      ***************************************************************************/
      /*!
      * \fn      virtual t_Void vPostAppList(const t_U32 cou32DeviceHandle,
      *                  const std::vector<t_U32>& corfvecAppList )
      * \brief   To Post the list of all applications supported by a device.
      * \param   rUserContext      : [IN] Context information passed from the caller
      * \param   cou32DeviceHandle : [IN] Device Id
      * \param   corfvecAppList    : [IN] List of applications
      * \retval  t_Void
      ***************************************************************************/
      t_Void vPostAppList(trUserContext rUserContext,
         const t_U32 cou32DeviceHandle,
         const std::vector<t_U32>& corfvecAppList );

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLConnection::vPostSetUPnPPublicKeyResp()
      ***************************************************************************/
      /*!
      * \fn      virtual t_Void vPostSetUPnPPublicKeyResp(t_U32 u32DeviceHandle,
      *                  t_Bool bXMLValidationSucceeded,const trUserContext& corfrUsrCntxt )
      * \brief   To Post the Set UPnP Public Key response
      * \param   u32DeviceHandle : [IN] Device Id
      * \param   bXMLValidationSucceeded : [IN] TRUE- Success
      *                                         FALSE - error in setting
      * \param   rUserContext      : [IN] Context information passed from the caller
      * \retval  t_Void
      ***************************************************************************/
      t_Void vPostSetUPnPPublicKeyResp(t_U32 u32DeviceHandle, 
         t_Bool bXMLValidationSucceeded,
         const trUserContext& corfrUsrCntxt);

      /********End of Inherited from spi_tclMLVncRespDAP **************************/
};
/*! } */
#endif // SPI_TCLMLCONNECTION_H_
