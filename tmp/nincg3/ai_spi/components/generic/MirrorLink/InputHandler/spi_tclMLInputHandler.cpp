/*!
 *******************************************************************************
 * \file              spi_tclMLInputHandler.cpp
 * \brief             SPI input handler for mirrorlink devices
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Input handler class to send input events from MLClient to
                MLServer for mirrorlink devices
 AUTHOR:        Hari Priya E R (RBEI/ECP2)
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.11.2013 |  Hari Priya E R              | Initial Version
 07.01.2014 |  Hari Priya E R              | Changes for touch handling
 03.07.2014 |  Hari Priya E R              | Changes for Input Response Interface
 23.09.2014 |  Hari Priya E R              | Added Conditional checks before sending key events 
 06.05.2015 |  Tejaswini HB                | Lint Fix
 25.06.2015 |  Sameer Chandra              | Added ML XDeviceKey Support for PSA

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#define VNC_USE_STDINT_H

#include "spi_tclMLVncManager.h"
#include "spi_tclMLInputHandler.h"
#include "spi_tclConfigReader.h"
#include "spi_tclMLVncCmdViewer.h"
#include "spi_tclInputRespIntf.h"


#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_INPUTHANDLER
#include "trcGenProj/Header/spi_tclMLInputHandler.cpp.trc.h"
#endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	


/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static spi_tclMLVncManager* spoMLVNCMngr = NULL;


/***************************************************************************
** FUNCTION:  spi_tclMLInputHandler::spi_tclMLInputHandler()
***************************************************************************/
spi_tclMLInputHandler::spi_tclMLInputHandler(spi_tclInputRespIntf* poInputRespIntf):
m_bIsDispDimensionChanged(false)
{
   ETG_TRACE_USR1(("spi_tclMLInputHandler::spi_tclMLInputHandler entered \n"));

SPI_INTENTIONALLY_UNUSED(poInputRespIntf);
   spoMLVNCMngr = spi_tclMLVncManager::getInstance();
   if(NULL != spoMLVNCMngr )
   {
           //Register for Viewer and Discoverer Responses
     (spoMLVNCMngr->bRegisterObject((spi_tclMLVncRespViewer*)this)) &&
             (spoMLVNCMngr->bRegisterObject((spi_tclMLVncRespDiscoverer*)this)) ;

   }
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLInputHandler::~spi_tclMLInputHandler()
***************************************************************************/
spi_tclMLInputHandler::~spi_tclMLInputHandler()
{
   ETG_TRACE_USR1(("spi_tclMLInputHandler::~spi_tclMLInputHandler entered \n"));
   spoMLVNCMngr = NULL;
   m_bIsDispDimensionChanged = false;

}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLInputHandler::vProcessTouchEvent
***************************************************************************/
t_Void spi_tclMLInputHandler::vProcessTouchEvent(
   t_U32 u32DeviceHandle,trTouchData &rfrTouchData)const
{
   if(NULL != spoMLVNCMngr )
   {
      spi_tclMLVncCmdViewer* poMLViewerCmd = const_cast<spi_tclMLVncCmdViewer*>
         (spoMLVNCMngr->poGetViewerInstance());
      if( NULL != poMLViewerCmd )
      {
         spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();
         if(NULL != poConfigReader)
         {
            trVideoConfigData rVideoConfigData;
            poConfigReader->vGetVideoConfigData(e8DEV_TYPE_MIRRORLINK,rVideoConfigData);

            /*If there is a change in the display width and height as compared to
            the initial frame buffer width and height,consider these values for scaling.
            Else,use the initial frame buffer width and height for scaling*/

            trScalingAttributes rfrScaleData;
            rfrScaleData.u32ScreenWidth = (false==m_bIsDispDimensionChanged)?
               (m_rInitialFramebufferDimensions.u32Width):
            (m_rServerDispDimensions.u32Width);

            rfrScaleData.u32ScreenHeight = (false==m_bIsDispDimensionChanged)?
               (m_rInitialFramebufferDimensions.u32Height):
            (m_rServerDispDimensions.u32Height);

            ETG_TRACE_USR4(("vProcessTouchEvent:Server Display  Width  %d \n",rfrScaleData.u32ScreenWidth));
            ETG_TRACE_USR4(("vProcessTouchEvent:Server Display  Height %d \n",rfrScaleData.u32ScreenHeight));

            t_U32 oServerScreenWidth  =  rfrScaleData.u32ScreenWidth;
            t_U32 oServerScreenHeight =	 rfrScaleData.u32ScreenHeight;

            t_Float fPhoneAspect = (t_Float)(rfrScaleData.u32ScreenWidth) / (rfrScaleData.u32ScreenHeight);
            t_Float fScreenAspect = (t_Float)rVideoConfigData.u32ProjScreen_Width / rVideoConfigData.u32ProjScreen_Height;
            ETG_TRACE_USR4(("vProcessTouchEvent:Phone Aspect ratio  %f \n",fPhoneAspect));
            ETG_TRACE_USR4(("vProcessTouchEvent:Screen Aspect ratio %f \n",fScreenAspect));

            if(fScreenAspect < fPhoneAspect)
            {
               /*If Screen aspect ratio is less than phone aspect ratio
                 calculate new aspect ratio maintaining screen width */

               rfrScaleData.u32ScreenWidth = rVideoConfigData.u32ProjScreen_Width;
               rfrScaleData.u32ScreenHeight = (t_U32)((t_Float)rVideoConfigData.u32ProjScreen_Width / fPhoneAspect);
            }
            else
            {
               rfrScaleData.u32ScreenWidth = (t_U32)((t_Float)rVideoConfigData.u32ProjScreen_Height * fPhoneAspect);
               rfrScaleData.u32ScreenHeight = rVideoConfigData.u32ProjScreen_Height;
            }

            //! Since aspect ratios change the actual display area to either less or more
            //! always calculate the aspect ratio, in normal case it will be 1.0 scaling factor.
            rfrScaleData.fWidthScaleValue    =   (t_Float)(oServerScreenWidth) /(rfrScaleData.u32ScreenWidth);
            rfrScaleData.fHeightScalingValue =   (t_Float)(oServerScreenHeight)/(rfrScaleData.u32ScreenHeight);

            ETG_TRACE_USR4(("vProcessTouchEvent:Scalling Factor for width %f \n",rfrScaleData.fWidthScaleValue));
            ETG_TRACE_USR4(("vProcessTouchEvent:Scalling Factor for height %f \n",rfrScaleData.fHeightScalingValue));

            rfrScaleData.s32XStartCoordinate = 0;
            rfrScaleData.s32YStartCoordinate = 0;
            //Calculate the scaling factor
            if((0!= rVideoConfigData.u32ProjScreen_Width)&& (0!= rVideoConfigData.u32ProjScreen_Height))
            {
               //check if the width is with in the display range
               if (rfrScaleData.u32ScreenWidth < rVideoConfigData.u32ProjScreen_Width)
               {
                  ETG_TRACE_USR4(("vProcessTouchEvent: Scaled width : %d  is less than screen width :%d \n"
                     ,rfrScaleData.u32ScreenWidth ,rVideoConfigData.u32ProjScreen_Width));

                  // Now calculate the new X axis origin
                  rfrScaleData.s32XStartCoordinate = (rVideoConfigData.u32ProjScreen_Width - rfrScaleData.u32ScreenWidth )/2;
               }

               //check if the height is with in the display range
               if (rfrScaleData.u32ScreenHeight < rVideoConfigData.u32ProjScreen_Height)
               {
                  ETG_TRACE_USR4(("vProcessTouchEvent: Scaled Height : %d  is less than screen Height :%d \n"
                     , rfrScaleData.u32ScreenHeight, rVideoConfigData.u32ProjScreen_Height));

                  // Now calculate the new Y axis origin
                  rfrScaleData.s32YStartCoordinate = (rVideoConfigData.u32ProjScreen_Height- rfrScaleData.u32ScreenHeight) / 2;
               }

               poMLViewerCmd->vTriggerSendTouchEventToServer(rfrTouchData,
               rfrScaleData,u32DeviceHandle);

            }
         }
      }
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLInputHandler::vProcessKeyEvents
***************************************************************************/
t_Void spi_tclMLInputHandler::vProcessKeyEvents(t_U32 u32DeviceHandle,
                                                tenKeyMode enKeyMode, tenKeyCode enKeyCode)const
{

   if(NULL != spoMLVNCMngr)
   {
      spi_tclMLVncCmdViewer* poMLViewerCmd = const_cast<spi_tclMLVncCmdViewer*>
         (spoMLVNCMngr->poGetViewerInstance());

      //Forward the Key events to ML Command Viewer
      if( NULL != poMLViewerCmd )
      {
         poMLViewerCmd->vTriggerSendKeyEventToServer(enKeyMode, 
            enKeyCode,u32DeviceHandle);
      }
   }
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostServerCapabilities
***************************************************************************/
t_Void spi_tclMLInputHandler::vPostServerCapabilities(
   trServerCapabilities* pServerCapabilities,const t_U32 cou32DeviceHandle)
{

   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   if(NULL!= pServerCapabilities)
   {
      //Knob Key Support
      m_oServerCapabilities.rKeyCapabilities.u32KnobKeySupport = 
         pServerCapabilities->rKeyCapabilities.u32KnobKeySupport;
      ETG_TRACE_USR4(("vPostServerCapabilities - Knob Key Support :%x\n",
         m_oServerCapabilities.rKeyCapabilities.u32KnobKeySupport));

      //Device Key Support
      m_oServerCapabilities.rKeyCapabilities.u32DeviceKeySupport = 
         pServerCapabilities->rKeyCapabilities.u32DeviceKeySupport;
       ETG_TRACE_USR4(("vPostServerCapabilities - Device Key Support :%x\n",
         m_oServerCapabilities.rKeyCapabilities.u32DeviceKeySupport));

      //Multimedia Key Support
      m_oServerCapabilities.rKeyCapabilities.u32MultimediaKeySupport = 
         pServerCapabilities->rKeyCapabilities.u32MultimediaKeySupport;
       ETG_TRACE_USR4(("vPostServerCapabilities - Multimedia Key Support :%x\n",
         m_oServerCapabilities.rKeyCapabilities.u32MultimediaKeySupport));

      //POinter and Touch Support
      m_oServerCapabilities.rKeyCapabilities.u32PointerTouchSupport = 
         pServerCapabilities->rKeyCapabilities.u32PointerTouchSupport;
       ETG_TRACE_USR4(("vPostServerCapabilities - PointerTouch Support :%x\n",
         m_oServerCapabilities.rKeyCapabilities.u32PointerTouchSupport));

       trMLSrvKeyCapabilities rSrvKeyCapabilities;

       rSrvKeyCapabilities.rKeyCapabilities = m_oServerCapabilities.rKeyCapabilities;

       //! Copy X_DeviceKeys suuported by the Device
       rSrvKeyCapabilities.vecrXDeviceKeyInfo = m_VecXDevKeysInfo;

       //! Size of the XDeviceKeyInfo List is the number of XDevices keys supported
       t_U16 u16NumXDeviceKeys = rSrvKeyCapabilities.vecrXDeviceKeyInfo.size();

       //! Send a property update via Input Handler
       if (NULL != m_rInputCallbacks.fvSrvKeyCapabilitiesInfo)
       {
          (m_rInputCallbacks.fvSrvKeyCapabilitiesInfo)(cou32DeviceHandle,u16NumXDeviceKeys,rSrvKeyCapabilities);
       }
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostFrameBufferDimension
***************************************************************************/
t_Void spi_tclMLInputHandler::vPostFrameBufferDimension(t_U32 u32Width,t_U32 u32Height,
                                                        const t_U32 cou32DeviceHandle)
{

   (t_Void)cou32DeviceHandle; //To avoid Lint Prio2 warning
   m_bIsDispDimensionChanged = false;
   m_rInitialFramebufferDimensions.u32Width = u32Width;
   m_rInitialFramebufferDimensions.u32Height = u32Height;

   ETG_TRACE_USR4(("vPostFrameBufferDimension():Initial FB Width : %d\n",
      m_rInitialFramebufferDimensions.u32Width));
   ETG_TRACE_USR4(("vPostFrameBufferDimension():Initial FB Height : %d\n",
      m_rInitialFramebufferDimensions.u32Height));

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostServerDispDimension
***************************************************************************/
t_Void spi_tclMLInputHandler::vPostServerDispDimension(t_U32 u32NewWidth,
                                                       t_U32 u32NewHeight,
                                                       const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle; //To avoid Lint Prio2 warning
   m_rServerDispDimensions.u32Width = u32NewWidth;
   m_rServerDispDimensions.u32Height = u32NewHeight;

   //Set the flag to TRUE indicating that the server display dimensions have changed.
   m_bIsDispDimensionChanged = true;

   ETG_TRACE_USR4(("vPostFrameBufferDimension():Server Display Width : %d\n",
      m_rServerDispDimensions.u32Width));
   ETG_TRACE_USR4(("vPostFrameBufferDimension():Server Display Height : %d\n",
      m_rServerDispDimensions.u32Height));
}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclMLInputHandler::vSelectDevice
***************************************************************************/
t_Void  spi_tclMLInputHandler::vSelectDevice(const t_U32 cou32DevId,
   const tenDeviceConnectionReq coenConnReq)
{
	/*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
  ETG_TRACE_USR1(("spi_tclMLInputHandler:vSelectDevice:Device-0x%x \n",cou32DevId));
  SPI_INTENTIONALLY_UNUSED(coenConnReq);
  if(NULL != m_rInputCallbacks.fvSelectDeviceResp)
  {
     (m_rInputCallbacks.fvSelectDeviceResp)(cou32DevId,e8NO_ERRORS);
  }//if(NULL != m_rVideoCallbacks.fpvSelectDeviceCb
}
/***************************************************************************
** FUNCTION:  t_Void  spi_tclMLInputHandler::vRegisterVideoCallbacks
***************************************************************************/
t_Void  spi_tclMLInputHandler::vRegisterInputCallbacks(const trInputCallbacks& corfrInputCallbacks)
{
  ETG_TRACE_USR1(("spi_tclMLInputHandler:vRegisterInputCallbacks() \n"));
   //Copy
   m_rInputCallbacks = corfrInputCallbacks;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLInputHandler::vPostAppIconData()
***************************************************************************/
t_Void spi_tclMLInputHandler::vPostKeyIconData(trUserContext rUsrCntxt,
                                          const t_U32 cou32DevId,
                                          const t_U8* pcou8BinaryData,
                                          const t_U32 u32Len)
{

   /*lint -esym(40,fvKeyIconDataResp)fvKeyIconDataResp Undeclared identifier */

   //Post response to HMI
   if ( NULL != m_rInputCallbacks.fvKeyIconDataResp)
   {
      (m_rInputCallbacks.fvKeyIconDataResp)(cou32DevId, pcou8BinaryData, u32Len, rUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLInputHandler::vGetKeyIconData()
***************************************************************************/
t_Void spi_tclMLInputHandler::vGetKeyIconData(const t_U32 cou32DevId,
                                       t_String szKeyIconUrl,
                                       tenDeviceCategory enDevCat,
                                       const trUserContext& rfrcUsrCntxt)
{
   /*lint -esym(40,fvKeyIconDataResp)fvKeyIconDataResp Undeclared identifier */
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclMLInputHandler::vGetKeyIconData:u8Index-%d IconURL-%s \n",
      u8Index,szKeyIconUrl.c_str()));

   if(NULL != spoMLVNCMngr)
   {
      spi_tclMLVncCmdDiscoverer* poCmdDisc = spoMLVNCMngr ->poGetDiscovererInstance();
      if ((NULL != poCmdDisc))
      {
         poCmdDisc->vGetKeyIconData(cou32DevId,szKeyIconUrl.c_str(),rfrcUsrCntxt);
      }
      else
      {
         if ( NULL != m_rInputCallbacks.fvKeyIconDataResp)
         {
            (m_rInputCallbacks.fvKeyIconDataResp)(cou32DevId,NULL,0,rfrcUsrCntxt);
         }
      }
   }

}

/***************************************************************************
** FUNCTION:  spi_tclMLInputHandler::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclMLInputHandler::vOnSelectDeviceResult(const t_U32 cou32DevId,
                                                        const tenDeviceConnectionReq coenConnReq,
                                                        const tenResponseCode coenRespCode)
{
   /*lint -esym(40,fvSrvKeyCapabilitiesInfo)fvSrvKeyCapabilitiesInfo Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLInputHandler::vOnSelectDeviceResult:Dev-0x%x ConnReq-%d, RespCode-%d",
                   cou32DevId,ETG_ENUM(CONNECTION_REQ,coenConnReq),coenRespCode));


   if ( (e8SUCCESS == coenRespCode) && (e8DEVCONNREQ_SELECT == coenConnReq) && (NULL != spoMLVNCMngr) )
   {
      //On successful Selection of Device retrieve XDevice Key details, which would be stored in a map.

      //!Get VNC Manager to get Command Discoverer.
      spi_tclMLVncCmdDiscoverer* poCmdDisc = spoMLVNCMngr ->poGetDiscovererInstance();

      if ((NULL != poCmdDisc))
      {
         //! Number of X-Device keys supported
         t_U16 u16NumXDeviceKeys =0;

         //! List to hold XDeviceKeys
         std::vector<trXDeviceKeyDetails> vecrXDeviceKeyDetails;

         poCmdDisc->vGetXDeviceKeyInfo(cou32DevId,u16NumXDeviceKeys,vecrXDeviceKeyDetails);

         //! Propagate the Info to Input Handler which would be then updated as a CCA property.
         m_VecXDevKeysInfo = vecrXDeviceKeyDetails;

         trMLSrvKeyCapabilities rSrvKeyCapabilities;
         rSrvKeyCapabilities.vecrXDeviceKeyInfo = m_VecXDevKeysInfo;

         if (NULL != m_rInputCallbacks.fvSrvKeyCapabilitiesInfo)
         {
            (m_rInputCallbacks.fvSrvKeyCapabilitiesInfo)(cou32DevId,u16NumXDeviceKeys,rSrvKeyCapabilities);
         }
      }

   }
   else if(e8DEVCONNREQ_DESELECT == coenConnReq)
   {
      //Remove XDevice Key Info for the de-selected device.
      ETG_TRACE_USR2(("Removing XDevice Key Info for Dev-0x%x",cou32DevId));
      m_VecXDevKeysInfo.clear();
   }


}
//lint –restore





























































































































