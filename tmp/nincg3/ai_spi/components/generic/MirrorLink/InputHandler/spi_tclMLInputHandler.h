/*!
 *******************************************************************************
 * \file              spi_tclMLInputHandler.h
 * \brief             SPI input handler for mirrorlink devices
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Input handler class to send input events from MLClient to
                MLServer for mirrorlink devices
 AUTHOR:        Hari Priya E R (RBEI/ECP2)
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.11.2013 |  Hari Priya E R              | Initial Version
 03.07.2014 |  Hari Priya E R              | Added changes related to Input Response Interface
 25.06.2015 |  Sameer Chandra              | Added ML XDeviceKey Support for PSA

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMLINPUTHANDLER_H_
#define SPI_TCLMLINPUTHANDLER_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclMLVncRespViewer.h"
#include "spi_tclMLVncRespDiscoverer.h"
#include "spi_tclInputDevBase.h"
 

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/
 class spi_tclInputRespIntf;

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLInputHandler
 * \brief Input handler class to send input events from MLClient to
                MLServer for mirrorlink devices
 *
 */

class spi_tclMLInputHandler:public spi_tclInputDevBase,public spi_tclMLVncRespViewer,public spi_tclMLVncRespDiscoverer
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLInputHandler::spi_tclMLInputHandler(spi_tclInputResp ..)
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLInputHandler(spi_tclInputRespIntf* poInputRespIntf)
   * \brief   Parameterised constructor
   * \param   poInputRespIntf: [IN]Input Response Interface Pointer
   * \sa      ~spi_tclMLInputHandler()
   **************************************************************************/
   spi_tclMLInputHandler(spi_tclInputRespIntf* poInputRespIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLInputHandler::~spi_tclMLInputHandler()
   ***************************************************************************/
   /*!
   * \fn     ~ spi_tclMLInputHandler()
   * \brief   destructor
   * \sa      spi_tclMLInputHandler()
   **************************************************************************/
   ~spi_tclMLInputHandler();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vProcessTouchEvent
   ***************************************************************************/
   /*!
   * \fn      vProcessTouchEvent(t_U32 u32DeviceHandle,trTouchData &rfrTouchData)
   * \brief   Receives the Touch events and forwards its to ML Server through
   *          viewer wrapper
   * \param   u32DeviceHandle  : [IN] unique identifier to ML Server
   * \param   rfrTouchData     : [IN]reference to touch data structure which contains
   *          touch details received /ref trTouchData
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessTouchEvent(t_U32 u32DeviceHandle,trTouchData &rfrTouchData)const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vProcessKeyEvents
   ***************************************************************************/
   /*!
   * \fn      vProcessKeyEvents()
   * \brief   receives hard key events and forwards it to ML Server through
   *          viewer wrapper
   * \param   u32DeviceHandle : [IN] unique identifier to ML Server
   * \param   enKeyMode       : [IN] indicates keypress or keyrelease
   * \param   enKeyCode       : [IN] unique key code identifier
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessKeyEvents(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
      tenKeyCode enKeyCode)const;

   /***************************************************************************
   ** FUNCTION: virtual t_Void spi_tclMLInputHandler::vSelectDevice()
   ***************************************************************************/
  /*!
   * \fn      t_Void vSelectDevice(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq)
   * \brief   To setup Video related info when a device is selected or
   *          de selected.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Type.
   * \retval  t_Void
   **************************************************************************/
  t_Void  vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq);
	  
   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclMLInputHandler::vRegisterVideoCallbacks()
   ***************************************************************************/
  /*!
   * \fn      t_Void vRegisterInputCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo Video
   * \param   corfrVideoCallbacks : [IN] Video callabcks structure
   * \retval  t_Void
   **************************************************************************/
  t_Void  vRegisterInputCallbacks(const trInputCallbacks& corfrInputCallbacks);
  
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful/failed
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLInputHandler::spi_tclMLInputHandler(const spi_tclMLInputHandler..
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLInputHandler(const spi_tclMLInputHandler &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclMLInputHandler(const spi_tclMLInputHandler& corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLInputHandler& spi_tclMLInputHandler::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLInputHandler& operator= (const spi_tclMLInputHandler &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclMLInputHandler& operator=(const spi_tclMLInputHandler& corfrSrc);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vPostServerCapabilities
   ***************************************************************************/
   /*!
   * \fn      vPostServerCapabilities( trServerCapabilities* pServerCapabilities,
   *                                         const tvU32 cou32DeviceHandle = 0)
   * \brief   Posts the Server event configuration parameters to the SPI
   * \param   pServerCapabilities  : [IN] Server capabilities
   * \param   cou32DeviceHandle    : [IN] Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostServerCapabilities(
      trServerCapabilities* pServerCapabilities,
      const t_U32 cou32DeviceHandle = 0);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vPostFrameBufferDimension
   ***************************************************************************/
   /*!
   * \fn      vPostFrameBufferDimension()
   * \brief   Posts the Framebuffer Initial Width and Height to SPI
   * \param   u32Width          : [IN] Initial Width of framebuffer
   * \param   u32Height         : [IN] Initial Height of framebuffer
   * \param   cou32DeviceHandle : [IN] Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostFrameBufferDimension(t_U32 u32Width,t_U32 u32Height,
      const t_U32 cou32DeviceHandle = 0);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vPostServerDispDimension
   ***************************************************************************/
   /*!
   * \fn      vPostServerDispDimension()
   * \brief   Posts the Changed Server Display Width and Height to SPI
   * \param   u32NewWidth        : [IN] New Width of server display
   * \param   u32NewHeight       : [IN] New Height of server display
   * \param   cou32DeviceHandle  : [IN]  Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostServerDispDimension(t_U32 u32NewWidth,t_U32 u32NewHeight,
      const t_U32 cou32DeviceHandle = 0);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vGetKeyIconData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetKeyIconData(t_String szKeyIconUrl,
   *         tenDeviceCategory enDevCat,
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the Key icon data
   * \param  szKeyIconUrl  : [IN] Key Icon URL
   * \pram   enDevCat      : [IN] Identifies the Connection Category
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vGetKeyIconData(const t_U32 cou32DevId,
                                  t_String szKeyIconUrl,
                                  tenDeviceCategory enDevCat,
                                  const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vPostKeyIconData
   ***************************************************************************/
   /*!
   * \fn      vPostServerCapabilities( trUserContext rUsrCntxt,
                                             const t_U32 cou32DevId,
                                             const t_U8* pcou8BinaryData,
                                             const t_U32 u32Len)
   * \brief   Posts the key Icon data parameters to the SPI
   * \param   pServerCapabilities  : [IN] Server capabilities
   * \param   cou32DevId           : [IN] Device handle to identify the MLServer
   * \param   pcou8BinaryData      : [OUT] Binary Data for the Icon URL
   * \param   u32Len               : [IN] Length of the binary data
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vPostKeyIconData(trUserContext rUsrCntxt,
                                             const t_U32 cou32DevId,
                                             const t_U8* pcou8BinaryData,
                                             const t_U32 u32Len);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vProcessKnobKeyEvents
   ***************************************************************************/
   /*!
   * \fn      vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt
   * \brief   Receives Knob key enocder change and forwards it to
   *          further handlers for processing
   * \param   u32DeviceHandle : [IN] unique identifier to CP Server
   * \param   s8EncoderDeltaCount : [IN] encoder delta count
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt)
      const
   {
      SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
      SPI_INTENTIONALLY_UNUSED(s8EncoderDeltaCnt);
   }
#if 0


   /***************************************************************************
   ** FUNCTION:  void spi_tclMLInputHandler::vProcessKnobRotationEvent(
   unsigned int u32DeviceHandle,signed int s8DeltaCnt,
   tenKeyCode enKeyCode)
   ***************************************************************************/
   /*!
   * \fn      vProcessKnobRotationEvent()
   * \brief   Receives Knob rotation delta count value and forwards it to 
   viewer wrapper
   * \param   u32DeviceHandle : [IN] unique identifier to ML Server
   * \param   s8DeltaCnt      : [IN] No of rotations of the knob
   * \param   enKeyCode       : [IN]unique key code identifier
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessKnobRotationEvent(t_U32 u32DeviceHandle,
      t_S8 s8DeltaCnt,tenKeyCode& enKeyCode);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLInputHandler::vProcessSingleDimenKnobRotation
   ***************************************************************************/
   /*!
   * \fn      vProcessSingleDimenKnobRotation()
   * \brief   Processes the keycode for single dimensional rotation of knob
   * \param   s8DeltaCnt: [IN]Num of rotations of knob
   * \param   enKeyCode:  [IN]Key Code for the knob
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessSingleDimenKnobRotation(t_S8 s8DeltaCnt,tenKeyCode& enKeyCode);

   /***************************************************************************
   ** FUNCTION:  tvVoid spi_tclMLInputHandler::vProcessTwoDimenKnobRotation
   ***************************************************************************/
   /*!
   * \fn      vProcessTwoDimenKnobRotation()
   * \brief   Processes the keycode for single dimensional rotation of knob
   * \param   s8DeltaCnt: [IN]Num of rotations of knob
   * \param   enKeyCode:  [IN]Key Code for the knob
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessTwoDimenKnobRotation(t_S8 s8DeltaCnt,tenKeyCode& enKeyCode);

#endif

   //!Framebuffer Dimensions object
   trInitialFramebufferDimensions m_rInitialFramebufferDimensions;

   //!Changed server display dimensions
   trServerDisplayDimensions m_rServerDispDimensions;

   //!Flag to keep track of change in server display dimensions
   t_Bool m_bIsDispDimensionChanged;

   //! Indicates the server cpabilities 
   trServerCapabilities m_oServerCapabilities;

   //! Input Callbacks structure
   trInputCallbacks m_rInputCallbacks;

   //! vector to hold selected device XDeviceKey details.
   std::vector<trXDeviceKeyDetails> m_VecXDevKeysInfo;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif /* SPI_TCLMLINPUTHANDLER_H_ */
