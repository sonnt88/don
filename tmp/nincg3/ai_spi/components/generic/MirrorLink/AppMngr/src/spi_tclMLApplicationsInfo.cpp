/***********************************************************************/
/*!
* \file  spi_tclMLApplicationsInfo.cpp
* \brief To Store the Applications inforamtion and provide interfaces to read the data
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.02.2014  | Shiva Kumar Gurija    | Initial Version
23.04.2014  | Shiva Kumar Gurija    | Changes in AppList Handling for CTS Test
11.08.2014  | Ramya Murthy          | Added functions to read/set notification 
                                      support status of apps
27.05.2015  | Shiva Kumar Gurija    | Lint Fix

\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <algorithm>

#include "spi_tclMLApplicationsInfo.h"
#include "spi_tclAppSettings.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclMLApplicationsInfo.cpp.trc.h"
#endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef std::map<const t_U32,trMLDeviceAppInfo>::iterator tItmapAppInfo;
typedef std::vector<trAppDetails>::iterator tItvecAppDetails;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclMLApplicationsInfo::spi_tclMLApplicationsInfo()
***************************************************************************/
spi_tclMLApplicationsInfo::spi_tclMLApplicationsInfo()
{
   //Constructor
   m_mapAppInfo.clear();
}

/***************************************************************************
** FUNCTION:  spi_tclMLApplicationsInfo::~spi_tclMLApplicationsInfo()
***************************************************************************/
spi_tclMLApplicationsInfo::~spi_tclMLApplicationsInfo()
{
   //Destructor
   m_oLock.s16Lock();
   m_mapAppInfo.clear();
   m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vInsertAppInfo()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vInsertAppInfo(const t_U32 cou32DevHandle,
                                                 const trMLDeviceAppInfo& rfrAppInfo)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vInsertAppInfo:Dev-0x%x,Apps-%d",cou32DevHandle,
      rfrAppInfo.vecAppDetailsList.size()));

   //Insert into the map
   m_oLock.s16Lock();
   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      if (m_mapAppInfo.end() != m_mapAppInfo.find(cou32DevHandle))
      {
         ETG_TRACE_USR4(("vInsertAppInfo:No.of apps that were supported by Device - 0x%x Apps-%d", cou32DevHandle,
            m_mapAppInfo[cou32DevHandle].vecAppDetailsList.size()));
         m_mapAppInfo.erase(cou32DevHandle);
      }//if(m_mapAppInfo.end() != itmapAppInfo)
      else
      {
         ETG_TRACE_USR4(("vInsertAppInfo:Device not found in the list"));
      }
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   else
   {
      ETG_TRACE_USR4(("vInsertAppInfo:No devices are connected by now"));
   }
   m_mapAppInfo.insert(std::pair<t_U32,trMLDeviceAppInfo>(cou32DevHandle,rfrAppInfo));
   ETG_TRACE_USR4(("No.of apps supported by Device - 0x%x Apps-%d",cou32DevHandle,
      m_mapAppInfo[cou32DevHandle].vecAppDetailsList.size()));

   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vClearAppsInfo()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vClearAppsInfo(const t_U32 cou32DevHandle)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vClearAppsInfo:Dev-0x%x",cou32DevHandle));

   m_oLock.s16Lock();

   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
      if(m_mapAppInfo.end() != itmapAppInfo)
      {
         m_mapAppInfo.erase(itmapAppInfo);
      }//if(m_mapAppInfo.end() != itmapAppInfo)
   }//if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetAppList()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vGetAppList(const t_U32 cou32DevHandle,
                                              std::vector<trAppDetails>& rfvecAppList)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vGetAppList:Dev-0x%x",cou32DevHandle));
   rfvecAppList.clear();

   m_oLock.s16Lock();

   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
      if(m_mapAppInfo.end() != itmapAppInfo)
      {
         rfvecAppList = itmapAppInfo->second.vecAppDetailsList;
      }//if(m_mapAppInfo.end() != itmapAppInfo)
   }//if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetUIAppList()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vGetUIAppList(const t_U32 cou32DevHandle,
                                                std::vector<trAppDetails>& rfvecAppList,
                                                t_Bool bNonCertApps)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vGetUIAppList:Dev-0x%x NonCertApps-%d",
      cou32DevHandle,ETG_ENUM(BOOL,bNonCertApps)));

   rfvecAppList.clear();
   m_oLock.s16Lock();

   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
   }//if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   if(m_mapAppInfo.end() != itmapAppInfo)
   {
      for(tItvecAppDetails itvecAppDetails=itmapAppInfo->second.vecAppDetailsList.begin();
            itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
      {
         //Fetch only UI Applications.
         //If the Non certified applications are also required, provide the info of all UI Apps.
         //If only certified apps are required, provide info of the certified applications only
         // Applications with category 0xFFFExxxx and 0xFFFFxxxx are also UI applications also UI apps.
         // so show the applications with this application category to the user.
         t_Bool bDisplayTestApp = ( 
            (e32APP_CERT_TEST == static_cast<tenAppCategory>(itvecAppDetails->enAppCategory & e32APP_CERT_TEST)) ||
            (e32APP_SYSTEM == static_cast<tenAppCategory>(itvecAppDetails->enAppCategory & e32APP_SYSTEM)) );


         if(
            ( (e32APPUNKNOWN != itvecAppDetails->enAppCategory)&&
            ((TRUE == bDisplayTestApp) || (0 == static_cast<t_U32>((itvecAppDetails->enAppCategory) & e32APP_NOUI))) )
            &&
            ( (true==bNonCertApps)  ||  (itvecAppDetails->enAppCertificationInfo != e8NOT_CERTIFIED) ) 
            )
         {
            rfvecAppList.push_back(*itvecAppDetails);
         }//if(((e32APPUNKNOWN != itvecAppDetails->enAppCategory)&&(0 
      }//for(itvecAppDetails=itmapAppInfo->second.vecAppDetailsList.begin()

   }//if(m_mapAppInfo.end() != itmapAppInfo)

   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetFilteredAppList()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vGetFilteredAppList(const t_U32 cou32DevHandle,
                                                      tenAppCertificationFilter enAppCertFilter,
                                                      t_Bool bNotifSupportedApps,
                                                      std::vector<trAppDetails>& rfvecAppList)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vGetFilteredAppList: Dev-0x%x, AppCertFilter-%d, FilterNotiSupApps-%d \n",
         cou32DevHandle, ETG_ENUM(APP_CERT_FILTER, enAppCertFilter), ETG_ENUM(BOOL, bNotifSupportedApps)));

   rfvecAppList.clear();

   m_oLock.s16Lock();
   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
   }//if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   if(m_mapAppInfo.end() != itmapAppInfo)
   {
      for(tItvecAppDetails itvecAppDetails=itmapAppInfo->second.vecAppDetailsList.begin();
            itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
      {
         //Fetch Applications based on Certification type & Notification support.
         //If the Non certified applications are also required, provide the info of all UI Apps.
         //If only certified apps are required, provide info of the certified applications only
         if(
            ((e32APPUNKNOWN != itvecAppDetails->enAppCategory)
            && (0 == (itvecAppDetails->enAppCategory & e32APP_NOUI)))
            && (itvecAppDetails->enAppCertificationInfo != e8NOT_CERTIFIED)
            )
         {
            //Currently, Park mode apps are considered as e8BASE_CERTIFIED, e8NONDRIVE_CERTIFIED_ONLY,
            //e8DRIVE_CERTIFIED_ONLY and e8DRIVE_CERTIFIED apps (i.e. all except e8NOT_CERTIFIED).

            //Evaluate whether to filter apps based on Notification support (only if bNotifSupportedApps is set).
            t_Bool bMatchesNotiSuppCriteria =
                  ((false == bNotifSupportedApps) || (bNotifSupportedApps == itvecAppDetails->bNotificationSupport));

            //For PARK mode (Filter apps based on Notification support if bNotifSupportedApps is set)
            if (
               (e8APP_CERT_FILTER_PARK_MODE == enAppCertFilter)
               &&
               (true == bMatchesNotiSuppCriteria)
               )
            {
               rfvecAppList.push_back(*itvecAppDetails);
            }//if ((e8APP_CERT_FILTER_PARK_MODE == enAppCertFilter)&&...)

            //For DRIVE mode (Filter apps based on Notification support if bNotifSupportedApps is set, and
            //also based on app's certification type).
            else if (
               (e8APP_CERT_FILTER_DRIVE_MODE == enAppCertFilter)
               &&
               (true == bMatchesNotiSuppCriteria)
               &&
               ((itvecAppDetails->enAppCertificationInfo == e8DRIVE_CERTIFIED) ||
                        (itvecAppDetails->enAppCertificationInfo == e8DRIVE_CERTIFIED_ONLY))
               )
            {
               rfvecAppList.push_back(*itvecAppDetails);
            }//else if ((e8APP_CERT_FILTER_DRIVE_MODE == enAppCertFilter)&&...)

         }//if(((e32APPUNKNOWN != itvecAppDetails->enAppCategory)&&...)
      }//for(itvecAppDetails=itmapAppInfo->second.vecAppDetailsList.begin()
   }//if(m_mapAppInfo.end() != itmapAppInfo)

   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetAppInfo()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vGetAppInfo(const t_U32 cou32DevHandle, 
                                           const t_U32 cou32AppHandle,
                                           trAppDetails& rfrAppDetails)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vGetAppInfo:Dev-0x%x App-0x%x",
      cou32DevHandle,cou32AppHandle));

   m_oLock.s16Lock();

   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
   }//if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   if(m_mapAppInfo.end() != itmapAppInfo)
   {
      for(tItvecAppDetails itvecAppDetails=itmapAppInfo->second.vecAppDetailsList.begin();
            itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
      {
         if( cou32AppHandle == itvecAppDetails->u32AppHandle )
         {
            rfrAppDetails = *itvecAppDetails;
            //Move out of the loop, once the required app info is fetched
            break;
         }//if( cou32AppId == itvecAppDetails->u32AppHandle )
      }//for(itvecAppDetails=itmapAppInfo->second.vecAppDetailsList.begin()
   }//if(m_mapAppInfo.end() != itmapAppInfo)

   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  tenIconMimeType spi_tclMLApplicationsInfo::enGetAppIconMIMEType()
***************************************************************************/
tenIconMimeType spi_tclMLApplicationsInfo::enGetAppIconMIMEType(const t_U32 cou32DevHandle,
                                                                t_String szAppIconUrl)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::enGetAppIconMIMEType:Dev-0x%x URL-%s",
      cou32DevHandle,szAppIconUrl.c_str()));

   tenIconMimeType enIconMIMEType = e8ICON_INVALID;

   m_oLock.s16Lock();

   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
   }//if(SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   if(m_mapAppInfo.end() != itmapAppInfo)
   {
      //Loop till the required Application URL is found.
      //Move out of loop, once the element is found
      for(tItvecAppDetails itvecAppDetails=itmapAppInfo->second.vecAppDetailsList.begin();
            (itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end())&&
            (e8ICON_INVALID == enIconMIMEType); itvecAppDetails++ )
      {
         std::vector<trIconAttributes>::iterator itAppIconInfo;
         for( itAppIconInfo = itvecAppDetails->tvecAppIconList.begin();
            (itAppIconInfo != itvecAppDetails->tvecAppIconList.end());itAppIconInfo++)
         {
            if( szAppIconUrl  == itAppIconInfo->szIconURL )
            {
               enIconMIMEType = itAppIconInfo->enIconMimeType;
               break;
            }//if( szAppIconUrl  == itAppIconInfo->szIconURL )
         }//for( itAppIconInfo = itvecAppDetails->tv
      }//for(itvecAppDetails=itmapAppInfo->second.vecAppDetailsList.begin();
   }//if(m_mapAppInfo.end() != itmapAppInfo)

   m_oLock.vUnlock();

   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::enGetAppIconMIMEType:Dev-0x%x, MIMEType -%d, szAppIconUrl-%s",
      cou32DevHandle,ETG_ENUM(ICON_MIME_TYPE,enIconMIMEType),szAppIconUrl.c_str()));

   return enIconMIMEType;
}


/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLApplicationsInfo::bUpdateAppStatus()
***************************************************************************/
t_Bool spi_tclMLApplicationsInfo::bUpdateAppStatus(const t_U32 cou32DevHandle,
                                                   const t_U32 cou32AppHandle,
                                                   tenAppStatus enAppStatus)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::bUpdateAppStatus: Dev-0x%x, App-0x%x, AppStatus-%d \n",
      cou32DevHandle, cou32AppHandle, ETG_ENUM(APP_STATUS, enAppStatus)));
   t_Bool bRet = false;

   m_oLock.s16Lock();

   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   if(m_mapAppInfo.end() != itmapAppInfo)
   {
      for(tItvecAppDetails itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
         itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
      {
         //Some devices are providing the application status for the applications, which are moved to foreground only.
         //So set the status of other applications to background as a work around. so that HMi can apply
         //blocking based on the application status, whenever there is change in Vehicle mode (Park/Drive mode)
         if( e8FOREGROUND == enAppStatus )
         { 
            if(cou32AppHandle == itvecAppDetails->u32AppHandle)
            {               
               itvecAppDetails->enAppStatus = enAppStatus;
            }//if(cou32AppHandle == itvecAppDetails->u32AppHandle)
            else if(e8FOREGROUND == itvecAppDetails->enAppStatus)
            {
               itvecAppDetails->enAppStatus = e8BACKGROUND;
            }//else if(e8FOREGROUND == itvecAppDetails->enAppStatus)
            else
            {
               //Nothing to do
            }
            bRet = true;
         }//if( e8FOREGROUND == enAppStatus )
         else
         {
            if(( cou32AppHandle == itvecAppDetails->u32AppHandle)&&
               (enAppStatus != itvecAppDetails->enAppStatus))
            {
               bRet = true;
               trAppDetails& rAppDetails = *itvecAppDetails;
               rAppDetails.enAppStatus = enAppStatus;
               break;
            }//if( cou32AppHandle == itvecAppDetails->u32AppHandle)
         }//else
      }//for(itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
   }//if(m_mapAppInfo.end() != itmapAppInfo)

   m_oLock.vUnlock();

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLApplicationsInfo::bCheckAppValidity()
***************************************************************************/
t_Bool spi_tclMLApplicationsInfo::bCheckAppValidity(const t_U32 cou32DevHandle, 
                                                    const t_U32 cou32AppHandle)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::bCheckAppValidity:Dev-0x%x  App-0x%x ",
      cou32DevHandle,cou32AppHandle));

   t_Bool bRet = false;

   m_oLock.s16Lock();
   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      tItmapAppInfo itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);

      if(m_mapAppInfo.end() != itmapAppInfo)
      {
         for(tItvecAppDetails itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
               itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
         {
            if( cou32AppHandle == itvecAppDetails->u32AppHandle )
            {
               bRet = true;
               break;
            }//if( cou32AppHandle == itvecAppDetails->u32AppHandle)
         }//for(itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
      }//if(m_mapAppInfo.end() != itmapAppInfo)
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   m_oLock.vUnlock();

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::bCheckAppValidity()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vGetLaunchedAppIds(const t_U32 cou32DevHandle,
                                                     std::vector<t_U32>& rfvecLaunchedAppIds)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vGetLaunchedAppIds:Dev-0x%x",
      cou32DevHandle));

   rfvecLaunchedAppIds.clear();
   m_oLock.s16Lock();

   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      tItmapAppInfo itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
      if(m_mapAppInfo.end() != itmapAppInfo)
      {
         for(tItvecAppDetails itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
            itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
         {
            //treat the application as launched, if the app is in either
            //foreground or background.
            if( ( e8FOREGROUND == itvecAppDetails->enAppStatus) || 
               ( e8BACKGROUND == itvecAppDetails->enAppStatus) )
            {
               rfvecLaunchedAppIds.push_back(itvecAppDetails->u32AppHandle);
            }//if( ( e8FOREGROUND == itvecAppDetails->enAppStatus)
         }//for(itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
      }//if(m_mapAppInfo.end() != itmapAppInfo)
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetDeviceVersionInfo()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vGetDeviceVersionInfo(const t_U32 cou32DevHandle,
                                                        trVersionInfo& rfrVersionInfo)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vGetDeviceVersionInfo:Dev-0x%x",
      cou32DevHandle));

   m_oLock.s16Lock();

   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      tItmapAppInfo itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);
      if(m_mapAppInfo.end() != itmapAppInfo)
      {
         rfrVersionInfo = itmapAppInfo->second.rVersionInfo;
      }//if(m_mapAppInfo.end() != itmapAppInfo)
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLApplicationsInfo::bUpdateAppName()
***************************************************************************/
t_Bool spi_tclMLApplicationsInfo::bUpdateAppName(const t_U32 cou32DevHandle,
                                                 const t_U32 cou32AppHandle,
                                                 t_String szAppName)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::bUpdateAppName: Dev-0x%x, App-0x%x, AppName-%s \n",
         cou32DevHandle, cou32AppHandle, szAppName.c_str()));

   t_Bool bRet = false;

   m_oLock.s16Lock();

   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      tItmapAppInfo itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);

      if(m_mapAppInfo.end() != itmapAppInfo)
      {
         for(tItvecAppDetails itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
               itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
         {
            if(( cou32AppHandle == itvecAppDetails->u32AppHandle)&&
               (szAppName != itvecAppDetails->szAppName)&&
               (szAppName != coszUnKnownAppName))
            {
               bRet = true;
               trAppDetails& rAppDetails = *itvecAppDetails;
               rAppDetails.szAppName = szAppName.c_str();
               break;
            }//if( cou32AppHandle == itvecAppDetails->u32AppHandle)
         }//for(itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
      }//if(m_mapAppInfo.end() != itmapAppInfo)
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   m_oLock.vUnlock();

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLApplicationsInfo::bUpdateAppNotiSupport()
***************************************************************************/
t_Bool spi_tclMLApplicationsInfo::bUpdateAppNotiSupport(const t_U32 cou32DevHandle,
                                                        const tvecMLApplist& rfcovecNotiSuppApplist)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::bUpdateAppNotiSupport: Dev-0x%x, Num.Apps-%d \n",
         cou32DevHandle, rfcovecNotiSuppApplist.size()));

   m_oLock.s16Lock();

   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      tItmapAppInfo itmapAppInfo = m_mapAppInfo.find(cou32DevHandle);

      if(m_mapAppInfo.end() != itmapAppInfo)
      {
         for (tItvecAppDetails itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
              itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
         {
            //Check if each app in AppList storage is a Notification supported app,
            //(i.e if it is present in the received Notification supported app list) and set its
            //bNotificationSupport indicator.
            //If app is not present in received list, clear its bNotificationSupport indicator.
            t_Bool bAppSupportsNotif = (rfcovecNotiSuppApplist.end() !=
                  std::find(rfcovecNotiSuppApplist.begin(), rfcovecNotiSuppApplist.end(), itvecAppDetails->u32AppHandle));

            (*itvecAppDetails).bNotificationSupport = bAppSupportsNotif;

         }//for(itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
      }//if(m_mapAppInfo.end() != itmapAppInfo)
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   m_oLock.vUnlock();

   return true; //currently unused
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::bIsAppAllowedToUse()
***************************************************************************/
t_Bool spi_tclMLApplicationsInfo::bIsAppAllowedToUse(t_U32 u32DevID, 
                                                     t_U32 u32ConvAppID,  
                                                     t_Bool bDriveModeEnabled,
                                                     t_Bool bNonCertAppsAllowed)
{
   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::bIsAppAllowedToUse:Dev-0x%x App-0x%x \
                   bDriveModeEnabled-%d bNonCertAppsAllowed-%d",                                                
                   u32DevID,u32ConvAppID,ETG_ENUM(BOOL,bDriveModeEnabled),
                   ETG_ENUM(BOOL,bNonCertAppsAllowed)));

   t_Bool bRet=false;

   m_oLock.s16Lock();

   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(u32DevID);
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   if( m_mapAppInfo.end() != itmapAppInfo )
   {
      for (tItvecAppDetails itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
         itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
      {
         if(u32ConvAppID == itvecAppDetails->u32ConvAppHandle)
         {
            ETG_TRACE_USR4(("spi_tclMLApplicationsInfo::bIsAppAllowedToUse:AppCertStatus-%d",
               ETG_ENUM(APP_CERTIFICATION_STATUS,itvecAppDetails->enAppCertificationInfo)));

            if(true == bDriveModeEnabled)
            {
               bRet =( (e8DRIVE_CERTIFIED_ONLY == itvecAppDetails->enAppCertificationInfo) ||
                  (e8DRIVE_CERTIFIED == itvecAppDetails->enAppCertificationInfo) );
            }//if(true == bDriveModeEnabled)
            else
            {
               //If the Non Certified applications are allowed, all the applications listed in the app list are Mirror link aware 
               //are shared with HMI and all apps can be used by user.So, in Park mode directly return true.
               //Else check the application certification status and return the value.
               bRet = bNonCertAppsAllowed?true:(e8NOT_CERTIFIED != itvecAppDetails->enAppCertificationInfo);
            }
            break;
         }//if(  (u32AppID == itvecAppDetails->u32ConvAppHandle)&&
      }//for(itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
   }//if(m_mapAppInfo.end() != itmapAppInfo)

   m_oLock.vUnlock();

   return bRet;
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetAppCertInfo()
***************************************************************************/
t_Void spi_tclMLApplicationsInfo::vGetAppCertInfo(t_U32 u32DevID, 
                                                  t_U32 u32AppUUID,
                                                  t_U32& rfu32AppID,
                                                  tenAppCertificationInfo& rfenAppCertInfo )
{

   m_oLock.s16Lock();

   tItmapAppInfo itmapAppInfo = m_mapAppInfo.end();
   if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))
   {
      itmapAppInfo = m_mapAppInfo.find(u32DevID);
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   if( m_mapAppInfo.end() != itmapAppInfo )
   {
      for (tItvecAppDetails itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
         itvecAppDetails != itmapAppInfo->second.vecAppDetailsList.end();itvecAppDetails++)
      {
         if(u32AppUUID == itvecAppDetails->u32ConvAppHandle)
         {
            ETG_TRACE_USR4(("spi_tclMLApplicationsInfo::vGetAppCertInfo:AppCertStatus-%d",
               ETG_ENUM(APP_CERTIFICATION_STATUS,itvecAppDetails->enAppCertificationInfo)));

            rfu32AppID = itvecAppDetails->u32AppHandle;
            rfenAppCertInfo=itvecAppDetails->enAppCertificationInfo;

            break;
         }//if(  (u32AppUUID == itvecAppDetails->u32ConvAppHandle)&&
      }//for(itvecAppDetails = itmapAppInfo->second.vecAppDetailsList.begin();
   }//if(m_mapAppInfo.end() != itmapAppInfo)

   m_oLock.vUnlock();


   ETG_TRACE_USR1(("spi_tclMLApplicationsInfo::vGetAppCertInfo:Dev-0x%x u32AppUUID-0x%x \
                   rfu32AppID-0x%x rfenAppCertInfo-%d",
                   u32DevID,u32AppUUID,rfu32AppID,
                   ETG_ENUM(APP_CERTIFICATION_STATUS,rfenAppCertInfo)));
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
