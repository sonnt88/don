/***********************************************************************/
/*!
* \file  spi_tclMLAppFilter.cpp
* \brief To Filter the Mirror Link Applications and store in the storage class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.02.2014  | Shiva Kumar Gurija    | Initial Version
23.04.2014  | Shiva Kumar Gurija    | Changes in AppList Filtering for CTS Test
29.04.2014  | Shiva Kumar Gurija    | Updated with the Changes in App certification status
11.08.2014  | Ramya Murthy          | Added bUpdateAppNotiSupport() to modify notification 
                                      support status of apps

\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <algorithm>
#include "spi_tclAppSettings.h"
#include "spi_tclConfigReader.h"
#include "spi_tclMLApplicationsInfo.h"
#include "spi_tclMLAppFilter.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclMLAppFilter.cpp.trc.h"
#endif
#endif

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_String coszCertEntityCCC = "CCC";
static const t_String coszCertEntityEmpty = "";
static const t_String coszLocaleWorld = "WORLD";

typedef std::vector<trApplicationInfo>::iterator tItvecAppInfo;
typedef std::vector<trAppCertInfo>::iterator tItAppCertInfo;
/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclMLAppFilter::spi_tclMLAppFilter()
***************************************************************************/
spi_tclMLAppFilter::spi_tclMLAppFilter()
{
   //Constructor
   ETG_TRACE_USR1(("spi_tclMLAppFilter::spi_tclMLAppFilter()"));
}

/***************************************************************************
** FUNCTION:  spi_tclMLAppFilter::~spi_tclMLAppFilter()
***************************************************************************/
spi_tclMLAppFilter::~spi_tclMLAppFilter()
{
   //Destructor
   ETG_TRACE_USR1(("spi_tclMLAppFilter::~spi_tclMLAppFilter()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppFilter::vStoreAppInfo()
***************************************************************************/
t_Void spi_tclMLAppFilter::vStoreAppInfo(const t_U32 cou32DeviceHandle,
                                         const trVersionInfo& corfrVersionInfo,
                                         std::vector<trAppDetails> vecAppDetailsList)
{
   ETG_TRACE_USR1(("spi_tclMLAppFilter::vStoreAppInfo()"));
   spi_tclMLApplicationsInfo* poAppsInfo = spi_tclMLApplicationsInfo::getInstance();
   if(NULL != poAppsInfo)
   {
      trMLDeviceAppInfo rMLDevAppinfo;
      rMLDevAppinfo.rVersionInfo = corfrVersionInfo;
      rMLDevAppinfo.vecAppDetailsList = vecAppDetailsList;

      poAppsInfo->vInsertAppInfo(cou32DeviceHandle,rMLDevAppinfo);
   }//if(NULL != poAppsInfo)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppFilter::vFilterApplications()
***************************************************************************/
t_Void spi_tclMLAppFilter::vFilterApplications(const t_U32 cou32DeviceHandle,
                                               const trVersionInfo& corfrVersionInfo,
                                               std::vector<trApplicationInfo> vecAppInfoList)
{
   ETG_TRACE_USR1(("spi_tclMLAppFilter::vFilterApplications:Dev-0x%x Major-%d Minor-%d Apps-%d",
      cou32DeviceHandle,corfrVersionInfo.u32MajorVersion,corfrVersionInfo.u32MinorVersion,
      vecAppInfoList.size()));

   std::vector<trAppDetails> vecAppDetails;
   //If the application certification is not required, store the all applications info
   //with out checking for the application certification status.
   if( (NULL != spi_tclAppSettings::getInstance())&&
      (false == (spi_tclAppSettings::getInstance())->bEnableAppCertification(corfrVersionInfo)))
   {
      for(tItvecAppInfo itvecAppInfo=vecAppInfoList.begin();itvecAppInfo!=vecAppInfoList.end();itvecAppInfo++)
      {
         vecAppDetails.push_back(itvecAppInfo->rAppdetails);
      }//(tItvecAppInfo itvecAppInfo=vecAppInfoList.begin();
   }
   else
   {
      //Set the certification status of all applications. By default it is set to Not Certified.
      for(tItvecAppInfo itvecAppInfo=vecAppInfoList.begin();itvecAppInfo!=vecAppInfoList.end();
         itvecAppInfo++)
      {
         ETG_TRACE_USR1(("spi_tclMLAppFilter::vFilterApplications:App-0x%x cert count-%d",
            itvecAppInfo->rAppdetails.u32AppHandle,itvecAppInfo->u8AppCertEntityCount));

         tenAppCertificationEntity enAppCertEntity = e8UNKNOWN_ENTITY;
         itvecAppInfo->rAppdetails.enAppCertificationInfo =  
            enGetAppCertStatus(itvecAppInfo->vecAppCertInfo,enAppCertEntity);
         itvecAppInfo->rAppdetails.enAppCertificationEntity = enAppCertEntity;

         vecAppDetails.push_back(itvecAppInfo->rAppdetails);
      }//for(tItvecAppInfo itvecAppInfo=vecAppInfoList.begin();itvecAppInfo!
   }

   vStoreAppInfo(cou32DeviceHandle,corfrVersionInfo,vecAppDetails);

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppFilter::vClearAppInfo()
***************************************************************************/
t_Void spi_tclMLAppFilter::vClearAppInfo(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLAppFilter::vClearAppInfo:Dev-0x%x",cou32DeviceHandle));
   spi_tclMLApplicationsInfo* poAppsInfo = spi_tclMLApplicationsInfo::getInstance();
   if(NULL != poAppsInfo)
   {
      poAppsInfo->vClearAppsInfo(cou32DeviceHandle);
   }//if(NULL != poAppsInfo)
}

/***************************************************************************
** FUNCTION:  tenAppCertificationInfo spi_tclMLAppFilter::enGetAppCertStatus()
***************************************************************************/
tenAppCertificationInfo spi_tclMLAppFilter::enGetAppCertStatus(std::vector<trAppCertInfo> vecAppCertInfo,
                                                               tenAppCertificationEntity& rfenAppCertEntity)
{
   tenAppCertificationInfo enAppCertStatus = e8NOT_CERTIFIED;
   rfenAppCertEntity = e8UNKNOWN_ENTITY;

   t_String szCurrentLocale="";
   spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
   if(NULL != poAppSettings)
   {
      //Get the current Locale
      szCurrentLocale = poAppSettings->szGetRegion();
      std::transform(szCurrentLocale.begin(), szCurrentLocale.end(), szCurrentLocale.begin(), 
         ::toupper);
   }//if(NULL != poAppSettings)
//If the app is drive certified by either CCC or CCC member exit the loop.
//or else check for the all the CCC Certificate signing entity restricted/Non Restricted list
for(tItAppCertInfo itAppCertInfo = vecAppCertInfo.begin(); itAppCertInfo != vecAppCertInfo.end();itAppCertInfo++)
{
   t_String szCertEntity = itAppCertInfo->szCertEntityName;

   //Certification Entity value can be received in capital letters or small letters or mix of both.
   //So convert it to Upper case and check
   std::transform(szCertEntity.begin(), szCertEntity.end(),szCertEntity.begin(), 
      ::toupper);

   //Usage of e8DRIVE_CERTIFIED_ONLY is deprecated. It's confirmed that there wont be any application, which is only DRIVE Certified.
   //if an application DRIVE Certified, user must be able to use it in PARK mode also.
   //usage of e8BASE_CERTIFIED also deprecated, as the Spec says that all the BASE Certified applications must be PARK certified 
   //applications and should be able to use it in all regions.
   t_Bool bDriveCert = false;

   /*
   AppCertificationInfo and AppCertificationEntity values are derived based as per the below table.
   ----------------------------------------------------------------------------------------------------------
   |  CCC                  |CCC-Member           | T_e8_AppCertificationInfo    | T_e8_AppCertificationEntity|
   ----------------------------------------------------------------------------------------------------------
   |  Not certified        | Not certified       | NOT_CERTIFIED                |  NOT_CERTIFIED             |
   |  Not certified        | Non-drive certified | NONDRIVE_CERTIFIED_ONLY      |  CCC_MEMBER_CERTIFIED      |
   |  Not certified        | Drive certified     | DRIVE_CERTIFIED              |  CCC_MEMBER_CERTIFIED      |
   |  Non-drive certified  | Drive certified     | DRIVE_CERTIFIED              |  CCC_MEMBER_CERTIFIED      |
   |  Drive certified      | Non-drive certified | DRIVE_CERTIFIED              |  CCC_CERTIFIED             |
   |  Drive certified      | Drive certified     | DRIVE_CERTIFIED              |  CCC_CERTIFIED             |
   |  Non-drive certified  | Non-drive certified | NONDRIVE_CERTIFIED_ONLY      |  CCC_CERTIFIED             |
   |  Non-drive certified  | Not certified       | NONDRIVE_CERTIFIED_ONLY      |  CCC_CERTIFIED             |
   |  Drive certified      | Not certified       | DRIVE_CERTIFIED              |  CCC_CERTIFIED             |
   ----------------------------------------------------------------------------------------------------------
   */
   if(true == bCheckAppCertification(szCertEntity.c_str(),
      itAppCertInfo->vecCertEntityRestrictedList,szCurrentLocale))
   {
      enAppCertStatus = e8DRIVE_CERTIFIED;
      bDriveCert = true;
      if(szCertEntity == coszCertEntityCCC)
      {
         rfenAppCertEntity = e8CCC_CERTIFIED;
         break;
      }
      else
      {
         rfenAppCertEntity = e8CCC_MEMBER_CERTIFIED;
      }
   }//if(true == bCheckAppCertification(

   //if the App is Drive certified CCC/CCC-Member, we don't need to check for the PARK certification Status of the 
   //application. And if the app
   if(  (false == bDriveCert) && (rfenAppCertEntity != e8CCC_CERTIFIED)&&
      (true == bCheckAppCertification(szCertEntity.c_str(),
      itAppCertInfo->vecCertEntityNonRestrictedList,szCurrentLocale))  )
   {
      enAppCertStatus = e8NONDRIVE_CERTIFIED_ONLY ;
      rfenAppCertEntity = (szCertEntity == coszCertEntityCCC)? 
         e8CCC_CERTIFIED:e8CCC_MEMBER_CERTIFIED;
   }//if( (false == bDriveCert) && (rfenAppCertEnti
}//for(tItAppCertInfo itAppCertInfo = vecAppCertInfo.begin(); (i

   ETG_TRACE_USR1(("spi_tclMLAppFilter::enGetAppCertStatus-%d enAppCertEntity-%d",
      enAppCertStatus,rfenAppCertEntity));

   return enAppCertStatus;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppFilter::bUpdateAppStatus()
***************************************************************************/
t_Bool spi_tclMLAppFilter::bUpdateAppStatus(const t_U32 cou32DeviceHandle,t_U32 cou32AppHandle,
                                            tenAppStatus enAppStatus) const
{
   ETG_TRACE_USR1(("spi_tclMLAppFilter::vUpdateAppStatus-Dev-0x%x\n",cou32DeviceHandle));
   t_Bool bRet=false;
   //Check whether the app is there in the map, if it exists then only update and send the
   //app list change request to HMI
   spi_tclMLApplicationsInfo* poAppsInfo = spi_tclMLApplicationsInfo::getInstance();
   if(NULL != poAppsInfo)
   {
      bRet=poAppsInfo->bUpdateAppStatus(cou32DeviceHandle,cou32AppHandle,enAppStatus);
   }//if(NULL != poAppsInfo)
   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppFilter::bUpdateAppName()
***************************************************************************/
t_Bool spi_tclMLAppFilter::bUpdateAppName(const t_U32 cou32DeviceHandle,
                                          const t_U32 cou32AppHandle,
                                          t_String szAppName) const
{
   ETG_TRACE_USR1(("spi_tclMLAppFilter::bUpdateAppName-Dev-0x%x\n",cou32DeviceHandle));
   t_Bool bRet=false;
   //Check whether the app is there in the map, if it exists then only update and send the
   //app list change request to HMI
   spi_tclMLApplicationsInfo* poAppsInfo = spi_tclMLApplicationsInfo::getInstance();
   if(NULL != poAppsInfo)
   {
      bRet=poAppsInfo->bUpdateAppName(cou32DeviceHandle,cou32AppHandle,szAppName.c_str());
   }//if(NULL != poAppsInfo)
   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppFilter::bUpdateAppNotiSupport()
***************************************************************************/
t_Bool spi_tclMLAppFilter::bUpdateAppNotiSupport(const t_U32 cou32DeviceHandle,
                                                 const tvecMLApplist& rfcovecNotiSuppApplist) const
{
   ETG_TRACE_USR1(("spi_tclMLAppFilter::bUpdateAppNotiSupport-Dev-0x%x\n",cou32DeviceHandle));

   //Update notification support status in map.
   spi_tclMLApplicationsInfo* poAppsInfo = spi_tclMLApplicationsInfo::getInstance();
   if(NULL != poAppsInfo)
   {
      poAppsInfo->bUpdateAppNotiSupport(cou32DeviceHandle, rfcovecNotiSuppApplist);
   }//if(NULL != poAppsInfo)

   return true; //currently unused
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppFilter::bCheckAppCertification()
***************************************************************************/
t_Bool spi_tclMLAppFilter::bCheckAppCertification(t_String szCertEntity,
                                                  std::vector<t_String>& rfvecLocaleList,
                                                  t_String szCurrentLocale)
{
   //ETG_TRACE_USR1(("spi_tclMLAppFilter::bCheckAppCertification()\n"));

   t_Bool bRet=false;

   //Certification Entity value can be received in capital letters or small letters or mix of both.
   //So convert it to Uppercase and check
   std::transform(szCertEntity.begin(), szCertEntity.end(),szCertEntity.begin(),
      ::toupper);

   t_String szCCCMemberName;
   spi_tclConfigReader *poCnfgReader = spi_tclConfigReader::getInstance();
   if(NULL != poCnfgReader)
   {
      //Manufacturer name
      szCCCMemberName = poCnfgReader->szGetClientManufacturerName().c_str();
   }//if(NULL != poCnfgReader)

   std::transform(szCCCMemberName.begin(), szCCCMemberName.end(), szCCCMemberName.begin(), ::toupper);
   //Check whether the application is either CCC certified or CCC-Member certified
   if( (coszCertEntityCCC == szCertEntity) || ((coszCertEntityEmpty!= szCCCMemberName)&&
      (szCertEntity==szCCCMemberName)) )
   {
      std::vector<std::string>::iterator itszLocaleList;
      for(itszLocaleList=rfvecLocaleList.begin(); (itszLocaleList != rfvecLocaleList.end());itszLocaleList++)
      {
         t_String szLocale = (itszLocaleList->c_str());
         std::transform(szLocale.begin(), szLocale.end(),szLocale.begin(),
            ::toupper);
         //Application should be either Global certified or Certified in the 
         //current region
         if( (szCurrentLocale == szLocale)||(szLocale == coszLocaleWorld) )
         {
            bRet=true;
            break;
         }//if( (szCurrentLocale == szLocale)||(szLocale == coszLocaleWorld) )
      }//for(itszRestrictedList=itAppCertInfo->vecCertEntityRestrictedList.begin();
   }
   return bRet;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>


