/***********************************************************************/
/*!
* \file  spi_tclMLAppMngr.cpp
* \brief Mirror Link Application Manager 
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Mirror Link Application Manager 
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
26.12.2013  | Shiva Kumar Gurija    | Updated with new elements for 
                                       Handling Devices and App's Info
23.04.2014  | Shiva Kumar Gurija    | Updated with AppList Handling Chnages for 
                                      CTS Test & ML Notifications Implementation
24.07.2014  | Shiva Kumar Gurija    | Work around to fetch the missing app name
11.08.2014  | Ramya Murthy          | Revised logic to subscribe/unsubscribe 
                                      for Notification supported apps.
24.11.2014  | Shiva Kumar Gurija    | Fix for GMMY16-20349
30.03.2015  | Shiva Kumar Gurija    | Changes to convert UTF8 strings to ASCII
08.05.2015  | Shiva Kumar Gurija    | Changes to get Client profile configs from
                                     Config Reader.
12.05.2015  | Shiva Kumar Gurija    | Changes to work with ML1.2 or above Phones
21.05.2014  | Shiva Kumar Gurija    | Changes to use ML2.x or above Phones also
27.05.2015  | Shiva Kumar Gurija    | Lint Fix
06.05.2015  |Tejaswini HB           | Lint Fix
26.05.2015  |  Tejaswini H B        | Added Lint comments to suppress C++11 Errors
03.07.2015  | Shiva Kumar Gurija    | improvements in ML Version checking
09.09.2015  |  Shiva Kumar Gurija   | Sending Framebuffer blocking notification for 
                                      Invalid FB updates is moved to Wrapper. (SUZUKI-24554)
\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include <map>
#include <unistd.h>
#include "BaseTypes.h"
#include "SPITypes.h"
#include "RespBase.h"
#include "RespRegister.h"
#include "spi_tclMLVncManager.h"
#include "spi_tclMLVncCmdDiscoverer.h"
#include "spi_tclMLVncCmdNotif.h"
#include "spi_tclAppMngrDefines.h"
#include "spi_tclAppSettings.h"
#include "spi_tclConfigReader.h"
#include "spi_tclMLAppFilter.h"
#include "spi_tclMLAppDataIntf.h"
#include "spi_tclMLAppMngr.h"


#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclMLAppMngr.cpp.trc.h"
#endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static spi_tclMLVncManager* spoMLVNCMngr = NULL;
static spi_tclMLVncCmdDiscoverer* spoCmdDisc = NULL;
static spi_tclMLVncCmdViewer* spoCmdViewer = NULL;
static spi_tclMLAppFilter* spoMLAppFilter=NULL;
static spi_tclMLVncCmdNotif* spoCmdNoti=NULL;
static const t_String coszEmptyStr = "";
static std::map<t_String,t_String> m_mapUTF8ToAscii;

/***************************************************************************
** FUNCTION:  spi_tclMLAppMngr::spi_tclMLAppMngr()
***************************************************************************/
spi_tclMLAppMngr::spi_tclMLAppMngr():m_u32SelDevId(0),m_bSubscribeForEvents(false),
m_bDriveModeEnabled(false), m_bNotiSubscriptionActive(false)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr() entered "));
   m_mapIconMimeTypeInfo.clear();
}
/***************************************************************************
** FUNCTION:  spi_tclMLAppMngr::~spi_tclMLAppMngr()
***************************************************************************/
spi_tclMLAppMngr::~spi_tclMLAppMngr()
{
   ETG_TRACE_USR1(("~spi_tclMLAppMngr() entered "));

   m_u32SelDevId=0;
   m_bDriveModeEnabled = false;

   m_oIconMimeTypeLock.s16Lock();
   m_mapIconMimeTypeInfo.clear();
   m_oIconMimeTypeLock.vUnlock();

   m_oDevVNCURLsLock.s16Lock();
   m_mapDevVNCURLs.clear();
   m_oDevVNCURLsLock.vUnlock();

   //Set the variables to NULL, Free the memory in case if vUnInitialize() is called
   RELEASE_MEM(spoMLAppFilter);

   spoCmdNoti = NULL ;
   spoCmdDisc = NULL;
   spoCmdViewer = NULL ;
   spoMLVNCMngr=NULL;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppMngr::bInitialize()
***************************************************************************/
t_Bool spi_tclMLAppMngr::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::bInitialize() entered "));
   t_Bool bRet = false;

   vMapUTF8StrsToAscii();

   spoMLVNCMngr = spi_tclMLVncManager::getInstance();
   if (NULL != spoMLVNCMngr)
   {
      spoCmdDisc = spoMLVNCMngr ->poGetDiscovererInstance();
      spoCmdViewer = spoMLVNCMngr->poGetViewerInstance();
      spoCmdNoti = spoMLVNCMngr->poGetNotifInstance();
      bRet = (spoMLVNCMngr->bRegisterObject((spi_tclMLVncRespDiscoverer*) this))&&
         (spoMLVNCMngr->bRegisterObject((spi_tclMLVncRespViewer*)this))&&
         (spoMLVNCMngr->bRegisterObject((spi_tclMLVncRespNotif*)this)) &&
         (spoMLVNCMngr->bRegisterObject((spi_tclMLVncRespDAP*)this));
   }// if (NULL != pVncManager)

   spoMLAppFilter = new spi_tclMLAppFilter();
   SPI_NORMAL_ASSERT((NULL == spoCmdDisc)||(NULL == spoMLAppFilter)||(NULL == spoCmdViewer) ||
      (NULL == spoCmdNoti)||(NULL == spoMLVNCMngr));

   bRet = bRet&&((NULL != spoCmdDisc)&&(NULL != spoMLAppFilter)&&(NULL != spoCmdViewer) &&
      (NULL != spoCmdNoti)&&(NULL != spoMLVNCMngr));

   if (NULL != spoCmdNoti)
   {
      bRet = bRet&&(spoCmdNoti->bInitializeNotifications());
   }//if (NULL != spoCmdNoti)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vUninitialize()
***************************************************************************/
t_Void spi_tclMLAppMngr::vUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vUnInitialize() entered "));

   m_oIconMimeTypeLock.s16Lock();
   m_mapIconMimeTypeInfo.clear();
   m_oIconMimeTypeLock.vUnlock();

   if (NULL != spoCmdNoti)
   {
      spoCmdNoti->vUnInitializeNotifications();
   }//if (NULL != spoCmdNoti)

   spoCmdNoti = NULL ;
   spoCmdDisc = NULL;
   spoCmdViewer = NULL ;
   spoMLVNCMngr=NULL;

   //Release resources
   RELEASE_MEM(spoMLAppFilter);

}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclMLAppMngr::vRegisterAppMngrCallbacks()
***************************************************************************/
t_Void spi_tclMLAppMngr::vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs)
{
   ETG_TRACE_USR1((" spi_tclMLAppMngr::vRegisterAppMngrCallbacks"));
   //Copy call back structure
   m_rAppMngrCallbacks = corfrAppMngrCbs;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vSelectDevice()
***************************************************************************/
t_Void spi_tclMLAppMngr::vSelectDevice(const t_U32 cou32DevId,
                                       const tenDeviceConnectionReq coenConnReq)
{

	/*lint -esym(40,fpvSelectDeviceResult)fpvSelectDeviceResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vSelectDevice: Dev-0x%x, ConnReq - %d ",
      cou32DevId,ETG_ENUM(CONNECTION_REQ,coenConnReq)));

   if (coenConnReq == e8DEVCONNREQ_SELECT)
   {
      //execute deselect sequence for the last selected device.
      //This will reset the flags, in case if the deselect was not received from Connection manager
      if(cou32DevId != m_u32SelDevId)
      {
         vOnDeviceDeselect(m_u32SelDevId);
      }//if(cou32DevId != m_u32SelDevId)
      vOnDeviceSelect(cou32DevId);
   }//if (coenConnReq == e8DEVCONNREQ_SELECT)
   else
   {
      vOnDeviceDeselect(cou32DevId);
   }

   //Always return true, there is nothing major to send device selection failure
   if (NULL != m_rAppMngrCallbacks.fpvSelectDeviceResult)
   {
      (m_rAppMngrCallbacks.fpvSelectDeviceResult)(true);
   }//if (NULL != m_rAppMngrCallbacks.fpvSelectDeviceResult)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vOnDeviceSelect()
***************************************************************************/
t_Void spi_tclMLAppMngr::vOnDeviceSelect(const t_U32 cou32DevId)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vOnDeviceSelect: Already Selected Device:0x%x" \
      "Req received for Device:0x%x  Subscribed for UPNP Notifications:%d",
      m_u32SelDevId,cou32DevId,ETG_ENUM(BOOL,m_bSubscribeForEvents)));

   //Registering for UPnP Server events (AppStatus & AppList updates) is done on
   //select device result. This is done to avoid sending appstatus update to HMI,
   //before the SelectDeviceResult is posted.

   m_u32SelDevId = cou32DevId;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vOnDeviceDeselect()
***************************************************************************/
t_Void spi_tclMLAppMngr::vOnDeviceDeselect(const t_U32 cou32DevId)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vOnDeviceDeselect:Selected Sevice:0x%x" \
      "Req received for Device:0x%x  Subscribed for UPNP Notifications:%d",
      m_u32SelDevId,cou32DevId,ETG_ENUM(BOOL,m_bSubscribeForEvents)));

   //on startup or when the device is successfully disconnected m_u32SelDevId will be set to Zero.
   //so don't do any processing, when the m_u32SelDevId is zero. discard it.
   if((0 != m_u32SelDevId)&&(cou32DevId == m_u32SelDevId))
   {
      //set the selected device handle to zero, to avoid updating change in applist,
      //app status after the de selection. Unsubscribing for device notifications or
      //terminating applications can cause changes in the status of applications.
      m_u32SelDevId = 0;

      //Un subscribe for the Applications Notifications
      if (true == m_bNotiSubscriptionActive)
      {
         spoCmdNoti->bSetNotiSubscription(cou32DevId, false);
         m_bNotiSubscriptionActive = false;
      }//if (true == m_bNotiSubscriptionActive)

      //No need to send disable driver distraction avoidance request to the Viewer SDK,
      //since the viewer will be destroyed on device dis selection

      //Un subscribe for the updates of the device.
      if (true == m_bSubscribeForEvents)
      {
         trUserContext rUsrCntxt;
         spoCmdDisc->vSubscribeForDeviceEvents(cou32DevId, false, rUsrCntxt);
         m_bSubscribeForEvents = false;
      }//if (true == m_bSubscribeForEvents)

      //terminate all the launched applications of the device.
      spi_tclMLAppDataIntf oDataIntf;

      std::vector<t_U32> vecLaunchedAppIds;
      oDataIntf.vGetLaunchedAppIds(cou32DevId, vecLaunchedAppIds);

      t_U16 u16AppListLen = vecLaunchedAppIds.size();
      for (t_U16 u16Index = 0; u16Index < u16AppListLen; u16Index++)
      {
         trUserContext rUsrCntxt;
         spoCmdDisc->vTerminateApplication(rUsrCntxt, cou32DevId, vecLaunchedAppIds[u16Index]);
      }//for(t_U16 u16Index = 0 ; u16Index<u16AppListLen;u16Index++)
   }
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppMngr::bLaunchApp()
***************************************************************************/
t_Void spi_tclMLAppMngr::vLaunchApp(const t_U32 cou32DevId, 
                                    t_U32 u32AppHandle,
                                    const trUserContext& rfrcUsrCntxt)
{

   /*lint -esym(40,fpvLaunchAppResult)fpvLaunchAppResult Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclMLAppMngr::vLaunchApp:DevID-0x%x AppID-0x%x", 
      cou32DevId,u32AppHandle));

   if (NULL != spoCmdDisc)
   {
      spoCmdDisc->vLaunchApplication(rfrcUsrCntxt,cou32DevId,u32AppHandle);
   }//if (NULL != spoCmdDisc)
   else
   {
      if (m_rAppMngrCallbacks.fpvLaunchAppResult)
      {
         (m_rAppMngrCallbacks.fpvLaunchAppResult)(cou32DevId,u32AppHandle,e8DIPO_NOT_USED,
            e8UNKNOWN_ERROR,rfrcUsrCntxt);
      }//if (m_rAppMngrCallbacks.fpvLaunchAppResult)
   }

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vTerminateApp()
***************************************************************************/
t_Void spi_tclMLAppMngr::vTerminateApp(const t_U32 cou32devId,
                                       const t_U32 cou32AppId,
                                       const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1((" spi_tclMLAppMngr::vTerminateApp:DevID-0x%x AppID-0x%x", 
      cou32devId,cou32AppId));
   //@todo - check whether app handle is valid or not.
   //If the app handle is invalid, send invalid app handle error
   if (NULL != spoCmdDisc)
   {
      spoCmdDisc->vTerminateApplication(rfrcUsrCntxt,cou32devId,cou32AppId);
   }//if (NULL != spoCmdDisc)

}

/***************************************************************************
** FUNCTION: void spi_tclMLAppMngr::vGetAppDetailsList()
***************************************************************************/
t_Void spi_tclMLAppMngr::vGetAppDetailsList(const t_U32 u32DeviceHandle,
                                            t_U32& u32NumAppInfoList,
                                            std::vector<trAppDetails>& corfvecrAppDetailsList)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vGetAppList: DevID - 0x%x",u32DeviceHandle));

   corfvecrAppDetailsList.clear();
   u32NumAppInfoList = 0;

   //Fetch the complete app list, if the CTS Test is enabled,
   //else provide only UI Applications
   spi_tclMLAppDataIntf oDataIntf;
   spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance() ;
   if (NULL != poAppSettings)
   {
      (poAppSettings->bEnableCTSTest())?(oDataIntf.vGetAppList(u32DeviceHandle,corfvecrAppDetailsList)):
         (oDataIntf.vGetUIAppList(u32DeviceHandle,corfvecrAppDetailsList));
   }//if (NULL != poAppSettings)

   u32NumAppInfoList = corfvecrAppDetailsList.size();
   ETG_TRACE_USR4(("NumApps-%d\n",u32NumAppInfoList));

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vGetAppIconData()
***************************************************************************/
t_Void spi_tclMLAppMngr::vGetAppIconData(t_String szAppIconUrl, 
                                         const trUserContext& rfrcUsrCntxt)
{


   /*lint -esym(40,fpvCbAppIconDataResult)fpvCbAppIconDataResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vGetAppIconData: szAppIconUrl- %s ",szAppIconUrl.c_str()));

   //Get the application's Icon MIME Type. If it is either img/jpeg or img/png then only
   //send request for AppIcon Data or else send Error Not Supported response.
   //Currently only jpeg&png are supported.
   tenIconMimeType enIconMIMEType = e8ICON_INVALID;

   spi_tclMLAppDataIntf oDataIntf;
   enIconMIMEType = oDataIntf.enGetAppIconMIMEType(m_u32SelDevId,szAppIconUrl.c_str());

   ETG_TRACE_USR2(("spi_tclMLAppMngr::vGetAppIconData: MIME Type - %d ",
      ETG_ENUM(ICON_MIME_TYPE,enIconMIMEType)));

   if ((e8ICON_INVALID != enIconMIMEType) && (NULL != spoCmdDisc))
   {
      //Insert the Application Icon MIME Type for which the Icon data fetch req has come.
      //This is required, to send the update to HMI, for which which MIME Type the data is fetched.
      m_oIconMimeTypeLock.s16Lock();
      std::pair<t_U32,t_String> mapIconInfoKey = std::make_pair(m_u32SelDevId,szAppIconUrl.c_str());
      m_mapIconMimeTypeInfo.insert(std::pair< std::pair<t_U32,t_String>,tenIconMimeType >(mapIconInfoKey,
         enIconMIMEType));
      m_oIconMimeTypeLock.vUnlock();

      spoCmdDisc->vGetAppIconData(m_u32SelDevId,szAppIconUrl.c_str(),rfrcUsrCntxt);
   }//if ((e8ICON_INVALID != enIconMIMEType)&&
   else
   {
      if ( NULL != m_rAppMngrCallbacks.fpvCbAppIconDataResult)
      {
         (m_rAppMngrCallbacks.fpvCbAppIconDataResult)(enIconMIMEType,NULL,0,rfrcUsrCntxt);
      } //if ( NULL != m_rAppMngrCallbacks.fpvCbAppIconDataResult)
   }

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vGetAppInfo()
***************************************************************************/
t_Void spi_tclMLAppMngr::vGetAppInfo(const t_U32 cou32DevId, 
                                     const t_U32 cou32AppId,
                                     trAppDetails& rfrAppDetails)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vGetAppInfo:DevID-0x%x AppID-0x%x",
      cou32DevId,cou32AppId));

   spi_tclMLAppDataIntf oDataIntf;
   oDataIntf.vGetAppInfo(cou32DevId,cou32AppId,rfrAppDetails);

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vDisplayAllAppsInfo()
***************************************************************************/
t_Void spi_tclMLAppMngr::vDisplayAllAppsInfo(const t_U32 cou32DevId)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vDisplayAllAppsInfo:DevID-0x%x ",
      cou32DevId));

   spi_tclMLAppDataIntf oDataIntf;
   oDataIntf.vDisplayAllAppsInfo(cou32DevId);

}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLAppMngr::vSetAppIconAttributes()
***************************************************************************/
t_Bool spi_tclMLAppMngr::bSetAppIconAttributes(t_U32 u32DeviId,
                                               t_U32 u32AppHandle,
                                               const trIconAttributes &rfrIconAttributest)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::bSetAppIconAttributes:DevID-0x%x,AppID-0x%x ",
      u32DeviId,u32AppHandle));
   //Currently not supported return Error not supported.
   //@todo - Implement it later

   //if the setAppIconAttr is success, then update the Icon attribute in the 
   //app info storage also

   SPI_INTENTIONALLY_UNUSED(u32DeviId);
   SPI_INTENTIONALLY_UNUSED(u32AppHandle);
   SPI_INTENTIONALLY_UNUSED(rfrIconAttributest);
   return false;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vSetVehicleMode()
***************************************************************************/
t_Void spi_tclMLAppMngr::vSetVehicleMode(t_Bool bEnableDriveMode)
{

	/*lint -esym(40,fpvNotifySessionStatus)fpvNotifySessionStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vSetVehicleMode:Drive mode enabled-%d ",
      ETG_ENUM(BOOL,bEnableDriveMode)));

   //Send the request to SDK and set the requested mode
   if (m_bDriveModeEnabled != bEnableDriveMode)
   {
      //Set the current mode
      m_bDriveModeEnabled = bEnableDriveMode;
      t_Bool bSendSessionSuspendUpdate = false;

      if ((NULL != spoCmdViewer)&& ( NULL != spoCmdDisc ))
      {
         spoCmdViewer->vTriggerSetDriverDistractionAvoidance(bEnableDriveMode, m_u32SelDevId);

         if( true == bEnableDriveMode )
         {        
            trVersionInfo rVersionInfo;
            spoCmdDisc->vGetDeviceVersion(m_u32SelDevId,rVersionInfo);

            //In case currently selected device is ML1.0 device, directly send session suspend update to HMI.
            if( true == bIsML10Device(rVersionInfo.u32MajorVersion,rVersionInfo.u32MinorVersion))
            {
               bSendSessionSuspendUpdate = true;
            }//if( (rVersionInfo.u32MajorVersion == 1) && 
            //If it is ML1.1 or above device, send blocking notification to Phone and Session suspend upate to HMI.
            else 
            { 
               spi_tclMLAppDataIntf oMLDataIntf;

               m_oContextInfoLock.s16Lock();
               if(false == oMLDataIntf.bIsAppAllowedToUse(m_u32SelDevId,m_rAppCntxtInfo.u32AppUUID,
                  m_bDriveModeEnabled))
               {
                  //Application which is there on the FG is not allowed to use in the current vehicle mode.
                  //Send the blocking request to Phone and Notification to HMI.
                  spoCmdViewer->bTriggerFramebufferBlockingNotification(m_rAppCntxtInfo.u32AppUUID,
                     m_rAppCntxtInfo.rMLRect,e16MLAPPLICATION_UNIQUEID_NOTALLOWED,m_u32SelDevId);

                  bSendSessionSuspendUpdate = true;

               }//if(false == oMLDataIntf.bIsAppAllowedToUse(m_u32SelD
               m_oContextInfoLock.vUnlock();

            }//else 
         }//if(true == bEnableDriveMode)
      }//if (NULL != spoCmdViewer)

      if( (true == bSendSessionSuspendUpdate) && 
         (NULL != m_rAppMngrCallbacks.fpvNotifySessionStatus) )
      {
         (m_rAppMngrCallbacks.fpvNotifySessionStatus)(m_u32SelDevId,
            e8DEV_TYPE_MIRRORLINK,e8_SESSION_SUSPENDED_NON_DRIVE_APP);
      }//if( (true == bSendSessionSuspendUpdate) && (NULL != m_rVideo

      //There is a change in the vehicle mode.
      //get the list of notifications supported in the particular vehicle mode and 
      //Subscribe for notifications of that apps
      vSubscribeNotiAllowedApps(m_u32SelDevId);

   }//if (m_bDriveModeEnabled != bEnableDriveMode)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vSetVehicleConfig()
***************************************************************************/
t_Void spi_tclMLAppMngr::vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vSetVehicleConfig:%d",enVehicleConfig));
   SPI_INTENTIONALLY_UNUSED(bSetConfig);

   switch(enVehicleConfig)
   {
   case e8PARK_MODE:
   case e8DRIVE_MODE:
      {
         t_Bool bEnableDriveMode = (enVehicleConfig == e8DRIVE_MODE);
         vSetVehicleMode(bEnableDriveMode);
      }
      break;
   default:
      {
         //Nothin to do
      }
      break;
   }//switch(enVehicleConfig)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppMngr::bCheckAppValidity()
**************************************************************************/
t_Bool spi_tclMLAppMngr::bCheckAppValidity(const t_U32 cou32DevId, 
                                           const t_U32 cou32AppId)
{
   ETG_TRACE_USR1((" spi_tclMLAppMngr::bCheckAppValidity:DevID-0x%x AppID-0x%x ",
      cou32DevId,cou32AppId));
   t_Bool bRet = false;

   spi_tclMLAppDataIntf oDataIntf;
   bRet = oDataIntf.bCheckAppValidity(cou32DevId,cou32AppId);

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vDisplayAppcertificationInfo()
***************************************************************************/
t_Void spi_tclMLAppMngr::vDisplayAppcertificationInfo(const t_U32 cou32DevId,
                                                      const t_U32 cou32AppId)
{
   if (NULL != spoCmdDisc)
   {
      spoCmdDisc->vDisplayAppcertificationInfo(cou32DevId,cou32AppId);
   }//(NULL != spoCmdDisc)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppMngr::bSetClientProfile()
***************************************************************************/
t_Bool spi_tclMLAppMngr::bSetClientProfile(const t_U32 cou32DevId,
                                           const trClientProfile& corfrClientProfile)
{
   t_Bool bRet=false;

   if (NULL != spoCmdDisc)
   {
      trUserContext rUsrCntxt;
      bRet=spoCmdDisc->bSetClientProfile(cou32DevId,corfrClientProfile,rUsrCntxt);
   }//if (NULL != spoCmdDisc)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vSetVehicleBTAddress()
***************************************************************************/
t_Void spi_tclMLAppMngr::vSetVehicleBTAddress(const t_U32 cou32DevId,
                                              t_String szBTAddress,
                                              const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vSetVehicleBTAddress: DevID-0x%x, BTAddr-%s ",
         cou32DevId, szBTAddress.c_str()));

   trClientProfile rClntProfile;
   rClntProfile.szProfileBdAddr = szBTAddress.c_str();
   if ((NULL != spoCmdDisc) && (false == spoCmdDisc->bSetClientProfile(cou32DevId,
      rClntProfile,corfrUsrCntxt)))
   {
      ETG_TRACE_ERR(("spi_tclMLAppMngr::vSetVehicleBTAddress:Error in Setting BT Address"));
   }//if (NULL != spoCmdDisc)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vSetMLNotificationEnabledInfo()
***************************************************************************/
t_Void spi_tclMLAppMngr::vSetMLNotificationEnabledInfo(const t_U32 cou32DevId,
                                                       t_U16 u16NumNotiEnableList,
                                                       std::vector<trNotiEnable> vecrNotiEnableList,
                                                       const trUserContext& corfrUsrCntxt)
{

	/*lint -esym(40,fpvEnableNotiResult)fpvEnableNotiResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vSetMLNotificationEnabledInfo:DevID-0x%x,Num.Apps-%d",
      cou32DevId, u16NumNotiEnableList));

   //if the u16NumNotiEnableList is 0xFFFF & drive mode, remove all the non certified apps from the 
   //Noti supported application list and send the request to SDK
   if ((NULL != spoCmdNoti) &&
      (false == spoCmdNoti->bEnableAppNotifications(
            cou32DevId, u16NumNotiEnableList, vecrNotiEnableList, corfrUsrCntxt)))
   {
      ETG_TRACE_ERR(("spi_tclMLAppMngr::vSetMLNotificationEnabledInfo: Enabling app notifications failed! \n"));

      //send error response
      if ((corEmptyUsrContext != corfrUsrCntxt) && (NULL != m_rAppMngrCallbacks.fpvEnableNotiResult))
      {
         (m_rAppMngrCallbacks.fpvEnableNotiResult)(cou32DevId, e8UNSUPPORTED_OPERATION, corfrUsrCntxt);
      }//if ((corEmptyUsrContext != corfrUsrCntxt) && (NULL != m_rAppMngrCallbacks.fpvEnableNotiResult))
   }//if ((NULL != spoCmdNoti)&&
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vInvokeNotificationAction()
***************************************************************************/
t_Void spi_tclMLAppMngr::vInvokeNotificationAction(const t_U32 cou32DevId,
                                 t_U32 u32NotificationID,
                                 t_U32 u32NotificationActionID,
                                 const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vInvokeNotificationAction:DevID-0x%x,NotiID-0x%x NotiActionID-0x%x",
      cou32DevId,u32NotificationID,u32NotificationActionID));

   if (NULL != spoCmdNoti)
   {
      spoCmdNoti->vInvokeNotiAction(cou32DevId,u32NotificationID,u32NotificationActionID,
         corfrUsrCntxt);
   }// if (NULL != spoCmdNoti)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vSetMLNotificationOnOff()
***************************************************************************/
t_Void spi_tclMLAppMngr::vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
{
   ETG_TRACE_USR1((" spi_tclMLAppMngr::vSetMLNotificationOnOff:bSetNotificationsOn-%d ",
      ETG_ENUM(BOOL,bSetNotificationsOn)));

   if (true == bSetNotificationsOn)
   {
      //if the session is established with any device, send Enable/Disable Notifications request
      //to the device
      vSubscribeNotiAllowedApps(m_u32SelDevId);
   }
   else
   {
      //Unsubscribe for all notifications
      std::vector<trNotiEnable> vecrNotiEnableList; //empty list
      vSetMLNotificationEnabledInfo(m_u32SelDevId, 0, vecrNotiEnableList, corEmptyUsrContext);
   }
}


/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vDisplayAppListXml()
***************************************************************************/
t_Void spi_tclMLAppMngr::vDisplayAppListXml(const t_U32 cou32DevId)
{
   ETG_TRACE_USR1((" spi_tclMLAppMngr::vDisplayAppListXml:DevID-0x%x ",cou32DevId));

   if (NULL != spoCmdDisc)
   {
      spoCmdDisc->vDisplayAppListXml(cou32DevId);
   }//if (NULL!=spoCmdDisc)
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostDeviceInfo
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostDeviceInfo(
   const t_U32 cou32DevId,
   const trMLDeviceInfo& corfrMLDeviceInfo)
{
   ETG_TRACE_USR1((" spi_tclMLAppMngr::vPostDeviceInfo:DevID-0x%x ",cou32DevId));
   SPI_INTENTIONALLY_UNUSED(corfrMLDeviceInfo);
   if ((NULL != spoCmdDisc ) && (NULL != spoCmdNoti) && (e8DEV_TYPE_MIRRORLINK == corfrMLDeviceInfo.enDeviceCategory))
   {
      //Set the client Profile. At least Profile Manufacturer must be set to get the member certified applications.
      //And Icon preferences can be set, before the application details are fetched.
      trClientProfile rClntProfile;
      rClntProfile.szRtpPayLoadTypes = "98,99";
      rClntProfile.u32ProfileMajorVersion = cou32MLClientMajorversion ;
      rClntProfile.u32ProfileMinorVersion = cou32MLClientMinorVersion ;
      rClntProfile.bNotiUISupport = cobMLNotiUISupport;
      rClntProfile.u8NotiMaxActions = cou8MLNotiMaxActions;
      //@TODO: Set actionName, notiTitle & notiBody MaxLengths.

      spi_tclConfigReader *poCnfgReader = spi_tclConfigReader::getInstance();
      if(NULL != poCnfgReader)
      {
         //Set some ID to the ML client profile. "0" is the default client profile and it 
         //must be supported by all the ML Servers.
         rClntProfile.szClientProfileId = poCnfgReader->szGetClientID().c_str();

         //Manufacturer name is required to fetch the CCC Member certified apps
         rClntProfile.szProfileManufacturer = poCnfgReader->szGetClientManufacturerName().c_str();

         //Set the Friendly name
         rClntProfile.szFriendlyName = poCnfgReader->szGetClientFriendlyName().c_str();

      }//if(NULL != poCnfgReader)

      //delete the below commented part later, if it is no more required 
      /*
      spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance() ;
      if (NULL != poAppSettings)
      {
         //@todo - Extend policy & use this if required
         //HMI directly converts & displays the Icon's on HMI (Discussed with Suzuki & GM).
         //If required for PSA, Policy will be extended and read from that
         trIconAttributes rIconAttr;
         poAppSettings->vGetIconPreferences(rIconAttr);
         rClntProfile.rIconPreferences = rIconAttr;
      }//if (NULL != poAppSettings)
      */

      trUserContext rUsrCntxt;
      //If the set client profile request is sent successfully, fetch the Applist
      //after receiving the SetClientProfile result, if the request itself fails,
      //retrieve the Application's info with the default values.
      if (false == spoCmdDisc->bSetClientProfile(cou32DevId,rClntProfile,rUsrCntxt))
      {
         spoCmdDisc->vGetAppList(rUsrCntxt,cou32DevId);
      }//if (false == spoCmdDisc->bSetClientProfile(
   }//if (NULL != spoCmdDisc )
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostDeviceDisconncted()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostDeviceDisconnected(const t_U32 cou32DevId)
{
   ETG_TRACE_USR1((" spi_tclMLAppMngr::vPostDeviceDisconncted:DevID-0x%x "
      ,cou32DevId));
   //Clear the content in the Storage.
   //send it to app filter
   if (NULL != spoMLAppFilter)
   {
      spoMLAppFilter->vClearAppInfo(cou32DevId);
   }//if (NULL != spoMLAppFilter)

   if (NULL != spoCmdNoti)
   {
      spoCmdNoti->vDeviceDisconnected(cou32DevId);
   }//if (NULL != spoCmdNoti)

   //Clear the DAP attested URL's issue
   m_oDevVNCURLsLock.s16Lock();
   if(SPI_MAP_NOT_EMPTY(m_mapDevVNCURLs))
   {
      if(  (m_mapDevVNCURLs.end() != m_mapDevVNCURLs.find(cou32DevId))  )
      {
         m_mapDevVNCURLs.erase(cou32DevId);
      }//if((m_mapDevVNCURLs.end() != m_mapDevVNCURLs.find
   }//if(SPI_MAP_NOT_EMPTY(m_mapDevVNCURLs))
   m_oDevVNCURLsLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostAppList(trUserContext..
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostAppList(trUserContext rUserContext,
                                      const t_U32 cou32DevId,
                                      const std::vector<t_U32>& corfvecAppList )
{

	/*lint -esym(40,fpvNotifyAppListChangeCb)fpvNotifyAppListChangeCb Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAppManager:vPostAppList: DevID-0x%x ",cou32DevId));

   t_U16 u16AppListSize = corfvecAppList.size();
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   //Observed that appList is received with the size zero sometimes, whenever there is a change in 
   //status of any application. This can set the applications info of the devices to NULL.
   // so do not process or do anything, if the applist is empty

   std::vector<trApplicationInfo> vecAppInfo;

   if (u16AppListSize>0)
   {
      if ( NULL != spoCmdDisc )
      {
         for(t_U16 u16Index = 0; u16Index < u16AppListSize ; u16Index++ )
         {
            //Insert AppList in the map
            trApplicationInfo rAppInfo;
            spoCmdDisc->vGetAppInfo(cou32DevId,corfvecAppList[u16Index],rAppInfo);
            vecAppInfo.push_back(rAppInfo);
         }//for(t_U16 u16Index = 0; u16Index < u16AppListSize ; u16Index++ )

         //For debugging purpose
         u16AppListSize = vecAppInfo.size();
         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Applications supported by the Device-0x%x are %d "
            ,cou32DevId,u16AppListSize));

         for(t_U16 U16Index=0;U16Index<u16AppListSize;U16Index++)
         {
            ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS," AppID - 0x%x AppName - %s ",
               vecAppInfo[U16Index].rAppdetails.u32AppHandle,vecAppInfo[U16Index].rAppdetails.szAppName.c_str()));
            ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS," AppStatus - %d  AppCategory - 0x%x",
               ETG_ENUM(APP_STATUS,vecAppInfo[U16Index].rAppdetails.enAppStatus),vecAppInfo[U16Index].rAppdetails.enAppCategory));

            //Application name is not fetched, try to fetch it synchronously again.
            if(coszUnKnownAppName == vecAppInfo[U16Index].rAppdetails.szAppName)
            {
               vecAppInfo[U16Index].rAppdetails.szAppName = spoCmdDisc->szFetchAppName(cou32DevId,
                  vecAppInfo[U16Index].rAppdetails.u32AppHandle).c_str();
            }//if(coszUnKnownAppName == vecAppInfo[U16Index].rAppdetails.szAppName)

            // We have already tried to fetch the Application Category for 2 times, 
            // But still the SDK returned an error.try to fetch it synchronously again for the 3rd time.
            if(e32APPUNKNOWN == vecAppInfo[U16Index].rAppdetails.enAppCategory)
            {
               vecAppInfo[U16Index].rAppdetails.enAppCategory = spoCmdDisc->enGetAppCategory(cou32DevId,
                  vecAppInfo[U16Index].rAppdetails.u32AppHandle);
            }//if(e32APPUNKNOWN == vecAppInfo[U16Index].rAppdetails.enAppCategory)

            vecAppInfo[U16Index].rAppdetails.szAppName = szConvertUTF8ToAscii(
            		vecAppInfo[U16Index].rAppdetails.szAppName.c_str()).c_str();

         }//for(t_U16 U16Index=0;U16Index<u16AppListSize;U16Index++)

         //Filter and insert the data into storage
         trVersionInfo rVersionInfo;
         spoCmdDisc->vGetDeviceVersion(cou32DevId,rVersionInfo);
         if (NULL != spoMLAppFilter)
         {
            spoMLAppFilter->vFilterApplications(cou32DevId,rVersionInfo,vecAppInfo);
         }//if (NULL != spoMLAppFilter)


         //Post the update to HMI, if the app list is fetched for the selected device
         if (
            (m_u32SelDevId == cou32DevId)
            &&
            (NULL != m_rAppMngrCallbacks.fpvNotifyAppListChangeCb)
            )
         {
            (m_rAppMngrCallbacks.fpvNotifyAppListChangeCb)(cou32DevId,e8APP_LIST_CHANGED);
         }//if ((m_u32SelDevId == cou32DeviceHandle)

         // Fetching the application name synchronously is failed for 2 times.
         //Try to fetch the application name asynchronously, if the name is invalid/empty
         for(t_U16 U16Index=0;U16Index<u16AppListSize;U16Index++)
         {
            if (coszUnKnownAppName == vecAppInfo[U16Index].rAppdetails.szAppName)
            {
               spoCmdDisc->vFetchAppName(cou32DevId,vecAppInfo[U16Index].rAppdetails.u32AppHandle,rUserContext);
            }//if (coszUnKnownAppName == vecAppInfo[U16Index].rAppdetails.szAppName)
         }//for(t_U16 U16Index=0;U16Index<u16AppListSize;U16Index++)

         if (NULL != spoCmdNoti)
         {
            //Get the list of applications that supports notification
            spoCmdNoti->vGetNotiSupportedApps(cou32DevId,rUserContext);
         }      
      }//if ( NULL != spoCmdDisc )
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostAppInfo(trUserContext...
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostAppInfo(trUserContext rUserContext,
                                      const t_U32 cou32DevId,
                                      t_U32 u32AppId,
                                      const trApplicationInfo& corfrAppInfo)
{
   ETG_TRACE_USR1(("vPostAppInfo: DeviceID-0x%x AppID-0x%x",cou32DevId,u32AppId));
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   SPI_INTENTIONALLY_UNUSED(corfrAppInfo);

   //Currently unused
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostTerminateAppStatus(trUserContext..
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostTerminateAppStatus(
   trUserContext rUserContext,
   const t_U32 cou32DevId,
   t_U32 u32AppId,
   t_Bool bResult)
{

	/*lint -esym(40,fpvTerminateAppResult)fpvTerminateAppResult Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclMLAppMngr::vPostTerminateAppStatus: DevID-0x%x AppID-0x%x Result-%d "
      ,cou32DevId,u32AppId,ETG_ENUM(BOOL,bResult)));

   tenErrorCode enErrorCode = (bResult)?e8NO_ERRORS:e8TERMINATE_FAILED;
   //Post the Result to tcl App Mngr, if the response is for the currently selected device.
   if ((m_u32SelDevId == cou32DevId) && (NULL != m_rAppMngrCallbacks.fpvTerminateAppResult))
   {
      (m_rAppMngrCallbacks.fpvTerminateAppResult)(cou32DevId, u32AppId, enErrorCode, rUserContext);
   }//if ( NULL != m_rAppMngrCallbacks.fpvTerminateAppResult )
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostLaunchAppStatus()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostLaunchAppStatus(
   trUserContext rUserContext,
   const t_U32 cou32DevId,
   t_U32 u32AppId,
   t_Bool bResult)
{

	/*lint -esym(40,fpvLaunchAppResult)fpvLaunchAppResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr: vPostLaunchAppStatus - DevID:0x%x AppID:0x%x Result:%d",
      cou32DevId,u32AppId,ETG_ENUM(BOOL,bResult)));

   if(NULL != spoCmdDisc)
   {
      trVersionInfo rVerInfo;
      spoCmdDisc->vGetDeviceVersion(cou32DevId,rVerInfo);
      
      if( false == bIsML10Device(rVerInfo.u32MajorVersion,rVerInfo.u32MinorVersion) )
      {
         //For ML1.1 or above Devices, Application's URL and the VNC URL recieved in the DAP attestation response must be same.
         //If the Phone doesn't list, VNC Component in the DAP attestation response, don't perform validation of the VNC URL's.
         //For MLS, it's not mandatory to list VNC Component in the DAP set. It is must to list only Mirrorlink Device and UPNP 
         //Components in the DAP minimum set. Phone may or may not list VNC component. In such case ignore validation of VNC URL's
         //for ML1.0 Devices, send true response always.
         bResult = false;

         //Fetch the Applications VNC URL
         t_String szAppUri;
         t_String szAppProtocolID;
         spoCmdDisc->vFetchAppUri(cou32DevId,u32AppId,szAppUri,szAppProtocolID);
         ETG_TRACE_USR4(("ML AppMngr: vPostLaunchAppStatus - VNC URI of the launched app is : %s",
            szAppUri.c_str()));
         ETG_TRACE_USR4(("ML AppMngr: vPostLaunchAppStatus - Protocol ID of launched app is : %s",
            szAppProtocolID.c_str()));

         //Launch app response will be received for the apps, when the sub modules launches the applications with out 
         //discoverer listener id's. 
         //So, for ex, if the DAP or CDB or RTP apps are launched, dont send the TRUE response to HMI.
         //And termination of the apps, whose protocol ID is not VNC is also not required as terminating RTP apps could cause loss of audio
         if("VNC" == szAppProtocolID)
         {
            m_oDevVNCURLsLock.s16Lock();

            //Device Handle & VNC URL of it will be available, only if the Phone has listed the VNC-Server in the DAP Attestation response.
            //So allow the usage of app, even if the URL is not available in map,as we need to allow the Phones which are not 
            //listed VNC-Server in Attestation response.
            if(SPI_MAP_NOT_EMPTY(m_mapDevVNCURLs))
            {
               if(m_mapDevVNCURLs.end() != m_mapDevVNCURLs.find(cou32DevId))
               {
                  //If Device is available in map and URL"s are not matching, don't allow the usage of app.
                  //If the VNC Componnet is listed and the VNC URL is empry, then also don't allow the User to use apps.
                  //Ignore validation only when VNC Component is not listed in Attestation response
                  //Send the error response to HMI and request for termination of app, in failure case
                  if((coszEmptyStr != szAppUri)&&(szAppUri == m_mapDevVNCURLs[cou32DevId])) 
                  {
                     ETG_TRACE_USR4(("ML AppMngr: vPostLaunchAppStatus - VNC URL in DAP Attestation Response: %s -URL's are matching - Allow the Device",
                        m_mapDevVNCURLs[cou32DevId].c_str()));
                     //URL's are matching. Send the launch app response as true
                     bResult = true;
                  }//if((m_mapDevVNCURLs.end() != m_mapDevVNCURLs.find
                  else
                  {
                     ETG_TRACE_USR4(("ML AppMngr: vPostLaunchAppStatus - App URI is empty or VNC URL's available with App" \
                        "and Attestation Response are not same - Device will not be allowed to Use"));
                  }
               }
               else
               {
                  ETG_TRACE_USR4(("ML AppMngr: vPostLaunchAppStatus - This Device has not listed VNC component in DAP minimum set." \
                     "Ignore validation of VNC URL's"));
                  bResult = true;
               }
            }//if(SPI_MAP_NOT_EMPTY(m_mapDevVNCURLs))
            else
            {
               ETG_TRACE_USR4(("ML AppMngr: vPostLaunchAppStatus - This Device has not listed VNC component in DAP minimum set." \
                  "Ignore validation of VNC URL's"));
               bResult = true;
            }

            m_oDevVNCURLsLock.vUnlock();

            if(false == bResult)
            {
               //Application URL is not attested. Terminate the application
               trUserContext rUsrCntxt;
               spoCmdDisc->vTerminateApplication(rUsrCntxt,cou32DevId,u32AppId);
            }//if(false == bResult)
         }//if("VNC" == szAppProtocolID)

      }//if( false == bIsML10Device(rVerInfo.u32MajorVersion,rVerInfo.u32MinorVersion) )


      if ( NULL != m_rAppMngrCallbacks.fpvLaunchAppResult )
      {
         tenErrorCode enErrorCode = (bResult)? e8NO_ERRORS : e8LAUNCH_FAILED;
         (m_rAppMngrCallbacks.fpvLaunchAppResult)(cou32DevId, u32AppId, e8DIPO_NOT_USED, 
            enErrorCode, rUserContext);
      }//if ( NULL != m_rAppMngrCallbacks.fpvLaunchAppResult )

   }//if(NULL != spoCmdDisc)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPostAppIconData()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostAppIconData(trUserContext rUsrCntxt,
                                          const t_U32 cou32DevId,
                                          t_String szAppIconUrl,
                                          const t_U8* pcou8BinaryData,
                                          const t_U32 u32Len)
{

	/*lint -esym(40,fpvCbAppIconDataResult)fpvCbAppIconDataResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostAppIconData:DevID-0x%x Icon Length-%d Icon Url-%s",
      cou32DevId,u32Len,szAppIconUrl.c_str()));

   tenIconMimeType enIconMIMEType = e8ICON_INVALID;

   //Get the MIME Type of the Icon for which we have fetched Icon Data
   m_oIconMimeTypeLock.s16Lock();

   if (SPI_MAP_NOT_EMPTY(m_mapIconMimeTypeInfo))
   {
      std::pair<t_U32,t_String> mapIconMimeTypeInfoKey = std::make_pair(cou32DevId,szAppIconUrl);

      std::map< std::pair<t_U32,t_String>,tenIconMimeType >::iterator itmapIconMimeTypeInfo = 
         m_mapIconMimeTypeInfo.find(mapIconMimeTypeInfoKey);

      if ( itmapIconMimeTypeInfo != m_mapIconMimeTypeInfo.end())
      {
         enIconMIMEType = itmapIconMimeTypeInfo->second;
         m_mapIconMimeTypeInfo.erase(itmapIconMimeTypeInfo);
      }//if ( itmapIconInfo != m_mapIconInfo.end())
   }//if (SPI_MAP_NOT_EMPTY(m_mapAppInfo))

   m_oIconMimeTypeLock.vUnlock();

   ETG_TRACE_USR4(("spi_tclMLAppMngr::vPostAppIconData: IconMIMEType-%d ", ETG_ENUM(ICON_MIME_TYPE,enIconMIMEType)));
   //Post response to HMI
   if ( NULL != m_rAppMngrCallbacks.fpvCbAppIconDataResult)
   {
      (m_rAppMngrCallbacks.fpvCbAppIconDataResult)(enIconMIMEType, pcou8BinaryData, u32Len, rUsrCntxt);
   }//if ( NULL != m_rAppMngrCallbacks.fpvCbAppIconDataResult)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPostDevEventSubscriptionStatus()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostDevEventSubscriptionStatus(const trUserContext& corfrUsrCntxt,
                                                         const t_U32 cou32DevId,
                                                         tenEventSubscription enEventSubscription,
                                                         t_Bool bResult)
{
   ETG_TRACE_USR1(("vPostDevEventSubscriptionStatus: DevID-0x%x Subscription-%d Result-%d",
      cou32DevId,ETG_ENUM(DEV_EVENT_SUB,enEventSubscription),
      ETG_ENUM(BOOL,bResult)));
   //This response is unused - just for debugging purpose
   //Device shall take lot of time, to send response for the event subscription request
   SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostAppStatus()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostAppStatus(trUserContext rUserContext,
                                        const t_U32 cou32DevId,
                                        t_U32 u32AppId,
                                        tenAppStatus enAppStatus)
{

   ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Change in AppStatus: DevID-0x%x AppID-0x%x Status-%d",
      cou32DevId,u32AppId,ETG_ENUM(APP_STATUS,enAppStatus)));

   SPI_INTENTIONALLY_UNUSED(rUserContext);

   t_Bool bNotifyApplistChange=false;
   if (NULL != spoMLAppFilter)
   {
      //Check whether the application is there in the stored application list
      //If the app is not stored( not certified or doesn't satisify filtering criteria,
      //dont update HMI about the change in App list
      bNotifyApplistChange=spoMLAppFilter->bUpdateAppStatus(cou32DevId,u32AppId,enAppStatus);
   }//if (NULL != spoMLAppFilter)

   //Post the change in App List update to HMI, If this application is
   //already inserted in the applications info storage.
   if ((m_u32SelDevId == cou32DevId) &&  (true == bNotifyApplistChange) &&
      (NULL != m_rAppMngrCallbacks.fpvNotifyAppListChangeCb))
   {
      (m_rAppMngrCallbacks.fpvNotifyAppListChangeCb)(cou32DevId,e8APP_LIST_CHANGED);
   }//if ((true == bUpdateAppListChange)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostDriverDistractionAvoidanceResult
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostDriverDistractionAvoidanceResult(t_Bool bDriverDistAvoided,
                                                               const t_U32 cou32DevId)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostDriverDistractionAvoidanceResult: DevID-0x%x, Driver Distraction Avoidance Enabled-%d ",
      cou32DevId, ETG_ENUM(BOOL, bDriverDistAvoided)));
   //Response is unused. No need to send response for setDriveMode
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclMLAppMngr::vOnSelectDeviceResult(const t_U32 cou32DevId,
                                               const tenDeviceConnectionReq coenConnReq,
                                               const tenResponseCode coenRespCode)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vOnSelectDeviceResult: DevID-0x%x, ConnReq-%d, RespCode-%d ",
      cou32DevId, ETG_ENUM(CONNECTION_REQ,coenConnReq), ETG_ENUM(RESPONSE_CODE,coenRespCode)));

   if (e8SUCCESS == coenRespCode)
   {
      //On selection, subscribe for notifications of the applications (if the notifications was set to ON)
      if ((0 != cou32DevId) && (e8DEVCONNREQ_SELECT == coenConnReq) && (NULL != spoCmdDisc) && (NULL != spoCmdNoti))
      {

         //Subscribe for UPnP Application Server service updates from Phone
         if (false == m_bSubscribeForEvents)
         {
            trUserContext rcUsrCntxt;
            spoCmdDisc->vSubscribeForDeviceEvents(cou32DevId, true, rcUsrCntxt);
            m_bSubscribeForEvents = true;
         }//if ((u32DevId != m_u32SelDevId)

         //Subscribe for the Notification Events from the device
         m_bNotiSubscriptionActive = spoCmdNoti->bSetNotiSubscription(cou32DevId, true);

         ETG_TRACE_USR3(("spi_tclMLAppMngr::vOnSelectDeviceResult: bNotiSubscriptionActive-%d",
            ETG_ENUM(BOOL,m_bNotiSubscriptionActive)));

         //Subscribe for the Notifications of the applications based on the vehicle mode
         vSubscribeNotiAllowedApps(cou32DevId);

      }//if ((0 != cou32DevId) && (e

      //post the response to HMI
      if (NULL != m_rAppMngrCallbacks.fpvNotifyAppListChangeCb)
      {
         //If the application list is not fetched by the time we receive select device, Empty app list will be sent.
         //And again App list change notification will be sent, after the application details are fetched.
         //When a new device is selected, update the app list change and if the device is disconnected,
         //update with the App status unknown status.
         tenAppStatusInfo enAppStatus = (coenConnReq == e8DEVCONNREQ_SELECT) ?
            (e8APP_LIST_CHANGED) : (e8APP_STATUS_NOT_KNOWN);

         (m_rAppMngrCallbacks.fpvNotifyAppListChangeCb)(cou32DevId, enAppStatus);

      }//if (NULL != m_rAppMngrCallbacks.fpvNotifyAppListChangeCb)

   }//if (e8SUCCESS == coenRespCode)
   //Device selection is failed. clear internal flags.
   else if ( (e8FAILURE == coenRespCode) && (e8DEVCONNREQ_SELECT == coenConnReq) )
   {
      //device selection is failed, reset device id
      m_u32SelDevId = 0;
   } //if ( (e8FAILURE == coenRespCode) && (e8DEVCONNREQ_SELECT == coenConnReq) )

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPostSetClientProfileResult()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostSetClientProfileResult(const trUserContext& corfrUsrCntxt,
                                                     const t_U32 cou32DevId,
                                                     t_Bool bResult)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostSetClientProfileResult: DevID-0x%x, Result-%d ",
      cou32DevId, ETG_ENUM(BOOL, bResult)));
   SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);

   //Fetch the App List after the Client Profile is set, irrespective of the result
   //if the request is success, Device is will set&provide the info in the requested format/values
   //else it will provide the default values supported by the device
   if (NULL!=spoCmdDisc)
   {
      spoCmdDisc->vGetAppList(corfrUsrCntxt, cou32DevId);
   }//if (NULL!=spoCmdDisc)

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPostNotiAllowedApps()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostNotiAllowedApps(trUserContext rUsrCntxt,
                                              const t_U32 cou32DevId,
                                              tvecMLApplist &vecApplist)
{
   SPI_INTENTIONALLY_UNUSED(rUsrCntxt);
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostNotiAllowedApps:DevID-0x%x,List Len-%d",
      cou32DevId,vecApplist.size()));

   for (t_U16 u16Index = 0; u16Index < vecApplist.size(); u16Index++)
   {
      ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Notification Apps supported by the Device-0x%x App-0x%x",
         cou32DevId, vecApplist[u16Index]));
   }//for(t_U16 u16Index=0;u16Index<vecApplist.size();u16Index++)

   //Update the NotificationSupport status of apps in stored AppList
   if (NULL != spoMLAppFilter)
   {
      spoMLAppFilter->bUpdateAppNotiSupport(cou32DevId, vecApplist);

      //There is a change in the application list.
      //Re-subscribe for notification supported apps of selected device.
      if (cou32DevId == m_u32SelDevId)
      {
         vSubscribeNotiAllowedApps(m_u32SelDevId);
      }//if (cou32DevId == m_u32SelDevId)
   }//if (NULL != spoMLAppFilter)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPostSetNotiAllowedAppsResult()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostSetNotiAllowedAppsResult(trUserContext rUsrCntxt,
                                                       const t_U32 cou32DevId, 
                                                       t_Bool bSetNotiAppRes)
{

   /*lint -esym(40,fpvEnableNotiResult)fpvEnableNotiResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostSetNotiAllowedAppsResult: DevID-0x%x, Result-%d ",
      cou32DevId, ETG_ENUM(BOOL, bSetNotiAppRes)));
   //Send the response to HMI
   if (NULL != m_rAppMngrCallbacks.fpvEnableNotiResult)
   {
      tenErrorCode enErrorCode = (bSetNotiAppRes) ? e8NO_ERRORS : e8UNKNOWN_ERROR;
      (m_rAppMngrCallbacks.fpvEnableNotiResult)(cou32DevId, enErrorCode, rUsrCntxt);
   }//if (NULL != m_rAppMngrCallbacks.fpvEnableNotiResult)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPostInvokeNotiActionResult()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostInvokeNotiActionResult(trUserContext rUsrCntxt,
                                                     const t_U32 cou32DevId,
                                                     t_Bool bNotiActionRes)
{

	/*lint -esym(40,fpvInvokeNotiActionResult)fpvInvokeNotiActionResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostInvokeNotiActionResult: DevID-0x%x, Result-%d ",
      cou32DevId, ETG_ENUM(BOOL, bNotiActionRes)));

   //Send the response to HMI
   if (NULL != m_rAppMngrCallbacks.fpvInvokeNotiActionResult)
   {
      tenErrorCode enErrorCode = (bNotiActionRes)? e8NO_ERRORS : e8UNKNOWN_ERROR;
      (m_rAppMngrCallbacks.fpvInvokeNotiActionResult)(cou32DevId, enErrorCode, rUsrCntxt);
   }//if (NULL != m_rAppMngrCallbacks.fpvInvokeNotiActionResult)

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPostNotiEvent()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostNotiEvent(const t_U32 cou32DevId,
                                        const trNotiData &rfrNotiDetails)
{

	/*lint -esym(40,fpvNotifyNoticationInfo)fpvNotifyNoticationInfo Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostNotiEvent:DevID-0x%x",cou32DevId));

   //Post Notification data to HMI
   if (NULL != m_rAppMngrCallbacks.fpvNotifyNoticationInfo)
   {
      (m_rAppMngrCallbacks.fpvNotifyNoticationInfo)(cou32DevId,rfrNotiDetails);
   }//if (NULL != m_rAppMngrCallbacks.fpvNotifyNoticationInfo)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPostAppName()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostAppName(t_U32 u32DeviceHandle, 
                                      t_U32 u32AppHandle,
                                      t_String szAppName,
                                      const trUserContext& corfrUsrCntxt)
{

	/*lint -esym(40,fpvNotifyAppListChangeCb)fpvNotifyAppListChangeCb Undeclared identifier */
	SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostAppName:DevID-0x%x",u32DeviceHandle));
   t_Bool bNotifyApplistChange=false;
   if (NULL != spoMLAppFilter)
   {
      //Check whether the application is there in the stored application list
      //If the app is not stored( not certified or doesn't satisify filtering criteria,
      //dont update HMI about the change in App list
	  szAppName = szConvertUTF8ToAscii(szAppName.c_str()).c_str();
      bNotifyApplistChange=spoMLAppFilter->bUpdateAppName(u32DeviceHandle,u32AppHandle,szAppName.c_str());
   }//if (NULL != spoMLAppFilter)

   //Post the change in App List update to HMI, If this application is
   //already inserted in the applications info storage.
   if ((m_u32SelDevId == u32DeviceHandle) &&  (true == bNotifyApplistChange) &&
      (NULL != m_rAppMngrCallbacks.fpvNotifyAppListChangeCb))
   {
      (m_rAppMngrCallbacks.fpvNotifyAppListChangeCb)(u32DeviceHandle,e8APP_LIST_CHANGED);
   }//if ((true == bUpdateAppListChange)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostViewerSessionProgress
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostViewerSessionProgress(tenSessionProgress enSessionProgress,
                                                    const t_U32 cou32DeviceHandle)
{
   //By default SDK enables park mode, during the initial handshake of the Mirror link session.
   //So if the current mode is drive mode,enable the Driver distraction avoidance
   //by sending a device status request to phone, immediately after the ML session is established
   if ((enSessionProgress == e16_SESSION_PROGRESS_SESSION_ESTABLISHED) &&
      (true == m_bDriveModeEnabled) && (NULL != spoCmdViewer))
   {
      ETG_TRACE_USR3_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Viewer Session established successfully"));
      //If the current mode is drive mode, send driver distraction avoidance request for
      //the viewer. 
      spoCmdViewer->vTriggerSetDriverDistractionAvoidance(m_bDriveModeEnabled,cou32DeviceHandle);
   }//if (enSessionProgress == tenSessionProgress)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vSubscribeNotiAllowedApps()
***************************************************************************/
t_Void spi_tclMLAppMngr::vSubscribeNotiAllowedApps(t_U32 u32DevId)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vSubscribeNotiAllowedApps: DevID-0x%x ", u32DevId));

   //Change subscription of notification apps if user's Notification setting is ON and
   //the Notification subscription is successful.

   //NotificationsOnOff variable is by default set to FALSE in Settings.
   //So for SUZUKI and PSA, bGetMLNotificationEnabledInfo() always returns FALSE.
   //HMI has to set the Noti supported application based on the application certification status.
   //It directly uses the interface vSetMLNotificationEnabledInfo() to set the Noti suppoprted apps
   spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
   if ((0 != u32DevId) && (true == m_bNotiSubscriptionActive)
       && (NULL != poAppSettings)&& (true == poAppSettings->bGetMLNotificationEnabledInfo()))
   {
      spi_tclMLAppDataIntf oDataIntf;
      std::vector<trAppDetails> vecrAppList;
      tenAppCertificationFilter enAppCertFilter = (m_bDriveModeEnabled)?
            (e8APP_CERT_FILTER_DRIVE_MODE) : (e8APP_CERT_FILTER_PARK_MODE);

      //Filter notification supported apps based on drive mode
      oDataIntf.vGetFilteredAppList(u32DevId, enAppCertFilter, true, vecrAppList);

      //Create Notification enabled apps list
      std::vector<trNotiEnable> vecrNotiEnableList;
      vPopulateNotiEnableList(vecrAppList, vecrNotiEnableList);

      //Subscribe for notifications of specific applications
      vSetMLNotificationEnabledInfo(u32DevId, vecrNotiEnableList.size(), vecrNotiEnableList, corEmptyUsrContext);

   }//if ((0 != u32DevId)&&(NULL != poAppSettings)&&...)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vPopulateNotiEnableList()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPopulateNotiEnableList(
      const std::vector<trAppDetails>& rfcovecrAppList,
      std::vector<trNotiEnable>& rfvecrNotiEnableList) const
{
   //Clear output list
   rfvecrNotiEnableList.clear();

   if (SPI_VECTOR_EMPTY(rfcovecrAppList))
   {
      typedef std::vector<trAppDetails>::const_iterator tItrAppDetailsList;
      for (tItrAppDetailsList itrAppList = rfcovecrAppList.begin();
           itrAppList != rfcovecrAppList.end();
           ++itrAppList)
      {
         trNotiEnable rNotiEnable;
         rNotiEnable.u32AppHandle = (*itrAppList).u32AppHandle;
         rNotiEnable.enEnabledInfo = e8USAGE_ENABLED;
         rfvecrNotiEnableList.push_back(rNotiEnable);
      }
   }//if (SPI_VECTOR_EMPTY(rfcovecrAppList))
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLAppMngr::bIsLaunchAppReq()
***************************************************************************/
t_Bool spi_tclMLAppMngr::bIsLaunchAppReq(t_U32 u32DevID,
                                         t_U32 u32AppID,
                                         t_U32 u32NotificationID,
                                         t_U32 u32NotificationActionID)
{

   t_Bool bIsLaunchAppRequired = false;

   if(NULL != spoCmdNoti )
   {
      bIsLaunchAppRequired = spoCmdNoti->bIsLaunchAppReq(u32DevID,u32AppID,
         u32NotificationID,u32NotificationActionID);
   }//if(NULL != spoCmdNoti )
   return bIsLaunchAppRequired;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppMngr::vPostAppContextInfo()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo)
{

	/*lint -esym(40,fpvNotifySessionStatus)fpvNotifySessionStatus Undeclared identifier */
	/*lint -esym(40,fpvNotifyActiveAppInfo)fpvNotifyActiveAppInfo Undeclared identifier */
   tenSessionStatus enSessionStatus = e8_SESSION_UNKNOWN;
   spi_tclMLAppDataIntf oMLDataIntf;

   if((NULL != spoCmdViewer)&&(NULL != spoCmdDisc))
   {
      //Whenever user goes to non Mirror link application or some invalid content is shown on the 
      //screen, ML Server may send the application context info with the AppID=0 & AppCat=0.

      //Currently blocking the application context info with the application category value zero.
      //Application ID criteria is not used, since in the CCC spec, it is mentioned that it may be zero.
      //so device can provide either zero or proper App ID, even though App ID is mandatory field in the 
      //App list details.Hence App ID is not used for content blocking.
      if(e32APPUNKNOWN == static_cast<tenAppCategory>(corfrAppCntxtInfo.u32AppCategory))
      {
         //Blocking Notification is sent to Phone in VNCViewer itself-SUZUKI-24554

         //Status update to HMI
         enSessionStatus = (false == m_bDriveModeEnabled ) ? e8_SESSION_SUSPENDED_NON_ML_APP: e8_SESSION_SUSPENDED_NON_DRIVE_APP;

      }//if(e32APPUNKNOWN == corfrAppCntxtInfo.u32AppCategory)
      else if(e32APP_SWITCHTO_CLIENT_NATIVE_UI == static_cast<tenAppCategory>(corfrAppCntxtInfo.u32AppCategory) )
      {
         //Device requested to switch to Native UI. Send session suspended update to HMI, 
         //so that it will move SPI screen to background and sends enable video blocking request to SPI
         enSessionStatus = e8_SESSION_SUSPENDED ;
      }//else if( e32APP_SWITCHTO_CLIENT_NATIVE_UI == corfrAppCntxtInfo.u32AppCategory )
      else
      { 

         //Usage of Car Mode bit to detect the Drive certified application usage is not always correct.
         //Whether the app is drive certified or not in the particular region can be checked based on the 
         //certification status stored . Application Manger sets the application certification
         //status based on the Region. and Phone may/may not set the car mode bit, if the app is certified in 
         //one particular region.

         //And in the park mode, we should be able to block the usage of Non certified applications,
         //if the app i snot listed in the HMi application list. so content blocking is
         //Applied based on the application ID and application certification status stored internally
         //and the Project specific configuration

         t_Bool bSendBlockingNotification = false;

         //Check the application certification status only for the device version 1.1 or more.
         //if the ML1.0 device is connected, In drive mode apply blocking for all applications
         trVersionInfo rVersionInfo;
         spoCmdDisc->vGetDeviceVersion(m_u32SelDevId,rVersionInfo);
         if( true == bIsML10Device(rVersionInfo.u32MajorVersion,rVersionInfo.u32MinorVersion) )
         {
            //ML1.0 device is connected and the vehicle is in drive mode. Send the blocking notification to Phone
            //and the Notification to HMI
            enSessionStatus = m_bDriveModeEnabled?e8_SESSION_SUSPENDED_NON_DRIVE_APP:e8_SESSION_UNKNOWN ;
            bSendBlockingNotification = m_bDriveModeEnabled;
         }//if( true == bIsML10Device(rVersionInfo.u32MajorVersion,rVersionInfo.u32MinorVersion) )
         else
         {
            //ML1.1 or more version device is connected.
            //Check whether  Non Certified apps are listed in the application list shared with HMI. 
            //If not block the usage of Mirror link aware applications in the park mode.

            //Block all the park certified and non certified applications in the drive mode

            t_Bool bIsAppAllowed = false;

            //Check whether the app is allowed to use in the Current Vehicle mode
            bIsAppAllowed = oMLDataIntf.bIsAppAllowedToUse(m_u32SelDevId,corfrAppCntxtInfo.u32AppUUID,
               m_bDriveModeEnabled);

            if(false == bIsAppAllowed)
            {
               //Current application which is there on FG, cannot be shown to User. Send blocking request to Device
               //and a notification to HMI with the proper status update
               enSessionStatus = m_bDriveModeEnabled?e8_SESSION_SUSPENDED_NON_DRIVE_APP:
                  e8_SESSION_SUSPENDED_NON_ML_APP;
            bSendBlockingNotification = true;
            }//if(false == bIsAppAllowed)
         }//else


         if(true == bSendBlockingNotification)
         {
            //Blocking Notification to Phone
            spoCmdViewer->bTriggerFramebufferBlockingNotification(corfrAppCntxtInfo.u32AppUUID,
               corfrAppCntxtInfo.rMLRect,e16MLAPPLICATION_UNIQUEID_NOTALLOWED,m_u32SelDevId);
         }//if(true == bSendBlockingNotification)
      }//else if(NULL != spoCmdDisc)

   }//if(NULL != spoCmdViewer)

   m_oContextInfoLock.s16Lock();
   m_rAppCntxtInfo = corfrAppCntxtInfo;
   m_oContextInfoLock.vUnlock();

   if((e8_SESSION_UNKNOWN != enSessionStatus)&&
      (NULL != m_rAppMngrCallbacks.fpvNotifySessionStatus ))
   {
      (m_rAppMngrCallbacks.fpvNotifySessionStatus)(m_u32SelDevId,
         e8DEV_TYPE_MIRRORLINK,enSessionStatus);
   }//if(NULL != m_rVideoCallbacks.fpvNotifySessionStatus )

   t_U32 u32AppId=0;
   tenAppCertificationInfo enAppCertInfo = e8NOT_CERTIFIED;
   oMLDataIntf.vGetAppCertInfo(m_u32SelDevId,corfrAppCntxtInfo.u32AppUUID,u32AppId,enAppCertInfo);

   if(NULL != m_rAppMngrCallbacks.fpvNotifyActiveAppInfo)
   {
      (m_rAppMngrCallbacks.fpvNotifyActiveAppInfo)(m_u32SelDevId,
         e8DEV_TYPE_MIRRORLINK,u32AppId,enAppCertInfo);
   }//if(NULL != m_rAppMngrCall

}


/***************************************************************************
** FUNCTION:t_Void spi_tclMLAppMngr::vPostDAPAttestResp()
***************************************************************************/
t_Void spi_tclMLAppMngr::vPostDAPAttestResp(trUserContext rUsrCntext,
                                              tenMLAttestationResult enAttestationResult,
                                              t_String szVNCAppPublickey,
                                              t_String szUPnPAppPublickey,
                                              t_String szVNCUrl,
                                              const t_U32 cou32DeviceHandle,
                                              t_Bool bVNCAvailInDAPAttestResp)
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vPostDAPAttestResp: DevID-0x%x VNC-Server listed in Attest Resp-%d  VNC URL-%s", 
      cou32DeviceHandle,ETG_ENUM(BOOL,bVNCAvailInDAPAttestResp),szVNCUrl.c_str()));

   SPI_INTENTIONALLY_UNUSED(rUsrCntext);
   SPI_INTENTIONALLY_UNUSED(szUPnPAppPublickey);
   SPI_INTENTIONALLY_UNUSED(szVNCAppPublickey);

   //Do not insert the device id in the map, if the VNC Component is not listed in the DAP attested response
   //of the Phone. In this case, we don't need to perform validation of VNC URL's. It is an optional (SHOULD) feature 
   //for MLS
   if((enAttestationResult == e8MLATTESTATION_SUCCESSFUL) && (true == bVNCAvailInDAPAttestResp))
   {
      m_oDevVNCURLsLock.s16Lock();
      m_mapDevVNCURLs.insert(std::pair<t_U32,t_String>(cou32DeviceHandle,szVNCUrl.c_str()));
      m_oDevVNCURLsLock.vUnlock();
   }//if((coszEmptyStr != szVNCUrl) && (enAttestationRe

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAppMngr::vMapUTF8StrsToAscii()
***************************************************************************/
t_Void spi_tclMLAppMngr::vMapUTF8StrsToAscii() const
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::vMapUTF8StrsToAscii"));
   //Currently we are converting &amp;,&apos;,&quot;,&gt; and &lt; strings only.
   m_mapUTF8ToAscii["&amp;"]  = "&";
   m_mapUTF8ToAscii["&apos;"]  = "'";
   m_mapUTF8ToAscii["&quot;"] = "\"";
   m_mapUTF8ToAscii["&gt;"]  = ">";
   m_mapUTF8ToAscii["&lt;"]  = "<";
}

/***************************************************************************
** FUNCTION: t_String spi_tclMLAppMngr::szConvertUTF8ToAscii()
***************************************************************************/
t_String spi_tclMLAppMngr::szConvertUTF8ToAscii(t_String szAppName) const
{
   ETG_TRACE_USR1(("spi_tclMLAppMngr::szConvertUTF8ToAscii-%s",szAppName.c_str()));
   std::map<t_String,t_String>::const_iterator cItr;
   //search whether any of the stored strings are there in the input string
   for ( cItr = m_mapUTF8ToAscii.begin(); cItr != m_mapUTF8ToAscii.end(); cItr++)
   {
      while(t_String::npos != szAppName.find(cItr->first))
      {
         szAppName.replace(szAppName.find(cItr->first),(cItr->first).length(),cItr->second);
      }//if(t_String::npos != rfrStr.find(cItr->first))

   }//for (auto cItr = m_mapUTF8ToAscii.begin(); it

   ETG_TRACE_USR4(("spi_tclMLAppMngr::szConvertUTF8ToAscii-%s",szAppName.c_str()));

   return szAppName.c_str();
}
//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>

