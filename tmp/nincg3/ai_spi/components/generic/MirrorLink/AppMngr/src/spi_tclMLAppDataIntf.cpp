/***********************************************************************/
/*!
* \file  spi_tclMLAppDataIntf.h
* \brief To Get the Appication data from the container
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.02.2014  | Shiva Kumar Gurija    | Initial Version
23.04.2014  | Shiva Kumar Gurija    | Changes in AppList Fetching for CTS Test
11.08.2014  | Ramya Murthy          | Added vGetFilteredAppList() to filter apps
                                      based on certification and notification support.

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclMLApplicationsInfo.h"
#include "spi_tclAppSettings.h"
#include "spi_tclMLAppDataIntf.h"


#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclMLAppDataIntf.cpp.trc.h"
#endif
#endif

/***************************************************************************
** FUNCTION:  spi_tclMLAppDataIntf::spi_tclMLAppDataIntf()
***************************************************************************/
spi_tclMLAppDataIntf::spi_tclMLAppDataIntf():m_poApplicationsInfo(NULL)
{
   //Constructor
   m_poApplicationsInfo = spi_tclMLApplicationsInfo::getInstance();
}

/***************************************************************************
** FUNCTION:  spi_tclMLAppDataIntf::~spi_tclMLAppDataIntf()
***************************************************************************/
spi_tclMLAppDataIntf::~spi_tclMLAppDataIntf()
{
   //Destrucor
   m_poApplicationsInfo = NULL;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetAppList()
***************************************************************************/
t_Void spi_tclMLAppDataIntf::vGetAppList(const t_U32 cou32DevHandle,
                                         std::vector<trAppDetails>& rfvecAppList)
{
   ETG_TRACE_USR1(("spi_tclMLAppDataIntf::vGetAppList:Dev-0x%x",cou32DevHandle));
   if(NULL != m_poApplicationsInfo)
   {
      m_poApplicationsInfo->vGetAppList(cou32DevHandle,rfvecAppList);
   }//if(NULL != m_poApplicationsInfo)
}
/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetUIAppList()
***************************************************************************/
t_Void spi_tclMLAppDataIntf::vGetUIAppList(const t_U32 cou32DevHandle,
                                           std::vector<trAppDetails>& rfvecAppList)
{
   ETG_TRACE_USR1(("spi_tclMLAppDataIntf::vGetUIAppList:Dev-0x%x",cou32DevHandle));
   spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
   if((NULL != m_poApplicationsInfo)&&(NULL != poAppSettings))
   {
      t_Bool bNonCertApps = true;
      trVersionInfo rVersionInfo;
      m_poApplicationsInfo->vGetDeviceVersionInfo(cou32DevHandle,rVersionInfo);
      //If the application certification required for this particular device ML version,
      //then only check whether the non certified applications are required for the specific 
      //project or not.
      //For Ex: For ML1.0 devices, Application certification check is not required
      //in such cases, we should give the non certified applications to HMI.
      //so incase of ML1.0 devices get Non certified applications also, though policy says that
      //only certified apps are required.
      if(true == poAppSettings->bEnableAppCertification(rVersionInfo))
      {
         bNonCertApps = poAppSettings->bNonCertAppsReq();
      }//if(true == bAppCertEnabled)
      m_poApplicationsInfo->vGetUIAppList(cou32DevHandle,rfvecAppList,bNonCertApps);
   }//if(NULL != m_poApplicationsInfo)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetFilteredAppList()
***************************************************************************/
t_Void spi_tclMLAppDataIntf::vGetFilteredAppList(const t_U32 cou32DevHandle,
                                                 tenAppCertificationFilter enAppCertFilter,
                                                 t_Bool bNotifSupportedApps,
                                                 std::vector<trAppDetails>& rfvecAppList)
{
   ETG_TRACE_USR1(("spi_tclMLAppDataIntf::vGetFilteredAppList:Dev-0x%x",cou32DevHandle));

   if (NULL != m_poApplicationsInfo)
   {
      m_poApplicationsInfo->vGetFilteredAppList(cou32DevHandle,
            enAppCertFilter, bNotifSupportedApps, rfvecAppList);
   }//if (NULL != m_poApplicationsInfo)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetAppInfo()
***************************************************************************/
t_Void spi_tclMLAppDataIntf::vGetAppInfo(const t_U32 cou32DevHandle, 
                                         const t_U32 cou32AppHandle,
                                         trAppDetails& rfrAppDetails)
{
   ETG_TRACE_USR1(("spi_tclMLAppDataIntf::vGetAppInfo:Dev-0x%x App-0x%x",
      cou32DevHandle,cou32AppHandle));

   if(NULL != m_poApplicationsInfo)
   {
      m_poApplicationsInfo->vGetAppInfo(cou32DevHandle,cou32AppHandle,rfrAppDetails);
   }//if(NULL != m_poApplicationsInfo)
}

/***************************************************************************
** FUNCTION:  tenIconMimeType spi_tclMLAppDataIntf::enGetAppIconMIMEType()
***************************************************************************/
tenIconMimeType spi_tclMLAppDataIntf::enGetAppIconMIMEType(const t_U32 cou32DevHandle,
                                                           t_String szAppIconUrl)
{   
   ETG_TRACE_USR1(("spi_tclMLAppDataIntf::enGetAppIconMIMEType:Dev-0x%x szAppIconUrl-%s",
      cou32DevHandle,szAppIconUrl.c_str()));

   tenIconMimeType enIconMIMEType = e8ICON_INVALID;

   if(NULL != m_poApplicationsInfo)
   {
      enIconMIMEType = m_poApplicationsInfo->enGetAppIconMIMEType(cou32DevHandle,szAppIconUrl);
   }//if(NULL != m_poApplicationsInfo)

   return enIconMIMEType;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppDataIntf::vDisplayAllAppsInfo()
***************************************************************************/
t_Void spi_tclMLAppDataIntf::vDisplayAllAppsInfo(const t_U32 cou32DevHandle)
{
   std::vector<trAppDetails> vecrAppDetailsList;
   if(NULL != m_poApplicationsInfo)
   {
      //@Logiscope comliance - get the app list supported by the Phone
      m_poApplicationsInfo->vGetAppList(cou32DevHandle,vecrAppDetailsList);
   }//if(NULL != m_poApplicationsInfo)

   t_U32 u32AppListLen = vecrAppDetailsList.size();
   ETG_TRACE_USR2(("vDisplayAllAppsInfo:Dev-0x%x  Num Apps %u ",cou32DevHandle,u32AppListLen));

   for( t_U16 u16Index = 0 ; u16Index < u32AppListLen ; u16Index++ )
   {
      //@Logiscope comliance - Print app handle
      ETG_TRACE_USR4(("u32AppHandle - 0x%x  ",
         vecrAppDetailsList[u16Index].u32AppHandle));
	  //@Logiscope comliance - Print app name
      ETG_TRACE_USR4(("szAppName - %s",
         vecrAppDetailsList[u16Index].szAppName.c_str()));
      //@Logiscope comliance - Print app status
      ETG_TRACE_USR4(("enAppStatus - %d",
         ETG_ENUM(APP_STATUS,vecrAppDetailsList[u16Index].enAppStatus)));
      //@Logiscope comliance - Print app variant
      ETG_TRACE_USR4(("szAppVariant - %s",
         vecrAppDetailsList[u16Index].szAppVariant.c_str()));
      //@Logiscope comliance - Print app provider name
      ETG_TRACE_USR4(("szAppProviderName - %s",
         vecrAppDetailsList[u16Index].szAppProviderName.c_str()));
      //@Logiscope comliance - Print app URL
      ETG_TRACE_USR4(("szAppProviderURL - %s",
         vecrAppDetailsList[u16Index].szAppProviderURL.c_str()));
      ETG_TRACE_USR4(("szAppDescription - %s",
         vecrAppDetailsList[u16Index].szAppDescription.c_str()));
      //@Logiscope comliance - Print app Certfiifcates URL
      ETG_TRACE_USR4(("szAppCertificateUrl - %s",
         vecrAppDetailsList[u16Index].szAppCertificateURL.c_str()));
      //@Logiscope comliance - Print app trust level
      ETG_TRACE_USR4(("enAppCategory - 0x%x \n enTrustLevel - %d",
         vecrAppDetailsList[u16Index].enAppCategory,
         ETG_ENUM(APP_TRUST_LEVEL,vecrAppDetailsList[u16Index].enTrustLevel)));
      //@Logiscope comliance - Print app allowed profiles
      ETG_TRACE_USR4(("AppAllowedProfiles - %d",
         vecrAppDetailsList[u16Index].AppAllowedProfiles.size()));

      //@Logiscope comliance - Print app display info
      ETG_TRACE_USR4(("Display:enAppDisplayCategory - %d",ETG_ENUM(APP_DISPLAY_CAT,
         vecrAppDetailsList[u16Index].rAppDisplayInfo.enAppDisplayCategory)));
      ETG_TRACE_USR4(("Display:enTrustLevel - %d",ETG_ENUM(APP_TRUST_LEVEL,
         vecrAppDetailsList[u16Index].rAppDisplayInfo.enTrustLevel)));
      ETG_TRACE_USR4(("Display:u32AppDisplayRules - 0x%x",
         vecrAppDetailsList[u16Index].rAppDisplayInfo.u32AppDisplayRules));
      ETG_TRACE_USR4(("Display:szAppDisplayOrientation - %s",
         vecrAppDetailsList[u16Index].rAppDisplayInfo.szAppDisplayOrientation.c_str()));

      //@Logiscope comliance - Print app audio info
      ETG_TRACE_USR4(("Audio: szAppAudioType- %s",
         vecrAppDetailsList[u16Index].rAppAudioInfo.szAppAudioType.c_str()));
      ETG_TRACE_USR4(("Audio: enAppAudioCategory - %d",ETG_ENUM(APP_AUDIO_CAT,
         vecrAppDetailsList[u16Index].rAppAudioInfo.enAppAudioCategory)));
      ETG_TRACE_USR4(("Audio: enTrustLevel - %d",ETG_ENUM(APP_TRUST_LEVEL,
         vecrAppDetailsList[u16Index].rAppAudioInfo.enTrustLevel)));
      ETG_TRACE_USR4(("Audio: u32AppAudioRules - 0x%x",
         vecrAppDetailsList[u16Index].rAppAudioInfo.u32AppAudioRules));

      //@Logiscope comliance - Print app remoting info
      ETG_TRACE_USR4(("RemotingInfo:szRemotingProtocolID - %s",
         vecrAppDetailsList[u16Index].rAppRemotingInfo.szRemotingProtocolID.c_str()));
      ETG_TRACE_USR4(("RemotingInfo:szRemotingFormat - %s",
         vecrAppDetailsList[u16Index].rAppRemotingInfo.szRemotingFormat.c_str()));
      ETG_TRACE_USR4(("RemotingInfo:szRemotingDirection - %s",
         vecrAppDetailsList[u16Index].rAppRemotingInfo.szRemotingDirection.c_str()));
      ETG_TRACE_USR4(("RemotingInfo:u32RemotingAudioIPL - 0x%x",
         vecrAppDetailsList[u16Index].rAppRemotingInfo.u32RemotingAudioIPL));
      ETG_TRACE_USR4(("RemotingInfo:u32RemotingAudioMPL - 0x%x",
         vecrAppDetailsList[u16Index].rAppRemotingInfo.u32RemotingAudioMPL));

      //@Logiscope comliance - Print app certification details
      ETG_TRACE_USR4(("Certification Status - %d",
         ETG_ENUM(APP_CERTIFICATION_STATUS,vecrAppDetailsList[u16Index].enAppCertificationInfo)));

      //@Logiscope comliance - Print app icon details
      ETG_TRACE_USR4(("u32NumAppIcons : - %d",
         vecrAppDetailsList[u16Index].u32NumAppIcons));
      for(t_U32 u32Index=0; u32Index < vecrAppDetailsList[u16Index].u32NumAppIcons;u32Index++)
      {
         ETG_TRACE_USR4(("Icon:%d -> u32IconWidth - 0x%x",u32Index,
            vecrAppDetailsList[u16Index].tvecAppIconList[u32Index].u32IconWidth));
         ETG_TRACE_USR4(("Icon:%d -> u32IconHeight - 0x%x",u32Index,
            vecrAppDetailsList[u16Index].tvecAppIconList[u32Index].u32IconHeight));
         ETG_TRACE_USR4(("Icon:%d -> u32IconDepth - 0x%x",u32Index,
            vecrAppDetailsList[u16Index].tvecAppIconList[u32Index].u32IconDepth));
         ETG_TRACE_USR4(("Icon:%d -> enIconMimeType - %d",u32Index,
            ETG_ENUM(ICON_MIME_TYPE,vecrAppDetailsList[u16Index].tvecAppIconList[u32Index].enIconMimeType)));
         ETG_TRACE_USR4(("Icon:%d -> szIconURL - %s",u32Index,
            vecrAppDetailsList[u16Index].tvecAppIconList[u32Index].szIconURL.c_str()));
      }//for(int u8Index=0; u8Index < vecrAppDetailsList[u16Index].u32NumAppIcons;u8Index++)

   }//for(tU16 u16Index;u16Index<u16ListSize;u16Index++)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLAppDataIntf::bCheckAppValidity()
***************************************************************************/
t_Bool spi_tclMLAppDataIntf::bCheckAppValidity(const t_U32 cou32DevId, 
                                               const t_U32 cou32AppId)
{
   ETG_TRACE_USR1(("spi_tclMLAppDataIntf::bCheckAppValidity:Dev-0x%x  App-0x%x ",
      cou32DevId,cou32AppId));

   t_Bool bRet=false;
   if(NULL != m_poApplicationsInfo)
   {
      bRet = m_poApplicationsInfo->bCheckAppValidity(cou32DevId,cou32AppId);
   }//if(NULL != m_poApplicationsInfo)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetLaunchedAppIds()
***************************************************************************/
t_Void spi_tclMLAppDataIntf::vGetLaunchedAppIds(const t_U32 cou32DevId,
                                                std::vector<t_U32>& rfvecLaunchedAppIds)
{
   ETG_TRACE_USR1(("spi_tclMLAppDataIntf::vGetLaunchedAppIds:Dev-0x%x ",
      cou32DevId));
   if(NULL != m_poApplicationsInfo)
   {
      m_poApplicationsInfo->vGetLaunchedAppIds(cou32DevId,rfvecLaunchedAppIds);
   }//if(NULL != m_poApplicationsInfo)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppDataIntf::bIsAppAllowedToUse()
***************************************************************************/
t_Bool spi_tclMLAppDataIntf::bIsAppAllowedToUse(t_U32 u32DevID, 
                                                t_U32 u32ConvAppID, 
                                                t_Bool bDriveModeEnabled)
{
   ETG_TRACE_USR1(("spi_tclMLAppDataIntf::bIsAppAllowedToUse:Dev-0x%x App-0x%x \
       bDriveModeEnabled-%d", u32DevID,u32ConvAppID,ETG_ENUM(BOOL,bDriveModeEnabled)));

   t_Bool bRet=false;
   if(NULL != m_poApplicationsInfo)
   {
      //Check whether the Project allows Non certified apps (Mirror link Aware) applications of ML1.1 device,
      //based on that set the flag in Park mode.
      //In case of GM, blocking should be applied, when the non certified application is launched.
      t_Bool bNonCertAppsAllowed = false;

      //In drive mode no need to check whether the project wants non certified apps or not.
      //Only allow drive certified apps for the driver safety.
      if(false == bDriveModeEnabled)
      {
         spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
         if(NULL != poAppSettings)
         {
            bNonCertAppsAllowed = poAppSettings->bNonCertAppsReq();
         }//if(NULL != poAppSettings)
      }//if(false == bDriveModeEnabled)

      bRet = m_poApplicationsInfo->bIsAppAllowedToUse(u32DevID,
         u32ConvAppID,
         bDriveModeEnabled,
         bNonCertAppsAllowed);

   }//if(NULL != m_poApplicationsInfo)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetAppCertInfo()
***************************************************************************/
t_Void spi_tclMLAppDataIntf::vGetAppCertInfo(t_U32 u32DevID, 
                                             t_U32 u32AppUUID,
                                             t_U32& rfu32AppID,
                                             tenAppCertificationInfo& rfenAppCertInfo )
{

   if(NULL != m_poApplicationsInfo)
   {
      m_poApplicationsInfo->vGetAppCertInfo(u32DevID,
         u32AppUUID,
         rfu32AppID,
         rfenAppCertInfo);
   }//if(NULL != m_poApplicationsInfo)
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>