/***********************************************************************/
/*!
* \file  spi_tclMLAppFilter.h
* \brief To Filter the Mirror Link Applications and store in the storage class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.02.2014  | Shiva Kumar Gurija    | Initial Version
23.04.2014  | Shiva Kumar Gurija    | Changes in AppList Filtering for CTS Test
29.04.2014  | Shiva Kumar Gurija    | Updated with the Changes in App certification status
11.08.2014  | Ramya Murthy          | Added bUpdateAppNotiSupport() to modify notification 
                                      support status of apps
\endverbatim
*************************************************************************/

#ifndef _SPI_TCLMLAPPFILTER_H
#define _SPI_TCLMLAPPFILTER_H

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclMLAppFilter
* \brief To Filter the Mirror Link Applications and store in the storage class
****************************************************************************/
class spi_tclMLAppFilter
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLAppFilter::spi_tclMLAppFilter()
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLAppFilter()
   * \brief   Parameterized Constructor
   * \sa      ~spi_tclAppMngr()
   **************************************************************************/
   spi_tclMLAppFilter();

   /***************************************************************************
   ** FUNCTION:  spi_tclMLAppFilter::~spi_tclMLAppFilter()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclMLAppFilter()
   * \brief   Destructor
   * \sa      spi_tclMLAppFilter()
   **************************************************************************/
   ~spi_tclMLAppFilter();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppFilter::vFilterApplications()
   ***************************************************************************/
   /*!
   * \fn      t_Void vFilterApplications(const t_U32 cou32DeviceHandle,
   *            const trVersionInfo& rVersionInfo,
   *            std::vector<trApplicationInfo> vecAppInfo)
   * \brief   To Filter the non certified aplications based on the version
   *          of the device & store the application data in the storgae class
   * \pram    cou32DeviceHandle  : [IN] Uniquely identifies the target Device.
   * \pram    rVersionInfo : [IN] Device Version Info
   * \pram    vecAppInfo   : [IN] List of Application Info's
   * \retval  t_Void
   **************************************************************************/
   t_Void vFilterApplications(const t_U32 cou32DeviceHandle,
      const trVersionInfo& rVersionInfo,
      std::vector<trApplicationInfo> vecAppInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppFilter::vClearAppInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vClearAppInfo(const t_U32 cou32DeviceHandle)
   * \brief   To Remove the app info stored
   * \pram    cou32DeviceHandle  : [IN] Uniquely identifies the target Device.
   * \retval  t_Void
   **************************************************************************/
   t_Void vClearAppInfo(const t_U32 cou32DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLAppFilter::bUpdateAppStatus()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bUpdateAppStatus(const t_U32 cou32DeviceHandle,t_U32 cou32AppHandle,
   *             tenAppStatus enAppStatus) const
   * \brief   To update the status os any application which is stored in the storage.
   *            and returns whether the updation is success or not
   * \pram    cou32DeviceHandle  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppHandle     : [IN] Application Id
   * \param   enAppStatus        : [IN] Application status
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bUpdateAppStatus(const t_U32 cou32DeviceHandle,
      t_U32 cou32AppHandle,
      tenAppStatus enAppStatus) const;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLAppFilter::bUpdateAppName()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bUpdateAppName(const t_U32 cou32DevHandle,const t_U32 cou32AppHandle,
   *             t_String szAppName)
   * \brief   To update the app name of any application which is stored in the storage.
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppHandle  : [IN] Application Id
   * \param   szAppName       : [IN] Application Name
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bUpdateAppName(const t_U32 cou32DeviceHandle,const t_U32 cou32AppHandle,
      t_String szAppName) const;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLAppFilter::bUpdateAppNotiSupport()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bUpdateAppName(const t_U32 cou32DevHandle,
   *             const tvecMLApplist& rfcovecNotiSuppApplist)
   * \brief   To update the NotificationSupport flag of applications which is stored in the storage.
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   rfcovecNotiSuppApplist : [IN] List of notification supported apps
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bUpdateAppNotiSupport(const t_U32 cou32DeviceHandle,
         const tvecMLApplist& rfcovecNotiSuppApplist) const;

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/
private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLAppFilter& spi_tclMLAppFilter::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLAppFilter& operator= (const spi_tclMLAppFilter &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclMLAppFilter& operator= (const spi_tclMLAppFilter &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLAppFilter::spi_tclMLAppFilter(const spi_tclAppMngr..
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLAppFilter(const spi_tclMLAppFilter &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclMLAppFilter(const spi_tclMLAppFilter &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  tenAppCertificationInfo spi_tclMLAppFilter::enGetAppCertStatus()
   ***************************************************************************/
   /*!
   * \fn      tenAppCertificationInfo  enGetAppCertStatus(
   *           std::vector<trAppCertInfo> vecAppCertInfo,
   *           tenAppCertificationEntity& rfenAppCertEntity)
   * \brief   To Get the App certification status
   * \pram    vecAppCertInfo   : [IN] Application certification info
   * \param   rfenAppCertEntity: [IN] Appl Certifying entity (CCC/CCC-Member)
   * \retval  tenAppCertificationInfo
   **************************************************************************/
   tenAppCertificationInfo enGetAppCertStatus(std::vector<trAppCertInfo> vecAppCertInfo,
      tenAppCertificationEntity& rfenAppCertEntity);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppFilter::vStoreAppInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vStoreAppInfo(const t_U32 cou32DeviceHandle,
   *            const trVersionInfo& rVersionInfo,
   *            std::vector<trAppDetails> vecAppDetailsList)
   * \brief   To store the application data in the storgae class
   * \pram    cou32DeviceHandle   : [IN] Uniquely identifies the target Device.
   * \pram    rVersionInfo        : [IN] Device Version Info
   * \pram    vecAppDetailsList   : [IN] List of Application Info's
   * \retval  t_Void
   **************************************************************************/
   t_Void vStoreAppInfo(const t_U32 cou32DeviceHandle,
      const trVersionInfo& corfrVersionInfo,
      std::vector<trAppDetails> vecAppDetailsList);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLAppFilter::bCheckAppCertification()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bCheckAppCertification(t_String szCertEntity,
   *            std::vector<t_String>& rfvecLocaleList,
   *            t_String szCurrentLocale)
   * \brief   To Check whether the app is certified 
   * \pram    szCertEntity   : [IN] CCC Certfying Entity
   * \pram    rfvecLocaleList: [IN] Locale's List
   * \pram    szCurrentLocale: [IN] Locale - Which regions guidelines should be followed
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bCheckAppCertification(t_String szCertEntity,
      std::vector<t_String>& rfvecLocaleList,
      t_String szCurrentLocale);

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};//spi_tclMLAppFilter

#endif //_SPI_TCLMLAPPFILTER_H