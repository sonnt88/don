/***********************************************************************/
/*!
* \file  spi_tclMLApplicationsInfo.h
* \brief To Store the Applications inforamtion and provide interfaces to read the data
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.02.2014  | Shiva Kumar Gurija    | Initial Version
23.04.2014  | Shiva Kumar Gurija    | Changes in AppList Handling for CTS Test
11.08.2014  | Ramya Murthy          | Added functions to read/set notification 
                                      support status of apps

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLMLAPPLICATIONSINFO_H_
#define _SPI_TCLMLAPPLICATIONSINFO_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "GenericSingleton.h"
#include "Lock.h"


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclMLApplicationsInfo
* \brief To Store the Applications inforamtion and provide interfaces 
*        to read the data
****************************************************************************/
class spi_tclMLApplicationsInfo:public GenericSingleton<spi_tclMLApplicationsInfo>
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLApplicationsInfo::~spi_tclMLApplicationsInfo()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLApplicationsInfo()
   * \brief   Destructor
   * \sa      spi_tclMLApplicationsInfo()
   **************************************************************************/
   ~spi_tclMLApplicationsInfo();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vInsertAppInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vInsertAppInfo(const t_U32 cou32DevHandle,
   *           const trMLDeviceAppInfo& rfrAppInfo))
   * \brief   To Insert the app details 
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \pram    rfrAppInfo  : [IN] Device applications info
   * \retval  t_Void
   **************************************************************************/
   t_Void vInsertAppInfo(const t_U32 cou32DevHandle,const trMLDeviceAppInfo& rfrAppInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vClearAppsInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vClearAppsInfo(const t_U32 cou32DevHandle)
   * \brief   To delete the application details of specific device
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \retval  t_Void
   **************************************************************************/
   t_Void vClearAppsInfo(const t_U32 cou32DevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetAppList()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetAppList(const t_U32 cou32DevHandle,
   *           ,std::vector<trAppDetails>& rfvecAppList)
   * \brief   To Get the All applications details
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \pram    rfvecAppList  : [IN] List of Vector Applications details
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppList(const t_U32 cou32DevHandle,
      std::vector<trAppDetails>& rfvecAppList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetUIAppList()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetUIAppList(const t_U32 cou32DevHandle,
   *            std::vector<trAppDetails>& rfvecAppList,
   *            t_Bool bNonCertApps)
   * \brief   To Get the UIapplications details
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \pram    rfvecAppList  : [IN] List of Vector Applications details
   * \param   bNonCertApps  : [IN] True - provide Non certified applications also
   *                               False - Provide Only Certified applications
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetUIAppList(const t_U32 cou32DevHandle,
      std::vector<trAppDetails>& rfvecAppList,
      t_Bool bNonCertApps);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetFilteredAppList()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetFilteredAppList(const t_U32 cou32DevHandle,
   *              tenAppCertificationFilter enAppCertFilter, t_Bool bNotifSupportedApps,
   *              std::vector<trAppDetails>& rfvecAppList)
   * \brief   To Get the UIapplications details
   * \pram    cou32DevHandle      : [IN] Uniquely identifies the target Device.
   * \pram    enAppCertFilter     : [IN] Type of certified apps to be filtered
   * \pram    bNotifSupportedApps : [IN] Indicates whether only notification
   *              supported should be filtered.
   *              If true - Only Notification supported apps will be provided
   *              If false - Notification supported & not-supported apps will be provided.
   * \pram    rfvecAppList  : [OUT] List of filtered Applications details
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetFilteredAppList(const t_U32 cou32DevHandle,
         tenAppCertificationFilter enAppCertFilter,
         t_Bool bNotifSupportedApps,
         std::vector<trAppDetails>& rfvecAppList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetAppInfo()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetAppInfo(const t_U32 cou32DevHandle, 
   *         const t_U32 cou32AppHandle,
   *         trAppDetails& rfrAppDetails)
   * \brief  To Get an applications info
   * \param    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param    cou32AppHandle  : [IN] Application Id
   * \param    rfrAppDetails : [OUT] Application Info
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppInfo(const t_U32 cou32DevHandle, 
      const t_U32 cou32AppHandle,
      trAppDetails& rfrAppDetails);

   /***************************************************************************
   ** FUNCTION:  tenIconMimeType spi_tclMLApplicationsInfo::enGetAppIconMIMEType()
   ***************************************************************************/
   /*!
   * \fn      tenIconMimeType enGetAppIconMIMEType(const t_U32 cou32DevHandle, 
   *             t_String szAppIconUrl)
   * \brief   To Get an applications info
   * \param   cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   szAppIconUrl  : [IN] String Url
   * \retval  tenIconMimeType
   **************************************************************************/
   tenIconMimeType enGetAppIconMIMEType(const t_U32 cou32DevHandle,t_String szAppIconUrl);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLApplicationsInfo::bUpdateAppStatus()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bUpdateAppStatus(const t_U32 cou32DevHandle,t_U32 cou32AppHandle,
   *             tenAppStatus enAppStatus) const
   * \brief   To update the status os any application which is stored in the storage.
   *            and returns whether the updation is success or not
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppHandle     : [IN] Application Id
   * \param   enAppStatus        : [IN] Application status
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bUpdateAppStatus(const t_U32 cou32DevHandle,
      const t_U32 cou32AppHandle,
      tenAppStatus enAppStatus);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLApplicationsInfo::bUpdateAppName()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bUpdateAppName(const t_U32 cou32DevHandle,const t_U32 cou32AppHandle,
   *             t_String szAppName)
   * \brief   To update the name of any application which is stored in the storage.
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppHandle  : [IN] Application Id
   * \param   szAppName       : [IN] Application Name
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bUpdateAppName(const t_U32 cou32DeviceHandle,const t_U32 cou32AppHandle,
      t_String szAppName);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLApplicationsInfo::bUpdateAppNotiSupport()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bUpdateAppNotiSupport(const t_U32 cou32DevHandle,
   *             const tvecMLApplist& rfcovecNotiSuppApplist)
   * \brief   To update the name of any application which is stored in the storage.
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   rfcovecNotiSuppApplist : [IN] List of notification supported apps
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bUpdateAppNotiSupport(const t_U32 cou32DevHandle,
         const tvecMLApplist& rfcovecNotiSuppApplist);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLApplicationsInfo::bCheckAppValidity()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bCheckAppValidity(const t_U32 cou32DevHandle,
   *             const t_U32 cou32AppHandle)
   * \brief   To check whether the application exists or not
   * \pram    cou32DevHandle     : [IN] Uniquely identifies the target Device.
   * \param   cou32AppHandle     : [IN] Application Id
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bCheckAppValidity(const t_U32 cou32DevHandle, 
     const t_U32 cou32AppHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetLaunchedAppIds()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetLaunchedAppIds(const t_U32 cou32DevHandle,
   *             std::vector<t_U32>& rfvecLaunchedAppIds)
   * \brief   To get the launched application id's
   * \pram    cou32DevHandle     : [IN] Uniquely identifies the target Device.
   * \param   rfvecLaunchedAppIds: [OUT] List of Application Id's
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetLaunchedAppIds(const t_U32 cou32DevHandle,
      std::vector<t_U32>& rfvecLaunchedAppIds);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetDeviceVersionInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetDeviceVersionInfo(const t_U32 cou32DevHandle,
   *             trVersionInfo& rfrVersionInfo)
   * \brief   To get the ML version of the device.
   * \pram    cou32DevHandle     : [IN] Uniquely identifies the target Device.
   * \param   rfrVersionInfo     : [OUT] Populate Device Version Info
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetDeviceVersionInfo(const t_U32 cou32DevHandle,
      trVersionInfo& rfrVersionInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::bIsAppAllowedToUse()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bIsAppAllowedToUse(t_U32 u32DevID, 
   *             t_U32 u32ConvAppID,t_Bool bDriveModeEnabled)
   * \brief   To get whether the app can be shown to user in the current vehicle mode
   * \pram    u32DevID  : [IN] Uniquely identifies the Device.
   * \param   u32ConvAppID  : [IN] Uniquely identifies the application
   * \param   bDriveModeEnabled   : [IN] Informs the current vehicle mode
   * \param   bNonCertAppsAllowed : [IN] Informs whether Non certified apps are allowed
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bIsAppAllowedToUse(t_U32 u32DevID, 
      t_U32 u32ConvAppID,  
      t_Bool bDriveModeEnabled,
      t_Bool bNonCertAppsAllowed);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLApplicationsInfo::vGetAppCertInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetAppCertInfo(t_U32 u32DevID, 
   *             t_U32 u32AppUUID,t_U32& rfu32AppID,
   *             tenAppCertificationInfo& rfenAppCertInfo )
   * \brief   To get the actual app id and the certification status of an app
   * \pram    u32DevID      : [IN] Uniquely identifies the Device.
   * \param   rfu32AppID      : [IN] Uniquely identifies the application
   * \param   rfu32HMISharedAppId    : [IN] Application ID shared with HMI
   * \param   rfenAppCertInfo : [IN] Application certification status
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppCertInfo(t_U32 u32DevID, 
      t_U32 u32AppUUID,
      t_U32& rfu32AppID,
      tenAppCertificationInfo& rfenAppCertInfo );

   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<spi_tclMLApplicationsInfo>;


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/
private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLApplicationsInfo::spi_tclMLApplicationsInfo()
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLApplicationsInfo()
   * \brief   Default Constructor
   * \sa      ~spi_tclMLApplicationsInfo()
   **************************************************************************/
   spi_tclMLApplicationsInfo();
   
   //! Map to store the Devices applications info
   std::map<const t_U32,trMLDeviceAppInfo> m_mapAppInfo;

   //! Lock Variable
   Lock m_oLock;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/
};

#endif//_SPI_TCLMLAPPLICATIONSINFO_H_
