/***********************************************************************/
/*!
* \file  spi_tclMLAppMngr.h
* \brief Mirror Link Application Manager 
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Mirror Link Application Manager
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
26.12.2013  | Shiva Kumar Gurija    | Updated with new elements for 
                                       Handling Devices and App's Info
23.04.2014  | Shiva Kumar Gurija    | Updated with ML Notifications Impl
24.07.2014  | Shiva Kumar Gurija    | Work around to fetch the missing app name
11.08.2014  | Ramya Murthy          | Revised logic to subscribe/unsubscribe 
                                      for Notification supported apps.

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLMLAPPMNGR_H_
#define _SPI_TCLMLAPPMNGR_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include <map>
#include "Lock.h"
#include "spi_tclMLVncRespDiscoverer.h"
#include "spi_tclMLVncRespViewer.h"
#include "spi_tclMLVncRespDAP.h"
#include "spi_tclAppMngrDev.h"
#include "spi_tclMLVncRespNotif.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

class spi_tclMLVncManager;
class spi_tclRespInterface;


/****************************************************************************/
/*!
* \class spi_tclAppManager
* \brief Mirror Link Application Manager
****************************************************************************/
class spi_tclMLAppMngr : public spi_tclAppMngrDev, public spi_tclMLVncRespDiscoverer,public spi_tclMLVncRespViewer,
   public spi_tclMLVncRespNotif,public spi_tclMLVncRespDAP
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLAppMngr::spi_tclMLAppMngr(spi_tclRespInterface* poSpiRespIntf)
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLAppMngr(spi_tclRespInterface* poSpiRespIntf)
   * \brief   Parameterized Constructor
   * \param   poSpiRespIntf : [IN] Response Interface pointer object
   * \sa      ~spi_tclMLAppMngr()
   **************************************************************************/
   spi_tclMLAppMngr();

   /***************************************************************************
   ** FUNCTION:  spi_tclMLAppMngr::~spi_tclMLAppMngr()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLAppMngr()
   * \brief   Destructor
   * \sa      spi_tclMLAppMngr()
   **************************************************************************/
   ~spi_tclMLAppMngr();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLAppMngr::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the ML App Mngr related things
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief   To Uninitialize all the ML App Mngr related things
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vUnInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclMLAppMngr::vRegisterAppMngrCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo App Mngr
   * \param   corfrAppMngrCbs : [IN] Application Manager callabcks structure
   * \retval  t_Void 
   **************************************************************************/
   t_Void vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSelectDevice(const t_U32 cou32DevId,
   *          const tenDeviceConnectionReq coenConnReq)
   * \brief   To update teh changes in the app list of the slected device
   * \pram    cou32DevId : [IN] Unique Device Id
   * \param   coenConnReq : [IN] connected/disconnected
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchApp(const t_U32 cou32DevId, 
   *           t_U32 u32AppHandle, 
   *           const trUserContext& rfrcUsrCntxt)
   * \brief   To Launch the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   u32AppHandle : [IN]  Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface. 
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   *          rfrcUsrCntxt : [IN] user Context
   * \retval  t_Void
   * \sa      vTerminateApp()
   **************************************************************************/
   t_Void vLaunchApp(const t_U32 cou32DevId, 
      t_U32 u32AppHandle,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vTerminateApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vTerminateApp(trUserContext rUserContext,
   *          t_U32 u32DeviceId, t_U32 u32AppId)
   * \brief   To Terminate an Application asynchronously.
   * \param   rUserContext : [IN] Context Message
   * \param   u32DeviceId  : [IN] Device Id
   * \param   u32AppId     : [IN] Application Id
   * \retval  t_Void
   * \sa      t_Bool bLaunchApp(t_U32 u32DeviceId, t_U32 u32AppId)
   **************************************************************************/
   t_Void vTerminateApp(const t_U32 cou32DevId, 
      const t_U32 cou32AppId,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vGetAppDetailsList()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetDeviceInfoList(t_U32 u32DevId,
   *         t_U32& u32NumAppInfoList,
   *         std::vector<trAppDetails>& corfvecrAppDetailsList,
   *         const trUserContext rcUsrCntxt);
   * \brief  It provides a list of applications supported by a device.
   * \param  u32DevId  : [IN] Device Handle
   * \param  u32NumAppInfoList : [OUT] Number of Applications in the List
   * \param  corfvecrAppDetailsList : [OUT] List of applicationinfo's of the 
   *                                 Applications supported by the device
   * \param  rcUsrCntxt : [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppDetailsList(const t_U32 u32DeviceHandle,
      t_U32& u32NumAppInfoList,
      std::vector<trAppDetails>& corfvecrAppDetailsList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vGetAppIconData()
   ***************************************************************************/
   /*!
   * \fn    virtual t_Void vGetAppIconData(t_String szAppIconUrl, 
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  szAppIconUrl  : [IN] Application Icon data
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppIconData(t_String szAppIconUrl, 
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vGetAppInfo()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetAppInfo(const t_U32 cou32DevId, 
   *         const t_U32 cou32AppId,
   *         trAppDetails& rfrAppDetails)
   * \brief  To Get an applications info
   * \param    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param    cou32AppId  : [IN] Application Id
   * \param    rfrAppDetails : [OUT]App  Info
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppInfo(const t_U32 cou32DevId, 
      const t_U32 cou32AppId,
      trAppDetails& rfrAppDetails);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vDisplayAllAppsInfo()
   ***************************************************************************/
   /*!
   * \fn    t_Void vDisplayAllAppsInfo(const t_U32 cou32DevId)
   * \brief  To display all the stored applications info
   * \param    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \retval  t_Void
   **************************************************************************/
   t_Void vDisplayAllAppsInfo(const t_U32 cou32DevId);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLAppMngr::vSetAppIconAttributes()
   ***************************************************************************/
   /*!
   * \fn     t_Bool vSetAppIconAttributes(t_U32 u32DevId, 
   *            t_U32 u32AppHandle,
   *            const IconAttributes &rfrIconAttributes)
   * \brief  sets application icon attributes for retrieval of application icons.
   * \param  u32DevId      :  [IN] Uniquely identifies the target Device.
   * \param  u32AppHandle  : [IN] Uniquely identifies an application on the 
   *              target device. This value will be obtained from GetAppList Interface.
   * \param  rfrIconAttributes : [IN] Icon details.
   **************************************************************************/
   t_Bool bSetAppIconAttributes(t_U32 u32DevId,
      t_U32 u32AppHandle,
      const trIconAttributes &rfrIconAttributes);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
   *          t_Bool bSetConfig,const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the Vehicle configurations.
   * \param  [IN] enVehicleConfig :  Identifies the Vehicle Configuration.
   * \param  [IN] bSetConfig      : Enable/Disable config
   **************************************************************************/
   t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLAppMngr::bCheckAppValidity()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bCheckAppValidity(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId)
   * \brief   To check whether the application exists or not
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId  : [IN] Application Id
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bCheckAppValidity(const t_U32 cou32DevId, 
      const t_U32 cou32AppId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vDisplayAppcertificationInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId)
   * \brief   To disaply th eapplication certification info includes
   *           restricted & non restricted list of the application
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId  : [IN] Application Id
   * \retval  t_Void
   **************************************************************************/
   t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevId,
      const t_U32 cou32AppId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLAppMngr::bSetClientProfile()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bSetClientProfile(const t_U32 cou32DevHandle,
   *             const trClientProfile& corfrClientProfile)
   * \brief   To set the client profiles
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   corfrClientProfile  : [IN] Client Profile to be set
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bSetClientProfile(const t_U32 cou32DevId,
      const trClientProfile& corfrClientProfile);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vSetVehicleBTAddress()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetVehicleBTAddress(const t_U32 cou32DevHandle,
   *             t_String szBTAddress,
   *             const trUserContext& corfrUsrCntxt)
   * \brief   To set the ML Client Bluetooth address
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   szBTAddress  : [IN] 12-bit BT Address
   * \param   corfrUsrCntxt : [IN] User context
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetVehicleBTAddress(const t_U32 cou32DevId,
      t_String szBTAddress,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vSetMLNotificationEnabledInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetMLNotificationEnabledInfo(const t_U32 cou32DevId,
   *            t_U16 u16NumNotiEnableList,
   *            std::vector<NotificationEnable> vecrNotiEnableList,
   *            const trUserContext corfrUsrCntxt)
   * \brief  Interface to set the device notification preference for
   *         applications.
   *         Set u32DeviceHandle to 0xFFFF, to indicate all devices.
   *         If notification for all the applications has to be:
   *         Enabled - Set NumNotificationEnableList to 0xFFFF
   *                   and NotificationEnableList should be empty.
   *         Disabled - Set NumNotificationEnableList to 0x00.
   *                   and NotificationEnableList should be empty.
   * \param  cou32DevId           : [IN] Uniquely identifies the Device.
   * \param  u16NumNotiEnableList : [IN] Total number of records in
   *              NotificationEnableList.
   * \param  vecrNotiEnableList   : [IN] List of NotificationEnable records.
   * \param  corfrUsrCntxt        : [IN] User Context Details.
   * \sa     spi_tclRespInterface::vPostSetMLNotificationEnabledInfoResult
   **************************************************************************/
   t_Void vSetMLNotificationEnabledInfo(const t_U32 cou32DevId,
      t_U16 u16NumNotiEnableList,
      std::vector<trNotiEnable> vecrNotiEnableList,
      const trUserContext& corfrUsrCntxt);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vInvokeNotificationAction()
   ***************************************************************************/
   /*!
   * \fn     t_Void vInvokeNotificationAction(const t_U32 cou32DevId,
   *            t_U32 u32NotificationID, 
   *            t_U32 u32NotificationActionID,
   *            const trUserContext& corfrUsrCntxt)
   * \brief  Interface to invoke the respective action for the received
   *         Notification Event.
   * \param  cou32DevId        : [IN]  Uniquely identifies the target Device.
   * \param  u32NotificationID : [IN]  Notification Identifier.
   * \param  u32NotificationActionID : [IN]  Notification action Identifier.
   * \param  corfrUsrCntxt     : [IN]  User Context Details.
   **************************************************************************/
   t_Void vInvokeNotificationAction(const t_U32 cou32DevId,
      t_U32 u32NotificationID,
      t_U32 u32NotificationActionID,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vSetMLNotificationOnOff()
   ***************************************************************************/
   /*!
   * \fn     t_Bool vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
   * \brief  To Set the Notifications to On/Off
   * \param  bSetNotificationsOn : [IN] True-Set Notifications to ON
   *                                    False - Set Notifications to OFF
   * \retval t_Void 
   **************************************************************************/
   t_Void vSetMLNotificationOnOff(t_Bool bSetNotificationsOn);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vDisplayAppListXml()
   ***************************************************************************/
   /*!
   * \fn     t_Void vDisplayAppListXml(const t_U32 cou32DevId)
   * \brief  Method to retrieve the Applications XML Buffer of the device
   *          This is ML specific
   * \param  cou32DevId : [IN] Device ID
   * \retval t_Void
   **************************************************************************/
   t_Void vDisplayAppListXml(const t_U32 cou32DevId);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLAppMngr::bIsLaunchAppReq()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bIsLaunchAppReq(t_U32 u32DevID,
   *                          t_U32 u32AppID,
   *                          t_U32 u32NotificationID,
   *                          t_U32 u32NotificationActionID)
   *            
   * \brief   Checks whether the launch app is required for the user action
   *          upon receiving a Notification
   * \param   u32DevID           : [IN] Device handle to identify the MLServer
   * \param   u32AppID           : [IN] ApplicationID for which Notification was available
   * \param   u32NotificationID  : [IN] Notification identifier sent during
   *                                    notification event
   * \param   u32NotificationActionID : [IN]  ActionID corresponding to the user action
   *                                     on HMI
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bIsLaunchAppReq(t_U32 u32DevID,
      t_U32 u32AppID,
      t_U32 u32NotificationID,
      t_U32 u32NotificationActionID);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostDeviceInfo
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostDeviceInfo(const const t_U32  cou32DeviceHandle,
   *                                    const trDeviceInfo& corfrDeviceInfo)
   * \brief   To Post the device info to SPI, when a new device is detected
   * \param   cou32DeviceHandle : [IN] Device Id
   * \param   corfrDeviceInfo   : [IN] const reference to the DeviceInfo structure.
   * \retval  t_Void
   ***************************************************************************/
   t_Void vPostDeviceInfo(const t_U32 cou32DeviceHandle,
      const trMLDeviceInfo& corfrDeviceInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostDeviceDisconncted()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostDeviceDisconncted(const t_U32 
   *          cou32DeviceHandle)
   * \brief   To Post the Device Id to SPI, when a device is disconnected
   * \param   rUserContext      : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle : [IN] Device Id
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostDeviceDisconnected(const t_U32 cou32DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostLaunchAppStatus(trUserContext...
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostLaunchAppStatus(const t_U32 cou32DeviceHandle,
   *                                   t_U32 u32AppId, t_Bool bResult);
   * \brief   To post the response of launch application request.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle  : [IN] Device Id
   * \param   u32AppId     : [IN] Application Id
   * \param   bResult      : [IN] Response True if Application is launched,
   *                                       else False
   * \retval  t_Void
   * \sa      vPostTerminateAppStatus()
   ***************************************************************************/
   t_Void vPostLaunchAppStatus(
      trUserContext rUserContext,
      const t_U32 cou32DeviceHandle,
      t_U32 u32AppId,
      t_Bool bResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostTerminateAppStatus(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostTerminateAppStatus(const t_U32 cou32DeviceHandle,
   *                                   t_U32 u32AppId, t_Bool bResult);
   * \brief   To post the response of terminate appliaction request.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle  : [IN] Device Id
   * \param   u32AppId     : [IN] Application Id
   * \param   bResult      : [IN] Response True if Application is terminated,
   *                                       else False
   * \retval  t_Void
   * \sa      vPostLaunchAppStatus()
   ***************************************************************************/
   t_Void vPostTerminateAppStatus(
      trUserContext rUserContext,
      const t_U32 cou32DeviceHandle,
      t_U32 u32AppId,
      t_Bool bResult);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostAppList(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostAppList(const t_U32 cou32DeviceHandle,
   *                  const std::vector<t_U32>& corfvecAppList )
   * \brief   To Post the list of all applications supported by a device.
   * \param   rUserContext      : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle : [IN] Device Id
   * \param   corfvecAppList    : [IN] List of applications
   * \retval  t_Void
   ***************************************************************************/
   t_Void vPostAppList(trUserContext rUserContext,
      const t_U32 cou32DeviceHandle,
      const std::vector<t_U32>& corfvecAppList );

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostAppInfo(trUserContext...
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostAppInfo(trUserContext rUserContext,
   *                              const t_U32 cou32DeviceHandle,
   *                              t_U32 u32AppId,
   *                              const trvncAppDetails& corfrAppDetails);
   * \brief   To post the Information of an application.
   * \param   rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle     : [IN] Device Id
   * \param   u32AppId        : [IN] Application Id
   * \param   corfrAppDetails : [IN] const reference to Application Info structure.
   * \retval  t_Void
   ***************************************************************************/
   t_Void vPostAppInfo(
      trUserContext rUserContext,
      const t_U32 cou32DeviceHandle,
      t_U32 u32AppId,
      const trApplicationInfo& corfrAppInfo);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostAppIconData()
   ***************************************************************************/
   /*!
   * \fn     vPostAppIconData(trUserContext rUserContext,
   *           const t_U32 u32DeviceHandle,
   *           t_String szAppIconUrl,
   *           const t_U8* pcou8BinaryData,
   *           const t_U32 u32Len)
   * \brief  It updates the Fetch App Icon data result to SPI
   * \param  rcUsrCntxt : [IN]  User Context Details.
   * \param  u32DeviceHandle: [IN] Device Id
   * \param  szAppIconUrl : [IN] App Icon URL
   * \param  pcou8BinaryData : Byte Data Stream from the icon image file.
   * \param  u32Len : [IN] Icon Length
   **************************************************************************/
   t_Void vPostAppIconData(trUserContext rUserContext,
      const t_U32 u32DeviceHandle,
      t_String szAppIconUrl,
      const t_U8* pcou8BinaryData,
      const t_U32 u32Len);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostDevEventSubscriptionStatus()
   ***************************************************************************/
   /*!
   * \fn     t_Void vPostDevEventSubscriptionStatus(const trUserContext& corfrUsrCntxt,
   *             const t_U32 u32DeviceHandle,
   *             tenEventSubscription enEventSubscription,
   *             t_Bool bResult)
   * \brief  It updates the Fetch App Icon data result to SPI
   * \param  corfrUsrCntxt : [IN]  User Context Details.
   * \param  u32DeviceHandle: [IN] Device Id
   * \param  enEventSubscription : [IN] Event subscription enumeration
   * \param  bResult : [IN] success/failure
   * \retval t_Void
   **************************************************************************/
   t_Void vPostDevEventSubscriptionStatus(const trUserContext& corfrUsrCntxt,
      const t_U32 u32DeviceHandle,
      tenEventSubscription enEventSubscription,
      t_Bool bResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostAppStatus()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostAppStatus(trUserContext rUserContext,
   *               const t_U32 cou32DeviceHandle,t_U32 u32AppHandle,
   *               tenAppStatus enAppStatus)
   * \brief   To Post an application status
   * \param   rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle   : [IN] Device Id
   * \param   u32AppHandle        : [IN] App Id
   * \param   enAppStatus       : [IN] Application status
   * \retval  t_Void
   ***************************************************************************/
   t_Void vPostAppStatus(
          trUserContext rUserContext,
          const t_U32 cou32DeviceHandle,
          t_U32 u32AppId,
          tenAppStatus enAppStatus);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostDriverDistractionAvoidanceResult
   ***************************************************************************/
   /*!
   * \fn      vPostDriverDistractionAvoidanceResult()
   * \brief   Posts the device status callback after the driver distraction avoidance is
   *          enabled or disabled.
   * \param   bDriverDistAvoided : [IN] enabled/disabled
   * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostDriverDistractionAvoidanceResult(t_Bool bDriverDistAvoided,
      const t_U32 cou32DeviceHandle = 0);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vSetVehicleMode()
   ***************************************************************************/
      /*!
   * \fn     t_Void vSetVehicleMode(t_Bool bEnableDriveMode,
   *            const trUserContext& rfrcUsrCntxt);
   * \brief  To set the vehicle mode
   * \param  bEnableDriveMode : [IN] TRUE - Enable drive mode
   *                                 FALSE - Enable Park mode
   **************************************************************************/
   t_Void vSetVehicleMode(t_Bool bEnableDriveMode);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostSetClientProfileResult()
   ***************************************************************************/
   /*!
   * \fn     t_Void vPostSetClientProfileResult(const trUserContext& corfrUsrCntxt,
   *             const t_U32 cou32DevId,
   *             t_Bool bResult)
   * \brief  It updates the Set Client Profile result to SPI
   * \param  corfrUsrCntxt : [IN]  User Context Details.
   * \param  cou32DevId    : [IN] Device Id
   * \param  bResult : [IN] success/failure
   * \retval t_Void
   **************************************************************************/
   t_Void vPostSetClientProfileResult(const trUserContext& corfrUsrCntxt,
      const t_U32 cou32DevId,
      t_Bool bResult);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostNotiAllowedApps()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostNotiAllowedApps(trUserContext rUsrCntxt, 
   *             const t_U32 cou32DevId,
   *             tvecMLApplist &vecApplist)
   * \brief   callback in response to  vGetNotiSupportedApps().
   *          provides the list of applications that support notifications
   * \param   rUsrCntxt   : [IN] Context information passed from the caller
   * \param   cou32DevId  :[IN] Device handle
   * \param   MLApplist    : [OUT] Notification identifier sent during
   *                           notification event.
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostNotiAllowedApps(trUserContext rUsrCntxt,
      const t_U32 cou32DevId,
      tvecMLApplist &vecApplist);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostSetNotiAllowedAppsResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostSetNotiAllowedAppsResult(trUserContext rUsrCntxt,
   *             const t_U32 cou32DevId,
   *             t_Bool bSetNotiAppRes)
   * \brief   callback in response to bSetNotiEnableAppsInfo()
   * \param   rUsrCntxt   : [IN] Context information passed from the caller
   * \param   cou32DevId  :[IN] Device handle
   * \param   bSetNotiAppRes : [IN] The outcome of the request:
   *          true:     if the request was completed successfully
   *          false:    if the request failed
   **************************************************************************/
   t_Void vPostSetNotiAllowedAppsResult(trUserContext rUsrCntxt,
      const t_U32 cou32DevId, 
      t_Bool bSetNotiAppRes);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostInvokeNotiActionResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostInvokeNotiActionResult(trUserContext rUsrCntxt,
   *             const t_U32 cou32DevId,t_Bool bNotiActionRes)
   * \brief   callback in response to
   *          vInvokeNotiAction(t_U32 u32NotiId, t_U32 u32NotiActionID).
   *          provides the result of the received action (success or failure)
   * \param   rUsrCntxt   : [IN] Context information passed from the caller
   * \param   cou32DevId  :[IN] Device handle
   * \param   bNotiActionRes : [IN] The outcome of the request:
   *          true:     if the request was completed successfully
   *          false:    if the request failed
   **************************************************************************/
   t_Void vPostInvokeNotiActionResult(trUserContext rUsrCntxt,
      const t_U32 cou32DevId,
      t_Bool bNotiActionRes);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostNotiEvent()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostNotiEvent(const t_U32 cou32DevId,
   *             const trNotiData &rfrNotiDetails)
   * \brief   posts a event when notification is received from MLServer
   * \param   cou32DevId     :[IN] Device handle
   * \param   rfrNotiDetails : [OUT] structure containing notification details
   *                         such as icon details, action details etc.
   *                         /ref MLNotiDetails
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostNotiEvent(const t_U32 cou32DevId,
      const trNotiData &rfrNotiDetails);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPostAppName()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostAppName(t_U32 u32DeviceHandle,
   *             t_U32 u32AppHandle,t_String szAppName,const trUserContext& corfrUsrCntxt)
   * \brief   posts the application name of a particular app
   * \param   u32DeviceHandle     :[IN] Device handle
   * \param   u32AppHandle        :[IN] Application handle
   * \param   szAppName           :[IN] App name
   * \param   corfrUsrCntxt       :[IN] User context
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostAppName(t_U32 u32DeviceHandle, 
      t_U32 u32AppHandle,
      t_String szAppName,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostViewerSessionProgress
   ***************************************************************************/
   /*!
   * \fn      vPostViewerSessionProgress( tenSessionProgress enSessionProgress, 
   *                       const t_U32 cou32DeviceHandle = 0);
   * \brief   Posts the VNC Viewer Session establishment Progress
   * \param   enSessionProgress; [IN]Session Progress status
   * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
   * \retval  NONE
   **************************************************************************/
   t_Void vPostViewerSessionProgress(tenSessionProgress enSessionProgress, 
      const t_U32 cou32DeviceHandle = 0);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vSubscribeNotiAllowedApps()
   ***************************************************************************/
   /*!
   * \fn      vSubscribeNotiAllowedApps(t_U32 u32DevId)
   * \brief   Subscribes for notification supported apps based on drive mode status.
   *          If Drive mode is active, subscribes for Drive mode certified apps.
   *          If Park mode is active, subscribes for Park mode certified apps
   * \param   u32DevId : [IN] Unique Device Id
   * \retval  NONE
   **************************************************************************/
   t_Void vSubscribeNotiAllowedApps(t_U32 u32DevId);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vPopulateNotiEnableList()
   ***************************************************************************/
   /*!
   * \fn      vPopulateNotiEnableList(const std::vector<trAppDetails>& rfcovecrAppList,
   *              std::vector<trNotiEnable>& rfvecrNotiEnableList)
   * \brief   Populates a NotificationEnable list with Apps present in AppDetails list.
   * \param   rfcovecrAppList: [IN] AppDetails list
   * \param   rfvecrNotiEnableList : [OUT] NotificationEnable list
   * \retval  NONE
   **************************************************************************/
   t_Void vPopulateNotiEnableList(const std::vector<trAppDetails>& rfcovecrAppList,
         std::vector<trNotiEnable>& rfvecrNotiEnableList) const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vPostAppContextInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo)
   * \brief   Posts the context info of the application on the FG
   * \param   corfrAppCntxtInfo : [IN]  Application context info
   * \retval  t_Void
   **************************************************************************/
   t_Void vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vOnDeviceDeselect()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnDeviceDeselect(const t_U32 cou32DevId)
   * \brief   Deallocates the resources allocated for a device
   * \param   cou32DevId : [IN] Unique Device ID
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnDeviceDeselect(const t_U32 cou32DevId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppMngr::vOnDeviceSelect()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnDeviceSelect(const t_U32 cou32DevId)
   * \brief   Allocates the resources required for a device
   * \param   cou32DevId : [IN] Unique Device ID
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnDeviceSelect(const t_U32 cou32DevId);

   /***************************************************************************
   ** FUNCTION:t_Void spi_tclMLAppMngr::vPostDAPAttestResp()
   ***************************************************************************/
   /*!
   * \fn    t_Void vPostDAPAttestResp(trUserContext rUsrCntext,
   *            tenMLAttestationResult enAttestationResult,
   *            t_String szVNCAppPublickey,
   *            t_String szUPnPAppPublickey,
   *            t_String szVNCUrl,
   *            const t_U32 cou32DeviceHandle,
   *            t_Bool bVNCAvailInDAPAttestResp)
   * \brief   posts result of DAP request is complete
   * \param   rUsrCntext             : [IN] Context information passed from the caller
   * \param   enAttestationResult    : [IN] DAP attestation response
   * \param   szVNCAppPublickey      :[IN] Only for the component TerminalMode:VNC-Server
   *                                  Application public key will be sent.
   * \param   szUPnPAppPublickey     :[IN] Only for the component TerminalMode:UPnP-Server
   *                                  Application public key will be sent.
   * \param   szVNCUrl               :[IN] VNC Server URL
   * \param   cou32DeviceHandle      :[IN] Device handle
   * \param  bVNCAvailInDAPAttestResp:[IN] VNC-Server is listed in DAP attestation response
   * \retval t_Void
   **************************************************************************/
   t_Void vPostDAPAttestResp(trUserContext rUsrCntext,
      tenMLAttestationResult enAttestationResult,
      t_String szVNCAppPublickey,
      t_String szUPnPAppPublickey,
      t_String szVNCUrl,
      const t_U32 cou32DeviceHandle,
      t_Bool bVNCAvailInDAPAttestResp);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLAppMngr::vMapUTF8StrsToAscii()
   ***************************************************************************/
   /*!
   * \fn     t_Void vMapUTF8StrsToAscii() const
   * \brief  Method to insert identified UTF8 strings and equivalent ASCII characters
   * 		 in the map
   * \retval t_Void
   **************************************************************************/
   t_Void vMapUTF8StrsToAscii() const;

   /***************************************************************************
   ** FUNCTION: t_String spi_tclMLAppMngr::szConvertUTF8ToAscii()
   ***************************************************************************/
   /*!
   * \fn     t_String szConvertUTF8ToAscii(t_String szUTF8Str) const
   * \brief  Method to convert UTF String to ASCII string
   * \param   szUTF8Str     : [IN] String in UTF8 Format
   * \retval  t_String : String in ASCII format
   **************************************************************************/
   t_String szConvertUTF8ToAscii(t_String szUTF8Str) const;

   //! currently selected device
   t_U32 m_u32SelDevId;

   //std::map<t_U32,tenIconMimeType> m_mapIconInfo;
   std::map< std::pair<t_U32,t_String>,tenIconMimeType > m_mapIconMimeTypeInfo;

   //! call back structure tos end response to Main App lmgr
   trAppMngrCallbacks m_rAppMngrCallbacks;

   //! Lock Variable to protect Icon MIME Type map
   Lock m_oIconMimeTypeLock;

   //! Variable to maintain Event subscription status
   t_Bool m_bSubscribeForEvents;

   //Flag to maintain the current drive mode
   t_Bool m_bDriveModeEnabled;

   //Flag to maintain the status of subscription
   t_Bool m_bNotiSubscriptionActive;

   //! Context info of the app in FG
   trAppContextInfo m_rAppCntxtInfo;

   //! Lock variable protect context info member variable
   Lock m_oContextInfoLock;

   //!map to store Devices VNC URL's
   std::map< t_U32,t_String> m_mapDevVNCURLs;

   //! Lock protect the usage of m_mapDevVNCURLs
   Lock m_oDevVNCURLsLock;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclMLAppMngr

#endif //_SPI_TCLMLAPPMNGR_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>
