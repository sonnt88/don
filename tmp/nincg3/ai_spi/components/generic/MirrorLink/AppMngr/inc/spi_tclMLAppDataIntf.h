
/***********************************************************************/
/*!
* \file  spi_tclMLAppDataIntf.h
* \brief To Get the Appication data from the container
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.02.2014  | Shiva Kumar Gurija    | Initial Version
11.08.2014  | Ramya Murthy          | Added vGetFilteredAppList() to filter apps
                                      based on certification and notification support.

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLMLDATAINTF_H_
#define _SPI_TCLMLDATAINTF_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
class spi_tclMLApplicationsInfo;

/****************************************************************************/
/*!
* \class spi_tclMLAppDataIntf
* \brief To Get the Appication data from the container
****************************************************************************/
class spi_tclMLAppDataIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLAppDataIntf::spi_tclMLAppDataIntf()
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLAppDataIntf()
   * \brief   Default Constructor
   * \sa      ~spi_tclMLAppDataIntf()
   **************************************************************************/
   spi_tclMLAppDataIntf();

   /***************************************************************************
   ** FUNCTION:  spi_tclMLAppDataIntf::~spi_tclMLAppDataIntf()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLAppDataIntf()
   * \brief   Destructor
   * \sa      spi_tclMLAppDataIntf()
   **************************************************************************/
   ~spi_tclMLAppDataIntf();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetAppList()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetAppList(const t_U32 cou32DevHandle,
   *           ,std::vector<trAppDetails>& rfvecAppList)
   * \brief   To Get the All applications details
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \pram    rfvecAppList  : [IN] List of Vector Applications details
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppList(const t_U32 cou32DevHandle,std::vector<trAppDetails>& rfvecAppList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetUIAppList()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetUIAppList(const t_U32 cou32DevHandle,
   *           ,std::vector<trAppDetails>& rfvecAppList)
   * \brief   To Get the UIapplications details
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \pram    rfvecAppList  : [IN] List of Vector Applications details
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetUIAppList(const t_U32 cou32DevHandle,std::vector<trAppDetails>& rfvecAppList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetFilteredAppList()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetFilteredAppList(const t_U32 cou32DevHandle,
   *              tenAppCertificationFilter enAppCertFilter, t_Bool bNotifSupportedApps,
   *              std::vector<trAppDetails>& rfvecAppList)
   * \brief   To Get a list of filtered application details
   * \pram    cou32DevHandle      : [IN] Uniquely identifies the target Device.
   * \pram    enAppCertFilter     : [IN] Type of certified apps to be filtered
   * \pram    bNotifSupportedApps : [IN] If set, only notification supported apps will be
   *              filtered, else all apps.
   * \pram    rfvecAppList  : [IN] List of filtered Applications details
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetFilteredAppList(const t_U32 cou32DevHandle,
         tenAppCertificationFilter enAppCertFilter,
         t_Bool bNotifSupportedApps,
         std::vector<trAppDetails>& rfvecAppList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetAppInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetAppInfo(const t_U32 cou32DevHandle,
   *             const t_U32 cou32AppHandle,
   *             trAppDetails& rfrAppDetails)
   * \brief   To Get an application info
   * \param   cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppHandle  : [IN] Application Id
   * \param   rfrAppDetails   : [OUT] Application details struct
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppInfo(const t_U32 cou32DevHandle, 
      const t_U32 cou32AppHandle,
      trAppDetails& rfrAppDetails);

   /***************************************************************************
   ** FUNCTION:  tenIconMimeType spi_tclMLAppDataIntf::enGetAppIconMIMEType()
   ***************************************************************************/
   /*!
   * \fn      tenIconMimeType enGetAppIconMIMEType(const t_U32 cou32DevHandle, 
   *             t_String szAppIconUrl)
   * \brief   To Get an applications info
   * \param   cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   szAppIconUrl  : [IN] String Url
   * \retval  tenIconMimeType
   **************************************************************************/
   tenIconMimeType enGetAppIconMIMEType(const t_U32 cou32DevHandle,t_String szAppIconUrl);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppDataIntf::vDisplayAllAppsInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDisplayAllAppsInfo(const t_U32 cou32DevHandle)
   * \brief   To Display all applications info
   * \param   cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \retval  t_Void
   **************************************************************************/
   t_Void vDisplayAllAppsInfo(const t_U32 cou32DevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLAppDataIntf::bCheckAppValidity()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bCheckAppValidity(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId)
   * \brief   To check whether the application exists or not
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId     : [IN] Application Id
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bCheckAppValidity(const t_U32 cou32DevId, 
     const t_U32 cou32AppId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppDataIntf::bCheckAppValidity()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetLaunchedAppIds(const t_U32 cou32DeviceHandle,
   *             std::vector<t_U32>& rfvecLaunchedAppIds)
   * \brief   To get the launched application id's
   * \pram    cou32DeviceHandle  : [IN] Uniquely identifies the target Device.
   * \param   rfvecLaunchedAppIds: [OUT] List of Application Id's
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetLaunchedAppIds(const t_U32 cou32DevId,
      std::vector<t_U32>& rfvecLaunchedAppIds);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppDataIntf::bIsAppAllowedToUse()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bIsAppAllowedToUse(t_U32 u32DevID, 
   *             t_U32 u32ConvAppID,t_Bool bDriveModeEnabled)
   * \brief   To get whether the app can be shown to user in the current vehicle mode
   * \pram    u32DevID  : [IN] Uniquely identifies the Device.
   * \param   u32ConvAppID  : [IN] Uniquely identifies the application
   * \param   bDriveModeEnabled : [IN] Informs the current vehicle mode
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bIsAppAllowedToUse(t_U32 u32DevID, 
      t_U32 u32ConvAppID, 
      t_Bool bDriveModeEnabled);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLAppDataIntf::vGetAppCertInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetAppCertInfo(t_U32 u32DevID, 
   *             t_U32 u32AppUUID,t_U32& rfu32AppID,
   *             tenAppCertificationInfo& rfenAppCertInfo )
   * \brief   To get the actual app id and the certification status of an app
   * \pram    u32DevID      : [IN] Uniquely identifies the Device.
   * \param   u32AppUUID    : [IN] Uniquely identifies the application
   * \param   rfu32AppID      : [IN] Application ID shared with HMI
   * \param   rfenAppCertInfo : [IN] Application certification status
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppCertInfo(t_U32 u32DevID, 
      t_U32 u32AppUUID,
      t_U32& rfu32AppID,
      tenAppCertificationInfo& rfenAppCertInfo );


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/
private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   spi_tclMLApplicationsInfo* m_poApplicationsInfo;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/
};//spi_tclMLAppDataIntf

#endif //_SPI_TCLMLDATAINTF_H_
