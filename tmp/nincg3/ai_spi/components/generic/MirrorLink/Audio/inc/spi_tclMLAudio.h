/*!
 *******************************************************************************
 * \file             spi_tclMLAudio.h
 * \brief            Implements the Audio functionality for Mirror Link using 
 interface to Real VNC SDK through VNC Wrapper.
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Implementation for Mirror Link
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 23.10.2013 |  Hari Priya E R(RBEI/ECP2)   | Initial Version
 23.10.2014 |  Hari Priya E R(RBEI/ECP2)   | Added changes for Audio Blocking/Unblocking
 18.08.2015 |  Shiva Kumar Gurija          | Implemetation of ML Dynamic Audio
 16.09.2015 |  Shiva Kumar Gurija          | Implementation of Audio Blocking based on Audio source

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLMLAUDIO_H
#define SPI_TCLMLAUDIO_H

#include "spi_tclAudioDevBase.h"
#include "spi_tclMLVncRespAudio.h"
#include "spi_tclMLVncRespViewer.h"
#include "Lock.h"


struct trMLAudioMixing
{
   //based the current Active Audio source and Application Catgeory, whether to request for source change
   //or apply audio blocking or not decisions are made.
   //Active Audio source
   tenAudioDir enActiveAudioDir;

   //Application category received in the audio stream.
   tenAppCategory enAppCategory;

   // Says whether to change the audio source or not
   t_Bool bChangeAudioSrc;

   //Says which audio source to be allocated, in case if source change is required.
   //It is to be validated only if bChangeAudioSrc is true.
   tenAudioDir enNewAudioDir;

   //Says whether received stream is related to the Active Audio source.
   //If this is false, audio source needs to be de alloacted.

   t_Bool bActiveStreamAvailable;

   //Says whether audio blocking needs to be applied for this specific audio source or not.
   t_Bool bApplyAudioBlocking;

   //Says whether ducking needs to be performed for this context
   t_Bool bPerformAudDuck;


   trMLAudioMixing& operator=(const trMLAudioMixing& corfrMLAudioMixing)
   {
      if(this != &corfrMLAudioMixing)
      {
         enActiveAudioDir = corfrMLAudioMixing.enActiveAudioDir;
         enAppCategory = corfrMLAudioMixing.enAppCategory;
         bChangeAudioSrc = corfrMLAudioMixing.bChangeAudioSrc;
         enNewAudioDir = corfrMLAudioMixing.enNewAudioDir;
         bActiveStreamAvailable = corfrMLAudioMixing.bActiveStreamAvailable;
         bApplyAudioBlocking = corfrMLAudioMixing.bApplyAudioBlocking;
         bPerformAudDuck = corfrMLAudioMixing.bPerformAudDuck;
      }//if(this != &corfrMLAudioMixing)
      return * this;
   }
};


struct trAudBlockingTuple
{
   t_U32 u32AppID;
   tenAppCategory enAppCat;
};


class spi_tclMLVncCmdAudio;
/**
 *  class definitions.
 */
/**
 * This class implements the AudioRoutingLib support of GenericMediaPlayer.
 */
class spi_tclMLAudio: public spi_tclAudioDevBase,
      public spi_tclMLVncRespAudio,
      public spi_tclMLVncRespViewer
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLAudio::spi_tclMLAudio(tenRegID eRegID);
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLAudio()
       * \brief   Default Constructor
       **************************************************************************/
      spi_tclMLAudio();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLAudio::~spi_tclMLAudio();
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclMLAudio()
       * \brief   Virtual Destructor
       **************************************************************************/
      virtual ~spi_tclMLAudio();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLAudio::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  interface for the creator class to register for the required
       *        callbacks.
       * \param rfrAudCallbacks : reference to the callback structure
       *        populated by the caller
       **************************************************************************/
      t_Void vRegisterCallbacks(trAudioCallbacks &rfrAudCallbacks);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLAudio::bInitializeAudioPlayback(t_U32)
       ***************************************************************************/
      /*!
       * \fn      bInitializeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
       * \brief   Perform necessary actions to prepare for an Audio Playback.
       *          Function will be called prior to a Play Command from Audio Manager.
       *          Optional Interface to be implemented by Device Class.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device
	   * \param   [enAudDir]: Audio route being allocated
       * \retval  Bool value
       **************************************************************************/
      t_Bool bInitializeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLAudio::bStartAudio(t_U32,t_String)
       ***************************************************************************/
      /*!
       * \fn      bStartAudio(t_U32 u32DeviceId, t_String szAudioDev)
       * \brief   Start Streaming of Audio from the CE Device to the Audio Output
       *          Device assigned by the Audio Manager for the Source.
       *          Mandatory Interface to be implemented.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *         [szAudioDev]: ALSA Audio Device
       *          [enAudDir]    :Specify the Audio Direction(Alternate or Main Audio).
       * \retval  Bool value
       **************************************************************************/
      virtual t_Bool bStartAudio(t_U32 u32DeviceId, t_String szAudioDev,
            tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLAudio::vStopAudio(t_U32)
       ***************************************************************************/
      /*!
       * \fn      vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
       * \brief   Stop Streaming of Audio from the CE Device to the Audio Output
       *          Device assigned by the Audio Manager for the Source.
       *          Mandatory Interface to be implemented.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *          [enAudDir]  :Specify the Audio Direction.
       * \retval  None
       **************************************************************************/
      virtual t_Void vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLAudio::bFinalizeAudioPlayback(t_U32)
       ***************************************************************************/
      /*!
       * \fn      bFinalizeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
       * \brief   Perform necessary actions on completion of Audio Playback.
       *          Function will be called after to a Stop Command from Audio Manager.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
	   * \param   [enAudDir]: Audio route being deallocated
       * \retval  Bool value
       **************************************************************************/
      t_Bool bFinalizeAudioPlayback(t_U32 u32DeviceId, tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLAudio::bSelectAudioDevice(t_U32)
       ***************************************************************************/
      /*!
       * \fn      bSelectAudioDevice(t_U32 u32DeviceId)
       * \brief   Perform necessary actions specific to a device selection like
       *          obtaining audio capabilities of device, supported modes etc
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       * \retval  Bool value
       **************************************************************************/
      virtual t_Bool bSelectAudioDevice(t_U32 u32DeviceId);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLAudio::vDeselectAudioDevice()
       ***************************************************************************/
      /*!
       * \fn      vDeselectAudioDevice(t_U32 uu32DeviceId
       * \brief   Perform necessary actions specific to a device on de-selection.
       *          Optional Interface for implementation.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vDeselectAudioDevice(t_U32 u32DeviceId);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLAudio::bIsAudioLinkSupported(t_U32,
       *                                            tenAudioLink)
       ***************************************************************************/
      /*!
       * \fn      bIsAudioLinkSupported(t_U32 u32DeviceId)
       * \brief   Perform necessary actions specific to a device on de-selection.
       *          Optional Interface for implementation.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *          [enLink]: Specify the Audio Link Type for which Capability is
       *          requested. Mandatory interface to be implemented.
       * \retval  Bool value, TRUE if Supported, FALSE otherwise
       **************************************************************************/
      virtual t_Bool bIsAudioLinkSupported(t_U32 u32DeviceId,
            tenAudioLink enLink);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLAudio::vPostAudioCapabilities(trAudioCapabilities...)
       ***************************************************************************/
      /*!
       * \fn      vPostAudioCapabilities(trAudioCapabilities& oAudioCapabilities,
       *                        const t_U32 cou32DeviceHandle = 0)
       * \brief   Function overridden from Audio Response class.
       Gives the audio protocols supported by the device
       * \param   oAudioCapabilities: [IN]Protocols
       *            supported by the server
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vPostAudioCapabilities(trAudioCapabilities& rfrAudioCapabilities,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLAudio::vPostAudioCapabilitiesError
       ***************************************************************************/
      /*!
       * \fn      vPostAudioCapabilitiesError(MLAudioRouterError s32error,
       *                                const t_U32 cou32DeviceHandle = 0);
       * \brief   Posts the error in retrieving protocols supported by the ML server
       * \param   s32error: [IN]Error in retrieving protocols
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vPostAudioCapabilitiesError(t_S32 s32AudioRouterError,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostSetAudioLinkInfo (t_Void *pContext ...)
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostSetAudioLinkInfo(t_Void *pContext,
       *                                trMLAudioLinkInfo& rfrAudioLinkInfo,
       *                                t_Bool bRTPInAvail,
       *                                t_Bool bRTPOutAvail,
       *                                const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the RTP extra info required for RTP streaming
       * \param   rfrAudioLinkInfo: [IN]RTP Extra Info
       * \param   bRTPInAvail: [IN] RTP IN URL is valid
       * \param   bRTPOutAvail: [IN] RTP OUT URL is valid
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vPostSetAudioLinkInfo(t_Void *pContext,
         trMLAudioLinkInfo& rfrAudioLinkInfo,
         t_Bool bRTPInAvail,
         t_Bool bRTPOutAvail,
         const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioLinkRemovalFailError(trUserContext ...)
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostAudioLinkRemovalFailError()
       * \brief   Posts the error in removing audio links
       * \param   rUserContext : [IN] Context information passed from the caller
       * \param   u16AudioRouterError: [IN]Error in removing audio links
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vPostAudioLinkRemovalFailError(trUserContext rUserContext,
            t_U16 u16AudioRouterError, const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioLinkAddFailError
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostAudioLinkAddFailError(t_U16 u16AudioRouterError,
       *                               const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the error in adding audio links
       * \param   u16AudioRouterError: [IN]Error in adding audio links
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vPostAudioLinkAddFailError(t_S32 s32AudioRouterError,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLAudio::bSetAudioBlockingMode()
       ***************************************************************************/
      /*!
       * \fn     t_Bool bSetAudioBlockingMode(const t_U32 cou32DevId,
       *         const tenBlockingMode coenBlockingMode)
       * \brief  Interface to set the audio blocking mode.
       *         This is expected to call, only when global mute is enabled by the User.
       * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
       * \param  coenBlockingMode : [IN] Identifies the Blocking Mode.
       * \retval t_Bool
       **************************************************************************/
      t_Bool bSetAudioBlockingMode(const t_U32 cou32DevId,
            const tenBlockingMode coenBlockingMode);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLAudio::vOnAudioError()
       ***************************************************************************/
      /*!
       * \fn    t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError)
       * \brief  Interface to set the audio error.
       * \param  enAudDir       : [IN] Uniquely identifies the target Device.
       * \param  enAudioError : [IN] Audio Error
       **************************************************************************/
      t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError){}

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vPostRTPOutData()
      ***************************************************************************/
      /*!
      * \fn      t_Void vPostRTPOutData(const t_U32* cpu32RTPOutData, 
      *            t_U16 u16RTPOutDatalen,t_U8 u8MarkerBit)
      * \brief   Posts the RTP Out Data recieved from Phone
      * \param   cpu32RTPOutData: [IN] RTP Out Data
      * \param   u16RTPOutDatalen : [IN] Length of RTP Out Data
      * \param   u8MarkerBit    : [IN] Market bit - says whether it is end of stream.
      * \retval  t_Void
      **************************************************************************/
      t_Void vPostRTPOutData(const t_U32* cpu32RTPOutData, t_U16 u16RTPOutDatalen,t_U8 u8MarkerBit);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclMLAudio::bEnableRTPOutStreaming()
      ***************************************************************************/
      /*!
      * \fn      t_Bool vEnableRTPOutStreaming(t_String szAudioDev)
      * \brief   Implements initialization of RTP Out Streaming
      * \param   szAudioDev: [IN] RALSA Device name
      * \retval  t_Bool
      **************************************************************************/
      t_Bool bEnableRTPOutStreaming(t_String szAudioDev);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclMLAudio::bConnectAudioOutDevice()
      ***************************************************************************/
      /*!
      * \fn      t_Bool bConnectAudioOutDevice(t_String szALSADev)
      * \brief   Handles RTP Out ALSA Device connection
      * \param   szALSADev: [IN] ALSA Device name
      * \retval  t_Bool
      **************************************************************************/
      t_Bool bConnectAudioOutDevice(t_String szALSADev);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclMLAudio::bDisconnectAudioOutDevice()
      ***************************************************************************/
      /*!
      * \fn      t_Bool bDisconnectAudioOutDevice()
      * \brief   Handles RTP Out ALSA Device disconnection
      * \retval  t_Bool
      **************************************************************************/
      t_Bool bDisconnectAudioOutDevice();

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclMLAudio::bEvalRTPStream(...)
      ***************************************************************************/
      /*!
      * \fn      t_Bool bEvalRTPStream(t_U32 u32AppCat,t_Bool& rfbApplyBlocking,
      *            tenAudioDir& rfenAudDir,t_Bool& rfActiveStreamAvail,
      *            t_Bool& rfbPerformDuck)
      * \brief   To check whether we need to apply blocking or source change is
      *            required.
      * \param   u32AppCat        : [IN]  Application Category
      * \param   rfbApplyBlocking : [OUT] Says whether blocking needs to be applied
      * \param   rfenAudDir       : [OUT] Updated, if high priority source needs to be tuned.
      * \param   rfActiveStreamAvail : [OUT] Active stream is available or not.
      * \param   rfbPerformDuck   : [OUT] TRUE- Audio ducking needs to performed.
      *                                   FALSE - Ducking not required
      * \retval  t_Bool
      **************************************************************************/
      t_Bool bEvalRTPStream(t_U32 u32AppCat,
         t_Bool& rfbApplyBlocking, 
         tenAudioDir& rfenAudDir,
         t_Bool& rfActiveStreamAvail,
         t_Bool& rfbPerformDuck);

      /***************************************************************************
      ** FUNCTION: t_U32 spi_tclMLAudio::u32ChangeEndian(t_U32 u32Val)
      ***************************************************************************/
      /*!
      * \fn      t_U32 u32ChangeEndian(t_U32 u32Val)
      * \brief   To Change endian ness of an integer
      * \retval  t_U32
      **************************************************************************/
      t_U32 u32ChangeEndian(t_U32 u32Val);

      /***************************************************************************
      ** FUNCTION: tenAudioDir spi_tclMLAudio::enGetCurAudioDir()
      ***************************************************************************/
      /*!
      * \fn      tenAudioDir enGetCurAudioDir()
      * \brief   To Get the Active Audio direction
      * \retval  tenAudioDir
      **************************************************************************/
      tenAudioDir enGetCurAudioDir();

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vSetCurAudioDir(tenAudioDir enAudioDir)
      ***************************************************************************/
      /*!
      * \fn      t_Void vSetCurAudioDir(tenAudioDir enAudioDir)
      * \brief   To Set the Current Audio Dir
      * \param   enAudioDir   : [IN] Audio Direction
      * \retval  t_Void
      **************************************************************************/
      t_Void vSetCurAudioDir(tenAudioDir enAudioDir);

      /***************************************************************************
      ** FUNCTION: t_U32 spi_tclMLAudio::u32GetActiveDev()
      ***************************************************************************/
      /*!
      * \fn      t_U32 u32GetActiveDev()
      * \brief   To Get the Active Device ID
      * \retval  t_U32
      **************************************************************************/
      t_U32 u32GetActiveDev();

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vSetActiveDev(t_U32 u32DevID)
      ***************************************************************************/
      /*!
      * \fn      t_Void vSetActiveDev(t_U32 u32DevID)
      * \brief   To Set the Active Device ID
      * \param   u32DevID        : [IN] Active Device ID
      * \retval  t_Void
      **************************************************************************/
      t_Void vSetActiveDev(t_U32 u32DevID);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vRequestAVDeact(tenAudioDir enAudDir)
      ***************************************************************************/
      /*!
      * \fn      t_Void vRequestAVDeact(tenAudioDir enAudDir)
      * \brief   To Request for the deactivation of a audio channel.
      * \param   enAudDir        : [IN] Audio Channel Direction
      * \retval  t_Void
      **************************************************************************/
      t_Void vRequestAVDeact(tenAudioDir enAudDir);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vRequestAVAct(tenAudioDir enAudDir)
      ***************************************************************************/
      /*!
      * \fn      t_Void vRequestAVAct(tenAudioDir enAudDir,tenAudioSamplingRate enSamplingRate)
      * \brief   To Request for the activation of a audio channel.
      * \param   enAudDir        : [IN] Audio Channel Direction
      * \param   enSamplingRate  : [IN] Audio Sampling Rate
      * \retval  t_Void
      **************************************************************************/
      t_Void vRequestAVAct(tenAudioDir enAudDir,tenAudioSamplingRate enSamplingRate);

      /***************************************************************************
      ** FUNCTION: tenAudioChannelStatus spi_tclMLAudio::enGetAudioChannelStatus(..)
      ***************************************************************************/
      /*!
      * \fn      tenAudioChannelStatus enGetAudioChannelStatus(tenAudioDir enAudDir)
      * \brief   To Get the Audio channel status of requested audio dir
      * \param   enAudDir          : [IN] Audio dir
      * \retval  tenAudioChannelStatus
      **************************************************************************/
      tenAudioChannelStatus enGetAudioChannelStatus(tenAudioDir enAudDir);

     /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vSetAudioChannelStatus(...)
      ***************************************************************************/
      /*!
      * \fn      t_Void vSetAudioChannelStatus(tenAudioDir enAudDir,
      *                     tenAudioChannelStatus enAudChannelStatus)
      * \brief   To Set the Audio channel status
      * \param   enAudDir          : [IN] Audio dir
      * \param   enAudChannelStatus: [IN] Audio Channel Status
      * \retval  t_Void
      **************************************************************************/
      t_Void vSetAudioChannelStatus(tenAudioDir enAudDir,tenAudioChannelStatus enAudChannelStatus);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclMLAudio::bApplyAudblocking(...)
      ***************************************************************************/
      /*!
      * \fn      t_Bool bApplyAudblocking(t_U32 u32AppId,
      *                     tenMLAudioBlockReason enAudioBlockReason)
      * \brief   To Send request for audio blocking
      * \param   u32AppId          : [IN] Application ID
      * \param   enAudioBlockReason: [IN] Audio Blocking Reason
      * \retval  t_Bool
      **************************************************************************/
      t_Bool bApplyAudblocking(t_U32 u32AppId, 
         tenMLAudioBlockReason enAudioBlockReason);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vStoreAudBlockedTuple(...)
      ***************************************************************************/
      /*!
      * \fn      t_Void vStoreAudBlockedTuple(t_U32 u32AppId,
      *                      tenAppCategory enAppCat)
      * \brief   To store the tuple for which ML blocking is applied.
      *           this can be used to apply unblocking.
      * \param   u32AppId          : [IN] Application ID
      * \param   enAppCat          : [IN] Application category
      * \retval  t_Void
      **************************************************************************/
      t_Void vStoreAudBlockedTuple(t_U32 u32AppID, tenAppCategory enAppCat);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vUnBlockBlockedApps(...)
      ***************************************************************************/
      /*!
      * \fn      t_Void vUnBlockBlockedApps()
      * \brief   To unblock the audio of blocked apps
      *           this can be used to apply unblocking.
      * \retval  t_Void
      **************************************************************************/
      t_Void vUnBlockBlockedApps();

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLAudio::vSetAudioDucking(...)
      ***************************************************************************/
      /*!
      * \fn      t_Void vSetAudioDucking(t_Bool bEnableDucking)
      * \brief   To enable/disable ducking
      * \param   bEnableDucking : [IN] TRUE - Enable ducking
      *                               FALSE - disable ducking
      * \retval  t_Void
      **************************************************************************/
      t_Void vSetAudioDucking(t_Bool bEnableDucking);

      //! Protocols supported by the ML server
      trAudioCapabilities m_rAudioCapabilities;

      //! Pointer to spi_tclMLVncCmdAudio class
      spi_tclMLVncCmdAudio* m_poCmdAudio;

      //! Structure object for Function pointers .
      //! This will be used by Audio Manager to register for response callbacks from ML and DiPo Audio
      trAudioCallbacks m_rAudCallbacks;

      //! Current Audio link info
      trMLAudioLinkInfo m_rAudioLinkInfo;

      //!Policy value that indicates whether audio blocking based on AppCat is supported or not
      t_Bool m_bAudBlockingBasedOnAppCat;

      //!Policy value that indicates whether audio blocking based on Global Mute is supported or not
      t_Bool m_bAudBlockingBasedOnAGlobalMute;

      // Flag to maintain to Active Audio source
      tenAudioDir m_enCurAudDir;

      Lock m_oCurAudDirLock;

      //To maintain Active Device ID
      t_U32 m_u32ActiveDevice;

      Lock m_oActDevLock;

      //currently connected ALSA Device name
      t_String m_szActiveALSADev;

      //Maintains the audio all available audio channel statuses
      tenAudioChannelStatus m_aeAudChannelStatus[e8AUD_INVALID];

      Lock m_AudChannelStatusLock;

      //Policy value that indicates Dynamic Audio support
      t_Bool m_bDynAudioSupport;

      //Phones may not provide valid AppID in the RTP stream. so don't use it a unique id (Key) to store.
      //Multiple applications may have same application category, so don't use Application Category also as a Unique ID (Key)
      //Store in a list and iterate through every time before we insert and remove. though it's time consuming, we have to go with that.
      std::vector<trAudBlockingTuple> m_vecAudBlockedAppList;

      Lock m_vecAudBlockedAppListLock;

      t_Bool m_bAudDuckEnabled;

      t_Bool m_bGlobalMuteEnabled;

      //! member variable to store MAIN ALSA Device name.
      t_String m_szMainAudioALSADevice;
};
#endif // SPI_TCLMLAUDIO_H
