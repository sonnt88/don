/*!
 *******************************************************************************
 * \file             spi_tclMLAudio.cpp
 * \brief            Implements the Audio functionality for Mirror Link using 
 interface to Real VNC SDK through VNC Wrapper.
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Implementation for Mirror Link
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 23.10.2013 |  Raghavendra S               | Initial Version
 23.10.2014 |  Hari Priya E R(RBEI/ECP2)   | Added changes for Audio Blocking/Unblocking
 17.12.2014 |  Hari Priya E R(RBEI/ECP2)   | Fix for GMMY16-20400
 06.05.2015 |  Tejaswini HB                | Lint Fix
 21.05.2014 |  Shiva Kumar Gurija          | Changes to use ML1.x or above Phones
 26.05.2015 |  Tejaswini H B(RBEI/ECP2)    | Added Lint comments to suppress C++11 Errors
 03.07.2015 |  Shiva Kumar Gurija          | improvements in ML Version checking
 18.08.2015 |  Shiva Kumar Gurija          | Implemetation of ML Dynamic Audio
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include <gst/gst.h>

//VNC Wrapper Includes
#include "SPITypes.h"
#include "spi_tclMLVncManager.h"
#include "spi_tclMLVncCmdAudio.h"
#include "spi_tclMLVncCmdViewer.h"
#include "spi_tclAudioSettings.h"
#include "spi_tclMLAudio.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclMLAudio.cpp.trc.h"
#endif

static const t_String sczEmptyString= "";

static const trMLAudioMixing scoarMLAudioMixing[]= 
#include "spi_tclMLAudio_Mixing_Blocking_Strategy.cfg"

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::spi_tclMLAudio()
 ***************************************************************************/
 spi_tclMLAudio::spi_tclMLAudio() : m_poCmdAudio(NULL),
      m_bAudBlockingBasedOnAppCat(false),
      m_bAudBlockingBasedOnAGlobalMute(false),
      m_enCurAudDir(e8AUD_INVALID),
      m_u32ActiveDevice(0),
      m_bDynAudioSupport(false),
      m_bAudDuckEnabled(false), 
      m_bGlobalMuteEnabled(false),
      m_szMainAudioALSADevice("AdevEnt1Out")
{
   ETG_TRACE_USR1((" spi_tclMLAudio::spi_tclMLAudio() "));
   t_Bool bRetVal = false;

   spi_tclMLVncManager* poVNCMngr = spi_tclMLVncManager::getInstance();
   if (NULL != poVNCMngr)
   {
      //Register for both the Resp Viewer & Resp Audio
      bRetVal = (poVNCMngr->bRegisterObject((spi_tclMLVncRespViewer*) this))
            && (poVNCMngr->bRegisterObject((spi_tclMLVncRespAudio*) this));

      // Initializations to be done which are not specific to device connected/selected
      m_poCmdAudio
            = const_cast<spi_tclMLVncCmdAudio*> (poVNCMngr->poGetAudioInstance());
   }//if(NULL != poVNCMngr)

   if ((true == bRetVal) && (NULL != m_poCmdAudio))
   {
      //Initialize Audio Router SDK
      bRetVal = m_poCmdAudio->bInitializeAudioRouterSDK();
      gst_init(NULL, NULL);
   }//if((true == bRetVal) && (NULL != m_poCmdAudio))
   
   //Get the policy value to enable/disable audio blocking
   spi_tclAudioSettings* poAudSettings = spi_tclAudioSettings::getInstance();
   if (NULL != poAudSettings)
   {
      m_bAudBlockingBasedOnAppCat = poAudSettings->bGetAudBlockTriggerBasedOnCat();
      m_bAudBlockingBasedOnAGlobalMute = poAudSettings->bGetAudBlockTriggerBasedOnGlobaleMute();
      m_bDynAudioSupport = poAudSettings->bGetMLDynAudioSupport();
   }

   //! Initialize audio channel states.
   for(t_U8 u8Cnt = 0; u8Cnt < e8AUD_INVALID; u8Cnt++)
   {
      m_aeAudChannelStatus[u8Cnt] = e8_AUD_NOT_ACTIVE;
   }//for(t_U8 u8Cnt = 0; u8Cnt < e8AUD_INVALID; u8Cnt++)

}//spi_tclMLAudio::spi_tclMLAudio()


/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::~spi_tclMLAudio()
 ***************************************************************************/
spi_tclMLAudio::~spi_tclMLAudio()
{
   ETG_TRACE_USR1((" spi_tclMLAudio::~spi_tclMLAudio()"));
   m_poCmdAudio = NULL ;
}//spi_tclMLAudio::~spi_tclMLAudio()

/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::u8InitAudio(t_U32 u32DeviceId)
 ***************************************************************************/
t_Bool spi_tclMLAudio::bInitializeAudioPlayback(t_U32 u32DeviceId, tenAudioDir enAudDir)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   SPI_INTENTIONALLY_UNUSED(enAudDir);

   return true;

}//t_Bool spi_tclMLAudio::bInitializeAudioPlayback(t_U32 u32DeviceId)

/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::bFinalizeAudioPlayback(t_U32 u32DeviceId)
 ***************************************************************************/
t_Bool spi_tclMLAudio::bFinalizeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   SPI_INTENTIONALLY_UNUSED(enAudDir);

   return true;

}//t_Bool spi_tclMLAudio::bFinalizeAudioPlayback(t_U32 u32DeviceId)

/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::vRegisterCallbacks()
 ***************************************************************************/
t_Void spi_tclMLAudio::vRegisterCallbacks(trAudioCallbacks &rfrAudCallbacks)
{
   ETG_TRACE_USR1((" spi_tclMLAudio::vRegisterCallbacks "));
   m_rAudCallbacks = rfrAudCallbacks;
}//t_Void spi_tclMLAudio::vRegisterCallbacks(trAudioCallbacks &rfrAudCallbacks)


/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::bSelectAudioDevice(t_U32)
 ***************************************************************************/
t_Bool spi_tclMLAudio::bSelectAudioDevice(t_U32 u32DeviceId)
{
   /*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclMLAudio::bSelectAudioDevice:Dev-0x%x", u32DeviceId));

   t_Bool bRetVal = false;

   if (NULL != m_poCmdAudio)
   {
      bRetVal = m_poCmdAudio->bCreateAudioRouter();
      bRetVal = (true == bRetVal) ? (m_poCmdAudio->bSetRouterToML(u32DeviceId))
            : (bRetVal);
      bRetVal = (true == bRetVal) ? (m_poCmdAudio->bGetAudioCapabilities())
            : (bRetVal);

      vSetActiveDev(u32DeviceId);
   }

   //! Call the registered callbacks
   //! This callback takes care to inform Connection Manager if bCreateAudioRouter()
   //! or bSetRouterToML() fails.
   if ((NULL != m_rAudCallbacks.fvSelectDeviceResp) && (bRetVal == false))
   {
      (m_rAudCallbacks.fvSelectDeviceResp)(u32DeviceId, e8DEVCONNREQ_SELECT,
            false);
   }

   return bRetVal;

}//t_Bool spi_tclMLAudio::bSelectAudioDevice(t_U32)

/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::vDeselectAudioDevice(t_U32)
 ***************************************************************************/
t_Void spi_tclMLAudio::vDeselectAudioDevice(t_U32 u32DeviceId)
{
   /*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   ETG_TRACE_USR1((" spi_tclMLAudio::vDeselectAudioDevice-0x%x",u32DeviceId));
   t_Bool bRetVal = false;

   if (NULL != m_poCmdAudio)
   {
      if (true == m_rAudioLinkInfo.bRTPOutStreaming)
      {
         m_rAudioLinkInfo.bRTPOutStreaming = false;
         m_poCmdAudio->bStopRTPOutStreaming();
      }//if (true == m_rAudioLinkInfo.bRTPOutStreaming)

      if(true == (m_poCmdAudio->bDestroyAudioRouter()))
      {
         bRetVal = true ;
         //CleanUp the audio link info of the device stored internally.
         m_rAudioLinkInfo.pContext = NULL;
         m_rAudioLinkInfo.rRtpOutExtraInfo.szUri= sczEmptyString;
         m_rAudioLinkInfo.rRtpInExtraInfo.szUri = sczEmptyString;
      }//if(true == (m_poCmdAudio->bDestroyAudioRouter()))

   }//if (NULL != m_poCmdAudio)

   //Set m_rAudioCapabilities to default values on disconnection.
   //or else last connected device values will be used, getAvailableProtocols throws an error.
   m_rAudioCapabilities.vInitialize();

   //Set the Active device to invalid
   vSetActiveDev(0);

   //! Device is de selected. De allocate audio channels, if any were allocated.
   for(t_U8 u8Cnt = 0; u8Cnt < e8AUD_INVALID; u8Cnt++)
   {
      vRequestAVDeact(static_cast<tenAudioDir>(u8Cnt));
   }//for(t_U8 u8Cnt = 0; u8Cnt < e8AUD_INVALID; u8Cnt++)

   //Clear the vector list audio blocked applications.
   m_vecAudBlockedAppListLock.s16Lock();
   //@todo - Check whether to send audio unblocking request for all the blocked apps.
   //for(t_U8 u8Index=0;u8Index<m_vecAudBlockedAppList.size();u8Index++)
   //{
   //   bApplyAudblocking(m_vecAudBlockedAppList[u8Index].u32AppID,e16MLAUDIO_UNBLOCK);
   //}
   m_vecAudBlockedAppList.clear();
   m_vecAudBlockedAppListLock.vUnlock();

   //! Call the registered callbacks
   if (NULL != m_rAudCallbacks.fvSelectDeviceResp)
   {
      (m_rAudCallbacks.fvSelectDeviceResp)(u32DeviceId,
            e8DEVCONNREQ_DESELECT, bRetVal);
   }

}//t_Void spi_tclMLAudio::bDeselectAudioDevice(t_U32 u32DeviceId)


/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::bStartAudio(t_U32,t_String,tenAudioLink)
 ***************************************************************************/
t_Bool spi_tclMLAudio::bStartAudio(t_U32 u32DeviceId, t_String szAudioDev,
      tenAudioDir enAudDir)
{
   ETG_TRACE_USR1(("spi_tclMLAudio::bStartAudio: DevID-0x%x AudioDir-%d ALSADevice-%s",
      u32DeviceId,ETG_ENUM(AUDIO_DIRECTION,enAudDir),szAudioDev.c_str()));

   SPI_INTENTIONALLY_UNUSED(u32DeviceId);

   /*lint -esym(40,fvStartAudioResp)fvStartAudioResp Undeclared identifier */
   t_Bool bRetVal = false;

   //Update the current audio direction.
   vSetCurAudioDir(enAudDir);

   //Update the Audio Channel allocation status
   vSetAudioChannelStatus(enAudDir,e8_AUD_ACT_GRANTED);

   switch(enAudDir)
   {
   case e8AUD_MAIN_OUT:
   case e8AUD_TRANSIENT:
   case e8AUD_STEREO_MIX_OUT:
      {
         //Store the ENTERTAIN AUDIO device, when MAIN channel is allocated.
         if(e8AUD_MAIN_OUT == enAudDir)
         {
            m_szMainAudioALSADevice = szAudioDev.c_str();
         }
         bRetVal = bEnableRTPOutStreaming(szAudioDev.c_str());

         //Deactivate SPI_MIX channel, in case if any of it was Active before SPI_MAIN/TRANSIENT becomes Active 
         //When SPI_MAIN is Active, we don't need to have SPI_MIX as Active in case of ML.
         if( e8AUD_STEREO_MIX_OUT != enAudDir)
         {
            vRequestAVDeact(e8AUD_STEREO_MIX_OUT);
            //Send request for un duck
            vSetAudioDucking(false);
         }//if(enAudDir != e8AUD_
      }
      break;
   case e8AUD_VR_IN:
      {
         ETG_TRACE_USR4(("MLAudio: Voice Rec source[RTP In] is Active - Not supported at the moment"));
         //StartRTPInStreaming to be called here
      }
      break;
   case e8AUD_INVALID:
   case e8AUD_ALERT_OUT:
   case e8AUD_PHONE_IN:
   case e8AUD_MIX_OUT:
   case e8AUD_DUCK:
   default:
      {
         ETG_TRACE_USR4(("MLAudio:Native Media/Audio source which is not supported for ML is ACTIVE"));
         if(e8AUD_INVALID != enAudDir )
         {
            vRequestAVDeact(enAudDir);
         }//if(e8AUD_INVALID != enAudDir )
      }
      break;
   }//switch(enAudDir)

   //Method to unblock the blocked audio depends on the Active Source.
   vUnBlockBlockedApps();

   //! Call the registered callbacks
   if (NULL != m_rAudCallbacks.fvStartAudioResp)
   {
      (m_rAudCallbacks.fvStartAudioResp)(enAudDir, bRetVal);
   }

   return bRetVal;

}//t_Bool bStartAudio(t_U32 u32DeviceId, tString szAudioDev, tenAudioLink enLink)

/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::vStopAudio(t_U32)
 ***************************************************************************/
t_Void spi_tclMLAudio::vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
{
   /*lint -esym(40,fvStopAudioResp)fvStopAudioResp Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclMLAudio::vStopAudio: Dev-0x%x Aud Dir -%d",
      u32DeviceId,ETG_ENUM(AUDIO_DIRECTION,enAudDir)));

   t_Bool bStopStreaming = false;

   //Update Active Audio direction, only if the stop audio is received for the Active source
   if(enGetCurAudioDir() == enAudDir)
   {
      vSetCurAudioDir(e8AUD_INVALID);

      //Set the flag to TRUE, to trigger disconnect_audio_out for ALSA device disconnect.
      bStopStreaming = true;
   }//if(enGetCurAudioDir() == enAudDir)

   //Update the audio allocation status of the Aud dir
   vSetAudioChannelStatus(enAudDir,e8_AUD_NOT_ACTIVE);

   switch(enAudDir)
   {
   case e8AUD_MAIN_OUT:
   case e8AUD_TRANSIENT:
   case e8AUD_STEREO_MIX_OUT:
      {
         bStopStreaming = (bStopStreaming)?bDisconnectAudioOutDevice():bStopStreaming;

         if(enAudDir == e8AUD_STEREO_MIX_OUT)
         {
            //Send request for un duck
            vSetAudioDucking(false);
         }//if(enAudDir == e8AUD_STEREO_MIX_OUT)
         //Applying audio blocking will be taken care on reception of RTP Out data.
      }
      break;
   case e8AUD_VR_IN:
      {
         ETG_TRACE_USR4(("MLAudio: Voice Rec source[RTP In] beacme Inactive - Not supported at the moment"));
         //StopRTPInStreaming to be called here
      }
      break;
   case e8AUD_MIX_OUT:
   case e8AUD_DUCK:
   case e8AUD_INVALID:
   case e8AUD_ALERT_OUT:
   case e8AUD_PHONE_IN:
   default:
      {
         ETG_TRACE_USR4(("MLAudio: Native Media/unused ML source became Inactive"));
      }
      break;
   }//switch(enAudDir)

   //! Call the registered callbacks
   if (NULL != m_rAudCallbacks.fvStopAudioResp)
   {
      (m_rAudCallbacks.fvStopAudioResp)(enAudDir, bStopStreaming);
   }

}//t_Void vStopAudio(t_U32 u32DeviceId)

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLAudio::bEnableRTPOutStreaming(t_String szAudioDev)
***************************************************************************/
t_Bool spi_tclMLAudio::bEnableRTPOutStreaming(t_String szAudioDev)
{
   ETG_TRACE_USR1(("spi_tclMLAudio::bEnableRTPOutStreaming-%s",szAudioDev.c_str()));

   t_Bool bRetVal= false;

   if ((NULL != m_poCmdAudio) && (false == szAudioDev.empty()))
   {
      //bStartRTPOutStreaming should be called only once per device, in case of Audio OUT.
      //It creates the required gstreamer pipeline for RTP Out Audio. 

      //ADIT uses this ALSA Device provided with this interface to establish UDP connection 
      //between MLS and MLC.
      if(false == m_rAudioLinkInfo.bRTPOutStreaming)
      {
         if(true ==  m_poCmdAudio->bStartRTPOutStreaming(m_rAudioLinkInfo.pContext,
                   m_rAudioLinkInfo, szAudioDev))
         {
            m_szActiveALSADev = szAudioDev.c_str();
            bRetVal = true;
            m_rAudioLinkInfo.bRTPOutStreaming = bRetVal;
         }//if(true ==  m_poCmdAudio->bStartR
      }//if(true != m_rAudioLinkInfo.bRTPOutStreaming)
      else 
      {
         bRetVal = bConnectAudioOutDevice(szAudioDev.c_str());
      }//else 
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLAudio::bConnectAudioOutDevice(t_String szALSADev)
***************************************************************************/
t_Bool spi_tclMLAudio::bConnectAudioOutDevice(t_String szALSADev)
{
   t_Bool bRet=true;

   if(NULL != m_poCmdAudio)
   {
      //Check if a ALSA device was already active
      if(true != m_szActiveALSADev.empty())
      { 
         //A new ALSA device to be connected. disconnect the active one and connect new one.
         //@todo - check what should be done, if the last active source and new source are using same ALSA Device?.
         //Consider that tcl_Audio triggers this method on every launch app apart from 
         //SOurceActivityOn received from Audio manager also.
         if(m_szActiveALSADev != szALSADev)
         {
            bRet = bDisconnectAudioOutDevice();

            m_poCmdAudio->vConnectAudioOutDevice(szALSADev);
            m_szActiveALSADev = szALSADev.c_str();
         }//if(m_szActiveALSADev != szAudioDev)
         else
         {
            ETG_TRACE_USR4(("MLAudio: This ALSA Device is already Active"));
         }
      }//if(true != m_szActiveALSADev.empty())
      else
      {
         m_poCmdAudio->vConnectAudioOutDevice(szALSADev);
         m_szActiveALSADev = szALSADev.c_str();
      }//else - if(true != m_szActiveALSADev.empty())
   }

   return bRet;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLAudio::bDisconnectAudioOutDevice()
***************************************************************************/
t_Bool spi_tclMLAudio::bDisconnectAudioOutDevice()
{
   ETG_TRACE_USR1(("spi_tclMLAudio::bDisconnectAudioOutDevice()"));
   t_Bool bRet=false;

   if ( (NULL != m_poCmdAudio)&&(true != m_szActiveALSADev.empty()))
   {
      m_poCmdAudio->vDisconnectAudioOutDevice();
      m_szActiveALSADev.clear();
      bRet=true;
   }//if ( (NULL != m_poCmdAudio)&&

   return bRet;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLAudio::bDeselectAudioDevice(t_U32)
 ***************************************************************************/
t_Bool spi_tclMLAudio::bIsAudioLinkSupported(t_U32 u32DeviceId,
      tenAudioLink enLink)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   ETG_TRACE_USR1(("spi_tclMLAudio::bIsAudioLinkSupported"));
   t_Bool bRetVal = false;

   switch (enLink)
   {
      case e8LINK_RTPOUT:
      {
         bRetVal = m_rAudioCapabilities.bRTPOutSupport;
         break;
      }
      case e8LINK_RTPIN:
      {
         bRetVal = m_rAudioCapabilities.bRTPInSupport;
         break;
      }
      case e8LINK_BTA2DP:
      {
         bRetVal = m_rAudioCapabilities.bBTA2DPSupport;
         break;
      }
      case e8LINK_BTHFP:
      {
         bRetVal = m_rAudioCapabilities.bBTHFPSupport;
         break;
      }
      default:
      {
         break;
      }
   }//End of switch(enLink)

   return bRetVal;
}//t_Bool spi_tclMLAudio::bIsAudioLinkSupported(t_U32 u32DeviceId, tenAudioLink enLink)


/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLAudio::vPostAudioCapabilities(trAudioCapabilities...)
 ***************************************************************************/
t_Void spi_tclMLAudio::vPostAudioCapabilities(
   trAudioCapabilities& rfrAudioCapabilities, 
   const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" spi_tclMLAudio::vPostAudioCapabilities"));
   //Capabilities can be stored in this class or in the Audio Command class.
   //and provide a get function to retreive the capabilities.
   //to be checked
   m_rAudioCapabilities = rfrAudioCapabilities;
   t_Bool bRetVal = false;

   if (NULL != m_poCmdAudio)
   {
      //Add audio links for the selected protocol
      bRetVal = m_poCmdAudio->bAddAudioLinks(cou32DeviceHandle);
   }

   //Invoke the callback function here if required for bSelectAudioDevice()
   if (NULL != (m_rAudCallbacks.fvSelectDeviceResp))
   {
      (m_rAudCallbacks.fvSelectDeviceResp)(cou32DeviceHandle,
            e8DEVCONNREQ_SELECT, bRetVal);
   }
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLAudio::vPostAudioCapabilitiesError(trAudioCapabilities...)
 ***************************************************************************/
t_Void spi_tclMLAudio::vPostAudioCapabilitiesError(t_S32 s32AudioRouterError,
                                                   const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLAudio::vPostAudioCapabilitiesError-%d",s32AudioRouterError));
   /*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
   //! Call the registered callbacks
   if (NULL != (m_rAudCallbacks.fvSelectDeviceResp))
   {
      (m_rAudCallbacks.fvSelectDeviceResp)(cou32DeviceHandle,
            e8DEVCONNREQ_SELECT, false);
   }
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostSetAudioLinkInfo (...)
 ***************************************************************************/
t_Void spi_tclMLAudio::vPostSetAudioLinkInfo(t_Void *pContext,
                                             trMLAudioLinkInfo& rfrAudioLinkInfo,
                                             t_Bool bRTPInAvail,
                                             t_Bool bRTPOutAvail,
                                             const t_U32 cou32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);

   ETG_TRACE_USR4(("vPostSetAudioLinkInfo: pContext = %p RtpOut Uri %s ",
      pContext, rfrAudioLinkInfo.rRtpOutExtraInfo.szUri.c_str()));

   ETG_TRACE_USR4(("RtpIn Uri %s ",
      rfrAudioLinkInfo.rRtpInExtraInfo.szUri.c_str()));

   m_rAudioLinkInfo.pContext = pContext;

   if(bRTPOutAvail)
   {
      m_rAudioLinkInfo.rRtpOutExtraInfo.szUri
         = rfrAudioLinkInfo.rRtpOutExtraInfo.szUri.c_str();

      //Update the Audio Channel allocation status
      if(e8_AUD_ACT_GRANTED == enGetAudioChannelStatus(e8AUD_MAIN_OUT))
      {
         //Audio route was allocated before the RTP OUT URL becomes available.
         //Call bStartRTPStreaming() method to start the streaming.
         ETG_TRACE_USR2(("Entertain audio channel is already allocated to SPI, but the ML streaming is not started.Start it"));
         bEnableRTPOutStreaming(m_szMainAudioALSADevice.c_str());
      }//if(e8_AUD_ACT_GRANTED == enGetAudioChannelStatus(e8AUD_MAIN_OUT))
   }//if(sczEmptyString != rfrAudioLin

   if(bRTPInAvail)
   {
      m_rAudioLinkInfo.rRtpInExtraInfo.szUri
         = rfrAudioLinkInfo.rRtpInExtraInfo.szUri.c_str();
   }//if( sczEmptyString != rfrAudioLin
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioLinkAddFailError (t_S32 ...)
 ***************************************************************************/
t_Void spi_tclMLAudio::vPostAudioLinkAddFailError(t_S32 s32AudioRouterError,
      const t_U32 cou32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLAudio::vPostAudioLinkAddFailError "
            "s32AudioRouterError = %d ", s32AudioRouterError));
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLAudio::bSetAudioBlockingMode()
 ***************************************************************************/
t_Bool spi_tclMLAudio::bSetAudioBlockingMode(const t_U32 cou32DevId,
      const tenBlockingMode coenBlockingMode)
{
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   ETG_TRACE_USR1(("spi_tclMLAudio::bSetAudioBlockingMode - Blocking Mode-%d ",
      coenBlockingMode));

   m_bGlobalMuteEnabled = (e8ENABLE_BLOCKING == coenBlockingMode);

   if(e8ENABLE_BLOCKING != coenBlockingMode)
   {
      //unblock the audio from blocked applications, when the Global Mute is disabled.
      //There might have some source changes happened, when the Global Mute is enabled. So only
      //unblock the audio from apps based on the Active Audio source.
      vUnBlockBlockedApps();
   }

   return true;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vPostRTPOutData()
***************************************************************************/
t_Void spi_tclMLAudio::vPostRTPOutData(const t_U32* cpu32RTPOutData, 
                                       t_U16 u16RTPOutDatalen,
                                       t_U8 u8MarkerBit)
{

   //Lint compression
   /*lint -esym(40,fvTerminateAudioReq)fvTerminateAudioReq Undeclared identifier */
   /*lint -esym(40,fvLaunchAudioReq)fvLaunchAudioReq Undeclared identifier */

   if(true == m_bDynAudioSupport)
   {
      ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,"spi_tclMLAudio::vPostRTPOutData:"
         "RTPDataLen-%d MarkerBit-%d", u16RTPOutDatalen,u8MarkerBit));

      tenAudioDir enNewAudioDir=e8AUD_INVALID;
      t_Bool bCurStreamActive=false;
      t_Bool bPerformDuck = false;

      //RTP Header extension is received in Little Endian format. Change it to Big Endian and use it.
      for(t_U16 u16Index=0;u16Index<u16RTPOutDatalen ;u16Index += 2)
      {
         t_U32 u32AppID = u32ChangeEndian(cpu32RTPOutData[u16Index]); 
         t_U32 u32AppCat = u32ChangeEndian(cpu32RTPOutData[u16Index+1]);

         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,
            "AppId-0x%x AppCat-0x%x",u32AppID,u32AppCat));

         //0xFFFF0010 is a special case[RTP Out - Which needs to be played on TRANSIENT], where in 
         //0XFFFFxxxx to be played on SPI_MIX. 
         if(0xFFFF0010 != (u32AppCat&0xFFFF0010))
         {
            u32AppCat = u32AppCat&0xFFFF0000;
         }//if(0xFFFF0010 != u32AppCat&0xFFFF0010)

         t_Bool bApplyBlocking = false;

         //return value is unused for now.
         bEvalRTPStream(u32AppCat,bApplyBlocking,enNewAudioDir,bCurStreamActive,bPerformDuck);

         //Apply Audio blocking in below cases
         //1. When the audio from a particular app[AppCategory] is not allowed to play with the Active audio source
         //2. When the Global Mute is enabled.
         if ( ((true == m_bAudBlockingBasedOnAppCat) && (true == bApplyBlocking)) || 
            ((true == m_bAudBlockingBasedOnAGlobalMute) && (true == m_bGlobalMuteEnabled)) )
         {
            tenMLAudioBlockReason enAudioBlockReason = (true == m_bGlobalMuteEnabled)?
                                    e16MLGLOBALLY_MUTED:e16MLAUDIO_APP_UNIQUEID_NOTALLOWED;

            if(true == bApplyAudblocking(u32AppID,enAudioBlockReason))
            {
               vStoreAudBlockedTuple(u32AppID,(tenAppCategory)(u32AppCat));
            }//if(true == bApplyAudblocking(u32

         }//if ( (true == bApplyBlocking) && 
      }//for(int i=0;i<u16Extension_size;i++)

      //bPerformDuck flag will be set to TRUE, only when the MIX Channel is ACTIVE & 
      //audio stream which needs to be ducked is available in RTP Packet.
      vSetAudioDucking(bPerformDuck);

      //When SPI_ENTERTAIN is Active, if we need to enable SPI_TRANSIENT, don't request for deactivation of 
      //SPI_ENTERTAIN. This source needs to be resumed after SPI_TRANSIENT is over. So just request for
      //Activation of SPI_TRANSIENT. This is taken care in Config file.

      //if RTP Stream is not available from the Phone, deallocate the current active source.
      if( false == bCurStreamActive )
      {
         vRequestAVDeact(enGetCurAudioDir());
      }//if((false == bCurStreamActive)

      //if a RTP stream which has to be played on high priority source is available, request  
      // for the activation of required source.
      if(e8AUD_INVALID != enNewAudioDir)
      {
         vRequestAVAct(enNewAudioDir,e8AUD_SAMPLERATE_DEFAULT);
      }//if(e8AUD_INVALID != enNewAudioDir)

   }//if(true == bMLDynAudioSupported)
   //This gets executed, when Mirror link Dynamic audio feature is disabled in Policy and the 
   //Global Mute is enabled by HMI/Project Adaptation Layer
   else if((true == m_bAudBlockingBasedOnAGlobalMute) && (true == m_bGlobalMuteEnabled))
   {
      //RTP Header extension is received in Little Endian format. Change it to Big Endian and use it.
      for(t_U16 u16Index=0; u16Index < u16RTPOutDatalen; u16Index += 2)
      {
         t_U32 u32AppID = u32ChangeEndian(cpu32RTPOutData[u16Index]); 
         t_U32 u32AppCat = u32ChangeEndian(cpu32RTPOutData[u16Index+1]);

         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,
            "AppId-0x%x AppCat-0x%x",u32AppID,u32AppCat));

         //When the Global Mute is enabled, MLC needs to send audio blocking notification
         //For all the apps, from which audio is available. So send request for all the active apps.
         if(true == bApplyAudblocking(u32AppID,e16MLGLOBALLY_MUTED))
         {
            vStoreAudBlockedTuple(u32AppID,(tenAppCategory)(u32AppCat));
         }//if(true == bApplyAudblocking(u32

      }//for(t_U16 u16Index=0;u16Index<u16RTPOutDatalen/2;u16Index++)
   }//else if(true == m_bGlobalMuteEnabled)

}

/***************************************************************************
** FUNCTION: t_U32 spi_tclMLAudio::u32ChangeEndian(t_U32 u32Val)
***************************************************************************/
t_U32 spi_tclMLAudio::u32ChangeEndian(t_U32 u32Val)
{
   return ( ( u32Val >> 24 ) | ((u32Val << 8) & 0x00ff0000 )|((u32Val >> 8) & 0x0000ff00) | ( u32Val << 24) );
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLAudio::bEvalRTPStream(...)
***************************************************************************/
t_Bool spi_tclMLAudio::bEvalRTPStream(t_U32 u32AppCat,
                                      t_Bool& rfbApplyBlocking, 
                                      tenAudioDir& rfenAudDir,
                                      t_Bool& rfbActiveStreamAvail,
                                      t_Bool& rfbPerformDuck)
{

   t_Bool bRet = true; // unused for now

   t_U16 u16Length = sizeof(scoarMLAudioMixing)/sizeof(trMLAudioMixing);
   for(t_U16 u16Ind=0;u16Ind<u16Length;u16Ind++)
   {
      if( (enGetCurAudioDir() == scoarMLAudioMixing[u16Ind].enActiveAudioDir)
         &&(static_cast<tenAppCategory>(u32AppCat) == scoarMLAudioMixing[u16Ind].enAppCategory) )
      {
         rfbApplyBlocking = scoarMLAudioMixing[u16Ind].bApplyAudioBlocking;

         /*Audio sources Priority Order*/
         //e8AUD_TRANSIENT - 1st
         //e8AUD_STEREO_MIX_OUT  - 2nd
         //e8AUD_MAIN_OUT  - 3rd 
         if((true == scoarMLAudioMixing[u16Ind].bChangeAudioSrc) && (e8AUD_TRANSIENT != rfenAudDir))
         {
            //Currently SPI is not requesting to enable SPI_ENTERTAIN, depends on the audio content played.
            //So, If not TRANSIENT, we would request for STEREO MIX Channel only.
            rfenAudDir = scoarMLAudioMixing[u16Ind].enNewAudioDir;
         }//if(true == scoarMLAudioMixing[u16Ind].bChangeAudioSrc)

         if( (true != rfbActiveStreamAvail) && (true == scoarMLAudioMixing[u16Ind].bActiveStreamAvailable))
         {
            rfbActiveStreamAvail = true;
         }//if( (true != bCurStreamActive) && (true 

         //if a RTP Packet contains two different streams, which has to be ducked and which doesn't need to be ducked,
         //preference is given to Duck.
         if(true != rfbPerformDuck)
         {
            rfbPerformDuck = scoarMLAudioMixing[u16Ind].bPerformAudDuck;
         }//if(true != rfbPerformDuck)

         bRet = true;

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,"MLAudio: Element found in the table:"
            "CurAudDir-%d AppCat-0x%x ActiveStreamAvail-%d Perform Duck-%d",
            ETG_ENUM(AUDIO_DIRECTION,enGetCurAudioDir()),u32AppCat,ETG_ENUM(BOOL,rfbActiveStreamAvail),
            ETG_ENUM(BOOL,rfbPerformDuck)));

      }//if( (enGetCurAudioDir() == scoarMLAudioMixing[u16Ind].enActiveAudioDir)
   }//for(t_U16 u16Ind=0;u16Ind<u16Length;u16Ind++)

   return bRet;
}

/***************************************************************************
** FUNCTION: tenAudioDir spi_tclMLAudio::enGetCurAudioDir()
***************************************************************************/
tenAudioDir spi_tclMLAudio::enGetCurAudioDir()
{
   m_oCurAudDirLock.s16Lock();
   tenAudioDir enAudDir = m_enCurAudDir;
   m_oCurAudDirLock.vUnlock();

   return enAudDir;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vSetCurAudioDir(tenAudioDir enAudioDir)
***************************************************************************/
t_Void spi_tclMLAudio::vSetCurAudioDir(tenAudioDir enAudioDir)
{
   m_oCurAudDirLock.s16Lock();
   m_enCurAudDir = enAudioDir;
   m_oCurAudDirLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_U32 spi_tclMLAudio::u32GetActiveDev()
***************************************************************************/
t_U32 spi_tclMLAudio::u32GetActiveDev()
{
   m_oActDevLock.s16Lock();
   t_U32 u32DevID = m_u32ActiveDevice ;
   m_oActDevLock.vUnlock();
   return u32DevID;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vSetActiveDev(t_U32 u32DevID)
***************************************************************************/
t_Void spi_tclMLAudio::vSetActiveDev(t_U32 u32DevID)
{
   m_oActDevLock.s16Lock();
   m_u32ActiveDevice = u32DevID ;
   m_oActDevLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vRequestAVDeact(tenAudioDir enAudDir)
***************************************************************************/
t_Void spi_tclMLAudio::vRequestAVDeact(tenAudioDir enAudDir)
{
   tenAudioChannelStatus enChannelStatus = enGetAudioChannelStatus(enAudDir);

   if( 
      (e8AUD_INVALID != enAudDir) &&
      (e8_AUD_DEACT_REQUESTED != enChannelStatus) && 
      (e8_AUD_NOT_ACTIVE != enChannelStatus) && 
      (NULL != m_rAudCallbacks.fvTerminateAudioReq) )
   {
      ETG_TRACE_USR1(("MLAudio: Request for Deactivation of audio channel -%d",
         ETG_ENUM(AUDIO_DIRECTION,enAudDir)));

      vSetAudioChannelStatus(enAudDir,e8_AUD_DEACT_REQUESTED);

      (m_rAudCallbacks.fvTerminateAudioReq)(u32GetActiveDev(),enAudDir);
   }//if((false == bCurStreamActive)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vRequestAVAct(...)
***************************************************************************/
t_Void spi_tclMLAudio::vRequestAVAct(tenAudioDir enAudDir,
                                     tenAudioSamplingRate enSamplingRate)
{
   tenAudioChannelStatus enChannelStatus = enGetAudioChannelStatus(enAudDir);

   if(  
      (e8AUD_INVALID != enAudDir) &&
      (e8_AUD_ACT_REQUESTED != enChannelStatus) &&
      (e8_AUD_ACT_GRANTED != enChannelStatus) &&
      (NULL != m_rAudCallbacks.fvLaunchAudioReq) )
   {
      ETG_TRACE_USR1(("MLAudio: Request for Activation of audio channel -%d",
         ETG_ENUM(AUDIO_DIRECTION,enAudDir)));

      vSetAudioChannelStatus(enAudDir,e8_AUD_ACT_REQUESTED);

      (m_rAudCallbacks.fvLaunchAudioReq)(u32GetActiveDev(),e8DEV_TYPE_MIRRORLINK,
         enAudDir, enSamplingRate);

   }//if(e8AUD_INVALID != enNewAudioDir)
}

/***************************************************************************
** FUNCTION: tenAudioChannelStatus spi_tclMLAudio::enGetAudioChannelStatus(..)
***************************************************************************/
tenAudioChannelStatus spi_tclMLAudio::enGetAudioChannelStatus(tenAudioDir enAudDir)
{
   tenAudioChannelStatus enChannelStatus = e8_AUD_NOT_ACTIVE;

   m_AudChannelStatusLock.s16Lock();
   enChannelStatus = m_aeAudChannelStatus[enAudDir];
   m_AudChannelStatusLock.vUnlock();

   return enChannelStatus;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vSetAudioChannelStatus(...)
***************************************************************************/
t_Void spi_tclMLAudio::vSetAudioChannelStatus(tenAudioDir enAudDir,
                                              tenAudioChannelStatus enAudChannelStatus)
{
   m_AudChannelStatusLock.s16Lock();
   m_aeAudChannelStatus[enAudDir]=enAudChannelStatus;
   m_AudChannelStatusLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLAudio::bApplyAudblocking(...)
***************************************************************************/
t_Bool spi_tclMLAudio::bApplyAudblocking(t_U32 u32AppId, 
                                         tenMLAudioBlockReason enAudioBlockReason)
{

   t_Bool bRet = false;

   if (NULL != m_poCmdAudio) 
   {
      //Allow audio blocking for only,ML1.1 or above devices.Behavior is inconsistent with ML1.0 Phones
      trVersionInfo rVersionInfo;
      m_poCmdAudio->vFetchDeviceVersion(u32GetActiveDev(),rVersionInfo);

      if(true == bIsML11OrAbove(rVersionInfo.u32MajorVersion,rVersionInfo.u32MinorVersion))
      {
         spi_tclMLVncManager* poVNCMngr = spi_tclMLVncManager::getInstance();
         if (NULL != poVNCMngr)
         {
            spi_tclMLVncCmdViewer* poMLViewerCmd = poVNCMngr->poGetViewerInstance();
            if (NULL != poMLViewerCmd)
            {
               ETG_TRACE_USR4(("MLAudio: Apply Audio Blocking - AppId-0x%x, Blocking Reason-%d",
                  u32AppId, enAudioBlockReason));

               bRet = poMLViewerCmd->bTriggerAudioBlockingNotification(u32AppId, 
                  enAudioBlockReason, u32GetActiveDev());
            }//if(NULL != poMLViewerCmd)
         }//if(NULL != poVNCMngr)
      }//if(true == bIsML11OrAbove(r
      else
      {
         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,"MLAudio:Apply Audio Blocking: ML1.0 Device - Blocking not supported"));
      }
   }

   return bRet;

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vStoreAudBlockedTuple(...)
***************************************************************************/
t_Void spi_tclMLAudio::vStoreAudBlockedTuple(t_U32 u32AppID, tenAppCategory enAppCat)
{
   m_vecAudBlockedAppListLock.s16Lock();

   t_Bool bEntryExists = false;

   for(t_U8 u8Index=0;u8Index<m_vecAudBlockedAppList.size();u8Index++)
   {
      if( (m_vecAudBlockedAppList[u8Index].enAppCat == enAppCat) && 
         (m_vecAudBlockedAppList[u8Index].u32AppID == u32AppID) )
      {
         bEntryExists = true;
         break;
      }
   }//for(t_U8 u8Index=0;u8Index<m_vec
   
   //This entry is not available in the list. Store it.
   if(false == bEntryExists)
   {
      ETG_TRACE_USR4(("MLAudio: Blocking is applied for"
         "App-0x%x AppCat-0x%x", u32AppID,enAppCat));

      trAudBlockingTuple rAudBlockingTuple;
      rAudBlockingTuple.u32AppID = u32AppID;
      rAudBlockingTuple.enAppCat = enAppCat;

      m_vecAudBlockedAppList.push_back(rAudBlockingTuple);
   }//if(true == bEntryExists)

   m_vecAudBlockedAppListLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vUnBlockBlockedApps(...)
***************************************************************************/
t_Void spi_tclMLAudio::vUnBlockBlockedApps()
{
   // Method to unblock the blocked audio depends on the Active Source.
   //Iterate the through the list. If that particular Application category is allowed to play with the
   //current audio source, unblock it.

   m_vecAudBlockedAppListLock.s16Lock();

   for(t_U8 u8Index=0;u8Index<m_vecAudBlockedAppList.size();u8Index++)
   {
      t_U16 u16Length = sizeof(scoarMLAudioMixing)/sizeof(trMLAudioMixing);
      for(t_U16 u16Ind=0;u16Ind<u16Length;u16Ind++)
      {
         if( (enGetCurAudioDir() == scoarMLAudioMixing[u16Ind].enActiveAudioDir)
            &&(m_vecAudBlockedAppList[u8Index].enAppCat == scoarMLAudioMixing[u16Ind].enAppCategory)&&
            (false == scoarMLAudioMixing[u16Ind].bApplyAudioBlocking))
         {
            //This Application category is allowed to use in the current audio source.
            //unblock it.
            bApplyAudblocking(m_vecAudBlockedAppList[u8Index].u32AppID,e16MLAUDIO_UNBLOCK);
            m_vecAudBlockedAppList.erase(m_vecAudBlockedAppList.begin()+u8Index);
         }//if( (enGetCurAudioDir() == scoar
      }//for(t_U16 u16Ind=0;u16Ind<u1
   }//for(t_U8 u8Index=0;u8Index<m_vecAudBlockedAppList.s

   m_vecAudBlockedAppListLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLAudio::vSetAudioDucking(...)
***************************************************************************/
t_Void spi_tclMLAudio::vSetAudioDucking(t_Bool bEnableDucking)
{

   if(m_bAudDuckEnabled != bEnableDucking)
   {
      tenDuckingType enDuckType = (true == bEnableDucking)? e8_DUCKINGTYPE_DUCK : e8_DUCKINGTYPE_UNDUCK;

      ETG_TRACE_USR1(("MLAudio: Sending request to enable Audio Ducking - %d",
         ETG_ENUM(BOOL,bEnableDucking)));

      /*lint -esym(40,fbSetAudioDucking)fbSetAudioDucking Undeclared identifier */
      //Send request for Audio Ducking
      if (NULL != m_rAudCallbacks.fbSetAudioDucking)
      {
         (m_rAudCallbacks.fbSetAudioDucking)(0,0,enDuckType,e8DEV_TYPE_MIRRORLINK);
      }//if (NULL != m_rAudCallbacks.fbSetAudioDucking)

      m_bAudDuckEnabled = bEnableDucking ;

   }//if(m_bAudDuckEnabled != bEnableDucking)
}
//lint –restore

///////////////////////////////////////////////////EOF///////////////////////////////////////////////////////////////
