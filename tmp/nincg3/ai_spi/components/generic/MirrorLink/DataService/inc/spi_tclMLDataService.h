/*!
 *******************************************************************************
 * \file             spi_tclMLDataService.h
 * \brief            ML Data Service class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Mirrorlink Data Service class implements Data Service Info Management for
 Mirrorlink capable devices. This class must be derived from base Data Service class.
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      		| Modifications
 27.03.2014 |  Ramya Murthy                		| Initial Version
 13.06.2014 |  Ramya Murthy                		| Implementation for VDSensor data integration
 05.02.2015 |  Ramya Murthy                		| Changes to start CDB services based on availability of LocationData
 23.04.2015 |  Ramya Murthy                | Added interface to set region code

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMLLOCATION_H_
#define SPI_TCLMLLOCATION_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclDataServiceDevBase.h"
#include "spi_tclMLVncRespDiscoverer.h"
#include "spi_tclMLVncRespCDB.h"
#include "spi_tclDataServiceTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
struct trMLSelectedDeviceInfo
{
   t_U32 u32DeviceHandle;
   t_U32 u32CdbAppID;

   trMLSelectedDeviceInfo():
      u32DeviceHandle(0), u32CdbAppID(0)
   {
   }
};

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */
class spi_tclMLVncCmdCDB;

/*!
 * \class spi_tclMLDataService
 * \brief Mirrorlink Connection class implements Data Service Info Management for
 *        Mirrorlink capable devices. This class must be derived from base
 *        Data Service class.
 */

class spi_tclMLDataService :
   public spi_tclDataServiceDevBase,  //! Base Connection Class
   public spi_tclMLVncRespDiscoverer, //! Vnc Response class used for retrieving CDB Protocol ID
   public spi_tclMLVncRespCDB //! Vnc Response class used for retrieving location data notification request
{
public:
   /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclMLDataService::spi_tclMLDataService(const trDataServiceCb...))
    ***************************************************************************/
   /*!
    * \fn     spi_tclMLDataService(const trDataServiceCb& rfcorDataServiceCb)
    * \brief  Parameterised Constructor
    * \param  rfcorDataServiceCb: [IN] Structure containing callbacks to
    *            DataService Manager.
    * \sa     ~spi_tclMLDataService()
    **************************************************************************/
   spi_tclMLDataService(const trDataServiceCb& rfcorDataServiceCb);

   /***************************************************************************
    ** FUNCTION:  spi_tclMLDataService::~spi_tclMLDataService
    ***************************************************************************/
   /*!
    * \fn     ~spi_tclMLDataService()
    * \brief  Destructor
    * \sa     spi_tclMLDataService()
    **************************************************************************/
   virtual ~spi_tclMLDataService();


   /***** Start of Methods overridden from spi_tclDataServiceDevBase *********/

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLDataService::bInitialise();
   ***************************************************************************/
   /*!
   * \fn      bInitialise()
   * \brief   Method to initialises the service handler. (Performs initialisations which
   *          are not device specific.)
   *          Mandatory interface to be implemented.
   * \retval  t_Bool: TRUE - If ServiceHandler is initialised successfully, else FALSE.
   * \sa      bUninitialise()
   ***************************************************************************/
   virtual t_Bool bInitialise();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLDataService::bUninitialise();
   ***************************************************************************/
   /*!
   * \fn      bUninitialise()
   * \brief   Method to uninitialise the service handler.
   *          Mandatory interface to be implemented.
   * \retval  t_Bool: TRUE - If ServiceHandler is uninitialised successfully, else FALSE.
   * \sa      bInitialise()
   ***************************************************************************/
   virtual t_Bool bUninitialise();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLDataService::vOnSelectDeviceResult(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vOnSelectDeviceResult(t_U32 u32DeviceHandle)
    * \brief   Called when a device is selected.
    *          Mandatory interface to be implemented.
    * \param   [IN] u32DeviceHandle: Unique handle of selected device
    * \retval  None
    * \sa      vOnDeselectDevice()
    **************************************************************************/
   virtual t_Void vOnSelectDeviceResult(t_U32 u32DeviceHandle);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLDataService::vOnDeselectDeviceResult(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vOnDeselectDeviceResult(t_U32 u32DeviceHandle)
    * \brief   Called when currently selected device is de-selected.
    *          Mandatory interface to be implemented.
    * \param   [IN] u32DeviceHandle: Unique handle of selected device
    * \retval  None
    * \sa      vOnSelectDevice()
    **************************************************************************/
   virtual t_Void vOnDeselectDeviceResult(t_U32 u32DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLDataService::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   *          Optional interface to be implemented.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLDataService::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLDataService::vSetLocDataAvailablility(const trDataServiceConfigData& rfrDataServiceConfigInfo)
   ***************************************************************************/
   /*!
   * \fn      vSetLocDataAvailablility(const trDataServiceConfigData& rfrDataServiceConfigInfo)
   * \brief   Interface to set the availability of LocationData
   * \param   [IN] bLocDataAvailable:
   *              If true - Location data is available
   *              If false - Location data is unavailable
   * \param   [IN] bIsDeadReckonedData:
   *              True - if Location data is dead reckoned data, else False
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSensorDataAvailablility(const trDataServiceConfigData& rfrDataServiceConfigInfo);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLDataService::vSetRegion(...)
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetRegion(tenRegion enRegion)
   * \brief  Interface to set the current region
   * \param  [IN] enRegion : Region enumeration
   * \sa
   **************************************************************************/
   virtual t_Void vSetRegion(tenRegion enRegion);

   /******* End of Methods overridden from spi_tclDataServiceDevBase *********/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /********* Start of Methods overridden from spi_tclMLVncRespCDB ***********/

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclMLDataService::vPostSetLocDataSubscription(...)
    ***************************************************************************/
   /*!
    * \fn      vPostSetLocDataSubscription(t_Bool bSubscriptionOn)
    * \brief   Method to subscribe/unsubscribe for location data information.
    * \param   bSubscriptionOn: [IN] Indicates if location data should be
    *             subscribed/unsubscribed.
    * \retval  None
    **************************************************************************/
   virtual t_Void vPostSetLocDataSubscription(t_Bool bSubscriptionOn);

   /********* End of Methods overridden from spi_tclMLVncRespCDB *************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLDataService::vStartDataServices();
   ***************************************************************************/
   /*!
   * \fn      vStartDataServices()
   * \brief   Method to start the data services provided by the DataService handler
   *          for currently selected device.
   *          NOTE: This can only be called after DataService handler is initialised via
   *          the interface bInitialise().
   *          Optional interface to be implemented.
   * \param   None
   * \retval  None
   * \sa      vStopDataServices()
   ***************************************************************************/
   virtual t_Void vStartDataServices();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLDataService::vStopDataServices();
   ***************************************************************************/
   /*!
   * \fn      vStopDataServices()
   * \brief   Method to stop the data services provided by the DataService handler
   *          for currently selected device.
   *          NOTE: After this method is called, DataService handler can be uninitialised via
   *          the interface bUninitialise().
   *          Optional interface to be implemented.
   * \param   None
   * \retval  None
   * \sa      vStartDataServices()
   ***************************************************************************/
   virtual t_Void vStopDataServices();


   /***************************************************************************
    ** Data Members
    ***************************************************************************/

   /***************************************************************************
    ** VNC CDB Command class pointer
    ***************************************************************************/
   spi_tclMLVncCmdCDB*        m_poCmdCdb;

   /***************************************************************************
    ** Selected device's information
    ***************************************************************************/
   trMLSelectedDeviceInfo     m_rSelDevInfo;

   /***************************************************************************
    ** DataService callbacks structure
    ***************************************************************************/
   trDataServiceCb            m_rDataServiceCb;

   /***************************************************************************
    ** DataService callbacks structure
    ***************************************************************************/
   t_Bool                     m_bLocDataAvailable;

   /***************************************************************************
    ** Current region
    ***************************************************************************/
   tenRegion                  m_enCurRegion;

};
#endif // SPI_TCLMLLOCATION_H_
