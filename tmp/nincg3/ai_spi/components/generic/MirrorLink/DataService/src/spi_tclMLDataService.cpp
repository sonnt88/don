/*!
 *******************************************************************************
 * \file             spi_tclMLDataService.h
 * \brief            ML Data Service class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Mirrorlink Data Service class implements Data Service Info Management for
 Mirrorlink capable devices. This class must be derived from base Data Service class.
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.03.2014 |  Ramya Murthy                | Initial Version
 13.06.2014 |  Ramya Murthy                | Implementation for VDSensor data integration
 05.02.2015 |  Ramya Murthy                | Changes to start CDB services based on availability of LocationData
 23.04.2015 |  Ramya Murthy                | Implementation to block CDB services for China region
 21.05.2015 |  Ramya Murthy                | Implementation to readg CDB services support from policy 

 \endverbatim
 ******************************************************************************/
#include "spi_tclMLVncCmdCDB.h"
#include "spi_tclMLVncManager.h"
#include "spi_tclDataServiceSettings.h"
#include "spi_tclMLDataService.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DATASERVICE
      #include "trcGenProj/Header/spi_tclMLDataService.cpp.trc.h"
   #endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_String coszCDBAppProtocolID = "CDB";
static const trMLSelectedDeviceInfo corEmptyDeviceInfo;

/****************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMLDataService::spi_tclMLDataService
 ***************************************************************************/
spi_tclMLDataService::spi_tclMLDataService(const trDataServiceCb& rfcorDataServiceCb)
   : m_poCmdCdb(NULL),
     m_rDataServiceCb(rfcorDataServiceCb),
     m_bLocDataAvailable(true),
     m_enCurRegion(e8_INVALID)
{
   ETG_TRACE_USR1(("spi_tclMLDataService() entered "));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLDataService::spi_tclMLDataService
 ***************************************************************************/
spi_tclMLDataService::~spi_tclMLDataService()
{
   ETG_TRACE_USR1(("~spi_tclMLDataService() entered "));
   m_poCmdCdb = NULL;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLDataService::bInitialise()
 ***************************************************************************/
t_Bool spi_tclMLDataService::bInitialise()
{
   ETG_TRACE_USR1(("spi_tclMLDataService::bInitialise() entered "));

   //! If data service is not initialised, perform initializations which are
   // not device dependent. (e.g, Initialise CDB SDK)
   t_Bool bInitSuccess = false;
   spi_tclMLVncManager* poVncManager = spi_tclMLVncManager::getInstance();

   if (
      (NULL != poVncManager)
      &&
      (true == poVncManager->bRegisterObject((spi_tclMLVncRespDiscoverer*) this))
      &&
      (true == poVncManager->bRegisterObject((spi_tclMLVncRespCDB*) this))
      )
   {
      m_poCmdCdb = poVncManager->poGetCDBInstance();

      if (NULL != m_poCmdCdb)
      {
         bInitSuccess = m_poCmdCdb->bInitialiseCDBSDK();

         //! Read the settings from policy
         spi_tclDataServiceSettings* poDataSvcSettings = spi_tclDataServiceSettings::getInstance();
         if (NULL != poDataSvcSettings)
         {
            std::vector<trCdbServiceConfig> CdbSvcConfigList;
            trCdbServiceConfig rCdbSvcConfig;

            rCdbSvcConfig.enServiceType = e8CDB_GPS_SERVICE;
            rCdbSvcConfig.bServiceEnabled = poDataSvcSettings->bGetCdbGPSSeviceEnabled();
            CdbSvcConfigList.push_back(rCdbSvcConfig);

            rCdbSvcConfig.enServiceType = e8CDB_LOC_SERVICE;
            rCdbSvcConfig.bServiceEnabled = poDataSvcSettings->bGetCdbLocationSeviceEnabled();
            CdbSvcConfigList.push_back(rCdbSvcConfig);

            m_poCmdCdb->vSetServicesList(CdbSvcConfigList);
         }//if (NULL != poDataSvcSettings)
      }//if (NULL != m_poCmdCdb)
   }// if (NULL != pVncManager)

   ETG_TRACE_USR2(("bInitialise() left with result = %u ", bInitSuccess));
   return bInitSuccess;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLDataService::bUninitialise()
 ***************************************************************************/
t_Bool spi_tclMLDataService::bUninitialise()
{
   ETG_TRACE_USR1(("spi_tclMLDataService::bUninitialise() entered "));

   //! Uninitialise CDB SDK
   if (NULL != m_poCmdCdb)
   {
      m_poCmdCdb->vUninitialiseCDBSDK();
   }
   return true;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLDataService::vOnSelectDeviceResult(t_U32...)
 ***************************************************************************/
t_Void spi_tclMLDataService::vOnSelectDeviceResult(t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLDataService::vOnSelectDeviceResult() entered: u32DeviceHandle = %u ", u32DeviceHandle));

   if (e8_CHN != m_enCurRegion)
   {
      //! Clear Selected device's info & update the newly selected device's handle.
      m_rSelDevInfo = corEmptyDeviceInfo;
      m_rSelDevInfo.u32DeviceHandle = u32DeviceHandle;

      //! Start CDB services if location data is available
      if (true == m_bLocDataAvailable)
      {
         t_Bool bIsCDBSupported =  false;
         //! Trigger request to Discoverer to get the device's CDB App ProtocolID
         spi_tclMLVncManager* poVncManager = spi_tclMLVncManager::getInstance();
         if (NULL != poVncManager)
         {
            spi_tclMLVncCmdDiscoverer* poVncDisc = poVncManager->poGetDiscovererInstance();
            if (NULL != poVncDisc)
            {
               bIsCDBSupported = poVncDisc->bGetAppIDforProtocol(u32DeviceHandle,
                     coszCDBAppProtocolID, m_rSelDevInfo.u32CdbAppID);
            } //if (NULL != poVncDisc)
         } //if (NULL != poVncManager)

         //! Store CDB Application ID of currently Selected Device & Start data service
         if (true == bIsCDBSupported)
         {
            vStartDataServices();
            vPostSetLocDataSubscription(true);
         }
      }//if (true == m_bLocDataAvailable)
   }
   else
   {
      ETG_TRACE_USR4(("vOnSelectDeviceResult(): Unsupported region. Nothing to be done "));
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLDataService::vOnDeselectDeviceResult(t_U32...)
 ***************************************************************************/
t_Void spi_tclMLDataService::vOnDeselectDeviceResult(t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLDataService::vOnDeselectDeviceResult() entered: u32DeviceHandle = %u ", u32DeviceHandle));

   if (e8_CHN != m_enCurRegion)
   {
      //! Stop data services (if started)
      if ((true == m_bLocDataAvailable) && (u32DeviceHandle == m_rSelDevInfo.u32DeviceHandle))
      {
         vStopDataServices();

         //! Clear Selected device's info
         m_rSelDevInfo = corEmptyDeviceInfo;

         //! Unsubscribe for location data (required here for proper clean-up)
         vPostSetLocDataSubscription(false);
      }//if ((true == m_bLocDataAvailable) && (u32DeviceHandle == m_rSelDevInfo.u32DeviceHandle))
   }
   else
   {
      ETG_TRACE_USR4(("vOnDeselectDeviceResult(): Unsupported region. Nothing to be done "));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLDataService::vOnData(const trGPSData& rfcorGpsData)
***************************************************************************/
t_Void spi_tclMLDataService::vOnData(const trGPSData& rfcorGpsData)
{
   //! Forward data to CmdCDB class
   if (NULL != m_poCmdCdb)
   {
      m_poCmdCdb->vOnData(rfcorGpsData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLDataService::vOnData(const trSensorData& rfcorSensorData)
***************************************************************************/
t_Void spi_tclMLDataService::vOnData(const trSensorData& rfcorSensorData)
{
   //! Forward data to CmdCDB class
   if (NULL != m_poCmdCdb)
   {
      m_poCmdCdb->vOnData(rfcorSensorData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLDataService::vSetLocDataAvailablility(t_Bool...)
***************************************************************************/
t_Void spi_tclMLDataService::vSetSensorDataAvailablility(const trDataServiceConfigData& rfrDataServiceConfigInfo)
{
    ETG_TRACE_USR1(("spi_tclMLDataService::vSetLocDataAvailablility() entered: bLocDataAvailable = %d ",
         ETG_ENUM(BOOL, rfrDataServiceConfigInfo.bLocDataAvailable)));
    m_bLocDataAvailable = rfrDataServiceConfigInfo.bLocDataAvailable;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDataService::vSetRegion(...)
***************************************************************************/
t_Void spi_tclMLDataService::vSetRegion(tenRegion enRegion)
{
    m_enCurRegion = enRegion;
}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLDataService::vStartDataServices();
***************************************************************************/
t_Void spi_tclMLDataService::vStartDataServices()
{
   ETG_TRACE_USR1(("spi_tclMLDataService::vStartDataServices() entered "));

   //! Launch CDB application on selected device.
   if (
      (NULL != m_poCmdCdb)
      &&
      (0 != m_rSelDevInfo.u32DeviceHandle)
      &&
      (0 != m_rSelDevInfo.u32CdbAppID)
      )
   {
      m_poCmdCdb->vLaunchCDBApp(m_rSelDevInfo.u32DeviceHandle, m_rSelDevInfo.u32CdbAppID);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLDataService::vStopDataServices();
***************************************************************************/
t_Void spi_tclMLDataService::vStopDataServices()
{
   ETG_TRACE_USR1(("spi_tclMLDataService::vStopDataServices() entered "));

   //! Terminate CDB application on selected device.
   if (
      (NULL != m_poCmdCdb)
      &&
      (0 != m_rSelDevInfo.u32DeviceHandle)
      )
   {
      m_poCmdCdb->vTerminateCDBApp(m_rSelDevInfo.u32DeviceHandle);
   }
}

/********* Start of Methods overridden from spi_tclMLVncRespCDB ***********/

/***************************************************************************
 ** FUNCTION:  virtual spi_tclMLDataService::vPostSetLocDataSubscription(...)
 ***************************************************************************/
t_Void spi_tclMLDataService::vPostSetLocDataSubscription(t_Bool bSubscriptionOn)
{

	/*lint -esym(40,fvSubscribeForLocationData)fvSubscribeForLocationData Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMLDataService::vPostSetLocDataSubscription() entered: bSubscriptionOn = %u ",
         ETG_ENUM(BOOL, bSubscriptionOn)));

   //! Forward LocationData subscription/unsubscription request to DataService manager
   if (NULL != m_rDataServiceCb.fvSubscribeForLocationData)
   {
      m_rDataServiceCb.fvSubscribeForLocationData(bSubscriptionOn, e8GPS_DATA);
      //@Note: Currently only GPS data is being subscribed to (since only
      //GGA & RMC sentences are supported).
      //In future if DeadReckoning data is required, will need to subscribe for it.
   }
}
/********* End of Methods overridden from spi_tclMLVncRespCDB *************/

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
