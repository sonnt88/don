  /*!
 *******************************************************************************
 * \file         spi_tclMLBluetooth.cpp
 * \brief        MirrorLink Bluetooth class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    MirrorLink Bluetooth handler class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 05.02.2015 |  Ramya Murthy (RBEI/ECP2)         | Changes to prevent SwitchDevice during ongoing selection 
                                                  and BT disconnection after ML activation.
28.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclBluetoothIntf.h"
#include "spi_tclBluetoothPolicyBase.h"
#include "spi_tclMLBluetooth.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_BLUETOOTH
      #include "trcGenProj/Header/spi_tclMLBluetooth.cpp.trc.h"
   #endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLBluetooth::spi_tclMLBluetooth();
***************************************************************************/
spi_tclMLBluetooth::spi_tclMLBluetooth(spi_tclBluetoothIntf* poBTInterface,
      spi_tclBluetoothPolicyBase* poBTPolicyBase)
      : m_cpoBTInterface(poBTInterface),
        m_cpoBTPolicyBase(poBTPolicyBase)
{
   ETG_TRACE_USR1(("spi_tclMLBluetooth() entered "));
   SPI_NORMAL_ASSERT(NULL == m_cpoBTInterface);
   SPI_NORMAL_ASSERT(NULL == m_cpoBTPolicyBase);
} //!end of spi_tclMLBluetooth()

/***************************************************************************
** FUNCTION:  spi_tclMLBluetooth::~spi_tclMLBluetooth();
***************************************************************************/
spi_tclMLBluetooth::~spi_tclMLBluetooth()
{
   ETG_TRACE_USR1(("~spi_tclMLBluetooth() entered "));
} //!end of ~spi_tclMLBluetooth()

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vOnSPISelectDeviceRequest(t_U32...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle)
{
   ETG_TRACE_USR1(("spi_tclMLBluetooth::vOnSPISelectDeviceRequest() entered "));

   vRegisterBTCallbacks();

   //! If any BT device is connected, disconnect it if it is NOT the ML device.
   if (NULL != m_cpoBTInterface)
   {
      t_Bool bIsBTDisconnReq = bValidateBTDisconnectionRequired();

      if ((true == bIsBTDisconnReq) && (NULL != m_cpoBTPolicyBase))
      {
         //Fetch BT DeviceHandle of connected device
         t_String szConnBTDevAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();
         t_U32 u32ConnBTDevHandle = m_cpoBTPolicyBase->u32GetBTDeviceHandle(szConnBTDevAddr);
         ETG_TRACE_USR4((" vOnSPISelectDeviceRequest: Retrieved connected BT DeviceHandle = 0x%x (for BTAddress = %s) ",
                  u32ConnBTDevHandle, szConnBTDevAddr.c_str()));

         //Fetch Call Status of connected Device
         t_Bool bCallActive = m_cpoBTInterface->bGetCallStatus();

         //! Notify HMI about switch from BT to Projection device.
         //! Store info about device change.
         m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SWITCH_IN_PROGRESS);
         //@Note: Since BT connection is allowed on ML device, device switch is only triggered
         //when BT & ML devices are different.
         m_cpoBTInterface->vSendDeviceSwitchEvent(u32ConnBTDevHandle, u32ProjectionDevHandle,
               e8SWITCH_BT_TO_ML, false, bCallActive);

      }//if (true == bValidateBTDisconnectionRequired())
      else if (false == bIsBTDisconnReq)
      {
         //! Set Device status - this is in order to prevent SwitchDevice during ongoing selection
         m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SELECTION_IN_PROGRESS);
         //! If BT processing is complete (i.e. BT disconnection not required),
         //! send success result for SelectDevice
         m_cpoBTInterface->vSendSelectDeviceResult(true);
      }
   }//if (NULL != m_cpoBTInterface)

} //!end of vOnSPISelectDeviceRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vOnSPISelectDeviceResponse(t_U32...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
      tenResponseCode enRespCode)
{
   ETG_TRACE_USR1(("spi_tclMLBluetooth::vOnSPISelectDeviceResponse() entered "));
SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle)
   if (NULL != m_cpoBTInterface)
   {
      //! Clear device status
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);

      //! If SelectDevice operation is successful, proceed with BT Connection of ML device.
      if (e8SUCCESS == enRespCode)
      {
         //! Check if any other BT device is connected & disconnect it.
         //! Else try to establish BT connection for ML device.
         //! @Note: Other BT may be connected at this stage if the BT device was connected
         //! after ML selection is started.
         t_Bool bIsBTDisconnReq = bValidateBTDisconnectionRequired();

         //! If any other BT device is connected, trigger disconnection
         if ((true == bIsBTDisconnReq) && (false == bTriggerBTDeviceDisconnection()))
         {
            ETG_TRACE_ERR((" Sending BT disconn request failed! Hence deactivating ML device. "));
            m_cpoBTInterface->vSendDeselectDeviceRequest();
         }
         //! Else initiate BT connection of ML device.
         else if (false == bIsBTDisconnReq)
         {
            bTriggerBTDeviceConnection();
         }
      }//if (e8SUCCESS == enRespCode)
   }//if (NULL != m_cpoBTInterface)
} //!end of vOnSPISelectDeviceResponse()

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vOnSPISelectDeviceRequest(t_U32...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vOnSPIDeselectDeviceRequest(t_U32 u32ProjectionDevHandle)
{
   ETG_TRACE_USR1(("spi_tclMLBluetooth::vOnSPIDeselectDeviceRequest() entered"));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);
   if (NULL != m_cpoBTInterface)
   {
      // Set Device status - this is in order to prevent SwitchDevice during ongoing deselection
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_DESELECTION_IN_PROGRESS);
      // Nothing else to be done. Simply send success result.
      m_cpoBTInterface->vSendSelectDeviceResult(true);
   }
} //!end of vOnSPIDeselectDeviceRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vOnSPISelectDeviceResponse(t_U32...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vOnSPIDeselectDeviceResponse(t_U32 u32ProjectionDevHandle,
      tenResponseCode enRespCode,
      t_Bool bIsDeviceSwitch)
{
   SPI_INTENTIONALLY_UNUSED(enRespCode);
   ETG_TRACE_USR1(("spi_tclMLBluetooth::vOnSPIDeselectDeviceResponse() entered"));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);
   SPI_INTENTIONALLY_UNUSED(bIsDeviceSwitch);

   // Re-enable BT A2DP profile
   if (NULL != m_cpoBTPolicyBase)
   {
      m_cpoBTPolicyBase->vSetBTProfile(e8A2DP_ENABLED);
   }
   // Clear device status
   if (NULL != m_cpoBTInterface)
   {
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
   }
} //!end of vOnSPIDeselectDeviceResponse()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLBluetooth::bCheckBTPairingRequired()
***************************************************************************/
t_Bool spi_tclMLBluetooth::bCheckBTPairingRequired()
{
   //! BT pairing is required for ML device since BT connection is allowed on same device.
   t_Bool bPairingRequired = false;

   if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))
   {
      //! Get pairing status.
      //! If device is already paired (pairing status = true), Pairing is not required.
      bPairingRequired = !(m_cpoBTPolicyBase->bGetPairingStatus(
            m_cpoBTInterface->szGetSelectedDevBTAddress()));
   }
   ETG_TRACE_USR1(("spi_tclMLBluetooth::bCheckBTPairingRequired() left with bPairingRequired = %d ",
         ETG_ENUM(BOOL, bPairingRequired)));
   return bPairingRequired;
} //!end of bCheckBTPairingRequired()

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vOnBTDeviceSwitchAction(t_U32...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
      t_U32 u32ProjectionDevHandle,
      tenBTChangeInfo enInitialBTChange,
      tenBTChangeInfo enFinalBTChange)
{
   ETG_TRACE_USR1(("spi_tclMLBluetooth::vOnInvokeBTDeviceAction() entered: "));
   ETG_TRACE_USR4((" vOnInvokeBTDeviceAction: BT DevHandle = 0x%x, Projection DevHandle = 0x%x ",
         u32BluetoothDevHandle, u32ProjectionDevHandle));
   ETG_TRACE_USR4((" vOnInvokeBTDeviceAction: InitialBTChange = %d, enFinalBTChange = %d ",
         ETG_ENUM(BT_CHANGE_INFO, enInitialBTChange), ETG_ENUM(BT_CHANGE_INFO, enFinalBTChange)));

   //! Handle requested device switch action only if device change is in progress
   //! (i.e. if SPI had previously sent a device switch event)
   if (
      (NULL != m_cpoBTInterface)
      &&
      (e8DEVICE_SWITCH_IN_PROGRESS == m_cpoBTInterface->enGetSelectedDevStatus())
      )
   {
      //! Handle switch from BT device to ML device.
      //! @Note: When switch from BT to ML device is triggered, if user selects to:
      //! a) Switch to ML device - BT device should be disconnected in order to
      //!    continue with ML device activation.
      //! b) Cancel switch operation - ML device selection should be canceled, and
      //!    BT connection should remain unchanged.

      if (e8SWITCH_BT_TO_ML == enInitialBTChange)
      {
         vHandleBTtoMLSwitch(enFinalBTChange);
      }//if (e8SWITCH_BT_TO_ML == enInitialBTChange)
      else if (e8SWITCH_ML_TO_BT == enInitialBTChange)
      {
         vHandleMLtoBTSwitch(enFinalBTChange);
      }//else if (e8SWITCH_BT_TO_ML == enInitialBTChange)
   } //if ((NULL != m_cpoBTInterface)&&...)
   else
   {
      ETG_TRACE_ERR((" vOnInvokeBTDeviceAction: Null ptr/Device Switch not in progress. "));
   }
}

/***************************************************************************
*********************************PROTECTED**********************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vOnBTConnectionResult(...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vOnBTConnectionResult(t_String szBTDeviceAddress,
      tenBTConnectionResult enBTConnResult)
{
   ETG_TRACE_USR1(("spi_tclMLBluetooth::vOnBTConnectionResult() entered: BTConnectionResult = %d, BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult), szBTDeviceAddress.c_str()));

   //! @Note: BT Device connection/disconnection result will be received only for
   //! an SPI-triggered BT device connection/disconnection.

   if (NULL != m_cpoBTInterface)
   {
      tenDeviceStatus enSelectedDevStatus = m_cpoBTInterface->enGetSelectedDevStatus();

      //! Handle BT connection/disconnection result only if
      //! a Projection device is active OR being activated.
      if (IS_VALID_DEVHANDLE(m_cpoBTInterface->u32GetSelectedDevHandle()))
      {
         switch (enBTConnResult)
         {
            //@Note - connection result not used. Code retained for future use.
            /*case e8BT_RESULT_CONNECTED:
            {
               ETG_TRACE_USR1((" ML device successfully BT connected. "));
               //@TODO: Forward result to Audio
            }
               break;
            case e8BT_RESULT_CONN_FAILED:
            {
               ETG_TRACE_USR1((" ML device BT connection failed! "));
               //@TODO: Forward result to Audio
            }
               break;*/
            case e8BT_RESULT_DISCONNECTED:
            {
               ETG_TRACE_USR1((" BT device successfully disconnected. "));
               //! If disconnection success result is received during:
               //! 1. Selection of ML device - proceed with ML selection.
               //! 2. When user cancels switch from ML to BT device - try connecting ML via BT
               if (e8DEVICE_CHANGE_IN_PROGRESS == enSelectedDevStatus)
               {
                  //! Set Device status - this is in order to prevent SwitchDevice during ongoing selection
                  m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SELECTION_IN_PROGRESS);
                  m_cpoBTInterface->vSendSelectDeviceResult(true);
               }
               else if (e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus)
               {
                  bTriggerBTDeviceConnection();
               }
            }
               break;
            case e8BT_RESULT_DISCONN_FAILED:
            {
               ETG_TRACE_USR1((" BT device disconnection failed! "));
               //! If disconnection failure result is received during:
               //! 1. Selection of ML device - stop ML device selection.
               //! 2. When user cancels switch from ML to BT device - Deselect ML device.
               if (e8DEVICE_CHANGE_IN_PROGRESS == enSelectedDevStatus)
               {
                  m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
                  m_cpoBTInterface->vSendSelectDeviceResult(false);
               }
               else if (e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus)
               {
                  m_cpoBTInterface->vSendDeselectDeviceRequest();
               }
            }
               break;
            default:
               ETG_TRACE_ERR((" vOnBTConnectionResult: Invalid enum encountered. "));
               break;
         } //switch (enBTConnResult)
      } //if (IS_VALID_BT_ADDRESS(m_cpoBTInterface->u32GetSelectedDevHandle()))
      else
      {
         ETG_TRACE_ERR((" vOnBTConnectionResult: No device is currently active/being activated! "));
      }
   }//if (NULL != m_cpoBTInterface)
} //!end of vOnBTConnectionResponse()

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vOnBTConnectionChanged(t_String...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vOnBTConnectionChanged(t_String szBTDeviceAddress,
      tenBTConnectionResult enBTConnResult)
{
   ETG_TRACE_USR1(("spi_tclMLBluetooth::vOnBTConnectionChanged() entered: BTConnectionResult = %d, BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult), szBTDeviceAddress.c_str()));

   //! If a BT device has become connected and BT & ML devices are different,
   //! trigger device switch event to client.

   if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))
   {
      tenDeviceStatus enSelectedDevStatus = m_cpoBTInterface->enGetSelectedDevStatus();
      t_U32 u32MLDevHandle = m_cpoBTInterface->u32GetSelectedDevHandle();
      
      if (
         (e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus)
         &&
         (IS_VALID_DEVHANDLE(u32MLDevHandle))      /*If ML device is active*/
         &&
         (e8BT_RESULT_CONNECTED == enBTConnResult)    /*If a BT device has been connected*/
         &&
         (IS_VALID_BT_ADDRESS(szBTDeviceAddress))  /*If received BT device address is valid*/
         )
      {
         //@Note: For ML devices that do not provide BT address, ignore any BT connections
         //so that the ML device can be connected via BT. (Fix for GMMY16-16068)
         //Only disabling of BT A2DP profile is required in this case.

         t_String szMLDevBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();
         if (
            (IS_VALID_BT_ADDRESS(szMLDevBTAddress))   /*If ML BT device address is valid*/
            &&
            (szBTDeviceAddress != szMLDevBTAddress) /*If devices are different*/
            )
         {
            //Fetch DeviceHandle of connected BT device
            t_U32 u32ConnBTDevHandle = m_cpoBTPolicyBase->u32GetBTDeviceHandle(szBTDeviceAddress);

            //@Note: Call Status is not required to be sent for this scenario,
            //since BT is the newly connected device.
            m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SWITCH_IN_PROGRESS);
            //@Note: Since BT connection is allowed on ML device, device switch is only triggered
            //when BT & ML devices are different.
            m_cpoBTInterface->vSendDeviceSwitchEvent(u32ConnBTDevHandle, u32MLDevHandle,
                  e8SWITCH_ML_TO_BT, false, false);
         }
         else /*If devices are same*/
         {
            //Disable BT A2DP profile
            m_cpoBTPolicyBase->vSetBTProfile(e8A2DP_DISABLED);
         }
      }//if ((IS_VALID_DEVHANDLE(u32MLDevHandle) &&...)
   }//if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))

} //!end of vOnBTConnectionChanged()


/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vRegisterBTCallbacks()
***************************************************************************/
t_Void spi_tclMLBluetooth::vRegisterBTCallbacks()
{
	/*lint -esym(40,fvOnBTConnectionResult)fvOnBTConnectionResult Undeclared identifier */
	/*lint -esym(40,fvOnBTConnectionChanged)fvOnBTConnectionChanged Undeclared identifier */
	/*lint -esym(40,_1)_1 Undeclared identifier */
	/*lint -esym(40,_2)_2 Undeclared identifier */
   if (NULL != m_cpoBTPolicyBase)
   {
      //!Initialize BT response callbacks structure
      trBluetoothCallbacks rBluetoothCb;
      rBluetoothCb.fvOnBTConnectionResult =
         std::bind(&spi_tclMLBluetooth::vOnBTConnectionResult,
               this,
               SPI_FUNC_PLACEHOLDERS_2);
      rBluetoothCb.fvOnBTConnectionChanged =
         std::bind(&spi_tclMLBluetooth::vOnBTConnectionChanged,
               this,
               SPI_FUNC_PLACEHOLDERS_2);
      m_cpoBTPolicyBase->vRegisterCallbacks(rBluetoothCb);
   }//if (NULL != m_cpoBTPolicyBase)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLBluetooth::bValidateBTDisconnection()
***************************************************************************/
t_Bool spi_tclMLBluetooth::bValidateBTDisconnectionRequired()
{
   //! For ML, BT disconnection is required only if connected BT device is NOT the ML device.
   t_Bool bDisconnectionReq = false;

   if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))
   {
      //Fetch BT Address of connected BT device (if any)
      t_String szConnBTDevAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();
      ETG_TRACE_USR4((" bValidateBTDisconnectionRequired: Retrieved connected device BTAddress = %s ",
            szConnBTDevAddr.c_str()));

      //Fetch ML device BT address
      t_String szMLDevBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();

      if (
         (IS_VALID_BT_ADDRESS(szConnBTDevAddr)) /*If a BT device is connected*/
         &&
         //@Note: For ML devices that do not provide BT address, ignore any BT connections
         //so that the ML device can be connected via BT. (Fix for GMMY16-16068)
         (IS_VALID_BT_ADDRESS(szMLDevBTAddress))
         &&
         (szConnBTDevAddr != szMLDevBTAddress) /*If devices are different*/
         )
      {
         bDisconnectionReq = true;
      }
   } //if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))

   ETG_TRACE_USR2(("spi_tclMLBluetooth::bValidateBTDisconnectionRequired() left with DisconnRequired = %u ",
         ETG_ENUM(BOOL, bDisconnectionReq)));
   return bDisconnectionReq;
} //!end of bValidateBTDisconnectionRequired()

/***************************************************************************
** FUNCTION:   t_Bool spi_tclMLBluetooth::bTriggerBTDeviceDisconnection()
***************************************************************************/
t_Bool spi_tclMLBluetooth::bTriggerBTDeviceDisconnection()
{
   //! Request disconnection of currently connected BT device (if any connection exists)
   t_Bool bDisconnectTriggered = false;

   if (NULL != m_cpoBTPolicyBase)
   {
      //! Fetch BT Address of connected device
      t_String szConnBTDevAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();
      ETG_TRACE_USR4((" bTriggerBTDeviceDisconnection: Retrieved connected device BTAddress = %s ",
            szConnBTDevAddr.c_str()));

      if (IS_VALID_BT_ADDRESS(szConnBTDevAddr)) /*If a BT device is connected*/
      {
         bDisconnectTriggered = m_cpoBTPolicyBase->bDisconnectBTDevice(szConnBTDevAddr);
      }
   } //if (NULL != m_cpoBTPolicyBase)

   ETG_TRACE_USR2(("spi_tclMLBluetooth::bTriggerBTDeviceDisconnection() left with DisconnectTriggered = %u ",
         ETG_ENUM(BOOL, bDisconnectTriggered)));
   return bDisconnectTriggered;
} //!end of vTriggerBTDeviceDisconnection()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLBluetooth::bTriggerBTDeviceConnection()
***************************************************************************/
t_Bool spi_tclMLBluetooth::bTriggerBTDeviceConnection()
{
   //! Trigger Bluetooth connection for ML device, only if it is not connected.
   t_Bool bConnectTriggered = false;

   if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))
   {
      t_String szMLDevBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();
      ETG_TRACE_USR4((" bTriggerBTDeviceConnection: Retrieved ML device BTAddress = %s ",
            szMLDevBTAddress.c_str()));

      if (IS_VALID_BT_ADDRESS(szMLDevBTAddress))
      {
         //! If device is not paired, send pairing trigger
         if (false == m_cpoBTPolicyBase->bGetPairingStatus(szMLDevBTAddress))
         {
            ETG_TRACE_USR4((" Informing client to start BT pairing "));
            m_cpoBTInterface->vSendBTPairingRequired(szMLDevBTAddress);
         }
         //! If device is paired but not connected, trigger BT connection
         else if (false == m_cpoBTPolicyBase->bGetConnectionStatus(szMLDevBTAddress))
         {
            ETG_TRACE_USR4((" Attempting to establish BT connection for projection device "));
            bConnectTriggered = m_cpoBTPolicyBase->bConnectBTDevice(szMLDevBTAddress);
         }
         //! If device is paired & connected, disable A2DP profile
         else
         {
            m_cpoBTPolicyBase->vSetBTProfile(e8A2DP_DISABLED);
         }
      }
      //! If ML device BT address is invalid, only disable A2DP profile.
      else
      {
         m_cpoBTPolicyBase->vSetBTProfile(e8A2DP_DISABLED);
      }
   }//if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))

   ETG_TRACE_USR2(("spi_tclMLBluetooth::bTriggerBTDeviceConnection() left with ConnectTriggered = %u ",
         ETG_ENUM(BOOL, bConnectTriggered)));
   return bConnectTriggered;
} //!end of bTriggerBTDeviceConnection()


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vHandleBTtoMLSwitch(tenBTChangeInfo...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vHandleBTtoMLSwitch(tenBTChangeInfo enBTChangeAction)
{
   if (NULL != m_cpoBTInterface)
   {
      switch (enBTChangeAction)
      {
         case e8SWITCH_BT_TO_ML:
         {
            //! Disconnect BT device
            ETG_TRACE_USR1((" BT to ML switch - Attempting to disconnect BT device "));
            if (true == bTriggerBTDeviceDisconnection())
            {
               m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_IN_PROGRESS);
               //This status will be used to validate result of BT disconnection
               //(i.e. whether disconnection is triggered during BT to ML switch or not)
            }
            else
            {
               ETG_TRACE_ERR((" Sending BT disconn request failed! Hence stopping ML device activation. "));
               m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
               m_cpoBTInterface->vSendSelectDeviceResult(false);
            }
         }
            break;
         case e8NO_CHANGE:
         {
            //! Send SelectDevice error
            ETG_TRACE_USR1((" Cancel BT to ML switch - Stopping ML device activation "));
            m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
            m_cpoBTInterface->vSendSelectDeviceResult(false);
         }
            break;
         default:
            ETG_TRACE_ERR((" vHandleBTtoMLSwitch: Invalid user action for BT to ML switch. "));
            break;
      }//switch (enBTChangeAction)
   }//if (NULL != m_cpoBTInterface)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLBluetooth::vHandleBTtoMLSwitch(tenBTChangeInfo...)
***************************************************************************/
t_Void spi_tclMLBluetooth::vHandleMLtoBTSwitch(tenBTChangeInfo enBTChangeAction)
{
   if (NULL != m_cpoBTInterface)
   {
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);

      switch (enBTChangeAction)
      {
         case e8SWITCH_ML_TO_BT:
         {
            //! Deselect ML device
            ETG_TRACE_USR1((" ML to BT switch - Deactivating ML device "));
            m_cpoBTInterface->vSendDeselectDeviceRequest();
         }
            break;
         case e8NO_CHANGE:
         {
            //! Disconnect BT device
            ETG_TRACE_USR1((" Cancel ML to BT switch - Attempting to disconnect BT device "));
            if (false == bTriggerBTDeviceDisconnection())
            {
               ETG_TRACE_ERR((" Sending BT disconn request failed! Hence deactivating ML device. "));
               m_cpoBTInterface->vSendDeselectDeviceRequest();
            }
         }
            break;
         default:
            ETG_TRACE_ERR((" vHandleMLtoBTSwitch: Invalid user action for ML to ML switch. "));
            break;
      }//switch (enBTChangeAction)
   }//if (NULL != m_cpoBTInterface)
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
