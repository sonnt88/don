  /*!
 *******************************************************************************
 * \file         spi_tclMLBluetooth.h
 * \brief        MirrorLink Bluetooth class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    MirrorLink Bluetooth handler class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 
 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLMLBLUETOOTH_H
#define _SPI_TCLMLBLUETOOTH_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_BluetoothTypedefs.h"
#include "spi_tclBluetoothDevBase.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/* Forward Declarations. */
class spi_tclBluetoothIntf;
class spi_tclBluetoothPolicyBase;

/*!
* \class spi_tclMLBluetooth
* \brief This is the MirrorLink Bluetooth class that handles the Bluetooth
*        connection logic during a MirrorLink device session
*/
class spi_tclMLBluetooth : public spi_tclBluetoothDevBase
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLBluetooth::spi_tclMLBluetooth(...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLBluetooth(spi_tclBluetoothIntf* poBTInterface,
   *             spi_tclBluetoothPolicyBase* poBTPolicyBase)
   * \brief   Parameterized Constructor
   * \param   [IN] poBTInterface: Pointer to Bluetooth manager interface
   * \param   [IN] poBTPolicyBase: Pointer to Bluetooth policy
   ***************************************************************************/
   spi_tclMLBluetooth(spi_tclBluetoothIntf* poBTInterface,
         spi_tclBluetoothPolicyBase* poBTPolicyBase);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLBluetooth::~spi_tclMLBluetooth();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLBluetooth()
   * \brief   Virtual Destructor
   ***************************************************************************/
   virtual ~spi_tclMLBluetooth();

   /*********Start of functions overridden from spi_tclBluetoothDevBase*******/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when SelectDevice request is received by SPI.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \retval  None
   **************************************************************************/
   t_Void vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenResponseCode enRespCode)
   * \brief   Called when SelectDevice operation is complete, with the result
   *          of the operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enRespCode: Response code enumeration
   * \retval  None
   **************************************************************************/
   t_Void vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenResponseCode enRespCode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when DeselectDevice request is received by SPI.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \retval  None
   **************************************************************************/
   t_Void vOnSPIDeselectDeviceRequest(t_U32 u32ProjectionDevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenResponseCode enRespCode)
   * \brief   Called when DeselectDevice operation is complete, with the result
   *          of the operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enRespCode: Response code enumeration
   * \param   [IN] bIsDeviceSwitch: True - if a projection device switch is in progress
   * \retval  None
   **************************************************************************/
   t_Void vOnSPIDeselectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenResponseCode enRespCode,
         t_Bool bIsDeviceSwitch);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLBluetooth::bCheckBTPairingRequired()
   ***************************************************************************/
   /*!
   * \fn      bCheckBTPairingRequired()
   * \brief   Validates if BT pairing is required for current selected
   *          projection device.
   *          Mandatory interface to be implemented.
   * \retval  Bool: TRUE - if BT pairing is required, else FALSE.
   **************************************************************************/
   t_Bool bCheckBTPairingRequired();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vOnBTDeviceSwitchAction(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
   *             t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange)
   * \brief   Called with user action when there is a device switch occurring
   *          between a Projection and a BT device.
   *          Mandatory interface to be implemented.
   * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
   * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
   * \param  [IN] enBTChange  : Indicates user's device change action
   * \retval  None
   **************************************************************************/
   t_Void vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enInitialBTChange,
         tenBTChangeInfo enFinalBTChange);


   /*********End of functions overridden from spi_tclBluetoothDevBase*********/

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /*********Start of functions overridden from spi_tclBluetoothDevBase*******/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vOnBTConnectionResult(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTConnectionResult(t_String szBTDeviceAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Interface to receive result of a BT device connection/disconnection request.
   * \param   [IN] szBTDeviceAddress: Bluetooth address of device
   *          Mandatory interface to be implemented.
   * \param   [IN] enBTConnResult: Connection/Disconnection result.
   * \retval  None
   **************************************************************************/
   t_Void vOnBTConnectionResult(t_String szBTDeviceAddress,
         tenBTConnectionResult enBTConnResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vOnBTConnectionChanged(t_String...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclBluetoothDevBase(t_String szBTDeviceAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Interface to receive notification on a BT device connection/disconnection.
   *          Mandatory interface to be implemented.
   * \param   [IN] szBTDeviceAddress: Bluetooth address of device
   * \param   [IN] enBTConnResult: Connection/Disconnection result.
   * \retval  None
   **************************************************************************/
   t_Void vOnBTConnectionChanged(t_String szBTDeviceAddress,
         tenBTConnectionResult enBTConnResult);

   /*********End of functions overridden from spi_tclBluetoothDevBase*********/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
    ** FUNCTION: spi_tclMLBluetooth(const spi_tclMLBluetooth &rfcoBluetooth)
    ***************************************************************************/
   /*!
    * \fn      spi_tclMLBluetooth(const spi_tclMLBluetooth &rfcoBluetooth)
    * \brief   Copy constructor not implemented hence made private
    **************************************************************************/
   spi_tclMLBluetooth(const spi_tclMLBluetooth &rfcoBluetooth);

   /***************************************************************************
    ** FUNCTION: const spi_tclMLBluetooth & operator=(
    **                                 const spi_tclMLBluetooth &rfcoBluetooth);
    ***************************************************************************/
   /*!
    * \fn      const spi_tclMLBluetooth & operator=(const spi_tclMLBluetooth &rfcoBluetooth);
    * \brief   assignment operator not implemented hence made private
    **************************************************************************/
   const spi_tclMLBluetooth & operator=(
            const spi_tclMLBluetooth &rfcoBluetooth);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vRegisterBTCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterBTCallbacks()
   * \brief   Registers callbacks to Bluetooth client.
   * \retval  None
   **************************************************************************/
   t_Void vRegisterBTCallbacks();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLBluetooth::bValidateBTDisconnectionRequired()
   ***************************************************************************/
   /*!
   * \fn      bValidateBTDisconnectionRequired()
   * \brief   Validates if disconnection of a BT device is required.
   * \retval  Bool: TRUE - if BT disconnection is required, else FALSE.
   **************************************************************************/
   t_Bool bValidateBTDisconnectionRequired();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLBluetooth::bTriggerBTDeviceDisconnection()
   ***************************************************************************/
   /*!
   * \fn      bTriggerBTDeviceDisconnection()
   * \brief   Triggers disconnection of active BT device.
   * \retval  Bool: TRUE - if BT device disconnection is triggered, else FALSE
   **************************************************************************/
   t_Bool bTriggerBTDeviceDisconnection();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLBluetooth::bTriggerBTDeviceConnection()
   ***************************************************************************/
   /*!
   * \fn      bTriggerBTDeviceConnection()
   * \brief   Triggers BT connection of currently selected device, if selected
   *          device is a ML Device, and it is already BT paired and not connected.
   * \retval  Bool: TRUE - if BT device connection is triggered, else FALSE
   **************************************************************************/
   t_Bool bTriggerBTDeviceConnection();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vHandleBTtoMLSwitch(tenBTChangeInfo...)
   ***************************************************************************/
   /*!
   * \fn      vHandleBTtoMLSwitch(tenBTChangeInfo enBTChangeAction)
   * \brief   Implements logic to switch from BT to ML device
   * \retval  None
   **************************************************************************/
   t_Void vHandleBTtoMLSwitch(tenBTChangeInfo enBTChangeAction);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLBluetooth::vHandleBTtoMLSwitch(tenBTChangeInfo...)
   ***************************************************************************/
   /*!
   * \fn      vHandleMLtoBTSwitch(tenBTChangeInfo enBTChangeAction)
   * \brief   Implements logic to switch from ML to BT device
   * \retval  None
   **************************************************************************/
   t_Void vHandleMLtoBTSwitch(tenBTChangeInfo enBTChangeAction);

   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   /***************************************************************************
   ** BT Manager interface pointer
   ***************************************************************************/
   spi_tclBluetoothIntf* const   m_cpoBTInterface;

   /***************************************************************************
   ** BT PolicyBase pointer
   ***************************************************************************/
   spi_tclBluetoothPolicyBase* const   m_cpoBTPolicyBase;


   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

};

#endif // _SPI_TCLMLBLUETOOTH_H

