/*!
*******************************************************************************
* \file              spi_tclMLVncCmdNotif.h
* \brief             RealVNC Wrapper for Notifications
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    VNC Wrapper for wrapping VNC calls for receiving
Notifications from ML Server
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
22.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version
18.04.2013 |  Shiva Kumar Gurija          | Updated the wrapper
14.05.2014 | Shiva Kumar Gurija           | Changes in AppList Handling 
for CTS Test
24.05.2014 |  Shiva Kumar Gurija          | Changes Invoke Notification Actions

\endverbatim
******************************************************************************/

#ifndef SPI_MLVNCCMDNOTIF_H_
#define SPI_MLVNCCMDNOTIF_H_

/******************************************************************************
| includes:
| 1)RealVNC sdk - includes
| 2)Typedefines
|----------------------------------------------------------------------------*/
#include "vncdiscoverysdk.h"
#include "vncmirrorlinkkeys.h"
#include "SPITypes.h"
#include "Lock.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef enum
{
   u32GET_NOTI_SUPPORTED_APPS = 1,
   u32SET_NOTI_ALLOWED_APPS = 2,
   u32INVOKE_NOTI_ACTION = 3
} tenNotiReqID;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

//! Forward declarations
class MsgContext;
class spi_tclMLVncMsgQInterface;
/*!
* \class spi_tclMLVncCmdNotif
* \brief This class wraps the RealVNC SDK calls to SPI component
*        implement the notification service provided by MLServer.
*        This class is used by ViewerWrapper
*
*/

class spi_tclMLVncCmdNotif
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCmdNotif::spi_tclMLVncCmdNotif();
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCmdNotif()
   * \brief   Default Constructor
   * \sa      ~spi_tclMLVncCmdNotif()
   **************************************************************************/
   spi_tclMLVncCmdNotif();

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclMLVncCmdNotif::~spi_tclMLVncCmdNotif()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclMLVncCmdNotif()
   * \brief   Destructor
   * \sa      spi_tclMLVncCmdNotif()
   **************************************************************************/
   virtual ~spi_tclMLVncCmdNotif();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdNotif::bInitializeNotifications()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitializeNotifications()
   * \brief   To register for the discoverer listeners.
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bInitializeNotifications();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdNotif::vUnInitializeNotifications()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUnInitializeNotifications()
   * \brief   To Unregister for the discoverer listeners.
   * \retval  t_Void
   **************************************************************************/
   t_Void vUnInitializeNotifications();

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bSetNotiSubscription()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bSetNotiSubscription(const t_U32 cou32DevID,t_Bool bSubscription)
   * \brief   Method to subscribe or un subscribe for notifications
   * \param   cou32DevID    : [IN]  Device handle to identify the MLServer
   * \param   bSubscription : [IN] TRUE - Subscribe
   *                               FALSE - Unsubscribe
   * \retval  t_Bool :
   *        TRUE   if the request was completed successfully,
   *        FALSE  if the request was not completed successfully,
   **************************************************************************/
   t_Bool bSetNotiSubscription(const t_U32 cou32DevID,t_Bool bSubscription);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vGetNotiSupportedApps()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetNotiSupportedApps(const t_U32 cou32DevID,
   *               const trUserContext& corfrUsrCntxt)
   * \brief   Fetches the apps that support notifications from ML server.
   *          This call is asynchronous and the result is available in callback
   *          vPostNotiAllowedApps.
   * \param   cou32DevID    : [IN]  Device handle to identify the MLServer
   * \param   corfrUsrCntxt : [IN] User Context Info
   * \retval  t_Void
   * \sa      vPostNotiAllowedApps
   **************************************************************************/
   t_Void vGetNotiSupportedApps(const t_U32 cou32DevID,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bEnableAppNotifications()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bEnableAppNotifications(const t_U32 cou32DevID,
   *               const t_U16 cou16NumApps,
   *               std::vector<trNotiEnable> &rfvecrNotiEnableList,
   *               const trUserContext& corfrUsrCntxt)
   * \brief   To Enable the Notifications for a set of applications, for which the 
   *           Notifications are supported
   * \param   cou32DevID    : [IN]  Device handle to identify the MLServer
   * \param   cou16NumApps  : [IN] Number of Apps
   * \param   rfvecrNotiEnableList : [IN] List of applications
   * \param   corfrUsrCntxt : [IN] User Context Info
   * \retval  t_Bool
   * \sa     vPostSetNotiAllowedAppsResult
   **************************************************************************/
   t_Bool bEnableAppNotifications(const t_U32 cou32DevID,
      const t_U16 cou16NumApps,
      std::vector<trNotiEnable> &rfvecrNotiEnableList,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vInvokeNotiAction()
   ***************************************************************************/
   /*!
   * \fn      vInvokeNotiAction(const t_U32 cou32DevID,
   *            const t_U32 cou32NotiID, 
   *            const t_U32 cou32NotiActionID,
   *            const trUserContext& corfrUsrCntxt)
   * \brief   Passes the user action for the received notification to the server
   *          to take appropriate action.
   *          This call is asynchronous and the result is available in callback
   *          vPostTriggerNotiActionResult
   * \param   cou32DevID    : [IN]  Device handle to identify the MLServer
   * \param   cou32NotiID   : [IN] Notification identifier sent during
   *                           notification event
   * \param   cou32NotiActionID : [IN] ActionID corresponding to the user action
   *                                 on HMI
   * \param   corfrUsrCntxt : [IN]  User Context info
   * \retval  t_Void
   * \sa     vPostNotiEvent,vPostTriggerNotiActionResult
   **************************************************************************/
   t_Void vInvokeNotiAction(const t_U32 cou32DevID,
      const t_U32 cou32NotiID, 
      const t_U32 cou32NotiActionID,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vDeviceDisconnected()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDeviceDisconnected(t_U32 u32DevID);
   * \brief   Method to clear the stored notifications details on device disconnection
   * \param   u32DevID :[IN] Unique Device iD
   * \retval  t_Void
   **************************************************************************/
   t_Void vDeviceDisconnected(t_U32 u32DevID);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bIsLaunchAppReq()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bIsLaunchAppReq(t_U32 u32DevID,
   *                          t_U32 u32AppID,
   *                          t_U32 u32NotificationID,
   *                          t_U32 u32NotificationActionID)
   *            
   * \brief   Checks whether the launch app is required for this particular user action.
   * \param   u32DevID           : [IN] Device handle to identify the MLServer
   * \param   u32AppID           : [IN] ApplicationID for which Notification was available
   * \param   u32NotificationID  : [IN] Notification identifier sent during
   *                                    notification event
   * \param   u32NotificationActionID : [IN]  ActionID corresponding to the user action
   *                                     on HMI
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bIsLaunchAppReq(t_U32 u32DevID,
      t_U32 u32AppID,
      t_U32 u32NotificationID,
      t_U32 u32NotificationActionID);

   /***************************************************************************
   **************************** END OF PUBLIC*********************************
   ***************************************************************************/


protected:
   /***************************************************************************
   *********************************PROTECTED************************************
   ***************************************************************************/

   /***************************************************************************
   **************************** END OF PROTECTED*******************************
   ***************************************************************************/
private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION: spi_tclMLVncCmdNotif(const spi_tclMLVncCmdNotif &corfobjCRCBResp)
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCmdNotif(const spi_tclMLVncCmdNotif &corfobjCRCBResp)
   * \brief   Copy constructor not implemented hence made protected
   * \sa      ~spi_tclMLVncCmdNotif()
   **************************************************************************/
   spi_tclMLVncCmdNotif(const spi_tclMLVncCmdNotif &corfobjCRCBResp);

   /***************************************************************************
   ** FUNCTION: const spi_tclMLVncCmdNotif & operator=(
   **                                 const spi_tclMLVncCmdNotif &corfobjCRCBResp);
   ***************************************************************************/
   /*!
   * \fn      const spi_tclMLVncCmdNotif & operator=(const spi_tclMLVncCmdNotif &corfobjCRCBResp);
   * \brief   assignment operator not implemented hence made protected
   * \sa      ~spi_tclMLVncCmdNotif()
   **************************************************************************/
   const spi_tclMLVncCmdNotif & operator=(
      const spi_tclMLVncCmdNotif &corfobjCRCBResp);


   /***************************************************************************
   ** FUNCTION: static t_Void VNCCALL
   ** spi_tclMLVncCmdNotif::vPostEntityValueCallback()
   ***************************************************************************/
   /*!
   * \fn    vPostEntityValueCallback(t_Void *pSDKContext,
   *           VNCDiscoverySDKRequestId u32RequestId,
   *           VNCDiscoverySDKDiscoverer *pDiscoverer,
   *           VNCDiscoverySDKEntity *pEntity, t_Char *pczKey, t_Char *pczValue,
   *           VNCDiscoverySDKError s32Error)
   * \brief   RealVNC SDK callback registered to receive the response for
   *          setting a value specified during postEntityValueRespondToListener
   *          method provided by RealVNC DiscovererSDK
   * \param pSDKContext The SDK context that was set by the client application for
   *                 callbacks.
   * \param u32RequestId The ID of the request. This was given to the client
   *                  application when the application made the request
   *                  initially.
   * \param pDiscoverer The Discoverer that discovered the entity.
   * \param pEntity The entity whose value has been set.
   * \param pczKey The key that was requested. This string is owned by the client
   *             SDK once this call is made.
   * \param pczValue : Value to be set. It can be NULL
   * \param u32Error The outcome of the request: VNCDiscoverySDKErrorNone if the
   *              request was completed successfully,
   *              VNCDiscoverySDKErrorNotSupported if the key is not supported by
   *              the Entity, or is read-only, VNCDiscoverySDKErrorOutOfMemory if
   *              there is not enough memory to complete the request,
   *              VNCDiscoverySDKErrorInvalidParameter if the Discoverer, Entity
   *              or Key was NULL, or the value was set to NULL when the Entity
   *              doesn't support un-setting of the key,
   *              VNCDiscoverySDKErrorUnkownEntity if the Entity doesn't exist, or
   *              VNCDiscoverySDKErrorCancelled if the Discoverer was stopped
   *              while processing the request, or another error specific to the Discoverer.
   * \sa     vRegisterDiscoveryCallbacks()
   **************************************************************************/
   static t_Void VNCCALL vPostEntityValueCallback(t_Void *pSDKContext,
      VNCDiscoverySDKRequestId u32RequestId,
      VNCDiscoverySDKDiscoverer *pDiscoverer,
      VNCDiscoverySDKEntity *pEntity, 
      t_Char *pczKey, 
      t_Char *pczValue,
      VNCDiscoverySDKError s32Error);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif:: vFetchEntityValueCallback();
   ***************************************************************************/
   /*!
   * \fn  vFetchEntityValueCallback(t_Void *pSDKContext,
   *        VNCDiscoverySDKRequestId u32RequestId,
   *        VNCDiscoverySDKDiscoverer *pDiscoverer,
   *        VNCDiscoverySDKEntity *pEntity, t_Char *pczKey,
   *        VNCDiscoverySDKError s32Error, t_Char *pczValue);
   * \brief    RealVNC SDK callback registered to receive the response for
   *          setting a value specified during VNCDiscoverySDKFetchEntityValue
   *          method provided by RealVNC DiscovererSDK
   *
   * \param pSDKContext The SDK context that was set by the client application for
   *                 callbacks.
   * \param u32RequestId The ID of the request. This was given to the client
   *                  application when the application made the request
   *                  initially.
   * \param pDiscoverer The Discoverer that discovered the entity.
   * \param pEntity The entity whose value has been retrieved.
   * \param pczKey The key that was requested. This string is owned by the client
   *             SDK once this call is made. The string can be NULL if an Out of
   *             Memory error occurred.
   * \param u32Error The outcome of the request: VNCDiscoverySDKErrorNone if the
   *              request was completed successfully,
   *              VNCDiscoverySDKErrorNotSupported if the key is not supported by
   *              the Entity, VNCDiscoverySDKErrorOutOfMemory if there is not
   *              enough memory to complete the request,
   *              VNCDiscoverySDKErrorInvalidParameter if the Entity or Key was
   *              NULL, VNCDiscoverySDKErrorUnkownEntity if the Entity doesn't exist,
   *              VNCDiscoverySDKErrorCancelled if the Discoverer was stopped
   *              while processing the request, or another error specific to the Discoverer.
   * \param pczValue The value requested, corresponding to the key. This string is
   *               owned by the client of the SDK. This can be NULL if some error
   *               occurred.
   *
   * \see VNCDiscoverySDKFetchEntityValue
   * \sa     vRegisterDiscoveryCallbacks(),
   *          VNCDiscoverySDKFetchEntityValue
   **************************************************************************/
   static t_Void VNCCALL vFetchEntityValueCallback(t_Void *pSDKContext,
      VNCDiscoverySDKRequestId u32RequestId,
      VNCDiscoverySDKDiscoverer *pDiscoverer,
      VNCDiscoverySDKEntity *pEntity, 
      t_Char *pczKey,
      VNCDiscoverySDKError s32Error, 
      t_Char *pczValue);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif:: vEntityChangedCallback()
   ***************************************************************************/
   /*!
   * \fn   static t_Void VNCCALL vEntityChangedCallback(t_Void *pSDKContext,
   *  VNCDiscoverySDKDiscoverer *pDiscoverer,
   * VNCDiscoverySDKEntity *pEntity,
   * const t_Char *copczChangeDescription);
   * \brief   Notifies the client that an entity has changed.
   * \param pDiscoverer The Discoverer that discovered the entity.
   * \param pEntity The entity that changed.
   * \param pChangeDescription A string that describes what has changed. Can be
   * NULL. Ownership of the string stays with the SDK.
   *
   * \sa     vRegisterDiscoveryCallbacks(),
   **************************************************************************/
   static t_Void VNCCALL vEntityChangedCallback(t_Void *pSDKContext,
      VNCDiscoverySDKDiscoverer *pDiscoverer,
      VNCDiscoverySDKEntity *pEntity,
      const t_Char *copczChangeDescription);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vSendNotiSupportedApps()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSendNotiSupportedApps(const t_U32 cou32DevID,
   *           const t_Char *pcoczNotiApps,
   *           trUserContext &corfrUserContext)const
   * \brief   Method to post the Notifications supported Application List to the SPI
   * \param   cou32DevID    : [IN]  Device handle to identify the MLServer
   * \param   pcoczNotiApps : [IN] pointer to t_Character array containing semicolon
   *                         separated list of application's id's
   * \param   corfrUserContext : [IN] User context
   * \retval  t_Void
   * \sa      bPopulateVNCAppId,
   **************************************************************************/
   t_Void vSendNotiSupportedApps(const t_U32 cou32DevID,
      const t_Char *pcoczNotiApps,
      const trUserContext &corfrUserContext)const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vProcessNotiEvent()
   ***************************************************************************/
   /*!
   * \fn      vProcessNotiEvent(const t_U32 cou32DevID,t_String szNotiID)
   * \brief   Receives the Notification event from RealVNCSDK for further processing
   * \param   cou32DevID    : [IN]  Device handle to identify the MLServer
   * \param   szNotiID      : [IN]  Notification ID
   * \retval  t_Void
   * \sa      bPopulateNotiIconDetails,bPopulateNotiActionDetails,
   *          bPopulateNotiActionIconDetails
   **************************************************************************/
   t_Void vProcessNotiEvent(const t_U32 cou32DevID,t_String szNotiID);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPopulateVNCAppId()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bPopulateVNCAppId(const t_U32 cou32DevID,t_String& rfszNotiAllowedApps,
   *             const tvecMLApplist &corfvecApplist)
   * \brief   converts vector of strings to string of semicolon
   *          separated list of app id to
   * \param   cou32DevID : [IN] Device ID
   * \param   rfszNotiAllowedApps  : [OUT] comma separated list of applications
   * \param   corfvecApplist : [IN] App list received from HMI
   * \sa      bPopulateMLAppId
   **************************************************************************/
   t_Void vPopulateVNCAppId(const t_U32 cou32DevID,t_String& rfszNotiAllowedApps,
      const tvecMLApplist &corfvecApplist);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPopulateNotiIconDetails()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bPopulateNotiIconDetails(VNCDiscoverySDK* poDiscSDK,
   *                 VNCDiscoverySDKDiscoverer* poDisc, 
   *                 VNCDiscoverySDKEntity* poEntity,
   *                 const t_Char *pcoczNotiID,
   *                 trIconAttributes &rfrNotiIconDetails, 
   *                 t_U32 u32Icon)
   * \brief   Method to fetch and populate the  Notifications Icons attributes
   * \param   poDiscSDK   : [IN] Discovery SDK pointer
   * \param   poDisc      : [IN] Discoverer POinter
   * \param   poEntity    : [IN] Entity Pointer
   * \param   pcoczNotiID : [IN] Array of Notification ID
   * \param   rfrNotiIconDetails: [OUT] Notification Icon details for the present
   *                            Notification event.
   * \param   u32Icon     : [IN] Icon ID of the Notification Icons
   * \retval  t_Bool : True on Successful population else false
   * \sa     bPopulateNotiActionDetails,
   *         bPopulateNotiActionIconDetails
   **************************************************************************/
   t_Bool bPopulateNotiIconDetails(VNCDiscoverySDK* poDiscSDK,
      VNCDiscoverySDKDiscoverer* poDisc, 
      VNCDiscoverySDKEntity* poEntity,
      const t_Char *pcoczNotiID,
      trIconAttributes &rfrNotiIconDetails, 
      t_U32 u32IconCount);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPopulateNotiActionDetails()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bPopulateNotiActionDetails(VNCDiscoverySDK* poDiscSDK,
   *                 VNCDiscoverySDKDiscoverer* poDisc, 
   *                 VNCDiscoverySDKEntity* poEntity,
   *                 const t_Char *pcoczNotiID,
   *                 trNotiAction &rfrNotiActionDetails, 
   *                 t_U32 u32NotiAction,,
   *                 t_String& rfszNotiActionID)
   * \brief   Method to fetch and populate the  Notifications Icons attributes
   * \param   poDiscSDK   : [IN] Discovery SDK pointer
   * \param   poDisc      : [IN] Discoverer POinter
   * \param   poEntity    : [IN] Entity Pointer
   * \param   pcoczNotiID : [IN] Array of Notification ID
   * \param   rfrNotiActionDetails : [OUT] Notification Acion details of Notification
   * \param   u32NotiAction : [IN] Notification Action ID
   * \param   rfszNotiActionID : [OUT] Actual Notification Action ID
   * \retval  t_Bool : True on Successful population else false
   * \sa     bPopulateNotiIconDetails,
   *         bPopulateNotiActionIconDetails
   **************************************************************************/
   t_Bool bPopulateNotiActionDetails( VNCDiscoverySDK* poDiscSDK,
      VNCDiscoverySDKDiscoverer* poDisc, 
      VNCDiscoverySDKEntity* poEntity,
      const t_Char *pcoczNotiID,
      trNotiAction &rfrNotiActionDetails, 
      t_U32 u32NotiAction,
      t_String& rfszNotiActionID);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPopulateNotiActionIconDetails()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bPopulateNotiActionIconDetails(VNCDiscoverySDK* poDiscSDK,
   *                 VNCDiscoverySDKDiscoverer* poDisc, 
   *                 VNCDiscoverySDKEntity* poEntity,
   *                 const t_Char *pcoczNotiID,
   *                 trIconAttributes &rfrNotiActionIconDetails, 
   *                 t_U32 u32NotiAction,
   *                 t_U32 u32NotiActionIcon)
   * \brief   populate notification action details received from MLServer
   * \param   poDiscSDK   : [IN] Discovery SDK pointer
   * \param   poDisc      : [IN] Discoverer POinter
   * \param   poEntity    : [IN] Entity Pointer
   * \param   pcoczNotiID : [IN] Array of Notification ID
   * \param   rfrNotiActionIconDetails: [OUT] Notification Action Icon details 
   * \param   u32NotiAction : [IN] Notification Action ID
   * \param   u32NotiActionIcon : [IN] Icon ID of the current notification action ID
   * \retval  t_Bool : True on Successful population else false
   * \sa      bPopulateNotiIconDetails,
   *          bPopulateNotiActionDetails
   **************************************************************************/
   t_Bool bPopulateNotiActionIconDetails(VNCDiscoverySDK* poDiscSDK,
      VNCDiscoverySDKDiscoverer* poDisc, 
      VNCDiscoverySDKEntity* poEntity,
      const t_Char *pcoczNotiID,
      trIconAttributes &rfrNotiActionIconDetails, 
      t_U32 u32NotiAction,
      t_U32 u32NotiActionIcon);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bFetchEntityValueBlocking()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bFetchEntityValueBlocking(VNCDiscoverySDK* poDiscSDK,
   *                 VNCDiscoverySDKDiscoverer* poDisc, 
   *                 VNCDiscoverySDKEntity* poEntity,
   *                 const t_Char* pcocKey, 
   *                 t_String& rfrVal,
   *                 MLDiscoverySDKTimeoutMicroseconds s32TimeOut)const
   * \brief   Method to fetch the value 
   * \param   poDiscSDK   : [IN] Discovery SDK pointer
   * \param   poDisc      : [IN] Discoverer POinter
   * \param   poEntity    : [IN] Entity Pointer
   * \param   pcocKey     : [IN] Key to fetch the value from the SDK
   * \param   rfrVal      : [OUT] Return value of the SDK
   * \param   s32TimeOut  : [IN] Time out to SDK
   * \retval  t_Bool : True on Successful population else false
   **************************************************************************/
   t_Bool bFetchEntityValueBlocking(VNCDiscoverySDK* poDiscSDK,
      VNCDiscoverySDKDiscoverer* poDisc, 
      VNCDiscoverySDKEntity* poEntity,
      const t_Char* pcocKey, 
      t_String& rfrVal,
      MLDiscoverySDKTimeoutMicroseconds s32TimeOut=cs32MLDiscoveryInstantCall)const;

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPostEntityValueBlocking()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bPostEntityValueBlocking(const t_U32 cou32DevID,
   *                 const t_Char* pcocKey, 
   *                 const t_Char* pcocValue,
   *                 MLDiscoverySDKTimeoutMicroseconds s32TimeOut)const
   * \brief   Method to fetch the value 
   * \param   cou32DevID  : [IN] Device ID
   * \param   pcocKey     : [IN] Key to fetch the value from the SDK
   * \param   pcocValue   : [IN] Value to be set the for the Key
   * \param   s32TimeOut  : [IN] Time out to SDK
   * \retval  t_Bool : True on Successful population else false
   **************************************************************************/
   t_Bool bPostEntityValueBlocking(const t_U32 cou32DevID,
      const t_Char* pcocKey, 
      const t_Char* pcocValue,
      MLDiscoverySDKTimeoutMicroseconds s32TimeOut=cs32MLDiscoveryInstantCall)const;

   /***************************************************************************
   ** FUNCTION: t_U32 spi_tclMLVncCmdNotif::u32GenerateUniqueId()
   ***************************************************************************/
   /*!
   * \fn      t_U32 u32GenerateUniqueId(t_String szUniqueId)const
   * \brief   Method to generate the unique ID using CRC
   * \param   szUniqueId :[IN] App ID in string format
   * \retval  t_U32
   **************************************************************************/
   t_U32 u32GenerateUniqueId(t_String szUniqueId)const;

   /***************************************************************************
   ** FUNCTION: t_U16 spi_tclMLVncCmdNotif::u16GenerateUniqueId()
   ***************************************************************************/
   /*!
   * \fn      t_U6 u16GenerateUniqueId(t_String szUniqueId)const
   * \brief   Method to generate the unique ID using CRC
   * \param   szUniqueId :[IN] App ID in string format
   * \retval  t_U16
   **************************************************************************/
   t_U16 u16GenerateUniqueId(t_String szUniqueId)const;

   /***************************************************************************
   ** FUNCTION: t_String spi_tclMLVncCmdNotif::szGetNotiID()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetNotiID(t_U32 u32DevID,t_U32 u32NotiID)
   * \brief   Method to get the actual notification id received from the 
   *           phone using generated ID
   * \param   u32DevID :[IN] Unique Device iD
   * \param   u32NotiID :[IN] Unique Notification ID (generated)
   * \retval  t_String
   **************************************************************************/
   t_String szGetNotiID(t_U32 u32DevID,t_U32 u32NotiID);

   /***************************************************************************
   ** FUNCTION: t_U32 spi_tclMLVncCmdNotif::u32GetNotiID()
   ***************************************************************************/
   /*!
   * \fn      t_U32 u32GetNotiID(t_U32 u32DevID,t_String szNotiID)
   * \brief   Method to get the generated Noti ID using the  string recieved from 
   *           phone and to store it internally
   * \param   u32DevID :[IN] Unique Device iD
   * \param   szNotiID :[IN] Unique Notification ID received from Phone
   * \retval  t_U32
   **************************************************************************/
   t_U32 u32GetNotiID(t_U32 u32DevID,t_String szNotiID);

   /***************************************************************************
   ** FUNCTION: t_String spi_tclMLVncCmdNotif::szGetNotiActionID()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetNotiActionID(t_U32 u32DevID,t_U32 u32NotiID,
   *            u32NotiActionID)
   * \brief   Method to get the actual notification action id received from the 
   *           phone using generated ID
   * \param   u32DevID :[IN] Unique Device iD
   * \param   u32NotiID :[IN] Unique Notification ID (generated)
   * \param   u32NotiActionID : [IN] Unique Notification Action ID (generated)
   * \retval  t_String
   **************************************************************************/
   t_String szGetNotiActionID(t_U32 u32DevID, t_U32 u32NotiID, 
      t_U32 u32NotiActionID);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vInsertNotiActionIDs()
   ***************************************************************************/
   /*!
   * \fn      t_Void vInsertNotiActionIDs(t_U32 u32DevID,t_U32 u32NotiID, 
   *            const std::map<t_U32, t_String>& corfmapNotiActionIDs);
   * \brief   Method to store notification actions details internally
   * \param   u32DevID :[IN] Unique Device iD
   * \param   u32NotiID :[IN] Unique Notification ID (generated)
   * \param   rfmapNotiActionIDs : [IN] Notification action supported by the notification
   * \retval  t_Void
   **************************************************************************/
   t_Void vInsertNotiActionIDs(t_U32 u32DevID, t_U32 u32NotiID, 
      const std::map<t_U32, t_String>& corfmapNotiActionIDs);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vClearNotiActionInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vClearNotiActionInfo(t_U32 u32DevID);
   * \brief   Method to clear the notification actions of a notification
   * \param   u32DevID :[IN] Unique Device iD
   * \param   u32NotiID :[IN] Unique Notification ID (generated)
   * \retval  t_Void
   **************************************************************************/
   t_Void vClearNotiActionInfo(t_U32 u32DevID,t_U32 u32NotiID);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vClearNotiInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vClearNotiInfo(t_U32 u32DevID);
   * \brief   Method to clear the notification details of a notification
   * \param   u32DevID :[IN] Unique Device iD
   * \param   u32NotiID :[IN] Unique Notification ID (generated)
   * \retval  t_Void
   **************************************************************************/
   t_Void vClearNotiInfo(t_U32 u32DevID, t_U32 u32NotiID);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vClearDevNotiActionInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vClearDevNotiActionInfo(t_U32 u32DevID);
   * \brief   Method to clear the notification action details of a device
   * \param   u32DevID :[IN] Unique Device iD
   * \retval  t_Void
   **************************************************************************/
   t_Void vClearDevNotiActionInfo(t_U32 u32DevID);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdNotif::vClearDevNotiInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vClearDevNotiInfo(t_U32 u32DevID);
   * \brief   Method to clear the notification details of a device
   * \param   u32DevID :[IN] Unique Device iD
   * \retval  t_Void
   **************************************************************************/
   t_Void vClearDevNotiInfo(t_U32 u32DevID);

   //! member variable to store the listener ID 
   t_U32 m_u32ListnerID;

   //! message context utility
   static MsgContext m_oMsgContext;

   static spi_tclMLVncCmdNotif* m_poVncCmdNoti;

   //To store the Notifications details supported by all the devices. 
   std::map< t_U32, std::map<t_U32, t_String> > m_mapNotiDetails;

   //To store the notification action details of each notification came from the device.
   std::map< std::pair<t_U32,t_U32>, std::map<t_U32, t_String> > m_mapNotiActionDetails;

   //Lock variable to protect Notification ID info
   Lock m_oLock_NotiID;

   //Lock variable to protect Notification Action ID info
   Lock m_oLock_NotiActionID;

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/
};

#endif /* SPI_MLVNCCMDNOTIF_H_ */

///////////////////////////////////////////////////////////////////////////////
// <EOF>