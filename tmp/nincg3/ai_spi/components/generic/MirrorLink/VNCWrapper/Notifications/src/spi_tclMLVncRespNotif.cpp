/*!
*******************************************************************************
* \file              spi_tclMLVncRespNotif.h
* \brief             RealVNC Wrapper Response class
*******************************************************************************
\verbatim
PROJECT:       Gen3
SW-COMPONENT:  Smart Phone Integration
DESCRIPTION:   VNC Notifications Response interface
COPYRIGHT:     &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
30.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version
18.04.2013 |  Shiva Kumar Gurija          | Updated the wrapper

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/

#include "SPITypes.h"
#include "spi_tclMLVncRespNotif.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
#include "trcGenProj/Header/spi_tclMLVncRespNotif.cpp.trc.h"
#endif
#endif

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncRespNotif::spi_tclMLVncRespNotif();
***************************************************************************/
spi_tclMLVncRespNotif::spi_tclMLVncRespNotif() :
RespBase(e16NOTIFICATIONS_REGID)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespNotif::spi_tclMLVncRespNotif() Entered \n "));
}

/***************************************************************************
** FUNCTION:  virtual spi_tclMLVncRespNotif::~spi_tclMLVncRespNotif()
***************************************************************************/
spi_tclMLVncRespNotif::~spi_tclMLVncRespNotif()
{
   ETG_TRACE_USR1(("spi_tclMLVncRespNotif::~spi_tclMLVncRespNotif() Entered \n "));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncRespNotif::vPostNotiAllowedApps()
***************************************************************************/
t_Void spi_tclMLVncRespNotif::vPostNotiAllowedApps(trUserContext rUserContext,
                                                   const t_U32 cou32DeviceHandle,
                                                   tvecMLApplist &vecApplist)
{
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespNotif::vPostNotiAllowedApps-0x%x\n",cou32DeviceHandle));
   ETG_TRACE_USR4(("Notification allowed apps : "));
   for (t_U32 u32AppCount = 0; u32AppCount < vecApplist.size(); u32AppCount++)
   {
      ETG_TRACE_USR4(("%d \t", vecApplist.at(u32AppCount)));
   }//for (t_U32 u32AppCount = 0; u32AppCo
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncRespNotif::vPostSetNotiAllowedAppsResult()
***************************************************************************/
t_Void spi_tclMLVncRespNotif::vPostSetNotiAllowedAppsResult(trUserContext rUserContext,
                                                            const t_U32 cou32DeviceHandle, 
                                                            t_Bool bSetNotiAppRes)
{
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespNotif::vPostSetNotiAllowedAppsResult-0x%x, Response-%d\n",
      cou32DeviceHandle,bSetNotiAppRes));

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncRespNotif::vPostInvokeNotiActionResult()
***************************************************************************/
t_Void spi_tclMLVncRespNotif::vPostInvokeNotiActionResult(trUserContext rUserContext,
                                                          const t_U32 cou32DeviceHandle,
                                                          t_Bool bNotiActionRes)
{
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespNotif::vPostInvokeNotiActionResult-0x%x,Response-%d\n",
      cou32DeviceHandle,bNotiActionRes));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncRespNotif::vPostNotiEvent()
***************************************************************************/
t_Void spi_tclMLVncRespNotif::vPostNotiEvent(const t_U32 cou32DeviceHandle,
                                             const trNotiData &rfrNotiDetails)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespNotif::vPostNotiEvent-0x%x\n",cou32DeviceHandle));
   ETG_TRACE_USR4(("**** Notification Details **** \n"));
   ETG_TRACE_USR4(("NotiID = %d \n NotiTitle = %s \n", rfrNotiDetails.u32NotiID, rfrNotiDetails.szNotiTitle.c_str()));
   ETG_TRACE_USR4((" NotiBody = %s \n", rfrNotiDetails.szNotiBody.c_str()));
   ETG_TRACE_USR4(("NotiAppID = %d, NotiIconCount = %d", rfrNotiDetails.u32NotiAppID, rfrNotiDetails.u32NotiIconCount ));
   ETG_TRACE_USR4(("NotiActionCount = %d \n",  rfrNotiDetails.tvecNotiActionList.size()));
}

