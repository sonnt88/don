/***********************************************************************/
/*!
 * \file  spi_tclMLVncNotifDispatcher.cpp
 * \brief Message Dispatcher for Notification Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Notification Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 18.04.2013  | Shiva Kumar Gurija    | Updated the wrapper

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/

#include "RespRegister.h"
#include "StringHandler.h"
#include "spi_tclMLVncNotifDispatcher.h"
#include "spi_tclMLVncRespNotif.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
#include "trcGenProj/Header/spi_tclMLVncNotifDispatcher.cpp.trc.h"
#endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleNotiMsg(this);                   \
   }                                                        \
   vDeAllocateMsg();                                        \
}                                                           \

/***************************************************************************
 ** FUNCTION:  MLNotifMsgBase::MLNotifMsgBase
 ***************************************************************************/
MLNotifMsgBase::MLNotifMsgBase()
{
   vSetServiceID( e32MODULEID_VNCNOTIFICATIONS);
}
/***************************************************************************
 ** FUNCTION:  MLNotiApplistMsg::MLNotiApplistMsg
 ***************************************************************************/
MLNotiApplistMsg::MLNotiApplistMsg(): u32DeviceHandle(0), pvecNotiAppList(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLNotiApplistMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLNotiApplistMsg, spi_tclMLVncNotifDispatcher)

/***************************************************************************
 ** FUNCTION:  MLNotiApplistMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLNotiApplistMsg::vAllocateMsg()
{
   pvecNotiAppList = new std::vector<t_U32>;
   SPI_NORMAL_ASSERT(NULL == pvecNotiAppList);
}

/***************************************************************************
 ** FUNCTION:  MLNotiApplistMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLNotiApplistMsg::vDeAllocateMsg()
{
   RELEASE_MEM(pvecNotiAppList);
}

/***************************************************************************
 ** FUNCTION:  MLSetNotiAppsErrorMsg::MLSetNotiAppsErrorMsg
 ***************************************************************************/
MLSetNotiAppsErrorMsg::MLSetNotiAppsErrorMsg():bSetNotiAppRes(false),u32DeviceHandle(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLSetNotiAppsErrorMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLSetNotiAppsErrorMsg, spi_tclMLVncNotifDispatcher)

/***************************************************************************
 ** FUNCTION:  MLNotiActionRespMsg::MLNotiActionRespMsg
 ***************************************************************************/
MLNotiActionRespMsg::MLNotiActionRespMsg():bNotiActionRes(false),u32DeviceHandle(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLNotiActionRespMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLNotiActionRespMsg, spi_tclMLVncNotifDispatcher)

/***************************************************************************
 ** FUNCTION:  MLNotiDataMsg::MLNotiDataMsg
 ***************************************************************************/
MLNotiDataMsg::MLNotiDataMsg():prNotiData(NULL), u32DeviceHandle(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLNotiDataMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLNotiDataMsg, spi_tclMLVncNotifDispatcher)

/***************************************************************************
 ** FUNCTION:  MLNotiDataMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLNotiDataMsg::vAllocateMsg()
{
   prNotiData = new trNotiData;
   SPI_NORMAL_ASSERT(NULL == prNotiData);
}

/***************************************************************************
 ** FUNCTION:  MLNotiDataMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLNotiDataMsg::vDeAllocateMsg()
{
   RELEASE_MEM(prNotiData);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncNotifDispatcher::spi_tclMLVncNotifDispatcher
 ***************************************************************************/
spi_tclMLVncNotifDispatcher::spi_tclMLVncNotifDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncNotifDispatcher::~spi_tclMLVncNotifDispatcher
 ***************************************************************************/
spi_tclMLVncNotifDispatcher::~spi_tclMLVncNotifDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleNotiMsg(MLNotiApplistMsg* poApplistMsg)
 ***************************************************************************/
t_Void spi_tclMLVncNotifDispatcher::vHandleNotiMsg(
         MLNotiApplistMsg* poApplistMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poApplistMsg) && (NULL != poApplistMsg->pvecNotiAppList))
   {
      trUserContext rUsrCntxt;
      poApplistMsg->vGetUserContext(rUsrCntxt);

      CALL_REG_OBJECTS(spi_tclMLVncRespNotif,
               e16NOTIFICATIONS_REGID,
               vPostNotiAllowedApps(rUsrCntxt,poApplistMsg->u32DeviceHandle,
                        *(poApplistMsg->pvecNotiAppList)));

   } //  if (NULL != poApplistMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleNotiMsg(MLSetNotiAppsErrorMsg* poSetNotiAppsErrorMsg)
 ***************************************************************************/
t_Void spi_tclMLVncNotifDispatcher::vHandleNotiMsg(
         MLSetNotiAppsErrorMsg* poSetNotiAppsErrorMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poSetNotiAppsErrorMsg)
   {
      trUserContext rUserContext;
      poSetNotiAppsErrorMsg->vGetUserContext(rUserContext);
      t_Bool bSetNotiAppRes = poSetNotiAppsErrorMsg->bSetNotiAppRes;
      CALL_REG_OBJECTS(spi_tclMLVncRespNotif,
               e16NOTIFICATIONS_REGID,
               vPostSetNotiAllowedAppsResult(rUserContext,
               poSetNotiAppsErrorMsg->u32DeviceHandle,
               bSetNotiAppRes));
   } //  if (NULL != poSetNotiAppsErrorMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleNotiMsg(MLNotiActionRespMsg* poNotiActionRespMsg)
 ***************************************************************************/
t_Void spi_tclMLVncNotifDispatcher::vHandleNotiMsg(
         MLNotiActionRespMsg* poNotiActionRespMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poNotiActionRespMsg)
   {
      trUserContext rUserContext;
      poNotiActionRespMsg->vGetUserContext(rUserContext);
      t_Bool bNotiActionRes = poNotiActionRespMsg->bNotiActionRes;
      CALL_REG_OBJECTS(spi_tclMLVncRespNotif,
               e16NOTIFICATIONS_REGID,
               vPostInvokeNotiActionResult(rUserContext,
               poNotiActionRespMsg->u32DeviceHandle,
               bNotiActionRes));
   } //  if (NULL != poNotiActionRespMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleNotiMsg(MLNotiDataMsg* poNotiDataMsg)
 ***************************************************************************/
t_Void spi_tclMLVncNotifDispatcher::vHandleNotiMsg(MLNotiDataMsg* poNotiDataMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poNotiDataMsg) && (NULL != poNotiDataMsg->prNotiData))
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespNotif,
               e16NOTIFICATIONS_REGID,
               vPostNotiEvent(poNotiDataMsg->u32DeviceHandle,
              *(poNotiDataMsg->prNotiData)));
   } //  if (NULL != poNotiDataMsg)
}
