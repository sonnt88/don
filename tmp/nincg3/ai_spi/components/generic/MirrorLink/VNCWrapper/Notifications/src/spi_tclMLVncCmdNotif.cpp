/*!
*******************************************************************************
* \file              spi_tclMLVncCmdNotif.cpp
* \brief            RealVNC Wrapper for Notifications
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    VNC Wrapper for wrapping VNC calls for receiving
Notifications from ML Server
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
22.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version
18.04.2013 |  Shiva Kumar Gurija          | Updated the wrapper
14.05.2014 |  Shiva Kumar Gurija          | Changes in AppList Handling 
24.05.2014 |  Shiva Kumar Gurija          | Changes Invoke Notification Actions 
13.11.2014 |  Shiva Kumar Gurija          | Fix for Suzuki-20039
                                            for CTS Test
06.05.2015  |Tejaswini HB                 |Lint Fix
27.05.2015 | Shiva Kumar Gurija           | Lint Fix

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

/** Utilities **/
#include<sstream>
#include<string>
#include <algorithm>
#include "SPITypes.h"
#include "StringHandler.h"
#include "MsgContext.h"
#include "crc.h"

/** Specific Headers **/
#include "spi_tclMLVncDiscovererDataIntf.h"
#include "spi_tclMLVncMsgQInterface.h"
#include "spi_tclMLVncCmdNotif.h"
#include "spi_tclVncSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
#include "trcGenProj/Header/spi_tclMLVncCmdNotif.cpp.trc.h"
#endif
#endif


//! Static variables
//static const t_S32 cos32NotiTimeout = 5000;         //Commented to Fix Lint
MsgContext spi_tclMLVncCmdNotif::m_oMsgContext;
static const t_String cszImagePng = "image/png";
static const t_String cszImageJpeg = "image/jpg";
static const t_U32 cou16EnableNotifications = 0xFFFF;
static const t_U32 cou16DisableNotifications = 0;
static const t_String cszAppListdelimiter = ",";
static const t_String cszClearEventNotiActionID = "0x00";

spi_tclMLVncCmdNotif* spi_tclMLVncCmdNotif::m_poVncCmdNoti = NULL;
static const t_String coszLaunchAppReq = "TRUE";


#define STRNCMP(VAR1,VAR2) \
   strncmp(VAR1,VAR2,strlen(VAR2))


typedef std::map<t_U32, std::map<t_U32,t_String> >::iterator tItNotiIDDeatils;
typedef std::map<t_U32,t_String>::iterator tItNotiIDpair;
typedef std::pair<t_U32,t_U32> tItDevNotiIDpair;
typedef std::map< std::pair<t_U32,t_U32>, std::map<t_U32, t_String> >::iterator tItNotiActionIDDetails;


/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdNotif::spi_tclMLVncCmdNotif();
***************************************************************************/
spi_tclMLVncCmdNotif::spi_tclMLVncCmdNotif() : m_u32ListnerID(0)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::spi_tclMLVncCmdNotif() \n"));
   m_poVncCmdNoti = this;
}

/***************************************************************************
** FUNCTION:  virtual spi_tclMLVncCmdNotif::~spi_tclMLVncCmdNotif()
***************************************************************************/
spi_tclMLVncCmdNotif::~spi_tclMLVncCmdNotif()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::~spi_tclMLVncCmdNotif() \n"));
   m_poVncCmdNoti=NULL;
   m_mapNotiActionDetails.clear();
   m_mapNotiDetails.clear();
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdNotif::bInitializeNotifications()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bInitializeNotifications()
{
   t_Bool bRet=false;

   //Get the discovery sdk and register for listeners
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   VNCDiscoverySDK* poDiscSDK = oDiscDataIntf.poGetDiscSDK();
   VNCDiscoverySDKDiscoverer* poMLDisc = oDiscDataIntf.poGetMLDisc();

   VNCDiscoverySDKCallbacks oDiscSDKCbs = { 0 };
   oDiscSDKCbs.fetchEntityValue = &vFetchEntityValueCallback;
   oDiscSDKCbs.postEntityValue = &vPostEntityValueCallback;
   oDiscSDKCbs.entityChanged = &vEntityChangedCallback;

   if((NULL != poDiscSDK)&&(NULL != poMLDisc))
   {
      //Currently we have only mirror link discoverer, so registering for that.
      //If any new discoverers are used, we should register for that listener also.
      t_S32 s32Error = poDiscSDK->addDiscovererListener(poMLDisc, this,
         &oDiscSDKCbs, sizeof(oDiscSDKCbs),&m_u32ListnerID);

      bRet = (s32Error == cs32MLDiscoveryErrorNone);

   }//if((NULL != poDiscSDK)&&(NULL != poMLDisc))

   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::bInitializeNotifications-Result-%d \n",
      ETG_ENUM(BOOL,bRet)));
   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdNotif::vUnInitializeNotifications()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vUnInitializeNotifications()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vUnInitializeNotifications() \n"));

   //Get the discovery sdk and register for listeners
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   VNCDiscoverySDK* poDiscSDK = oDiscDataIntf.poGetDiscSDK();
   VNCDiscoverySDKDiscoverer* poMLDisc = oDiscDataIntf.poGetMLDisc();

   if(( NULL != poDiscSDK )&&(NULL != poMLDisc))
   {
      //Remove the discoverer Listeners
      t_S32 s32Error = poDiscSDK->removeDiscovererListener(poMLDisc,m_u32ListnerID);
      if (cs32MLDiscoveryErrorNone != s32Error)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdNotif::vUnInitializeNotifications: Listener unreg failed - %d ", 
            s32Error));
      } // if (cs32MLDiscoveryErrorNone != s32Error)
   } //if(( NULL != poDiscSDK )&&(NULL != poMLDisc))

}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bSetNotiSubscription()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bSetNotiSubscription(const t_U32 cou32DevID,
                                                  t_Bool bSubscription)
{

   ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS," Subscribe For Notifications of Device-0x%x bSubscription-%d",
      cou32DevID,ETG_ENUM(BOOL,bSubscription)));

   t_Bool bRet = false;

   t_Char czValue[MAX_KEYSIZE] = {0};

   (true == bSubscription)?snprintf(czValue,MAX_KEYSIZE,VNCDiscoverySDKMLSubscribe):
      snprintf(czValue,MAX_KEYSIZE,VNCDiscoverySDKMLUnsubscribe);

   t_Char czKey[MAXKEYSIZE]={0};

   spi_tclVncSettings* poVNCSettings = spi_tclVncSettings::getInstance();
   if(NULL != poVNCSettings)
   {
      t_U16 u16UPnPEventsEnabledFromThePortNo = poVNCSettings->u16GetUPnPNotiEventsEnabledFromThePort();
      if(0 != u16UPnPEventsEnabledFromThePortNo)
      {
         static t_U16 u16EnableUPnPEventsOnPort = u16UPnPEventsEnabledFromThePortNo;
         t_U8 u8UPnPEventsEnabledOnNumOfPorts = poVNCSettings->u8TotalUPnPNotiEventsEnabledPorts();

         //Port number is more than the largest firewall disabled port. Reset the number to initial
         //port number from which the firewall is disabled
         if(u16EnableUPnPEventsOnPort > (u16UPnPEventsEnabledFromThePortNo+u8UPnPEventsEnabledOnNumOfPorts-1))
         {
            u16EnableUPnPEventsOnPort = u16UPnPEventsEnabledFromThePortNo;
         }//if(u16EnableUPnPEventsOnPort > (u16UPnPEventsEn

         ETG_TRACE_USR4(("spi_tclMLVncCmdNotif:UPnP Notifications are enabled on the Port-%d",u16EnableUPnPEventsOnPort));
         snprintf(czKey,MAXKEYSIZE - 1,VNCDiscoverySDKMLNotificationSubscriptionWithPort,u16EnableUPnPEventsOnPort);

         //Increment the port number on every un subscription request. so that next port number will be used for subsequent
         //subscription request
         u16EnableUPnPEventsOnPort = (false == bSubscription)?(u16EnableUPnPEventsOnPort+1):u16EnableUPnPEventsOnPort ;
      }
      else
      {
         ETG_TRACE_USR4(("spi_tclMLVncCmdNotif:UPnP Notifications are enabled on all Ports"));
         snprintf(czKey,MAXKEYSIZE - 1,VNCDiscoverySDKMLNotificationSubscription);
      }
   }//if(NULL != poVNCSettings)

   bRet = bPostEntityValueBlocking(cou32DevID,czKey,czValue,cs32MLDiscoveryNoTimeout) ;

   return bRet;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vGetNotiSupportedApps()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vGetNotiSupportedApps(const t_U32 cou32DevID,
                                                   const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vGetNotiSupportedApps: Dev-0x%x \n",cou32DevID));

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rVncHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DevID);

   if((NULL != rVncHandles.poDiscoverySdk)&&(NULL != rVncHandles.poDiscoverer)&&
      (NULL != rVncHandles.poEntity))
   {
      t_U32 u32RequestID = 0;

      //! Request for application that support notifications
      u32RequestID = const_cast<VNCDiscoverySDK*>(rVncHandles.poDiscoverySdk)->fetchEntityValueRespondToListener(
         m_u32ListnerID,
         const_cast<VNCDiscoverySDKDiscoverer*>(rVncHandles.poDiscoverer), 
         const_cast<VNCDiscoverySDKEntity*>(rVncHandles.poEntity),
         VNCDiscoverySDKMLGetNotificationSupportedApplications,
         cs32MLDiscoveryNoTimeout);

      if(u32RequestID != cu32MLDiscoveryNoRequestId)
      {
         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS," Request to get the Notisupported Apps of the device-0x%x is success",
            cou32DevID));
      }
      else
      {
         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS," Request to get the Notisupported Apps of the device-0x%x returned error",
            cou32DevID));
      }

      //Store the request ID. Will be used in callback function
      trMsgContext rNotiAppsMsgCtxt;
      rNotiAppsMsgCtxt.rUserContext = corfrUsrCntxt;
      rNotiAppsMsgCtxt.rAppContext.u32ResID = u32GET_NOTI_SUPPORTED_APPS;
      m_oMsgContext.vAddMsgContext(u32RequestID, rNotiAppsMsgCtxt);
   }//if((NULL != rVncHandles.poDiscoverySdk)&&(NULL != rVncHandles.poDiscoverer)&&
}


/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bEnableAppNotifications()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bEnableAppNotifications(const t_U32 cou32DevID,
                                                     const t_U16 cou16NumApps,
                                                     std::vector<trNotiEnable> &rfvecrNotiEnableList,
                                                     const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::bEnableAppNotifications:Device-0x%x \n",cou32DevID));

   t_Char czNotiAllowedApps[BUFFERSIZE] = { 0 };
   t_U32 u32RequestID = 0;


   if(cou16NumApps == cou16EnableNotifications)
   {
      //If the value is 0xFF, enable notifications for all Apps. use *
      snprintf(czNotiAllowedApps,MAX_KEYSIZE,"*");
      ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Requesting to enable Notifications the Device-0x%x (SetAllowedApps)",
         cou32DevID));
   }//if(cou32NumApps == 0xFF)
   else if(cou16NumApps == cou16DisableNotifications)
   {
      //If the value is 0x00, disable notifications for all Apps. use ""
      snprintf(czNotiAllowedApps,MAX_KEYSIZE,"");
      ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Requesting to disable Notifications the Device-0x%x (SetAllowedApps)",
         cou32DevID));
   }//else if(cou32NumApps == 0x00)
   else
   {
      tvecMLApplist vecAppList;
      t_U32 u32ListLen = rfvecrNotiEnableList.size();
      for(t_U32 u32Index = 0 ;u32Index<u32ListLen;u32Index++)
      {
         if(e8USAGE_ENABLED == rfvecrNotiEnableList[u32Index].enEnabledInfo)
         {
            vecAppList.push_back(rfvecrNotiEnableList[u32Index].u32AppHandle);
         }//if(e8USAGE_ENABLED == rfvecrNotiEnableList[u32Index].enEnabledInfo)
      }//for(t_U32 u32Index = 0 ;u32Index<u32ListLen;u32Index++)
      //Populate the string as the comma separated list of app id's
      t_String szAppStream;
      vPopulateVNCAppId(cou32DevID,szAppStream, vecAppList);
      snprintf(czNotiAllowedApps,MAX_KEYSIZE,"%s",szAppStream.c_str());
   }

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rVncHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DevID);

   if((NULL != rVncHandles.poDiscoverySdk)&&(NULL != rVncHandles.poDiscoverer)&&(NULL != rVncHandles.poEntity))
   {
      u32RequestID = const_cast<VNCDiscoverySDK*>(rVncHandles.poDiscoverySdk)->postEntityValueRespondToListener(
         m_u32ListnerID,const_cast<VNCDiscoverySDKDiscoverer*>(rVncHandles.poDiscoverer), 
         const_cast<VNCDiscoverySDKEntity*>(rVncHandles.poEntity),
         VNCDiscoverySDKMLSetNotificationAllowedApplications,
         czNotiAllowedApps,cs32MLDiscoveryNoTimeout);

      //Store the request ID. Will be used in callback function
      trMsgContext rNotiAllowedAppsMsgCtxt;
      rNotiAllowedAppsMsgCtxt.rUserContext = corfrUsrCntxt;
      rNotiAllowedAppsMsgCtxt.rAppContext.u32ResID = u32SET_NOTI_ALLOWED_APPS;
      m_oMsgContext.vAddMsgContext(u32RequestID,rNotiAllowedAppsMsgCtxt);
   }//if((NULL != rVncHandles.poDiscoverySdk)&&(NULL != rVncHandles.poDiscoverer)&&

   return (u32RequestID != cu32MLDiscoveryNoRequestId);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vInvokeNotiAction()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vInvokeNotiAction(const t_U32 cou32DevID,
                                               const t_U32 cou32NotiID, 
                                               const t_U32 cou32NotiActionID,
                                               const trUserContext& corfrUsrCntxt)
{

   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vInvokeNotiAction: Dev-0x%x, NotiID-0x%x, NotiActionID-0x%x \n",
      cou32DevID,cou32NotiID,cou32NotiActionID));

   t_U32 u32ReqID = 0;
   t_Char czKey[BUFFERSIZE] = { 0 };

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rVncHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DevID);

   if((NULL != rVncHandles.poDiscoverySdk)&&(NULL != rVncHandles.poDiscoverer)&&
      (NULL != rVncHandles.poEntity))
   {
      t_String szNotiID = szGetNotiID(cou32DevID,cou32NotiID).c_str();
      t_String szNotiActionID = (0 == cou32NotiActionID) ?
            (cszClearEventNotiActionID.c_str()):
            (szGetNotiActionID(cou32DevID,cou32NotiID,cou32NotiActionID).c_str());

      //Clear the Notification Info that is stored in SDK Cache. and then call InvokeNotiAction.
      //this will clear the Notification Info stored at the device end.
      if(false == bPostEntityValueBlocking(cou32DevID,
          VNCDiscoverySDKMLClearNotificationInfo,szNotiID.c_str()))
      {
          ETG_TRACE_ERR(("spi_tclMLVncCmdNotif::vInvokeNotiAction- Error in Clearing Notification Info"));
      }//if(cs32MLDiscoveryErrorNone != rVncHandles.poDiscoverySdk->postEnt

      snprintf(czKey, BUFFERSIZE - 1, VNCDiscoverySDKMLInvokeNotiAction, szNotiID.c_str());
      u32ReqID = const_cast<VNCDiscoverySDK*>(rVncHandles.poDiscoverySdk)->postEntityValueRespondToListener(
         m_u32ListnerID,
         const_cast<VNCDiscoverySDKDiscoverer*>(rVncHandles.poDiscoverer), 
         const_cast<VNCDiscoverySDKEntity*>(rVncHandles.poEntity),
         czKey,szNotiActionID.c_str(),cs32MLDiscoveryNoTimeout);

      if (cu32MLDiscoveryNoRequestId == u32ReqID)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdNotif::vInvokeNotiAction: Error in Invoking Noti Action \n"));
      }//if( 0 == u32ReqID)

      //Store the request ID. Will be used in callback function
      trMsgContext rNotiActionMsgCtxt;
      rNotiActionMsgCtxt.rUserContext = corfrUsrCntxt;
      rNotiActionMsgCtxt.rAppContext.u32ResID = u32INVOKE_NOTI_ACTION;
      m_oMsgContext.vAddMsgContext(u32ReqID,rNotiActionMsgCtxt);

      //Clear the Notification actions that are stored internally.
      vClearNotiActionInfo(cou32DevID,cou32NotiID);

   }//if((NULL != rVncHandles.poDiscoverySdk)&&

}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncDiscoverer:: bPostEntityValueBlocking()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bPostEntityValueBlocking(const t_U32 cou32DevID,
                                                      const t_Char* pcocKey, 
                                                      const t_Char* pcocValue,
                                                      MLDiscoverySDKTimeoutMicroseconds s32TimeOut)const
{
   t_Bool bRet=false;

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rVncHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DevID);

   if((NULL != rVncHandles.poDiscoverySdk)&&(NULL != rVncHandles.poDiscoverer)&&
      (NULL != rVncHandles.poEntity)&&(NULL != pcocKey))
   {
      t_S32 s32Error = cs32MLDiscoveryUnknownError;

      //Get the Error code if any
      s32Error = const_cast<VNCDiscoverySDK*>(rVncHandles.poDiscoverySdk)->postEntityValueBlocking(
         const_cast<VNCDiscoverySDKDiscoverer*>(rVncHandles.poDiscoverer), 
         const_cast<VNCDiscoverySDKEntity*>(rVncHandles.poEntity),
         pcocKey, pcocValue, s32TimeOut);

      bRet = (cs32MLDiscoveryErrorNone == s32Error);
      ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::bPostEntityValueBlocking: s32Error - %d, Key - %s \n",
            s32Error, pcocKey));
      if(NULL != pcocValue)
      {
         ETG_TRACE_USR4(("spi_tclMLVncCmdNotif::bPostEntityValueBlocking: Value - %s \n", pcocValue));
      }//if(NULL != pcocValue)
   }//if((NULL != rVncHandles.poDiscoverySdk)&&(NULL != rVncHandles.poDiscoverer)&&

   return bRet;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vPostEntityValueCallback()
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdNotif::vPostEntityValueCallback(
   t_Void *pListenerContext, 
   VNCDiscoverySDKRequestId u32RequestId,
   VNCDiscoverySDKDiscoverer *pDiscoverer, 
   VNCDiscoverySDKEntity *pEntity,
   t_Char *pczKey, 
   t_Char *pczValue, 
   VNCDiscoverySDKError s32Error)
{
   SPI_INTENTIONALLY_UNUSED(pListenerContext);
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vPostEntityValueCallback: Error-%d \n",s32Error));

   t_Bool bResult = (cs32MLDiscoveryErrorNone == s32Error);

   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
   if ((NULL != pczKey) && (NULL != pDiscoverer) && (NULL!= pEntity) && (NULL != poMsgQinterface))
   {
      ETG_TRACE_USR4(("spi_tclMLVncCmdNotif::vPostEntityValueCallback: pkey = %s \n", pczKey));

      if(NULL != pczValue)
      {
         ETG_TRACE_USR4(("spi_tclMLVncCmdNotif::vPostEntityValueCallback: pczValue = %s \n", pczValue));
      }//if(NULL != pczValue)

      //Get the Device ID using Discoverer Data Interface
      spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
      t_U32 u32DevID = 0;
      oDiscDataIntf.vGetDeviceId(pDiscoverer,pEntity,u32DevID);

      trMsgContext rPostCbCtxt = m_oMsgContext.rGetMsgContext(u32RequestId);
      if (rPostCbCtxt.rAppContext.u32ResID == u32INVOKE_NOTI_ACTION)
      {
         MLNotiActionRespMsg oNotiActionResp;
         oNotiActionResp.vSetUserContext(rPostCbCtxt.rUserContext);
         oNotiActionResp.u32DeviceHandle = u32DevID;
         oNotiActionResp.bNotiActionRes = bResult;
         poMsgQinterface->bWriteMsgToQ(&oNotiActionResp, sizeof(oNotiActionResp));
      }//if (rPostCbCtxt.rAppContext.u32ResID == u32INVOKE_NOTI_ACTION)
      else if (rPostCbCtxt.rAppContext.u32ResID  == u32SET_NOTI_ALLOWED_APPS)
      {
         MLSetNotiAppsErrorMsg oSetNotiAppsRes;
         oSetNotiAppsRes.vSetUserContext(rPostCbCtxt.rUserContext);
         oSetNotiAppsRes.u32DeviceHandle = u32DevID;
         oSetNotiAppsRes.bSetNotiAppRes = bResult;
         poMsgQinterface->bWriteMsgToQ(&oSetNotiAppsRes, sizeof(oSetNotiAppsRes));
      }//else if (rPostCbCtxt.rAppContext.u32ResID  == u32SET_NOTI_ALLOWED_APPS)
   }//if ((NULL != pczKey) && (NULL != pDiscoverer) && (NULL!= pEntity) 
   else
   {
      ETG_TRACE_ERR(("spi_tclMLVncCmdNotif::vPostEntityValueCallback: pkey /pValue / pEntity/ pDiscoverer is NULL \n"));
   }

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif:: vFetchEntityValueCallback()
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdNotif::vFetchEntityValueCallback(
   t_Void *pListenerContext, 
   VNCDiscoverySDKRequestId u32RequestId,
   VNCDiscoverySDKDiscoverer *pDiscoverer, 
   VNCDiscoverySDKEntity *pEntity,
   t_Char *pczKey, 
   VNCDiscoverySDKError s32Error, 
   t_Char *pczValue)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vFetchEntityValueCallback:Error-%d",s32Error));
   SPI_INTENTIONALLY_UNUSED(pListenerContext);
   if(NULL != pczKey) 
   {
      ETG_TRACE_USR4(("spi_tclMLVncCmdNotif::vFetchEntityValueCallback:Key-%s",pczKey));
      if(NULL != pczValue)
      {
         ETG_TRACE_USR4(("spi_tclMLVncCmdNotif::vFetchEntityValueCallback:pczValue-%s",pczValue));
      }//if(NULL != pczValue)

      //Get the Device ID using Discoverer Data Interface
      spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
      t_U32 u32DevID = 0;
      oDiscDataIntf.vGetDeviceId(pDiscoverer,pEntity,u32DevID);

      trMsgContext rMsgCtxt = m_oMsgContext.rGetMsgContext(u32RequestId);
      if ((NULL != m_poVncCmdNoti) && (rMsgCtxt.rAppContext.u32ResID == u32GET_NOTI_SUPPORTED_APPS)&&
         ( 0 != u32DevID))
      {
         if( cs32MLDiscoveryErrorNone != s32Error )
         {
            ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS," Error in fetching the Noti supported Applist of the device-0x%x",
               u32DevID));
            pczValue = NULL ;
         }

         m_poVncCmdNoti->vSendNotiSupportedApps(u32DevID,pczValue,rMsgCtxt.rUserContext);
      }//if ((NULL != m_spoVncCmdNoti) && (rFetchCbCtxt.rAppConte

   }//if( cs32MLDiscoveryErrorNone != s32Error )
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif:: vEntityChangedCallback()
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdNotif::vEntityChangedCallback(
   t_Void *pSDKContext, 
   VNCDiscoverySDKDiscoverer *pDiscoverer,
   VNCDiscoverySDKEntity *pEntity, 
   const t_Char *copczChangeDescription)
{   
   SPI_INTENTIONALLY_UNUSED(pSDKContext);
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vEntityChangedCallback() entered \n"));

   if(NULL != copczChangeDescription)
   {
      ETG_TRACE_USR3(("spi_tclMLVncCmdNotif::vEntityChangedCallback: Change - %s \n",
         copczChangeDescription));

      spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
      t_U32 u32DevID = 0;
      oDiscDataIntf.vGetDeviceId(pDiscoverer,pEntity,u32DevID);

      //Device send Notification event update with the change description: "NotificationEvent:xxxxxx"
      if (0 == STRNCMP(copczChangeDescription, VNCDiscoverySDKMLNotificationEvent) )
      {
         //generate the notification ID, by retrieving the value after the ":" in the string "NotificationEvent:xxxxxx"
         if( (NULL != m_poVncCmdNoti) && 
            (strlen(copczChangeDescription) > strlen(VNCDiscoverySDKMLNotificationEvent)) )
         {
            m_poVncCmdNoti->vProcessNotiEvent(u32DevID,(copczChangeDescription + strlen( VNCDiscoverySDKMLNotificationEvent)));
         }//if(NULL != m_poVncCmdNoti)
      }//if (0 == strncmp(copczChangeDescription, VNCDiscoverySDKMLNotific
      //Change in the Noti supported app list
      else if(0 == STRNCMP(copczChangeDescription,VNCDiscoverySDKMLNotificationAppListUpdate) )
      {
         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS," Change in Noti supported Applist of the device-0x%x",
            u32DevID));
         if(NULL != m_poVncCmdNoti)
         {
            //@directly fetching the Noti supported application list.
            trUserContext rUsrCntxt;
            m_poVncCmdNoti->vGetNotiSupportedApps(u32DevID,rUsrCntxt);
         }
      }
   }//if (NULL != copczChangeDescription)
   else
   {
      ETG_TRACE_ERR(("spi_tclMLVncCmdNotif::vEntityChangedCallback - No valid data"));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vSendNotiSupportedApps()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vSendNotiSupportedApps(const t_U32 cou32DevID,
                                                    const t_Char *pcoczNotiApps,
                                                    const trUserContext &corfrUserContext)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vSendNotiSupportedApps() \n"));
   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
   if(NULL != poMsgQinterface)
   {
      MLNotiApplistMsg oMLNotiAppList;
      oMLNotiAppList.vSetUserContext(corfrUserContext);
      oMLNotiAppList.u32DeviceHandle = cou32DevID;

      if(NULL != pcoczNotiApps)
      {
         StringHandler oStrUtil;
         std::vector<std::string> vecszAppIds;
         oStrUtil.vSplitString(pcoczNotiApps,',',vecszAppIds);

         std::vector<t_U32> vecszUniqueAppIDs;

         //generate the CRC for all the strings
         for(t_U16 u16Index = 0; u16Index< vecszAppIds.size(); u16Index++)
         {
            t_U32 u32UniqueID = u32GenerateUniqueId(vecszAppIds[u16Index].c_str());
            vecszUniqueAppIDs.push_back(u32UniqueID);
         }

         if (NULL != oMLNotiAppList.pvecNotiAppList)
         {
            *(oMLNotiAppList.pvecNotiAppList) = vecszUniqueAppIDs;
         }
      }//if(NULL != pcoczNotiApps)

      poMsgQinterface->bWriteMsgToQ(&oMLNotiAppList, sizeof(oMLNotiAppList));
   }//if(NULL != poMsgQinterface)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vProcessNotiEvent()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vProcessNotiEvent(const t_U32 cou32DevID, t_String szNotiID)
{
   ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"New Notification received for the Device-0x%x NotiID-%s",
      cou32DevID,szNotiID.c_str()));

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rVncHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DevID);

   VNCDiscoverySDK* poDiscSDK = const_cast<VNCDiscoverySDK*>(rVncHandles.poDiscoverySdk);
   VNCDiscoverySDKDiscoverer* poDisc = const_cast<VNCDiscoverySDKDiscoverer*>(rVncHandles.poDiscoverer);
   VNCDiscoverySDKEntity* poEntity = const_cast<VNCDiscoverySDKEntity*>(rVncHandles.poEntity) ;

   trNotiData rNotifData;
   t_Char czKey[MAXKEYSIZE] = { 0 };
   t_String szValue;

   //Fetch NotificationID
   if( false == szNotiID.empty())
   {
      rNotifData.u32NotiID = u32GetNotiID(cou32DevID,szNotiID.c_str());
      ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"New Notification received for the Device-0x%x NotiID-0x%x",
         cou32DevID,rNotifData.u32NotiID));

      //Fetch Notification Title
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiTitle, szNotiID.c_str());
      rNotifData.szNotiTitle = (bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,
         szValue))?szValue.c_str():"";

      ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Notification Title:%s",rNotifData.szNotiTitle.c_str()));

      //Fetch Notification Body
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiBody, szNotiID.c_str());
      rNotifData.szNotiBody = (bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,
         szValue))?szValue.c_str():"";

      ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Notification Body:%s",rNotifData.szNotiBody.c_str()));

      //Fetch Notification Icon Count
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiIconCount, szNotiID.c_str());
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         StringHandler oStrUtil;
         rNotifData.u32NotiIconCount = oStrUtil.u32ConvertStrToInt(szValue.c_str());
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      //Fetch Notification Icon details
      for(t_U32 u32NotiIcon = 0 ; u32NotiIcon<rNotifData.u32NotiIconCount;u32NotiIcon++)
      {
         trIconAttributes rIconAttr;
         if (true == bPopulateNotiIconDetails(poDiscSDK,poDisc,poEntity,szNotiID.c_str(), rIconAttr,u32NotiIcon))
         {
            rNotifData.tvecNotiIconList.push_back(rIconAttr);
         }//if (true == bPopulateNotiIconDetails(czNotiID, rIconAtt
      }//for(t_U32 u32NotiIcon = 0 ; u32NotiIcon<rNotifData

      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiAppId,szNotiID.c_str());
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         rNotifData.u32NotiAppID = u32GenerateUniqueId(szValue.c_str());
         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Notification available for the App:0x%x. Launch it",
            rNotifData.u32NotiAppID));
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      //Fetch NotificationActioncount
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionCount,szNotiID.c_str());
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue,
         cs32MLDiscoveryNoTimeout))
      {
         StringHandler oStrUtil;
         t_U32 u32NotiActionCount = (t_U8)oStrUtil.u32ConvertStrToInt(szValue.c_str());

         std::map<t_U32, t_String> mapNotiActionIDs;
         for(t_U32 u32Index=0;u32Index<u32NotiActionCount;u32Index++)
         {
            trNotiAction rNotiAction;
            std::string szNotiActionID;            
            if(true == bPopulateNotiActionDetails(poDiscSDK,poDisc,poEntity,szNotiID.c_str(),
               rNotiAction,u32Index,szNotiActionID))
            {
               rNotifData.tvecNotiActionList.push_back(rNotiAction);
               mapNotiActionIDs.insert(std::pair<t_U32,t_String>(rNotiAction.u32NotiActionID,
                  szNotiActionID.c_str()));            
            }//if(true == bPopulateNotiActionDetails(czNotiID,rNotiAction,u32Index))
         }//for(t_U32 u32Index=0;u32Index<u32NotiActionCount;u32Index++)
         vInsertNotiActionIDs(cou32DevID,rNotifData.u32NotiID,mapNotiActionIDs);

      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntit
   }//if(true == bFetchEntityValueBlocking(poDiscSDK,

   MLNotiDataMsg oNotiDataMsg;
   oNotiDataMsg.u32DeviceHandle = cou32DevID;
   if (NULL != oNotiDataMsg.prNotiData)
   {
      *(oNotiDataMsg.prNotiData) = rNotifData;
   }

   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
   if(NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oNotiDataMsg, sizeof(oNotiDataMsg));
   }//if(NULL != poMsgQinterface)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncDiscoverer:: bFetchEntityValueBlocking()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bFetchEntityValueBlocking(VNCDiscoverySDK* poDiscSDK,
                                                       VNCDiscoverySDKDiscoverer* poDisc, 
                                                       VNCDiscoverySDKEntity* poEntity,
                                                       const t_Char* pcocKey, 
                                                       t_String& rfrszVal,
                                                       MLDiscoverySDKTimeoutMicroseconds s32TimeOut)const
{
   t_Bool bRet = false;
   rfrszVal.clear();

   if((NULL != poDiscSDK)&&(NULL != poEntity)&&(NULL != poDisc)&&(NULL != pcocKey))
   {
      t_Char* czResult = NULL; 
      //Get the error code, if any
      t_S32 s32Error = poDiscSDK->fetchEntityValueBlocking(poDisc,
         poEntity, pcocKey, &czResult, s32TimeOut);
      if((cs32MLDiscoveryErrorNone == s32Error)&&(NULL != czResult))
      {
         bRet = true;
         rfrszVal = czResult ;
         poDiscSDK->freeString(czResult);
         czResult = NULL ;
      }//if((cs32MLDiscoveryErrorNone == s32Error)&&(NULL != czResult))
      ETG_TRACE_USR2(("bFetchEntityValueBlocking: s32Error - %d , Key - %s ",s32Error, pcocKey));
      ETG_TRACE_USR2(("bFetchEntityValueBlocking: szValue - %s",rfrszVal.c_str()));
   }//if((NULL != poDiscSDK)&&(NULL != poEntit
   else
   {
      ETG_TRACE_ERR(("bFetchEntityValueBlocking: either poDiscSdk or poDisc or poEntity or Key are null"));
   }

   return bRet;
}


/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPopulateVNCAppId()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vPopulateVNCAppId(const t_U32 cou32DevID,
                                               t_String& rfszNotiAllowedApps,
                                               const tvecMLApplist &rfvecApplist)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::bPopulateVNCAppId: Dev-0x%x \n",cou32DevID));
   t_U32 u32NumApps = rfvecApplist.size();
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   std::stringstream szAppID;
   for(t_U32 u32Index = 0;u32Index<u32NumApps;u32Index++)
   {
      szAppID<<oDiscDataIntf.szGetAppID(cou32DevID,rfvecApplist[u32Index]);
      //! Append delimiter if it is not the last app in the list.
      if ((u32NumApps - 1) > u32Index)
      {
         szAppID<<cszAppListdelimiter;
      }
   }//for(t_U32 u32Index = 0;u32Index<u32NumApps;u32Index++)
   rfszNotiAllowedApps = szAppID.str();

   ETG_TRACE_USR3(("spi_tclMLVncCmdNotif::bPopulateVNCAppId: szNotiAllowedApps-%s \n",rfszNotiAllowedApps.c_str()));
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPopulateNotiIconDetails()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bPopulateNotiIconDetails(VNCDiscoverySDK* poDiscSDK,
                                                      VNCDiscoverySDKDiscoverer* poDisc, 
                                                      VNCDiscoverySDKEntity* poEntity,
                                                      const t_Char *pcoczNotiID, 
                                                      trIconAttributes &rfrNotiIconDetails,
                                                      t_U32 u32Icon)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::bPopulateNotiIconDetails Entered \n"));
   t_Bool bRet = false;
   if (NULL != pcoczNotiID)
   {
      //!Buffer to populate key for fetch entity value blocking call
      t_Char czKey[MAXKEYSIZE] = { 0 };
      StringHandler oStrUtil;
      t_String szValue;

      //Fetch Notification Icon MimeType
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiIconMimeType,pcoczNotiID, u32Icon);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         if(cszImagePng == szValue)
         {
            rfrNotiIconDetails.enIconMimeType = e8ICON_PNG;
         }//if(cszImagePng == szValue)
         else if(cszImageJpeg == szValue)
         {
            rfrNotiIconDetails.enIconMimeType = e8ICON_JPEG;
         }//if(cszImageJpeg == szValue)
         else
         {
            rfrNotiIconDetails.enIconMimeType = e8ICON_INVALID;
         }
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      // Fetch Notification Icon Width
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiIconWidth,pcoczNotiID, u32Icon);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         rfrNotiIconDetails.u32IconWidth = oStrUtil.u32ConvertStrToInt(szValue.c_str());
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      // Fetch Notification Icon Height
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiIconHeight,pcoczNotiID, u32Icon);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         rfrNotiIconDetails.u32IconHeight = oStrUtil.u32ConvertStrToInt(szValue.c_str());
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      // Fetch Notification Icon depth
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiIconDepth,pcoczNotiID, u32Icon);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         rfrNotiIconDetails.u32IconDepth = oStrUtil.u32ConvertStrToInt(szValue.c_str());
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      // Fetch Notification Icon URL
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiIconUrl,pcoczNotiID, u32Icon);
      rfrNotiIconDetails.szIconURL = (bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,
         szValue))?szValue.c_str():"";

      bRet = true;
   }
   return bRet;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPopulateNotiActionDetails()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bPopulateNotiActionIconDetails(VNCDiscoverySDK* poDiscSDK,
                                                            VNCDiscoverySDKDiscoverer* poDisc, 
                                                            VNCDiscoverySDKEntity* poEntity,
                                                            const t_Char *pcoczNotiID, 
                                                            trIconAttributes &rfrNotiActionIconDetails,
                                                            t_U32 u32NotiAction,
                                                            t_U32 u32NotiActionIcon)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::bPopulateNotiActionIconDetails()\n"));
   t_Bool bRet = false;

   if (NULL != pcoczNotiID)
   {
      //!Buffer to populate key for fetch entity value blocking call
      t_Char czKey[MAXKEYSIZE] = { 0 };
      StringHandler oStrUtil;
      t_String szValue;

      //Fetch Notification Icon MimeType
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionIconMimeType,pcoczNotiID, 
         u32NotiAction,u32NotiActionIcon);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         if(cszImagePng == szValue)
         {
            rfrNotiActionIconDetails.enIconMimeType = e8ICON_PNG;
         }//if(cszImagePng == szValue)
         else if(cszImageJpeg == szValue)
         {
            rfrNotiActionIconDetails.enIconMimeType = e8ICON_JPEG;
         }//if(cszImageJpeg == szValue)
         else
         {
            rfrNotiActionIconDetails.enIconMimeType = e8ICON_INVALID;
         }
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      // Fetch Notification Icon Width
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionIconWidth,pcoczNotiID, 
         u32NotiAction,u32NotiActionIcon);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         rfrNotiActionIconDetails.u32IconWidth = oStrUtil.u32ConvertStrToInt(szValue.c_str());
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      // Fetch Notification Icon Height
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionIconHeight,pcoczNotiID, 
         u32NotiAction,u32NotiActionIcon);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         rfrNotiActionIconDetails.u32IconHeight = oStrUtil.u32ConvertStrToInt(szValue.c_str());
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      // Fetch Notification Icon depth
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionIconDepth,pcoczNotiID, 
         u32NotiAction,u32NotiActionIcon);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         rfrNotiActionIconDetails.u32IconDepth = oStrUtil.u32ConvertStrToInt(szValue.c_str());
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      // Fetch Notification Icon URL
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionIconUrl,pcoczNotiID, 
         u32NotiAction,u32NotiActionIcon);
      rfrNotiActionIconDetails.szIconURL = (bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,
         szValue))?szValue.c_str():"";

      bRet = true;
   }

   return bRet;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bPopulateNotiActionDetails()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bPopulateNotiActionDetails(VNCDiscoverySDK* poDiscSDK,
                                                        VNCDiscoverySDKDiscoverer* poDisc, 
                                                        VNCDiscoverySDKEntity* poEntity,
                                                        const t_Char *pcoczNotiID, 
                                                        trNotiAction &rfrNotiActionDetails,
                                                        t_U32 u32NotiAction,
                                                        t_String& rfszNotiActionID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::bPopulateNotiActionDetails()\n"));
   t_Bool bRet = false;

   //!Buffer to populate and send key value to fetch entity value blocking call
   t_Char czKey[MAXKEYSIZE] = { 0 };

   rfszNotiActionID.clear();

   if (NULL != pcoczNotiID)
   {
      StringHandler oStrUtil;
      t_String szValue;

      //Fetch Notification Action ID
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionId,pcoczNotiID,u32NotiAction);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Notification Action-%s",
            szValue.c_str()));
         rfszNotiActionID = szValue.c_str();
         rfrNotiActionDetails.u32NotiActionID = u16GenerateUniqueId(szValue.c_str());
         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Notification Action-0x%x",
            rfrNotiActionDetails.u32NotiActionID));
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      //Fetch Notification Action Name
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionName,pcoczNotiID,u32NotiAction);
      rfrNotiActionDetails.szNotiActionName = (bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,
         szValue))?szValue.c_str():"";
      if(false == szValue.empty())
      {
         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Notification Action-%d: Action Name-%s",
            rfrNotiActionDetails.u32NotiActionID,rfrNotiActionDetails.szNotiActionName.c_str()));
      }//if(false == szValue.empty())

      //Fetch Application to be launched for this Notification Action
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionLaunchApp,pcoczNotiID,u32NotiAction);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         std::transform(szValue.begin(), szValue.end(),szValue.begin(),::toupper);
         rfrNotiActionDetails.bLaunchAppReq = (coszLaunchAppReq  == szValue);
         ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Notification Action: Launch App req-%d",
            ETG_ENUM(BOOL,rfrNotiActionDetails.bLaunchAppReq)));
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      //Fetch Number of Notification Action Icons
      snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionIconCount,pcoczNotiID,u32NotiAction);
      if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity,czKey,szValue))
      {
         rfrNotiActionDetails.u32NotiActionIconCount = oStrUtil.u32ConvertStrToInt(szValue.c_str());
      }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity

      for(t_U32 u32NotiActionIcon=0;u32NotiActionIcon<rfrNotiActionDetails.u32NotiActionIconCount;u32NotiActionIcon++)
      {
         trIconAttributes rNotiActionIconAttr;
         if (true == bPopulateNotiActionIconDetails(poDiscSDK,poDisc,poEntity,pcoczNotiID,
            rNotiActionIconAttr, u32NotiAction,u32NotiActionIcon))
         {
            rfrNotiActionDetails.tvecNotiActionIconList.push_back(rNotiActionIconAttr);
         }//if (true == bPopulateNotiActionIconDetails(poDiscSDK,poDisc
      }//for(t_U32 u32Index=0;u32Index<u32NotiActionIconCount;u32Index++)

      bRet = true;
   }
   return bRet;
}

/***************************************************************************
** FUNCTION: t_U32 spi_tclMLVncCmdNotif::u32GenerateUniqueId()
***************************************************************************/
t_U32 spi_tclMLVncCmdNotif::u32GenerateUniqueId(t_String szUniqueId)const
{
   t_U32 u32UniqueId = u32CalcCRC32((t_UChar*) (const_cast<t_Char*>(szUniqueId.c_str())), szUniqueId.size());

   ETG_TRACE_USR2(("spi_tclMLVncCmdNotif:u32GenerateUniqueID u32UniqueId - 0x%x\n",
      u32UniqueId));

   return u32UniqueId;
}

/***************************************************************************
** FUNCTION: t_U16 spi_tclMLVncCmdNotif::u16GenerateUniqueId()
***************************************************************************/
t_U16 spi_tclMLVncCmdNotif::u16GenerateUniqueId(t_String szUniqueId)const
{
   t_U16 u16UniqueId = u16CalcCRC16((t_UChar*) (const_cast<t_Char*>(szUniqueId.c_str())), szUniqueId.size());

   ETG_TRACE_USR2(("spi_tclMLVncCmdNotif:u16GenerateUniqueId u16UniqueId - 0x%x",
      u16UniqueId));

   return u16UniqueId;
}

/***************************************************************************
** FUNCTION: t_String spi_tclMLVncCmdNotif::szGetNotiID()
***************************************************************************/
t_String spi_tclMLVncCmdNotif::szGetNotiID(t_U32 u32DevID,t_U32 u32NotiID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::szGetNotiID: Dev-0x%x NotiID-0x%x", u32DevID,u32NotiID));
   t_String szNotiID;

   m_oLock_NotiID.s16Lock();
   if(SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   {
      tItNotiIDDeatils itNotiIDDetails = m_mapNotiDetails.find(u32DevID);
      if(( itNotiIDDetails != m_mapNotiDetails.end()) &&(
         SPI_MAP_NOT_EMPTY(itNotiIDDetails->second)))
      {
         tItNotiIDpair itNotiIDinfo = itNotiIDDetails->second.find(u32NotiID); 
         if(itNotiIDinfo != itNotiIDDetails->second.end())
         {
            szNotiID = itNotiIDinfo->second.c_str();
         }//if(itNotiIDinfo != itNotiIDDetails->second.end())
      }//if(( itNotiIDDetails != m_mapNotiDetails.end()) &&(
   } //if(SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   m_oLock_NotiID.vUnlock();

   ETG_TRACE_USR2(("spi_tclMLVncCmdNotif::szGetNotiID: NotiID-%s", 
      szNotiID.c_str()));

   return szNotiID;
}

/***************************************************************************
** FUNCTION: t_U32 spi_tclMLVncCmdNotif::u32GetNotiID()
***************************************************************************/
t_U32 spi_tclMLVncCmdNotif::u32GetNotiID(t_U32 u32DevID,t_String szNotiID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::u32GetNotiID: Dev-0x%x NotiID-%s", 
      u32DevID,szNotiID.c_str()));

   //Notification ID is currently t_U16 in the HMI interface. so generating U16 CRC
   t_U32 u32NotiID = (t_U32) u16GenerateUniqueId(szNotiID.c_str());

   std::map<t_U32,t_String> mapNotiIDDetails;
   mapNotiIDDetails[u32NotiID] = szNotiID.c_str();

   m_oLock_NotiID.s16Lock();
   //If the notification info already exists, over write it
   m_mapNotiDetails[u32DevID] = mapNotiIDDetails;
   m_oLock_NotiID.vUnlock();

   return u32NotiID ;

}

/***************************************************************************
** FUNCTION: t_String spi_tclMLVncCmdNotif::szGetNotiActionID()
***************************************************************************/
t_String spi_tclMLVncCmdNotif::szGetNotiActionID(t_U32 u32DevID, t_U32 u32NotiID, 
                                                 t_U32 u32NotiActionID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::szGetNotiActionID: Dev-0x%x NotiID-0x%x NotiActionID-0x%x", 
      u32DevID,u32NotiID,u32NotiActionID));

   t_String szNotiActionID;

   m_oLock_NotiActionID.s16Lock();
   if(SPI_MAP_NOT_EMPTY(m_mapNotiActionDetails))
   {
      tItNotiActionIDDetails itNotiActionIDDetails= 
         m_mapNotiActionDetails.find(tItDevNotiIDpair(u32DevID,u32NotiID));

      if((itNotiActionIDDetails != m_mapNotiActionDetails.end()) &&
         (SPI_MAP_NOT_EMPTY(itNotiActionIDDetails->second)) )
      {
         tItNotiIDpair itNotiActionIDInfo = itNotiActionIDDetails->second.find(u32NotiActionID);
         if(itNotiActionIDInfo != itNotiActionIDDetails->second.end())
         {
            szNotiActionID = itNotiActionIDInfo->second.c_str();
         }//tItNotiIDpair itNotiActionIDInfo = itNotiActionIDDetails->se
      }//if((itNotiActionIDDetails != m_mapNotiActionDetails.end()) &&
   }//if(SPI_MAP_NOT_EMPTY(m_mapNotiActionDetails))
   m_oLock_NotiActionID.vUnlock();

   ETG_TRACE_USR2(("spi_tclMLVncCmdNotif::szGetNotiActionID: NotiActionID-%s", 
      szNotiActionID.c_str()));

   return szNotiActionID;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vInsertNotiActionIDs()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vInsertNotiActionIDs(t_U32 u32DevID, t_U32 u32NotiID, 
                                                  const std::map<t_U32, t_String>& corfmapNotiActionIDs)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::u32GetNotiActionID: Dev-0x%x NotiID-0x%x", 
      u32DevID,u32NotiID));
   m_oLock_NotiActionID.s16Lock();
   //Over write the entry in the map, if it already exists
   m_mapNotiActionDetails[tItDevNotiIDpair(u32DevID,u32NotiID)] = corfmapNotiActionIDs;
   m_oLock_NotiActionID.vUnlock();

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vClearNotiActionInfo()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vClearNotiActionInfo(t_U32 u32DevID,t_U32 u32NotiID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vClearNotiActionInfo: Dev-0x%x NotiID-0x%x ", 
      u32DevID,u32NotiID));

   m_oLock_NotiActionID.s16Lock();
   if(SPI_MAP_NOT_EMPTY(m_mapNotiActionDetails))
   {
      tItNotiActionIDDetails itNotiActionIDDetails = 
         m_mapNotiActionDetails.find(tItDevNotiIDpair(u32DevID,u32NotiID));

      if( (itNotiActionIDDetails != m_mapNotiActionDetails.end()) && 
         (SPI_MAP_NOT_EMPTY(itNotiActionIDDetails->second)))
      {
         itNotiActionIDDetails->second.clear();
      }//if( (itNotiActionIDDetails != m_mapNotiActionDetails.end()) && 
   }//if(SPI_MAP_NOT_EMPTY(m_mapNotiActionDetails))
   m_oLock_NotiActionID.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vClearNotiInfo()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vClearNotiInfo(t_U32 u32DevID, t_U32 u32NotiID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vClearNotiInfo: Dev-0x%x NotiID-0x%x ", 
      u32DevID,u32NotiID));
   m_oLock_NotiID.s16Lock();
   if(SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   {
      tItNotiIDDeatils itNotiIDInfo = m_mapNotiDetails.find(u32DevID);
      if(( itNotiIDInfo != m_mapNotiDetails.end()) &&( SPI_MAP_NOT_EMPTY(itNotiIDInfo->second)))
      {
         itNotiIDInfo->second.clear();
      }//if(( itNotiIDInfo != m_mapNotiDetails.end()) 
   } //if(SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   m_oLock_NotiID.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vClearDevNotiActionInfo()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vClearDevNotiActionInfo(t_U32 u32DevID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vClearDevNotiActionInfo: Dev-0x%x ", 
      u32DevID));

   m_oLock_NotiActionID.s16Lock();

   tItNotiActionIDDetails itNotiActionDetails = m_mapNotiActionDetails.begin();
   while(itNotiActionDetails != m_mapNotiActionDetails.end())
   {
      if(itNotiActionDetails->first.first == u32DevID)
      {
         //usage of erase in for loop. causes iterator to point unknown address.
         //do not change this.
         m_mapNotiActionDetails.erase(itNotiActionDetails++);
      }//if((itAppsInfo->first.first == corfrdevID)
      else
      {
         itNotiActionDetails++;
      }
   }//while(itAppsInfo != poMLVncDisc->m_mapAppsInfo.end())

   m_oLock_NotiActionID.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vClearDevNotiInfo()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vClearDevNotiInfo(t_U32 u32DevID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vClearDevNotiInfo: Dev-0x%x ", 
      u32DevID));

   m_oLock_NotiID.s16Lock();
   if(SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   {
      tItNotiIDDeatils itNotiIDDetails = m_mapNotiDetails.find(u32DevID);
      if(itNotiIDDetails != m_mapNotiDetails.end())
      {
         m_mapNotiDetails.erase(u32DevID);
      }//if(itNotiIDDetails != m_mapNotiDetails.end())
   }//if(SPI_MAP_NOT_EMPTY(m_mapNotiDetails))
   m_oLock_NotiID.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdNotif::vDeviceDisconnected()
***************************************************************************/
t_Void spi_tclMLVncCmdNotif::vDeviceDisconnected(t_U32 u32DevID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdNotif::vDeviceDisconnected: Dev-0x%x ", 
      u32DevID));
   vClearDevNotiActionInfo(u32DevID);
   vClearDevNotiInfo(u32DevID);
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdNotif::bIsLaunchAppReq()
***************************************************************************/
t_Bool spi_tclMLVncCmdNotif::bIsLaunchAppReq(t_U32 u32DevID,
                                             t_U32 u32AppID,
                                             t_U32 u32NotificationID,
                                             t_U32 u32NotificationActionID)
{

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rVncHandles = oDiscDataIntf.corfrGetDiscHandles(u32DevID);
   t_Bool bIsLaunchAppRequired=false;
   SPI_INTENTIONALLY_UNUSED(u32AppID);
   if((NULL != rVncHandles.poDiscoverySdk)&&(NULL != rVncHandles.poDiscoverer)&&
      (NULL != rVncHandles.poEntity))
   {
      t_String szNotiID = szGetNotiID(u32DevID,u32NotificationID).c_str();
      t_String szNotiActionID = szGetNotiActionID(u32DevID,u32NotificationID,u32NotificationActionID).c_str();

      if(0 != u32NotificationActionID)
      {
         //!Buffer to populate and send key value to fetch entity value blocking call
         t_Char czKey[MAXKEYSIZE] = { 0 };
         t_String szValue;
         t_U8 u8OrdinalOfNotiAction = 0xFF;

         //get the ordinal of the notification action in the Notification action list
         snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionCount,szNotiID.c_str());
         if(true == bFetchEntityValueBlocking(const_cast<VNCDiscoverySDK*> (rVncHandles.poDiscoverySdk),
            const_cast<VNCDiscoverySDKDiscoverer*> (rVncHandles.poDiscoverer),
            const_cast<VNCDiscoverySDKEntity*> (rVncHandles.poEntity),czKey,szValue,
            cs32MLDiscoveryNoTimeout))
         {
            StringHandler oStrUtil;
            t_U32 u32NotiActionCount = oStrUtil.u32ConvertStrToInt(szValue.c_str());

            for(t_U32 u32Index=0;u32Index<u32NotiActionCount;u32Index++)
            {
               snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionId,szNotiID.c_str(),u32Index);
               if((true == bFetchEntityValueBlocking(const_cast<VNCDiscoverySDK*> (rVncHandles.poDiscoverySdk),
                  const_cast<VNCDiscoverySDKDiscoverer*> (rVncHandles.poDiscoverer),
                  const_cast<VNCDiscoverySDKEntity*> (rVncHandles.poEntity),czKey,szValue)) &&
                  (szValue == szNotiActionID) )
               {
                  u8OrdinalOfNotiAction = u32Index;
                  break;
               }//if(true == bFetchEntityValueBlocking(poDiscSDK,poDisc,poEntity
            }//for(t_U32 u32Index=0;u32Index<u32NotiActionCount;u32Index++)
         }

         //If the ordinal of the Notification Action ID is fetched successfully, check whether the
         //we need to launch the application for this notification
         if(0xFF != u8OrdinalOfNotiAction)
         {
            //Fetch Application to be launched for this Notification Action
            snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLNotiActionLaunchApp,szNotiID.c_str(),
               u8OrdinalOfNotiAction);
            if(true == bFetchEntityValueBlocking(const_cast<VNCDiscoverySDK*> (rVncHandles.poDiscoverySdk),
               const_cast<VNCDiscoverySDKDiscoverer*> (rVncHandles.poDiscoverer),
               const_cast<VNCDiscoverySDKEntity*> (rVncHandles.poEntity),
               czKey,szValue))
            {
               ETG_TRACE_USR4(("spi_tclMLVncCmdNotif::bIsLaunchAppRequired:LaunchAppReq-%s",szValue.c_str()));
               std::transform(szValue.begin(), szValue.end(),szValue.begin(),::toupper);
               bIsLaunchAppRequired = (coszLaunchAppReq  == szValue) ;
            }//if(true == bFetchEntityValueBlocking
         }
      }//if(0 != u32NotificationActionID)
   }//if((NULL != rVncHandles.poDiscoverySdk)&&(N

   ETG_TRACE_USR4(("LaunchAppReq For the Notification Action-%d",ETG_ENUM(BOOL,bIsLaunchAppRequired)));

   return bIsLaunchAppRequired;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
