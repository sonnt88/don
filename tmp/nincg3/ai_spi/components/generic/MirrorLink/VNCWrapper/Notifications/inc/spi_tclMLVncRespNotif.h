/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespNotif.h
 * \brief             RealVNC Wrapper Response class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:   VNC Response class for receiving response receiving response
 for spi_tclMLVncRespNotif calls to implement the notification service provided
 by MLServer. This class is used by ViewerWrapper
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 30.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 18.04.2013 |  Shiva Kumar Gurija          | Updated the wrapper

 \endverbatim
 ******************************************************************************/

#ifndef SPI_MLVNCRESPNOTIF_H_
#define SPI_MLVNCRESPNOTIF_H_
/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/

#include "RespBase.h"
#include "SPITypes.h"
/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLVncRespNotif
 * \brief VNC Response class for receiving response for spi_tclMLVncRespNotif calls to
 *        implement the notification service provided by MLServer.
 */

class spi_tclMLVncRespNotif: public RespBase
{

   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclMLVncRespNotif::~spi_tclMLVncRespNotif()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclMLVncRespNotif()
       * \brief   Destructor
       * \sa      spi_tclMLVncRespNotif()
       **************************************************************************/
      virtual ~spi_tclMLVncRespNotif();

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespNotif::vPostNotiAllowedApps()
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostNotiAllowedApps(trUserContext rUsrCntxt, 
       *             const t_U32 cou32DeviceHandle,
       *             tvecMLApplist &vecApplist)
       * \brief   callback in response to  vGetNotiSupportedApps().
       *          provides the list of applications that support notifications
       * \param   rUsrCntxt   : [IN] Context information passed from the caller
       * \param   cou32DeviceHandle :[IN] Device handle
       * \param   vecApplist    : [IN] Notification identifier sent during
       *                           notification event.
       * \retval  t_Void
       * \sa     vGetNotiSupportedApps()
       **************************************************************************/
      virtual t_Void vPostNotiAllowedApps(trUserContext rUsrCntxt,
         const t_U32 cou32DeviceHandle,
         tvecMLApplist &vecApplist);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespNotif::vPostSetNotiAllowedAppsResult()
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostSetNotiAllowedAppsResult(trUserContext rUsrCntxt,
       *             const t_U32 cou32DeviceHandle,
       *             t_Bool bSetNotiAppRes)
       * \brief   callback in response to bSetNotiEnableAppsInfo()
       * \param   rUsrCntxt   : [IN] Context information passed from the caller
       * \param   cou32DeviceHandle :[IN] Device handle
       * \param   bSetNotiAppRes : [IN] The outcome of the request:
       *          true:     if the request was completed successfully
       *          false:    if the request failed
       * \sa     bSetNotiEnableAppsInfo
       **************************************************************************/
      virtual t_Void vPostSetNotiAllowedAppsResult(trUserContext rUsrCntxt,
         const t_U32 cou32DeviceHandle, 
         t_Bool bSetNotiAppRes);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespNotif::vPostInvokeNotiActionResult()
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostInvokeNotiActionResult(trUserContext rUsrCntxt,
       *             const t_U32 cou32DeviceHandle,t_Bool bNotiActionRes)
       * \brief   callback in response to vInvokeNotiAction().
       *          provides the result of the received action (success or failure)
       * \param   rUsrCntxt   : [IN] Context information passed from the caller
       * \param   cou32DeviceHandle :[IN] Device handle
       * \param   bNotiActionRes : [IN] The outcome of the request:
       *          true:     if the request was completed successfully
       *          false:    if the request failed
       * \sa     vPostNotiEvent,vRequestTriggerNotiAction
       **************************************************************************/
      virtual t_Void vPostInvokeNotiActionResult(trUserContext rUsrCntxt,
         const t_U32 cou32DeviceHandle,
         t_Bool bNotiActionRes);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespNotif::vPostNotiEvent()
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostNotiEvent(const t_U32 cou32DeviceHandle,
       *             const trNotiData &rfrNotiDetails)
       * \brief   posts a event when notification is received from MLServer
       * \param   cou32DeviceHandle :[IN] Device handle
       * \param   rfrNotiDetails : [OUT] structure containing notification details
       *                         such as icon details, action details etc.
       *                         /ref MLNotiDetails
       * \retval  t_Void
       * \sa      vPostNotiEvent(t_U32 u32NotiId,const MLNotiDetails &oNotiDetails);
       *          vRequestTriggerNotiAction(t_U32 u32NotiId, t_U32 u32NotiActionID);
       **************************************************************************/
      virtual t_Void vPostNotiEvent(const t_U32 cou32DeviceHandle,
         const trNotiData &rfrNotiDetails);

   protected:

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncRespNotif::spi_tclMLVncRespNotif();
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncRespNotif()
       * \brief   Default Constructor. Made potected to prevent object creation.
       *          Expects the implementation class to derive from this and implement
       *          the necessary virtual functions
       * \sa      ~spi_tclMLVncRespNotif()
       **************************************************************************/
      spi_tclMLVncRespNotif();

      /***************************************************************************
       ** FUNCTION: spi_tclMLVncRespNotif(const spi_tclMLVncRespNotif &corfobjCRCBResp)
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncRespNotif(const spi_tclMLVncRespNotif &corfobjCRCBResp)
       * \brief   Copy constructor not implemented hence made protected
       * \sa      ~spi_tclMLVncRespNotif()
       **************************************************************************/
      spi_tclMLVncRespNotif(const spi_tclMLVncRespNotif &corfobjCRCBResp);

      /***************************************************************************
       ** FUNCTION: const spi_tclMLVncRespNotif & operator=(
       **                                 const spi_tclMLVncRespNotif &corfobjCRCBResp);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclMLVncRespNotif & operator=(const spi_tclMLVncRespNotif &corfobjCRCBResp);
       * \brief   assignment operator not implemented hence made protected
       * \sa      ~spi_tclMLVncRespNotif()
       **************************************************************************/
      const spi_tclMLVncRespNotif & operator=(
            const spi_tclMLVncRespNotif &corfobjCRCBResp);

};

#endif /* SPI_MLVNCRESPNOTIF_H_ */
