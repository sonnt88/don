/***********************************************************************/
/*!
 * \file  spi_tclMLVncNotifDispatcher.h
 * \brief Message Dispatcher for Notification Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Notification Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 18.04.2013 |  Shiva Kumar Gurija    | Updated the wrapper

 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLMLVNCNOTIFDISPATCHER_H_
#define SPI_TCLMLVNCNOTIFDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"

/**************Forward Declarations******************************************/
class spi_tclMLVncNotifDispatcher;

/****************************************************************************/
/*!
 * \class MLNotifMsgBase
 * \brief Base Message type for all notification messages
 ****************************************************************************/
class MLNotifMsgBase: public trMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  MLNotifMsgBase::MLNotifMsgBase
       ***************************************************************************/
      /*!
       * \fn      MLNotifMsgBase()
       * \brief   Default constructor
       **************************************************************************/
      MLNotifMsgBase();

      /***************************************************************************
       ** FUNCTION:  MLNotifMsgBase::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncNotifDispatcher* poNotiDispatcher)
       * \brief   Pure virtual function to be overridden by inherited classes for
       *          dispatching the message
       * \param poNotiDispatcher : Pointer to Notifications Message Dispatcher
       **************************************************************************/
      virtual t_Void vDispatchMsg(
               spi_tclMLVncNotifDispatcher* poNotiDispatcher) = 0;

      /***************************************************************************
       ** FUNCTION:  MLNotifMsgBase::~MLNotifMsgBase
       ***************************************************************************/
      /*!
       * \fn      ~MLNotifMsgBase()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLNotifMsgBase()
      {

      }
};

/****************************************************************************/
/*!
 * \class MLNotiApplistMsg
 * \brief Mirrorlink application list message
 ****************************************************************************/
class MLNotiApplistMsg: public MLNotifMsgBase
{
   public:
      t_U32 u32DeviceHandle;
      std::vector<t_U32> *pvecNotiAppList;
      /***************************************************************************
       ** FUNCTION:  MLNotiApplistMsg::MLNotiApplistMsg
       ***************************************************************************/
      /*!
       * \fn      MLNotiApplistMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLNotiApplistMsg();

      /***************************************************************************
       ** FUNCTION:  MLNotiApplistMsg::~MLNotiApplistMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLNotiApplistMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLNotiApplistMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLNotiApplistMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncNotifDispatcher* poNotiDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poNotiDispatcher : Pointer to Notifications Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncNotifDispatcher* poNotiDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLNotiApplistMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLNotiApplistMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLSetNotiAppsErrorMsg
 * \brief Mirrorlink Error message while setting notification apps message
 ****************************************************************************/
class MLSetNotiAppsErrorMsg: public MLNotifMsgBase
{
   public:
      //! result of setting notifications allowed applications
      t_Bool bSetNotiAppRes;
      t_U32 u32DeviceHandle;

      /***************************************************************************
       ** FUNCTION:  MLSetNotiAppsErrorMsg::MLSetNotiAppsErrorMsg
       ***************************************************************************/
      /*!
       * \fn      MLSetNotiAppsErrorMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLSetNotiAppsErrorMsg();

      /***************************************************************************
       ** FUNCTION:  MLSetNotiAppsErrorMsg::~MLSetNotiAppsErrorMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLSetNotiAppsErrorMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLSetNotiAppsErrorMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLSetNotiAppsErrorMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncNotifDispatcher* poNotiDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poNotiDispatcher : Pointer to Notifications Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncNotifDispatcher* poNotiDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLSetNotiAppsErrorMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLSetNotiAppsErrorMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLNotiActionRespMsg
 * \brief Mirrorlink Response message after action is taken by the user for
 *        notifications
 ****************************************************************************/
class MLNotiActionRespMsg: public MLNotifMsgBase
{
   public:
      //! Result of Notification action
      t_Bool bNotiActionRes;
      t_U32 u32DeviceHandle;

      /***************************************************************************
       ** FUNCTION:  MLNotiActionRespMsg::MLNotiActionRespMsg
       ***************************************************************************/
      /*!
       * \fn      MLNotiActionRespMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLNotiActionRespMsg();

      /***************************************************************************
       ** FUNCTION:  MLNotiActionRespMsg::~MLNotiActionRespMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLNotiActionRespMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLNotiActionRespMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLNotiActionRespMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncNotifDispatcher* poNotiDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poNotiDispatcher : Pointer to Notifications Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncNotifDispatcher* poNotiDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLNotiActionRespMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLNotiActionRespMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}

};

/****************************************************************************/
/*!
 * \class MLNotiDataMsg
 * \brief Complete information about a notification. Used for posting
 *        notification event
 ****************************************************************************/
class MLNotiDataMsg: public MLNotifMsgBase
{
   public:

      trNotiData *prNotiData;
      t_U32 u32DeviceHandle;

      /***************************************************************************
       ** FUNCTION:  MLNotiDataMsg::MLNotiDataMsg
       ***************************************************************************/
      /*!
       * \fn      MLNotiDataMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLNotiDataMsg();

      /***************************************************************************
       ** FUNCTION:  MLNotiDataMsg::~MLNotiDataMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLNotiDataMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLNotiDataMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLNotiDataMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncNotifDispatcher* poNotiDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poNotiDispatcher : Pointer to Notifications Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncNotifDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLNotiDataMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLNotiDataMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class spi_tclMLVncNotifDispatcher
 * \brief Message Dispatcher for Notification Messages
 ****************************************************************************/
class spi_tclMLVncNotifDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncNotifDispatcher::spi_tclMLVncNotifDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncNotifDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclMLVncNotifDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncNotifDispatcher::~spi_tclMLVncNotifDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclMLVncNotifDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclMLVncNotifDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleNotiMsg(MLNotiApplistMsg* poApplistMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleNotiMsg(MLNotiApplistMsg* poApplistMsg)
       * \brief   Handles Messages of MLNotiApplistMsg type
       * \param poApplistMsg : pointer to MLNotiApplistMsg
       **************************************************************************/
      t_Void vHandleNotiMsg(MLNotiApplistMsg* poApplistMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleNotiMsg(MLSetNotiAppsErrorMsg* poSetNotiAppsErrorMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleNotiMsg(MLSetNotiAppsErrorMsg* poSetNotiAppsErrorMsg)
       * \brief   Handles Messages of MLSetNotiAppsErrorMsg type
       * \param poSetNotiAppsErrorMsg : pointer to MLSetNotiAppsErrorMsg
       **************************************************************************/
      t_Void vHandleNotiMsg(MLSetNotiAppsErrorMsg* poSetNotiAppsErrorMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleNotiMsg(MLNotiActionRespMsg* poNotiActionRespMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleNotiMsg(MLNotiActionRespMsg* poNotiActionRespMsg)
       * \brief   Handles Messages of MLNotiActionRespMsg type
       * \param poNotiActionRespMsg : pointer to MLNotiActionRespMsg
       **************************************************************************/
      t_Void vHandleNotiMsg(MLNotiActionRespMsg* poNotiActionRespMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleNotiMsg(MLNotiDataMsg* poNotiDataMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleNotiMsg(MLNotiDataMsg* poNotiDataMsg)
       * \brief   Handles Messages of MLNotiActionRespMsg type
       * \param poNotiDataMsg : pointer to MLNotiDataMsg
       **************************************************************************/
      t_Void vHandleNotiMsg(MLNotiDataMsg* poNotiDataMsg)const;
};

#endif /* SPI_TCLMLVNCNOTIFDISPATCHER_H_ */
