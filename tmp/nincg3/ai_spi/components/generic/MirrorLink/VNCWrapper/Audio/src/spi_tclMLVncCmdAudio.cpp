/*!
 *******************************************************************************
 * \file             spi_tclMLVncCmdAudio.cpp
 * \brief            RealVNC Wrapper for Audio
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper for wrapping VNC calls for receiving
 Audio from ML Server
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 27.08.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
 30.07.2014 |  Shiva Kumar G(RBEI/ECP2)        | Fix for the Nokia X7 audio distrotion issue
 06.05.2015 |  Tejaswini HB                    | Lint Fix
 21.05.2014 |  Shiva Kumar Gurija              | Changes in determining ML version of the Phones
 27.05.2015 |  Shiva Kumar Gurija              | Lint Fix
 03.07.2015 |  Shiva Kumar Gurija              | improvements in ML Version checking
 18.08.2015 |  Shiva Kumar Gurija              | Implemetation of new interfaces for
                                                ML Dynamic Audio
 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include <sched.h>
#include "sstream"
#include "vnctypes.h"
#include "mlink_dlt.h"
#include "mlink_common.h"
#include "mlink_gst.h"
#include "spi_tclMLVncViewerDataIntf.h"
#include "StringHandler.h"
#include "spi_tclMLVncDiscovererDataIntf.h"
#include "spi_tclMLVncMsgQInterface.h"
#include "WrapperTypeDefines.h"
#include "spi_tclMLVncCmdAudio.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
#include "trcGenProj/Header/spi_tclMLVncCmdAudio.cpp.trc.h"
#endif
#endif

using namespace std;

trAudioCapabilities spi_tclMLVncCmdAudio::m_rAudioCapabilities;

t_U32 spi_tclMLVncCmdAudio::m_u32RTPPayload = 99;
t_U32 spi_tclMLVncCmdAudio::m_u32AudioIPL = 4800;

static const t_String cszDefaultRTPPayload = "99";
static const t_U32 scou32SchedulingPriority = 42;

const t_U8 cu8AudProtocolTable[] =
   { VNCAudioRouterProtocolRtpOut, VNCAudioRouterProtocolRtpIn,
      VNCAudioRouterProtocolBtA2dp, VNCAudioRouterProtocolBtHfp };

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncCmdAudio::spi_tclMLVncCmdAudio();
 ***************************************************************************/
spi_tclMLVncCmdAudio::spi_tclMLVncCmdAudio() :
   m_pAudioRouterSDK(NULL), m_pAudioRouter(NULL), m_pRTPOutContext(NULL)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::spi_tclMLVncCmdAudio Entered \n"));

   m_pAudioRouterSDK = new VNCAudioRouterSDK;
   //Assert if the m_pAudioRouterSDK is null
   SPI_NORMAL_ASSERT(NULL == m_pAudioRouterSDK);
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclMLVncCmdAudio::~spi_tclMLVncCmdAudio()
 ***************************************************************************/
spi_tclMLVncCmdAudio::~spi_tclMLVncCmdAudio()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::~spi_tclMLVncCmdAudio Entered \n"));

   m_pAudioRouter = NULL;
   m_pRTPOutContext = NULL;

   //Delete the Audio router SDK instance
   RELEASE_MEM (m_pAudioRouterSDK);
}

/**************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vGetRegisteredAudioResp
 **************************************************************************/
t_Void spi_tclMLVncCmdAudio::vGetRegisteredAudioResp(t_U32 u32DeviceHandle) const
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vGetRegisteredAudioResp Entered\n "));

   //! Send the response to registered objects

   spi_tclMLVncMsgQInterface *poMsgQInterface =
            spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLAudioSubscriResultMsg oMLAudioSubscriResultMsg;
      poMsgQInterface->bWriteMsgToQ(&oMLAudioSubscriResultMsg,
               sizeof(oMLAudioSubscriResultMsg));
   }
}

/*****************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vInitializeAudioRouterSDK()
 *****************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bInitializeAudioRouterSDK()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bInitializeAudioRouterSDK() entered \n"));
   t_Bool bRetVal = false;

   //Initialise the Audio Router SDK
   if (NULL != m_pAudioRouterSDK)
   {
      MLAudioRouterError s32SDKInitializeError = VNCAudioRouterInitialize(m_pAudioRouterSDK,
                        sizeof(VNCAudioRouterSDK));

      //If no error ,audio router initialisation is successful
      ETG_TRACE_ERR(("VNCAudioRouterInitialize  error %d\n", s32SDKInitializeError));
      bRetVal = (VNCAudioRouterErrorNone == s32SDKInitializeError);
   }

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bCreateAudioRouter(const t_U32 cou32DeviceHandle = 0)
 ***************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bCreateAudioRouter(t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bCreateAudioRouter Entered \n"));

   t_Bool bRetVal = false;
   spi_tclMLVncViewerDataIntf oViewerDataIntf;

   //Get the viewer data
   trViewerData* pViewerData = oViewerDataIntf.pGetViewerData();

   SPI_NORMAL_ASSERT(NULL == pViewerData);
   SPI_NORMAL_ASSERT(NULL == m_pAudioRouterSDK);
   //Get the viewer data
   if ((NULL != m_pAudioRouterSDK) && (NULL != pViewerData) && (NULL
            != pViewerData->pViewerSDK) && (NULL != pViewerData->pVNCViewer))
   {
      //register for the audio router callbacks
      VNCAudioRouterCallbacks oCallbacks;
      size_t callbacksSize = sizeof(VNCAudioRouterCallbacks);
      memset(&oCallbacks, 0, callbacksSize);

      oCallbacks.protocolsAvailableCb = &vGetAudioCapabilitiesCallback;
      oCallbacks.checkBtConnectionCb = &vCheckBTConnectionCallback;
      oCallbacks.linkAddedCb = &vAudioLinkAddedCallback;
      oCallbacks.linkRemovedCb = &vAudioLinkRemovedCallback;
      oCallbacks.protocolCheckErrorCb = &vGetAudioCapabilitiesErrorCallback;
      oCallbacks.linkFailedCb = &vAudioLinkAddFailedCallback;
      oCallbacks.linkRemovalFailedCb = &vAudioLinkRemovalFailedCallback;
      oCallbacks.log = &mlink_dlt_audiorouter_log_callback;

      //Create the audio router
      MLAudioRouterError s32AudioRouterError =
               m_pAudioRouterSDK->create(&oCallbacks,
                        callbacksSize,
                        (t_Void*)(const_cast<t_Char*>("")),  //Empty string is added as user data
                        pViewerData->pViewerSDK,
                        pViewerData->pVNCViewer,
                        &m_pAudioRouter);

      if (NULL != m_pAudioRouter)
      {
         m_pAudioRouterSDK->setLoggingSeverity(m_pAudioRouter,
                  VNCAudioRouterLogSeverityDebug);
      }//if(NULL != m_pAudioRouter)

      ETG_TRACE_ERR(("bCreateAudioRouter  error %d\n", s32AudioRouterError));
      bRetVal = (VNCAudioRouterErrorNone == s32AudioRouterError);
   }

   ETG_TRACE_USR1(("bCreateAudioRouter  bRetVal %d\n", bRetVal));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bSetRouterToML(const t_U32 cou32DeviceHandle))
 ***************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bSetRouterToML(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bSetRouterToML() Entered \n"));
   ETG_TRACE_USR4(("bSetRouterToML cou32DeviceHandle %d\n", cou32DeviceHandle));
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   t_Bool bRetVal = false;

   //Check for NULL pointers
   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      trvncDeviceHandles rVncHandles = oDiscDataIntf.corfrGetDiscHandles(
               cou32DeviceHandle);
      if ((NULL != rVncHandles.poDiscoverySdk) && (NULL
               != rVncHandles.poDiscoverer) && (NULL != rVncHandles.poEntity))
      {
         //Set the Audio Router for ML usage
         MLAudioRouterError s32AudioRouterError =
                  m_pAudioRouterSDK->useMirrorLinkDevice(m_pAudioRouter,
                           rVncHandles.poDiscoverySdk,
                           (t_Void*)(const_cast<VNCDiscoverySDKDiscoverer*>(rVncHandles.poDiscoverer)),
                           (t_Void*)(const_cast<VNCDiscoverySDKEntity*>(rVncHandles.poEntity)));

         ETG_TRACE_USR1(("bSetRouterToML() s32AudioRouterError %d \n", s32AudioRouterError));
         bRetVal = (VNCAudioRouterErrorNone == s32AudioRouterError);
      }
      else
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdAudio::bSetRouterToML : DiscHandles are NULL\n"));
      }
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bSetRouterToNone())
 ***************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bSetRouterToNone()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bSetRouterToNone() Entered \n"));
   t_Bool bRetVal = false;

   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Set the Audio Router for ML usage
      MLAudioRouterError s32AudioRouterError =
               m_pAudioRouterSDK->useNone(m_pAudioRouter);

      ETG_TRACE_USR1(("bSetRouterToNone() s32AudioRouterError %d \n", s32AudioRouterError));
      bRetVal = (VNCAudioRouterErrorNone == s32AudioRouterError);
   }
   return bRetVal;
}

/********************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bGetAudioCapabilities( ...)
 ********************************************************************************/

t_Bool spi_tclMLVncCmdAudio::bGetAudioCapabilities(t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bGetAudioCapabilities Entered \n"));
   t_Bool bRetVal = false;

   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Retrieve the protocols supported by the server
      MLAudioRouterError s32AudioRouterError =
               m_pAudioRouterSDK->getAvailableProtocols(m_pAudioRouter);

      ETG_TRACE_USR1(("bGetAudioCapabilities error %d \n", s32AudioRouterError));
      bRetVal = (VNCAudioRouterErrorNone == s32AudioRouterError);

   }
   return bRetVal;
}

/*******************************************************************************
 ** FUNCTION: t_Void VNCCALL spi_tclMLVncCmdAudio::vGetAudioCapabilitiesCallback(
 ** t_Void *pContext,VNCAudioRouterProtocols protocols)
 *******************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdAudio::vGetAudioCapabilitiesCallback(
         t_Void *pContext, VNCAudioRouterProtocols protocols)
{
   SPI_INTENTIONALLY_UNUSED(pContext);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vGetAudioCapabilitiesCallback() entered \n"));

   //Check the protocols supported by the server

   m_rAudioCapabilities.bBTHFPSupport = ((protocols
            & VNCAudioRouterProtocolBtHfp) != 0);
   m_rAudioCapabilities.bBTA2DPSupport = ((protocols
            & VNCAudioRouterProtocolBtA2dp) != 0);
   m_rAudioCapabilities.bRTPOutSupport = ((protocols
            & VNCAudioRouterProtocolRtpOut) != 0);
   m_rAudioCapabilities.bRTPInSupport = ((protocols
            & VNCAudioRouterProtocolRtpIn) != 0);

   ETG_TRACE_USR1(("m_rAudioCapabilities.bBTHFPSupport %d \n", m_rAudioCapabilities.bBTHFPSupport));
   ETG_TRACE_USR1(("m_rAudioCapabilities.bBTA2DPSupport %d \n", m_rAudioCapabilities.bBTA2DPSupport));
   ETG_TRACE_USR1(("m_rAudioCapabilities.bRTPOutSupport %d \n", m_rAudioCapabilities.bRTPOutSupport));
   ETG_TRACE_USR1(("m_rAudioCapabilities.bRTPInSupport %d \n", m_rAudioCapabilities.bRTPInSupport));

   spi_tclMLVncMsgQInterface *poMsgQInterface =
            spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLAudioCapabilitiesMsg oAudioCapabilitiesMsg;
      oAudioCapabilitiesMsg.rAudioCapabilities = m_rAudioCapabilities;
      poMsgQInterface->bWriteMsgToQ(&oAudioCapabilitiesMsg,
               sizeof(oAudioCapabilitiesMsg));
   }
}

/***********************************************************************************
 ** FUNCTION: t_Void VNCCALL spi_tclMLVncCmdAudio::vGetAudioCapabilitiesErrorCallback(
 t_Void *pContext,VNCAudioRouterError error)
 ***********************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdAudio::vGetAudioCapabilitiesErrorCallback(
         t_Void *pContext, VNCAudioRouterError error)
{
   SPI_INTENTIONALLY_UNUSED(pContext);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vGetAudioCapabilitiesErrorCallback() entered \n"));

   spi_tclMLVncMsgQInterface *poMsgQInterface =
            spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLAudioCapabilitiesErrorMsg oAudioCapabilitiesErrorMsg;
      oAudioCapabilitiesErrorMsg.s32AudioCapabilitiesError = error;
      poMsgQInterface->bWriteMsgToQ(&oAudioCapabilitiesErrorMsg,
               sizeof(oAudioCapabilitiesErrorMsg));
   }
}

/**************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vGetRemotingInfo
 **************************************************************************/
t_Void spi_tclMLVncCmdAudio::vGetRemotingInfo(
         trAppRemotingInfo &rfrAppRemotingInfo, const t_U32 cou32DeviceHandle)
{
   (t_Void) cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vGetRemotingInfo Entered \n"));
   StringHandler oStringUtil;

   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Get the Remoting Protocol ID
      if (true == m_rAudioCapabilities.bBTHFPSupport)
      {
         rfrAppRemotingInfo.szRemotingProtocolID = "BTHFP";
      }
      if (true == m_rAudioCapabilities.bBTA2DPSupport)
      {
         rfrAppRemotingInfo.szRemotingProtocolID = "BTA2DP";
      }
      if (true == m_rAudioCapabilities.bRTPOutSupport
               || m_rAudioCapabilities.bRTPInSupport)
      {
         rfrAppRemotingInfo.szRemotingProtocolID = "RTP";
      }
      //Get the Format
      rfrAppRemotingInfo.szRemotingFormat = "bi";
      ETG_TRACE_USR4(("RemotingFormat = %s \n",rfrAppRemotingInfo.szRemotingFormat.c_str()));

      //Get the Audio IPL value
      rfrAppRemotingInfo.u32RemotingAudioIPL
               = oStringUtil. u32ConvertStrToInt(m_pAudioRouterSDK->getParameter(m_pAudioRouter,
                        VNCAudioRouterParameterKeyRtpAudioIpl));
      ETG_TRACE_USR4(("RemotingAudioIPL = %d \n",rfrAppRemotingInfo.u32RemotingAudioIPL));

      //Get the Audio MPL value
      rfrAppRemotingInfo.u32RemotingAudioMPL
               = oStringUtil. u32ConvertStrToInt(m_pAudioRouterSDK->getParameter(m_pAudioRouter,
                        VNCAudioRouterParameterKeyRtpAudioMpl));
      ETG_TRACE_USR4(("RemotingAudioMPL = %d \n",rfrAppRemotingInfo.u32RemotingAudioMPL));
   }
}

/*****************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bSetRTPPayload(t_U32 u32RTPPayload...)
 ****************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bSetRTPPayload(t_U32 u32RTPPayload,
         t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bSetRTPPayload() entered \n"));
   t_Bool bRetVal = false;
   t_String szRTPPayload;
   StringHandler oStringUtil;
   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Convert the Payload type value to string value
      oStringUtil.vConvertIntToStr(u32RTPPayload, szRTPPayload, DECIMAL_STRING);

      //Set the RTP Payload value
      VNCAudioRouterError s32SetRTPError =
               m_pAudioRouterSDK->setParameter(m_pAudioRouter,
                        VNCAudioRouterParameterKeyRtpPayloadType,
                        szRTPPayload.c_str());

      bRetVal = (VNCAudioRouterErrorNone == s32SetRTPError);
   }
   return bRetVal;
}

/*****************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vGetRTPPayload(std::vector<t_U32>& ..)
 ****************************************************************************/
t_Void spi_tclMLVncCmdAudio::vGetRTPPayload(
         std::vector<t_U32> & rfvecRTPPayloads, const t_U32 cou32DeviceHandle)
{
   (t_Void) cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vecGetRTPPayload() entered \n"));
   StringHandler oStringUtil;

   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Get the RTP Payload type that has been previously set
      const t_Char* pcocRTPPayloadType =
               m_pAudioRouterSDK->getParameter(m_pAudioRouter,
                        VNCAudioRouterParameterKeyRtpPayloadType);
      t_Char cDelim = ',';

      //Parse the retrieved string of payload types to obtain the individual 
      //payload values
      oStringUtil.vParseString(pcocRTPPayloadType, cDelim, rfvecRTPPayloads);
   }
}

/*****************************************************************************
 ** FUNCTION: t_Bool bSetAudioIPL(t_U32 u32AudioIPL, const t_U32 cou32DeviceHandle = 0)
 ****************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bSetAudioIPL(t_U32 u32AudioIPL,
         t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bSetAudioIPL() entered \n"));
   t_Bool bRetVal = false;
   t_String szAudioIPL;
   StringHandler oStringUtil;
   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Convert the Audio IPL value to string value
      oStringUtil.vConvertIntToStr(u32AudioIPL, szAudioIPL, DECIMAL_STRING);

      //Set the Audio IPL value
      VNCAudioRouterError s32SetIPLError =
               m_pAudioRouterSDK->setParameter(m_pAudioRouter,
                        VNCAudioRouterParameterKeyRtpAudioIpl,
                        szAudioIPL.c_str());

      bRetVal = (VNCAudioRouterErrorNone == s32SetIPLError);
   }
   return bRetVal;
}

/*****************************************************************************
 ** FUNCTION:  t_U32 u32GetAudioIPL(const t_U32 cou32DeviceHandle = 0)
 ****************************************************************************/
t_U32 spi_tclMLVncCmdAudio::u32GetAudioIPL(t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::u32GetAudioIPL() entered \n"));
   t_U32 u32AudioIPL = 0;
   StringHandler oStringUtil;

   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Get the Audio IPL that has been previously set
      const t_Char* cocAudioIPL =
               m_pAudioRouterSDK->getParameter(m_pAudioRouter,
                        VNCAudioRouterParameterKeyRtpAudioIpl);

      //Conver the retrieved string value to integer 
      u32AudioIPL = oStringUtil.u32ConvertStrToInt(cocAudioIPL);
   }
   return u32AudioIPL;
}

/***************************************************************************
 ** FUNCTION:  t_Bool bCheckBTConnection(const t_U32 cou32DeviceHandle = 0)
 ***************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bCheckBTConnection(t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bCheckBTConnection Entered \n"));
   t_Bool bRetVal = false;
   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Check the existing BT connection
      MLAudioRouterError s32AudioRouterError =
               m_pAudioRouterSDK->checkBtConnection(m_pAudioRouter);

      bRetVal = (VNCAudioRouterErrorNone == s32AudioRouterError);
   }
   return bRetVal;
}

/************************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vCheckBTConnectionCallback
 ***********************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdAudio::vCheckBTConnectionCallback(
         t_Void *pContext, VNCAudioRouterError error,
         VNCAudioRouterError btError, vnc_int32_t btEnabled,
         VNCAudioRouterError bondedProfilesError,
         VNCAudioRouterProtocols bondedBtProfiles,
         VNCAudioRouterError connectedProfilesError,
         VNCAudioRouterProtocols connectedBtProfiles, const t_Char *btAddress)
{
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(error);
   SPI_INTENTIONALLY_UNUSED(connectedBtProfiles);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vCheckBTConnectionCallback Entered \n"));
   trBtInfo orBtInfo;

   //Populate the BT info to a structure to be passed to SPI
   orBtInfo.s32BtError = btError;
   orBtInfo.u32BtEnabled = btEnabled;
   orBtInfo.s32BondedProfilesError = bondedProfilesError;
   orBtInfo.s32BondedBtProfiles = bondedBtProfiles;
   orBtInfo.s32ConnectedProfilesError = connectedProfilesError;
   orBtInfo.s32ConnectedBtProfiles = connectedProfilesError;

   strncpy(orBtInfo.pcBtAddress, btAddress, sizeof(btAddress));

   spi_tclMLVncMsgQInterface *poMsgQInterface =
            spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLPostBtInfoMsg oMLPostBtInfoMsg;
      if (NULL != oMLPostBtInfoMsg.prBtInfo)
      {
         *(oMLPostBtInfoMsg.prBtInfo) = orBtInfo;
      }
      poMsgQInterface->bWriteMsgToQ(&oMLPostBtInfoMsg, sizeof(oMLPostBtInfoMsg));
   }
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bAddAudioLinks
 ***************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bAddAudioLinks(const t_U32 cou32DeviceHandle)
{
   t_Bool bRetVal = false;

   trAppRemotingInfo rAppRemotingInfo ;
   vGetRemotingInfo(rAppRemotingInfo);

   //Check for null pointers
   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Add the audio router links
      MLAudioRouterError s32RTPOutAddLinkReq = VNCAudioRouterErrorNone;
      MLAudioRouterError s32RTPInAddLinkReq = VNCAudioRouterErrorNone;
      MLAudioRouterError s32BTHFPAddLinkReq = VNCAudioRouterErrorNone;
      MLAudioRouterError s32BTA2DPAddLinkReq = VNCAudioRouterErrorNone;

      //Pay load type supported by the client must be set before the addLinks is called.
      //Here Set the payload type to 99 (Default payload type which must be supported by ML Client & ML Server).

      //To extend the support for Payload Type 98, We should check whether the ML Server supports Payload type 98 in 
      //the audio capabilities of the device. Based on the capabilities payload type should be set using setParameter()
      //and the same payload type value should be passed as a parameter to gst_startrtpstreaming()(ADIT interface)

      MLAudioRouterError s32SetRTPPayLoadError = m_pAudioRouterSDK->setParameter(m_pAudioRouter, 
         VNCAudioRouterParameterKeyRtpPayloadType, cszDefaultRTPPayload.c_str());

      bRetVal = (VNCAudioRouterErrorNone == s32SetRTPPayLoadError);


      if (true == m_rAudioCapabilities.bRTPOutSupport)
      {
         s32RTPOutAddLinkReq = m_pAudioRouterSDK->addLinks(m_pAudioRouter,
                  VNCAudioRouterProtocolRtpOut);
         bRetVal = (VNCAudioRouterErrorNone == s32RTPOutAddLinkReq);
      }

      //@todo - check whether client supports RTPIn, before addling links for RTPIn.
      if (true == m_rAudioCapabilities.bRTPInSupport)
      {
         s32RTPInAddLinkReq = m_pAudioRouterSDK->addLinks(m_pAudioRouter,
                  VNCAudioRouterProtocolRtpIn);
         bRetVal = (VNCAudioRouterErrorNone == s32RTPInAddLinkReq) && bRetVal;
      }

      if(false == bRetVal)
      {
         ETG_TRACE_ERR(("bAddAudioLinks: Error in placing request s32RTPOutAddLinkReq %d\n", s32RTPOutAddLinkReq));
         ETG_TRACE_ERR(("bAddAudioLinks: Error in placing request s32RTPInAddLinkReq %d\n", s32RTPInAddLinkReq));
         ETG_TRACE_ERR(("bAddAudioLinks: Error in placing request s32BTHFPAddLinkReq %d\n", s32BTHFPAddLinkReq));
         ETG_TRACE_ERR(("bAddAudioLinks: Error in placing request s32BTA2DPAddLinkReq %d\n", s32BTA2DPAddLinkReq));
         ETG_TRACE_ERR(("bAddAudioLinks: Error in setting payload type:%d", s32SetRTPPayLoadError));
      }
   }
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bAddAudioLinks Request: cou32DeviceHandle = %d bRetVal = %d \n",
            cou32DeviceHandle, bRetVal));

   return bRetVal;
}

/*****************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vAudioLinkAddedCallback(t_Void *pContext,
 ** VNCAudioRouterProtocols protocols, const t_Void *pvAudLinkInfo)
 *****************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdAudio::vAudioLinkAddedCallback(t_Void *pContext,
         VNCAudioRouterProtocols protocols, const t_Void *pvAudLinkInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vAudioLinkAddedCallback Entered \n"));
   ETG_TRACE_USR4(("vAudioLinkAddedCallback protocols %d \n", protocols));

   trMLAudioLinkInfo rMLAudLinkInfo;

   t_Bool bRTPInAvail = false;
   t_Bool bRTPOutAvail = false;

   if ((protocols & VNCAudioRouterProtocolRtpOut) && (NULL != pvAudLinkInfo))
   {
      bRTPOutAvail = true;
      vExtractAudRtpOutInfo(pvAudLinkInfo, rMLAudLinkInfo);
   }

   if ((protocols & VNCAudioRouterProtocolRtpIn) && (NULL != pvAudLinkInfo))
   {
      bRTPInAvail = true;
      vExtractAudRtpInInfo(pvAudLinkInfo, rMLAudLinkInfo);
   }

   spi_tclMLVncMsgQInterface *poMsgQInterface =
            spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLAudioRouterExtraInfoMsg oMLAudioRouterExtraInfoMsg;
      if (NULL != oMLAudioRouterExtraInfoMsg.prAudioLinkInfo)
      {
         *(oMLAudioRouterExtraInfoMsg.prAudioLinkInfo) = rMLAudLinkInfo;
      }
      oMLAudioRouterExtraInfoMsg.pContext = pContext;
      oMLAudioRouterExtraInfoMsg.m_bRTPInAvail = bRTPInAvail;
      oMLAudioRouterExtraInfoMsg.m_bRTPOutAvail = bRTPOutAvail;
      poMsgQInterface->bWriteMsgToQ(&oMLAudioRouterExtraInfoMsg,
               sizeof(oMLAudioRouterExtraInfoMsg));
   }
}

/*****************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vExtractAudRtpOutInfo(const t_Void *pvAudLinkInfo,
 trMLAudioLinkInfo& rfrAudLinkInfo)
 *****************************************************************************/
t_Void spi_tclMLVncCmdAudio::vExtractAudRtpOutInfo(const t_Void *pvAudLinkInfo,
         trMLAudioLinkInfo& rfrAudLinkInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vExtractAudRtpOutInfo Entered \n"));

   if (NULL != pvAudLinkInfo)
   {
      VNCAudioRouterExtraInfoRtpOut* prRtpOutInfo =
               (VNCAudioRouterExtraInfoRtpOut*) (const_cast<t_Void*>(pvAudLinkInfo));

      //Retrieve the URI value
      ETG_TRACE_USR4(("Received RTPOut Info : uri %s \n", prRtpOutInfo->uri));
      rfrAudLinkInfo.rRtpOutExtraInfo.szUri = prRtpOutInfo->uri;
   }//End of if(NULL != pvAudLinkInfo)
}//t_Void spi_tclMLVncCmdAudio::vExtractAudRtpOutInfo(..)


/*****************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vExtractAudRtpInInfo(const t_Void *pvAudLinkInfo,
 trMLAudioLinkInfo& rfrAudLinkInfo)
 *****************************************************************************/
t_Void spi_tclMLVncCmdAudio::vExtractAudRtpInInfo(const t_Void *pvAudLinkInfo,
         trMLAudioLinkInfo& rfrAudLinkInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vExtractAudRtpInInfo Entered \n"));

   if (NULL != pvAudLinkInfo)
   {
      VNCAudioRouterExtraInfoRtpIn* prRtpInInfo =
               (VNCAudioRouterExtraInfoRtpIn*) (const_cast<t_Void*>(pvAudLinkInfo));

      ETG_TRACE_USR4(("Received RTPIn Info : uri %s \n", prRtpInInfo->uri));
      ETG_TRACE_USR4(("PayloadTypes %s \n", prRtpInInfo->payloadTypes));

      /*Retrieve the URI for RTP IN.*/
      rfrAudLinkInfo.rRtpInExtraInfo.szUri = prRtpInInfo->uri;

      /*Retrieve the Payload Types for RTP IN.*/
      rfrAudLinkInfo.rRtpInExtraInfo.szPayloadTypes = prRtpInInfo->payloadTypes;

      /*Retrieve the Audio IPL for RTP IN.*/
      /*If AudioIPL is empty, or NULL, the default value, as
       specified by the MirrorLink specification, is assumed*/
      if (NULL != prRtpInInfo->audioIpl)
      {
         ETG_TRACE_USR4(("AudioIPL %s \n", prRtpInInfo->audioIpl));
         rfrAudLinkInfo.rRtpInExtraInfo.szAudioIpl = prRtpInInfo->audioIpl;
      }
      else
      {
         rfrAudLinkInfo.rRtpInExtraInfo.szAudioIpl = "4800";
      }//End of if(NULL != prRtpInInfo->audioIpl)

      /*Retrieve the Audio MPL for RTP IN.*/
      /*If AudioMPL is empty, or NULL, the default value, as
       specified by the MirrorLink specification, is assumed*/
      if (NULL != prRtpInInfo->audioMpl)
      {
         ETG_TRACE_USR4(("AudioMPL %s \n", prRtpInInfo->audioMpl));
         rfrAudLinkInfo.rRtpInExtraInfo.szAudioMpl = prRtpInInfo->audioMpl;
      }
      else
      {
         rfrAudLinkInfo.rRtpInExtraInfo.szAudioMpl = "9600";
      }//End of if(NULL != prRtpInInfo->audioMpl)

   }//End of if(NULL != pvAudLinkInfo)
}//t_Void spi_tclMLVncCmdAudio::vExtractAudRtpInInfo(..)


/***************************************************************************
 ** FUNCTION: t_Bool bStartRTPOutStreaming(t_Void* pContext,
 * trMLAudioLinkInfo rAudioLinkInfo)
 ***************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bStartRTPOutStreaming(t_Void* pContext,
         trMLAudioLinkInfo rAudioLinkInfo, t_String szDeviceName)
{

   ETG_TRACE_USR4(("bStartRTPOutStreaming: szUri %s ", rAudioLinkInfo.rRtpOutExtraInfo.szUri.c_str()));
   ETG_TRACE_USR4(("bStartRTPOutStreaming: m_u32RTPPayload %d m_u32AudioIPL %d", m_u32RTPPayload,m_u32AudioIPL));

   std::stringstream ssDeviceName;
   ssDeviceName << "alsasink device=" << szDeviceName.c_str();
   ETG_TRACE_USR4(("bStartRTPOutStreaming: szDeviceName %s \n", ssDeviceName.str().c_str()));

   DltContext* pDltContext = NULL;
   t_Bool bRetVal = false;

   //Disabled currently 
   spi_tclMLVncViewerDataIntf oViewerData;
   mlink_dlt_context* pm_dlt_context = oViewerData.pGetDLTContext();
   if (NULL != pm_dlt_context)
   {
      pDltContext = mlink_dlt_get_dlt_context(pm_dlt_context);
      //Added for debugging purpose
      if (NULL != pDltContext)
      {
         ETG_TRACE_USR4(("bStartRTPStreaming() pm_dlt_context = %d \n", pm_dlt_context));
      }//if (NULL != pDltContext)
   }//if (NULL != pm_dlt_context)


   m_oRTPOutCntxtLock.s16Lock();
   if(NULL == m_pRTPOutContext)
   {
   m_pRTPOutContext = mlink_gst_start_rtp_out_streaming(rAudioLinkInfo.rRtpOutExtraInfo.szUri.c_str(),
                     const_cast<t_Char*>(ssDeviceName.str().c_str()),
                     m_u32RTPPayload,
                     m_u32AudioIPL,
                     &vGst_rtp_out_error_callback,
                     &vGst_rtp_out_data_callback,
                     &vGst_streaming_thread_callback,
                     pContext,
                     pDltContext);
   }
   bRetVal = (NULL == m_pRTPOutContext) ? (bRetVal) : (true);
   ETG_TRACE_USR4(("spi_tclMLVncCmdAudio::bStartRTPStreaming  m_pRTPOutContext = %d \n", m_pRTPOutContext));
   m_oRTPOutCntxtLock.vUnlock();

   return bRetVal;
}

/********************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::bStopRTPOutStreaming()
 ********************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bStopRTPOutStreaming()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bStopRTPOutStreaming()"));
   t_Bool bRetVal = false;

   m_oRTPOutCntxtLock.s16Lock();
   ETG_TRACE_USR2(("spi_tclMLVncCmdAudio::bStopRTPOutStreaming  m_pRTPOutContext = %d \n", m_pRTPOutContext));
   if (NULL != m_pRTPOutContext)
   {
      mlink_gst_stop_rtp_out_streaming(m_pRTPOutContext);
      m_pRTPOutContext = NULL;
      bRetVal = true;
      ETG_TRACE_USR2(("spi_tclMLVncCmdAudio::bStopRTPOutStreaming request is sent"));
   }//if (NULL != m_pRTPOutContext)
   m_oRTPOutCntxtLock.vUnlock();

   return bRetVal;

}

/**************************************************************************
 ** FUNCTION: t_Void vGst_rtp_out_error_callback
 **************************************************************************/
t_Void spi_tclMLVncCmdAudio::vGst_rtp_out_error_callback(t_Void * p_user_ctx,
         mlink_rtp_out_context * p_rtp_out_ctx, t_Char * pMessage)
{
   SPI_INTENTIONALLY_UNUSED(p_user_ctx);
   SPI_INTENTIONALLY_UNUSED(p_rtp_out_ctx);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vGst_rtp_out_error_callback()Entered "));

   if(NULL != pMessage)
   {
      ETG_TRACE_USR2(("spi_tclMLVncCmdAudio::pMessage %s", pMessage));
   }//if(NULL != pMessage)
}

//#define MAX_SIZE_EXTENSION 100

/****************************************************************************
 ** FUNCTION: t_Void vGst_rtp_out_data_callback
 ****************************************************************************/
t_Void spi_tclMLVncCmdAudio::vGst_rtp_out_data_callback(t_Void * p_user_ctx,
         t_U32 * pu32Extension, t_U16 u16Extension_size, t_U8 u8Markerbit)
{
   SPI_INTENTIONALLY_UNUSED(p_user_ctx);

   spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if(NULL != poMsgQInterface)
   {
      MLAudioRTPOutData oRTPOutData;
      oRTPOutData.m_u16RTPOutDataSize = u16Extension_size ;
      oRTPOutData.m_u8MarkerBit = u8Markerbit ; 
      if(u16Extension_size>0)
      {
         oRTPOutData.m_pu32RTPOutData = new (std::nothrow)t_U32[u16Extension_size];
         if(NULL != oRTPOutData.m_pu32RTPOutData)
         {
            memcpy((t_Void*) oRTPOutData.m_pu32RTPOutData, pu32Extension, (u16Extension_size*sizeof(t_U32)));
         }//if(NULL != oRTPOutData.m_pu32RTPOutData)
      }//if(u16Extension_size>0)      
      poMsgQInterface->bWriteMsgToQ(&oRTPOutData,sizeof(oRTPOutData));
   }//if(NULL != poMsgQInterface)
}

/****************************************************************************
 ** FUNCTION: t_Void vGst_streaming_thread_callback
 ****************************************************************************/
t_Void spi_tclMLVncCmdAudio::vGst_streaming_thread_callback(t_Void* pUserContext, t_Char* pName)
{
   SPI_INTENTIONALLY_UNUSED(pUserContext);
   if(NULL == pName)
   {
      ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vGst_streaming_thread_callback()"));
   }//if(NULL == pName)
   else
   {
      ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vGst_streaming_thread_callback-%s",pName));
   }
   SPI_INTENTIONALLY_UNUSED(pUserContext);
	
   struct sched_param sched_param_setting;

   //! get current parameter for scheduling
   t_S32 s32Error = sched_getparam(0, &sched_param_setting);

   if(0 == s32Error)
   {
      //! reset the scheduling priority
      sched_param_setting.sched_priority = scou32SchedulingPriority;
      //! Set realtime scheduling for audio streaming thread and set priority to 42
      //! PID is set to 0 so that scheduling priorities are set for the calling thread
      s32Error = sched_setscheduler(0, SCHED_FIFO, &sched_param_setting);
   }

   if(0 != s32Error)
   {
      ETG_TRACE_USR1(("Setting scheduling priority failed priority = %d", 
         sched_param_setting.sched_priority));
   }
}

/**************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vAudioLinkAddFailedCallback
 **************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdAudio::vAudioLinkAddFailedCallback(
         t_Void *pContext, VNCAudioRouterProtocols protocols,
         VNCAudioRouterError error, const t_Void *extraInfo)
{
   (t_Void) extraInfo;
   SPI_INTENTIONALLY_UNUSED(pContext);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vAudioLinkAddFailedCallback() "));

   ETG_TRACE_USR4(("vAudioLinkAddFailedCallback() protocols %d error = %d", 
      protocols, error));

   spi_tclMLVncMsgQInterface *poMsgQInterface =
            spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLAudioLinkAddErrorMsg oMLAudioLinkAddErrorMsg;
      oMLAudioLinkAddErrorMsg.u16LinkAddError = error;
      poMsgQInterface->bWriteMsgToQ(&oMLAudioLinkAddErrorMsg,
               sizeof(oMLAudioLinkAddErrorMsg));
   }
}

/***************************************************************************
 ** t_Bool spi_tclMLVncCmdAudio::bRemoveAudioLinks
 ***************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bRemoveAudioLinks(tenAudioLink enLink,
         t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::bRemoveAudioLinks Entered "));
   t_Bool bRetVal = false;

   //Check for NULL pointers
   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Remove the audio links added
      MLAudioRouterError s32AudioRouterError =
               m_pAudioRouterSDK->removeLinks(m_pAudioRouter,
                        cu8AudProtocolTable[enLink]);

      bRetVal = (VNCAudioRouterErrorNone == s32AudioRouterError);
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vAudioLinkRemovedCallback()
 **************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdAudio::vAudioLinkRemovedCallback(
         t_Void *pContext, VNCAudioRouterProtocols protocols,
         const t_Void *extraInfo)
{
   (t_Void) extraInfo;
   SPI_INTENTIONALLY_UNUSED(pContext);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vAudioLinkRemovedCallback Entered \n"));

   ETG_TRACE_USR4(("vAudioLinkRemovedCallback protocols %d \n", protocols));
}

/*************************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vAudioLinkRemovalFailedCallback
 *************************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdAudio::vAudioLinkRemovalFailedCallback(
         t_Void *pContext, VNCAudioRouterProtocols protocols,
         VNCAudioRouterError error, const t_Void *extraInfo)
{
   (t_Void) extraInfo;
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(protocols);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vAudioLinkRemovalFailedCb Entered \n"));

   spi_tclMLVncMsgQInterface *poMsgQInterface =
            spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLAudioLInkRemoveErrorMsg oMLAudioLInkRemoveErrorMsg;
      oMLAudioLInkRemoveErrorMsg.u16LinkRemoveError = error;
      poMsgQInterface->bWriteMsgToQ(&oMLAudioLInkRemoveErrorMsg,
               sizeof(oMLAudioLInkRemoveErrorMsg));
   }
}

/***************************************************************************
 ** FUNCTION: t_Bool vDestroyAudioRouter(t_U32 u32DeviceHandle = 0)
 ***************************************************************************/
t_Bool spi_tclMLVncCmdAudio::bDestroyAudioRouter(t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vDestroyAudioRouter Entered \n"));
   t_Bool bRetVal = false;

   //Check for NULL pointers
   if ((NULL != m_pAudioRouterSDK) && (NULL != m_pAudioRouter))
   {
      //Destroy the audio router
      m_pAudioRouterSDK->destroy(m_pAudioRouter);
      m_pAudioRouter = NULL;
      bRetVal = true;
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdAudio::vFetchDeviceVersion()
***************************************************************************/
t_Void spi_tclMLVncCmdAudio::vFetchDeviceVersion(const t_U32 cou32DeviceHandle,trVersionInfo& rfrVersionInfo)
{
   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rvncDeviceHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DeviceHandle);

   if((NULL != rvncDeviceHandles.poDiscoverySdk) &&(NULL != rvncDeviceHandles.poDiscoverer)&&(NULL != rvncDeviceHandles.poEntity))
   {
      //VNCDiscoverySDKMLRootMajorVersion
      t_U32 u32MLMajorVer = u32FetchKeyValue(rvncDeviceHandles.poDiscoverySdk,
         rvncDeviceHandles.poDiscoverer, rvncDeviceHandles.poEntity, VNCDiscoverySDKMLRootMajorVersion);

      rfrVersionInfo.u32MajorVersion = ( bIsInvalidMLMajorVersion(u32MLMajorVer) )
         ?(scou32ML_Major_Version_1):(u32MLMajorVer);

      rfrVersionInfo.u32MinorVersion = u32FetchKeyValue(rvncDeviceHandles.poDiscoverySdk,
         rvncDeviceHandles.poDiscoverer, rvncDeviceHandles.poEntity, VNCDiscoverySDKMLRootMinorVersion);

      rfrVersionInfo.u32PatchVersion=0;

      ETG_TRACE_USR2(("spi_tclMLVncCmdAudio::vFetchDeviceVersion:Dev-0x%x Major-%d Minor-%d\n",
         cou32DeviceHandle,rfrVersionInfo.u32MajorVersion,rfrVersionInfo.u32MinorVersion));
   }
}

/***************************************************************************
** FUNCTION:  t_U32 spi_tclMLVncCmdAudio::u32FetchKeyValue()
***************************************************************************/
t_U32 spi_tclMLVncCmdAudio::u32FetchKeyValue(const VNCDiscoverySDK* poDiscSDK, 
                                           const VNCDiscoverySDKDiscoverer* poDisc,
                                           const VNCDiscoverySDKEntity* poEntity,
                                           const t_Char *pczKey,
                                           t_S32 s32TimeOut)
{

   t_U32 u32RetVal = 0;

   t_Char *czResult = NULL ;
   if((NULL != poDiscSDK)&&(NULL != poEntity)&&(NULL != poDisc)&&(NULL != pczKey))
   {
      //Get the error code, if any
      t_S32 s32Error = poDiscSDK->fetchEntityValueBlocking(const_cast<VNCDiscoverySDKDiscoverer*>(poDisc),
         const_cast<VNCDiscoverySDKEntity*>(poEntity), pczKey, &czResult, s32TimeOut);

      if((cs32MLDiscoveryErrorNone == s32Error)&&(NULL != czResult))
      {
         //String Utility Handler
         StringHandler oStrUtil(czResult);
         u32RetVal = oStrUtil.u32ConvertStrToInt();
         ETG_TRACE_USR4((" spi_tclMLVncViewer::u32FetchKeyValue:czResult - %s\n",czResult));
         poDiscSDK->freeString(czResult);
         czResult = NULL;
      }//if((cs32MLDiscoveryErrorNone == s32Error)&&(NULL != czResult))
   }//if((NULL != poDiscSDK)&&(NULL != poEntity)&&(NULL != 

   return u32RetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdAudio::vConnectAudioDevice(...)
***************************************************************************/
t_Void spi_tclMLVncCmdAudio::vConnectAudioOutDevice(t_String szAudioDev)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vConnectAudioOutDevice:ALSA Device-%s",szAudioDev.c_str()));

   m_oRTPOutCntxtLock.s16Lock();
   if(NULL != m_pRTPOutContext)
   {
      /*****************************************************************************************************
      MLINK_GST_RAMP_NONE will result in no ramp but an immediate change of volume at the �end of the ramp�.
      MLINK_GST_RAMP_LINEAR will result in a linear change of volume from the ramp start until the ramp end.
      MLINK_GST_RAMP_CUBIC will result in a cubic spline interpolation from the ramp start until the ramp end.

      In the current implementation,MLINK_GST_RAMP_CUBIC gives is very similar result like MLINK_GST_RAMP_LINEAR.
      ADIT recommended  to use MLINK_GST_RAMP_LINEAR  or let the ADR3 apply fades(and tell the GStreamer connectivity 
      layer to not apply fades: fade_out_ms = 0 / fade_in_ms = 0)
      ********************************************************************************************************/

      mlink_gst_connect_audio_device(m_pRTPOutContext,
         (gchar*)(const_cast<t_Char*>(szAudioDev.c_str())),
         0,MLINK_GST_RAMP_LINEAR);

   }//if(NULL != m_pRTPOutContext)
   m_oRTPOutCntxtLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdAudio::vDisconnectAudioOutDevice()
***************************************************************************/
t_Void spi_tclMLVncCmdAudio::vDisconnectAudioOutDevice()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdAudio::vDisconnectAudioOutDevice()"));

   m_oRTPOutCntxtLock.s16Lock();

   if(NULL != m_pRTPOutContext)
   {
      mlink_gst_disconnect_audio_device(m_pRTPOutContext,0,MLINK_GST_RAMP_LINEAR);
   }//if(NULL != m_pRTPOutContext)

   m_oRTPOutCntxtLock.vUnlock();
}

/////////////////////////////////////////// EOF ////////////////////////////////////////