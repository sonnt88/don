/*!
 *******************************************************************************
 * \file              spi_tclMLVncCmdAudio.h
 * \brief             RealVNC Wrapper for Audio
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper for wrapping VNC calls for receiving
 Audio from ML Server
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 27.08.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
 18.08.2015 |  Shiva Kumar Gurija              | Implemetation of new interfaces for
                                                ML Dynamic Audio
 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMLVNCCMDAUDIO_H_
#define SPI_TCLMLVNCCMDAUDIO_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include <set>
#include "vncviewersdk.h"
#include "vncdiscoverysdk.h"
#include "vncmirrorlinkkeys.h"
#include "vncaudiorouter.h"
#include "BaseTypes.h"
#include "SPITypes.h"
#include "WrapperTypeDefines.h"
#include "Lock.h"

struct mlink_dlt_context;
struct mlink_adapter_context;
struct mlink_rtp_out_context;

class spi_tclMLVncViewerDataIntf;
class RespBase;
class spi_tclMLVncDiscovererDataIntf;

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLVncCmdAudio
 * \brief This class wraps the RealVNC SDK calls to implement the
 *        audio service provided by MLServer.
 *
 */

class spi_tclMLVncCmdAudio
{

   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncCmdAudio::spi_tclMLVncCmdAudio();
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncCmdAudio()
       * \brief   Default Constructor
       * \sa      ~spi_tclMLVncCmdAudio()
       **************************************************************************/
      spi_tclMLVncCmdAudio();


      /***************************************************************************
       ** FUNCTION:  virtual spi_tclMLVncCmdAudio::~spi_tclMLVncCmdAudio()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclMLVncCmdAudio()
       * \brief   Destructor
       * \sa      spi_tclMLVncCmdAudio()
       **************************************************************************/
      virtual ~spi_tclMLVncCmdAudio();


      /**************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vGetRegisteredAudioResp
       **************************************************************************/
      /*!
       * \fn      vGetRegisteredAudioResp(const t_U32 cou32DeviceHandle = 0)
       * \brief   Register for audio
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vGetRegisteredAudioResp(const t_U32 cou32DeviceHandle = 0) const;


      /*****************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdAudio::bInitializeAudioRouterSDK()
       *****************************************************************************/
      /*!
       * \fn      bInitializeAudioRouterSDK()
       * \brief   Initialize the Audio Router SDK
       * \param   NONE
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bInitializeAudioRouterSDK();


      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bCreateAudioRouter(const t_U32 cou32DeviceHandle = 0)
       ***************************************************************************/
      /*!
       * \fn      bCreateAudioRouter(const t_U32 cou32DeviceHandle = 0
       * \brief   Create Audio router
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bCreateAudioRouter(const t_U32 cou32DeviceHandle = 0);


      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bDestroyAudioRouter(...)
       ***************************************************************************/
      /*!
       * \fn      t_Bool bDestroyAudioRouter(t_U32 u32DeviceHandle = 0)
       * \brief   Destroy Audio router
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bDestroyAudioRouter(t_U32 u32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bSetRouterToML(t_U32 u32DeviceId)
       ***************************************************************************/
      /*!
       * \fn      bSetRouterToML(t_U32 u32DeviceId)
       * \brief   Set the Audio Router to ML usage
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bSetRouterToML(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bSetRouterToNone()
       ***************************************************************************/
      /*!
       * \fn      bSetRouterToNone()
       * \brief   Set the Audio Router to stop any usage mode
       * \param   NONE
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bSetRouterToNone();

      /********************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bGetAudioCapabilities
       ********************************************************************************/
      /*!
       * \fn      bGetAudioCapabilities(const t_U32 cou32DeviceHandle = 0)
       * \brief   Fetches the protocols supported by the ML server.
       *          This call is asynchronous and the result is available in callback
       *          vPostAvailableAudioRouterProtocols().
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       * \sa     vPostAvailableAudioRouterProtocols()
       **************************************************************************/
      t_Bool bGetAudioCapabilities(const t_U32 cou32DeviceHandle = 0);

      /**************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vGetRemotingInfo()
       **************************************************************************/
      /*!
       * \fn      vGetRemotingInfo()
       * \brief   Get the Remoting Info of the protocols supported
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \param  rfrAppRemotingInfo: [OUT] Remoting Info of the protocols supported
       **************************************************************************/
      t_Void vGetRemotingInfo(trAppRemotingInfo &rfrAppRemotingInfo,
               const t_U32 cou32DeviceHandle = 0);

      /*****************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bSetRTPPayload(t_U32 u32RTPPayload...)
       ****************************************************************************/
      /*!
       * \fn       bSetRTPPayload(t_U32 u32RTPPayload, const t_U32 cou32DeviceHandle = 0)
       * \brief   Set the RTP Payload value
       * \param   u32RTPPayload: [IN]RTP Payload value
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bSetRTPPayload(t_U32 u32RTPPayload, const t_U32 cou32DeviceHandle = 0);

      /*****************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdAudio::vGetRTPPayload(std::vector<t_U32>& ..)
       ****************************************************************************/
      /*!
       * \fn      std::vector<t_U32>& vecGetRTPPayload(const t_U32 cou32DeviceHandle = 0);
       * \brief   Get the RTP Payload value
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \param  rfvecRTPPayloads:[OUT]Vector containing supported Payload types
       **************************************************************************/
      t_Void vGetRTPPayload(std::vector<t_U32> & rfvecRTPPayloads,
               const t_U32 cou32DeviceHandle = 0);

      /*****************************************************************************
       ** FUNCTION: t_Bool bSetAudioIPL(t_U32 u32AudioIPL, const t_U32 cou32DeviceHandle = 0)
       ****************************************************************************/
      /*!
       * \fn      t_Bool bSetAudioIPL(t_U32 u32AudioIPL, const t_U32 cou32DeviceHandle = 0)
       * \brief   Set the Audio IPL value
       * \param   u32AudioIPL: [IN]Audio IPL value
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bSetAudioIPL(t_U32 u32AudioIPL, const t_U32 cou32DeviceHandle = 0);

      /*****************************************************************************
       ** FUNCTION:  t_U32 u32GetAudioIPL(const t_U32 cou32DeviceHandle = 0)
       ****************************************************************************/
      /*!
       * \fn       t_U32 u32GetAudioIPL(const t_U32 cou32DeviceHandle = 0)
       * \brief   Get the Audio IPL value
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_U32
       **************************************************************************/
      t_U32 u32GetAudioIPL(const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:  t_Bool bCheckBTConnection(const t_U32 cou32DeviceHandle = 0)
       ***************************************************************************/
      /*!
       * \fn       t_Bool bCheckBTConnection(const t_U32 cou32DeviceHandle = 0)
       * \brief   Checks existing connections between the mobile device and the
       *          Bluetooth audio device.
       *          This call is asynchronous and the result is available in callback
       *          vCheckBTConnectionCallback()
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       * \sa      vCheckBTConnectionCallback
       **************************************************************************/
      t_Bool bCheckBTConnection(const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bAddAudioLinks
       ***************************************************************************/
      /*!
       * \fn     bAddAudioLinks(t_String szDeviceName, tenAudioLink enLink,
       *                     const t_U32 cou32DeviceHandle = 0)
       * \brief   Creates connections between the mobile device and audio device
       *          for the protocols requested
       *          This call is asynchronous and the result is available in callback
       *          vLinkAddedCallback()
       * \param   szDeviceName : [IN]Playback device name
       * \param   enAudioDir : [IN] Indicates audio stream direction
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       * \sa      vLinkAddedCallback
       **************************************************************************/
      t_Bool bAddAudioLinks(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** t_Bool spi_tclMLVncCmdAudio::bRemoveAudioLinks
       ***************************************************************************/
      /*!
       * \fn      bRemoveAudioLinks(MLAudioRouterProtocols enAudioRouterProtocols,
       *                                    const t_U32 cou32DeviceHandle = 0)
       * \brief   Removes connections between the mobile device and audio device
       *          for the protocols requested
       *          This call is asynchronous and the result is available in callback
       *          vAudioLinkRemovedCallback();
       * \param   enAudioRouterProtocols : [IN] Bitmask of the protocols for which
       *          the audio links should be removed
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       * \sa     vAudioLinkRemovedCallback()
       **************************************************************************/
      t_Bool bRemoveAudioLinks(tenAudioLink enLink,
               const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdAudio::bStartRTPOutStreaming
       ***************************************************************************/
      /*!
       * \fn      bStartRTPOutStreaming(t_Void* pContext,
       *         trMLAudioRouterExtraInfo oAudioRouterExtraInfo, t_String szDeviceName)
       * \brief   Method to send a request to create gstreamer pipeline for Audio Out and
       *          and render audio on the given ALSA Device name
       * \param   pContext : [IN] Context Pointer
       * \param   oAudioRouterExtraInfo: [IN]Extra Info about the protocols
       * \retval  t_Bool
       * \sa      vLinkAddedCallback()
       **************************************************************************/
      t_Bool bStartRTPOutStreaming(t_Void* pContext,
               trMLAudioLinkInfo rAudioLinkInfo, t_String szDeviceName);

      /********************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdAudio::bStopRTPOutStreaming()
       ********************************************************************************/
      /*!
       * \fn      bStopRTPOutStreaming()
       * \brief   Stop the RTP Out streaming
       * \param   NONE
       * \retval  NONE
       **************************************************************************/
      t_Bool bStopRTPOutStreaming();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdAudio::vFetchDeviceVersion(..)
    ***************************************************************************/
    /*!
    * \fn      vFetchDeviceVersion(const t_U32 cou32DevID,trVersionInfo& rfrVersionInfo)
    * \brief   To get the ML Device version info
    * \param   cou32DevID     : [IN] Unique Device ID
    * \param   rfrVersionInfo : [IN] Version Information
    * \retval  t_Void 
    **************************************************************************/
    t_Void vFetchDeviceVersion(const t_U32 cou32DevID,trVersionInfo& rfrVersionInfo);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdAudio::vConnectAudioOutDevice(...)
    ***************************************************************************/
    /*!
    * \fn      t_Void vConnectAudioOutDevice(t_String szAudioDev)
    * \brief   To send a request to connect ALSA Device to the Gstreamer pipeline
    * \param   szAudioDev     : [IN] ALSA Device name to be used
    * \retval  t_Void 
    **************************************************************************/
    t_Void vConnectAudioOutDevice(t_String szAudioDev);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdAudio::vDisconnectAudioOutDevice()
    ***************************************************************************/
    /*!
    * \fn      t_Void vDisconnectAudioOutDevice()
    * \brief   To send a request to disconnect the connected ALSA Device
    * \retval  t_Void 
    **************************************************************************/
    t_Void vDisconnectAudioOutDevice();

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/
      /************************************************************************************
       ** FUNCTION:  t_Void vExtractAudRtpOutInfo(const t_Void *pvAudLinkInfo,
       **              trMLAudioLinkInfo& rfrAudLinkInfo)
       ************************************************************************************/
      /*!
       * \fn      t_Void vExtractAudRtpOutInfo(const t_Void *pvAudLinkInfo, 
       *							trMLAudioLinkInfo& rfrAudLinkInfo)
       * \brief   Method to Extract RTP Out Audio info received from vAudioLinkAddedCallback.
       * \param   pvAudLinkInfo : [IN]Data pointer containing Extra info for RTP Out
       * \param   rfrAudLinkInfo: [OUT]Reference to Structure to be filled with RTP Out info.
       * \retval  NONE
       *************************************************************************/
      static t_Void vExtractAudRtpOutInfo(const t_Void *pvAudLinkInfo,
               trMLAudioLinkInfo& rfrAudLinkInfo);

      /************************************************************************************
       ** FUNCTION:  t_Void vExtractAudRtpInInfo(const t_Void *pvAudLinkInfo,
       **              trMLAudioLinkInfo& rfrAudLinkInfo)
       ************************************************************************************/
      /*!
       * \fn      t_Void vExtractAudRtpInInfo(const t_Void *pvAudLinkInfo, 
       *							trMLAudioLinkInfo& rfrAudLinkInfo)
       * \brief   Method to Extract RTP In Audio info received from vAudioLinkAddedCallback.
       * \param   pvAudLinkInfo : [IN]Data pointer containing Extra info for RTP In
       * \param   rfrAudLinkInfo: [OUT]Reference to Structure to be filled with RTP In info.
       * \retval  NONE
       *************************************************************************/
      static t_Void vExtractAudRtpInInfo(const t_Void *pvAudLinkInfo,
               trMLAudioLinkInfo& rfrAudLinkInfo);

      /************************************************************************************
       ** FUNCTION: static t_Void VNCCALL spi_tclMLVncCmdAudio::vGetAudioCapabilitiesCallback
       **              (t_Void *pContext,VNCAudioRouterProtocols protocols)
       ************************************************************************************/
      /*!
       * \fn      vGetAudioCapabilitiesCallback(t_Void *pContext,
       **            VNCAudioRouterProtocols protocols)
       * \brief   Callback that provides the protocols supported by the server
       * \param   pContext : [IN]Context pointer
       * \param   protocols: [IN]Supported protocols
       * \retval  NONE
       *************************************************************************/
      static t_Void VNCCALL vGetAudioCapabilitiesCallback(t_Void *pContext,
               VNCAudioRouterProtocols protocols);

      /*********************************************************************************************
       ** FUNCTION: static t_Void spi_tclMLVncCmdAudio::VNCCALL vCheckBTConnectionCallback(
       **        t_Void *pContext,VNCAudioRouterError error,
       **        VNCAudioRouterError btError, vnc_int32_t btEnabled,
       **        VNCAudioRouterError bondedProfilesError,VNCAudioRouterProtocols bondedBtProfiles,
       **        VNCAudioRouterError connectedProfilesError,VNCAudioRouterProtocols connectedBtProfiles,
       **        const t_Char *btAddress)
       **********************************************************************************************/
      /*!
       * \fn      static t_Void VNCCALL vCheckBTConnectionCallback(t_Void *pContext, VNCAudioRouterError error,
       VNCAudioRouterError btError, vnc_int32_t btEnabled,
       VNCAudioRouterError bondedProfilesError,
       VNCAudioRouterProtocols bondedBtProfiles,
       VNCAudioRouterError connectedProfilesError,
       VNCAudioRouterProtocols connectedBtProfiles, const t_Char *btAddress)
       * \brief   Callback that provides the status of the existing BT connection
       * \param   t_Void *pContext : [IN]Context pointer
       * \param   VNCAudioRouterError error: [IN]outcome of attempting to send the check
       *               request to the mobile device
       * \param   VNCAudioRouterError btError: [IN]error related to checking the current
       *          Bluetooth state of the mobile device
       * \param   vnc_int32_t btEnabled: [IN]Specifies whether Bluetooth is
       *          switched on or off on the device
       * \param   VNCAudioRouterError bondedProfilesError: [IN]error related to checking
       *      whether the mobile device is bonded (paired and trusted) with the required Bluetooth device
       * \param   VNCAudioRouterProtocols bondedBtProfiles: [IN]Specifies with which profiles
       *          the mobile and the Bluetooth device are bonded
       * \param   VNCAudioRouterError connectedProfilesError: [IN]error related to checking
       *          whether the mobile device is connected with the required Bluetooth audio device
       * \param   VNCAudioRouterProtocols connectedBtProfiles: [IN]Specifies with which profiles the
       *          mobile and the Bluetooth device are connected
       * \param   const t_Char *btAddress: [IN]Confirms the Bluetooth address of the audio device for
       *           which the check was done
       * \retval  t_Void
       ***********************************************************************************************************/
      static t_Void VNCCALL vCheckBTConnectionCallback(t_Void *pContext, VNCAudioRouterError error,
               VNCAudioRouterError btError, vnc_int32_t btEnabled,
               VNCAudioRouterError bondedProfilesError,
               VNCAudioRouterProtocols bondedBtProfiles,
               VNCAudioRouterError connectedProfilesError,
               VNCAudioRouterProtocols connectedBtProfiles, const t_Char *btAddress);

      /********************************************************************************
       ** FUNCTION:  t_Void VNCCALL spi_tclMLVncCmdAudio::vGetAudioCapabilitiesErrorCallback
       **              (t_Void *pContext,VNCAudioRouterError error)
       *********************************************************************************/
      /*!
       * \fn       t_Void VNCCALL vGetAvailableProtocolsErrorCallback
       *              (t_Void *pContext,VNCAudioRouterError error)
       * \brief   Callback that indicates the error in checking BT connection
       * \param   t_Void *pContext : [IN]Context pointer
       * \param   error: [IN]Error in checking BT connection
       * \retval  NONE
       *********************************************************************************/

      static t_Void vGetAudioCapabilitiesErrorCallback(t_Void *pContext,
               VNCAudioRouterError error);

      /********************************************************************************
       ** FUNCTION:  t_Void VNCCALL spi_tclMLVncCmdAudio::vLinkAddedCallback(t_Void *pContext,
       VNCAudioRouterProtocols protocols, const t_Void *extraInfo)
       *********************************************************************************/
      /*!
       * \fn       t_Void VNCCALL vAudioLinkAddedCallback(t_Void *pContext,
       *	            VNCAudioRouterProtocols protocols, const t_Void *pvAudLinkInfo)
       * \brief   Callback that indicates the Successful response to the add links request
       * \param   pContext : [IN]Context pointer
       * \param   protocols: [IN]Supported protocols
       * \param   pvAudLinkInfo: [IN]Extra information about the linked protocol
       * \retval  NONE
       *********************************************************************************/
      static t_Void VNCCALL vAudioLinkAddedCallback(t_Void *pContext,
               VNCAudioRouterProtocols protocols, const t_Void *pvAudLinkInfo);

      /**************************************************************************
       ** FUNCTION: t_Void vGst_rtp_out_error_callback(
       ** t_Void * p_user_ctx,  mlink_rtp_out_context * p_rtp_out_ctx, t_Char * pMessage)
       **************************************************************************/
      /*!
       * \fn       t_Void vGst_rtp_out_error_callback(
       *     t_Void * p_user_ctx,  mlink_rtp_out_context * p_rtp_out_ctx, t_Char * pMessage)
       * \brief   Callback that indicates an error in RTP out streaming
       * \param   p_user_ctx :     [IN]User Context pointer
       * \param   p_rtp_out_ctx:   [IN]RTP Out Context pointer
       * \param   pMessage:        [IN]Message
       * \retval  NONE
       *********************************************************************************/
      static t_Void vGst_rtp_out_error_callback(t_Void * p_user_ctx,
               mlink_rtp_out_context * p_rtp_out_ctx, t_Char * pMessage);

      /****************************************************************************
       ** FUNCTION: t_Void vGst_rtp_out_data_callback(
       **  t_Void * p_user_ctx, guint32 * extension, guint16 extension_size,
       **  guint8 markerbit)
       ****************************************************************************/
      /*!
       * \fn       vGst_rtp_out_data_callback(
       **    t_Void * p_user_ctx, guint32 * extension, guint16 extension_size,
       **    guint8 markerbit)
       * \brief   This callback is called for every incoming rtp packet.
       *          The markerbit and the header extension is passed to this
       *          function to detect included audio streams in the RTP packet
       * \param   p_user_ctx :     [IN]User Context pointer
       * \param   extension:       [IN]RTP Header extension
       * \param   extension_size:  [IN]extension size
       * \param   markerbit:       [IN]Marker bit
       * \retval  NONE
       *********************************************************************************/
      static t_Void vGst_rtp_out_data_callback(t_Void * p_user_ctx,
               t_U32 * extension, t_U16 extension_size, t_U8 markerbit);

      /****************************************************************************
       ** FUNCTION: t_Void vGst_streaming_thread_callback
       ****************************************************************************/
      /*!
       * \fn       vGst_streaming_thread_callback
       * \brief   This callback is use to set priority for audio streaming thread
       * \param   pUserContext :     [IN]User Context pointer
       * \param   pName:       [IN]Name of the thread
       * \retval  NONE
       *********************************************************************************/
      static t_Void vGst_streaming_thread_callback(t_Void* pUserContext, t_Char* pName);

      /*****************************************************************************************
       ** FUNCTION:  t_Void VNCCALL spi_tclMLVncCmdAudio::vAudioLinkAddFailedCallback(t_Void *pContext,
       ** VNCAudioRouterProtocols protocols,VNCAudioRouterError error, const t_Void *extraInfo)
       ******************************************************************************************/
      /*!
       * \fn       t_Void VNCCALL vAudioLinkAddFailedCallback(t_Void *pContext,
       **           VNCAudioRouterProtocols protocols,VNCAudioRouterError error,
       **           const t_Void *extraInfo)
       * \brief   Callback that indicates the failure response to the add links request
       * \param   pContext : [IN]Context pointer
       * \param   protocols: [IN]Supported protocols
       * \param   error: [IN]Error in adding audio links
       * \param   extraInfo: [IN]Extra information about the linked protocol
       * \retval  NONE
       *********************************************************************************/
      static t_Void VNCCALL vAudioLinkAddFailedCallback(t_Void *pContext,
               VNCAudioRouterProtocols protocols,
               VNCAudioRouterError error, const t_Void *extraInfo);

      /*******************************************************************************
       ** FUNCTION: static t_Void VNCCALL spi_tclMLVncCmdAudio::vAudioLinkRemovedCallback(
       ** t_Void *pContext,VNCAudioRouterProtocols protocols,const t_Void *extraInfo)
       ********************************************************************************/
      /*!
       * \fn      static t_Void VNCCALL vAudioLinkRemovedCallback(t_Void *pContext,
       *          VNCAudioRouterProtocols protocols,const t_Void *extraInfo)
       * \brief   Callback that indicates the Successful response to the remove audio
       *          links request
       * \param   pContext : [IN]Context pointer
       * \param   protocols: [IN]Supported protocols
       * \param   extraInfo: [IN]Extra information about the linked protocol
       * \retval  NONE
       ********************************************************************************/
      static t_Void VNCCALL vAudioLinkRemovedCallback(t_Void *pContext, VNCAudioRouterProtocols
               protocols,const t_Void *extraInfo);

      /********************************************************************************
       ** FUNCTION: static t_Void VNCCALL spi_tclMLVncCmdAudio::vAudioLinkRemovalFailedCallback(
       ** t_Void *pContext, VNCAudioRouterProtocols protocols,
       ** VNCAudioRouterError error, const t_Void *extraInfo)
       ********************************************************************************/
      /*!
       * \fn      static t_Void vAudioLinkRemovalFailedCallback(
       **          t_Void *pContext, VNCAudioRouterProtocols protocols,
       **          VNCAudioRouterError error, const t_Void *extraInfo)
       * \brief   Callback that indicates the failure response to the remove audio
       *          links request
       * \param   pContext : [IN]Context pointer
       * \param   protocols: [IN]Supported protocols
       * \param   error: Link Removal Failed error
       * \param   extraInfo: [IN]Extra information about the linked protocol
       * \retval  t_Void
       *********************************************************************************/
      static t_Void VNCCALL vAudioLinkRemovalFailedCallback(t_Void *pContext, VNCAudioRouterProtocols
               protocols,VNCAudioRouterError error, const t_Void *extraInfo);


      /***************************************************************************
      ** FUNCTION:  t_U32 spi_tclMLVncCmdAudio::u32FetchKeyValue()
      ***************************************************************************/
      /*!
      * \fn      u32FetchKeyValue(const VNCDiscoverySDK* poDiscSDK,
         const VNCDiscoverySDKDiscoverer* poDisc,
         const VNCDiscoverySDKEntity* poEntity,
         const t_Char *pczKey,
         t_S32 s32TimeOut= 0)
      * \brief   To Get the int attribute, after the fetching the value
      * \param   poDiscSDK    :  [IN] Pointer to discovery SDK
      * \param   poDisc       :  [IN] Pointer to discoverer
      * \param   poEntity     :  [IN] Pointer to Entity
      * \param   pczKey       :  [IN] Key value used to fetch 
      * \param   s32TimeOut   :  [IN] Timeout
      * \retval  t_U32
      **************************************************************************/
      t_U32 u32FetchKeyValue(const VNCDiscoverySDK* poDiscSDK,
       const VNCDiscoverySDKDiscoverer* poDisc,
       const VNCDiscoverySDKEntity* poEntity,
       const t_Char *pczKey,
       t_S32 s32TimeOut= 0);

      //! Audio Router SDK instance. obtained during initialization
      VNCAudioRouterSDK *m_pAudioRouterSDK;

      //! Audio Router instance. obtained during initialization
      VNCAudioRouter* m_pAudioRouter;

      //! RTP Out Context pointer
      mlink_rtp_out_context* m_pRTPOutContext;

      //! Audio IPL
      static t_U32 m_u32AudioIPL;

      //! Vector of payload types
      std::vector<t_U32> m_vecRTPPayload;

      //! Remoting Info
      trAppRemotingInfo m_trRemotingInfo;

      //! Audio Capabilities
      static trAudioCapabilities m_rAudioCapabilities;

      //! RTP Payload type
      static t_U32 m_u32RTPPayload;

      //! Lock to prevent simultaneous access to m_pRTPOutContext
      Lock m_oRTPOutCntxtLock;

};
#endif /* SPI_TCLMLVNCCMDAUDIO_H_ */
