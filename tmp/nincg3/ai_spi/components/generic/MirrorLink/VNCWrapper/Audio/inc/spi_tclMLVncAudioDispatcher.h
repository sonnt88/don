/***********************************************************************/
/*!
 * \file  spi_tclMLVncAudioDispatcher.h
 * \brief Message Dispatcher for Audio Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Audio Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 18.08.2015  |  Shiva Kumar Gurija   | Implemetation of new interfaces for
                                      ML Dynamic Audio
 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLMLVNCAUDIODISPATCHER_H_
#define SPI_TCLMLVNCAUDIODISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"

/**************Forward Declarations******************************************/
class spi_tclMLVncAudioDispatcher;

/****************************************************************************/
/*!
 * \class MLAudioMsgBase
 * \brief Base Message type for all Audio messages
 ****************************************************************************/
class MLAudioMsgBase: public trMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  MLAudioMsgBase::MLAudioMsgBase
       ***************************************************************************/
      /*!
       * \fn      MLAudioMsgBase()
       * \brief   Default constructor
       **************************************************************************/
      MLAudioMsgBase();

      /***************************************************************************
       ** FUNCTION:  MLAudioMsgBase::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   Pure virtual function to be overridden by inherited classes for
       *          dispatching the message
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      virtual t_Void vDispatchMsg(
               spi_tclMLVncAudioDispatcher* poAudioDispatcher) = 0;

      /***************************************************************************
       ** FUNCTION:  MLAudioMsgBase::~MLAudioMsgBase
       ***************************************************************************/
      /*!
       * \fn      ~MLAudioMsgBase()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAudioMsgBase()
      {

      }
};

/****************************************************************************/
/*!
 * \class MLAudioCapabilitiesMsg
 * \brief
 ****************************************************************************/
class MLAudioCapabilitiesMsg: public MLAudioMsgBase
{
   public:
      trAudioCapabilities rAudioCapabilities;
      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesMsg::MLAudioCapabilitiesMsg
       ***************************************************************************/
      /*!
       * \fn      MLAudioCapabilitiesMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAudioCapabilitiesMsg();

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesMsg::~MLAudioCapabilitiesMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAudioCapabilitiesMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAudioCapabilitiesMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLAudioCapabilitiesErrorMsg
 * \brief
 ****************************************************************************/
class MLAudioCapabilitiesErrorMsg: public MLAudioMsgBase
{
   public:
      t_S32 s32AudioCapabilitiesError;

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesErrorMsg::MLAudioCapabilitiesErrorMsg
       ***************************************************************************/
      /*!
       * \fn      MLAudioCapabilitiesErrorMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAudioCapabilitiesErrorMsg();

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesErrorMsg::~MLAudioCapabilitiesErrorMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAudioCapabilitiesErrorMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAudioCapabilitiesErrorMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesErrorMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesErrorMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAudioCapabilitiesErrorMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLPostBtInfoMsg
 * \brief
 ****************************************************************************/
class MLPostBtInfoMsg: public MLAudioMsgBase
{
   public:
      trBtInfo *prBtInfo;
      /***************************************************************************
       ** FUNCTION:  MLPostBtInfoMsg::MLPostBtInfoMsg
       ***************************************************************************/
      /*!
       * \fn      MLPostBtInfoMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLPostBtInfoMsg();

      /***************************************************************************
       ** FUNCTION:  MLPostBtInfoMsg::~MLPostBtInfoMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLPostBtInfoMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLPostBtInfoMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLPostBtInfoMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLPostBtInfoMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLPostBtInfoMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLAudioRouterExtraInfoMsg
 * \brief
 ****************************************************************************/
class MLAudioRouterExtraInfoMsg: public MLAudioMsgBase
{
   public:
      t_Void *pContext;
      trMLAudioLinkInfo *prAudioLinkInfo;
      t_Bool m_bRTPInAvail;
      t_Bool m_bRTPOutAvail;

      /***************************************************************************
       ** FUNCTION:  MLAudioRouterExtraInfoMsg::MLAudioRouterExtraInfoMsg
       ***************************************************************************/
      /*!
       * \fn      MLAudioRouterExtraInfoMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAudioRouterExtraInfoMsg();

      /***************************************************************************
       ** FUNCTION:  MLAudioRouterExtraInfoMsg::~MLAudioRouterExtraInfoMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAudioRouterExtraInfoMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAudioRouterExtraInfoMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLAudioRouterExtraInfoMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAudioRouterExtraInfoMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLAudioRouterExtraInfoMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLAudioSubscriResultMsg
 * \brief
 ****************************************************************************/
class MLAudioSubscriResultMsg: public MLAudioMsgBase
{
   public:

      //TODO What is the result posted?? Bool Value?
      /***************************************************************************
       ** FUNCTION:  MLAudioSubscriResultMsg::MLAudioSubscriResultMsg
       ***************************************************************************/
      /*!
       * \fn      MLAudioSubscriResultMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAudioSubscriResultMsg();

      /***************************************************************************
       ** FUNCTION:  MLAudioSubscriResultMsg::~MLAudioSubscriResultMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAudioSubscriResultMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAudioSubscriResultMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLAudioSubscriResultMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAudioSubscriResultMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAudioSubscriResultMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLAudioLinkAddErrorMsg
 * \brief
 ****************************************************************************/
class MLAudioLinkAddErrorMsg: public MLAudioMsgBase
{
   public:
      t_U16 u16LinkAddError; //TODO check if t_U32 is required
      /***************************************************************************
       ** FUNCTION:  MLAudioLinkAddErrorMsg::MLAudioLinkAddErrorMsg
       ***************************************************************************/
      /*!
       * \fn      MLAudioLinkAddErrorMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAudioLinkAddErrorMsg();

      /***************************************************************************
       ** FUNCTION:  MLAudioLinkAddErrorMsg::~MLAudioLinkAddErrorMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAudioLinkAddErrorMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAudioLinkAddErrorMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLAudioLinkAddErrorMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAudioLinkAddErrorMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAudioLinkAddErrorMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLAudioLInkRemoveErrorMsg
 * \brief
 ****************************************************************************/
class MLAudioLInkRemoveErrorMsg: public MLAudioMsgBase
{
   public:
      t_U16 u16LinkRemoveError; //TODO check if t_U32 is required
      /***************************************************************************
       ** FUNCTION:  MLAudioLInkRemoveErrorMsg::MLAudioLInkRemoveErrorMsg
       ***************************************************************************/
      /*!
       * \fn      MLAudioLInkRemoveErrorMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAudioLInkRemoveErrorMsg();

      /***************************************************************************
       ** FUNCTION:  MLAudioLInkRemoveErrorMsg::~MLAudioLInkRemoveErrorMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAudioLInkRemoveErrorMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAudioLInkRemoveErrorMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLAudioLInkRemoveErrorMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAudioLInkRemoveErrorMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAudioLInkRemoveErrorMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*!
 * \class MLAudioRTPOutData
 * \brief RTPOut Data - Data present in RTP Header Extension
 ****************************************************************************/
class MLAudioRTPOutData: public MLAudioMsgBase
{
   public:

      t_U32* m_pu32RTPOutData;
      t_U16 m_u16RTPOutDataSize;
      t_U8 m_u8MarkerBit;

      /***************************************************************************
       ** FUNCTION:  MLAudioRTPOutData::MLAudioRTPOutData
       ***************************************************************************/
      /*!
       * \fn      MLAudioRTPOutData()
       * \brief   Default constructor
       **************************************************************************/
      MLAudioRTPOutData();

      /***************************************************************************
       ** FUNCTION:  MLAudioRTPOutData::~MLAudioRTPOutData
       ***************************************************************************/
      /*!
       * \fn      ~MLAudioRTPOutData()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAudioRTPOutData(){}

      /***************************************************************************
       ** FUNCTION:  MLAudioRTPOutData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poAudioDispatcher :pointer to  Dispatcher for Audio Messages
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncAudioDispatcher* poAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAudioRTPOutData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*!
 * \class spi_tclMLVncAudioDispatcher
 * \brief Message Dispatcher for Audio Messages
 ****************************************************************************/
class spi_tclMLVncAudioDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::spi_tclMLVncAudioDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncAudioDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclMLVncAudioDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::~spi_tclMLVncAudioDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclMLVncAudioDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclMLVncAudioDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioCapabilitiesMsg* poAudioCapabilitiesMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(MLAudioCapabilitiesMsg* poAudioCapabilitiesMsg)
       * \brief   Handles Messages of MLAudioCapabilitiesMsg type
       * \param poAudioCapabilitiesMsg : pointer to MLAudioCapabilitiesMsg
       **************************************************************************/
      t_Void vHandleAudioMsg(MLAudioCapabilitiesMsg* poAudioCapabilitiesMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioCapabilitiesErrorMsg* poAudioCapabilitiesErrorMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(MLAudioCapabilitiesErrorMsg* poAudioCapabilitiesErrorMsg)
       * \brief   Handles Messages of MLAudioCapabilitiesErrorMsg type
       * \param poAudioCapabilitiesErrorMsg : pointer to MLAudioCapabilitiesErrorMsg
       **************************************************************************/
      t_Void vHandleAudioMsg(
               MLAudioCapabilitiesErrorMsg* poAudioCapabilitiesErrorMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLPostBtInfoMsg* poPostBtInfoMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(MLPostBtInfoMsg* poPostBtInfoMsg)
       * \brief   Handles Messages of MLPostBtInfoMsg type
       * \param poPostBtInfoMsg : pointer to MLPostBtInfoMsg
       **************************************************************************/
      t_Void vHandleAudioMsg(MLPostBtInfoMsg* poPostBtInfoMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioRouterExtraInfoMsg* poAudioRouterExtraInfoMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(MLAudioRouterExtraInfoMsg* poAudioRouterExtraInfoMsg)
       * \brief   Handles Messages of MLAudioRouterExtraInfoMsg type
       * \param poAudioRouterExtraInfoMsg : pointer to AudioRouterExtraInfoMsg
       **************************************************************************/
      t_Void vHandleAudioMsg(MLAudioRouterExtraInfoMsg* poAudioRouterExtraInfoMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioSubscriResultMsg* poAudioSubscriResultMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(MLAudioSubscriResultMsg* poAudioSubscriResultMsg)
       * \brief   Handles Messages of MLAudioSubscriResultMsg type
       * \param poAudioSubscriResultMsg : pointer to AudioSubscriResultMsg
       **************************************************************************/
      t_Void vHandleAudioMsg(MLAudioSubscriResultMsg* poAudioSubscriResultMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioLinkAddErrorMsg* poAudioLinkAddErrorMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(MLAudioLinkAddErrorMsg* poAudioLinkAddErrorMsg)
       * \brief   Handles Messages of MLAudioLinkAddErrorMsg type
       * \param poAudioLinkAddErrorMsg : pointer to poAudioLinkAddErrorMsg
       **************************************************************************/
      t_Void vHandleAudioMsg(MLAudioLinkAddErrorMsg* poAudioLinkAddErrorMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioLInkRemoveErrorMsg* poAudioLInkRemoveErrorMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(MLAudioLInkRemoveErrorMsg* poAudioLInkRemoveErrorMsg)
       * \brief   Handles Messages of MLAudioLInkRemoveErrorMsg type
       * \param poAudioLInkRemoveErrorMsg : pointer to poAudioLInkRemoveErrorMsg
       **************************************************************************/
      t_Void vHandleAudioMsg(
               MLAudioLInkRemoveErrorMsg* poAudioLInkRemoveErrorMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioRTPOutData* poMLAudioRTPOutData)
       ***************************************************************************/
      /*!
       * \fn      t_Void vHandleAudioMsg(MLAudioRTPOutData* poMLAudioRTPOutData) const
       * \brief   Handles Messages of MLAudioRTPOutData type
       * \param   poMLAudioRTPOutData : pointer to poMLAudioRTPOutData
       **************************************************************************/
      t_Void vHandleAudioMsg(MLAudioRTPOutData* poMLAudioRTPOutData)const;

};

#endif /* SPI_TCLMLVNCAUDIODISPATCHER_H_ */
