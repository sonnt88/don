/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespAudio.h
 * \brief             Response class for Audio RealVNC Wrapper
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Response calls for receiving the values from VNC wrapper callbacks
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 30.08.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
 18.08.2015 |  Shiva Kumar Gurija              | Implemetation of new interfaces for
                                                ML Dynamic Audio
 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMLVNCRESPAUDIO_H_
#define SPI_TCLMLVNCRESPAUDIO_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include "SPITypes.h"
#include "RespBase.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLVncRespAudio
 * \brief This class receives the response from the callbacks from Real
 VNC Audio Wrappers
 *
 */

class spi_tclMLVncRespAudio: public RespBase
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncRespAudio::spi_tclMLVncRespAudio()
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncRespAudio()
       * \brief   Constructor
       * \sa      ~spi_tclMLVncRespAudio()
       **************************************************************************/
      spi_tclMLVncRespAudio();

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclMLVncRespAudio::~spi_tclMLVncRespAudio()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclMLVncRespAudio()
       * \brief   Destructor
       * \sa      spi_tclMLVncRespAudio()
       **************************************************************************/
      virtual ~spi_tclMLVncRespAudio();

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioCapabilities
       ***************************************************************************/
      /*!
       * \fn      vPostAudioCapabilities(trAudioCapabilities& oAudioCapabilities,
       *                        const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the protocols supported by the ML server
       * \param   oAudioCapabilities: [IN]Protocols
       *            supported by the server
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostAudioCapabilities(trAudioCapabilities& oAudioCapabilities,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioCapabilitiesError
       ***************************************************************************/
      /*!
       * \fn      vPostAudioCapabilitiesError(MLAudioRouterError s32error,
       *                                const t_U32 cou32DeviceHandle = 0);
       * \brief   Posts the error in retreiving protocols supported by the ML server
       * \param   s32error: [IN]Error in retreiving protocols
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostAudioCapabilitiesError(t_S32 s32AudioRouterError,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostBtInfo
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostBtInfo( BtInfo& trBtInfo,
       *                                                                               const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the BT Profile and address info if supported by the ML server
       * \param   trBtInfo : [IN]BT Info
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostBtInfo(trBtInfo& rfrBtInfo, const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostSetAudioLinkInfo
       ***************************************************************************/
      /*!
      * \fn      t_Void vPostSetAudioLinkInfo(t_Void *pContext,
      *                                trMLAudioLinkInfo& rfrAudioLinkInfo,
      *                                t_Bool bRTPInAvail,
      *                                t_Bool bRTPOutAvail,
      *                                const t_U32 cou32DeviceHandle = 0)
      * \brief   Posts the RTP extra info required for RTP streaming
      * \param   rfrAudioLinkInfo: [IN]RTP Extra Info
      * \param   bRTPInAvail: [IN] RTP IN URL is valid
      * \param   bRTPOutAvail: [IN] RTP OUT URL is valid
      * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
      * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostSetAudioLinkInfo(t_Void *pContext,
         trMLAudioLinkInfo& rfrAudioLinkInfo,
         t_Bool bRTPInAvail,
         t_Bool bRTPOutAvail,
         const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioSubscriptionResult
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostAudioSubscriptionResult(const t_U32 cou32DeviceHandle = 0);
       * \brief   Posts the result of the subscription
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostAudioSubscriptionResult( const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioLinkAddFailError
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostAudioLinkAddFailError(t_U16 u16AudioRouterError,  const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the error in adding audio links
       * \param   u16AudioRouterError: [IN]Error in adding audio links
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostAudioLinkAddFailError( t_S32 s32AudioRouterError,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioLinkRemovalFailError
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostAudioLinkRemovalFailError()
       * \brief   Posts the error in removing audio links
       * \param   u16AudioRouterError: [IN]Error in removing audio links
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostAudioLinkRemovalFailError( t_S32 s32AudioRouterError,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostRTPOutData()
      ***************************************************************************/
      /*!
      * \fn      t_Void vPostRTPOutData(const t_U32* cpu32RTPOutData, 
      *            t_U16 u16RTPOutDatalen,t_U8 u8MarkerBit)
      * \brief   Posts the RTP Out Data recieved from Phone
      * \param   cpu32RTPOutData: [IN] RTP Out Data
      * \param   u16RTPOutDatalen : [IN] Length of RTP Out Data
      * \param   u8MarkerBit    : [IN] Market bit - says whether it is end of stream.
      * \retval  t_Void
      **************************************************************************/
      virtual t_Void vPostRTPOutData(const t_U32* cpu32RTPOutData, t_U16 u16RTPOutDatalen,
         t_U8 u8MarkerBit){}
};

#endif /* SPI_TCLMLVNCRESPAUDIO_H_ */
