/***********************************************************************/
/*!
 * \file  spi_tclMLVncAudioDispatcher.cpp
 * \brief Message Dispatcher for Audio Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Audio Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 18.08.2015  |  Shiva Kumar Gurija   | Implemetation of new interfaces for
                                      ML Dynamic Audio
 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "RespRegister.h"
#include "spi_tclMLVncAudioDispatcher.h"
#include "spi_tclMLVncRespAudio.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
         #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
         #include "trcGenProj/Header/spi_tclMLVncAudioDispatcher.cpp.trc.h"
      #endif
   #endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleAudioMsg(this);                  \
   }                                                        \
   vDeAllocateMsg();                                        \
}

/***************************************************************************
 ** FUNCTION:  MLAudioMsgBase::MLAudioMsgBase
 ***************************************************************************/
MLAudioMsgBase::MLAudioMsgBase()
{
   vSetServiceID( e32MODULEID_VNCAUDIO);
}

/***************************************************************************
 ** FUNCTION:  MLAudioCapabilitiesMsg::MLAudioCapabilitiesMsg
 ***************************************************************************/
MLAudioCapabilitiesMsg::MLAudioCapabilitiesMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAudioCapabilitiesMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAudioCapabilitiesMsg, spi_tclMLVncAudioDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAudioCapabilitiesErrorMsg::MLAudioCapabilitiesErrorMsg
 ***************************************************************************/
MLAudioCapabilitiesErrorMsg::MLAudioCapabilitiesErrorMsg() :
   s32AudioCapabilitiesError(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAudioCapabilitiesErrorMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAudioCapabilitiesErrorMsg, spi_tclMLVncAudioDispatcher)

/***************************************************************************
 ** FUNCTION:  MLPostBtInfoMsg::MLPostBtInfoMsg
 ***************************************************************************/
MLPostBtInfoMsg::MLPostBtInfoMsg(): prBtInfo(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLPostBtInfoMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLPostBtInfoMsg, spi_tclMLVncAudioDispatcher)

/***************************************************************************
 ** FUNCTION:  MLPostBtInfoMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLPostBtInfoMsg::vAllocateMsg()
{
   prBtInfo = new trBtInfo;
   SPI_NORMAL_ASSERT(NULL == prBtInfo);
}

/***************************************************************************
 ** FUNCTION:  MLPostBtInfoMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLPostBtInfoMsg::vDeAllocateMsg()
{
   RELEASE_MEM(prBtInfo);
}

/***************************************************************************
 ** FUNCTION:  MLAudioRouterExtraInfoMsg::MLAudioRouterExtraInfoMsg
 ***************************************************************************/
MLAudioRouterExtraInfoMsg::MLAudioRouterExtraInfoMsg() : pContext(NULL), prAudioLinkInfo(NULL),
m_bRTPInAvail(false),m_bRTPOutAvail(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAudioRouterExtraInfoMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAudioRouterExtraInfoMsg, spi_tclMLVncAudioDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAudioRouterExtraInfoMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLAudioRouterExtraInfoMsg::vAllocateMsg()
{
   prAudioLinkInfo = new trMLAudioLinkInfo;
   SPI_NORMAL_ASSERT(NULL == prAudioLinkInfo);
}

/***************************************************************************
 ** FUNCTION:  MLAudioRouterExtraInfoMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLAudioRouterExtraInfoMsg::vDeAllocateMsg()
{
   RELEASE_MEM(prAudioLinkInfo);
}


/***************************************************************************
 ** FUNCTION:  MLAudioSubscriResultMsg::MLAudioSubscriResultMsg
 ***************************************************************************/
MLAudioSubscriResultMsg::MLAudioSubscriResultMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAudioSubscriResultMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAudioSubscriResultMsg, spi_tclMLVncAudioDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAudioLinkAddErrorMsg::MLAudioLinkAddErrorMsg
 ***************************************************************************/
MLAudioLinkAddErrorMsg::MLAudioLinkAddErrorMsg() : u16LinkAddError(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAudioLinkAddErrorMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAudioLinkAddErrorMsg, spi_tclMLVncAudioDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAudioLInkRemoveErrorMsg::MLAudioLInkRemoveErrorMsg
 ***************************************************************************/
MLAudioLInkRemoveErrorMsg::MLAudioLInkRemoveErrorMsg() : u16LinkRemoveError(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAudioLInkRemoveErrorMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAudioLInkRemoveErrorMsg, spi_tclMLVncAudioDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAudioRTPOutData::MLAudioRTPOutData
 ***************************************************************************/
MLAudioRTPOutData::MLAudioRTPOutData() : m_pu32RTPOutData(NULL),m_u16RTPOutDataSize(0),m_u8MarkerBit(0)
{

}

/***************************************************************************
 ** FUNCTION:  MLAudioRTPOutData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAudioRTPOutData, spi_tclMLVncAudioDispatcher)


/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::spi_tclMLVncAudioDispatcher
 ***************************************************************************/
spi_tclMLVncAudioDispatcher::spi_tclMLVncAudioDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::~spi_tclMLVncAudioDispatcher
 ***************************************************************************/
spi_tclMLVncAudioDispatcher::~spi_tclMLVncAudioDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioCapabilitiesMsg* poAudioCapabilitiesMsg)
 ***************************************************************************/
t_Void spi_tclMLVncAudioDispatcher::vHandleAudioMsg(
      MLAudioCapabilitiesMsg* poAudioCapabilitiesMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poAudioCapabilitiesMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespAudio,
            e16AUDIO_REGID,
            vPostAudioCapabilities(poAudioCapabilitiesMsg->rAudioCapabilities));
   } //  if (NULL != poAudioCapabilitiesMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioCapabilitiesErrorMsg* poAudioCapabilitiesErrorMsg)
 ***************************************************************************/
t_Void spi_tclMLVncAudioDispatcher::vHandleAudioMsg(
      MLAudioCapabilitiesErrorMsg* poAudioCapabilitiesErrorMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poAudioCapabilitiesErrorMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespAudio,
            e16AUDIO_REGID,
            vPostAudioCapabilitiesError(poAudioCapabilitiesErrorMsg->s32AudioCapabilitiesError));
   } //  if (NULL != poAudioCapabilitiesErrorMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLPostBtInfoMsg* poPostBtInfoMsg)
 ***************************************************************************/
t_Void spi_tclMLVncAudioDispatcher::vHandleAudioMsg(
      MLPostBtInfoMsg* poPostBtInfoMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poPostBtInfoMsg) && (NULL != poPostBtInfoMsg->prBtInfo))
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespAudio,
            e16AUDIO_REGID,
            vPostBtInfo(*(poPostBtInfoMsg->prBtInfo)));
   } //  if (NULL != poPostBtInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioRouterExtraInfoMsg* poAudioRouterExtraInfoMsg)
 ***************************************************************************/
t_Void spi_tclMLVncAudioDispatcher::vHandleAudioMsg(
      MLAudioRouterExtraInfoMsg* poAudioRouterExtraInfoMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAudioRouterExtraInfoMsg) && (NULL != poAudioRouterExtraInfoMsg->prAudioLinkInfo))
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespAudio,
            e16AUDIO_REGID,
            vPostSetAudioLinkInfo(poAudioRouterExtraInfoMsg->pContext,
                  *(poAudioRouterExtraInfoMsg->prAudioLinkInfo),
                  poAudioRouterExtraInfoMsg->m_bRTPInAvail,
                  poAudioRouterExtraInfoMsg->m_bRTPOutAvail));
   } //  if (NULL != poAudioRouterExtraInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioSubscriResultMsg* poAudioSubscriResultMsg)
 ***************************************************************************/
t_Void spi_tclMLVncAudioDispatcher::vHandleAudioMsg(
      MLAudioSubscriResultMsg* poAudioSubscriResultMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poAudioSubscriResultMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespAudio,
            e16AUDIO_REGID,
            vPostAudioSubscriptionResult());
   } //  if (NULL != poAudioSubscriResultMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioLinkAddErrorMsg* poAudioLinkAddErrorMsg)
 ***************************************************************************/
t_Void spi_tclMLVncAudioDispatcher::vHandleAudioMsg(
      MLAudioLinkAddErrorMsg* poAudioLinkAddErrorMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poAudioLinkAddErrorMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespAudio,
            e16AUDIO_REGID,
            vPostAudioLinkAddFailError(poAudioLinkAddErrorMsg->u16LinkAddError));
   } //  if (NULL != poAudioLinkAddErrorMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioLInkRemoveErrorMsg* poAudioLInkRemoveErrorMsg)
 ***************************************************************************/
t_Void spi_tclMLVncAudioDispatcher::vHandleAudioMsg(
      MLAudioLInkRemoveErrorMsg* poAudioLInkRemoveErrorMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poAudioLInkRemoveErrorMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespAudio,
            e16AUDIO_REGID,
            vPostAudioLinkRemovalFailError(poAudioLInkRemoveErrorMsg->u16LinkRemoveError));
   } //  if (NULL != poAudioLInkRemoveErrorMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncAudioDispatcher::vHandleAudioMsg(MLAudioRTPOutData* poMLAudioRTPOutData)
 ***************************************************************************/
t_Void spi_tclMLVncAudioDispatcher::vHandleAudioMsg(
   MLAudioRTPOutData* poMLAudioRTPOutData)const
{
   ETG_TRACE_USR1((" spi_tclMLVncAudioDispatcher::MLAudioRTPOutData"));
   if (NULL != poMLAudioRTPOutData)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespAudio,
         e16AUDIO_REGID,
         vPostRTPOutData(poMLAudioRTPOutData->m_pu32RTPOutData,
         poMLAudioRTPOutData->m_u16RTPOutDataSize,
         poMLAudioRTPOutData->m_u8MarkerBit));
      RELEASE_ARRAY_MEM(poMLAudioRTPOutData->m_pu32RTPOutData);
   } //  if (NULL != poAudioLInkRemoveErrorMsg)
}