/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespAudio.cpp
 * \brief             Response class for Audio RealVNC Wrapper
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Response calls for receiving the values from VNC wrapper callbacks
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 30.08.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
//#define VNC_USE_STDINT_H  //Commented to fix Lint Warnings
#include "spi_tclMLVncRespAudio.h"

//! Includes for Trace files
#include "Trace.h"

#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncRespAudio.cpp.trc.h"
   #endif
#endif

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncRespAudio::spi_tclMLVncRespAudio
 ***************************************************************************/
spi_tclMLVncRespAudio::spi_tclMLVncRespAudio() :  RespBase(e16AUDIO_REGID)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::spi_tclMLVncRespAudio() entered\n"));

}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclMLVncRespAudio::~spi_tclMLVncRespAudio()
 ***************************************************************************/
spi_tclMLVncRespAudio::~spi_tclMLVncRespAudio()
{
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::Virtual Destructor \n"));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioCapabilities
 ***************************************************************************/
t_Void spi_tclMLVncRespAudio::vPostAudioCapabilities(trAudioCapabilities& rfrAudioCapabilities,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(rfrAudioCapabilities);
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::vPostAudioCapabilities entered \n"));

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioCapabilitiesError
 ***************************************************************************/
t_Void spi_tclMLVncRespAudio::vPostAudioCapabilitiesError(t_S32 s32AudioRouterError,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(s32AudioRouterError);
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::vPostAudioCapabilitiesError entered \n"));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostBtInfo
 ***************************************************************************/
t_Void spi_tclMLVncRespAudio::vPostBtInfo(
      trBtInfo& rfrBtInfo, const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(rfrBtInfo);
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::vPostBtInfo entered \n"));

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostRTPExtraInfo
 ***************************************************************************/
t_Void spi_tclMLVncRespAudio::vPostSetAudioLinkInfo(
	  t_Void *pContext,
      trMLAudioLinkInfo& rfrAudioLinknfo,
      t_Bool bRTPInAvail,
      t_Bool bRTPOutAvail,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(rfrAudioLinknfo);
   SPI_INTENTIONALLY_UNUSED(bRTPInAvail);
   SPI_INTENTIONALLY_UNUSED(bRTPOutAvail);
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::vPostSetAudioLinkInfo entered \n"));

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioSubscriptionResult
 ***************************************************************************/
t_Void spi_tclMLVncRespAudio::vPostAudioSubscriptionResult(const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::vPostAudioSubscriptionResult() entered\n"));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioLinkAddFailError
 ***************************************************************************/
t_Void spi_tclMLVncRespAudio::vPostAudioLinkAddFailError(t_S32 s32AudioRouterError,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(s32AudioRouterError);
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::vPostAudioLinkFailError() entered \n"));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncRespAudio::vPostAudioLinkRemovalFailError
 ***************************************************************************/
t_Void spi_tclMLVncRespAudio::vPostAudioLinkRemovalFailError(t_S32 s32AudioRouterError,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(s32AudioRouterError);
   ETG_TRACE_USR1(("spi_tclMLVncRespAudio::vPostAudioLinkRemovalFailError() entered \n"));
}

