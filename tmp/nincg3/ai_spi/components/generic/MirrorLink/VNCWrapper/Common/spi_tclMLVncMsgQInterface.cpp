/***********************************************************************/
/*!
 * \file  spi_tclMLVncMsgQInterface.cpp
 * \brief interface for writing data to Q to use the MsgQ based
 *        threading model for VNC Wrappers
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    interface for writing data to Q to use the MsgQ based
 threading model for VNC Wrappers
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "spi_tclMLVncMsgQInterface.h"
#include "MessageQueue.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclMLVncMsgQInterface.cpp.trc.h"
   #endif
#endif

using namespace shl::thread;

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncMsgQInterface::spi_tclMLVncMsgQInterface()
 ***************************************************************************/
spi_tclMLVncMsgQInterface::spi_tclMLVncMsgQInterface():m_poVncMsgQThreadable(NULL), m_poVncMsgQThreader(NULL)
{
   ETG_TRACE_USR1(("spi_tclMLVncMsgQInterface::spi_tclMLVncMsgQInterface()"));
   m_poVncMsgQThreadable = new spi_tclMLVncMsgQThreadable();
   SPI_NORMAL_ASSERT(NULL == m_poVncMsgQThreadable);
   m_poVncMsgQThreader = new MsgQThreader(m_poVncMsgQThreadable, true);
   SPI_NORMAL_ASSERT(NULL == m_poVncMsgQThreader);
   if(NULL != m_poVncMsgQThreader)
   {
      m_poVncMsgQThreader->vSetThreadName("VNCRespQ");
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncMsgQInterface::~spi_tclMLVncMsgQInterface()
 ***************************************************************************/
spi_tclMLVncMsgQInterface::~spi_tclMLVncMsgQInterface()
{
   ETG_TRACE_USR1((" spi_tclMLVncMsgQInterface::~spi_tclMLVncMsgQInterface()"));

   //Terminate the Message Queue threader before deleting it.
   if(NULL != m_poVncMsgQThreader)
   {
      MessageQueue *poMsgQ = m_poVncMsgQThreader->poGetMessageQueu();
      if(NULL != poMsgQ)
      {
         trMsgBase oMsgQTermMsg;
         poMsgQ->s16Push(static_cast<t_Void*>(&oMsgQTermMsg),0,1,e8_TCL_THREAD_TERMINATE_MESSAGE);
      }//if(NULL != poMsgQ)
      //Wait for the message queue thread to join.
      Threader::vWaitForTermination(m_poVncMsgQThreader->pGetThreadID());
   }//if(NULL != m_poVncMsgQThreader)

   RELEASE_MEM(m_poVncMsgQThreader);
   RELEASE_MEM(m_poVncMsgQThreadable);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncMsgQInterface::bWriteMsgToQ
 ***************************************************************************/
t_Bool spi_tclMLVncMsgQInterface::bWriteMsgToQ(trMsgBase *prMsgBase,
         t_U32 u32MsgSize)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetQ = false;

   if (NULL != m_poVncMsgQThreader)
   {
      //! Get the MsgQ form threader and push the message to Q
      MessageQueue *poMsgQ = m_poVncMsgQThreader->poGetMessageQueu();
      if ((NULL != poMsgQ) && (NULL != prMsgBase))
      {
         t_S32 s32RetMsgQ = poMsgQ->s16Push(static_cast<t_Void*>(prMsgBase), u32MsgSize);
         bRetQ = (0 == s32RetMsgQ);
      } // if ((NULL != poMsgQ) && (NULL != prMsgBase))
   } //if (NULL != m_poVncMsgQThreader)

   if(bRetQ == false)
   {
      ETG_TRACE_ERR(("Write to MsgQ failed  \n"));
   }

   return bRetQ;
}
