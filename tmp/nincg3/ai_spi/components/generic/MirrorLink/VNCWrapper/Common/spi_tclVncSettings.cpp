/*!
 *******************************************************************************
 * \file             spi_tclVncSettings.h
 * \brief            Project specific settings for vnc wrapper
 * \addtogroup       Vncectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Project specific settings for Vncection Management
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 28.07.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 13.11.2014 | Shiva Kumar Gurija           | Fix for Suzuki-20039

06.05.2015  |Tejaswini HB                 |Lint Fix
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <cstring>
#include "SPITypes.h"
#include "FileHandler.h"
#include "XmlDocument.h"
#include "XmlReader.h"
#include "spi_tclVncSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclVncSettings.cpp.trc.h"
#endif
#endif

#ifdef VARIANT_S_FTR_ENABLE_GM
static const t_Char scoczGMXmlConfigFile[] = "/opt/bosch/gm/vncsettings.xml";
#else
static const t_Char scoczG3GXmlConfigFile[] = "/opt/bosch/spi/xml/vncsettings.xml";
#endif
static const char sczMLCertiPrefFile[]= "/var/opt/bosch/dynamic/spi/CertiPref.dat";
//static const char sczCCCCertificate[]="CCC"; - unused currently
static const char sczCTSCertificate[]="CTS";


using namespace shl::xml;


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclVncSettings::~spi_tclVncSettings
 ***************************************************************************/
spi_tclVncSettings::~spi_tclVncSettings()
{

}
/***************************************************************************
 ** FUNCTION:  tenEnabledInfo spi_tclVncSettings::bIntializeVncSettings
 ***************************************************************************/
t_Void spi_tclVncSettings::vIntializeVncSettings()
{
   const t_Char* pczConfigFilePath = NULL;
#ifdef VARIANT_S_FTR_ENABLE_GM
   pczConfigFilePath = scoczGMXmlConfigFile;
#else
   pczConfigFilePath = scoczG3GXmlConfigFile;
#endif

   //Check the validity of the xml file
   spi::io::FileHandler oPolicySettingsFile(pczConfigFilePath,
            spi::io::SPI_EN_RDONLY);
   if (true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument oXmlDoc(pczConfigFilePath);
      tclXmlReader oXmlReader(&oXmlDoc, this);
      oXmlReader.bRead("VNCSETTINGS");
   } // if (true == oPolicySettingsFile.bIsValid())

   vDisplayVncSettings();
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclVncSettings::vGetLicenseFilePath
 ***************************************************************************/
t_Void spi_tclVncSettings::vGetLicenseFilePath(t_String &rfszLicenseFilePath)
{
   rfszLicenseFilePath = m_szLicensePath;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclVncSettings::vGetMLCertificatePath
 ***************************************************************************/
t_Void spi_tclVncSettings::vGetMLCertificatePath(t_String &rfszMLCertificatePath)
{
   rfszMLCertificatePath = m_szMLCertificatePath;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclVncSettings::vGetExcludedDeviceList
 ***************************************************************************/
t_Void spi_tclVncSettings::vGetExcludedDeviceList(std::multimap<t_S32, t_S32> &rfrExcludedDevList)
{
   rfrExcludedDevList = m_mapDevExclusionList;
}

/***************************************************************************
** FUNCTION:  t_U16 spi_tclVncSettings::u16GetUPnPAppEventsEnabledFromThePort
***************************************************************************/
t_U16 spi_tclVncSettings::u16GetUPnPAppEventsEnabledFromThePort()
{
   ETG_TRACE_USR2(("spi_tclVncSettings::u16GetUPnPAppEventsEnabledFromThePort-%d",
      m_u16UPnPAppEventsEnabledFromThePort));
   return m_u16UPnPAppEventsEnabledFromThePort;
}

/***************************************************************************
** FUNCTION:  t_U8 spi_tclVncSettings::u8TotalUPnPAppEventsEnabledPorts
***************************************************************************/
t_U8 spi_tclVncSettings::u8TotalUPnPAppEventsEnabledPorts()
{
   ETG_TRACE_USR2(("spi_tclVncSettings::u8TotalUPnPAppEventsEnabledPorts-%d",
      m_u8UPnPAppEventsEnabledOnNumOfPorts));
   return m_u8UPnPAppEventsEnabledOnNumOfPorts;
}

/***************************************************************************
** FUNCTION:  t_U16 spi_tclVncSettings::u16GetUPnPNotiEventsEnabledFromThePort
***************************************************************************/
t_U16 spi_tclVncSettings::u16GetUPnPNotiEventsEnabledFromThePort()
{
   ETG_TRACE_USR2(("spi_tclVncSettings::u16GetUPnPNotiEventsEnabledFromThePort-%d",
      m_u16UPnPNotiEventsEnabledFromThePort));
   return m_u16UPnPNotiEventsEnabledFromThePort;
}

/***************************************************************************
** FUNCTION:  t_U8 spi_tclVncSettings::u8TotalUPnPNotiEventsEnabledPorts
***************************************************************************/
t_U8 spi_tclVncSettings::u8TotalUPnPNotiEventsEnabledPorts()
{
   ETG_TRACE_USR2(("spi_tclVncSettings::u8TotalUPnPNotiEventsEnabledPorts-%d",
      m_u8UPnPNotiEventsEnabledOnNumOfPorts));
   return m_u8UPnPNotiEventsEnabledOnNumOfPorts;
}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclVncSettings::spi_tclVncSettings
 ***************************************************************************/
spi_tclVncSettings::spi_tclVncSettings():m_u16UPnPAppEventsEnabledFromThePort(0),
m_u8UPnPAppEventsEnabledOnNumOfPorts(0),
m_u16UPnPNotiEventsEnabledFromThePort(0),
m_u8UPnPNotiEventsEnabledOnNumOfPorts(0)
{
   ETG_TRACE_USR1((" spi_tclVncSettings::spi_tclVncSettings() entered \n"));
   //! Values initialized to default values (Used if xml settings not found)
}

/*************************************************************************
 ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
 *************************************************************************/
t_Bool spi_tclVncSettings::bXmlReadNode(xmlNodePtr poNode)
{
  // t_S32 s32Value = 0;
   t_Bool bRetVal = false;
   t_String szNodeName;

   if (NULL != poNode)
   {
      szNodeName = (const t_Char*) (poNode->name);
   } // if (NULL != poNode)

   if ("LICENSE_FILES" == szNodeName)
   {
      bRetVal = bGetAttribute("REALVNC_LICENSE", poNode, m_szLicensePath);

      //! Check the preferred certificate file before loading the
      //! mirrorlink trust root certificate path.
      //! CTS or CCC trust root certificate is loaded based on the
      //! preeference set in sczMLCertiPrefFile file. by default
      //! CCC certificate is used
      spi::io::FileHandler oFileHandler(sczMLCertiPrefFile,
               spi::io::SPI_EN_RDONLY);
      t_S32 s32FileSize = oFileHandler.s32GetSize();

      t_Char* pczCertiPref = NULL;
      if (s32FileSize > 0)
      {
         pczCertiPref = new t_Char[s32FileSize + 1];
         if (NULL != pczCertiPref)
         {
            memset((t_Void*) pczCertiPref, '\0', s32FileSize + 1);
            oFileHandler.bFRead(pczCertiPref, s32FileSize);
         }
      }

      if((NULL != pczCertiPref) && (0 == strncmp(pczCertiPref,sczCTSCertificate, sizeof(sczCTSCertificate))))
      {
         ETG_TRACE_USR4(("spi_tclVncSettings::bXmlReadNode pczCertiPref = %s\n", pczCertiPref));
         bRetVal = bGetAttribute("CTS_CERTIFICATE", poNode, m_szMLCertificatePath);
      }
      else
      {
         bRetVal = bGetAttribute("CCC_CERTIFICATE", poNode, m_szMLCertificatePath);
      }
      RELEASE_ARRAY_MEM(pczCertiPref);
   } //if ("PERSISTENT_STORAGE" == szNodeName)

   else if ("DEVICE_EXCLUSION_LIST" == szNodeName)
   {
      t_S32 s32VendorID, s32ProductID = 0;
      bRetVal = bGetAttribute("VENDORID", poNode, s32VendorID);
      bRetVal = bGetAttribute("PRODUCTID", poNode, s32ProductID) && bRetVal;
      if( true == bRetVal)
      {
         m_mapDevExclusionList.insert(std::pair<t_S32, t_S32> (s32VendorID, s32ProductID));
      }
   } //if ("PERSISTENT_STORAGE" == szNodeName)
   else if("UPNP_APPEVENTS_ENABLED_PORTS" == szNodeName)
   {
      t_S32 s32PortNoFrom = 0 ;
      t_S32 s32TotalNumOfPorts = 0 ;
      bRetVal = bGetAttribute("PORTNOFROM", poNode,s32PortNoFrom);
      bRetVal = bGetAttribute("NUMOFPORTS", poNode,s32TotalNumOfPorts);
      m_u16UPnPAppEventsEnabledFromThePort = (0<s32PortNoFrom)?((t_U16)s32PortNoFrom):0 ;
      m_u8UPnPAppEventsEnabledOnNumOfPorts = (0<s32TotalNumOfPorts)?((t_U8)s32TotalNumOfPorts):0;
   }
   else if("UPNP_NOTIFICATIONS_ENABLED_PORTS" == szNodeName)
   {
      t_S32 s32PortNoFrom = 0 ;
      t_S32 s32TotalNumOfPorts = 0 ;
      bRetVal = bGetAttribute("PORTNOFROM", poNode,s32PortNoFrom);
      bRetVal = bGetAttribute("NUMOFPORTS", poNode,s32TotalNumOfPorts);
      m_u16UPnPNotiEventsEnabledFromThePort = (0<s32PortNoFrom)?((t_U16)s32PortNoFrom):0 ;
      m_u8UPnPNotiEventsEnabledOnNumOfPorts = (0<s32TotalNumOfPorts)?((t_U8)s32TotalNumOfPorts):0;
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclVncSettings::vDisplayVncSettings
 ***************************************************************************/
t_Void spi_tclVncSettings::vDisplayVncSettings()
{
   ETG_TRACE_USR1((" spi_tclVncSettings::vDisplayVncSettings() entered \n"));
   ETG_TRACE_USR2((" License file Path  = %s\n",m_szLicensePath.c_str()));
   ETG_TRACE_USR2((" Mirrorlink certificate Path  = %s\n",m_szMLCertificatePath.c_str()));

   ETG_TRACE_USR2((" UPnP Application Events Enabled from the Port: %d Total ports %d",
      m_u16UPnPAppEventsEnabledFromThePort,
      m_u8UPnPAppEventsEnabledOnNumOfPorts));

   ETG_TRACE_USR2((" UPnP Notifications Events Enabled from the Port: %d Total ports %d",
      m_u16UPnPNotiEventsEnabledFromThePort,
      m_u8UPnPNotiEventsEnabledOnNumOfPorts));

   ETG_TRACE_USR2((" Blacklisted Devices  List : \n"));

   for(auto itMapExclusionList = m_mapDevExclusionList.begin();
            itMapExclusionList != m_mapDevExclusionList.end();
            itMapExclusionList++)
   {
      m_mapDevExclusionList.equal_range(itMapExclusionList->first);
      ETG_TRACE_USR2((" VendorID = %d\t", (itMapExclusionList->first)));
      ETG_TRACE_USR2((" ProductID = %d\n",(itMapExclusionList->second)));
   }
}

//lint –restore
