/*!
 *******************************************************************************
 * \file             spi_tclVncSettings.h
 * \brief            Project specific settings for vnc wrapper
 * \addtogroup       Vncectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Project specific settings for Vncection Management
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 28.07.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 13.11.2014 | Shiva Kumar Gurija           | Fix for Suzuki-20039

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLVNCSETTINGS_H_
#define SPI_TCLVNCSETTINGS_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <map>
#include "BaseTypes.h"
#include "GenericSingleton.h"
#include "Xmlable.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclVncSettings
 * \brief Project specific settings for vnc wrapper
 */
class spi_tclVncSettings: public GenericSingleton<spi_tclVncSettings>, public shl::xml::tclXmlReadable
{
      public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclVncSettings::~spi_tclVncSettings
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclVncSettings()
       * \brief  Destructor
       * \sa     spi_tclVncSettings()
       **************************************************************************/
      ~spi_tclVncSettings();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclVncSettings::vIntializeVncSettings
       ***************************************************************************/
      /*!
       * \fn     t_Void vIntializeVncSettings()
       * \brief  Reads settings from xml file and store it internally
       **************************************************************************/
      t_Void vIntializeVncSettings();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclVncSettings::vDisplayVncSettings
       ***************************************************************************/
      /*!
       * \fn     t_Void vDisplayVncSettings()
       * \brief  Displays Vnc wrapper settings from xml file
       **************************************************************************/
      t_Void vDisplayVncSettings();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclVncSettings::vGetLicenseFilePath
       ***************************************************************************/
      /*!
       * \fn     t_Void vGetLicenseFilePath()
       * \brief  returns path for license file storage
       **************************************************************************/
      t_Void vGetLicenseFilePath(t_String &rfszLicenseFilePath);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclVncSettings::vGetMLCertificatePath
       ***************************************************************************/
      /*!
       * \fn     t_Void vGetMLCertificatePath()
       * \brief  returns path for Mirrorlink certificate storage
       **************************************************************************/
      t_Void vGetMLCertificatePath(t_String &rfszMLCertificatePath);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclVncSettings::vGetExcludedDeviceList
       ***************************************************************************/
      /*!
       * \fn     t_Void vGetExcludedDeviceList()
       * \brief  returns the Devices which has to be exclused from offering to
       *         Mirrorlink discovery
       **************************************************************************/
      t_Void vGetExcludedDeviceList(std::multimap<t_S32, t_S32> &rfrExcludedDevList);

      /***************************************************************************
       ** FUNCTION:  t_U16 spi_tclVncSettings::u16GetUPnPAppEventsEnabledFromThePort
       ***************************************************************************/
      /*!
       * \fn     t_U16 u16GetUPnPAppEventsEnabledFromThePort()
       * \brief  Method to provide the static Port Number, on which UPnP AppList and AppStatus
       *         updates reception is enabled
       * \retval : t_U16 - Static Port Number
       **************************************************************************/
      t_U16 u16GetUPnPAppEventsEnabledFromThePort();

      /***************************************************************************
       ** FUNCTION:  t_U8 spi_tclVncSettings::u8TotalUPnPAppEventsEnabledPorts
       ***************************************************************************/
      /*!
       * \fn     t_U8 u8TotalUPnPAppEventsEnabledPorts()
       * \brief  Method to provide the total number of ports, on which UPnP AppList and AppStatus
       *         updates reception is enabled
       * \retval : t_U8 - Total ports
       **************************************************************************/
      t_U8 u8TotalUPnPAppEventsEnabledPorts();

      /***************************************************************************
       ** FUNCTION:  t_U16 spi_tclVncSettings::u16GetUPnPNotiEventsEnabledFromThePort
       ***************************************************************************/
      /*!
       * \fn     t_U16 u16GetUPnPNotiEventsEnabledFromThePort()
       * \brief  Method to provide the static Port Number, on which UPnP Notification
       *         updates reception is enabled
       * \retval : t_U16 - Static Port Number
       **************************************************************************/
      t_U16 u16GetUPnPNotiEventsEnabledFromThePort();

      /***************************************************************************
       ** FUNCTION:  t_U8 spi_tclVncSettings::u8TotalUPnPNotiEventsEnabledPorts
       ***************************************************************************/
      /*!
       * \fn     t_U8 u8TotalUPnPNotiEventsEnabledPorts()
       * \brief  Method to provide the total number of ports, on which UPnP Notification
       *         updates reception is enabled
       * \retval : t_U8 - Total ports
       **************************************************************************/
      t_U8 u8TotalUPnPNotiEventsEnabledPorts();

   private:
      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclVncSettings::spi_tclVncSettings
       ***************************************************************************/
      /*!
       * \fn     spi_tclVncSettings()
       * \brief  Default Constructor
       * \sa      ~spi_tclVncSettings()
       **************************************************************************/
      spi_tclVncSettings();

      /*************************************************************************
       ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
       *************************************************************************/
      /*!
       * \fn     virtual bool bXmlReadNode(xmlNode *poNode)
       * \brief  virtual function to read data from a xml node
       * \param  poNode : [IN] pointer to xml node
       * \retval bool : true if success, false otherwise.
       *************************************************************************/
      virtual t_Bool bXmlReadNode(xmlNodePtr poNode);

      //! Generic singleton class (to access private constructor)
      friend class GenericSingleton<spi_tclVncSettings> ;

      //! Following member variables store the settings read from xml file
      //! Storage path for license
      t_String m_szLicensePath;

      //! Storage path for Mirrorlink certificate
      t_String m_szMLCertificatePath;

      //!Device exclusion list
      std::multimap<t_S32, t_S32> m_mapDevExclusionList;

     //!Port number from which the UPnP App events reception is enabled
      t_U16 m_u16UPnPAppEventsEnabledFromThePort;

      //! Number of ports, that can be used to receive UPnP App events
      t_U8 m_u8UPnPAppEventsEnabledOnNumOfPorts;

      //!Port number from which the UPnP Notification events reception is enabled
      t_U16 m_u16UPnPNotiEventsEnabledFromThePort;

      //! Number ports, that can be used to receive UPnP notification events
      t_U8 m_u8UPnPNotiEventsEnabledOnNumOfPorts;
};
/*! } */



#endif /* SPI_TCLVNCSETTINGS_H_ */
