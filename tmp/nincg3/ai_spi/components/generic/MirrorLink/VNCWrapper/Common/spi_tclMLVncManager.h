/*!
 *******************************************************************************
 * \file              spi_tclMLVncManager.h
 * \brief            RealVNC Wrapper Manager
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper Manager to provide interface to SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 22.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/
#ifndef SPI_TCLMLVNCMANAGER_H_
#define SPI_TCLMLVNCMANAGER_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "GenericSingleton.h"
#include "spi_tclMLVncCmdNotif.h"
#include "spi_tclMLVncCmdDiscoverer.h"
#include "spi_tclMLVncCmdDAP.h"
#include "spi_tclMLVncCmdViewer.h"
#include "spi_tclMLVncCmdAudio.h"
#include "spi_tclMLVncCmdCDB.h"

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLVncCmdNotif
 * \brief Manager class  to provide interface for SPI to interact with
 *        VNCWrapper classes
 *
 */
class spi_tclMLVncManager: public GenericSingleton<spi_tclMLVncManager>
{
   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncManager::~spi_tclMLVncManager();
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclMLVncManager()
       * \brief   Destructor
       * \sa      spi_tclMLVncManager()
       **************************************************************************/
      ~spi_tclMLVncManager();

      
      /***************************************************************************
       ** FUNCTION:    spi_tclMLVncCmdAudio* poGetAudioInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclMLVncCmdAudio* poGetAudioInstance()
       * \brief   Get Audio command class's instance
       **************************************************************************/
      spi_tclMLVncCmdAudio* poGetAudioInstance();


      /***************************************************************************
       ** FUNCTION:    spi_tclMLVncCmdNotif* poGetNotifInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclMLVncCmdNotif* poGetNotifInstance()
       * \brief   Get Notification command class's instance
       **************************************************************************/
      spi_tclMLVncCmdNotif* poGetNotifInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclMLVncCmdDiscoverer* poGetDiscovererInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclMLVncCmdDiscoverer* poGetDiscovererInstance()
       * \brief   Get Discoverer command class's instance
       **************************************************************************/
      spi_tclMLVncCmdDiscoverer* poGetDiscovererInstance();

      /***************************************************************************
       ** FUNCTION:   spi_tclMLVncCmdDAP* poGetDAPInstance()
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncCmdDAP* poGetDAPInstance()
       * \brief   Get DAP command class's instance
       **************************************************************************/
      spi_tclMLVncCmdDAP* poGetDAPInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclMLVncCmdViewer* poGetViewerInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclMLVncCmdViewer* poGetViewerInstance()
       * \brief   Get Viewer command class's instance
       **************************************************************************/
      spi_tclMLVncCmdViewer* poGetViewerInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclMLVncCmdCDB* poGetCDBInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclMLVncCmdCDB* poGetCDBInstance()
       * \brief    Get Viewer command class's instance
       **************************************************************************/
      spi_tclMLVncCmdCDB* poGetCDBInstance();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLVncManager::bRegisterObject(RespBase *poRespBase)
       ***************************************************************************/
      /*!
       * \fn       t_Bool spi_tclMLVncManager::bRegisterObject(RespBase *poRespBase)
       * \brief   Registers the response class object
       * \ret      returns true on success
       **************************************************************************/
      t_Bool bRegisterObject(RespBase *poRespBase);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLVncManager::bUnResgisterObject(RespBase *poRespBase)
       ***************************************************************************/
      /*!
       * \fn       t_Bool spi_tclMLVncManager::bUnResgisterObject(RespBase *poRespBase)
       * \brief   Registers the response class object
       * \ret      returns true on success
       **************************************************************************/
      t_Bool bUnResgisterObject(RespBase *poRespBase);

      //! Base Singleton class
      friend class GenericSingleton<spi_tclMLVncManager> ;

      /***************************************************************************
       *******************************PRIVATE************************************
       **************************************************************************/
   private:

      //! Notification command class object
      spi_tclMLVncCmdNotif *m_poCmdNotif;

      //! Discoverer command class object
      spi_tclMLVncCmdDiscoverer *m_poCmdDiscoverer;

      //! DAP command class object
      spi_tclMLVncCmdDAP *m_poCmdDAP;

      //! Viewer command class object
      spi_tclMLVncCmdViewer *m_poCmdViewer;

      //! Audio command class object
      spi_tclMLVncCmdAudio *m_poCmdAudio;

      //! CDB command class object
      spi_tclMLVncCmdCDB *m_poCmdCdb;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncManager::spi_tclMLVncManager()
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncManager()
       * \brief   Default Constructor
       * \sa      ~spi_tclMLVncManager()
       **************************************************************************/
      spi_tclMLVncManager();
};

#endif

