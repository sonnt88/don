/***********************************************************************/
/*!
 * \file  spi_tclMLVncMsgQThreadable.h
 * \brief implements threading based on MsgQthreader for VNC Wrappers
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    implements threading based on MsgQthreader for VNC Wrappers
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 29.10.2013  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/

#include "spi_tclMLVncMsgQThreadable.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclMLVncMsgQThreadable.cpp.trc.h"
   #endif
#endif

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscoverer::spi_tclMLVncDiscoverer()
 ***************************************************************************/
spi_tclMLVncMsgQThreadable::spi_tclMLVncMsgQThreadable() :
                  m_poDiscovererDispatcher(NULL),
                  m_poViewerDispatcher(NULL),
                  m_poAudioDispatcher(NULL),
                  m_poDAPDispatcher(NULL),
                  m_poNotifDispatcher(NULL),
                  m_poCDBDispatcher(NULL)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_poDiscovererDispatcher = new spi_tclMLVncDiscovererDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poDiscovererDispatcher);
   m_poViewerDispatcher = new spi_tclMLVncViewerDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poViewerDispatcher);
   m_poAudioDispatcher = new spi_tclMLVncAudioDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poAudioDispatcher);
   m_poNotifDispatcher = new spi_tclMLVncNotifDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poNotifDispatcher);
   m_poDAPDispatcher = new spi_tclMLVncDAPDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poDAPDispatcher);
   m_poCDBDispatcher = new spi_tclMLVncCDBDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poCDBDispatcher);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscoverer::~spi_tclMLVncDiscoverer()
 ***************************************************************************/

spi_tclMLVncMsgQThreadable::~spi_tclMLVncMsgQThreadable()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   RELEASE_MEM(m_poDiscovererDispatcher);
   RELEASE_MEM(m_poViewerDispatcher);
   RELEASE_MEM(m_poAudioDispatcher);
   RELEASE_MEM(m_poNotifDispatcher);
   RELEASE_MEM(m_poDAPDispatcher);
   RELEASE_MEM(m_poCDBDispatcher);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscoverer::vExecute
 ***************************************************************************/

t_Void spi_tclMLVncMsgQThreadable::vExecute(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trMsgBase *prMsgBase = static_cast<trMsgBase*>(poMessage->pvBuffer);
      if (NULL != prMsgBase)
      {
         t_U32 u32ServiceID = prMsgBase->u32GetServiceID();
         ETG_TRACE_USR1(("ServiceID %d \n", u32ServiceID));

         //! Handle message to corresponding dispatcher based on Service ID
         switch (u32ServiceID)
         {
            case e32MODULEID_VNCDISCOVERER:
            {
               MLDiscovererMsgBase *prDiscMsgBase =
                        static_cast<MLDiscovererMsgBase*>(poMessage->pvBuffer);
               if (NULL != prDiscMsgBase)
               {
                  prDiscMsgBase->vDispatchMsg(m_poDiscovererDispatcher);
               } // if (NULL != prDiscMsgBase)
            }
               break;

            case e32MODULEID_VNCVIEWER:
            {
               MLViewerMsgBase *prViewerMsgBase =
                        static_cast<MLViewerMsgBase*>(poMessage->pvBuffer);
               if (NULL != prViewerMsgBase)
               {
                  prViewerMsgBase->vDispatchMsg(m_poViewerDispatcher);
               } // if (NULL != prViewerMsgBase)
            }
               break;

            case e32MODULEID_VNCCDB:
            {
               MLCDBMsgBase *prCDBMsgBase =
                        static_cast<MLCDBMsgBase*>(poMessage->pvBuffer);
               if (NULL != prCDBMsgBase)
               {
                  prCDBMsgBase->vDispatchMsg(m_poCDBDispatcher);
               } // if (NULL != prViewerMsgBase)
            }
               break;

            case e32MODULEID_VNCAUDIO:
            {
               MLAudioMsgBase *prAudioMsgBase =
                        static_cast<MLAudioMsgBase*>(poMessage->pvBuffer);
               if (NULL != prAudioMsgBase)
               {
                  prAudioMsgBase->vDispatchMsg(m_poAudioDispatcher);
               }// if (NULL != prAudioMsgBase)
            }
               break;

            case e32MODULEID_VNCNOTIFICATIONS:
            {
               MLNotifMsgBase *prNotifMsgBase =
                        static_cast<MLNotifMsgBase*>(poMessage->pvBuffer);
               if (NULL != prNotifMsgBase)
               {
                  prNotifMsgBase->vDispatchMsg(m_poNotifDispatcher);
               } // if (NULL != prNotifMsgBase)
            }
               break;

            case e32MODULEID_VNCDAP:
            {
               MLDAPMsgBase *prDAPMsgBase =
                        static_cast<MLDAPMsgBase*>(poMessage->pvBuffer);
               if (NULL != prDAPMsgBase)
               {
                  prDAPMsgBase->vDispatchMsg(m_poDAPDispatcher);
               } //  if (NULL != prDAPMsgBase)
            }
               break;

            default:
            {
               ETG_TRACE_ERR(("Unknown ServiceID %d \n", u32ServiceID));
            }
               break;
         } // switch (u32ServiceID)
      } // if (NULL != prMsgBase)

      t_U8 *pU8Buffer = static_cast<t_PU8> (poMessage->pvBuffer);
      RELEASE_ARRAY_MEM(pU8Buffer);
   }
   RELEASE_MEM(poMessage);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscoverer::~spi_tclMLVncDiscoverer()
 ***************************************************************************/
tShlMessage* spi_tclMLVncMsgQThreadable::poGetMsgBuffer(size_t siBuffer)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   tShlMessage* poMessage = new tShlMessage;

   if (NULL != poMessage)
   {
      if(0 < siBuffer)
      {
         //! Allocate the requested memory
         poMessage->pvBuffer = new t_U8[siBuffer];
      }
      else
      {
         poMessage->pvBuffer = NULL;
      } // if(0 < siBuffer)

      if (NULL != poMessage->pvBuffer)
      {
         poMessage->size = siBuffer;
      }
      else
      {
         //! Free the message as internal allocation failed.
         delete poMessage;
         poMessage = NULL;
      } //   if (NULL != poMessage->pvBuffer)
   } // if (NULL != poMessage)

   return poMessage;
}
