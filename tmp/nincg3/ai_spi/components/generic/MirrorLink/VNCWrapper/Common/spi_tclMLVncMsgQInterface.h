/***********************************************************************/
/*!
 * \file  spi_tclMLVncMsgQInterface.h
 * \brief interface for writing data to Q to use the MsgQ based
 *        threading model for VNC Wrappers
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    interface for writing data to Q to use the MsgQ based
                 threading model for VNC Wrappers
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/

#ifndef SPI_TCLMLVNCMSGQINTERFACE_H_
#define SPI_TCLMLVNCMSGQINTERFACE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "MsgQThreader.h"
#include "spi_tclMLVncMsgQThreadable.h"
#include "GenericSingleton.h"

using namespace shl::thread;

/****************************************************************************/
/*!
* \class spi_tclMLVncMsgQInterface
* \brief interface for writing data to Q to use the MsgQ based
*        threading model for VNC Wrappers
****************************************************************************/
class spi_tclMLVncMsgQInterface : public GenericSingleton<spi_tclMLVncMsgQInterface>
{
   public:

      /***************************************************************************
      ** FUNCTION:  spi_tclMLVncMsgQInterface::spi_tclMLVncMsgQInterface()
      ***************************************************************************/
      /*!
      * \fn      spi_tclMLVncMsgQInterface()
      * \brief   Default Constructor
      * \sa      ~spi_tclMLVncMsgQInterface()
      **************************************************************************/
      spi_tclMLVncMsgQInterface();

      /***************************************************************************
      ** FUNCTION:  spi_tclMLVncMsgQInterface::~spi_tclMLVncMsgQInterface()
      ***************************************************************************/
      /*!
      * \fn      ~spi_tclMLVncMsgQInterface()
      * \brief   Destructor
      * \sa      spi_tclMLVncMsgQInterface()
      **************************************************************************/
      ~spi_tclMLVncMsgQInterface();

      /***************************************************************************
      ** FUNCTION:  spi_tclMLVncMsgQInterface::bWriteMsgToQ
      ***************************************************************************/
      /*!
      * \fn      bWriteMsgToQ(trMsgBase *prMsgBase, t_U32 u32MsgSize)
      * \brief   Write Msg to Q for dispatching the message by a seperate thread
      * \param prMsgBase : Pointer to Base Message type
      * \param u32MsgSize :  size of the message to be written to the MsgQ
      **************************************************************************/
      t_Bool bWriteMsgToQ(trMsgBase *prMsgBase, t_U32 u32MsgSize);

      //! Base Singleton class
      friend class GenericSingleton<spi_tclMLVncMsgQInterface> ;

   private:

      //! Pointer to the overridden MsgQThreadable class
      spi_tclMLVncMsgQThreadable *m_poVncMsgQThreadable;

      //! pointer to MsgQThreader
      MsgQThreader *m_poVncMsgQThreader;

};


#endif /* SPI_TCLMLVNCMSGQINTERFACE_H_ */
