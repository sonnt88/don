/*!
 *******************************************************************************
 * \file              spi_tclMLVncManager.cpp
 * \brief            RealVNC Wrapper Manager
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper Manager to provide interface to SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 22.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H
#include "RespRegister.h"
#include "spi_tclMLVncManager.h"
#include "spi_tclVncSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncManager.cpp.trc.h"
   #endif
#endif


/***************************************************************************
 ** FUNCTION:  spi_tclMLVncManager::spi_tclMLVncManager()
 ***************************************************************************/
spi_tclMLVncManager::spi_tclMLVncManager() :
   m_poCmdNotif(NULL), m_poCmdDiscoverer(NULL), m_poCmdDAP(NULL),
            m_poCmdViewer(NULL)
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::spi_tclMLVncManager  Entered \n"));
   m_poCmdNotif = new spi_tclMLVncCmdNotif();
   SPI_NORMAL_ASSERT(NULL == m_poCmdNotif);
   m_poCmdDiscoverer = new spi_tclMLVncCmdDiscoverer();
   SPI_NORMAL_ASSERT(NULL == m_poCmdDiscoverer);
   m_poCmdDAP = new spi_tclMLVncCmdDAP();
   SPI_NORMAL_ASSERT(NULL == m_poCmdDAP);
   m_poCmdViewer = new spi_tclMLVncCmdViewer();
   SPI_NORMAL_ASSERT(NULL == m_poCmdViewer);
   m_poCmdAudio = new spi_tclMLVncCmdAudio();
   SPI_NORMAL_ASSERT(NULL == m_poCmdAudio);
   m_poCmdCdb = new spi_tclMLVncCmdCDB();
   SPI_NORMAL_ASSERT(NULL == m_poCmdCdb);

   spi_tclVncSettings *poVncSettings = spi_tclVncSettings::getInstance();
   if(NULL != poVncSettings)
   {
      poVncSettings->vIntializeVncSettings();
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncManager::~spi_tclMLVncManager()
 ***************************************************************************/
spi_tclMLVncManager::~spi_tclMLVncManager()
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::~spi_tclMLVncManager  Entered \n"));
   RELEASE_MEM(m_poCmdCdb);
   RELEASE_MEM(m_poCmdAudio);
   RELEASE_MEM(m_poCmdViewer);
   RELEASE_MEM(m_poCmdDAP);
   RELEASE_MEM(m_poCmdDiscoverer);
   RELEASE_MEM(m_poCmdNotif);
}

/***************************************************************************
 ** FUNCTION:  const  spi_tclMLVncCmdAudio* poGetAudioInstance()
 ***************************************************************************/
spi_tclMLVncCmdAudio* spi_tclMLVncManager::poGetAudioInstance()
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::poGetAudioInstance  Entered \n"));
   return m_poCmdAudio;
}


/***************************************************************************
 ** FUNCTION:   spi_tclMLVncCmdNotif* poGetNotifInstance()
 ***************************************************************************/
spi_tclMLVncCmdNotif* spi_tclMLVncManager::poGetNotifInstance()
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::poGetNotifInstance  Entered \n"));
   return m_poCmdNotif;
}

/***************************************************************************
 ** FUNCTION:   spi_tclMLVncCmdDiscoverer* poGetDiscovererInstance()
 ***************************************************************************/
spi_tclMLVncCmdDiscoverer* spi_tclMLVncManager::poGetDiscovererInstance()
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::poGetDiscovererInstance  Entered \n"));
   return m_poCmdDiscoverer;
}

/***************************************************************************
 ** FUNCTION:   spi_tclMLVncCmdDAP* poGetDAPInstance()
 ***************************************************************************/
spi_tclMLVncCmdDAP* spi_tclMLVncManager::poGetDAPInstance()
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::poGetDAPInstance  Entered \n"));
   return m_poCmdDAP;
}

/***************************************************************************
 ** FUNCTION:   spi_tclMLVncCmdViewer* poGetViewerInstance()
 ***************************************************************************/
spi_tclMLVncCmdViewer* spi_tclMLVncManager::poGetViewerInstance()
{
   return m_poCmdViewer;
}

/***************************************************************************
 ** FUNCTION:    spi_tclMLVncCmdCDB* poGetCDBInstance()
 ***************************************************************************/
spi_tclMLVncCmdCDB* spi_tclMLVncManager::poGetCDBInstance()
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::poGetCDBInstance  Entered \n"));
   return m_poCmdCdb;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncManager::bRegisterObject(RespBase *poRespReg)
 ***************************************************************************/
t_Bool spi_tclMLVncManager::bRegisterObject(RespBase *poRespBase)
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::bRegisterObject  Entered \n"));
   RespRegister *pRespRegister = RespRegister::getInstance();
   t_Bool bRetReg = false;
   if(NULL!= pRespRegister)
   {
      bRetReg = pRespRegister->bRegisterObject(poRespBase);
   }
   return bRetReg;
}


/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncManager::bUnResgisterObject(RespBase *poRespReg)
 ***************************************************************************/
t_Bool spi_tclMLVncManager::bUnResgisterObject(RespBase *poRespBase)
{
   ETG_TRACE_USR1(("spi_tclMLVncManager::bUnResgisterObject  Entered \n"));
   RespRegister *pRespRegister = RespRegister::getInstance();
   t_Bool bRetReg = false;
   if(NULL!= pRespRegister)
   {
      bRetReg = pRespRegister->bUnregisterObject(poRespBase);
   }
   return bRetReg;
}
