/*
 * MLTypeDefines.h
 *
 *  Created on: Aug 2, 2013
 *      Author: ptr4kor
 */

#ifndef WRAPPERTYPEDEFINES_H_
#define WRAPPERTYPEDEFINES_H_

#include "vncint.h"
#include "BaseTypes.h"
#include <vector>
#include <string>
#include <cstring>
#include <stdio.h>
#include <sstream>
#include <stdlib.h>
#include "vnctypes.h"
#include "vncviewersdk.h"
#include "vncdiscoverysdk.h"
#include "vncmirrorlinkkeys.h"
#include <set>
#include <map>


/**********************Discovery Wrapper Defines Start*************************/
#define MAX_KEYSIZE 256
#define DISP(VAR) (std::cout<<#VAR<<" = "<<VAR<<std::endl)

typedef struct
{
      t_U32 u32ReqID;

} MLContext;


typedef enum enDiscovererType
{
   enMLDiscoverer = 1,
   enUSBDiscoverer,
   enWiFiDiscoverer,
   enIPODDiscoverer,
   enUnKnownDiscoverer
} tenDiscovererType ;

struct trvncDeviceHandles
{
   //SDK Handle
   const VNCDiscoverySDK* poDiscoverySdk;
   //Discoverer Handle
   const VNCDiscoverySDKDiscoverer* poDiscoverer;
   //Entity Handle
   const VNCDiscoverySDKEntity* poEntity;


   trvncDeviceHandles()
   {
      poDiscoverySdk = NULL ;
      poDiscoverer = NULL ;
      poEntity = NULL ;
   }
   ~trvncDeviceHandles()
   {
      poDiscoverySdk = NULL ;
      poDiscoverer = NULL ;
      poEntity = NULL ;
   }

   t_Void vInitialize()
   {
      poDiscoverySdk = NULL ;
      poDiscoverer = NULL ;
      poEntity = NULL ;
   }

};

/**********************Discovery Wrapper Defines End*************************/


/**********************Viewer Wrapper Defines Start*************************/

typedef struct
  {
	  VNCViewerSDK* pViewerSDK;
	  VNCViewer* pVNCViewer;
  }trViewerData;

/**********************Viewer Wrapper Defines End*************************/

/**********************Audio Wrapper Defines Start*************************/
  typedef struct
{
	pthread_mutex_t mutex;
	pthread_t gst_thread;

	//GMainLoop * gst_mainloop;
	t_U32 u32payloadtype;
	t_U32 u32channels;
	t_U32 u32samplerate;
	t_U32 u32ipl_ms;
	t_U32 u32ipaddr;
	t_U32 u32port;
	t_PU8 playback_device;

	//mlink_gst_rtp_out_error_callback * error_cb;
	//mlink_gst_rtp_out_data_callback * data_cb;
	t_Void * p_user_ctx;
	//DltContext * p_dlt_ctx;
}ML_rtp_out_context;
//typedef ML_rtp_out_context mlink_rtp_out_context;

typedef struct
{
	pthread_mutex_t mutex;
	pthread_t gst_thread;

	//GMainLoop * gst_mainloop;
	t_U32 payloadtype;
	t_U32 channels;
	t_U32 samplerate;
	t_U32 ipaddr;
	t_U32 port;
	t_PU8 capture_device;

	//mlink_gst_rtp_in_error_callback * error_cb;
	t_Void * p_user_ctx;
	//DltContext * p_dlt_ctx;
}ML_rtp_in_context;

//typedef ML_rtp_in_context mlink_rtp_in_context;

typedef struct
{
	t_PU8 audiosrc;
	t_PU8 audiosink;
	ML_rtp_out_context *p_rtp_out_ctx;
	ML_rtp_in_context *p_rtp_in_ctx;

	t_U32 u32rtp_payload_type;
	t_U32 u32IPL;

}MLapp_data;

/**********************Audio Wrapper Defines End*************************/



#endif /* WRAPPERTYPEDEFINES_H_ */
