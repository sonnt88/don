/***********************************************************************/
/*!
* \file  spi_tclMLVncMsgQThreadable.h
* \brief implements threading based on MsgQthreader for VNC Wrappers
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    implements threading based on MsgQthreader for VNC Wrappers
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
29.10.2013  | Pruthvi Thej Nagaraju | Initial Version

\endverbatim
*************************************************************************/

#ifndef SPI_TCLMLVNCMSGQTHREADER_H_
#define SPI_TCLMLVNCMSGQTHREADER_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "MsgQThreadable.h"
#include "spi_tclMLVncNotifDispatcher.h"
#include "spi_tclMLVncDAPDispatcher.h"
#include "spi_tclMLVncAudioDispatcher.h"
#include "spi_tclMLVncDiscovererDispatcher.h"
#include "spi_tclMLVncViewerDispatcher.h"
#include "spi_tclMLVncCDBDispatcher.h"

using namespace shl::thread;

/****************************************************************************/
/*!
* \class spi_tclMLVncMsgQThreadable
* \brief implements threading based on MsgQthreader for VNC Wrappers
*
* Responsible for calling the respective dispatchers and to allocate memory
* when a message arrives on Q
*
****************************************************************************/

class spi_tclMLVncMsgQThreadable : public MsgQThreadable
{
   public:

      /***************************************************************************
      ** FUNCTION:  spi_tclMLVncMsgQThreadable::spi_tclMLVncMsgQThreadable()
      ***************************************************************************/
      /*!
      * \fn      spi_tclMLVncMsgQThreadable()
      * \brief   Default Constructor
      * \sa      ~spi_tclMLVncMsgQThreadable()
      **************************************************************************/
      spi_tclMLVncMsgQThreadable();

      /***************************************************************************
      ** FUNCTION:  spi_tclMLVncMsgQThreadable::~spi_tclMLVncMsgQThreadable()
      ***************************************************************************/
      /*!
      * \fn      ~spi_tclMLVncMsgQThreadable()
      * \brief   Destructor
      * \sa      spi_tclMLVncMsgQThreadable()
      **************************************************************************/
      ~spi_tclMLVncMsgQThreadable();

   protected:
      /***************************************************************************
      ** FUNCTION:  spi_tclMLVncDiscoverer::vExecute
      ***************************************************************************/
      /*!
      * \fn      t_Void vExecute(tShlMessage *poMessage)
      * \brief   Responsible for posting the message to respective dispatchers
      * \param   poMessage : message received from MsgQ for dispatching
      **************************************************************************/
      virtual t_Void vExecute(tShlMessage *poMessage);

      /***************************************************************************
      ** FUNCTION:  spi_tclMLVncDiscoverer::tShlMessage* poGetMsgBuffer(size_t )
      ***************************************************************************/
      /*!
      * \fn      tShlMessage* poGetMsgBuffer(size_t )
      * \brief  This function will be called for requesting the storage allocation for received
      *           message
      * \param siBuffer: size of the buffer to be allocated for the received message
      **************************************************************************/
      virtual tShlMessage* poGetMsgBuffer(size_t siBuffer);

   private:

      //! Pointers to Message Dispatchers
      spi_tclMLVncDiscovererDispatcher *m_poDiscovererDispatcher;
      spi_tclMLVncViewerDispatcher *m_poViewerDispatcher;
      spi_tclMLVncAudioDispatcher *m_poAudioDispatcher;
      spi_tclMLVncDAPDispatcher *m_poDAPDispatcher;
      spi_tclMLVncNotifDispatcher *m_poNotifDispatcher;
      spi_tclMLVncCDBDispatcher *m_poCDBDispatcher;
};


#endif /* SPI_TCLMLVNCMSGQTHREADER_H_ */
