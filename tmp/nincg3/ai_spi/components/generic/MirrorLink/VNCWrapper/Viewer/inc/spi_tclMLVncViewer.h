/*!
 *******************************************************************************
 * \file              spi_tclMLVncViewer.h
 * \brief             RealVNC command Wrapper for processing VNC Viewer
 *******************************************************************************
 \verbatim
 PROJECT:         Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper for wrapping VNC calls for receiving
                 Framebuffer updates from ML Server
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 02.09.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
 20.11.2013 |  Shiva Kumar Gurija(RBEI/ECP2)   | Updated with the new elements for Video
 07.01.2014 |  Hari Priya E R                  | Changes for touch handling
 03.04.2013 |  Shiva Kumar Gurija(RBEI/ECP2)   | Device status messages
 08.07.2014 |  Shiva Kumar Gurija(RBEI/ECP2)   | FrameBufferBlocking changes to 
                                                  fix CTS issues
 06.11.2014  |  Hari Priya E R                 | Added changes to set Client key capabilities

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMLVNCVIEWER_H_
#define SPI_TCLMLVNCVIEWER_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "vncmirrorlink.h"
#include "vncviewersdk.h"
#include "Lock.h"
#include "GenericSingleton.h"
#include "BaseTypes.h"
#include "SPITypes.h"
#include "mlink_common.h"

class spi_tclMLVncRespViewer;
class RespRegister;

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLVncViewer
 * \brief This class wraps the RealVNC SDK calls to SPI component
 *        to implement the VNC Viewer service provided by MLServer.
 *
 */

class spi_tclMLVncViewer: public GenericSingleton<spi_tclMLVncViewer>
{

   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclMLVncViewer::~spi_tclMLVncViewer()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclMLVncViewer()
       * \brief   Destructor
       * \sa      spi_tclMLVncViewer()
       **************************************************************************/
       virtual ~spi_tclMLVncViewer();

      /***************************************************************************
        ** FUNCTION:  t_Bool spi_tclMLVncViewer::bInitializeViewerSDK()
        ***************************************************************************/
       /*!
        * \fn      bInitializeViewerSDK()
        * \brief   Initialize the Viewer SDK
        * \param   NONE
        * \retval  t_Bool
        **************************************************************************/
       t_Bool bInitializeViewerSDK();

       /***************************************************************************
        ** FUNCTION:  t_Void spi_tclMLVncViewer::vUninitializeViewerSDK()
        ***************************************************************************/
       /*!
        * \fn      vUninitializeViewerSDK()
        * \brief   Uninitialize the Viewer SDK
        * \param   NONE
        * \retval  NONE
        **************************************************************************/
        t_Void vUninitializeViewerSDK();

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vCreateVNCViewer()
       ***************************************************************************/
      /*!
       * \fn      vCreateVNCViewer(t_Void)
       * \brief   Create the VNC Viewer
       * \param   pu8ExtensionName : [IN]Extension name to be enabled
       * \retval  NONE
       **************************************************************************/
        t_Void vCreateVNCViewer();

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vDestroyVNCViewer()
       ***************************************************************************/
      /*!
       * \fn      vDestroyVNCViewer()
       * \brief   Destroy the VNC Viewer Instance
       * \param   NONE
       * \retval  NONE
       **************************************************************************/
       t_Void vDestroyVNCViewer();

       /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncViewer::bMLWLInitialize()
       ***************************************************************************/
       /*!
       * \fn      t_Bool bMLWLInitialize(t_U32 u32layer_id, t_U32 u32surface_id,t_U32 u32screen_offset_x,
       *          t_U32 u32screen_offset_y,t_U32 u32screen_width, t_U32 u32screen_height,
       *          t_Bool bForceFullScreen,t_U8 u8Flag)
       * \brief   To Initialize the wayland layer manager
       * \param   u32layer_id: [IN]Layer ID for ML Application
       * \param   u32surface_id: [IN] Surface ID for ML Application
       * \param   u32screen_offset_x: [IN] Initial screen offset x of LayerManager surface
       * \param   u32screen_offset_y: [IN] Initial screen offset y of LayerManager surface
       * \param   u32screen_width: [IN] Initial screen width of LayerManager surface
       * \param   u32screen_height; [IN]Initial screen height of LayerManager surface
       * \param   bForceFullScreen: [IN]If set to FALSE screen size will maintain
       *                                phone screen aspect ratio.
       *                                If set to TRUE phone screen is scaled to
       *                                screen_width and screen_height
       * \param   u32ScreenHeight_Mm: [IN] Screen Height in MM
       * \param   u32ScreenWidth_Mm : [IN] Screen width in MM
       * \param   u8Flag : [IN] Touch Input Handling Enable/disable
       * \retval  t_Bool
       **************************************************************************/
       t_Bool bMLWLInitialize(t_U32 u32layer_id, 
          t_U32 u32surface_id,
          t_U32 u32screen_offset_x,
          t_U32 u32screen_offset_y,
          t_U32 u32screen_width, 
          t_U32 u32screen_height,
          t_Bool bForceFullScreen,
          t_U8 u8Flag,
          t_U32 u32ScreenHeight_Mm,
          t_U32 u32ScreenWidth_Mm);

       /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vMLWLFinalise()
       ***************************************************************************/
       /*!
       * \fn      vMLWLFinalise()
       * \brief   Uninitialise Wayland
       * \param   NONE
       * \retval  NONE
       **************************************************************************/
       t_Void vMLWLFinalise();

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vSetContext()
       ***************************************************************************/
      /*!
       * \fn      vSetContext()
       * \brief   Sets the VNC Viewer SDK Context Pointer
       * \param   NONE
       * \retval  NONE
       **************************************************************************/
       t_Void vSetContext()const;

       /***************************************************************************
        ** FUNCTION: t_Void spi_tclMLVncViewer::rfrGetScreenAttributes(
        trScreenAttributes& rfrScreenAttributes)
        ***************************************************************************/
       /*!
        * \fn      rfrGetScreenAttributes(trScreenAttributes& rfrScreenAttributes)
        * \brief   Gets the server screen attributes
        * \param   rfrScreenAttributes: [IN] Reference to the screen attributes
        * \retval  NONE
        **************************************************************************/
       t_Void rfrGetScreenAttributes(trScreenAttributes& rfrScreenAttributes);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncViewer::bIsTouchSupported()
       ***************************************************************************/
      /*!
       * \fn      bIsTouchSupported()
       * \brief   Checks if the server supports touch events
       * \param   NONE
       * \retval  t_Bool
       **************************************************************************/
       t_Bool bIsTouchSupported();

	   /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncViewer::bIsPointerSupported()
       ***************************************************************************/
      /*!
       * \fn      bIsPointerSupported()
       * \brief   Checks if the server supports pointer events
       * \param   NONE
       * \retval  t_Bool
       **************************************************************************/
       t_Bool bIsPointerSupported();

       /***************************************************************************
       ** FUNCTION:  t_Void vSendTouchEventToServer(const t_U32 cou32DeviceHandle,
          trTouchData& rfrTouchData,
          trScalingAttributes& rfrScaleData)
       ***************************************************************************/
       /*!
       * \fn      vSendTouchEventToServer(trTouchData* pTouchData)
       * \brief   Send the touch related info to the server to trigger touch event
	   * \param   cou32DeviceHandle: [IN]Device Handle 
       * \param   pTouchData : [IN]Touch  related info
	   * \param   rfrScaleData: [IN]Scaling data
       * \retval  NONE
       **************************************************************************/
       t_Void vSendTouchEventToServer(const t_U32 cou32DeviceHandle,
          trTouchData& rfrTouchData,
          trScalingAttributes& rfrScaleData);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vSendKeyEventToServer(
       **                               tenKeyMode enKeyMode,tenKeyCode enKeyCode)
       ***************************************************************************/
      /*!
       * \fn      vSendKeyEventToServer(tenKeyMode enKeyMode,tenKeyCode enKeyCode)
       * \brief   Send the device key info to the server to trigger a device key event
       * \param   enKeyMode: [IN]Device Press or release
       * \param   enKeyCode: [IN]Key symbol code
       * \retval  NONE
       **************************************************************************/
       t_Void vSendKeyEventToServer(tenKeyMode enKeyMode,tenKeyCode enKeyCode);


      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncViewer::bSendDeviceStatusRequest
       **                                   (t_U32 u32DeviceStatusRequest)
       ***************************************************************************/
      /*!
       * \fn      bSendDeviceStatusRequest(t_U8 u8DeviceStatusRequest)
       * \brief   Send a device Status Message to the server
       * \param   u32DeviceStatusRequest : [IN]Device Status Feature to be sent
       * \retval  t_Bool
       **************************************************************************/
       t_Bool bSendDeviceStatusRequest(t_U32 u32DeviceStatusRequest);

       /***************************************************************************
        ** FUNCTION: t_Bool spi_tclMLVncViewer::bRegisterExtension
        **                                   (const t_Char* pcocExtensionname)
        ***************************************************************************/
       /*!
        * \fn      bRegisterExtension(const t_Char* pcocExtensionname)
        * \brief   Register Extension Messages to the server
        * \param   pcocExtensionname : [IN]Extension to be enabled
        * \retval  NONE
        **************************************************************************/
        t_Bool bRegisterExtension(const t_Char* pcocExtensionname);

      /*****************************************************************************
       ** FUNCTION: VNCViewerSDK* spi_tclMLVncViewer::pGetViewerSDK()
       *****************************************************************************/
      /*!
       * \fn      VNCViewerSDK* spi_tclMLVncViewer::pGetViewerSDK()
       * \brief   Returns the Viewer SDK instance
       * \param   NONE
       * \retval  VNCViewerSDK*: Viewer SDK instance
       **************************************************************************/
       VNCViewerSDK* pGetViewerSDK();

       /*****************************************************************************
        ** FUNCTION: VNCViewer* spi_tclMLVncViewer::pGetViewerInstance()
        *****************************************************************************/
       /*!
        * \fn      VNCViewer* spi_tclMLVncViewer::pGetViewerInstance()
        * \brief   Returns the VNC Viewer instance
        * \param   NONE
        * \retval  VNCViewer*: VNC Viewer instance
        **************************************************************************/
       VNCViewer* pGetViewerInstance();

       /*****************************************************************************
        ** FUNCTION: t_Void spi_tclMLVncViewer::vSubscribeforViewer()
        *****************************************************************************/
       /*!
        * \fn      t_Void vSubscribeforViewer()
        * \brief   Registers the viewer class
        * \param   NONE
        * \retval  NONE
        **************************************************************************/
        t_Void vSubscribeforViewer()const;

        /*****************************************************************************
         ** FUNCTION: t_Bool spi_tclMLVncViewer::bProcessCmdString
         ** (const t_U32 cou32AppId, const t_U32 cou32DeviceId)
         *****************************************************************************/
        /*!
         * \fn      bProcessCmdString(const t_U32 u32AppId, const t_U32 cou32DeviceId)
         * \brief   Fetch and Process the VNC Command String of an application
         * \param   cou32AppId: [IN] Application Id
         * \param   cou32DeviceId: [IN]Device Id
         * \retval  t_Bool
         **************************************************************************/
        t_Bool bProcessCmdString(const t_U32 cou32AppId, const t_U32 cou32DeviceId);

        /*****************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vSetSessionParameters
         ** (const t_Char* pcocPrefferedEncoding,
         ** const t_Char* pcocCntAttestFlag,const t_Char* pcocPublickey)
         *****************************************************************************/
        /*!
         * \fn      t_Void spi_tclMLVncViewer::vSetSessionParameters
         *               (const tChar* czPrefferedEncoding,t_U8 u8CntAttestFlag)
         * \brief   Sets the initial parameters for the VNC session
         * \param   pcocPrefferedEncoding: [IN]Preferred Encoding Method
         * \param   pcocCntAttestFlag: [IN]Content Attestation flag to
         *                 determine which contents to be attested
         * \param   pcocPublickey: [IN]ServerPublic Key
         * \retval  NONE
         **************************************************************************/
         t_Void vSetSessionParameters(const t_Char* pcocPrefferedEncoding,
           const t_Char* pcocCntAttestFlag,const t_Char* pcocPublickey);

         /*****************************************************************************
          ** FUNCTION: t_Bool spi_tclMLVncViewer::bAddLicense()
          *****************************************************************************/
         /*!
          * \fn      bAddLicense()
          * \brief   Adds the license to VNC Viewer
          * \param   NONE
          * \retval  t_Bool
          **************************************************************************/
         t_Bool bAddLicense();

         /*****************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vPumpWaylandEvents()
         *****************************************************************************/
         /*!
         * \fn      t_Void vPumpWaylandEvents()
         * \brief   Send the touch events to wayland.layer manager
         * \param   t_Void
         * \retval  t_Void
         **************************************************************************/
         t_Void vPumpWaylandEvents();


         /****************************************CRCB****************************/

         /***************************************************************************
          ** FUNCTION: t_Bool spi_tclMLVncViewer::
          **                  bSendFramebufferBlockingNotification(const t_U32 cou32AppID..)
          ***************************************************************************/
         /*!
          * \fn  bSendFramebufferBlockingNotification(const t_U32 cou32AppID,
          *        MLRectangle &corfrMLRectangle,
          *       MLFramebufferBlockReason enMLFrameBufferBlockReason);
          * \brief   requests the server to block the requested applications
          *          Framebuffer content
          * \param   cou32AppID : [IN] Application ID of which the Framebuffer
          *                      content is to be blocked
          * \param   corfrMLRectangle :[IN] Rectangular area of the Display whose
          *                      Framebuffer content has to be blocked
          * \param   enMLFrameBufferBlockReason : [IN] Reason for blocking FrameBuffer
          *                      content from a particular application
          * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
          * \reval   true :  Frame buffer blocking was successful
          *          false : Frame buffer blocking failed
          * \sa     bSendAudioBlockingNotification
          **************************************************************************/
         t_Bool bSendFramebufferBlockingNotification(const t_U32 cou32AppID,
               const trMLRectangle &corfrMLRectangle,
               tenMLFramebufferBlockReason enMLFrameBufferBlockReason,
               const t_U32 cou32DeviceHandle = 0);

         /***************************************************************************
          ** FUNCTION: t_Bool spi_tclMLVncViewer::
          **                        bSendAudioBlockingNotification(const t_U32 cou32AppID..)
          ***************************************************************************/
         /*!
          * \fn bSendAudioBlockingNotification(const t_U32 cou32AppID,
          *       MLAudioBlockReason enMLAudioBlockReason);
          * \brief   requests the server to block the requested applications
          *          Audio content
          * \param   cou32AppID :[IN} Application ID of which the Framebuffer content is to
          *                     be blocked
          * \param   enMLAudioBlockReason : [IN] Reason for blocking Audio
          *                      content from a particular application
          * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
          * \reval   true :  Audio blocking was successful
          *          false : Audio blocking failed
          * \sa     bSendFramebufferBlockingNotification
          **************************************************************************/
         t_Bool bSendAudioBlockingNotification(const t_U32 cou32AppID,
               tenMLAudioBlockReason enMLAudioBlockReason,
               const t_U32 cou32DeviceHandle = 0);

         /***************************************************************************
          ** FUNCTION: t_Bool spi_tclMLVncViewer::vRequestSetDriverDistractionAvoidance
          **                                    (const trUserContext corUserContext, t_Bool bDistractionAvoidance);
          ***************************************************************************/
         /*!
          * \fn vRequestSetDriverDistractionAvoidance(const trUserContext corUserContext, 
          *                     t_Bool bDistractionAvoidance)
          * \brief   enable or disable driver distraction avoidance. Enabling
          *          driver distraction will force the font, font size etc to be
          *          suitable for automotive environment and also disable content
          *          such as video
          * \param corUserContext : [IN] Context information passed from the caller of this function
          * \param   bDistractionAvoidance : [IN] t_Boolean value indication enabling or
          *             disabling of driver distraction avoidance feature
          * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
          * \sa   vPostDriverDistractionStatus
          **************************************************************************/
         t_Void vRequestSetDriverDistractionAvoidance(t_Bool bDistractionAvoidance,
                  const t_U32 cou32DeviceHandle = 0);

         /*****************************************************************************
         ** FUNCTION: mlink_dlt_context* spi_tclMLVncViewer::pGetDLTContext()
         *****************************************************************************/
         /*!
         * \fn      mlink_dlt_context* pGetDLTContext()
         * \brief   To return the DLT context pointer
         * \retval  mlink_dlt_context*
         **************************************************************************/ 
         mlink_dlt_context* pGetDLTContext();

         /*****************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vPopulateTouchEnableFlag(t_U16 
         u16TouchEnableFlag)
         *****************************************************************************/
         /*!
         * \fn      t_Void vPopulateTouchEnableFlag(t_U16 u16TouchEnableFlag)
         * \brief   Populate the touch enable flag indicating the enabling of touch events
         * \param   u16TouchEnableFlag: [IN]Touch Enable Flag
         * \retval  t_Void
         **************************************************************************/ 
         t_Void vPopulateTouchEnableFlag(t_U16 u16TouchEnableFlag);

         /*****************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vSetDayNightMode()
         *****************************************************************************/
         /*!
         * \fn      t_Void vSetDayNightMode(const t_Bool cobEnableNightMode)
         * \brief   Method to set the Day night mode
         * \param   cobEnableNightMode: [IN] Night mode Enable/disable
         * \retval  t_Void
         **************************************************************************/ 
         t_Void vSetDayNightMode(const t_Bool cobEnableNightMode);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vEnableKeyLock()
         ***************************************************************************/
         /*!
         * \fn     t_Void vEnableKeyLock(const t_Bool cobEnable)
         * \brief  Interface to Enable/disable Key lock (pointer& touch events from Phone)
         * \param  cobEnable   : [IN] TRUE - Enable & FALSE - Disable the Key Lock
         * \retval t_Void
         **************************************************************************/
         t_Void vEnableKeyLock(const t_Bool cobEnable);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vEnableDeviceLock()
         ***************************************************************************/
         /*!
         * \fn     t_Void vEnableDeviceLock(const t_Bool cobEnable)
         * \brief  Interface to Enable/disable Device lock 
         * \param  cobEnable   : [IN] TRUE - Enable & FALSE - Disable 
         * \retval t_Void
         **************************************************************************/
         t_Void vEnableDeviceLock(const t_Bool cobEnable);

         /***************************************************************************
         ** FUNCTION: t_Bool spi_tclMLVncViewer::bSetOrientation()
         ***************************************************************************/
         /*!
         * \fn     t_Bool bSetOrientation(const tenOrientationMode coenOrientationMode)
         * \brief  Interface to set the orientation mode of the projected display.
         * \param  coenOrientationMode : [IN] Orientation Mode Value.
         * \retval t_Bool
         **************************************************************************/
         t_Bool bSetOrientation(const tenOrientationMode coenOrientationMode);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vEnableScreenSaver()
         ***************************************************************************/
         /*!
         * \fn     t_Void vEnableScreenSaver(const t_Bool cobEnable)
         * \brief  Interface to Enable/disable ScreenSaver
         * \param  cobEnable   : [IN] TRUE - Enable & FALSE - Disable 
         * \retval t_Void
         **************************************************************************/
         t_Void vEnableScreenSaver(const t_Bool cobEnable);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vEnableVoiceRecognition()
         ***************************************************************************/
         /*!
         * \fn     t_Void vEnableVoiceRecognition(const t_Bool cobEnable)
         * \brief  Interface to Enable/disable VoiceRecognition
         * \param  cobEnable   : [IN] TRUE - Enable & FALSE - Disable 
         * \retval t_Void
         **************************************************************************/
         t_Void vEnableVoiceRecognition(const t_Bool cobEnable);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vEnableMicroPhoneInput()
         ***************************************************************************/
         /*!
         * \fn     t_Void vEnableMicroPhoneInput(const t_Bool cobEnable)
         * \brief  Interface to Enable/disable MicroPhoneInput
         * \param  cobEnable   : [IN] TRUE - Enable & FALSE - Disable 
         * \retval t_Void
         **************************************************************************/
         t_Void vEnableMicroPhoneInput(const t_Bool cobEnable);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vSendContentAttestationFailureResult()
         ***************************************************************************/
         /*!
         * \fn     t_Void vSendContentAttestationFailureResult(t_U32 u32Reason)
         * \brief  Interface to send response to Content attestation failure 
         *          callback
         * \param  u32Reason  : [IN] Zero - End the VNC Session
         *                           Non-Zero - continue with the session
         * \retval t_Void
         **************************************************************************/
         t_Void vSendContentAttestationFailureResult(t_U32 u32Reason);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vSetPixelFormat()
         ***************************************************************************/
         /*!
         * \fn     t_Void vSetPixelFormat
         * \brief  Method to set pixel format of the video dynamically
         * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
         * \param  coenDevCat       : [IN] Device category
         * \param  coenPixelFormat  : [IN] Pixel format to be set
         * \retval t_Void
         **************************************************************************/
         t_Void vSetPixelFormat(const tenPixelFormat coenPixelFormat);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vEnableFrameBufferUpdates()
         ***************************************************************************/
         /*!
         * \fn     t_Void vEnableFrameBufferUpdates(t_Bool bEnable)
         * \brief  Method to send request to start/stop frame buffer updates
         * \param  bEnable : [IN] TRUE - To start sending frame buffer updates
         *                 :      FALSE - To stop sending frame buffer updates       
         * \retval t_Void
         **************************************************************************/
         t_Void vEnableFrameBufferUpdates(t_Bool bEnable);

         /***************************************************************************
         ** FUNCTION: t_Void spi_tclMLVncViewer::vSetPixelResolution()
         ***************************************************************************/
         /*!
         * \fn     t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution)
         * \brief  Method to set pixel resolution for the ML Session
         * \param  coenPixResolution: [IN] Client display resolution
         * \retval t_Void
         **************************************************************************/
         t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution);

         /***************************************************************************
         ** FUNCTION:  t_Void spi_tclMLVncViewer::vSetClientCapabilities(const trClient...)
         ***************************************************************************/
         /*!
         * \fn      t_Void vSetClientCapabilities(
         const trClientCapabilities& corfrClientCapabilities)
         * \brief   To set the client capabilities
         * \param   corfrClientCapabilities: [IN]Client Capabilities
         * \retval  t_Void
         **************************************************************************/
         t_Void vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities);

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/
       friend class GenericSingleton<spi_tclMLVncViewer>;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewer::spi_tclMLVncViewer();
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncViewer()
       * \brief   Default Constructor
       * \param
       * \sa      ~spi_tclMLVncViewer()
       **************************************************************************/
       spi_tclMLVncViewer();

       /**********************************************************************************
       ** FUNCTION:  t_Void VNCCALL spi_tclMLVncViewer::vViewerSessionProgressCallback()
       **********************************************************************************/
       /*!
       * \fn      t_Void vViewerSessionProgressCallback(VNCViewer *pViewer,
                  t_Void *pContext, VNCViewerSessionProgress sessionProgress)
       * \brief   Notifies the viewer application about the progress while
       *              establishing a VNC session
       * \param   pViewer: [IN]VNC Viewer Instance
       * \param   pContext: [IN]Context Pointer
       * \param   sessionProgress:Indicates the next action that will be taken by
       *            the viewer thread in attempting to establish the VNC session
       * \retval  NONE
       **************************************************************************/
       static t_Void VNCCALL vViewerSessionProgressCallback(VNCViewer *pViewer,
          t_Void *pContext, VNCViewerSessionProgress sessionProgress);

       /**********************************************************************************
       ** FUNCTION:  t_Void VNCCALL spi_tclMLVncViewer:: vViewerServerInitCallback()
       **********************************************************************************/
       /*!
       * \fn     t_Void VNCCALL vViewerServerInitCallback(VNCViewer *pViewer,void *pContext,
       t_U16 width,t_U16 height,const char *desktopname,
       const VNCPixelFormat *pServerNativePixelFormat)
       * \brief   Informs that the RFB session with the VNC server has been successfully 
       authenticated and is now completely established.
       * \param   pViewer:     [IN]VNC Viewer Instance
       * \param   pContext:    [IN]Context Pointer
       * \param   width:       [IN] Initial Width of the framebuffer
       * \param   height:      [IN]Initial Height of the framebuffer
       * \param   desktopname: [IN]The name of the server desktop, as a NUL-terminated UTF-8
       encoded string.
       * \param   pServerNativePixelFormat: [IN] The native pixel format of the server.
       * \retval  NONE
       **************************************************************************/
       static t_Void VNCCALL vViewerServerInitCallback(VNCViewer *pViewer,t_Void *pContext,
          t_U16 width,t_U16 height,const char *desktopname,
          const VNCPixelFormat *pServerNativePixelFormat);

       /**********************************************************************************
       ** FUNCTION:  t_Void VNCCALL spi_tclMLVncViewer:: vViewerDesktopResizeCallback()
       /**********************************************************************************/
       /*!
       * \fn     VNCCALL vViewerDesktopResizeCallback(VNCViewer *pViewer,
       t_Void *pContext,
       vnc_uint16_t width,
       vnc_uint16_t height)
       * \brief   Notifies that the size of the VNC Server's
       display has changed.
       * \param   pViewer: [IN]VNC Viewer Instance
       * \param   pContext:[IN]Context Pointer
       * \param   width:  [IN]New Width of the server display
       * \param   height: [IN]New Height of the server display
       * \retval  NONE
       **************************************************************************/
       static t_Void VNCCALL vViewerDesktopResizeCallback(VNCViewer *pViewer,
          t_Void *pContext,
          t_U16 width,
          t_U16 height);

       /********************************************************************************************
       ** FUNCTION:  static t_Void vML_wl_error_callback(mlink_adapter_context * pCtx, t_Char * pMessage)
       **********************************************************************************************/
       /*!
       * \fn      vML_wl_error_callback(mlink_adapter_context * pCtx, t_Char * pMessage)
       * \brief   Error Callback upon WL initialise
       * \param   pCtx: [IN]Adapter context
       * \param   pMessage: [IN]Message
       * \retval  NONE
       **************************************************************************/
       static t_Void vML_wl_error_callback(mlink_adapter_context * pCtx, t_Char * pMessage);


       /**************************************************************************************************
       ** FUNCTION: static t_Void VNCCALL spi_tclMLVncViewer::vServerEventConfigCallback()
       **************************************************************************************************/
      /*!
       * \fn      t_Void vServerEventConfigCallback
       * \brief   Notifies the viewer application that the SDK has received the server's
       *          ServerEventConfiguration
       * \param   pViewer : [IN] VNC Viewer Instance
       * \param   pContext: [IN] Context pointer
       * \param   pServerEventConfiguration: [IN]Pointer to Server Event Configuration structure
       * \param   serverEventConfigurationSize: [IN] Size of Server Event Configuration structure
       * \param   pClientEventConfiguration: [IN]Pointer to Client Event Configuration structure
       * \param   clientEventConfigurationSize: [IN] Size of Server Event Configuration structure
       * \param   pFirstDeviceStatusRequest: [IN]Pointer to Device Status Request
       * \param   firstDeviceStatusRequestSize: [IN]size of Device Status request
       * \retval  NONE
       **************************************************************************/
       static t_Void VNCCALL vServerEventConfigCallback(VNCViewer *pViewer,
          t_Void *pContext,
          const VNCServerEventConfiguration *pServerEventConfiguration,
          size_t serverEventConfigurationSize,
          VNCClientEventConfiguration *pClientEventConfiguration,
          size_t clientEventConfigurationSize,
          VNCDeviceStatusRequest *pFirstDeviceStatusRequest,
          size_t firstDeviceStatusRequestSize);

       /**********************************************************************************
       ** FUNCTION: static t_S32 VNCCALL vErrorCallback()
       **********************************************************************************/
       /*!
       * \fn      t_S32 VNCCALL vErrorCallback(VNCViewer *pViewer,
       *          t_Void *pContext, VNCViewerError error, t_Void *pReservedForFutureUse)
       * \brief   Notifies the viewer application about Errors occured
       *              during a VNC session
       * \param   pViewer: [IN]VNC Viewer Instance
       * \param   pContext: [IN]Context Pointer
       * \param   error: [IN] Error message that is occured
       * \param   pReservedForFutureUse : [IN]
       * \retval  t_S32
       **************************************************************************/
       static t_S32 VNCCALL
          vErrorCallback(VNCViewer *pViewer,
          t_Void *pContext,
          VNCViewerError error,
          t_Void *pReservedForFutureUse);

       /**********************************************************************************
       ** FUNCTION:static t_Void VNCCALL ViewerThreadStartedCallback(VNCViewer *pViewer...
       **********************************************************************************/
       /*!
       * \fn      t_Void VNCCALL ViewerThreadStartedCallback(VNCViewer *pViewer,
       *          t_Void *pContext)
       * \brief   Notifies the viewer application, when the viewer thread is started
       * \param   pViewer: [IN]VNC Viewer Instance
       * \param   pContext: [IN]Context Pointer
       * \retval  t_Void
       **************************************************************************/
       static t_Void VNCCALL ViewerThreadStartedCallback(VNCViewer *pViewer, t_Void *pContext);

       /**********************************************************************************
       ** FUNCTION:static t_Void VNCCALL vViewerContextInfoCallback(VNCViewer *pViewer...
       **********************************************************************************/
       /*!
       * \fn      t_Void VNCCALL vViewerContextInfoCallback(VNCViewer *pViewer,
       *             t_Void *pContext, 
       *             const VNCRectangle *pRectangle, 
       *             const VNCContextInformation *pContextInformation, 
       *             size_t contextInformationSize)
       * \brief   Notifies the viewer application about the foreground application
       * \param   pViewer: [IN]VNC Viewer Instance
       * \param   pContext: [IN]Context Pointer
       * \param   pRectangle:[IN] rectangle
       * \param   pContextInformation : [IN] application's context info
       * \param   contextInformationSize : [IN] context info size
       * \retval  t_Void
       **************************************************************************/
       static t_Void vViewerContextInfoCallback(VNCViewer *pViewer, 
          t_Void *pContext, 
          const VNCRectangle *pRectangle, 
          const VNCContextInformation *pContextInformation, 
          size_t contextInformationSize);

       /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vPopulateClientCapabilities()
       ***************************************************************************/
       /*!
       * \fn      vPopulateClientCapabilities(
       VNCServerEventConfiguration *pServerEventConfiguration
       **            VNCClientEventConfiguration *pClientEventConfiguration)
       * \brief   Populates the client event configuration structure
       * \param   pClientEventConfiguration: [IN]Client Event configuration received
       *                                      in callback
       * \param   pServerEventConfiguration: [IN]Server Event configuration received
       *                                      in callback
       * \retval  NONE
       **************************************************************************/
       t_Void vPopulateClientCapabilities(
          const VNCServerEventConfiguration *pServerEventConfiguration,
          VNCClientEventConfiguration *pClientEventConfiguration);

       /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vPopulateServerCapabilities()
       ***************************************************************************/
       /*!
       * \fn      vPopulateServerCapabilities(
       trServerCapabilities& rfrServerCapabilities,
       const VNCServerEventConfiguration *pServerEventConfiguration)
       * \brief   Populates the server event configuration structure
       * \param   pServerEventConfiguration: [IN]Server Event configuration received
       *                                      in callback
       * \retval  NONE
       **************************************************************************/
       t_Void vPopulateServerCapabilities(
          const VNCServerEventConfiguration *pServerEventConfiguration);

       /*****************************************************************************************
       ** FUNCTION: tenOrientationMode enGetOrientationMode(t_U32 u32OrientationMode);
       *****************************************************************************************/
       /*!
       * \fn      tenOrientationMode enGetOrientationMode(t_U32 u32OrientationMode);
       * \brief   Returns the Orientation mode based on the device status feature
       * \param   u32OrientationMode: [IN]Orientation Mode enum Value
       * \retval  tenOrientationMode
       *****************************************************************************************/
       tenOrientationMode enGetOrientationMode(t_U32 u32OrientationMode)const;

       /***************************************************************************************
       ** FUNCTION: static t_Void VNCCALL  spi_tclMLVncViewer::vServerDisplayConfigCallback()
       ***************************************************************************************/
       /*!
       * \fn      t_Void vServerDisplayConfigCallback(VNCViewer *pViewer,t_Void *pContext,
       *            const VNCServerDisplayConfiguration *pServerDisplayConfiguration,
       *            size_t serverDisplayConfigurationSize,
       *            VNCClientDisplayConfiguration *pClientDisplayConfiguration,
       *            size_t clientDisplayConfigurationSize)
       * \brief   Notifies the viewer application that the SDK has received the server's
       *          ServerDisplayConfiguration
       * \param   pViewer : [IN] VNC Viewer Instance
       * \param   pContext: [IN] Context pointer
       * \param   pServerDisplayConfiguration: [IN]Pointer to Server Display Configuration structure
       * \param   serverDisplayConfigurationSize: [IN] Size of Server Display Configuration structure
       * \param   pClientDisplayConfiguration: [IN]Pointer to Client Display Configuration structure
       * \param   clientDisplayConfigurationSize: [IN] Size of Server Display Configuration structure
       * \retval  NONE
       **************************************************************************/
      static t_Void VNCCALL vServerDisplayConfigCallback(VNCViewer *pViewer,
          t_Void *pContext,
          const VNCServerDisplayConfiguration *pServerDisplayConfiguration,
          size_t serverDisplayConfigurationSize,
          VNCClientDisplayConfiguration *pClientDisplayConfiguration,
          size_t clientDisplayConfigurationSize);


     /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vPopulateDisplayCapabilities()
       ***************************************************************************/
      /*!
       * \fn      vPopulateDisplayCapabilities(trDisplayCapabilities& rfrDispCapabilities,
                  const VNCServerDisplayConfiguration*
                  pServerDisplayConfiguration)
       * \brief   Populates the server display capabilities structure
       * \param   rfrDispCapabilities : [IN]Display capabilities structure
       * \param   pServerDisplayConfiguration: [IN]Display Capabilities received from server
       * \retval  NONE
       **************************************************************************/
      t_Void vPopulateDisplayCapabilities(trDisplayCapabilities& rfrDispCapabilities,
              const VNCServerDisplayConfiguration*
              pServerDisplayConfiguration)const;

      /***************************************************************************
       ** FUNCTION: vPopulateVNCRectangle();
       ***************************************************************************/
      /*!
       * \fn  vPopulateVNCRectangle(const MLRectangle &corfrMLRectangle,
       VNCRectangle &rfrVNCRectangle);
       * \brief   supporting function to populate VNCRectangle structure
       *          from MLRectangle structure
       * \param   corfrMLRectangle : [IN] reference to MLrectangle structure from which
       *                         VNCRectangle has to be popultaed
       * \param   rfrVNCRectangle :[OUT] reference to VNCRectangle  which is populated
       *                         from VNCRectangle
       **************************************************************************/
      t_Void vPopulateVNCRectangle(const trMLRectangle &corfrMLRectangle,
            VNCRectangle &rfrVNCRectangle)const;

      /*****************************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vDeviceStatusCallback()
       *****************************************************************************************/
      /*!
       * \fn     t_Void vDeviceStatusCallback(VNCViewer *pViewer,t_Void *pContext,
       *                 const VNCDeviceStatus *pDeviceStatus,size_t deviceStatusSize)
       * \brief   Notifies the application that the requested device status features have been set
       * \param   pViewer : [IN]VNC Viewer instance
       * \param   pContext: [IN]Context Pointer
       * \param   pDeviceStatus: [IN]Device Status Feature
       * \param   deviceStatusSize: [IN]Size of the DEvice Status Feature
       * \retval  NONE
       *****************************************************************************************/
      static t_Void VNCCALL vDeviceStatusCallback(VNCViewer *pViewer,t_Void *pContext,
          const VNCDeviceStatus *pDeviceStatus,size_t deviceStatusSize);

      /*****************************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncViewer::vExtensionEnabledCallback()
       *****************************************************************************************/
      /*!
       * \fn    t_Void vExtensionEnabledCallback(VNCViewer *pViewer,t_Void *pContext,VNCExtension *pExtension,
                                          t_Void *pExtensionContext,vnc_int32_t enabled);
       * \brief   Notifies the application that the Extension Messages have been enabled
       * \param   pViewer : [IN]VNC Viewer instance
       * \param   pContext: [IN]Context Pointer
       * \param   pExtension: [IN]Extension Message
       * \param   pExtensionContext: [IN]Extension pointer
       * \param   enabled: [IN]If set to TRUE,Extension Messages are enabled
       * \retval  NONE
       *****************************************************************************************/
      static t_Void VNCCALL vExtensionEnabledCallback(VNCViewer *pViewer,
                                        t_Void *pContext,VNCExtension *pExtension,
                                        t_Void *pExtensionContext,vnc_int32_t enabled);

      /*****************************************************************************************
      ** FUNCTION: t_Void VNCCALL spi_tclMLVncViewer::vExtensionMessageReceivedCallback()
      *****************************************************************************************/
      /*!
      * \fn      t_Void vExtensionMessageReceivedCallback(
               VNCViewer *pViewer, t_Void *pContext,
                 VNCExtension *pExtension,t_Void *pExtensionContext,
                 const vnc_uint8_t *payload, size_t payloadLength)
      * \brief   Notifies the application that the requested device status features have been set
      * \param   pViewer : [IN]VNC Viewer instance
      * \param   pContext: [IN]Context Pointer
      * \param   pDeviceStatus: [IN]Device Status Feature
      * \param   deviceStatusSize: [IN]Size of the DEvice Status Feature
      * \retval  NONE
       *****************************************************************************************/
      static t_Void VNCCALL vExtensionMessageReceivedCallback(VNCViewer *pViewer, t_Void *pContext,
         VNCExtension *pExtension,t_Void *pExtensionContext,
         const vnc_uint8_t *payload, size_t payloadLength);

      /*****************************************************************************************
      ** FUNCTION: t_Void VNCCALL spi_tclMLVncViewer::vViewerContentAttestationFailureCallback()
      *****************************************************************************************/
      /*!
      * \fn      t_Void vViewerContentAttestationFailureCallback(
      VNCViewer *pViewer, t_Void *pContext,
      VNCViewerContentAttestationFailure enContentAttestationFailure)
      * \brief   Notifies the application about the content attestation failure
      * \param   pViewer : [IN]VNC Viewer instance
      * \param   pContext: [IN]Context Pointer
      * \param   enContentAttestationFailure: [IN]Content Attestation Failure reason
      * \retval  NONE
      *****************************************************************************************/
      static t_Void VNCCALL vViewerContentAttestationFailureCallback(VNCViewer *pViewer, 
         t_Void *pContext, 
         VNCViewerContentAttestationFailure enContentAttestationFailure);

      /*****************************************************************************************
      ** FUNCTION: t_Void VNCCALL spi_tclMLVncViewer::vViewerFrameBufferUpdateEndCallback()
      *****************************************************************************************/
      /*!
      * \fn      t_Void vViewerFrameBufferUpdateEndCallback(
      *          VNCViewer *pViewer,t_Void *pContext, const VNCRectangle *pInvalidatedRectangles,
      *          size_t u8RectangleCount)
      * \brief   Notifies the application about the frame buffer updates
      * \param   pViewer : [IN]VNC Viewer instance
      * \param   pContext: [IN]Context Pointer
      * \param   pInvalidatedRectangles: [IN]Invalid rectangles info
      * \param   u8RectangleCount : [IN] Invalid rectangles count
      * \retval  NONE
      *****************************************************************************************/
      static t_Void vViewerFrameBufferUpdateEndCallback(VNCViewer *pViewer,
         t_Void *pContext, const VNCRectangle *pInvalidatedRectangles,
         size_t u8RectangleCount);


      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclMLVncViewer::vFetchDeviceVersion()
      ***************************************************************************/
      /*!
      * \fn      t_Bool vFetchDeviceVersion(const t_U32 cou32DeviceHandle,
      *            trVersionInfo& rfrVersionInfo)
      * \brief   To get the Device ML version Info
      * \param   cou32DeviceHandle  : [IN] Device Id
      * \param   rfrVersionInfo     : [OUT] Device VersionInfo
      * \retval  t_Void
      **************************************************************************/
      t_Void vFetchDeviceVersion(const t_U32 cou32DeviceHandle,trVersionInfo& rfrVersionInfo);

      /***************************************************************************
      ** FUNCTION:  t_U32 spi_tclMLVncViewer::u32FetchKeyValue()
      ***************************************************************************/
      /*!
      * \fn      u32FetchKeyValue(const VNCDiscoverySDK* poDiscSDK,
         const VNCDiscoverySDKDiscoverer* poDisc,
         const VNCDiscoverySDKEntity* poEntity,
         const t_Char *pczKey,
         t_S32 s32TimeOut= 0)
      * \brief   To Get the int attribute, after the fetching the value
      * \param   poDiscSDK    :  [IN] Pointer to discovery SDK
      * \param   poDisc       :  [IN] Pointer to discoverer
      * \param   poEntity     :  [IN] Pointer to Entity
      * \param   pczKey       :  [IN] Key value used to fetch 
      * \param   s32TimeOut   :  [IN] Timeout
      * \retval  t_U32
      **************************************************************************/
      t_U32 u32FetchKeyValue(const VNCDiscoverySDK* poDiscSDK,
       const VNCDiscoverySDKDiscoverer* poDisc,
       const VNCDiscoverySDKEntity* poEntity,
       const t_Char *pczKey,
       t_S32 s32TimeOut= 0);


      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclMLVncViewer::vSendPointerEvents()
      ***************************************************************************/
      /*!
      * \fn      t_Void vSendPointerEvents(t_S32 xCoordinate,t_S32 yCoordinate,
                                                t_U8 u8TouchMode)
      * \brief   To send the pointer events to the server
      * \param   xCoordinate  : [IN] X Co-ordinate value
      * \param   yCoordinate  : [IN] Y Co-ordinate value
      * \param   u8TouchMode  : [IN] Touch Mode-Pressed or released
      * \retval  t_Void
      **************************************************************************/
      t_Void vSendPointerEvents(t_S32 xCoordinate,t_S32 yCoordinate,
                                                t_U8 u8TouchMode);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclMLVncViewer::vSetViewerParameter()
      ***************************************************************************/
      /*!
      * \fn      t_Void vSetViewerParameter(const t_Char *cpoParameter, const t_Char *cpoValue)
      * \brief   To set the VNC Viewer parameters
      * \param   cpoParameter  : [IN] Key
      * \param   cpoValue  : [IN] Value
      * \retval  t_Void
      **************************************************************************/
      t_Void vSetViewerParameter(const t_Char *cpoParameter, const t_Char *cpoValue);

      //! pointer to the VNC Viewer SDK instance. obtained during initialization
      VNCViewerSDK* m_pViewerSDK;

      //! Boolean value that indicates whether the server supports touch events or not
      t_U32 m_u32IsTouchSupported;

      //! VNC Viewer Instance
      VNCViewer* m_pVNCViewer;

      //Static VNCViewer instance
      static spi_tclMLVncViewer* m_pMLVncViewer;

      //! pointer to DeviceStatus. used to set driver distraction avoidance
      static VNCDeviceStatus m_rDeviceStatus;

      //! Adapter context
      static mlink_adapter_context ml_adapter_ctx;

      //!Flag to enable touch events
      static t_U16 m_u16TouchEnableFlag;

      //! Indicates the server cpabilities 
      static trServerCapabilities m_oServerCapabilities;

      //! Variables to maintain Client Display width&height
      t_U32 m_u32ScreenHeight;
      t_U32 m_u32ScreenWidth;

      //!Indicates whether the server supports touch events or not
      static t_Bool m_bTouchSupport;

      //!Indicates whether the server supports pointer events or not
      static t_Bool m_bPointerSupport;

      //!ML Device Version Info
      static trVersionInfo m_rVersionInfo;

      //!Device ID
      static t_U32 m_u32DeviceId;

      //!Client Capabilities
      static trClientCapabilities m_rClientCapabilities;

      //! Lock variable for Client Capabilities
      Lock m_oLock;

      //! Screen width and Height in Milli meters
      t_U32 m_u32ScreenWidth_Mm;
      t_U32 m_u32ScreenHeight_Mm;

};


#endif /* SPI_TCLMLVNCVIEWER_H_ */
