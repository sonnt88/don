/*!
*******************************************************************************
* \file              spi_tclMLVncCmdViewer.cpp
* \brief             RealVNC command Wrapper for Viewer
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    VNC Wrapper for wrapping VNC calls for sending triggers received
                from SPI to the Viewer class for processing

COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                          | Modifications
12.09.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
20.11.2013 |  Shiva Kumar Gurija(RBEI/ECP2)   | Updated with the new elements for Video
07.01.2014 |  Hari Priya E R                  | Changes for touch handling
03.04.2013 |  Shiva Kumar Gurija(RBEI/ECP2)   | Device status messages
08.07.2014 |  Shiva Kumar Gurija(RBEI/ECP2)   | FrameBufferBlocking changes to 
                                                 fix CTS issues
06.11.2014  |  Hari Priya E R                 | Added changes to set Client key capabilities
27.05.2015  | Shiva Kumar Gurija              | Lint Fix
\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)RealVNC sdk - includes
| 2)Typedefines
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H


#include "WrapperTypeDefines.h"
#include "spi_tclMLVncCmdViewer.h"
#include "spi_tclMLVncViewer.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCmdViewer.cpp.trc.h"
   #endif
#endif

using namespace std;



/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdViewer::spi_tclMLVncCmdViewer()
***************************************************************************/

spi_tclMLVncCmdViewer::spi_tclMLVncCmdViewer()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::spi_tclMLVncViewer()"));
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdViewer::~spi_tclMLVncCmdViewer()
***************************************************************************/

spi_tclMLVncCmdViewer::~spi_tclMLVncCmdViewer()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::~spi_tclMLVncViewer()"));

}


/**********************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdViewer::bInitialiseViewerSDK()
**********************************************************************************/

t_Bool spi_tclMLVncCmdViewer::bInitialiseViewerSDK()const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vInitialiseViewerSDK()"));
   t_Bool bRetVal = false;

      //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Trigger to Initialise the viewer
      bRetVal = pMLVncViewer->bInitializeViewerSDK();
      if(true != bRetVal)
      {
         ETG_TRACE_ERR(("Initialise viewer SDK failed \n"));
      }
   }
   return bRetVal;
}

/**********************************************************************************
** FUNCTION:  spi_tclMLVncCmdViewer::vUnInitializeViewerSDK()
**********************************************************************************/
t_Void spi_tclMLVncCmdViewer::vUnInitializeViewerSDK()const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vUnInitializeViewerSDK()"));

   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Uninitialise the Viewer SDK
      pMLVncViewer->vUninitializeViewerSDK();
   }//if(NULL!=pMLVncViewer)
}

/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vCreateViewer
****************************************************************************************/
t_Void spi_tclMLVncCmdViewer::vCreateViewer(const t_Char* pu8ExtensionName)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vCreateViewer()"));

   
   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {

      //Start VNC Viewer
      pMLVncViewer->vCreateVNCViewer();

      //Register for Extension messages
      if(true!= pMLVncViewer->bRegisterExtension(pu8ExtensionName))
      {
         ETG_TRACE_USR3(("[ERR]:vCreateViewer:Register extension failed"));
      } //if(true!= bRetVal)

   }

}


/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vDestroyViewer
 ***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vDestroyViewer(const t_U32 cou32DeviceHandle)const
{
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vDestroyViewer()"));
   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Destroy VNC Viewer
      pMLVncViewer->vDestroyVNCViewer();
   } //if(NULL!=pMLVncViewer)
}


/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdViewer::bInitializeWayland()
***************************************************************************/
t_Bool spi_tclMLVncCmdViewer::bInitializeWayland(t_U32 u32layer_id, 
                                                 t_U32 u32surface_id,
                                                 t_U32 u32screen_offset_x,
                                                 t_U32 u32screen_offset_y,
                                                 t_U32 u32screen_width, 
                                                 t_U32 u32screen_height,
                                                 t_Bool bForceFullScreen,
                                                 t_U8 u8Flag,
                                                 t_U32 u32ScreenHeight_Mm,
                                                 t_U32 u32ScreenWidth_Mm)const
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   t_Bool bRetInitWL = false;
   if(NULL!=pMLVncViewer)
   {
      bRetInitWL = pMLVncViewer->bMLWLInitialize(u32layer_id,u32surface_id,u32screen_offset_x,
         u32screen_offset_y,u32screen_width,u32screen_height,bForceFullScreen,u8Flag,
         u32ScreenHeight_Mm,u32ScreenWidth_Mm);
   } //if(NULL!=pMLVncViewer)
   return bRetInitWL;
}

/*****************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vUnInitializeWayland()
*****************************************************************************/
t_Void spi_tclMLVncCmdViewer::vUnInitializeWayland()const
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      pMLVncViewer->vMLWLFinalise();
   } //if(NULL!=pMLVncViewer)
}

/**********************************************************************************
** FUNCTION:   t_Void vTriggerSetSessionParameters
*********************************************************************************/
t_Void spi_tclMLVncCmdViewer::vTriggerSetSessionParameters(const t_Char* pcocPrefferedEncoding,
                                                           const t_Char* pcocCntAttestFlag,
                                                           const t_Char* pcocPublickey,
                                                           const t_U32 cou32DeviceHandle)const
{
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vTriggerSetSessionParameters()"));

   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Set the session parameters
      pMLVncViewer->vSetSessionParameters(pcocPrefferedEncoding,
         pcocCntAttestFlag,pcocPublickey);
   }
}

/*************************************************************************
 ** FUNCTION:t_Bool bTriggerProcessCmdString
 *************************************************************************/
t_Bool spi_tclMLVncCmdViewer::bTriggerProcessCmdString(const t_U32 cou32AppId, 
                                                       const t_U32 cou32DeviceHandle)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::bTriggerProcessCmdString()"));
   t_Bool bRetVal = false;

   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Process the command string
      bRetVal = pMLVncViewer->bProcessCmdString(cou32AppId,cou32DeviceHandle);
      if(true != bRetVal)
      {
         ETG_TRACE_USR3(("Processing Command String Failed \n"));
      } // if(true != bRetVal)
   } //if(NULL!=pMLVncViewer)
   return bRetVal;
}



/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vTriggerSendTouchEventToServer
 ***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vTriggerSendTouchEventToServer(trTouchData& rfrTouchData,
                                                             trScalingAttributes& rfrScaleData, 
                                                             const t_U32 cou32DeviceHandle)const
{

   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      pMLVncViewer->vSendTouchEventToServer(cou32DeviceHandle,rfrTouchData,rfrScaleData);

   }
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vTriggerSendDevicekeyEventToServer
 ***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vTriggerSendKeyEventToServer(tenKeyMode enKeyMode,
                                                           tenKeyCode enKeyCode, 
                                                           const t_U32 cou32DeviceHandle)const
{
   (t_Void)cou32DeviceHandle;

   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Send the Device key event to the server
      pMLVncViewer->vSendKeyEventToServer(enKeyMode,enKeyCode);
   }
}

/****************************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdViewer::bTriggerSendDeviceStatusRequest(
**                                             t_U32 u32DeviceStatusRequest...)
***************************************************************************************/
t_Bool spi_tclMLVncCmdViewer:: bTriggerSendDeviceStatusRequest(t_U32 u32DeviceStatusRequest, const t_U32 cou32DeviceHandle)const
{
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vTriggerSendDeviceStatusRequest()"));
   t_Bool bRetVal = false;

   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Send the device status request to the server
      bRetVal = pMLVncViewer->bSendDeviceStatusRequest(
                                   u32DeviceStatusRequest);
      if(true != bRetVal)
      {
         ETG_TRACE_ERR(("Device Status Request send failed \n"));
      }
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdViewer::rfrTriggerGetScreenAttributes
 ****************************************************************************/
 t_Void spi_tclMLVncCmdViewer::rfrTriggerGetScreenAttributes(trScreenAttributes& rfrScreenAttributes ,  
	 const t_U32 cou32DeviceHandle )const
{
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::rfrTriggerGetScreenAttributes()"));

   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //get the screen attributes from server
      pMLVncViewer->rfrGetScreenAttributes(rfrScreenAttributes);
   }
}

 /***********************************************************************
  ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vTriggerSubscribeViewer
  ************************************************************************/
t_Void spi_tclMLVncCmdViewer::vTriggerSubscribeViewer(const t_U32 cou32DeviceHandle )const
{
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vTriggerSubscribeViewer"));

   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Trigger to subscribe for viewer
      pMLVncViewer->vSubscribeforViewer();
   }
}


/**********************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdViewer::bTriggerAddViewerLicense()
**********************************************************************************/
t_Bool spi_tclMLVncCmdViewer::bTriggerAddViewerLicense()const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vTriggerAddViewerLicense()"));
   t_Bool bRetVal = false;

   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      //Add viewer License
      bRetVal = pMLVncViewer->bAddLicense();
      if(true != bRetVal)
      {
         ETG_TRACE_ERR(("[ERR]:vTriggerAddViewerLicense: Adding License failed "));
      } //if(true != bRetVal)
   } //if(NULL!=pMLVncViewer)
   return bRetVal;
}


/*****************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vPumpWaylandEvents()
*****************************************************************************/
t_Void spi_tclMLVncCmdViewer::vPumpWaylandEvents()const
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if(NULL!=pMLVncViewer)
   {
      pMLVncViewer->vPumpWaylandEvents();
   } //if(NULL!=pMLVncViewer)
}


/*******************************CRCB*****************************************/

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncViewer::
 **                  bTriggerFramebufferBlockingNotification(const t_U32 cou32AppID..)
 ***************************************************************************/
t_Bool spi_tclMLVncCmdViewer::bTriggerFramebufferBlockingNotification(
      const t_U32 cou32AppID, const trMLRectangle &corfrMLRectangle,
      tenMLFramebufferBlockReason enMLFrameBufferBlockReason,
      const t_U32 cou32DeviceHandle )const
{
   t_Bool bFBBlockRes = false;

   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      bFBBlockRes = pMLVncViewer->bSendFramebufferBlockingNotification(cou32AppID,
            corfrMLRectangle, enMLFrameBufferBlockReason, cou32DeviceHandle);
   }//if (NULL != pMLVncViewer)

   return bFBBlockRes;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncViewer::
 **                  bTriggerFramebufferBlockingNotification(const t_U32 cou32AppID..)
 ***************************************************************************/
t_Bool spi_tclMLVncCmdViewer::bTriggerAudioBlockingNotification(
      const t_U32 cou32AppID, tenMLAudioBlockReason enMLAudioBlockReason,
      const t_U32 cou32DeviceHandle )const
{
   ETG_TRACE_USR1(
         ("spi_tclMLVncCmdViewer::bTriggerAudioBlockingNotification()"));
   t_Bool bFBBlockRes = false;
   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if (NULL != pMLVncViewer)
   {
      //Destroy VNC Viewer
      bFBBlockRes = pMLVncViewer->bSendAudioBlockingNotification(cou32AppID,
            enMLAudioBlockReason, cou32DeviceHandle);
   }
   return bFBBlockRes;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncViewer::
 **                  bTriggerFramebufferBlockingNotification(const t_U32 cou32AppID..)
 ***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vTriggerSetDriverDistractionAvoidance(
       t_Bool bDistractionAvoidance, const t_U32 cou32DeviceHandle )const
{
   ETG_TRACE_USR1(
         ("spi_tclMLVncCmdViewer::vTriggerSetDriverDistractionAvoidance()"));
   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if (NULL != pMLVncViewer)
   {
      //Destroy VNC Viewer
      pMLVncViewer->vRequestSetDriverDistractionAvoidance(bDistractionAvoidance, cou32DeviceHandle);
   }
}

/*****************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vPopulateTouchEnableFlag(t_U16 
u16TouchEnableFlag)
*****************************************************************************/
t_Void spi_tclMLVncCmdViewer::vPopulateTouchEnableFlag(t_U16 u16TouchEnableFlag)
{
	ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::vPopulateTouchEnableFlag()"));
   //Get the Viewer Instance
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();

   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vPopulateTouchEnableFlag(u16TouchEnableFlag);
   }//if (NULL != pMLVncViewer)
}

/*****************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vSetDayNightMode()
*****************************************************************************/
t_Void spi_tclMLVncCmdViewer::vSetDayNightMode(const t_Bool cobEnableNightMode)
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vSetDayNightMode(cobEnableNightMode);
   }//if (NULL != pMLVncViewer)
}

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableKeyLock(t_Bool bEnable)
   ***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vEnableKeyLock(t_Bool bEnable) const
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vEnableKeyLock(bEnable);
   }//if (NULL != pMLVncViewer)

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableDeviceLock(t_Bool bEnable)
***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vEnableDeviceLock(t_Bool bEnable) const
{

   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vEnableDeviceLock(bEnable);
   }//if (NULL != pMLVncViewer)

}

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdViewer::bSetOrientation()
   ***************************************************************************/
t_Bool spi_tclMLVncCmdViewer::bSetOrientation(
   const tenOrientationMode coenOrientationMode) const
{
   t_Bool bRet=false;

   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      bRet = pMLVncViewer->bSetOrientation(coenOrientationMode);
   }//if (NULL != pMLVncViewer)

   return bRet;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableScreenSaver(t_Bool bEnable)
***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vEnableScreenSaver(t_Bool bEnable) const
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vEnableScreenSaver(bEnable);
   }//if (NULL != pMLVncViewer)
}

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableVoiceRecognition(t_Bool bEnable)
   ***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vEnableVoiceRecognition(t_Bool bEnable) const
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vEnableVoiceRecognition(bEnable);
   }//if (NULL != pMLVncViewer)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableMicroPhoneInput(t_Bool bEnable)
***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vEnableMicroPhoneInput(t_Bool bEnable) const
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vEnableMicroPhoneInput(bEnable);
   }//if (NULL != pMLVncViewer)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vSendContentAttestationFailureResult()
***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vSendContentAttestationFailureResult(t_U32 u32Reason)
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vSendContentAttestationFailureResult(u32Reason);
   }//if (NULL != pMLVncViewer)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vSetPixelFormat()
***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vSetPixelFormat(const t_U32 cou32DevId,
            const tenPixelFormat coenPixelFormat)
{
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vSetPixelFormat(coenPixelFormat);
   }//if (NULL != pMLVncViewer)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableFrameBufferUpdates()
***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vEnableFrameBufferUpdates(t_Bool bEnable)
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vEnableFrameBufferUpdates(bEnable);
   }//if (NULL != pMLVncViewer)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdViewer::vSetPixelResolution()
***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vSetPixelResolution(const tenPixelResolution coenPixResolution)
{
   spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vSetPixelResolution(coenPixResolution);
   }//if (NULL != pMLVncViewer)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdViewer::vSetClientCapabilities(const trClient...)
***************************************************************************/
t_Void spi_tclMLVncCmdViewer::vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities)
{
    spi_tclMLVncViewer* pMLVncViewer = spi_tclMLVncViewer::getInstance();
   if (NULL != pMLVncViewer)
   {
      pMLVncViewer->vSetClientCapabilities(corfrClientCapabilities);
   }//if (NULL != pMLVncViewer)
}
