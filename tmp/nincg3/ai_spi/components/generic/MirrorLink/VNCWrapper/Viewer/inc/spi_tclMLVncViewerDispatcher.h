/***********************************************************************/
/*!
 * \file  spi_tclMLVncViewerDispatcher.h
 * \brief Message Dispatcher for Viewer Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Viewer Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 07.04.2014  | Shiva Kuamr Gurija    | Updated with Device status & Few Other Responses

 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLMLVNCVIEWERDISPATCHER_H_
#define SPI_TCLMLVNCVIEWERDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"

/**************Forward Declarations******************************************/
class spi_tclMLVncViewerDispatcher;

/****************************************************************************/
/*!
 * \class MLViewerMsgBase
 * \brief Base Message type for all Viewer messages
 ****************************************************************************/
class MLViewerMsgBase: public trMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  MLViewerMsgBase::MLViewerMsgBase
       ***************************************************************************/
      /*!
       * \fn      MLViewerMsgBase()
       * \brief   Default constructor
       **************************************************************************/
      MLViewerMsgBase();

      /***************************************************************************
       ** FUNCTION:  MLViewerMsgBase::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   Pure virtual function to be overridden by inherited classes for
       *          dispatching the message
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      virtual t_Void vDispatchMsg(
               spi_tclMLVncViewerDispatcher* poViewerDispatcher) = 0;

      /***************************************************************************
       ** FUNCTION:  MLViewerMsgBase::~MLViewerMsgBase
       ***************************************************************************/
      /*!
       * \fn      ~MLViewerMsgBase()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLViewerMsgBase()
      {

      }
};

/****************************************************************************/
/*!
 * \class MLSessionProgressMsg
 * \brief
 ****************************************************************************/
class MLSessionProgressMsg: public MLViewerMsgBase
{
   public:
      tenSessionProgress enSessionProgress;
      /***************************************************************************
       ** FUNCTION:  MLSessionProgressMsg::MLSessionProgressMsg
       ***************************************************************************/
      /*!
       * \fn      MLSessionProgressMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLSessionProgressMsg();

      /***************************************************************************
       ** FUNCTION:  MLSessionProgressMsg::~MLSessionProgressMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLSessionProgressMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLSessionProgressMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLSessionProgressMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLSessionProgressMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLSessionProgressMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*!
 * \class MLClientCapabilitiesMsg
 * \brief
 ****************************************************************************/
class MLClientCapabilitiesMsg: public MLViewerMsgBase
{
   public:
      trClientCapabilities rClientCapabilities;
      /***************************************************************************
       ** FUNCTION:  MLClientCapabilitiesMsg::MLClientCapabilitiesMsg
       ***************************************************************************/
      /*!
       * \fn      MLClientCapabilitiesMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLClientCapabilitiesMsg();

      /***************************************************************************
       ** FUNCTION:  MLClientCapabilitiesMsg::~MLClientCapabilitiesMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLClientCapabilitiesMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLClientCapabilitiesMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLClientCapabilitiesMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLClientCapabilitiesMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLClientCapabilitiesMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLServerCapabilitiesMsg
 * \brief
 ****************************************************************************/
class MLServerCapabilitiesMsg: public MLViewerMsgBase
{
   public:
      trServerCapabilities rServerCapabilities;
      /***************************************************************************
       ** FUNCTION:  MLServerCapabilitiesMsg::MLServerCapabilitiesMsg
       ***************************************************************************/
      /*!
       * \fn      MLServerCapabilitiesMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLServerCapabilitiesMsg();

      /***************************************************************************
       ** FUNCTION:  MLServerCapabilitiesMsg::~MLServerCapabilitiesMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLServerCapabilitiesMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLServerCapabilitiesMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLServerCapabilitiesMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLServerCapabilitiesMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLServerCapabilitiesMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*!
 * \class MLOrientationModeMsg
 * \brief
 ****************************************************************************/
class MLOrientationModeMsg: public MLViewerMsgBase
{
   public:
      tenOrientationMode enOrientationMode;
      /***************************************************************************
       ** FUNCTION:  MLOrientationModeMsg::MLOrientationModeMsg
       ***************************************************************************/
      /*!
       * \fn      MLOrientationModeMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLOrientationModeMsg();

      /***************************************************************************
       ** FUNCTION:  MLOrientationModeMsg::~MLOrientationModeMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLOrientationModeMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLOrientationModeMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLOrientationModeMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLOrientationModeMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLOrientationModeMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*!
 * \class MLDisplayCapabilitiesMsg
 * \brief
 ****************************************************************************/
class MLDisplayCapabilitiesMsg: public MLViewerMsgBase
{
   public:
      trDisplayCapabilities rDispCapabilities;
      /***************************************************************************
       ** FUNCTION:  MLDisplayCapabilitiesMsg::MLDisplayCapabilitiesMsg
       ***************************************************************************/
      /*!
       * \fn      MLDisplayCapabilitiesMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLDisplayCapabilitiesMsg();

      /***************************************************************************
       ** FUNCTION:  MLDisplayCapabilitiesMsg::~MLDisplayCapabilitiesMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLDisplayCapabilitiesMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLDisplayCapabilitiesMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLDisplayCapabilitiesMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLDisplayCapabilitiesMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLDisplayCapabilitiesMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*!
 * \class MLDeviceStatusMsg
 * \brief
 ****************************************************************************/
class MLDeviceStatusMsg: public MLViewerMsgBase
{
   public:
      t_U32 u32DeviceStatusFeature;
      /***************************************************************************
       ** FUNCTION:  MLDeviceStatusMsg::MLDeviceStatusMsg
       ***************************************************************************/
      /*!
       * \fn      MLDeviceStatusMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLDeviceStatusMsg();

      /***************************************************************************
       ** FUNCTION:  MLDeviceStatusMsg::~MLDeviceStatusMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLDeviceStatusMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLDeviceStatusMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLDeviceStatusMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLDeviceStatusMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLDeviceStatusMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*!
 * \class MLViewerSubscriResultMsg
 * \brief
 ****************************************************************************/
class MLViewerSubscriResultMsg: public MLViewerMsgBase
{
   public:
      //TODO : check return value (t_Bool or some value is needed)
      /***************************************************************************
       ** FUNCTION:  MLViewerSubscriResultMsg::MLViewerSubscriResultMsg
       ***************************************************************************/
      /*!
       * \fn      MLViewerSubscriResultMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLViewerSubscriResultMsg();

      /***************************************************************************
       ** FUNCTION:  MLViewerSubscriResultMsg::~MLViewerSubscriResultMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLViewerSubscriResultMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLViewerSubscriResultMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLViewerSubscriResultMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLViewerSubscriResultMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLViewerSubscriResultMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*!
 * \class MLExtensionEnabledMsg
 * \brief
 ****************************************************************************/
class MLExtensionEnabledMsg: public MLViewerMsgBase
{
   public:
      t_U32 u32ExtEnabled;
      /***************************************************************************
       ** FUNCTION:  MLExtensionEnabledMsg::MLExtensionEnabledMsg
       ***************************************************************************/
      /*!
       * \fn      MLExtensionEnabledMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLExtensionEnabledMsg();

      /***************************************************************************
       ** FUNCTION:  MLExtensionEnabledMsg::~MLExtensionEnabledMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLExtensionEnabledMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLExtensionEnabledMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLExtensionEnabledMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLExtensionEnabledMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLExtensionEnabledMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*!
 * \class MLWLInitialiseErrorMsg
 * \brief
 ****************************************************************************/
class MLWLInitialiseErrorMsg: public MLViewerMsgBase
{
   public:
      t_String *pszWLinitFailureMsg;
      /***************************************************************************
       ** FUNCTION:  MLWLInitialiseErrorMsg::MLWLInitialiseErrorMsg
       ***************************************************************************/
      /*!
       * \fn      MLWLInitialiseErrorMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLWLInitialiseErrorMsg();

      /***************************************************************************
       ** FUNCTION:  MLWLInitialiseErrorMsg::~MLWLInitialiseErrorMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLWLInitialiseErrorMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLWLInitialiseErrorMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLWLInitialiseErrorMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLWLInitialiseErrorMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLWLInitialiseErrorMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLViewerErrorMsg
 * \brief
 ****************************************************************************/
class MLViewerErrorMsg: public MLViewerMsgBase
{
   public:
      t_U32 u32ViewerError;
      /***************************************************************************
       ** FUNCTION:  MLViewerErrorMsg::MLViewerErrorMsg
       ***************************************************************************/
      /*!
       * \fn      MLViewerErrorMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLViewerErrorMsg();

      /***************************************************************************
       ** FUNCTION:  MLViewerErrorMsg::~MLViewerErrorMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLViewerErrorMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLViewerErrorMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLViewerErrorMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLViewerErrorMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLViewerErrorMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*!
 * \class MLDriverDistAvoidanceMsg
 * \brief
 ****************************************************************************/
class MLDriverDistAvoidanceMsg: public MLViewerMsgBase
{
   public:
      t_Bool bDriverDistAvoided;
      /***************************************************************************
       ** FUNCTION:  MLDriverDistAvoidanceMsg::MLDriverDistAvoidanceMsg
       ***************************************************************************/
      /*!
       * \fn      MLDriverDistAvoidanceMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLDriverDistAvoidanceMsg();

      /***************************************************************************
       ** FUNCTION:  MLDriverDistAvoidanceMsg::~MLDriverDistAvoidanceMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLDriverDistAvoidanceMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLDriverDistAvoidanceMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLDriverDistAvoidanceMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLDriverDistAvoidanceMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLDriverDistAvoidanceMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

class MLFrameBufferDimensionMsg:public MLViewerMsgBase
{
   public:
      t_U32 u32Width;
	  t_U32 u32Height;
      /***************************************************************************
       ** FUNCTION:  MLFrameBufferDimensionMsg::MLFrameBufferDimensionMsg
       ***************************************************************************/
      /*!
       * \fn      MLFrameBufferDimensionMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLFrameBufferDimensionMsg();

      /***************************************************************************
       ** FUNCTION:  MLFrameBufferDimensionMsg::~MLFrameBufferDimensionMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLFrameBufferDimensionMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLFrameBufferDimensionMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLFrameBufferDimensionMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLFrameBufferDimensionMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLFrameBufferDimensionMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

class MLServerDispDimensionMsg:public MLViewerMsgBase
{
   public:
      t_U32 u32Width;
	  t_U32 u32Height;
      /***************************************************************************
       ** FUNCTION:  MLServerDispDimensionMsg::MLServerDispDimensionMsg
       ***************************************************************************/
      /*!
       * \fn      MLServerDispDimensionMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLServerDispDimensionMsg();

      /***************************************************************************
       ** FUNCTION:  MLServerDispDimensionMsg::~MLServerDispDimensionMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLServerDispDimensionMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLServerDispDimensionMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLServerDispDimensionMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLServerDispDimensionMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLServerDispDimensionMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

class MLAppCntxtInfo:public MLViewerMsgBase
{
   public:
      trAppContextInfo rAppCntxtInfo;
      /***************************************************************************
       ** FUNCTION:  MLAppCntxtInfo::MLAppCntxtInfo
       ***************************************************************************/
      /*!
       * \fn      MLAppCntxtInfo()
       * \brief   Default constructor
       **************************************************************************/
      MLAppCntxtInfo();

      /***************************************************************************
       ** FUNCTION:  MLAppCntxtInfo::~MLAppCntxtInfo
       ***************************************************************************/
      /*!
       * \fn      ~MLAppCntxtInfo()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAppCntxtInfo(){}

      /***************************************************************************
       ** FUNCTION:  MLAppCntxtInfo::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAppCntxtInfo::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAppCntxtInfo::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

class MLDayNightModeInfo:public MLViewerMsgBase
{
   public:
      t_Bool bNightModeEnabled;
      /***************************************************************************
       ** FUNCTION:  MLDayNightModeInfo::MLDayNightModeInfo
       ***************************************************************************/
      /*!
       * \fn      MLDayNightModeInfo()
       * \brief   Default constructor
       **************************************************************************/
      MLDayNightModeInfo();

      /***************************************************************************
       ** FUNCTION:  MLDayNightModeInfo::~MLDayNightModeInfo
       ***************************************************************************/
      /*!
       * \fn      ~MLDayNightModeInfo()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLDayNightModeInfo(){}

      /***************************************************************************
       ** FUNCTION:  MLDayNightModeInfo::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLDayNightModeInfo::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLDayNightModeInfo::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

class MLVoiceInputInfo : public MLViewerMsgBase
{
public:
      t_Bool bVoiceInputEnabled;
      /***************************************************************************
       ** FUNCTION:  MLVoiceInputInfo::MLVoiceInputInfo
       ***************************************************************************/
      /*!
       * \fn      MLVoiceInputInfo()
       * \brief   Default constructor
       **************************************************************************/
      MLVoiceInputInfo();

      /***************************************************************************
       ** FUNCTION:  MLVoiceInputInfo::~MLVoiceInputInfo
       ***************************************************************************/
      /*!
       * \fn      ~MLVoiceInputInfo()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLVoiceInputInfo(){}

      /***************************************************************************
       ** FUNCTION:  MLVoiceInputInfo::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLVoiceInputInfo::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLVoiceInputInfo::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
}; 

class MLMicroPhoneInputInfo : public MLViewerMsgBase
{
public:
      t_Bool bMicroPhoneInputEnabled;
      /***************************************************************************
       ** FUNCTION:  MLMicroPhoneInputInfo::MLMicroPhoneInputInfo
       ***************************************************************************/
      /*!
       * \fn      MLMicroPhoneInputInfo()
       * \brief   Default constructor
       **************************************************************************/
      MLMicroPhoneInputInfo();

      /***************************************************************************
       ** FUNCTION:  MLMicroPhoneInputInfo::~MLMicroPhoneInputInfo
       ***************************************************************************/
      /*!
       * \fn      ~MLMicroPhoneInputInfo()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLMicroPhoneInputInfo(){}

      /***************************************************************************
       ** FUNCTION:  MLMicroPhoneInputInfo::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLMicroPhoneInputInfo::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLMicroPhoneInputInfo::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

class MLContentAttestationfailureInfo : public MLViewerMsgBase
{
public:
      tenContentAttestationFailureReason enFailureReason;
      /***************************************************************************
      ** FUNCTION:  MLContentAttestationfailureInfo::MLContentAttestationfailureInfo
      ***************************************************************************/
      /*!
      * \fn      MLContentAttestationfailureInfo()
      * \brief   Default constructor
      **************************************************************************/
      MLContentAttestationfailureInfo();

      /***************************************************************************
       ** FUNCTION:  MLContentAttestationfailureInfo::~MLContentAttestationfailureInfo
       ***************************************************************************/
      /*!
       * \fn      ~MLContentAttestationfailureInfo()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLContentAttestationfailureInfo(){}

      /***************************************************************************
      ** FUNCTION:  MLContentAttestationfailureInfo::vDispatchMsg
      ***************************************************************************/
      /*!
      * \fn      vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher)
      * \brief   virtual function for dispatching the message of 'this' type
      * \param poViewerDispatcher : pointer to Message Dispatcher for Viewer
      **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncViewerDispatcher* poViewerDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLContentAttestationfailureInfo::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLContentAttestationfailureInfo::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class spi_tclMLVncViewerDispatcher
 * \brief Message Dispatcher for Viewer Messages
 ****************************************************************************/
class spi_tclMLVncViewerDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::spi_tclMLVncViewerDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncViewerDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclMLVncViewerDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::~spi_tclMLVncViewerDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclMLVncViewerDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclMLVncViewerDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLSessionProgressMsg* poSessionProgressMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLSessionProgressMsg* poSessionProgressMsg)
       * \brief   Handles Messages of MLSessionProgressMsg type
       * \param poSessionProgressMsg : pointer to MLSessionProgressMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLSessionProgressMsg* poSessionProgressMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLClientCapabilitiesMsg* poClientCapabilitiesMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLClientCapabilitiesMsg* poClientCapabilitiesMsg)
       * \brief   Handles Messages of MLClientCapabilitiesMsg type
       * \param poClientCapabilitiesMsg : pointer to MLClientCapabilitiesMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLClientCapabilitiesMsg* poClientCapabilitiesMsg)const;

	  /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLServerCapabilitiesMsg* poServerCapabilitiesMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLServerCapabilitiesMsg* poServerCapabilitiesMsg)
       * \brief   Handles Messages of MLServerCapabilitiesMsg type
       * \param poServerCapabilitiesMsg : pointer to MLServerCapabilitiesMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLServerCapabilitiesMsg* poServerCapabilitiesMsg)const;



      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLOrientationModeMsg* poOrientationModeMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLOrientationModeMsg* poOrientationModeMsg)
       * \brief   Handles Messages of MLOrientationModeMsg type
       * \param poOrientationModeMsg : pointer to MLOrientationModeMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLOrientationModeMsg* poOrientationModeMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLDisplayCapabilitiesMsgDisplayCapabilitiesMsgeMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLDisplayCapabilitiesMsg* poDisplayCapabilitiesMsg)
       * \brief   Handles Messages of MLDisplayCapabilitiesMsg type
       * \param poDisplayCapabilitiesMsg : pointer to MLDisplayCapabilitiesMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLDisplayCapabilitiesMsg* poDisplayCapabilitiesMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLDeviceStatusMsg* poDeviceStatusMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLDeviceStatusMsg* poDeviceStatusMsg)
       * \brief   Handles Messages of MLDeviceStatusMsg type
       * \param poDeviceStatusMsg : pointer to MLDeviceStatusMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLDeviceStatusMsg* poDeviceStatusMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLViewerSubscriResultMsg* poViewerSubscriResultMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLViewerSubscriResultMsg* poViewerSubscriResultMsg)
       * \brief   Handles Messages of MLViewerSubscriResultMsg type
       * \param poViewerSubscriResultMsg : pointer to MLViewerSubscriResultMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLViewerSubscriResultMsg* poViewerSubscriResultMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLExtensionEnabledMsg* poExtensionEnabledMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLExtensionEnabledMsg* poExtensionEnabledMsg)
       * \brief   Handles Messages of MLExtensionEnabledMsg type
       * \param poExtensionEnabledMsg : pointer to MLExtensionEnabledMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLExtensionEnabledMsg* poExtensionEnabledMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLWLInitialiseErrorMsg* poWLInitialiseErrorMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLWLInitialiseErrorMsg* poWLInitialiseErrorMsg)
       * \brief   Handles Messages of MLWLInitialiseErrorMsg type
       * \param poWLInitialiseErrorMsg : pointer to MLWLInitialiseErrorMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLWLInitialiseErrorMsg* poWLInitialiseErrorMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLViewerErrorMsg* poViewerErrorMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLViewerErrorMsg* poViewerErrorMsg)const
       * \brief   Handles Messages of Viewer ErrorMsg type
       * \param poViewerErrorMsg : pointer to ViewereErrorMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLViewerErrorMsg* poViewerErrorMsg) const ;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLDriverDistAvoidanceMsg* poDriverDistAvoidanceMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLDriverDistAvoidanceMsg* poDriverDistAvoidanceMsg)
       * \brief   Handles Messages of MLDriverDistAvoidanceMsg type
       * \param poDriverDistAvoidanceMsg : pointer to MLDriverDistAvoidanceMsg
       **************************************************************************/
      t_Void vHandleViewerMsg(MLDriverDistAvoidanceMsg* poDriverDistAvoidanceMsg)const;

	  /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLFrameBufferDimensionMsg* poFrameBufferDimensionMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLFrameBufferDimensionMsg* poFrameBufferDimensionMsg)
       * \brief   Handles Messages of MLFrameBufferDimensionMsg type
       * \param   poFrameBufferDimensionMsg : pointer to MLFrameBufferDimensionMsg
       **************************************************************************/
	  t_Void vHandleViewerMsg(MLFrameBufferDimensionMsg* poFrameBufferDimensionMsg)const;

	   /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLServerDispDimensionMsg* poServerDispDimensionMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLServerDispDimensionMsg* poServerDispDimensionMsg)
       * \brief   Handles Messages of MLServerDispDimensionMsg type
       * \param   poServerDispDimensionMsg : pointer to MLServerDispDimensionMsg
       **************************************************************************/
     t_Void vHandleViewerMsg(MLServerDispDimensionMsg* poServerDispDimensionMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLAppCntxtInfo* poAppCntxtInfo)
       * \brief   Handles Messages of MLAppCntxtInfo type
       * \param   poAppCntxtInfo : pointer to MLAppCntxtInfo
       **************************************************************************/
     t_Void vHandleViewerMsg(MLAppCntxtInfo* poAppCntxtInfo) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
       ***************************************************************************/
      /*!
       * \fn      vHandleViewerMsg(MLDayNightModeInfo* poDayNightModeInfo)
       * \brief   Handles Messages of MLDayNightModeInfo type
       * \param   poNightModeInfo : pointer to Nightmode message info
       **************************************************************************/
     t_Void vHandleViewerMsg(MLDayNightModeInfo* poDayNightModeInfo) const;

     /***************************************************************************
     ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
     ***************************************************************************/
     /*!
     * \fn      vHandleViewerMsg(MLVoiceInputInfo* poVoiceInputInfo)
     * \brief   Handles Messages of MLVoiceInputInfo type
     * \param   poNightModeInfo : pointer to Voicd Input message info
     **************************************************************************/
     t_Void vHandleViewerMsg(MLVoiceInputInfo* poVoiceInputInfo) const;

     /***************************************************************************
     ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
     ***************************************************************************/
     /*!
     * \fn      vHandleViewerMsg(MLMicroPhoneInputInfo* poMicroPhoneInputInfo)
     * \brief   Handles Messages of MLMicroPhoneInputInfo type
     * \param   poNightModeInfo : pointer to Micro Phone message info
     **************************************************************************/
     t_Void vHandleViewerMsg(MLMicroPhoneInputInfo* poMicroPhoneInputInfo) const;

     /***************************************************************************
     ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
     ***************************************************************************/
     /*!
     * \fn      vHandleViewerMsg(MLContentAttestationfailureInfo* poContAttestationFailureInfo)
     * \brief   Handles Messages of MLContentAttestationfailureInfo type
     * \param   poContAttestationFailureInfo : pointer to Content Attestation failure info
     **************************************************************************/
     t_Void vHandleViewerMsg(MLContentAttestationfailureInfo* poContAttestationFailureInfo) const;
};

#endif /* SPI_TCLMLVNCVIEWERDISPATCHER_H_ */
