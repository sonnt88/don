/*!
*******************************************************************************
* \file              spi_tclMLVncViewerDataIntf.h
* \brief             VNC Viewer Data Interface Class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface class for providing necesary Viewer Data
                to the requesting classes
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                          | Modifications
13.09.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/

#ifndef SPI_TCLMLVNCVIEWERDATAINTF_H_
#define SPI_TCLMLVNCVIEWERDATAINTF_H_

/******************************************************************************
| includes:
| 1)RealVNC sdk - includes
| 2)Typedefines
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "vncviewersdk.h"
#include "WrapperTypeDefines.h"

class spi_tclMLVncViewer;
struct mlink_dlt_context;

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \class spi_tclMLVncViewerDataIntf
* \brief This class acts as the Interface class for providing necesary
*        Viewer Data to the requesting classes
*
*/

class spi_tclMLVncViewerDataIntf
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncViewerDataIntf::spi_tclMLVncViewerDataIntf()
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncViewerDataIntf()
   * \brief   Constructor
   * \sa      virtual ~spi_tclMLVncViewerDataIntf()
   **************************************************************************/
	spi_tclMLVncViewerDataIntf();

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclMLVncViewerDataIntf::~spi_tclMLVncViewerDataIntf()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclMLVncViewerDataIntf()
   * \brief   Destructor
   * \sa      spi_tclMLVncViewerDataIntf()
   **************************************************************************/
   virtual ~spi_tclMLVncViewerDataIntf();

   /***************************************************************************
   ** FUNCTION:  VNCViewerData* spi_tclMLVncViewerDataIntf::pGetViewerData()
   ***************************************************************************/
   /*!
   * \fn      pGetViewerData()
   * \brief   Returns the VNC ViewerData instance
   * \param   NONE
   * \retval  trViewerData: Pointer to structure holding the VNC Viewer Data
   **************************************************************************/
   trViewerData* pGetViewerData();

   mlink_dlt_context* pGetDLTContext();

private:

   /***************************************************************************
   *********************************PRIVATE*************************************
   ***************************************************************************/
   trViewerData* m_rViewerData;


};






#endif /* SPI_TCLVIEWERDATAINTF_H_ */
