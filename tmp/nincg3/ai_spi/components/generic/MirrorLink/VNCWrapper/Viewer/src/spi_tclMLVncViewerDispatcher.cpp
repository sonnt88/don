/***********************************************************************/
/*!
 * \file  spi_tclMLVncViewerDispatcher.cpp
 * \brief Message Dispatcher for Viewer Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Viewer Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 07.04.2014  | Shiva Kuamr Gurija    | Updated with Device status & Few Other Responses

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/

#include "RespRegister.h"
#include "spi_tclMLVncViewerDispatcher.h"
#include "spi_tclMLVncRespViewer.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclMLVncViewerDispatcher.cpp.trc.h"
   #endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleViewerMsg(this);                 \
   }                                                        \
   vDeAllocateMsg();                                        \
}

/***************************************************************************
 ** FUNCTION:  MLViewerMsgBase::MLViewerMsgBase
 ***************************************************************************/
MLViewerMsgBase::MLViewerMsgBase()
{
   vSetServiceID(e32MODULEID_VNCVIEWER);
}

/***************************************************************************
 ** FUNCTION:  MLSessionProgressMsg::MLSessionProgressMsg
 ***************************************************************************/
MLSessionProgressMsg::MLSessionProgressMsg() : enSessionProgress(e16_SESSION_PROGRESS_NONE)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLSessionProgressMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLSessionProgressMsg, spi_tclMLVncViewerDispatcher)


/***************************************************************************
 ** FUNCTION:  MLClientCapabilitiesMsg::MLClientCapabilitiesMsg
 ***************************************************************************/
MLClientCapabilitiesMsg::MLClientCapabilitiesMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLClientCapabilitiesMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLClientCapabilitiesMsg, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  MLServerCapabilitiesMsg::MLServerCapabilitiesMsg
 ***************************************************************************/
MLServerCapabilitiesMsg::MLServerCapabilitiesMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLServerCapabilitiesMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLServerCapabilitiesMsg, spi_tclMLVncViewerDispatcher)


/***************************************************************************
 ** FUNCTION:  MLOrientationModeMsg::MLOrientationModeMsg
 ***************************************************************************/
MLOrientationModeMsg::MLOrientationModeMsg()
{
   enOrientationMode = e8INVALID_MODE;
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLOrientationModeMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLOrientationModeMsg, spi_tclMLVncViewerDispatcher)


/***************************************************************************
 ** FUNCTION:  MLDisplayCapabilitiesMsg::MLDisplayCapabilitiesMsg
 ***************************************************************************/
MLDisplayCapabilitiesMsg::MLDisplayCapabilitiesMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLDisplayCapabilitiesMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLDisplayCapabilitiesMsg, spi_tclMLVncViewerDispatcher)


/***************************************************************************
 ** FUNCTION:  MLDeviceStatusMsg::MLDeviceStatusMsg
 ***************************************************************************/
MLDeviceStatusMsg::MLDeviceStatusMsg() : u32DeviceStatusFeature(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLDeviceStatusMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLDeviceStatusMsg, spi_tclMLVncViewerDispatcher)


/***************************************************************************
 ** FUNCTION:  MLViewerSubscriResultMsg::MLViewerSubscriResultMsg
 ***************************************************************************/
MLViewerSubscriResultMsg::MLViewerSubscriResultMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLViewerSubscriResultMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLViewerSubscriResultMsg, spi_tclMLVncViewerDispatcher)


/***************************************************************************
 ** FUNCTION:  MLExtensionEnabledMsg::MLExtensionEnabledMsg
 ***************************************************************************/
MLExtensionEnabledMsg::MLExtensionEnabledMsg() : u32ExtEnabled(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLExtensionEnabledMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLExtensionEnabledMsg, spi_tclMLVncViewerDispatcher)


/***************************************************************************
 ** FUNCTION:  MLWLInitialiseErrorMsg::MLWLInitialiseErrorMsg
 ***************************************************************************/
MLWLInitialiseErrorMsg::MLWLInitialiseErrorMsg(): pszWLinitFailureMsg(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLWLInitialiseErrorMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLWLInitialiseErrorMsg, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  MLViewerErrorMsg::MLViewerErrorMsg
 ***************************************************************************/
MLViewerErrorMsg::MLViewerErrorMsg():u32ViewerError(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLViewerErrorMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLViewerErrorMsg, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  MLWLInitialiseErrorMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLWLInitialiseErrorMsg::vAllocateMsg()
{
   pszWLinitFailureMsg  = new t_String;
   SPI_NORMAL_ASSERT(NULL == pszWLinitFailureMsg);
}

/***************************************************************************
 ** FUNCTION:  MLWLInitialiseErrorMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLWLInitialiseErrorMsg::vDeAllocateMsg()
{
   RELEASE_MEM(pszWLinitFailureMsg);
}

/***************************************************************************
 ** FUNCTION:  MLDriverDistAvoidanceMsg::MLDriverDistAvoidanceMsg
 ***************************************************************************/
MLDriverDistAvoidanceMsg::MLDriverDistAvoidanceMsg(): bDriverDistAvoided(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLDriverDistAvoidanceMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLDriverDistAvoidanceMsg, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  MLFrameBufferDimensionMsg::MLFrameBufferDimensionMsg
 ***************************************************************************/
MLFrameBufferDimensionMsg::MLFrameBufferDimensionMsg(): u32Width(0),u32Height(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLFrameBufferDimensionMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLFrameBufferDimensionMsg, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  MLServerDispDimensionMsg::MLServerDispDimensionMsg
 ***************************************************************************/
MLServerDispDimensionMsg::MLServerDispDimensionMsg(): u32Width(0),u32Height(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLServerDispDimensionMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLServerDispDimensionMsg, spi_tclMLVncViewerDispatcher)


/***************************************************************************
 ** FUNCTION:  MLAppCntxtInfo::MLAppCntxtInfo
 ***************************************************************************/
MLAppCntxtInfo::MLAppCntxtInfo()
{
   vAllocateMsg();
}
/***************************************************************************
 ** FUNCTION:  MLAppCntxtInfo::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAppCntxtInfo, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  MLDayNightModeInfo::MLDayNightModeInfo
 ***************************************************************************/
MLDayNightModeInfo::MLDayNightModeInfo():bNightModeEnabled(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLDayNightModeInfo::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLDayNightModeInfo, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  MLVoiceInputInfo::MLVoiceInputInfo
 ***************************************************************************/
MLVoiceInputInfo::MLVoiceInputInfo():bVoiceInputEnabled(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLVoiceInputInfo::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLVoiceInputInfo, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  MLMicroPhoneInputInfo::MLMicroPhoneInputInfo
 ***************************************************************************/
MLMicroPhoneInputInfo::MLMicroPhoneInputInfo():bMicroPhoneInputEnabled(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLMicroPhoneInputInfo::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLMicroPhoneInputInfo, spi_tclMLVncViewerDispatcher)

/***************************************************************************
** FUNCTION:  MLContentAttestationfailureInfo::MLContentAttestationfailureInfo
***************************************************************************/
MLContentAttestationfailureInfo::MLContentAttestationfailureInfo() :
         enFailureReason(e32MLContentAttestationFailureErrorNotImplemented)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLContentAttestationfailureInfo::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLContentAttestationfailureInfo, spi_tclMLVncViewerDispatcher)

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::spi_tclMLVncViewerDispatcher
 ***************************************************************************/
spi_tclMLVncViewerDispatcher::spi_tclMLVncViewerDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::~spi_tclMLVncViewerDispatcher
 ***************************************************************************/
spi_tclMLVncViewerDispatcher::~spi_tclMLVncViewerDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLSessionProgressMsg* poSessionProgressMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLSessionProgressMsg* poSessionProgressMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poSessionProgressMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostViewerSessionProgress(poSessionProgressMsg->enSessionProgress));
   } // if (NULL != poSessionProgressMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLClientCapabilitiesMsg* poClientCapabilitiesMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLClientCapabilitiesMsg* poClientCapabilitiesMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poClientCapabilitiesMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostClientCapabilities(&(poClientCapabilitiesMsg->rClientCapabilities)));
   } // if (NULL != poClientCapabilitiesMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLServerCapabilitiesMsg* poServerCapabilitiesMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLServerCapabilitiesMsg* poServerCapabilitiesMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poServerCapabilitiesMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostServerCapabilities(&(poServerCapabilitiesMsg->rServerCapabilities)));
   } // if (NULL != poServerCapabilitiesMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLOrientationModeMsg* poOrientationModeMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLOrientationModeMsg* poOrientationModeMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poOrientationModeMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostOrientationMode(poOrientationModeMsg->enOrientationMode));
   } // if (NULL != poOrientationModeMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLDisplayCapabilitiesMsg* poDisplayCapabilitiesMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLDisplayCapabilitiesMsg* poDisplayCapabilitiesMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poDisplayCapabilitiesMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostDisplayCapabilities(&(poDisplayCapabilitiesMsg->rDispCapabilities)));
   } // if (NULL != poDisplayCapabilitiesMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLDeviceStatusMsg* poDeviceStatusMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLDeviceStatusMsg* poDeviceStatusMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poDeviceStatusMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostDeviceStatusMessages(poDeviceStatusMsg->u32DeviceStatusFeature));
   } // if (NULL != poDeviceStatusMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLViewerSubscriResultMsg* poViewerSubscriResultMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLViewerSubscriResultMsg* poViewerSubscriResultMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poViewerSubscriResultMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostViewerSubscriptionResult());
   } // if (NULL != poViewerSubscriResultMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLExtensionEnabledMsg* poExtensionEnabledMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLExtensionEnabledMsg* poExtensionEnabledMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poExtensionEnabledMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostExtensionEnabledStatus(poExtensionEnabledMsg->u32ExtEnabled));
   } // if (NULL != poExtensionEnabledMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLWLInitialiseErrorMsg* poWLInitialiseErrorMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLWLInitialiseErrorMsg* poWLInitialiseErrorMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poWLInitialiseErrorMsg) && (NULL != poWLInitialiseErrorMsg->pszWLinitFailureMsg))
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostWLInitialiseError(*(poWLInitialiseErrorMsg->pszWLinitFailureMsg)));
   }  // if (NULL != poWLInitialiseErrorMsg)
   //TODO Remove const cast and pass string in the response class
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLViewerErrorMsg* poViewerErrorMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLViewerErrorMsg* poViewerErrorMsg) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poViewerErrorMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostViewerError(poViewerErrorMsg->u32ViewerError));
   }  // if (NULL != poViewerErrorMsg)
}


/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLDriverDistAvoidanceMsg* poDriverDistAvoidanceMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLDriverDistAvoidanceMsg* poDriverDistAvoidanceMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poDriverDistAvoidanceMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostDriverDistractionAvoidanceResult(poDriverDistAvoidanceMsg->bDriverDistAvoided));
   } // if (NULL != poDriverDistAvoidanceMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLFrameBufferDimensionMsg* poFrameBufferDimensionMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLFrameBufferDimensionMsg* poFrameBufferDimensionMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poFrameBufferDimensionMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostFrameBufferDimension(poFrameBufferDimensionMsg->u32Width,
               poFrameBufferDimensionMsg->u32Height));
   } // if (NULL != poFrameBufferDimensionMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLServerDispDimensionMsg* poServerDispDimensionMsg)
 ***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(
         MLServerDispDimensionMsg* poServerDispDimensionMsg)const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poServerDispDimensionMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
               e16VIEWER_REGID,
               vPostServerDispDimension(poServerDispDimensionMsg->u32Width,
               poServerDispDimensionMsg->u32Height));
   } // if (NULL != poServerDispDimensionMsg)
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLAppCntxtInfo* poAppCntxtInfo) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != poAppCntxtInfo)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
         e16VIEWER_REGID,
         vPostAppContextInfo(poAppCntxtInfo->rAppCntxtInfo));
   }//if(NULL != poAppCntxtInfo)
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLDayNightModeInfo* 
                                                      poDayNightModeInfo) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != poDayNightModeInfo)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
         e16VIEWER_REGID,
         vPostDayNightModeInfo(poDayNightModeInfo->bNightModeEnabled));
   }//if(NULL != poNightModeInfo)
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLVoiceInputInfo* 
                                                      poVoiceInputInfo) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != poVoiceInputInfo)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
         e16VIEWER_REGID,
         vPostVoiceInputInfo(poVoiceInputInfo->bVoiceInputEnabled));
   }//if(NULL != poVoiceInputInfo)
}


/***************************************************************************
** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLMicroPhoneInputInfo* 
                                                      poMicroPhoneInputInfo) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != poMicroPhoneInputInfo)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
         e16VIEWER_REGID,
         vPostMicroPhoneInputInfo(poMicroPhoneInputInfo->bMicroPhoneInputEnabled));
   }//if(NULL != poMicroPhoneInputInfo)
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncViewerDispatcher::vHandleViewerMsg()
***************************************************************************/
t_Void spi_tclMLVncViewerDispatcher::vHandleViewerMsg(MLContentAttestationfailureInfo* 
                                                      poContAttestationFailureInfo) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != poContAttestationFailureInfo)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespViewer,
         e16VIEWER_REGID,
         vPostContentAttestationFailureInfo(poContAttestationFailureInfo->enFailureReason));
   }//if(NULL != poContAttestationFailureInfo)
}
