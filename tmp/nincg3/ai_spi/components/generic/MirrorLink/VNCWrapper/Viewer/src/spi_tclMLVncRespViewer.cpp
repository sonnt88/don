/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespViewer.cpp
 * \brief             RealVNC Response Wrapper for Viewer
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper for wrapping  response from VNC Viewer callbacks
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 03.09.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
27.05.2015  | Shiva Kumar Gurija               | Lint Fix

 \endverbatim
 ******************************************************************************/
#include "spi_tclMLVncRespViewer.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncRespViewer.cpp.trc.h"
   #endif
#endif

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncRespViewer::spi_tclMLVncRespViewer()
 ***************************************************************************/
spi_tclMLVncRespViewer::spi_tclMLVncRespViewer() :
   RespBase(e16VIEWER_REGID)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdViewer::spi_tclMLVncCmdViewer()"));

}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncRespViewer::~spi_tclMLVncRespViewer()
 ***************************************************************************/
spi_tclMLVncRespViewer::~spi_tclMLVncRespViewer()
{
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::spi_tclMLVncRespViewer()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostViewerSessionProgress
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostViewerSessionProgress(tenSessionProgress enSessionProgress,
      const t_U32 cou32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostViewerSessionProgress-%d",
      ETG_ENUM(SESSION_PROGRESS_STATUS,enSessionProgress)));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostClientCapabilities
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostClientCapabilities( trClientCapabilities* pClientCapabilities,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(pClientCapabilities);
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostClientCapabilities()"));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostServerCapabilities
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostServerCapabilities( trServerCapabilities* pServerCapabilities,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(pServerCapabilities);
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostServerCapabilities()"));
}

/***************************************************************************
 ** FUNCTION:t_Void vPostOrientationMode
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostOrientationMode(
      tenOrientationMode enOrientationMode, const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(enOrientationMode);
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostOrientationMode()"));
}

/***************************************************************************
 ** FUNCTION:  t_Void vPostDisplayCapabilities
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostDisplayCapabilities(trDisplayCapabilities* pDispCapabilities,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(pDispCapabilities);
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostDisplayCapabilities()"));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostDeviceStatusMessages
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostDeviceStatusMessages(t_U32 u32DeviceStatusFeature,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(u32DeviceStatusFeature);
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostDeviceStatusMessages()"));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostViewerSubscriptionResult
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostViewerSubscriptionResult(const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostViewerSubscriptionResult()"));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostExtensionEnabledStatus
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostExtensionEnabledStatus( t_U32 u32ExtEnabled,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(u32ExtEnabled);
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostExtensionEnabledStatus()"));
}

 /***************************************************************************
 ** FUNCTION:  t_Void vPostWLInitialiseError
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostWLInitialiseError( t_String szMessage,
      const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(szMessage);
	ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostWLInitialiseError()"));
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostDriverDistractionAvoidanceResult
 ***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostDriverDistractionAvoidanceResult(
   t_Bool bDriverDistAvoided,
   const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostDriverDistractionAvoidanceResult:Dev-0x%x bAvoided-%d",
      cou32DeviceHandle,bDriverDistAvoided));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostFrameBufferDimension
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostFrameBufferDimension(t_U32 u32Width,
                                                         t_U32 u32Height,
                                                         const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostFrameBufferDimension()"));
   ETG_TRACE_USR4(("vPostFrameBufferDimension():FB Width : %d\n",u32Width));
   ETG_TRACE_USR4(("vPostFrameBufferDimension():FB Height : %d\n",u32Height));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostServerDispDimension
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostServerDispDimension(t_U32 u32Width,
                                                        t_U32 u32Height,
                                                        const t_U32 cou32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32Width);
   SPI_INTENTIONALLY_UNUSED(u32Height);
   (t_Void)cou32DeviceHandle;
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostServerDispDimension()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostViewerError()
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostViewerError(t_U32 u32ViewerError)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostViewerError() - %d ",u32ViewerError));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostAppContextInfo()
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo)
{
   SPI_INTENTIONALLY_UNUSED(corfrAppCntxtInfo);
   //no traces kept , since this is called very frequently
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostDayNightModeInfo()
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostDayNightModeInfo(t_Bool bNightModeEnabled)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostDayNightModeInfo:bNightModeEnabled-%d",
      ETG_ENUM(BOOL,bNightModeEnabled)));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostVoiceInputInfo()
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostVoiceInputInfo(t_Bool bVoiceInputEnabled)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostVoiceInputInfo:VoiceInputEnabled-%d",
      ETG_ENUM(BOOL,bVoiceInputEnabled)));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostMicroPhoneInputInfo()
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostMicroPhoneInputInfo(t_Bool bMicroPhoneInputEnabled)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostMicroPhoneInputInfo:MicroPhoneInputEnabled-%d",
      ETG_ENUM(BOOL,bMicroPhoneInputEnabled)));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostContentAttestationFailureInfo()
***************************************************************************/
t_Void spi_tclMLVncRespViewer::vPostContentAttestationFailureInfo(
   tenContentAttestationFailureReason enFailureReason)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespViewer::vPostContentAttestationFailureInfo:Reason-0x%x",
      enFailureReason));
}