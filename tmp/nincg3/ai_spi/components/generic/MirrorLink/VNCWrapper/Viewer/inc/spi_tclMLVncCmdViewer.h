/*!
 *******************************************************************************
 * \file              spi_tclMLVncCmdViewer.h
 * \brief             RealVNC command Wrapper for Viewer
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper for wrapping VNC calls for sending triggers received
 from SPI to the Viewer class for processing
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 12.09.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
 20.11.2013 |  Shiva Kumar Gurija(RBEI/ECP2)   | Updated with the new elements for Video
 07.01.2014 |  Hari Priya E R                  | Changes for touch handling
 03.04.2013 |  Shiva Kumar Gurija(RBEI/ECP2)   | Device status messages
 08.07.2014 |  Shiva Kumar Gurija(RBEI/ECP2)   | FrameBufferBlocking changes to 
                                                  fix CTS issues
 06.11.2014  |  Hari Priya E R                 | Added changes to set Client key capabilities

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMLVNCCMDVIEWER_H_
#define SPI_TCLMLVNCCMDVIEWER_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/

#include "vncmirrorlink.h"
#include "vncviewersdk.h"
#include "BaseTypes.h"
#include "SPITypes.h"

class spi_tclMLVncViewer;
class RespRegister;

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLVncCmdViewer
 * \brief This class wraps the commands to trigger RealVNC SDK calls to SPI
 *  component to implement the VNC Viewer service provided by MLServer.
 *
 */

class spi_tclMLVncCmdViewer
{

   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncCmdViewer::spi_tclMLVncCmdViewer();
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncCmdViewer()
       * \brief   Default Constructor
       * \sa      ~spi_tclMLVnspi_tclMLVncViewercCmdViewer()
       **************************************************************************/
      spi_tclMLVncCmdViewer();

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclMLVncCmdViewer::~spi_tclMLVncCmdViewer()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclMLVncCmdViewer()
       * \brief   Destructor
       * \sa      spi_tclMLVncCmdViewer()
       **************************************************************************/
      virtual ~spi_tclMLVncCmdViewer();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLVncCmdViewer::bInitialiseViewerSDK()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bInitialiseViewerSDK()
       * \brief   Initialise the VNC Viewer
       * \param   NONE
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bInitialiseViewerSDK()const;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vUnInitializeViewerSDK()
       ***************************************************************************/
      /*!
       * \fn      vUnInitializeViewerSDK(t_Void)
       * \brief   Command to uninitialise the viewer SDK
       * \param   NONE
       * \retval  NONE
       **************************************************************************/
      t_Void vUnInitializeViewerSDK()const;

      /****************************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vCreateViewer
       ****************************************************************************************/
      /*!
       * \fn      vCreateViewer(const t_Char* pu8ExtensionName);
       * \brief   Command to start the VNC Viewer 
       * \param   pu8ExtensionName : [IN]Extension name to be enabled
       * \retval  NONE
       ***************************************************************************************/
      t_Void vCreateViewer(const t_Char* pu8ExtensionName = NULL)const;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vDestroyViewer
       ***************************************************************************/
      /*!
       * \fn      vDestroyViewer(const t_U32 cou32DeviceHandle = 0);
       * \brief   Command to destroy the VNC viewer
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vDestroyViewer(const t_U32 cou32DeviceHandle = 0)const;

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclMLVncCmdViewer::bInitializeWayland()
      ***************************************************************************/
      /*!
      * \fn      t_Bool bInitializeWayland(t_U32 u32layer_id, t_U32 u32surface_id,t_U32 u32screen_offset_x,
      *          t_U32 u32screen_offset_y,t_U32 u32screen_width, t_U32 u32screen_height,
      *          t_Bool bForceFullScreen,t_U8 u8Flag)
      * \brief   Initialize wayland layer manager.
      * \param   u32layer_id: [IN]Layer ID for ML Application
      * \param   u32surface_id: [IN] Surface ID for ML Application
      * \param   u32screen_offset_x: [IN] Initial screen offset x of LayerManager surface
      * \param   u32screen_offset_y: [IN] Initial screen offset y of LayerManager surface
      * \param   u32screen_width: [IN] Initial screen width of LayerManager surface
      * \param   u32screen_height; [IN]Initial screen height of LayerManager surface
      * \param   bForceFullScreen: [IN]If set to FALSE screen size will maintain
      *                                phone screen aspect ratio.
      *                                If set to TRUE phone screen is scaled to
      *                                screen_width and screen_height
      * \param   u8Flag : [IN] Touch Input Handling Enable/disable
      * \param   u32ScreenHeight_Mm: [IN] Screen Height in MM
      * \param   u32ScreenWidth_Mm : [IN] Screen width in MM
      * \retval  t_Bool
      **************************************************************************/
      t_Bool bInitializeWayland(t_U32 u32layer_id, 
         t_U32 u32surface_id,
         t_U32 u32screen_offset_x,
         t_U32 u32screen_offset_y,
         t_U32 u32screen_width, 
         t_U32 u32screen_height,
         t_Bool bForceFullScreen,
         t_U8 u8Flag,
         t_U32 u32ScreenHeight_Mm,
         t_U32 u32ScreenWidth_Mm)const;

      /*****************************************************************************
      ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vPumpWaylandEvents()
      *****************************************************************************/
      /*!
      * \fn      t_Void vUnInitializeWayland()
      * \brief   UnInitialize wayland.layer manager
      * \param   t_Void
      * \retval  t_Void
      **************************************************************************/
      t_Void vUnInitializeWayland()const;

      /**********************************************************************************
       ** FUNCTION:   t_Void vTriggerSetSessionParameters
       *********************************************************************************/
      /*!
       * \fn      vTriggerSetSessionParameters(const t_Char* pcocPrefferedEncoding,
       const t_Char* pcocCntAttestFlag,const t_Char* pcocPublickey, const t_U32 cou32DeviceHandle = 0);
       * \brief   Trigger to set the preferred session parameters
       * \param   pcocPrefferedEncoding : [IN]Preferred Encoding method
       * \param   pcocCntAttestFlag: [IN]Content attestation Flag
       * \param   pcocPublickey: [IN] Public key to enable content attestation
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       *********************************************************************************/
      t_Void vTriggerSetSessionParameters(const t_Char* pcocPrefferedEncoding,
            const t_Char* pcocCntAttestFlag, const t_Char* pcocPublickey,
            const t_U32 cou32DeviceHandle = 0)const;

      /*************************************************************************
       ** FUNCTION:t_Bool bTriggerProcessCmdString
       *************************************************************************/
      /*!
       * \fn     bTriggerProcessCmdString(
       *            const t_U32 cou32AppId, const t_U32 cou32DeviceHandle = 0)
       * \brief   Trigger to Fetch and Process the command string of the Application
       * \param   co u32AppId : [IN] AppId
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bTriggerProcessCmdString(const t_U32 cou32AppId, const t_U32 cou32DeviceHandle = 0)const;


      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdViewer::rfrTriggerGetScreenAttributes
       ****************************************************************************/
      /*!
       * \fn      rfrTriggerGetScreenAttributes(
       *             trScreenAttributes& rfrScreenAttributes,  const t_U32 cou32DeviceHandle = 0);
       * \brief   Trigger to get the server screen attributes
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       ****************************************************************************/
      t_Void rfrTriggerGetScreenAttributes(trScreenAttributes& rfrScreenAttributes,
            const t_U32 cou32DeviceHandle = 0)const;

      /***********************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vTriggerSubscribeViewer
       ************************************************************************/
      /*!
       * \fn      vTriggerSubscribeViewer(const t_U32 cou32DeviceHandle = 0)
       * \brief   Trigger for subcribing the viewer to receive responses
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       *************************************************************************/
      t_Void vTriggerSubscribeViewer(const t_U32 cou32DeviceHandle = 0)const;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vTriggerSendTouchEventToServer
       ***************************************************************************/
      /*!
       * \fn     vTriggerSendTouchEventToServer(
       *              trTouchData& rfrTouchData,t_Float fWidthScalingFactor,
		              t_Float fHeightScalingFactor,t_Bool bTouchSupported,
		              const t_U32 cou32DeviceHandle = 0);
       * \brief   Command to send the touch related info to the server
       *          to trigger touch event
       * \param   rfrTouchData: [IN]Touch  related info
	   * \param   rfrScaleData: [IN]Scaling data
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vTriggerSendTouchEventToServer(trTouchData& rfrTouchData,trScalingAttributes& rfrScaleData,
    		  	  	  	  	  	  	  	  	  const t_U32 cou32DeviceHandle = 0)const;


      /***************************************************************************
       ** FUNCTION: tvVoid spi_tclMLVncCmdViewer::vTriggerSendKeyEventToServer
       ***************************************************************************/
      /*!
       * \fn      vTriggerSendKeyEventToServer(
       *           tenKeyMode enKeyMode, tenKeyCode tenKeyCode, const tvU32 cou32DeviceHandle = 0)
       * \brief   Command to send the device key info to the server to trigger
       *          a device key event
       * \param   enKeyMode: Key pressed or released
       * \param   tenKeyCode: [IN]KeyCode of the Device key
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vTriggerSendKeyEventToServer(tenKeyMode enKeyMode,
            tenKeyCode tenKeyCode, const t_U32 cou32DeviceHandle = 0)const;

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdViewer::bTriggerSendDeviceStatusRequest
       ** (t_U8 u8DeviceStatusRequest ...)
       ***************************************************************************/
      /*!
       * \fn      bTriggerSendDeviceStatusRequest(t_U32 u32DeviceStatusRequest,
       *                                           const t_U32 cou32DeviceHandle = 0);
       * \brief   Command to send a device status message to server
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Bool bTriggerSendDeviceStatusRequest(t_U32 u32DeviceStatusRequest,
            const t_U32 cou32DeviceHandle = 0)const;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vTriggerSubscribeForViewer
       ***************************************************************************/
      /*!
       * \fn        vTriggerSubscribeForViewer(const t_U32 cou32DeviceHandle = 0)
       * \brief   Command to trigger the subscription for Viewer
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      t_Void vTriggerSubscribeForViewer(const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdViewer::bTriggerAddViewerLicense()
       ***************************************************************************/
      /*!
       * \fn      bTriggerAddViewerLicense()
       * \brief   Command to Add license to the viewer
       * \param   NONE
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bTriggerAddViewerLicense()const;


      /*****************************************************************************
      ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vPumpWaylandEvents()
      *****************************************************************************/
      /*!
      * \fn      t_Void vPumpWaylandEvents()
      * \brief   Send the touch events to wayland.layer manager
      * \param   t_Void
      * \retval  t_Void
      **************************************************************************/
      t_Void vPumpWaylandEvents()const;


      /****************************************CRCB****************************/

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncViewer::
       **                  bTriggerFramebufferBlockingNotification(const t_U32 cou32AppID..)
       ***************************************************************************/
      /*!
       * \fn  bTriggerFramebufferBlockingNotification(const t_U32 cou32AppID,
       *        MLRectangle &corfrMLRectangle,
       *       MLFramebufferBlockReason enMLFrameBufferBlockReason);
       * \brief   requests the server to block the requested applications
       *          Framebuffer content
       * \param   cou32AppID : [IN] Application ID of which the Framebuffer
       *                      content is to be blocked
       * \param   corfrMLRectangle :[IN] Rectangular area of the Display whose
       *                      Framebuffer content has to be blocked
       * \param   enMLFrameBufferBlockReason : [IN] Reason for blocking FrameBuffer
       *                      content from a particular application
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \reval   true :  Frame buffer blocking was successful
       *          false : Frame buffer blocking failed
       * \sa     bTriggerAudioBlockingNotification
       **************************************************************************/
      t_Bool bTriggerFramebufferBlockingNotification(const t_U32 cou32AppID,
            const trMLRectangle &corfrMLRectangle,
            tenMLFramebufferBlockReason enMLFrameBufferBlockReason,
            const t_U32 cou32DeviceHandle = 0)const;

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncViewer::
       **                        bTriggerAudioBlockingNotification(const t_U32 cou32AppID..)
       ***************************************************************************/
      /*!
       * \fn bTriggerAudioBlockingNotification(const t_U32 cou32AppID,
       *       MLAudioBlockReason enMLAudioBlockReason);
       * \brief   requests the server to block the requested applications
       *          Audio content
       * \param   cou32AppID :[IN} Application ID of which the Framebuffer content is to
       *                     be blocked
       * \param   enMLAudioBlockReason : [IN] Reason for blocking Audio
       *                      content from a particular application
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \reval   true :  Audio blocking was successful
       *          false : Audio blocking failed
       * \sa     bTriggerFramebufferBlockingNotification
       **************************************************************************/
      t_Bool bTriggerAudioBlockingNotification(const t_U32 cou32AppID,
            tenMLAudioBlockReason enMLAudioBlockReason,
            const t_U32 cou32DeviceHandle = 0)const;

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncViewer::vTriggerSetDriverDistractionAvoidance
       **                                    (const trUserContext corUserContext, t_Bool bDistractionAvoidance);
       ***************************************************************************/
      /*!
       * \fn vTriggerSetDriverDistractionAvoidance(const trUserContext corUserContext, t_Bool bDistractionAvoidance)
       * \brief   enable or disable driver distraction avoidance. Enabling
       *          driver distraction will force the font, font size etc to be
       *          suitable for automotive environment and also disable content
       *          such as video
       * \param corUserContext : [IN] Context information passed from the caller of this function
       * \param   bDistractionAvoidance : [IN] t_Boolean value indication enabling or
       *             disabling of driver distraction avoidance feature
       * \param cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \sa   vPostDriverDistractionStatus
       **************************************************************************/
      t_Void
      vTriggerSetDriverDistractionAvoidance(t_Bool bDistractionAvoidance,
               const t_U32 cou32DeviceHandle = 0)const;

	  
	  /*****************************************************************************
      ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vPopulateTouchEnableFlag(t_U16 
	                u16TouchEnableFlag)
      *****************************************************************************/
      /*!
      * \fn      t_Void vPopulateTouchEnableFlag(t_U16 u16TouchEnableFlag)
      * \brief   Populate the touch enable flag indicating the enabling of touch events
      * \param   u16TouchEnableFlag: [IN]Touch Enable Flag
      * \retval  t_Void
      **************************************************************************/ 
	  t_Void vPopulateTouchEnableFlag(t_U16 u16TouchEnableFlag);

     /*****************************************************************************
     ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vSetDayNightMode()
     *****************************************************************************/
     /*!
     * \fn      t_Void vSetDayNightMode(const t_Bool cobEnableNightMode)
     * \brief   Method to set the Day Night mode
     * \param   cobEnableNightMode: [IN] Night mode Enable/disable
     * \retval  t_Void
     **************************************************************************/ 
     t_Void vSetDayNightMode(const t_Bool cobEnableNightMode);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableKeyLock(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableKeyLock(t_Bool bEnable)
   * \brief  Interface to Enable/diable Key lock (pointer& touch events from Phone)
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable the Key Lock
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableKeyLock(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableDeviceLock(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableDeviceLock(t_Bool bEnable)
   * \brief  Interface to Enable/diable Device lock 
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable 
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableDeviceLock(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdViewer::bSetOrientation()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bSetOrientation(const tenOrientationMode coenOrientationMode)
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  coenOrientationMode : [IN] Orientation Mode Value.
   * \retval t_Bool
   **************************************************************************/
   t_Bool bSetOrientation(const tenOrientationMode coenOrientationMode) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableScreenSaver(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableScreenSaver(t_Bool bEnable)
   * \brief  Interface to Enable/diable ScreenSaver
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable 
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableScreenSaver(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableVoiceRecognition(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableVoiceRecognition(t_Bool bEnable)
   * \brief  Interface to Enable/disable VoiceRecognition
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable 
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableVoiceRecognition(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableMicroPhoneInput(t_Bool bEnable)
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableMicroPhoneInput(t_Bool bEnable)
   * \brief  Interface to Enable/disable MicroPhoneInput
   * \param  bEnable   : [IN] TRUE - Enable & FALSE - Disable 
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableMicroPhoneInput(t_Bool bEnable) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vSendContentAttestationFailureResult()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSendContentAttestationFailureResult(t_U32 u32Reason)
   * \brief  Interface to send response to Content attestation failure 
   *          callback
   * \param  u32Reason  : [IN] Zero - End the VNC Session
   *                           Non-Zero - continue with the session
   * \retval t_Void
   **************************************************************************/
   t_Void vSendContentAttestationFailureResult(t_U32 u32Reason);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vSetPixelFormat()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetPixelFormat
   * \brief  Method to set pixel format of the video dynamically
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  coenPixelFormat  : [IN] Pixel format to be set
   * \retval t_Void
   **************************************************************************/
   t_Void vSetPixelFormat(const t_U32 cou32DevId,
               const tenPixelFormat coenPixelFormat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vEnableFrameBufferUpdates()
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableFrameBufferUpdates(t_Bool bEnable)
   * \brief  Method to send request to start/stop frame buffer updates
   * \param  bEnable : [IN] TRUE - To start sending frame buffer updates
   *                 :      FALSE - To stop sending frame buffer updates       
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableFrameBufferUpdates(t_Bool bEnable);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdViewer::vSetPixelResolution()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution)
   * \brief  Method to set pixel resolution
   * \param  coenPixResolution: [IN] Client display resolution
   * \retval t_Void
   **************************************************************************/
   t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdViewer::vSetClientCapabilities(const trClient...)
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetClientCapabilities(
                   const trClientCapabilities& corfrClientCapabilities)
   * \brief   To set the client capabilities
   * \param   corfrClientCapabilities: [IN]Client Capabilities
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities);
   
   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

};

#endif /* SPI_TCLMLVNCCMDVIEWER_H_ */
