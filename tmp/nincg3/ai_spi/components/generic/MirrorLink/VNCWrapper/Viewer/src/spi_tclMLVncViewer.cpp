/*!
*******************************************************************************
* \file              spi_tclMLVncViewer.cpp
* \brief             RealVNC Wrapper for Viewer
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    VNC Wrapper for wrapping VNC calls for receiving
Framebuffer updates from ML Server
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                          | Modifications
02.09.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version
20.11.2013 |  Shiva Kumar Gurija(RBEI/ECP2)   | Updated with the new elements for Video
02.12.2013 |  Hari Priya E R(RBEI/ECP2)       | Updated with changes for touch input
03.04.2013 |  Shiva Kumar Gurija(RBEI/ECP2)   | Device status messages
14.05.2014 |  Shiva Kumar Gurija              | Changes in ClientDisplayConfig
                                                 for CTS Test
14.07.2014  |  Hari Priya E R                 | Changes to handle touch events based on Device Version
06.11.2014  |  Hari Priya E R                 | Added changes to set Client key capabilities
06.05.2015  |Tejaswini HB                     |Lint Fix
21.05.2014  |  Shiva Kumar Gurija             | Changes in determining ML version of the Phones
                                                - Changes to enable multi touch for ML1.x or above
                                                - Setting the MLC ML version in display config.
27.05.2015  | Shiva Kumar Gurija              | Lint Fix
01.06.2015  | Shiva kumar Gurija              | Set names to VNC Threads
25.06.2015  | Shiva kaumr Gurija              | Changes in touch event pressure mask
03.07.2015  | Shiva Kumar Gurija              | improvements in ML Version checking
09.09.2015  | Shiva Kumar Gurija              | Extended ML Keys support for PSA SOP1
09.09.2015  |  Shiva Kumar Gurija             | Sending Framebuffer blocking notification for 
                                                Invalid FB updates. (SUZUKI-24554)
\endverbatim
******************************************************************************/
#define VNC_USE_STDINT_H

#include <math.h>
#include <string.h>
#include "vncdiscoverysdk.h"
#include "vncmirrorlinkkeys.h"
#include "mlink_dlt.h"
#include "mlink_wl.h"
#include "StringHandler.h"
#include "FileHandler.h"
#include "ThreadNamer.h"
#include "spi_tclMLVncMsgQInterface.h"
#include "spi_tclMLVncDiscovererDataIntf.h"
#include "spi_tclMLVncViewer.h"
#include "spi_tclVncSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
#include "trcGenProj/Header/spi_tclMLVncViewer.cpp.trc.h"
#endif
#endif

using namespace std;

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define VIEWER_LOG_LEVEL 100
#define TRLE_ENCODING "TRLE"
#define RSA_KEYS_BITS 512
#define TIMESTAMP_SECONDS 0
#define TIMESTAMP_NANOSECONDS 0
#define NO_TOUCH_PRESSURE 0
#define TOUCH_PRESSURE 1
#define ENABLE_TOUCH_FLAG 1


//specific values for the Pixel format
static const t_S32 s32RGB565RedBitDepth = 5;
static const t_S32 s32RGB565GreenBitDepth = 6;
static const t_S32 s32RGB565BlueBitDepth = 5;

static const t_S32 s32ARGB888RedBitDepth = 8;
static const t_S32 s32ARGB888GreenBitDepth = 8;
static const t_S32 s32ARGB888BlueBitDepth = 8;

static const t_S32 s32BigEndianFlag = 0;

static const t_U32 scou32MultiTouchEnable = 1;

//This is to set the pressure levels to 2 and and the number of touch events to 2 in the client config
static const t_U32 scou32TouchEventsPressureMask = 0x0101FFFF;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
spi_tclMLVncViewer* spi_tclMLVncViewer::m_pMLVncViewer = NULL;
VNCDeviceStatus spi_tclMLVncViewer::m_rDeviceStatus;
trServerCapabilities spi_tclMLVncViewer::m_oServerCapabilities;
trClientCapabilities spi_tclMLVncViewer::m_rClientCapabilities;
t_Bool spi_tclMLVncViewer::m_bTouchSupport = false;
t_Bool spi_tclMLVncViewer::m_bPointerSupport = false;
mlink_adapter_context spi_tclMLVncViewer::ml_adapter_ctx;
t_U16 spi_tclMLVncViewer::m_u16TouchEnableFlag = 0;
t_U32 spi_tclMLVncViewer::m_u32DeviceId = 0;
trVersionInfo spi_tclMLVncViewer::m_rVersionInfo;
static const t_U16 scou16DefaultUserDistanceMM = 900;


//! Default values for device status are set as follows
//! VNCDeviceStatusFeatureDriverDistractionAvoidanceDisabled   = 0x00020000,
//! VNCDeviceStatusFeatureOrientationLandscape                 = 0x10000000,
//! VNCDeviceStatusFeatureFramebufferRotation0Degrees          = 0x04000000,
//! VNCDeviceStatusFeatureVoiceInputDisabled                   = 0x00000200,
//! VNCDeviceStatusFeatureMicrophoneInputDisabled              = 0x00000800,
//! VNCDeviceStatusFeatureScreenSaverDisabled                  = 0x00000020,
//! VNCDeviceStatusFeatureNightModeDisabled                    = 0x00000080,
//! VNCDeviceStatusFeatureDeviceLockDisabled                   = 0x00000008,
//! VNCDeviceStatusFeatureKeyLockDisabled                      = 0x00000002,

static const t_U32 scou32DefaultDeviceStatus                  = 0x14020AAA;


typedef enum 
{
   e8ML_KNOB_KEY = 0,
   e8ML_DEV_KEY = 1,
   e8ML_MULTIMEDIA_KEY = 2,
   e8ML_MISC_KEY = 3
}tenMLKeyType;


//map the key codes available in SPITypes.h and VNCViewerSDK
typedef struct 
{
   tenKeyCode enKeyCode;
   t_U32 u32KeyCode;
   tenMLKeyType enKeyType;
}trKeyCodeMapping;


static const trKeyCodeMapping scoarKeyCodeMapping[] = {
   {e32DEV_BACKWARD,       static_cast<t_U32>(VNCDeviceKeySupportBackward),    e8ML_DEV_KEY},
   {e32DEV_MENU,           static_cast<t_U32>(VNCDeviceKeySupportMenu),        e8ML_DEV_KEY},
   {e32MULTIMEDIA_NEXT,    static_cast<t_U32>(VNCMultimediaKeySupportNext),    e8ML_MULTIMEDIA_KEY},
   {e32MULTIMEDIA_PREVIOUS,static_cast<t_U32>(VNCMultimediaKeySupportPrevious),e8ML_MULTIMEDIA_KEY},
   {e32MULTIMEDIA_PLAY,    static_cast<t_U32>(VNCMultimediaKeySupportPlay),    e8ML_MULTIMEDIA_KEY},
   {e32MULTIMEDIA_PAUSE,   static_cast<t_U32>(VNCMultimediaKeySupportPause),   e8ML_MULTIMEDIA_KEY},
   {e32MULTIMEDIA_STOP,    static_cast<t_U32>(VNCMultimediaKeySupportStop),    e8ML_MULTIMEDIA_KEY},
   {e32DEV_PHONE_CALL,     static_cast<t_U32>(VNCDeviceKeySupportPhoneCall),   e8ML_DEV_KEY},
   {e32DEV_PHONE_END,      static_cast<t_U32>(VNCDeviceKeySupportPhoneEnd),    e8ML_DEV_KEY},
   {e32DEV_SOFT_LEFT,      static_cast<t_U32>(VNCDeviceKeySupportSoftLeft),    e8ML_DEV_KEY},
   {e32DEV_SOFT_MIDDLE,    static_cast<t_U32>(VNCDeviceKeySupportSoftMiddle),  e8ML_DEV_KEY},
   {e32DEV_SOFT_RIGHT,     static_cast<t_U32>(VNCDeviceKeySupportSoftRight),   e8ML_DEV_KEY},
   {e32DEV_APPLICATION,    static_cast<t_U32>(VNCDeviceKeySupportApplication), e8ML_DEV_KEY},
   {e32DEV_OK,             static_cast<t_U32>(VNCDeviceKeySupportOk),          e8ML_DEV_KEY},
   {e32DEV_DELETE,         static_cast<t_U32>(VNCDeviceKeySupportDelete),      e8ML_DEV_KEY},
   {e32DEV_ZOOM_IN,        static_cast<t_U32>(VNCDeviceKeySupportZoomIn),      e8ML_DEV_KEY},
   {e32DEV_ZOOM_OUT,       static_cast<t_U32>(VNCDeviceKeySupportZoomOut),     e8ML_DEV_KEY},
   {e32DEV_CLEAR,          static_cast<t_U32>(VNCDeviceKeySupportClear),       e8ML_DEV_KEY},
   {e32DEV_FORWARD,        static_cast<t_U32>(VNCDeviceKeySupportForward),     e8ML_DEV_KEY},
   {e32DEV_HOME,           static_cast<t_U32>(VNCDeviceKeySupportHome),        e8ML_DEV_KEY},
   {e32DEV_SEARCH,         static_cast<t_U32>(VNCDeviceKeySupportSearch),      e8ML_DEV_KEY}
};

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncViewer::spi_tclMLVncViewer()
***************************************************************************/

spi_tclMLVncViewer::spi_tclMLVncViewer():m_pViewerSDK(NULL),
m_pVNCViewer(NULL),
m_u32ScreenWidth(0),
m_u32ScreenHeight(0),
m_u32ScreenWidth_Mm(0),
m_u32ScreenHeight_Mm(0)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::spi_tclMLVncViewer()"));

   m_rDeviceStatus.features = scou32DefaultDeviceStatus;
   
   //Create the Viewer SDK Instance
   m_pViewerSDK = new VNCViewerSDK;
   SPI_NORMAL_ASSERT(NULL == m_pViewerSDK);

   memset(&ml_adapter_ctx, 0, sizeof(ml_adapter_ctx));

   //Initialize DLT Logging
   ml_adapter_ctx.dlt_ctx = mlink_dlt_initialize();
   SPI_NORMAL_ASSERT(NULL == ml_adapter_ctx.dlt_ctx);

   m_pMLVncViewer = this;
   SPI_NORMAL_ASSERT(NULL == m_pMLVncViewer);

}

/***************************************************************************
** FUNCTION:  spi_tclMLVncViewer::~spi_tclMLVncViewer()
***************************************************************************/

spi_tclMLVncViewer::~spi_tclMLVncViewer()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::~spi_tclMLVncViewer() "));

   if(NULL != ml_adapter_ctx.wl_ctx)
   {
      mlink_wl_finalize(ml_adapter_ctx.wl_ctx);
      ml_adapter_ctx.wl_ctx = NULL;
   }//if(NULL != ml_adapter_ctx.wl_ctx)

   if(NULL != ml_adapter_ctx.dlt_ctx)
   {
      //UnInitialize dlt logging
      mlink_dlt_finalize (ml_adapter_ctx.dlt_ctx);
      ml_adapter_ctx.dlt_ctx = NULL;
   } //if(NULL != ml_adapter_ctx.dlt_ctx)

   m_u32DeviceId =0;
   m_pVNCViewer = NULL;
   m_u16TouchEnableFlag = 0;
   m_bTouchSupport = false;
   m_bPointerSupport = false;
   m_pMLVncViewer = NULL;


   //Delete the Viewer SDK Instance
   RELEASE_MEM(m_pViewerSDK);

}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncViewer::bInitializeViewerSDK()
***************************************************************************/
t_Bool spi_tclMLVncViewer::bInitializeViewerSDK()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::bInitializeViewerSDK() "));
   t_Bool bRetVal = false;

   //Initialise the Viewer SDK
   if (NULL != m_pViewerSDK)
   {
      VNCViewerError s32ViewerSDKInitializeError = VNCViewerSDKInitialize(
         m_pViewerSDK, sizeof(VNCViewerSDK));

      bRetVal = (VNCViewerErrorNone == s32ViewerSDKInitializeError);
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncViewer::vUninitializeViewerSDK()
***************************************************************************/
t_Void spi_tclMLVncViewer::vUninitializeViewerSDK()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vUninitializeViewerSDK() "));

   if (NULL != m_pViewerSDK)
   {
      //Uninitialise the Viewer SDK
      m_pViewerSDK->vncViewerSDKUninitialize();
   }

}

/***************************************************************************
** FUNCTION:  VNCViewer* spi_tclMLVncViewer::vCreateVNCViewer()
***************************************************************************/
t_Void spi_tclMLVncViewer::vCreateVNCViewer()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vCreateVNCViewer() "));

   if (NULL != m_pViewerSDK)
   {
      //Register for VNC Viewer Callbacks
      VNCViewerCallbacks oCallbacks;
      memset(&oCallbacks, 0, sizeof(oCallbacks));

      size_t callbacksSize = sizeof(VNCViewerCallbacks);
      oCallbacks.pServerDisplayConfigurationCallback = &vServerDisplayConfigCallback;
      oCallbacks.pServerEventConfigurationCallback = &vServerEventConfigCallback;
      oCallbacks.pDeviceStatusCallback = &vDeviceStatusCallback;

      //Override the ADIT callbacks and register for these callbacks in case of GM
      if(m_u16TouchEnableFlag == 0)
      {
         ETG_TRACE_USR2(("[DESC]: ML Touch is handled by SPI. Register with our interfaces"));
         oCallbacks.pServerInitCallback = &vViewerServerInitCallback;
         oCallbacks.pDesktopResizeCallback = &vViewerDesktopResizeCallback;
      }
      else
      {
         ETG_TRACE_USR2(("[DESC]: ML Touch is handled by ADIT. Register with ADIT interfaces"));
         oCallbacks.pServerInitCallback = &mlink_wl_server_init_callback;
         oCallbacks.pDesktopResizeCallback = &mlink_wl_desktop_resize_callback;
      }

      oCallbacks.pLockRectangleExCallback = &mlink_wl_lock_rectangle_ex_callback;
      oCallbacks.pFrameBufferUpdateEndCallback = &vViewerFrameBufferUpdateEndCallback; 
      oCallbacks.pSessionProgressCallback = &vViewerSessionProgressCallback;
      oCallbacks.pErrorCallback = &vErrorCallback;
      oCallbacks.pLogCallback = &mlink_dlt_viewer_log_callback;
      oCallbacks.pThreadStartedCallback = &ViewerThreadStartedCallback ;
      oCallbacks.pContextInformationCallback = & vViewerContextInfoCallback ;
      oCallbacks.pContentAttestationFailureCallback = &vViewerContentAttestationFailureCallback ;

      //Set the initial FrameBuffer Pixel format
      VNCPixelFormat pFrameBufferPixelFormat = VNCPixelFormatRGB(s32RGB565RedBitDepth,
               s32RGB565GreenBitDepth, s32RGB565BlueBitDepth, s32BigEndianFlag);

      ETG_TRACE_USR4(("[PARAM]: vCreateVNCViewer:BigEndianFlag = %d",pFrameBufferPixelFormat.bigEndianFlag));
      ETG_TRACE_USR4(("[PARAM]: vCreateVNCViewer:bitsPerPixel = %d",pFrameBufferPixelFormat.bitsPerPixel));
      ETG_TRACE_USR4(("[PARAM]: vCreateVNCViewer:blueMax = %d",pFrameBufferPixelFormat.blueMax));

      //Create the VNC Viewer
      m_pVNCViewer = m_pViewerSDK->vncViewerCreate(&oCallbacks,
         callbacksSize,
         &pFrameBufferPixelFormat);

      if(NULL != m_pVNCViewer )
      {
         if(NULL != ml_adapter_ctx.dlt_ctx)
         {
            //Set the context 
            m_pViewerSDK->vncViewerSetContext(m_pVNCViewer,&ml_adapter_ctx);
         }//if(NULL != ml_adapter_ctx.dlt_ctx)

         // Set the Pixel format
         m_pViewerSDK->vncViewerSetPixelFormat(m_pVNCViewer,&pFrameBufferPixelFormat);

         ETG_TRACE_USR4((" [PARAM]: vCreateVNCViewer:Pixel format RGB%d%d%d set during the viewer creation",
            s32RGB565RedBitDepth,s32RGB565GreenBitDepth,s32RGB565BlueBitDepth));

         //set the logging level
         t_Char czLogLevel[MAX_KEYSIZE] = { 0 };
         snprintf(czLogLevel,MAX_KEYSIZE,"*:%d",VIEWER_LOG_LEVEL);
         vSetViewerParameter(VNCParameterLog,czLogLevel);

         //return type is unused for now.
         bAddLicense();

         //Set Invalid region parameter tracking
         vSetViewerParameter(VNCParameterInvalidRegionTracking,VNCViewerInvalidRegionTrackingBounding);

         //VNCParameterMirrorLink and VNCParameterRSAKeys must be set before viewer thread is started.
         //Set the session to ML
         vSetViewerParameter(VNCParameterMirrorLink,"1");

         //Encoding must be set before wayland initialization happens
         //Or else nothing will be shown on HMI
         vSetViewerParameter(VNCParameterPreferredEncoding,TRLE_ENCODING);

         //Set the RSA Key with SDK
         t_Char* rsaKeys = m_pViewerSDK->vncGenerateRSAKeys(RSA_KEYS_BITS);
         if (NULL != rsaKeys)
         {
            vSetViewerParameter(VNCParameterRSAKeys,rsaKeys);
            m_pViewerSDK->vncFreeString(rsaKeys);
         }//if (NULL != rsaKeys)
      }
   }
}

/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vDestroyVNCViewer()
***************************************************************************************/
t_Void spi_tclMLVncViewer::vDestroyVNCViewer()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vDestroyVNCViewer()"));
   m_rDeviceStatus.features = scou32DefaultDeviceStatus;
   if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      //Destroy the VNC Viewer
      m_pViewerSDK->vncViewerDestroy(m_pVNCViewer);
      m_pVNCViewer = NULL;
   }
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncViewer::bMLWLInitialize()
***************************************************************************/
t_Bool spi_tclMLVncViewer::bMLWLInitialize(t_U32 u32layer_id, 
                                           t_U32 u32surface_id,
                                           t_U32 u32screen_offset_x,
                                           t_U32 u32screen_offset_y,
                                           t_U32 u32screen_width, 
                                           t_U32 u32screen_height,
                                           t_Bool bForceFullScreen,
                                           t_U8 u8Flag,
                                           t_U32 u32ScreenHeight_Mm,
                                           t_U32 u32ScreenWidth_Mm)
{

   ETG_TRACE_USR1(("spi_tclMLVncViewer::bMLWLInitialize() - Layer:%d Surface:%d X-Offset:%d Y-Offset:%d \n" \
      "Screen Width:%d Screen Height:%d Render image to Full Screen with out aspect ratio:%d Enable Wayland touch:%d ",
      u32layer_id,u32surface_id,u32screen_offset_x,u32screen_offset_y,u32screen_width,u32screen_height,
      ETG_ENUM(BOOL,bForceFullScreen),u8Flag));

   m_u32ScreenHeight = u32screen_height;
   m_u32ScreenWidth = u32screen_width;
   m_u32ScreenHeight_Mm = u32ScreenHeight_Mm;
   m_u32ScreenWidth_Mm = u32ScreenWidth_Mm;
   t_Bool bRetVal = false;

   //Initial FrameBuffer Pixel format
   VNCPixelFormat pFrameBufferPixelFormat = VNCPixelFormatRGB(s32RGB565RedBitDepth,
            s32RGB565GreenBitDepth, s32RGB565BlueBitDepth, s32BigEndianFlag);
   ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS," [DESC]: Client requested to use RGB%d%d%d pixel format",
            s32RGB565RedBitDepth,s32RGB565GreenBitDepth,s32RGB565BlueBitDepth));
   if( (NULL != m_pViewerSDK)&&(NULL != m_pVNCViewer ))
   {
      DltContext* pDltContext = NULL;

      if(NULL != ml_adapter_ctx.dlt_ctx)
      {
         pDltContext=mlink_dlt_get_dlt_context(ml_adapter_ctx.dlt_ctx);
         //Added for debugging purpose
         if(NULL == pDltContext)
         {
            ETG_TRACE_ERR(("[ERR]:bMLWLInitialize: pDltContext is NULL "));
         }//if(NULL == pDltContext)
      }

      //Initialize Wayland for Video Rendering
      ml_adapter_ctx.wl_ctx = mlink_wl_initialize(
         m_pViewerSDK, m_pVNCViewer,pDltContext,
         u32layer_id, u32surface_id, &pFrameBufferPixelFormat,
         u32screen_offset_x, u32screen_offset_y,
         u32screen_width, u32screen_height, bForceFullScreen,(mlink_wl_flags) u8Flag, vML_wl_error_callback);

 
      //Added for debugging purpose
      if( NULL == ml_adapter_ctx.wl_ctx )
      {
         ETG_TRACE_USR1(("[ERR]:bMLWLInitialize: ml_adapter_ctx.wl_ctx is NULL "));
      }//if( NULL == ml_adapter_ctx.wl_ctx )
      bRetVal = ( NULL != ml_adapter_ctx.wl_ctx );
   }
   return bRetVal;
}

/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vMLWLFinalise()
***************************************************************************************/
t_Void spi_tclMLVncViewer::vMLWLFinalise()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vMLWLFinalise() "));
   if(NULL != ml_adapter_ctx.wl_ctx)
   {
      mlink_wl_finalize(ml_adapter_ctx.wl_ctx);
      //Set the wl context to NULL, after the wl finalize
      ml_adapter_ctx.wl_ctx = NULL;

      m_u32DeviceId = 0;
   }//if(NULL != ml_adapter_ctx.wl_ctx)
}

/**********************************************************************************
** FUNCTION:static t_Void VNCCALL ViewerThreadStartedCallback(VNCViewer *pViewer...
**********************************************************************************/
t_Void spi_tclMLVncViewer::ViewerThreadStartedCallback(VNCViewer *pViewer,
                                                     t_Void *pContext)
{
   SPI_INTENTIONALLY_UNUSED(pViewer);
   SPI_INTENTIONALLY_UNUSED(pContext);
   //This call back is added for debugging purpose
   ETG_TRACE_USR1(("spi_tclMLVncViewer::ViewerThreadStartedCallback() "));
   DEFINE_THREAD_NAME("VNCViewerSDK");
}

/********************************************************************************************
** FUNCTION:  static t_Void vML_wl_error_callback(mlink_adapter_context * pCtx, t_Char * pMessage)
**********************************************************************************************/
t_Void spi_tclMLVncViewer::vML_wl_error_callback(mlink_adapter_context * pCtx,
                                               t_Char * pMessage)
{
   SPI_INTENTIONALLY_UNUSED(pCtx);
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vML_wl_error_callback(): Message - %s ",pMessage));

   spi_tclMLVncMsgQInterface *poMsgQInterface =
      spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLWLInitialiseErrorMsg oMLWLInitialiseErrorMsg;
      if (NULL != oMLWLInitialiseErrorMsg.pszWLinitFailureMsg)
      {
         *(oMLWLInitialiseErrorMsg.pszWLinitFailureMsg) = pMessage;
      }
      poMsgQInterface->bWriteMsgToQ(&oMLWLInitialiseErrorMsg,
         sizeof(oMLWLInitialiseErrorMsg));
   }//if (NULL != poMsgQInterface)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncViewer::bProcessCmdString
***************************************************************************/
t_Bool spi_tclMLVncViewer::bProcessCmdString(const t_U32 cou32AppId, const t_U32 cou32DeviceId)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::bProcessCmdString(): Device-0x%x,App-0x%x ",cou32DeviceId,cou32AppId));

   t_Bool bRetVal = false;
   m_u32DeviceId= cou32DeviceId;

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rvncDeviceHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DeviceId);
   std::string szAppId = oDiscDataIntf.szGetAppID(cou32DeviceId,cou32AppId);

   //Buffer to write the VNC Keys along with AppId, to fecth values from SDK.
   t_Char czKey[MAX_KEYSIZE] = { 0 };
   t_Char* pcVal = NULL;

   if((NULL != rvncDeviceHandles.poDiscoverySdk)&&(NULL != rvncDeviceHandles.poDiscoverer)
      &&(NULL != rvncDeviceHandles.poEntity))
   {
      //Fetch the VNCCmd of the Application using discoverer handles
      sprintf(czKey, VNCDiscoverySDKMLAppVncCmd, szAppId.c_str());

      t_S32 s32Error = cs32MLDiscoveryErrorNone;
      s32Error = const_cast<VNCDiscoverySDK*>(rvncDeviceHandles.poDiscoverySdk) -> 
         fetchEntityValueBlocking(
         const_cast<VNCDiscoverySDKDiscoverer*>(rvncDeviceHandles.poDiscoverer),
         const_cast<VNCDiscoverySDKEntity*>(rvncDeviceHandles.poEntity),
         czKey,&pcVal,cs32MLDiscoveryInstantCall);
      //Debug purpose
      if(s32Error != cs32MLDiscoveryErrorNone )
      {
         ETG_TRACE_ERR(("[ERR]:bProcessCmdString:VncCmd Fetch failed with the error: %d ",s32Error));
      }//if(s32Error != cs32MLDiscoveryErrorNone )
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:bProcessCmdString: One or more Discovery Handlers are NULL "));
   }

   if((NULL!= m_pViewerSDK)&&(NULL!= m_pVNCViewer)&&(NULL != pcVal)&&
      (NULL != rvncDeviceHandles.poDiscoverySdk))
   {
      ETG_TRACE_USR2(("[PARAM]: ProcessCmdString: Value - %s ",pcVal));

      //Process the VNC Command String
      VNCViewerError s32ViewerError = m_pViewerSDK->vncViewerProcessCommandString(m_pVNCViewer,pcVal);
      bRetVal = (VNCViewerErrorNone == s32ViewerError);

      const_cast<VNCDiscoverySDK*>(rvncDeviceHandles.poDiscoverySdk) -> freeString(pcVal);
   }//if((NULL!= m_pViewerSDK)&&(NULL!= m_pVNCViewer)&&(NULL != pcVal)&&

   return bRetVal;
}

/*****************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vPumpWaylandEvents()
*****************************************************************************/
t_Void spi_tclMLVncViewer::vPumpWaylandEvents()
{
   //Send Wayland events only when the wl context is not null
   if(NULL != ml_adapter_ctx.wl_ctx )
   {
      mlink_wl_pump_events(ml_adapter_ctx.wl_ctx);
   }//if(NULL != ml_adapter_ctx.wl_ctx )
}

/***************************************************************************
** FUNCTION:  VNCViewer* spi_tclMLVncViewer::vSetSessionParameters()
***************************************************************************/
t_Void spi_tclMLVncViewer::vSetSessionParameters(
   const t_Char* pcocPrefferedEncoding, const t_Char* pcocCntAttestFlag,
   const t_Char* pcocPublickey)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vSetParameter()"));

   //Set the Preferred Encoding. Encoding format shouldn't be NULL.
   if (NULL != pcocPrefferedEncoding)
   {
      vSetViewerParameter(VNCParameterPreferredEncoding,pcocPrefferedEncoding);
   }

   //Set Content attestation signature Flag to enable content attestation
   if (NULL != pcocCntAttestFlag)
   {
      ETG_TRACE_USR4(("[PARAM]:vSetParameter:Content Attestation Flag value -%s",pcocCntAttestFlag));
      vSetViewerParameter(VNCParameterContentAttestationSignatureFlag,pcocCntAttestFlag);
   }

   //Set the VNCServerPublicKey to enable content attestation
   if (NULL != pcocPublickey)
   {
      ETG_TRACE_USR4(("[PARAM]:vSetParameter:Public key value-%s",pcocPublickey));
      vSetViewerParameter(VNCParameterMirrorLinkVNCServerPublicKey,pcocPublickey);
   }
}

/***************************************************************************
** FUNCTION:  VNCViewerSDK* spi_tclMLVncViewer::pGetViewerSDK()
***************************************************************************/
VNCViewerSDK* spi_tclMLVncViewer::pGetViewerSDK()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::pGetViewerSDK()"));
   //Return the viewer SDK instance
   return m_pViewerSDK;
}

/***************************************************************************
** FUNCTION:  VNCViewer* spi_tclMLVncViewer::pGetViewerInstance()
***************************************************************************/
VNCViewer* spi_tclMLVncViewer::pGetViewerInstance()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::pGetViewerInstance()"));
   //Return the viewer instance
   return m_pVNCViewer;
}

/***************************************************************************
** FUNCTION:  VNCViewer* spi_tclMLVncViewer::vSetContext()
***************************************************************************/
t_Void spi_tclMLVncViewer::vSetContext()const
{
   //unused
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vSetContext()"));
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncViewer::bRegisterExtension(const t_Char*
**                                                    pu8Extensionname)
***************************************************************************/
t_Bool spi_tclMLVncViewer::bRegisterExtension(const t_Char* pcocExtensionname)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::bRegisterExtension()"));
   t_Bool bRetVal = false;

   VNCExtension* pExtension = NULL;

   if ((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer) && (NULL
      != pcocExtensionname))
   {
      //Register the extension
      VNCViewerError s32ViewerError = m_pViewerSDK->vncViewerRegisterExtension(
         m_pVNCViewer, pcocExtensionname, &vExtensionEnabledCallback,
         &vExtensionMessageReceivedCallback, &pExtension);

      bRetVal = (VNCViewerErrorNone == s32ViewerError);
   } //if ((NULL != m_pViewerSDK..
   return bRetVal;

}

/*******************************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncViewer::vExtensionMessageReceivedCallback()
*******************************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vExtensionMessageReceivedCallback(VNCViewer *pViewer,
                                                                   t_Void *pContext,
                                                                   VNCExtension *pExtension,
                                                                   t_Void *pExtensionContext,
                                                                   const vnc_uint8_t *payload,
                                                                   size_t payloadLength)
{
   (t_Void)payload;
   SPI_INTENTIONALLY_UNUSED(pViewer);
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(pExtension);
   SPI_INTENTIONALLY_UNUSED(pExtensionContext);
   SPI_INTENTIONALLY_UNUSED(payloadLength);
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vExtensionMessageReceivedCallback() "));
}

/***************************************************************************
** FUNCTION:  VNCViewer* spi_tclMLVncViewer::bIsTouchSupported()
***************************************************************************/
t_Bool spi_tclMLVncViewer::bIsTouchSupported()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::bIsTouchSupported()"));
   t_Bool bTouchSupport = false;
   //Check if touch events are supported
   if ((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   {
      //Check if touch is supported
      bTouchSupport = m_pViewerSDK->vncViewerGetProperty(m_pVNCViewer,
         VNCViewerPropertyServerSupportsTouchEvents);
      ETG_TRACE_USR4(("[DESC]: Are Touch events supported by Phone: %d", ETG_ENUM(BOOL,bTouchSupport)));
   }
   return bTouchSupport;
}

/***************************************************************************
** FUNCTION:  VNCViewer* spi_tclMLVncViewer::bIsPointerSupported()
***************************************************************************/
t_Bool spi_tclMLVncViewer::bIsPointerSupported()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::bIsPointerSupported()"));
   t_Bool bPointerSupport = false;
   //Check if touch events are supported
   if ((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   {
      //Check if pointer event is supported
      bPointerSupport = m_pViewerSDK->vncViewerGetProperty(m_pVNCViewer,
         VNCViewerPropertyServerSupportsPointerEvents);
      ETG_TRACE_USR4(("[DESC]: Are Pointer events supported by Phone: %d", ETG_ENUM(BOOL,bPointerSupport)));
   }
   return bPointerSupport;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncViewer::rfrGetScreenAttributes()
***************************************************************************/
t_Void spi_tclMLVncViewer::rfrGetScreenAttributes(
   trScreenAttributes& rfrScreenAttributes)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::rfrGetScreenAttributes()"));

   if ((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   {
      //Get the Screen Width
      rfrScreenAttributes.u32ScreenWidth = m_pViewerSDK->vncViewerGetProperty(
         m_pVNCViewer, VNCViewerPropertyServerDesktopWidth);
      ETG_TRACE_USR4(("[PARAM]: rfrGetScreenAttributes:Server Desktop Width = %d ", rfrScreenAttributes.u32ScreenWidth));

      //Get the screen height
      rfrScreenAttributes.u32ScreenHeight = m_pViewerSDK->vncViewerGetProperty(
         m_pVNCViewer, VNCViewerPropertyServerDesktopHeight);
      ETG_TRACE_USR4(("[PARAM]: rfrGetScreenAttributes:Server Desktop Height = %d ", rfrScreenAttributes.u32ScreenHeight));
   }
}

/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vSendTouchEventToServer(const t_U32 ..)
***************************************************************************************/
t_Void spi_tclMLVncViewer::vSendTouchEventToServer(const t_U32 cou32DeviceHandle,
                                                   trTouchData& rfrTouchData,
                                                   trScalingAttributes& rfrScaleData)
{
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   t_U8 u8Indentifier = 0;
   t_U8 u8TouchCount = 0;
   t_U8 u8TouchMode = 0;
   t_Bool bMultiTouch = false;
   t_S32 s32xCoordinate = 0;
   t_S32 s32yCoordinate = 0;

   std::vector<trTouchInfo>::iterator itTouchInfo;
   std::vector<trTouchCoordinates> vecTouchCoord;
   std::vector<trTouchCoordinates>::iterator itTouchCoord;
    


   //Check for NULL pointers
   if ((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   {
      size_t u32touchDescriptorCount = rfrTouchData.u32TouchDescriptors;
      ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:Touch Desciptor Count : %d ",u32touchDescriptorCount));

      VNCTouchDescriptor atouchDescriptors[u32touchDescriptorCount];
	  memset(atouchDescriptors, 0X00, sizeof(atouchDescriptors));

      //Iterate through the list and populate the touch co-ordinates
      for (itTouchInfo = rfrTouchData.tvecTouchInfoList.begin(); itTouchInfo
         != rfrTouchData.tvecTouchInfoList.end(); itTouchInfo++)
      {
         vecTouchCoord = itTouchInfo->tvecTouchCoordinatesList;

         for (itTouchCoord = vecTouchCoord.begin(); itTouchCoord != vecTouchCoord.end(); itTouchCoord++)
         {
            u8TouchMode = static_cast<tenTouchMode>(itTouchCoord->enTouchMode);
            ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:Touch Mode : %d ",u8TouchMode));

            u8Indentifier = itTouchCoord->u8Identifier;

            // Compute new (X,Y)coordinates based on scaling factor/new x,y axis.
            s32xCoordinate = ((itTouchCoord->s32XCoordinate)- (rfrScaleData.s32XStartCoordinate));
            s32yCoordinate = ((itTouchCoord->s32YCoordinate)- (rfrScaleData.s32YStartCoordinate));

            ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:X Co-Ordinate  : %ld ",s32xCoordinate));
            ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:Y Co-Ordinate  : %ld ",s32yCoordinate));

            if ( ((0 > s32xCoordinate) || (s32xCoordinate > (t_S32)rfrScaleData.u32ScreenWidth)) ||
               ((0 > s32yCoordinate) || (s32yCoordinate > (t_S32)rfrScaleData.u32ScreenHeight)) )
            {
               ETG_TRACE_USR4(("[DESC]:vSendTouchEventToServer: Touch Coordinates not in the display area "));
            	  return;
            }
            // Apply Scaling now
            s32xCoordinate = (t_S32)(s32xCoordinate *rfrScaleData.fWidthScaleValue);
            s32yCoordinate = (t_S32)(s32yCoordinate *rfrScaleData.fHeightScalingValue);

            ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:Scalled  X Co-Ordinate  : %ld ",s32xCoordinate));
            ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:Scalled  Y Co-Ordinate  : %ld ",s32yCoordinate));

            if(m_bTouchSupport && 
               (VNCPointerSupportTouchEvents == (m_oServerCapabilities.rKeyCapabilities.u32PointerTouchSupport & VNCPointerSupportTouchEvents)))
            {
               bMultiTouch = true;
               atouchDescriptors[u8TouchCount].location.x = s32xCoordinate;
               atouchDescriptors[u8TouchCount].location.y = s32yCoordinate;

                     atouchDescriptors[u8TouchCount].pressure = u8TouchMode?TOUCH_PRESSURE:NO_TOUCH_PRESSURE;
                     atouchDescriptors[u8TouchCount].identifier = u8Indentifier;
                     atouchDescriptors[u8TouchCount].buttonMask
                        = u8TouchMode?VNCPointerDeviceButton1:VNCPointerDeviceButtonNone;

                     ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:Pressure : %d ",atouchDescriptors[u8TouchCount].pressure));
                     ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:Identifier : %d ",atouchDescriptors[u8TouchCount].identifier));
                     ETG_TRACE_USR4(("[PARAM]:vSendTouchEventToServer:Button Mask : %d ",atouchDescriptors[u8TouchCount].buttonMask));
                     /*Currently timestamp is not provided by the HMI,hence use the values 0,so that SDK takes care of the timestamp
                     u32TimeStampSecs = (itr->u32TimeStamp) * 1000;
                     u32TimeStampNanoSecs = u32TimeStampSecs * pow(10, -9);*/

               u8TouchCount++;
            } //if(m_bTouchSupport && 
         } //for (itTouchCoord = vecTouchCoord.begin();
      } //for (itTouchInfo = rfrTouchData.tvecTouchInfoList.begin();
      if( false == bIsML10Device(m_rVersionInfo.u32MajorVersion,m_rVersionInfo.u32MinorVersion) )
      {
         if(true == bMultiTouch)
         {
            ETG_TRACE_USR2(("[PARAM]:vSendTouchEventToServer:Send Touch Event"));
            //Send the touch event to server
            m_pViewerSDK->vncViewerSendTouchEvent(m_pVNCViewer, atouchDescriptors,
               u32touchDescriptorCount, TIMESTAMP_SECONDS, TIMESTAMP_NANOSECONDS);
         }//if(true == bMultiTouch)
         else
         {
            ETG_TRACE_USR2(("[PARAM]:vSendTouchEventToServer:Send Pointer Event"));
            vSendPointerEvents(s32xCoordinate,s32yCoordinate,u8TouchMode);
         }
      } //if( false == bIsML10Device(m_rVersionInfo.u32MajorVer
      else
      {
         //If the Server is ML1.0 Phone, only enable pointer events irrespective of the ServerEventConfig
         //Some ML1.0 Servers advertising that it supports Multi touch but when we send touch events, it is crashing.
         vSendPointerEvents(s32xCoordinate,s32yCoordinate,u8TouchMode);
      }//else
   }//if ((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   else
   {
      ETG_TRACE_ERR(("[ERR]:vSendTouchEventToServer:Either m_pViewerSDK or m_pVNCViewer is NULL"));
   }
}

/****************************************************************************************
  ** FUNCTION: t_Void spi_tclMLVncViewer::vSendPointerEvents(t_S32 ..)
***************************************************************************************/
t_Void spi_tclMLVncViewer::vSendPointerEvents(t_S32 xCoordinate,t_S32 yCoordinate,
                                              t_U8 u8TouchMode)
{

   if(m_bPointerSupport && (NULL != m_pViewerSDK) && (NULL != m_pVNCViewer) && 
      (VNCPointerSupportPointerEvents == (m_oServerCapabilities.rKeyCapabilities.u32PointerTouchSupport & VNCPointerSupportPointerEvents)))
   {
      ETG_TRACE_USR1((" spi_tclMLVncViewer::vSendPointerEvents "));
      VNCPointerDeviceButton buttonMask = u8TouchMode?VNCPointerDeviceButton1:VNCPointerDeviceButtonNone;
      m_pViewerSDK->vncViewerSendPointerEvent(m_pVNCViewer, buttonMask,
         (t_U16)xCoordinate,(t_U16)yCoordinate);
   }
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncViewer::vFetchDeviceVersion()
***************************************************************************/
t_Void spi_tclMLVncViewer::vFetchDeviceVersion(const t_U32 cou32DeviceHandle,trVersionInfo& rfrVersionInfo)
{
   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rvncDeviceHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DeviceHandle);

   if((NULL != rvncDeviceHandles.poDiscoverySdk) &&(NULL != rvncDeviceHandles.poDiscoverer)&&
      (NULL != rvncDeviceHandles.poEntity))
   {
      //ML Major Version & Minor Version
      t_U32 u32MLMajorVer = u32FetchKeyValue(rvncDeviceHandles.poDiscoverySdk,
         rvncDeviceHandles.poDiscoverer, rvncDeviceHandles.poEntity, VNCDiscoverySDKMLRootMajorVersion);

      rfrVersionInfo.u32MajorVersion = ( bIsInvalidMLMajorVersion(u32MLMajorVer) )
         ?(scou32ML_Major_Version_1):(u32MLMajorVer);

      rfrVersionInfo.u32MinorVersion = u32FetchKeyValue(rvncDeviceHandles.poDiscoverySdk,
         rvncDeviceHandles.poDiscoverer, rvncDeviceHandles.poEntity, VNCDiscoverySDKMLRootMinorVersion);

      rfrVersionInfo.u32PatchVersion=0;

      ETG_TRACE_USR2(("spi_tclMLVncViewer::vFetchDeviceVersion:Dev-0x%x Major-%d Minor-%d",
         cou32DeviceHandle,rfrVersionInfo.u32MajorVersion,rfrVersionInfo.u32MinorVersion));
   }
}

/***************************************************************************
** FUNCTION:  t_U32 spi_tclMLVncViewer::u32FetchKeyValue()
***************************************************************************/
t_U32 spi_tclMLVncViewer::u32FetchKeyValue(const VNCDiscoverySDK* poDiscSDK, 
                                           const VNCDiscoverySDKDiscoverer* poDisc,
                                           const VNCDiscoverySDKEntity* poEntity,
                                           const t_Char *pczKey,
                                           t_S32 s32TimeOut)
{

   t_U32 u32RetVal = 0;

      t_Char *czResult = NULL ;
      if((NULL != poDiscSDK)&&(NULL != poEntity)&&(NULL != poDisc)&&(NULL != pczKey))
      {
         //Get the error code, if any
         t_S32 s32Error = poDiscSDK->fetchEntityValueBlocking(const_cast<VNCDiscoverySDKDiscoverer*>(poDisc),
         const_cast<VNCDiscoverySDKEntity*>(poEntity), pczKey, &czResult, s32TimeOut);

         if((cs32MLDiscoveryErrorNone == s32Error)&&(NULL != czResult))
         {
            //String Utility Handler
            StringHandler oStrUtil(czResult);
            u32RetVal = oStrUtil.u32ConvertStrToInt();

            ETG_TRACE_USR4((" [PARAM]:u32FetchKeyValue:Key - %s",pczKey));
            ETG_TRACE_USR4((" [PARAM]:u32FetchKeyValue:Value - %s",czResult));

            poDiscSDK->freeString(czResult);
            czResult = NULL;
         }
         else
         {
            ETG_TRACE_ERR(("[ERR]:spi_tclMLVncViewer::u32FetchKeyValue:Error in fetching value of %s",pczKey));
         }
      }
      else
      {
         ETG_TRACE_ERR(("[ERR]:spi_tclMLVncViewer::u32FetchKeyValue:Discover handles are NULL"));
      }

   return u32RetVal;
}



/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vSendKeyEventToServer()
***************************************************************************************/
t_Void spi_tclMLVncViewer::vSendKeyEventToServer(tenKeyMode enKeyMode,
                                                 tenKeyCode enKeyCode)
{
   t_Bool bTriggerKeyEvent = false;


   t_U8 u8Length= sizeof(scoarKeyCodeMapping)/sizeof(trKeyCodeMapping);
   for(t_U8 u8Index=0;u8Index<u8Length;u8Index++)
   {
      if(enKeyCode == scoarKeyCodeMapping[u8Index].enKeyCode)
      {
         switch(scoarKeyCodeMapping[u8Index].enKeyType)
         {
         case e8ML_DEV_KEY:
            {
               bTriggerKeyEvent = (scoarKeyCodeMapping[u8Index].u32KeyCode == 
                  (m_oServerCapabilities.rKeyCapabilities.u32DeviceKeySupport & scoarKeyCodeMapping[u8Index].u32KeyCode));
            }
            break;
         case e8ML_MULTIMEDIA_KEY:
            {
               bTriggerKeyEvent = (scoarKeyCodeMapping[u8Index].u32KeyCode == 
                  (m_oServerCapabilities.rKeyCapabilities.u32MultimediaKeySupport & scoarKeyCodeMapping[u8Index].u32KeyCode));
            }
            break;
         case e8ML_MISC_KEY:
            {
               bTriggerKeyEvent = (scoarKeyCodeMapping[u8Index].u32KeyCode == 
                  (m_oServerCapabilities.rKeyCapabilities.u32MiscKeySupport & scoarKeyCodeMapping[u8Index].u32KeyCode));
            }
            break;
         case e8ML_KNOB_KEY:
            {
               bTriggerKeyEvent = (scoarKeyCodeMapping[u8Index].u32KeyCode == 
                  (m_oServerCapabilities.rKeyCapabilities.u32KnobKeySupport & scoarKeyCodeMapping[u8Index].u32KeyCode));
            }
            break;
         default:
            {
               //None
            }
         }
         //element found, exit from the loop.
         break;
      }//if(enKeyCode == scoarKeyCode
   }//for(t_U8 u8In

   if((true == bTriggerKeyEvent) && (NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   {
      m_pViewerSDK->vncViewerSendKeyEvent(m_pVNCViewer, enKeyMode, enKeyCode);
      ETG_TRACE_USR4(("[DESC]:Key Event is sent to Phone:Key Mode -%d,Key Code - %d ",enKeyMode,enKeyCode));
   }//if((true == bTriggerKeyEvent) && (N
   else
   {
      ETG_TRACE_ERR(("[DESC]:%d Key Event is not supported for ML or Objects are NULL",enKeyCode));
   }//else
}

/***********************************************************************************************
** FUNCTION: t_Void VNCCALL spi_tclMLVncViewer::vViewerSessionProgressCallback()
***********************************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vViewerSessionProgressCallback(VNCViewer *pViewer,
                                                                t_Void *pContext,
                                                                VNCViewerSessionProgress enSessionProgress)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vViewerSessionProgressCallback:Session status: %d ",
      ETG_ENUM(SESSION_PROGRESS_STATUS,enSessionProgress)));

   (t_Void)pViewer;
   (t_Void)pContext;

   if((VNCViewerSessionProgressSessionEstablished == enSessionProgress)&& (NULL!= m_pMLVncViewer))
   {
      //Fetch the device version info
      m_pMLVncViewer->vFetchDeviceVersion(m_u32DeviceId,m_rVersionInfo);
      m_bTouchSupport = m_pMLVncViewer->bIsTouchSupported();

      //Check whether server supports touch events and then enable multi touch
      //Enable the multi touch for the devices, ML1.1 or more only      
      if( m_bTouchSupport && (NULL!= ml_adapter_ctx.wl_ctx)&& (ENABLE_TOUCH_FLAG == m_u16TouchEnableFlag)&&
         (false == bIsML10Device(m_rVersionInfo.u32MajorVersion,m_rVersionInfo.u32MinorVersion))
         )
      {
         ETG_TRACE_USR2(("[DESC]: Enabling Multi touch  for connected ML Device on ADIT"));
         mlink_wl_enable_multitouch(ml_adapter_ctx.wl_ctx, scou32MultiTouchEnable);
      }//if( m_bTouchSupport && (NULL!= ml_adapter_ctx.wl_ctx)&& (ENABLE_TOUCH_FLAG == 

      m_bPointerSupport = m_pMLVncViewer->bIsPointerSupported();
   }

   spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if(NULL!= poMsgQInterface)
   {
      MLSessionProgressMsg oMLSessionProgressMsg;
      oMLSessionProgressMsg.enSessionProgress = static_cast<tenSessionProgress>(enSessionProgress);
      poMsgQInterface->bWriteMsgToQ(&oMLSessionProgressMsg, sizeof(oMLSessionProgressMsg));
   }//if(NULL!= poMsgQInterface)
}

/**********************************************************************************
** FUNCTION:  t_Void VNCCALL spi_tclMLVncViewer:: vViewerServerInitCallback()
**********************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vViewerServerInitCallback(VNCViewer *pViewer,
                                                             t_Void *pContext,
                                                             t_U16 width,
                                                             t_U16 height,
                                                             const char *desktopname,
                                                             const VNCPixelFormat *pServerNativePixelFormat)
{
   ETG_TRACE_USR1(("vViewerServerInitCallback:framebuffer Width-%d Height-%d ",
      width,height));

   if((NULL!=pViewer) && (NULL!= desktopname) && (NULL!= pServerNativePixelFormat))
   {
      ETG_TRACE_USR4(("[PARAM]:vViewerServerInitCallback:Desktop name - %s ",desktopname));

      mlink_wl_server_init_callback(pViewer, pContext,
         width, height, desktopname,
         pServerNativePixelFormat);

      spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
      if(NULL!= poMsgQInterface)
      {
         MLFrameBufferDimensionMsg oMLFrameBufferDimensionMsg;
         oMLFrameBufferDimensionMsg.u32Width = width;
         oMLFrameBufferDimensionMsg.u32Height = height;
         poMsgQInterface->bWriteMsgToQ(&oMLFrameBufferDimensionMsg, sizeof(oMLFrameBufferDimensionMsg));
      }

   }
}

/**********************************************************************************
** FUNCTION:  t_Void VNCCALL spi_tclMLVncViewer:: vViewerDesktopResizeCallback()
/**********************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vViewerDesktopResizeCallback(VNCViewer *pViewer,
                                                                t_Void *pContext,
                                                                t_U16 width,
                                                                t_U16 height)
{
   ETG_TRACE_USR1(("vViewerDesktopResizeCallback:Server Display Width-%d Height-%d",
	   width,height));

   if(NULL!=pViewer)
   {
      mlink_wl_desktop_resize_callback(pViewer,pContext,
         width, height);

      spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
      if(NULL!= poMsgQInterface)
      {
         MLServerDispDimensionMsg oMLServerDispDimensionMsg;
         oMLServerDispDimensionMsg.u32Width = width;
         oMLServerDispDimensionMsg.u32Height = height;
         poMsgQInterface->bWriteMsgToQ(&oMLServerDispDimensionMsg, sizeof(oMLServerDispDimensionMsg));
      }
   }
}

	
/***************************************************************************
** FUNCTION:  VNCViewer* spi_tclMLVncViewer::vExtensionEnabledCallback()
***************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vExtensionEnabledCallback(VNCViewer *pViewer,
                                                           t_Void *pContext,
                                                           VNCExtension *pExtension,
                                                           t_Void *pExtensionContext,
                                                           vnc_int32_t enabled)
{
   SPI_INTENTIONALLY_UNUSED(pViewer);
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(pExtension);
   SPI_INTENTIONALLY_UNUSED(pExtensionContext);
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vExtensionEnabledCallback() entered "));

   t_U32 u32ExtensionEnabled = enabled;
   spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if(NULL!= poMsgQInterface)
   {
      MLExtensionEnabledMsg oMLExtensionEnabledMsg;
      oMLExtensionEnabledMsg.u32ExtEnabled = u32ExtensionEnabled;
      poMsgQInterface->bWriteMsgToQ(&oMLExtensionEnabledMsg, sizeof(oMLExtensionEnabledMsg));
   }
}

/*****************************************************************************************************
** FUNCTION: static t_Void VNCCALL spi_tclMLVncViewer::vServerEventConfigCallback()
*****************************************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vServerEventConfigCallback(VNCViewer *pViewer,t_Void *pContext,
                                                            const VNCServerEventConfiguration *pServerEventConfiguration,
                                                            size_t serverEventConfigurationSize,
                                                            VNCClientEventConfiguration *pClientEventConfiguration,
                                                            size_t clientEventConfigurationSize,
                                                            VNCDeviceStatusRequest *pFirstDeviceStatusRequest,
                                                            size_t firstDeviceStatusRequestSize)
{
   SPI_INTENTIONALLY_UNUSED(pViewer);
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(clientEventConfigurationSize);
   SPI_INTENTIONALLY_UNUSED(firstDeviceStatusRequestSize);
   SPI_INTENTIONALLY_UNUSED(serverEventConfigurationSize);
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vServerEventConfigCallback() entered "));
   spi_tclMLVncViewer* pVncViewer = spi_tclMLVncViewer::getInstance();

   if(
      (NULL!= pVncViewer)
      && 
      (NULL!= pFirstDeviceStatusRequest)
      &&
      (NULL!= pServerEventConfiguration)
      &&
      (NULL!= pClientEventConfiguration)
      )
   {
      //Populate the server Configuration structure
      pVncViewer->vPopulateServerCapabilities(
         pServerEventConfiguration);

      //Populate the client capabilities structure
      pVncViewer->vPopulateClientCapabilities(
         pServerEventConfiguration,pClientEventConfiguration);


      ETG_TRACE_USR2(("[PARAM]:vServerEventConfigCallback: Device Status Message feature value-0x%x ",
         pFirstDeviceStatusRequest->features));

      //get the orientation mode
      tenOrientationMode enOrientationMode = pVncViewer->enGetOrientationMode((
         pFirstDeviceStatusRequest->features)&
         VNCDeviceStatusFeatureOrientationMASK);

      spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();

      if(NULL!= poMsgQInterface)
      {
         MLServerCapabilitiesMsg oMLServerCapabilitiesMsg;
         oMLServerCapabilitiesMsg.rServerCapabilities = m_oServerCapabilities;
         poMsgQInterface->bWriteMsgToQ(&oMLServerCapabilitiesMsg, sizeof(oMLServerCapabilitiesMsg));
      }


      if(NULL!= poMsgQInterface)
      {
         MLClientCapabilitiesMsg oMLClientCapabilitiesMsg;
         spi_tclMLVncViewer* poViewer = spi_tclMLVncViewer::getInstance();
         if(NULL != poViewer)
         {
            (poViewer->m_oLock).s16Lock();
            oMLClientCapabilitiesMsg.rClientCapabilities = m_rClientCapabilities;
            (poViewer->m_oLock).vUnlock();
         }
         poMsgQInterface->bWriteMsgToQ(&oMLClientCapabilitiesMsg, sizeof(oMLClientCapabilitiesMsg));
      }

      if(NULL!= poMsgQInterface)
      {
         MLOrientationModeMsg oMLOrientationModeMsg;
         oMLOrientationModeMsg.enOrientationMode = enOrientationMode;
         poMsgQInterface->bWriteMsgToQ(&oMLOrientationModeMsg, sizeof(oMLOrientationModeMsg));
      }
   }

}

/*****************************************************************************************
** FUNCTION: tenOrientationMode enGetOrientationMode(t_U32 u32OrientationMode);
*****************************************************************************************/
tenOrientationMode spi_tclMLVncViewer::enGetOrientationMode(
   t_U32 u32OrientationMode)const
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::enGetOrientationMode()"));
   tenOrientationMode enOrientationMode;

   switch (u32OrientationMode)
   {
   case VNCDeviceStatusFeatureOrientationPortrait:
      {
         //Portrait Mode
         enOrientationMode = e8PORTRAIT_MODE;
      }
      break;
   case VNCDeviceStatusFeatureOrientationLandscape:
      {
         //Landscape Mode
         enOrientationMode = e8LANDSCAPE_MODE;
      }
      break;
   default:
      {
         //Invalid Mode
         enOrientationMode = e8INVALID_MODE;
      }
      break;
   }

   return enOrientationMode;
}

/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vPopulateClientCapabilities()
***************************************************************************************/
t_Void spi_tclMLVncViewer::vPopulateClientCapabilities(
   const VNCServerEventConfiguration *pServerEventConfiguration,
   VNCClientEventConfiguration *pClientEventConfiguration)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vPopulateClientCapabilities()"));

   if(
      ( NULL != pClientEventConfiguration )
      &&
      ( NULL != pServerEventConfiguration )
      )
   {
      //Populate the client capabilities

      // Populate pointer support - CCC-TS-010 Table11:ServerEventConfiguration
      //0th bit => pointer support
      //1st bit => touch support
      //2nd-7th - unused
      //8th-15th => pointer button support. this can be kept similar to server config, as these are nothing but pointer events
      //16th-23rd =>  supports only two touch events Set 16th bit to 1.
      //24th-31st => supports only 2 pressure levels=> Set 24th bit to 1.
      //To set only 16th & 24th bits and reset others in 16th-31 bits, AND the configuration with 0x0101FFFF(scou32TouchEventsPressureMask)
      if(VNCPointerSupportTouchEvents ==((pServerEventConfiguration->pointerSupport)& 
         (VNCPointerSupportTouchEvents)))
      {

         if(0 == m_rClientCapabilities.rKeyCapabilities.u32PointerTouchSupport)
         {
            pClientEventConfiguration->pointerSupport= 
               ((pClientEventConfiguration->pointerSupport)
               | VNCPointerSupportTouchEvents |
               VNCPointerSupportTouchCount1 |
               VNCPointerSupportTouchCount2 |
               (1 << VNCPointerSupportTouchEventPressureMaskSHIFT))& scou32TouchEventsPressureMask;

            ETG_TRACE_USR4(("[DESC]:HMI has not used SetClientCapabilities. Enable Multi touch by default"));
         }
         else
         {
            ETG_TRACE_USR4(("[PARAM]:HMI has used SetClientCapabilities. Pointer Touch Support:0x%x",
               m_rClientCapabilities.rKeyCapabilities.u32PointerTouchSupport));
            pClientEventConfiguration->pointerSupport= m_rClientCapabilities.rKeyCapabilities.u32PointerTouchSupport;
         }

      }
      else
      {
         ETG_TRACE_USR4(("[DESC]:ML Phone doesn't support Multi touch. So multi touch is not configured in the client config"));
      }

      /*Set the Client configuration key support based on the actual keys supported 
      by the HU*/
      spi_tclMLVncViewer* poViewer = spi_tclMLVncViewer::getInstance();
      if(NULL != poViewer)
      {
         (poViewer->m_oLock).s16Lock();

         pClientEventConfiguration->deviceKeySupport = 
            m_rClientCapabilities.rKeyCapabilities.u32DeviceKeySupport;


         pClientEventConfiguration->knobKeySupport
            = m_rClientCapabilities.rKeyCapabilities.u32KnobKeySupport;


         pClientEventConfiguration->miscKeySupport
            = m_rClientCapabilities.rKeyCapabilities.u32MiscKeySupport ;


         pClientEventConfiguration->multimediaKeySupport = 
            m_rClientCapabilities.rKeyCapabilities.u32MultimediaKeySupport;


         ETG_TRACE_USR4(("[PARAM]:Client Key Capabilities: DeviceKeySupport:0x%x  KnobKeySupport:0x%x  MiscKeySupport:0x%x" \
                  "MultimediaKeySupport:0x%x  PointerTouchSupport:0x%x",
                  pClientEventConfiguration->deviceKeySupport,pClientEventConfiguration->knobKeySupport,
                  pClientEventConfiguration->miscKeySupport,pClientEventConfiguration->multimediaKeySupport,
                  pClientEventConfiguration->pointerSupport));

         (poViewer->m_oLock).vUnlock();
      }
   }

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vPopulateServerCapabilities()
***************************************************************************/
t_Void spi_tclMLVncViewer::vPopulateServerCapabilities(
   const VNCServerEventConfiguration *pServerEventConfiguration)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vPopulateServerCapabilities()"));

   if( NULL != pServerEventConfiguration)
   {
      //Populate the client capabilities
      m_oServerCapabilities.rKeyCapabilities.u32DeviceKeySupport
         = pServerEventConfiguration->deviceKeySupport;

      m_oServerCapabilities.rKeyCapabilities.u32KnobKeySupport
         = pServerEventConfiguration->knobKeySupport;

      m_oServerCapabilities.rKeyCapabilities.u32MiscKeySupport
         = pServerEventConfiguration->miscKeySupport;

      m_oServerCapabilities.rKeyCapabilities.u32MultimediaKeySupport
         = pServerEventConfiguration->multimediaKeySupport;

      m_oServerCapabilities.rKeyCapabilities.u32PointerTouchSupport
         = pServerEventConfiguration->pointerSupport;

      ETG_TRACE_USR4(("[PARAM]:Server Key Capabilities: DeviceKeySupport:0x%x  KnobKeySupport:0x%x  MiscKeySupport:0x%x " \
         "MultimediaKeySupport:0x%x  PointerTouchSupport:0x%x",
         m_oServerCapabilities.rKeyCapabilities.u32DeviceKeySupport,m_oServerCapabilities.rKeyCapabilities.u32KnobKeySupport,
         m_oServerCapabilities.rKeyCapabilities.u32MiscKeySupport,m_oServerCapabilities.rKeyCapabilities.u32MultimediaKeySupport,
         m_oServerCapabilities.rKeyCapabilities.u32PointerTouchSupport));
   }

}

/***************************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vServerDisplayConfigCallback()
****************************************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vServerDisplayConfigCallback(VNCViewer *pViewer,
                                                                t_Void *pContext,
                                                                const VNCServerDisplayConfiguration *pServerDisplayConfiguration,
                                                                size_t serverDisplayConfigurationSize,
                                                                VNCClientDisplayConfiguration *pClientDisplayConfiguration,
                                                                size_t clientDisplayConfigurationSize)
{
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(pViewer);
   SPI_INTENTIONALLY_UNUSED(serverDisplayConfigurationSize);
   SPI_INTENTIONALLY_UNUSED(clientDisplayConfigurationSize);

   ETG_TRACE_USR1(("spi_tclMLVncViewer::vServerDisplayConfigCallback()"));

   if(NULL != pServerDisplayConfiguration)
   {

      ETG_TRACE_USR4(("[PARAM]:Server Display config: MajorVer-0x%x Minorver-0x%x FrameBufferConfig-0x%x",
         pServerDisplayConfiguration->serverMajorVersion,pServerDisplayConfiguration->serverMinorVersion,
         pServerDisplayConfiguration->framebufferConfiguration));

      ETG_TRACE_USR4(("[PARAM]:Server Display config: Relative PixelWidth-0x%x PixelHeight-0x%x PixelFormatSuppport-0x%x",
         pServerDisplayConfiguration->relativePixelWidth,pServerDisplayConfiguration->relativePixelHeight,
         pServerDisplayConfiguration->pixelFormatSupport));

      if( (pServerDisplayConfiguration->framebufferConfiguration & VNCFramebufferConfigurationServerSideOrientationSwitch)
         == VNCFramebufferConfigurationServerSideOrientationSwitch )
      {
         //server supports orientation switch, so inform server that the client is intend to use this feature
         //set the flag in client display configuration
         pClientDisplayConfiguration->framebufferConfiguration |= VNCFramebufferConfigurationServerSideOrientationSwitch ;
      }
   }//if(NULL != pServerDisplayConfiguration)

   if(NULL != pClientDisplayConfiguration)
   {

     // ML Client Major version & minor version must not be higher than the ML version supported by the ML Server
     // Ref-VNC based Display and Control -> CCC-TS-010 => section: Display configuration message
     pClientDisplayConfiguration->clientMajorVersion = scou32ML_Major_Version_1;

     //@todo - ML Major version and ML Minor version needs to be set based on OEM.
     //Read from config and store it
     if(NULL != pServerDisplayConfiguration)
     {
        pClientDisplayConfiguration->clientMinorVersion = (scou32ML_Minor_Version_1 <= (pServerDisplayConfiguration->serverMinorVersion))?
                     scou32ML_Minor_Version_1:scou32ML_Minor_Version_0;
     }//if(NULL != pServerDisplayConfiguration)


     ETG_TRACE_USR4(("[PARAM]:Client Display config: MajorVer-0x%x Minorver-0x%x FrameBufferConfig-0x%x",
        pClientDisplayConfiguration->clientMajorVersion,pClientDisplayConfiguration->clientMinorVersion,
        pClientDisplayConfiguration->framebufferConfiguration));

      //Set the User distance to default values as per the CCC spec - 900 mm
      pClientDisplayConfiguration->clientDistanceFromUserMillimeters = scou16DefaultUserDistanceMM;

      spi_tclMLVncViewer* poViewer = spi_tclMLVncViewer::getInstance();
      if(NULL != poViewer)
      {
         ////Set  width and height in mm
         pClientDisplayConfiguration->clientDisplayWidthMillimeters = poViewer->m_u32ScreenWidth_Mm;
         pClientDisplayConfiguration->clientDisplayHeightMillimeters = poViewer->m_u32ScreenHeight_Mm;
         //Set  width and height in pixels
         pClientDisplayConfiguration->clientDisplayHeightPixels = poViewer->m_u32ScreenHeight;
         pClientDisplayConfiguration->clientDisplayWidthPixels = poViewer->m_u32ScreenWidth;
      }

      ETG_TRACE_USR4(("[PARAM]:Client Display config: DispWidthpixels-0x%x DispPixelpixels-0x%x DispWidthmm-0x%x HeightMM-0x%x",
         pClientDisplayConfiguration->clientDisplayWidthPixels,pClientDisplayConfiguration->clientDisplayHeightPixels,
         pClientDisplayConfiguration->clientDisplayWidthMillimeters,pClientDisplayConfiguration->clientDisplayHeightMillimeters));

      ETG_TRACE_USR4(("[PARAM]:Client Display config:pixelFormatSupport- 0x%x",
         pClientDisplayConfiguration->pixelFormatSupport));

      ETG_TRACE_USR4(("[PARAM]:Client Display config:resizeFactors- 0x%x",
         pClientDisplayConfiguration->resizeFactors));
   }// if(NULL != pClientDisplayConfiguration)

   spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if( (NULL!= poMsgQInterface) && (NULL!= spi_tclMLVncViewer::getInstance()))
   {
      MLDisplayCapabilitiesMsg oMLDisplayCapabilitiesMsg;

      //Populate the display capabilities
      spi_tclMLVncViewer::getInstance()->vPopulateDisplayCapabilities(
         oMLDisplayCapabilitiesMsg.rDispCapabilities,pServerDisplayConfiguration);

      poMsgQInterface->bWriteMsgToQ(&oMLDisplayCapabilitiesMsg, sizeof(oMLDisplayCapabilitiesMsg));
   }//if(NULL!= poMsgQInterface) && (NULL!= spi_tclMLVncViewer::getInstance()))

}

/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vPopulateServerDispConfig()
***************************************************************************************/
t_Void spi_tclMLVncViewer::vPopulateDisplayCapabilities(
   trDisplayCapabilities& rfrDispCapabilities,
   const VNCServerDisplayConfiguration* pServerDisplayConfiguration)const
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vPopulateDisplayCapabilities()"));

   if (NULL != pServerDisplayConfiguration)
   {
      //Populate the FrameBuffer Configuration
      rfrDispCapabilities.u16FrameBufferConfiguration=
         pServerDisplayConfiguration->framebufferConfiguration;

      //Populate the Pixel format
      rfrDispCapabilities.u32PixelFormat =
         pServerDisplayConfiguration->pixelFormatSupport;
   }

}

/*********************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vDeviceStatusCallback()
**********************************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vDeviceStatusCallback(VNCViewer *pViewer,t_Void *pContext,
   const VNCDeviceStatus *pDeviceStatus,
   size_t deviceStatusSize)
{
   SPI_INTENTIONALLY_UNUSED(deviceStatusSize);
   SPI_INTENTIONALLY_UNUSED(pViewer);
   SPI_INTENTIONALLY_UNUSED(pContext);
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vDeviceStatusCallback()"));

   spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if((NULL != pDeviceStatus)&&(NULL!= poMsgQInterface))
   {
      //Retain the current value for Drive Mode and Night Mode.Reset other values
      m_rDeviceStatus.features &= (VNCDeviceStatusFeatureDriverDistractionAvoidanceMASK | VNCDeviceStatusFeatureNightModeMASK);
      VNCDeviceStatus rDeviceStatus = *pDeviceStatus;
      ETG_TRACE_USR4(("[PARAM]:vDeviceStatusCallback Retain current value -0x%x",m_rDeviceStatus.features));

      //Reset the Drive and Night Mode values returned by the device. Retain Other values
      rDeviceStatus.features &= (~(VNCDeviceStatusFeatureDriverDistractionAvoidanceMASK | VNCDeviceStatusFeatureNightModeMASK));
      m_rDeviceStatus.features |= rDeviceStatus.features;
      ETG_TRACE_USR4(("[PARAM]:vDeviceStatusCallback Reset the Drive and Night Mode values returned by the device -0x%x",m_rDeviceStatus.features));

      MLDeviceStatusMsg oMLDeviceStatus;
      oMLDeviceStatus.u32DeviceStatusFeature = m_rDeviceStatus.features;
      poMsgQInterface->bWriteMsgToQ(&oMLDeviceStatus, sizeof(oMLDeviceStatus));
      //if(NULL!= poMsgQInterface)

      ETG_TRACE_USR4(("[PARAM]:vDeviceStatusCallback:Device Status Message Value:0x%x",m_rDeviceStatus.features));

      //Check the driver distraction avoidance
      MLDriverDistAvoidanceMsg oDriverDistAvoidanceMsg;
      //Check DriverDistractionAvoidance
      if( VNCDeviceStatusFeatureDriverDistractionAvoidanceEnabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureDriverDistractionAvoidanceMASK))
      {
         oDriverDistAvoidanceMsg.bDriverDistAvoided = true ;
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:DriverDistractionAvoidance Enabled"));
      }//if( (m_rDeviceStatus.features & VNCDeviceStatusFeature
      else if( VNCDeviceStatusFeatureDriverDistractionAvoidanceDisabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureDriverDistractionAvoidanceMASK))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:DriverDistractionAvoidance Disabled"));
      }
      else
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:DriverDistractionAvoidance enable/disable request ignored by the device"));
      }
      poMsgQInterface->bWriteMsgToQ(&oDriverDistAvoidanceMsg, sizeof(oDriverDistAvoidanceMsg));

      //Check the Night mode
      MLDayNightModeInfo oMLDayNightMode;
      if( VNCDeviceStatusFeatureNightModeEnabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureNightModeMASK))
      {
         oMLDayNightMode.bNightModeEnabled =  true ;
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:NightMode Enabled"));
      }//if( (m_rDeviceStatus.features & VNCDeviceStatusFeature
      else if( VNCDeviceStatusFeatureNightModeDisabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureNightModeMASK))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:NightMode Disabled"));
      }
      else
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:NightMode enable/disable request ignored by the device"));
      }
      poMsgQInterface->bWriteMsgToQ(&oMLDayNightMode, sizeof(oMLDayNightMode));

      //Check the Orientation Mode
      MLOrientationModeMsg oMLOrientationModeMsg;
      spi_tclMLVncViewer* pVncViewer = spi_tclMLVncViewer::getInstance();
      if(NULL!= pVncViewer)
      {
         oMLOrientationModeMsg.enOrientationMode = pVncViewer->enGetOrientationMode(
            m_rDeviceStatus.features & VNCDeviceStatusFeatureOrientationMASK);
      }
      poMsgQInterface->bWriteMsgToQ(&oMLOrientationModeMsg, sizeof(oMLOrientationModeMsg));

      //Check whether the MicroPhone is enabled/disabled
      MLMicroPhoneInputInfo oMLMicroPhoneInfoMsg;
      if( VNCDeviceStatusFeatureVoiceInputReroutingEnabled == 
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureVoiceInputReroutingMASK ))
      {
         oMLMicroPhoneInfoMsg.bMicroPhoneInputEnabled = true ;
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:MicroPhone Enabled"));
      }//if( (m_rDeviceStatus.features & VNCDeviceStatusFeature
      else if( VNCDeviceStatusFeatureVoiceInputReroutingDisabled == 
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureVoiceInputReroutingMASK ))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:MicroPhone Disabled"));
      }
      else
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:MicroPhone enable/disable request ignored by the device"));
      }
      poMsgQInterface->bWriteMsgToQ(&oMLMicroPhoneInfoMsg, sizeof(oMLMicroPhoneInfoMsg));

      //Check whether the Voice input in is enabled/disabled
      MLVoiceInputInfo oMLVoiceInputInfoMsg;
      if(VNCDeviceStatusFeatureVoiceInputEnabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureVoiceInputMASK ))
      {
         oMLVoiceInputInfoMsg.bVoiceInputEnabled = true ;
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:Voice input Enabled"));
      }//if( (m_rDeviceStatus.features & VNCDeviceStatusFeature
      else if(VNCDeviceStatusFeatureVoiceInputDisabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureVoiceInputMASK ))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:Voice input Disabled"));
      }
      else
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:Voice input enable/disable request ignored by the device"));
      }
      poMsgQInterface->bWriteMsgToQ(&oMLVoiceInputInfoMsg, sizeof(oMLVoiceInputInfoMsg));

      //Check KeyLock
      if( VNCDeviceStatusFeatureKeyLockEnabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureKeyLockMASK ))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:KeyLock Enabled"));
      }//if( (m_rDeviceStatus.features & VNCDeviceStatusFeature
      else if( VNCDeviceStatusFeatureKeyLockDisabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureKeyLockMASK ))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:KeyLock Disabled"));
      }
      else
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:KeyLock enable/disable request ignored by the device"));
      }
      //Check Device Lock
      if( VNCDeviceStatusFeatureDeviceLockEnabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureDeviceLockMASK ))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:DeviceLock Enabled"));
      }//if( (m_rDeviceStatus.features & VNCDeviceStatusFe
      else if( VNCDeviceStatusFeatureDeviceLockDisabled ==
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureDeviceLockMASK ))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:DeviceLock Disabled"));
      }
      else
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:DeviceLock enable/disable request ignored by the device"));
      }
      //!Check Screen Saver Mode
      if( VNCDeviceStatusFeatureScreenSaverEnabled == 
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureScreenSaverMASK ))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:Screen Saver Enabled"));
      }//if( (m_rDeviceStatus.features & VNCDeviceStatusFe
      else if(VNCDeviceStatusFeatureScreenSaverDisabled == 
         (m_rDeviceStatus.features & VNCDeviceStatusFeatureScreenSaverMASK ))
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:Screen Saver Disabled"));
      }
      else
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:Screen Saver enable/disable request ignored by the device"));
      }

   }//if((NULL != pDeviceStatus)&&(NULL!= poMsgQInterface))
   else
   {
      ETG_TRACE_ERR(("[ERR]:vDeviceStatusCallback:: pDeviceStatus or MsgQInterface is NULL"));
   }
}

/****************************************************************************************
** FUNCTION: t_Bool spi_tclMLVncViewer::bSendDeviceStatusRequest(t_U8 u8DeviceStatusRequest)
***************************************************************************************/
t_Bool spi_tclMLVncViewer::bSendDeviceStatusRequest(
   t_U32 u32DeviceStatusRequest)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer:: bSendDeviceStatusRequest() "));
   t_Bool bRetVal = false;

   VNCDeviceStatusRequest oDeviceStatusRequest;
   oDeviceStatusRequest.features = u32DeviceStatusRequest;

   //Send the Device Status Request to server
   if ((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   {
      VNCViewerError s32ViewerError =
         m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &oDeviceStatusRequest, sizeof(VNCDeviceStatusRequest));

      bRetVal = (VNCViewerErrorNone == s32ViewerError);
      if(false == bRetVal)
      {
         ETG_TRACE_ERR(("[ERR]: Error in sending Device Status Request to Phone"));
      }//if(false == bRetVal)
   }//if ((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   return bRetVal;
}

/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vSubscribeforViewer()
***************************************************************************************/
t_Void spi_tclMLVncViewer::vSubscribeforViewer()const
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vSubscribeforViewer()"));
   spi_tclMLVncMsgQInterface *poMsgQInterface =
      spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLViewerSubscriResultMsg oMLViewerSubscriResultMsg;
      poMsgQInterface->bWriteMsgToQ(&oMLViewerSubscriResultMsg,
         sizeof(oMLViewerSubscriResultMsg));
   } // if (NULL != poMsgQInterface)
}

/****************************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vAddLicense()
***************************************************************************************/
t_Bool spi_tclMLVncViewer::bAddLicense()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::bAddLicense()"));
   t_Bool bRetVal = false , bReadRes=false;

   t_String szLicensePath,szLicenseFile;

   spi_tclVncSettings *poVncSettings = spi_tclVncSettings::getInstance();

   if(NULL != poVncSettings)
   {
      poVncSettings->vGetLicenseFilePath(szLicensePath);
   }

   // Read the license file
   spi::io::FileHandler oLicenseFile(szLicensePath.c_str(),
      spi::io::SPI_EN_RDONLY);

   if (true == oLicenseFile.bIsValid())
   {
      t_S32 s32LicenseFileSize = oLicenseFile.s32GetSize();

      if (0 < s32LicenseFileSize)
      {
         t_Char czLicenseFile[s32LicenseFileSize + 1];
         bReadRes = oLicenseFile.bFRead(czLicenseFile, s32LicenseFileSize);

         if (true == bReadRes)
         {
            czLicenseFile[s32LicenseFileSize] = '\0';
            szLicenseFile.assign(czLicenseFile);

         }//if (true == bReadRes)

      }//  if (0 <= s32LicenseFileS

   }// if (true == oLicenseFile.bIsValid
   size_t serialNumberSize = 0;

   if ((true == bReadRes) && (NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      // add license
      VNCViewerError enLicenseError =
         m_pViewerSDK->vncViewerAddLicense(m_pVNCViewer,
         szLicenseFile.c_str(),
         NULL,
         serialNumberSize);

      ETG_TRACE_USR1(("[PARAM]:bAddLicense: %d ", enLicenseError));
      bRetVal = (VNCViewerErrorNone == enLicenseError);
   }

   ETG_TRACE_USR1(("[DESC]:Viewer License added successfully: %d",ETG_ENUM(BOOL,bRetVal)));

   return bRetVal;
}

/**********************************************************************************
** FUNCTION: static t_S32 VNCCALL vErrorCallback(VNCViewer *pViewervoid *pContext..
**********************************************************************************/
t_S32 VNCCALL spi_tclMLVncViewer::vErrorCallback(VNCViewer *pViewer,
                                                 t_Void *pContext,
                                                 VNCViewerError error,
                                                 t_Void *pReservedForFutureUse)
{
   SPI_INTENTIONALLY_UNUSED(pViewer);
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(pReservedForFutureUse);
   //Added this callback for debugging purpose
   ETG_TRACE_USR1(("spi_tclMLVncViewer::VNCViewerErrorCallback:Error: 0x%x ",error));

   MLViewerErrorMsg oViewerErrorMsg;
   oViewerErrorMsg.u32ViewerError = error;

   spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if(NULL!= poMsgQInterface)
   {
      poMsgQInterface->bWriteMsgToQ(&oViewerErrorMsg, sizeof(oViewerErrorMsg));
   }//if(NULL!= poMsgQInterface)
   //This method should return some value.to SDK.
   return VNCViewerErrorNone;
}

/**************************************************************************/
/*********************CRCB*************************************************/
/**************************************************************************/

/***************************************************************************
** FUNCTION: MLDiscoveryError spi_tclMLVncViewer::
**                  bSendFramebufferBlockingNotification( const t_U32 cou32AppID..)
***************************************************************************/
t_Bool spi_tclMLVncViewer::bSendFramebufferBlockingNotification(
   const t_U32 cou32AppID, const trMLRectangle &corfoMLRectangle,
   tenMLFramebufferBlockReason enMLFrameBufferBlockReason,
   t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);

   ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"[DESC]:Sending FramebufferBlockingNotification with Reason-%d ",
      ETG_ENUM(FB_BLOCK_REASON,enMLFrameBufferBlockReason)));

   VNCViewerError enSendStatus = VNCViewerErrorFailed;
   t_Bool bSendStatus = false;

   if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      ////!Rectangular area of screen that needs to be blocked
      VNCRectangle oVNCRectangle;
      vPopulateVNCRectangle(corfoMLRectangle, oVNCRectangle);
      VNCFramebufferBlockingNotification enVNCBlockNotification;

      enVNCBlockNotification.blockReason
         = static_cast<VNCFramebufferBlockReason> (enMLFrameBufferBlockReason);
      enVNCBlockNotification.applicationUniqueId = cou32AppID;

      //! Send blocking request to MLServer through RealVNC SDK
      enSendStatus
         = m_pViewerSDK->vncViewerSendFramebufferBlockingNotification(
         m_pVNCViewer, &oVNCRectangle, &enVNCBlockNotification,
         sizeof(enVNCBlockNotification));

      //!check the return value
      bSendStatus = (VNCViewerErrorNone == enSendStatus);
      if (false == bSendStatus)
      {
         ETG_TRACE_ERR(("[PARAM]: bSendFramebufferBlockingNotification :  error %d"
            , ETG_ENUM(VNCViewerError, enSendStatus)));
      }
   }
   return bSendStatus;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncViewer::
**                        bSendAudioBlockingNotification( const t_U32 cou32AppID..)
***************************************************************************/
t_Bool spi_tclMLVncViewer::bSendAudioBlockingNotification(
   const t_U32 cou32AppID, tenMLAudioBlockReason enMLAudioBlockReason,
   t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(
      ("spi_tclMLVncViewer::bSendAudioBlockingNotification Entered "));
   VNCViewerError enSendStatus = VNCViewerErrorFailed;
   t_Bool bSendStatus = false;

   if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      VNCAudioBlockingNotification enVNCAudioNotification;
      enVNCAudioNotification.applicationUniqueId = cou32AppID;

      ETG_TRACE_USR4(("[PARAM]:bSendAudioBlockingNotification: Framebuffer block reason = %d  ", ETG_ENUM(
         tenMLAudioBlockReason, enMLAudioBlockReason)));
      enVNCAudioNotification.blockReason
         = static_cast<VNCAudioBlockReason> (enMLAudioBlockReason);

      //! Send blocking request to MLServer through RealVNC SDK
      enSendStatus = m_pViewerSDK->vncViewerSendAudioBlockingNotification(
         m_pVNCViewer, &enVNCAudioNotification,
         sizeof(enVNCAudioNotification));

      //!check the return value
      bSendStatus = (VNCViewerErrorNone == enSendStatus);
      if (false == bSendStatus)
      {
         ETG_TRACE_ERR(("[ERR]:bSendFramebufferBlockingNotification :  error %d", ETG_ENUM(
            VNCViewerError, enSendStatus)));
      }
   }

   return bSendStatus;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncViewer::vRequestSetDriverDistractionAvoidance()
***************************************************************************/
t_Void spi_tclMLVncViewer::vRequestSetDriverDistractionAvoidance(
   t_Bool bDistractionAvoidance, t_U32 u32DeviceHandle)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vRequestSetDriverDistractionAvoidance:Enable-%d ",
      ETG_ENUM(BOOL,bDistractionAvoidance)));

   if ((NULL != m_pVNCViewer) && (NULL!= m_pViewerSDK))
   {
      ETG_TRACE_USR4(("[PARAM]:vRequestSetDriverDistractionAvoidance: Device Status Message Val:0x%x",m_rDeviceStatus.features));
      //VNCDeviceStatus rDeviceStatus = m_rDeviceStatus;

      //! Reset the current setting for driver distraction avoidance
      m_rDeviceStatus.features
         &= (~VNCDeviceStatusFeatureDriverDistractionAvoidanceMASK);

      if (true == bDistractionAvoidance)
      {
         // Set the feature to Enabled
         m_rDeviceStatus.features |= VNCDeviceStatusFeatureDriverDistractionAvoidanceEnabled;
      }
      else
      {
         // Set the feature to Disabled
         m_rDeviceStatus.features |= VNCDeviceStatusFeatureDriverDistractionAvoidanceDisabled;
      }

      ETG_TRACE_USR4(("[PARAM]:vRequestSetDriverDistractionAvoidance: Device Status Message Val:0x%x",m_rDeviceStatus.features));

      if(VNCViewerErrorNone != m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &m_rDeviceStatus, sizeof(m_rDeviceStatus)))
      {
         ETG_TRACE_ERR(("[ERR]:spi_tclMLVncViewer::vRequestSetDriverDistractionAvoidance: Error in sending request"));
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDevi
   }
}

/*****************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vSetDayNightMode()
*****************************************************************************/
t_Void spi_tclMLVncViewer::vSetDayNightMode(const t_Bool cobEnableNightMode)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vSetDayNightMode: Enable Night Mode: %d  ",
      ETG_ENUM(BOOL,cobEnableNightMode)));

   if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      ETG_TRACE_USR4(("[PARAM]:vSetDayNightMode: Device Status Message Val:0x%x",m_rDeviceStatus.features));
      //VNCDeviceStatus rDeviceStatus = m_rDeviceStatus;

      //! Reset the current setting for Night Mode
      m_rDeviceStatus.features &= (~VNCDeviceStatusFeatureNightModeMASK);

      //Set the requested Night mode ( Enabled/Disabled)
      m_rDeviceStatus.features |= (cobEnableNightMode)?VNCDeviceStatusFeatureNightModeEnabled:
         VNCDeviceStatusFeatureNightModeDisabled;

      ETG_TRACE_USR4(("[PARAM]:vSetDayNightMode: Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //Send the device status request to SDK
      if(VNCViewerErrorNone != m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &m_rDeviceStatus, sizeof(m_rDeviceStatus)))
      {
         ETG_TRACE_ERR(("[ERR]:spi_tclMLVncViewer::vSetDayNightMode() : Error in sending request"));
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDevi
   } //if ( && (NULL != m_pVNCViewer) && 
}


/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vEnableKeyLock()
***************************************************************************/
t_Void spi_tclMLVncViewer::vEnableKeyLock(const t_Bool cobEnable)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vEnableKeyLock: Enable-%d",ETG_ENUM(BOOL,cobEnable)));
  if ( (NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      //VNCDeviceStatus rDeviceStatus = m_rDeviceStatus;
      ETG_TRACE_USR4(("[PARAM]: vEnableKeyLock:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //! Reset the current setting for Key lock
      m_rDeviceStatus.features &= (~VNCDeviceStatusFeatureKeyLockMASK );

      //Set the requested Key-Lock ( Enabled/Disabled)
      m_rDeviceStatus.features |= (cobEnable)?VNCDeviceStatusFeatureKeyLockEnabled:
         VNCDeviceStatusFeatureKeyLockDisabled;

      ETG_TRACE_USR4(("[PARAM]: vEnableKeyLock:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //Send the device status request to SDK
      if(VNCViewerErrorNone != m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &m_rDeviceStatus, sizeof(m_rDeviceStatus)))
      {
         ETG_TRACE_ERR(("[ERR]: vEnableKeyLock: Error in sending request"));
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDevi   
   } //if ((NULL != m_pVNCViewer) && 

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vEnableDeviceLock()
***************************************************************************/
t_Void spi_tclMLVncViewer::vEnableDeviceLock(const t_Bool cobEnable)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vEnableDeviceLock:Enable-%d",ETG_ENUM(BOOL,cobEnable)));

  if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      //VNCDeviceStatus rDeviceStatus = m_rDeviceStatus;
      ETG_TRACE_USR4(("[PARAM]:vEnableDeviceLock:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //! Reset the current setting for Device Lock
      m_rDeviceStatus.features &= (~VNCDeviceStatusFeatureDeviceLockMASK);

      //Set the requested Device Lock ( Enabled/Disabled)
      m_rDeviceStatus.features |= (cobEnable)?VNCDeviceStatusFeatureDeviceLockEnabled:
         VNCDeviceStatusFeatureDeviceLockDisabled;

      ETG_TRACE_USR4(("[PARAM]:vEnableDeviceLock:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //Send the device status request to SDK
      if(VNCViewerErrorNone != m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &m_rDeviceStatus, sizeof(m_rDeviceStatus)))
      {
         ETG_TRACE_ERR(("[ERR]:vEnableDeviceLoc: Error in sending request"));
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDevi   
   } //if ((NULL != m_pVNCViewer) && 
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncViewer::bSetOrientation()
***************************************************************************/
t_Bool spi_tclMLVncViewer::bSetOrientation(const tenOrientationMode coenOrientationMode)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer:bSetOrientation:Enable-%d",coenOrientationMode));
   t_Bool bRet = false;

  if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      //VNCDeviceStatus rDeviceStatus = m_rDeviceStatus;

      ETG_TRACE_USR4(("[PARAM]:bSetOrientation:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //! Reset the current setting for Device Lock
      m_rDeviceStatus.features &= (~VNCDeviceStatusFeatureOrientationMASK);
      switch(coenOrientationMode)
      {
      case e8PORTRAIT_MODE:
         {
            m_rDeviceStatus.features |= VNCDeviceStatusFeatureOrientationPortrait;
         }
         break;
      case e8LANDSCAPE_MODE:
         {
            m_rDeviceStatus.features |= VNCDeviceStatusFeatureOrientationLandscape;
         }
         break;
      default:
         {
            m_rDeviceStatus.features |= VNCDeviceStatusFeatureOrientationIgnored;
         }
         break;
      }//switch(coenOrientationMode)

      ETG_TRACE_USR4(("[PARAM]:bSetOrientation:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //Send the device status request to SDK
      if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &m_rDeviceStatus, sizeof(m_rDeviceStatus)))
      {
         bRet = true;
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDevi   
   } //if ((NULL != m_pVNCViewer) && 
   return bRet;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vEnableScreenSaver()
***************************************************************************/
t_Void spi_tclMLVncViewer::vEnableScreenSaver(const t_Bool cobEnable)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vEnableScreenSaver:Enable-%d",ETG_ENUM(BOOL,cobEnable)));

   if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      //VNCDeviceStatus rDeviceStatus = m_rDeviceStatus;
      ETG_TRACE_USR4(("[PARAM]:vEnableScreenSaver:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //! Reset the current setting for Device Lock
      m_rDeviceStatus.features &= (~VNCDeviceStatusFeatureScreenSaverMASK);

      //Set the requested Screen saver mode ( Enabled/Disabled)
      m_rDeviceStatus.features |= (cobEnable)?VNCDeviceStatusFeatureScreenSaverEnabled:
         VNCDeviceStatusFeatureScreenSaverDisabled;

      ETG_TRACE_USR4(("[PARAM]:vEnableScreenSaver:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //Send the device status request to SDK
      if(VNCViewerErrorNone != m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &m_rDeviceStatus, sizeof(m_rDeviceStatus)))
      {
         ETG_TRACE_ERR(("[ERR]vEnableScreenSaver: Error in sending request"));
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDevi   
   } //if ((NULL != m_pVNCViewer) && 
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vEnableVoiceRecognition()
***************************************************************************/
t_Void spi_tclMLVncViewer::vEnableVoiceRecognition(const t_Bool cobEnable)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vEnableVoiceRecognition:Enable-%d",ETG_ENUM(BOOL,cobEnable)));

   if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      //VNCDeviceStatus rDeviceStatus = m_rDeviceStatus;
      ETG_TRACE_USR4(("[PARAM]:vEnableVoiceRecognition:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //! Reset the current setting for Device Lock
      m_rDeviceStatus.features &= (~VNCDeviceStatusFeatureVoiceInputMASK);

      //Set the request: Voice Recognition ( Enabled/Disabled)
      m_rDeviceStatus.features |= (cobEnable)?VNCDeviceStatusFeatureVoiceInputEnabled:
         VNCDeviceStatusFeatureVoiceInputDisabled;

      ETG_TRACE_USR4(("[PARAM]:vEnableVoiceRecognition:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //Send the device status request to SDK
      if(VNCViewerErrorNone != m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &m_rDeviceStatus, sizeof(m_rDeviceStatus)))
      {
         ETG_TRACE_ERR(("[ERR]:vEnableVoiceRecognition(): Error in sending request"));
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDevi   
   } //if ((NULL != m_pVNCViewer) && 
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vEnableMicroPhoneInput()
***************************************************************************/
t_Void spi_tclMLVncViewer::vEnableMicroPhoneInput(const t_Bool cobEnable)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vEnableMicroPhoneInput:Enable-%d",ETG_ENUM(BOOL,cobEnable)));

   if ((NULL != m_pVNCViewer) && (NULL != m_pViewerSDK))
   {
      //VNCDeviceStatus rDeviceStatus = m_rDeviceStatus;
      ETG_TRACE_USR4(("[PARAM]:vEnableMicroPhoneInput:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //! Reset the current setting for Device Lock
      m_rDeviceStatus.features &= (~VNCDeviceStatusFeatureMicrophoneInputMASK);

      //Set the request: MicrophoneIn ( Enabled/Disabled)
      m_rDeviceStatus.features |= (cobEnable)?VNCDeviceStatusFeatureMicrophoneInputEnabled:
         VNCDeviceStatusFeatureMicrophoneInputDisabled;

      ETG_TRACE_USR4(("[PARAM]:vEnableMicroPhoneInput:Device Status Message Val:0x%x",m_rDeviceStatus.features));

      //Send the device status request to SDK
      if(VNCViewerErrorNone != m_pViewerSDK->vncViewerSendDeviceStatusRequest(m_pVNCViewer,
         &m_rDeviceStatus, sizeof(m_rDeviceStatus)))
      {
         ETG_TRACE_ERR(("[ERR]:vEnableMicroPhoneInput(): Error in sending request"));
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncViewerSendDevi   
   } //if ((NULL != m_pVNCViewer) && 
}

/*****************************************************************************
** FUNCTION: mlink_dlt_context* spi_tclMLVncViewer::pGetDLTContext()
*****************************************************************************/
mlink_dlt_context* spi_tclMLVncViewer::pGetDLTContext()
{

   //Debugging purpose
   if(NULL == ml_adapter_ctx.dlt_ctx)
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclMLVncViewer::pGetDLTContext: ml_adapter_ctx.dlt_ctx - 0x%x",
         ml_adapter_ctx.dlt_ctx));
   }//if(NULL == ml_adapter_ctx.dlt_ctx)

   return ml_adapter_ctx.dlt_ctx;
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

//!Internal Supporting functions
/***************************************************************************
** FUNCTION: vPopulateVNCRectangle(const MLRectangle &corfrMLRectangle,...);
***************************************************************************/
t_Void spi_tclMLVncViewer::vPopulateVNCRectangle(
   const trMLRectangle &corfrMLRectangle, VNCRectangle &rfrVNCRectangle)const
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vPopulateVNCRectangle "));

   rfrVNCRectangle.bottomRight.x = corfrMLRectangle.rBottomRight.u16x;
   rfrVNCRectangle.bottomRight.y = corfrMLRectangle.rBottomRight.u16y;
   rfrVNCRectangle.topLeft.x = corfrMLRectangle.rTopLeft.u16x;
   rfrVNCRectangle.topLeft.y = corfrMLRectangle.rTopLeft.u16y;
}


/*****************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vPopulateTouchEnableFlag(t_U16...)
*****************************************************************************/
t_Void spi_tclMLVncViewer::vPopulateTouchEnableFlag(t_U16 u16TouchEnableFlag)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vPopulateTouchEnableFlag()"));

   m_u16TouchEnableFlag= u16TouchEnableFlag;
}

/**********************************************************************************
** FUNCTION:static t_Void VNCCALL vViewerContextInfoCallback(VNCViewer *pViewer...
**********************************************************************************/
t_Void spi_tclMLVncViewer::vViewerContextInfoCallback(VNCViewer *pViewer, 
                                                      t_Void *pContext, 
                                                      const VNCRectangle *pRectangle, 
                                                      const VNCContextInformation *pContextInformation, 
                                                      size_t contextInformationSize)
{
   SPI_INTENTIONALLY_UNUSED(contextInformationSize);
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(pViewer);
   if((NULL != pRectangle)&&(NULL != pContextInformation))
   {

      trAppContextInfo rAppCntxtInfo;       
      rAppCntxtInfo.u32AppUUID = pContextInformation->applicationUniqueId;
      rAppCntxtInfo.u32AppCategory = pContextInformation->applicationCategory;         
      rAppCntxtInfo.u32AppCategoryTrustLevel = pContextInformation->applicationCategoryTrustLevel;         
      rAppCntxtInfo.u32AppContentCategroy = pContextInformation->contentCategory;         
      rAppCntxtInfo.u32AppContentCategoryTrustLevel = pContextInformation->contentCategoryTrustLevel;         
      rAppCntxtInfo.u32ContentRulesFollowed = pContextInformation->contentRulesFollowed;

      ETG_TRACE_USR4(("[PARAM]: App Context Info: AppID-0x%x AppCat-0x%x App TrustLevel-0x%x",
          rAppCntxtInfo.u32AppUUID,rAppCntxtInfo.u32AppCategory,rAppCntxtInfo.u32AppCategoryTrustLevel));
       ETG_TRACE_USR4(("[PARAM]: App Context Info: ContentCat-0x%x TrustLevel-0x%x ContentRulesFollowed-0x%x",
          rAppCntxtInfo.u32AppContentCategroy,rAppCntxtInfo.u32AppContentCategoryTrustLevel,
          rAppCntxtInfo.u32ContentRulesFollowed));

       rAppCntxtInfo.rMLRect.rTopLeft.u16x = pRectangle->topLeft.x;
       rAppCntxtInfo.rMLRect.rTopLeft.u16y = pRectangle->topLeft.y;
       rAppCntxtInfo.rMLRect.rBottomRight.u16x = pRectangle->bottomRight.x;
       rAppCntxtInfo.rMLRect.rBottomRight.u16y = pRectangle->bottomRight.y;

      //SUZUKI-24554:This part is moved from ML AppMngr to here, to avoid delay in sending frame buffer blocking notification
      //to Phone, when an invalid frame buffer update is received.
      //Due to the CmdQThreader, there is a delay in sending blocking notification and caused
      //CTS frame buffer blocking test cases failure. So, it is moved to here.
      if(e32APPUNKNOWN == static_cast<tenAppCategory>(pContextInformation->applicationCategory))
      {
         spi_tclMLVncViewer* poViewer = spi_tclMLVncViewer::getInstance();
         if(NULL != poViewer)
         {
            //Blocking Notification to Phone
            if(true == poViewer->bSendFramebufferBlockingNotification(pContextInformation->applicationUniqueId,
               rAppCntxtInfo.rMLRect,e16MLAPPLICATIONCATEGORY_NOTALLOWED))
            {
               ETG_TRACE_USR4(("[DESC]: Frame buffer blocking request sent for the recieved invalid update"));
            }
         }
      }//if(e32APPUNKNOWN == static_cast<tenAppCategory>(pConty)

       MLAppCntxtInfo oMLAppCntxtInfo;
       oMLAppCntxtInfo.rAppCntxtInfo = rAppCntxtInfo;

       spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
       if(NULL!= poMsgQInterface)
       {
          poMsgQInterface->bWriteMsgToQ(&oMLAppCntxtInfo, sizeof(oMLAppCntxtInfo));
       }//if(NULL!= poMsgQInterface)
   }//if(NULL != pRectangle)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vViewerContextInfoCallback - pRectangle or pContextInformation is NULL "));
   }
 }

/*****************************************************************************************
** FUNCTION: t_Void VNCCALL spi_tclMLVncViewer::vViewerContentAttestationFailureCallback()
*****************************************************************************************/
t_Void VNCCALL spi_tclMLVncViewer::vViewerContentAttestationFailureCallback(VNCViewer *pViewer, 
                                                                            t_Void *pContext, 
                                                                            VNCViewerContentAttestationFailure 
                                                                            enContentAttestationFailure)
{

	 SPI_INTENTIONALLY_UNUSED(pContext);
	 SPI_INTENTIONALLY_UNUSED(pViewer);
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vViewerContentAttestationFailureCallback-0x%x",enContentAttestationFailure));
   spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if(NULL!= poMsgQInterface)
   {
      MLContentAttestationfailureInfo oMLContAttestFailure;
      oMLContAttestFailure.enFailureReason = static_cast<tenContentAttestationFailureReason>(enContentAttestationFailure);
      poMsgQInterface->bWriteMsgToQ(&oMLContAttestFailure, sizeof(oMLContAttestFailure));
   }//if(NULL!= poMsgQInterface)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vSendContentAttestationFailureResult()
***************************************************************************/
t_Void spi_tclMLVncViewer::vSendContentAttestationFailureResult(t_U32 u32Reason)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vSendContentAttestationFailureResult-%d",u32Reason));
   if((NULL != m_pViewerSDK)&&(NULL!= m_pVNCViewer))
   {
      m_pViewerSDK->vncViewerContentAttestationFailureResult(m_pVNCViewer,u32Reason);
   }//if((NULL != m_pViewerSDK)&&(NULL!= m_pVNCViewer))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vSetPixelFormat()
***************************************************************************/
t_Void spi_tclMLVncViewer::vSetPixelFormat(const tenPixelFormat coenPixelFormat)
{
   t_Bool bRetVal = false;
   VNCViewerError s32Error = VNCViewerErrorFailed;
   if((NULL != m_pViewerSDK)&&(NULL!= m_pVNCViewer))
   {
      switch(coenPixelFormat)
      {
         case e8_PIXELFORMAT_RGB565:
         {
            VNCPixelFormat rFrameBufferPixelFormat =
                                       VNCPixelFormatRGB(s32RGB565RedBitDepth,
                                                s32RGB565GreenBitDepth,
                                                s32RGB565BlueBitDepth,
                                                s32BigEndianFlag);
            s32Error = m_pViewerSDK->vncViewerSetPixelFormat(
                     m_pVNCViewer, &rFrameBufferPixelFormat);
            break;
         }// case e8_PIXELFORMAT_RGB565:
         case e8_PIXELFORMAT_ARGB888:
         {
            VNCPixelFormat rFrameBufferPixelFormat = VNCPixelFormatRGB (s32ARGB888RedBitDepth,
                     s32ARGB888GreenBitDepth,
                     s32ARGB888BlueBitDepth,
                     s32BigEndianFlag);
            s32Error = m_pViewerSDK->vncViewerSetPixelFormat(
                     m_pVNCViewer, &rFrameBufferPixelFormat);
            break;
         } // case e8_PIXELFORMAT_ARGB888:
         default:
         {
            break;
         }
      }// switch(coenPixelFormat)
   }//if((NULL != m_pViewerSDK)&&(NULL!= m_pVNCViewer))

   bRetVal = (VNCViewerErrorNone == s32Error);

   ETG_TRACE_USR1(("spi_tclMLVncViewer::vSetPixelFormat PixelFormat-%d, Success-%d",
            ETG_ENUM(PIXEL_FORMAT, coenPixelFormat), ETG_ENUM(BOOL, bRetVal)));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vEnableFrameBufferUpdates()
***************************************************************************/
t_Void spi_tclMLVncViewer::vEnableFrameBufferUpdates(t_Bool bEnable)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vEnableFrameBufferUpdates:Enable-%d",
      ETG_ENUM(BOOL,bEnable)));

   if((NULL != m_pViewerSDK) && (NULL != m_pVNCViewer))
   {
      m_pViewerSDK->vncViewerEnableDisplayUpdates(m_pVNCViewer,bEnable);
   }//if(NULL != m_pViewerSDK)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncViewer::vSetPixelResolution()
***************************************************************************/
t_Void spi_tclMLVncViewer::vSetPixelResolution(const tenPixelResolution coenPixResolution)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vSetPixelResolution:Value:%d",
      ETG_ENUM(PIXEL_RESOLTION,coenPixResolution)));

   switch(coenPixResolution)
   {
   case e8_PIXEL_RES_320x240:
      {
         m_u32ScreenHeight = 240;
         m_u32ScreenWidth = 320;
      }
      break;
   case e8_PIXEL_RES_640x320:
      {
         m_u32ScreenHeight = 640;
         m_u32ScreenWidth = 320;
      }
      break;
   case e8_PIXEL_RES_800x480:
      {
         m_u32ScreenHeight = 480;
         m_u32ScreenWidth = 800;
      }
      break;
   case e8_PIXEL_RES_1200x720:
      {
         m_u32ScreenHeight = 720;
         m_u32ScreenWidth = 1200;
      }
      break;
   default:
      {
         //Nothing to do 
      }
   }//switch(coenPixResolution)
}

/*****************************************************************************************
** FUNCTION: t_Void VNCCALL spi_tclMLVncViewer::vViewerFrameBufferUpdateEndCallback()
*****************************************************************************************/
t_Void spi_tclMLVncViewer::vViewerFrameBufferUpdateEndCallback(VNCViewer *pViewer,
                                                               t_Void *pContext, 
                                                               const VNCRectangle *pInvalidatedRectangles,
                                                               size_t u8RectangleCount)
{
   //This can be used to check the frame buffer updates frequency.
   //And if we want to perform the content attestation on one out of 5/10 updates, this can be used. 
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,"spi_tclMLVncViewer::vViewerFrameBufferUpdateEndCallback "));
   //Add code
   mlink_wl_framebuffer_update_end_callback(pViewer,pContext,pInvalidatedRectangles,u8RectangleCount);
}



/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncViewer::vSetClientCapabilities(const trClient...)
***************************************************************************/
t_Void spi_tclMLVncViewer::vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities)
{
   spi_tclMLVncViewer* poViewer = spi_tclMLVncViewer::getInstance();
   if(NULL != poViewer)
   {
      (poViewer->m_oLock).s16Lock();
      m_rClientCapabilities = corfrClientCapabilities;
      (poViewer->m_oLock).vUnlock();
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncViewer::vSetViewerParameter()
***************************************************************************/
t_Void spi_tclMLVncViewer::vSetViewerParameter(const t_Char *cpoParameter, 
                                               const t_Char *cpoValue)
{
   ETG_TRACE_USR1(("spi_tclMLVncViewer::vSetViewerParameter() entered"));

   if( m_pViewerSDK && m_pVNCViewer && cpoParameter)
   {
      VNCViewerError s32SetViewerParameterError = VNCViewerErrorNone ;

      s32SetViewerParameterError = m_pViewerSDK->vncViewerSetParameter(m_pVNCViewer,
         cpoParameter, cpoValue);

      if(s32SetViewerParameterError == VNCViewerErrorNone)
      {
         ETG_TRACE_USR2(("[PARAM]:vSetViewerParameter: %s is set",cpoParameter));
      }//if(VNCViewerErrorNone == m_pViewerSDK->vncView
      else
      {
         ETG_TRACE_ERR(("[ERR]:Error %d in setting viewer parameter %s",s32SetViewerParameterError,cpoParameter));
      }//else
   }//if( m_pViewerSDK && m_pVNCViewer && cpoParameter)
   else
   {
      ETG_TRACE_ERR(("[ERR]:Either ViewerSDK or Viewer or parameter pointer is NULL"));
   }//else
}
