/*!
*******************************************************************************
* \file              spi_tclMLVncViewerDataIntf.cpp
* \brief             VNC Viewer Data Interface Class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface class for providing necessary Viewer Data
                to the requesting classes
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                          | Modifications
13.09.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)RealVNC sdk - includes
| 2)Typedefines
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include <iostream>
#include <stdio.h>
#include "mlink_dlt.h"
#include "spi_tclMLVncViewerDataIntf.h"
#include "spi_tclMLVncViewer.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncViewerDataIntf.cpp.trc.h"
   #endif
#endif

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncViewerDataIntf::spi_tclMLVncViewerDataIntf()
***************************************************************************/

spi_tclMLVncViewerDataIntf::spi_tclMLVncViewerDataIntf()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewerDataIntf::spi_tclMLVncViewerDataIntf() entered \n"));

   //Create the Viewer data instance
   m_rViewerData = new trViewerData;
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncViewerDataIntf::~spi_tclMLVncViewerDataIntf()
***************************************************************************/

spi_tclMLVncViewerDataIntf::~spi_tclMLVncViewerDataIntf()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewerDataIntf::~spi_tclMLVncViewerDataIntf() entered \n"));

   //Delete the Viewer data instance
    RELEASE_MEM(m_rViewerData);

}

/***************************************************************************************
** FUNCTION:  VNCViewerData* spi_tclMLVncViewerDataIntf::pGetViewerData()
****************************************************************************************/
trViewerData* spi_tclMLVncViewerDataIntf::pGetViewerData()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewerDataIntf::pGetViewerData() entered \n"));

   spi_tclMLVncViewer* pVncViewer = spi_tclMLVncViewer::getInstance();

   if((NULL!= pVncViewer)&& (NULL!= m_rViewerData))
   {
      //Get the Viewer SDK Instance
      m_rViewerData->pViewerSDK = pVncViewer->pGetViewerSDK();

      //Get the Viewer Instance
      m_rViewerData->pVNCViewer = pVncViewer->pGetViewerInstance();
   }

   return m_rViewerData;
}

mlink_dlt_context* spi_tclMLVncViewerDataIntf::pGetDLTContext()
{
   ETG_TRACE_USR1(("spi_tclMLVncViewerDataIntf::pGetDLTContext() entered \n"));
   spi_tclMLVncViewer* pVncViewer = spi_tclMLVncViewer::getInstance();
   return pVncViewer->pGetDLTContext();
}




