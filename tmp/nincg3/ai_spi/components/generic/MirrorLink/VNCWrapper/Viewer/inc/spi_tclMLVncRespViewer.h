/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespViewer.h
 * \brief             RealVNC Response Wrapper for Viewer
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper for wrapping  response from VNC Viewer callbacks
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                          | Modifications
 03.09.2013 |  Hari Priya E R(RBEI/ECP2)       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMLVNCRESPVIEWER_H_
#define SPI_TCLMLVNCRESPVIEWER_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "RespBase.h"
#include "SPITypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLVncRespViewer
 * \brief This class wraps the RealVNC SDK callbacks provide response
 * to SPI component to implement the VNC Viewer service provided by MLServer.
 *
 */

class spi_tclMLVncRespViewer: public RespBase
{

   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncRespViewer::spi_tclMLVncRespViewer();
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncRespViewer()
       * \brief    Constructor
       * \sa      ~spi_tclMLVncRespViewer()
       **************************************************************************/
      spi_tclMLVncRespViewer();

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclMLVncRespViewer::~spi_tclMLVncRespViewer()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclMLVncRespViewer()
       * \brief   Destructor
       * \sa      spi_tclMLVncRespViewer()
       **************************************************************************/
      virtual ~spi_tclMLVncRespViewer();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostViewerSessionProgress
       ***************************************************************************/
      /*!
       * \fn      vPostViewerSessionProgress( tenSessionProgress enSessionProgress, 
       *                       const t_U32 cou32DeviceHandle = 0);
       * \brief   Posts the VNC Viewer Session establishment Progress
       * \param   enSessionProgress; [IN]Session Progress status
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostViewerSessionProgress(tenSessionProgress enSessionProgress,
            const t_U32 cou32DeviceHandle = 0);

	   /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostServerCapabilities
       ***************************************************************************/
      /*!
       * \fn      vPostServerCapabilities( trServerCapabilities* pServerCapabilities,
       *                                                   const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the Server event configuration parameters to the SPI
       * \param   pServerCapabilities: [IN]Server capabilities
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostServerCapabilities(
            trServerCapabilities* pServerCapabilities,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostClientCapabilities
       ***************************************************************************/
      /*!
       * \fn      vPostClientCapabilities( trClientCapabilities* pClientCapabilities,
       *                                                   const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the Client event configuration parameters to the SPI
       * \param   pClientCapabilities: [IN]Client capabilities
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostClientCapabilities(
            trClientCapabilities* pClientCapabilities,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:t_Void vPostOrientationMode
       ***************************************************************************/
      /*!
       * \fn     vPostOrientationMode(
       *               tenOrientationMode enOrientationMode, const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the Orientation Mode to the SPI
       * \param   enOrientationMode: [IN]Orientation Mode
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostOrientationMode(tenOrientationMode enOrientationMode,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:  t_Void vPostDisplayCapabilities
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostDisplayCapabilities(trDisplayCapabilities* pDispCapabilities,
       *                                              const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts the server Display capabilities to the SPI
       * \param   pDispCapabilities: [IN]Display Capabilities
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostDisplayCapabilities(
            trDisplayCapabilities* pDispCapabilities,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostDeviceStatusMessages
       ***************************************************************************/
      /*!
       * \fn      vPostDeviceStatusMessages(t_U32 u32DeviceStatusFeature)
       * \brief   Posts the set device status messages to SPI
       * \param   u32DeviceStatusFeature: [IN]DEvice status Feature set
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostDeviceStatusMessages(t_U32 u32DeviceStatusFeature,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostViewerSubscriptionResult
       ***************************************************************************/
      /*!
       * \fn      vPostViewerSubscriptionResult()
       * \brief   Posts the Viewer Subscription Result to SPI
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostViewerSubscriptionResult(
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostExtensionEnabledStatus
       ***************************************************************************/
      /*!
       * \fn      vPostExtensionEnabledStatus(t_U32 u32ExtEnabled, const t_U32 cou32DeviceHandle = 0)
       * \brief   Posts whether a particular Extension has been enabled or not
       * \param   u32ExtEnabled: [IN]Extension enabled or disabled
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostExtensionEnabledStatus(t_U32 u32ExtEnabled,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:  t_Void vPostWLInitialiseError
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostWLInitialiseError
       * \brief   Posts whteher a particular Extension has been enabled or not
       * \param   szMessage: [IN]Message indicating the failure in WL initialise
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostWLInitialiseError( t_String szMessage,
            const t_U32 cou32DeviceHandle = 0);


      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostDriverDistractionAvoidanceResult
      ***************************************************************************/
      /*!
      * \fn      vPostDriverDistractionAvoidanceResult()
      * \brief   Posts the device status callback after the driver distraction avoidance is
      *          enabled or disabled.
      * \param   bDriverDistAvoided : [IN] enabled/disabled
      * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
      * \retval  t_Void
      **************************************************************************/
      virtual t_Void vPostDriverDistractionAvoidanceResult(t_Bool bDriverDistAvoided,
         const t_U32 cou32DeviceHandle = 0);

	  /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostFrameBufferDimension
       ***************************************************************************/
      /*!
       * \fn      vPostFrameBufferDimension()
       * \brief   Posts the Framebuffer Initial Width and Height to SPI
	   * \param   u32Width: [IN] Initial Width of framebuffer
	   * \param   u32Height: [IN] Initial Height of framebuffer
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostFrameBufferDimension(t_U32 u32Width,t_U32 u32Height,
            const t_U32 cou32DeviceHandle = 0);

	  /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostServerDispDimension
       ***************************************************************************/
      /*!
       * \fn      vPostServerDispDimension()
       * \brief   Posts the Changed Server Display Width and Height to SPI
	   * \param   u32Width: [IN] New Width of server display
	   * \param   u32Height: [IN] New Height of server display
       * \param   cou32DeviceHandle : [IN]  Device handle to identify the MLServer
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vPostServerDispDimension(t_U32 u32Width,t_U32 u32Height,
            const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostViewerError()
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostViewerError(t_U32 u32ViewerError)
       * \brief   Posts the Viewer Error message to SPI
       * \param   u32ViewerError : [IN]  Error code
       * \retval  t_Void
       **************************************************************************/
      virtual t_Void vPostViewerError(t_U32 u32ViewerError);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostAppContextInfo()
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo)
       * \brief   Posts the context info of the application on the FG
       * \param   corfrAppCntxtInfo : [IN]  Application context info
       * \retval  t_Void
       **************************************************************************/
      virtual t_Void vPostAppContextInfo(const trAppContextInfo& corfrAppCntxtInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostDayNightModeInfo()
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostDayNightModeInfo(t_Bool bNightModeEnabled)
       * \brief   Posts the day night mode info
       * \param   bNightModeEnabled : [IN]  True - If Night mode is enabled
       *                                    False - If Night Mode is disabled
       * \retval  t_Void
       **************************************************************************/
      virtual t_Void vPostDayNightModeInfo(t_Bool bNightModeEnabled);


      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostVoiceInputInfo()
       ***************************************************************************/
      /*!
       * \fn      t_Void vPostVoiceInputInfo(t_Bool bVoiceInputEnabled)
       * \brief   Posts the Voice Input info whether it is enabled or disabled
       * \param   bVoiceInputEnabled : [IN]  True - If VoiceInput is enabled
       *                                    False - If VoiceInput is disabled
       * \retval  t_Void
       **************************************************************************/
      virtual t_Void vPostVoiceInputInfo(t_Bool bVoiceInputEnabled);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostMicroPhoneInputInfo()
      ***************************************************************************/
      /*!
      * \fn      t_Void vPostMicroPhoneInputInfo(t_Bool bMicroPhoneInputEnabled)
      * \brief   Posts the Micro Phone info whether it is enabled or disabled
      * \param   bMicroPhoneInputEnabled : [IN]  True - If MicroPhoneInput is enabled
      *                                    False - If MicroPhoneInput is disabled
      * \retval  t_Void
      **************************************************************************/
      virtual t_Void vPostMicroPhoneInputInfo(t_Bool bMicroPhoneInputEnabled);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclMLVncRespViewer::vPostContentAttestationFailureInfo()
      ***************************************************************************/
      /*!
      * \fn      t_Void vPostContentAttestationFailureInfo(
      *            tenContentAttestationFailureReason enFailureReason)
      * \brief   Posts the Content attestation failure info to SPI
      * \param   enFailureReason : [IN]  Failure reason
      * \retval  t_Void
      **************************************************************************/
      virtual t_Void vPostContentAttestationFailureInfo(
         tenContentAttestationFailureReason enFailureReason);
   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

};
#endif /* SPI_TCLMLVNCRESPVIEWER_H_ */
