/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespDAP.h
 * \brief             RealVNC Wrapper Response class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:   VNC Response class for receiving response receiving response
 for spi_tclMLVncRespDAP calls to implement the client side Device
 Attestation Protocol (DAP).
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 04.09.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 11.04.2013 |  Shiva Kumar Gurija          | Updated the Attestation Response
                                              Handling

 \endverbatim
 ******************************************************************************/

#ifndef SPI_MLVNCRESPDAP_H_
#define SPI_MLVNCRESPDAP_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "RespBase.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMLVncRespDAP
 * \brief VNC Response class for receiving response receiving response
 *  for spi_tclMLVncRespDAP calls to implement the client side Device
 *  Attestation Protocol (DAP).
 */

class spi_tclMLVncRespDAP: public RespBase
{

   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
      ** FUNCTION:  virtual spi_tclMLVncRespDAP::~spi_tclMLVncRespDAP()
      ***************************************************************************/
      /*!
      * \fn      virtual ~spi_tclMLVncRespDAP()
      * \brief   Destructor
      * \sa      spi_tclMLVncRespDAP()
      **************************************************************************/
      virtual ~spi_tclMLVncRespDAP();

      /***************************************************************************
      ** FUNCTION:t_Void spi_tclMLVncRespDAP::vPostLaunchDAPRes()
      ***************************************************************************/
      /*!
      * \fn    t_Void vPostLaunchDAPRes(trUserContext rUserContext, t_Bool bLaunchDAPRes,
      *        const t_U32 cou32DeviceHandle = 0)
      * \brief   posts result of Launching DAP on MLServer
      * \param  rUserContext : [IN] Context information passed from the caller
      * \param   bLaunchDAPRes : true : Launch DAP on MLServer was successful
      *                          false : Launch DAP on MLServer failed
      * \param   cou32DeviceHandle :[IN] Device handle
      **************************************************************************/
      virtual t_Void vPostLaunchDAPRes(trUserContext rUserContext,
         t_Bool bLaunchDAPRes, 
         const t_U32 cou32DeviceHandle);

      /***************************************************************************
      ** FUNCTION:t_Void spi_tclMLVncRespDAP::vPostDAPAttestResp()
      ***************************************************************************/
      /*!
      * \fn    t_Void vPostDAPAttestResp(trUserContext rUsrCntext,
      *            tenMLAttestationResult enAttestationResult,
      *            t_String szVNCAppPublickey,
      *            t_String szUPnPAppPublickey,
      *            t_String szVNCUrl,
      *            const t_U32 cou32DeviceHandle,
      *            t_Bool bVNCAvailInDAPAttestResp)
      * \brief   posts result of DAP request is complete
      * \param   rUsrCntext             : [IN] Context information passed from the caller
      * \param   enAttestationResult    : [IN] DAP attestation response
      * \param   szVNCAppPublickey      :[IN] Only for the component TerminalMode:VNC-Server
      *                                  Application public key will be sent.
      * \param   szUPnPAppPublickey     :[IN] Only for the component TerminalMode:UPnP-Server
      *                                  Application public key will be sent.
      * \param   szVNCUrl               :[IN] VNC Server URL
      * \param   cou32DeviceHandle      :[IN] Device handle
      * \param  bVNCAvailInDAPAttestResp:[IN] VNC-Server is listed in DAP attestation response
      * \retval t_Void
      **************************************************************************/
      virtual t_Void vPostDAPAttestResp(trUserContext rUserContext,
         tenMLAttestationResult enAttestationResult,
         t_String szVNCAppPublickey,
         t_String szUPnPAppPublickey,
         t_String szVNCUrl,
         const t_U32 cou32DeviceHandle,
         t_Bool bVNCAvailInDAPAttestResp);


   protected:

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncRespDAP::spi_tclMLVncRespDAP();
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncRespDAP()
       * \brief  Constructor. Made potected to prevent object creation.
       *          Expects the implementation class to derive from this and implement
       *          the necessary virtual functions
       * \sa      ~spi_tclMLVncRespDAP()
       **************************************************************************/
      spi_tclMLVncRespDAP();

      /***************************************************************************
       ** FUNCTION: spi_tclMLVncRespDAP(const spi_tclMLVncRespDAP &corfobjCRCBResp)
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncRespDAP(const spi_tclMLVncRespDAP &corfobjCRCBResp)
       * \brief   Copy constructor not implemented hence made protected
       * \sa      ~spi_tclMLVncRespDAP()
       **************************************************************************/
      spi_tclMLVncRespDAP(const spi_tclMLVncRespDAP &corfobjCRCBResp);

      /***************************************************************************
       ** FUNCTION: const spi_tclMLVncRespDAP & operator=(
       **                                 const spi_tclMLVncRespDAP &corfobjCRCBResp);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclMLVncRespDAP & operator=(const spi_tclMLVncRespDAP &corfobjCRCBResp);
       * \brief   assignment operator not implemented hence made protected
       * \sa      ~spi_tclMLVncRespDAP()
       **************************************************************************/
      const spi_tclMLVncRespDAP & operator=(
               const spi_tclMLVncRespDAP &corfobjCRCBResp);

};

#endif /* SPI_MLVNCRESPDAP_H_ */
