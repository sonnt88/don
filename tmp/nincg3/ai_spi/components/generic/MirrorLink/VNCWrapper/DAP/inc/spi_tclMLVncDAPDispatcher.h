/***********************************************************************/
/*!
 * \file  spi_tclMLVncDAPDispatcher.h
 * \brief Message Dispatcher for DAP Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for DAP Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLMLVNCDAPDISPATCHER_H_
#define SPI_TCLMLVNCDAPDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "RespRegister.h"

/**************Forward Declarations******************************************/
class spi_tclMLVncDAPDispatcher;

/****************************************************************************/
/*!
 * \class MLDAPMsgBase
 * \brief Base Message type for all DAP messages
 ****************************************************************************/
class MLDAPMsgBase: public trMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  MLDAPMsgBase::MLDAPMsgBase
       ***************************************************************************/
      /*!
       * \fn      MLDAPMsgBase()
       * \brief   Default constructor
       **************************************************************************/
      MLDAPMsgBase();

      /***************************************************************************
       ** FUNCTION:  MLDAPMsgBase::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDAPDispatcher* poDAPDispatcher)
       * \brief   Pure virtual function to be overridden by inherited classes for
       *          dispatching the message
       * \param poDAPDispatcher : pointer to Message dispatcher for DAP
       **************************************************************************/
      virtual t_Void vDispatchMsg(
               spi_tclMLVncDAPDispatcher* poDAPDispatcher) = 0;

      /***************************************************************************
       ** FUNCTION:  MLDAPMsgBase::~MLDAPMsgBase
       ***************************************************************************/
      /*!
       * \fn      ~MLDAPMsgBase()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLDAPMsgBase()
      {

      }
};

/****************************************************************************/
/*!
 * \class MLLaunchDAPMsg
 * \brief DAP attestation response message
 ****************************************************************************/
class MLLaunchDAPMsg: public MLDAPMsgBase
{
   public:
      t_Bool bLaunchDAPRes;

      t_U32 u32DeviceHandle;
      /***************************************************************************
       ** FUNCTION:  MLLaunchDAPMsg::MLLaunchDAPMsg
       ***************************************************************************/
      /*!
       * \fn      MLLaunchDAPMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLLaunchDAPMsg();

      /***************************************************************************
       ** FUNCTION:  MLLaunchDAPMsg::~MLLaunchDAPMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLLaunchDAPMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLLaunchDAPMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLLaunchDAPMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDAPDispatcher* poDAPDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
	   * \param  poDAPDispatcher : pointer to Message dispatcher for DAP
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDAPDispatcher* poDAPDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLLaunchDAPMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLLaunchDAPMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*!
 * \class MLAttestationResponseMsg
 * \brief DAP attestation response message
 ****************************************************************************/
class MLAttestationResponseMsg: public MLDAPMsgBase
{
   public:
      t_U32 u32DeviceHandle; 
      tenMLAttestationResult enAttestationResult;
      t_String *pszVNCApplicationPublicKey64;
      t_String *pszUPnPApplicationPublicKey64;
      t_String *pszVNCUrl;
      t_Bool bVNCAvailInDAPAttestResp;

      /***************************************************************************
       ** FUNCTION:  MLAttestationResponseMsg::MLAttestationResponseMsg
       ***************************************************************************/
      /*!
       * \fn      MLAttestationResponseMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAttestationResponseMsg();

      /***************************************************************************
       ** FUNCTION:  MLAttestationResponseMsg::~MLAttestationResponseMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAttestationResponseMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAttestationResponseMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLAttestationResponseMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDAPDispatcher* poDAPDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDAPDispatcher : pointer to Message dispatcher for DAP
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDAPDispatcher* poDAPDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLAttestationResponseMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLAttestationResponseMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class spi_tclMLVncDAPDispatcher
 * \brief Message Dispatcher for DAP Messages
 ****************************************************************************/
class spi_tclMLVncDAPDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::spi_tclMLVncDAPDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncDAPDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclMLVncDAPDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::~spi_tclMLVncDAPDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclMLVncDAPDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclMLVncDAPDispatcher();


      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDAPMsg(MLLaunchDAPMsg* poLaunchDAPMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDAPMsg(MLLaunchDAPMsg* poLaunchDAPMsg)
       * \brief   Handles Messages of MLLaunchDAPMsg type
	   * \param   poLaunchDAPMsg : pointer to MLLaunchDAPMsg.
       **************************************************************************/
      t_Void vHandleDAPMsg(MLLaunchDAPMsg* poLaunchDAPMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDAPMsg(MLAttestationResponseMsg* poAttestationResponseMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDAPMsg(MLAttestationResponseMsg* poAttestationResponseMsg)
       * \brief   Handles Messages of MLAttestationResponseMsg type
       * \param poAttestationResponseMsg : pointer to MLAttestationResponseMsg.
       **************************************************************************/
      t_Void vHandleDAPMsg(MLAttestationResponseMsg* poAttestationResponseMsg)const;

};

#endif /* SPI_TCLMLVNCDAPDISPATCHER_H_ */
