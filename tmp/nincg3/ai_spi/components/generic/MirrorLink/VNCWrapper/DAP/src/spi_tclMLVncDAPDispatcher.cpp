/***********************************************************************/
/*!
 * \file  spi_tclMLVncDAPDispatcher.cpp
 * \brief Message Dispatcher for DAP Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for DAP Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 24.04.2014  |  Shiva kumar Gurija   | XML Validation

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "spi_tclMLVncDAPDispatcher.h"
#include "spi_tclMLVncRespDAP.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
#include "trcGenProj/Header/spi_tclMLVncDAPDispatcher.cpp.trc.h"
#endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleDAPMsg(this);                    \
   }                                                        \
   vDeAllocateMsg();                                        \
}

/***************************************************************************
 ** FUNCTION:  MLDAPMsgBase::MLDAPMsgBase
 ***************************************************************************/
MLDAPMsgBase::MLDAPMsgBase()
{
   vSetServiceID (e32MODULEID_VNCDAP);
}

/***************************************************************************
 ** FUNCTION:  MLLaunchDAPMsg::MLLaunchDAPMsg
 ***************************************************************************/
MLLaunchDAPMsg::MLLaunchDAPMsg() :
         bLaunchDAPRes(false), u32DeviceHandle(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLLaunchDAPMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLLaunchDAPMsg, spi_tclMLVncDAPDispatcher);

/***************************************************************************
 ** FUNCTION:  MLAttestationResponseMsg::MLAttestationResponseMsg
 ***************************************************************************/
MLAttestationResponseMsg::MLAttestationResponseMsg() : u32DeviceHandle(0),
         enAttestationResult(e8MLATTESTATION_FAILED),
         pszVNCApplicationPublicKey64(NULL),pszUPnPApplicationPublicKey64(NULL),
         pszVNCUrl(NULL),bVNCAvailInDAPAttestResp(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAttestationResponseMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAttestationResponseMsg, spi_tclMLVncDAPDispatcher);

/***************************************************************************
 ** FUNCTION:  MLAttestationResponseMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLAttestationResponseMsg::vAllocateMsg()
{
   pszVNCApplicationPublicKey64 = new t_String;
   pszUPnPApplicationPublicKey64 = new t_String;
   pszVNCUrl = new t_String;
   SPI_NORMAL_ASSERT(NULL == pszVNCApplicationPublicKey64);
   SPI_NORMAL_ASSERT(NULL == pszUPnPApplicationPublicKey64);
   SPI_NORMAL_ASSERT(NULL == pszVNCUrl);
}

/***************************************************************************
 ** FUNCTION:  MLAttestationResponseMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLAttestationResponseMsg::vDeAllocateMsg()
{
   RELEASE_MEM(pszVNCApplicationPublicKey64);
   RELEASE_MEM(pszUPnPApplicationPublicKey64);
   RELEASE_MEM(pszVNCUrl);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::spi_tclMLVncDAPDispatcher
 ***************************************************************************/
spi_tclMLVncDAPDispatcher::spi_tclMLVncDAPDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::~spi_tclMLVncDAPDispatcher
 ***************************************************************************/
spi_tclMLVncDAPDispatcher::~spi_tclMLVncDAPDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDAPMsg(MLLaunchDAPMsg* poLaunchDAPMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDAPDispatcher::vHandleDAPMsg(MLLaunchDAPMsg* poLaunchDAPMsg) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poLaunchDAPMsg)
   {
      trUserContext rUserContext;
      poLaunchDAPMsg->vGetUserContext(rUserContext);
      CALL_REG_OBJECTS(spi_tclMLVncRespDAP, e16DAP_REGID,
               vPostLaunchDAPRes(rUserContext, poLaunchDAPMsg->bLaunchDAPRes,
                        poLaunchDAPMsg->u32DeviceHandle));

   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDAPMsg()
 ***************************************************************************/
t_Void spi_tclMLVncDAPDispatcher::vHandleDAPMsg(
         MLAttestationResponseMsg* poAttestationResponseMsg) const
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAttestationResponseMsg)
            && (NULL != poAttestationResponseMsg->pszVNCApplicationPublicKey64)&&
            (NULL != poAttestationResponseMsg->pszUPnPApplicationPublicKey64)&&
            (NULL!= poAttestationResponseMsg->pszVNCUrl))
   {
      trUserContext rUserContext;
      poAttestationResponseMsg->vGetUserContext(rUserContext);
      CALL_REG_OBJECTS(spi_tclMLVncRespDAP, e16DAP_REGID,
               vPostDAPAttestResp(rUserContext,
               poAttestationResponseMsg->enAttestationResult,
               *(poAttestationResponseMsg->pszVNCApplicationPublicKey64),
               *(poAttestationResponseMsg->pszUPnPApplicationPublicKey64),
               *(poAttestationResponseMsg->pszVNCUrl),
               poAttestationResponseMsg->u32DeviceHandle,
               poAttestationResponseMsg->bVNCAvailInDAPAttestResp));
   } //if (NULL != poAttestationResponseMsg)
}

