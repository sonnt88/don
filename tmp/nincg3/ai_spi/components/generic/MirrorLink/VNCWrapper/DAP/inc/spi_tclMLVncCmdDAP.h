/*!
 *******************************************************************************
 * \file              spi_tclMLVncCmdDAP.h
 * \brief             RealVNC Wrapper for DAP
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper for implementing the client side Device
 Attestation Protocol (DAP)
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 05.09.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 11.12.2013 |  Shiva Kumar Gurija          | Updated for ML1.0 & ML1.1 DAP Clients
 11.04.2013 |  Shiva Kumar Gurija          | Updated the Attestation Response
                                              Handling
 24.04.2014 |  Shiva kumar Gurija          | XML Validation
 24.04.2014 |  Shiva kumar Gurija          | Set Timeout for DAP attestation Request
                                              - Fix for High CPU Consumption issue
01.06.2015  |  Shiva kumar Gurija          | Set names to VNC Threads

 \endverbatim
 ******************************************************************************/

#ifndef SPI_MLVNCCMDDAP_H_
#define SPI_MLVNCCMDDAP_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "vncdiscoverysdk.h"
#include "vncmirrorlinkkeys.h"
#include "vncdapsdk.h"
#include "SPITypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
static const t_U8 cou8ML_Num_DAPClients = 2;
static const t_U8 cou8ML_1_0_DapClient = 0;
static const t_U8 cou8ML_1_1_DapClient = 1;
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
//! Forward declarations
class spi_tclMLVncDiscovererDataIntf;
class MsgContext;

/*!
 * \class spi_tclMLVncCmdDAP
 * \brief VNC Wrapper for implementing the client side Device
 *        Attestation Protocol (DAP)
 */

class spi_tclMLVncCmdDAP
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncCmdDAP::spi_tclMLVncCmdDAP();
       ***************************************************************************/
      /*!
       * \fn     spi_tclMLVncCmdDAP()
       * \brief  Default Constructor
       * \sa      ~spi_tclMLVncCmdDAP()
       **************************************************************************/
      spi_tclMLVncCmdDAP();

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclMLVncCmdDAP::~spi_tclMLVncCmdDAP()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclMLVncCmdDAP()
       * \brief   Destructor
       * \sa      spi_tclMLVncCmdDAP()
       **************************************************************************/
      virtual ~spi_tclMLVncCmdDAP();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLVncCmdDAP::bIntializeDAPSDK(const t_U32...)
       ***************************************************************************/
      /*!
       * \fn      t_Bool bIntializeDAPSDK(const t_U32 cou32DeviceHandle,
       *          trMLVersion &rfrMLVersion
       * \brief   initializes the DAP SDK.Must be called only after discovery sdk
       *          is initialized. Must be called only once. Multiple DAP
       *          attestations can be made by creating multiple DAP clients within
       *          same SDK
       * \param   cou32DeviceHandle : unique ID of ML Server
       * \retval  true : The SDK was initialized successfully.
       * \retval  false : sdk initialization failed
       * \sa      vUnIntializeDAPSDK()
       **************************************************************************/
      t_Bool bIntializeDAPSDK();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncCmdDAP:: vUnIntializeDAPSDK( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vUnIntializeDAPSDK()
       * \brief   The application should call this function after Terminating DAP
       *          and destroying attestation responses
       * \sa      bIntializeDAPSDK
       **************************************************************************/
      t_Void vUnIntializeDAPSDK();

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdDAP::bCreateDAPClient
       ***************************************************************************/
      /*!
       * \fn     bCreateDAPClient(const trUserContext corUserContext)
       * \brief  Creates ML1.0 and ML1.1 DAP Clients to perform DAP Attestation for 
       *         1.0 and 1.1 devices.
       * \reval  true : DAP Client created successfully
       * \retval false : DAP Client creation failed
       **************************************************************************/
      t_Bool bCreateDAPClient();

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP::vLaunchDAP(const trUserContext ...);
       ***************************************************************************/
      /*!
       * \fn      t_Void vLaunchDAP(const trUserContext corUserContext,
       *             t_U32 u32DAPAppID , const t_U32 cou32DeviceHandle = 0);
       * \brief  Launches DAP on MLServer. This call is asynchronous and the result is returned
       *         in vPostLaunchDAPRes. Must be called after bCreateDAPClient
       * \param  corUserContext : [IN] Context information passed from the caller of this function
       * \param  u32DAPAppID : AppID of DAP application. default 0 (for 1.0 device)
       * \param  cou32DeviceHandle :[IN] Device handle to identify the MLServer
       **************************************************************************/
      t_Void vLaunchDAP(const trUserContext corUserContext, 
         t_U32 u32DAPAppID,
         const t_U32 cou32DeviceHandle = 0);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP::bDestroyDAPClient(const trUserContext ...);
       ***************************************************************************/
      /*!
       * \fn     t_Void vDestroyDAPClient()
       * \brief  Destroys the created DAP Clients. Currently DAP ML 1.0 and ML 1.1
       *         clients are created.
       * \reval  t_Void
       **************************************************************************/
      t_Void vDestroyDAPClient()const;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP::vRequestDAPAttestation()
       ***************************************************************************/
      /*!
       * \fn      t_Void vRequestDAPAttestation(const t_U32 cou32DeviceHandle, 
       *           const trUserContext& corfrUsrCntxt)
       * \brief   Performs DAP attestation for all components ML Server supports.
       * \param   cou32DeviceHandle :[IN] Device handle to identify the MLServer
       * \param   corfrUsrCntxt     :[IN] User information passed by the caller.
       *                                  will be returned with response
       **************************************************************************/
      t_Void vRequestDAPAttestation(const t_U32 cou32DeviceHandle,
         const trUserContext& corfrUsrCntxt);

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bAddDiscoveryListener()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bAddDiscoveryListener(VNCDiscoverySDK* poDiscSDK, 
       *          VNCDiscoverySDKDiscoverer* poDisc)
       * \brief   Adds discovery listener to get response for the discovery sdk
       *          callbacks.
       * \param  poDiscSDK   : [IN] Discovery SDK Pointer
       * \param  poDisc      : [IN] Discoverer Pointer
       * \reval   true  : if listener was added succesfully
       * \retval  false : if the discovery sdk returned an error while adding
       *                  listener
       **************************************************************************/
      t_Bool bAddDiscoveryListener(VNCDiscoverySDK* poDiscSDK, 
         VNCDiscoverySDKDiscoverer* poDisc);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bAddVNCLicense()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bAddVNCLicense(VNCDAPClient* poDAPClient)
       * \brief   reads the VNC License for DAP and adds it to DAP SDK
       * \param   poDAPClient : [IN] DAP Client
       * \retval  true: license successfully added
       * \retval  false : failed to add license
       * \sa      spi_tclMLVncCmdDAP()
       **************************************************************************/
      t_Bool bAddVNCLicense(VNCDAPClient* poDAPClient)const;

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bSetRootTrustCertificate()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bSetRootTrustCertificate(VNCDAPClient* poDAPClient)
       * \brief   reads the Root trust certificate and adds it to DAP SDK for
       *          doing Device Attestation
       * \param   poDAPClient : [IN] DAP Client
       * \retval  false : unable to read trust certificate or recognize it
       * \retval  true : added root trust certificate
       **************************************************************************/
      t_Bool bSetRootTrustCertificate(VNCDAPClient* poDAPClient)const;


      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bSetDAPServerUrl()
       ***************************************************************************/
      /*!
       * \fn     t_Bool bSetDAPServerUrl(t_String szDAPServerUrl,VNCDAPClient* poDAPClient)
       * \brief  sets the url for DAP server on ML Server
       * \param  szDAPServerUrl : DAP server URL
       * \param  poDAPClient : [IN] DAP Client
       * \retval true: The server URL was set successfully.
       * \retval false: The provided URL is not a valid DAP
       *         server URL.
       **************************************************************************/
      t_Bool bSetDAPServerUrl(t_String szDAPServerUrl,VNCDAPClient* poDAPClient)const;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vFetchDAPServerUrl()
       ***************************************************************************/
      /*!
       * \fn     t_Void vFetchDAPServerUrl( const trUserContext corUserContext, 
       *                                    t_String szDAPAppID,
       *                                    VNCDiscoverySDKDiscoverer *poDisc, 
       *                                    VNCDiscoverySDKEntity *poEntity)
       * \brief  fetches url of DAP server on ML Server using discovery SDK
       * \param  corUserContext  : [IN] User Context
       * \param  szDAPAppID  : [IN] DAP Application ID
       * \param  poDisc      : [IN] Discoverer Pointer
       * \param  poEntity    : [IN] Entity pointer
       * \param   szDAPAppID : App ID of DAP apllication on MLServer
       **************************************************************************/
      t_Void vFetchDAPServerUrl(
         const trUserContext corUserContext, 
         t_String szDAPAppID,
         VNCDiscoverySDKDiscoverer *pDisc, 
         VNCDiscoverySDKEntity *pEntity);

      /***************************************************************************
       ** FUNCTION: vGetComponentID(t_String &rfszComponentId...)
       ***************************************************************************/
      /*!
       * \fn      vGetComponentID(t_String &rfszComponentId, const tenMLComponentID coenComponentID)
       * \brief   Returns t_Character array of componentID enum type
       * \param   rfszComponentId :[OUT] reference to component ID in string format
       * \param   coenComponentID : [IN] enum value of component ID
       **************************************************************************/
      t_Void vGetComponentID(t_String &rfszComponentId,
               const tenMLComponentID coenComponentID)const;

      /***************************************************************************
       ** FUNCTION: tenMLAttestationResult enGetAttestationResult
       ***************************************************************************/
      /*!
       * \fn      tenMLAttestationResult enGetAttestationResult(const VNCDAPError coenVNCDAPError)
       * \brief   Returns t_Character array of componentID enum type
       * \param   coenVNCDAPError :[IN] enum value of VNCDAPError
       * \reval   enum value of tenMLDAPError
       **************************************************************************/
      tenMLAttestationResult enGetAttestationResult(
               const VNCDAPError coenVNCDAPError)const;

      /***************************************************************************
       ** FUNCTION: spi_tclMLVncCmdDAP(const spi_tclMLVncCmdDAP &rfcoobjCRCBResp)
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncCmdDAP(const spi_tclMLVncCmdDAP &rfcoobjCRCBResp)
       * \brief   Copy constructor not implemented hence made protected
       **************************************************************************/
      spi_tclMLVncCmdDAP(const spi_tclMLVncCmdDAP &rfcoobjCRCBResp);

      /***************************************************************************
       ** FUNCTION: const spi_tclMLVncCmdDAP & operator=(
       **                                 const spi_tclMLVncCmdDAP &rfcoobjCRCBResp);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclMLVncCmdDAP & operator=(const spi_tclMLVncCmdDAP &objCRCBResp);
       * \brief   assignment operator not implemented hence made protected
       **************************************************************************/
      const spi_tclMLVncCmdDAP & operator=(
               const spi_tclMLVncCmdDAP &rfcoobjCRCBResp);

      //! RealVNC SDK callbacks
      /***************************************************************************
       ** FUNCTION: static t_Void VNCCALL
       ** spi_tclMLVncCmdDAP::vPostEntityValueCallback(t_Void *pSDKContext...)
       ***************************************************************************/
      /*!
       * \fn    vPostEntityValueCallback(t_Void *pSDKContext,
       *        VNCDiscoverySDKRequestId requestId,
       *        VNCDiscoverySDKDiscoverer *pDiscoverer,
       *        VNCDiscoverySDKEntity *pEntity, t_Char *pKey, t_Char *pValue,
       *        VNCDiscoverySDKError error);
       * \brief   RealVNC SDK callback registered to receive the response for
       *          setting a value specified during postEntityValueRespondToListener
       *          method provided by RealVNC DiscovererSDK
       * \param pSDKContext The SDK context that was set by the client application for
       *                 callbacks.
       * \param u32RequestId The ID of the request. This was given to the client
       *                  application when the application made the request
       *                  initially.
       * \param pDiscoverer The Discoverer that discovered the entity.
       * \param pEntity The entity whose value has been set.
       * \param pczKey The key that was requested. This string is owned by the client
       *             SDK once this call is made.
       * \param u32Error The outcome of the request: VNCDiscoverySDKErrorNone if the
       *              request was completed successfully,
       *              VNCDiscoverySDKErrorNotSupported if the key is not supported by
       *              the Entity, or is read-only, VNCDiscoverySDKErrorOutOfMemory if
       *              there is not enough memory to complete the request,
       *              VNCDiscoverySDKErrorInvalidParameter if the Discoverer, Entity
       *              or Key was NULL, or the value was set to NULL when the Entity
       *              doesn't support un-setting of the key,
       *              VNCDiscoverySDKErrorUnkownEntity if the Entity doesn't exist, or
       *              VNCDiscoverySDKErrorCancelled if the Discoverer was stopped
       *              while processing the request, or another error specific to the Discoverer.
       * \sa     vRegisterDiscoveryCallbacks()
       **************************************************************************/
      static t_Void VNCCALL vPostEntityValueCallback(t_Void *pSDKContext,
               VNCDiscoverySDKRequestId u32RequestId,
               VNCDiscoverySDKDiscoverer *pDiscoverer,
               VNCDiscoverySDKEntity *pEntity, t_Char *pczKey, t_Char *pczValue,
               VNCDiscoverySDKError u32Error);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vFetchEntityValueCallback(t_Void *pSDKContext...);
       ***************************************************************************/
      /*!
       * \fn  vFetchEntityValueCallback(t_Void *pSDKContext,
       *       VNCDiscoverySDKRequestId requestId,
       *       VNCDiscoverySDKDiscoverer *pDiscoverer,
       *       VNCDiscoverySDKEntity *pEntity, t_Char *pKey,
       *       VNCDiscoverySDKError error, t_Char *pValue);
       * \brief    RealVNC SDK callback registered to receive the response for
       *          setting a value specified during VNCDiscoverySDKFetchEntityValue
       *          method provided by RealVNC DiscovererSDK
       *
       * \param pSDKContext The SDK context that was set by the client application for
       *                 callbacks.
       * \param u32RequestId The ID of the request. This was given to the client
       *                  application when the application made the request
       *                  initially.
       * \param pDiscoverer The Discoverer that discovered the entity.
       * \param pEntity The entity whose value has been retrieved.
       * \param pczKey The key that was requested. This string is owned by the client
       *             SDK once this call is made. The string can be NULL if an Out of
       *             Memory error occurred.
       * \param u32Error The outcome of the request: VNCDiscoverySDKErrorNone if the
       *              request was completed successfully,
       *              VNCDiscoverySDKErrorNotSupported if the key is not supported by
       *              the Entity, VNCDiscoverySDKErrorOutOfMemory if there is not
       *              enough memory to complete the request,
       *              VNCDiscoverySDKErrorInvalidParameter if the Entity or Key was
       *              NULL, VNCDiscoverySDKErrorUnkownEntity if the Entity doesn't exist,
       *              VNCDiscoverySDKErrorCancelled if the Discoverer was stopped
       *              while processing the request, or another error specific to the Discoverer.
       * \param pczValue The value requested, corresponding to the key. This string is
       *               owned by the client of the SDK. This can be NULL if some error
       *               occurred.
       *
       * \see VNCDiscoverySDKFetchEntityValue
       * \sa     vRegisterDiscoveryCallbacks(),
       *          VNCDiscoverySDKFetchEntityValue
       **************************************************************************/
      static t_Void VNCCALL vFetchEntityValueCallback(t_Void *pSDKContext,
               VNCDiscoverySDKRequestId u32RequestId,
               VNCDiscoverySDKDiscoverer *pDiscoverer,
               VNCDiscoverySDKEntity *pEntity, t_Char *pczKey,
               VNCDiscoverySDKError u32Error, t_Char *pczValue);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vDAPLogCb()
       ***************************************************************************/
      /*!
       * \fn    static t_Void VNCCALL vDAPLogCb(VNCDAPClient *poVNCDAPClient, 
       *             t_Void *poContext, 
       *             const t_Char *pcocCategory, 
       *             t_S32 s32Severity, 
       *             const t_Char *pcocText)
       * \brief  For the DAP SDK output logging  
       * \param  poVNCDAPClient :  [IN] DAP Client
       * \param  poContext      :  [IN] Context pointer
       * \param  pcocCategory   :  [IN] Category of the log
       * \param  s32Severity    :  [IN] Severity of the log
       * \param  pcocText       :  [IN] Text of th elog
       * \retval t_Void
       **************************************************************************/
      static t_Void VNCCALL vDAPLogCb(VNCDAPClient *pVNCDAPClient, 
         t_Void *poContext, 
         const t_Char *pcocCategory, 
         t_S32 s32Severity, 
         const t_Char *pcocText);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vGetDeviceVersions()
       ***************************************************************************/
      /*!
       * \fn    t_Void vGetDeviceVersions(const t_U32 cou32DevHndl,
       *             t_U32& u32MajorVersion,
       *             t_U32& u32MinorVersion)
       * \brief  To get the device major version and minor versions
       * \param  cou32DevHndl    :  [IN] Device ID
       * \param  u32MajorVersion :  [OUT] Major Version of the device
       * \param  u32MinorVersion :  [OUT] Minor version of the device
       * \retval t_Void
       **************************************************************************/
      t_Void vGetDeviceVersions(const t_U32 cou32DevHndl,
         t_U32& u32MajorVersion,
         t_U32& u32MinorVersion)const;

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bCreateClient()
       ***************************************************************************/
      /*!
       * \fn    t_Bool bCreateClient(const VNCDAPClientCallbacks& oDAPCallbacks,
       *        t_U32 u32MajorVer,
       *        t_U32 u32MinorVer)
       * \brief  Create the DAP Client with the requested versions
       * \param  oDAPCallbacks   :  [IN] Const reference to Callback structure
       * \param  u32MajorVersion :  [OUT] Major Version of the device
       * \param  u32MinorVersion :  [OUT] Minor version of the device
       * \retval t_Bool
       **************************************************************************/
      t_Bool bCreateClient(const VNCDAPClientCallbacks& oDAPCallbacks,
         t_U32 u32MajorVer,
         t_U32 u32MinorVer);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncCmdDAP:: vDestroyAttestationResp()
       ***************************************************************************/
      /*!
       * \fn      t_Void vDestroyAttestationResp(VNCDAPAttestationResponse *prVNCAttestationResp)
       * \brief   Destroys a VNCDAPAttestationResponse created by
       *          vRequestDAPAttestation
       * \param   prVNCAttestationResp : [IN]  Attestation Response
       * \retval  t_Void
       * \sa      vRequestDAPAttestation
       **************************************************************************/
      t_Void vDestroyAttestationResp( VNCDAPAttestationResponse *prVNCAttestationResp)const; 

       /***************************************************************************
       ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bPerformDAPAttestation()
       ***************************************************************************/
      /*!
       * \fn    t_Bool vPerformDAPAttestation
       * \brief  To perform the DAP attestation
       * \param  cocComponentId       :  [IN] Component for which attestation needs to be done
       * \param  u32MajorVer          :  [IN] Major version of the device
       * \param  u32MinorVer          :  [IN] Minor version of the device
       * \param  rfszVNCApplicationPublicKey  : [OUT] VNC Application Public Key 64-bit encoded
       * \param  rfszUPnPApplicationPublicKey :[OUT] UPnPApplication Public Key 64-bit encoded
       * \param  rfenAttestationError :[OUT] Attestation Error
       * \retval VNCDAPError
       **************************************************************************/
      t_Bool bPerformDAPAttestation( const t_Char *cocComponentId,
         t_U32 u32MajorVer,
         t_U32 u32MinorVer,
         t_String& rfszVNCApplicationPublicKey,
         t_String& rfszUPnPApplicationPublicKey,
         t_String& rfszVNCUrl,
         VNCDAPError& enAttestationError,
         const t_U32 cou32DeviceHandle,
         t_Bool& rfbVNCAvailInDAPSet);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclMLVncCmdDAP:: vTerminateDAPServer()
       ***************************************************************************/
      /*!
       * \fn      t_Void vTerminateDAPServer(const t_U32 coU32DeviceHandle)
       * \brief   Terminates DAP Server and disconnects DAP Client
       * \param   coU32DeviceHandle : [IN] Device Handle
       * \retval  t_Void
       **************************************************************************/
      t_Void vTerminateDAPServer(const t_U32 coU32DeviceHandle );

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclMLVncCmdDAP:: bValidateDAPServerURL()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bValidateDAPServerURL
       * \brief   validates the upnl url retrieved from discoverer
       * \param   rfrszAttestedUrl : [IN] Attested upnp url
       * \param   coU32DeviceHandle : [IN] Device Handle
       * \retval  t_Void
       **************************************************************************/
      t_Bool bValidateDAPServerURL(t_String &rfrszAttestedUrl, const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vDAPThreadStartedCb()
       ***************************************************************************/
      /*!
       * \fn    static t_Void VNCCALL vDAPThreadStartedCb(VNCDAPClient *pVNCDAPClient, 
       *             t_Void *pContext)
       * \brief  To inform that the DAP thread is started 
       * \param  pVNCDAPClient :  [IN] DAP Client
       * \param  pContext      :  [IN] Context pointer
       * \retval t_Void
       **************************************************************************/
      static t_Void VNCCALL vDAPThreadStartedCb(VNCDAPClient *pVNCDAPClient, 
         t_Void *pContext);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vDAPThreadStoppedCb()
       ***************************************************************************/
      /*!
       * \fn    static t_Void VNCCALL vDAPThreadStoppedCb(VNCDAPClient *pVNCDAPClient, 
       *             t_Void *pContext)
       * \brief  To inform that the DAP thread is stopped 
       * \param  pVNCDAPClient :  [IN] DAP Client
       * \param  pContext      :  [IN] Context pointer
       * \retval t_Void
       **************************************************************************/
      static t_Void VNCCALL vDAPThreadStoppedCb(VNCDAPClient *pVNCDAPClient, 
         t_Void *pContext);

      //! VNCDAPSDK object pointing to Device Attestation Protocol SDK of RealVNC
      VNCDAPSDK *m_pDAPsdk;

      //! DAP clients used for Device attestation of a DAP Server
      VNCDAPClient* m_pDAPClient[cou8ML_Num_DAPClients];

      /*! member variable to store the listner ID obatained during the callback
       registration with discovererSDK*/
      t_U32 m_u32ListnerID;

      //! message context utility
      static MsgContext m_oMsgContext;

      //! DAP AppID for current device
      t_U32 m_u32DAPAppID;

      //! Current DAP Client
      t_U8 m_u8DAPClientIndex;

};

#endif /* SPI_MLVNCCMDDAP_H_ */
