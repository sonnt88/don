/*!
 *******************************************************************************
 * \file              spi_tclMLVncCmdDAP.cpp
 * \brief             RealVNC Wrapper for DAP
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    VNC Wrapper for implementing the client side Device
 Attestation Protocol (DAP)
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 05.09.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 11.12.2013 |  Shiva Kumar Gurija          | Updated for ML1.0 & ML1.1 DAP Clients
 11.04.2013 |  Shiva Kumar Gurija          | Updated the Attestation Response
                                             Handling
 24.04.2014 |  Shiva kumar Gurija          | XML Validation
 24.04.2014 |  Shiva kumar Gurija          | Set Timeout for DAP attestation Request
                                             - Fix for High CPU Consumption issue
06.05.2015  |Tejaswini HB                  |Lint Fix
12.05.2015  | Shiva kumar Gurija           | Changes to perform DAP attestation
                                                For ML1.2 or above Phones
 21.05.2014 | Shiva Kumar Gurija           | Changes to use ML1.x or above Phones

 27.05.2015 |  Tejaswini H B(RBEI/ECP2)    | Added Lint comments to suppress C++11 Errors
 21.05.2014 | Shiva Kumar Gurija           | Changes to use ML1.x or above Phones
 01.06.2015 |  Shiva kumar Gurija          | Set names to VNC Threads
 03.07.2015 | Shiva Kumar Gurija           | improvements in ML Version checking

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

/** Utilities **/
#include "FileHandler.h"
#include "StringHandler.h"
#include "MsgContext.h"
#include "ThreadNamer.h"

/** Specific Headers **/
#include "SPITypes.h"
#include "spi_tclMLVncDiscovererDataIntf.h"
#include  "spi_tclMLVncDiscoverer.h"
#include "spi_tclMLVncMsgQInterface.h"
#include "spi_tclMLVncCmdDAP.h"
#include "spi_tclVncSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCmdDAP.cpp.trc.h"
   #endif
#endif

using namespace spi::io;


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
enum enDAPRespID
{
   u32LAUNCHDAP = 1, // 0 reserved for no request ID
   u32FETCHURL_REQUEST = 2,
   u32TERMINATE_DAP = 3
};
;


//! Static variables
static const t_Char cszTerminalModeVNCServer[BUFFERSIZE] = "TerminalMode:VNC-Server";
static const t_Char cszTerminalModeUPnPServer[BUFFERSIZE] = "TerminalMode:UPnP-Server";
static const VNCDiscoverySDKTimeoutMicroseconds cou32DAPTimeout = 5 * 1000* 1000;
//timer in milliseconds to DAP SDK, to send response for DAP attestation request(blocking)
static const t_U32 cou32DAPSDKTimeout = 10000;
static const t_String cszEmptyStr = "";
MsgContext spi_tclMLVncCmdDAP::m_oMsgContext;


/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncCmdDAP::spi_tclMLVncCmdDAP();
 ***************************************************************************/
spi_tclMLVncCmdDAP::spi_tclMLVncCmdDAP() :
                  m_pDAPsdk(NULL),
                  m_u32ListnerID(0)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::spi_tclMLVncCmdDAP Entered \n"));

   m_pDAPsdk = new VNCDAPSDK;
   SPI_NORMAL_ASSERT(NULL == m_pDAPsdk);

   for(t_U8 u8Index=0; u8Index<cou8ML_Num_DAPClients;u8Index++)
   {
      m_pDAPClient[u8Index]=NULL;
   }//for(t_U8 u8Index=0; u8In

}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclMLVncCmdDAP::~spi_tclMLVncCmdDAP()
 ***************************************************************************/
spi_tclMLVncCmdDAP::~spi_tclMLVncCmdDAP()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::~spi_tclMLVncCmdDAP Entered \n"));

   for(t_U8 u8Index=0; u8Index<cou8ML_Num_DAPClients;u8Index++)
   {
      m_pDAPClient[u8Index]=NULL;
   }//for(t_U8 u8Index=0; u8In

   RELEASE_MEM(m_pDAPsdk);

}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncCmdDAP:: bIntializeDAPSDK()
 ** ***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bIntializeDAPSDK()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bIntializeDAPSDK Entered \n"));
   t_Bool bInitDap = false;

   if (
      (NULL != m_pDAPsdk) 
      &&
      (VNCDAPErrorNone == VNCDAPSDKInitialize(m_pDAPsdk,sizeof(*m_pDAPsdk)))
      )
   {
      //Get the discovery sdk and register for listeners
      spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
      VNCDiscoverySDK* poDiscSDK = oDiscDataIntf.poGetDiscSDK();
      //Get the ML discoverer. 
      VNCDiscoverySDKDiscoverer* poMLDisc = oDiscDataIntf.poGetMLDisc();

      bInitDap = bAddDiscoveryListener(poDiscSDK,poMLDisc);
      //@note: If a new discoverer is added. addDiscovery Listener should be called  
      //for that discoverer also
   } //if((NULL != m_pDAPsdk) 

   return bInitDap;

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncCmdDAP:: vUnIntializeDAPSDK()
 ***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vUnIntializeDAPSDK()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::enUnIntializeDAPSDK Entered \n"));
   //Get the discovery sdk and register for listeners
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   VNCDiscoverySDK* poDiscSDK = oDiscDataIntf.poGetDiscSDK();
   //Get the ML discoverer. If a new discoverer is added. addDiscovery Listener should be called 
   //and then remove discovery Listeners should also be called.
   VNCDiscoverySDKDiscoverer* poMLDisc = oDiscDataIntf.poGetMLDisc();

   if(
      ( NULL != poDiscSDK )
      &&
      (NULL != poMLDisc)
      )
   {
      //Remove the discoverer Listeners
      t_S32 s32Error = poDiscSDK->removeDiscovererListener(poMLDisc,m_u32ListnerID);
      if (cs32MLDiscoveryErrorNone != s32Error)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdDAP: Listener unregistraction failed with error =%d ", s32Error));
      } // if (cs32MLDiscoveryErrorNone != s32Error)
   } //if(( NULL != poDiscSDK )

   if (NULL != m_pDAPsdk)
   {
      m_pDAPsdk->vncDAPSDKUninitialize();
   } //if (NULL != m_pDAPsdk)
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdDAP::bCreateDAPClient
 ***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bCreateDAPClient()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bCreateDAPClient Entered \n"));
   t_Bool bCreateDapClientRes = false;

   //!holds callbacks for registration with DAPSDK (presently not populated)
   VNCDAPClientCallbacks oDAPCallbacks = { 0 };

   //DAP Logging call back is added for debugging
   oDAPCallbacks.pLogCallback = &vDAPLogCb;
   oDAPCallbacks.pThreadStartedCallback = &vDAPThreadStartedCb;
   oDAPCallbacks.pThreadStoppedCallback = &vDAPThreadStoppedCb;

   //!Create the DAP Client. m_pDAPClient will be allocated by SDK hence NULL
   if (
      (NULL == m_pDAPClient[cou8ML_1_0_DapClient])
      &&
      (NULL == m_pDAPClient[cou8ML_1_1_DapClient]) 
      && 
      (NULL != m_pDAPsdk)
      &&
      //If the creation of ML1.0 DAP Client is successful, create ML1.1 Client and return success
      (true == bCreateClient(oDAPCallbacks,scou32ML_Major_Version_1,scou32ML_Minor_Version_0))
      )
   {
      bCreateDapClientRes = bCreateClient(oDAPCallbacks,scou32ML_Major_Version_1,scou32ML_Minor_Version_1);
   } // if ((NULL == m_pDAPClient[cou8ML_1_0_DapClient])&&(NUL

   return bCreateDapClientRes;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdDAP::vDestroyDAPClient()
 ***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vDestroyDAPClient()const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bDestroyDAPClient Entered \n"));

   //Destroy all the created DAP Clients
   if(NULL != m_pDAPsdk)
   {
      VNCDAPError enDestroyClientError = VNCDAPErrorNone;
      if(NULL != m_pDAPClient[cou8ML_1_0_DapClient])
      {
         enDestroyClientError = m_pDAPsdk->vncDAPClientDestroy(m_pDAPClient[cou8ML_1_0_DapClient]);
         if(VNCDAPErrorNone != enDestroyClientError)
         {
            ETG_TRACE_ERR(("bDestroyDAPClient1.0 Error  %d", enDestroyClientError));
         }//if(VNCDAPErrorNone != enDestroyClientError)
      }//if(NULL != m_pDAPClient[cou8ML_1_0_DapClient])
      if(NULL != m_pDAPClient[cou8ML_1_1_DapClient])
      {
         enDestroyClientError = m_pDAPsdk->vncDAPClientDestroy(m_pDAPClient[cou8ML_1_1_DapClient]);
         if(VNCDAPErrorNone != enDestroyClientError)
         {
            ETG_TRACE_ERR(("bDestroyDAPClient1.1 Error  %d", enDestroyClientError));
         }//if(VNCDAPErrorNone != enDestroyClientError)
      }//if(NULL != m_pDAPClient[cou8ML_1_1_DapClient])

   } //if ((NULL != m_pDAPsdk) && (NULL != m_pDAPClient

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdDAP::vLaunchDAP(const trUserContext ...);
 ***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vLaunchDAP(const trUserContext corUserContext,
         t_U32 u32DAPAppID, t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vLaunchDAP: Device-0x%x App-0x%x ",
      u32DeviceHandle,u32DAPAppID));

   m_u32DAPAppID = u32DAPAppID;
   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rvncDeviceHandles = oDiscDataIntf.corfrGetDiscHandles(u32DeviceHandle);
   t_String szDAPAppID = oDiscDataIntf.szGetAppID(u32DeviceHandle,u32DAPAppID);

   ETG_TRACE_USR2(("spi_tclMLVncCmdDAP::bLaunchDAP szDAPAppID = %s \n", szDAPAppID.c_str()));

   //! Launch DAP server on ML Server
   if(
      (NULL != rvncDeviceHandles.poDiscoverySdk)
      &&
      (NULL != rvncDeviceHandles.poDiscoverer)
      &&
      (NULL != rvncDeviceHandles.poEntity)
      )
   {
      t_U32 u32LaunchAppRequest =
         const_cast<VNCDiscoverySDK*>(rvncDeviceHandles.poDiscoverySdk)->postEntityValueRespondToListener(
         m_u32ListnerID,
         const_cast<VNCDiscoverySDKDiscoverer*>(rvncDeviceHandles.poDiscoverer),
         const_cast<VNCDiscoverySDKEntity*>(rvncDeviceHandles.poEntity),
         szDAPAppID.c_str(),
         VNCDiscoverySDKMLLaunchApplicationValue,
         cou32DAPTimeout);

      //Store the request ID. Will be used in callback function
       trMsgContext rMsgCtxt;
       rMsgCtxt.rUserContext = corUserContext;
       rMsgCtxt.rAppContext.u32ResID = u32LAUNCHDAP;
       m_oMsgContext.vAddMsgContext(u32LaunchAppRequest, rMsgCtxt);

   } // if ((NULL != rvncDeviceHandles.poDiscoverySdk)...
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdDAP::vRequestDAPAttestation()
***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vRequestDAPAttestation(const t_U32 cou32DeviceHandle,
                                                  const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vRequestDAPAttestation:Device-0x%x \n",cou32DeviceHandle));
   VNCDAPError enAttestationError = VNCDAPErrorFailed;

   t_U32 u32MajorVer = 0;
   t_U32 u32MinorVer = 0;

   //Fetch the Major version and Minor version of the device
   vGetDeviceVersions(cou32DeviceHandle,u32MajorVer,u32MinorVer);
   //VNCDAPAttestationResponse *prVNCAttestationResp = NULL;

   ETG_TRACE_USR2(("spi_tclMLVncCmdDAP::vRequestDAPAttestation Major Ver-%d Minor ver-%d \n",
      u32MajorVer,u32MinorVer));

   MLAttestationResponseMsg oAttestResp;
   t_String szVNCApplicationPublicKey;
   t_String szUPnPApplicationPublickKey;
   t_String szVNCUrl;
   t_Bool bVNCAvailInDAPSet = false;
   if(false ==
            bPerformDAPAttestation(VNCDAPComponentAll,u32MajorVer,u32MinorVer,
                     szVNCApplicationPublicKey,szUPnPApplicationPublickKey,szVNCUrl,
                     enAttestationError, cou32DeviceHandle,bVNCAvailInDAPSet))
   {
      oAttestResp.enAttestationResult = e8MLATTESTATION_DIFFERENT_URL;
   }

   vTerminateDAPServer(cou32DeviceHandle);

   spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      if (e8MLATTESTATION_DIFFERENT_URL != oAttestResp.enAttestationResult)
      {
         oAttestResp.enAttestationResult = enGetAttestationResult(enAttestationError);
      }
      ETG_TRACE_USR4(("spi_tclMLVncCmdDAP::vRequestDAPAttestation:VNC Application Public key - %s",
         szVNCApplicationPublicKey.c_str()));
      ETG_TRACE_USR4(("spi_tclMLVncCmdDAP::vRequestDAPAttestation:UPnP Application Public key - %s",
         szUPnPApplicationPublickKey.c_str()));
      ETG_TRACE_USR4(("spi_tclMLVncCmdDAP::vRequestDAPAttestation:VNC Server URL - %s",
         szVNCUrl.c_str()));
      if(NULL != oAttestResp.pszVNCApplicationPublicKey64)
      {
         *(oAttestResp.pszVNCApplicationPublicKey64) = szVNCApplicationPublicKey.c_str();
      }//if(NULL != oAttestResp.pszVNCApplicationPublicKey64)
      if(NULL != oAttestResp.pszUPnPApplicationPublicKey64)
      {
         *(oAttestResp.pszUPnPApplicationPublicKey64) = szUPnPApplicationPublickKey.c_str();
      }//if(NULL != oAttestResp.psz
      if(NULL != oAttestResp.pszVNCUrl)
      {
         *(oAttestResp.pszVNCUrl) = szVNCUrl.c_str();
      }//if(NULL != oAtt

      oAttestResp.u32DeviceHandle = cou32DeviceHandle;
      oAttestResp.vSetUserContext(corfrUsrCntxt);
      oAttestResp.bVNCAvailInDAPAttestResp = bVNCAvailInDAPSet ;
      poMsgQInterface->bWriteMsgToQ(&oAttestResp, sizeof(oAttestResp));
   } // if (NULL != poMsgQInterface)
}

/***************************************************************************
 *********************************PRIVATE************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bAddDiscoveryListener()
 ***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bAddDiscoveryListener(VNCDiscoverySDK* poDiscSDK, 
                                                 VNCDiscoverySDKDiscoverer* poDisc)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bAddDiscoveryListener Entered \n"));

   t_Bool bAddListenerRes = false;
   VNCDiscoverySDKCallbacks rDiscoverySDKCallbacks = { 0 };

   //! callback registration for asynchronous calls to discovery sdk
   rDiscoverySDKCallbacks.fetchEntityValue = &vFetchEntityValueCallback;
   rDiscoverySDKCallbacks.postEntityValue = &vPostEntityValueCallback;

   if (
      (NULL != poDiscSDK) 
      && 
      (NULL != poDisc)
      )
   {
      VNCDiscoverySDKError u32AddListnerError =
         poDiscSDK->addDiscovererListener(poDisc,
         this,
         &rDiscoverySDKCallbacks,
         sizeof(rDiscoverySDKCallbacks),
         &m_u32ListnerID);
      bAddListenerRes = (VNCDiscoverySDKErrorNone == u32AddListnerError);

      if (false == bAddListenerRes)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdDAP::bAddDiscoveryListener Error = %d \n",u32AddListnerError));
      } // (false == bAddListenerRes)
   } // if ((NULL != poDiscSDK) && (NULL != poDisc))

   return bAddListenerRes;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bAddVNCLicense()
 ***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bAddVNCLicense(VNCDAPClient* pDAPClient)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bAddVNCLicense Entered \n"));
   t_Bool bAddLicense = false , bReadRes = false;
   t_String szLicensePath,szLicenseFile;

   spi_tclVncSettings *poVncSettings = spi_tclVncSettings::getInstance();

   if(NULL != poVncSettings)
   {
      poVncSettings->vGetLicenseFilePath(szLicensePath);
   }

   //! Read the license file
   FileHandler oLicenseFile(szLicensePath.c_str(), SPI_EN_RDONLY);

   if (true == oLicenseFile.bIsValid())
   {
      t_S32 s32LicenseFileSize = oLicenseFile.s32GetSize();

      if (0 < s32LicenseFileSize)
      {
         t_Char czLicenseFile[s32LicenseFileSize + 1];
         bReadRes = oLicenseFile.bFRead(czLicenseFile, s32LicenseFileSize);

         if (true == bReadRes)
         {
            czLicenseFile[s32LicenseFileSize] = '\0';
            szLicenseFile.assign(czLicenseFile);
         }//if (true == bReadRes)

      }//if (0 <= s32LicenseFileSize)

   }//if (true == oLicenseFile.bIsValid())
   //! add license
   if (
      (true == bReadRes) 
      && 
      (NULL != m_pDAPsdk) 
      && 
      (NULL != pDAPClient)
      )
   {
      //Serial Number is unused. so Serial Number and Serial Number size are set to NULL
      VNCDAPError enLicenseError = 
         m_pDAPsdk->vncDAPClientAddLicense(pDAPClient, 
         szLicenseFile.c_str(),
         NULL,
         0);
      bAddLicense = (VNCDAPErrorNone == enLicenseError);
      if (false == bAddLicense)
      {
         ETG_TRACE_ERR(("Adding license failed with error %d ", enLicenseError));
      } // if (false == bAddLicense)
   } // if ((true == bReadRes) && (NULL != m_pDAPsdk) && (NULL != poDAPClient))

   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bAddVNCLicense left with result = %u \n", ETG_ENUM(BOOL, bAddLicense)));
   return bAddLicense;
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bSetRootTrustCertificate()
 ***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bSetRootTrustCertificate(VNCDAPClient* poDAPClient)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bSetRootTrustCertificate Entered \n"));
   t_Bool bSetRootCertiRes = false;

   t_String szRootTrustCerti;

   spi_tclVncSettings *poVncSettings = spi_tclVncSettings::getInstance();

   if(NULL != poVncSettings)
   {
      poVncSettings->vGetMLCertificatePath(szRootTrustCerti);
   }

   //! Read trust certificate
   FileHandler oTrustCerti(szRootTrustCerti.c_str(), SPI_EN_RDONLY);
   t_S32 s32TrustCertiSize = oTrustCerti.s32GetSize();
   t_U8 czTrustCerti[s32TrustCertiSize+1];

   t_Bool bReadRes = oTrustCerti.bFRead(czTrustCerti, s32TrustCertiSize);
   czTrustCerti[s32TrustCertiSize] = '\0';

   //! Set trust root
   if ((true == bReadRes) && (NULL != m_pDAPsdk) && (NULL != poDAPClient))
   {
      VNCDAPError enVNCRootCertiError =
         m_pDAPsdk->vncDAPClientSetTrustRoot(poDAPClient,
                        czTrustCerti,
                        s32TrustCertiSize);
      bSetRootCertiRes = (enVNCRootCertiError == VNCDAPErrorNone);
      const t_Char* pczDAPError =  m_pDAPsdk->vncDAPGetErrorName(enVNCRootCertiError);

      if ((false == bSetRootCertiRes) && (NULL != pczDAPError))
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdDAP::bSetRootTrustCertificate Error = %s /n ", pczDAPError));
      } // if (false == bSetRootCertiRes)
   } // if ((true == bReadRes) && (NULL != m_pDAPsdk) && (NULL != poDAPClient))

   return bSetRootCertiRes;
}


/***************************************************************************
 ** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bSetDAPServerUrl()
 ***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bSetDAPServerUrl(t_String szDAPServerUrl,VNCDAPClient* poDAPClient)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bSetDAPServerUrl Entered \n"));
   t_Bool bSetDAPServerRes = false;
   if ((NULL != m_pDAPsdk) && (NULL != poDAPClient))
   {
      VNCDAPError enSetDapUrlError = m_pDAPsdk->vncDAPClientSetServerURL(poDAPClient, szDAPServerUrl.c_str());
      bSetDAPServerRes = (enSetDapUrlError == VNCDAPErrorNone);

      const t_Char* pczDAPError =  m_pDAPsdk->vncDAPGetErrorName(enSetDapUrlError);
      if ((false == bSetDAPServerRes) && (NULL != pczDAPError))
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdDAP::bSetDAPServerUrl Error = %s /n ", pczDAPError));
      } // if(false == bSetDAPServerRes)
   } // if ((NULL != m_pDAPsdk) && (NULL != poDAPClient))
   return bSetDAPServerRes;
}


/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vFetchDAPServerUrl()
 ***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vFetchDAPServerUrl(const trUserContext corUserContext, 
                                              t_String szDAPAppID, 
                                              VNCDiscoverySDKDiscoverer *poDisc,
                                              VNCDiscoverySDKEntity *poEntity)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vFetchDAPServerUrl Entered \n"));

   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   VNCDiscoverySDK* poDiscSDK = oDiscDataIntf.poGetDiscSDK();
   if (
      (NULL != poDiscSDK) 
      && 
      (NULL != poDisc)
      && 
      (NULL != poEntity)
      )
   {
      std::string szDapUrlkey = szDAPAppID + ":uri";
      t_U32 u32FetchUrlRequest =
         poDiscSDK->fetchEntityValueRespondToListener(
         m_u32ListnerID,
         poDisc,
         poEntity,
         szDapUrlkey.c_str(),
         cou32DAPTimeout);

      //Store the request ID. Will be used in callback function
       trMsgContext rMsgCtxt;
       rMsgCtxt.rUserContext = corUserContext;
       rMsgCtxt.rAppContext.u32ResID = u32FETCHURL_REQUEST;
       m_oMsgContext.vAddMsgContext(u32FetchUrlRequest, rMsgCtxt);
   }//if((NULL != poDiscSDK) 
   else
   {
      ETG_TRACE_ERR(("poDiscSDK/poDisc/poEntity is/are NULL \n"));
   }//else
}

/***************************************************************************
 ** FUNCTION: vGetComponentID(t_String &rfszComponentId,...)
 ***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vGetComponentID(t_String &rfszComponentId,
         const tenMLComponentID coenComponentID)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vGetComponentID Entered \n"));
   switch (coenComponentID)
   {
      case e8TERMINALMODE_VNCSERVER:
      {
         rfszComponentId = "TerminalMode:VNC-Server";
         break;
      }
      case e8TERMINALMODE_UPNPSERVER:
      {
         rfszComponentId = "TerminalMode:UPnP-Server";
         break;
      }
      case e8TERMINALMODE_RTPSERVER:
      {
         rfszComponentId = "TerminalMode:RTP-Server";
         break;
      }
      case e8TERMINALMODE_RTPCLIENT:
      {
         rfszComponentId = "TerminalMode:RTP-Client";
         break;
      }
      case e8TERMINALMODE_CDBENDPOINT:
      {
         rfszComponentId = "MirrorLink:CDB-Endpoint";
         break;
      }
      case e8TERMINALMODE_MLDEVICE:
      {
         rfszComponentId = "MirrorLink:Device";
         break;
      }
      default:
      {
         rfszComponentId = "";
         ETG_TRACE_ERR(("spi_tclMLVncCmdDAP::DAP Component ID not known \n"));
         break;
      }
   } // switch (coenComponentID)
}

/***************************************************************************
 ** FUNCTION: tenMLAttestationResult enGetAttestationResult
 ***************************************************************************/
tenMLAttestationResult spi_tclMLVncCmdDAP::enGetAttestationResult(
         const VNCDAPError coenVNCDAPError)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vGetAttestationError Entered \n"));
   tenMLAttestationResult enAttestationRes;
   switch (coenVNCDAPError)
   {
      case VNCDAPErrorNone:
      {
         enAttestationRes = e8MLATTESTATION_SUCCESSFUL;
         break;
      }

      case VNCDAPResultComponentNotImplemented:
      {
         enAttestationRes = e8MLCOMPONENT_NOTEXISTING;
         break;
      }

      case VNCDAPResultVersionUnsupported:
      {
         enAttestationRes = e8MLVERSION_NOTSUPPORTED;
         break;
      }

      case VNCDAPResultUnknownTrustRoot:
      {
         enAttestationRes = e8MLUNKNOWN_TRUSTROOT;
         break;
      }

      case VNCDAPResultUnknownComponent:
      {
         enAttestationRes = e8MLUNKNOWN_COMPONENTID;
         break;
      }

      case VNCDAPResultAttestationFailed:
      {
         enAttestationRes = e8MLATTESTATION_FAILED;
         break;
      }
      default:
      {
         enAttestationRes = e8MLATTESTATION_FAILED;
         break;
      }
   } //  switch (enAttestationError)
   return enAttestationRes;
}

//!Private Static RealVNC SDK Callbacks
/***************************************************************************
 ** FUNCTION: static t_Void VNCCALL
 ** spi_tclMLVncCmdDAP::vPostEntityValueCallback(t_Void *pSDKContext...)
 ***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdDAP::vPostEntityValueCallback(
   t_Void *pSDKContext,
         VNCDiscoverySDKRequestId u32RequestId,
         VNCDiscoverySDKDiscoverer *pDiscoverer, 
         VNCDiscoverySDKEntity *pEntity,
         t_Char *pczKey, 
         t_Char *pczValue, 
         VNCDiscoverySDKError u32Error)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vPostEntityValueCallback: Error-%d \n",u32Error));

   if ((NULL != pczKey) && (NULL != pczValue) && (NULL != pDiscoverer)
            && (NULL != pSDKContext) && (VNCDiscoverySDKErrorNone == u32Error))
   {
      ETG_TRACE_USR4(("PostentityValueCallback pczKey=%s \n ", pczKey));
      ETG_TRACE_USR4(("PostentityValueCallback pczValue=%s \n ", pczValue));

      trMsgContext rFetchCbCtxt = m_oMsgContext.rGetMsgContext(u32RequestId);
      trUserContext rUserContext = rFetchCbCtxt.rUserContext;

      if (u32LAUNCHDAP == rFetchCbCtxt.rAppContext.u32ResID)
      {
         spi_tclMLVncCmdDAP *pMLVncCmdDAP = static_cast<spi_tclMLVncCmdDAP*>(pSDKContext);
            t_String szDAPAppID = pczKey;
            pMLVncCmdDAP->vFetchDAPServerUrl(rUserContext, szDAPAppID,pDiscoverer,pEntity);
      } //  if (u32LAUNCHDAP == rFetchCbCtxt.rAppContext.u32ResID)


      if (u32TERMINATE_DAP == rFetchCbCtxt.rAppContext.u32ResID)
      {
         spi_tclMLVncCmdDAP *pMLVncCmdDAP = static_cast<spi_tclMLVncCmdDAP*>(pSDKContext);
         if (NULL != pMLVncCmdDAP->m_pDAPClient[pMLVncCmdDAP->m_u8DAPClientIndex])// No need to check pMLVncCmdDAP for NULL, as already check is done for pSDKContext
         {
            pMLVncCmdDAP->m_pDAPsdk->vncDAPClientDisconnect(pMLVncCmdDAP->m_pDAPClient[pMLVncCmdDAP->m_u8DAPClientIndex]);
         }
      } //  if (u32LAUNCHDAP == rFetchCbCtxt.rAppContext.u32ResID)


   } //  if ((NULL != pczKey) && (NULL != pczValue) && (NULL != pDiscoverer)&& (NULL != pSDKContext))
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vFetchEntityValueCallback(t_Void *pSDKContext...);
 ***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdDAP::vFetchEntityValueCallback(
         t_Void *pSDKContext, 
         VNCDiscoverySDKRequestId u32RequestId,
         VNCDiscoverySDKDiscoverer *poDisc, 
         VNCDiscoverySDKEntity *poEntity,
         t_Char *pczKey, 
         VNCDiscoverySDKError error, 
         t_Char *pczValue)
{
   SPI_INTENTIONALLY_UNUSED(error);
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vFetchEntityValueCallback Entered \n"));

   t_U32 u32DeviceHandle = 0;
   if ((NULL != pczKey) && (NULL != pczValue) && (NULL != pSDKContext))
   {
      ETG_TRACE_USR2( ("FetchentityValue Callback pczKey= %s  \n", pczKey));
      ETG_TRACE_USR2( ("FetchentityValue Callback pczValue= %s  \n", pczValue));

      trMsgContext rFetchCbCtxt = m_oMsgContext.rGetMsgContext(u32RequestId);
      trUserContext rUserContext = rFetchCbCtxt.rUserContext;

      t_U32 u32MajorVer = 0;
      t_U32 u32MinorVer = 0;

      if (u32FETCHURL_REQUEST == rFetchCbCtxt.rAppContext.u32ResID)
      {
         spi_tclMLVncCmdDAP *pMLVncCmdDAP =
            static_cast<spi_tclMLVncCmdDAP*>(pSDKContext);
         t_String szDAPServerUrl = pczValue;
         //Pointer to result of fetch. populated by vncsdk hence NULL
         t_Char *pczVal = NULL;
         StringHandler oStringUtil;

         //Get the discoverer handles using Discoverer Data Interface
         spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
         VNCDiscoverySDK* poDiscSDK = oDiscDataIntf.poGetDiscSDK();

         //! Fetch the Major version and minor version of the device
         if(
            (NULL != poDiscSDK)
            &&
            (NULL != poDisc)
            &&
            (NULL != poEntity)
            )
         {
            //Fetch DeviceID
            oDiscDataIntf.vGetDeviceId(poDisc, poEntity, u32DeviceHandle );

            //Fetch Major version of the device
            t_U32 u32FetchError=
               poDiscSDK->fetchEntityValueBlocking(
               poDisc,
               poEntity,
               VNCDiscoverySDKMLRootMajorVersion,
               &(pczVal),
               VNCDiscoverySDKInstantCall);
            if ((VNCDiscoverySDKErrorNone == u32FetchError) && (NULL!= pczVal))
            {
               u32MajorVer = oStringUtil.u32ConvertStrToInt(pczVal);
            }//if ((VNCDiscoverySDKErrorNone == u32FetchError) && (NULL!= pczVal))

            //Fetch Minor version of the device
            u32FetchError =
               poDiscSDK->fetchEntityValueBlocking(
               poDisc,
               poEntity,
               VNCDiscoverySDKMLRootMinorVersion,
               &(pczVal),
               VNCDiscoverySDKInstantCall);
            if ((VNCDiscoverySDKErrorNone == u32FetchError) && (NULL!= pczVal))
            {
               u32MinorVer = oStringUtil.u32ConvertStrToInt(pczVal);
            }//if ((VNCDiscoverySDKErrorNone == u32FetchError) && (NULL!= pczVal))
            //SDK free string
            poDiscSDK->freeString(pczVal);
         } //if((NULL != poDiscSDK)

         if(true == bIsML10Device(u32MajorVer,u32MinorVer))
         {
            pMLVncCmdDAP->m_u8DAPClientIndex = cou8ML_1_0_DapClient;
         } //if(true == bIsML10Device(u32MajorVer,u32MinorVer))
         else
         {
            //use this client for all the ML1.1 or above devices
            pMLVncCmdDAP->m_u8DAPClientIndex = cou8ML_1_1_DapClient;
         }//else


         //Get the device handle and then major version and minor version
         //pass the proper DAP Client to which URL should be set
         t_Bool bLaunchDAPRes = pMLVncCmdDAP->bSetDAPServerUrl(szDAPServerUrl,pMLVncCmdDAP->m_pDAPClient[pMLVncCmdDAP->m_u8DAPClientIndex]);

         spi_tclMLVncMsgQInterface *poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
         if (NULL != poMsgQInterface)
         {
            MLLaunchDAPMsg oLauncMsg;
            oLauncMsg.bLaunchDAPRes = bLaunchDAPRes;
            oLauncMsg.u32DeviceHandle = u32DeviceHandle;
            oLauncMsg.vSetUserContext(rUserContext);
            poMsgQInterface->bWriteMsgToQ(&oLauncMsg, sizeof(oLauncMsg));
         } // if (NULL != poMsgQInterface)
      } // if (u32FETCHURL_REQUEST == rFetchCbCtxt.rAppContext.u32ResID)
   } // if ((NULL != pczKey) && (NULL != pczValue) && (NULL != pDiscoverer)&& (NULL != pSDKContext))
}


/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vDAPLogCb()
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdDAP::vDAPLogCb(VNCDAPClient *poVNCDAPClient, 
                                             t_Void *poContext, 
                                             const t_Char *pcocCategory, 
                                             t_S32 s32Severity, 
                                             const t_Char *pcocText)
{
   SPI_INTENTIONALLY_UNUSED(poContext);
   SPI_INTENTIONALLY_UNUSED(poVNCDAPClient);

   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vDAPLogCb:severity - 0x%x \n",s32Severity));
   if(NULL != pcocCategory)
   {
      ETG_TRACE_USR2(("spi_tclMLVncCmdDAP::vDAPLogCb:category - %s \n",pcocCategory));
   }//if(NULL != pcocCategory)
   if(NULL != pcocText)
   {
      ETG_TRACE_USR2(("spi_tclMLVncCmdDAP::vDAPLogCb:text - %s \n",pcocText));
   }//if(NULL != pcocText)
}


/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vGetDeviceVersions()
***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vGetDeviceVersions(const t_U32 cou32DevHndl,
                                              t_U32& u32MajorVersion,
                                              t_U32& u32MinorVersion)const
{
   u32MajorVersion = 0;
   u32MinorVersion = 0;

   //Pointer to result of fetch. populated by vncsdk hence NULL
   t_Char *pczVal = NULL;
   StringHandler oStringUtil;

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rvncDeviceHandles = oDiscDataIntf.corfrGetDiscHandles(cou32DevHndl);

   VNCDiscoverySDK* poDiscSDK = const_cast<VNCDiscoverySDK*>(rvncDeviceHandles.poDiscoverySdk) ;
   VNCDiscoverySDKDiscoverer* poDisc = const_cast<VNCDiscoverySDKDiscoverer*>(rvncDeviceHandles.poDiscoverer);
   VNCDiscoverySDKEntity* poEntity = const_cast<VNCDiscoverySDKEntity*>(rvncDeviceHandles.poEntity);

   //! Fetch the Major version and minor version of the device
   if(
      (NULL != poDiscSDK)
      &&
      (NULL != poDisc)
      &&
      (NULL != poEntity)
      )
   {
      //Fetch Major version of the device
      t_U32 u32FetchError=
         poDiscSDK->fetchEntityValueBlocking(
         poDisc,
         poEntity,
         VNCDiscoverySDKMLRootMajorVersion,
         &(pczVal),
         VNCDiscoverySDKInstantCall);
      if ((VNCDiscoverySDKErrorNone == u32FetchError) && (NULL!= pczVal))
      {
         u32MajorVersion = oStringUtil.u32ConvertStrToInt(pczVal);
      }//if ((VNCDiscoverySDKErrorNone == u32FetchError) && (NULL!= pczVal))

      //Fetch Minor version of the device
      u32FetchError =
         poDiscSDK->fetchEntityValueBlocking(
         poDisc,
         poEntity,
         VNCDiscoverySDKMLRootMinorVersion,
         &(pczVal),
         VNCDiscoverySDKInstantCall);
      if ((VNCDiscoverySDKErrorNone == u32FetchError) && (NULL!= pczVal))
      {
         u32MinorVersion = oStringUtil.u32ConvertStrToInt(pczVal);
      } //if ((VNCDiscoverySDKErrorNone == u32FetchError) && (NULL!= pczVal))
      //SDK free string
      poDiscSDK->freeString(pczVal);
   } //if((NULL != poDiscSDK)&&&..
   else
   {
      ETG_TRACE_ERR(("poDiscSDK = %d or poDisc = %d  or poEntity = %d is NULL  \n", poDiscSDK, poDisc, poEntity));
   }
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bCreateClient()
***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bCreateClient(const VNCDAPClientCallbacks& oDAPCallbacks,
                                         t_U32 u32MajorVer,
                                         t_U32 u32MinorVer)
{
   ETG_TRACE_USR1(("bCreateClient: Creating DAP Client MajorVersion = %d, MinorVersion  = %d\n", u32MajorVer,u32MinorVer ));

   t_Bool bResp = false;
   VNCDAPError enClientError = VNCDAPErrorNone;
   VNCDAPClient* poDAPClient = NULL ;

   if(NULL != m_pDAPsdk)
   {
      enClientError = m_pDAPsdk->vncDAPClientCreate(&poDAPClient,
         u32MajorVer,
         u32MinorVer,
         this,
         &oDAPCallbacks,
         sizeof(oDAPCallbacks));

      const t_Char* pczDAPError =  m_pDAPsdk->vncDAPGetErrorName(enClientError);
      if(NULL != pczDAPError)
      {
         ETG_TRACE_USR2(("bCreateClient Create DAPClient  Result =  %s", pczDAPError));
      }

      if(
         ( NULL != poDAPClient )
         &&
         ( VNCDAPErrorNone == enClientError )
         &&
         ( true == bAddVNCLicense(poDAPClient) )
         &&
         ( true == bSetRootTrustCertificate(poDAPClient))
         )
      {
         ETG_TRACE_USR2(("bCreateClient: successfully added license and Certified DAPClient \n"));
         bResp = true;

         if(VNCDAPErrorNone == m_pDAPsdk->vncDAPClientSetTimeout(poDAPClient,cou32DAPSDKTimeout))
         {
            ETG_TRACE_ERR(("bCreateClient%d.%d: Set Timeout success ", u32MajorVer,u32MinorVer));
         }//if(VNCDAPErrorNone !m_pDAPsdk->vncDAPClientSetTimeout(m_pDA

         //Assign the DAP Client address to member variable based on the Version
         if(true == bIsML10Device(u32MajorVer,u32MinorVer))
         {
            m_pDAPClient[cou8ML_1_0_DapClient] = poDAPClient;
         }//if(true == bIsML10Device(u32MajorVer,u32MinorVer)
         else if(true == bIsML11Device(u32MajorVer,u32MinorVer))
         {
            //DAP1.1 client is used for all the ML1.1 or above devices
            m_pDAPClient[cou8ML_1_1_DapClient] = poDAPClient;
         }//else if(true == bIsML11Device(u32MajorVer,u32MinorVer)
         else
         {
            ETG_TRACE_ERR(("bCreateClient: currently this version is not supported \n"));
         }//else

      } //if((VNCDAPErrorNone == enClientError)
   }//if((NULL == poDAPClient)

   return bResp;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncCmdDAP:: vDestroyAttestationResp(const t_U32 ...)
 ***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vDestroyAttestationResp( VNCDAPAttestationResponse *prVNCAttestationResp)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vDestroyAttestationResp Entered \n"));
   if ((NULL != m_pDAPsdk) && (NULL != prVNCAttestationResp))
   {
      VNCDAPError enDestroyResult = m_pDAPsdk->vncDAPAttestationResponseDestroy(prVNCAttestationResp);
      ETG_TRACE_USR2(("Attestation Response Destroy Error = %d",  enDestroyResult));
   } // if ((NULL != m_pDAPsdk) && (NULL != prVNCAttestationResp))
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdDAP:: bPerformDAPAttestation()
***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bPerformDAPAttestation( const t_Char *cocComponentId,
                                                  t_U32 u32MajorVer,
                                                  t_U32 u32MinorVer,
                                                  t_String& rfszVNCApplicationPublicKey,
                                                  t_String& rfszUPnPApplicationPublicKey,
                                                  t_String& rfszVNCUrl,
                                                  VNCDAPError& rfenAttestationError,
                                                  const t_U32 cou32DeviceHandle,
                                                  t_Bool& rfbVNCAvailInDAPSet)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::bPerformDAPAttestation()"));
   t_Bool bRetVal = false;
   rfenAttestationError = VNCDAPErrorFailed;
   size_t AttestationResponseSize = 0;
   VNCDAPAttestationResponse *prVNCAttestationResp = NULL ;
   VNCDAPClient* poDAPClient = NULL;

   //request for DAP Attestation for all components based on the device versions
   if(true == bIsML10Device(u32MajorVer,u32MinorVer))
   {
      poDAPClient = m_pDAPClient[cou8ML_1_0_DapClient];
   }//if(true == bIsML10Device(u32MajorVer,u32MinorVer))
   else 
   {
      //Use the DAP client created for ML1.1 Servers, incase if a MLS with 1.2 or above is connected.
      poDAPClient = m_pDAPClient[cou8ML_1_1_DapClient];
   }//else 


   if((NULL != m_pDAPsdk)&&(NULL != poDAPClient)&&(NULL != cocComponentId))
   {
      rfenAttestationError =
         m_pDAPsdk->vncDAPClientAttestationRequest(poDAPClient,
         cocComponentId,
         &prVNCAttestationResp,
         &AttestationResponseSize);
      ETG_TRACE_USR4(("bPerformDAPAttestation: Attestation Error - %d ",rfenAttestationError));
   }//if((NULL != m_pDAPsdk)&&(NULL != poDAPClient))

   //Get the TerminalMode:VNC-Server Application Public Key for the Content Attestation.
   //Get the TerminalMode:UPnP-Server Application Public Key for the XML validation.
   if((rfenAttestationError == VNCDAPErrorNone)&&(NULL != prVNCAttestationResp))
   {
      for(t_U8 u8Index = 0;u8Index<(prVNCAttestationResp->attestationCount);u8Index++)
      {
         if(( NULL != prVNCAttestationResp->attestations ) &&
            ( NULL != (prVNCAttestationResp->attestations[u8Index].componentId) ) )
         {
            if ((0 == strncmp(cszTerminalModeVNCServer,
               prVNCAttestationResp->attestations[u8Index].componentId,
               strlen(cszTerminalModeVNCServer))))
            {
               rfbVNCAvailInDAPSet = true;
               ETG_TRACE_USR2(("bPerformDAPAttestation: VNC-Server is lisetd in DAP Attestation Response"));
               if(NULL != (prVNCAttestationResp->attestations[u8Index].applicationPublicKeyBase64))
               {
                  ETG_TRACE_USR2(("bPerformDAPAttestation:Terminal Mode:VNC-Server Attestation Key - %s ",
                     prVNCAttestationResp->attestations[u8Index].applicationPublicKeyBase64));
                  rfszVNCApplicationPublicKey = prVNCAttestationResp->attestations[u8Index].applicationPublicKeyBase64;
               }//if(NULL != (prVNCAttestationResp->attestations[u8Index
               if(NULL != (prVNCAttestationResp->attestations[u8Index].url))
               {
                  ETG_TRACE_USR2(("bPerformDAPAttestation:Terminal Mode:VNC-Server URL - %s ",
                     prVNCAttestationResp->attestations[u8Index].url));
                  rfszVNCUrl = prVNCAttestationResp->attestations[u8Index].url;
               }//if(NULL != (prVNCAttestationResp->a
            }//if ((0 == strncmp(cszTerminalModeVNCServer,

            if ((0 == strncmp(cszTerminalModeUPnPServer,
               prVNCAttestationResp->attestations[u8Index].componentId,
               strlen(cszTerminalModeUPnPServer))))
            {
               if(NULL != (prVNCAttestationResp->attestations[u8Index].applicationPublicKeyBase64))
               {
                  ETG_TRACE_USR2(("bPerformDAPAttestation:Terminal Mode:UPnP-Server Attestation Key - %s \n",
                     prVNCAttestationResp->attestations[u8Index].applicationPublicKeyBase64));
                  rfszUPnPApplicationPublicKey = prVNCAttestationResp->attestations[u8Index].applicationPublicKeyBase64;
               }//if(NULL != (prVNCAttestationResp->attestations[u8Index].
               if(NULL != (prVNCAttestationResp->attestations[u8Index].url))
               {
                  t_String szAttestedURL =  prVNCAttestationResp->attestations[u8Index].url;
                  ETG_TRACE_USR2(("bPerformDAPAttestation:TerminalMode:UPnPServer URL - %s \n",
                     szAttestedURL.c_str() ));
                  bRetVal = bValidateDAPServerURL(szAttestedURL, cou32DeviceHandle);
               }//if(NULL != (prVNCAttestationResp->attestations[u8In
            }//if ((0 == strncmp(cszTerminalModeUPnPServer,
         }//if((NULL != prVNCAttestationResp->attestations)
      }//for(t_U8 u8Index = 0;u8Index<(prVNCAttestation
   }//if((enAttestationError == 0)&&(


   if(NULL != prVNCAttestationResp)
   {
      //Destroy Attestation Response.
      vDestroyAttestationResp(prVNCAttestationResp);
   }//if(NULL != prVNCAttestationResp)

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncCmdDAP:: vTerminateDAPServer()
 ***************************************************************************/
t_Void spi_tclMLVncCmdDAP::vTerminateDAPServer(const t_U32 coU32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vTerminateDAPServer() : "\
            "Terminating DAP server for %d\n", coU32DeviceHandle));

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rvncDeviceHandles = oDiscDataIntf.corfrGetDiscHandles(
            coU32DeviceHandle);

   VNCDiscoverySDK* poDiscSDK =
            const_cast<VNCDiscoverySDK*> (rvncDeviceHandles.poDiscoverySdk);
   VNCDiscoverySDKDiscoverer
            * poDisc =
                     const_cast<VNCDiscoverySDKDiscoverer*> (rvncDeviceHandles.poDiscoverer);
   VNCDiscoverySDKEntity* poEntity =
            const_cast<VNCDiscoverySDKEntity*> (rvncDeviceHandles.poEntity);
   t_String szDAPAppID = oDiscDataIntf.szGetAppID(coU32DeviceHandle,
            m_u32DAPAppID);

   if ((NULL != poDiscSDK) && (NULL != poEntity) && (NULL != poDisc) && (cszEmptyStr != szDAPAppID))
   {
      ETG_TRACE_USR2(("vTerminateDAPServer:Listener:%d  DAPID:%s",m_u32ListnerID,szDAPAppID.c_str()));

      t_U32 u32TerminateDAPReq = poDiscSDK->postEntityValueRespondToListener(m_u32ListnerID, poDisc,
         poEntity, szDAPAppID.c_str(),
         VNCDiscoverySDKMLTerminateApplicationValue, cou32DAPTimeout);

      if(0 != u32TerminateDAPReq)
      {
         ETG_TRACE_USR4(("vTerminateDAPServer:Request sent -%d",u32TerminateDAPReq));
      }

      //Store the request ID. Will be used in callback function
       trMsgContext rMsgCtxt;
       rMsgCtxt.rAppContext.u32ResID = u32TERMINATE_DAP;
       m_oMsgContext.vAddMsgContext(u32TerminateDAPReq, rMsgCtxt);
   }


}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncCmdDAP:: bValidateDAPServerURL
 ***************************************************************************/
t_Bool spi_tclMLVncCmdDAP::bValidateDAPServerURL(t_String &rfrszAttestedUrl,
         const t_U32 cou32DeviceHandle)
{
   t_Bool bURLValid =false;

   //Get the discoverer handles using Discoverer Data Interface
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   trvncDeviceHandles rvncDeviceHandles = oDiscDataIntf.corfrGetDiscHandles(
            cou32DeviceHandle);

   VNCDiscoverySDK* poDiscSDK =
            const_cast<VNCDiscoverySDK*> (rvncDeviceHandles.poDiscoverySdk);
   VNCDiscoverySDKDiscoverer  * poDisc =
                     const_cast<VNCDiscoverySDKDiscoverer*> (rvncDeviceHandles.poDiscoverer);
   VNCDiscoverySDKEntity* poEntity =
            const_cast<VNCDiscoverySDKEntity*> (rvncDeviceHandles.poEntity);

   //! Fetch Upnp server URl from discovery sdk
   t_String szUPnPServerURL;
   if ((NULL != poDiscSDK) && (NULL != poEntity) && (NULL != poDisc))
   {
      t_Char* pcVal = NULL;
      poDiscSDK->fetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLUPnPServerURL, &pcVal,
               VNCDiscoverySDKInstantCall);
      if (NULL != pcVal)
      {
         szUPnPServerURL = pcVal;
         poDiscSDK->freeString(pcVal);
         pcVal = NULL;
      }
   }

   //! Compare the attested url and the one fteched from discovery sdk
   if(szUPnPServerURL.size() == rfrszAttestedUrl.size())
   {
      //! Compare the protocol portion of the URLs with case insensitivity.
      //! Compare the remaining portion with case sensitivity.
      const t_Char* czSeperator = strstr(rfrszAttestedUrl.c_str(), ":");
      size_t siOffset = (NULL != czSeperator) ? (((size_t)(czSeperator - rfrszAttestedUrl.c_str())) / sizeof(t_Char)) : 0;

      bURLValid = (0 == strncasecmp(rfrszAttestedUrl.c_str(), szUPnPServerURL.c_str(), siOffset));
      bURLValid = bURLValid && (strncmp(&rfrszAttestedUrl[siOffset], &szUPnPServerURL[siOffset], (szUPnPServerURL.size()-siOffset)) == 0);
   }

   ETG_TRACE_USR2(("spi_tclMLVncCmdDAP::bValidateDAPServerURL : szUPnPServerURL = %s \n", szUPnPServerURL.c_str()));
   ETG_TRACE_USR2(("spi_tclMLVncCmdDAP::bValidateDAPServerURL : bURLValid = %d rfrszAttestedUrl = %s\n",
            bURLValid, rfrszAttestedUrl.c_str()));

   return bURLValid;

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vDAPThreadStartedCb()
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdDAP::vDAPThreadStartedCb(VNCDAPClient *pVNCDAPClient, 
                                                       t_Void *pContext)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vDAPThreadStartedCb"));
   DEFINE_THREAD_NAME("VNCDAPSDK");
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(pVNCDAPClient);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdDAP:: vDAPThreadStoppedCb()
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdDAP::vDAPThreadStoppedCb(VNCDAPClient *pVNCDAPClient, 
                                                       t_Void *pContext)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDAP::vDAPThreadStoppedCb"));
   SPI_INTENTIONALLY_UNUSED(pContext);
   SPI_INTENTIONALLY_UNUSED(pVNCDAPClient);
}

//lint –restore
///////////////////////////////////////// EOF ////////////////////////////////////////////////////////
