/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespDAP.h
 * \brief             RealVNC Wrapper Response class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:   VNC Response class for receiving response receiving response
 for spi_tclMLVncRespDAP calls to implement the client side Device
 Attestation Protocol (DAP). This class is used by ViewerWrapper
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 04.09.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 11.04.2013 |  Shiva Kumar Gurija          | Updated the Attestation Response
                                               Handling

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclMLVncRespDAP.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncRespDAP.cpp.trc.h"
   #endif
#endif

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  virtual spi_tclMLVncRespDAP::~spi_tclMLVncRespDAP()
 ***************************************************************************/
spi_tclMLVncRespDAP::~spi_tclMLVncRespDAP()
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDAP::~spi_tclMLVncRespDAP() Entered \n "));
}

/*************************************************
 //!Callbacks to be implemented by inherting class
 * **********************************************/

/***************************************************************************
 ** FUNCTION:t_Void vPostLaunchDAPRes(trUserContext rUserContext ... );
 ***************************************************************************/
t_Void  spi_tclMLVncRespDAP::vPostLaunchDAPRes(trUserContext rUserContext,
         t_Bool bLaunchDAPRes, const t_U32 cou32DeviceHandle)
{
   (t_Void)cou32DeviceHandle;
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDAP::vPostLaunchDAPRes Entered \n"));
   ETG_TRACE_USR4(("bLaunchDAPRes = %d ", bLaunchDAPRes));
}

/***************************************************************************
** FUNCTION:t_Void spi_tclMLVncRespDAP::vPostDAPAttestResp()
***************************************************************************/
t_Void spi_tclMLVncRespDAP::vPostDAPAttestResp(trUserContext rUserContext,
                                                 tenMLAttestationResult enAttestationResult,
                                                 t_String szVNCAppPublickey,
                                                 t_String szUPnPAppPublickey,
                                                 t_String szVNCUrl,
                                                 const t_U32 cou32DeviceHandle,
                                                 t_Bool bVNCAvailInDAPAttestResp)
{
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   SPI_INTENTIONALLY_UNUSED(bVNCAvailInDAPAttestResp);
   ETG_TRACE_USR4(("spi_tclMLVncRespDAP::vPostDAPAttestResp: Device-0x%x Result-%d VNC Key-%s", 
      cou32DeviceHandle,enAttestationResult,szVNCAppPublickey.c_str()));
   ETG_TRACE_USR4(("spi_tclMLVncRespDAP::vPostDAPAttestResp: UPnP Key-%s", 
      szUPnPAppPublickey.c_str()));
   ETG_TRACE_USR4(("spi_tclMLVncRespDAP::vPostDAPAttestResp: VNC URL-%s", 
      szVNCUrl.c_str()));
}

/***************************************************************************
 *******************************PROTECTED***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncRespDAP::spi_tclMLVncRespDAP()
 ***************************************************************************/
spi_tclMLVncRespDAP::spi_tclMLVncRespDAP() :
         RespBase(e16DAP_REGID)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDAP::spi_tclMLVncRespDAP() Entered \n "));
}
