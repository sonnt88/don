/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbLocService.cpp
* \brief CDB Location service class
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    CDB Location service class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version
21.05.2015  | Ramya Murthy          | Working implementation (for PSA)

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclMLVncCdbLocService.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCdbLocService.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbLocService::spi_tclMLVncCdbLocService(const ...
***************************************************************************/
spi_tclMLVncCdbLocService::spi_tclMLVncCdbLocService(const VNCCDBSDK* coprCdbSdk,
	VNCCDBServiceAccessControl enAccessCntrl)
      : spi_tclMLVncCdbService(
           coprCdbSdk,
           copczLOCATION_SERVICE_NAME,
           u8CDB_VERSION_MAJOR,
           u8CDB_VERSION_MINOR,
           VNCCDBServiceConfigurationNone, //TODO
           enAccessCntrl,
           e8CDB_LOC_SERVICE),
        m_oGeoLocation(coprCdbSdk)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbLocService() Entered "));

   //Add objects of service
	vAddObject(&m_oGeoLocation);
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbLocService::~spi_tclMLVncCdbLocService()
***************************************************************************/
spi_tclMLVncCdbLocService::~spi_tclMLVncCdbLocService()
{
   ETG_TRACE_USR1(("~spi_tclMLVncCdbLocService() entered "));
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbLocService::vOnData(const trGPSData& rfcorGpsData)
***************************************************************************/
t_Void spi_tclMLVncCdbLocService::vOnData(const trGPSData& rfcorGpsData)
{
   //! Forward the GPS data
	m_oGeoLocation.vOnData(rfcorGpsData);

	//! Send location data update for OnChange subscription type
   if ((u8SUBSCRIPTIONTYPE_ONCHANGE == m_oGeoLocation.enGetSubscriptionType())
      &&
      (true == m_oGeoLocation.bIsLocDataAvailable()))
   {
      VNCSBPProtocolError enSbpError =
               enSendGetResponse(m_oGeoLocation.u32GetObjectUID(), VNCSBPCommandTypeSubscribe);

      if (VNCSBPProtocolErrorNone != enSbpError)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbLocService::vOnData(): "
                  "Sending loc update failed with error = %x ",
                  ETG_ENUM(SBP_ERROR, enSbpError)));
      }
   }//if (u8SUBSCRIPTIONTYPE_ONCHANGE == m_oGeoLocation.enGetSubscriptionType())
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbLocService::vOnData(const trSensorData& rfcorSensorData)
***************************************************************************/
t_Void spi_tclMLVncCdbLocService::vOnData(const trSensorData& rfcorSensorData)
{
   t_Bool bCurLocDataAvailablility = m_oGeoLocation.bIsLocDataAvailable();

   //! Forward the Sensor data
   m_oGeoLocation.vOnData(rfcorSensorData);

   t_Bool bNewLocDataAvailablility = m_oGeoLocation.bIsLocDataAvailable();

   //! Send notification when Location data becomes unavailable
   tenSubscriptionType enSubType = m_oGeoLocation.enGetSubscriptionType();

   if ((false == bNewLocDataAvailablility)
      &&
      (true == bCurLocDataAvailablility)
      &&
      ((u8SUBSCRIPTIONTYPE_ONCHANGE == enSubType) || (u8SUBSCRIPTIONTYPE_REGULARINTERVAL == enSubType)))
   {
      VNCSBPProtocolError enSbpError =
               enSendGetResponse(m_oGeoLocation.u32GetObjectUID(), VNCSBPCommandTypeGet); //TODO - cmd type for notification

      if (VNCSBPProtocolErrorNone != enSbpError)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbLocService::vOnData(): "
                  "Sending loc data unavailable nitification failed with error = %x ",
                  ETG_ENUM(SBP_ERROR, enSbpError)));
      }
   }//if ((e8GNSSQUALITY_NOFIX == rfcorSensorData.enGnssQuality) &&...)
}


///////////////////////////////////////////////////////////////////////////////
// <EOF>
