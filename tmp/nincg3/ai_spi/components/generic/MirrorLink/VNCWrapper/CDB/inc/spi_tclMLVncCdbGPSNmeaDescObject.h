/***********************************************************************/
/*!
* \file  spi_tclMLVncCdbGPSNmeaDescObject.h
* \brief GPS NMEA Description Object class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    GPS NMEA Description Object class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
24.03.2013  | Ramya Murthy          | Initial Version (moved class from
                                      spi_tclMLVncCdbGPSService.h)

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVNCCDBGPSNMEADESCOBJ_H_
#define _SPI_TCLVNCCDBGPSNMEADESCOBJ_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H
#include "vnccdbsdk.h"
#include "vnccdbtypes.h"
#include "vncsbptypes.h"
#include "vncsbpserialize.h"

#include "BaseTypes.h"
#include "spi_tclMLVncCdbObject.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
 * * \brief forward declaration
 */

/****************************************************************************/
/*!
* \class spi_tclMLVncCdbGPSNmeaDescObject
* \brief
*******************************************************************************/
class spi_tclMLVncCdbGPSNmeaDescObject : public spi_tclMLVncCdbObject
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/
   
   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaDescObject::spi_tclMLVncCdbGPSNmeaDescObject(...
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGPSNmeaDescObject(spi_tclMLVncCdbGPSService& rfoGPSService,
   *              const VNCCDBSDK* coprCdbSdk)
   * \brief   Constructor
   * \sa      ~spi_tclMLVncCdbGPSNmeaDescObject()
   ***************************************************************************/
   spi_tclMLVncCdbGPSNmeaDescObject(const VNCCDBSDK* coprCdbSdk);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaDescObject::~spi_tclMLVncCdbGPSNmeaDescObject()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCdbGPSNmeaDescObject()
   * \brief   Constructor
   * \sa      spi_tclMLVncCdbGPSNmeaDescObject()
   ***************************************************************************/
   virtual ~spi_tclMLVncCdbGPSNmeaDescObject();


   /******** Start of Methods overridden from spi_tclMLVncCdbObject **********/

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbGPSNmeaDescObject::enGet(VNCSBPSerialize*...
   ***************************************************************************/
   /*!
   * \fn       enGet(VNCSBPSerialize* prSerialize)
   * \brief    Get serialized data.
   * \param    prSerialize  : [IN] Pointer to serializer
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   * \sa
   ***************************************************************************/
   virtual VNCSBPProtocolError enGet(VNCSBPSerialize* prSerialize);
   
   /********* End of Methods overridden from spi_tclMLVncCdbService **********/


protected:
   /***************************************************************************
   ********************************PROTECTED***********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaDescObject::s32GetSupportedSentences()
   ***************************************************************************/
   /*!
   * \fn       u32GetSupportedSentences()
   * \brief    Gets NMEA sentences
   * \retval   t_U32: Bit wise OR of supported sentences
   * \sa
   ***************************************************************************/
   virtual t_U32 u32GetSupportedSentences();

private:
   /***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/

   /**************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaDescObject::spi_tclMLVncCdbGPSNmeaDescObject(
   **                const spi_tclMLVncCdbGPSNmeaDescObject& oObject)
   **************************************************************************/
   /*!
   * \fn      spi_tclGPSNmeaObject(const spi_tclMLVncCdbGPSNmeaDescObject& oObject)
   * \brief   Copy Constructor, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbGPSNmeaDescObject(const spi_tclMLVncCdbGPSNmeaDescObject& oObject);

   /**************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaDescObject& spi_tclMLVncCdbGPSNmeaDescObject::
   **                operator=(const spi_tclMLVncCdbGPSNmeaDescObject& oObject)
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGPSNmeaDescObject& operator=(
   *              const spi_tclMLVncCdbGPSNmeaDescObject& oObject)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbGPSNmeaDescObject& operator=(
         const spi_tclMLVncCdbGPSNmeaDescObject& oObject);

};

#endif // _SPI_TCLVNCCDBGPSNMEADESCOBJ_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
