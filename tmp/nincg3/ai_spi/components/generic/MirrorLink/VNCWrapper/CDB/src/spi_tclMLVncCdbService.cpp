/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbService.cpp
* \brief Base class for a CDB service
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for a CDB service
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <map>

#include "spi_tclMLVncCdbObject.h"
#include "spi_tclMLVncCdbServiceMngr.h"
#include "spi_tclMLVncMsgQInterface.h"
#include "spi_tclMLVncCdbService.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCdbService.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef std::map<VNCSBPUID, spi_tclMLVncCdbObject*>::iterator tSpiObjMapItr;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbService::spi_tclMLVncCdbService(const VNCCDB...
***************************************************************************/
spi_tclMLVncCdbService::spi_tclMLVncCdbService(const VNCCDBSDK* coprCdbSdk,
   const t_Char* copczServiceName,
   t_U8 u8VersionMajor,
   t_U8 u8VersionMinor,
   VNCCDBServiceConfiguration enServiceConfig,
   VNCCDBServiceAccessControl enAccessCntrl,
   tenCDBServiceType enServiceType) 
   : m_coprCdbSdk(coprCdbSdk), 
     m_prCdbEndpoint(NULL),
     m_bIsServiceActive(false),
     m_enServiceType(enServiceType)
{
   SPI_NORMAL_ASSERT(NULL == coprCdbSdk);
   SPI_NORMAL_ASSERT(NULL == copczServiceName);

   ETG_TRACE_USR1(("spi_tclMLVncCdbService() entered: enServiceType = %u : "
         "u8VersionMajor = %u, u8VersionMinor = %u, enServiceConfig = %u, "
         "enAccessCntrl = %u, copczServiceName = %s ",
         ETG_ENUM(CDB_SERVICE_TYPE, enServiceType),
         u8VersionMajor,
         u8VersionMinor,
         ETG_ENUM(CDB_SVCCONFIGURATION, enServiceConfig),
         ETG_ENUM(CDB_SVCACCESSCONTROL, enAccessCntrl),
         copczServiceName));

   //! Initialise CDBService structure info
   m_rCdbService.name = copczServiceName;
   m_rCdbService.versionMajor = u8VersionMajor;
   m_rCdbService.versionMinor = u8VersionMinor;
   m_rCdbService.configuration = enServiceConfig;
   m_rCdbService.accessControl = enAccessCntrl;
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbService::~spi_tclMLVncCdbService()
***************************************************************************/
spi_tclMLVncCdbService::~spi_tclMLVncCdbService()
{
   ETG_TRACE_USR1(("~spi_tclMLVncCdbService() entered "));

   //Remove all objects
   m_oLock.s16Lock();
   m_ObjectsMap.clear();
   m_oLock.vUnlock();
   //@Note: Memory deallocation not required for map items since it is handled
   //in derived classes

   m_coprCdbSdk = NULL;
   m_prCdbEndpoint = NULL;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCdbService::bIsServiceActive()
***************************************************************************/
t_Bool spi_tclMLVncCdbService::bIsServiceActive() const
{
   ETG_TRACE_USR2(("spi_tclMLVncCdbService::bIsServiceActive() left with IsServiceActive = %u ",
         ETG_ENUM(BOOL, m_bIsServiceActive)));
   return m_bIsServiceActive;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbService::vStartStopService(t_Bool...
***************************************************************************/
t_Void spi_tclMLVncCdbService::vStartStopService(t_Bool bStartService,
      t_Bool bResponseRequired)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbService::vStartStopService() entered: "
         "bStartService = %u, bResponseRequired = %u ",
         ETG_ENUM(BOOL, bStartService),
         ETG_ENUM(BOOL, bResponseRequired)));

   /*if (bStartService != m_bIsServiceActive)
   {
      //Subscribe or unsubscribe for location data
      vSubscribeForLocationData(bStartService);
   }*/

   //! Evaluate response to be sent & send the response for the request
   /*if (true == bResponseRequired)
   {
      VNCCDBServiceResponse enServiceResp = VNCCDBServiceResponseNone;
      if ((true == bStartService) && (false == m_bIsServiceActive))
      {
         enServiceResp = VNCCDBServiceResponseOKServiceStarted;
      }
      else if ((true == bStartService) && (true == m_bIsServiceActive))
      {
         enServiceResp = VNCCDBServiceResponseWarningServiceRunning;
      }
      else if ((false == bStartService) && (false == m_bIsServiceActive))
      {
         enServiceResp = VNCCDBServiceResponseWarningServiceNotRunning;
      }
      else if ((false == bStartService) && (true == m_bIsServiceActive))
      {
         enServiceResp = VNCCDBServiceResponseOKServiceStopped;
      }

      vSendServiceResponse(enServiceResp);

   } //if (true == bResponseRequired)*/ 
   //@Note: Sending response is removed since SDK handles it. Kept for future use.

   //! Set service activity status
   m_bIsServiceActive = bStartService;
}

/***************************************************************************
** FUNCTION:  VNCCDBServiceId spi_tclMLVncCdbService::u32GetServiceId()
***************************************************************************/
VNCCDBServiceId spi_tclMLVncCdbService::u32GetServiceId() const
{
   //ETG_TRACE_USR2(("spi_tclMLVncCdbService::u32GetServiceId() left with ServiceID = 0x%x ",
         //m_rCdbService.serviceId));
   return m_rCdbService.serviceId;
}

/***************************************************************************
** FUNCTION:  tenCDBServiceType spi_tclMLVncCdbService::enGetServiceType()
***************************************************************************/
tenCDBServiceType spi_tclMLVncCdbService::enGetServiceType() const
{
   //ETG_TRACE_USR2(("spi_tclMLVncCdbService::enGetServiceType() left with enServiceType = 0x%x ",
         //ETG_ENUM(CDB_SERVICE_TYPE, m_enServiceType)));
   return m_enServiceType;
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbService::enAddToEndpoint(VNCCDBEndpoint...
***************************************************************************/
VNCCDBError spi_tclMLVncCdbService::enAddToEndpoint(VNCCDBEndpoint* prCdbEndpoint,
   const VNCSBPSourceCallbacks* coprSbpSrcCallbacks,
   size_t CallbacksSize,
   spi_tclMLVncCdbServiceMngr* poSvcManager)
{
   //! Add an SBP service to Endpoint
   m_prCdbEndpoint = prCdbEndpoint;
   VNCCDBError enRetError = m_coprCdbSdk->vncCDBEndpointAddSBPSource(
         prCdbEndpoint,
         poSvcManager,
         &m_rCdbService,
         sizeof(m_rCdbService),
         coprSbpSrcCallbacks,
         CallbacksSize);

   //! Clear the ServiceID if service is not added successfully
   if (VNCCDBErrorNone != enRetError)
   {
      m_rCdbService.serviceId = 0;
   }

   ETG_TRACE_USR2(("spi_tclMLVncCdbService::enAddToEndpoint() "
         "left with VNCCDBError = %x, Assigned ServiceID = 0x%x ",
         ETG_ENUM(CDB_ERROR, enRetError),
         m_rCdbService.serviceId));

   return enRetError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enSet(VNCSBPUID...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbService::enSet(VNCSBPUID u32ObjUID,
   const t_U8* copu8Payload,
   t_U32 u32PayloadSize)
{
   VNCSBPProtocolError enSbpError = (true == m_bIsServiceActive) ?
      VNCSBPProtocolErrorNone : VNCSBPProtocolErrorNotAvailable;
   
   //! Forward Set request to object
   if (VNCSBPProtocolErrorNone == enSbpError)
   {
      m_oLock.s16Lock();
      tSpiObjMapItr ObjItr = m_ObjectsMap.find(u32ObjUID);
      enSbpError = ((m_ObjectsMap.end() != ObjItr) && (NULL != ObjItr->second)) ?
         (ObjItr->second->enSet(copu8Payload, u32PayloadSize)) :
         (VNCSBPProtocolErrorUnknownUID);
      m_oLock.vUnlock();
   } //if (VNCSBPProtocolErrorNone == enSbpError)

   ETG_TRACE_USR3(("spi_tclMLVncCdbService::enSet"
         "(for u32ObjUID = 0x%x, u32PayloadSize = %u) left with VNCSBPProtocolError = %x ",
         u32ObjUID,
         u32PayloadSize,
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enSubscribe(VNCSBPUID...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbService::enSubscribe(
   VNCSBPUID u32ObjUID,
   VNCSBPSubscriptionType* penSubType,
   t_U32* pu32IntervalMs)
{
   VNCSBPProtocolError enSbpError = (true == m_bIsServiceActive) ?
      VNCSBPProtocolErrorNone : VNCSBPProtocolErrorNotAvailable;

   //! Forward Subscribe request to object
   if (VNCSBPProtocolErrorNone == enSbpError)
   {
      if ((NULL != penSubType) && (NULL != pu32IntervalMs))
      {
         ETG_TRACE_USR1(("spi_tclMLVncCdbService::enSubscribe: "
               "u32ObjUID = 0x%x, penSubType = %x, pu32IntervalMs = %u ",
               u32ObjUID, ETG_ENUM(SBP_SUBSCRIPTIONTYPE, *penSubType), *pu32IntervalMs));

         m_oLock.s16Lock();
         tSpiObjMapItr ObjItr = m_ObjectsMap.find(u32ObjUID);
         enSbpError = ((m_ObjectsMap.end() != ObjItr) && (NULL != ObjItr->second)) ?
            (ObjItr->second->enSubscribe(penSubType, pu32IntervalMs)) :
            (VNCSBPProtocolErrorUnknownUID);
         m_oLock.vUnlock();
      }
      else
      {
         enSbpError = VNCSBPProtocolErrorMissingMandatoryData;
      }
   } //if (VNCSBPProtocolErrorNone == enSbpError)

   ETG_TRACE_USR3(("spi_tclMLVncCdbService::enSubscribe() left with VNCSBPProtocolError = %x ",
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enCancelSubscribption(...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbService::enCancelSubscribption(VNCSBPUID u32ObjUID)
{
   VNCSBPProtocolError enSbpError = (true == m_bIsServiceActive) ?
      VNCSBPProtocolErrorNone : VNCSBPProtocolErrorNotAvailable;

   if (VNCSBPProtocolErrorNone == enSbpError)
   {
      m_oLock.s16Lock();
      //Forward cancel subscribe request to object
      tSpiObjMapItr ObjItr = m_ObjectsMap.find(u32ObjUID);
      enSbpError = ((m_ObjectsMap.end() != ObjItr) && (NULL != ObjItr->second)) ?
            (ObjItr->second->enCancelSubscribe()) :
            (VNCSBPProtocolErrorGenericRecoverable);
      m_oLock.vUnlock();
   }

   ETG_TRACE_USR3(("spi_tclMLVncCdbService::enCancelSubscribption"
         "(for u32ObjUID = 0x%x) left with VNCSBPProtocolError = %x ",
         u32ObjUID,
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enSendGetResponse(VNCSBPUID...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbService::enSendGetResponse(VNCSBPUID u32ObjUID,
   VNCSBPCommandType enCmdType)
{
   //!Function can be called for an explicit Get request, or by SDK
   //for sending periodic updates based on Sink's interval-based
   //subscription. This can be differentiated via enCmdType.
   //But here we send data regardless of enCmdType.
   
   VNCSBPProtocolError enSbpError = (true == m_bIsServiceActive) ?
      VNCSBPProtocolErrorNone : VNCSBPProtocolErrorNotAvailable;

   enSbpError = ((VNCSBPProtocolErrorNone == enSbpError) && (NULL == m_prCdbEndpoint)) ?
      VNCSBPProtocolErrorGenericUnrecoverable : enSbpError;

   //!Serialize object data & send it to SDK
   if (VNCSBPProtocolErrorNone == enSbpError)
   {
      VNCSBPSerialize* prSerialize = NULL;

      SPI_NORMAL_ASSERT(NULL == m_coprCdbSdk);
      if (
         (NULL != m_coprCdbSdk)
         &&
         (VNCCDBErrorNone == (m_coprCdbSdk->vncSBPSerializeCreate(&prSerialize)))
         &&
         (NULL != prSerialize)
         &&
         (VNCCDBErrorNone == (m_coprCdbSdk->vncSBPSerializeBeginPacket(prSerialize)))
         )
      {
         const t_U8* pu8SerializedData = NULL;
         size_t u32SerializedSize = 0;

         //! Acquire response body
         enSbpError = enGet(u32ObjUID, prSerialize);

         //! Finalize packet and send response
         if (VNCSBPProtocolErrorNone == enSbpError)
         {
            if (
               (VNCCDBErrorNone == m_coprCdbSdk->vncSBPSerializeEnd(prSerialize))
               &&
               (VNCCDBErrorNone == m_coprCdbSdk->vncSBPSerializeFinalize(prSerialize,
                        &pu8SerializedData, &u32SerializedSize))
               )
            {
               SPI_NORMAL_ASSERT(NULL == pu8SerializedData);
               ETG_TRACE_USR4(("spi_tclMLVncCdbService::enSendGetResponse: u32SerializedSize = %u ",
                        u32SerializedSize));
            }
            else
            {
               ETG_TRACE_ERR(("spi_tclMLVncCdbService::enSendGetResponse: "
                        "vncSBPSerializeEnd/vncSBPSerializeFinalize failed! "));

               //@Note: To send empty payload
               pu8SerializedData = NULL;
               u32SerializedSize = 0;
               enSbpError = VNCSBPProtocolErrorGenericRecoverable; //TODO
            }
         } //if (enSbpError == VNCSBPProtocolErrorNone)

         VNCCDBError enSendCdbError = m_coprCdbSdk->vncSBPSourceSendGetResponse(
               m_prCdbEndpoint, u32GetServiceId(), NULL, enCmdType, u32ObjUID,
               enSbpError, pu8SerializedData, u32SerializedSize);
         //@Note: CommandType should be same as that received in Get callback from SDK.

         if (VNCCDBErrorNone != enSendCdbError)
         {
            ETG_TRACE_ERR(("spi_tclMLVncCdbService::enSendGetResponse: "
                  "vncSBPSourceSendGetResponse VNCCDBError = %x ",
                  ETG_ENUM(CDB_ERROR, enSendCdbError)));
         }//if (VNCCDBErrorNone != enSendCdbError)

         //! Destroy serialize data
         m_coprCdbSdk->vncSBPSerializeDestroy(prSerialize);
         prSerialize = NULL;

      } //if ((NULL != m_coprCdbSdk)&&...)
      else
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbService::enSendGetResponse: "
                 "vncSBPSerializeEnd/vncSBPSerializeFinalize failed! "));
      }
   }//if (VNCSBPProtocolErrorNone == enSbpError)

   ETG_TRACE_USR3(("spi_tclMLVncCdbService::enSendGetResponse"
         "(for u32ObjUID = 0x%x, enCmdType = %x) left with VNCSBPProtocolError = %x ",
         u32ObjUID,
         ETG_ENUM(SBP_CMDTYPE, enCmdType),
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbService::vSendSubscribeResponse(VNCSBPUID...
***************************************************************************/
t_Void spi_tclMLVncCdbService::vSendSubscribeResponse(VNCSBPUID u32ObjUID,
      VNCSBPProtocolError enSbpError)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbService::vSendSubscribeResponse() entered with "
         "u32ObjUID = 0x%x, enSbpError = %x ",
         u32ObjUID,
         ETG_ENUM(SBP_ERROR, enSbpError)));

   m_oLock.s16Lock();
   t_Bool bObjectExists = (m_ObjectsMap.end() != m_ObjectsMap.find(u32ObjUID));
   m_oLock.vUnlock();

   if ((NULL != m_coprCdbSdk) && (NULL != m_prCdbEndpoint) && (true == bObjectExists))
   {
      VNCCDBError enCdbError = m_coprCdbSdk->vncSBPSourceSendSubscribeResponse(
            m_prCdbEndpoint,
            u32GetServiceId(),
            NULL,
            u32ObjUID,
            enSbpError);

      if (VNCCDBErrorNone != enCdbError)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbService::vSendSubscribeResponse: "
               "vncSBPSourceSendSubscribeResponse VNCCDBError = %u ", ETG_ENUM(CDB_ERROR, enCdbError)));
      }
   }//if (NULL != m_coprCdbSdk)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbService::vSendSetResponse(VNCSBPUID...
***************************************************************************/
t_Void spi_tclMLVncCdbService::vSendSetResponse(VNCSBPUID u32ObjUID,
      VNCSBPProtocolError enSbpError)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbService::vSendSetResponse() entered with "
         "u32ObjUID = 0x%x, enSbpError = %x ",
         u32ObjUID,
         ETG_ENUM(SBP_ERROR, enSbpError)));

   m_oLock.s16Lock();
   t_Bool bObjectExists = (m_ObjectsMap.end() != m_ObjectsMap.find(u32ObjUID));
   m_oLock.vUnlock();

   if ((NULL != m_coprCdbSdk) && (NULL != m_prCdbEndpoint) && (true == bObjectExists))
   {
      VNCCDBError enCdbError = m_coprCdbSdk->vncSBPSourceSendSetResponse(
            m_prCdbEndpoint,
            u32GetServiceId(),
            NULL,
            u32ObjUID,
            enSbpError);

      if (VNCCDBErrorNone != enCdbError)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbService::vSendSetResponse: "
               "vncSBPSourceSendSetResponse VNCCDBError = %u ", ETG_ENUM(CDB_ERROR, enCdbError)));
      }
   }//if (NULL != m_coprCdbSdk)
}


/***************************************************************************
*********************************PROTECTED**********************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbService::vAddObject(spi_tclMLVncCdbObject...
***************************************************************************/
t_Void spi_tclMLVncCdbService::vAddObject(spi_tclMLVncCdbObject* poObject)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbService::vAddObject() entered "));

   SPI_NORMAL_ASSERT(NULL == poObject);
   if (NULL != poObject)
   {
      //Add object into map
      m_oLock.s16Lock();
      m_ObjectsMap[poObject->u32GetObjectUID()] = poObject;
      m_oLock.vUnlock();
   }
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enGet(VNCSBPUID...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbService::enGet(VNCSBPUID u32ObjUID,
   VNCSBPSerialize* prSerialize)
{
   //! Forward Get request to Object
   VNCSBPProtocolError enSbpError = (true == m_bIsServiceActive) ?
         VNCSBPProtocolErrorNone : VNCSBPProtocolErrorNotAvailable;

   if (VNCSBPProtocolErrorNone == enSbpError)
   {
      m_oLock.s16Lock();
      tSpiObjMapItr ObjItr = m_ObjectsMap.find(u32ObjUID);
      enSbpError = ((m_ObjectsMap.end() != ObjItr) && (NULL != ObjItr->second)) ?
            (ObjItr->second->enGet(prSerialize)) :
            (VNCSBPProtocolErrorUnknownUID);
      m_oLock.vUnlock();
   }

   ETG_TRACE_USR3(("spi_tclMLVncCdbService::enGet"
         "(for u32ObjUID = 0x%x) left with VNCSBPProtocolError = %x ",
         u32ObjUID,
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbService::vSubscribeForLocationData()
***************************************************************************/
t_Void spi_tclMLVncCdbService::vSubscribeForLocationData(t_Bool bSubscribe) const
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbService::vSubscribeForLocationData() entered: bSubscribe = %u ",
         ETG_ENUM(BOOL, bSubscribe)));

   //! Trigger request to register/unregister for LocationData notifications
   spi_tclMLVncMsgQInterface* poMsgQInterface = spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      MLLocDataSubscriptionSetMsg oSetLocDataSubMsg;
      oSetLocDataSubMsg.bSubscribe = bSubscribe;
      poMsgQInterface->bWriteMsgToQ(&oSetLocDataSubMsg, sizeof(oSetLocDataSubMsg));
   } //if (NULL != poMsgQInterface)
}

/***************************************************************************
**********************************PRIVATE***********************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbService::vSendServiceResponse(VNCCDBServiceResponse...)
***************************************************************************/
t_Void spi_tclMLVncCdbService::vSendServiceResponse(VNCCDBServiceResponse enResponse) const
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbService::vSendServiceResponse() entered: enResponse = %x ",
         ETG_ENUM(CDB_SVC_RESPONSE, enResponse)));

   //! Send service-related response to SDK
   if (NULL != m_coprCdbSdk)
   {
      VNCCDBError enSendError =
            m_coprCdbSdk->vncCDBEndpointSendServiceResponse(m_prCdbEndpoint,
                  u32GetServiceId(), enResponse);

      if (VNCCDBErrorNone != enSendError)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbService::vSendServiceResponse: "
               "vncCDBEndpointSendServiceResponse VNCCDBError = %x ", ETG_ENUM(CDB_ERROR, enSendError)));
      }
   } //if (NULL != m_coprCdbSdk)
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
