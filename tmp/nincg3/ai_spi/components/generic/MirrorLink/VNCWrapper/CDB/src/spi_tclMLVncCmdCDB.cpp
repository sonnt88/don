/***************************************************************************/
/*!
* \file  spi_tclMLVncCmdCDB.cpp
* \brief Interface to interact with VNC CDB SDK
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface to interact with VNC CDB SDK
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version
21.05.2015  | Ramya Murthy          | Removed vAddServiceToConfigList() since config is read from policy
27.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
01.06.2015  | Shiva kumar Gurija            | Set names to VNC Threads

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <algorithm>

#define VNC_USE_STDINT_H

#include "BaseTypes.h"
#include "StringHandler.h"
#include "FileHandler.h"
using namespace spi::io;
#include "ThreadNamer.h"
#include "spi_tclMLVncDiscovererDataIntf.h"
#include "spi_tclMLVncCdbServiceMngr.h"
#include "spi_tclMLVncCmdCDB.h"
#include "spi_tclVncSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCmdCDB.cpp.trc.h"
   #endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
enum enCDBRespID
{
   u32LAUNCH_CDB = 1 // 0 reserved for no request ID
};


/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const VNCDiscoverySDKTimeoutMicroseconds cou32CDBTimeout = 5 * 1000 * 1000;

MsgContext spi_tclMLVncCmdCDB::m_oMsgContext;


/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdCDB::spi_tclMLVncCmdCDB()
***************************************************************************/
spi_tclMLVncCmdCDB::spi_tclMLVncCmdCDB()
   :  m_prCDBSdk(NULL),
      m_prCDBEndPt(NULL),
      m_prDiscSDK(NULL),
      m_prMLDisc(NULL),
      m_poServiceMngr(NULL),
      m_u32ListenerID(0),
      m_bIsCdbEnabled(true),
      m_bIsCdbSdkInitialised(false),
      m_bIsEndpointStarted(false)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB() entered \n"));
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdCDB::~spi_tclMLVncCmdCDB()
***************************************************************************/
spi_tclMLVncCmdCDB::~spi_tclMLVncCmdCDB()
{
   ETG_TRACE_USR1(("~spi_tclMLVncCmdCDB() entered \n"));
   
   vUninitialiseCDBEndpoint();
   vUninitialiseCDBSDK();
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bInitialiseCDBSDK()
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bInitialiseCDBSDK()
{
   //! If CDB SDK is not created, create new instance of CDB SDK & initialise it.
   if (false == m_bIsCdbSdkInitialised)
   {
      m_prCDBSdk = new VNCCDBSDK;
      SPI_NORMAL_ASSERT(NULL == m_prCDBSdk);

      if (NULL != m_prCDBSdk)
      {
         VNCCDBError enInitError = VNCCDBSDKInitialize(m_prCDBSdk, sizeof(*m_prCDBSdk));
         m_bIsCdbSdkInitialised = (VNCCDBErrorNone == enInitError);
         if (true == m_bIsCdbSdkInitialised)
         {
            bAddDiscoveryListener();
            //@note: If more discoverer(s) are added in future, addDiscoveryListener
            //should also be called for them.
         }
         else
         {
            ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::bInitialiseCDBSDK: "
                  "SDKInitialise failed with error = 0x%x \n", (t_U32)enInitError));
         }
      } //if (NULL != m_prCDBSdk)
   } //if (false == m_bIsCdbSdkInitialised)

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bInitialiseCDBSDK() left with result = %u \n",
         ETG_ENUM(BOOL, m_bIsCdbSdkInitialised)));

   return m_bIsCdbSdkInitialised;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vUninitialiseCDBSDK()
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vUninitialiseCDBSDK()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vUninitialiseCDBSDK() entered \n"));
   
   //! Remove the discoverer Listener
   bRemoveDiscoveryListener();

   //! Uninitialise CDB SDK
   SPI_NORMAL_ASSERT(NULL == m_prCDBSdk);
   if (NULL != m_prCDBSdk)
   {
      m_prCDBSdk->vncCDBSDKUninitialize();
   }
   RELEASE_MEM(m_prCDBSdk);

   m_bIsCdbSdkInitialised = false;
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdCDB::vLaunchCDBApp(t_U32 u32DeviceHandle...
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vLaunchCDBApp(t_U32 u32DeviceHandle,
   const t_U32 cou32CDBAppId)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vLaunchCDBApp() entered: cou32CDBAppId = 0x%x \n", cou32CDBAppId));

   //@Note: Device handle is currently not used. It is kept for
   //future usage, to extend feature support for multiple devices.
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);

   if ((true == m_bIsCdbSdkInitialised) && (true == m_bIsCdbEnabled))
   {
      //!Get the discoverer handles & App IDusing Discoverer Data Interface
      spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
      trvncDeviceHandles rVncDeviceHandles = oDiscDataIntf.corfrGetDiscHandles(u32DeviceHandle);
      t_String szCDBAppID = oDiscDataIntf.szGetAppID(u32DeviceHandle,cou32CDBAppId);

      ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::vLaunchCDBApp: szCDBAppID = %s \n", szCDBAppID.c_str()));

      //!Launch CDB application on ML Server
      if (
         (NULL != rVncDeviceHandles.poDiscoverySdk)
         &&
         (NULL != rVncDeviceHandles.poDiscoverer)
         &&
         (NULL != rVncDeviceHandles.poEntity)
         )
      {
         //Post Launch CDB App request
         t_U32 u32LaunchAppRequestId = const_cast<VNCDiscoverySDK*>(rVncDeviceHandles.poDiscoverySdk)->
            postEntityValueRespondToListener(
                  m_u32ListenerID,
                  const_cast<VNCDiscoverySDKDiscoverer*>(rVncDeviceHandles.poDiscoverer),
                  const_cast<VNCDiscoverySDKEntity*>(rVncDeviceHandles.poEntity),
                  szCDBAppID.c_str(),
                  VNCDiscoverySDKMLLaunchApplicationValue,
                  cou32CDBTimeout);

         //Store the request ID. Will be used in callback function
         trMsgContext rMsgCtxt;
         rMsgCtxt.rAppContext.u32ResID = u32LAUNCH_CDB;
         m_oMsgContext.vAddMsgContext(u32LaunchAppRequestId, rMsgCtxt);

         ETG_TRACE_USR4(("spi_tclMLVncCmdCDB::vLaunchCDBApp: u32LaunchAppRequestId = 0x%x \n", u32LaunchAppRequestId));

      }// if ((NULL != rVncDeviceHandles.poDiscoverySdk)&&...)
   } //if (true == m_bIsCdbSdkInitialised)
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdCDB::vTerminateCDBApp(t_U32 u32DeviceHandle)
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vTerminateCDBApp(t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vTerminateCDBApp() entered \n"));
   
   //@Note: Device handle is currently not used. It is kept for
   //future usage, to extend feature support for multiple devices.
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);

   if ((true == m_bIsCdbSdkInitialised) && (true == m_bIsCdbEnabled))
   {
      vUninitialiseCDBEndpoint();
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vGetServicesList(std::vector...
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vGetServicesList(
      std::vector<trCdbServiceConfig>& rfSvcConfigList) const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vGetServicesList() entered \n"));
   rfSvcConfigList = m_SvcConfigList;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vSetServicesList(const std::vector...
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vSetServicesList(
      const std::vector<trCdbServiceConfig>& rfSvcConfigList)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vSetServicesList() entered: rfSvcConfigList list size = %u \n",
         rfSvcConfigList.size()));
   m_SvcConfigList = rfSvcConfigList;
   //@Note: Only the internal ServiceConfigList is updated here since
   //it may change any number of times before Services are started.
   //ServiceManager's service list will be updated when starting CDB Endpoint.
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vSetCDBUsage(t_U32 u32DeviceHandle...
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vSetCDBUsage(t_U32 u32DeviceHandle, t_Bool bCdbEnabled)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vSetCDBUsage() entered: bCdbEnabled = %u \n", ETG_ENUM(BOOL, bCdbEnabled)));
   //@Note: DeviceHandle is kept for future usage, to extend feature support for multiple devices
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);

   m_bIsCdbEnabled = bCdbEnabled;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bGetCDBUsage(t_U32 u32DeviceHandle)
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bGetCDBUsage(t_U32 u32DeviceHandle)
{
   //@Note: DeviceHandle is kept for future usage, to extend feature support for multiple devices
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bGetCDBUsage() left with result = %u \n", ETG_ENUM(BOOL, m_bIsCdbEnabled)));

   return m_bIsCdbEnabled;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vOnData(const trGPSData& rfcorGpsData)
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vOnData(const trGPSData& rfcorGpsData)
{
   //! Forward data to ServiceManager class
   if (NULL != m_poServiceMngr)
   {
      m_poServiceMngr->vOnData(rfcorGpsData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vOnData(const trSensorData& rfcorSensorData)
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vOnData(const trSensorData& rfcorSensorData)
{
   //! Forward data to ServiceManager class
   if (NULL != m_poServiceMngr)
   {
      m_poServiceMngr->vOnData(rfcorSensorData);
   }
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdCDB:: bAddVNCLicense()
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bAddVNCLicense()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::bAddVNCLicense() entered \n"));

   t_Bool bAddLicense = false , bReadRes =false;
   t_String szLicensePath, szLicenseFile;

   spi_tclVncSettings *poVncSettings = spi_tclVncSettings::getInstance();

   if (NULL != poVncSettings)
   {
      poVncSettings->vGetLicenseFilePath(szLicensePath);
   }

   //!Read the license file
   FileHandler oLicenseFile(szLicensePath.c_str(), SPI_EN_RDONLY);

   if (true == oLicenseFile.bIsValid()) 
   {

	   t_S32 s32LicenseFileSize = oLicenseFile.s32GetSize();

		if (0 < s32LicenseFileSize) 
		{

			t_Char czLicenseFile[s32LicenseFileSize + 1];
			bReadRes = oLicenseFile.bFRead(czLicenseFile, s32LicenseFileSize);

			if (true == bReadRes) 
			{

				czLicenseFile[s32LicenseFileSize] = '\0';
				szLicenseFile.assign(czLicenseFile);
			} // if (true == bReadRes)

		}// if (0 <= s32LicenseFileSize)

	}// if (true == oLicenseFile.bIsValid())


   SPI_NORMAL_ASSERT(NULL == m_prCDBSdk);
   SPI_NORMAL_ASSERT(NULL == m_prCDBEndPt);

   //!Add license to Endpoint
   if (
      (true == bReadRes)
      &&
      (NULL != m_prCDBSdk)
      &&
      (NULL != m_prCDBEndPt)
      )
   {
      //Serial Number is unused, hence Serial Number and Serial Number size are set to NULL
      VNCCDBError enAddLicenseError = m_prCDBSdk->vncCDBEndpointAddLicense(
            m_prCDBEndPt,
            szLicenseFile.c_str(),
            NULL/*Serial Number*/,
            0/*Serial Number size*/);
      bAddLicense = (VNCCDBErrorNone == enAddLicenseError);

      if (false == bAddLicense)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::bAddVNCLicense: AddLicense failed with error = %x \n", 
               ETG_ENUM(CDB_ERROR, enAddLicenseError)));
      }
   } // if ((true == bReadRes)...)

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bAddVNCLicense() left with result = %u \n", ETG_ENUM(BOOL, bAddLicense)));

   return bAddLicense;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdCDB::bAddDiscoveryListener()
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bAddDiscoveryListener()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::bAddDiscoveryListener() entered \n"));

   t_Bool bAddListenerResult = false;

   //!Get the discovery sdk & ML discoverer handles
   spi_tclMLVncDiscovererDataIntf oDiscDataIntf;
   m_prDiscSDK = oDiscDataIntf.poGetDiscSDK();
   m_prMLDisc = oDiscDataIntf.poGetMLDisc();

   //! Add discoverer listener
   SPI_NORMAL_ASSERT(NULL == m_prDiscSDK);
   SPI_NORMAL_ASSERT(NULL == m_prMLDisc);
   if (
      (NULL != m_prDiscSDK)
      &&
      (NULL != m_prMLDisc)
      )
   {
      //!Callback registration for asynchronous calls to discovery sdk
      VNCDiscoverySDKCallbacks rDiscoverySDKCallbacks = { 0 };
      rDiscoverySDKCallbacks.postEntityValue = &vPostEntityValueCallback;

      VNCDiscoverySDKError s32AddListnerError = m_prDiscSDK->addDiscovererListener(
            m_prMLDisc,
            this,
            &rDiscoverySDKCallbacks,
            sizeof(rDiscoverySDKCallbacks),
            &m_u32ListenerID);
      bAddListenerResult = (VNCDiscoverySDKErrorNone == s32AddListnerError);

      if (false == bAddListenerResult)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::bAddDiscoveryListener Error = 0x%x \n", s32AddListnerError));
      }
   } //if ((NULL != m_prDiscSDK) && (NULL != m_prMLDisc))

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bAddDiscoveryListener() left with result = %u \n",
         ETG_ENUM(BOOL, bAddListenerResult)));

   return bAddListenerResult;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclMLVncCmdCDB::bRemoveDiscoveryListener()
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bRemoveDiscoveryListener()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::bRemoveDiscoveryListener() entered \n"));

   t_Bool bRemListenerResult = false;

   //!Remove the discoverer Listeners
   SPI_NORMAL_ASSERT(NULL == m_prDiscSDK);
   SPI_NORMAL_ASSERT(NULL == m_prMLDisc);
   if (
      (NULL != m_prDiscSDK)
      &&
      (NULL != m_prMLDisc)
      )
   {
      t_S32 s32RemListenerError = m_prDiscSDK->removeDiscovererListener(m_prMLDisc, m_u32ListenerID);
      //@note: If a new discoverer is added in bInitialiseCDBSDK(),
      //remove discovery Listeners should also be called for that discoverer.
      bRemListenerResult = (cs32MLDiscoveryErrorNone == s32RemListenerError);

      if (false == bRemListenerResult)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::bRemoveDiscoveryListener : "
               "RemoveListener failed with error = 0x%x \n", s32RemListenerError));
      }
   } //if (( NULL != m_prDiscSDK) && (NULL != m_prMLDisc))

   //! Reset the Discovery SDK & ML Discoverer pointers
   m_prDiscSDK = NULL;
   m_prMLDisc = NULL;

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bRemoveDiscoveryListener() left with result = %u \n",
         ETG_ENUM(BOOL, bRemListenerResult)));

   return bRemListenerResult;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdCDB::vFetchCDBServerUrl(t_String...
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vFetchCDBServerUrl(t_String szCDBAppID,
   VNCDiscoverySDKEntity* prDiscEntity)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vFetchCDBServerUrl() entered \n"));

   SPI_NORMAL_ASSERT(NULL == m_prDiscSDK);
   SPI_NORMAL_ASSERT(NULL == m_prMLDisc);
   SPI_NORMAL_ASSERT(NULL == prDiscEntity);
   SPI_NORMAL_ASSERT(NULL == m_prCDBSdk);

   //! Retrieve CDB server URL from Discovery SDK & use it to create CDB Endpoint.
   if (
      (NULL != m_prDiscSDK)
      &&
      (NULL != m_prMLDisc)
      &&
      (NULL != prDiscEntity)
      &&
      (NULL != m_prCDBSdk)
     )
   {
      std::string szCDBUrlkey = szCDBAppID + ":uri";
      t_Char* pczCDBUrl = NULL;

      t_S32 s32FetchError = m_prDiscSDK->fetchEntityValueBlocking(
         m_prMLDisc,
         prDiscEntity,
         szCDBUrlkey.c_str(),
         &pczCDBUrl,
         cou32CDBTimeout);

      if ((cs32MLDiscoveryErrorNone == s32FetchError) && (NULL != pczCDBUrl))
      {
         ETG_TRACE_USR4(("spi_tclMLVncCmdCDB::vFetchCDBServerUrl: Retrieved CDB URL: %s \n", pczCDBUrl));
         vInitialiseCDBEndpoint(pczCDBUrl);
      }
      else if (cs32MLDiscoveryErrorNone != s32FetchError)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::vFetchCDBServerUrl: "
               "Fetching CDB URL failed with error = 0x%x \n", s32FetchError));
      }
	  
	  //! Release memory allocated for URL
	  if (NULL != pczCDBUrl)
      {
         m_prDiscSDK->freeString(pczCDBUrl);
         pczCDBUrl = NULL;
      }
   } //if (((NULL != poDiscSDK)&&...)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vInitialiseCDBEndpoint(const t_Char...
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vInitialiseCDBEndpoint(const t_Char* pococzCDBUrl)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vInitialiseCDBEndpoint() entered \n"));

   t_Bool bInitSuccess = false;

   m_prCDBEndPt = NULL;
   m_poServiceMngr = NULL;
   //@Note: Assumed that if a previous Endpoint existed, the Endpoint and
   //its Service Manager is already properly destroyed elsewhere.

   //! Create & start Endpoint, and
   if (NULL != pococzCDBUrl)
   {
      //Add services & start endpoint
      bInitSuccess = ((true == bCreateCDBEndpoint(pococzCDBUrl)) &&
                     (true == bStartCDBEndpoint()));
   } //if (NULL != pococzCDBUrl)

   //! In case of any error, destroy Endpoint.
   if (false == bInitSuccess)
   {
      vUninitialiseCDBEndpoint();
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vUninitialiseCDBEndpoint()
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vUninitialiseCDBEndpoint()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vUninitialiseCDBEndpoint() entered \n"));

   //! Stop & destroy the Endpoint
   bStopCDBEndpoint();
   vDestroyCDBEndpoint();
}


/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bCreateCDBEndpoint(const t_Char*...
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bCreateCDBEndpoint(const t_Char* pococzCDBUrl)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::bCreateCDBEndpoint() entered \n"));

   t_Bool bCreateSuccess = false;

   SPI_NORMAL_ASSERT(NULL == m_prCDBSdk);
   SPI_NORMAL_ASSERT(NULL == pococzCDBUrl);
   if ((NULL == m_prCDBEndPt) && (NULL != m_prCDBSdk) && (NULL != pococzCDBUrl))
   {
      ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bCreateCDBEndpoint: Received info: pococzCDBUrl = %s \n", pococzCDBUrl));

      //!Initialise CDB descriptor and callbacks
      VNCCDBEndpointDescriptor rEndPtDescriptor = { 0 };
      VNCCDBEndpointCallbacks rEndPtCallbacks = { 0 };

      rEndPtDescriptor.url = pococzCDBUrl;
      rEndPtDescriptor.endpointType = VNCCDBEndpointTypeClient;
      rEndPtDescriptor.pContext = this;
      rEndPtDescriptor.versionMajor = u8CDB_VERSION_MAJOR;
      rEndPtDescriptor.versionMinor = u8CDB_VERSION_MINOR;

      rEndPtCallbacks.pStatusCallback = &vEndpointStatusCallback;
      rEndPtCallbacks.pThreadStartedCallback = &vThreadStartedCallback;
      rEndPtCallbacks.pThreadStoppedCallback = &vThreadStoppedCallback;
      rEndPtCallbacks.pLogCallback = &vLogCallback;

      //!Create CDB endpoint & Add VNC licence to it
      VNCCDBError enCreateError = m_prCDBSdk->vncCDBEndpointCreate(
            &m_prCDBEndPt,
            &rEndPtDescriptor,
            sizeof(rEndPtDescriptor),
            &rEndPtCallbacks,
            sizeof(rEndPtCallbacks));
      bCreateSuccess = (VNCCDBErrorNone == enCreateError);
      SPI_NORMAL_ASSERT(NULL == m_prCDBEndPt);

	   if (false == bCreateSuccess)
	   {
			ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::bCreateCDBEndpoint: "
				"EndpointCreate failed with error = %x \n", ETG_ENUM(CDB_ERROR,
					enCreateError)));
		}// End of if (false == bCreateSuccess)
	  
	  bCreateSuccess = (true == bCreateSuccess ) ? (bAddVNCLicense()):(bCreateSuccess);
	}//End of if ((NULL == m_prCDBEndPt) &&...

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bCreateCDBEndpoints() left with result = %u \n",
         ETG_ENUM(BOOL, bCreateSuccess)));

   return bCreateSuccess;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vDestroyCDBEndpoint()
***************************************************************************/
t_Void spi_tclMLVncCmdCDB::vDestroyCDBEndpoint()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vDestroyCDBEndpoint() entered \n"));

   //! Destroy the Endpoint
   SPI_NORMAL_ASSERT(NULL == m_prCDBEndPt);
   if ((NULL != m_prCDBEndPt) && (NULL != m_prCDBSdk))
   {
      VNCCDBError enDestroyError = m_prCDBSdk->vncCDBEndpointDestroy(m_prCDBEndPt);
      if (VNCCDBErrorNone != enDestroyError)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::vDestroyCDBEndpoint: "
               "EndpointDestroy failed with error = %x! \n",  ETG_ENUM(CDB_ERROR, enDestroyError)));
      }
   } //if ((NULL != m_prCDBEndPt) && (NULL != m_prCDBSdk))

   m_prCDBEndPt = NULL;
}

/**************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdCDB::bInitCDBEndPointServices()
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bInitCDBEndPointServices()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::bInitCDBEndPointServices() entered \n"));

   t_Bool bInit = false;

   //! Create Service manager for the Endpoint & Add services in Service Manager
   if ((NULL == m_poServiceMngr) && (NULL != m_prCDBEndPt) && (NULL != m_prCDBSdk))
   {
      m_poServiceMngr = new spi_tclMLVncCdbServiceMngr(m_prCDBEndPt, m_prCDBSdk);
      SPI_NORMAL_ASSERT(NULL == m_poServiceMngr);

      if ((NULL != m_poServiceMngr) && (0 != m_SvcConfigList.size()))
      {
         bInit = m_poServiceMngr->bSetServicesList(m_SvcConfigList);
      }
   } //if (NULL == m_poServiceMngr)
   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bInitCDBEndPointServices() left with result = %u \n",
         ETG_ENUM(BOOL, bInit)));

   return bInit;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdCDB::bUninitCDBEndPointServices()
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bUninitCDBEndPointServices()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::bUninitCDBEndPointServices() entered \n"));

   t_Bool bUninit = false;

   //! Remove all service from Service manager & destory service manager
   SPI_NORMAL_ASSERT(NULL == m_poServiceMngr);
   if (NULL != m_poServiceMngr)
   {
      std::vector<trCdbServiceConfig> SvcConfigList; //empty vector
      bUninit = m_poServiceMngr->bSetServicesList(SvcConfigList);
   } //if (NULL != poServiceMngr)

   RELEASE_MEM(m_poServiceMngr);

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bUninitCDBEndPointServices() left with result = %u \n",
         ETG_ENUM(BOOL, bUninit)));

   return bUninit;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bStartCDBEndpoint()
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bStartCDBEndpoint()
{
   t_Bool bStartSuccess = false;

   //! Start Endpoint if it is not running.
   SPI_NORMAL_ASSERT(NULL == m_prCDBSdk);
   SPI_NORMAL_ASSERT(NULL == m_prCDBEndPt);
   if (
      (false == m_bIsEndpointStarted)
      &&
      (NULL != m_prCDBSdk)
      &&
      (NULL != m_prCDBEndPt)
      &&
      (true == bInitCDBEndPointServices())
      )
   {
      VNCCDBError enStartError = m_prCDBSdk->vncCDBEndpointStart(m_prCDBEndPt);
      bStartSuccess = (VNCCDBErrorNone == enStartError);
      m_bIsEndpointStarted = bStartSuccess;

      if (false == bStartSuccess)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::bStartCDBEndpoint: "
               "EndpointStart failed with error = %x! \n",  ETG_ENUM(CDB_ERROR, enStartError)));
      }
   } //if ((false == m_bIsEndpointStarted)&&...)

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bStartCDBEndpoint() left with result = %u \n",
         ETG_ENUM(BOOL, bStartSuccess)));

   return bStartSuccess;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bStopCDBEndpoint()
***************************************************************************/
t_Bool spi_tclMLVncCmdCDB::bStopCDBEndpoint()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::bStopCDBEndpoint() entered \n"));

   t_Bool bStopSuccess = false;

   //Stop Endpoint if it is running.
   SPI_NORMAL_ASSERT(NULL == m_prCDBSdk);
   SPI_NORMAL_ASSERT(NULL == m_prCDBEndPt);
   if (
      (true == m_bIsEndpointStarted)
      &&
      (NULL != m_prCDBSdk)
      &&
      (NULL != m_prCDBEndPt)
      )
   {
      VNCCDBError enStopError = m_prCDBSdk->vncCDBEndpointStop(m_prCDBEndPt);
      bStopSuccess = (VNCCDBErrorNone == enStopError);

      //! Reset Endpoint status flag
      m_bIsEndpointStarted = false;
      //@Note: m_bIsEndpointStarted flag status should not be dependent on success
      //of vncCDBEndpointStop(), because Endpoint may already stopped by SDK prior
      //to vncCDBEndpointStop() call. Hence flag is simply reset here to enable
      //re-creation of Endpoint.

      //! Cleanup services
      bUninitCDBEndPointServices();
      //@Note: IMPORTANT! Endpoint should be stopped before removing services,
      //since SDK does not support dynamic removal of services!

      if (false == bStopSuccess)
      {
         ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::bStopCDBEndpoint: EndpointStop failed with error = %x! \n",
            ETG_ENUM(CDB_ERROR, enStopError)));
      }
   } //if ((true == m_bIsEndpointStarted)&&...)

   ETG_TRACE_USR2(("spi_tclMLVncCmdCDB::bStopCDBEndpoint() left with result = %u \n",
         ETG_ENUM(BOOL, bStopSuccess)));

   return bStopSuccess;
}


//!Private Static RealVNC SDK Callbacks
/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdCDB::vPostEntityValueCallback(t_Void*...
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdCDB::vPostEntityValueCallback(
   t_Void* pSDKContext,
   VNCDiscoverySDKRequestId u32RequestId,
   VNCDiscoverySDKDiscoverer* pDiscoverer,
   VNCDiscoverySDKEntity* pEntity,
   t_Char* pczKey,
   t_Char* pczValue,
   VNCDiscoverySDKError u32Error)
{
   if (VNCDiscoverySDKErrorNone == u32Error)
   {
      if ((NULL != pczKey) &&
         (NULL != pczValue) &&
         (NULL != pDiscoverer) &&
         (NULL != pSDKContext))
      {
         ETG_TRACE_USR4(("spi_tclMLVncCmdCDB::vPostEntityValueCallback: pczKey = %s \n", pczKey));
         ETG_TRACE_USR4(("spi_tclMLVncCmdCDB::vPostEntityValueCallback: pczValue = %s \n", pczValue));

         trMsgContext rFetchCbCtxt = m_oMsgContext.rGetMsgContext(u32RequestId);

         if (u32LAUNCH_CDB == rFetchCbCtxt.rAppContext.u32ResID)
         {
            spi_tclMLVncCmdCDB* poMLVncCmdCDB = static_cast<spi_tclMLVncCmdCDB*>(pSDKContext);
            t_String szCDBAppID = pczKey;
            poMLVncCmdCDB->vFetchCDBServerUrl(szCDBAppID, pEntity);

         } //if (u32LAUNCH_CDB == rFetchCbCtxt.rAppContext.u32ResID)
      } //if ((NULL != pczKey) && (NULL != pczValue) && (NULL != pDiscoverer)&& (NULL != pSDKContext))
   } //if (VNCDiscoverySDKErrorNone == u32Error)
   else
   {
      ETG_TRACE_ERR(("spi_tclMLVncCmdCDB::vPostEntityValueCallback: VNCDiscoverySDKError = 0x%x \n", u32Error));
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdCDB::vEndpointStatusCallback(VNCCDBEndpoint*...
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdCDB::vEndpointStatusCallback(
      VNCCDBEndpoint* pEndpoint,
      t_Void* pEndpointContext,
      VNCCDBEndpointStatus enStatus,
      vnc_intptr_t AdditionalInfo)
{
   SPI_INTENTIONALLY_UNUSED(pEndpoint);
   SPI_INTENTIONALLY_UNUSED(pEndpointContext);

   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vEndpointStatusCallback() entered: "
         "VNCCDBEndpointStatus = %x, AdditionalInfo = %x \n",
         ETG_ENUM(CDB_ENDPOINT_STATUS, enStatus),
         ETG_ENUM(CDB_ERROR, AdditionalInfo)));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdCDB::vThreadStartedCallback(VNCCDBEndpoint*...
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdCDB::vThreadStartedCallback(
      VNCCDBEndpoint* pEndpoint,
      t_Void* pEndpointContext)
{
   SPI_INTENTIONALLY_UNUSED(pEndpoint);
   SPI_INTENTIONALLY_UNUSED(pEndpointContext);

   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vThreadStartedCallback() entered "));
   DEFINE_THREAD_NAME("VNCCDBSDK");
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdCDB::vThreadStoppedCallback(VNCCDBEndpoint*...
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdCDB::vThreadStoppedCallback(
      VNCCDBEndpoint* pEndpoint,
      t_Void* pEndpointContext,
      VNCCDBError enCDBErrorCode)
{
   SPI_INTENTIONALLY_UNUSED(pEndpoint);
   SPI_INTENTIONALLY_UNUSED(pEndpointContext);

   ETG_TRACE_USR1(("spi_tclMLVncCmdCDB::vThreadStoppedCallback() entered: enCDBErrorCode = %x ",
         ETG_ENUM(CDB_ERROR, enCDBErrorCode)));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdCDB::vLogCallback(VNCCDBEndpoint*...
***************************************************************************/
t_Void VNCCALL spi_tclMLVncCmdCDB::vLogCallback(
      VNCCDBEndpoint* pEndpoint,
      t_Void* pEndpointContext,
      const t_Char* pcczCategory,
      vnc_int32_t s32Severity,
      const t_Char* pcczText)
{
   SPI_INTENTIONALLY_UNUSED(pEndpoint);
   SPI_INTENTIONALLY_UNUSED(pEndpointContext);
   if ((NULL != pcczCategory) && (NULL != pcczText))
   {
      if(s32Severity<=30)
      {
      ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,"spi_tclMLVncCmdCDB::vLogCallback: ps32Severity = %d, pcczCategory = %s \n",
         s32Severity, pcczCategory));
      ETG_TRACE_USR4(("spi_tclMLVncCmdCDB::vLogCallback: pcczText = %s \n", pcczText));
      }
      else
      {
         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,"spi_tclMLVncCmdCDB::vLogCallback: ps32Severity = %d, pcczCategory = %s \n",
            s32Severity, pcczCategory));
         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG,"spi_tclMLVncCmdCDB::vLogCallback: pcczText = %s \n", pcczText));
      }
   } //if ((NULL != pcczCategory) && (NULL != pcczText))
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
