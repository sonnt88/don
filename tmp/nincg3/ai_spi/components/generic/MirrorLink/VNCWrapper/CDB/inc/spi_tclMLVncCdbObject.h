
/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbObject.cpp
* \brief CDB Object Base class
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for a CDB Object
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
07.01.2014  | Ramya Murthy          | Initial Version

\endverbatim
****************************************************************************/

#ifndef _SPI_TCLVNCCDBOBJECT_H_
#define _SPI_TCLVNCCDBOBJECT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H
#include "vnccdbsdk.h"        //For type: VNCCDBSDK
#include "vncsbptypes.h"      //For type: VNCSBPSubscriptionType, VNCSBPProtocolError
#include "vncsbpserialize.h"  //For type: VNCSBPUID, VNCSBPSerialize

#include "SPITypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
enum tenSubscriptionType
{
   u8SUBSCRIPTIONTYPE_REGULARINTERVAL = 0,
   u8SUBSCRIPTIONTYPE_ONCHANGE = 1,
   u8SUBSCRIPTIONTYPE_AUTOMATIC = 2,
   //Add new types here in same order as defined by SDK
   u8SUBSCRIPTIONTYPE_UNKNOWN = 255
};

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_U16 cou16CdbDefaultNotificationIntervalMS = 1000;

/*!
 * * \brief forward declaration
 */

/******************************************************************************/
/*!
* \class spi_tclMLVncCdbObject
* \brief CDB Object Base class, provides some basic functions to store ObjectID &
*        Subscription type and return error for functions which are not supported by an Object.
*******************************************************************************/
class spi_tclMLVncCdbObject
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbObject::spi_tclMLVncCdbObject(const VNCCDBSDK...
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbObject(const VNCCDBSDK* coprCdbSdk)
   * \brief   Constructor
   * \sa      ~spi_tclMLVncCdbObject()
   ***************************************************************************/
   spi_tclMLVncCdbObject(VNCSBPUID u32ObjectUID, const VNCCDBSDK* coprCdbSdk);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbObject::~spi_tclMLVncCdbObject()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCdbObject()
   * \brief   Constructor
   * \sa      spi_tclMLVncCdbObject()
   ***************************************************************************/
   virtual ~spi_tclMLVncCdbObject();

   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::enGet(VNCSBPSeri...
   ***************************************************************************/
   /*!
   * \fn      enGet(VNCSBPSerialize* prSerialize) 
   * \brief   Gets serialised data.
   *          Optional interface to be implemented.
   * \param   prSerialize: [OUT] Pointer to serializer
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa
   ***************************************************************************/
   virtual VNCSBPProtocolError enGet(VNCSBPSerialize* prSerialize);
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::enSet(const...
   ***************************************************************************/
   /*!
   * \fn      enSet(const t_U8* copu8Serialized, t_U32 u32SerializedSize)
   * \brief   Sets serialised data to object.
   *          Optional interface to be implemented.
   * \param   prSerialize: [OUT] Pointer to serialised data
   * \param   u32SerializedSize: [OUT] Size of serialised data
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa
   ***************************************************************************/
   virtual VNCSBPProtocolError enSet(const t_U8* copu8Serialized,
      t_U32 u32SerializedSize);
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::enSubscribe(const...
   ***************************************************************************/
   /*!
   * \fn      enSubscribe(VNCSBPSubscriptionType* penSubType, t_U32* pu32IntervalMs)
   * \brief   Sets the subscription for the object.
   *          Optional interface to be implemented.
   * \param   penSubType  : [IN] Subscription type
   * \param   pu32IntervalMs  : [IN] Time interval between notifications
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa
   ***************************************************************************/
   virtual VNCSBPProtocolError enSubscribe(VNCSBPSubscriptionType* penSubType,
      t_U32* pu32IntervalMs);
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::enCancelSubscribe()
   ***************************************************************************/
   /*!
   * \fn      enCancelSubscribe()
   * \brief   Cancels the subscription for the object.
   *          Optional interface to be implemented.
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa
   ***************************************************************************/
   virtual VNCSBPProtocolError enCancelSubscribe();
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::u32GetObjectUID()
   ***************************************************************************/
   /*!
   * \fn      sbpuidGetObjectUID()
   * \brief   Sets serialised data to object
   * \retval  VNCSBPUID: Unique object ID 
   * \sa
   ***************************************************************************/
   virtual VNCSBPUID u32GetObjectUID() const;

   /***************************************************************************
   ** FUNCTION:  tenSubscriptionType spi_tclMLVncCdbObject::enGetSubscriptionType()
   ***************************************************************************/
   /*!
   * \fn      enGetSubscriptionType()
   * \brief   Sets serialised data to object
   * \param   None
   * \retval  tenSubscriptionType: Subscription type for the object
   * \sa      u32GetObjectUID()
   ***************************************************************************/
   virtual tenSubscriptionType enGetSubscriptionType() const;

protected:
   /***************************************************************************
   ********************************PROTECTED***********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbObject::vSetSubscriptionType(...)
   ***************************************************************************/
   /*!
   * \fn      vSetSubscriptionType(tenSubscriptionType enSubscriptionType)
   * \brief   Sets serialised data to object
   * \param   enSubType: [IN] Subscription type for the object
   * \retval  t_Void
   * \sa      u32GetObjectUID()
   ***************************************************************************/
   virtual t_Void vSetSubscriptionType(tenSubscriptionType enSubType);

   /*!
   * \brief   Pointer to CDB SDK
   *
   * This pointer is retrieved in constructor and will be
   * used throughout the service manager's lifetime.
   */
   const VNCCDBSDK* m_coprCdbSdk;


private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbObject::spi_tclMLVncCdbObject(cons...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbObject(const spi_tclMLVncCdbObject& oObject)
   * \brief   Copy Constructor, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbObject(const spi_tclMLVncCdbObject& oObject);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbObject& spi_tclMLVncCdbObject::operator=(const devp...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbObject& operator=(const spi_tclMLVncCdbObject& oObject)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbObject& operator=(const spi_tclMLVncCdbObject& oObject);

   /*!
   * \brief   Unique object ID
   *
   * Unique object ID which is specified by CCC.
   */
   VNCSBPUID m_u32ObjectUID;

   /*!
   * \brief   Subscription type
   *
   * Subscription type for the object
   */
   tenSubscriptionType m_enSubType;
};

#endif // _SPI_TCLVNCCDBOBJECT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
