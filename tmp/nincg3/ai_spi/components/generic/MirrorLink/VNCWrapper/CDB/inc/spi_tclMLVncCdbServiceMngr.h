/***********************************************************************/
/*!
* \file  spi_tclMLVncCdbServiceMngr.h
* \brief Class to manage SBP services
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to manage SBP services
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVNCCDBSVCMNGR_H_
#define _SPI_TCLVNCCDBSVCMNGR_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <map>
#include <vector>

#define VNC_USE_STDINT_H
#include "vnccdbsdk.h"        //For type: VNCCDBSDK
#include "vnccdbtypes.h"      //For type: VNCCDBEndpoint, VNCCDBServiceId, VNCCDBError
#include "vncsbptypes.h"      //For type: VNCSBPSubscriptionType, VNCSBPProtocolError, VNCSBPCommandType
#include "vncsbpserialize.h"  //For type: VNCSBPUID
#include "vnccall.h"          //For type: VNCCALL

#include "SPITypes.h"
#include "Lock.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/*!
 * * \brief forward declaration
 */
class spi_tclMLVncCdbService;

/******************************************************************************/
/*!
* \class spi_tclMLVncCdbServiceMngr
* \brief Class to manage SBP services of a CDB Source Endpoint.
*******************************************************************************/

class spi_tclMLVncCdbServiceMngr
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbServiceMngr::spi_tclMLVncCdbServiceMngr(VNCC...
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbServiceMngr(VNCCDBEndpoint* prEndpoint, 
   *              const VNCCDBSDK* copCdbSdk)
   * \brief   Constructor
   * \sa      ~spi_tclMLVncCdbServiceMngr()
   ***************************************************************************/
   spi_tclMLVncCdbServiceMngr(VNCCDBEndpoint* prEndpoint, const VNCCDBSDK* copCdbSdk);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbServiceMngr::~spi_tclMLVncCdbServiceMngr()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCdbServiceMngr()
   * \brief   Destructor
   * \sa      spi_tclMLVncCdbServiceMngr()
   ***************************************************************************/
   ~spi_tclMLVncCdbServiceMngr();

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enStartStopService(VNCCD...
   ***************************************************************************/
   /*!
   * \fn      enStartStopService(VNCCDBServiceId u16ServiceId, t_Bool bStartService)
   * \brief   Starts/Stops a service
   * \param   u16ServiceId : [IN] Unique service ID
   * \param   bStartService  : [IN] Indicates Start/Stop function. If set to TRUE,
   *              starts a service. Else stops a service.
   * \param   bResponseRequired  : [IN] Indicates if response should be sent by Service
   *             to SDK, for this request.
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa      enStartCallback(), enStopCallback(), enResetCallback()
   ***************************************************************************/
   VNCCDBError enStartStopService(VNCCDBServiceId u16ServiceId,
         t_Bool bStartService, t_Bool bResponseRequired);

   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enGet(VNCCDBService...
   ***************************************************************************/
   /*!
   * \fn      enGet(VNCCDBServiceId u16ServiceId, VNCSBPUID u32ObjUID,
   *              VNCSBPCommandType enCmdType)
   * \brief   Get Request handler for Get Callback invoked by CDB SDK.
   * \param   u16ServiceId : [IN] Unique service ID
   * \param   u32ObjUID  : [IN] Unique ID of an object in the service
   * \param   enCmdType  : [IN] SBP command type
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa      enGetCallback()
   ***************************************************************************/
   VNCSBPProtocolError enGet(VNCCDBServiceId u16ServiceId,
      VNCSBPUID u32ObjUID,
      VNCSBPCommandType enCmdType);
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enSet(VNCCDBService...
   ***************************************************************************/
   /*!
   * \fn      enSet(VNCCDBServiceId u16ServiceId, VNCSBPUID u32ObjUID,
   *              const t_U8* copu8Payload, t_U32 u32PayloadSize)
   * \brief   Set Request handler for Set Callback invoked by CDB SDK.
   * \param   u16ServiceId : [IN] Unique service ID
   * \param   u32ObjUID  : [IN] Unique ID of an object in the service
   * \param   copu8Payload  : [IN] data payload
   * \param   u32PayloadSize  : [IN] size of payload
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa      enSetCallback()
   ***************************************************************************/
   VNCSBPProtocolError enSet(VNCCDBServiceId u16ServiceId,
      VNCSBPUID u32ObjUID,
      const t_U8* copu8Payload,
      t_U32 u32PayloadSize);
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enSubscribe(VNCCDBS...
   ***************************************************************************/
   /*!
   * \fn      enSubscribe(VNCCDBServiceId u16ServiceId, VNCSBPUID u32ObjUID,
   *              VNCSBPSubscriptionType* penSubType,t_U32* pu32IntervalMs)
   * \brief   Subscribe Request handler for Subscribe Callback invoked by CDB SDK.
   * \param   u16ServiceId : [IN] Unique service ID
   * \param   u32ObjUID  : [IN] Unique ID of an object in the service
   * \param   penSubType  : [IN] Subscription type
   * \param   pu32IntervalMs  : [IN] Time interval between notifications
   *              in milliseconds.
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa      enSubscribeCallback()
   ***************************************************************************/
   VNCSBPProtocolError enSubscribe(VNCCDBServiceId u16ServiceId,
      VNCSBPUID u32ObjUID,
      VNCSBPSubscriptionType* penSubType,
      t_U32* pu32IntervalMs);

   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enCancelSubscribe(...
   ***************************************************************************/
   /*!
   * \fn      enCancelSubscribe(VNCCDBServiceId u16ServiceId, VNCSBPUID u32ObjUID)
   * \brief   CancelSubscribe Request handler for CancelSubscribe Callback
   *              invoked by CDB SDK.
   * \param   u16ServiceId : [IN] Unique service ID
   * \param   u32ObjUID  : [IN] Unique ID of an object in the service
   * \retval  VNCSBPProtocolError:SBP protocol error code
   * \sa      enCancelCallback()
   ***************************************************************************/
   VNCSBPProtocolError enCancelSubscribe(VNCCDBServiceId u16ServiceId,
      VNCSBPUID u32ObjUID);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vSendSubscribeResponse(...
   ***************************************************************************/
   /*!
   * \fn       vSendSubscribeResponse(VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError)
   * \brief    Handler to send Subscribe response to SDK.
   * \param    u16ServiceId : [IN] Unique service ID
   * \param    u32ObjUID  : [IN] Unique object ID
   * \param    enSbpError : [IN] SBP error to be sent in the reponse
   * \retval   None
   ***************************************************************************/
   t_Void vSendSubscribeResponse(VNCCDBServiceId u16ServiceId,
      VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError);
   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vSendSetResponse(...
   ***************************************************************************/
   /*!
   * \fn       vSendSetResponse(VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError)
   * \brief    Handler to send Set response to SDK.
   * \param    u16ServiceId : [IN] Unique service ID
   * \param    u32ObjUID  : [IN] Unique object ID
   * \param    enSbpError : [IN] SBP error to be sent in the reponse
   * \retval   None
   ***************************************************************************/
   t_Void vSendSetResponse(VNCCDBServiceId u16ServiceId,
      VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCdbServiceMngr::bSetServicesList(std::ve...
   ***************************************************************************/
   /*!
   * \fn      bSetServicesList(std::vector<trCdbServiceConfig>& SvcConfigList)
   * \brief   Interface to set required services & their availability.
   * \param   coSvcConfigList : [IN] List of services & their availability
   * \retval  t_Bool: TRUE if services are added/removed successfully.
   * \sa      vGetServicesList()
   ***************************************************************************/
   t_Bool bSetServicesList(const std::vector<trCdbServiceConfig>& coSvcConfigList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCdbServiceMngr::bFindService(tenCDBServic...
   ***************************************************************************/
   /*!
   * \fn      bFindService(tenCDBServiceType enServiceType, 
   *              VNCCDBServiceId& rfu32ServiceID)
   * \brief   Searches for a service of type enServiceType in m_ServicesMap
   *              with Service ID
   * \param   enServiceType : [IN] Service type enumeration
   * \param   rfu32ServiceID : [OUT] Unique Service ID (not modified if service
   *              is not found)
   * \retval  t_Bool: TRUE if service exists in m_ServicesMap, else FALSE.
   ***************************************************************************/
   t_Bool bFindService(tenCDBServiceType enServiceType,
 		VNCCDBServiceId& rfu32ServiceID);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCdbServiceMngr::bAddService(tenCDBServic...
   ***************************************************************************/
   /*!
   * \fn      bAddService(tenCDBServiceType enServiceType)
   * \brief   Adds a service of type enServiceType to be managed by Service
   *              Manager. If there already exists a service of enServiceType,
   *              no new service is added.
   * \param   enServiceType : [IN] Service type enumeration
   * \retval  t_Bool: TRUE if service is added, else FALSE.
   * \sa      bRemoveService()
   ***************************************************************************/
   t_Bool bAddService(tenCDBServiceType enServiceType);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCdbServiceMngr::bRemoveService(tenCDBSer...
   ***************************************************************************/
   /*!
   * \fn      bRemoveService(tenCDBServiceType enServiceType)
   * \brief   Removes a service of type enServiceType managed by Service
   *              Manager. If service of  enServiceType does not exist,
   *              no service is removed.
   * \param   enServiceType : [IN] Service type enumeration
   * \retval  t_Bool: TRUE if service is removed, else FALSE.
   * \sa      bAddService()
   ***************************************************************************/
   t_Bool bRemoveService(tenCDBServiceType enServiceType);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vClearServicesMap()
   ***************************************************************************/
   /*!
   * \fn      vClearServicesMap()
   * \brief   Removes all services in ServicesMap.
   * \param   None
   * \retval  t_Void
   ***************************************************************************/
   t_Void vClearServicesMap();


   /*****************Start of callbacks provided to SDK***********************/

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enStartCallback(VNC...
   ***************************************************************************/
   /*!
   * \fn      enStartCallback(VNCCDBEndpoint* prEndpoint, t_Void* pEndpointContext,
   *              VNCCDBServiceId u16ServiceId, t_Void* pServiceCtxt,
   *              t_U8 u8MajorVersion, t_U8 u8MinorVersion)
   * \brief   Static function, whose address is passed in SBP Source Callbacks struct.
   *              Called by CDB SDK to start a service.
   * \param   prEndpoint       : [IN] Pointer to CDB Endpoint that is the source for the service.
   * \param   pEndpointContext : [IN] Endpoint context pointer specified by the
   *             application in the VNCCDBEndpointDescriptor (i.e, ServiceManager).
   * \param   u16ServiceId     : [IN] ID of a service, as returned by
   *             VNCCDBEndpointAddService().
   * \param   pServiceCtxt     : [IN] Service context pointer specified by the application
   *             in the VNCCDBService structure (i.e, ServiceManager).
   * \param   u8MajorVersion   : [IN] Service version requested by the peer endpoint.
   * \param   u8MinorVersion   : [IN] Service version requested by the peer endpoint.
   * \retval  VNCCDBError: CDB error code
   * \sa      enStartStopService()
   ***************************************************************************/
   static VNCCDBError VNCCALL enStartCallback(VNCCDBEndpoint* prEndpoint,
         t_Void* pEndpointContext,
         VNCCDBServiceId u16ServiceId,
         t_Void* pServiceCtxt,
         t_U8 u8MajorVersion,
         t_U8 u8MinorVersion);

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enStopCallback(VNC...
   ***************************************************************************/
   /*!
   * \fn      enStopCallback(VNCCDBEndpoint* prEndpoint, t_Void* pEndpointContext,
   *              VNCCDBServiceId u16ServiceId, t_Void* pServiceCtxt)
   * \brief   Static function, whose address is passed in SBP Source Callbacks struct.
   *              Called by CDB SDK to stop a service.
   * \param   prEndpoint       : [IN] Pointer to CDB Endpoint that is the source for the service.
   * \param   pEndpointContext : [IN] Endpoint context pointer specified by the
   *             application in the VNCCDBEndpointDescriptor (i.e, ServiceManager).
   * \param   u16ServiceId     : [IN] ID of a service, as returned by
   *             VNCCDBEndpointAddService().
   * \param   pServiceCtxt     : [IN] Service context pointer specified by the application
   *             in the VNCCDBService structure (i.e, ServiceManager).
   * \retval  VNCCDBError: CDB error code
   * \sa      enStartStopService()
   ***************************************************************************/
   static VNCCDBError VNCCALL enStopCallback(VNCCDBEndpoint* prEndpoint,
         t_Void* pEndpointContext,
         VNCCDBServiceId u16ServiceId,
         t_Void* pServiceCtxt);

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enResetCallback(VNC...
   ***************************************************************************/
   /*!
   * \fn      enResetCallback(VNCCDBEndpoint* prEndpoint, VNCCDBServiceId u16ServiceId,
   *              t_Void* pServiceCtxt)
   * \brief   Static function, whose address is passed in SBP Source Callbacks struct.
   *              Called by CDB SDK to reset a service. (no action required on service
   *              side, just an information to service to re-initialise itself)
   * \param   prEndpoint   : [IN] Pointer to CDB Endpoint that is the source for the service.
   * \param   u16ServiceId : [IN] ID of a service, as returned by
   *             VNCCDBEndpointAddService().
   * \param   pServiceCtxt : [IN] Service context pointer specified by the application
   *             in the VNCCDBService structure (i.e, ServiceManager).
   * \retval  VNCCDBError: CDB error code
   * \sa      enStartStopService()
   ***************************************************************************/
   static VNCCDBError VNCCALL enResetCallback(VNCCDBEndpoint* prEndpoint,
         VNCCDBServiceId u16ServiceId,
         t_Void* pServiceCtxt);

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enGetCallback(VNC...
   ***************************************************************************/
   /*!
   * \fn      enGetCallback(VNCCDBEndpoint* prEndpoint, VNCCDBServiceId u16ServiceId,
   *              t_Void* pServiceCtxt, VNCSBPCommandType enCmdType,
   *              VNCSBPUID u32ObjUID, VNCSBPProtocolError* penErrCode)
   * \brief   Static function, whose address is passed in SBP Source Callbacks struct.
   *              Called by CDB SDK to get an object's data in a service.
   * \param   prEndpoint   : [IN] Pointer to CDB Endpoint that is the source for the service.
   * \param   u16ServiceId : [IN] ID of a service, as returned by
   *             VNCCDBEndpointAddService().
   * \param   pServiceCtxt : [IN] Service context pointer specified by the application
   *             in the VNCCDBService structure (i.e, ServiceManager).
   * \param   enCmdType    : [IN] SBP command type (Get or Subscribe).
   * \param   u32ObjUID    : [IN] Object for which state has been requested.
   * \param   penErrCode   : [OUT] SBP error to use if local SBP error is returned
   * \retval  VNCCDBError: CDB error code as follows:
   *          If VNCCDBErrorNone - Get response has been / will be sent asynchronously
   *              with VNCSBPSourceSendGetResponse().
   *          If VNCCDBErrorLocalSBPError - Send 'penErrCode' as immediate response with
   *              empty payload.
   * \sa      enGet()
   ***************************************************************************/
   static VNCCDBError VNCCALL enGetCallback(VNCCDBEndpoint* prEndpoint,
      VNCCDBServiceId u16ServiceId,
      t_Void* pServiceCtxt,
      VNCSBPCommandType enCmdType,
      VNCSBPUID u32ObjUID,
      VNCSBPProtocolError* penErrCode);

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enSetCallback(VNC...
   ***************************************************************************/
   /*!
   * \fn      enSetCallback(VNCCDBEndpoint* prEndpoint, VNCCDBServiceId u16ServiceId,
   *              t_Void* pServiceCtxt, VNCSBPUID u32ObjUID,
   *              const t_U8* copu8Payload, size_t PayloadSize, 
   *              VNCSBPProtocolError* penErrCode)
   * \brief   Static function, whose address is passed in SBP Source Callbacks struct.
   *              Called by CDB SDK to set data to an object of a service. 
   * \param   prEndpoint   : [IN] Pointer to CDB Endpoint that is the source for the service.
   * \param   u16ServiceId : [IN] ID of a service, as returned by
   *             VNCCDBEndpointAddService().
   * \param   pServiceCtxt : [IN] Service context pointer specified by the application
   *             in the VNCCDBService structure (i.e, ServiceManager).
   * \param   u32ObjUID    : [IN] Object for which state has been requested.
   * \param   copu8Payload : [IN] Pointer to serialized payload supplying updated object state
   * \param   u32PayloadSize : [IN] Bytes available in 'copu8Payload'
   * \param   penErrCode   : [OUT] SBP error to use if local SBP error is returned.
   * \retval  VNCCDBError: CDB error code as follows:
   *          If VNCCDBErrorNone - Set response has been / will be sent asynchronously
   *              with VNCSBPSourceSendSetResponse().
   *          If VNCCDBErrorLocalSBPError - Send 'penErrCode' as immediate response.
   * \sa      enSet()
   ***************************************************************************/
   static VNCCDBError VNCCALL enSetCallback(VNCCDBEndpoint* prEndpoint,
      VNCCDBServiceId u16ServiceId,
      t_Void* pServiceCtxt,
      VNCSBPUID u32ObjUID,
      const t_U8* copu8Payload,
      size_t PayloadSize,
      VNCSBPProtocolError* penErrCode);

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enSubscribeCallback(...
   ***************************************************************************/
   /*!
   * \fn      enSubscribeCallback(VNCCDBEndpoint* prEndpoint, VNCCDBServiceId u16ServiceId,
   *              t_Void* pServiceCtxt, VNCSBPUID u32ObjUID, 
   *              VNCSBPSubscriptionType* enSubtype, t_U32* u32IntervalMs, 
   *              VNCSBPProtocolError* penErrCode)
   * \brief   Static function, whose address is passed in SBP Source Callbacks struct.
   *              Called by CDB SDK to subscribe to a service's object.
   * \param   prEndpoint    : [IN] Pointer to CDB Endpoint that is the source for the service.
   * \param   u16ServiceId  : [IN] ID of a service, as returned by
   *             VNCCDBEndpointAddService().
   * \param   pServiceCtxt  : [IN] Service context pointer specified by the application
   *             in the VNCCDBService structure (i.e, ServiceManager).
   * \param   u32ObjUID     : [IN] Object for which state has been requested.
   * \param   enSubtype     : [IO] Requested subscription type (if set to automatic,
   *              should be changed by service to Interval based or On change).
   * \param   u32IntervalMs : [IO] Requested update interval in milliseconds.
   *              (If subscription type is automatic & service chooses
   *              to provide interval based subscription, interval value should be 
   *              set by the service).
   * \param   penErrCode    : [OUT] SBP protocol error code, to be set by service
   *              in case of an error in processing Get request.
   * \retval  VNCCDBError: CDB error code as follows:
   *          If VNCCDBErrorNone - Set response has been / will be sent asynchronously
   *              with VNCSBPSourceSendSetResponse().
   *          If VNCCDBErrorLocalSBPError - Send errorCode as immediate response.
   * \sa      enSubscribe()
   ***************************************************************************/
   static VNCCDBError VNCCALL enSubscribeCallback(VNCCDBEndpoint* prEndpoint,
      VNCCDBServiceId u16ServiceId,
      t_Void* pServiceCtxt,
      VNCSBPUID u32ObjUID,
      VNCSBPSubscriptionType* enSubtype,
      t_U32* u32IntervalMs,
      VNCSBPProtocolError* penErrCode);

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enCancelCallback(...
   ***************************************************************************/
   /*!
   * \fn      enCancelCallback(VNCCDBEndpoint*, VNCCDBServiceId u16ServiceId,
   *              t_Void* pServiceCtxt, VNCSBPCommandType enCmdType, VNCSBPUID u32ObjUID)
   * \brief   Static function, whose address is passed in SBP Source Callbacks struct.
   *              Called by CDB SDK to cancel a request already made to a service.
   * \param   prEndpoint   : [IN] Pointer to CDB Endpoint that is the source for the service.
   * \param   u16ServiceId : [IN] ID of a service, as returned by
   *             VNCCDBEndpointAddService().
   * \param   pServiceCtxt : [IN] Service context pointer specified by the application
   *             in the VNCCDBService structure (i.e, ServiceManager).
   * \param   enCmdType    : [IN] Command type being cancelled.
   * \param   u32ObjUID    : [IN] Object for which command is being cancelled
   * \retval  VNCCDBError: CDB error code - Reserved for future expansion,
   *             always return VNCCDBErrorNone
   * \sa      enCancelSubscribe()
   ***************************************************************************/
   static VNCCDBError VNCCALL enCancelCallback(VNCCDBEndpoint* prEndpoint,
      VNCCDBServiceId u16ServiceId,
      t_Void* pServiceCtxt,
      VNCSBPCommandType enCmdType,
      VNCSBPUID u32ObjUID);

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enResponseErrorCallback(...
   ***************************************************************************/
   /*!
   * \fn      enResponseErrorCallback(VNCCDBEndpoint*, VNCCDBServiceId u16ServiceId,
   *              t_Void* pServiceCtxt, VNCSBPCommandType enCmdType,
   *              VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpErrorCode)
   * \brief   Static function, whose address is passed in SBP Source Callbacks struct.
   *              Called by CDB SDK to cancel a request already made to a service.
   * \param   prEndpoint   : [IN] Pointer to CDB Endpoint that is the source for the service.
   * \param   u16ServiceId : [IN] ID of a service, as returned by
   *             VNCCDBEndpointAddService().
   * \param   pServiceCtxt : [IN] Service context pointer specified by the application
   *             in the VNCCDBService structure (i.e, ServiceManager).
   * \param   enCmdType    : [IN] Command type being cancelled.
   * \param   u32ObjUID    : [IN] Object for which command is being cancelled
   * \param   enSbpErrorCode : [IN] Response SBP error code
   * \retval  VNCCDBError: CDB error code - Reserved for future expansion,
   *             always return VNCCDBErrorNone
   * \sa
   ***************************************************************************/
   static VNCCDBError VNCCALL enResponseErrorCallback(VNCCDBEndpoint* prEndpoint,
      VNCCDBServiceId u16ServiceId,
      t_Void* pServiceCtxt,
      t_Void* pCommandContext,
      VNCSBPCommandType enCmdType,
      VNCSBPUID u32ObjUID,
      VNCCDBError enCDBSdkError,
      VNCSBPProtocolError enSbpErrorCode);

   /******************End of callbacks provided to SDK************************/


   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbServiceMngr::spi_tclMLVncCdbServiceMngr(cons...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbServiceMngr(const spi_tclMLVncCdbServiceMngr& oServiceMngr)
   * \brief   Copy Constructor, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbServiceMngr(const spi_tclMLVncCdbServiceMngr& oServiceMngr);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbServiceMngr& spi_tclMLVncCdbServiceMngr::operator=(const devp...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbServiceMngr& operator=(const spi_tclMLVncCdbServiceMngr& oServiceMngr)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbServiceMngr& operator=(const spi_tclMLVncCdbServiceMngr& oServiceMngr);

   /*!
   * \brief   Pointer to CDB SDK
   *
   * This pointer is retrieved in constructor and will be 
   * used throughout the service manager's lifetime.
   */
   const VNCCDBSDK* m_prCdbSdk;

   /*!
   * \brief   Pointer to CDB Endpoint
   *
   * This pointer is retrieved in constructor and will be
   * used throughout the service manager's lifetime.
   */
   VNCCDBEndpoint* m_prEndpoint;

   /*!
   * \brief   Map of CDB services
   *
   * Contains all supported & available CDB services. 
   * Key : is the unique service ID assigned by CDB CDK when a 
   *       service is added to Endpoint.
   * Value : Pointer to Service with service ID as that of Key.
   */
   std::map<VNCCDBServiceId, spi_tclMLVncCdbService*> m_ServicesMap;

   /*!
   * \brief   Structure containing SBP source related callbacks pointers.
   *
   * These callbacks are used by CDB SDK to send requests to services.
   * The service requested is identified by unique service ID in the callbacks
   * and rerouted to correct service by the service manager.
   */
   VNCSBPSourceCallbacks m_SbpSrcCallbacks;

   /*!
   * \brief   Lock object
   */
   Lock  m_oLock;

};

#endif // _SPI_TCLVNCCDBSVCMNGR_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
