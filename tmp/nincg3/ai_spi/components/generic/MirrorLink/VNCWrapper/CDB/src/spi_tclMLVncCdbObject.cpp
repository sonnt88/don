
/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbObject.cpp
* \brief CDB Object Base class
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for a CDB Object 
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
07.01.2014  | Ramya Murthy          | Initial Version
06.05.2015  |Tejaswini HB                 |Lint Fix

\endverbatim
*****************************************************************************/

/*****************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|---------------------------------------------------------------------------*/
#include "spi_tclMLVncCdbObject.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCdbObject.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbObject::spi_tclMLVncCdbObject(const VNCCDBSDK...
***************************************************************************/
spi_tclMLVncCdbObject::spi_tclMLVncCdbObject(VNCSBPUID u32ObjectUID,
   const VNCCDBSDK* coprCdbSdk) 
   : m_coprCdbSdk(coprCdbSdk),
     m_u32ObjectUID(u32ObjectUID),
     m_enSubType(u8SUBSCRIPTIONTYPE_UNKNOWN)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbObject() entered "));
   SPI_NORMAL_ASSERT(NULL == m_coprCdbSdk);
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbObject::~spi_tclMLVncCdbObject()
***************************************************************************/
spi_tclMLVncCdbObject::~spi_tclMLVncCdbObject()
{
   ETG_TRACE_USR1(("~spi_tclMLVncCdbObject() entered "));
   m_coprCdbSdk = NULL;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::enGet(VNCSBPSeri...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbObject::enGet(
   VNCSBPSerialize* prSerialize)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbObject::enGet() entered "));
   SPI_INTENTIONALLY_UNUSED(prSerialize);
   
   //@Note: Error is returned since this class is not meant to provide any functionality.
   //(only serves as a base class for an actual service's Object).
   return VNCSBPProtocolErrorFeatureNotSupported;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::enSet(const...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbObject::enSet(
   const t_U8* copu8Serialized,
   t_U32 u32SerializedSize)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbObject::enSet() entered "));
   SPI_INTENTIONALLY_UNUSED(copu8Serialized);
   SPI_INTENTIONALLY_UNUSED(u32SerializedSize);

   //@Note: Error is returned since this class is not meant to provide any functionality.
   //(only serves as a base class for an actual service's Object).
   return VNCSBPProtocolErrorWriteNotAllowed;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::enSubscribe(const...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbObject::enSubscribe(
   VNCSBPSubscriptionType* penSubType,
   t_U32* pu32IntervalMs)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbObject::enSubscribe() entered "));
   SPI_INTENTIONALLY_UNUSED(pu32IntervalMs);
   SPI_INTENTIONALLY_UNUSED(penSubType);

   //@Note: Error is returned since this class is not meant to provide any functionality.
   //(only serves as a base class for an actual service's Object).
   return VNCSBPProtocolErrorFeatureNotSupported;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::enCancelSubscribe()
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbObject::enCancelSubscribe()
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbObject::enCancelSubscribe() entered "));
   
   //@Note: Error is returned since this class is not meant to provide any functionality.
   //(only serves as a base class for an actual service's Object).
   return VNCSBPProtocolErrorFeatureNotSupported;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbObject::u32GetObjectUID()
***************************************************************************/
VNCSBPUID spi_tclMLVncCdbObject::u32GetObjectUID() const
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbObject::u32GetObjectUID() left with ObjectID = 0x%x ",
         m_u32ObjectUID));
   return m_u32ObjectUID;
}

/***************************************************************************
** FUNCTION:  tenSubscriptionType spi_tclMLVncCdbObject::enGetSubscriptionType()
***************************************************************************/
tenSubscriptionType spi_tclMLVncCdbObject::enGetSubscriptionType() const
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbObject::enGetSubscriptionType() left with SubscriptionType = 0x%x ",
         ETG_ENUM(SBP_SUBSCRIPTIONTYPE, m_enSubType)));
   return m_enSubType;
}

/***************************************************************************
********************************PROTECTED***********************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbObject::vSetSubscriptionType(...)
***************************************************************************/
t_Void spi_tclMLVncCdbObject::vSetSubscriptionType(tenSubscriptionType enSubType)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbObject::vSetSubscriptionType() entered: SubscriptionType = 0x%x ",
         ETG_ENUM(SBP_SUBSCRIPTIONTYPE, enSubType)));
   m_enSubType = enSubType;
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
