/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbGPSNmeaObject.cpp
* \brief GPS NMEA Object class
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    GPS NMEA Object class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
24.03.2013  | Ramya Murthy          | Initial Version (moved class from
                                      spi_tclMLVncCdbGPSService.h)

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include<cmath>
#include "spi_tclMLVncCdbGPSNmeaObject.h"
#include "NmeaEncoder.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCdbGPSNmeaObject.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static t_String szNmeaSentences = "";
static t_Char SentencesBuffer[256];

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbGPSNmeaObject::spi_tclMLVncCdbGPSNmeaObject(...
***************************************************************************/
spi_tclMLVncCdbGPSNmeaObject::spi_tclMLVncCdbGPSNmeaObject(
   const VNCCDBSDK* coprCdbSdk)
   : spi_tclMLVncCdbObject(cou32GPS_NMEA_OBJECT_UID, coprCdbSdk)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGPSNmeaObject() entered "));
   SPI_NORMAL_ASSERT(NULL == coprCdbSdk);

   vClearSentencesBuffer();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbGPSNmeaObject::vOnData(const trGPSData& rfcorGpsData)
***************************************************************************/
t_Void spi_tclMLVncCdbGPSNmeaObject::vOnData(const trGPSData& rfcorGpsData)
{
   //! Update stored GPS data
   m_oLock.s16Lock();
   m_rGpsData = rfcorGpsData;
   m_oLock.vUnlock();

   //@TODO: Send data to SDK in case of OnChange subscription type.
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbGPSNmeaObject::vOnData(const trSensorData& rfcorSensorData)
***************************************************************************/
t_Void spi_tclMLVncCdbGPSNmeaObject::vOnData(const trSensorData& rfcorSensorData)
{
   //! Update stored sensor data
   m_oLock.s16Lock();
   m_rSensorData = rfcorSensorData;
   m_oLock.vUnlock();
}

/***********Start of methods overridden from spi_tclMLVncCdbObject**********/

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbGPSNmeaObject::enGet(VNCSBPSerialize* ...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbGPSNmeaObject::enGet(VNCSBPSerialize* prSerialize)
{
   VNCSBPProtocolError enSbpError = VNCSBPProtocolErrorNotAvailable;

   //! Using VNC serialize APIs, serialize Nmea Object data (sentences in bytes + timestamp)
   SPI_NORMAL_ASSERT(NULL == m_coprCdbSdk);
   if ((NULL != m_coprCdbSdk) && (NULL != prSerialize))
   {
      t_U64 u64Timestamp = 0;
      t_U32 u32SentencesSize = 0;
      const t_S8* ps8Sentences = (t_S8*)pchGetSentences(u64Timestamp, u32SentencesSize);

      SPI_NORMAL_ASSERT(NULL == ps8Sentences);
      if (NULL != ps8Sentences)
      {
         //! Serialize the data
         if (
            (VNCCDBErrorNone == m_coprCdbSdk->vncSBPSerializeBytes(
                  prSerialize, cou32GPS_NMEA_DATA_FIELD_UID, ps8Sentences, u32SentencesSize))
            &&
            (VNCCDBErrorNone == m_coprCdbSdk->vncSBPSerializeLong(
                  prSerialize, cou32GPS_NMEA_TIMESTAMP_FIELD_UID, u64Timestamp))
            )
         {
            enSbpError = VNCSBPProtocolErrorNone;
         }
      } //if (NULL != ps8Sentences)
   } //if ((NULL != m_coprCdbSdk)&&...)
   else
   {
      enSbpError = VNCSBPProtocolErrorGenericRecoverable;
   }

   ETG_TRACE_USR4(("spi_tclMLVncCdbGPSNmeaObject::enGet() left with "
         "VNCSBPProtocolError = %x ", ETG_ENUM(SBP_ERROR, enSbpError)));
   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbGPSNmeaObject::enSubscribe(...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbGPSNmeaObject::enSubscribe(
   VNCSBPSubscriptionType* penSubType, t_U32* pu32IntervalMs)
{
   VNCSBPProtocolError enSbpError = VNCSBPProtocolErrorNone;

   //! Evaluate the subscription type
   if ((NULL != penSubType) && (NULL != pu32IntervalMs))
   {
      ETG_TRACE_USR3(("spi_tclMLVncCdbGPSNmeaObject::enSubscribe: "
            "entered with: penSubType = %x, pu32IntervalMs = %u(ms)",
            ETG_ENUM(SBP_SUBSCRIPTIONTYPE, *penSubType),
            *pu32IntervalMs));

      switch (*penSubType)
      {
         case VNCSBPSubscriptionTypeRegularInterval:
         {
            //SDK handles this type. SDK automatically calls VNCSBPSourceGetRequestCallback()
            //function at the correct interval to obtain the update to send to the Sink.

            //Set error if requested interval is lesser than minimum supported interval(1s)
			//(interval >= minimum interval is ok)
            if (cou16CdbDefaultNotificationIntervalMS > *pu32IntervalMs)
            {
               enSbpError = VNCSBPProtocolErrorFeatureNotSupported ; //VNCSBPProtocolErrorWrongSubscriptionInterval;
            }
         }
            break;
         case VNCSBPSubscriptionTypeOnChange:
         {
            enSbpError = VNCSBPProtocolErrorWrongSubscriptionType;
         }
            break;
         case VNCSBPSubscriptionTypeAutomatic:
         {
            //By default, we support interval based subscription
            //Hence we update the return values for SDK
            *penSubType = VNCSBPSubscriptionTypeRegularInterval;
            *pu32IntervalMs = cou16CdbDefaultNotificationIntervalMS;
         }
            break;
         default:
            ETG_TRACE_ERR(("spi_tclMLVncCdbGPSNmeaObject::enSubscribe: "
                  "Invalid SubscriptionType enum encountered: %x! ",
                  ETG_ENUM(SBP_SUBSCRIPTIONTYPE, *penSubType)));
            break;
      } //switch (*penSubType)

       vSetSubscriptionType(static_cast<tenSubscriptionType>(*penSubType));

       ETG_TRACE_USR3(("spi_tclMLVncCdbGPSNmeaObject::enSubscribe: "
             "left with: penSubType = %x, pu32IntervalMs = %u(ms)",
             ETG_ENUM(SBP_SUBSCRIPTIONTYPE, *penSubType),
             *pu32IntervalMs));
   }

   ETG_TRACE_USR4(("spi_tclMLVncCdbGPSNmeaObject::enSubscribe() left with "
         "VNCSBPProtocolError = %x ", ETG_ENUM(SBP_ERROR, enSbpError)));
   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbGPSNmeaObject::enCancelSubscribe()
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbGPSNmeaObject::enCancelSubscribe()
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGPSNmeaObject::enCancelSubscribe() entered "));
   vSetSubscriptionType(u8SUBSCRIPTIONTYPE_UNKNOWN);
   return VNCSBPProtocolErrorNone;
}
/************End of methods overridden from spi_tclMLVncCdbObject**********/


/***************************************************************************
********************************PROTECTED***********************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbGPSNmeaObject::pchGetSentences(t_U64* u64Timestam...
***************************************************************************/
t_Char* spi_tclMLVncCdbGPSNmeaObject::pchGetSentences(t_U64& rfu64Timestamp,
   t_U32& rfu32SentencesSize)
{
   //! Convert GPS & Sensor data to NMEA sentences

   m_oLock.s16Lock();

   //Dummy data,not used
   trVehicleData rVehicleData;
   NmeaEncoder oNmeaEncoder(m_rGpsData, m_rSensorData,rVehicleData);
   rfu64Timestamp = static_cast<t_U64>(abs(m_rGpsData.PosixTime));
   m_oLock.vUnlock();

   //! Combine required NMEA sentences into a single string
   szNmeaSentences = oNmeaEncoder.szGetNmeaGGASentence(e8NMEA_SOURCE_GPS);
   szNmeaSentences.append(oNmeaEncoder.szGetNmeaRMCSentence(e8NMEA_SOURCE_GPS));

   //! Copy string into data buffer
   vClearSentencesBuffer();
   rfu32SentencesSize = szNmeaSentences.size();
   memcpy(SentencesBuffer, (const t_Char*)(szNmeaSentences.c_str()), rfu32SentencesSize);
   SentencesBuffer[rfu32SentencesSize] = '\0'; 
   //@Note: This is done as precaution. But it is not really required since vClearSentencesBuffer() 
   //causes all bytes to be set to null character.

   ETG_TRACE_USR4(("spi_tclMLVncCdbGPSNmeaObject::pchGetSentences left with: "
         "rfu32SentencesSize = %u, SentencesBuffer = %s ",
         rfu32SentencesSize, SentencesBuffer));
   ETG_TRACE_USR4(("spi_tclMLVncCdbGPSNmeaObject::pchGetSentences left with: "
         "rfu64Timestamp = 0x%x ",
         rfu64Timestamp));
   //@Note: ETG trace macro does not work as expected when printing 64-bit values
   //in combination with other data. Hence 64-bit data is printed separately.

   return SentencesBuffer;
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbGPSNmeaObject::vClearSentencesBuffer()
***************************************************************************/
t_Void spi_tclMLVncCdbGPSNmeaObject::vClearSentencesBuffer()
{
   //! Clear all data in buffer
   memset(SentencesBuffer, 0, sizeof(SentencesBuffer));
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
