/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbGPSService.cpp
* \brief GPS CDB service class
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    GPS CDB service class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version
24.03.2013  | Ramya Murthy          | Moved out GPS NMEA Object and NMEA
                                      Description Object classes

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclMLVncCdbGPSService.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCdbGPSService.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbGPSService::spi_tclMLVncCdbGPSService(const ...
***************************************************************************/
spi_tclMLVncCdbGPSService::spi_tclMLVncCdbGPSService(
   const VNCCDBSDK* coprCdbSdk,
   VNCCDBServiceAccessControl enAccessCntrl)
   : spi_tclMLVncCdbService(
        coprCdbSdk, 
        copczGPS_SERVICE_NAME,
        u8CDB_VERSION_MAJOR,
        u8CDB_VERSION_MINOR,
        VNCCDBServiceConfigurationNone, //TODO - to check default value
        enAccessCntrl,
        e8CDB_GPS_SERVICE),
     m_oNmeaObject(coprCdbSdk),
     m_oNmeaDescriptionObject(coprCdbSdk)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGPSService() entered \n"));

   //Add objects of service
   vAddObject(&m_oNmeaObject);
   vAddObject(&m_oNmeaDescriptionObject);
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbGPSService::~spi_tclMLVncCdbGPSService()
***************************************************************************/
spi_tclMLVncCdbGPSService::~spi_tclMLVncCdbGPSService()
{
   ETG_TRACE_USR1(("~spi_tclMLVncCdbGPSService() entered \n"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbGPSService::vOnData(const trGPSData& rfcorGpsData)
***************************************************************************/
t_Void spi_tclMLVncCdbGPSService::vOnData(const trGPSData& rfcorGpsData)
{
   //! Forward data to GPS NMEA Object class
   m_oNmeaObject.vOnData(rfcorGpsData);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbGPSService::vOnData(const trSensorData& rfcorSensorData)
***************************************************************************/
t_Void spi_tclMLVncCdbGPSService::vOnData(const trSensorData& rfcorSensorData)
{
   //! Forward data to GPS NMEA Object class
   m_oNmeaObject.vOnData(rfcorSensorData);
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
