/***********************************************************************/
/*!
 * \file  spi_tclMLVncCDBDispatcher.h
 * \brief Message Dispatcher for CDB Messages. implemented using
 *        double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for CDB Messages
 AUTHOR:         Ramya Murthy
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 01.04.2014  | Ramya Murthy          | Initial Version (taken from spi_tclMLVncDAPDispatcher.h)

 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLMLVNCCDBDISPATCHER_H_
#define SPI_TCLMLVNCCDBDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "RespRegister.h"

/**************Forward Declarations******************************************/
class spi_tclMLVncCDBDispatcher;

/****************************************************************************/
/*!
 * \class MLCDBMsgBase
 * \brief Base Message type for all CDB messages
 ****************************************************************************/
class MLCDBMsgBase: public trMsgBase
{
   public:

   /***************************************************************************
    ** FUNCTION:  MLCDBMsgBase::MLCDBMsgBase
    ***************************************************************************/
   /*!
    * \fn      MLCDBMsgBase()
    * \brief   Default constructor
    **************************************************************************/
   MLCDBMsgBase();

   /***************************************************************************
    ** FUNCTION:  MLCDBMsgBase::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclMLVncCDBDispatcher* poCDBDispatcher)
    * \brief   Pure virtual function to be overridden by inherited classes for
    *          dispatching the message
    * \param   poCDBDispatcher : pointer to Message dispatcher for CDB
    **************************************************************************/
   virtual t_Void vDispatchMsg(spi_tclMLVncCDBDispatcher* poCDBDispatcher) = 0;

   /***************************************************************************
    ** FUNCTION:  MLCDBMsgBase::~MLCDBMsgBase
    ***************************************************************************/
   /*!
    * \fn      ~MLCDBMsgBase()
    * \brief   Destructor
    **************************************************************************/
   virtual ~MLCDBMsgBase()
   {

   }
};

/****************************************************************************/
/*!
 * \class MLLocDataSubscriptionSetMsg
 * \brief ML Location Data subscription msg
 ****************************************************************************/
class MLLocDataSubscriptionSetMsg: public MLCDBMsgBase
{
   public:

   t_Bool bSubscribe;

   /***************************************************************************
    ** FUNCTION:  MLLocDataSubscriptionSetMsg::MLLocDataSubscriptionSetMsg
    ***************************************************************************/
   /*!
    * \fn      MLLocDataSubscriptionSetMsg()
    * \brief   Default constructor
    **************************************************************************/
   MLLocDataSubscriptionSetMsg();

   /***************************************************************************
    ** FUNCTION:  MLLocDataSubscriptionSetMsg::~MLLocDataSubscriptionSetMsg
    ***************************************************************************/
   /*!
    * \fn      ~MLLocDataSubscriptionSetMsg()
    * \brief   Destructor
    **************************************************************************/
   virtual ~MLLocDataSubscriptionSetMsg(){}

   /***************************************************************************
    ** FUNCTION:  MLLocDataSubscriptionSetMsg::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclMLVncCDBDispatcher* poCDBDispatcher)
    * \brief   virtual function for dispatching the message of 'this' type
    * \param   poCDBDispatcher : pointer to Message dispatcher for CDB
    **************************************************************************/
   t_Void vDispatchMsg(spi_tclMLVncCDBDispatcher* poCDBDispatcher);

   /***************************************************************************
    ** FUNCTION:  MLLocDataSubscriptionSetMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg() {}

   /***************************************************************************
    ** FUNCTION:  MLLocDataSubscriptionSetMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*!
 * \class spi_tclMLVncCDBDispatcher
 * \brief Message Dispatcher for CDB Messages
 ****************************************************************************/
class spi_tclMLVncCDBDispatcher
{
   public:
   /***************************************************************************
    ** FUNCTION:  spi_tclMLVncCDBDispatcher::spi_tclMLVncCDBDispatcher
    ***************************************************************************/
   /*!
    * \fn      spi_tclMLVncCDBDispatcher()
    * \brief   Default constructor
    **************************************************************************/
   spi_tclMLVncCDBDispatcher();

   /***************************************************************************
    ** FUNCTION:  spi_tclMLVncCDBDispatcher::~spi_tclMLVncCDBDispatcher
    ***************************************************************************/
   /*!
    * \fn      ~spi_tclMLVncCDBDispatcher()
    * \brief   Destructor
    **************************************************************************/
   ~spi_tclMLVncCDBDispatcher();

   /***************************************************************************
    ** FUNCTION:  spi_tclMLVncCDBDispatcher::vHandleCDBMsg(MLLocDataSubscriptionSetMsg*...)
    ***************************************************************************/
   /*!
    * \fn      vHandleCDBMsg(MLLocDataSubscriptionSetMsg* poLocDataSubscriptionSetMsg)
    * \brief   Handles Messages of MLLocDataSubscriptionSetMsg type
    * \param   poLocDataSubscriptionSetMsg : pointer to MLLocDataSubscriptionSetMsg.
    **************************************************************************/
   t_Void vHandleCDBMsg(MLLocDataSubscriptionSetMsg* poLocDataSubscriptionSetMsg) const;

};

#endif /* SPI_TCLMLVNCCDBDISPATCHER_H_ */
