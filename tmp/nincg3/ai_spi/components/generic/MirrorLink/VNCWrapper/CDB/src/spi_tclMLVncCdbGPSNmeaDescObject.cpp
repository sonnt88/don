/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbGPSNmeaDescObject.cpp
* \brief GPS NMEA Description Object class
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    GPS NMEA Description Object class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
24.03.2013  | Ramya Murthy          | Initial Version (moved class from
                                      spi_tclMLVncCdbGPSService.h)

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclMLVncCdbGPSNmeaDescObject.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCdbGPSNmeaDescObject.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbGPSNmeaDescObject::spi_tclMLVncCdbGPSNmeaDescObject(...
***************************************************************************/
spi_tclMLVncCdbGPSNmeaDescObject::spi_tclMLVncCdbGPSNmeaDescObject(
   const VNCCDBSDK* coprCdbSdk)
   : spi_tclMLVncCdbObject(cou32GPS_NMEA_DESC_OBJECT_UID, coprCdbSdk)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGPSNmeaDescObject() entered \n"));
   SPI_NORMAL_ASSERT(NULL == coprCdbSdk);
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbGPSNmeaDescObject::~spi_tclMLVncCdbGPSNmeaDescObject()
***************************************************************************/
spi_tclMLVncCdbGPSNmeaDescObject::~spi_tclMLVncCdbGPSNmeaDescObject()
{
   ETG_TRACE_USR1(("~spi_tclMLVncCdbGPSNmeaDescObject() entered \n"));
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbGPSNmeaDescObject::enGet(VNCSBPSerialize*...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbGPSNmeaDescObject::enGet(VNCSBPSerialize* prSerialize)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGPSNmeaDescObject::enGet() entered \n"));

   VNCSBPProtocolError enSbpError = VNCSBPProtocolErrorGenericRecoverable;

   if ((NULL != m_coprCdbSdk) && (NULL != prSerialize))
   {
      t_U32 u32SuppSentences = u32GetSupportedSentences();

      enSbpError = (VNCCDBErrorNone == m_coprCdbSdk->vncSBPSerializeInt(
            prSerialize, cou32GPS_NMEA_DESC_SUPP_SENTENCES_UID, u32SuppSentences)) ?
            (VNCSBPProtocolErrorNone) : (enSbpError);
   }
   return enSbpError;
}

/***************************************************************************
********************************PROTECTED***********************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbGPSNmeaDescObject::s32GetSupportedSentences()
***************************************************************************/
t_U32 spi_tclMLVncCdbGPSNmeaDescObject::u32GetSupportedSentences()
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGPSNmeaDescObject::u32GetSupportedSentences() entered \n"));

   return (cou32GPS_SUPPORT_GGA | cou32GPS_SUPPORT_RMC);
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
