/***********************************************************************/
/*!
* \file  spi_tclMLVncCdbGPSNmeaObject.h
* \brief GPS NMEA Object class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    GPS NMEA Object class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
24.03.2013  | Ramya Murthy          | Initial Version (moved class from
                                      spi_tclMLVncCdbGPSService.h)

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVNCCDBGPSNMEAOBJECT_H_
#define _SPI_TCLVNCCDBGPSNMEAOBJECT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H
#include "vnccdbsdk.h"
#include "vnccdbtypes.h"
#include "vncsbptypes.h"
#include "vncsbpserialize.h"

#include "BaseTypes.h"
#include "spi_tclMLVncCdbObject.h"
#include "Lock.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
 * * \brief forward declaration
 */
class spi_tclMLVncCdbGPSService;

/******************************************************************************/
/*!
* \class spi_tclMLVncCdbGPSNmeaObject
* \brief 
*******************************************************************************/
class spi_tclMLVncCdbGPSNmeaObject : public spi_tclMLVncCdbObject
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/
   
   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaObject::spi_tclMLVncCdbGPSNmeaObject(spi_tclMLVncCdbG...
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGPSNmeaObject(spi_tclMLVncCdbGPSService& rfoGPSService,
   *              const VNCCDBSDK* coprCdbSdk)
   * \brief   Constructor
   * \sa      ~spi_tclMLVncCdbGPSNmeaObject()
   ***************************************************************************/
   spi_tclMLVncCdbGPSNmeaObject(const VNCCDBSDK* coprCdbSdk);
   
   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaObject::~spi_tclMLVncCdbGPSNmeaObject()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCdbGPSNmeaObject()
   * \brief   Constructor
   * \sa      spi_tclMLVncCdbGPSNmeaObject()
   ***************************************************************************/
   virtual ~spi_tclMLVncCdbGPSNmeaObject() { }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbGPSNmeaObject::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbGPSNmeaObject::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

   /***********Start of methods overridden from spi_tclMLVncCdbObject**********/

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbGPSNmeaObject::enGet(VNCSBPSerialize* ...
   ***************************************************************************/
   /*!
   * \fn       enGet(VNCSBPSerialize* prSerialize)
   * \brief    Get serialized data
   * \param    prSerialize  : [IN] Pointer to serializer
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   * \sa       enSet()
   ***************************************************************************/
   virtual VNCSBPProtocolError enGet(VNCSBPSerialize* prSerialize);
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbGPSNmeaObject::enSubscribe(...
   ***************************************************************************/
   /*!
   * \fn        enSubscribe(VNCSBPSubscriptionType* penSubType, t_U32* pu32IntervalMs
   * \brief     Sets subscription of object
   * \param     penSubType  : [OUT] Pointer to subscription type
   * \param     pu32IntervalMs : [OUT] Pointer to Interval in milliseconds
   * \retval    VNCSBPProtocolError:SBP protocol error code
   * \sa        enCancelSubscribe()
   ***************************************************************************/
   virtual VNCSBPProtocolError enSubscribe(VNCSBPSubscriptionType* penSubType,
      t_U32* pu32IntervalMs);

   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbGPSNmeaObject::enCancelSubscribe()
   ***************************************************************************/
   /*!
   * \fn      enCancelSubscribe()
   * \brief   Cancels subscription of object
   * \retval  t_Void
   * \sa      enSubscribe()
   ***************************************************************************/
   virtual VNCSBPProtocolError enCancelSubscribe();

   /************End of methods overridden from spi_tclMLVncCdbObject**********/

protected:
   /***************************************************************************
   ********************************PROTECTED***********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Char* spi_tclMLVncCdbGPSNmeaObject::pchGetSentences(t_U64&...
   ***************************************************************************/
   /*!
   * \fn       pchGetSentences(t_U64& rfu64Timestamp, t_U32& rfu32SentencesSize)
   * \brief    Provides a pointer to NMEA sentences
   * \param    rfu64Timestamp  : [OUT] Timestamp
   * \param    rfu32SentencesSize : [OUT] Total size of sentences
   * \retval   Pointer to sentence(s)
   * \sa
   ***************************************************************************/
   virtual t_Char* pchGetSentences(t_U64& rfu64Timestamp,
      t_U32& rfu32SentencesSize);

private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbGPSNmeaObject::vClearSentencesBuffer()
   ***************************************************************************/
   /*!
   * \fn       vClearSentencesBuffer()
   * \brief    Clears the NMEA sentences buffer
   ***************************************************************************/
   t_Void vClearSentencesBuffer();

   /**************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaObject::spi_tclMLVncCdbGPSNmeaObject
   **                   (const spi_tclMLVncCdbGPSNmeaObject& oObject)
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGPSNmeaObject(const spi_tclMLVncCdbGPSNmeaObject& oObject)
   * \brief   Copy Constructor, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbGPSNmeaObject(const spi_tclMLVncCdbGPSNmeaObject& oObject);

   /**************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSNmeaObject& spi_tclMLVncCdbGPSNmeaObject::
   **                   operator=(const spi_tclMLVncCdbGPSNmeaObject& oObject)
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGPSNmeaObject& operator=(
   *             const spi_tclMLVncCdbGPSNmeaObject& oObject)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbGPSNmeaObject& operator=(const spi_tclMLVncCdbGPSNmeaObject& oObject);

   /*!
   * \brief   Structure containing GPS data
   */
   trGPSData  m_rGpsData;

   /*!
   * \brief   Structure containing GPS data
   */
   trSensorData  m_rSensorData;

   /*!
   * \brief   Lock variable
   */
   Lock  m_oLock;
};

#endif // _SPI_TCLVNCCDBGPSNMEAOBJECT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
