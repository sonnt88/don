/***********************************************************************/
/*!
 * \file  spi_tclMLVncCDBDispatcher.h
 * \brief Message Dispatcher for CDB Messages. implemented using
 *        double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for CDB Messages
 AUTHOR:         Ramya Murthy
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 01.04.2014  | Ramya Murthy          | Initial Version (taken from spi_tclMLVncDAPDispatcher.h)

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "spi_tclMLVncCDBDispatcher.h"
#include "spi_tclMLVncRespCDB.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclMLVncCDBDispatcher.cpp.trc.h"
   #endif
#endif

/***************************************************************************
 ** FUNCTION:  MLCDBMsgBase::MLCDBMsgBase
 ***************************************************************************/
MLCDBMsgBase::MLCDBMsgBase()
{
   ETG_TRACE_USR1((" %s entered ", __PRETTY_FUNCTION__));
   vSetServiceID(e32MODULEID_VNCCDB);
}

/***************************************************************************
 ** FUNCTION:  MLLocDataSubscriptionSetMsg::MLLaunchCDBMsg
 ***************************************************************************/
MLLocDataSubscriptionSetMsg::MLLocDataSubscriptionSetMsg(): bSubscribe(false)
{
   ETG_TRACE_USR1((" %s entered ", __PRETTY_FUNCTION__));
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLLocDataSubscriptionSetMsg::vDispatchMsg
 ***************************************************************************/
t_Void MLLocDataSubscriptionSetMsg::vDispatchMsg(
         spi_tclMLVncCDBDispatcher* poCDBDispatcher)
{
   ETG_TRACE_USR1((" %s entered ", __PRETTY_FUNCTION__));
   if (NULL != poCDBDispatcher)
   {
      poCDBDispatcher->vHandleCDBMsg(this);
   }
   vDeAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncCDBDispatcher::spi_tclMLVncCDBDispatcher
 ***************************************************************************/
spi_tclMLVncCDBDispatcher::spi_tclMLVncCDBDispatcher()
{
   ETG_TRACE_USR1((" %s entered ", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncCDBDispatcher::~spi_tclMLVncCDBDispatcher
 ***************************************************************************/
spi_tclMLVncCDBDispatcher::~spi_tclMLVncCDBDispatcher()
{
   ETG_TRACE_USR1((" %s entered ", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncCDBDispatcher::vHandleCDBMsg(MLLocDataSubscriptionSetMsg...)
 ***************************************************************************/
t_Void spi_tclMLVncCDBDispatcher::
      vHandleCDBMsg(MLLocDataSubscriptionSetMsg* poLocDataSubscriptionSetMsg)const
{
   ETG_TRACE_USR1((" %s entered ", __PRETTY_FUNCTION__));
   if (NULL != poLocDataSubscriptionSetMsg)
   {
      CALL_REG_OBJECTS(spi_tclMLVncRespCDB,
         e16CDB_REGID,
         vPostSetLocDataSubscription(poLocDataSubscriptionSetMsg->bSubscribe));
   }
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
