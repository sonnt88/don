/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbServiceMngr.cpp
* \brief Class to manage SBP services
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to manage SBP services
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version
21.05.2015  | Ramya Murthy          | Support for Location Service

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclMLVncCdbService.h"
#include "spi_tclMLVncCdbGPSService.h"
#include "spi_tclMLVncCdbLocService.h"
#include "spi_tclMLVncCdbServiceMngr.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCdbServiceMngr.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef std::map<VNCCDBServiceId, spi_tclMLVncCdbService*>::iterator tSpiSvcMapItr;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbServiceMngr::spi_tclMLVncCdbServiceMngr(VNCC...
***************************************************************************/
spi_tclMLVncCdbServiceMngr::spi_tclMLVncCdbServiceMngr(
   VNCCDBEndpoint* prEndpoint,
   const VNCCDBSDK* coprCdbSdk)
   : m_prEndpoint(prEndpoint),
     m_prCdbSdk(coprCdbSdk)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbServiceMngr() entered \n"));
   SPI_NORMAL_ASSERT(NULL == m_prEndpoint);
   SPI_NORMAL_ASSERT(NULL == m_prCdbSdk);

   memset(&m_SbpSrcCallbacks, 0, sizeof(m_SbpSrcCallbacks));
   m_SbpSrcCallbacks.pStartCallback = &enStartCallback;
   m_SbpSrcCallbacks.pStopCallback  = &enStopCallback;
   m_SbpSrcCallbacks.pResetCallback = &enResetCallback;
   m_SbpSrcCallbacks.pGetRequestCallback = &enGetCallback;
   m_SbpSrcCallbacks.pSetRequestCallback = &enSetCallback;
   m_SbpSrcCallbacks.pSubscribeRequestCallback = &enSubscribeCallback;
   m_SbpSrcCallbacks.pCancelRequestCallback = &enCancelCallback;
   m_SbpSrcCallbacks.pResponseErrorCallback = &enResponseErrorCallback;

}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbServiceMngr::~spi_tclMLVncCdbServiceMngr()
***************************************************************************/
spi_tclMLVncCdbServiceMngr::~spi_tclMLVncCdbServiceMngr()
{
   ETG_TRACE_USR1(("~spi_tclMLVncCdbServiceMngr() entered \n"));
   
   //Destory all services
   vClearServicesMap();

	m_prEndpoint = NULL;
	m_prCdbSdk = NULL;
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enStartStopService(VNCCD...
***************************************************************************/
VNCCDBError spi_tclMLVncCdbServiceMngr::enStartStopService(
   VNCCDBServiceId u16ServiceId,
   t_Bool bStartService,
   t_Bool bResponseRequired)
{
   m_oLock.s16Lock();
   tSpiSvcMapItr SvcItr = m_ServicesMap.find(u16ServiceId);
   VNCCDBError enCdbError = (m_ServicesMap.end() == SvcItr) ?
         (VNCCDBErrorNotFound) : (VNCCDBErrorNone);
   spi_tclMLVncCdbService* poService = (VNCCDBErrorNone == enCdbError) ?
         (SvcItr->second) : (NULL);
   m_oLock.vUnlock();

   if (NULL != poService)
   {
      poService->vStartStopService(bStartService, bResponseRequired);
      enCdbError = (bStartService != poService->bIsServiceActive()) ?
            VNCCDBErrorFailed : enCdbError;
   }

   ETG_TRACE_USR3(("spi_tclMLVncCdbServiceMngr::enStartStopService"
         "(for u16ServiceId = 0x%x, bStartService = %u) left with VNCCDBError = %x \n",
         u16ServiceId,
         ETG_ENUM(BOOL, bStartService),
         ETG_ENUM(CDB_ERROR, enCdbError)));

   return enCdbError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enGet(VNCCDBService...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enGet(
   VNCCDBServiceId u16ServiceId,
   VNCSBPUID u32ObjUID,
   VNCSBPCommandType enCmdType)
{
   //Forward get request to service

   m_oLock.s16Lock();
   tSpiSvcMapItr SvcItr = m_ServicesMap.find(u16ServiceId);
   VNCSBPProtocolError enSbpError  = (m_ServicesMap.end() == SvcItr) ?
         (VNCSBPProtocolErrorGenericRecoverable) : (VNCSBPProtocolErrorNone);
   spi_tclMLVncCdbService* poService = (VNCSBPProtocolErrorNone == enSbpError) ?
         (SvcItr->second) : (NULL);
   m_oLock.vUnlock();

   if (NULL != poService)
   {
      enSbpError = poService->enSendGetResponse(u32ObjUID, enCmdType);
   }

   ETG_TRACE_USR3(("spi_tclMLVncCdbServiceMngr::enGet"
         "(for u16ServiceId = 0x%x, u32ObjUID = 0x%x, enCmdType = %x) left with VNCSBPProtocolError = %x \n",
         u16ServiceId,
         u32ObjUID,
         ETG_ENUM(SBP_CMDTYPE, enCmdType),
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enSet(VNCCDBService...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enSet(
   VNCCDBServiceId u16ServiceId,
   VNCSBPUID u32ObjUID,
   const t_U8* copu8Payload,
   t_U32 u32PayloadSize)
{
   //Forward set request to service
   VNCSBPProtocolError enSbpError = (NULL == copu8Payload) ?
         (VNCSBPProtocolErrorMissingMandatoryData) : (VNCSBPProtocolErrorNone);

   if (VNCSBPProtocolErrorNone == enSbpError)
   {
      m_oLock.s16Lock();
      tSpiSvcMapItr SvcItr = m_ServicesMap.find(u16ServiceId);
      enSbpError  = (m_ServicesMap.end() == SvcItr) ?
            (VNCSBPProtocolErrorGenericRecoverable) : (VNCSBPProtocolErrorNone);
      spi_tclMLVncCdbService* poService = (VNCSBPProtocolErrorNone == enSbpError) ?
            (SvcItr->second) : (NULL);
      m_oLock.vUnlock();

      if (NULL != poService)
      {
         enSbpError = poService->enSet(u32ObjUID, copu8Payload, u32PayloadSize);
      }
   }

   ETG_TRACE_USR3(("spi_tclMLVncCdbServiceMngr::enSet"
         "(for u16ServiceId = 0x%x, u32ObjUID = 0x%x, u32PayloadSize = %u) left with VNCSBPProtocolError = %x \n",
         u16ServiceId,
         u32ObjUID,
         u32PayloadSize,
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enSubscribe(VNCCDBS...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enSubscribe(
   VNCCDBServiceId u16ServiceId,
   VNCSBPUID u32ObjUID,
   VNCSBPSubscriptionType* penSubType,
   t_U32* pu32IntervalMs)
{
   //Forward subscribe request to service
   VNCSBPProtocolError enSbpError = ((NULL == penSubType) || (NULL == pu32IntervalMs)) ?
         (VNCSBPProtocolErrorMissingMandatoryData) : (VNCSBPProtocolErrorNone);

   if (VNCSBPProtocolErrorNone == enSbpError)
   {
      m_oLock.s16Lock();
      tSpiSvcMapItr SvcItr = m_ServicesMap.find(u16ServiceId);
      enSbpError  = (m_ServicesMap.end() == SvcItr) ?
            (VNCSBPProtocolErrorGenericRecoverable) : (VNCSBPProtocolErrorNone);
      spi_tclMLVncCdbService* poService = (VNCSBPProtocolErrorNone == enSbpError) ?
            (SvcItr->second) : (NULL);
      m_oLock.vUnlock();

      if (NULL != poService)
      {
         enSbpError = poService->enSubscribe(u32ObjUID, penSubType, pu32IntervalMs);
      }
   }

   ETG_TRACE_USR3(("spi_tclMLVncCdbServiceMngr::enSubscribe"
         "(for u16ServiceId = 0x%x, u32ObjUID = 0x%x) left with VNCSBPProtocolError = %x \n",
         u16ServiceId,
         u32ObjUID,
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enCancelSubscribe(...
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbServiceMngr::enCancelSubscribe(
   VNCCDBServiceId u16ServiceId, 
   VNCSBPUID u32ObjUID)
{
   //Forward cancel subscribe request to service

   m_oLock.s16Lock();
   tSpiSvcMapItr SvcItr = m_ServicesMap.find(u16ServiceId);
   VNCSBPProtocolError enSbpError  = (m_ServicesMap.end() == SvcItr) ?
         (VNCSBPProtocolErrorGenericRecoverable) : (VNCSBPProtocolErrorNone);
   spi_tclMLVncCdbService* poService = (VNCSBPProtocolErrorNone == enSbpError) ?
         (SvcItr->second) : (NULL);
   m_oLock.vUnlock();

   if (NULL != poService)
   {
      enSbpError = poService->enCancelSubscribption(u32ObjUID);
   }

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::enCancelSubscribe"
         "(for u16ServiceId = 0x%x, u32ObjUID = 0x%x) left with VNCSBPProtocolError = %x \n",
         u16ServiceId,
         u32ObjUID,
         ETG_ENUM(SBP_ERROR, enSbpError)));

   return enSbpError;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vSendSubscribeResponse(...
***************************************************************************/
t_Void spi_tclMLVncCdbServiceMngr::vSendSubscribeResponse(
   VNCCDBServiceId u16ServiceId,
   VNCSBPUID u32ObjUID,
   VNCSBPProtocolError enSbpError)
{
   m_oLock.s16Lock();
   tSpiSvcMapItr SvcItr = m_ServicesMap.find(u16ServiceId);
   spi_tclMLVncCdbService* poService = (m_ServicesMap.end() != SvcItr) ?
         (SvcItr->second) : (NULL);
   m_oLock.vUnlock();

   if (NULL != poService)
   {
      poService->vSendSubscribeResponse(u32ObjUID, enSbpError);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vSendSetResponse(...
***************************************************************************/
t_Void spi_tclMLVncCdbServiceMngr::vSendSetResponse(VNCCDBServiceId u16ServiceId,
   VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError)
{
   m_oLock.s16Lock();
   tSpiSvcMapItr SvcItr = m_ServicesMap.find(u16ServiceId);
   spi_tclMLVncCdbService* poService = (m_ServicesMap.end() != SvcItr) ?
         (SvcItr->second) : (NULL);
   m_oLock.vUnlock();

   if (NULL != poService)
   {
      poService->vSendSetResponse(u32ObjUID, enSbpError);
   }
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCdbServiceMngr::bSetServicesList(const std::vec...
***************************************************************************/
t_Bool spi_tclMLVncCdbServiceMngr::bSetServicesList(
   const std::vector<trCdbServiceConfig>& coSvcConfigList)
{
   //@Note: IMPORTANT! The following should be ensured before function this is called:
   //(1) If service(s) need to be added to ServiceManager, it should be done only before Endpoint is started.
   //(2) If service(s) should be removed from ServiceManager, it should be done only after Endpoint is stopped.

   t_Bool bSuccess = false;

   //! Remove all existing services
   vClearServicesMap();

   //! If ServiceConfigList is not empty, add services as per the input ServiceConfigList
   if (0 != coSvcConfigList.size())
   {
      for (t_U32 u32LstIndex = 0; u32LstIndex < coSvcConfigList.size(); u32LstIndex++)
      {
         tenCDBServiceType enServiceType = coSvcConfigList[u32LstIndex].enServiceType;
         t_Bool bServiceEnabled = coSvcConfigList[u32LstIndex].bServiceEnabled;

         ETG_TRACE_USR4(("spi_tclMLVncCdbServiceMngr::bSetServicesList: "
               "Received ServiceType = %x, ServiceEnabled = %u \n",
               ETG_ENUM(CDB_SERVICE_TYPE, enServiceType),
               ETG_ENUM(BOOL, bServiceEnabled)));

         //! Add service if it is enabled in the ServiceConfigList
         if (true == bServiceEnabled)
         {
            t_Bool bAddSvcSuccess = bAddService(enServiceType);
            bSuccess = bSuccess || bAddSvcSuccess; //TODO - check if logic can be changed
            if (false == bAddSvcSuccess)
            {
               ETG_TRACE_ERR(("spi_tclMLVncCdbServiceMngr::bSetServicesList: Adding Service failed! \n"));
            }
         }
      } //for (t_U32 u32LstIndex = ...)
   } //if(0 != coSvcConfigList.size())
   else
   {
      bSuccess = true;
   }

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::bSetServicesList"
         "(coSvcConfigList size = %u) left with result = %u \n",
         coSvcConfigList.size(),
         ETG_ENUM(BOOL, bSuccess)));

   return bSuccess;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vOnData(const trGPSData& rfcorGpsData)
***************************************************************************/
t_Void spi_tclMLVncCdbServiceMngr::vOnData(const trGPSData& rfcorGpsData)
{
   //! Forward data to Service classes
   m_oLock.s16Lock();
   for (tSpiSvcMapItr SvcItr = m_ServicesMap.begin();
       SvcItr != m_ServicesMap.end();
       ++SvcItr)
   {
	   if (NULL != SvcItr->second)
	   {
		   SvcItr->second->vOnData(rfcorGpsData);
	   }
   }
   m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vOnData(const trSensorData& rfcorSensorData)
***************************************************************************/
t_Void spi_tclMLVncCdbServiceMngr::vOnData(const trSensorData& rfcorSensorData)
{
   //! Forward data to Service classes
   m_oLock.s16Lock();
   for (tSpiSvcMapItr SvcItr = m_ServicesMap.begin();
       SvcItr != m_ServicesMap.end();
       ++SvcItr)
   {
	   if (NULL != SvcItr->second)
	   {
		   SvcItr->second->vOnData(rfcorSensorData);
	   }
   }
   m_oLock.vUnlock();
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCdbServiceMngr::bFindService(tenCDBServic...
***************************************************************************/
t_Bool spi_tclMLVncCdbServiceMngr::bFindService(tenCDBServiceType enServiceType,
   VNCCDBServiceId& rfu32ServiceID)
{
   t_Bool bServiceExists = false;

   m_oLock.s16Lock();

   for (tSpiSvcMapItr SvcItr = m_ServicesMap.begin();
       SvcItr != m_ServicesMap.end();
       ++SvcItr)
   {
      SPI_NORMAL_ASSERT(NULL == SvcItr->second);
      if (
         (NULL != SvcItr->second)
         &&
         (enServiceType == (SvcItr->second->enGetServiceType()))
         )
      {
         bServiceExists = true;
         //Copy the Service ID to return parameter
         rfu32ServiceID = SvcItr->first;
         break;
      }
   } //for (tSvcMapItr SvcItr...)

   m_oLock.vUnlock();

   if (false == bServiceExists)
   {
      ETG_TRACE_ERR(("spi_tclMLVncCdbServiceMngr::bFindService: Service %x not found! \n",
            ETG_ENUM(CDB_SERVICE_TYPE, enServiceType)));
   }
   return bServiceExists;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCdbServiceMngr::bAddService(tenCDBServic...
***************************************************************************/
t_Bool spi_tclMLVncCdbServiceMngr::bAddService(tenCDBServiceType enServiceType)
{
   t_Bool bAddSuccess = false;
   VNCCDBServiceId u32ServID = 0; //not used

   //! Add a new service of type enServiceType to Endpoint & ServicesMap list.
   SPI_NORMAL_ASSERT(NULL == m_prCdbSdk);
   SPI_NORMAL_ASSERT(NULL == m_prEndpoint);
   if ((NULL != m_prCdbSdk) && (NULL != m_prEndpoint) && (false == bFindService(enServiceType, u32ServID)))
   {
      //! Create new service
      spi_tclMLVncCdbService* poService = NULL;

      switch (enServiceType)
      {
         case e8CDB_GPS_SERVICE:
         {
            poService = new spi_tclMLVncCdbGPSService(m_prCdbSdk, VNCCDBServiceAccessControlUnlimited);
         }
            break;
         case e8CDB_LOC_SERVICE:
         {
            poService = new spi_tclMLVncCdbLocService(m_prCdbSdk, VNCCDBServiceAccessControlUnlimited);
         }
            break;
         default:
            ETG_TRACE_ERR(("spi_tclMLVncCdbServiceMngr::bAddService: Invalid ServiceType enum = %x",
                  ETG_ENUM(CDB_SERVICE_TYPE, enServiceType)));
            break;
      } //switch (enServiceType)

      //! Add the new service to Endpoint.
      if (NULL != poService)
      {
          VNCCDBError enRetError = poService->enAddToEndpoint(
                m_prEndpoint,
                &m_SbpSrcCallbacks,
                sizeof(m_SbpSrcCallbacks),
                this);

          //If service was added successfully, include it in internal ServiceMap.
          if (VNCCDBErrorNone == enRetError)
          {
             typedef std::pair<VNCCDBServiceId, spi_tclMLVncCdbService*> tServiceMapPair;

             m_oLock.s16Lock();
             m_ServicesMap.insert(tServiceMapPair(poService->u32GetServiceId(), poService));
             m_oLock.vUnlock();
             bAddSuccess = true;
          }
          else
          {
              ETG_TRACE_ERR(("spi_tclMLVncCdbServiceMngr::bAddService: VNCCDBError for AddToEndpoint = %x \n",
                         ETG_ENUM(CDB_ERROR, enRetError)));
              //RELEASE_MEM(poService); //To resolve LINT warning
              delete poService;
              poService= NULL;
          }
      }
   } //if ((NULL != m_prEndpoint)&&...)

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::bAddService"
         "(for enServiceType = %x) left with result = %u \n",
         ETG_ENUM(CDB_SERVICE_TYPE, enServiceType),
         ETG_ENUM(BOOL, bAddSuccess)));

   return bAddSuccess;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCdbServiceMngr::bRemoveService(tenCDBSer...
***************************************************************************/
t_Bool spi_tclMLVncCdbServiceMngr::bRemoveService(tenCDBServiceType enServiceType)
{
   t_Bool bRemSuccess = false;
   VNCCDBServiceId u32ServID = 0;

   //! Remove service from ServicesMap list.
   //@Note: IMPORTANT! Endpoint should be stopped before removing services,
   //since SDK does not support dynamic removal of services! (taken care by CmdCDB class)
   if (true == bFindService(enServiceType, u32ServID))
   {
      //! IMPORTANT - Lock is used after bFindService() call is complete, since
      //! bFindService() also uses the same lock.

      m_oLock.s16Lock();

      //Deallocate memory used by service
      RELEASE_MEM(m_ServicesMap[u32ServID]);
      //Remove service from Map
      m_ServicesMap.erase(u32ServID);

      m_oLock.vUnlock();

      bRemSuccess = true;
   }

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::bRemoveService"
         "(for enServiceType = %x) left with result = %u \n",
         ETG_ENUM(CDB_SERVICE_TYPE, enServiceType),
         ETG_ENUM(BOOL, bRemSuccess)));

   return bRemSuccess;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbServiceMngr::vClearServicesMap()
***************************************************************************/
t_Void spi_tclMLVncCdbServiceMngr::vClearServicesMap()
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbServiceMngr::vClearServicesMap() entered \n"));

   m_oLock.s16Lock();

   //! Remove all services from ServicesMap
   for (tSpiSvcMapItr Itr = m_ServicesMap.begin(); Itr != m_ServicesMap.end(); Itr++)
   {
      //De-allocate memory used by service
      if (NULL != Itr->second)
      {
         RELEASE_MEM(m_ServicesMap[Itr->first]);
      }
   }
   //! Remove all entries from map
   m_ServicesMap.clear();

   m_oLock.vUnlock();
}


/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enStartCallback(VNC...
***************************************************************************/
VNCCDBError VNCCALL spi_tclMLVncCdbServiceMngr::enStartCallback(
   VNCCDBEndpoint* prEndpoint,
   t_Void* pEndpointContext,
   VNCCDBServiceId u16ServiceId,
   t_Void* pServiceCtxt,
   t_U8 u8MajorVersion,
   t_U8 u8MinorVersion)
{
   SPI_INTENTIONALLY_UNUSED(prEndpoint);
   SPI_INTENTIONALLY_UNUSED(pEndpointContext); //@Note: Cmd class context. Hence unused.

   VNCCDBError enCdbError = VNCCDBErrorLocalSBPError;
   if (NULL != pServiceCtxt)
   {
      spi_tclMLVncCdbServiceMngr* poManager =
         static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      enCdbError = poManager->enStartStopService(u16ServiceId, true, true);
   }

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::enStartCallback"
         "(for u16ServiceId = 0x%x, u8MajorVersion = %u, u8MinorVersion = %u) left with VNCCDBError = %x \n",
         u16ServiceId,
         u8MajorVersion,
         u8MinorVersion,
         ETG_ENUM(CDB_ERROR, enCdbError)));

   return VNCCDBErrorNone;
   //@note: Return value reserved for future expansion, always return VNCCDBErrorNone.
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enStopCallback(VNC...
***************************************************************************/
VNCCDBError VNCCALL spi_tclMLVncCdbServiceMngr::enStopCallback(
   VNCCDBEndpoint* prEndpoint,
   t_Void* pEndpointContext,
   VNCCDBServiceId u16ServiceId,
   t_Void* pServiceCtxt)
{
   SPI_INTENTIONALLY_UNUSED(prEndpoint);
   SPI_INTENTIONALLY_UNUSED(pEndpointContext); //@Note: Cmd class context. Hence unused.

   VNCCDBError enCdbError = VNCCDBErrorLocalSBPError;
   if (NULL != pServiceCtxt)
   {
      spi_tclMLVncCdbServiceMngr* poManager =
         static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      enCdbError = poManager->enStartStopService(u16ServiceId, false, true);
   }

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::enStopCallback"
         "(for u16ServiceId = 0x%x) left with VNCCDBError = %x \n",
         u16ServiceId,
         ETG_ENUM(CDB_ERROR, enCdbError)));

   return VNCCDBErrorNone;
   //@note: Return value reserved for future expansion, always return VNCCDBErrorNone.
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enResetCallback(VNC...
***************************************************************************/
VNCCDBError VNCCALL spi_tclMLVncCdbServiceMngr::enResetCallback(
   VNCCDBEndpoint* prEndpoint,
   VNCCDBServiceId u16ServiceId,
   t_Void* pServiceCtxt)
{
   SPI_INTENTIONALLY_UNUSED(prEndpoint);

   ETG_TRACE_USR1(("spi_tclMLVncCdbServiceMngr::enResetCallback() entered: "
         "u16ServiceId = 0x%x \n", u16ServiceId));

   if (NULL != pServiceCtxt)
   {
      spi_tclMLVncCdbServiceMngr* poManager =
         static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      VNCCDBError enCdbError = poManager->enStartStopService(u16ServiceId, false, false);

      if (VNCCDBErrorNone != enCdbError)
      {
         ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::enResetCallback: VNCCDBError for Reset request = %x \n",
               ETG_ENUM(CDB_ERROR, enCdbError)));
      }
   }

   return VNCCDBErrorNone;
   //@note: Return value reserved for future expansion, always return VNCCDBErrorNone.
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enGetCallback(VNC...
***************************************************************************/
VNCCDBError VNCCALL spi_tclMLVncCdbServiceMngr::enGetCallback(
   VNCCDBEndpoint* prEndpoint,
   VNCCDBServiceId u16ServiceId,
   t_Void* pServiceCtxt,
   VNCSBPCommandType enCmdType,
   VNCSBPUID u32ObjUID,
   VNCSBPProtocolError* penErrCode)
{
   SPI_INTENTIONALLY_UNUSED(prEndpoint);

   VNCCDBError enCdbError = VNCCDBErrorLocalSBPError;

   if ((NULL != pServiceCtxt) && (NULL != penErrCode))
   {
      spi_tclMLVncCdbServiceMngr* poManager =
         static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      *penErrCode = poManager->enGet(u16ServiceId, u32ObjUID, enCmdType);

      //@note: Special case for VNCCDBErrorNone: since "get" requires the response 
      //to be sent at a higher level (since it also includes serialization), 
      //an OK error code return indicates it's already been sent and the SDK 
      //should do nothing more.

      if (VNCSBPProtocolErrorNone == *penErrCode)
      {
         enCdbError = VNCCDBErrorNone;
      }
      else
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbServiceMngr::enGetCallback: VNCSBPProtocolError for Get request = %x \n",
               ETG_ENUM(SBP_ERROR, *penErrCode)));
      }
   } //if ((NULL != pServiceCtxt) && (NULL != penErrCode))

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::enGetCallback"
         "(for u16ServiceId = 0x%x, enCmdType = %x, u32ObjUID = 0x%x) left with VNCCDBError = %x \n",
         u16ServiceId,
         ETG_ENUM(SBP_CMDTYPE, enCmdType),
         u32ObjUID,
         ETG_ENUM(CDB_ERROR, enCdbError)));

   return enCdbError;
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enSetCallback(VNC...
***************************************************************************/
VNCCDBError VNCCALL spi_tclMLVncCdbServiceMngr::enSetCallback(
   VNCCDBEndpoint* prEndpoint,
   VNCCDBServiceId u16ServiceId,
   t_Void* pServiceCtxt,
   VNCSBPUID u32ObjUID,
   const t_U8* copu8Payload,
   size_t PayloadSize,
   VNCSBPProtocolError* penErrCode)
{
   SPI_INTENTIONALLY_UNUSED(prEndpoint);

   VNCCDBError enCdbError = VNCCDBErrorLocalSBPError;

   if ((NULL != pServiceCtxt) && (NULL != penErrCode) && (NULL != copu8Payload))
   {
      spi_tclMLVncCdbServiceMngr* poManager =
         static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      *penErrCode = poManager->enSet(u16ServiceId, u32ObjUID, copu8Payload, PayloadSize);

      if (VNCSBPProtocolErrorNone == *penErrCode)
      {
         enCdbError = VNCCDBErrorNone;
      }
      else
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbServiceMngr::enSetCallback: VNCSBPProtocolError for Set request = %x \n",
               ETG_ENUM(SBP_ERROR, *penErrCode)));
      }
   }//if ((NULL != pServiceCtxt)&&...)

   //! Send Set response if enSet() has returned with no error.
   //! (since if there is an error, penErrCode is already set to required value.)
   //! Not required to send a response in error scenario.
   if ((VNCCDBErrorNone == enCdbError) && (NULL != pServiceCtxt) && (NULL != penErrCode))
   {
      spi_tclMLVncCdbServiceMngr* poManager = static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      poManager->vSendSetResponse(u16ServiceId, u32ObjUID, *penErrCode);
   }

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::enSetCallback"
         "(for u16ServiceId = 0x%x, u32ObjUID = 0x%x, PayloadSize = %u) left with VNCCDBError = %x \n",
         u16ServiceId,
         u32ObjUID,
         PayloadSize,
         ETG_ENUM(CDB_ERROR, enCdbError)));

   return enCdbError;
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enSubscribeCallback(...
***************************************************************************/
VNCCDBError VNCCALL spi_tclMLVncCdbServiceMngr::enSubscribeCallback(
   VNCCDBEndpoint* prEndpoint,
   VNCCDBServiceId u16ServiceId,
   t_Void* pServiceCtxt,
   VNCSBPUID u32ObjUID,
   VNCSBPSubscriptionType* penSubType,
   t_U32* u32IntervalMs,
   VNCSBPProtocolError* penErrCode)
{
   SPI_INTENTIONALLY_UNUSED(prEndpoint);

   VNCCDBError enCdbError = VNCCDBErrorLocalSBPError;

   if ((NULL != pServiceCtxt) && (NULL != penErrCode) && (NULL != penSubType))
   {
      ETG_TRACE_USR1(("spi_tclMLVncCdbServiceMngr::enSubscribeCallback: penSubType = %x \n",
            ETG_ENUM(SBP_SUBSCRIPTIONTYPE, *penSubType)));

      spi_tclMLVncCdbServiceMngr* poManager = static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      *penErrCode = poManager->enSubscribe(u16ServiceId, u32ObjUID, penSubType, u32IntervalMs);

      if (VNCSBPProtocolErrorNone == *penErrCode)
      {
         enCdbError = VNCCDBErrorNone;
      }
      else
      {
         ETG_TRACE_ERR(("spi_tclMLVncCdbServiceMngr::enSubscribeCallback: "
               "VNCSBPProtocolError for Subscribe request = %x \n",
               ETG_ENUM(SBP_ERROR, *penErrCode)));
      }
   }

   //! Send subscribe response if enSubscribe() has returned with no error.
   //! (since if there is an error, penErrCode is already set to required value.)
   //! Not required to send a response in error scenario.
   if ((VNCCDBErrorNone == enCdbError) && (NULL != pServiceCtxt) && (NULL != penErrCode))
   {
      spi_tclMLVncCdbServiceMngr* poManager = static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      poManager->vSendSubscribeResponse(u16ServiceId, u32ObjUID, *penErrCode);
   }

   ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::enSubscribeCallback"
         "(for u16ServiceId = 0x%x, u32ObjUID = 0x%x) left with VNCCDBError = %x \n",
         u16ServiceId,
         u32ObjUID,
         ETG_ENUM(CDB_ERROR, enCdbError)));

   return enCdbError;
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enCancelCallback(...
***************************************************************************/
VNCCDBError VNCCALL spi_tclMLVncCdbServiceMngr::enCancelCallback(
   VNCCDBEndpoint* prEndpoint,
   VNCCDBServiceId u16ServiceId,
   t_Void* pServiceCtxt,
   VNCSBPCommandType enCmdType,
   VNCSBPUID u32ObjUID)
{
   SPI_INTENTIONALLY_UNUSED(prEndpoint);

   ETG_TRACE_USR1(("spi_tclMLVncCdbServiceMngr::enCancelCallback() entered: "
         "u16ServiceId = 0x%x, enCmdType = %x , u32ObjUID = 0x%x \n",
         u16ServiceId, ETG_ENUM(SBP_CMDTYPE, enCmdType), u32ObjUID));

   //@Note: Applications will typically only use this to cancel any on-change subscriptions which
   //are currently in effect. The SDK will automatically cancel any interval-based
   //subscriptions and perform any other cleanup associated with an outstanding request.

   if ((NULL != pServiceCtxt) && (VNCSBPCommandTypeSubscribe == enCmdType))
   {
      spi_tclMLVncCdbServiceMngr* poManager =
            static_cast<spi_tclMLVncCdbServiceMngr*>(pServiceCtxt);
      VNCSBPProtocolError enSbpError =
            poManager->enCancelSubscribe(u16ServiceId, u32ObjUID);

      if (VNCSBPProtocolErrorNone != enSbpError)
      {
         ETG_TRACE_USR2(("spi_tclMLVncCdbServiceMngr::enSubscribeCallback: "
               "VNCSBPProtocolError for CancelSubscribe request = %x \n",
               ETG_ENUM(SBP_ERROR, enSbpError)));
      }
   }//if ((NULL != pServiceCtxt) && (VNCSBPCommandTypeSubscribe == enCmdType))
	
   return VNCCDBErrorNone; 
   //@note: Return value reserved for future expansion, always return VNCCDBErrorNone.
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbServiceMngr::enResponseErrorCallback(...
***************************************************************************/
VNCCDBError VNCCALL spi_tclMLVncCdbServiceMngr::enResponseErrorCallback(
   VNCCDBEndpoint* prEndpoint,
   VNCCDBServiceId u16ServiceId,
   t_Void* pServiceCtxt,
   t_Void* pCommandContext,
   VNCSBPCommandType enCmdType,
   VNCSBPUID u32ObjUID,
   VNCCDBError enCDBSdkError,
   VNCSBPProtocolError enSBPErrorCode)
{
   SPI_INTENTIONALLY_UNUSED(prEndpoint);
   SPI_INTENTIONALLY_UNUSED(pServiceCtxt);
   SPI_INTENTIONALLY_UNUSED(pCommandContext);

   ETG_TRACE_USR1(("spi_tclMLVncCdbServiceMngr::enResponseErrorCallback() entered: "
          "u16ServiceId = 0x%x, enCmdType = %x , u32ObjUID = 0x%x, "
          "enCDBSdkError = %x, enSBPErrorCode = %x \n",
          u16ServiceId, ETG_ENUM(SBP_CMDTYPE, enCmdType), u32ObjUID,
          ETG_ENUM(CDB_ERROR, enCDBSdkError), ETG_ENUM(SBP_ERROR, enSBPErrorCode)));

   return VNCCDBErrorNone;
   //@note: Return value reserved for future expansion, always return VNCCDBErrorNone.
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
