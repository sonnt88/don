/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespCDB.cpp
 * \brief             RealVNC Wrapper Response class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:   VNC Response class for receiving response receiving response
 for spi_tclMLVncRespCDB calls to implement the client side Common Data Bus (CDB).
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 04.09.2013 |  A VIJETH                    | Initial Version
 01.04.2014 |  Ramya Murthy                | Redefined interface

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
//#define VNC_USE_STDINT_H   //Commented to fix Lint Warnings

#include <stdio.h>

#include "spi_tclMLVncRespCDB.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncRespCDB.cpp.trc.h"
   #endif
#endif

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  virtual spi_tclMLVncRespCDB::spi_tclMLVncRespCDB()
 ***************************************************************************/
spi_tclMLVncRespCDB::spi_tclMLVncRespCDB(): RespBase(e16CDB_REGID)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncRespCDB::spi_tclMLVncRespCDB();
 ***************************************************************************/
spi_tclMLVncRespCDB::~spi_tclMLVncRespCDB()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclMLVncRespCDB::vPostSetLocDataSubscription(...)
 ***************************************************************************/
t_Void spi_tclMLVncRespCDB::vPostSetLocDataSubscription(t_Bool bStartNotification)
{
SPI_INTENTIONALLY_UNUSED(bStartNotification);
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
