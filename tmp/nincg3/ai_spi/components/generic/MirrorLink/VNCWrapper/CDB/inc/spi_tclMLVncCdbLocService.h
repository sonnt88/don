
/***********************************************************************/
/*!
* \file  spi_tclMLVncCdbLocService.h
* \brief CDB Location service class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    CDB Location service class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version
21.05.2015  | Ramya Murthy          | Working implementation (for PSA)

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVNCCDBLOCNSERVICE_H_
#define _SPI_TCLVNCCDBLOCNSERVICE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H
#include "vnccdbsdk.h"
#include "vnccdbtypes.h"
#include "vncsbptypes.h"
#include "vncsbpserialize.h"

#include "SPITypes.h"
#include "WrapperTypeDefines.h"
#include "spi_tclMLVncCdbService.h"
#include "spi_tclMLVncCdbGeoLocatnObject.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************/
/*!
* \class spi_tclMLVncCdbLocService
* \brief
*******************************************************************************/
class spi_tclMLVncCdbLocService : public spi_tclMLVncCdbService
{
public:

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbLocService::spi_tclMLVncCdbLocService(const ...
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbLocService(const VNCCDBSDK* coprCdbSdk,
   *               VNCCDBServiceAccessControl enAccessCntrl)
   * \brief   Constructor
   * \sa      ~spi_tclMLVncCdbService()
   ***************************************************************************/
  spi_tclMLVncCdbLocService(const VNCCDBSDK* coprCdbSdk,
		  VNCCDBServiceAccessControl enAccessCntrl);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbLocService::~spi_tclMLVncCdbLocService()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCdbLocService()
   * \brief   Destructor
   * \sa      spi_tclMLVncCdbLocService()
   ***************************************************************************/
  virtual ~spi_tclMLVncCdbLocService();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbLocService::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbLocService::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

protected:

   /*!
   * \brief   GeoLocation object
   *
   */
   spi_tclMLVncCdbGeoLocatnObject m_oGeoLocation;

private:

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbLocService::spi_tclMLVncCdbLocService(cons...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbLocService(const spi_tclMLVncCdbLocService& oService)
   * \brief   Copy Constructor, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbLocService(const spi_tclMLVncCdbLocService& oService);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbLocService& spi_tclMLVncCdbLocService::
   **                operator=(const devp...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbLocService& operator=(
   *              const spi_tclMLVncCdbLocService& oService)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbLocService& operator=(const spi_tclMLVncCdbLocService& oService);

};

#endif // _SPI_TCLVNCCDBLOCNSERVICE_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
