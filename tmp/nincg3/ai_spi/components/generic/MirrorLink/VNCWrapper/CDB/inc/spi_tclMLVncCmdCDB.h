
/***********************************************************************/
/*!
* \file  spi_tclMLVncCmdCDB.h
* \brief Interface to interact with VNC CDB SDK
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface to interact with VNC CDB SDK
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version
21.05.2015  | Ramya Murthy          | Removed vAddServiceToConfigList() since config is read from policy

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLMLVNCCDB_H_
#define _SPI_TCLMLVNCCDB_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H
#include "vncint.h"        //For type: vnc_intptr_t
#include "vnccdbsdk.h"     //For type: VNCCDBSDK
#include "vnccdbtypes.h"   //For type: VNCCDBEndpoint, VNCCDBEndpointStatus
#include "vnccall.h"       //For type: VNCCALL

#include "SPITypes.h"
#include "MsgContext.h"
#include "WrapperTypeDefines.h" //For Discoverer types

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/* Forward declaration */
class spi_tclMLVncCdbServiceMngr;

/******************************************************************************/
/*!
* \class spi_tclMLVncCmdCDB
* \brief Interface to interact with VNC CDB SDK
*
* It provides an interface to interact with VNC CDB SDK.
* It is responsible for creation & initialization of CDB SDK and creation of
* CDB (Source) Endpoint.
* It also creates the ServiceManager to manage the services of the created Endpoint.
*******************************************************************************/
class spi_tclMLVncCmdCDB
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCmdCDB::spi_tclMLVncCmdCDB()
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCmdCDB()
   * \brief   Default Constructor
   * \sa      ~spi_tclMLVncCmdCDB()
   ***************************************************************************/
   spi_tclMLVncCmdCDB();

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCmdCDB::~spi_tclMLVncCmdCDB()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCmdCDB()
   * \brief   Destructor
   * \sa      spi_tclMLVncCmdCDB()
   ***************************************************************************/
   ~spi_tclMLVncCmdCDB();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bInitialiseCDBSDK()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialiseCDBSDK()
   * \brief   Creates and initialises an instance of CDB SDK
   * \retval  t_Bool  :  True if the Discovery SDK is initialised, else False
   * \sa      vUninitialiseCDBSDK()
   ***************************************************************************/
   t_Bool bInitialiseCDBSDK();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vUninitialiseCDBSDK()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialiseCDBSDK()
   * \brief   Uninitialises and destroys an instance of CDB SDK
   * \retval  t_Void
   * \sa      bInitialiseCDBSDK()
   ***************************************************************************/
   t_Void vUninitialiseCDBSDK();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vLaunchCDBApp(t_U32 u32DeviceHandle,...
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchCDBApp(t_U32 u32DeviceHandle, const t_U32 cou32CDBAppId)
   * \brief   Posts a request for CDB URL to Discovery SDK, which in turn triggers
   *          initialisation of endpoint & its services and launches the CDB
   *          app at peer endpoint.
   * \param   u32DeviceHandle : [IN] Unique device handle of a device
   *             (currently not used, kept for future usage)
   * \param   cou32CDBAppId  : [IN] Application ID of CDB app at peer endpoint
   * \retval  t_Void
   * \sa      vTerminateCDBApp(), vPostEntityValueCallback
   ***************************************************************************/
   t_Void vLaunchCDBApp(t_U32 u32DeviceHandle, const t_U32 cou32CDBAppId);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vTerminateCDBApp(t_U32 u32DeviceHandle)
   ***************************************************************************/
   /*!
   * \fn      vTerminateCDBApp(t_U32 u32DeviceHandle)
   * \brief   Destroys source endpoint.
   * \param   u32DeviceHandle : [IN] Unique device handle of a device
   *             (currently not used, kept for future usage)
   * \retval  t_Void
   * \sa      vLaunchCDBApp()
   ***************************************************************************/
   t_Void vTerminateCDBApp(t_U32 u32DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vGetServicesList(std::vector<Cdb...
   ***************************************************************************/
   /*!
   * \fn      vGetServicesList(std::vector<trCdbServiceConfig>& rfSvcConfigList)
   * \brief   Interface to get the supported services list.
   * \param   rfSvcConfigList : [OUT] List of services with their activation status.
   * \retval  t_Void
   * \sa      vSetServicesList()
   ***************************************************************************/
   t_Void vGetServicesList(std::vector<trCdbServiceConfig>& rfSvcConfigList) const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vSetServicesList(const std::vect...
   ***************************************************************************/
   /*!
   * \fn      vSetServicesList(const std::vector<trCdbServiceConfig>& rfSvcConfigList)
   * \brief   Interface to set the supported services list.
   * \param   rfSvcConfigList : [OUT] List of services with their activation status.
   * \retval  t_Void
   * \sa      vGetServicesList()
   ***************************************************************************/
   t_Void vSetServicesList(const std::vector<trCdbServiceConfig>& rfSvcConfigList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vSetCDBUsage(t_U32 u32DeviceHandle...
   ***************************************************************************/
   /*!
   * \fn      vSetCDBUsage(t_U32 u32DeviceHandle, t_Bool bCdbEnabled)
   * \brief   Interface to enable/disable CDB service usage
   * \param   u32DeviceHandle : [IN] Unique device handle of a device
   *             (currently not used, kept for future usage)
   * \param   bCdbEnabled : [IN] CDB enable/disable status
   * \retval  t_Void
   * \sa      bGetCDBUsage()
   ***************************************************************************/
   t_Void vSetCDBUsage(t_U32 u32DeviceHandle, t_Bool bCdbEnabled);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bGetCDBUsage(t_U32 u32DeviceHandle)
   ***************************************************************************/
   /*!
   * \fn      bGetCDBUsage(t_U32 u32DeviceHandle)
   * \brief   Interface to get CDB service usage status
   * \param   u32DeviceHandle : [IN] Unique device handle of a device
   *             (currently not used, kept for future usage)
   * \retval  t_Bool: TRUE if CDB usage is enabled, else FALSE.
   * \sa      vSetCDBUsage()
   ***************************************************************************/
   t_Bool bGetCDBUsage(t_U32 u32DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bAddLicense()
   ***************************************************************************/
   /*!
   * \fn      bAddLicense()
   * \brief   Adds a license to CDB endpoint.
   * \retval  t_Bool
   * \sav     CreateCDBEndpoint()
   ***************************************************************************/
   t_Bool bAddVNCLicense();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::bAddDiscoveryListener()
   ***************************************************************************/
   /*!
   * \fn      t_Void bAddDiscoveryListener()
   * \brief   Adds a Discovery listener
   * \retval  t_Void
   * \sa      bRemoveDiscoveryListener()
   ***************************************************************************/
   t_Bool bAddDiscoveryListener();

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclMLVncCmdCDB::bRemoveDiscoveryListener()
   ***************************************************************************/
   /*!
   * \fn      t_Void bRemoveDiscoveryListener()
   * \brief   Removes a Discovery listener
   * \retval  t_Void
   * \sa      bAddDiscoveryListener()
   ***************************************************************************/
   t_Bool bRemoveDiscoveryListener();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vFetchCDBServerUrl(t_String szCDB...
   ***************************************************************************/
   /*!
   * \fn      vFetchCDBServerUrl(t_String szCDBAppID, VNCDiscoverySDKEntity* prEntity)
   * \brief   Retrieves the CDB service URL from Discovery SDK.
   * \param   szCDBAppID : [IN] CDB app ID in string
   * \param   prDiscEntity : [IN] Pointer to discovery entity
   * \retval  t_Void
   * \sa      vPostEntityValueCallback()
   ***************************************************************************/
   t_Void vFetchCDBServerUrl(t_String szCDBAppID, VNCDiscoverySDKEntity* prDiscEntity);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vInitialiseCDBEndpoint(const t_Char...
   ***************************************************************************/
   /*!
   * \fn      vInitialiseCDBEndpoint(const t_Char* pococzCDBUrl);
   * \brief   Creates and initialises an instance of CDB Endpoint.
   * \param   pococzCDBUrl: [IN] URL of CDB application in ML server
   * \retval  t_Void
   * \sa      vUninitialiseCDBEndpoint()
   ***************************************************************************/
   t_Void vInitialiseCDBEndpoint(const t_Char* pococzCDBUrl);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vUninitialiseCDBEndpoint(const t_Char...
   ***************************************************************************/
   /*!
   * \fn      vInitialiseCDBEndpoint(const t_Char* pococzCDBUrl);
   * \brief   Uninitialises and destroys an instance of CDB Endpoint.
   * \retval  t_Void
   * \sa      vInitialiseCDBEndpoint()
   ***************************************************************************/
   t_Void vUninitialiseCDBEndpoint();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bCreateCDBEndpoint(const t_Char...
   ***************************************************************************/
   /*!
   * \fn      bCreateCDBEndpoint(const t_Char* pococzCDBUrl)
   * \brief   Creates a CDB Endpoint.
   * \param   pococzCDBUrl : [IO] Pointer to CDB app URL
   * \retval  t_Bool  : True if the Endpoint is created properly.
   * \sa      vDestroyCDBEndpoint()
   ***************************************************************************/
   t_Bool bCreateCDBEndpoint(const t_Char* pococzCDBUrl);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vDestroyCDBEndpoint(VNCCDBEndpoint...
   ***************************************************************************/
   /*!
   * \fn      vDestroyCDBEndpoint()
   * \brief   Destroys a CDB Endpoint.
   * \retval  t_Void
   * \sa      bCreateCDBEndpoint()
   ***************************************************************************/
   t_Void vDestroyCDBEndpoint();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bInitCDBEndPointServices()
   ***************************************************************************/
   /*!
   * \fn      bInitCDBEndPointServices()
   * \brief   Adds all services with ActuivationStatus == TRUE in ServiceConfigList
   * \retval  t_Bool: TRUE if all the services are added successfully, else FALSE.
   * \sa      bUninitEndPointServices()
   ***************************************************************************/
   t_Bool bInitCDBEndPointServices();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bUninitCDBEndPointServices()
   ***************************************************************************/
   /*!
   * \fn      bUninitCDBEndPointServices()
   * \brief   Removes all services with ActuivationStatus == TRUE in ServiceConfigList
   * \retval  t_Bool: TRUE if all the services are removed successfully, else FALSE.
   * \sa      bStartCDBEndpoint()
   ***************************************************************************/
   t_Bool bUninitCDBEndPointServices();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bStartCDBEndpoint()
   ***************************************************************************/
   /*!
   * \fn      t_Void bStartCDBEndpoint()
   * \brief   Starts a CDB Endpoint
   * \retval  t_Bool: TRUE if Endpoint is started successfully, else FALSE.
   * \sa      bStopCDBEndpoint()
   ***************************************************************************/
   t_Bool bStartCDBEndpoint();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdCDB::bStopCDBEndpoint()
   ***************************************************************************/
   /*!
   * \fn      bStopCDBEndpoint()
   * \brief   Stops a CDB Endpoint
   * \retval  t_Bool: TRUE if Endpoint is stopped successfully, else FALSE.
   * \sa      bStartCDBEndpoint()
   ***************************************************************************/
   t_Bool bStopCDBEndpoint();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vPostEntityValueCallback(t_Void*...
   ***************************************************************************/
   /*!
   * \fn      vPostEntityValueCallback(t_Void* pSDKContext,
   *             VNCDiscoverySDKRequestId u32RequestId,
   *             VNCDiscoverySDKDiscoverer* pDiscoverer,
   *             VNCDiscoverySDKEntity* pEntity,
   *             t_Char* pczKey, t_Char* pczValue, VNCDiscoverySDKError u32Error);
   * \brief   Static function, whose address is passed in DiscoverySDKCallbacks struct.
   *          Discovery SDK callback, used to post a value to disc SDK.
   * \param   pSDKContext  : [IN] Pointer to SDK context
   * \param   u32RequestId : [IN] Unique request ID
   * \param   pDiscoverer  : [IN] Pointer to ML discoverer
   * \param   pEntity  : [IN] Pointer to disc entity
   * \param   pczKey   : [IN] Pointer to map Key at disc entity
   * \param   u32Error : [IN] SDK error code
   * \retval  t_Void
   * \sa      vLaunchCDBApp()
   ***************************************************************************/
   static t_Void VNCCALL vPostEntityValueCallback(
    t_Void* pSDKContext,
    VNCDiscoverySDKRequestId u32RequestId,
    VNCDiscoverySDKDiscoverer* pDiscoverer,
    VNCDiscoverySDKEntity* pEntity,
    t_Char* pczKey,
    t_Char* pczValue,
    VNCDiscoverySDKError u32Error);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdCDB::vEndpointStatusCallback(t_Void*...
   ***************************************************************************/
   /*!
   * \fn      vEndpointStatusCallback(VNCCDBEndpoint* pEndpoint,
   *             t_Void* pEndpointContext, VNCCDBEndpointStatus enStatus,
   *             vnc_intptr_t AdditionalInfo);
   * \brief   Static function, whose address is passed in vEndpointStatusCallback struct.
   *          CDB Endpoint callback, which reports status of Endpoint.
   * \param   pEndpoint  : [IN] Pointer to CDB Endpoint
   * \param   pEndpointContext : [IN] The endpoint context, as supplied in
   *             VNCCDBEndpointDescriptor in the call to VNCCDBEndpointCreate().
   * \param   enStatus  : [IN] The endpoint's current status.
   * \param   AdditionalInfo  : [IN] For certain values of status, this parameter
   *             may provide additional information.
   * \retval  t_Void
   * \sa      vLaunchCDBApp()
   ***************************************************************************/
   static t_Void VNCCALL vEndpointStatusCallback(VNCCDBEndpoint* pEndpoint,
       t_Void* pEndpointContext,
       VNCCDBEndpointStatus enStatus,
       vnc_intptr_t AdditionalInfo);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdCDB::vThreadStartedCallback(VNCCDBEndpoint*...
   ***************************************************************************/
   /*!
   * \fn      vThreadStartedCallback(VNCCDBEndpoint* pEndpoint, t_Void* pEndpointContext)
   * \brief   Static function, whose address is passed in vEndpointStatusCallback struct.
   *          CDB Endpoint callback, notifies the application that a CDB endpoint
   *          started a new thread.
   * \param   pEndpoint  : [IN] Pointer to CDB Endpoint
   * \param   pEndpointContext : [IN] The endpoint context, as supplied in
   *             VNCCDBEndpointDescriptor in the call to VNCCDBEndpointCreate().
   * \retval  t_Void
   * \sa      vThreadStoppedCallback()
   ***************************************************************************/
   static t_Void VNCCALL vThreadStartedCallback(VNCCDBEndpoint* pEndpoint,
         t_Void* pEndpointContext);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdCDB::vThreadStoppedCallback(VNCCDBEndpoint*...
   ***************************************************************************/
   /*!
   * \fn      vThreadStoppedCallback(VNCCDBEndpoint* pEndpoint, t_Void* pEndpointContext)
   * \brief   Static function, whose address is passed in vEndpointStatusCallback struct.
   *          CDB Endpoint callback, notifies the application that a CDB endpoint
   *          thread is about to terminate.
   * \param   pEndpoint  : [IN] Pointer to CDB Endpoint
   * \param   pEndpointContext : [IN] The endpoint context, as supplied in
   *             VNCCDBEndpointDescriptor in the call to VNCCDBEndpointCreate().
   * \param   enCDBErrorCode  : [IN] Error due to which thread stopped
   * \retval  t_Void
   * \sa      vThreadStartedCallback()
   ***************************************************************************/
   static t_Void VNCCALL vThreadStoppedCallback(VNCCDBEndpoint* pEndpoint,
         t_Void* pEndpointContext,
         VNCCDBError enCDBErrorCode);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdCDB::vLogCallback(VNCCDBEndpoint*...
   ***************************************************************************/
   /*!
   * \fn      vLogCallback(VNCCDBEndpoint* pEndpoint, t_Void* pEndpointContext)
   * \brief   Static function, whose address is passed in vEndpointStatusCallback struct.
   *          CDB Endpoint callback, informs the application of a log message
   *          emitted by a CDB endpoint.
   * \param   pEndpoint: [IN] Pointer to CDB Endpoint
   * \param   pEndpointContext : [IN] The endpoint context, as supplied in
   *             VNCCDBEndpointDescriptor in the call to VNCCDBEndpointCreate().
   * \param   pcczCategory : [IN] Log message category, as a NUL-terminated UTF-8 string.
   * \param   s32Severity  : [IN] Log message severity.
*                Severities range from 0 (error) to 100 (debug).
   * \param   pcczText : [IN] Log message text, as a NUL-terminated UTF-8 string.
   * \retval  t_Void
   * \sa
   ***************************************************************************/
   static t_Void VNCCALL vLogCallback(VNCCDBEndpoint* pEndpoint,
         t_Void* pEndpointContext,
         const t_Char* pcczCategory,
         vnc_int32_t s32Severity,
         const t_Char* pcczText);

   /*!
   * \brief   Pointer to VNCCDB SDK
   *
   * This will be populated during the VNC CDB SDK initialization and
   * destroyed during Uninitialization. Required memory needs to be
   * allocated before passing it as a parameter to SDK Initialization
   */
   VNCCDBSDK* m_prCDBSdk;

   /*!
   * \brief   Pointer to CDB Endpoint
   *
   * This will be created when Vnc session is established with an entity
   * i.e, on SelectDevice.
   * @Note: Only one endpoint is created since currently only
   * one device session is supported at a time. If this is extended to
   * more devices, same number of endpoints will have to be created.
   */
   VNCCDBEndpoint* m_prCDBEndPt;

   /*!
   * \brief   Pointer to VNCDiscovery SDK
   *
   * This will be retrieved from Discovery data interface.
   */
   VNCDiscoverySDK* m_prDiscSDK;

   /*!
   * \brief   Pointer to Discoverer in VNCDiscoverySDK
   *
   * This will be retrieved from Discovery data interface.
   * @Note: If more discoverers are created, separate pointers
   * will have to be created for each discoverer.
   */
   VNCDiscoverySDKDiscoverer* m_prMLDisc;

   /*!
   * \brief   Pointer to VNCDiscovery SDK
   *
   * This will be initialised during CDB SDK initialisation.
   * @Note: One service manager corresponds to one Endpoint. i.e,
   * this manager currently manages the services of only the
   * selected entity/device's source endpoint. If more endpoints
   * are required in future (one for each entity), the same number
   * of service managers would have to be created.
   */
   spi_tclMLVncCdbServiceMngr* m_poServiceMngr;

   /*!
   * \brief Discovery listener ID
   *
   * Member variable to store the listner ID obtained during
   *  the callback registration with discovererSDK.
   */
   t_U32 m_u32ListenerID;

   /*!
   * \brief Services Config List
   *
   * Container to store the list of our supported services
   * along with current activation status of each service.
   */
   std::vector<trCdbServiceConfig> m_SvcConfigList;

   /*!
   * \brief CDB Enabled flag
   *
   * Stores the status of CDB services.
   * If TRUE - CDB services are allowed.
   */
   t_Bool m_bIsCdbEnabled;

   /*!
   * \brief CDB SDK initialisation status flag
   *
   * Stores the status of CDB SDK initialisation.
   * If TRUE - CDB SDK is created & initialised..
   */
   t_Bool m_bIsCdbSdkInitialised;

   /*!
   * \brief CDB Endpoint started flag
   *
   * Stores the running status of CDB Endpoint.
   * If TRUE - CDB Endpoint is started.
   */
   t_Bool m_bIsEndpointStarted;

   /*!
   * \brief   Message context utility
   */
   static MsgContext m_oMsgContext;

};

#endif // _SPI_TCLMLVNCCDB_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
