/***********************************************************************/
/*!
* \file  spi_tclMLVncCdbGPSService.h
* \brief GPS CDB service class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    GPS CDB service class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version
24.03.2013  | Ramya Murthy          | Moved out GPS NMEA Object and NMEA
                                      Description Object classes

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVNCCDBGPSSERVICE_H_
#define _SPI_TCLVNCCDBGPSSERVICE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H
#include "vnccdbsdk.h"        //For type: VNCCDBSDK
#include "vnccdbtypes.h"      //For type: VNCCDBServiceAccessControl
#include "vncsbptypes.h"
#include "vncsbpserialize.h"

#include "BaseTypes.h"
#include "spi_tclMLVncCdbService.h"
#include "spi_tclMLVncCdbGPSNmeaObject.h"
#include "spi_tclMLVncCdbGPSNmeaDescObject.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/* Forward declarations */
//class spi_tclMLVncCdbGPSNmeaObject;
//class spi_tclMLVncCdbGPSNmeaDescObject;

/******************************************************************************/
/*!
* \class spi_tclMLVncCdbGPSService
* \brief
*******************************************************************************/
class spi_tclMLVncCdbGPSService : public spi_tclMLVncCdbService
{
public:

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSService::spi_tclMLVncCdbGPSService(const ...
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGPSService(const VNCCDBSDK* coprCdbSdk,
   *               VNCCDBServiceAccessControl enAccessCntrl)
   * \brief   Constructor
   * \sa      ~spi_tclMLVncCdbService()
   ***************************************************************************/
   spi_tclMLVncCdbGPSService(const VNCCDBSDK* coprCdbSdk,
      VNCCDBServiceAccessControl enAccessCntrl);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSService::~spi_tclMLVncCdbGPSService()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCdbGPSService()
   * \brief   Destructor
   * \sa      spi_tclMLVncCdbGPSService()
   ***************************************************************************/
   virtual ~spi_tclMLVncCdbGPSService();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbGPSService::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbGPSService::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

protected:

   spi_tclMLVncCdbGPSNmeaObject      m_oNmeaObject;

   spi_tclMLVncCdbGPSNmeaDescObject  m_oNmeaDescriptionObject;

private:

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSService::spi_tclMLVncCdbGPSService(cons...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGPSService(const spi_tclMLVncCdbGPSService& oService)
   * \brief   Copy Constructor, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbGPSService(const spi_tclMLVncCdbGPSService& oService);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGPSService& spi_tclMLVncCdbGPSService::
   **                operator=(const devp...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGPSService& operator=(
   *              const spi_tclMLVncCdbGPSService& oService)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbGPSService& operator=(const spi_tclMLVncCdbGPSService& oService);

};

#endif // _SPI_TCLVNCCDBGPSSERVICE_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
