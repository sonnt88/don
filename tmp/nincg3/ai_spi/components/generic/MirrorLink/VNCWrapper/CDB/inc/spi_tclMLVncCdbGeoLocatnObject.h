
/***********************************************************************/
/*!
* \file  spi_tclMLVncCdbGeoLocatnObject.h
* \brief CDB Location service class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    CDB Location service class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVNCCDBGEOLOCOBJECT_H_
#define _SPI_TCLVNCCDBGEOLOCOBJECT_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H
#include "vnccdbsdk.h"
#include "vnccdbtypes.h"
#include "vncsbptypes.h"
#include "vncsbpserialize.h"

#include "SPITypes.h"
#include "WrapperTypeDefines.h"
#include "spi_tclMLVncCdbObject.h"
#include "Lock.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
 * * \brief forward declaration
 */

/******************************************************************************/
/*!
* \class spi_tclMLVncCdbGeoLocatnObject
* \brief 
*******************************************************************************/
class spi_tclMLVncCdbGeoLocatnObject : public spi_tclMLVncCdbObject
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/
   
   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGeoLocatnObject::spi_tclMLVncCdbGeoLocatnObject(...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGeoLocatnObject(const VNCCDBSDK* coprCdbSdk)
   * \brief   Constructor
   * \sa      ~spi_tclMLVncCdbGeoLocatnObject()
   ***************************************************************************/
   spi_tclMLVncCdbGeoLocatnObject(const VNCCDBSDK* coprCdbSdk);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGeoLocatnObject::~spi_tclMLVncCdbGeoLocatnObject()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCdbGeoLocatnObject()
   * \brief   Constructor
   * \sa      spi_tclMLVncCdbGeoLocatnObject()
   ***************************************************************************/
   virtual ~spi_tclMLVncCdbGeoLocatnObject() { }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbGeoLocatnObject::vOnData(...)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbGeoLocatnObject::vOnData(...)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

   /***********Start of methods overridden from spi_tclMLVncCdbObject**********/

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbGeoLocatnObject::enGet(...)
   ***************************************************************************/
   /*!
   * \fn       enGet(VNCSBPSerialize* prSerialize)
   * \brief    Get serialized data
   * \param    prSerialize  : [IN] Pointer to serializer
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   * \sa
   ***************************************************************************/
   virtual VNCSBPProtocolError enGet(VNCSBPSerialize* prSerialize);

   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbGeoLocatnObject::enSubscribe(...)
   ***************************************************************************/
   /*!
   * \fn        enSubscribe(VNCSBPSubscriptionType* penSubType, t_U32* pu32IntervalMs
   * \brief     Sets subscription of object
   * \param     penSubType  : [OUT] Pointer to subscription type
   * \param     pu32IntervalMs : [OUT] Pointer to Interval in milliseconds
   * \retval    VNCSBPProtocolError:SBP protocol error code
   * \sa
   ***************************************************************************/
   virtual VNCSBPProtocolError enSubscribe(VNCSBPSubscriptionType* penSubType,
     t_U32* pu32IntervalMs);

   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbGeoLocatnObject::enCancelSubscribe()
   ***************************************************************************/
   /*!
   * \fn      enCancelSubscribe()
   * \brief   Cancels subscription of object
   * \retval  t_Void
   * \sa      enSubscribe()
   ***************************************************************************/
   virtual VNCSBPProtocolError enCancelSubscribe();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCdbLocatnCoord::bIsLocDataAvailable()
   ***************************************************************************/
   /*!
   * \fn      bIsLocDataAvailable()
   * \brief   Provides availability of Location data
   * \retval  t_Bool
   * \sa
   ***************************************************************************/
   t_Bool bIsLocDataAvailable();

protected:
   /***************************************************************************
   ********************************PROTECTED***********************************
   ***************************************************************************/

private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGeoLocatnObject::spi_tclMLVncCdbGeoLocatnObject(...)
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGeoLocatnObject(
   *              const spi_tclMLVncCdbGeoLocatnObject& oObject)
   * \brief   Copy Constructor, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbGeoLocatnObject(const spi_tclMLVncCdbGeoLocatnObject& oObject);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbGeoLocatnObject& spi_tclMLVncCdbGeoLocatnObject
   **                ::operator=(...)
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbGeoLocatnObject& operator=(
   *              const spi_tclMLVncCdbGeoLocatnObject& oObject)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbGeoLocatnObject& operator=(const spi_tclMLVncCdbGeoLocatnObject& oObject);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCdbLocatnCoord::bSerializeCoordStructure(VNCSBPSerialize...
   ***************************************************************************/
   /*!
   * \fn      bSerializeCoordStructure(VNCSBPSerialize* prSerialize, const VNCCDBSDK* coprCdbSdk)
   * \brief   Serialises the data
   * \retval  t_Bool
   * \sa
   ***************************************************************************/
   t_Bool bSerializeCoordStructure(VNCSBPSerialize* prSerialize);

   /*!
   * \brief   GPS service reference
   *
   */
   //spi_tclMLVncCdbLocService& m_rfoLocService;

   /*!
   * \brief   Structure containing GPS data
   */
   trGPSData  m_rGpsData;

   /*!
   * \brief   Indicates GPS signal quality
   */
   tenGnssQuality m_enGnssQuality;

   /*!
   * \brief   Lock variable
   */
   Lock  m_oLock;
};

#endif // _SPI_TCLVNCCDBGEOLOCOBJECT_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
