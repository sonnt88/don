/***********************************************************************/
/*!
* \file  spi_tclMLVncCdbService.h
* \brief Base class for a CDB service
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for a CDB service
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVNCCDBSERVICE_H_
#define _SPI_TCLVNCCDBSERVICE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <map>

#define VNC_USE_STDINT_H
#include "vnccdbsdk.h"        //For type: VNCCDBSDK
#include "vnccdbtypes.h"      //For type: VNCCDBServiceConfiguration, VNCCDBServiceAccessControl, VNCCDBEndpoint
#include "vncsbp.h"           //For type: VNCSBPSourceCallbacks
#include "vncsbptypes.h"      //For type: VNCSBPSubscriptionType, VNCSBPProtocolError
#include "vncsbpserialize.h"  //For type: VNCSBPUID, VNCSBPSerialize

#include "SPITypes.h"
#include "spi_tclMLVncCdbObject.h"
#include "Lock.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/*!
 * * \brief forward declaration
 */
class spi_tclMLVncCdbServiceMngr;

/******************************************************************************/
/*!
* \class spi_tclMLVncCdbService
* \brief CDB Service Base class, handles basic functionality of an SBP service.
*******************************************************************************/
class spi_tclMLVncCdbService
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbService::spi_tclMLVncCdbService(const VNCCDBSDK...
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbService(const VNCCDBSDK* coprCdbSdk,
   *               const t_Char* copczServiceName,
   *               t_U8 u8VersionMajor, 
   *               t_U8 u8VersionMinor,
   *               VNCCDBServiceConfiguration enServiceConfig,
   *               VNCCDBServiceAccessControl enAccessCntrl,
   *               tenCDBServiceType enServiceType)
   * \brief   Constructor
   * \sa      ~spi_tclMLVncCdbService()
   ***************************************************************************/
   spi_tclMLVncCdbService(const VNCCDBSDK* coprCdbSdk,
      const t_Char* copczServiceName,
      t_U8 u8VersionMajor, 
      t_U8 u8VersionMinor,
      VNCCDBServiceConfiguration enServiceConfig,
      VNCCDBServiceAccessControl enAccessCntrl,
	   tenCDBServiceType enServiceType);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbService::~spi_tclMLVncCdbService()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclMLVncCdbService()
   * \brief   Destructor
   * \sa      spi_tclMLVncCdbService()
   ***************************************************************************/
   virtual ~spi_tclMLVncCdbService();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCdbService::bIsServiceActive()
   ***************************************************************************/
   /*!
   * \fn       bIsServiceActive()
   * \brief    Reads the running status of service
   * \retval   t_Bool: TRUE - If service is currently active (i.e, data is being sent
   *           to SDK).
   * \sa       vStartStopService()
   ***************************************************************************/
   t_Bool bIsServiceActive() const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbService::vStartStopService(t_Bool...
   ***************************************************************************/
   /*!
   * \fn       vStartStopService(t_Bool bStartService)
   * \brief    Starts or Stops the service.
   *           If Subscription is of OnChange type:
   *           a) When service is started, starts sending data SDK.
   *           b) When service is stopped, stop sending data to SDK.
   * \param    bStartService : [IN] Indicates whether service should be started/stopped.
   * \param    bResponseRequired : [IN] Indicates whether service should send
   *           back a response to SDK for this request.
   * \retval   t_Void
   * \sa       bIsServiceActive()
   ***************************************************************************/
   t_Void vStartStopService(t_Bool bStartService, t_Bool bResponseRequired);

   /***************************************************************************
   ** FUNCTION:  VNCCDBServiceId spi_tclMLVncCdbService::u32GetServiceId()
   ***************************************************************************/
   /*!
   * \fn       u32GetServiceId()
   * \brief    Gets the unique service ID
   * \retval   VNCCDBServiceId: SBP Service ID
   ***************************************************************************/
   VNCCDBServiceId u32GetServiceId() const;

   /***************************************************************************
   ** FUNCTION:  tenCDBServiceType spi_tclMLVncCdbService::enGetServiceType()
   ***************************************************************************/
   /*!
   * \fn       enGetServiceType()
   * \brief    Gets the service type
   * \retval   tenCDBServiceType: Service type
   ***************************************************************************/
   tenCDBServiceType enGetServiceType() const;

   /***************************************************************************
   ** FUNCTION:  VNCCDBError spi_tclMLVncCdbService::enAddToEndpoint(VNCCDBEndpoint...
   ***************************************************************************/
   /*!
   * \fn       enGetServiceType(VNCCDBEndpoint* prCdbEndpoint,
   *              const VNCSBPSourceCallbacks* coprSbpSrcCallbacks,
   *              size_t CallbacksSize,
   *              spi_tclMLVncCdbServiceMngr* poSvcManager)
   * \brief    Interface to add the service to Endpoint
   * \param    prCdbEndpoint  : [IN] Endpoint pointer
   * \param    coprSbpSrcCallbacks : [IN] Structure of SBP source callback pointers 
   * \param    CallbacksSize  : [IN] Size of callbacks structure
   * \param    poSvcManager   : [IN] Service Manager pointer (to use as context)
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   ***************************************************************************/
   VNCCDBError enAddToEndpoint(VNCCDBEndpoint* prCdbEndpoint,
      const VNCSBPSourceCallbacks* coprSbpSrcCallbacks,
      size_t CallbacksSize,
      spi_tclMLVncCdbServiceMngr* poSvcManager);
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enSet(VNCSBPUID...
   ***************************************************************************/
   /*!
   * \fn       enSet(VNCSBPUID u32ObjUID, const t_U8* copu8Payload,t_U32 u32PayloadSize)
   * \brief    Set Request handler for Set Callback invoked by CDB SDK.
   * \param    u32ObjUID  : [IN] Unique object ID
   * \param    copu8Payload : [IN] Pointer to payload data
   * \param    u32PayloadSize : [IN] Size of payload data
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   ***************************************************************************/
   VNCSBPProtocolError enSet(VNCSBPUID u32ObjUID, const t_U8* copu8Payload,
      t_U32 u32PayloadSize); 
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enSubscribe(VNCSBPUID...
   ***************************************************************************/
   /*!
   * \fn       enSubscribe(VNCSBPUID u32ObjUID, VNCSBPSubscriptionType* penSubType,
   *              t_U32* pu32IntervalMs)
   * \brief    Subscribe Request handler for Subscribe Callback invoked by CDB SDK.
   * \param    u32ObjUID  : [IN] Unique object ID
   * \param    penSubType : [IN] Pointer to SBP subscription type enum
   * \param    pu32IntervalMs : [IN] Pointer to subscription interval (in milliseconds)
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   ***************************************************************************/
   VNCSBPProtocolError enSubscribe(VNCSBPUID u32ObjUID,
      VNCSBPSubscriptionType* penSubType,
      t_U32* pu32IntervalMs); 
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enCancelSubscribption(...
   ***************************************************************************/
   /*!
   * \fn       enCancelSubscribption(VNCSBPUID u32ObjUID)
   * \brief    CancelSubscribe Request handler for Cancel Callback invoked by CDB SDK.
   * \param    u32ObjUID  : [IN] Unique object ID
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   ***************************************************************************/
   VNCSBPProtocolError enCancelSubscribption(VNCSBPUID u32ObjUID);
   
   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enSendGetResponse(VNCSBPUID...
   ***************************************************************************/
   /*!
   * \fn       enSendGetResponse(VNCSBPUID u32ObjUID, VNCSBPCommandType enCmdType)
   * \brief    Subscribe Request handler for Subscribe Callback invoked by CDB SDK.
   * \param    u32ObjUID  : [IN] Unique object ID
   * \param    enCmdType : [IN] SBP command type enum
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   ***************************************************************************/
   VNCSBPProtocolError enSendGetResponse(VNCSBPUID u32ObjUID,
      VNCSBPCommandType enCmdType);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbService::vSendSubscribeResponse(VNCSBPUID...
   ***************************************************************************/
   /*!
   * \fn       vSendSubscribeResponse(VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError)
   * \brief    Handler to send Subscribe response to SDK.
   * \param    u32ObjUID  : [IN] Unique object ID
   * \param    enSbpError : [IN] SBP error to be sent in the reponse
   * \retval   None
   ***************************************************************************/
   t_Void vSendSubscribeResponse(VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbService::vSendSetResponse(VNCSBPUID...
   ***************************************************************************/
   /*!
   * \fn       vSendSetResponse(VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError)
   * \brief    Handler to send Set response to SDK.
   * \param    u32ObjUID  : [IN] Unique object ID
   * \param    enSbpError : [IN] SBP error to be sent in the reponse
   * \retval   None
   ***************************************************************************/
   t_Void vSendSetResponse(VNCSBPUID u32ObjUID, VNCSBPProtocolError enSbpError);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbService::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   *          Optional interface to be implemented.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData){};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbService::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData){};

protected:
   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbService::vAddObject(spi_tclMLVncCdbObject...
   ***************************************************************************/
   /*!
   * \fn       vAddObject(spi_tclMLVncCdbObject* poObject)
   * \brief    Gets the service type
   * \param    poObject : [IN] pointer to an Object of the service
   * \retval   t_Void
   * \sa
   ***************************************************************************/
   t_Void vAddObject(spi_tclMLVncCdbObject* poObject);

   /***************************************************************************
   ** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbService::enGet(VNCSBPUID...
   ***************************************************************************/
   /*!
   * \fn       enGet(VNCSBPUID u32ObjUID, VNCSBPSerialize* prSerialize)
   * \brief    Get Request handler for Get Callback invoked by CDB SDK.
   * \param    u32ObjUID  : [IN] Unique object ID
   * \param    prSerialize : [IN] Serialised data pointer
   * \retval   VNCSBPProtocolError: SBP Protocol error code
   ***************************************************************************/
   VNCSBPProtocolError enGet(VNCSBPUID u32ObjUID, VNCSBPSerialize* prSerialize);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbService::vSubscribeForLocationData(t_Bool...)
   ***************************************************************************/
   /*!
   * \fn       vSubscribeForLocationData(t_Bool bSubscribe)
   * \brief    Method to trigger subscription for location data notifications.
   * \param    bSubscribe: [IN]
    *               If TRUE - Subscribes for location data notifications.
    *               If FALSE - Unsubscribes for location data notifications.
   * \retval   t_Void
   ***************************************************************************/
   t_Void vSubscribeForLocationData(t_Bool bSubscribe) const;

   /**************************************************************************
   ** Data Members
   **************************************************************************/

   /*!
   * \brief   Pointer to CDB SDK
   *
   * This pointer is retrieved in constructor and will be
   * used throughout the service manager's lifetime.
   */
   const VNCCDBSDK* 	   m_coprCdbSdk;

private:
   /**************************************************************************
   *********************************PRIVATE***********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCdbService::vSendServiceResponse(VNCCDBServiceResponse...)
   ***************************************************************************/
   /*!
   * \fn       vSubscribeForLocationData(t_Bool bSubscribe)
   * \brief    Method to send response to CDB SDK using VNCSendServiceResponse()
   * \param    enResponse: [IN] Response enumeration
   * \retval   t_Void
   ***************************************************************************/
   t_Void vSendServiceResponse(VNCCDBServiceResponse enResponse) const;

   /**************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbService::spi_tclMLVncCdbService(const...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbService(const spi_tclMLVncCdbService& oService)
   * \brief   Copy Constructor, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbService(const spi_tclMLVncCdbService& oService);

   /**************************************************************************
   ** FUNCTION:  spi_tclMLVncCdbService& spi_tclMLVncCdbService::operator=(const...
   **************************************************************************/
   /*!
   * \fn      spi_tclMLVncCdbService& operator=(const spi_tclMLVncCdbService& oService)
   * \brief   Assignment Operator, will not be implemented.
   **************************************************************************/
   spi_tclMLVncCdbService& operator=(const spi_tclMLVncCdbService& oService);


   /**************************************************************************
   ** Data Members
   **************************************************************************/

   /*!
   * \brief   Map of service's objects 
   *
   * Contains all objects supported by the service.
   * Key : is the unique Object ID as defined in CCC specifications.
   * Value : Pointer to Object with ObjectID as that of Key.
   */
   std::map<VNCSBPUID, spi_tclMLVncCdbObject*> 	m_ObjectsMap;

   /*!
   * \brief   Pointer to CDB Endpoint
   *
   * Points to the Endpoint where current service is added.
   */
   VNCCDBEndpoint* 	m_prCdbEndpoint;

   /*!
   * \brief   CDB service details structure
   *
   * Contains details required by CDK SDK.
   */
   VNCCDBService 	   m_rCdbService;

   /*!
   * \brief   Service type
   *
   * Identifies the service's type.
   * Initialised in constructor of service.
   */
   tenCDBServiceType 	m_enServiceType;

   /*!
   * \brief   Service activity flag
   *
   * Identifies whether Service is currently active or not.
   */
   t_Bool   m_bIsServiceActive;

   /*!
   * \brief   Lock object
   */
   Lock  m_oLock;

};

#endif // _SPI_TCLVNCCDBSERVICE_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
