/***************************************************************************/
/*!
* \file  spi_tclMLVncCdbGeoLocatnObject.cpp
* \brief CDB Location service class
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    CDB Location service class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.12.2013  | Ramya Murthy          | Initial Version

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include<cmath>
#include "spi_tclMLVncCdbGeoLocatnObject.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCdbGeoLocatnObject.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclMLVncCdbGeoLocatnObject::spi_tclMLVncCdbGeoLocatnObject(...)
***************************************************************************/
spi_tclMLVncCdbGeoLocatnObject::spi_tclMLVncCdbGeoLocatnObject(
		const VNCCDBSDK* coprCdbSdk)
        : spi_tclMLVncCdbObject(cou32LOCATION_GEOLOCATION_OBJECT_UID, coprCdbSdk),
          m_enGnssQuality(e8GNSSQUALITY_NOFIX)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGeoLocatnObject() Entered "));
   SPI_NORMAL_ASSERT(NULL == coprCdbSdk);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbGeoLocatnObject::vOnData(...)
***************************************************************************/
t_Void spi_tclMLVncCdbGeoLocatnObject::vOnData(const trGPSData& rfcorGpsData)
{
	m_oLock.s16Lock();
	m_rGpsData = rfcorGpsData;
	m_oLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCdbGeoLocatnObject::vOnData(...)
***************************************************************************/
t_Void spi_tclMLVncCdbGeoLocatnObject::vOnData(const trSensorData& rfcorSensorData)
{
   m_oLock.s16Lock();
	m_enGnssQuality = rfcorSensorData.enGnssQuality;
   m_oLock.vUnlock();
}

/***********Start of methods overridden from spi_tclMLVncCdbObject**********/

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbGeoLocatnObject::enGet(...)
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbGeoLocatnObject::enGet(
	VNCSBPSerialize* prSerialize)
{
   VNCSBPProtocolError enSbpError = VNCSBPProtocolErrorNotAvailable;
   //@Note: This eror code needs to be set if Location data is unavailable

   if (true == bIsLocDataAvailable())
   {
      if ((NULL != prSerialize) && (NULL != m_coprCdbSdk))
      {
         //! Serialize Coordinates
         if (true == bSerializeCoordStructure(prSerialize))
         {
            m_oLock.s16Lock();
            t_U64 u64Timestamp = static_cast<t_U64>(abs(m_rGpsData.PosixTime));
            m_oLock.vUnlock();

            //! Serialize timestamp
            VNCCDBError enCdbError = m_coprCdbSdk->vncSBPSerializeLong(prSerialize,
               cou32LOCATION_GEOLOCATION_TIMESTAMP_FIELD_UID, u64Timestamp);

            if (VNCCDBErrorNone != enCdbError)
            {
               ETG_TRACE_ERR(("spi_tclMLVncCdbGeoLocatnObject::enGet(): vncSBPSerializeLong error = %x ",
                        ETG_ENUM(CDB_ERROR, enCdbError)));
            }
            else
            {
               enSbpError = VNCSBPProtocolErrorNone;
            }
         }
         else
         {
            ETG_TRACE_ERR(("spi_tclMLVncCdbGeoLocatnObject::enGet(): Serializing coordinates structure failed "));
         }
      }//if ((NULL != prSerialize) && (NULL != m_coprCdbSdk))
      else
      {
         enSbpError = VNCSBPProtocolErrorGenericRecoverable;
      }
   }//if (true == bIsLocDataAvailable())

   ETG_TRACE_USR4(("spi_tclMLVncCdbGeoLocatnObject::enGet() left with VNCSBPProtocolError = %x ",
            ETG_ENUM(SBP_ERROR, enSbpError)));
   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbGeoLocatnObject::enSubscribe(...)
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbGeoLocatnObject::enSubscribe(
	VNCSBPSubscriptionType* penSubType, 
	t_U32* pu32IntervalMs)
{
   VNCSBPProtocolError enSbpError = VNCSBPProtocolErrorNone;

   //! Evaluate the subscription type
   if (false == bIsLocDataAvailable())
   {
      enSbpError = VNCSBPProtocolErrorNotAvailable;
   }
   else if ((NULL != penSubType) && (NULL != pu32IntervalMs))
	{
		ETG_TRACE_USR3(("spi_tclMLVncCdbGeoLocatnObject::enSubscribe: entered with: "
		         "penSubType = %x, pu32IntervalMs = %u(ms)",
               ETG_ENUM(SBP_SUBSCRIPTIONTYPE, *penSubType),
               *pu32IntervalMs));

       switch (*penSubType)
       {
         case VNCSBPSubscriptionTypeRegularInterval:
         {
            //SDK handles this type. SDK automatically calls VNCSBPSourceGetRequestCallback()
            //function at the correct interval to obtain the update to send to the Sink.

            //Set error if requested interval is lesser than minimum supported interval(1s)
            //(interval >= minimum interval is ok)
            if (cou16CdbDefaultNotificationIntervalMS > *pu32IntervalMs)
            {
              enSbpError = VNCSBPProtocolErrorFeatureNotSupported ; //VNCSBPProtocolErrorWrongSubscriptionInterval;
            }
         }
            break;
         case VNCSBPSubscriptionTypeOnChange:
         {
            //enSbpError = VNCSBPProtocolErrorWrongSubscriptionType;
            *pu32IntervalMs = cou16CdbDefaultNotificationIntervalMS;
         }
            break;
         case VNCSBPSubscriptionTypeAutomatic:
         {
            //By default, we support interval based subscription
            //Hence we update the return values for SDK
            *penSubType = VNCSBPSubscriptionTypeRegularInterval;
            *pu32IntervalMs = cou16CdbDefaultNotificationIntervalMS;
         }
            break;
         default:
            ETG_TRACE_ERR(("spi_tclMLVncCdbGeoLocatnObject::enSubscribe: "
                     "Invalid SubscriptionType enum encountered: %x! ",
                     ETG_ENUM(SBP_SUBSCRIPTIONTYPE, *penSubType)));
            break;
       } //switch (*penSubType)

        vSetSubscriptionType(static_cast<tenSubscriptionType>(*penSubType));

        ETG_TRACE_USR3(("spi_tclMLVncCdbGeoLocatnObject::enSubscribe: "
            "left with: penSubType = %x, pu32IntervalMs = %u(ms)",
            ETG_ENUM(SBP_SUBSCRIPTIONTYPE, *penSubType),
            *pu32IntervalMs));
	}//if ((NULL != penSubType) && (NULL != pu32IntervalMs))

   ETG_TRACE_USR4(("spi_tclMLVncCdbGeoLocatnObject::enSubscribe() left with "
         "VNCSBPProtocolError = %x ", ETG_ENUM(SBP_ERROR, enSbpError)));
   return enSbpError;
}

/***************************************************************************
** FUNCTION:  VNCSBPProtocolError spi_tclMLVncCdbGeoLocatnObject::enCancelSubscribe()
***************************************************************************/
VNCSBPProtocolError spi_tclMLVncCdbGeoLocatnObject::enCancelSubscribe()
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGPSNmeaObject::enCancelSubscribe() entered "));
   vSetSubscriptionType(u8SUBSCRIPTIONTYPE_UNKNOWN);
   return VNCSBPProtocolErrorNone;
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCdbGeoLocatnObject::bSerializeCoordStructure(...)
***************************************************************************/
t_Bool spi_tclMLVncCdbGeoLocatnObject::bSerializeCoordStructure(VNCSBPSerialize* prSerialize)
{
   ETG_TRACE_USR1(("spi_tclMLVncCdbGeoLocatnObject::bSerializeCoordStructure Entered "));

   t_Bool bSuccess = true;

   trCoordinates rCoord;
   rCoord.dLatitude = static_cast<t_Double>((m_rGpsData.s32Latitude) / (m_rGpsData.dLatLongResolution)); //convert to degrees
   rCoord.dLongitude = static_cast<t_Double>((m_rGpsData.s32Longitude) / (m_rGpsData.dLatLongResolution)); //convert to degrees
   rCoord.dAltitude = static_cast<t_Double>(m_rGpsData.s32Altitude);
   rCoord.dSpeed = static_cast<t_Double>((m_rGpsData.s16Speed) * 100); //convert to mps

   rCoord.dHeading = static_cast<t_Double>((m_rGpsData.dHeading) / (m_rGpsData.dHeadingResolution));
   if ((0 > rCoord.dHeading) && (e8ANTICLOCKWISE == m_rGpsData.enHeadingDir))
   {
      rCoord.dHeading = 360.0 - rCoord.dHeading;
   }
   //@Note: AltitudeAccuracy and LongLatAccuracy are not available.

   if (
      (NULL != prSerialize)
      &&
      (NULL != m_coprCdbSdk)

      )
   {
      VNCCDBError enCdbError = m_coprCdbSdk->vncSBPSerializeBeginStructure(
                              prSerialize, cou32LOCATION_GEOLOCATION_COORD_FIELD_UID);

      if (VNCCDBErrorNone == enCdbError)
      {
         if (VNCCDBErrorNone != m_coprCdbSdk->vncSBPSerializeDouble(prSerialize,
                  cou32LOCATION_GEOLOCATION_LATITUDE_FIELD_UID, rCoord.dLatitude))
         {
            ETG_TRACE_ERR(("bSerializeCoordStructure(): Serializing Latitude %f failed! ", rCoord.dLatitude));
         }
         if (VNCCDBErrorNone != m_coprCdbSdk->vncSBPSerializeDouble(prSerialize,
                  cou32LOCATION_GEOLOCATION_LONGITUDE_FIELD_UID, rCoord.dLongitude))
         {
            ETG_TRACE_ERR(("bSerializeCoordStructure(): Serializing Longitude %f failed! ", rCoord.dLongitude));
         }
         if (VNCCDBErrorNone != m_coprCdbSdk->vncSBPSerializeDouble(prSerialize,
                  cou32LOCATION_GEOLOCATION_ACCURACY_FIELD_UID, rCoord.dLongLatAccuracy))
         {
            ETG_TRACE_ERR(("bSerializeCoordStructure(): Serializing LongLatAccuracy %f failed! ", rCoord.dLongLatAccuracy));
         }
         if (VNCCDBErrorNone != m_coprCdbSdk->vncSBPSerializeDouble(prSerialize,
                  cou32LOCATION_GEOLOCATION_ALTITUDE_FIELD_UID, rCoord.dAltitude))
         {
            ETG_TRACE_ERR(("bSerializeCoordStructure(): Serializing Altitude %f failed! ", rCoord.dAltitude));
         }
         if (VNCCDBErrorNone != m_coprCdbSdk->vncSBPSerializeDouble(prSerialize,
                  cou32LOCATION_GEOLOCATION_ALTITUDEACCURACY_FIELD_UID, rCoord.dAltitudeAccuracy))
         {
            ETG_TRACE_ERR(("bSerializeCoordStructure(): Serializing AltitudeAccuracy %f failed! ", rCoord.dAltitudeAccuracy));
         }
         if (VNCCDBErrorNone != m_coprCdbSdk->vncSBPSerializeDouble(prSerialize,
                  cou32LOCATION_GEOLOCATION_HEADING_FIELD_UID, rCoord.dHeading))
         {
            ETG_TRACE_ERR(("bSerializeCoordStructure(): Serializing Heading %f failed! ", rCoord.dHeading));
         }
         if (VNCCDBErrorNone != m_coprCdbSdk->vncSBPSerializeDouble(prSerialize,
                  cou32LOCATION_GEOLOCATION_SPEED_FIELD_UID,rCoord.dSpeed))
         {
            ETG_TRACE_ERR(("bSerializeCoordStructure(): Serializing Speed %f failed! ", rCoord.dSpeed));
         }
         if (VNCCDBErrorNone != m_coprCdbSdk->vncSBPSerializeEnd(prSerialize))
         {
            ETG_TRACE_ERR(("bSerializeCoordStructure(): Serialize End failed! "));
         }
      }
      else
      {
         ETG_TRACE_ERR(("bSerializeCoordStructure(): vncSBPSerializeBeginStructure error = %x ", ETG_ENUM(CDB_ERROR, enCdbError)));
      }
   }

   return bSuccess;
}

/***************************************************************************
** FUNCTION:  VNCCDBError spi_tclMLVncCdbGeoLocatnObject::bIsLocDataAvailable()
***************************************************************************/
t_Bool spi_tclMLVncCdbGeoLocatnObject::bIsLocDataAvailable()
{
   m_oLock.s16Lock();
   t_Bool bLocDataAvailable = (e8GNSSQUALITY_NOFIX != m_enGnssQuality);
   m_oLock.vUnlock();

   ETG_TRACE_USR4(("bIsLocDataAvailable(): %d ", bLocDataAvailable));

   return bLocDataAvailable;
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
