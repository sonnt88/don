/*!
 *******************************************************************************
 * \file              spi_tclMLVncRespCDB.h
 * \brief             RealVNC Wrapper Response class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:   VNC Response class for receiving response receiving response
 for spi_tclMLVncRespCDB calls to implement the client side Common Data Bus (CDB).
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 04.09.2013 |  A VIJETH                    | Initial Version
 01.04.2014 |  Ramya Murthy                | Redefined interface

 \endverbatim
 ******************************************************************************/
#ifndef SPI_TCLMLVNCRESPCDB_H
#define SPI_TCLMLVNCRESPCDB_H

#include "SPITypes.h"
#include "RespBase.h"

class spi_tclMLVncRespCDB: public RespBase
{

public:

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclMLVncRespCDB::spi_tclMLVncRespCDB()
    ***************************************************************************/
   /*!
    * \fn      spi_tclMLVncRespCDB()
    * \brief   Constructor
    * \sa      spi_tclMLVncRespCDB()
    **************************************************************************/
   spi_tclMLVncRespCDB();

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclMLVncRespCDB::~spi_tclMLVncRespCDB()
    ***************************************************************************/
   /*!
    * \fn      virtual ~spi_tclMLVncRespCDB()
    * \brief   Destructor
    * \sa      spi_tclMLVncRespCDB()
    **************************************************************************/
   virtual ~spi_tclMLVncRespCDB();

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclMLVncRespCDB::vPostSetLocDataSubscription(...)
    ***************************************************************************/
   /*!
    * \fn     vPostSetLocDataSubscription(t_Bool bSubscriptionOn)
    * \brief   Method to subscribe/unsubscribe for location data information.
    * \param   bSubscriptionOn: [IN] Indicates if location data should be
    *             subscribed/unsubscribed.
    * \retval  None
    **************************************************************************/
   virtual t_Void vPostSetLocDataSubscription(t_Bool bSubscriptionOn);

};

#endif
