
/***********************************************************************/
/*!
* \file  spi_tclMLVncCmdDiscoverer.h
* \brief ML Discoverer Input Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    ML Discoverer Input Interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version
24.07.2014  | Shiva Kumar Gurija    | Work around to fetch the missing app name
24.04.2014  |  Shiva kumar Gurija   | XML Validation
25.06.2015  | Sameer Chandra        | Added ML XDeviceKey Support for PSA

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLMLVNCCMDDISCOVERER_H_
#define _SPI_TCLMLVNCCMDDISCOVERER_H_


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclMLVncCmdDiscoverer
* \brief ML Discoverer Input Interface
*
* It provides an interface to SPI, to fetch and set the device related
* information. Which includes, DeviceInfo, Applications Supported by device
* (both certified and non certified), Application Icon Attribute Info,
* Applications Status details, Application Certification Info, BT Address of
* the device and Launch and Termination of applications.
*
****************************************************************************/
class spi_tclMLVncCmdDiscoverer
{

public:

    /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncCmdDiscoverer::spi_tclMLVncCmdDiscoverer()
    ***************************************************************************/
    /*!
    * \fn      spi_tclMLVncCmdDiscoverer()
    * \brief   Default Constructor
    * \param   t_Void
    * \sa      ~spi_tclMLVncCmdDiscoverer()
    **************************************************************************/
    spi_tclMLVncCmdDiscoverer();

    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncCmdDiscoverer::~spi_tclMLVncCmdDiscoverer()
    ***************************************************************************/
    /*!
    * \fn      virtual ~spi_tclMLVncCmdDiscoverer()
    * \brief   Destructor
    * \param   t_Void
    * \sa      spi_tclMLVncCmdDiscoverer()
    **************************************************************************/
    virtual ~spi_tclMLVncCmdDiscoverer();


    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::vInitializeDiscoverers()
    ***************************************************************************/
    /*!
    * \fn      t_Bool vInitializeDiscoverers()
    * \brief   Initialize SDK and create discoverers
    * \param   t_Void
    * \retval  t_Bool
    * \sa      vInitializeDiscoverers()
    **************************************************************************/
    t_Bool bInitializeDiscoverers()const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vUnInitializeDiscoverers()
    ***************************************************************************/
    /*!
    * \fn      t_Void vUnInitializeDiscoverers()
    * \brief   UnInitialize sdk and destroy discoverers
    * \param   t_Void
    * \retval  t_Void
    * \sa      vUnInitializeDiscoverers()
    **************************************************************************/
    t_Void vUnInitializeDiscoverers()const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vStartDiscovery()
    ***************************************************************************/
    /*!
    * \fn      t_Void vStartDeviceDiscovery()
    * \brief   Starts all discoverers for device detection
    * \param   t_Void
    * \retval  t_Void
    * \sa      vStopDeviceDiscovery()
    **************************************************************************/
    t_Void vStartDeviceDiscovery()const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vStopDeviceDiscovery()
    ***************************************************************************/
    /*!
    * \fn      t_Void vStopDeviceDiscovery()
    * \brief   Stops all device discoverers
    * \param   t_Void
    * \retval  t_Void
    * \sa      vStartDeviceDiscovery()
    **************************************************************************/
    t_Void vStopDeviceDiscovery()const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vRefetchDeviceInfo
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppList(const t_U32 cou32DeviceHandle)
    * \brief   To get the Application list of a device.
    * \param   u16DeviceId  : [IN] Device Id
    * \retval  t_Void
    * \sa      vGetCertifiedAppList(tU16 u16DeviceId)
    **************************************************************************/
    t_Void vRefetchDeviceInfo(const t_U32 cou32DeviceHandle)const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppList(const trUserContext...)
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppList(const t_U32 cou32DeviceHandle)
    * \brief   To get the Application list of a device.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \retval  t_Void
    * \sa      vGetCertifiedAppList(const t_U32 cou32DeviceHandle)
    **************************************************************************/
    t_Void vGetAppList(const trUserContext corUserContext, const t_U32 cou32DeviceHandle)const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetCertifiedAppList(const trUserContext...)
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetCertifiedAppList(const t_U32 cou32DeviceHandle)
    * \brief   To get the certified application list of a device.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \retval  t_Void
    * \sa      vGetAppList(const t_U32 cou32DeviceHandle)
    **************************************************************************/
    t_Void vGetCertifiedAppList(const trUserContext corUserContext, const t_U32 cou32DeviceHandle)const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppInfo(const trUserContext..
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppInfo(const t_U32 cou32DeviceHandle,t_U32 u32AppId)
    * \brief   To get an application info.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \param   u32AppId     : [IN] Application Id
    * \retval  t_Void
    **************************************************************************/
    t_Void vGetAppInfo(const trUserContext corUserContext, 
       const t_U32 cou32DeviceHandle,
       t_U32 u32AppId)const;

     /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppInfo(const t_U32 cou32DeviceHandle,t_U32 u32AppId,
    *            trApplicationInfo& corfrAppInfo)
    * \brief   To get an application info.
    * \param   u16DeviceId  : [IN] Device Id
    * \param   u32AppId     : [IN] Application Id
    * \param   corfrAppInfo : [OUT] Application Info
    * \retval  t_Void
    **************************************************************************/
    t_Void vGetAppInfo(const t_U32 cou32DeviceHandle,
       const t_U32 cou32AppId,
       trApplicationInfo& corfrAppInfo);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vLaunchApplication(const trUserContext..
    ***************************************************************************/
    /*!
    * \fn      t_Void vLaunchApplication(const t_U32 cou32DeviceHandle,tU32 u32AppId)
    * \brief   To Launch an Application.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \param   u32AppId     : [IN] Application Id
    * \retval  t_Void
    * \sa      vTerminateApplication(const t_U32 cou32DeviceHandle,t_U32 u32AppId)
    **************************************************************************/
    t_Void vLaunchApplication(const trUserContext corUserContext, const t_U32 cou32DeviceHandle,t_U32 u32AppId)const;


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vTerminateApplication((const trUserContext..
    ***************************************************************************/
    /*!
    * \fn      t_Void vTerminateApplication(const t_U32 cou32DeviceHandle, t_U32 u32AppId)
    * \brief   To Terminate an application.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \param   u32AppId     : [IN] Application Id
    * \retval  t_Void
    * \sa      vLaunchApplication(const t_U32 cou32DeviceHandle,t_U32 u32AppId)
    **************************************************************************/
    t_Void vTerminateApplication(const trUserContext corUserContext, const t_U32 cou32DeviceHandle,t_U32 u32AppId)const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppStatus(const trUserContext ...
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppStatus(const t_U32 cou32DeviceHandle,t_U32 u32AppId )
    * \brief   To get the status of an application.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \param   u32AppId     : [IN] Application Id
    * \retval  t_Void
    * \sa      vGetAllAppsStatuses(const t_U32 cou32DeviceHandle)
    **************************************************************************/
    t_Void vGetAppStatus(const trUserContext corUserContext, const t_U32 cou32DeviceHandle,t_U32 u32AppId )const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAllAppsStatuses(const trUserContext...
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAllAppsStatuses(const t_U32 cou32DeviceHandle)
    * \brief   To get the statuses of the all applications supported by device.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \retval  t_Void
    * \sa      vGetAppStatus(const t_U32 cou32DeviceHandle,t_U32 u32AppId )
    **************************************************************************/
    t_Void vGetAllAppsStatuses(const trUserContext corUserContext, const t_U32 cou32DeviceHandle)const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetBTAddress(const trUserContext..
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetBTAddress(const t_U32 cou32DeviceHandle)
    * \brief   To get the BT Address of the Device
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \retval  t_Void
    * \sa      vSetBTAddress(const t_U32 cou32DeviceHandle, const t_Char* cszVal)
    **************************************************************************/
    t_Void vGetBTAddress(const trUserContext corUserContext, const t_U32 cou32DeviceHandle)const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vSetBTAddress(const trUserContext..
    ***************************************************************************/
    /*!
    * \fn      t_Void vSetBTAddress(const t_U32 cou32DeviceHandle, const t_Char* pcocVal)
    * \brief   To set the BT Address of the device.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId  : [IN] Device Id
    * \param   pcocVal      : [IN] BT Address value to set
    * \retval  t_Void
    * \sa      vGetBTAddress(const t_U32 cou32DeviceHandle)
    **************************************************************************/
    t_Void vSetBTAddress(
        const trUserContext corUserContext,
        const t_U32 cou32DeviceHandle,
        const t_Char* pcocVal)const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppIconAttr(const trUserContext..
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppIconAttr(const t_U32 cou32DeviceHandle,t_U32 u32AppId,t_U32 u32IconId)
    * \brief   To get the particular Icon's attributes of an an Application.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u16DeviceId    : [IN] Device Id
    * \param   u32AppId       : [IN] Application Id
    * \param   u32IconId      : [IN] Icon Id
    * \retval  t_Void
    * \sa      vSetAppIconAttr()
    **************************************************************************/
    t_Void vGetAppIconAttr(
        const trUserContext corUserContext,
        const t_U32 cou32DeviceHandle,
        t_U32 u32AppId, 
        t_U32 u32IconId)const;

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bSetAppIconAttr(const trUserContext..
    ***************************************************************************/
    /*!
    * \fn      t_Bool bSetAppIconAttr(const t_U32 cou32DeviceHandle, t_U32 u32AppId,
    *                            const IconAttributes& corfrIconAttr)
    * \brief   To set the particular Icon's attributes of an an Application.
    * \param   u16DeviceId    : [IN] Device Id
    * \param   u32AppId       : [IN] Application Id
    * \param   u32IconId      : [IN] Icon Id
    * \param   corfrIconAttr  : [IN] Icon Attributes structure to set
    * \retval  t_Bool
    * \sa      vGetAppIconAttr(const t_U32 cou32DeviceHandle, t_U32 u32AppId, t_U32 u32IconId)
    **************************************************************************/
    t_Bool bSetAppIconAttr(
      const t_U32 cou32DeviceHandle,
      t_U32 u32AppId,
      t_U32 u32IconId,
      const trIconAttributes& corfrIconAttr) const;

     /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bLaunchApplication(const 
    ***************************************************************************/
    /*!
    * \fn      t_Bool bLaunchApplication(const trUserContext corUserContext,
    *          , const t_U32 cou32DeviceHandle,t_U32 u32AppId)
    * \brief   To Launch an Application synchronously.
    * \param   cou32DeviceHandle  : [IN] Device Id
    * \param   u32AppId     : [IN] Application Id
    * \retval  t_Bool
    * \sa      vTerminateApplication(const t_U32 cou32DeviceHandle,t_U32 u32AppId)
    **************************************************************************/
    t_Bool bLaunchApplication(
       const t_U32 cou32DeviceHandle,
       t_U32 u32AppId)const;

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::vGetDeviceVersion()
    ***************************************************************************/
    /*!
    * \fn      t_Bool vGetDeviceVersion(const t_U32 cou32DeviceHandle,
    *            trVersionInfo& rfrVersionInfo)
    * \brief   To get the Device ML version Info
    * \param   cou32DeviceHandle  : [IN] Device Id
    * \param   rfrVersionInfo     : [OUT] Device VersionInfo
    * \retval  t_Void
    **************************************************************************/
    t_Void vGetDeviceVersion(const t_U32 cou32DeviceHandle,trVersionInfo& rfrVersionInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppIconData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetAppIconData(const t_U32 cou32DeviceHandle,
   *         t_String szAppIconUrl, 
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  cou32DeviceHandle  : [IN] Device Id
   * \param  szAppIconUrl  : [IN] Application Icon data
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval t_Void
   **************************************************************************/
    t_Void vGetAppIconData(const t_U32 cou32DeviceHandle,
       t_String szAppIconUrl, 
       const trUserContext& rfrcUsrCntxt);

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bGetAppIDforProtocol()
    ***************************************************************************/
    /*!
    * \fn    t_Bool bGetAppIDforProtocol
    * \brief  To Get the protocol IDs for applications
    * \param  cou32DeviceID  : [IN] Device Id
    * \param  corfrszProtocolID  : [IN] protocol ID
    * \param  rfru32AppID  : [OUT] AppID for protocol corfrszProtocolID
    * \retval t_Void
       **************************************************************************/
    t_Bool bGetAppIDforProtocol(const t_U32 cou32DeviceID,
             const t_String &corfrszProtocolID, t_U32 &rfru32AppID);

    /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vSubscribeForDeviceEvents()
   ***************************************************************************/
   /*!
   * \fn    t_Void vSubscribeForDeviceEvents(const t_U32 cou32DeviceHandle,
   *         t_Bool bSubscribe,
       const trUserContext& corfrUsrCntxt)
   * \brief  To register with the device to notify, if there is any change in
   *         the applist or application status after launching/terminating appl.
   * \param  cou32DeviceHandle  : [IN] Device Id
   * \param  bSubscribe         : [IN] Subscribe - True
   *                                   Unsubscribe - False
   * \param  corfrUsrCntxt       : [IN] User Context
   * \retval t_Void
   **************************************************************************/
    t_Void vSubscribeForDeviceEvents(const t_U32 cou32DeviceHandle,
       t_Bool bSubscribe,
       const trUserContext& corfrUsrCntxt) const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vDisplayAppcertificationInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevHandle,
   *             const t_U32 cou32AppHandle)
   * \brief   To disaply th eapplication certification info includes
   *           restricted & non restricted list of the application
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppHandle  : [IN] Application Id
   * \retval  t_Void
   **************************************************************************/
    t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevHandle,
       const t_U32 cou32AppHandle);


   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bSetClientProfile()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bSetClientProfile(const t_U32 cou32DevHandle,
   *             const trClientProfile& corfrClientProfile,
   *             const trUserContext& corfrUsrCntxt)
   * \brief   To set the client profiles
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   corfrClientProfile  : [IN] Client Profile to be set
   * \param   corfrUsrCntxt : [IN] User context
   * \retval  t_Bool
   **************************************************************************/
    t_Bool bSetClientProfile(const t_U32 cou32DevHandle,
       const trClientProfile& corfrClientProfile,
       const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncCmdDiscoverer::vDisplayAppListXml()
   ***************************************************************************/
   /*!
   * \fn     t_Void vDisplayAppListXml(const t_U32 cou32DevHandle)
   * \brief  Method to retrieve the Applications XML Buffer of the device
   * \param  cou32DevHandle : [IN] Device ID
   * \retval t_Void
   **************************************************************************/
    t_Void vDisplayAppListXml(const t_U32 cou32DevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vFetchAppName()
   ***************************************************************************/
   /*!
   * \fn    t_Void vFetchAppName(t_U32 u32DevHandle,
   *         t_U32 u32AppHandle,const trUserContext& cofrUserCntxt)
   * \brief  To Get the application name
   * \param  u32DevHandle  : [IN] Device Id
   * \param  u32AppHandle  : [IN] Application handle
   * \param  cofrUserCntxt : [IN] User Context
   * \retval t_Void
   **************************************************************************/
    t_Void vFetchAppName(t_U32 u32DevHandle, 
       t_U32 u32AppHandle, 
       const trUserContext& cofrUserCntxt);

    /***************************************************************************
    ** FUNCTION:  t_String spi_tclMLVncCmdDiscoverer::szFetchAppName()
    ***************************************************************************/
    /*!
    * \fn    t_String szFetchAppName(t_U32 u32DevHandle,
    *         t_U32 u32AppHandle)
    * \brief  To Get the application name
    * \param  u32DevHandle  : [IN] Device Id
    * \param  u32AppHandle  : [IN] Application handle
    * \param  cofrUserCntxt : [IN] User Context
    * \retval t_String
    **************************************************************************/
    t_String szFetchAppName(t_U32 u32DevHandle, 
       t_U32 u32AppHandle);

   /***************************************************************************
   ** FUNCTION:  tenAppCategory spi_tclMLVncCmdDiscoverer::enGetAppCategory()
   ***************************************************************************/
   /*!
   * \fn    tenAppCategory enGetAppCategory(t_U32 u32DevHandle,
   *         t_U32 u32AppHandle)
   * \brief  To Get the application category
   * \param  u32DevHandle  : [IN] Device Id
   * \param  u32AppHandle  : [IN] Application handle
   * \retval tenAppCategory
   **************************************************************************/
   tenAppCategory enGetAppCategory(t_U32 u32DevHandle,t_U32 u32AppHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vSetUPnPAppPublicKeyForXMLValidation()
   ***************************************************************************/
   /*!
   * \fn    t_Void vSetUPnPAppPublicKeyForXMLValidation(t_U32 u32DevHandle,
   *         t_String szUPnPAppPublicKey,const trUserContext& corfrUsrCntxt)
   * \brief  To Set the UPnP application public key for XML validation
   * \param  u32DevHandle  : [IN] Device Id
   * \param  szUPnPAppPublicKey  : [IN] UPnP Application Public Key
   * \param  corfrUsrCntxt : [IN] User context
   * \retval t_Void
   **************************************************************************/
   t_Void vSetUPnPAppPublicKeyForXMLValidation(t_U32 u32DevHandle,
      t_String szUPnPAppPublicKey,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vFetchAppUri()
   ***************************************************************************/
   /*!
   * \fn    t_Void vFetchAppUri(const t_U32 cou32DevID,
   *         const t_U32 cou32AppID,t_String& rfszAppUri,t_String& rfszProtocolID)
   * \brief  Method to retrieve the URL of an application
   * \param  cou32DevID  : [IN] Device Id
   * \param  cou32AppID  : [IN] Application ID
   * \param  rfszAppUri  : [OUT] App URI
   * \param  rfszProtocolID : [OUT] App Protocol ID
   * \retval t_Void
   **************************************************************************/
   t_Void vFetchAppUri(const t_U32 cou32DevID, 
      const t_U32 cou32AppID, 
      t_String& rfszAppUri,
      t_String& rfszProtocolID); 

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::bRequestMirrorlinkSwitch()
   ***************************************************************************/
   /*!
   * \fn    t_Void bRequestMirrorlinkSwitch(const t_U32 cou32DevID)
   * \brief  Method to request mirrorlink switch
   * \param  cou32DevID  : [IN] Device Id
   * \retval t_Bool true if switch command was successfully issued.
   **************************************************************************/
   t_Bool bRequestMirrorlinkSwitch(const t_U32 cou32DevID);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bisDeviceInMirrorlinkMode()
   ***************************************************************************/
   /*!
   * \fn    t_Bool bisDeviceInMirrorlinkMode
   * \brief  checks if device is in mirrorlink mode
   * \param  cou32DeviceHandle  : [IN] Device Id
   * \retval t_Bool : returns true if device is in mirrorlink mode
   **************************************************************************/
   t_Bool bisDeviceInMirrorlinkMode(const t_U32 cou32DevID);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetKeyIconData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetKeyIconData(const t_U32 cou32DeviceHandle,
   *         t_String szKeyIconUrl,
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  cou32DeviceHandle  : [IN] Device Id
   * \param  szKeyIconUrl  : [IN] Key Icon data
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval t_Void
   **************************************************************************/
    t_Void vGetKeyIconData(const t_U32 cou32DeviceHandle,
       t_String szKeyIconUrl,
       const trUserContext& rfrcUsrCntxt);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetXDeviceKeyInfo()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetKeyIconData(const t_U32 cou32DeviceHandle,
   *         t_String szKeyIconUrl,
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  cou32DeviceHandle  : [IN] Device Id
   * \param  szKeyIconUrl  : [IN] Key Icon data
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval t_Void
   **************************************************************************/
    t_Void vGetXDeviceKeyInfo(const t_U32 u32DeviceID,
          t_U16& u16NumXDevices,std::vector<trXDeviceKeyDetails> &vecrXDeviceKeyDetails);

    /***************************************************************************
    ****************************END OF PUBLIC***********************************
    ***************************************************************************/

protected:

    /***************************************************************************
    *********************************PROTECTED**********************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncCmdDiscoverer(const spi_tclMLVncCmdDiscoverer...
    ***************************************************************************/
    /*!
    * \fn      spi_tclMLVncCmdDiscoverer(
    *                             const spi_tclMLVncCmdDiscoverer& corfoSrc)
    * \brief   Copy constructor - Do not allow the creation of copy constructor
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclMLVncCmdDiscoverer()
    ***************************************************************************/
    spi_tclMLVncCmdDiscoverer(const spi_tclMLVncCmdDiscoverer& corfoSrc);


    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncCmdDiscoverer& operator=( const spi_tclMLV...
    ***************************************************************************/
    /*!
    * \fn      spi_tclMLVncCmdDiscoverer& operator=(
    *                          const spi_tclMLVncCmdDiscoverer& corfoSrc))
    * \brief   Assignment operator
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclMLVncCmdDiscoverer(const spi_tclMLVncCmdDiscoverer& otrSrc)
    ***************************************************************************/
    spi_tclMLVncCmdDiscoverer& operator=(const spi_tclMLVncCmdDiscoverer& corfoSrc);


    /***************************************************************************
    ****************************END OF PROTECTED********************************
    ***************************************************************************/

private:

    /***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/


    /***************************************************************************
    ****************************END OF PRIVATE *********************************
    ***************************************************************************/


}; //class spi_tclMLVncCmdDiscoverer

#endif //_SPI_TCLMLVNCCMDDISCOVERER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
