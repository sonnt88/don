
/***********************************************************************/
/*!
* \file  spi_tclMLVncDiscovererDataIntf.h
* \brief ML Discoverer Data Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    ML Discoverer Data Interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version

\endverbatim
**************************************************************************/

#ifndef _SPI_TCLMLVNCDISCOVERERDATAINTF_H_
#define _SPI_TCLMLVNCDISCOVERERDATAINTF_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "WrapperTypeDefines.h"


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

//! forward declaration of MLVncDiscoverer class
class spi_tclMLVncDiscoverer;


/****************************************************************************/
/*!
* \class spi_tclMLVncDiscovererDataIntf
* \brief ML Discoverer Data Interface
*
* It provides an interface to get the Mirrorlink doscoverer's information.
* It provides interface to get VNCDiscoverySDK*, VNCDiscoverySDKDiscoverer* and
* VNCDiscoverySDKEntity* based on the Device ID and vice versa
****************************************************************************/

class spi_tclMLVncDiscovererDataIntf
{

public:

    /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncDiscovererDataIntf::spi_tclMLVncDiscovererDataIntf()
    ***************************************************************************/
    /*!
    * \fn      spi_tclMLVncDiscovererDataIntf()
    * \brief   Constructor
    * \param
    * \sa      ~spi_tclMLVncDiscovererDataIntf()
    **************************************************************************/
    spi_tclMLVncDiscovererDataIntf();

    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncDiscovererDataIntf::~spi_tclMLVncDiscovererDataIntf()
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclMLVncDiscovererDataIntf()
    * \brief   Destructor
    * \param
    * \sa      spi_tclMLVncDiscovererDataIntf()
    **************************************************************************/
    ~spi_tclMLVncDiscovererDataIntf();

    /***************************************************************************
    ** FUNCTION:  const trvncDeviceHandles& spi_tclMLVncDiscovererDataIntf::co...
    ***************************************************************************/
    /*!
    * \fn      const trDeviceHandles& corfrGetDiscHandles(t_U32 u32DeviceID)
    * \brief   To get the device handles based on Device Id
    * \param   u32DeviceID : [IN] Device Id
    * \retval  trvncDeviceHandles& : const reference to the device handle structure
    * \sa      u16GetDeviceId()
    **************************************************************************/
    const trvncDeviceHandles& corfrGetDiscHandles(t_U32 u32DeviceId);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscovererDataIntf::vGetDeviceId(VNCDisco...
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetDeviceId(VNCDiscoverySDKDiscoverer* poDisc,
    *                  VNCDiscoverySDKEntity* poEntity,t_U16& u32DeviceID)
    * \brief   To get the Device Id based on Device handles.
    * \param   poDisc       : [IN]  Pointer to VNCDiscoverySDKDiscoverer
    * \param   poEntity     : [IN]  Pointer to VNCDiscoverySDKEntity
    * \param   u32DeviceID  : [OUT] Id of the Device pointed by poDisc and poEntity
    * \retval  t_Void
    * \sa      poGetDiscoveryHandles(t_U32 u32DeviceID)
    **************************************************************************/
    t_Void vGetDeviceId(
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity* poEntity,
        t_U32& u32DeviceID)const;

    /***************************************************************************
    ** FUNCTION:  VNCDiscoverySDK* spi_tclMLVncDiscovererDataIntf::poGetDiscSDK()
    ***************************************************************************/
    /*!
    * \fn      VNCDiscoverySDK* poGetDiscSDK()
    * \brief   To get the DiscoverySDK pointer
    * \retval  VNCDiscoverySDK*
    **************************************************************************/
    VNCDiscoverySDK* poGetDiscSDK()const;

    /***************************************************************************
    ** FUNCTION:  VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscovererDataIntf::poGetMLDisc()
    ***************************************************************************/
    /*!
    * \fn      VNCDiscoverySDKDiscoverer* poGetMLDisc()
    * \brief   To get the ML Discoverer pointer
    * \retval  VNCDiscoverySDKDiscoverer*
    **************************************************************************/
    VNCDiscoverySDKDiscoverer* poGetMLDisc()const;

    /***************************************************************************
    ** FUNCTION:  t_String spi_tclMLVncDiscovererDataIntf::szGetAppID()
    ***************************************************************************/
    /*!
    * \fn      t_String szGetAppID(const t_U32 cou32DevID, 
    *                const t_U32 cou32AppID)
    * \brief   To get the equivalent string application ID for the received
    *                Device ID & Application ID
    * \param   cou32DevID     : [IN] Unique Device ID
    * \param   cou32AppID     : [IN] Uique Application ID
    * \retval  t_String 
    **************************************************************************/
    t_String szGetAppID(const t_U32 cou32DevID, const t_U32 cou32AppID);

    /***************************************************************************
    ****************************END OF PUBLIC***********************************
    ***************************************************************************/

protected:

    /***************************************************************************
    *********************************PROTECTED**********************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncDiscovererDataIntf(const spi_tclMLVncDiscoverer...
    ***************************************************************************/
    /*!
    * \fn      spi_tclMLVncDiscovererDataIntf(
    *                             const spi_tclMLVncDiscovererDataIntf& corfoSrc)
    * \brief   Copy constructor - Do not allow the creation of copy constructor
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclMLVncDiscovererDataIntf()
    ***************************************************************************/
    spi_tclMLVncDiscovererDataIntf(const spi_tclMLVncDiscovererDataIntf& corfoSrc);


    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncDiscovererDataIntf& operator=( const spi_tclMLV...
    ***************************************************************************/
    /*!
    * \fn      spi_tclMLVncDiscovererDataIntf& operator=(
    *                          const spi_tclMLVncDiscovererDataIntf& corfoSrc))
    * \brief   Assignment operator
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclMLVncDiscovererDataIntf(
    *                         const spi_tclMLVncDiscovererDataIntf& otrSrc)
    ***************************************************************************/
    spi_tclMLVncDiscovererDataIntf& operator=(
        const spi_tclMLVncDiscovererDataIntf& otrSrc);


    /***************************************************************************
    ****************************END OF PROTECTED********************************
    ***************************************************************************/

private:

    /***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/

    //! Device Handle structure contains Discoverer, DiscoverySDK, Entity info
    trvncDeviceHandles m_rDeviceHandles;


    /***************************************************************************
    ****************************END OF PRIVATE *********************************
    ***************************************************************************/

};// class spi_tclMLVncDiscoverer


#endif   //_SPI_TCLMLVNCDISCOVERERDATAINTF_H_

////////////////////////////////////////////////////////////////////////////////
// <EOF>
