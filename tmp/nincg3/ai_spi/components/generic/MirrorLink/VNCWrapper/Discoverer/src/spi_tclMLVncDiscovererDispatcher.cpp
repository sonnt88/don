/***********************************************************************/
/*!
 * \file  spi_tclMLVncDiscovererDispatcher.cpp
 * \brief Message Dispatcher for Discoverer Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Discoverer Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 14.05.2014  | Shiva Kumar Gurija    | Changes in AppList Handling 
                                        for CTS Test
 25.06.2015  | Sameer Chandra        | Added ML XDeviceKey Support for PSA

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/

#include "StringHandler.h"
#include "RespRegister.h"
#include "spi_tclMLVncDiscovererDispatcher.h"
#include "spi_tclMLVncRespDiscoverer.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
#include "trcGenProj/Header/spi_tclMLVncDiscovererDispatcher.cpp.trc.h"
#endif
#endif

#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleDiscoveryMsg(this);              \
   }                                                        \
   vDeAllocateMsg();                                        \
}                                                           \

/***************************************************************************
 ** FUNCTION:  MLDiscovererMsgBase::MLDiscovererMsgBase
 ***************************************************************************/
MLDiscovererMsgBase::MLDiscovererMsgBase(): m_u32DeviceHandle(0)
{
   vSetServiceID( e32MODULEID_VNCDISCOVERER);
}

/***************************************************************************
 ** FUNCTION:  MLDiscovererMsgBase::u32GetDeviceHandle
 ***************************************************************************/
t_U32 MLDiscovererMsgBase::u32GetDeviceHandle()
{
   return m_u32DeviceHandle;
}
/***************************************************************************
 ** FUNCTION:  MLDiscovererMsgBase::vSetDeviceHandle
 ***************************************************************************/
t_Void MLDiscovererMsgBase::vSetDeviceHandle(t_U32 u32DevHndle)
{
   m_u32DeviceHandle = u32DevHndle;
}

/***************************************************************************
 ** FUNCTION:  MLDeviceInfoMsg::MLDeviceInfoMsg
 ***************************************************************************/
MLDeviceInfoMsg::MLDeviceInfoMsg():prDeviceInfo(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLDeviceInfoMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLDeviceInfoMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLDeviceInfoMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLDeviceInfoMsg::vAllocateMsg()
{
   prDeviceInfo = new trMLDeviceInfo;
   SPI_NORMAL_ASSERT(NULL == prDeviceInfo);
}

/***************************************************************************
 ** FUNCTION:  MLDeviceInfoMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLDeviceInfoMsg::vDeAllocateMsg()
{
   RELEASE_MEM(prDeviceInfo);
}

/***************************************************************************
 ** FUNCTION:  MLDeviceDisconnMsg::MLDeviceDisconnMsg
 ***************************************************************************/
MLDeviceDisconnMsg::MLDeviceDisconnMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLDeviceDisconnMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLDeviceDisconnMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLBTAddrMsg::MLBTAddrMsg
 ***************************************************************************/
MLBTAddrMsg::MLBTAddrMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLBTAddrMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLBTAddrMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLBTAddrMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLBTAddrMsg::vAllocateMsg()
{
   pszBTAddr= new t_String;
   SPI_NORMAL_ASSERT(NULL == pszBTAddr);
}

/***************************************************************************
 ** FUNCTION:  MLBTAddrMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLBTAddrMsg::vDeAllocateMsg()
{
   RELEASE_MEM(pszBTAddr);
}

/***************************************************************************
 ** FUNCTION:  MLAppName::MLAppName()
 ***************************************************************************/
MLAppName::MLAppName():m_u32AppHandle(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAppName::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAppName, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAppName::vAllocateMsg
 ***************************************************************************/
t_Void MLAppName::vAllocateMsg()
{
   m_pszAppName= new t_String;
   SPI_NORMAL_ASSERT(NULL == m_pszAppName);
}

/***************************************************************************
 ** FUNCTION:  MLAppName::vDeAllocateMsg
 ***************************************************************************/
t_Void MLAppName::vDeAllocateMsg()
{
   RELEASE_MEM(m_pszAppName);
}


/***************************************************************************
 ** FUNCTION:  MLAppStatusMsg::MLAppStatusMsg
 ***************************************************************************/
MLAppStatusMsg::MLAppStatusMsg() :
   u32AppID(0),enAppStatus(e8NOTRUNNING)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAppStatusMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAppStatusMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAllAppStatusMsg::MLAllAppStatusMsg
 ***************************************************************************/
MLAllAppStatusMsg::MLAllAppStatusMsg():prMapAllStatuses(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAllAppStatusMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAllAppStatusMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAllAppStatusMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLAllAppStatusMsg::vAllocateMsg()
{
   prMapAllStatuses = new std::map<t_U32, std::map<t_String, t_String> >;
   SPI_NORMAL_ASSERT(NULL == prMapAllStatuses);
}

/***************************************************************************
 ** FUNCTION:  MLAllAppStatusMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLAllAppStatusMsg::vDeAllocateMsg()
{
   RELEASE_MEM(prMapAllStatuses);
}

/***************************************************************************
 ** FUNCTION:  MLAppListMsg::MLAppListMsg
 ***************************************************************************/
MLAppListMsg::MLAppListMsg() : pvecszAppIds(NULL),u16AppListLen(0),bCertAppList(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAppListMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAppListMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAppListMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLAppListMsg::vAllocateMsg()
{
   pvecszAppIds = new std::vector<t_U32>;
   SPI_NORMAL_ASSERT(NULL == pvecszAppIds);
}

/***************************************************************************
 ** FUNCTION:  MLAppListMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLAppListMsg::vDeAllocateMsg()
{
   RELEASE_MEM(pvecszAppIds);
}

/***************************************************************************
 ** FUNCTION:  MLProtocolIDMsg::MLProtocolIDMsg
 ***************************************************************************/
MLProtocolIDMsg::MLProtocolIDMsg() : pszProtocolID(NULL), u32AppID(0), bIsLastApp(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLProtocolIDMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLProtocolIDMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLProtocolIDMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLProtocolIDMsg::vAllocateMsg()
{
   pszProtocolID = new t_String;
   SPI_NORMAL_ASSERT(NULL == pszProtocolID);
}

/***************************************************************************
 ** FUNCTION:  MLProtocolIDMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLProtocolIDMsg::vDeAllocateMsg()
{
   RELEASE_MEM(pszProtocolID);
}

/***************************************************************************
 ** FUNCTION:  MLLaunchAppStatusMsg::MLLaunchAppStatusMsg
 ***************************************************************************/
MLLaunchAppStatusMsg::MLLaunchAppStatusMsg() :
   u32AppID(0), bLaunchAppStatus(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLLaunchAppStatusMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLLaunchAppStatusMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLTerminateAppStatusMsg::MLTerminateAppStatusMsg
 ***************************************************************************/
MLTerminateAppStatusMsg::MLTerminateAppStatusMsg() :
   u32AppID(0), bTerminateAppStatus(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLTerminateAppStatusMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLTerminateAppStatusMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAppInfoMsg::MLAppInfoMsg
 ***************************************************************************/
MLAppInfoMsg::MLAppInfoMsg() :prAppInfo(NULL),
   u32AppID(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAppInfoMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAppInfoMsg, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAppInfoMsg::vAllocateMsg
 ***************************************************************************/
t_Void MLAppInfoMsg::vAllocateMsg()
{
   prAppInfo = new trApplicationInfo;
   SPI_NORMAL_ASSERT(NULL == prAppInfo);
}

/***************************************************************************
 ** FUNCTION:  MLAppInfoMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MLAppInfoMsg::vDeAllocateMsg()
{
   RELEASE_MEM(prAppInfo);
}

/***************************************************************************
 ** FUNCTION:  MLAppIconAttr::MLAppIconAttr
 ***************************************************************************/
MLAppIconAttr::MLAppIconAttr() : prAppIconAttr(NULL),
   u32AppID(0)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAppIconAttr::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAppIconAttr, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAppIconAttr::vAllocateMsg
 ***************************************************************************/
t_Void MLAppIconAttr::vAllocateMsg()
{
   prAppIconAttr = new trIconAttributes;
   SPI_NORMAL_ASSERT(NULL == prAppIconAttr);
}

/***************************************************************************
 ** FUNCTION:  MLAppIconAttr::vDeAllocateMsg
 ***************************************************************************/
t_Void MLAppIconAttr::vDeAllocateMsg()
{
   RELEASE_MEM(prAppIconAttr);
}


/***************************************************************************
 ** FUNCTION:  MLAppIconData::MLAppIconData
 ***************************************************************************/
MLAppIconData::MLAppIconData():u32DevID(0),u32Size(0),pszAppIconUrl(NULL),pu8BinaryData(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAppIconData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLAppIconData, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  MLAppIconData::vAllocateMsg
 ***************************************************************************/
t_Void MLAppIconData::vAllocateMsg()
{
   pszAppIconUrl = new t_String;
   SPI_NORMAL_ASSERT(NULL == pszAppIconUrl);
}

/***************************************************************************
 ** FUNCTION:  MLAppIconData::vDeAllocateMsg
 ***************************************************************************/
t_Void MLAppIconData::vDeAllocateMsg()
{
   RELEASE_MEM(pszAppIconUrl);
}

/***************************************************************************
 ** FUNCTION:  MLDeviceEventSubscription::MLDeviceEventSubscription
 ***************************************************************************/
MLDeviceEventSubscription::MLDeviceEventSubscription() :
         bResult(false), enEventSub(e8SUBSCRIBE)
{
   vAllocateMsg();
}
/***************************************************************************
** FUNCTION:  MLDeviceEventSubscription::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLDeviceEventSubscription, spi_tclMLVncDiscovererDispatcher)


/***************************************************************************
 ** FUNCTION:  MLSetClientProfile::MLSetClientProfile
 ***************************************************************************/
MLSetClientProfile::MLSetClientProfile():bResult(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLSetClientProfile::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLSetClientProfile, spi_tclMLVncDiscovererDispatcher)


/***************************************************************************
 ** FUNCTION:  MLSetUPnPPublicKeyResp::MLSetUPnPPublicKeyResp
 ***************************************************************************/
MLSetUPnPPublicKeyResp::MLSetUPnPPublicKeyResp():m_bXMLValidationSucceeded(false)
{
   vAllocateMsg();
}
/***************************************************************************
** FUNCTION:  MLSetUPnPPublicKeyResp::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLSetUPnPPublicKeyResp, spi_tclMLVncDiscovererDispatcher)


/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::spi_tclMLVncDiscovererDispatcher
 ***************************************************************************/
spi_tclMLVncDiscovererDispatcher::spi_tclMLVncDiscovererDispatcher()
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::spi_tclMLVncDiscovererDispatcher()"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::~spi_tclMLVncDiscovererDispatcher
 ***************************************************************************/
spi_tclMLVncDiscovererDispatcher::~spi_tclMLVncDiscovererDispatcher()
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::~spi_tclMLVncDiscovererDispatcher()"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLDeviceInfoMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLDeviceInfoMsg* poDeviceInfoMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLDeviceInfoMsg* poDeviceInfoMsg)"));
   if ((NULL != poDeviceInfoMsg) && (NULL != poDeviceInfoMsg->prDeviceInfo))
   {
      t_U32 u32DeviceHandle = poDeviceInfoMsg->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostDeviceInfo(u32DeviceHandle, *(poDeviceInfoMsg->prDeviceInfo)));
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLDeviceDisconnMsg* poDeviceDisconnMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLDeviceDisconnMsg* poDeviceDisconnMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLDeviceDisconnMsg* poDeviceDisconnMsg)"));
   if (NULL != poDeviceDisconnMsg)
   {
      t_U32 u32DeviceHandle = poDeviceDisconnMsg->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostDeviceDisconnected(u32DeviceHandle));
   } // if (NULL != poDeviceDisconnMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLBTAddrMsg* poBTAddrMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLBTAddrMsg* poBTAddrMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLBTAddrMsg* poBTAddrMsg)"));
   if ((NULL != poBTAddrMsg) && (NULL != poBTAddrMsg->pszBTAddr))
   {
      trUserContext rUserContext;
      poBTAddrMsg->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poBTAddrMsg->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostBTAddr(rUserContext,u32DeviceHandle, (poBTAddrMsg->pszBTAddr)->c_str()));
   } // if (NULL != poBTAddrMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppStatusMsg* poAppStatusMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLAppStatusMsg* poAppStatusMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppStatusMsg* poAppStatusMsg)"));
   if (NULL != poAppStatusMsg)
   {
      trUserContext rUserContext;
      poAppStatusMsg->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poAppStatusMsg->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostAppStatus(rUserContext,u32DeviceHandle, poAppStatusMsg->u32AppID, poAppStatusMsg->enAppStatus));
   } // if (NULL != poAppStatusMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAllAppStatusMsg* poAllAppStatusMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLAllAppStatusMsg* poAllAppStatusMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAllAppStatusMsg* poAllAppStatusMsg)"));
   if((NULL != poAllAppStatusMsg) && (NULL != poAllAppStatusMsg->prMapAllStatuses))
   {
      trUserContext rUserContext;
      poAllAppStatusMsg->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poAllAppStatusMsg->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostAllAppsStatuses(rUserContext, u32DeviceHandle, *(poAllAppStatusMsg->prMapAllStatuses)));
   } //  if (NULL != poAllAppStatusMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAppListMsg* poAppListMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLAppListMsg* poAppListMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppListMsg* poAppListMsg)"));
   if ((NULL != poAppListMsg) && (NULL != poAppListMsg->pvecszAppIds))
   {
      trUserContext rUserContext;
      poAppListMsg->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poAppListMsg->u32GetDeviceHandle();

      if(false == poAppListMsg->bCertAppList)
      {
         CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
            e16DISCOVERER_REGID,
            vPostAppList(rUserContext, u32DeviceHandle, *(poAppListMsg->pvecszAppIds)));
      }//if(false == poAppListMsg->bCertAppList)
      else
      {
         CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
            e16DISCOVERER_REGID,
            vPostCertAppList(rUserContext, u32DeviceHandle, *(poAppListMsg->pvecszAppIds)));
      }
   } // if (NULL != poAppListMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLProtocolIDMsg* poProtocolIDMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLProtocolIDMsg* poProtocolIDMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLProtocolIDMsg* poProtocolIDMsg)"));
   if ((NULL != poProtocolIDMsg) && (NULL != poProtocolIDMsg->pszProtocolID))
   {
      trUserContext rUserContext;
      poProtocolIDMsg->vGetUserContext(rUserContext);
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostAppProtcolID(rUserContext,
                        poProtocolIDMsg->u32GetDeviceHandle(),
                        poProtocolIDMsg->u32AppID,
                        *(poProtocolIDMsg->pszProtocolID),
                        poProtocolIDMsg->bIsLastApp));
   } // if (NULL != poProtocolIDMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLLaunchAppStatusMsg* poLaunchAppStatusMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLLaunchAppStatusMsg* poLaunchAppStatusMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLLaunchAppStatusMsg* poLaunchAppStatusMsg)"));
   if (NULL != poLaunchAppStatusMsg)
   {
      trUserContext rUserContext;
      poLaunchAppStatusMsg->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poLaunchAppStatusMsg->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostLaunchAppStatus(rUserContext,u32DeviceHandle,
                        poLaunchAppStatusMsg->u32AppID,
                        poLaunchAppStatusMsg->bLaunchAppStatus));
   } // if (NULL != poLaunchAppStatusMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLTerminateAppStatusMsg* poTerminateAppStatusMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLTerminateAppStatusMsg* poTerminateAppStatusMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLTerminateAppStatusMsg* poTerminateAppStatusMsg)"));
   if (NULL != poTerminateAppStatusMsg)
   {
      trUserContext rUserContext;
      poTerminateAppStatusMsg->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poTerminateAppStatusMsg->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostTerminateAppStatus(rUserContext,u32DeviceHandle,
                        poTerminateAppStatusMsg->u32AppID,
                        poTerminateAppStatusMsg->bTerminateAppStatus));
   } // if (NULL != poTerminateAppStatusMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAppInfoMsg* poAppInfoMsg)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLAppInfoMsg* poAppInfoMsg)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppInfoMsg* poAppInfoMsg)"));
   if ((NULL != poAppInfoMsg) && (NULL != poAppInfoMsg->prAppInfo))
   {
      trUserContext rUserContext;
      poAppInfoMsg->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poAppInfoMsg->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostAppInfo(rUserContext,u32DeviceHandle,
                        poAppInfoMsg->u32AppID,
                        *(poAppInfoMsg->prAppInfo)));
   } //  if (NULL != poAppInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAppIconAttr* poAppIconAttr)
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(
         MLAppIconAttr* poAppIconAttr)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppIconAttr* poAppIconAttr)"));
   if ((NULL != poAppIconAttr) && (NULL != poAppIconAttr->prAppIconAttr))
   {
      trUserContext rUserContext;
      poAppIconAttr->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poAppIconAttr->u32GetDeviceHandle();
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
               e16DISCOVERER_REGID,
               vPostAppIconAttr(rUserContext, u32DeviceHandle,
                        poAppIconAttr->u32AppID,
                        *(poAppIconAttr->prAppIconAttr)));
   } //  if (NULL != poAppIconAttr)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppIconData* poAppIconData)
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppIconData* poAppIconData)"));
   if((NULL != poAppIconData) && (NULL != poAppIconData->pszAppIconUrl))
   {
      trUserContext rUserContext;
      poAppIconData->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poAppIconData->u32GetDeviceHandle();

      ETG_TRACE_USR2(("[PARAM]:MLAppIconData:szAppIconUrl-%s",
         poAppIconData->pszAppIconUrl->c_str()));

       CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
                e16DISCOVERER_REGID,
                vPostAppIconData(rUserContext,
                      u32DeviceHandle,
                      *(poAppIconData->pszAppIconUrl),
                      poAppIconData->pu8BinaryData,
                      poAppIconData->u32Size));
       RELEASE_ARRAY_MEM(poAppIconData->pu8BinaryData);
   }//if(NULL != poAppIconData)

}

/***************************************************************************
** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLDeviceEventSubscription* poDevEventSubscription) const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLDeviceEventSubscription* poDevEventSubscription)"));
   if(NULL != poDevEventSubscription)
   {
      trUserContext rUsrCntxt;
      poDevEventSubscription->vGetUserContext(rUsrCntxt);
      t_U32 u32DeviceHandle = poDevEventSubscription->u32GetDeviceHandle();

      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
         e16DISCOVERER_REGID,
         vPostDevEventSubscriptionStatus(rUsrCntxt,
         u32DeviceHandle,
         poDevEventSubscription->enEventSub,
         poDevEventSubscription->bResult));

   }//if(NULL != poDevEventSubscription)
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLSetClientProfile* poSetClntProfile) const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLSetClientProfile* poSetClntProfile)"));
   if(NULL != poSetClntProfile)
   {
      trUserContext rUsrCntxt;
      poSetClntProfile->vGetUserContext(rUsrCntxt);
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
         e16DISCOVERER_REGID,
         vPostSetClientProfileResult(rUsrCntxt,
         poSetClntProfile->u32GetDeviceHandle(),
         poSetClntProfile->bResult));

   }//if(NULL != poDevEventSubscription)
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppName* poMLAppName)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLAppName* poMLAppName)"));
   if( (NULL != poMLAppName) && (NULL != poMLAppName->m_pszAppName))
   {
      t_U32 u32DeviceHandle = poMLAppName->u32GetDeviceHandle();
      trUserContext rUsrCntxt;
      poMLAppName->vGetUserContext(rUsrCntxt);
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
         e16DISCOVERER_REGID,
         vPostAppName(u32DeviceHandle,
         poMLAppName->m_u32AppHandle,
         (poMLAppName->m_pszAppName)->c_str(),
         rUsrCntxt));
   }
}


/***************************************************************************
** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLSetUPnPPublicKeyResp* 
                                                             poMLSetUPnPPublicKeyResp)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLSetUPnPPublicKeyResp*  poMLSetUPnPPublicKeyResp)"));
   if(NULL != poMLSetUPnPPublicKeyResp)
   {
      t_U32 u32DeviceHandle = poMLSetUPnPPublicKeyResp->u32GetDeviceHandle();
      trUserContext rUsrCntxt;
      poMLSetUPnPPublicKeyResp->vGetUserContext(rUsrCntxt);
      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
         e16DISCOVERER_REGID,
         vPostSetUPnPPublicKeyResp(u32DeviceHandle,
         poMLSetUPnPPublicKeyResp->m_bXMLValidationSucceeded,
         rUsrCntxt));
   }
}

/***************************************************************************
 ** FUNCTION:  MLKeyIconData::MLKeyIconData
 ***************************************************************************/
MLKeyIconData::MLKeyIconData():u32DevID(0),u32Size(0),pu8BinaryData(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MLAppIconData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MLKeyIconData, spi_tclMLVncDiscovererDispatcher)

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
 ***************************************************************************/
t_Void spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLKeyIconData* poKeyIconData)const
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg(MLKeyIconData* poKeyIconData)"));
   if( NULL != poKeyIconData )
   {
      trUserContext rUserContext;
      poKeyIconData->vGetUserContext(rUserContext);
      t_U32 u32DeviceHandle = poKeyIconData->u32GetDeviceHandle();

      CALL_REG_OBJECTS(spi_tclMLVncRespDiscoverer,
         e16DISCOVERER_REGID,
         vPostKeyIconData(rUserContext,
         u32DeviceHandle,
         poKeyIconData->pu8BinaryData,
         poKeyIconData->u32Size));

       RELEASE_ARRAY_MEM(poKeyIconData->pu8BinaryData);
   }

}
