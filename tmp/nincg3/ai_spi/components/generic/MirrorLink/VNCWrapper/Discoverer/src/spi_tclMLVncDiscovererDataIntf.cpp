
/***********************************************************************/
/*!
* \file  spi_tclMLVncDiscovererDataIntf.cpp
* \brief ML Discoverer Data Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    ML Discoverer Data Interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include "BaseTypes.h"
#include "SPITypes.h"
#include "spi_tclMLVncDiscoverer.h"
#include "spi_tclMLVncDiscovererDataIntf.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncDiscovererDataIntf.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclMLVncDiscovererDataIntf::spi_tclMLVncDiscovererDataIntf()
***************************************************************************/
spi_tclMLVncDiscovererDataIntf::spi_tclMLVncDiscovererDataIntf()
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscovererDataIntf::spi_tclMLVncDiscovererDataIntf()"));

   //Initialize device handles structure to null values
   m_rDeviceHandles.vInitialize();
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncDiscovererDataIntf::~spi_tclMLVncDiscovererDataIntf()
***************************************************************************/
spi_tclMLVncDiscovererDataIntf::~spi_tclMLVncDiscovererDataIntf()
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscovererDataIntf::~spi_tclMLVncDiscovererDataIntf()"));

   //Initialize device handles structure to null values
   m_rDeviceHandles.vInitialize();

}


/***************************************************************************
** FUNCTION:  const trDeviceHandles& spi_tclMLVncDiscoverer::corfrGetDisc..
***************************************************************************/
const trvncDeviceHandles& spi_tclMLVncDiscovererDataIntf::corfrGetDiscHandles(
   t_U32 u32DeviceID)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscovererDataIntf::corfrGetDiscHandles: DeviceID-0x%x",u32DeviceID));

   spi_tclMLVncDiscoverer* poMLDisc = spi_tclMLVncDiscoverer::getInstance();

   //Initialize device handles structure to null values,
   //or else it would send recently retrieved values again
   m_rDeviceHandles.vInitialize();

   //get the Device handles based on the Device Id
   if( NULL != poMLDisc)
   {
      m_rDeviceHandles.poDiscoverySdk = poMLDisc->poGetDiscSdk();
      m_rDeviceHandles.poEntity = poMLDisc->poGetEntity(u32DeviceID) ;
      m_rDeviceHandles.poDiscoverer = poMLDisc->poGetDiscoverer(u32DeviceID);
   } // if( NULL != poMLDisc)

   return m_rDeviceHandles;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncDiscovererDataIntf::vGetDeviceId()
***************************************************************************/
t_Void spi_tclMLVncDiscovererDataIntf::vGetDeviceId(
   VNCDiscoverySDKDiscoverer* poDisc,
   VNCDiscoverySDKEntity* poEntity,
   t_U32& u32DeviceID)const
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscovererDataIntf::vGetDeviceId()"));

   spi_tclMLVncDiscoverer* poMLDisc = spi_tclMLVncDiscoverer::getInstance();

   //get the Unique Id of the device, based on the Device Handles.
   if( NULL != poMLDisc)
   {
      poMLDisc->vGetUniqueDeviceId(poDisc,poEntity,u32DeviceID);
   } // if( NULL != poMLDisc)

}

/***************************************************************************
** FUNCTION:  VNCDiscoverySDK* spi_tclMLVncDiscovererDataIntf::poGetDiscSDK()
***************************************************************************/
VNCDiscoverySDK* spi_tclMLVncDiscovererDataIntf::poGetDiscSDK()const
{
   VNCDiscoverySDK* poDiscSDK = NULL;

   spi_tclMLVncDiscoverer* poMLDisc = spi_tclMLVncDiscoverer::getInstance();
   if( NULL != poMLDisc)
   {
      poDiscSDK = const_cast<VNCDiscoverySDK*>(poMLDisc->poGetDiscSdk());
   }//if( NULL != poMLDisc)

   return poDiscSDK;
}


/***************************************************************************
** FUNCTION:  VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscovererDataIntf::poGetMLDisc()
***************************************************************************/
VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscovererDataIntf::poGetMLDisc()const
{
   VNCDiscoverySDKDiscoverer* poMLDiscoverer = NULL;

   spi_tclMLVncDiscoverer* poMLDisc = spi_tclMLVncDiscoverer::getInstance();
   if( NULL != poMLDisc)
   {
      poMLDiscoverer = const_cast<VNCDiscoverySDKDiscoverer*>(poMLDisc->poGetDiscoverer(enMLDiscoverer));
   }//if( NULL != poMLDisc)

   return poMLDiscoverer;
}

/***************************************************************************
** FUNCTION:  t_String spi_tclMLVncDiscovererDataIntf::szGetAppID()
***************************************************************************/
t_String spi_tclMLVncDiscovererDataIntf::szGetAppID(const t_U32 cou32DevID, const t_U32 cou32AppID)
{
   t_String szAppID;

   spi_tclMLVncDiscoverer* poMLDisc = spi_tclMLVncDiscoverer::getInstance();
   if( NULL != poMLDisc)
   {
      szAppID = poMLDisc->szGetAppId(cou32DevID,cou32AppID);
   }//if( NULL != poMLDisc)

   return szAppID.c_str();
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
