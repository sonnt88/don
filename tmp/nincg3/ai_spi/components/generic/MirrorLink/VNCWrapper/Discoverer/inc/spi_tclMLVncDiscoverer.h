
/***********************************************************************/
/*!
* \file  spi_tclMLVncDiscoverer.h
* \brief Interface to interact with VNC Discovery SDK
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface to interact with VNC Discovery SDK
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version
14.05.2014  | Shiva Kumar Gurija    | Changes in AppList Handling for CTS Test
24.07.2014  | Shiva Kumar Gurija    | Work around to fetch the missing app name
24.04.2014  |  Shiva kumar Gurija   | XML Validation
01.06.2015  |  Shiva kumar Gurija   | Set names to VNC Threads
25.06.2015  | Sameer Chandra        | Added ML XDeviceKey Support for PSA
04.02.2016  | Rachana L Achar       | Logiscope improvements

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLMLVNCDISCOVERER_H_
#define _SPI_TCLMLVNCDISCOVERER_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Lock.h"
#include "GenericSingleton.h"
#include "SPITypes.h"
#include "WrapperTypeDefines.h"
#include "MsgContext.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
//! Holds discoverer handles and discoverer type
struct trDiscHandles
{
    VNCDiscoverySDKDiscoverer* poDiscoverer;
    VNCDiscoverySDKEntity* poEntity;
    tenDiscovererType enDiscType;
    trDiscHandles() :
             poDiscoverer(NULL), poEntity(NULL), enDiscType(enDiscType)
    {

    }
};

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
/*!
 * * \brief forward declaration
 */
class spi_tclMLVncRespDiscoverer;
class Lock;


/****************************************************************************/
/*!
* \class spi_tclMLVncDiscoverer
* \brief Interface to interact with VNC Discovery SDK
*
* It provides an interface to interact with VNC Discovery SDK and responsible
* for discovering devices , Fetching and setting devices and applications
* information.
* It processes requests from Discoverer Input Interface calls and posts the
* to the Discoverer Output Interface.
****************************************************************************/
class spi_tclMLVncDiscoverer:public GenericSingleton<spi_tclMLVncDiscoverer>
{
public:

    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncDiscoverer::~spi_tclMLVncDiscoverer()
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclMLVncDiscoverer()
    * \brief   Destructor
    * \sa      spi_tclMLVncDiscoverer()
    **************************************************************************/
    ~spi_tclMLVncDiscoverer();

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bInitializeDiscoverySDK()
    ***************************************************************************/
    /*!
    * \fn      t_Bool bInitializeDiscoverySDK()
    * \brief   Initializes the VNC Discovery SDK
    * \retval  t_Bool  :  True if the Discovery SDK is initialized, else False
    * \sa      vUninitializeDiscoverySDK()
    **************************************************************************/
    t_Bool bInitializeDiscoverySDK();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vUninitializeDiscoverySDK()
    ***************************************************************************/
    /*!
    * \fn      t_Void vUninitializeDiscoverySDK()
    * \brief   Initializes the VNC Discovery SDK
    * \retval  t_Void
    * \sa      bInitializeDiscoverySDK()
    **************************************************************************/
    t_Void vUninitializeDiscoverySDK()const;

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bInitializeDiscoverer()
    ***************************************************************************/
    /*!
    * \fn      t_Bool bInitializeDiscoverer()
    * \brief   To create all the required discoverers.
    * \retval  t_Bool  : True if the discoverers are created properly.
    * \sa      vUninitializeDiscoverer()
    **************************************************************************/
    t_Bool bInitializeDiscoverer();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vUninitializeDiscoverer()
    ***************************************************************************/
    /*!
    * \fn      t_Void vUninitializeDiscoverer()
    * \brief   To destroy all the created discoverers
    * \retval  t_Void
    * \sa      vInitializeDiscoverer()
    **************************************************************************/
    t_Void vUninitializeDiscoverer();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vStartDiscovery()
    ***************************************************************************/
    /*!
    * \fn      t_Void vStartDeviceDiscovery()
    * \brief   To Start all the created discoverers for device detection
    * \retval  t_Void
    * \sa      vStopDeviceDiscovery()
    **************************************************************************/
    t_Void vStartDeviceDiscovery();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vStopDeviceDiscovery()
    ***************************************************************************/
    /*!
    * \fn      t_Void vStopDeviceDiscovery()
    * \brief   To stop all device discoverers
    * \retval  t_Void
    * \sa      vStartDeviceDiscovery()
    **************************************************************************/
    t_Void vStopDeviceDiscovery();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchDeviceInfo
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchDeviceInfo(t_U32 u32DeviceID)
    * \brief   To Refetch the device info.
    * \param   u32DeviceID  : [IN] Device Id
    * \retval  t_Void
    * \sa      vRetrieveDeviceInfo()
    **************************************************************************/
    t_Void vFetchDeviceInfo(t_U32 u32DeviceID);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppList
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppList(const trUserContext corUserContext, t_U32 u32DeviceID)
    * \brief   To Fetch the list of applications supported by a device.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID  : [IN] Device Id
    * \retval  t_Void
    * \sa      vFetchCertifiedAppList(const trUserContext corUserContext, t_U32 u32DeviceID)
    **************************************************************************/
    t_Void vFetchAppList(const trUserContext corUserContext, t_U32 u32DeviceID);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchCertifiedAppList
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchCertifiedAppList(const trUserContext corUserContext, t_U32 u32DeviceID)
    * \brief   To fetch the list of certified applications supported by a device.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID  : [IN] Device Id
    * \retval  t_Void
    * \sa      vFetchAppList(const trUserContext corUserContext, t_U32 u32DeviceID)
    **************************************************************************/
    t_Void vFetchCertifiedAppList(const trUserContext corUserContext, t_U32 u32DeviceID);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetAppInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppInfo(const trUserContext corUserContext, t_U32 u32DeviceID,t_U32 u32AppId)
    * \brief   To Fetch the requested application's information
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    *          u32AppId    :  [IN] Application Id
    * \retval  t_Void
    * \sa      vFetchAppList(const trUserContext corUserContext, t_U32 u32DeviceID)
    **************************************************************************/
    t_Void vGetAppInfo(const trUserContext corUserContext, t_U32 u32DeviceID,t_U32 u32AppId);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetAppInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppInfo(const t_U32 cou32DeviceHandle,t_U32 u32AppId,
    *          trApplicationInfo& rfrAppInfo)
    * \brief   To Fetch the requested application's information synchronously
    * \param   u32DeviceID :  [IN] Device Id
    * \param   u32AppId    :  [IN] Application Id
    * \param   rfrAppInfo  :  [OUT] ApplicationInfo
    * \retval  t_Void
    * \sa      vFetchAppList(const trUserContext corUserContext, t_U32 u32DeviceID)
    **************************************************************************/
    t_Void vGetAppInfo(const t_U32 cou32DeviceHandle,t_U32 u32AppId,
       trApplicationInfo& rfrAppInfo);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vLaunchApp
    ***************************************************************************/
    /*!
    * \fn      t_Void vLaunchApp(const trUserContext corUserContext, t_U32 u32DeviceID,t_U32 u32AppId)
    * \brief   To post a request to launch an application
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    *          u32AppId    :  [IN] Application Id
    * \retval  t_Void
    * \sa      vTerminateApp()
    **************************************************************************/
    t_Void vLaunchApp(const trUserContext corUserContext, t_U32 u32DeviceID,t_U32 u32AppId);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vTerminateApp(
    ***************************************************************************/
    /*!
    * \fn      t_Void vTerminateApp(const trUserContext corUserContext, t_U32 u32DeviceID, t_U32 u32AppId)
    * \brief   To post a request to terminate an application
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    *          u32AppId    :  [IN] Application Id
    * \retval  t_Void
    * \sa      vLaunchApp())
    **************************************************************************/
    t_Void vTerminateApp(const trUserContext corUserContext, t_U32 u32DeviceID, t_U32 u32AppId);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppStatus
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppStatus(const trUserContext corUserContext, t_U32 u32DeviceID,t_U32 u32AppId )
    * \brief   To Fetch an application status
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    *          u32AppId    :  [IN] Application Id
    * \retval  t_Void
    * \sa      vFetchAllAppsStatuses(const trUserContext corUserContext, t_U32 u32DeviceID)
    **************************************************************************/
    t_Void vFetchAppStatus(const trUserContext corUserContext, t_U32 u32DeviceID,t_U32 u32AppId );

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAllAppsStatuses
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAllAppsStatuses(const trUserContext corUserContext, t_U32 u32DeviceID)
    * \brief   To fetch the statuses of all applicatios supported by the device.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    * \retval  t_Void
    * \sa      vFetchAppStatus(const trUserContext corUserContext, t_U32 u32DeviceID,t_U32 u32AppId )
    **************************************************************************/
    t_Void vFetchAllAppsStatuses(const trUserContext corUserContext, t_U32 u32DeviceID);


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchBTAddr
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchBTAddr(const trUserContext corUserContext, t_U32 u32DeviceID)
    * \brief   To Fetch the Bt Address of a device
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    * \retval  t_Void
    * \sa      t_Void vSetBTAddr(const trUserContext corUserContext, t_U32 u32DeviceID, const t_Char* cszValue)
    **************************************************************************/
    t_Void vFetchBTAddr(const trUserContext corUserContext, t_U32 u32DeviceID);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetBTAddr
    ***************************************************************************/
    /*!
    * \fn      t_Void vSetBTAddr(const trUserContext corUserContext, t_U32 u32DeviceID, const t_Char* pcocValue)
    * \brief   To post a request to set the BT Address of a device
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    *          pcocValue   :  [IN] BT Address to set to the device
    * \retval  t_Void
    * \sa      vFetchBTAddr()
    **************************************************************************/
    t_Void vSetBTAddr(const trUserContext corUserContext, t_U32 u32DeviceID, const t_Char* pcocValue);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppIconAttrBlocking
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppIconAttrBlocking(t_U32 u32DeviceID, t_U32 u32AppId,
    *          t_U32 u32IconId,trIconAttributes& rfrIconAttr)
    * \brief   To Fecth an application Icon's Attributes.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    *          u32AppId    :  [IN] Application Id
    *          u32IconId   :  [IN] Icon Id
    *          rfrIconAttr :  [IN] App Icon Attr
    * \retval  t_Void
    * \sa      vPostAppIconAttr()
    **************************************************************************/
    t_Void vFetchAppIconAttrBlocking(t_U32 u32DeviceID, 
       t_U32 u32AppId, 
       t_U32 u32IconId,
       trIconAttributes& rfrIconAttr);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppIconAttr
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppIconAttr(const trUserContext corUserContext, t_U32 u32DeviceID, 
    *          t_U32 u32AppId, t_U32 u32IconId)
    * \brief   To Fecth an application Icon's Attributes.
    * \param corUserContext : [IN] Context information passed from the caller of this function
    * \param   u32DeviceID :  [IN] Device Id
    *          u32AppId    :  [IN] Application Id
    *          u32IconId   :  [IN] Icon Id
    * \retval  t_Void
    * \sa      vPostAppIconAttr()
    **************************************************************************/
    t_Void vFetchAppIconAttr(const trUserContext corUserContext, 
       t_U32 u32DeviceID, 
       t_U32 u32AppId, 
       t_U32 u32IconId);

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bSetAppIconAttr
    ***************************************************************************/
    /*!
    * \fn      t_Bool bSetAppIconAttr(const trUserContext corUserContext, t_U32 cou32DeviceHandle, 
    *                    t_U32 u32AppId,t_U32 u32IconId, const trIconAttributes& corfrIconAttr)
    * \brief   To post a request to set the attributes of an application's Icon.
    * \param   cou32DeviceHandle   :  [IN] Device Id
    *          u32AppId      :  [IN] Application Id
    *          u32IconId     :  [IN] Icon Id
    *          corfrIconAttr :  [IN] reference to Icon Attributes structure
    * \retval  t_Bool
    * \sa      vFetchAppIconAttr()
    **************************************************************************/
    t_Bool bSetAppIconAttr(
      const t_U32 cou32DeviceHandle,
      t_U32 u32AppId,
      t_U32 u32IconId,
      const trIconAttributes& corfrIconAttr);

    /***************************************************************************
    ** FUNCTION:  VNCDiscoverySDKEntity* spi_tclMLVncDiscoverer::poGetEntity..
    ***************************************************************************/
    /*!
    * \fn      VNCDiscoverySDKEntity* poGetEntity(t_U32 u32DeviceID)
    * \brief    To get the VNCDiscoverySDKEntity* of the device
    * \param   u32DeviceID  :  [IN] Device Id
    * \retval  VNCDiscoverySDKEntity* : Entity Pointer
    **************************************************************************/
    VNCDiscoverySDKEntity* poGetEntity(t_U32 u32DeviceID);

    /***************************************************************************
    ** FUNCTION:  VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscoverer::poGetD...
    ***************************************************************************/
    /*!
    * \fn      VNCDiscoverySDKDiscoverer* poGetDiscoverer(t_U32 u32DeviceID)
    * \brief   To get the VNCDiscoverySDKDiscoverer* of the device
    * \param   u32DeviceID  :  [IN] Device Id
    * \retval  VNCDiscoverySDKDiscoverer* :  Discoverer Pointer
    **************************************************************************/
    VNCDiscoverySDKDiscoverer* poGetDiscoverer(t_U32 u32DeviceID);

    /***************************************************************************
    ** FUNCTION:  VNCDiscoverySDK* spi_tclMLVncDiscoverer::poGetDiscSdk()
    ***************************************************************************/
    /*!
    * \fn      const VNCDiscoverySDK* poGetDiscSdk()
    * \brief   To get the VNCDiscoverySDK*
    * \retval  VNCDiscoverySDK*  : [OUT] Discoverer Pointer
    **************************************************************************/
    const VNCDiscoverySDK* poGetDiscSdk()const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetUniqueDeviceId(VNCDisc...
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetUniqueDeviceId(VNCDiscoverySDKDiscoverer* poDisc,
    *                  VNCDiscoverySDKEntity* poEntity,t_U32& rfu32DeviceID );
    * \brief   To Get the generated unique id of the device.
    * \param   poDisc        :  [IN] Discoverer Pointer
    *          poEntity      :  [IN] Entity Pointer
    *          rfu32DeviceID :  [OUT] Device Id
    * \retval  t_Void
    * \sa      u16GenerateUniqueId(tenDiscovererType enDiscovererType)
    **************************************************************************/
    t_Void vGetUniqueDeviceId(
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity* poEntity,
        t_U32& rfu32DeviceID );

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bLaunchApplication(const 
    ***************************************************************************/
    /*!
    * \fn      t_Bool bLaunchApplication(const trUserContext corUserContext,
    *          , const t_U32 cou32DeviceHandle,t_U32 u32AppId)
    * \brief   To Launch an Application synchronously.
    * \param   cou32DeviceHandle  : [IN] Device Id
    * \param   u32AppId     : [IN] Application Id
    * \retval  t_Bool
    * \sa      vTerminateApp(const t_U32 cou32DeviceHandle,t_U32 u32AppId)
    **************************************************************************/
    t_Bool bLaunchApplication(const t_U32 cou32DeviceHandle,t_U32 u32AppId);

    /***************************************************************************
    ** FUNCTION:  VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscoverer::poGetD...
    ***************************************************************************/
    /*!
    * \fn      VNCDiscoverySDKDiscoverer* poGetDiscoverer(
    *                                       tenDiscovererType enDiscType)
    * \brief   To get the pointer to the discoverer based on enum type
    * \param   enDiscType  : [IN] Type of the dicoverer
    * \retval  VNCDiscoverySDKDiscoverer* : Discoverer Pointer
    **************************************************************************/
    VNCDiscoverySDKDiscoverer* poGetDiscoverer(tenDiscovererType enDiscType);

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::vFetchDeviceVersion()
    ***************************************************************************/
    /*!
    * \fn      t_Bool vFetchDeviceVersion(const t_U32 cou32DeviceHandle,
    *            trVersionInfo& rfrVersionInfo)
    * \brief   To get the Device ML version Info
    * \param   cou32DeviceHandle  : [IN] Device Id
    * \param   rfrVersionInfo     : [OUT] Device VersionInfo
    * \retval  t_Void
    **************************************************************************/
    t_Void vFetchDeviceVersion(const t_U32 cou32DeviceHandle,
       trVersionInfo& rfrVersionInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetAppIconData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetAppIconData(const t_U32 cou32DeviceHandle,
   *         t_String szAppIconUrl, 
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  cou32DeviceHandle  : [IN] Device Id
   * \param  szAppIconUrl  : [IN] Application Icon data
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval t_Void
   **************************************************************************/
    t_Void vGetAppIconData(const t_U32 cou32DeviceHandle,
       t_String szAppIconUrl, 
       const trUserContext& rfrcUsrCntxt);

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bGetAppIDforProtocol()
    ***************************************************************************/
    /*!
    * \fn    t_Bool bGetAppIDforProtocol
    * \brief  To Get the protocol IDs for applications
    * \param  cou32DeviceID  : [IN] Device Id
    * \param  corfrszProtocolID  : [IN] protocol ID
    * \param  rfru32AppID  : [OUT] AppID for protocol corfrszProtocolID
    * \retval t_Void
    **************************************************************************/
    t_Bool bGetAppIDforProtocol(const t_U32 cou32DeviceID,
             const t_String &corfrszProtocolID,
            t_U32 &rfru32AppID);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSubscribeForDeviceEvents()
    ***************************************************************************/
    /*!
    * \fn    t_Void vSubscribeForDeviceEvents(const t_U32 cou32DeviceHandle,
    *         t_Bool bSubscribe,
    const trUserContext& corfrUsrCntxt)
    * \brief  To register with the device to notify, if there is any change in
    *         the applist or application status after launching/terminating appl.
    * \param  cou32DeviceHandle  : [IN] Device Id
    * \param  bSubscribe         : [IN] Subscribe - True
    *                                   Unsubscribe - False
    * \param  corfrUsrCntxt       : [IN] User Context
    * \retval t_Void
    **************************************************************************/
    t_Void vSubscribeForDeviceEvents(const t_U32 cou32DeviceHandle,
       t_Bool bSubscribe,
       const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDisplayAppcertificationInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevHandle,
   *             const t_U32 cou32AppHandle)
   * \brief   To disaply th eapplication certification info includes
   *           restricted & non restricted list of the application
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppHandle  : [IN] Application Id
   * \retval  t_Void
   **************************************************************************/
     t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevHandle,
       const t_U32 cou32AppHandle);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bSetClientProfile()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bSetClientProfile(const t_U32 cou32DevHandle,
   *             const trClientProfile& corfrClientProfile,
   *             const trUserContext& corfrUsrCntxt)
   * \brief   To set the client profiles
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   corfrClientProfile  : [IN] Client Profile to be set
   * \param   corfrUsrCntxt : [IN] User context
   * \retval  t_Bool
   **************************************************************************/
     t_Bool bSetClientProfile(const t_U32 cou32DevHandle,
        const trClientProfile& corfrClientProfile,
        const trUserContext& corfrUsrCntxt);

     /***************************************************************************
     ** FUNCTION: t_Void spi_tclMLVncDiscoverer::vDisplayAppListXml()
     ***************************************************************************/
     /*!
     * \fn     t_Void vDisplayAppListXml(const t_U32 cou32DevHandle)
     * \brief  Method to retrieve the Applications XML Buffer of the device
     * \param  cou32DevHandle : [IN] Device ID
     * \retval t_Void
     **************************************************************************/
     t_Void vDisplayAppListXml(const t_U32 cou32DevHandle);
	 
   /***************************************************************************
    ** FUNCTION:  t_String spi_tclMLVncDiscoverer::szGetAppID()
    ***************************************************************************/
    /*!
    * \fn      t_String szGetAppID(const t_U32 cou32DevID, 
    *                const t_U32 cou32AppID)
    * \brief   To get the equivalent string application ID for the received
    *                Device ID & Application ID
    * \param   cou32DevID     : [IN] Unique Device ID
    * \param   cou32AppID     : [IN] Uique Application ID
    * \retval  t_String 
    **************************************************************************/
     t_String szGetAppId(const t_U32 cou32DevID,const t_U32 cou32AppID);

    /***************************************************************************
    ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32GetAppId()
    ***************************************************************************/
    /*!
    * \fn      t_U32 u32GetAppId(const t_U32& corfrDevID, 
    *                ,t_String szAppId)
    * \brief   To get the equivalent t_U32 application ID for the received
    *                Device ID & Application ID in t_String format
    * \param   corfrDevID     : [IN] Unique Device ID
    * \param   szAppId        : [IN] String Application ID
    * \retval  t_U32 
    **************************************************************************/
     t_U32 u32GetAppId(const t_U32& corfrdevID,t_String szAppId);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppName()
   ***************************************************************************/
   /*!
   * \fn    t_Void vFetchAppName(t_U32 u32DevHandle,
   *         t_U32 u32AppHandle,const trUserContext& cofrUserCntxt)
   * \brief  To Get the application name
   * \param  u32DevHandle  : [IN] Device Id
   * \param  u32AppHandle  : [IN] Application handle
   * \param  cofrUserCntxt : [IN] User Context
   * \retval t_Void
   **************************************************************************/
     t_Void vFetchAppName(t_U32 u32DevHandle, 
        t_U32 u32AppHandle, 
        const trUserContext& cofrUserCntxt);

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclMLVncDiscoverer::szFetchAppName()
   ***************************************************************************/
   /*!
   * \fn    t_String szFetchAppName(t_U32 u32DevHandle,
   *         t_U32 u32AppHandle)
   * \brief  To Get the application name
   * \param  u32DevHandle  : [IN] Device Id
   * \param  u32AppHandle  : [IN] Application handle
   * \retval t_String
   **************************************************************************/
     t_String szFetchAppName(t_U32 u32DevHandle, 
        t_U32 u32AppHandle);

   /***************************************************************************
   ** FUNCTION:  tenAppCategory spi_tclMLVncDiscoverer::enGetAppCategory()
   ***************************************************************************/
   /*!
   * \fn    tenAppCategory enGetAppCategory(t_U32 u32DevHandle,
   *         t_U32 u32AppHandle)
   * \brief  To Get the application category
   * \param  u32DevHandle  : [IN] Device Id
   * \param  u32AppHandle  : [IN] Application handle
   * \retval tenAppCategory
   **************************************************************************/
     tenAppCategory enGetAppCategory(t_U32 u32DevHandle,t_U32 u32AppHandle);


     /***************************************************************************
     ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetUPnPAppPublicKeyForXMLValidation()
     ***************************************************************************/
     /*!
     * \fn    t_Void vSetUPnPAppPublicKeyForXMLValidation(t_U32 u32DevHandle,
     *         t_String szUPnPAppPublicKey,const trUserContext& corfrUsrCntxt)
     * \brief  To Set the UPnP application public key for XML validation
     * \param  u32DevHandle  : [IN] Device Id
     * \param  szUPnPAppPublicKey  : [IN] UPnP Application Public Key
     * \param  corfrUsrCntxt : [IN] User context
     * \retval t_Void
     **************************************************************************/
     t_Void vSetUPnPAppPublicKeyForXMLValidation(t_U32 u32DevHandle,
        t_String szUPnPAppPublicKey,
        const trUserContext& corfrUsrCntxt);

     /***************************************************************************
     ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppUri()
     ***************************************************************************/
     /*!
     * \fn    t_Void vFetchAppUri(const t_U32 cou32DevID,
     *         const t_U32 cou32AppID,t_String& rfszAppUri,t_String& rfszProtocolID)
     * \brief  Method to retrieve the URL of an application
     * \param  cou32DevID  : [IN] Device Id
     * \param  cou32AppID  : [IN] Application ID
     * \param  rfszAppUri  : [OUT] App URI
     * \param  rfszProtocolID : [OUT] App Protocol ID
     * \retval t_Void
     **************************************************************************/
     t_Void vFetchAppUri(const t_U32 cou32DevID, 
        const t_U32 cou32AppID, 
        t_String& rfszAppUri,
        t_String& rfszProtocolID);

     /***************************************************************************
     ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bRequestMirrorlinkSwitch()
     ***************************************************************************/
     /*!
     * \fn    t_Bool bRequestMirrorlinkSwitch
     * \brief  requests device to switch to mirrorlink mode
     * \param  cou32DeviceHandle  : [IN] Device Id
     * \retval t_Bool : returns true if ML command is successfully sent to the device
     **************************************************************************/
     t_Bool bRequestMirrorlinkSwitch(const t_U32 cou32DevID);

     /***************************************************************************
     ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bisDeviceInMirrorlinkMode()
     ***************************************************************************/
     /*!
     * \fn    t_Bool bisDeviceInMirrorlinkMode
     * \brief  checks if device is in mirrorlink mode
     * \param  cou32DeviceHandle  : [IN] Device Id
     * \retval t_Bool : returns true if device is in mirrorlink mode
     **************************************************************************/
     t_Bool bisDeviceInMirrorlinkMode(const t_U32 cou32DevID);

     /***************************************************************************
     ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetKeyIconData()
     ***************************************************************************/
     /*!
     * \fn    t_Void vGetKeyIconData(const t_U32 cou32DeviceHandle,
     *         t_String szKeyIconUrl,
     *         const trUserContext& rfrcUsrCntxt)
     * \brief  To Get the application icon data
     * \param  cou32DeviceHandle  : [IN] Device Id
     * \param  szKeyIconUrl  : [IN] Key Icon data
     * \param  rfrcUsrCntxt  : [IN] User Context
     * \retval t_Void
     **************************************************************************/
      t_Void vGetKeyIconData(const t_U32 cou32DeviceHandle,
         t_String szKeyIconUrl,
         const trUserContext& rfrcUsrCntxt);
		 
     /***************************************************************************
     ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetXDeviceKeyInfo()
     ***************************************************************************/
     /*!
     * \fn    t_Void vGetKeyIconData(const t_U32 cou32DeviceHandle,
     *         t_String szKeyIconUrl,
     *         const trUserContext& rfrcUsrCntxt)
     * \brief  To Get the application icon data
     * \param  cou32DeviceHandle  : [IN] Device Id
     * \param  szKeyIconUrl  : [IN] Key Icon data
     * \param  rfrcUsrCntxt  : [IN] User Context
     * \retval t_Void
     **************************************************************************/
      t_Void vGetXDeviceKeyInfo(const t_U32 u32DeviceID,
            t_U16& u16NumXDevices,std::vector<trXDeviceKeyDetails> &vecrXDeviceKeyDetails);

    /*!
    * \brief   Generic Singleton class
    */
    friend class GenericSingleton<spi_tclMLVncDiscoverer>;

private:

    /***************************************************************************
    ** FUNCTION:  spi_tclMLVncDiscoverer::spi_tclMLVncDiscoverer()
    ***************************************************************************/
    /*!
    * \fn      spi_tclMLVncDiscoverer()
    * \brief   Default Constructor
    * \sa      ~spi_tclMLVncDiscoverer()
    **************************************************************************/
    spi_tclMLVncDiscoverer();

    /***************************************************************************
    ** FUNCTION:  t_S32 spi_tclMLVncDiscoverer::s32GetDiscovererStatus(tenDis...
    ***************************************************************************/
    /*!
    * \fn      t_S32 s32GetDiscovererStatus(tenDiscovererType enDiscType)
    * \brief   To get the status of the discoverer
    * \param   enDiscType   : [IN] Type of the discoverer
    * \retval  t_S32 :  Discoverer Status
    **************************************************************************/
    t_S32 s32GetDiscovererStatus(tenDiscovererType enDiscType);

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bCreateDiscoverer(t_Char*..
    ***************************************************************************/
    /*!
    * \fn      t_Bool bCreateDiscoverer(t_Char* pcDiscType,
    *                                   VNCDiscoverySDKDiscoverer* poDisc)
    * \brief   Creates Discoverer
    * \param   pcDiscType   : [IN] Type of the Discoverer
    *          poDisc       : [IN] Pointer to discoverer
    * \retval  t_Bool  : True if the Discoverer is created successfully 
    *                    or it is already created, else False
    **************************************************************************/
    t_Bool bCreateDiscoverer(t_Char* pcDiscType,VNCDiscoverySDKDiscoverer* poDisc, 
                                tenDiscovererType enDiscType);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscovererStartedCallback()
    ***************************************************************************/
    /*!
    * \fn      t_Void vDiscovererStartedCallback(t_Void *poSDKContext,
    *          VNCDiscoverySDKDiscoverer* poDisc, MLDiscoveryError s32Error)
    * \brief   Callback to recieve Discoverer started callback update
    *          from the VNC Discovery SDK
    * \param   poSDKContext : [IN] Context pointer of the discoverer
    *          poDisc       : [IN] Pointer to discoverer
    *          s32Error     : [IN] Error code, if any
    * \retval  t_Void
    * \sa      vDiscovererStoppedCallback()
    **************************************************************************/
    static t_Void vDiscovererStartedCallback(
        t_Void *poSDKContext,
        VNCDiscoverySDKDiscoverer* poDisc,
        MLDiscoveryError s32Error);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscovererStoppedCallback()
    ***************************************************************************/
    /*!
    * \fn      static t_Void vDiscovererStoppedCallback(t_Void *poSDKContext,
    *          VNCDiscoverySDKDiscoverer* poDisc,MLDiscoveryError s32Error)
    * \brief   Callback to recieve Discoverer stopped callback update
    *          from the VNC Discovery SDK
    * \param   poSDKContext : [IN] Context pointer of the discoverer
    *          poDisc       : [IN] Pointer to discoverer
    *          s32Error     : [IN] Error code, if any
    * \retval  t_Void
    * \sa      vDiscovererStartedCallback()
    **************************************************************************/
    static t_Void vDiscovererStoppedCallback(
        t_Void *poSDKContext,
        VNCDiscoverySDKDiscoverer* poDisc,
        MLDiscoveryError s32Error);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vEntityAppearedCallback()
    ***************************************************************************/
    /*!
    * \fn      static t_Void vEntityAppearedCallback(t_Void *poSDKContext,
    *          VNCDiscoverySDKDiscoverer* poDiscoverer,VNCDiscoverySDKEntity *poEntity)
    * \brief   Callback to recieve the update whenever a new device is appeared.
    * \param   poSDKContext : [IN] Context pointer of the discoverer
    *          poDisc       : [IN] Pointer to discoverer
    *          poEntity     : [IN] Pointer to Entity
    * \retval  t_Void
    * \sa      vDiscovererStartedCallback(),vEntityDisappearedCallback()
    **************************************************************************/
    static t_Void vEntityAppearedCallback(
        t_Void *poSDKContext,
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity* poEntity);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vEntityDisappearedCallback()
    ***************************************************************************/
    /*!
    * \fn      static t_Void vEntityDisappearedCallback(t_Void *poSDKContext,
    *          VNCDiscoverySDKDiscoverer* poDiscoverer,VNCDiscoverySDKEntity *poEntity)
    * \brief   Callback to recieve an update, if any device is disappeared.
    * \param   poSDKContext : [IN] Context pointer of the discoverer
    *          poDisc       : [IN] Pointer to discoverer
    *          poEntity     : [IN] Pointer to Entity
    * \retval  t_Void
    * \sa      vEntityAppearedCallback()
    **************************************************************************/
    static t_Void vEntityDisappearedCallback(
        t_Void *poSDKContext,
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity *poEntity);


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vEntityDisappearedCallback()
    ***************************************************************************/
    /*!
    * \fn      static t_Void vEntityChangedCallback(t_Void *pSDKContext,
    *          VNCDiscoverySDKDiscoverer *poDisc,VNCDiscoverySDKEntity *poEntity,
    *          const t_Char *pcocChangeDesc)
    * \brief   Callback to recieve an update, update whenever there is a change in
    *          the applist or appstatus or application notofications
    * \param   poSDKContext  : [IN] Context pointer of the discoverer
    *          poDisc        : [IN] Pointer to discoverer
    *          poEntity      : [IN] Pointer to Entity
    *          pcocChangeDesc: [IN] Change description
    * \retval  t_Void
    * \sa      vEntityAppearedCallback()
    **************************************************************************/
    static t_Void vEntityChangedCallback(
        t_Void *pSDKContext,
        VNCDiscoverySDKDiscoverer *poDisc,
        VNCDiscoverySDKEntity *poEntity,
        const t_Char *pcocChangeDesc);


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscLogCb()
    ***************************************************************************/
    /*!
    * \fn      static t_Void vDiscLogCb(t_Void *pSDKContext,
    *          t_S32 s32Severity, const t_Char *pcocDisc, const t_Char *pcocText)
    * \brief   Callback to receive the Discovery SDK logs
    * \param   poSDKContext  : [IN] Context pointer of the discoverer
    *          s32Severity   : [IN] Severity of the trace
    *          pcocDisc      : [IN] Discoverer ptr
    *          pcocText      : [IN] Text - description
    * \retval  t_Void
    * \sa      vEntityAppearedCallback()
    **************************************************************************/
    static t_Void vDiscLogCb(t_Void *pSDKContext,
       t_S32 s32Severity, const t_Char *pcocDisc, const t_Char *pcocText);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vPostEntityValueCallback()
    ***************************************************************************/
    /*!
    * \fn      static t_Void vPostEntityValueCallback(t_Void *pSDKContext,
    *               VNCDiscoverySDKRequestId s32RequestId,
    *               VNCDiscoverySDKDiscoverer *poDisc,
    *               VNCDiscoverySDKEntity *poEntity,t_Char* pcocKey,
    *               t_Char* pcocValue,MLDiscoveryError s32Error)
    * \brief   Callback to recieve  responses to the vPostEntityValue()
    *          asynchronous calls.
    * \param   poSDKContext :  [IN] Context pointer of the discoverer
    *          s32RequestId :  [IN] Request Id given by the SDK
    *          poDisc       :  [IN] Pointer to discoverer
    *          poEntity     :  [IN] Pointer to Entity
    *          pcocKey      :  [IN] Key value
    *          pcocValue    :  [IN] Value of the Key
    *          s32Error     :  [IN] Error, if any
    * \retval  t_Void
    * \sa      vPostEntityValueBlocking(),s32PostEntityValue()
    **************************************************************************/
    static t_Void vPostEntityValueCallback(
        t_Void *pSDKContext,
        VNCDiscoverySDKRequestId s32RequestId,
        VNCDiscoverySDKDiscoverer *poDisc,
        VNCDiscoverySDKEntity *poEntity,
        t_Char* pcocKey,
        t_Char* pcocValue,
        MLDiscoveryError s32Error);


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchEntityValueCallback()
    ***************************************************************************/
    /*!
    * \fn      static t_Void vFetchEntityValueCallback(t_Void *pSDKContext,
    *             VNCDiscoverySDKRequestId s32RequestId,
    *             VNCDiscoverySDKDiscoverer *poDisc,
    *             VNCDiscoverySDKEntity *poEntity,t_Char *pcKey,
    *             MLDiscoveryError s32Error,t_Char *pcValue)
    *
    * \brief   Callback to recieve the requested values using vFetchEntityValue()
    *          Asynchronous calls.
    * \param   poSDKContext :  [IN] Context pointer of the discoverer
    *          s32RequestId :  [IN] Request Id given by the SDK
    *          poDisc       :  [IN] Pointer to discoverer
    *          poEntity     :  [IN] Pointer to Entity
    *          pcKey        :  [IN] Key value
    *         s32Error     :  [IN] Error, if any
    *          pcValue      :  [IN] Value of the Key
    * \retval  t_Void
    * \sa      bFetchEntityValueBlocking(),s32FetchEntityValue()
    **************************************************************************/
    static t_Void vFetchEntityValueCallback(
        t_Void *pSDKContext,
        VNCDiscoverySDKRequestId s32RequestId,
        VNCDiscoverySDKDiscoverer *poDisc,
        VNCDiscoverySDKEntity *poEntity,
        t_Char* pcKey,
        MLDiscoveryError s32Error,
        t_Char* pcValue);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vChooseDiscovererForDeviceCb()
    ***************************************************************************/
    /**
     * \brief Asks the client application to choose which Discoverer will handle
     *        a device.
     * \param pvSDKContext The SDK context that was set by the client application
     *        for callbacks.
     * \param poDevice The device that should be checked. Ownership stays with the
     *        SDK.
     * \param ppDiscoverersRequestingAccess Information regarding what Discoverers
     *        are requesting access.
     *
     * \see VNCDiscoverySDKOfferDeviceToDiscoverer, VNCDiscoverySDKDevice,
     * VNCDiscovererRequestingAccess, VNCDiscoverySDKCancelDeviceChoice
    **************************************************************************/
    static t_Void
    vChooseDiscovererForDeviceCb(t_Void *pvSDKContext,
                        const VNCDiscoverySDKDevice *poDevice,
                        const VNCDiscovererRequestingAccess * const *ppDiscoverersRequestingAccess);


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vCancelDeviceChoiceCb()
    ***************************************************************************/
    /**
     * \brief Cancels the request to choose which Discoverer will handle a device.
     *
     * \param pvSDKContext The SDK context that was set by the client application
     *        for callbacks.
     * \param poDevice The device for which the request is cancelled.
     *
     * \see VNCDiscoverySDKOfferDeviceToDiscoverer, VNCDiscoverySDKDevice,
     * VNCDiscoverySDKChooseDiscovererForDevice
    **************************************************************************/
    static t_Void  vCancelDeviceChoiceCb(t_Void *pvSDKContext,
                        const VNCDiscoverySDKDevice *poDevice);


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vRetrieveDeviceInfo(tenDis...
    ***************************************************************************/
    /*!
    * \fn      t_Void vRetrieveDeviceInfo(VNCDiscoverySDKDiscoverer* poDisc,
    *          VNCDiscoverySDKEntity *poEntity, trMLDeviceInfo& rfrDeviceInfo);
    * \brief   To retrieve a device specific info
    * \param   poDisc       :  [IN] Pointer to discoverer
    *          poEntity     :  [IN] Pointer to Entity
    *          rfrDeviceInfo:  [OUT] Reference to Device Info structure
    * \retval  t_Void
    * \sa
    **************************************************************************/
    t_Void vRetrieveDeviceInfo(
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity *poEntity, 
        tenDiscovererType enDiscType,
        trMLDeviceInfo& rfrDeviceInfo);

    /***************************************************************************
    ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32GenerateUniqueDeviceId(tenDis...
    ***************************************************************************/
    /*!
    * \fn      t_U32 u32GenerateUniqueDeviceId(t_String szUniqueName)const
    * \brief   To generate an unique id to the device
    * \param   szUniqueName : [IN] Unique name of the device
    * \retval  t_U32      : Unique Device Id
    * \sa      u32GetUniqueDeviceId()
    **************************************************************************/
    t_U32 u32GenerateUniqueDeviceId(t_String szUniqueName)const;

    /***************************************************************************
    ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32GenerateUniqueAppId(tenDis...
    ***************************************************************************/
    /*!
    * \fn      t_U32 u32GenerateUniqueAppId(t_String szUniqueId)const
    * \brief   To generate an unique id to the application
    * \param   szUniqueId : [IN] Unique Id of the application
    * \retval  t_U32      : Unique Application Id
    * \sa      u32GetUniqueDeviceId()
    **************************************************************************/
    t_U32 u32GenerateUniqueAppId(t_String szUniqueId)const;

    /***************************************************************************
    ** FUNCTION:  tenDiscovererType spi_tclMLVncDiscoverer::enGetDiscovererT..
    ***************************************************************************/
    /*!
    * \fn      tenDiscovererType enGetDiscovererType(VNCDiscoverySDKDiscoverer* poDisc)
    * \brief   To retrieve the discoverer type info
    * \param   poDisc     : [IN] Pointer to discoverer
    * \retval  tenDiscovererType :  Type of the Discoverer
    * \sa      enGetDiscovererType(t_U32 u32DeviceID)
    **************************************************************************/
    tenDiscovererType enGetDiscovererType(VNCDiscoverySDKDiscoverer* poDisc);


    /***************************************************************************
    ** FUNCTION:  tenDiscovererType spi_tclMLVncDiscoverer::enGetDiscovererT..
    ***************************************************************************/
    /*!
    * \fn      tenDiscovererType enGetDiscovererType(t_U32 u32DeviceID)
    * \brief   To retrieve the discoverer type info
    * \param   u32DeviceID  : [IN] Device Id
    * \retval  tenDiscovererType : Type of the Discoverer
    * \sa      enGetDiscovererType(VNCDiscoverySDKDiscoverer* poDisc)
    **************************************************************************/
    tenDiscovererType enGetDiscovererType(t_U32 u32DeviceID);

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer:: bFetchEntityValueBlocking(V...
    ***************************************************************************/
    /*!
    * \fn      t_Bool bFetchEntityValueBlocking(VNCDiscoverySDKDiscoverer* poDisc,
    *          VNCDiscoverySDKEntity *poEntity, const t_Char* pcocKey, 
    *          t_Char** ppcVal, VNCDiscoverySDKTimeoutMicroseconds s32TimeOut)
    * \brief   To Fetch a value from the VNC Discovery SDK synchronously
    * \param   poDisc       :  [IN] Pointer to discoverer
    *          poEntity     :  [IN] Pointer to Entity
    *          pcocKey      :  [IN] Key value
    *          ppcVal       :  [IN] Value of the Key
    *          s32TimeOut   :  [IN] Time out - By default No time out.
    * \retval   t_Bool
    * \sa       vFetchEntityValue(),vFetchEntityValueCallback()
    **************************************************************************/
    t_Bool bFetchEntityValueBlocking(
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity *poEntity,
        const t_Char* pcocKey,
        t_Char** ppcVal,
        MLDiscoverySDKTimeoutMicroseconds s32TimeOut= cs32MLDiscoveryInstantCall)const;
		
    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer:: bFetchEntityValueBlocking(V...
    ***************************************************************************/
    /*!
    * \fn      t_Bool bFetchEntityValueBlocking(VNCDiscoverySDKDiscoverer* poDisc,
    *          VNCDiscoverySDKEntity *poEntity,const t_String& corfszKey, 
    *          const t_String& corfszAppId,t_Char** ppcVal,
    *          VNCDiscoverySDKTimeoutMicroseconds s32TimeOut)
    * \brief   To Fetch a value from the VNC Discovery SDK synchronously
    * \param   poDisc       :  [IN] Pointer to discoverer
    *          poEntity     :  [IN] Pointer to Entity
    *          corfszKey    :  [IN] Key value
    *          corfszAppId  :  [IN] App ID string
    *          ppcVal       :  [IN] Value of the Key
    *          s32TimeOut   :  [IN] Time out - By default No time out.
    * \retval   t_Bool
    * \sa       vFetchEntityValue(),vFetchEntityValueCallback()
    **************************************************************************/
    t_Bool bFetchEntityValueBlocking(
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity *poEntity,
        const t_String& corfszKey, 
        const t_String& corfszAppId,
        t_Char** ppcVal,
        MLDiscoverySDKTimeoutMicroseconds s32TimeOut= cs32MLDiscoveryInstantCall)const;

    /***************************************************************************
    ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer:: u32FetchEntityValue(VNCDiscove...
    ***************************************************************************/
    /*!
    * \fn      t_U32 u32FetchEntityValue(VNCDiscoverySDKDiscoverer* poDisc,
    *          VNCDiscoverySDKEntity *poEntity,const t_Char* szKey,
    *          VNCDiscoverySDKTimeoutMicroseconds s32TimeOut)
    * \brief   To Fetch the a value from the VNC Discovery SDK asynchronously
    * \param   poDisc       :  [IN] Pointer to discoverer
    *          poEntity     :  [IN] Pointer to Entity
    *          szKey        :  [IN] Key value
    * \retval  t_S32  : Request Id
    * \sa      bFetchEntityValueBlocking(),vFetchEntityValueCallback()
    **************************************************************************/
    t_U32 u32FetchEntityValue(
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity *poEntity,
        const t_Char* szKey,
        MLDiscoverySDKTimeoutMicroseconds s32TimeOut= cs32MLDiscoveryNoTimeout)const;

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer:: bPostEntityValueBlocking(V...
    ***************************************************************************/
    /*!
    * \fn      t_Bool bPostEntityValueBlocking(VNCDiscoverySDKDiscoverer* poDisc,
    *          VNCDiscoverySDKEntity *poEntity,const t_Char* szKey,t_Char **szValue,
    *          VNCDiscoverySDKTimeoutMicroseconds s32TimeOut)
    * \brief   To Fetch a value from the VNC Discovery SDK synchronously
    * \param   poDisc       :  [IN] Pointer to discoverer
    *          poEntity     :  [IN] Pointer to Entity
    *          szKey        :  [IN] Key value
    *          szValue      :  [IN] Value of the Key
    *          s32TimeOut   :  [IN] Time out - By default No time out.
    * \retval   t_Bool
    * \sa       vPostEntityValue(),vPostEntityValueCallback()
    **************************************************************************/
    t_Bool bPostEntityValueBlocking(
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity *poEntity,
        const t_Char* pcocKey,
        const t_Char* pcocValue,
        MLDiscoverySDKTimeoutMicroseconds s32TimeOut = cs32MLDiscoveryInstantCall)const;

    /***************************************************************************
    ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32PostEntityValue(VNCDiscovery..
    ***************************************************************************/
    /*!
    * \fn      t_U32 u32PostEntityValue(VNCDiscoverySDKDiscoverer* poDisc,
    *          VNCDiscoverySDKEntity *poEntity, const t_Char* pcocKey,
    *          const t_Char* pcocValue,MLDiscoverySDKTimeoutMicroseconds s32TimeOut)
    * \brief   To post an asynchronous request to VNC Discovery SDK. Response will
    *          be recieved in PostEntityValueCallback() asynchronously.
    * \param   poDisc       :  [IN] Pointer to discoverer
    *          poEntity     :  [IN] Pointer to Entity
    *          pcocKey      :  [IN] Key value
    *          pcocValue    :  [IN] Value of the Key
    *          s32TimeOut   :  [IN] Time out - By default No time out.
    * \retval  t_U32  : Request Id
    * \sa      vPostEntityValueBlocking(),vPostEntityValueCallback()
    **************************************************************************/
    t_U32 u32PostEntityValue(
        VNCDiscoverySDKDiscoverer* poDisc,
        VNCDiscoverySDKEntity* poEntity,
        const t_Char* pcocKey,
        const t_Char* pcocValue,
        MLDiscoverySDKTimeoutMicroseconds s32TimeOut= cs32MLDiscoveryNoTimeout)const;

    /***************************************************************************
    ** FUNCTION:  tenTrustLevel spi_tclMLVncDiscoverer::enGetTrustLevel().
    ***************************************************************************/
    /*!
    * \fn      tenTrustLevel enGetTrustLevel(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             const t_String &corfszTrustLevelKey, 
    *             const t_String &corfszAppId)
    * \brief   To Get the enum type of the Trust level .
    * \param   poDisc               :  [IN] Pointer to discoverer
    * \param   poEntity             :  [IN] Pointer to Entity
    * \param   corfszTrustLevelKey  :  [IN] Key value used to fetch
    * \param   corfszAppId          :  [IN] App ID string
    * \retval  tenTrustLevel        : Enum Type of the value
    **************************************************************************/
    tenTrustLevel enGetTrustLevel(
       VNCDiscoverySDKDiscoverer* poDisc, 
       VNCDiscoverySDKEntity* poEntity,
       const t_String &corfszTrustLevelKey, 
       const t_String &corfszAppId);

    /***************************************************************************
    ** FUNCTION:  tenAppDisplayCategory spi_tclMLVncDiscoverer::enGetAppDisplayCa..
    ***************************************************************************/
    /*!
    * \fn      tenAppDisplayCategory enGetAppDisplayCategory(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             const t_Char *pczKey)
    * \brief   To Get the enum type of the Application Display category.
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   pczKey       :  [IN] Key value used to fetch 
    * \retval  tenAppDisplayCategory  : Enum Type of the value
    **************************************************************************/
    tenAppDisplayCategory enGetAppDisplayCategory(
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity,
       const t_Char *pczKey);

    /***************************************************************************
    ** FUNCTION:  tenAppCategory spi_tclMLVncDiscoverer::enGetAppCategory()
    ***************************************************************************/
    /*!
    * \fn      tenAppCategory enGetAppCategory(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             const t_Char *pczKey)
    * \brief   To Get the enum type of the Application category .
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   pczKey       :  [IN] Key value used to fetch 
    * \retval  tenAppDisplayCategory  : Enum Type of the value
    **************************************************************************/
    tenAppCategory enGetAppCategory(
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity,
       const t_Char *pczKey);

    /***************************************************************************
    ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32FetchKeyValue()
    ***************************************************************************/
    /*!
    * \fn      t_U32 u32FetchKeyValue(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             const t_Char *pczKey,
    *             t_S32 s32TimeOut = cs32MLDiscoveryInstantCall)
    * \brief   To Get the int attribute, after the fetching the value 
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   pczKey       :  [IN] Key value used to fetch 
    * \param   s32TimeOut   :  [IN] Timeout
    * \retval  t_U32
    **************************************************************************/
    t_U32 u32FetchKeyValue(
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity,
       const t_Char *pczKey,
       t_S32 s32TimeOut= cs32MLDiscoveryInstantCall);

    /***************************************************************************
    ** FUNCTION:  tenAppAudioCategory spi_tclMLVncDiscoverer::enGetAppAudioCategory()
    ***************************************************************************/
    /*!
    * \fn      tenAppAudioCategory enGetAppAudioCategory(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity, const t_String &corfszAppId,
    *             const t_U32 &corfu32DeviceHandle, t_String szProtocolID)
    * \brief   To Fetch the Application Audio category
    * \param   poDisc       	   :  [IN] Pointer to discoverer
    * \param   poEntity     	   :  [IN] Pointer to Entity
    * \param   corfszAppId  	   :  [IN] App ID string
    * \param   corfu32DeviceHandle :  [IN] Device handle
    * \param   szProtocolID		   :  [IN] Protocol ID for audio application
    * \retval  tenAppAudioCategory
    **************************************************************************/
    tenAppAudioCategory enGetAppAudioCategory(
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity,
       const t_String &corfszAppId,
       const t_U32 &corfu32DeviceHandle,
       t_String szProtocolID);

    /***************************************************************************
    ** FUNCTION:  tenIconMimeType spi_tclMLVncDiscoverer::enGetIconMimeType()
    ***************************************************************************/
    /*!
    * \fn      tenIconMimeType enGetIconMimeType(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             const t_Char *pczKey)
    * \brief   To Fetch the Application Icon MIME Type
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   pczKey       :  [IN] Key value used to fetch 
    * \retval  tenIconMimeType
    **************************************************************************/
    tenIconMimeType enGetIconMimeType(
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity,
       const t_Char *pczKey);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppDisplayInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppDisplayInfo(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             t_String szAppId,
    *             trAppDisplayInfo& rfrAppDispInfo)
    * \brief   To Fetch the Application Video Info
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   szAppId      :  [IN] App ID string
    * \param   rfrAppDispInfo: [OUT] Applicatio n display info
    * \retval  t_Void
    **************************************************************************/    
    t_Void vFetchAppDisplayInfo(
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity,
       t_String szAppId,
       trAppDisplayInfo& rfrAppDispInfo);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppAudioInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppAudioInfo(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             t_String szAppId,
    *             trAppAudioInfo& rfrAppAudioInfo)
    * \brief   To Fetch the Application Audio Info
    * \param   cou32DeviceHandle       :  [IN] Device Handle
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   szAppId      :  [IN] App ID string
    * \param   szProtocolID :  [IN] Protocol ID for audio application
    * \param   rfrAppAudioInfo: [OUT] Application Audio info
    * \retval  t_Void
    **************************************************************************/
    t_Void vFetchAppAudioInfo(const t_U32 cou32DeviceHandle,
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity,
       t_String szAppId,
       t_String szProtocolID,
       trAppAudioInfo& rfrAppAudioInfo);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppRemotingInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppRemotingInfo(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             t_String szAppId,
    *             trAppRemotingInfo& rfrAppRemotingInfo)
    * \brief   To Fetch the Application Remoting Info
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   szAppId      :  [IN] App ID string
    * \param   rfrAppRemotingInfo: [OUT] Application Remoting Info
    * \retval  t_Void
    **************************************************************************/
    t_Void vFetchAppRemotingInfo(
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity,
       t_String szAppId,
       trAppRemotingInfo& rfrAppRemotingInfo);

     /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppProviderInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppProviderInfo(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             t_String szAppId,
    *             t_String& rfszAppProviderName,
    *             t_String& rfszProviderURL,
    *             t_String& rfszAppVariant)
    * \brief   To Fetch the Application Remoting Info
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   szAppId      :  [IN] App ID string
    * \param   rfszAppProviderName: [OUT] Application Provider Name
    * \param   rfszProviderURL : [OUT] Application Provider URL
    * \param   rfszAppVariant  : [OUT] Application Provider Variant
    * \retval  t_Void
    **************************************************************************/
    t_Void vFetchAppProviderInfo(VNCDiscoverySDKDiscoverer* poDisc, 
       VNCDiscoverySDKEntity* poEntity,
       t_String szAppId,
       t_String& rfszAppProviderName,
       t_String& rfszProviderURL,
       t_String& rfszAppVariant);

    /***************************************************************************
    ** FUNCTION:  tenAppStatus spi_tclMLVncDiscoverer::enGetAppStatus()
    ***************************************************************************/
    /*!
    * \fn      tenAppStatus enGetAppStatus(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             const t_Char *pczKey)
    * \brief   To Fetch the Application Status
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   pczKey       :  [IN] Key value used to fetch 
    * \retval  tenAppStatus
    **************************************************************************/
    tenAppStatus enGetAppStatus( 
       VNCDiscoverySDKDiscoverer* poDisc, 
       VNCDiscoverySDKEntity* poEntity,
       const t_Char *pczKey);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppCertInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppCertInfo(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             t_String szAppId,
    *             trApplicationInfo& rfrAppInfo)
    * \brief   To Fetch the Application certification Info
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   szAppId      :  [IN] App id
    * \param   rfrAppInfo   :[OUT] App Info 
    * \retval  t_Void
    **************************************************************************/
    t_Void vFetchAppCertInfo( VNCDiscoverySDKDiscoverer* poDisc, 
       VNCDiscoverySDKEntity* poEntity,
       t_String szAppId,
       trApplicationInfo& rfrAppInfo);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetAppCertEntityList()
    ***************************************************************************/
    /*!
    * \fn      t_Void vGetAppCertEntityList(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             const t_Char *pczKey,
    *             std::vector<t_String>& rfvecCertEntityList)
    * \brief   To Fetch the Restricted or Non Restricted List of an entity
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   pczKey       :  [IN] Key value used to fetch 
    * \param   rfvecCertEntityList:[OUT] Restricted/Non Restricted List
    * \retval  t_Void
    **************************************************************************/
    t_Void vGetAppCertEntityList(
       VNCDiscoverySDKDiscoverer* poDisc, 
       VNCDiscoverySDKEntity* poEntity,
       const t_Char *pczKey,
       std::vector<t_String>& rfvecCertEntityList);


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchProtocolID()
    ***************************************************************************/
    /*!
    * \fn      vFetchProtocolID
    * \brief   Fetches the protocol IDs for a given applist
    * \param   cou32DeviceID: Device Handle
    * \param   pcoczAppList :  Pointer to applist
    * \param   rfrUsrCtxt: user context
    * \retval  t_Void
    **************************************************************************/
    t_Void vFetchProtocolID(const t_U32 cou32DeviceID,const t_Char* pcoczAppList,
             trUserContext &rfrUsrCtxt);


    /***************************************************************************
    ** FUNCTION:  tenAppStatus spi_tclMLVncDiscoverer::enGetAppStatus()
    ***************************************************************************/
    /*!
    * \fn      tenAppStatus enGetAppStatus(
    *             std::map<t_String,t_String>& rfmapAppStatus)
    * \brief   Get the status of the default client profile Id "0"
    * \param   rfmapAppStatus  :  [IN] status of all the profiles supported by the device
    * \retval  tenAppStatus
    **************************************************************************/
    tenAppStatus enGetAppStatus(std::map<t_String,t_String>& rfmapAppStatus) const;


    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vPostAppIconData()
    ***************************************************************************/
    /*!
    * \fn      vPostAppIconData(t_U32 u32DeviceID,t_Char* pcKey,t_Char* pcVal,
    *            trUserContext rUsrCntxt)
    * \brief   To fecth and post the app icon data, once icon download url is fetched
    * \param   u32DeviceID : [IN] Device Handle
    * \param   pcKey       : [IN] Icon Url
    * \param   pcVal       : [IN] Icon download Url
    * \param   rUsrCntxt   : [IN] user context
    * \retval  t_Void
    **************************************************************************/
    t_Void vPostAppIconData(t_U32 u32DeviceID,
       t_Char* pcKey,
       t_Char* pcVal,
       trUserContext rUsrCntxt);

 
    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFreeSdkString()
    ***************************************************************************/
    /*!
    * \fn      vFreeSdkString(t_Char*& czStr)
    * \brief   To free the memory allocated bu VNC SDK, during Fetching entity values
    * \param   czStr : [IN] variable for which memory needs to be deallocated
    * \retval  t_Void
    **************************************************************************/
    inline t_Void vFreeSdkString(t_Char*& czStr);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vProcessAppList()
    ***************************************************************************/
    /*!
    * \fn      t_Void vProcessAppList(const t_U32& corfru32DeviceID,
    *                t_Char* pcVal,t_Bool bCertAppList,const trUserContext& corfrUsrCntxt)
    * \brief   To process the received pointer to application list and update SPI
    * \param   corfru32DeviceID : [IN] Unique Device ID
    * \param   pcVal            : [IN] Pointer to App List
    * \param   bCertAppList     : [IN] To Say whether it is certified app list
    * \param   corfrUsrCntxt    : [IN] User Context
    * \retval  t_Void 
    **************************************************************************/
    t_Void vProcessAppList(const t_U32& corfru32DeviceID,
       t_Char* pcVal,
       t_Bool bCertAppList,
       const trUserContext& corfrUsrCntxt);
 
    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::bAddLicense()
    ***************************************************************************/
    /*!
    * \fn      bAddLicense()
    * \brief   Add RealVNC License for discoveer
    * \retval  t_Bool : true if adding license succeeded otherwise false
    **************************************************************************/
    t_Bool bAddLicense();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::bIsDeviceBlacklisted()
    ***************************************************************************/
    /*!
    * \fn      bIsDeviceBlacklisted
    * \brief   checks if the device is blacklisted
    * \param   s32ProductID    : [IN] Product id of the USB device
    * \param   s32VendorID    : [IN] Vendor ID of the USB device
    * \retval  t_Bool : true if device is blacklisted otherwise false
    **************************************************************************/
    t_Bool bIsDeviceBlacklisted(t_S32 s32ProductID, t_S32 s32VendorID);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vHandleUSBEntity()
    ***************************************************************************/
    /*!
    * \fn      vHandleUSBEntity
    * \brief   handles usb entities
    * \param   poMLVncDisc    : [IN] pointer to spi_tclMLVncDiscoverer
    * \param   poDisc         : [IN] pointer to discovery sdk
    * \param   poEntity       : [IN] pointer to usb entity
    * \retval  none
    **************************************************************************/
    t_Void vHandleUSBEntity(spi_tclMLVncDiscoverer* poMLVncDisc, VNCDiscoverySDKDiscoverer* poDisc,
             VNCDiscoverySDKEntity* poEntity);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vHandleMirrorlinkEntity()
    ***************************************************************************/
    /*!
    * \fn      vHandleMirrorlinkEntity
    * \brief   handles mirrorlink entities
    * \param   poMLVncDisc    : [IN] pointer to spi_tclMLVncDiscoverer
    * \param   poDisc         : [IN] pointer to discovery sdk
    * \param   poEntity       : [IN] pointer to mirrorlink entity
    * \retval  none
    **************************************************************************/
    t_Void vHandleMirrorlinkEntity(spi_tclMLVncDiscoverer* poMLVncDisc,
             VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity* poEntity);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscThreadStartedCb()
    ***************************************************************************/
    /**
     * \brief This is called, when SDK thread is started
     *
     * \param  pvSDKContext The SDK context that was set by the client application
     *         for callbacks.
     *
     * \retval None
    **************************************************************************/
    static t_Void  vDiscThreadStartedCb(t_Void *pvSDKContext);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscThreadEndedCb()
    ***************************************************************************/
    /**
     * \brief This is called, when SDK thread is stopped
     *
     * \param  pvSDKContext - The SDK context that was set by the client application
     *         for callbacks.
     * \param  s32Error - Error code
     * \retval None
    **************************************************************************/
    static t_Void  vDiscThreadEndedCb(t_Void *pvSDKContext,MLDiscoveryError s32Error);
	
    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vPostkeyIconData()
    ***************************************************************************/
    /*!
    * \fn      vPostAppIconData(t_U32 u32DeviceID,t_Char* pcKey,t_Char* pcVal,
    *            trUserContext rUsrCntxt)
    * \brief   To fecth and post the app icon data, once icon download url is fetched
    * \param   u32DeviceID : [IN] Device Handle
    * \param   pcKey       : [IN] Icon Url
    * \param   pcVal       : [IN] Icon download Url
    * \param   rUsrCntxt   : [IN] user context
    * \retval  t_Void
    **************************************************************************/
    t_Void vPostKeyIconData(t_U32 u32DeviceID,
       t_Char* pcKey,
       t_Char* pcVal,
       trUserContext rUsrCntxt);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchXDeviceKeyIconData()
    ***************************************************************************/
    /*!
    * \fn    vFetchXDeviceKeyIconData(VNCDiscoverySDKDiscoverer* poDisc,VNCDiscoverySDKEntity* poEntity,
    *                                 std::vector<trIconAttributes>& rfrtvecKeyIconList,t_U8 u8KeyIndex,
    *                                 t_U8 u8IconCount);
    * \brief   To fetch XDevice key Icon data.
    * \param   poDisc            : [IN] Pointer to SDK discoverer.
    * \param   poEntity          : [IN] Pointer to VNC disc entity
    * \param   rfrtvecKeyIconList       : [IN] Vector for Key Icon list
    * \param   u8KeyIndex        : [IN] Key Index
    * \param   u8IconCount       : [IN] Icon count
    * \retval  t_Void
    **************************************************************************/
    t_Void vFetchXDeviceKeyIconData(VNCDiscoverySDKDiscoverer* poDisc,VNCDiscoverySDKEntity* poEntity,
                                                          std::vector<trIconAttributes>& rfrtvecKeyIconList,t_U8 u8KeyIndex,
                                                          t_U8 u8IconCount);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetIconPreferences()
    ***************************************************************************/
    /*!
    * \fn    vSetIconPreferences(const trClientProfile& corfrClientProfile,
    *        VNCDiscoverySDKDiscoverer* poDisc,
    *        VNCDiscoverySDKEntity* poEntity)
    * \brief   To set the icon prefernces that ML client wanted to receive.
    * \param   corfrClientProfile: [IN] Client Profile to be set
    * \param   poDisc            : [IN] Pointer to SDK discoverer
    * \param   poEntity          : [IN] Pointer to VNC disc entity
    * \retval  t_Void
    **************************************************************************/
    t_Void vSetIconPreferences(const trClientProfile& corfrClientProfile,
           VNCDiscoverySDKDiscoverer* poDisc,
           VNCDiscoverySDKEntity* poEntity);
	   
    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetMLVersion()
    ***************************************************************************/
    /*!
    * \fn    vSetMLVersion(const trClientProfile& corfrClientProfile,
    *        VNCDiscoverySDKDiscoverer* poDisc,
    *        VNCDiscoverySDKEntity* poEntity)
    * \brief   To set ML major and minor versions supported by the client.
    * \param   corfrClientProfile: [IN] Client Profile to be set
    * \param   poDisc            : [IN] Pointer to SDK discoverer
    * \param   poEntity          : [IN] Pointer to VNC disc entity
    * \retval  t_Void
    **************************************************************************/
    t_Void vSetMLVersion(const trClientProfile& corfrClientProfile,
           VNCDiscoverySDKDiscoverer* poDisc,
           VNCDiscoverySDKEntity* poEntity);
	   
    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetNotificationSupport()
    ***************************************************************************/
    /*!
    * \fn    vSetNotificationSupport(const trClientProfile& corfrClientProfile,
    *        VNCDiscoverySDKDiscoverer* poDisc,
    *        VNCDiscoverySDKEntity* poEntity)
    * \brief   To set Notification UI and MaxActions support of the ML client.
    * \param   corfrClientProfile: [IN] Client Profile to be set
    * \param   poDisc            : [IN] Pointer to SDK discoverer
    * \param   poEntity          : [IN] Pointer to VNC disc entity
    * \retval  t_Void
    **************************************************************************/
    t_Void vSetNotificationSupport(const trClientProfile& corfrClientProfile,
           VNCDiscoverySDKDiscoverer* poDisc,
           VNCDiscoverySDKEntity* poEntity);
	   
    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::s32SendClientProfileMsg()
    ***************************************************************************/
    /*!
    * \fn    s32SendClientProfileMsg(const trClientProfile& corfrClientProfile,
    *        VNCDiscoverySDKDiscoverer* poDisc,
    *        VNCDiscoverySDKEntity* poEntity)
    * \brief   To send (commit) a configured client profile message to the service.
    *          Stores the request Id for future reference in callback functions.
    * \param   poDisc            : [IN] Pointer to SDK discoverer
    * \param   poEntity          : [IN] Pointer to VNC disc entity\
    * \param   corfrUsrCntxt     : [IN] User context
    * \retval  Request Id
    **************************************************************************/
    t_S32 s32SendClientProfileMsg(VNCDiscoverySDKDiscoverer* poDisc,
          VNCDiscoverySDKEntity* poEntity,
          const trUserContext& corfrUsrCntxt);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppCertEntityInfo()
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppCertEntityInfo(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity,
    *             const t_String &corfszAppId,t_U8 u8EntityCount,
    *             vecAppCertInfo& rfvecAppCertInfo)
    * \brief   To Fetch the Application certification entity Info
    * \param   poDisc             :  [IN] Pointer to discoverer
    * \param   poEntity           :  [IN] Pointer to Entity
    * \param   corfszAppId        :  [IN] App id
    * \param   u8EntityCount      :  [IN] Number of entities certified the app
    * \param   rfvecAppCertInfo   :  [OUT] App cert info 
    * \retval  t_Void
    **************************************************************************/	
    t_Void vFetchAppCertEntityInfo(VNCDiscoverySDKDiscoverer* poDisc, 
           VNCDiscoverySDKEntity* poEntity, const t_String &corfszAppId, 
           t_U8 u8EntityCount,std::vector<trAppCertInfo>& rfvecAppCertInfo);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppIconDimensions()
    ***************************************************************************/
    /*!
    * \fn      t_Void vFetchAppIconDimensions(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity, const t_String &corfszAppId,
    *             const t_U32 &corfu32IconId, trIconAttributes& rfrIconAttr)
    * \brief   To Fetch the Application certification entity Info
    * \param   poDisc       :  [IN] Pointer to discoverer
    * \param   poEntity     :  [IN] Pointer to Entity
    * \param   corfszAppId  :  [IN] App id
    * \param   rfrIconAttr  :  [OUT] App Icon attributes
    * \retval  t_Void
    **************************************************************************/			   
    t_Void vFetchAppIconDimensions(VNCDiscoverySDKDiscoverer* poDisc,
           VNCDiscoverySDKEntity* poEntity, const t_String &corfszAppId,
           const t_U32 &corfu32IconId, trIconAttributes& rfrIconAttr);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::bFetchRootKeyIconData()
    ***************************************************************************/
    /*!
    * \fn      t_Void bFetchRootKeyIconData(VNCDiscoverySDKDiscoverer* poDisc, 
    *             VNCDiscoverySDKEntity* poEntity, const t_String& corfszKey,
	*       	  t_U8 u8KeyIconIndex, t_U8 u8KeyIndex, t_String& rfszStr)
    * \brief   To Fetch the Application certification entity Info
    * \param   poDisc         :  [IN] Pointer to discoverer
    * \param   poEntity       :  [IN] Pointer to Entity
    * \param   corfszKey      :  [IN] Key value
    * \param   u8KeyIconIndex :  [IN] Key icon index
    * \param   u8KeyIndex     :  [IN] Key index
    * \param   rfszStr        :  [OUT] Key icon data
    * \retval  t_Bool
    **************************************************************************/				
    t_Bool bFetchRootKeyIconData(VNCDiscoverySDKDiscoverer* poDisc,
           VNCDiscoverySDKEntity* poEntity,	const t_Char* corfszKey ,
           t_U8 u8KeyIconIndex, t_U8 u8KeyIndex, t_String& rfszStr);

    /*!
    * \brief   Pointer to VNCDiscovery SDK Instance
    *
    * This will be created and populated during the VNC Discovery SDK.
    * initialization and destroyed during VNC Discovery SDK Uninitialization.
    */
    VNCDiscoverySDKInstance* m_poDiscoverySdkInstance;

    /*!
    * \brief   Pointer to VNCDiscovery SDK
    *
    * This will be populated during the VNC Discovery SDK initialization
    * destroyed during Uninitialization. Required memory needs to be
    * allocated before passing it as a parameter to SDK Initialization
    */
    VNCDiscoverySDK* m_poDiscoverySdk;

    /*!
    * \brief   Counter - used to generate unique id for the devices.
    */
    t_U32 m_u32Counter;

    /*!
    * \brief   Map to maintain the discoverers Info internally
    *
    * Each and every Discoverer has one particular enum value,
    * this will be used during generation of Unique Device Id
    * and fecthing the discoverer based on the Uniqu Device Id.
    */
    std::map<tenDiscovererType,VNCDiscoverySDKDiscoverer*> mapDiscTypes ;

    /*!
    * \brief   Iterator to the Discoverers Info Map
    */
    std::map<tenDiscovererType,VNCDiscoverySDKDiscoverer*>::iterator itMapDiscTypes;

    /*!
    * \brief   Map to maintain entities info
    *
    * This is used to maintain the details of the connected entities
    * with the corresponding Unique Device Id's.
    */
    std::map<t_U32, trDiscHandles> m_mapDevicesInfo;

    /*!
    * \brief   Iterator to the entities info map
    */
    std::map<t_U32,trDiscHandles>::iterator itMapDevicesInfo;

    //! message context utility
    static MsgContext m_oMsgContext;

   //! Lock variable to protect Device info map usage
    Lock m_oMapDevLock;

    //! Lock variable to protect Device applications map usage
    Lock m_oMapAppLock;

    /*!
    * Map to store the string application ID's supported by all the devices
    * Device ID & generated application ID's are used as the Key
    */
    std::map<std::pair<t_U32,t_U32>,t_String> m_mapAppsInfo;

	//! Protocol info for connected devices
    std::map<std::pair<t_U32,t_String>,t_U32> m_mapProtocolInfo;

    //! TODO Workaround: To be removed once the strategy for discovering USB device
    //! is finalized.
    t_U32 m_u32MLCmdDeviceHandle;

    //! Stores the Device mode
    std::map<t_U32, t_Bool> m_mapDeviceModes;

    //! Lock variable to protect Device mode map usage
     Lock m_oMapDevModeLock;

};

#endif // _SPI_TCLMLVNCDISCOVERER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
