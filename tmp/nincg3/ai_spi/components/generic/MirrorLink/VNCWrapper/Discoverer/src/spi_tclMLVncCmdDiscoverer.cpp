

/***********************************************************************/
/*!
* \file  spi_tclMLVncCmdDiscoverer.cpp
* \brief ML Discoverer Input Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    ML Discoverer Input Interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version
24.07.2014  | Shiva Kumar Gurija    | Work around to fetch the missing app name
24.04.2014  |  Shiva kumar Gurija   | XML Validation
25.06.2015  | Sameer Chandra        | Added ML XDeviceKey Support for PSA

\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include "WrapperTypeDefines.h"
#include "BaseTypes.h"
#include "SPITypes.h"
#include "spi_tclMLVncDiscoverer.h"
#include "spi_tclMLVncCmdDiscoverer.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncCmdDiscoverer.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdDiscoverer::spi_tclMLVncCmdDiscoverer()
***************************************************************************/
spi_tclMLVncCmdDiscoverer::spi_tclMLVncCmdDiscoverer()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::spi_tclMLVncCmdDiscoverer()"));
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncCmdDiscoverer::~spi_tclMLVncCmdDiscoverer()
***************************************************************************/
spi_tclMLVncCmdDiscoverer::~spi_tclMLVncCmdDiscoverer()
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::~spi_tclMLVncCmdDiscoverer()"));
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bInitializeDiscoverers()
***************************************************************************/
t_Bool spi_tclMLVncCmdDiscoverer::bInitializeDiscoverers()const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::bInitializeDiscoverers()"));

   t_Bool bRet = false ;

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   //Initialize the Discovery SDK and all the required discoverers
   if(
      (NULL != poMLVncDisc)
      &&
      //Initialize Discovery SDK
      (true == poMLVncDisc->bInitializeDiscoverySDK())
      )
   {
      //Initialize Discoverers
      bRet = poMLVncDisc->bInitializeDiscoverer();
   }  // if(NULL != poMLVncDisc)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vUnInitializeDiscoverers()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vUnInitializeDiscoverers()const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vUnInitializeDiscoverers()"));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   //Uninitialize Discoverers and Discovery SDK.
   if(NULL != poMLVncDisc)
   {
      //Destroy all the discoverers
      poMLVncDisc->vUninitializeDiscoverer();

      //Unintialize the discovery SDK
      poMLVncDisc->vUninitializeDiscoverySDK();
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vStartDiscovery()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vStartDeviceDiscovery()const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vStartDeviceDiscovery()"));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Start all the created discoverers, to start the device detection.
      poMLVncDisc->vStartDeviceDiscovery();
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vStopDeviceDiscovery()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vStopDeviceDiscovery()const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vStopDeviceDiscovery()"));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Stop of all the created discoverers, to stop the device detection
      poMLVncDisc->vStopDeviceDiscovery();
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vRefetchDeviceInfo
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vRefetchDeviceInfo(const t_U32 cou32DeviceHandle)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vRefetchDeviceInfo: DeviceID-0x%x",cou32DeviceHandle));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Fetch the Device Info
      poMLVncDisc->vFetchDeviceInfo(cou32DeviceHandle);
   }  // if(NULL != poMLVncDisc)

}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppList(const trUserContext...)
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetAppList(const trUserContext corUserContext,
                                              const t_U32 cou32DeviceHandle)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetAppList:DeviceID-0x%x ",cou32DeviceHandle));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Fetch the list of application ssupported by the Device
      poMLVncDisc->vFetchAppList(corUserContext, cou32DeviceHandle);
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetCertifiedAppList(const trUserContext...)
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetCertifiedAppList(const trUserContext corUserContext,
                                                       const t_U32 cou32DeviceHandle)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetCertifiedAppList:DeviceID-0x%x ",cou32DeviceHandle));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Fetch the list of Certified applications supported by the device.
      poMLVncDisc->vFetchCertifiedAppList(corUserContext, cou32DeviceHandle);
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppInfo(const trUserContext..
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetAppInfo(const trUserContext corUserContext,
                                              const t_U32 cou32DeviceHandle,
                                              t_U32 u32AppId)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetAppInfo():DeviceID-0x%x",cou32DeviceHandle));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Fetch the Info of the Application
      poMLVncDisc-> vGetAppInfo(corUserContext, cou32DeviceHandle,u32AppId);
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppInfo()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetAppInfo(const t_U32 cou32DeviceHandle,
                                                      const t_U32 cou32AppId,
                                                      trApplicationInfo& corfrAppInfo)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      //Fetch the Info of the Application
      poMLVncDisc-> vGetAppInfo(cou32DeviceHandle,cou32AppId,corfrAppInfo);
   }  // if(NULL != poMLVncDisc)
}
/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vLaunchApplication(const trUserContext..
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vLaunchApplication(const trUserContext corUserContext,
                                                     const t_U32 cou32DeviceHandle,
                                                     t_U32 u32AppId)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vLaunchApplication: DeviceID-0x%x,App - 0x%x",cou32DeviceHandle,u32AppId));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Post the App Id to Discovery SDK to Laucnch Application
      poMLVncDisc->vLaunchApp(corUserContext, cou32DeviceHandle,u32AppId);
   }  // if(NULL != poMLVncDisc)

}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vTerminateApplication((const trUserContext..
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vTerminateApplication(const trUserContext corUserContext,
                                                        const t_U32 cou32DeviceHandle,
                                                        t_U32 u32AppId)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vTerminateApplication:DeviceID-0x%x, App-0x%x",cou32DeviceHandle,u32AppId));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Post the App Id to Discovery SDK to Terminate Application
      poMLVncDisc->vTerminateApp(corUserContext, cou32DeviceHandle,u32AppId);
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppStatus(const trUserContext ...
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetAppStatus(const trUserContext corUserContext,
                                                const t_U32 cou32DeviceHandle,
                                                t_U32 u32AppId )const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetAppStatus:DeviceID-0x%x , App - 0x%x ",cou32DeviceHandle,u32AppId));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Fetch the Application Status
      poMLVncDisc->vFetchAppStatus(corUserContext, cou32DeviceHandle,u32AppId);
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAllAppsStatuses(const trUserContext...
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetAllAppsStatuses(const trUserContext corUserContext,
                                                      const t_U32 cou32DeviceHandle)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetAllAppsStatuses: DeviceID-0x%x",cou32DeviceHandle));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Fetch the statuses of the all applications supported by the device
      poMLVncDisc->vFetchAllAppsStatuses(corUserContext, cou32DeviceHandle);
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetBTAddress(const trUserContext..
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetBTAddress(const trUserContext corUserContext,
                                                const t_U32 cou32DeviceHandle)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetBTAddress: DeviceID-0x%x",cou32DeviceHandle));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Fetch the BT Address of the device
      poMLVncDisc->vFetchBTAddr(corUserContext, cou32DeviceHandle);
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vSetBTAddress(const trUserContext..
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vSetBTAddress(const trUserContext corUserContext,
                                                const t_U32 cou32DeviceHandle,
                                                const t_Char* pcocVal)const
{

   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vSetBTAddress: DeviceID-0x%x",cou32DeviceHandle));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != pcocVal)
   {
      ETG_TRACE_USR2(("[PARAM]:vSetBTAddress:BTAddr - %s",pcocVal));
   }

   if(NULL != poMLVncDisc)
   {
      //Post the value of the BT Address to be set for the device
      poMLVncDisc->vSetBTAddr(corUserContext, cou32DeviceHandle,pcocVal);
   }  // if(NULL != poMLVncDisc)

}
/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppIconAttr(const trUserContext..
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetAppIconAttr(const trUserContext corUserContext,
                                                  const t_U32 cou32DeviceHandle,
                                                  t_U32 u32AppId,
                                                  t_U32 u32IconId)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetAppIconAttr: DeviceID-0x%x,AppId - 0x%x ",cou32DeviceHandle,u32AppId));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Fetch the specified Icon Attributes
      poMLVncDisc->vFetchAppIconAttr( corUserContext, cou32DeviceHandle,u32AppId,u32IconId);
   }  // if(NULL != poMLVncDisc)

}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bSetAppIconAttr(const trUserContext..
***************************************************************************/
t_Bool spi_tclMLVncCmdDiscoverer::bSetAppIconAttr(
      const t_U32 cou32DeviceHandle,
      t_U32 u32AppId,
      t_U32 u32IconId,
      const trIconAttributes& corfrIconAttr) const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vSetAppIconAttr: DeviceID-0x%x,AppId - 0x%x ",cou32DeviceHandle,u32AppId));
   t_Bool bRet = false;

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Set the specified Icon Attributes
      //Extraction of MIMEType based on the enum value will be done Discoverer Core.
      bRet=poMLVncDisc->bSetAppIconAttr(cou32DeviceHandle,u32AppId,u32IconId,corfrIconAttr);
   }  // if(NULL != poMLVncDisc)
   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bLaunchApplication(const
***************************************************************************/
t_Bool spi_tclMLVncCmdDiscoverer::bLaunchApplication(const t_U32 cou32DeviceHandle,
                                                     t_U32 u32AppId)const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::bLaunchApplication:DeviceID-0x%x,AppId - 0x%x",cou32DeviceHandle,u32AppId));

   t_Bool bRet = false;

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      //Set the specified Icon Attributes
      //Extraction of MIMEType based on the enum value will be done Discoverer Core.
      bRet = poMLVncDisc->bLaunchApplication(cou32DeviceHandle,u32AppId);
   }  // if(NULL != poMLVncDisc)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::vGetDeviceVersion()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetDeviceVersion(const t_U32 cou32DeviceHandle,
                                                    trVersionInfo& rfrVersionInfo)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vFetchDeviceVersion(cou32DeviceHandle,rfrVersionInfo);
   }  // if(NULL != poMLVncDisc)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppIconData()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetAppIconData(const t_U32 cou32DeviceHandle,
                                                  t_String szAppIconUrl, 
                                                  const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetAppIconData:DeviceID-0x%x,AppURL - %s"
      ,cou32DeviceHandle,szAppIconUrl.c_str()));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vGetAppIconData(cou32DeviceHandle,szAppIconUrl,rfrcUsrCntxt);
   }  // if(NULL != poMLVncDisc)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bGetAppIDforProtocol()
 ***************************************************************************/
t_Bool spi_tclMLVncCmdDiscoverer::bGetAppIDforProtocol(const t_U32 cou32DeviceID,
         const t_String &corfrszProtocolID,
         t_U32 &rfru32AppID)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance();
   t_Bool bRetVal = false;
   if (NULL != poMLVncDisc)
   {
      bRetVal = poMLVncDisc->bGetAppIDforProtocol(cou32DeviceID, corfrszProtocolID,
               rfru32AppID);
   }  // if(NULL != poMLVncDisc)
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vSubscribeForDeviceEvents()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vSubscribeForDeviceEvents(const t_U32 cou32DeviceHandle,
                                                      t_Bool bSubscribe,
                                                      const trUserContext& corfrUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vSubscribeForDeviceEvents:DeviceID-0x%x,bSubscribe - %d "
      ,cou32DeviceHandle,ETG_ENUM(BOOL,bSubscribe)));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vSubscribeForDeviceEvents(cou32DeviceHandle,bSubscribe,corfrUsrCntxt);
   }  // if(NULL != poMLVncDisc)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vDisplayAppcertificationInfo()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vDisplayAppcertificationInfo(const t_U32 cou32DevHandle,
                                                               const t_U32 cou32AppHandle)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vDisplayAppcertificationInfo(cou32DevHandle,cou32AppHandle);
   }  // if(NULL != poMLVncDisc)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bSetClientProfile()
***************************************************************************/
t_Bool spi_tclMLVncCmdDiscoverer::bSetClientProfile(const t_U32 cou32DevHandle,
                                                    const trClientProfile& corfrClientProfile,
                                                    const trUserContext& corfrUsrCntxt)
{
   t_Bool bRet = false;
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      bRet=poMLVncDisc->bSetClientProfile(cou32DevHandle,corfrClientProfile,corfrUsrCntxt);
   }  // if(NULL != poMLVncDisc)
   return bRet;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncCmdDiscoverer::vDisplayAppListXml()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vDisplayAppListXml(const t_U32 cou32DevHandle)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vDisplayAppListXml(cou32DevHandle);
   }  // if(NULL != poMLVncDisc)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vFetchAppName()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vFetchAppName(t_U32 u32DevHandle, 
                                                t_U32 u32AppHandle, 
                                                const trUserContext& cofrUserCntxt)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vFetchAppName(u32DevHandle,u32AppHandle,cofrUserCntxt);
   }  // if(NULL != poMLVncDisc)
}

/***************************************************************************
** FUNCTION:  t_String spi_tclMLVncCmdDiscoverer::szFetchAppName()
***************************************************************************/
t_String spi_tclMLVncCmdDiscoverer::szFetchAppName(t_U32 u32DevHandle, 
                                                   t_U32 u32AppHandle)
{
   t_String szAppName = coszUnKnownAppName.c_str();
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      szAppName = (poMLVncDisc->szFetchAppName(u32DevHandle,u32AppHandle)).c_str();
   }  // if(NULL != poMLVncDisc)
   return szAppName;
}

/***************************************************************************
** FUNCTION:  tenAppCategory spi_tclMLVncCmdDiscoverer::enGetAppCategory()
***************************************************************************/
tenAppCategory spi_tclMLVncCmdDiscoverer::enGetAppCategory(t_U32 u32DevHandle,
                                                             t_U32 u32AppHandle)
{
   tenAppCategory enAppCat = e32APPUNKNOWN;
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      enAppCat = poMLVncDisc->enGetAppCategory(u32DevHandle,u32AppHandle);
   }  // if(NULL != poMLVncDisc)
   return enAppCat;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vSetUPnPAppPublicKeyForXMLValidation()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vSetUPnPAppPublicKeyForXMLValidation(t_U32 u32DevHandle,
                                                                       t_String szUPnPAppPublicKey,
                                                                       const trUserContext& corfrUsrCntxt)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vSetUPnPAppPublicKeyForXMLValidation(u32DevHandle,szUPnPAppPublicKey,corfrUsrCntxt);
   }  // if(NULL != poMLVncDisc)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vFetchAppUri()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vFetchAppUri(const t_U32 cou32DevID, 
                                               const t_U32 cou32AppID, 
                                               t_String& rfszAppUri,
                                               t_String& rfszProtocolID)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vFetchAppUri(cou32DevID,cou32AppID,rfszAppUri,rfszProtocolID);
   }  // if(NULL != poMLVncDisc)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::bRequestMirrorlinkSwitch()
***************************************************************************/
t_Bool spi_tclMLVncCmdDiscoverer::bRequestMirrorlinkSwitch(const t_U32 cou32DevID)
{
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   t_Bool bRetval =  false;
   if(NULL != poMLVncDisc)
   {
      bRetval = poMLVncDisc->bRequestMirrorlinkSwitch(cou32DevID);
   }  // if(NULL != poMLVncDisc)
   return bRetval;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncCmdDiscoverer::bisDeviceInMirrorlinkMode()
***************************************************************************/
t_Bool spi_tclMLVncCmdDiscoverer::bisDeviceInMirrorlinkMode(const t_U32 cou32DevID)
{
   t_Bool bRetVal = false;
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;
   if(NULL != poMLVncDisc)
   {
      bRetVal = poMLVncDisc->bisDeviceInMirrorlinkMode(cou32DevID);
   }  // if(NULL != poMLVncDisc)
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetAppIconData()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetKeyIconData(const t_U32 cou32DeviceHandle,
                                                  t_String szKeyIconUrl,
                                                  const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetKeyIconData: DeviceID-0x%x,Key Icon URL - %s"
      ,cou32DeviceHandle,szKeyIconUrl.c_str()));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance() ;

   if(NULL != poMLVncDisc)
   {
      poMLVncDisc->vGetKeyIconData(cou32DeviceHandle,szKeyIconUrl,rfrcUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncCmdDiscoverer::vGetXDeviceKeyInfo()
***************************************************************************/
t_Void spi_tclMLVncCmdDiscoverer::vGetXDeviceKeyInfo(const t_U32 u32DeviceID,
      t_U16& u16NumXDevices,std::vector<trXDeviceKeyDetails> &vecrXDeviceKeyDetails)
{
   ETG_TRACE_USR1(("spi_tclMLVncCmdDiscoverer::vGetXDeviceKeyInfo: DeviceID-0x%x,",u32DeviceID));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance();

   if(NULL != poMLVncDisc)
   {
      //! Get X-Device Key Info
      poMLVncDisc->vGetXDeviceKeyInfo(u32DeviceID,u16NumXDevices,vecrXDeviceKeyDetails);
   }
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
