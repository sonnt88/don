/***************************************************************************/
/*!
 * \file  spi_tclMLVncDiscoverer.cpp
 * \brief Interface to interact with VNC Discovery SDK
 ****************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Interface to interact with VNC Discovery SDK
 AUTHOR:         Shiva Kumar Gurija
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 23.09.2013  | Shiva Kumar Gurija    | Initial Version
 17.02.2014  | Shiva Kumar Gurija    | Updated for AppMngr
 14.05.2014  | Shiva Kumar Gurija    | Changes in AppList Handling for CTS Test
 24.07.2014  | Shiva Kumar Gurija    | Work around to fetch the missing app name
 12.08.2014  | Ramya Murthy          | Included Notification related info in SetClientProfile
 13.11.2014  | Shiva Kumar Gurija    | Fix for Suzuki-20039
 24.04.2014  | Shiva kumar Gurija    | XML Validation
 06.05.2015  | Tejaswini HB          | Lint Fix
 21.05.2015  | Shiva Kumar Gurija    | Changes in determining ML version of the Phones
 01.06.2015  | Shiva Kumar Gurija    | Set names to VNC Threads
 25.06.2015  | Sameer Chandra        | Added ML XDeviceKey Support for PSA
 03.07.2015  | Shiva Kumar Gurija    | improvements in ML Version checking
 04.02.2016  | Rachana L Achar       | Logiscope improvements
 \endverbatim
 *****************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include <map>
#include <algorithm>
#include "vncdiscoverysdktypes.h"
#include "mirrorlinkdiscovererdefs.h"
#include "vncmirrorlinkkeys.h"
#include "vncmirrorlink.h"
#include "BaseTypes.h"
#include "RespBase.h"
#include "crc.h"
#include "StringHandler.h"
#include "FileHandler.h"
#include "ThreadNamer.h"
#include "spi_tclMLVncDiscoverer.h"
#include "spi_tclMLVncMsgQInterface.h"
#include "spi_tclVncSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
#include "trcGenProj/Header/spi_tclMLVncDiscoverer.cpp.trc.h"
#endif
#endif

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
enum enDiscRespID
{
   //e32NO_REQ_ID = 0,           //Commented to suppress Lint Warning
   e32FETCH_APPLIST = 1,
   e32FETCH_CERTIFIED_APPLIST = 2,
   // e32FETCH_APPINFO = 3,       //Commented to suppress Lint Warning
   e32LAUNCH_APP = 4,
   e32TERMINATE_APP = 5,
   e32FETCH_APPSTATUS = 6,
   e32FETCH_ALLAPPSTATUS = 7,
   e32FETCH_BTADDR = 8,
   e32SET_BTADDR = 9,
   //e32FETCH_DEVICEINFO = 10,   //Commented to suppress Lint Warning
   //e32FETCH_PROTOCOLID = 11,   //Commented to suppress Lint Warning
   e32FETCH_APPICON_DOWNLOAD_ID = 12,
   e32EVENTSUBSCRIPTION = 13,
   e32SETCLIENTPROFILE = 14,
   e32APPLISTXML = 15,
   e32FETCHAPPNAME = 16,
   e32APP_PUBLIC_KEY = 17,
   e32FETCH_KEYICON_DOWNLOAD_ID = 18
};


/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/
#define ML_DISCOVERER_LIBRARIES_PATH "/usr/lib/"
#define INVALID_DEVICE_ID 0
#define APP_PROFILE_ID_KEY "profileID"
#define APP_STATUSTYPE_KEY "statusType"

#define ML_MAJOR_VERSION 1

static const t_Char coczDiscIgnoreInterfaces[] = "eth0;lo;127.0.0.1;0.0.0.0;fec0;lo0;inc-adr3;inc-scc;cplay;inc-fake0;inc-fake1";
static const t_String cszDefaultProfileId = "0";
static const t_String cszStatusForeGround = "Foreground";
static const t_String cszStatusBackGround = "Background";
static const t_Char* cszImagePng = "image/png";
static const t_Char* cszImageJpeg = "image/jpg";
static const t_String cszEmptyStr = "";
static const t_Char cocAppListdelimiter = ';';
static const t_U32 su32Timeout_250 = 250;
static const t_Char* coczMLClientVersion = "1.1";
static const t_U32 cou32DiscSDKLogLevel = 100;
static const t_S32 scos32AnyProductID = 0;

/*! 
 * \brief Macro for string comparision
 */
#define STRNCMP(VAR1,VAR2) \
   strncmp(VAR1,VAR2,strlen(VAR2))
#define STRCMP(VAR1,VAR2) \
   strcmp(VAR1,VAR2)

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \addtogroup Discoverer Types
 * \brief   Static strings to be used for the creation of Discoverers
 */

/*! @{*/
//! Mirrorlink discoverer
static const t_Char* m_pcocMirrorlink = "mirrorlink";
//! USB discoverer.
//static const t_Char* m_pcocUsb = "usb";  //Commented to suppress Lint Warning
/* If required, can be uncommented. To avoid warnings it is commented
 //! IPOD discoverer
 static const t_Char* m_pcocIpod = "ipod" ;
 //! Wi-Fi discoverer
 static const t_Char* m_pcocWiFi = "wifi";
 */
/*! @}*/

//! Typedef for the Device ID & App ID Key
typedef std::pair<t_U32, t_U32> tDevAppIDPair;
//! iterator to traverse through the m_mapAppsinfo
typedef std::map<std::pair<t_U32, t_U32>, t_String>::iterator tItAppsInfo;

//! Initialize the static variable
MsgContext spi_tclMLVncDiscoverer::m_oMsgContext;

using namespace spi::io;

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscoverer::spi_tclMLVncDiscoverer()
 ***************************************************************************/
spi_tclMLVncDiscoverer::spi_tclMLVncDiscoverer() :
   m_poDiscoverySdkInstance(NULL), m_poDiscoverySdk(NULL), m_u32Counter(0), m_u32MLCmdDeviceHandle(0)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::spi_tclMLVncDiscoverer()"));

   //Allocate memory to the Discovery SDK.
   m_poDiscoverySdk = new VNCDiscoverySDK();
   //Assert if the m_poDiscoverySdk is null
   SPI_NORMAL_ASSERT(NULL == m_poDiscoverySdk);

}

/***************************************************************************
 ** FUNCTION:  spi_tclMLVncDiscoverer::~spi_tclMLVncDiscoverer()
 ***************************************************************************/
spi_tclMLVncDiscoverer::~spi_tclMLVncDiscoverer()
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::~spi_tclMLVncDiscoverer()"));

   //Deletion of SDK Instance of will be done by SDK
   m_poDiscoverySdkInstance = NULL;
   //Deallocate the allocated memory
   RELEASE_MEM(m_poDiscoverySdk);

   //Set the counter to Zero
   m_u32Counter = 0;

}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bInitializeDiscoverySDK()
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bInitializeDiscoverySDK()
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bInitializeDiscoverySDK()"));

   t_Bool bRetVal = false;

   //Initialize VNCDiscoverySDK, if it is not initialized only.
   if (NULL == m_poDiscoverySdkInstance)
   {
      //Populate VNCDiscoverySDKCallbacks structure with the required callback functions.
      VNCDiscoverySDKCallbacks oDiscSDKCallbacks;
      memset(&oDiscSDKCallbacks, 0, sizeof(oDiscSDKCallbacks));

      oDiscSDKCallbacks.discovererStarted = &vDiscovererStartedCallback;
      oDiscSDKCallbacks.discovererStopped = &vDiscovererStoppedCallback;
      oDiscSDKCallbacks.entityAppeared = &vEntityAppearedCallback;
      oDiscSDKCallbacks.entityChanged = &vEntityChangedCallback;
      oDiscSDKCallbacks.entityDisappeared = &vEntityDisappearedCallback;
      oDiscSDKCallbacks.fetchEntityValue = &vFetchEntityValueCallback;
      oDiscSDKCallbacks.postEntityValue = &vPostEntityValueCallback;
      oDiscSDKCallbacks.cancelDeviceChoice = &vCancelDeviceChoiceCb;
      oDiscSDKCallbacks.log = &vDiscLogCb;
      oDiscSDKCallbacks.threadStarted = &vDiscThreadStartedCb;
      oDiscSDKCallbacks.threadEnded = &vDiscThreadEndedCb;
      //oDiscSDKCallbacks.chooseDiscovererForDevice = &vChooseDiscovererForDeviceCb;
      //Currently Context is set to this pointer
      oDiscSDKCallbacks.pSDKContext = this;

      //Initialize the Discovery SDK
      if (NULL != m_poDiscoverySdk)
      {
         m_poDiscoverySdkInstance = VNCDiscoverySDKInitialize(m_poDiscoverySdk,
                  sizeof(*m_poDiscoverySdk),
                  &oDiscSDKCallbacks,
                  sizeof(oDiscSDKCallbacks));

         SPI_NORMAL_ASSERT(NULL == m_poDiscoverySdkInstance);

         if ((NULL != m_poDiscoverySdkInstance) && (true == bAddLicense()))
         {
            //set the logging severity
            m_poDiscoverySdk->setLoggingSeverity(m_poDiscoverySdkInstance, cou32DiscSDKLogLevel);
            bRetVal = true;
         }//if ((NULL != m_poDiscoverySdkInsta
      }//if(NULL != m_poDiscoverySdk)
   }
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer:: bInitializeDiscoverySDK:Result-%d", ETG_ENUM(BOOL, bRetVal)));

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::bAddLicense()
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bAddLicense()
{

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bAddLicense()"));

   t_String szLicensePath, szLicenseFile;
   t_Bool bReadResult = false, bRet = false;

   spi_tclVncSettings *poVncSettings = spi_tclVncSettings::getInstance();

   if (NULL != poVncSettings)
   {
      poVncSettings->vGetLicenseFilePath(szLicensePath);
   }

   VNCDiscoverySDKError enLicenseError = VNCDiscoverySDKErrorFeatureNotLicensed;
   // Read the license file
   spi::io::FileHandler oLicenseFile(szLicensePath.c_str(), spi::io::SPI_EN_RDONLY);

   if (true == oLicenseFile.bIsValid())
   {
      t_S32 s32LicenseFileSize = oLicenseFile.s32GetSize();

      if (0 < s32LicenseFileSize)
      {
         t_Char czLicenseFile[s32LicenseFileSize + 1];
         bReadResult = oLicenseFile.bFRead(czLicenseFile, s32LicenseFileSize);

         if (true == bReadResult)
         {
            czLicenseFile[s32LicenseFileSize] = '\0';
            szLicenseFile.assign(czLicenseFile);

         }//if (true == bReadRes

      }//if (0 <= s32LicenseFileSize)

   }//  if (true == oLicenseFile.bIsValid()
   size_t serialNumberSize = 0;
   if ((true == bReadResult) && (NULL != m_poDiscoverySdkInstance) && (NULL != m_poDiscoverySdk))
   {
      // add license
      enLicenseError = m_poDiscoverySdk->addLicense(m_poDiscoverySdkInstance,
               szLicenseFile.c_str(),
               NULL,
               serialNumberSize);

      bRet = (VNCDiscoverySDKErrorNone == enLicenseError);

      if (false == bRet)
      {
         ETG_TRACE_ERR(("[ERR]:bAddLicense: failed with error = %x ", enLicenseError));
      }
   }

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bAddLicense left with result-%d ", ETG_ENUM(BOOL, bRet)));

   return bRet;
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vUninitializeDiscoverySDK()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vUninitializeDiscoverySDK() const
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vUninitializeDiscoverySDK()"));
   //m_poDiscoverySdkInstance should not be null, otherwise crash will occur
   if (NULL != m_poDiscoverySdkInstance)
   {
      //Destory VNCDiscoverySDK Instance
      VNCDiscoverySDKDestroy(m_poDiscoverySdkInstance);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bInitializeDiscoverer()
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bInitializeDiscoverer()
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bInitializeDiscoverer()"));

   t_Bool bRetVal = false;

   //Create Mirror link discoverer
   t_Char* pcDiscType = const_cast<t_Char*>(m_pcocMirrorlink);
   VNCDiscoverySDKDiscoverer* poDiscML = poGetDiscoverer(enMLDiscoverer);
   bRetVal = bCreateDiscoverer(pcDiscType, poDiscML, enMLDiscoverer);

   //Create USB Discoverers
   //pcDiscType =  (t_Char*) m_pcocUsb;
   //VNCDiscoverySDKDiscoverer* poDiscUSB = poGetDiscoverer(enUSBDiscoverer);
   //bRetVal = bRetVal && bCreateDiscoverer(pcDiscType,poDiscUSB, enUSBDiscoverer);

   return bRetVal;
}
/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bCreateDiscoverer
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bCreateDiscoverer(t_Char* pcDiscType, VNCDiscoverySDKDiscoverer* poDisc,
         tenDiscovererType enDiscType)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bCreateDiscoverer:Disc = %p Discoverer Type:%s", poDisc, pcDiscType));

   t_Bool bRetVal = false;

   if ((NULL != m_poDiscoverySdkInstance) && (NULL != m_poDiscoverySdk) && (NULL != pcDiscType))
   {
      //check whether the discoverer is already created for the requested type.
      //If already created, return true
      if (NULL == poDisc)
      {
         //Create the discoverer
         t_S32 s32Error = m_poDiscoverySdk->createDiscoverer(m_poDiscoverySdkInstance,
                  pcDiscType,
                  ML_DISCOVERER_LIBRARIES_PATH,
                  &poDisc);

         //Check if any error
         if (cs32MLDiscoveryErrorNone != s32Error)
         {
            //Some Error in creating discoverer and discoverer object is incomplete
            //destroy it.
            ETG_TRACE_ERR(("[ERR]:bCreateDiscoverer();Error in creating discoverer-%s ", pcDiscType));
            if (NULL != poDisc)
            {
               m_poDiscoverySdk->discovererDestroy(poDisc);
            }
         }

         if (NULL != poDisc)
         {
            ETG_TRACE_USR2(("[PARAM]:bCreateDiscoverer: %s Discover created", pcDiscType));

            //set the property - detects the devices found with the dhcp script
            m_poDiscoverySdk->discovererSetProperty(poDisc, "dhcpscript", "echo");

            // Request the MirrorLink Discoverer to ignore interfaces 
            m_poDiscoverySdk->discovererSetProperty(poDisc,
                     VNCMlDiscovererPropIgnoreInterfaces,
                     coczDiscIgnoreInterfaces);

            //! Uncomment below line once VNC SDK 2.9 is integrated.
            //! Enable Manual Mirrorlink command trigger
            m_poDiscoverySdk->discovererSetProperty(poDisc, VNCMlDiscovererPropManualUSBCommand, "1");

            //Push the discoverer pointer to map
            mapDiscTypes.insert(std::pair<tenDiscovererType, VNCDiscoverySDKDiscoverer*>(enDiscType, poDisc));

            bRetVal = true;
         }
         else
         {
            // poDisc is Null
            SPI_NORMAL_ASSERT_ALWAYS();
         }
      }
      else
      {
         bRetVal = true;
         ETG_TRACE_USR2(("[DESC]:bInitializeDiscoverer(): %s discoverer is already created", pcDiscType));
      }

   }
   return bRetVal;
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vUninitializeDiscoverer
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vUninitializeDiscoverer()
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vUninitializeDiscoverer()"));

   //Iterate through the Discoverers container and Get all the discoverer pointers
   for (itMapDiscTypes = mapDiscTypes.begin(); itMapDiscTypes != mapDiscTypes.end(); itMapDiscTypes++)
   {
      //Discoverer pointer should not be null. If it is null, crash will occur
      if ((NULL != itMapDiscTypes->second) && (NULL != m_poDiscoverySdk))
      {
         //destroy discoverers
         m_poDiscoverySdk->discovererDestroy(itMapDiscTypes->second);
         itMapDiscTypes->second = NULL;
      }
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vStartDeviceDiscovery()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vStartDeviceDiscovery()
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vStartDeviceDiscovery()- No of discoverers to be started-%d", mapDiscTypes.size()));
   //Iterate through the Discoverers container and Get all the discoverer pointers
   for (itMapDiscTypes = mapDiscTypes.begin(); itMapDiscTypes != mapDiscTypes.end(); itMapDiscTypes++)
   {
      //Discoverer pointer should not be null. If it is null, crash will occur
      if ((NULL != m_poDiscoverySdk) && (NULL != itMapDiscTypes->second))
      {
         ETG_TRACE_USR2(("[PARAM]:vStartDeviceDiscovery:Starting discoverer : %p ", itMapDiscTypes->second));
         //Get the status of the discoverer. If the status is undefined,then only start discoverer.
         //Initially Discoverer Status is undefined. Once it is started, status will be changed to 
         //Waiting or Scanning.
         if (cs32MLDiscoveryDiscovererStatusUndefined == s32GetDiscovererStatus(itMapDiscTypes->first))
         {
            //Start the discoverer
            m_poDiscoverySdk->discovererStart(itMapDiscTypes->second);
            ETG_TRACE_USR4(("[DESC]:Discoverer start request is sent"));
         }
      }
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vStopDeviceDiscovery()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vStopDeviceDiscovery()
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vStopDeviceDiscovery()"));

   //Iterate through the Discoverers container and Get all the discoverer pointers
   for (itMapDiscTypes = mapDiscTypes.begin(); itMapDiscTypes != mapDiscTypes.end(); itMapDiscTypes++)
   {
      //Discoverer pointer should not be null. If it is null, crash will occur
      if ((NULL != m_poDiscoverySdk) && (NULL != itMapDiscTypes->second))
      {

         ETG_TRACE_USR2(("vStopDeviceDiscovery() Stopping discoverer : %p ", itMapDiscTypes->second));
         //Behavior should be checked, We can stop a discoverer even if the status is undefined.
         //At present If the discoverer status is not undefined, then only stop the discoverer.
         //Disoverer Status undefined means the discover is not yet started.
         if (cs32MLDiscoveryDiscovererStatusUndefined != s32GetDiscovererStatus(itMapDiscTypes->first))
         {
            //Stop the discoverer
            m_poDiscoverySdk->discovererStop(itMapDiscTypes->second);
            ETG_TRACE_USR4(("[DESC]:Discoverer stop request is sent"));
         }
      }
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppList
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppList(const trUserContext corUserContext, t_U32 u32DeviceID)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppList(): Device ID-0x%x",u32DeviceID));

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   //Fetch the Application List
   t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, VNCDiscoverySDKMLAppList, VNCDiscoverySDKNoTimeout);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corUserContext;
   rMsgCtxt.rAppContext.u32ResID = e32FETCH_APPLIST;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vFetchAppList: Error in fetching "));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppList
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchCertifiedAppList(const trUserContext corUserContext, t_U32 u32DeviceID)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchCertifiedAppList(): Device ID-0x%x ", u32DeviceID));

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, VNCDiscoverySDKMLGetCertifiedApplicationsList);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corUserContext;
   rMsgCtxt.rAppContext.u32ResID = e32FETCH_CERTIFIED_APPLIST;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vFetchCertifiedAppList: Error in fetching "));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetAppInfo
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vGetAppInfo(const trUserContext corUserContext, t_U32 u32DeviceID, t_U32 u32AppId)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vGetAppInfo: DeviceID-0x%x, AppID-0x%x ", u32DeviceID, u32AppId));

   trApplicationInfo rAppInfo;
   vGetAppInfo(u32DeviceID, u32AppId, rAppInfo);

   MLAppInfoMsg oAppInfo;
   oAppInfo.vSetUserContext(corUserContext);
   oAppInfo.u32AppID = u32AppId;
   if (NULL != oAppInfo.prAppInfo)
   {
      *(oAppInfo.prAppInfo) = rAppInfo;
   }
   oAppInfo.vSetDeviceHandle(u32DeviceID);
   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oAppInfo, sizeof(oAppInfo));
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetAppInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vGetAppInfo(const t_U32 cou32DeviceHandle, t_U32 u32AppId, trApplicationInfo& rfrAppInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vGetAppInfo: DeviceID-0x%x, AppID-0x%x ", cou32DeviceHandle, u32AppId));

   //@todo 1: Provide retry mechanism to fetch the application details, in case if SDK returns an error.
   // (or) Fetch the application details asynchronously. 
   //@todo 2:If there are more than one application list updates, only process the most recent update and discard 
   //the last updates, instead of processing all the application list updates.

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DeviceHandle);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DeviceHandle);
   //get the App ID
   t_String szAppId = szGetAppId(cou32DeviceHandle, u32AppId);

   //String Utility Handler
   StringHandler oStrUtil;

   //Buffer to write the VNC Keys along with AppId, to fecth values from SDK.
   t_Char cKey[MAX_KEYSIZE] = { 0 };
   t_Char* czResult = NULL;

   rfrAppInfo.rAppdetails.u32AppHandle = u32AppId;

   //Convert the String Application ID to Integer form
   rfrAppInfo.rAppdetails.u32ConvAppHandle = oStrUtil.u32ConvertStrToInt(szAppId.c_str());

   //Name of the application.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLAppName, szAppId, &czResult, su32Timeout_250))
        && (NULL != czResult))
   {
      rfrAppInfo.rAppdetails.szAppName = czResult;
      ETG_TRACE_USR4(("spi_tclMLVncDiscoverer::vGetAppInfo: %s", rfrAppInfo.rAppdetails.szAppName.c_str()));
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD
   else
   {
      rfrAppInfo.rAppdetails.szAppName = coszUnKnownAppName.c_str();
   }

   //Fetch App Status
   sprintf(cKey, VNCDiscoverySDKMLAppStatus, szAppId.c_str());
   rfrAppInfo.rAppdetails.enAppStatus = enGetAppStatus(poDisc, poEntity, cKey);

   vFetchAppProviderInfo(poDisc,
            poEntity,
            szAppId,
            rfrAppInfo.rAppdetails.szAppProviderName,
            rfrAppInfo.rAppdetails.szAppProviderURL,
            rfrAppInfo.rAppdetails.szAppVariant);

   //Application Description.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppDescription, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfrAppInfo.rAppdetails.szAppDescription = czResult;
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD

   //Allowed Profiles for the application.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppAllowedProfiles, szAppId, &czResult))
        && (NULL != czResult))
   {
      //Needs to be revisited, assumption is that
      //profile id's will be received in the ';' separated list
      oStrUtil.vSplitString(czResult, ';', rfrAppInfo.rAppdetails.AppAllowedProfiles);
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD

   //URL of application certificate.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppCertificateUrl, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfrAppInfo.rAppdetails.szAppCertificateURL = czResult;
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD

   //Application Category.
   sprintf(cKey, VNCDiscoverySDKMLAppCategory, szAppId.c_str());
   rfrAppInfo.rAppdetails.enAppCategory = enGetAppCategory(poDisc, poEntity, cKey);
   //Fethcing application category, returned an error, try to fetch it again
   if (e32APPUNKNOWN == rfrAppInfo.rAppdetails.enAppCategory)
   {
      rfrAppInfo.rAppdetails.enAppCategory = enGetAppCategory(poDisc, poEntity, cKey);
   }//if(e32APPUNKNOWN == rfrAppInfo.rAppdetails.enAppCategory)


   //Application Trust Level.
   rfrAppInfo.rAppdetails.enTrustLevel = enGetTrustLevel(poDisc, poEntity, 
      VNCDiscoverySDKMLAppTrustLevel, szAppId);

   //! App Display Info
   vFetchAppDisplayInfo(poDisc, poEntity, szAppId, rfrAppInfo.rAppdetails.rAppDisplayInfo);

   //!App Remoting Info
   vFetchAppRemotingInfo(poDisc, poEntity, szAppId, rfrAppInfo.rAppdetails.rAppRemotingInfo);

   //!App Audio Info
   vFetchAppAudioInfo(cou32DeviceHandle,
            poDisc,
            poEntity,
            szAppId,
            rfrAppInfo.rAppdetails.rAppRemotingInfo.szRemotingProtocolID,
            rfrAppInfo.rAppdetails.rAppAudioInfo);

   //Number of Application Icons
   rfrAppInfo.rAppdetails.u32NumAppIcons = 0;

   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppIconCount, szAppId, &czResult))
        && (NULL != czResult))
   {
      StringHandler oStrUtils(czResult);
      rfrAppInfo.rAppdetails.u32NumAppIcons = oStrUtils.u32ConvertStrToInt();
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD

   //Fetch Icon Attributes of all applications
   for (t_U32 u32Index = 0; u32Index < rfrAppInfo.rAppdetails.u32NumAppIcons; u32Index++)
   {
      trIconAttributes rIconAttr;
      vFetchAppIconAttrBlocking(cou32DeviceHandle, u32AppId, u32Index, rIconAttr);
      rfrAppInfo.rAppdetails.tvecAppIconList.push_back(rIconAttr);
   }//for(t_U8 u8Index=0;u8Index<rfrAppInfo.rAppdetails.u32NumAppIcons;u8Index++)

   rfrAppInfo.rAppdetails.enAppCertificationInfo = e8NOT_CERTIFIED;

   //Get the application certification info
   vFetchAppCertInfo(poDisc, poEntity, szAppId, rfrAppInfo);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppProviderInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppProviderInfo(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, t_String szAppId, t_String& rfszAppProviderName, t_String& rfszProviderURL,
         t_String& rfszAppVariant)
{

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppProviderInfo:AppID-%s ", szAppId.c_str()));

   t_Char* czResult = NULL;

   //Application Variant.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLAppVariant, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfszAppVariant = czResult;
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD

   //Application Provider Name.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLAppProviderName, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfszAppProviderName = czResult;
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD

   //Application Provider URL.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLAppProviderUrl, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfszProviderURL = czResult;
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppCertInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppCertInfo(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity* poEntity,
         t_String szAppId, trApplicationInfo& rfrAppInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppCertInfo:AppID-%s ", szAppId.c_str()));

   t_Char* czResult = NULL;

   t_Bool bAppCertified = false;

   //If the app is certified, this call return true else false
   if (true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLGetAppCertificationStatus,
       szAppId, &czResult, cs32MLDiscoveryNoTimeout)
       && (NULL != czResult))
   {
      ETG_TRACE_USR4(("vFetchAppCertInfo:AppCertStatus-%s", czResult));
      bAppCertified = (0 == STRCMP(czResult,VNCDiscoverySDKMLTrue));
      vFreeSdkString(czResult);
   }//if(true == bFetchEntityVal

   //VNCDiscoverySDKMLGetApplicationCertificateInfo - if the Get CertificationInfo 
   //request is successful, then fetch the application certification info. 
   //This must be fetched, to retrieve the Application certification details.

   //This call returns the currently requested application id as the response, if the certification info of 
   // the application is retrieved successfully.
   //If the server gives some other applications certification info discard it.

   //Device returns certification status flag as false for the CCC-Member certified apps, if manufacturer name  
   //set in the client profile is not there in the Certifying entities list of the application.
   //if the app is CCC certified, this flag will be set to true always.
   if ((true == bAppCertified) && (true == bFetchEntityValueBlocking(poDisc,
            poEntity,
            VNCDiscoverySDKMLGetApplicationCertificateInfo,
            szAppId,
            &czResult,
            cs32MLDiscoveryNoTimeout)) && (NULL != czResult))
   {
      if (0 == STRCMP(czResult,szAppId.c_str()))
      {
         vFreeSdkString(czResult);

         rfrAppInfo.u8AppCertEntityCount = 0;

         if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLAppCertEntityCount,
              szAppId, &czResult)) && (NULL != czResult))
         {
            StringHandler oStrUtil(czResult);
            rfrAppInfo.u8AppCertEntityCount = (t_U8) oStrUtil.u32ConvertStrToInt();
            vFreeSdkString(czResult);
         }//if( (true == bFetchEntityValueBlocking(poDisc, p

         vFetchAppCertEntityInfo(poDisc, poEntity, szAppId, rfrAppInfo.u8AppCertEntityCount, rfrAppInfo.vecAppCertInfo);
      }
      else
      {
         vFreeSdkString(czResult);
      }
   }//if((true == bAppCertified)&&(true =

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppCertEntityInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppCertEntityInfo(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity* poEntity,
         const t_String &corfszAppId , t_U8 u8EntityCount, std::vector<trAppCertInfo>& rfvecAppCertInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppCertEntityInfo entered"));
   t_Char czKey[MAX_KEYSIZE] = { 0 };
   t_Char* czResult = NULL;
   for (t_U8 u8Index = 0; u8Index < u8EntityCount; u8Index++)
   {
      trAppCertInfo rCertInfo;
      snprintf(czKey,MAX_KEYSIZE-1, VNCDiscoverySDKMLAppCertEntityName, corfszAppId.c_str(), u8Index);
      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, czKey, &czResult)) && (NULL != czResult))
      {
         rCertInfo.szCertEntityName = czResult;
         vFreeSdkString(czResult);
         ETG_TRACE_USR4(("vFetchAppCertEntityInfo: AppCertEntityName-%s", rCertInfo.szCertEntityName.c_str()));
      }//if( (true == bFetchEntityValueBlockin
		
      //Fetch Restricted App cert entities
      sprintf(czKey, VNCDiscoverySDKMLAppCertEntityRestricted, corfszAppId.c_str(), u8Index);
      vGetAppCertEntityList(poDisc, poEntity, czKey, rCertInfo.vecCertEntityRestrictedList);
		
      //Fetch non-restricted App cert entities
      sprintf(czKey, VNCDiscoverySDKMLAppCertEntityNonRestricted, corfszAppId.c_str(), u8Index);
      vGetAppCertEntityList(poDisc, poEntity, czKey, rCertInfo.vecCertEntityNonRestrictedList);

      rfvecAppCertInfo.push_back(rCertInfo);
   }//for(t_U8 u8Index=0;u8Index<rAppInfo.u8AppCertEntityCount;u8Index++)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppDisplayInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppDisplayInfo(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity* poEntity,
         t_String szAppId, trAppDisplayInfo& rfrAppDispInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppDisplayInfo:AppID-%s ", szAppId.c_str()));

   t_Char cKey[MAX_KEYSIZE] = { 0 };
   t_Char* czResult = NULL;
   //Display Category
   sprintf(cKey, VNCDiscoverySDKMLAppDisplayCategory, szAppId.c_str());
   rfrAppDispInfo.enAppDisplayCategory = enGetAppDisplayCategory(poDisc, poEntity, cKey);

   //trust Level
   rfrAppDispInfo.enTrustLevel = enGetTrustLevel(poDisc, poEntity, 
       VNCDiscoverySDKMLAppDisplayTrustLevel, szAppId);

   //Display Orientation
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppDisplayOrientation, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfrAppDispInfo.szAppDisplayOrientation = czResult;
      vFreeSdkString(czResult);
   }

   //Display Rules
   rfrAppDispInfo.u32AppDisplayRules = 0xFFFFFFFF;

   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppDisplayRules, szAppId, &czResult))
        && (NULL != czResult))
   {
      StringHandler oStrUtil(czResult);
      rfrAppDispInfo.u32AppDisplayRules = oStrUtil.u32ConvertStrToInt();
      vFreeSdkString(czResult);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppAudioInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppAudioInfo(const t_U32 cou32DeviceHandle, VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, t_String szAppId, t_String szProtocolID, trAppAudioInfo& rfrAppAudioInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppAudioInfo:AppID-%s ", szAppId.c_str()));

   t_Char* czResult = NULL;

   //Audio Type
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLAppAudioType, szAppId, &czResult)) 
        && (NULL != czResult))
   {
      rfrAppAudioInfo.szAppAudioType = czResult;
      vFreeSdkString(czResult);
   }
	
   //Fetch App Audio Category
   rfrAppAudioInfo.enAppAudioCategory = enGetAppAudioCategory(poDisc, poEntity, szAppId, 
      cou32DeviceHandle, szProtocolID);
   
   //Fetch App Audio TrustLevel
   rfrAppAudioInfo.enTrustLevel = enGetTrustLevel(poDisc, poEntity, 
      VNCDiscoverySDKMLAppAudioTrustLevel, szAppId);

   //Audio Rules
   rfrAppAudioInfo.u32AppAudioRules = 0xFFFFFFFF;
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLAppAudioRules, szAppId, &czResult))
        && (NULL != czResult))
   {
      StringHandler oStrUtil(czResult);
      rfrAppAudioInfo.u32AppAudioRules = oStrUtil.u32ConvertStrToInt();
      vFreeSdkString(czResult);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppRemotingInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppRemotingInfo(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, t_String szAppId, trAppRemotingInfo& rfrAppRemotingInfo)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppRemotingInfo:AppID-%s ", szAppId.c_str()));

   t_Char* czResult = NULL;
   StringHandler oStrUtil;

   //Fetch App Remoting Protocol ID
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppRemotingProtocol, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfrAppRemotingInfo.szRemotingProtocolID = czResult;
      vFreeSdkString(czResult);
   }

   //Fetch App Remoting Format
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppRemotingFormat, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfrAppRemotingInfo.szRemotingFormat = czResult;
      vFreeSdkString(czResult);
   }

   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppRemotingDirection, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfrAppRemotingInfo.szRemotingDirection = czResult;
      vFreeSdkString(czResult);
   }

   //Fetch App Remoting AudioMPL
   rfrAppRemotingInfo.u32RemotingAudioMPL = 0xFFFFFFFF;
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppRemotingAudioMpl, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfrAppRemotingInfo.u32RemotingAudioMPL = oStrUtil.u32ConvertStrToInt(czResult);
      vFreeSdkString(czResult);
   }

   //Fetch App Remoting AudioIPL
   rfrAppRemotingInfo.u32RemotingAudioIPL = 0xFFFFFFFF;
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppRemotingAudioIpl, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfrAppRemotingInfo.u32RemotingAudioIPL = oStrUtil.u32ConvertStrToInt(czResult);
      vFreeSdkString(czResult);
   }
}

/***************************************************************************
 ** FUNCTION:  tenAppStatus spi_tclMLVncDiscoverer::enGetAppStatus()
 ***************************************************************************/
tenAppStatus spi_tclMLVncDiscoverer::enGetAppStatus(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity* poEntity,
         const t_Char *pczKey)
{
   tenAppStatus enAppStat = e8NOTRUNNING;
   //@todo - New enum value e8UNKNOWN will be defined in SPI FI.
   //Use this value, if there is any error in fetching the value

   t_Char* czResult = NULL;

   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, pczKey, &czResult, cs32MLDiscoveryNoTimeout))
        && (NULL != czResult))
   {
      ETG_TRACE_USR2(("[PARAM]:enGetAppStatus:Result - %s ", czResult));
      std::map<t_String, t_String> mapAppStatus;

      StringHandler oStrUtil;
      oStrUtil.vParseAppStatus(czResult, "profileID", "statusType", ';', '=', mapAppStatus);
      enAppStat = enGetAppStatus(mapAppStatus);
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::enGetAppStatus:App Status-%d ", ETG_ENUM(APP_STATUS, enAppStat)));
   return enAppStat;
}

/***************************************************************************
 ** FUNCTION:  tenAppStatus spi_tclMLVncDiscoverer::enGetAppStatus()
 ***************************************************************************/
tenAppStatus spi_tclMLVncDiscoverer::enGetAppStatus(std::map<t_String, t_String>& rfmapAppStatus) const
{
   tenAppStatus enAppStat = e8NOTRUNNING;
   //@todo - New enum value e8UNKNOWN will be defined in SPI FI.
   //Use it as the default value
   if ((0 != rfmapAppStatus.size()) && (rfmapAppStatus.end() != rfmapAppStatus.find(cszDefaultProfileId)))
   {
      if (rfmapAppStatus[cszDefaultProfileId] == cszStatusForeGround)
      {
         enAppStat = e8FOREGROUND;
      }//if( rfmapAppStatus[cszDefaultProfileId] == cszStatusForeGround)
      else if (rfmapAppStatus[cszDefaultProfileId] == cszStatusBackGround)
      {
         enAppStat = e8BACKGROUND;
      }//else if(rfmapAppStatus[cszDefaultProfileId] == cszStatusBackGround)
      else
      {
         //By defualt NOT RUNNING
      }
   }//if((0 != rfmapAppStatus.size())
   return enAppStat;
}
/***************************************************************************
 ** FUNCTION:  tenAppCategory spi_tclMLVncDiscoverer::enGetAppCategory()
 ***************************************************************************/
tenAppCategory spi_tclMLVncDiscoverer::enGetAppCategory(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, const t_Char *pczKey)
{
   //Default App category
   tenAppCategory enAppCat = e32APPUNKNOWN;

   if (NULL != pczKey)
   {
      t_Char *czResult = NULL;

      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, pczKey, &czResult, su32Timeout_250))
      && (NULL != czResult))
      {
         ETG_TRACE_USR2(("[PARAM]:enGetAppCategory: Result-%s", czResult));
         //String Utility Handler
         StringHandler oStrUtil(czResult);
         enAppCat = static_cast<tenAppCategory> (oStrUtil.u32ConvertStrToInt());
         //Test&Certifcation Apps can have category from 0XFFFE0000-0XFFFEFFFF
         //Currently we dont have enum values for all the categories 0XFFFExxxx.
         //So, defined only one category for all the Test apps and using it
         if ((e32APP_CERT_TEST) == static_cast<tenAppCategory>(enAppCat & e32APP_CERT_TEST))
         {
            enAppCat = e32APP_CERT_TEST;
         }//if(e32APP_CERT_TEST == (enApp

         //@todo - if the device returns a value, which is not there in the enum tenAppCategory
         //the returned value will be directly assigned to enAppCat.
         vFreeSdkString(czResult);
      }//if( (true == bFetchEntityValueBlocking(poDisc, poEnti

   }//if(NULL != pczKey)

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::enGetAppCategory: Application category-0x%x", enAppCat));
   return enAppCat;
}
/***************************************************************************
 ** FUNCTION:  tenTrustLevel spi_tclMLVncDiscoverer::enGetTrustLevel().
 ***************************************************************************/
tenTrustLevel spi_tclMLVncDiscoverer::enGetTrustLevel(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, const t_String &corfszTrustLevelKey, const t_String &corfszAppId)
{
   //Default TrustLevel
   tenTrustLevel enTrustLevel = e8UNKNOWN;

   t_Char *czResult = NULL;
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, corfszTrustLevelKey,corfszAppId, &czResult)) && (NULL != czResult))
   {
      ETG_TRACE_USR2(("[PARAM]:enGetTrustLevel Result- %s", czResult));
      //String Utility Handler
      StringHandler oStrUtil(czResult);
      switch (oStrUtil.u32ConvertStrToInt())
      {
         case e8USER_CONFIGURATION:
         {
            enTrustLevel = e8USER_CONFIGURATION;
         }
            break;
         case e8SELF_REGISTERED_APPLICATION:
         {
            enTrustLevel = e8SELF_REGISTERED_APPLICATION;
         }
            break;
         case e8REGISTERED_APPLICATION:
         {
            enTrustLevel = e8REGISTERED_APPLICATION;
         }
            break;
         case e8APPLICATION_CERTIFICATE:
         {
            enTrustLevel = e8APPLICATION_CERTIFICATE;
         }
            break;
         default:
         {
            ETG_TRACE_ERR(("[ERR]:enGetTrustLevel: UnKnown Trust Level"));
         }
            break;
      }//switch (oStrUtil.u32ConvertStrToInt())

      vFreeSdkString(czResult);
   }//if((true == bFetchEntityValueBlocking(poDisc, p

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::enGetTrustLevel: Trustlevel-%d", ETG_ENUM(APP_TRUST_LEVEL, enTrustLevel)));
   return enTrustLevel;
}

/***************************************************************************
 ** FUNCTION:  tenAppDisplayCategory spi_tclMLVncDiscoverer::enGetAppDisplayCa..
 ***************************************************************************/
tenAppDisplayCategory spi_tclMLVncDiscoverer::enGetAppDisplayCategory(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, const t_Char *pczKey)
{
   //Default Application Display category
   tenAppDisplayCategory enDispCat = e32CONTENT_MISC;
   //@todo - enum e32CONTENT_UNKNOWN will be defined in SPI FI, use it as a defualt value

   if (NULL != pczKey)
   {
      t_Char *czResult = NULL;

      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, pczKey, &czResult)) && (NULL != czResult))
      {
         ETG_TRACE_USR2(("[PARAM]:enGetAppDisplayCategory: Result - %s", czResult));
         StringHandler oStrUtil(czResult);
         switch (oStrUtil.u32ConvertStrToInt())
         {
            case e32CONTENT_TEXT:
            {
               enDispCat = e32CONTENT_TEXT;
            }
               break;
            case e32CONTENT_VIDEO:
            {
               enDispCat = e32CONTENT_VIDEO;
            }
               break;
            case e32CONTENT_IMAGE:
            {
               enDispCat = e32CONTENT_IMAGE;
            }
               break;
            case e32CONTENT_GRAPHICSVECTOR:
            {
               enDispCat = e32CONTENT_GRAPHICSVECTOR;
            }
               break;
            case e32CONTENT_GRAPHICS3D:
            {
               enDispCat = e32CONTENT_GRAPHICS3D;
            }
               break;
            case e32CONTENT_UI:
            {
               enDispCat = e32CONTENT_UI;
            }
               break;
            case e32CONTENT_CARMODE:
            {
               enDispCat = e32CONTENT_CARMODE;
            }
               break;
            default:
            {
               enDispCat = e32CONTENT_MISC;
               ETG_TRACE_ERR(("[ERR]:enGetAppDisplayCategory: Miscellaneous display content category "));
            }
               break;
         }//switch (oStrUtil.u32ConvertStrToInt())
         vFreeSdkString(czResult);
      }//if((true == bFetchEntityValueBloc

   }//if(NULL != pczKey)

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::enGetAppDisplayCategory: AppDisplay category-%d", ETG_ENUM(APP_DISPLAY_CAT, enDispCat)));
   return enDispCat;
}

/***************************************************************************
 ** FUNCTION:  tenAppCategory spi_tclMLVncDiscoverer::enGetAppAudioCategory()
 ***************************************************************************/
tenAppAudioCategory spi_tclMLVncDiscoverer::enGetAppAudioCategory(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, const t_String &corfszAppId,
         const t_U32 &corfu32DeviceHandle, t_String szProtocolID)
{
   tenAppAudioCategory enAppAudiocat = e32MISC_CONTENT;
   //@todo - enum e32AUDIO_UNKNOWN will be defined SPI FI, use it as default value
   t_Char *czResult = NULL;
   
      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
           VNCDiscoverySDKMLAppAudioCategory, corfszAppId, &czResult))
           && (NULL != czResult))
      {
         ETG_TRACE_USR2(("spi_tclMLVncDiscoverer::enGetAppAudioCategory: Result - %s", czResult));
         ETG_TRACE_USR2(("[PARAM]:enGetAppAudioCategory: Result - %s", czResult));
         StringHandler oStrUtil(czResult);
         switch (oStrUtil.u32ConvertStrToInt())
         {
            case e32PHONE_AUDIO:
               enAppAudiocat = e32PHONE_AUDIO;
               break;
            case e32MEDIA_AUDIOOUT:
               enAppAudiocat = e32MEDIA_AUDIOOUT;
               break;
            case e32MEDIA_AUDIOIN:
               enAppAudiocat = e32MEDIA_AUDIOIN;
               break;
            case e32VOICE_COMMANDOUT:
               enAppAudiocat = e32VOICE_COMMANDOUT;
               break;
            case e32VOICE_COMMANDIN:
               enAppAudiocat = e32VOICE_COMMANDIN;
               break;
            default:
               enAppAudiocat = e32MISC_CONTENT;
               break;
         }//switch(oStrUtil.u32ConvertStrToInt())
         vFreeSdkString(czResult);
      }//if((true == bFetchEntityVa
   
   trVersionInfo rVerInfo;
   vFetchDeviceVersion(corfu32DeviceHandle, rVerInfo);
   
   //Set App audio category if the version is 1.0
   if ((1 == rVerInfo.u32MajorVersion) && (0 == rVerInfo.u32MinorVersion)
		&& (e32MISC_CONTENT == enAppAudiocat))
   {
      if ("BTHFP" == szProtocolID)
      {
         enAppAudiocat = e32PHONE_AUDIO;
      }
      else if (("RTP" == szProtocolID) || ("BTA2DP" == szProtocolID))
      {
         enAppAudiocat = e32MEDIA_AUDIOOUT;
      }
   }//if ((1 == rVerInfo.u32MajorVersion) && (0 == rVerInfo.u32MinorVersion) &&

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::enGetAppAudioCategory: App Audio category-%d", ETG_ENUM(APP_AUDIO_CAT, enAppAudiocat)));
   return enAppAudiocat;
}

/***************************************************************************
 ** FUNCTION:  tenIconMimeType spi_tclMLVncDiscoverer::enGetIconMimeType()
 ***************************************************************************/
tenIconMimeType spi_tclMLVncDiscoverer::enGetIconMimeType(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, const t_Char *pczKey)
{
   tenIconMimeType enMIMEType = e8ICON_INVALID;

   if (NULL != pczKey)
   {
      t_Char *czResult = NULL;
      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, pczKey, &czResult)) && (NULL != czResult))
      {
         ETG_TRACE_USR2(("[PARAM]:enGetIconMimeType: Result - %s", czResult));
         if (0 == STRCMP(cszImageJpeg,czResult))
         {
            enMIMEType = e8ICON_JPEG;
         }//if( 0 == STRCMP(cszImageJpeg,czResult))
         else if (0 == STRCMP(cszImagePng,czResult))
         {
            enMIMEType = e8ICON_PNG;
         }//else if( 0 == STRCMP(cszImagePng,czResult))
         else
         {
            //Invalid Icon MIME Type
         }
         vFreeSdkString(czResult);
      }//if((true == bFetchEntityValueBlocking
   }//if(NULL != pczKey)
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::enGetIconMimeType: MIME Type - %d", ETG_ENUM(ICON_MIME_TYPE, enMIMEType)));
   return enMIMEType;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetAppCertEntityList()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vGetAppCertEntityList(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, const t_Char *pczKey, std::vector<t_String>& rfvecCertEntityList)
{
   rfvecCertEntityList.clear();

   if (NULL != pczKey)
   {
      t_Char *czResult = NULL;
      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, pczKey, &czResult)) && (NULL != czResult))
      {
         StringHandler oStrUtil;
         oStrUtil.vSplitString(czResult, ',', rfvecCertEntityList);
         vFreeSdkString(czResult);
      }//if((true == bFetchEntityValueBlocking(poDisc,
   }//if(NULL != pczKey)
}

/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32FetchKeyValue()
 ***************************************************************************/
t_U32 spi_tclMLVncDiscoverer::u32FetchKeyValue(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity* poEntity,
         const t_Char *pczKey, t_S32 s32TimeOut)
{

   t_U32 u32RetVal = 0;
   if (NULL != pczKey)
   {
      t_Char *czResult = NULL;
      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, pczKey, &czResult, s32TimeOut)) && (NULL != czResult))
      {
         //String Utility Handler
         StringHandler oStrUtil(czResult);
         u32RetVal = oStrUtil.u32ConvertStrToInt();
         ETG_TRACE_USR4(("[PARAM]:u32FetchKeyValue:czResult - %s", czResult));
         vFreeSdkString(czResult);
      }//if(NULL != czResult)
   }//if(NULL != pczKey)
   return u32RetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vLaunchApp
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vLaunchApp(const trUserContext corUserContext, t_U32 u32DeviceID, t_U32 u32AppId)
{

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vLaunchApp: DeviceID- 0x%x, AppId-0x%x ", u32DeviceID, u32AppId));

   t_String szAppId = szGetAppId(u32DeviceID, u32AppId);
   //Get the Discoverer Pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //Get the Entity Pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   //post the value to VNC
   t_U32 u32ReqId = u32PostEntityValue(poDisc,
            poEntity,
            szAppId.c_str(),
            VNCDiscoverySDKMLLaunchApplicationValue,
            cs32MLDiscoveryNoTimeout);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corUserContext;
   rMsgCtxt.rAppContext.u32ResID = e32LAUNCH_APP;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vLaunchApp: Error in sending Request "));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vTerminateApp
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vTerminateApp(const trUserContext corUserContext, t_U32 u32DeviceID, t_U32 u32AppId)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vTerminateApp: Device- 0x%x, AppId- 0x%x", u32DeviceID, u32AppId));

   t_String szAppId = szGetAppId(u32DeviceID, u32AppId);
   //Get the Discoverer Pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //Get the Entity Pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   //post the value to VNC
   t_U32 u32ReqId = u32PostEntityValue(poDisc, poEntity, szAppId.c_str(), VNCDiscoverySDKMLTerminateApplicationValue);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corUserContext;
   rMsgCtxt.rAppContext.u32ResID = e32TERMINATE_APP;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vTerminateApp: Error in sending Request "));
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppStatus
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppStatus(const trUserContext corUserContext, t_U32 u32DeviceID, t_U32 u32AppId)
{

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppStatus: Device- 0x%x, AppId-0x%x ", u32DeviceID, u32AppId));

   t_String szAppId = szGetAppId(u32DeviceID, u32AppId);
   //Get the Discoverer Pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //Get the Entity Pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   t_Char czKey[MAX_KEYSIZE] = { 0 };
   sprintf(czKey, VNCDiscoverySDKMLAppStatus, szAppId.c_str());

   //post the Fetch Entity Value Request to VNC
   t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, czKey);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corUserContext;
   rMsgCtxt.rAppContext.u32ResID = e32FETCH_APPSTATUS;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vFetchAppStatus: Error in sending Request "));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAllAppsStatuses
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAllAppsStatuses(const trUserContext corUserContext, t_U32 u32DeviceID)
{

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAllAppsStatuses: Device- 0x%x", u32DeviceID));

   //Get the Discoverer Pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //Get the Entity Pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   //post the value to VNC
   t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, VNCDiscoverySDKMLAllAppsStatuses);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corUserContext;
   rMsgCtxt.rAppContext.u32ResID = e32FETCH_ALLAPPSTATUS;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vFetchAllAppsStatuses: Error in sending  Request "));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchBTAddr
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchBTAddr(const trUserContext corUserContext, t_U32 u32DeviceID)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchBTAddr: Device- 0x%x", u32DeviceID));

   //Get the Discoverer Pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //Get the Entity Pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, VNCDiscoverySDKMLProfileBdAddr);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corUserContext;
   rMsgCtxt.rAppContext.u32ResID = e32FETCH_BTADDR;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vFetchBTAddr: Error in sending Request "));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetBTAddr
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vSetBTAddr(const trUserContext corUserContext, t_U32 u32DeviceID,
         const t_Char* pcocValue)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vSetBTAddr: Device-0x%x, Value-0x%x ", u32DeviceID, pcocValue));

   //Get the Discoverer Pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //Get the Entity Pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   t_U32 u32ReqId = u32PostEntityValue(poDisc, poEntity, VNCDiscoverySDKMLProfileBdAddr, pcocValue);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corUserContext;
   rMsgCtxt.rAppContext.u32ResID = e32SET_BTADDR;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vSetBTAddr: Error in sending Request "));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppIconAttrBlocking
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppIconAttrBlocking(t_U32 u32DeviceID, t_U32 u32AppId, t_U32 u32IconId,
         trIconAttributes& rfrIconAttr)
{

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppIconAttrBlocking: Device-0x%x, u32AppId-0x%x", 
      u32DeviceID, u32AppId));

   t_Char cKey[MAX_KEYSIZE] = { 0 };
   t_Char *czResult = NULL;

   t_String szAppId = szGetAppId(u32DeviceID, u32AppId);

   //Get the Discoverer Pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //Get the Entity Pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   vFetchAppIconDimensions(poDisc, poEntity, szAppId, u32IconId, rfrIconAttr);

   sprintf(cKey, VNCDiscoverySDKMLAppIconMimeType, szAppId.c_str(), u32IconId);
   rfrIconAttr.enIconMimeType = enGetIconMimeType(poDisc, poEntity, cKey);

   snprintf(cKey, MAX_KEYSIZE-1, VNCDiscoverySDKMLAppIconUrl, szAppId.c_str(), u32IconId);
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult)) && (NULL != czResult))
   {
      rfrIconAttr.szIconURL = czResult;
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppIconDimensions
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppIconDimensions(
       VNCDiscoverySDKDiscoverer* poDisc,
	   VNCDiscoverySDKEntity* poEntity, 
	   const t_String &corfszAppId, const t_U32 &corfu32IconId,
       trIconAttributes& rfrIconAttr)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppIconDimensions entered"));

   //String Utility Handler
   StringHandler oStrUtil;
   t_Char cKey[MAX_KEYSIZE] = { 0 };
   t_Char *czResult = NULL;
   
   sprintf(cKey, VNCDiscoverySDKMLAppIconHeight, corfszAppId.c_str(), corfu32IconId);
   rfrIconAttr.u32IconHeight = 0xFFFFFFFF;
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult))
        && (NULL != czResult))
   {
      rfrIconAttr.u32IconHeight = oStrUtil.u32ConvertStrToInt(czResult);
      vFreeSdkString(czResult);
   }//if((true == bFetchEntityValueBlocking(poDisc

   sprintf(cKey, VNCDiscoverySDKMLAppIconWidth, corfszAppId.c_str(), corfu32IconId);
   rfrIconAttr.u32IconWidth = 0xFFFFFFFF;
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult))
        && (NULL != czResult))
   {
      rfrIconAttr.u32IconWidth = oStrUtil.u32ConvertStrToInt(czResult);
      vFreeSdkString(czResult);
   }//if((true == bFetchEntityValueBlocking(poDisc

   sprintf(cKey, VNCDiscoverySDKMLAppIconDepth, corfszAppId.c_str(), corfu32IconId);
   rfrIconAttr.u32IconDepth = 0xFFFFFFFF;
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult))
        && (NULL != czResult))
   {
      rfrIconAttr.u32IconDepth = oStrUtil.u32ConvertStrToInt(czResult);
      vFreeSdkString(czResult);
   }//if((true == bFetchEntityValueBlocking(poDisc
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppIconAttr
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppIconAttr(const trUserContext corUserContext, t_U32 u32DeviceID, t_U32 u32AppId,
         t_U32 u32IconId)
{

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchAppIconAttr: Device-0x%x, u32AppId-0x%x", u32DeviceID, u32AppId));

   trIconAttributes rIconAttr;
   vFetchAppIconAttrBlocking(u32DeviceID, u32AppId, u32IconId, rIconAttr);

   MLAppIconAttr oAppIconAttr;
   oAppIconAttr.vSetUserContext(corUserContext);
   oAppIconAttr.u32AppID = u32AppId;
   if (NULL != oAppIconAttr.prAppIconAttr)
   {
      *(oAppIconAttr.prAppIconAttr) = rIconAttr;
   }
   oAppIconAttr.vSetDeviceHandle(u32DeviceID);

   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      //Post the Icon Attributes to SPI.
      poMsgQinterface->bWriteMsgToQ(&oAppIconAttr, sizeof(oAppIconAttr));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetAppIconAttr
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bSetAppIconAttr(const t_U32 cou32DeviceHandle, t_U32 u32AppId, t_U32 u32IconId,
         const trIconAttributes& corfrIconAttr)
{

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vSetAppIconAttr: Device-0x%x, u32AppId-0x%x", cou32DeviceHandle, u32AppId));
   t_Bool bRet = false;
   //String Utility Handler
   StringHandler oStrUtil;
   //Convert the u32AppId to HexString
   t_String szAppId = szGetAppId(cou32DeviceHandle, u32AppId);
   //Get the Discoverer Pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DeviceHandle);
   //Get the Entity Pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DeviceHandle);

   if (cszEmptyStr != szAppId)
   {
      t_Char cKey[MAX_KEYSIZE] = { 0 };
      t_String szTemp;

      switch (corfrIconAttr.enIconMimeType)
      {
         case e8ICON_PNG:
         {
            szTemp = cszImagePng;
         }
            break;
         case e8ICON_JPEG:
         {
            szTemp = cszImageJpeg;
         }
            break;
         default:
         {
            ETG_TRACE_ERR(("[ERR]:vSetAppIconAttr: Invalid MIME Type"));
         }
            break;
      }//switch (corfrIconAttr.enIconMimeType)
      if (szTemp != cszEmptyStr)
      {
         bRet = true;

         //Post Icon MIME Type
         sprintf(cKey, VNCDiscoverySDKMLAppIconMimeType, szAppId.c_str(), u32IconId);
         bRet = bRet && bPostEntityValueBlocking(poDisc, poEntity, cKey, szTemp.c_str());

         //Get Icon Width string and post
         oStrUtil.vConvertIntToStr(corfrIconAttr.u32IconWidth, szTemp, DECIMAL_STRING);
         sprintf(cKey, VNCDiscoverySDKMLAppIconWidth, szAppId.c_str(), u32IconId);
         bRet = bRet && bPostEntityValueBlocking(poDisc, poEntity, cKey, szTemp.c_str());

         //Get the Icon Height string and post
         oStrUtil.vConvertIntToStr(corfrIconAttr.u32IconHeight, szTemp, DECIMAL_STRING);
         sprintf(cKey, VNCDiscoverySDKMLAppIconHeight, szAppId.c_str(), u32IconId);
         bRet = bRet && bPostEntityValueBlocking(poDisc, poEntity, cKey, szTemp.c_str());

         //Get the Icon Depth string and post
         oStrUtil.vConvertIntToStr(corfrIconAttr.u32IconDepth, szTemp, DECIMAL_STRING);
         sprintf(cKey, VNCDiscoverySDKMLAppIconDepth, szAppId.c_str(), u32IconId);
         bRet = bRet && bPostEntityValueBlocking(poDisc, poEntity, cKey, szTemp.c_str());

         //Post Icon URL
         sprintf(cKey, VNCDiscoverySDKMLAppIconUrl, szAppId.c_str(), u32IconId);
         bRet = bRet && bPostEntityValueBlocking(poDisc, poEntity, cKey, (corfrIconAttr.szIconURL).c_str());
      }//if(szTemp != "")
   }//if (NULL != pcAppId)

   return bRet;
}

/***************************************************************************
 ** FUNCTION:  VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscoverer::poGetD...
 ***************************************************************************/
VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscoverer::poGetDiscoverer(tenDiscovererType enDiscType)
{
   VNCDiscoverySDKDiscoverer* poDisc = NULL;

   //get the discoverer pointer based on type.
   //If the key it self is not available , find() returns map::end()
   itMapDiscTypes = mapDiscTypes.find(enDiscType);

   if ((itMapDiscTypes != mapDiscTypes.end()) && (NULL != itMapDiscTypes->second))
   {
      poDisc = itMapDiscTypes->second;
   }

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::poGetDiscoverer: poDisc-%p", poDisc));
   return poDisc;
}

/***************************************************************************
 ** FUNCTION:  tenDiscovererStatus spi_tclMLVncDiscoverer::enGetDiscove...
 ***************************************************************************/
t_S32 spi_tclMLVncDiscoverer::s32GetDiscovererStatus(tenDiscovererType enDiscType)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::s32GetDiscovererStatus()"));

   t_S32 s32DiscStatus = cs32MLDiscoveryDiscovererStatusUndefined;

   //Get the discoverer of the specified enum type.
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(enDiscType);

   if ((NULL != m_poDiscoverySdk) && (NULL != poDisc))
   {
      s32DiscStatus = m_poDiscoverySdk->getDiscovererStatus(poDisc);
   }

   return s32DiscStatus;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscovererStartedCallback()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vDiscovererStartedCallback(t_Void *poSDKContext, VNCDiscoverySDKDiscoverer* poDisc,
         MLDiscoveryError s32Error)
{
   SPI_INTENTIONALLY_UNUSED(poSDKContext);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vDiscovererStartedCallback()"));

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance();
   if ((NULL != poMLVncDisc) && (NULL != poMLVncDisc->m_poDiscoverySdk))
   {
      // Extracting the type of the discoverer
      t_Char* pcDiscType = poMLVncDisc->m_poDiscoverySdk->getDiscovererType(poDisc);

      if (NULL != pcDiscType)
      {
         ETG_TRACE_USR2(("[PARAM]:vDiscovererStartedCallback: Error-%d  Discoverer-%s ", s32Error, pcDiscType));
         //Memory will be allocated by the VNCDiscoverySDK, when you call getDiscovererType().
         //and it should be freed by the caller.
         poMLVncDisc->vFreeSdkString(pcDiscType);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscovererStoppedCallback()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vDiscovererStoppedCallback(t_Void *poSDKContext, VNCDiscoverySDKDiscoverer* poDisc,
         MLDiscoveryError s32Error)
{
   SPI_INTENTIONALLY_UNUSED(poSDKContext);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vDiscovererStoppedCallback"));

   spi_tclMLVncDiscoverer* poMLVncDisc = getInstance();
   if ((NULL != poMLVncDisc) && (NULL != poMLVncDisc->m_poDiscoverySdk))
   {
      //Extract the type of the discoverer
      t_Char* pcDiscType = poMLVncDisc->m_poDiscoverySdk->getDiscovererType(poDisc);

      if (NULL != pcDiscType)
      {
         ETG_TRACE_USR2(("[PARAM]:vDiscovererStoppedCallback: Error-%d, Discoverer-%s", s32Error, pcDiscType));
         //Memory will be allocated by the VNCDiscoverySDK, when you call getDiscovererType().
         //and it should be freed by the caller.
         poMLVncDisc->vFreeSdkString(pcDiscType);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vEntityAppearedCallback()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vEntityAppearedCallback(t_Void *poSDKContext, VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity)
{
   SPI_INTENTIONALLY_UNUSED(poSDKContext);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vEntityAppearedCallback()"));

   spi_tclMLVncDiscoverer* poMLVncDisc = getInstance();

   //! uncomment below lines once VNC SDK 2.9 is integrated.
   //! Following implementation is required for blacklisting USB devices

   if ((NULL != poMLVncDisc) && (NULL != poEntity) && (NULL != poDisc))
   {
      //! fetch the entity type of the reported entity
      t_Char* pczType = NULL;
      VNCDiscoverySDK* poDiscSDK = const_cast<VNCDiscoverySDK*> (poMLVncDisc->poGetDiscSdk());
      poDiscSDK->fetchEntityValueBlocking(poDisc, poEntity, "type", &pczType, VNCDiscoverySDKNoTimeout);
      if ((NULL != pczType) && (NULL != poDiscSDK))
      {
         ETG_TRACE_USR2(("[PARAM]:entityAppearedCallback: poEntity %p type: %s", poEntity, pczType));

         //! check for blacklisted devices if the entity is reported as USB Device
         if (0 == strcmp(pczType, VNCMlDiscovererUSBDeviceEntity))
         {
            poMLVncDisc->vHandleUSBEntity(poMLVncDisc, poDisc, poEntity);
         }
         //! Report new device detection if the reported entity is Mirrorlink Device
         else if (0 == strcmp(pczType, VNCDiscoverySDKMLEntityType))
         {
            poMLVncDisc->vHandleMirrorlinkEntity(poMLVncDisc, poDisc, poEntity);
         }
      }
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vEntityDisappearedCallback()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vEntityDisappearedCallback(t_Void *poSDKContext, VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity *poEntity)
{
   SPI_INTENTIONALLY_UNUSED(poSDKContext);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vEntityDisappearedCallback()"));

   spi_tclMLVncDiscoverer* poMLVncDisc = getInstance();
   if (NULL != poMLVncDisc)
   {
      //Get the unique id of the device based on Discoverer and Entity pointers
      t_U32 u32DeviceID = INVALID_DEVICE_ID;
      poMLVncDisc->vGetUniqueDeviceId(poDisc, poEntity, u32DeviceID);

      //Remove the element from the devices info map
      if (INVALID_DEVICE_ID != u32DeviceID)
      {
         poMLVncDisc->m_oMapAppLock.s16Lock();
         //Clear the application ID's stored internally
         if (SPI_MAP_NOT_EMPTY(poMLVncDisc->m_mapAppsInfo))
         {
            tItAppsInfo itAppsInfo = poMLVncDisc->m_mapAppsInfo.begin();
            while (itAppsInfo != poMLVncDisc->m_mapAppsInfo.end())
            {
               if (itAppsInfo->first.first == u32DeviceID)
               {
                  //usage of erase in for loop. causes iterator to point unknown address.
                  //do not change this.
                  poMLVncDisc->m_mapAppsInfo.erase(itAppsInfo++);
               }//if((itAppsInfo->first.first == corfrdevID)
               else
               {
                  itAppsInfo++;
               }
            }//while(itAppsInfo != poMLVncDisc->m_mapAppsInfo.end())

         }//if(SPI_MAP_NOT_EMPTY(m_mapAppsInfo))
         poMLVncDisc->m_oMapAppLock.vUnlock();

         poMLVncDisc->m_oMapDevLock.s16Lock();
         if (SPI_MAP_NOT_EMPTY(poMLVncDisc->m_mapDevicesInfo))
         {
            poMLVncDisc->itMapDevicesInfo = poMLVncDisc->m_mapDevicesInfo.find(u32DeviceID);

            if (poMLVncDisc->itMapDevicesInfo != poMLVncDisc->m_mapDevicesInfo.end())
            {
               poMLVncDisc->m_mapDevicesInfo.erase(poMLVncDisc->itMapDevicesInfo);
            }//if (poMLVncDisc->itMapDevicesInfo
         }//if(SPI_MAP_NOT_EMPTY(poM
         poMLVncDisc->m_oMapDevLock.vUnlock();

         poMLVncDisc->m_oMapDevModeLock.s16Lock();
         //! Remove the device mode
         if(poMLVncDisc->m_mapDeviceModes.end() != poMLVncDisc->m_mapDeviceModes.find(u32DeviceID))
         {
            poMLVncDisc->m_mapDeviceModes.erase(u32DeviceID);
         }
         poMLVncDisc->m_oMapDevModeLock.vUnlock();

         MLDeviceDisconnMsg oDeviceDisconnMsg;
         oDeviceDisconnMsg.vSetDeviceHandle(u32DeviceID);
         spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
         if (NULL != poMsgQinterface)
         {
            poMsgQinterface->bWriteMsgToQ(&oDeviceDisconnMsg, sizeof(oDeviceDisconnMsg));
         }//if (NULL != poMsgQinterface)
      }//if (INVALID_DEVICE_ID != u32DeviceID)

   }//if (NULL != poMLVncDisc)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vEntityChangedCallback()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vEntityChangedCallback(t_Void *pSDKContext, VNCDiscoverySDKDiscoverer *poDisc,
         VNCDiscoverySDKEntity *poEntity, const t_Char* pChangeDesc)
{
   SPI_INTENTIONALLY_UNUSED(pSDKContext);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vEntityChangedCallback()"));
   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance();

   if ((NULL != pChangeDesc) && (NULL != poMLVncDisc))
   {
      //get the device id
      t_U32 u32DeviceID = INVALID_DEVICE_ID;

      poMLVncDisc->vGetUniqueDeviceId(poDisc, poEntity, u32DeviceID);

      if (INVALID_DEVICE_ID != u32DeviceID)
      {
         t_U32 u32ReqId = cu32MLDiscoveryNoRequestId;

         if (0 == STRNCMP(pChangeDesc, VNCDiscoverySDKMLAppStatusUpdate))
         {
            //get the list of applications whose status is changed
            std::vector<t_String> vecszAppId;

            //String Utility Handler
            StringHandler oStrUtil;
            oStrUtil.vSplitString((pChangeDesc + strlen(VNCDiscoverySDKMLAppStatusUpdate)), ',', vecszAppId);

            t_Char czKey[MAX_KEYSIZE] = { 0 };

            //There is a change in the status of the applications.
            //Fetch the status of the changed applications and post to SPI
            for (t_U8 u8Index = 0; u8Index < vecszAppId.size(); u8Index++)
            {
               sprintf(czKey, VNCDiscoverySDKMLAppStatus, vecszAppId[u8Index].c_str());
               u32ReqId = poMLVncDisc->u32FetchEntityValue(poDisc, poEntity, czKey, cs32MLDiscoveryNoTimeout);

               //Store the request ID. Will be used in callback function
               trMsgContext rMsgCtxt;
               rMsgCtxt.rAppContext.u32ResID = e32FETCH_APPSTATUS;
               m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

               if (cu32MLDiscoveryNoRequestId == u32ReqId)
               {
                  ETG_TRACE_ERR(("[ERR]:vEntityChangedCallback: Error in Fetch MLAppStatus  Request "));
               }
            }
         }
         else if ((0 == STRNCMP(pChangeDesc, VNCDiscoverySDKMLAppListUpdate)) && ((strlen(pChangeDesc)
                  > strlen(VNCDiscoverySDKMLAppListUpdate))))
         {
            //Application list of the device is changed. fetch the application list of the device.
            //and post to SPI
            u32ReqId = poMLVncDisc->u32FetchEntityValue(poDisc, poEntity, VNCDiscoverySDKMLAppList);

            //Store the request ID. Will be used in callback function
            trMsgContext rMsgCtxt;
            rMsgCtxt.rAppContext.u32ResID = e32FETCH_APPLIST;
            m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

            if (cu32MLDiscoveryNoRequestId == u32ReqId)
            {
               ETG_TRACE_ERR(("[ERR]:vEntityChangedCallback: Error in Fetch MLAppList  Request"));
            }
         }
         else
         {
            ETG_TRACE_ERR(("[ERR]:vEntityChangedCallback: Invalid update"));
         }
      }
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vPostEntityValueCallback()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vPostEntityValueCallback(t_Void *pSDKContext, VNCDiscoverySDKRequestId u32RequestId,
         VNCDiscoverySDKDiscoverer *poDisc, VNCDiscoverySDKEntity *poEntity, t_Char* pcKey, t_Char* pcVal,
         MLDiscoveryError s32Error)
{
   SPI_INTENTIONALLY_UNUSED(pSDKContext);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vPostEntityValueCallback:Error-%d ", s32Error));
   if (NULL != pcKey)
   {
      ETG_TRACE_USR2(("[PARAM]:vPostEntityValueCallback:Key-%s  ", pcKey));
   }

   t_Bool bResp = false;

   //get the device id
   t_U32 u32DeviceID = INVALID_DEVICE_ID;

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance();

   //pcVal can be NULL. We can set value as NULL to some parameters
   if ((NULL != poMLVncDisc) && (NULL != pcKey))
   {
      poMLVncDisc->vGetUniqueDeviceId(poDisc, poEntity, u32DeviceID);
      spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();

      if ((INVALID_DEVICE_ID != u32DeviceID) && (NULL != poMsgQinterface))
      {
         bResp = (VNCDiscoverySDKErrorNone == s32Error);

         trMsgContext rPostCbCtxt = m_oMsgContext.rGetMsgContext(u32RequestId);
         trUserContext rUserContext = rPostCbCtxt.rUserContext;

         switch (rPostCbCtxt.rAppContext.u32ResID)
         {
            case e32LAUNCH_APP:
            {
               t_U32 u32AppId = poMLVncDisc->u32GetAppId(u32DeviceID, pcKey);

               MLLaunchAppStatusMsg oLaunchAppStatusMsg;
               oLaunchAppStatusMsg.vSetDeviceHandle(u32DeviceID);
               oLaunchAppStatusMsg.vSetUserContext(rUserContext);
               oLaunchAppStatusMsg.u32AppID = u32AppId;
               oLaunchAppStatusMsg.bLaunchAppStatus = bResp;
               if (true == oLaunchAppStatusMsg.bLaunchAppStatus)
               {
                  ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, " [DESC]:Device-0x%x AppID-0x%x is launched successfully.", u32DeviceID, u32AppId));
               }//if (true == oLaunchAppStatusMsg.bLaunchAppStatus)
               else
               {
                  ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, " [DESC]:Device-0x%x AppID-0x%x launch request is failed", u32DeviceID, u32AppId));
               }
               poMsgQinterface->bWriteMsgToQ(&oLaunchAppStatusMsg, sizeof(oLaunchAppStatusMsg));
            }//case e32LAUNCH_APP:
               break;
            case e32TERMINATE_APP:
            {
               t_U32 u32AppId = poMLVncDisc->u32GetAppId(u32DeviceID, pcKey);
               MLTerminateAppStatusMsg oTerminateAppStatusMsg;
               oTerminateAppStatusMsg.vSetDeviceHandle(u32DeviceID);
               oTerminateAppStatusMsg.vSetUserContext(rUserContext);
               oTerminateAppStatusMsg.u32AppID = u32AppId;
               oTerminateAppStatusMsg.bTerminateAppStatus = bResp;
               if (true == oTerminateAppStatusMsg.bTerminateAppStatus)
               {
                  ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, " [DESC]:AppID-0x%x supported by the Device-0x%x is terminated successfully.",u32AppId, u32DeviceID));
               }//if (true == oTerminateAppStatusMsg.bTerminateAppStatus)
               else
               {
                  ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, " [DESC]:Terminate request for AppID-0x%x supported by the Device-0x%x is failed",  u32AppId,u32DeviceID));
               }//else
               poMsgQinterface->bWriteMsgToQ(&oTerminateAppStatusMsg, sizeof(oTerminateAppStatusMsg));
            }//case e32TERMINATE_APP:
               break;
            case e32SET_BTADDR:
            {
               MLBTAddrMsg oBTAddrMsg;
               oBTAddrMsg.vSetDeviceHandle(u32DeviceID);
               oBTAddrMsg.vSetUserContext(rUserContext);
               if ((NULL != pcVal) && (NULL != oBTAddrMsg.pszBTAddr))
               {
                  *(oBTAddrMsg.pszBTAddr) = pcVal;
               }//if(NULL != pcVal)
               poMsgQinterface->bWriteMsgToQ(&oBTAddrMsg, sizeof(oBTAddrMsg));
            }//case e32SET_BTADDR:
               break;
            case e32EVENTSUBSCRIPTION:
            {
               ETG_TRACE_USR1(("Event subscribe -%d", s32Error));
               MLDeviceEventSubscription oDevEventSub;
               oDevEventSub.vSetDeviceHandle(u32DeviceID);
               oDevEventSub.vSetUserContext(rUserContext);
               if (NULL != pcVal)
               {
                  if (0 == STRNCMP(pcVal,VNCDiscoverySDKMLAppEventsSubscribe))
                  {
                     oDevEventSub.enEventSub = e8SUBSCRIBE;
                     ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, "[DESC]:Subscribed for AppList and AppStatus change events of Device-0x%x", u32DeviceID));
                  }//if( 0 == strncmp(pcVal,VNCDisco
                  else if (0 == STRNCMP(pcVal,VNCDiscoverySDKMLAppEventsUnsubscribe))
                  {
                     ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, "[DESC]:Un subscribed for AppList and AppStatus change events of Device-0x%x", u32DeviceID));
                     oDevEventSub.enEventSub = e8UNSUBSCRIBE;
                  }//else if(0 == strncmp(pcVal,VNCDiscoverySDKML
               }//if(NULL != pcVal)

               oDevEventSub.bResult = (cs32MLDiscoveryErrorNone == s32Error);

               poMsgQinterface->bWriteMsgToQ(&oDevEventSub, sizeof(oDevEventSub));
            }//case e32EVENTSUBSCRIPTION:
               break;
            case e32SETCLIENTPROFILE:
            {
               ETG_TRACE_USR2(("[PARAM]:vPostEntityValueCallback:Set Client Profile Response - %d", 
                  (cs32MLDiscoveryErrorNone == s32Error)));
               MLSetClientProfile oSetClntProfile;
               oSetClntProfile.vSetDeviceHandle(u32DeviceID);
               oSetClntProfile.vSetUserContext(rUserContext);
               oSetClntProfile.bResult = (cs32MLDiscoveryErrorNone == s32Error);
               poMsgQinterface->bWriteMsgToQ(&oSetClntProfile, sizeof(oSetClntProfile));
            }//case e32SETCLIENTPROFILE:
               break;
            case e32APP_PUBLIC_KEY:
            {
               t_Bool bXMLValidationSucceeded = (cs32MLDiscoveryErrorNone == s32Error);
               ETG_TRACE_USR2(("[PARAM]:vPostEntityValueCallback:Set UPnP Application Public Key Response - %d", 
                  bXMLValidationSucceeded));
               MLSetUPnPPublicKeyResp oSetUPnpPublicKeyResp;
               oSetUPnpPublicKeyResp.vSetDeviceHandle(u32DeviceID);
               oSetUPnpPublicKeyResp.vSetUserContext(rUserContext);
               oSetUPnpPublicKeyResp.m_bXMLValidationSucceeded = bXMLValidationSucceeded;
               poMsgQinterface->bWriteMsgToQ(&oSetUPnpPublicKeyResp, sizeof(oSetUPnpPublicKeyResp));
            }//case e32APP_PUBLIC_KEY:
               break;
            default:
            {
               ETG_TRACE_ERR(("[ERR]:vPostEntityValueCallback: Invalid update"));
            }//default:
               break;
         }//switch(rPostCbCtxt.rAppContext.u32ResID)
      }//if ((INVALID_DEVICE_ID != u32DeviceID) && (NULL != poMsgQinterface))
      if (NULL != pcVal)
      {
         poMLVncDisc->vFreeSdkString(pcVal);
      }//if(NULL != pcVal)
   }//if ((NULL != poMLVncDisc) 
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchEntityValueCallback()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchEntityValueCallback(t_Void *pSDKContext, VNCDiscoverySDKRequestId u32RequestId,
         VNCDiscoverySDKDiscoverer *poDisc, VNCDiscoverySDKEntity *poEntity, t_Char* pcKey, MLDiscoveryError s32Error,
         t_Char* pcVal)
{
   SPI_INTENTIONALLY_UNUSED(pSDKContext);
   SPI_INTENTIONALLY_UNUSED(s32Error);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchEntityValueCallback:Error-%d", s32Error));

   //get the device id
   t_U32 u32DeviceID = INVALID_DEVICE_ID;

   spi_tclMLVncDiscoverer* poMLVncDisc = spi_tclMLVncDiscoverer::getInstance();

   if ((NULL != poMLVncDisc) && (NULL != pcKey))
   {
      ETG_TRACE_USR2(("[PARAM]:vFetchEntityValueCallback: Key %s", pcKey));
      poMLVncDisc->vGetUniqueDeviceId(poDisc, poEntity, u32DeviceID);

      spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
      //String Utility Handler
      StringHandler oStrUtil;

      if ((INVALID_DEVICE_ID != u32DeviceID) && (NULL != poMsgQinterface))
      {
         trMsgContext rPostCbCtxt = m_oMsgContext.rGetMsgContext(u32RequestId);
         trUserContext rUserContext = rPostCbCtxt.rUserContext;
         switch (rPostCbCtxt.rAppContext.u32ResID)
         {
            case e32FETCH_APPLIST:
            {
               //Parse Applist and fetch Protocol ID
               poMLVncDisc->vFetchProtocolID(u32DeviceID, pcVal, rUserContext);
               poMLVncDisc->vProcessAppList(u32DeviceID, pcVal, false, rUserContext);
            }//case e32FETCH_APPLIST
               break;
            case e32FETCH_CERTIFIED_APPLIST:
            {
               poMLVncDisc->vProcessAppList(u32DeviceID, pcVal, true, rUserContext);
            }//case e32FETCH_CERTIFIED_APPLIST 
               break;
            case e32FETCH_BTADDR:
            {
               MLBTAddrMsg oBTAddrMsg;
               oBTAddrMsg.vSetDeviceHandle(u32DeviceID);
               oBTAddrMsg.vSetUserContext(rUserContext);
               if (NULL != oBTAddrMsg.pszBTAddr)
               {
                  *(oBTAddrMsg.pszBTAddr) = (NULL != pcVal) ? pcVal : "";
               }
               poMsgQinterface->bWriteMsgToQ(&oBTAddrMsg, sizeof(oBTAddrMsg));
            }
               break;
            case e32FETCH_ALLAPPSTATUS:
            {
               //Parse the All applications statuses and post to SPI
               MLAllAppStatusMsg oAllAppStatusMsg;
               if (NULL != oAllAppStatusMsg.prMapAllStatuses)
               {
                  oStrUtil.vParseAllAppsStatuses(pcVal,
                           APP_PROFILE_ID_KEY,
                           APP_STATUSTYPE_KEY,
                           ';',
                           '=',
                           *(oAllAppStatusMsg.prMapAllStatuses));
               }
               oAllAppStatusMsg.vSetDeviceHandle(u32DeviceID);
               oAllAppStatusMsg.vSetUserContext(rUserContext);
               poMsgQinterface->bWriteMsgToQ(&oAllAppStatusMsg, sizeof(oAllAppStatusMsg));
            }
               break;
            case e32FETCH_APPSTATUS:
            {
               //get the app Id
               t_U32 u32AppId = poMLVncDisc->u32GetAppId(u32DeviceID, pcKey + strlen(VNCDiscoverySDKMLAppStatusBase));

               tenAppStatus enAppStat = e8NOTRUNNING;
               std::map<t_String, t_String> mapAppStatus;
               oStrUtil.vParseAppStatus(pcVal, "profileID", "statusType", ';', '=', mapAppStatus);
               enAppStat = poMLVncDisc->enGetAppStatus(mapAppStatus);

               ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, "[PARAM]:vFetchEntityValueCallback:AppStatus: Dev-0x%x u32AppId-0x%x status-%d", 
                  u32DeviceID, u32AppId, ETG_ENUM(APP_STATUS, enAppStat)));

               //Parse the string and post to SPI.
               MLAppStatusMsg oAppStatusMsg;
               oAppStatusMsg.vSetDeviceHandle(u32DeviceID);
               oAppStatusMsg.vSetUserContext(rUserContext);
               oAppStatusMsg.u32AppID = u32AppId;
               oAppStatusMsg.enAppStatus = enAppStat;
               poMsgQinterface->bWriteMsgToQ(&oAppStatusMsg, sizeof(oAppStatusMsg));
            }
               break;
            case e32FETCH_APPICON_DOWNLOAD_ID:
            {
               poMLVncDisc->vPostAppIconData(u32DeviceID, pcKey, pcVal, rUserContext);
            }
               break;
            case e32APPLISTXML:
            {
               //Since TTFis doesn't print the large amount of data on the console,
               //writing it to a file. This is just for debugging purpose.
               t_Char czFileName[MAX_KEYSIZE] = { 0 };
               sprintf(czFileName, "/home/root/%d_AppListXML.txt", u32DeviceID);
               FileHandler oFile(czFileName, SPI_EN_RDWR);
               if (pcVal != NULL)
               {
                  oFile.bFWrite((signed char*) pcVal, strlen(pcVal));
               }//if(pcVal != NULL)
            }
               break;
            case e32FETCHAPPNAME:
            {
               MLAppName oAppName;
               oAppName.vSetDeviceHandle(u32DeviceID);
               oAppName.vSetUserContext(rUserContext);
               if ((NULL != pcVal) && (NULL != oAppName.m_pszAppName))
               {
                  t_String szAppName = pcKey;
                  t_U32 u32DelimPos = szAppName.find(":");
                  if (u32DelimPos != std::string::npos)
                  {
                     oAppName.m_u32AppHandle
                              = poMLVncDisc->u32GenerateUniqueAppId(szAppName.substr(0, u32DelimPos).c_str());
                     *(oAppName.m_pszAppName) = (NULL != pcVal) ? pcVal : coszUnKnownAppName.c_str();
                  }//if(u32DelimPos != std::string::npos)
               }//if(NULL != pcVal)
               poMsgQinterface->bWriteMsgToQ(&oAppName, sizeof(oAppName));
            }
               break;
            case e32FETCH_KEYICON_DOWNLOAD_ID:
            {
              poMLVncDisc->vPostKeyIconData(u32DeviceID, pcKey, pcVal, rUserContext);
            }
            break;

            default:
            {
               ETG_TRACE_ERR(("[ERR]:vFetchEntityValueCallback: Invalid "));
            }
               break;
         }//switch(rPostCbCtxt.rAppContext.u32ResID)
      }//if ((INVALID_DEVICE_ID != u32DeviceID) && (NULL != poMsgQinterface))
      poMLVncDisc->vFreeSdkString(pcVal);
   }//if ((NULL != poMLVncDisc) 
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vProcessAppList()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vProcessAppList(const t_U32& corfru32DeviceID, t_Char* pcVal, t_Bool bCertAppList,
         const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vProcessAppList:Device ID-0x%x", corfru32DeviceID));
   if (NULL != pcVal)
   {
      ETG_TRACE_USR2(("[PARAM]:vProcessAppList::AppList-%s", pcVal));
   }
   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      MLAppListMsg oAppListMsg;
      oAppListMsg.vSetDeviceHandle(corfru32DeviceID);
      oAppListMsg.vSetUserContext(corfrUsrCntxt);
      oAppListMsg.bCertAppList = bCertAppList;

      if (NULL != pcVal)
      {
         StringHandler oStrUtil;
         std::vector<t_String> vecszAppIds;
         oStrUtil.vSplitString(pcVal, cocAppListdelimiter, vecszAppIds);

         std::vector<t_U32> vecUniqueAppIDs;

         //generate the CRC for all the strings and store it in a map

         for (t_U16 u16Index = 0; u16Index < vecszAppIds.size(); u16Index++)
         {
            t_U32 u32UniqueAppID = u32GenerateUniqueAppId(vecszAppIds[u16Index].c_str());
            vecUniqueAppIDs.push_back(u32UniqueAppID);

            m_oMapAppLock.s16Lock();
            m_mapAppsInfo.insert(std::pair<tDevAppIDPair, t_String>(tDevAppIDPair(corfru32DeviceID, u32UniqueAppID),
                     vecszAppIds[u16Index].c_str()));
            m_oMapAppLock.vUnlock();

         }//for(t_U16 u16Index=0;u16Index < u16NumApps;u16Index++)

         if (NULL != oAppListMsg.pvecszAppIds)
         {
            *(oAppListMsg.pvecszAppIds) = vecUniqueAppIDs;
         }

      }//if(NULL != pcVal)

      poMsgQinterface->bWriteMsgToQ(&oAppListMsg, sizeof(oAppListMsg));
   }//if(NULL != poMsgQinterface)
}

/***************************************************************************
 ** FUNCTION:  t_String spi_tclMLVncDiscoverer::szGetAppID()
 ***************************************************************************/
t_String spi_tclMLVncDiscoverer::szGetAppId(const t_U32 cou32DevID, const t_U32 cou32AppID)
{
   t_String szAppId;

   m_oMapAppLock.s16Lock();
   if (SPI_MAP_NOT_EMPTY(m_mapAppsInfo))
   {
      tItAppsInfo itAppsInfo = m_mapAppsInfo.find(tDevAppIDPair(cou32DevID, cou32AppID));
      if (itAppsInfo != m_mapAppsInfo.end())
      {
         szAppId = itAppsInfo->second.c_str();
      }//if(itAppsInfo != m_mapAppsInfo.end())
   }//if(SPI_MAP_NOT_EMPTY(m_mapAppsInfo))
   m_oMapAppLock.vUnlock();

   return szAppId.c_str();
}

/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32GetAppId()
 ***************************************************************************/
t_U32 spi_tclMLVncDiscoverer::u32GetAppId(const t_U32& corfrdevID, t_String szAppId)
{
   t_U32 u32AppID = 0;

   m_oMapAppLock.s16Lock();

   if (SPI_MAP_NOT_EMPTY(m_mapAppsInfo))
   {
      for (tItAppsInfo itAppsInfo = m_mapAppsInfo.begin(); itAppsInfo != m_mapAppsInfo.end(); itAppsInfo++)
      {
         if ((itAppsInfo->first.first == corfrdevID) && (itAppsInfo->second == szAppId))
         {
            u32AppID = itAppsInfo->first.second;
         }//if((itAppsInfo->first.first == corfrdevID)
      }//for(tItAppsInfo itAppsInfo = m_mapAppsIn
   }//if(SPI_MAP_NOT_EMPTY(m_mapAppsInfo))

   m_oMapAppLock.vUnlock();

   return u32AppID;
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vPostAppIconData()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vPostAppIconData(t_U32 u32DeviceID, t_Char* pcKey, t_Char* pcVal,
         trUserContext rUsrCntxt)
{
   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
   if ((NULL != poMsgQinterface) && (NULL != pcKey))
   {
      const t_U8* cou8Data = NULL;
      t_U32 u32Size = 0;

      //Extract the Url that was passed to VNC SDK.
      //Remove the downloadIcon: from the key value and then send this URL to SPI.
      t_U32 u32StrIconBaselen = strlen(VNCDiscoverySDKMLDownloadIconBase);
      t_String str = pcKey;
      t_String szAppIconUrl;

      if (strlen(pcKey) > u32StrIconBaselen)
      {
         std::size_t found = str.find(VNCDiscoverySDKMLDownloadIconBase);
         if (found != t_String::npos)
         {
            szAppIconUrl = str.substr(u32StrIconBaselen);
         }//if (found != t_String::npos)
      }//if(strlen(pcKey)>u32StrIconBaselen)
      ETG_TRACE_USR2(("[PARAM]:vPostAppIconData:AppIconUrl-%s ", szAppIconUrl.c_str()));

      //get the discoverer pointer
      VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
      //get the entity pointer
      VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

      //If the pcVal is null, send the empty message to SPI, so that it can post error message to HMI
      if ((NULL != pcVal) && (NULL != m_poDiscoverySdk) && (NULL != poDisc) && (NULL != poEntity))
      {
         u32Size = m_poDiscoverySdk->getEntityBinaryData(poDisc, poEntity, pcVal, &cou8Data);
      }//if((NULL != pcVal) &&
      ETG_TRACE_USR4(("[PARAM]:vPostAppIconData:Icon Size-%d ", u32Size));

      MLAppIconData oAppIconData;
      oAppIconData.vSetDeviceHandle(u32DeviceID);
      oAppIconData.vSetUserContext(rUsrCntxt);
      oAppIconData.u32Size = u32Size;
      if (NULL != oAppIconData.pszAppIconUrl)
      {
         *(oAppIconData.pszAppIconUrl) = szAppIconUrl.c_str();
      }
      if ((u32Size > 0) && (NULL != cou8Data))
      {
         oAppIconData.pu8BinaryData = new t_U8[u32Size];
         if (NULL != oAppIconData.pu8BinaryData)
         {
            memcpy((t_Void*) oAppIconData.pu8BinaryData, cou8Data, u32Size);
         }//if(NULL != oAppIconData.pcou8BinaryData)
      }//if(u32Size>0)
      //Post the message to Dispatcher
      poMsgQinterface->bWriteMsgToQ(&oAppIconData, sizeof(oAppIconData));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vChooseDiscovererForDeviceCb()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vChooseDiscovererForDeviceCb(t_Void *pvSDKContext,
         const VNCDiscoverySDKDevice *poDevice,
         const VNCDiscovererRequestingAccess * const *ppDiscoverersRequestingAccess)
{
   SPI_INTENTIONALLY_UNUSED(poDevice);
   SPI_INTENTIONALLY_UNUSED(pvSDKContext);
   SPI_INTENTIONALLY_UNUSED(ppDiscoverersRequestingAccess);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vChooseDiscovererForDeviceCb()"));

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vCancelDeviceChoiceCb()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vCancelDeviceChoiceCb(t_Void *pvSDKContext, const VNCDiscoverySDKDevice *poDevice)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vCancelDeviceChoiceCb"));
   if ((NULL != poDevice) && (NULL != poDevice->pDeviceProviderName) && (NULL != poDevice->pDeviceProviderName))
   {

      ETG_TRACE_USR2(("[PARAM]:vCancelDeviceChoiceCb:: cancel choice: device: identifier: 0x%x name: %s", poDevice->deviceIdentifier, poDevice->pDeviceProviderName));
      ETG_TRACE_USR2(("[PARAM]:vCancelDeviceChoiceCb:: path: %s ", poDevice->pDeviceProviderPath));
   }
   SPI_INTENTIONALLY_UNUSED(pvSDKContext);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchDeviceInfo
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchDeviceInfo(t_U32 u32DeviceID)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchDeviceInfo"));

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);
   //Get the discoverer type
   tenDiscovererType enDiscType = enGetDiscovererType(u32DeviceID);

   trMLDeviceInfo rDeviceInfo;
   vRetrieveDeviceInfo(poDisc, poEntity, enDiscType, rDeviceInfo);

   //Call callbacks for the registered objects
   MLDeviceInfoMsg oDeviceInfoMsg;
   oDeviceInfoMsg.vSetDeviceHandle(u32DeviceID);
   if (NULL != (oDeviceInfoMsg.prDeviceInfo))
   {
      *(oDeviceInfoMsg.prDeviceInfo) = rDeviceInfo;
   }
   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oDeviceInfoMsg, sizeof(oDeviceInfoMsg));
   }//if (NULL != poMsgQinterface)
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vRetrieveDeviceInfo(VNCDiscove..
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vRetrieveDeviceInfo(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity *poEntity,
         tenDiscovererType enDiscType, trMLDeviceInfo& rfrDeviceInfo)
{
   SPI_INTENTIONALLY_UNUSED(enDiscType);
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vRetrieveDeviceInfo"));

   t_Char* pcVal = NULL;

   //Unique device Name
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootUdn, &pcVal)) && (NULL != pcVal))
   {
      rfrDeviceInfo.szDeviceUUID = pcVal;
      vFreeSdkString(pcVal);
   }//if( (true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscover

   //Fetch FriendlyName of the device
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootFriendlyName, &pcVal))
        && (NULL != pcVal))
   {
      rfrDeviceInfo.szFriendlyDeviceName = pcVal;
      vFreeSdkString(pcVal);
   }//if( (true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscover

   //! Fetch Manufacturer Name
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootManufacturer, &pcVal))
        && (NULL != pcVal))
   {
      rfrDeviceInfo.szDeviceManufacturerName = pcVal;
      vFreeSdkString(pcVal);
   }//if( (true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscover

   //! Fetch Model Name
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootModelName, &pcVal)) && (NULL != pcVal))
   {
      rfrDeviceInfo.szDeviceModelName = pcVal;
      vFreeSdkString(pcVal);
   }//if( (true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscover

   //! Fetch BT Address
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootBtAddr, &pcVal)) && (NULL != pcVal))
   {
      rfrDeviceInfo.szBTAddress = pcVal;
      std::transform(rfrDeviceInfo.szBTAddress.begin(),
               rfrDeviceInfo.szBTAddress.end(),
               rfrDeviceInfo.szBTAddress.begin(),
               ::toupper);
      vFreeSdkString(pcVal);
   }//if( (true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscover

   //Fetch vnccmd of the Device
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLUPnPServerURL, &pcVal)) && (NULL != pcVal))
   {
      rfrDeviceInfo.szVnccmd = pcVal;
      vFreeSdkString(pcVal);
   }//if( (true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscover

   //Mirrorlink Major version & Minor version
   t_U32 u32MLMajorVer = u32FetchKeyValue(poDisc, poEntity, VNCDiscoverySDKMLRootMajorVersion);
   //if there is any error in retrieving the ML version of the Phone, set the Major Version as 1.
   rfrDeviceInfo.rVersionInfo.u32MajorVersion = ( bIsInvalidMLMajorVersion(u32MLMajorVer) )
      ?(scou32ML_Major_Version_1):(u32MLMajorVer);
   rfrDeviceInfo.rVersionInfo.u32MinorVersion = u32FetchKeyValue(poDisc, poEntity, VNCDiscoverySDKMLRootMinorVersion);

   //We use VNC Discovery SDK for only MirrorLink devices.
   rfrDeviceInfo.enDeviceCategory = e8DEV_TYPE_MIRRORLINK;

   //Device is connected.
   rfrDeviceInfo.enDeviceConnectionStatus = e8DEV_CONNECTED;

   //ADIT has yet to add device connection type parameter.
   //currently hardcoded to usb.
   rfrDeviceInfo.enDeviceConnectionType = e8USB_CONNECTED;

   ETG_TRACE_USR2(("[PARAM]:vRetrieveDeviceInfo:vnccmd - %s", rfrDeviceInfo.szVnccmd.c_str()));
   ETG_TRACE_USR2(("[PARAM]:vRetrieveDeviceInfo:UDN - %s", rfrDeviceInfo.szDeviceUUID.c_str()));

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer:: bFetchEntityValueBlocking(V...
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bFetchEntityValueBlocking(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, const t_Char* pcocKey, t_Char** ppcVal,
         MLDiscoverySDKTimeoutMicroseconds s32TimeOut) const
{
   t_Bool bRet = false;

   if ((NULL != m_poDiscoverySdk) && (NULL != poEntity) && (NULL != poDisc) && (NULL != pcocKey))
   {
      //Get the error code, if any
      t_S32 s32Error = m_poDiscoverySdk->fetchEntityValueBlocking(poDisc, poEntity, pcocKey, ppcVal, s32TimeOut);

      bRet = (cs32MLDiscoveryErrorNone == s32Error) ? true : false;
      ETG_TRACE_USR4(("[PARAM]:bFetchEntityValueBlocking: Error - %d , Key - %s ", s32Error, pcocKey));
   }// if ((NULL != m_poDiscoverySdk) && (NULL 
   else
   {
      ETG_TRACE_ERR(("[ERR]:bFetchEntityValueBlocking: either m_poDiscoverySdk or poDisc or poEntity or Key are null"));
   } // else
   return bRet;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer:: bFetchEntityValueBlocking(V...
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bFetchEntityValueBlocking(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity, const t_String& corfszKey, 
         const t_String& corfszAppId, t_Char** ppcVal,
         MLDiscoverySDKTimeoutMicroseconds s32TimeOut) const
{
   t_Bool bRet = false;
   t_Char czKey[MAX_KEYSIZE] = { 0 };
   if (false == corfszKey.empty())
   {
      //Get the key for the given AppId
      snprintf(czKey, MAXKEYSIZE - 1, corfszKey.c_str(), corfszAppId.c_str());

      if ((NULL != m_poDiscoverySdk) && (NULL != poEntity) && (NULL != poDisc))
      {
         //Get the error code, if any
         t_S32 s32Error = m_poDiscoverySdk->fetchEntityValueBlocking(poDisc, poEntity, czKey, ppcVal, s32TimeOut);

         bRet = (cs32MLDiscoveryErrorNone == s32Error) ? true : false;
         ETG_TRACE_USR4(("bFetchEntityValueBlocking: s32Error - %d , Key - %s ", s32Error, czKey));
      }
      else
     {
        //Send error for null pointer input
        ETG_TRACE_ERR(("bFetchEntityValueBlocking: either m_poDiscoverySdk or poDisc or poEntity or Key are null"));
     }
   }
   return bRet;
}

/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer:: u32FetchEntityValue(VNCDiscove...
 ***************************************************************************/
t_U32 spi_tclMLVncDiscoverer::u32FetchEntityValue(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity *poEntity,
         const t_Char* pcocKey, MLDiscoverySDKTimeoutMicroseconds s32TimeOut) const
{

   t_U32 u32ReqId = cu32MLDiscoveryNoRequestId;

   //Passing in a NULL Entity or pcocKey also results in zero request id
   if ((NULL != m_poDiscoverySdk) && (NULL != poEntity) && (NULL != poDisc) && (NULL != pcocKey))
   {
      //Get the request Id
      u32ReqId = m_poDiscoverySdk->fetchEntityValue(poDisc, poEntity, pcocKey, s32TimeOut);
   }//if( (NULL != m_poDiscoverySdk)&&(NULL != poEntity)&&(NULL != poDisc)
   else
   {
      ETG_TRACE_ERR(("[ERR]:u32FetchEntityValue: either m_poDiscoverySdk or poDisc or poEntity or pcocKey is NULL"));
   }//else

   return u32ReqId;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer:: vPostEntityValueBlocking(V...
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bPostEntityValueBlocking(VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity *poEntity, const t_Char* pcocKey, const t_Char* pcocValue,
         MLDiscoverySDKTimeoutMicroseconds s32TimeOut) const
{
   t_Bool bRet = false;
   if ((NULL != m_poDiscoverySdk) && (NULL != poEntity) && (NULL != poDisc) && (NULL != pcocKey))
   {
      t_S32 s32Error = cs32MLDiscoveryUnknownError;

      //Get the Error code if any
      s32Error = m_poDiscoverySdk->postEntityValueBlocking(poDisc, poEntity, pcocKey, pcocValue, s32TimeOut);

      bRet = (cs32MLDiscoveryErrorNone == s32Error);
      ETG_TRACE_USR4(("[PARAM]:bPostEntityValueBlocking:s32Error-%d, Key-%s ", s32Error, pcocKey));

      if (NULL != pcocValue)
      {
         ETG_TRACE_USR4(("[PARAM]:bPostEntityValueBlocking: Value-%s ", pcocValue));
      }

   }

   return true;
}
/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32PostEntityValue(VNCDiscovery..
 ***************************************************************************/
t_U32 spi_tclMLVncDiscoverer::u32PostEntityValue(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity *poEntity,
         const t_Char* pcocKey, const t_Char* pcocValue, MLDiscoverySDKTimeoutMicroseconds s32TimeOut) const
{

   t_U32 u32ReqId = cu32MLDiscoveryNoRequestId;
   //Passing in a NULL Entity or pcocKey also results in zero request id
   if ((NULL != m_poDiscoverySdk) && (NULL != poEntity) && (NULL != poDisc) && (NULL != pcocKey))
   {
      //get the request id
      u32ReqId = m_poDiscoverySdk->postEntityValue(poDisc, poEntity, pcocKey, pcocValue, s32TimeOut);
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:u32PostEntityValue: either m_poDiscoverySdk or poDisc or poEntity or Key are null"));
   }

   return u32ReqId;
}

/***************************************************************************
 ** FUNCTION:  t_U16 spi_tclMLVncDiscoverer::u32GenerateUniqueDeviceId(tenDis...
 ***************************************************************************/
t_U32 spi_tclMLVncDiscoverer::u32GenerateUniqueDeviceId(t_String szUniqueName) const
{
   t_U32 u32UniqueId = u16CalcCRC16((t_UChar*) (const_cast<t_Char*>(szUniqueName.c_str())), 
      szUniqueName.size());
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::u32GenerateUniqueDeviceId:UniqueId - 0x%x ", u32UniqueId));

   return u32UniqueId;
}

/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclMLVncDiscoverer::u32GenerateUniqueAppId(tenDis...
 ***************************************************************************/
t_U32 spi_tclMLVncDiscoverer::u32GenerateUniqueAppId(t_String szUniqueId) const
{
   t_U32 u32UniqueId = u32CalcCRC32((t_UChar*) (const_cast<t_Char*>(szUniqueId.c_str())), 
      szUniqueId.size());
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::u32GenerateUniqueAppId:UniqueId - 0x%x ", u32UniqueId));

   return u32UniqueId;
}

/***************************************************************************
 ** FUNCTION:  tenDiscovererType spi_tclMLVncDiscoverer::enGetDiscovererT..
 ***************************************************************************/
tenDiscovererType spi_tclMLVncDiscoverer::enGetDiscovererType(VNCDiscoverySDKDiscoverer* poDisc)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::enGetDiscovererType()"));

   tenDiscovererType enDiscType = enUnKnownDiscoverer;

   if (NULL != poDisc)
   {
      for (itMapDiscTypes = mapDiscTypes.begin(); itMapDiscTypes != mapDiscTypes.end(); itMapDiscTypes++)
      {
         //get the discoverer type
         if (poDisc == itMapDiscTypes->second)
         {
            enDiscType = itMapDiscTypes->first;
         }
      }
   }
   return enDiscType;
}

/***************************************************************************
 ** FUNCTION:  tenDiscovererType spi_tclMLVncDiscoverer::enGetDiscovererT..
 ***************************************************************************/
tenDiscovererType spi_tclMLVncDiscoverer::enGetDiscovererType(t_U32 u32DeviceID)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::enGetDiscovererType : Device ID 0x%x ", u32DeviceID));

   tenDiscovererType enDiscType = enUnKnownDiscoverer;

   m_oMapDevLock.s16Lock();

   if ((INVALID_DEVICE_ID != u32DeviceID) && (0 != m_mapDevicesInfo.size()))
   {
      //get the Entity pointer based on unique id.
      itMapDevicesInfo = m_mapDevicesInfo.find(u32DeviceID);

      //If the key it self is not available , find returns map::end()
      if (itMapDevicesInfo != m_mapDevicesInfo.end())
      {
         enDiscType = (itMapDevicesInfo->second).enDiscType;
      }
   }
   m_oMapDevLock.vUnlock();

   return enDiscType;
}

/***************************************************************************
 ** FUNCTION:  VNCDiscoverySDKEntity* spi_tclMLVncDiscoverer::poGetEntity..
 ***************************************************************************/
VNCDiscoverySDKEntity* spi_tclMLVncDiscoverer::poGetEntity(t_U32 u32DeviceID)
{

   VNCDiscoverySDKEntity* poEntity = NULL;

   m_oMapDevLock.s16Lock();

   if ((INVALID_DEVICE_ID != u32DeviceID) && (0 != m_mapDevicesInfo.size()))
   {
      //get the Entity pointer based on unique id.
      itMapDevicesInfo = m_mapDevicesInfo.find(u32DeviceID);

      //If the key it self is not available , find returns map::end()
      if ((itMapDevicesInfo != m_mapDevicesInfo.end()) && (NULL != (itMapDevicesInfo->second).poEntity))
      {
         poEntity = (itMapDevicesInfo->second).poEntity;
      }
   }

   m_oMapDevLock.vUnlock();

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::poGetEntity: Entity - %p", poEntity));
   return poEntity;

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetUniqueDeviceId(VNCDisc...
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vGetUniqueDeviceId(VNCDiscoverySDKDiscoverer* poDisc, VNCDiscoverySDKEntity* poEntity,
         t_U32& rfu32DeviceID)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vGetUniqueDeviceId() entered"));

   rfu32DeviceID = 0;

   if ((NULL != poDisc) && (NULL != poEntity))
   {
      m_oMapDevLock.s16Lock();

      //Check for the entity pointer in device info list and get unique id
      for (itMapDevicesInfo = m_mapDevicesInfo.begin(); itMapDevicesInfo != m_mapDevicesInfo.end(); itMapDevicesInfo++)
      {
         if ((poEntity == itMapDevicesInfo->second.poEntity) && (poDisc == (itMapDevicesInfo->second.poDiscoverer)))
         {
            rfu32DeviceID = itMapDevicesInfo->first;
         }
      }

      m_oMapDevLock.vUnlock();
   }

}

/***************************************************************************
 ** FUNCTION:  VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscoverer::poGetD...
 ***************************************************************************/
VNCDiscoverySDKDiscoverer* spi_tclMLVncDiscoverer::poGetDiscoverer(t_U32 u32DeviceID)
{
   VNCDiscoverySDKDiscoverer* poDisc = NULL;

   m_oMapDevLock.s16Lock();

   if ((INVALID_DEVICE_ID != u32DeviceID) && (0 != m_mapDevicesInfo.size()))
   {
      //get the Entity pointer based on unique id.
      itMapDevicesInfo = m_mapDevicesInfo.find(u32DeviceID);

      //If the key it self is not available , find returns map::end()
      if ((itMapDevicesInfo != m_mapDevicesInfo.end()) && (NULL != (itMapDevicesInfo->second).poDiscoverer))
      {
         poDisc = (itMapDevicesInfo->second).poDiscoverer;
      }
   }

   m_oMapDevLock.vUnlock();

   ETG_TRACE_USR4(("spi_tclMLVncDiscoverer::poGetDiscoverer: poDisc = %p", poDisc));

   return poDisc;
}

/***************************************************************************
 ** FUNCTION: const VNCDiscoverySDK* spi_tclMLVncDiscoverer::poGetDiscSdk()
 ***************************************************************************/
const VNCDiscoverySDK* spi_tclMLVncDiscoverer::poGetDiscSdk() const
{
   //@todo - Apply Lock

   //Return Discovery SDK pointer
   return m_poDiscoverySdk;

}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bLaunchApplication(const
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bLaunchApplication(const t_U32 cou32DeviceHandle, t_U32 u32AppId)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bLaunchApplication: DeviceID-0x%x AppID-0x%x", cou32DeviceHandle, u32AppId));
   t_Bool bRet = false;
   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DeviceHandle);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DeviceHandle);
   t_String szAppId = szGetAppId(cou32DeviceHandle, u32AppId);

   if ((NULL != m_poDiscoverySdk) && (NULL != poDisc) && (NULL != poEntity) && (cszEmptyStr != szAppId))
   {
      t_S32 s32Error = m_poDiscoverySdk->postEntityValueBlocking(poDisc,
               poEntity,
               szAppId.c_str(),
               VNCDiscoverySDKMLLaunchApplicationValue,
               VNCDiscoverySDKNoTimeout);
      bRet = (VNCDiscoverySDKErrorNone == s32Error);
      ETG_TRACE_USR2(("[PARAM]:bLaunchApplication:Error-%d", s32Error));
   }

   return bRet;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::vFetchDeviceVersion()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchDeviceVersion(const t_U32 cou32DeviceHandle, trVersionInfo& rfrVersionInfo)
{
   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DeviceHandle);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DeviceHandle);

   //Mirror link Major Version & minor version
   t_U32 u32MLMajorVer = u32FetchKeyValue(poDisc, poEntity, VNCDiscoverySDKMLRootMajorVersion);
   rfrVersionInfo.u32MajorVersion = ( bIsInvalidMLMajorVersion(u32MLMajorVer) )
      ?(scou32ML_Major_Version_1):(u32MLMajorVer);

   rfrVersionInfo.u32MinorVersion = u32FetchKeyValue(poDisc, poEntity, VNCDiscoverySDKMLRootMinorVersion);
   rfrVersionInfo.u32PatchVersion=0;

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchDeviceVersion:DevID-0x%x Major ver-%d Minor ver-%d",
      cou32DeviceHandle,rfrVersionInfo.u32MajorVersion,rfrVersionInfo.u32MinorVersion));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetAppIconData()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vGetAppIconData(const t_U32 cou32DeviceHandle, t_String szAppIconUrl,
         const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vGetAppIconData: Dev-0x%x, URL-%s", cou32DeviceHandle, szAppIconUrl.c_str()));

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DeviceHandle);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DeviceHandle);

   if ((NULL != m_poDiscoverySdk) && (NULL != poDisc) && (NULL != poEntity))
   {
      t_Char czKey[MAX_KEYSIZE] = { 0 };

      sprintf(czKey, VNCDiscoverySDKMLDownloadIcon, szAppIconUrl.c_str());
      t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, czKey, VNCDiscoverySDKNoTimeout);

      //Store the request ID. Will be used in callback function
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext = rfrcUsrCntxt;
      rMsgCtxt.rAppContext.u32ResID = e32FETCH_APPICON_DOWNLOAD_ID;
      m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

      if (cu32MLDiscoveryNoRequestId == u32ReqId)
      {
         ETG_TRACE_ERR(("[ERR]: vGetAppIconData: Error in sending Request "));
      }//if( cu32MLDiscoveryNoRequestId == u32ReqId )
   }//if( (NULL != m_poDiscoverySdk) && ( NULL != poDisc ) && ( NULL != poEntity) )
}
/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bGetAppIDforProtocol()
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bGetAppIDforProtocol(const t_U32 cou32DeviceID, const t_String &corfrszProtocolID,
         t_U32 &rfru32AppID)
{
   t_Bool bRetVal = false;

   std::map<std::pair<t_U32, t_String>, t_U32>::iterator itmapProtocolInfo;

   itmapProtocolInfo = m_mapProtocolInfo.find(std::pair<t_U32, t_String>(cou32DeviceID, corfrszProtocolID));

   if (m_mapProtocolInfo.end() != itmapProtocolInfo)
   {
      bRetVal = true;
      rfru32AppID = itmapProtocolInfo->second;
   }

   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bGetAppIDforProtocol : DeviceID-0x%x AppID-0x%x bRetVal-%d ProtocolID-%s ", 
      cou32DeviceID, rfru32AppID, bRetVal, corfrszProtocolID.c_str()));

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchProtocolID()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchProtocolID(const t_U32 cou32DeviceID, const t_Char* pcoczAppList,
         trUserContext &rfrUsrCtxt)
{
   SPI_INTENTIONALLY_UNUSED(rfrUsrCtxt);
   if (NULL != pcoczAppList)
   {
      ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchProtocolID : AppList-%s ", pcoczAppList));
   }
   StringHandler oStrHandler;

   //! convert the applist to a vector
   std::vector<t_String> vecAppList;
   oStrHandler.vSplitString(pcoczAppList, ';', vecAppList);

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DeviceID);

   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DeviceID);

   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();

   ETG_TRACE_USR2(("[PARAM]:vFetchProtocolID : Applist size-%d", vecAppList.size()));
   //! Fetch protocol ID for all Apps
   if (NULL != poMsgQinterface)
   {
      for (t_U32 u32AppCount = 0; u32AppCount < vecAppList.size(); u32AppCount++)
      {
         //! Construct the key with appid to fetch protocol ID
         t_Char *pczProtocolID = NULL;

         t_U32 u32AppID = u32GenerateUniqueAppId(vecAppList[u32AppCount].c_str());
         t_String szProtocolID;
         if (true == bFetchEntityValueBlocking(poDisc, poEntity, 
             VNCDiscoverySDKMLAppRemotingProtocol, vecAppList[u32AppCount], &pczProtocolID))
         {
            szProtocolID = pczProtocolID;
            vFreeSdkString(pczProtocolID);
            ETG_TRACE_USR2(("[PARAM]:vFetchProtocolID : DeviceID-0x%x,u32AppID-0x%x, szProtocolID-%s", 
               cou32DeviceID, u32AppID, szProtocolID.c_str()));

            m_mapProtocolInfo.insert(std::pair<std::pair<t_U32, t_String>, t_U32>(std::pair<t_U32, t_String>(cou32DeviceID,
                     szProtocolID),
                     u32AppID));
         }
      }
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSubscribeForDeviceEvents()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vSubscribeForDeviceEvents(const t_U32 cou32DeviceHandle, t_Bool bSubscribe,
         const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vSubscribeForDeviceEvents:Dev-0x%x Subscribe-%d", 
      cou32DeviceHandle, ETG_ENUM(BOOL,bSubscribe)));

   t_Char czVal[MAXKEYSIZE] = { 0 };
   t_Char czKey[MAXKEYSIZE] = { 0 };

   bSubscribe ? snprintf(czVal, MAXKEYSIZE - 1, VNCDiscoverySDKMLAppEventsSubscribe) : snprintf(czVal,
            MAXKEYSIZE - 1,
            VNCDiscoverySDKMLAppEventsUnsubscribe);

   spi_tclVncSettings* poVNCSettings = spi_tclVncSettings::getInstance();
   if (NULL != poVNCSettings)
   {
      t_U16 u16UPnPEventsEnabledFromThePortNo = poVNCSettings->u16GetUPnPAppEventsEnabledFromThePort();
      if (0 != u16UPnPEventsEnabledFromThePortNo)
      {
         static t_U16 u16EnableUPnPEventsOnPort = u16UPnPEventsEnabledFromThePortNo;
         t_U8 u8UPnPEventsEnabledOnNumOfPorts = poVNCSettings->u8TotalUPnPAppEventsEnabledPorts();

         //Port number is more than the largest firewall disabled port. Reset the number to initial
         //port number from which the firewall is disabled
         if (u16EnableUPnPEventsOnPort > (u16UPnPEventsEnabledFromThePortNo + u8UPnPEventsEnabledOnNumOfPorts - 1))
         {
            u16EnableUPnPEventsOnPort = u16UPnPEventsEnabledFromThePortNo;
         }//if(u16EnableUPnPEventsOnPort > (u16UPnPEv

         ETG_TRACE_USR2(("[DESC]:UPnP Notifications are enabled on the Port-%d", u16EnableUPnPEventsOnPort));
         snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLAppEventsSubscriptionWithPort, u16EnableUPnPEventsOnPort);

         //Increment the port number on every un subscription request
         u16EnableUPnPEventsOnPort = (false == bSubscribe)
                                                           ? (u16EnableUPnPEventsOnPort + 1)
                                                           : u16EnableUPnPEventsOnPort;
      }
      else
      {
         //port numbers are not defined in the settings, use any port
         ETG_TRACE_USR2(("[DESC]:UPnP Notifications are enabled on all Ports"));
         snprintf(czKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLAppEventsSubscription);
      }

      //get the discoverer pointer
      VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DeviceHandle);
      //get the entity pointer
      VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DeviceHandle);

      if ((NULL != m_poDiscoverySdk) && (NULL != poDisc) && (NULL != poEntity))
      {

         t_U32 u32ReqId = u32PostEntityValue(poDisc, poEntity, czKey, czVal, VNCDiscoverySDKNoTimeout);

         ETG_TRACE_USR4(("[PARAM]:vSubscribeForDeviceEvents:Request ID-%d ", u32ReqId));

         //Store the request ID. Will be used in callback function
         trMsgContext rMsgCtxt;
         rMsgCtxt.rUserContext = corfrUsrCntxt;
         rMsgCtxt.rAppContext.u32ResID = e32EVENTSUBSCRIPTION;
         m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);
      }//if( (NULL != m_poDiscoverySdk)&&(NULL != poDisc)&&(NULL != poEntity) )
   }//if(NULL != poVNCSettings)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFreeSdkString()
 ***************************************************************************/
inline t_Void spi_tclMLVncDiscoverer::vFreeSdkString(t_Char*& czStr)
{
   if ((NULL != m_poDiscoverySdk) && (NULL != czStr))
   {
      m_poDiscoverySdk->freeString(czStr);
      czStr = NULL;
   }//if((NULL != m_poDiscoverySdk) && (NULL != czStr) )
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDisplayAppcertificationInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vDisplayAppcertificationInfo(const t_U32 cou32DevHandle, const t_U32 cou32AppHandle)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vDisplayAppcertificationInfo: DeviceId- 0x%x, AppId-0x%x", cou32DevHandle, cou32AppHandle));

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DevHandle);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DevHandle);

   t_String szAppId = szGetAppId(cou32DevHandle, cou32AppHandle);

   //Buffer to write the VNC Keys along with AppId, to fecth values from SDK.
   t_Char cKey[MAX_KEYSIZE] = { 0 };
   t_Char* czResult = NULL;

   //VNCDiscoverySDKMLGetApplicationCertificateInfo - if the Get CertificationInfo 
   //request is successful, then fetch the application certification info. 
   //This must be fetched, to retrieve the Application certification details.
   sprintf(cKey, VNCDiscoverySDKMLGetApplicationCertificateInfo, szAppId.c_str());
   if (true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult, cs32MLDiscoveryNoTimeout))
   {
      if (NULL != czResult)
      {
         vFreeSdkString(czResult);
      }//if(NULL != czResult)
      StringHandler oStrUtil;

      sprintf(cKey, VNCDiscoverySDKMLAppCertEntityCount, szAppId.c_str());
      t_U8 u8AppCertEntityCount = 0;
      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult)) && (NULL != czResult))

      {
         oStrUtil.vSetString(czResult);
         u8AppCertEntityCount = (t_U8) oStrUtil.u32ConvertStrToInt();
         vFreeSdkString(czResult);
      }//if( (true == bFetchEntityValueBlocking(poDisc, p
      ETG_TRACE_USR4(("[PARAM]:vDisplayAppcertificationInfo:AppCertEntityCount - %d", u8AppCertEntityCount));

      for (t_U8 u8Index = 0; u8Index < u8AppCertEntityCount; u8Index++)
      {
         ETG_TRACE_USR4(("[PARAM]:vDisplayAppcertificationInfo:CertEntity Index - %d", u8Index));

         sprintf(cKey, VNCDiscoverySDKMLAppCertEntityName, szAppId.c_str(), u8Index);
         if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult)) && (NULL != czResult))
         {
            ETG_TRACE_USR4(("[PARAM]:vDisplayAppcertificationInfo:CertEntityName - %s", czResult));
            vFreeSdkString(czResult);
         }//if( (true == bFetchEntityValueBlocking(poDisc, p

         sprintf(cKey, VNCDiscoverySDKMLAppCertEntityRestricted, szAppId.c_str(), u8Index);
         if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult)) && (NULL != czResult))
         {
            ETG_TRACE_USR4(("[PARAM]:vDisplayAppcertificationInfo:Restricted list - %s", czResult));
            vFreeSdkString(czResult);
         }//if( (true == bFetchEntityValueBlocking(poDisc, p

         sprintf(cKey, VNCDiscoverySDKMLAppCertEntityNonRestricted, szAppId.c_str(), u8Index);
         if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &czResult)) && (NULL != czResult))
         {
            ETG_TRACE_USR4(("[PARAM]:vDisplayAppcertificationInfo:Non Restricted list - %s", czResult));
            vFreeSdkString(czResult);
         }//if( (true == bFetchEntityValueBlocking(poDisc, p
      }//for(t_U8 u8Index=0;u8Index<rAppInfo.u8AppCertEntityCount;u8Index++)
   }//if(true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &pcV

}


/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bSetClientProfile()
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bSetClientProfile(const t_U32 cou32DevHandle, const trClientProfile& corfrClientProfile,
         const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bSetClientProfile: DeviceID-0x%x", cou32DevHandle));

   t_S32 s32ReqId = 0;
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DevHandle);
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DevHandle);

   if ((NULL != m_poDiscoverySdk) && (NULL != poDisc) && (NULL != poEntity))
   {
      t_Char* czResult = NULL;

      //It is recommended to call VNCDiscoverySDKMLGetClientProfile before setting any fields. 
      //This is to ensure that some existing settings from the server are not removed unintentionally. 
      //Also some servers seem to require some of the options which they have set themselves, so not 
      //retrieving the existing profile, might mean that the new profile will not be accepted.

      //Currently these settings are made on the default active profile "0". So every device by default 
      //support the 1 profile ID and the profile ID is "0". SO these client profile settings are 
      //made on the profile ID "0"
      if ((true == bFetchEntityValueBlocking(poDisc,
               poEntity,
               VNCDiscoverySDKMLGetClientProfile,
               &czResult,
               VNCDiscoverySDKNoTimeout)) && (NULL != czResult))
      {
         ETG_TRACE_USR2(("bSetClientProfile:GetClientProfile-%s", czResult));
         vFreeSdkString(czResult);

         if (cszEmptyStr != corfrClientProfile.szClientProfileId)
         {
            //Set Client Profile Id
            bPostEntityValueBlocking(poDisc,
                     poEntity,
                     VNCDiscoverySDKMLProfileClientId,
                     corfrClientProfile.szClientProfileId.c_str());
         }//if( cszEmptyStr != corfrClientProfile.szClientProfileId )
         if (cszEmptyStr != corfrClientProfile.szProfileManufacturer)
         {
            //Set Manufacturers Name ( OEM )
            bPostEntityValueBlocking(poDisc,
                     poEntity,
                     VNCDiscoverySDKMLProfileManufacturer,
                     corfrClientProfile.szProfileManufacturer.c_str());
         }//if( cszEmptyStr != corfrClientProfile.szProfileManufacturer )

         //Set the ML Najor & minor versions supported by the Client
         vSetMLVersion(corfrClientProfile, poDisc, poEntity);

         //Set the Icon Preferences that the ML Client wanted to receive.
         //Set the Fields only, if they are not empty strings.
         vSetIconPreferences(corfrClientProfile, poDisc, poEntity);

         //Set the BT address of the ML Client - Set it only if it is not NULL
         if (cszEmptyStr != corfrClientProfile.szProfileBdAddr)
         {
            bPostEntityValueBlocking(poDisc,
                     poEntity,
                     VNCDiscoverySDKMLProfileBdAddr,
                     corfrClientProfile.szProfileBdAddr.c_str());
         }//if( cszEmptyStr != corfrClientProfile.szProfileBdAddr )

         if (false == corfrClientProfile.szRtpPayLoadTypes.empty())
         {
            bPostEntityValueBlocking(poDisc,
                     poEntity,
                     VNCDiscoverySDKMLProfileRtpPayloadType,
                     corfrClientProfile.szRtpPayLoadTypes.c_str());
         }

         //Set the Notification UI and MaxActions support of the ML client.
         vSetNotificationSupport(corfrClientProfile, poDisc, poEntity);

         //Set the Friendly Name of the ML Client - Set it only if it is not empty
         if( cszEmptyStr != corfrClientProfile.szFriendlyName )
         {
            bPostEntityValueBlocking(poDisc,poEntity,VNCDiscoverySDKMLProfileFriendlyName,
               corfrClientProfile.szFriendlyName.c_str());
         }//if( cszEmptyStr != corfrClientProfile.szFriendlyName )

         s32ReqId = s32SendClientProfileMsg(poDisc, poEntity, corfrUsrCntxt);
      }//if( (true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLGetClientProfil
   }//if((NULL != m_poDiscoverySdk)

   return (cu32MLDiscoveryNoRequestId != s32ReqId);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetMLVersion()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vSetMLVersion(
       const trClientProfile& corfrClientProfile,
	   VNCDiscoverySDKDiscoverer* poDisc,
	   VNCDiscoverySDKEntity* poEntity)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vSetMLVersion entered"));
   if (ML_MAJOR_VERSION <= corfrClientProfile.u32ProfileMajorVersion)
   {
      StringHandler oStrUtil;
      //Set Max ML Major Version supported by the ML Client
      t_String szMajorVersion;
      oStrUtil.vConvertIntToStr(corfrClientProfile.u32ProfileMajorVersion, szMajorVersion, DECIMAL_STRING);
      bPostEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLProfileMajorVersion, szMajorVersion.c_str());

      //Set Max ML Minor Version supported by the ML Client
      t_String szMinorVersion;
      oStrUtil.vConvertIntToStr(corfrClientProfile.u32ProfileMinorVersion, szMinorVersion, DECIMAL_STRING);
      bPostEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLProfileMinorVersion, szMinorVersion.c_str());
   }//if(ML_MAJOR_VERSION <= corfrClientProfile.u32ProfileMajorVersion)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetIconPreferences()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vSetIconPreferences(
       const trClientProfile& corfrClientProfile,
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vSetIconPreferences entered"));
   if ((0 != corfrClientProfile.rIconPreferences.u32IconWidth) && 
      (0 != corfrClientProfile.rIconPreferences.u32IconHeight) && 
      (0 != corfrClientProfile.rIconPreferences.u32IconDepth) && 
      (e8ICON_INVALID != corfrClientProfile.rIconPreferences.enIconMimeType))
   {
      StringHandler oStrUtil;
      t_String szIconPreferences;
      //Icon Width
      oStrUtil.vConvertIntToStr(corfrClientProfile.rIconPreferences.u32IconWidth,
        szIconPreferences, DECIMAL_STRING);
      bPostEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLProfileIconWidth, szIconPreferences.c_str());

      //Icon Height
      szIconPreferences = "";
      oStrUtil.vConvertIntToStr(corfrClientProfile.rIconPreferences.u32IconHeight,
        szIconPreferences, DECIMAL_STRING);
      bPostEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLProfileIconHeight, szIconPreferences.c_str());

      //Icon Depth
      szIconPreferences = "";
      oStrUtil.vConvertIntToStr(corfrClientProfile.rIconPreferences.u32IconDepth,
        szIconPreferences, DECIMAL_STRING);
      bPostEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLProfileIconDepth, szIconPreferences.c_str());

      //Icon MIME Type
      switch (corfrClientProfile.rIconPreferences.enIconMimeType)
      {
         case e8ICON_PNG:
           szIconPreferences = cszImagePng;
           break;
         case e8ICON_JPEG:
           szIconPreferences = cszImageJpeg;
           break;
         default:
           szIconPreferences = cszEmptyStr.c_str();
      }//switch(corfrClientProfile.enProfileIconMimeType)
      bPostEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLProfileIconMimeType, szIconPreferences.c_str());

   }//if( ( 0 != corfrClientProfile.u32ProfileIconWidth)&&(0 != corfrClientProfile.u32ProfileIconHeight)&&
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetNotificationSupport()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vSetNotificationSupport(
       const trClientProfile& corfrClientProfile,
       VNCDiscoverySDKDiscoverer* poDisc,
       VNCDiscoverySDKEntity* poEntity)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vSetNotificationSupport entered"));
   //Set the Notification UI support of the ML Client - Set it only if it is not false (default)
   if (false != corfrClientProfile.bNotiUISupport)
   {
      t_String szNotiUISupport = "true";
      bPostEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLProfileNotiUiSupport, szNotiUISupport.c_str());
   }//if( false != corfrClientProfile.bNotiUISupport )

   //Set the Notification MaxActions support of the ML Client - Set it only if it is not 2 (default)
   if (cou8DefaultMLNotiActions != corfrClientProfile.u8NotiMaxActions)
   {
      StringHandler oStrUtil;
      t_String szNotiMaxActions;
      oStrUtil.vConvertIntToStr(corfrClientProfile.u8NotiMaxActions, szNotiMaxActions, DECIMAL_STRING);
      bPostEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLProfileMaxActions, szNotiMaxActions.c_str());
   }//if( 2 != corfrClientProfile.u8NotiMaxActions )
}

/***************************************************************************
 ** FUNCTION:  t_S32 spi_tclMLVncDiscoverer::s32SendClientProfileMsg()
 ***************************************************************************/
t_S32 spi_tclMLVncDiscoverer::s32SendClientProfileMsg(
      VNCDiscoverySDKDiscoverer* poDisc,
      VNCDiscoverySDKEntity* poEntity,
      const trUserContext& corfrUsrCntxt)
{
   t_S32 s32ReqId = 0;
   //Send (commit) a configured client profile message to the service, 
   //with the client profile that has been configured so far. All the settings that we have made will be 
   //effective, if the SetClientProfile request is successful.
   s32ReqId = u32PostEntityValue(poDisc, poEntity,
              VNCDiscoverySDKMLSetClientProfile,
              NULL, VNCDiscoverySDKNoTimeout);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corfrUsrCntxt;
   rMsgCtxt.rAppContext.u32ResID = e32SETCLIENTPROFILE;
   m_oMsgContext.vAddMsgContext(s32ReqId, rMsgCtxt);
   return s32ReqId;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclMLVncDiscoverer::vDisplayAppListXml()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vDisplayAppListXml(const t_U32 cou32DevHandle)
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscoverer::vDisplayAppListXml:Device ID-0x%x ", cou32DevHandle));

   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DevHandle);
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DevHandle);

   t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, VNCDiscoverySDKMLAppListXmlBuffer, VNCDiscoverySDKNoTimeout);

   //Store the request ID. Will be used in callback function
   //Response ID & user context is not required currently.
   //If the XML reader is available, we can exteand and use this in the future
   // to fetch the application's info
   trMsgContext rMsgCtxt;
   rMsgCtxt.rAppContext.u32ResID = e32APPLISTXML;
   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vDisaplyAppListXml: Error in Request "));
   }//if( cu32MLDiscoveryNoRequestId == u32ReqId )

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::bIsDeviceBlacklisted()
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bIsDeviceBlacklisted(t_S32 s32ProductID, t_S32 s32VendorID)
{
   t_Bool bRetVal = false;

   /*lint -esym(40,first)first Undeclared identifier */
   /*lint -esym(40,second)second Undeclared identifier */
   //! Check if the device is blacklisted
   std::multimap<t_S32, t_S32> rExcludedDevList;
   spi_tclVncSettings *poVncSettings = spi_tclVncSettings::getInstance();
   if (NULL != poVncSettings)
   {
      poVncSettings->vGetExcludedDeviceList(rExcludedDevList);
   }

   auto itDevList = rExcludedDevList.find(s32VendorID);

   if (itDevList != rExcludedDevList.end())
   {
      ETG_TRACE_ERR(("[ERR]:bIsDeviceBlacklisted: Vendor ID matches "));
      auto itRange = rExcludedDevList.equal_range(itDevList->first);

      for (auto itProdIDs = itRange.first; itProdIDs != itRange.second; itProdIDs++)
      {
         if ((s32ProductID == itProdIDs->second) || (scos32AnyProductID == (itProdIDs->second)))
         {
            ETG_TRACE_ERR(("[ERR]:bIsDeviceBlacklisted: Device is Blacklisted"));
            bRetVal = true;
         }
      }
   }

   ETG_TRACE_USR1((" spi_tclMLVncDiscoverer::bIsDeviceBlacklisted:  ProductID = %d, VendorID = %d, bRetVal = %d", 
      s32ProductID, s32VendorID, bRetVal));

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vHandleUSBEntity()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vHandleUSBEntity(spi_tclMLVncDiscoverer* poMLVncDisc, VNCDiscoverySDKDiscoverer* poDisc,
         VNCDiscoverySDKEntity* poEntity)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vHandleUSBEntity()"));
   if ((NULL != m_poDiscoverySdk) && (NULL != m_poDiscoverySdkInstance))
   {
      //! Fetch the details of the reported USB entity
      t_Char* pczDetails = NULL;
      t_S32 s32Error = m_poDiscoverySdk->fetchEntityValueBlocking(poDisc,
               poEntity,
               VNCMlDiscovererUSBDeviceEntityDetails,
               &pczDetails,
               VNCDiscoverySDKNoTimeout);

      if ((NULL != pczDetails) && (VNCDiscoverySDKErrorNone == s32Error))
      {
         ETG_TRACE_USR2(("[PARAM]:entityAppearedCallback: USB Device Entity Details: %s", pczDetails));

         //! Use SDK utils to parse the device details
         VNCDiscoverySDKUtils oUtils;
         size_t u323UtilsSize = sizeof(VNCDiscoverySDKUtils);

         m_poDiscoverySdk->getUtils(m_poDiscoverySdkInstance, &oUtils, u323UtilsSize);

         VNCDiscoverySDKParser *pParser = NULL;
         oUtils.createParser(m_poDiscoverySdkInstance, pczDetails, &pParser);

         if (NULL != pParser)
         {
            //! Get vendorid and productid of the USB device
            t_S32 s32VendorID = 0;
            t_S32 s32ProductID = 0;
            oUtils.parserGetInt32Field(pParser, "vendorId", &s32VendorID);
            oUtils.parserGetInt32Field(pParser, "productId", &s32ProductID);
            ETG_TRACE_USR2(("[PARAM]:entityAppearedCallback:USB Device reported has VendorID-%d, ProductID-%d   ", 
               s32VendorID, s32ProductID));

            //! check whether the device is blacklisted. if not check whether the CDC/NCM profile is already activated on
            //Phone. Send profile switch request only if the CDC/NCM is not activated.
            if (false == bIsDeviceBlacklisted(s32ProductID, s32VendorID))
            {
               t_Char* pczNcmProfileEnabled = NULL;

               if ((true == bFetchEntityValueBlocking(poDisc,
                        poEntity,
                        VNCMlDiscovererUSBDeviceEntityIsNcm,
                        &pczNcmProfileEnabled)) && (NULL != pczNcmProfileEnabled) && (0 == strncmp("0",
                        pczNcmProfileEnabled,
                        strlen("0"))))
               {
                  ETG_TRACE_USR2(("[DESC]:entityAppearedCallback: CDC/NCM Profile Not yet enabled "));
               }//if( (true == bFetchEntityValueBlocking(poDis

               trMLDeviceInfo rDeviceInfo;
               rDeviceInfo.enDeviceCategory = e8DEV_TYPE_UNKNOWN;
               //! Added connection information as PSA project needs complete USB device list
               rDeviceInfo.enDeviceConnectionStatus = e8DEV_CONNECTED;
               rDeviceInfo.enDeviceConnectionType = e8USB_CONNECTED;
               //! Compute Device ID
               const t_Char *pczSerialNo = NULL;
               oUtils.parserGetStringField(pParser, "serial", &pczSerialNo);
               t_String szSerialNo = pczSerialNo;
               t_String szUniqueID = szSerialNo /*+ szProductID + szVendorID*/;
               t_U32 u32UniqueID = u16CalcCRC16((t_UChar*) (const_cast<t_Char*>(szUniqueID.c_str())), szUniqueID.size());
               ETG_TRACE_USR2(("[PARAM]:entityAppearedCallback:USB Device reported has UniqueID-0x%x, SerialNo-%s  ", 
                  u32UniqueID, szSerialNo.c_str()));

               //Get the discoverer type
               tenDiscovererType enDiscType = poMLVncDisc->enGetDiscovererType(poDisc);
               //Update container with the device info associated with the device UniqueId
               trDiscHandles rDiscHandles;
               rDiscHandles.poDiscoverer = poDisc;
               rDiscHandles.enDiscType = enDiscType;
               rDiscHandles.poEntity = poEntity;

               ETG_TRACE_USR2(("[PARAM]:entityAppearedCallback:USB Device reported has Disc-%p, Entity-%p ", 
                  poDisc, poEntity));
               poMLVncDisc->m_oMapDevLock.s16Lock();

               poMLVncDisc-> m_mapDevicesInfo.insert(std::pair<t_U32, trDiscHandles>(u32UniqueID, rDiscHandles));

               poMLVncDisc->m_oMapDevLock.vUnlock();

               poMLVncDisc->m_oMapDevModeLock.s16Lock();
               //! Store the device mode
               poMLVncDisc->m_mapDeviceModes.insert(std::pair<t_U32, t_Bool> (u32UniqueID, false));
               poMLVncDisc->m_oMapDevModeLock.vUnlock();

               MLDeviceInfoMsg oDeviceInfoMsg;
               oDeviceInfoMsg.vSetDeviceHandle(u32UniqueID);
               if (NULL != (oDeviceInfoMsg.prDeviceInfo))
               {
                  *(oDeviceInfoMsg.prDeviceInfo) = rDeviceInfo;
               }
               spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
               if (NULL != poMsgQinterface)
               {
                  poMsgQinterface->bWriteMsgToQ(&oDeviceInfoMsg, sizeof(oDeviceInfoMsg));
               }//if (NULL != poMsgQinterface)

            }//if (false == bIsDeviceBl
            else
            {
               ETG_TRACE_USR2(("[DESC]:Connected USB device is black listed. Mirrorlink switch won't be enabled"));
            }
         }//if (NULL != pParser)
      }//if ((NULL != pczDetails) && (
      else
      {
         ETG_TRACE_ERR(("[ERR]:entityAppearedCallback: Error in fetching USB Entity details"));
      }
   }//if(NULL !=m_poDiscoverySdk)
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vHandleMirrorlinkEntity()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vHandleMirrorlinkEntity(spi_tclMLVncDiscoverer* poMLVncDisc,
                                                       VNCDiscoverySDKDiscoverer* poDisc, 
                                                       VNCDiscoverySDKEntity* poEntity)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vHandleMirrorlinkEntity()"));

   if (NULL != poMLVncDisc)
   {

      //Get the discoverer type
      tenDiscovererType enDiscType = poMLVncDisc->enGetDiscovererType(poDisc);

      //Structure for Device Info
      trMLDeviceInfo rDeviceInfo;

      //Get the device info
      poMLVncDisc->vRetrieveDeviceInfo(poDisc, poEntity, enDiscType, rDeviceInfo);

      //Serial number
      t_String szSerialNo;
      t_Char* pcVal = NULL;
      if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootSerialNumber, &pcVal)) && (NULL != pcVal))
      {
         szSerialNo = pcVal;
         vFreeSdkString(pcVal);
      }
      //! Currently some devices like HTC One M8 is not returning serial number
      //! so the device id from serail number reported as USB entity will be used
      t_U32 u32UniqueID = m_u32MLCmdDeviceHandle;//u16CalcCRC16((t_UChar*) szSerialNo.c_str(), szSerialNo.size());
      if(0 == m_u32MLCmdDeviceHandle)
      {
         u32UniqueID = u16CalcCRC16((t_UChar*) szSerialNo.c_str(), szSerialNo.size());
      }
      ETG_TRACE_USR2(("[PARAM]:Mirrorlink Device reported has UniqueID = 0x%x and SerialNo = %s ", u32UniqueID, szSerialNo.c_str()));


      //Generate unique id for the device TODO: to be changed once serial number is reported by mirrorlink entity
      t_U32 u32DeviceID = u32UniqueID;//poMLVncDisc->u32GenerateUniqueDeviceId(rDeviceInfo.szDeviceUUID);

      //Update container with the device info associated with the device UniqueId
      trDiscHandles rDiscHandles;
      rDiscHandles.poDiscoverer = poDisc;
      rDiscHandles.enDiscType = enDiscType;
      rDiscHandles.poEntity = poEntity;

      poMLVncDisc->m_oMapDevLock.s16Lock();

      poMLVncDisc-> m_mapDevicesInfo[u32DeviceID]= rDiscHandles;

      poMLVncDisc->m_oMapDevLock.vUnlock();

      poMLVncDisc->m_oMapDevModeLock.s16Lock();
      //! Store the device mode as Mirrorlink mode. if entry already exists replace it.
      poMLVncDisc->m_mapDeviceModes[u32DeviceID] = true;
      poMLVncDisc->m_oMapDevModeLock.vUnlock();

      MLDeviceInfoMsg oDeviceInfoMsg;
      if (NULL != oDeviceInfoMsg.prDeviceInfo)
      {
         *(oDeviceInfoMsg.prDeviceInfo) = rDeviceInfo;
      }
      oDeviceInfoMsg.vSetDeviceHandle(u32DeviceID);
      spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();
      if (NULL != poMsgQinterface)
      {
         poMsgQinterface->bWriteMsgToQ(&oDeviceInfoMsg, sizeof(oDeviceInfoMsg));
      }
   }//if( NULL != poMLVncDisc)

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppName()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppName(t_U32 u32DevHandle, t_U32 u32AppHandle, const trUserContext& cofrUserCntxt)
{

   ETG_TRACE_USR1((" spi_tclMLVncDiscoverer::vFetchAppName:Device ID-0x%x AppID-0x%x", u32DevHandle, u32AppHandle));

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DevHandle);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DevHandle);
   //get the App ID
   t_String szAppId = szGetAppId(u32DevHandle, u32AppHandle).c_str();

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = cofrUserCntxt;
   rMsgCtxt.rAppContext.u32ResID = e32FETCHAPPNAME;

   t_Char cKey[MAX_KEYSIZE] = { 0 };
   sprintf(cKey, VNCDiscoverySDKMLAppName, szAppId.c_str());
   t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, cKey);

   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   ETG_TRACE_USR4((" [PARAM]:vFetchAppName:ReqID-%d, RespID-%d", u32ReqId, rMsgCtxt.rAppContext.u32ResID));

}

/***************************************************************************
 ** FUNCTION:  t_String spi_tclMLVncDiscoverer::szFetchAppName()
 ***************************************************************************/
t_String spi_tclMLVncDiscoverer::szFetchAppName(t_U32 u32DevHandle, t_U32 u32AppHandle)
{

   ETG_TRACE_USR1((" spi_tclMLVncDiscoverer::szFetchAppName:Device ID-0x%x AppID-0x%x", u32DevHandle, u32AppHandle));

   t_String szAppName = coszUnKnownAppName.c_str();
   t_Char* czResult = NULL;

   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DevHandle);
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DevHandle);
   t_String szAppId = szGetAppId(u32DevHandle, u32AppHandle).c_str();

   //Name of the application.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppName, szAppId, &czResult))
        && (NULL != czResult))
   {
      szAppName = czResult;
      ETG_TRACE_USR4(("[PARAM]:szFetchAppName:AppID-0x%x App Name:%s", u32AppHandle, szAppName.c_str()));
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD

   return szAppName;

}

/***************************************************************************
 ** FUNCTION:  tenAppCategory spi_tclMLVncDiscoverer::enGetAppCategory()
 ***************************************************************************/
tenAppCategory spi_tclMLVncDiscoverer::enGetAppCategory(t_U32 u32DevHandle, t_U32 u32AppHandle)
{
   tenAppCategory enAppCat = e32APPUNKNOWN;
   t_Char cKey[MAX_KEYSIZE] = { 0 };

   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DevHandle);
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DevHandle);
   t_String szAppId = szGetAppId(u32DevHandle, u32AppHandle).c_str();

   //Name of the application.
   sprintf(cKey, VNCDiscoverySDKMLAppCategory, szAppId.c_str());
   enAppCat = enGetAppCategory(poDisc, poEntity, cKey);

   ETG_TRACE_USR1((" spi_tclMLVncDiscoverer::enGetAppCategory:Device ID-0x%x AppID-0x%x App Cat-0x%x", 
      u32DevHandle, u32AppHandle, enAppCat));

   return enAppCat;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vSetUPnPAppPublicKeyForXMLValidation()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vSetUPnPAppPublicKeyForXMLValidation(t_U32 u32DevHandle, t_String szUPnPAppPublicKey,
         const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscoverer::vSetUPnPAppPublicKeyForXMLValidation:Dev-0x%x UPnPAppPublicKey-%s", 
      u32DevHandle, szUPnPAppPublicKey.c_str()));

   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DevHandle);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DevHandle);

   //Store the request ID. Will be used in callback function
   trMsgContext rMsgCtxt;
   rMsgCtxt.rUserContext = corfrUsrCntxt;
   rMsgCtxt.rAppContext.u32ResID = e32APP_PUBLIC_KEY;

   //post the value to VNC
   t_U32 u32ReqId = u32PostEntityValue(poDisc,
            poEntity,
            VNCDiscoverySDKMLUPnPApplicationPublicKey,
            szUPnPAppPublicKey.c_str(),
            cs32MLDiscoveryNoTimeout);

   m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

   if (cu32MLDiscoveryNoRequestId == u32ReqId)
   {
      ETG_TRACE_ERR(("[ERR]:vSetUPnPAppPublicKeyForXMLValidation: Error in Request "));
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscLogCb()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vDiscLogCb(t_Void *pSDKContext, t_S32 s32Severity, const t_Char *pcocDisc,
         const t_Char *pcocText)

{

   SPI_INTENTIONALLY_UNUSED(pSDKContext);
   SPI_INTENTIONALLY_UNUSED(pcocDisc);

   if (NULL != pcocText)
   {
      if (s32Severity <= 30)
      {
         ETG_TRACE_USR4(("[PARAM]:vDiscLogCb: Severity-%d  Text-%s", s32Severity, pcocText));
      }
      else
      {
         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_VNC_DEBUG, "[PARAM]:vDiscLogCb: Severity-%d  Text-%s", 
            s32Severity, pcocText));
      }
   }//if(NULL != pcocText)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchAppUri()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchAppUri(const t_U32 cou32DevID, const t_U32 cou32AppID, t_String& rfszAppUri,
         t_String& rfszProtocolID)
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscoverer::vFetchAppUri:Device ID-0x%x AppID-0x%x", cou32DevID, cou32AppID));

   rfszAppUri = "";
   rfszProtocolID = "";
   t_Char* czResult = NULL;

   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DevID);
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DevID);
   t_String szAppId = szGetAppId(cou32DevID, cou32AppID).c_str();

   //Application remoting protocol ID
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppRemotingProtocol, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfszProtocolID = czResult;
      vFreeSdkString(czResult);
      ETG_TRACE_USR4(("[PARAM]:vFetchAppUri: AppID-0x%x, Remoting ProtocolID = %s", 
         cou32AppID, rfszProtocolID.c_str()));
   }//if( (true == bFetchEntityValueB

   //URI of the application.
   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, 
        VNCDiscoverySDKMLAppUri, szAppId, &czResult))
        && (NULL != czResult))
   {
      rfszAppUri = czResult;
      ETG_TRACE_USR4(("[PARAM]:vFetchAppUri:AppID-0x%x ML AppUri:%s", 
         cou32AppID, rfszAppUri.c_str()));
      vFreeSdkString(czResult);
   }//if( (true == bFetchEntityValueBlocking(poD

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::bRequestMirrorlinkSwitch()
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bRequestMirrorlinkSwitch(const t_U32 cou32DevID)
{
   ETG_TRACE_USR1((" spi_tclMLVncDiscoverer::bRequestMirrorlinkSwitch: Device ID-0x%x ", cou32DevID));
   t_Bool bRetval = false;
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DevID);
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DevID);
   VNCDiscoverySDKError s32Error = m_poDiscoverySdk->postEntityValueBlocking(poDisc,
            poEntity,
            VNCMlDiscovererUSBDeviceEntityKeySendCommand,
            coczMLClientVersion,
            VNCDiscoverySDKNoTimeout);
   if (VNCDiscoverySDKErrorNone != s32Error)
   {
      ETG_TRACE_ERR(("[ERR]:Sending mirrorlink command failed Error: %s", m_poDiscoverySdk->errorToString(s32Error)));
   }//if (VNCDiscoverySDKErrorNone != s32Error)
   else
   {
      m_u32MLCmdDeviceHandle = cou32DevID;
      bRetval = true;
   }
   return bRetval;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bisDeviceInMirrorlinkMode()
***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bisDeviceInMirrorlinkMode(const t_U32 cou32DevID)
{
   t_Bool bRetVal = false;
   if(m_mapDeviceModes.end() != m_mapDeviceModes.find(cou32DevID))
   {
      bRetVal = m_mapDeviceModes[cou32DevID];
   }
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::bisDeviceInMirrorlinkMode DeviceID = 0x%x  Device is in Mirrorlink mode-%d",
         cou32DevID, ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscThreadStartedCb()
***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vDiscThreadStartedCb(t_Void *pvSDKContext)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vDiscThreadStartedCb"));
   DEFINE_THREAD_NAME("VNCDiscoverySDK");
   SPI_INTENTIONALLY_UNUSED(pvSDKContext);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vDiscThreadEndedCb()
***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vDiscThreadEndedCb(t_Void *pvSDKContext,
                                                  MLDiscoveryError s32Error)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vDiscThreadEndedCb: Error-%d",s32Error));
   SPI_INTENTIONALLY_UNUSED(pvSDKContext);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetKeyIconData()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vGetKeyIconData(const t_U32 cou32DeviceHandle, t_String szKeyIconUrl,
                                               const trUserContext& rfrcUsrCntxt )
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vGetKeyIconData: Device ID-0x%x, Key Icon URL-%s", 
      cou32DeviceHandle, szKeyIconUrl.c_str()));

   //! Get both SDK discoverer and SDK entity
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(cou32DeviceHandle);
   VNCDiscoverySDKEntity* poEntity = poGetEntity(cou32DeviceHandle);


   if ((NULL != m_poDiscoverySdk) && (NULL != poDisc) && (NULL != poEntity) && false == szKeyIconUrl.empty())
   {
      //! Container for downloadIcon Key
      t_Char czKey[MAX_KEYSIZE] = { 0 };

      sprintf(czKey, VNCDiscoverySDKMLDownloadIcon, szKeyIconUrl.c_str());

      t_U32 u32ReqId = u32FetchEntityValue(poDisc, poEntity, czKey, VNCDiscoverySDKNoTimeout);

      //Store the request ID. Will be used in callback function
      trMsgContext rMsgCtxt;
      rMsgCtxt.rUserContext = rfrcUsrCntxt;
      rMsgCtxt.rAppContext.u32ResID = e32FETCH_KEYICON_DOWNLOAD_ID;
      m_oMsgContext.vAddMsgContext(u32ReqId, rMsgCtxt);

      if (cu32MLDiscoveryNoRequestId == u32ReqId)
      {
         ETG_TRACE_ERR(("[ERR]:vGetKeyIconData: Error in Request "));
      }

    }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vPostKeyIconData()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vPostKeyIconData(t_U32 u32DeviceID, t_Char* pcKey, t_Char* pcVal, trUserContext rUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vPostKeyIconData: Device ID-0x%x", u32DeviceID));
   spi_tclMLVncMsgQInterface *poMsgQinterface = spi_tclMLVncMsgQInterface::getInstance();

   if ((NULL != poMsgQinterface) && (NULL != pcKey))
   {
      const t_U8* cou8Data = NULL;
      t_U32 u32Size = 0;

      //get the discoverer pointer
      VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
      //get the entity pointer
      VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

      //If the pcVal is null, send the empty message to SPI, so that it can post error message to HMI
      if ((NULL != pcVal) && (NULL != m_poDiscoverySdk) && (NULL != poDisc) && (NULL != poEntity))
      {
         u32Size = m_poDiscoverySdk->getEntityBinaryData(poDisc, poEntity, pcVal, &cou8Data);
      }//if((NULL != pcVal) &&

      ETG_TRACE_USR4(("[PARAM]:vPostKeyIconData:Icon Size-%d ", u32Size));

      MLKeyIconData oKeyIconData;
      oKeyIconData.vSetDeviceHandle(u32DeviceID);
      oKeyIconData.vSetUserContext(rUsrCntxt);
      oKeyIconData.u32Size = u32Size;

      if ((u32Size > 0) && (NULL != cou8Data))
      {
         oKeyIconData.pu8BinaryData = new t_U8[u32Size];
         if (NULL != oKeyIconData.pu8BinaryData)
         {
            memcpy((t_Void*) oKeyIconData.pu8BinaryData, cou8Data, u32Size);
         }
      }
      //Post the message to Dispatcher
      poMsgQinterface->bWriteMsgToQ(&oKeyIconData, sizeof(oKeyIconData));
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vGetXDeviceKeyInfo()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vGetXDeviceKeyInfo(const t_U32 u32DeviceID, 
                                                  t_U16& u16NumXDevices,
                                                  std::vector<trXDeviceKeyDetails> &vecrXDeviceKeyDetails)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vGetXDeviceKeyInfo: Device ID-0x%x", u32DeviceID));
   //get the discoverer pointer
   VNCDiscoverySDKDiscoverer* poDisc = poGetDiscoverer(u32DeviceID);
   //get the entity pointer
   VNCDiscoverySDKEntity* poEntity = poGetEntity(u32DeviceID);

   t_Char* pcVal = NULL;

   if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootKeyCount, &pcVal)) && (NULL != pcVal))
     {
        t_Char cKey[MAX_KEYSIZE] = { 0 };
        t_String szNumXdevice = pcVal;
        vFreeSdkString(pcVal);

        ETG_TRACE_USR2(("[PARAM]:vGetXDeviceKeyInfo:Number of X-device keys Advertised by the Phone-%s,", 
           szNumXdevice.c_str()));

        StringHandler oStringUtil;
        u16NumXDevices = oStringUtil.u32ConvertStrToInt(szNumXdevice.c_str());

        for(t_U8 u8Index = 0; u8Index < u16NumXDevices; ++u8Index)
           {
              trXDeviceKeyDetails rXDeviceDetails;

              rXDeviceDetails.u8KeyId = u8Index;
              //! Create the key string for X-Device Key Name.
              snprintf(cKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLRootKeyName, u8Index);
              if ((true == bFetchEntityValueBlocking(poDisc, poEntity,cKey, &pcVal)) && (NULL != pcVal))
              {
                 rXDeviceDetails.szXDeviceName = pcVal;
                 vFreeSdkString(pcVal);
                 ETG_TRACE_USR4(("[PARAM]:vGetXDeviceKeyInfo:X-device Key Name - %s", 
                    rXDeviceDetails.szXDeviceName.c_str()));
              }

              //! Create the key string for X-Device Key Symbol Value.
              snprintf(cKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLRootKeySymbolValue, u8Index);
              if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey,&pcVal)) && (NULL != pcVal))
              {
                 //Check for the symbol value, it should be supported by SPI.
                 rXDeviceDetails.enSymbolValue = static_cast<tenKeyCode>
                                                 (oStringUtil.s32ConvertStrToInt(pcVal));
                 vFreeSdkString(pcVal);
                 ETG_TRACE_USR4(("[PARAM]:vGetXDeviceKeyInfo:X-device Symbol Value - %d", 
                    rXDeviceDetails.enSymbolValue));
              }

              //! Create the key string for X-Device Key Icon Count.
              snprintf(cKey, MAXKEYSIZE - 1, VNCDiscoverySDKMLRootKeyIconCount, u8Index);
              if ((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey,&pcVal)) && (NULL != pcVal))
              {
                 t_U8 u8IconCount = static_cast<t_U8>(oStringUtil.u32ConvertStrToInt(pcVal));
                 ETG_TRACE_USR4(("[PARAM]:vGetXDeviceKeyInfo:X-device Key Icon count - %d", u8IconCount));
                 vFreeSdkString(pcVal);

                 //! Fetch Icon details for each Key Icon.
                 if (u8IconCount > 0)
                 {
                    vFetchXDeviceKeyIconData(poDisc,poEntity,rXDeviceDetails.tvecKeyIconList,u8Index,u8IconCount);
                 }
              }

               //! Set IsMandatory field to false since it is deprecated.
              rXDeviceDetails.bIsMandatory = false;
              vecrXDeviceKeyDetails.push_back(rXDeviceDetails);
          }

     }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclMLVncDiscoverer::vFetchXDeviceKeyIconData()
 ***************************************************************************/
t_Void spi_tclMLVncDiscoverer::vFetchXDeviceKeyIconData(VNCDiscoverySDKDiscoverer* poDisc,VNCDiscoverySDKEntity* poEntity,
                                                     std::vector<trIconAttributes>& rfrtvecKeyIconList,t_U8 u8KeyIndex,
                                                     t_U8 u8IconCount)
{
   ETG_TRACE_USR1(("spi_tclMLVncDiscoverer::vFetchXDeviceKeyIconData: XDevice Key Index-%d", u8KeyIndex));

   StringHandler oStrUtil;

   //! get Icon specific data for each Key Icon.
   for (t_U8 u8Index = 0; u8Index < u8IconCount; ++u8Index)
   {
      trIconAttributes rKeyIconAttributes; 
      t_String szStr;

      rKeyIconAttributes.enIconMimeType = e8ICON_INVALID;
      if (true == bFetchRootKeyIconData(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconMimeType,
          u8Index, u8KeyIndex, szStr))
      {
         rKeyIconAttributes.enIconMimeType = static_cast<tenIconMimeType>(oStrUtil.u32ConvertStrToInt(szStr.c_str()));
         ETG_TRACE_USR2(("vFetchXDeviceKeyIconData: X-device Key : %d Icon : %d MimeType: %d,",
           u8KeyIndex, u8Index, rKeyIconAttributes.enIconMimeType));
      }//if (bFetchRootKeyIconData(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconMimeType


      if (true == bFetchRootKeyIconData(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconWidth,
          u8Index, u8KeyIndex, szStr))
      {
         rKeyIconAttributes.u32IconWidth = oStrUtil.u32ConvertStrToInt(szStr.c_str());
         ETG_TRACE_USR2(("X-device Key : %d Icon : %d Icon width: %d,",
           u8KeyIndex, u8Index, rKeyIconAttributes.u32IconWidth));
      }//if (bFetchRootKeyIconData(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconWidth

      if (true == bFetchRootKeyIconData(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconHeight,
          u8Index, u8KeyIndex, szStr))
      {
         rKeyIconAttributes.u32IconHeight = oStrUtil.u32ConvertStrToInt(szStr.c_str());
         ETG_TRACE_USR2(("X-device Key : %d Icon : %d Icon height: %d,",
           u8KeyIndex, u8Index, rKeyIconAttributes.u32IconHeight));
      }//if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconHeight

      if (true == bFetchRootKeyIconData(poDisc, poEntity, 
          VNCDiscoverySDKMLRootKeyIconDepth, u8Index, u8KeyIndex, szStr))
      {
         rKeyIconAttributes.u32IconDepth =oStrUtil.u32ConvertStrToInt(szStr.c_str());

         ETG_TRACE_USR2(("X-device Key : %d Icon : %d Icon height: %d,",
           u8KeyIndex, u8Index, rKeyIconAttributes.u32IconDepth));
      }//if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconDepth

      if (true == bFetchRootKeyIconData(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconUrl, 
          u8Index, u8KeyIndex, szStr))
      {
         rKeyIconAttributes.szIconURL = szStr.c_str();
         ETG_TRACE_USR2(("X-device Key : %d Icon : %d Icon height: %d,", 
           u8KeyIndex, u8Index, rKeyIconAttributes.szIconURL.c_str()));
      }//if ((true == bFetchEntityValueBlocking(poDisc, poEntity, VNCDiscoverySDKMLRootKeyIconUrl

      rfrtvecKeyIconList.push_back(rKeyIconAttributes);
   }//for (t_U8 u8Index = 0; u8Index < u8IconCount; ++u8Index)

}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclMLVncDiscoverer::bFetchRootKeyIconData()
 ***************************************************************************/
t_Bool spi_tclMLVncDiscoverer::bFetchRootKeyIconData(VNCDiscoverySDKDiscoverer* poDisc,
                                                     VNCDiscoverySDKEntity* poEntity,
                                                     const t_Char* corfszKey,
                                                     t_U8 u8KeyIconIndex, 
                                                     t_U8 u8KeyIndex, 
                                                     t_String& rfszStr)
{
   t_Char* pcVal = NULL;
   t_Bool bRet = false;
   rfszStr = "";
   
   t_Char cKey[MAX_KEYSIZE] = { 0 };
   //Fetch the key whose info is needed
   snprintf(cKey, MAXKEYSIZE - 1, corfszKey, u8KeyIconIndex, u8KeyIndex);
   
   //Fetch the entity value
   if((true == bFetchEntityValueBlocking(poDisc, poEntity, cKey, &pcVal)) && (NULL != pcVal))
   {
      rfszStr = pcVal;
      bRet = true;
      vFreeSdkString(pcVal);
      ETG_TRACE_USR2(("vFetchRootKeyIconData: Key Index: %d Icon Index: %d Element: %s",
      u8KeyIndex, u8KeyIconIndex, corfszKey));
   }
   //Error in fetching the Key Icon data
   else
   {
      ETG_TRACE_ERR(("vFetchRootKeyIconData: Error in fetching value of Key Index: %d Icon Index: %d Element: %s",
        u8KeyIndex, u8KeyIconIndex, corfszKey));
   }
   
   return bRet;
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
