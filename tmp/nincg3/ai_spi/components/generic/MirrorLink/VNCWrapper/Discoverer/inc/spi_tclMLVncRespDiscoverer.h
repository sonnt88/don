/***********************************************************************/
/*!
* \file  spi_tclMLVncRespDiscoverer.h
* \brief ML Discoverer Output Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    ML Discoverer Output Interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version
24.04.2014  |  Shiva kumar Gurija   | XML Validation
25.06.2015  | Sameer Chandra        | Added ML XDeviceKey Support for PSA

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVNCRESPDISCOVERER_H_
#define _SPI_TCLVNCRESPDISCOVERER_H_

#include "RespBase.h"
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclMLVncRespDiscoverer
* \brief ML Discoverer Output Interface
*
* It provides an interface to SPI, To register for the Discoverer
* status updates and updates all the registered clients, whenever there is
* an update.
*
****************************************************************************/
class spi_tclMLVncRespDiscoverer:public RespBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncRespDiscoverer::spi_tclMLVncRespDiscoverer()
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncRespDiscoverer()
   * \brief   Constructor
   * \sa      ~spi_tclMLVncRespDiscoverer()
   **************************************************************************/
   spi_tclMLVncRespDiscoverer();

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncRespDiscoverer::~spi_tclMLVncRespDiscoverer()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclMLVncRespDiscoverer()
   * \brief   Destructor
   * \param   t_Void
   * \sa      spi_tclMLVncRespDiscoverer(RegID enRegId)
   **************************************************************************/
   virtual ~spi_tclMLVncRespDiscoverer();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostDeviceInfo
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostDeviceInfo(const t_U32 cou32DeviceHandle,
   *                                    const trDeviceInfo& corfrDeviceInfo)
   * \brief   To Post the device info to SPI, when a new device is detected
   * \param   cou32DeviceHandle     : [IN] Device Id
   * \param   corfrDeviceInfo : [IN] const reference to the DeviceInfo structure.
   * \retval  t_Void
   * \sa      vPostDeviceDisconncted(const t_U32 cou32DeviceHandle)
   ***************************************************************************/
   virtual t_Void vPostDeviceInfo(
       const t_U32 cou32DeviceHandle,
       const trMLDeviceInfo& corfrMLDeviceInfo);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostDeviceDisconnected(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostDeviceDisconnected(const t_U32 cou32DeviceHandle)
   * \brief   To Post the Device Id to SPI, when a device is disconnected
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle    : [IN] Device Id
   * \retval  t_Void
   * \sa      vPostNewDeviceInfo(const t_U32 cou32DeviceHandle,const trDeviceInfo& rDeviceInfo)
   **************************************************************************/
   virtual t_Void vPostDeviceDisconnected(const t_U32 cou32DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostBTAddr(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostBTAddr(const t_U32 cou32DeviceHandle,const t_Char* pcocAddr)
   * \brief   To Post the BT Address of the device
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle    : [IN] Device Id
   * \param   pcocAddr       : [IN] BT Address of the device.
   * \retval  t_Void
   * \sa
   ***************************************************************************/
   virtual t_Void vPostBTAddr(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
       const t_Char* pcocAddr);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppStatus(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostAppStatus(trUserContext rUserContext,
   *               const t_U32 cou32DeviceHandle,t_U32 u32AppHandle,
   *               tenAppStatus enAppStatus)
   * \brief   To Post an application status
   * \param   rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle   : [IN] Device Id
   * \param   u32AppHandle        : [IN] App Id
   * \param   enAppStatus       : [IN] Application status
   * \retval  t_Void
   * \sa      vPostAllAppsStatuses()
   ***************************************************************************/
   virtual t_Void vPostAppStatus(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
       t_U32 u32AppId,
      tenAppStatus enAppStatus);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAllAppsSta(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostAllAppsStatuses(const t_U32 cou32DeviceHandle,
   *          const std::map<t_U32, std::map<t_String, t_String> >& corfMapAllStatuses)
   * \brief   To post the statuses of all applications supported by a device.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle         : [IN] Device Id
   * \param   corfMapAllStatuses  : [IN] List of all applications and its statuses
   * \retval  t_Void
   * \sa      vPostAppStatus(),vPostAppList()
   ***************************************************************************/
   virtual t_Void vPostAllAppsStatuses(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
      const std::map<t_U32, std::map<t_String,t_String> >& corfMapAllStatuses);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppList(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostAppList(const t_U32 cou32DeviceHandle,
   *                  const std::vector<t_U32>& corfvecAppList )
   * \brief   To Post the list of all applications supported by a device.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle       : [IN] Device Id
   * \param   corfvecAppList   : [IN] List of applications
   * \retval  t_Void
   * \sa      vPostAppStatus(),vPostCertAppList()
   ***************************************************************************/
   virtual t_Void vPostAppList(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
       const std::vector<t_U32>& corfvecAppList );

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostCertAppList(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostCertAppList(const t_U32 cou32DeviceHandle,
   *                         const std::vector<t_U32>& corfvecAppList )
   * \brief   To post the list of certified apps supported by a device.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle     : [IN] Device Id
   * \param   corfvecAppList   : [IN] List of applications
   * \retval  t_Void
   * \sa      vPostAppStatus(),vPostAppList()
   ***************************************************************************/
   virtual t_Void vPostCertAppList(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
       const std::vector<t_U32>& corfvecAppList );

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppProtcolID(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostAppProtcolID
   * \brief   To post the protocol ID of the apps in the device
   * \param   rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle     : [IN] Device Id
   * \param	  u32AppID: [IN] App ID for which the Protocol ID is Posted
   * \param   szProtocolID   : [IN] Protocol ID for u32AppID
   * \param   bIsLastApp   : [IN] Set if u32AppID is the last application in the list
   * \retval  t_Void
   ***************************************************************************/
   virtual t_Void vPostAppProtcolID(trUserContext &rfrUserContext,
            const t_U32 cou32DeviceHandle, const t_U32 u32AppID,
            t_String szProtocolID, t_Bool bIsLastApp);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostLaunchAppStatus(trUserContext...
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostLaunchAppStatus(const t_U32 cou32DeviceHandle,
   *                                   t_U32 u32AppId, t_Bool bResult);
   * \brief   To post the response of launch application request.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle  : [IN] Device Id
   * \param   u32AppId     : [IN] Application Id
   * \param   bResult      : [IN] Response True if Application is launched,
   *                                       else False
   * \retval  t_Void
   * \sa      vPostTerminateAppStatus()
   ***************************************************************************/
   virtual t_Void vPostLaunchAppStatus(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
       t_U32 u32AppId, 
       t_Bool bResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostTerminateAppStatus(trUserContext..
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostTerminateAppStatus(const t_U32 cou32DeviceHandle,
   *                                   t_U32 u32AppId, t_Bool bResult);
   * \brief   To post the response of terminate appliaction request.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle  : [IN] Device Id
   * \param   u32AppId     : [IN] Application Id
   * \param   bResult      : [IN] Response True if Application is terminated,
   *                                       else False
   * \retval  t_Void
   * \sa      vPostLaunchAppStatus()
   ***************************************************************************/
   virtual t_Void vPostTerminateAppStatus(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
       t_U32 u32AppId, 
       t_Bool bResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppInfo(trUserContext...
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostAppInfo(const t_U32 cou32DeviceHandle,t_U32 u32AppId,
   *                                    const trvncAppDetails& corfrAppDetails);
   * \brief   To post the Information of an application.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle     : [IN] Device Id
   * \param   u32AppId        : [IN] Application Id
   * \param   corfrAppDetails : [IN] const reference to Application Info structure.
   * \retval  t_Void
   ***************************************************************************/
   virtual t_Void vPostAppInfo(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
       t_U32 u32AppId,
      const trApplicationInfo& corfrAppInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppIconAttr(trUserContext...
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostAppIconAttr(const t_U32 cou32DeviceHandle, t_U32 u32AppId,
   *                                     const trvncIconAttr& corfrAppIconAttr);
   * \brief   To Post the Application icon Attributes.
   * \param rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle       : [IN] Device Id
   * \param   u32AppId          : [IN] Application Id
   * \param   corfrAppIconAttr  : [IN] const reference to AppIconAttr structure
   * \retval  t_Void
   ***************************************************************************/
   virtual t_Void vPostAppIconAttr(
       trUserContext rUserContext,
       const t_U32 cou32DeviceHandle,
       t_U32 u32AppId,
      const trIconAttributes& corfrAppIconAttr);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostAppIconData()
   ***************************************************************************/
   /*!
   * \fn     vPostAppIconData(trUserContext rUserContext,
   *           const t_U32 u32DeviceHandle,
   *           t_String szAppIconUrl,
   *           const t_U8* pcou8BinaryData,
   *           const t_U32 u32Len)
   * \brief  It updates the Fetch App Icon data result to SPI
   * \param  rcUsrCntxt : [IN]  User Context Details.
   * \param  u32DeviceHandle: [IN] Device Id
   * \param  szAppIconUrl : [IN] App Icon URL
   * \param  pcou8BinaryData : Byte Data Stream from the icon image file.
   * \param  u32Len : [IN] Icon Length
   **************************************************************************/
   virtual t_Void vPostAppIconData(trUserContext rUserContext,
         const t_U32 u32DeviceHandle,
         t_String szAppIconUrl,
         const t_U8* pcou8BinaryData,
         const t_U32 u32Len);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostDevEventSubscriptionStatus()
   ***************************************************************************/
   /*!
   * \fn     t_Void vPostDevEventSubscriptionStatus(const trUserContext& corfrUsrCntxt,
   *             const t_U32 u32DeviceHandle,
   *             tenEventSubscription enEventSubscription,
   *             t_Bool bResult)
   * \brief  It updates the Fetch App Icon data result to SPI
   * \param  corfrUsrCntxt : [IN]  User Context Details.
   * \param  u32DeviceHandle: [IN] Device Id
   * \param  enEventSubscription : [IN] Event subscription enumeration
   * \param  bResult : [IN] success/failure
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vPostDevEventSubscriptionStatus(const trUserContext& corfrUsrCntxt,
         const t_U32 u32DeviceHandle,
         tenEventSubscription enEventSubscription,
         t_Bool bResult);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostSetClientProfileResult()
   ***************************************************************************/
   /*!
   * \fn     t_Void vPostSetClientProfileResult(const trUserContext& corfrUsrCntxt,
   *             const t_U32 u32DeviceHandle,
   *             t_Bool bResult)
   * \brief  It updates the Set Client Profile result to SPI
   * \param  corfrUsrCntxt : [IN]  User Context Details.
   * \param  u32DeviceHandle: [IN] Device Id
   * \param  bResult : [IN] success/failure
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vPostSetClientProfileResult(const trUserContext& corfrUsrCntxt,
      const t_U32 u32DeviceHandle,
      t_Bool bResult);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostAppName()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostAppName(t_U32 u32DeviceHandle,
   *             t_U32 u32AppHandle,t_String szAppName,const trUserContext& corfrUsrCntxt)
   * \brief   posts the application name of a particular app
   * \param   u32DeviceHandle     :[IN] Device handle
   * \param   u32AppHandle        :[IN] Application handle
   * \param   szAppName           :[IN] App name
   * \param   corfrUsrCntxt       :[IN] User context
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vPostAppName(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
      t_String szAppName,const trUserContext& corfrUsrCntxt);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostSetUPnPPublicKeyResp()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostSetUPnPPublicKeyResp(t_U32 u32DeviceHandle,
   *                  t_Bool bXMLValidationSucceeded,const trUserContext& corfrUsrCntxt )
   * \brief   To Post the Set UPnP Public Key response
   * \param   u32DeviceHandle : [IN] Device Id
   * \param   bXMLValidationSucceeded : [IN] TRUE- Success
   *                                         FALSE - error in setting
   * \param   rUserContext      : [IN] Context information passed from the caller
   * \retval  t_Void
   ***************************************************************************/
   virtual t_Void vPostSetUPnPPublicKeyResp(t_U32 u32DeviceHandle, 
      t_Bool bXMLValidationSucceeded,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostKeyIconData()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostKeyIconData(trUserContext rUserContext,const t_U32 u32DeviceHandle,
   *                                  const t_U8* pcou8BinaryData,
   *                                  const t_U32 u32Len);
   * \brief   To Post the Set UPnP Public Key response
   * \param   u32DeviceHandle : [IN] Device Id
   * \param   bXMLValidationSucceeded : [IN] TRUE- Success
   *                                         FALSE - error in setting
   * \param   pcou8BinaryData      : [IN] Binary Data
   * \param   u32Len               : [IN] Length of Binary Data
   * \retval  t_Void
   ***************************************************************************/
   virtual t_Void vPostKeyIconData(trUserContext rUserContext,const t_U32 u32DeviceHandle,
                                                       const t_U8* pcou8BinaryData,
                                                       const t_U32 u32Len);
   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncRespDiscoverer::spi_tclMLVncRespDiscoverer()
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncRespDiscoverer(
   *                          const spi_tclMLVncRespDiscoverer& corfoSrc))
   * \brief   Parameterized Constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \sa      spi_tclMLVncRespDiscoverer(RegID enRegId)
   **************************************************************************/
   spi_tclMLVncRespDiscoverer(const spi_tclMLVncRespDiscoverer& corfoSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclMLVncRespDiscoverer& operator=( const spi_tclMLV...
   ***************************************************************************/
   /*!
   * \fn      spi_tclMLVncRespDiscoverer& operator=(
   *                          const spi_tclMLVncRespDiscoverer& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclMLVncRespDiscoverer(const spi_tclMLVncRespDiscoverer& otrSrc)
   ***************************************************************************/
   spi_tclMLVncRespDiscoverer& operator=(const spi_tclMLVncRespDiscoverer& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

};


#endif //_SPI_TCLVNCRESPDISCOVERER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
