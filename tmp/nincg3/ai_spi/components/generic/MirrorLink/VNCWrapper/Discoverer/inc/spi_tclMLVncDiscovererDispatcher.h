/***********************************************************************/
/*!
 * \file  spi_tclMLVncDiscovererDispatcher.h
 * \brief Message Dispatcher for Discoverer Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Discoverer Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.11.2013  | Pruthvi Thej Nagaraju | Initial Version
 14.05.2014  | Shiva Kumar Gurija    | Changes in AppList Handling 
                                        for CTS Test
 25.06.2015  | Sameer Chandra        | Added ML XDeviceKey Support for PSA
 
 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLMLVNCDISCOVERERDISPATCHER_H_
#define SPI_TCLMLVNCDISCOVERERDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"

/**************Forward Declarations******************************************/
class spi_tclMLVncDiscovererDispatcher;

/****************************************************************************/
/*!
 * \class MLDiscovererMsgBase
 * \brief Base Message type for all Discoverer messages
 ****************************************************************************/
class MLDiscovererMsgBase: public trMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  MLDiscovererMsgBase::MLDiscovererMsgBase
       ***************************************************************************/
      /*!
       * \fn      MLDiscovererMsgBase()
       * \brief   Default constructor
       **************************************************************************/
      MLDiscovererMsgBase();

      /***************************************************************************
       ** FUNCTION:  MLDiscovererMsgBase::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   Pure virtual function to be overridden by inherited classes for
       *          dispatching the message
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      virtual t_Void vDispatchMsg(
               spi_tclMLVncDiscovererDispatcher* poDiscDispatcher) = 0;

      /***************************************************************************
       ** FUNCTION:  MLDiscovererMsgBase::~MLDiscovererMsgBase
       ***************************************************************************/
      /*!
       * \fn      ~MLDiscovererMsgBase()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLDiscovererMsgBase()
      {

      }

      /***************************************************************************
       ** FUNCTION:  MLDiscovererMsgBase::u32GetDeviceHandle
       ***************************************************************************/
      /*!
       * \fn      u32GetDeviceHandle()
       * \brief   returns the device handle
       **************************************************************************/
      virtual t_U32 u32GetDeviceHandle();

      /***************************************************************************
       ** FUNCTION:  MLDiscovererMsgBase::vSetDeviceHandle
       ***************************************************************************/
      /*!
       * \fn      vSetDeviceHandle()
       * \brief   sets the device handle
       **************************************************************************/
      virtual t_Void vSetDeviceHandle(t_U32 u32DevHndle);

   private:
      t_U32 m_u32DeviceHandle;
};

/****************************************************************************/
/*!
 * \class MLDeviceInfoMsg
 * \brief Device information message
 ****************************************************************************/
class MLDeviceInfoMsg: public MLDiscovererMsgBase
{
   public:
      trMLDeviceInfo* prDeviceInfo;

      /***************************************************************************
       ** FUNCTION:  MLDeviceInfoMsg::MLDeviceInfoMsg
       ***************************************************************************/
      /*!
       * \fn      MLDeviceInfoMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLDeviceInfoMsg();

      /***************************************************************************
       ** FUNCTION:  MLDeviceInfoMsg::~MLDeviceInfoMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLDeviceInfoMsg()
       * \brief   MLDeviceInfoMsg
       **************************************************************************/
      virtual ~MLDeviceInfoMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLDeviceInfoMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLDeviceInfoMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLDeviceInfoMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLDeviceDisconnMsg
 * \brief
 ****************************************************************************/
class MLDeviceDisconnMsg: public MLDiscovererMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  MLDeviceDisconnMsg::MLDeviceDisconnMsg
       ***************************************************************************/
      /*!
       * \fn      MLDeviceDisconnMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLDeviceDisconnMsg();

      /***************************************************************************
       ** FUNCTION:  MLDeviceDisconnMsg::~MLDeviceDisconnMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLDeviceDisconnMsg()
       * \brief   MLDeviceDisconnMsg
       **************************************************************************/
      virtual ~MLDeviceDisconnMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLDeviceDisconnMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLDeviceDisconnMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLDeviceDisconnMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}

};

/****************************************************************************/
/*!
 * \class MLBTAddrMsg
 * \brief TODO : To be removed: (Not used)
 ****************************************************************************/
class MLBTAddrMsg: public MLDiscovererMsgBase
{
   public:
      t_String *pszBTAddr;
      /***************************************************************************
       ** FUNCTION:  MLBTAddrMsg::MLBTAddrMsg
       ***************************************************************************/
      /*!
       * \fn      MLBTAddrMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLBTAddrMsg();

      /***************************************************************************
       ** FUNCTION:  MLBTAddrMsg::~MLBTAddrMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLBTAddrMsg()
       * \brief   MLBTAddrMsg
       **************************************************************************/
      virtual ~MLBTAddrMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLBTAddrMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher);

      /***************************************************************************
       ** FUNCTION:  MLBTAddrMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLBTAddrMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();

};

/****************************************************************************/
/*!
 * \class MLAppStatusMsg
 * \brief
 ****************************************************************************/
class MLAppStatusMsg: public MLDiscovererMsgBase
{
   public:

      t_U32 u32AppID;
      tenAppStatus enAppStatus;
      /***************************************************************************
       ** FUNCTION:  MLAppStatusMsg::MLAppStatusMsg
       ***************************************************************************/
      /*!
       * \fn      MLAppStatusMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAppStatusMsg();

      /***************************************************************************
       ** FUNCTION:  MLAppStatusMsg::~MLAppStatusMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAppStatusMsg()
       * \brief   MLAppStatusMsg
       **************************************************************************/
      virtual ~MLAppStatusMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAppStatusMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLAppStatusMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLAppStatusMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class MLAllAppStatusMsg
 * \brief
 ****************************************************************************/
class MLAllAppStatusMsg: public MLDiscovererMsgBase
{
   public:

      std::map<t_U32, std::map<t_String, t_String> > *prMapAllStatuses;
      /***************************************************************************
       ** FUNCTION:  MLAllAppStatusMsg::MLAllAppStatusMsg
       ***************************************************************************/
      /*!
       * \fn      MLAllAppStatusMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAllAppStatusMsg();

      /***************************************************************************
       ** FUNCTION:  MLAllAppStatusMsg::~MLAllAppStatusMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAllAppStatusMsg()
       * \brief   MLAllAppStatusMsg
       **************************************************************************/
      virtual ~MLAllAppStatusMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAllAppStatusMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLAllAppStatusMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLAllAppStatusMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLAppListMsg
 * \brief
 ****************************************************************************/
class MLAppListMsg: public MLDiscovererMsgBase
{
   public:

      // Pointer to the AppList Buffer. 
      std::vector<t_U32> *pvecszAppIds;
      //Buffer length
      t_U16 u16AppListLen;
      //Tells whether it is the certified application list or the complete application list
      t_Bool bCertAppList;

      /***************************************************************************
       ** FUNCTION:  MLAppListMsg::MLAppListMsg
       ***************************************************************************/
      /*!
       * \fn      MLAppListMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAppListMsg();

      /***************************************************************************
       ** FUNCTION:  MLAppListMsg::~MLAppListMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAppListMsg()
       * \brief   MLAppListMsg
       **************************************************************************/
      virtual ~MLAppListMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAppListMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLAppListMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLAppListMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();

};

/****************************************************************************/
/*!
 * \class MLProtocolIDMsg
 * \brief
 ****************************************************************************/
class MLProtocolIDMsg: public MLDiscovererMsgBase
{
   public:

      //! Protocol ID for the app <u32AppID>
      t_String *pszProtocolID;

      //! Application ID
      t_U32 u32AppID;

      //! Set when the protocol ID is fetched for the last application in the list
      t_Bool bIsLastApp;
      /***************************************************************************
       ** FUNCTION:  MLProtocolIDMsg::MLProtocolIDMsg
       ***************************************************************************/
      /*!
       * \fn      MLProtocolIDMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLProtocolIDMsg();

      /***************************************************************************
       ** FUNCTION:  MLProtocolIDMsg::~MLProtocolIDMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLProtocolIDMsg()
       * \brief   MLProtocolIDMsg
       **************************************************************************/
      virtual ~MLProtocolIDMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLProtocolIDMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLProtocolIDMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLProtocolIDMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};


/****************************************************************************/
/*!
 * \class MLLaunchAppStatusMsg
 * \brief
 ****************************************************************************/
class MLLaunchAppStatusMsg: public MLDiscovererMsgBase
{
   public:
      t_U32 u32AppID;
      t_Bool bLaunchAppStatus;
      /***************************************************************************
       ** FUNCTION:  MLLaunchAppStatusMsg::MLLaunchAppStatusMsg
       ***************************************************************************/
      /*!
       * \fn      MLLaunchAppStatusMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLLaunchAppStatusMsg();

      /***************************************************************************
       ** FUNCTION:  MLLaunchAppStatusMsg::~MLLaunchAppStatusMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLLaunchAppStatusMsg()
       * \brief   MLLaunchAppStatusMsg
       **************************************************************************/
      virtual ~MLLaunchAppStatusMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLLaunchAppStatusMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLLaunchAppStatusMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLLaunchAppStatusMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLTerminateAppStatusMsg
 * \brief
 ****************************************************************************/
class MLTerminateAppStatusMsg: public MLDiscovererMsgBase
{
   public:
      t_U32 u32AppID;
      t_Bool bTerminateAppStatus;
      /***************************************************************************
       ** FUNCTION:  MLTerminateAppStatusMsg::MLTerminateAppStatusMsg
       ***************************************************************************/
      /*!
       * \fn      MLTerminateAppStatusMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLTerminateAppStatusMsg();

      /***************************************************************************
       ** FUNCTION:  MLTerminateAppStatusMsg::~MLTerminateAppStatusMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLTerminateAppStatusMsg()
       * \brief   MLTerminateAppStatusMsg
       **************************************************************************/
      virtual ~MLTerminateAppStatusMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLTerminateAppStatusMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLTerminateAppStatusMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLTerminateAppStatusMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLAppInfoMsg
 * \brief
 ****************************************************************************/
class MLAppInfoMsg: public MLDiscovererMsgBase
{
   public:
      t_U32 u32AppID;

      trApplicationInfo *prAppInfo;
      /***************************************************************************
       ** FUNCTION:  MLAppInfoMsg::MLAppInfoMsg
       ***************************************************************************/
      /*!
       * \fn      MLAppInfoMsg()
       * \brief   Default constructor
       **************************************************************************/
      MLAppInfoMsg();

      /***************************************************************************
       ** FUNCTION:  MLAppInfoMsg::~MLAppInfoMsg
       ***************************************************************************/
      /*!
       * \fn      ~MLAppInfoMsg()
       * \brief   MLAppInfoMsg
       **************************************************************************/
      virtual ~MLAppInfoMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLAppInfoMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLAppInfoMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLAppInfoMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLAppIconAttr
 * \brief
 ****************************************************************************/
class MLAppIconAttr: public MLDiscovererMsgBase
{
   public:
      t_U32 u32AppID;

      trIconAttributes *prAppIconAttr;
      /***************************************************************************
       ** FUNCTION:  MLAppIconAttr::MLAppIconAttr
       ***************************************************************************/
      /*!
       * \fn      MLAppIconAttr()
       * \brief   Default constructor
       **************************************************************************/
      MLAppIconAttr();

      /***************************************************************************
       ** FUNCTION:  MLAppIconAttr::~MLAppIconAttr
       ***************************************************************************/
      /*!
       * \fn      ~MLAppIconAttr()
       * \brief   MLAppIconAttr
       **************************************************************************/
      virtual ~MLAppIconAttr() {}

      /***************************************************************************
       ** FUNCTION:  MLAppIconAttr::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLAppIconAttr::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLAppIconAttr::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLAppIconData
 * \brief To send the Application Icon data to SPI
 ****************************************************************************/
class MLAppIconData: public MLDiscovererMsgBase
{
public:
      t_U32 u32DevID;
      t_U32 u32Size;
      t_String *pszAppIconUrl;
      t_U8* pu8BinaryData;
      /***************************************************************************
       ** FUNCTION:  MLAppIconData::MLAppIconData
       ***************************************************************************/
      /*!
       * \fn      MLAppIconData()
       * \brief   Default constructor
       **************************************************************************/
      MLAppIconData();

      /***************************************************************************
       ** FUNCTION:  MLAppIconData::MLAppIconData
       ***************************************************************************/
      /*!
       * \fn      ~MLAppIconData()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAppIconData()
      {

      }
      /***************************************************************************
       ** FUNCTION:  MLAppIconData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLAppIconData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  MLAppIconData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class MLDeviceEventSubscription
 * \brief To send the event subscription response to SPI
 ****************************************************************************/
class MLDeviceEventSubscription: public MLDiscovererMsgBase
{
   public:
      t_Bool bResult;
      tenEventSubscription enEventSub;
      /***************************************************************************
       ** FUNCTION:  MLDeviceEventSubscription::MLDeviceEventSubscription
       ***************************************************************************/
      /*!
       * \fn      MLDeviceEventSubscription()
       * \brief   Default constructor
       **************************************************************************/
      MLDeviceEventSubscription();

      /***************************************************************************
       ** FUNCTION:  MLDeviceEventSubscription::MLDeviceEventSubscription
       ***************************************************************************/
      /*!
       * \fn      ~MLDeviceEventSubscription()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLDeviceEventSubscription()
      {

      }
      /***************************************************************************
       ** FUNCTION:  MLDeviceEventSubscription::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLDeviceEventSubscription::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLDeviceEventSubscription::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLSetClientProfile
 * \brief To send the Set Client Profile response to SPI
 ****************************************************************************/
class MLSetClientProfile: public MLDiscovererMsgBase
{
   public:
      t_Bool bResult;
      /***************************************************************************
       ** FUNCTION:  MLSetClientProfile::MLSetClientProfile
       ***************************************************************************/
      /*!
       * \fn      MLSetClientProfile()
       * \brief   Default constructor
       **************************************************************************/
      MLSetClientProfile();

      /***************************************************************************
       ** FUNCTION:  MLSetClientProfile::MLSetClientProfile
       ***************************************************************************/
      /*!
       * \fn      ~MLSetClientProfile()
       * \brief   Denstructor
       **************************************************************************/
      virtual ~MLSetClientProfile()
      {

      }
      /***************************************************************************
       ** FUNCTION:  MLSetClientProfile::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLSetClientProfile::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLSetClientProfile::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*!
 * \class MLAppName
 * \brief To send the application name to SPI
 ****************************************************************************/
class MLAppName: public MLDiscovererMsgBase
{
   public:
      t_U32 m_u32AppHandle;
      t_String* m_pszAppName;
      /***************************************************************************
       ** FUNCTION:  MLAppName::MLAppName
       ***************************************************************************/
      /*!
       * \fn      MLAppName()
       * \brief   Default constructor
       **************************************************************************/
      MLAppName();

      /***************************************************************************
       ** FUNCTION:  MLAppName::MLAppName
       ***************************************************************************/
      /*!
       * \fn      ~MLAppName()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLAppName()
      {

      }
      /***************************************************************************
       ** FUNCTION:  MLAppName::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLAppName::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() ;

      /***************************************************************************
       ** FUNCTION:  MLAppName::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() ;
};

/****************************************************************************/
/*!
 * \class MLAppName
 * \brief To send the application name to SPI
 ****************************************************************************/
class MLSetUPnPPublicKeyResp: public MLDiscovererMsgBase
{
   public:

      t_Bool m_bXMLValidationSucceeded;

      /***************************************************************************
       ** FUNCTION:  MLSetUPnPPublicKeyResp::MLSetUPnPPublicKeyResp
       ***************************************************************************/
      /*!
       * \fn      MLSetUPnPPublicKeyResp()
       * \brief   Default constructor
       **************************************************************************/
      MLSetUPnPPublicKeyResp();

      /***************************************************************************
       ** FUNCTION:  MLSetUPnPPublicKeyResp::`MLSetUPnPPublicKeyResp
       ***************************************************************************/
      /*!
       * \fn      ~MLSetUPnPPublicKeyResp()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLSetUPnPPublicKeyResp()
      {

      }
      /***************************************************************************
       ** FUNCTION:  MLSetUPnPPublicKeyResp::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLSetUPnPPublicKeyResp::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  MLSetUPnPPublicKeyResp::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*!
 * \class MLKeyIconData
 * \brief To send the Key Icon data to SPI
 ****************************************************************************/
class MLKeyIconData: public MLDiscovererMsgBase
{
public:
      t_U32 u32DevID;
      t_U32 u32Size;
      t_U8* pu8BinaryData;
      /***************************************************************************
       ** FUNCTION:  MLAppIconData::MLAppIconData
       ***************************************************************************/
      /*!
       * \fn      MLAppIconData()
       * \brief   Default constructor
       **************************************************************************/
      MLKeyIconData();

      /***************************************************************************
       ** FUNCTION:  MLKeyIconData::MLKeyIconData
       ***************************************************************************/
      /*!
       * \fn      ~MLKeyIconData()
       * \brief   Destructor
       **************************************************************************/
      virtual ~MLKeyIconData()
      {

      }
      /***************************************************************************
       ** FUNCTION:  MLKeyIconData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclMLVncDiscovererDispatcher* poDiscDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param   poDiscDispatcher: pointer to Discoverer Message Dispatcher
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclMLVncDiscovererDispatcher*);

      /***************************************************************************
       ** FUNCTION:  MLKeyIconData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  MLKeyIconData ::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class spi_tclMLVncDiscovererDispatcher
 * \brief Message Dispatcher for Discoverer Messages
 ****************************************************************************/
class spi_tclMLVncDiscovererDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::spi_tclMLVncDiscovererDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclMLVncDiscovererDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclMLVncDiscovererDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::~spi_tclMLVncDiscovererDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclMLVncDiscovererDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclMLVncDiscovererDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLDeviceInfoMsg* poDeviceInfoMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLDeviceInfoMsg* poDeviceInfoMsg)
       * \brief   Handles Messages of MLDeviceInfoMsg type
       * \param poDeviceInfoMsg : pointer to MLDeviceInfoMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLDeviceInfoMsg* poDeviceInfoMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLDeviceDisconnMsg* poDeviceDisconnMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLDeviceDisconnMsg* poDeviceDisconnMsg)
       * \brief   Handles Messages of MLDeviceDisconnMsg type
       * \param poDeviceDisconnMsg : pointer to MLDeviceDisconnMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLDeviceDisconnMsg* poDeviceDisconnMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLBTAddrMsg* poBTAddrMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLBTAddrMsg* poBTAddrMsg)
       * \brief   Handles Messages of MLBTAddrMsg type
       * \param poBTAddrMsg : pointer to MLBTAddrMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLBTAddrMsg* poBTAddrMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAppStatusMsg* poAppStatusMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLAppStatusMsg* poAppStatusMsg)
       * \brief   Handles Messages of MLAppStatusMsg type
       * \param poAppStatusMsg : pointer to MLAppStatusMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLAppStatusMsg* poAppStatusMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAllAppStatusMsg* poAllAppStatusMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLAllAppStatusMsg* poAllAppStatusMsg)
       * \brief   Handles Messages of MLAllAppStatusMsg type
       * \param poAllAppStatusMsg : pointer to MLAllAppStatusMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLAllAppStatusMsg* poAllAppStatusMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAppListMsg* poAppListMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLAppListMsg* poAppListMsg)
       * \brief   Handles Messages of MLAppListMsg type
       * \param poAppListMsg : pointer to MLAppListMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLAppListMsg* poAppListMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLProtocolIDMsg* poProtocolIDMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLProtocolIDMsg* poProtocolIDMsg)
       * \brief   Handles Messages of MLProtocolIDMsg type
       * \param poProtocolIDMsg : pointer to MLProtocolIDMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLProtocolIDMsg* poProtocolIDMsg) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLLaunchAppStatusMsg* poLaunchAppStatusMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLLaunchAppStatusMsg* poLaunchAppStatusMsg)
       * \brief   Handles Messages of MLLaunchAppStatusMsg type
       * \param poLaunchAppStatusMsg : pointer to MLLaunchAppStatusMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLLaunchAppStatusMsg* poLaunchAppStatusMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLTerminateAppStatusMsg* poTerminateAppStatusMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLTerminateAppStatusMsg* poTerminateAppStatusMsg)
       * \brief   Handles Messages of MLTerminateAppStatusMsg type
       * \param poTerminateAppStatusMsg : pointer to MLTerminateAppStatusMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(
               MLTerminateAppStatusMsg* poTerminateAppStatusMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAppInfoMsg* poAppInfoMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLAppInfoMsg* poAppInfoMsg)
       * \brief   Handles Messages of MLAppInfoMsg type
       * \param poAppInfoMsg : pointer to MLAppInfoMsg
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLAppInfoMsg* poAppInfoMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDAPDispatcher::vHandleDiscoveryMsg(MLAppIconAttr* poAppIconAttr)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLAppIconAttr* poAppIconAttr)
       * \brief   Handles Messages of MLAppIconAttr type
       * \param poAppIconAttr : pointer to MLAppIconAttr
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLAppIconAttr* poAppIconAttr)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLAppIconData* poAppIconData)
       * \brief   Handles Messages of MLAppIconData type
       * \param   poAppIconData : pointer to MLAppIconData
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLAppIconData* poAppIconData);

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLDeviceEventSubscription* poDeviceEventSubscription)
       * \brief   Handles Messages of MLDeviceEventSubscription type
       * \param   poDeviceEventSubscription : pointer to MLDeviceEventSubscription
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLDeviceEventSubscription* poDeviceEventSubscription)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLSetClientProfile* poSetClntProfile)
       * \brief   Handles Messages of MLSetClientProfile type
       * \param   poDeviceEventSubscription : pointer to MLSetClientProfile
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLSetClientProfile* poSetClntProfile)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLAppName* poMLAppName)
       * \brief   Handles Messages of MLAppName type
       * \param   poMLAppName : pointer to MLAppName
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLAppName* poMLAppName)const;

            /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLSetUPnPPublicKeyResp* poMLSetUPnPPublicKeyResp)
       * \brief   Handles Messages of MLSetUPnPPublicKeyResp type
       * \param   poMLAppName : pointer to MLAppName
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLSetUPnPPublicKeyResp* poMLSetUPnPPublicKeyResp) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclMLVncDiscovererDispatcher::vHandleDiscoveryMsg()
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscoveryMsg(MLKeyIconData* poKeyIconData)
       * \brief   Handles Messages of MLKeyIconData type
       * \param   poAppIconData : pointer to MLAppIconData
       **************************************************************************/
      t_Void vHandleDiscoveryMsg(MLKeyIconData* poKeyIconData)const;

      
};

#endif /* SPI_TCLMLVNCDISCOVERERDISPATCHER_H_ */
