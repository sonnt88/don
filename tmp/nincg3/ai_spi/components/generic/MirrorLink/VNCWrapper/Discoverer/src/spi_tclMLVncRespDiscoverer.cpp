
/***********************************************************************/
/*!
* \file  spi_tclMLVncRespDiscoverer.cpp
* \brief ML Discoverer Output Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    ML Discoverer Output Interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version
24.04.2014  | Shiva kumar Gurija    | XML Validation

06.05.2015  | Tejaswini HB          | Lint Fix
25.06.2015  | Sameer Chandra        | Added ML XDeviceKey Support for PSA

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include <map>
#include "SPITypes.h"
#include "RespBase.h"
#include "RespRegister.h"
#include "spi_tclMLVncRespDiscoverer.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
      #include "trcGenProj/Header/spi_tclMLVncRespDiscoverer.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclMLVncRespDiscoverer::spi_tclMLVncRespDiscoverer()
***************************************************************************/
spi_tclMLVncRespDiscoverer::spi_tclMLVncRespDiscoverer():RespBase(e16DISCOVERER_REGID)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer::spi_tclMLVncRespDiscoverer("));
}

/***************************************************************************
** FUNCTION:  spi_tclMLVncRespDiscoverer::~spi_tclMLVncRespDiscoverer()
***************************************************************************/
spi_tclMLVncRespDiscoverer::~spi_tclMLVncRespDiscoverer()
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer::~spi_tclMLVncRespDiscoverer()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostDeviceInfo
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostDeviceInfo(const t_U32 cou32DeviceHandle,
                                                   const trMLDeviceInfo& corfrDeviceInfo)
{
   (t_Void)corfrDeviceInfo;
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer::vPostDeviceInfo:Device-0x%x",cou32DeviceHandle));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostDeviceDisconnected(trUserContext..
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostDeviceDisconnected(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostDeviceDisconncted: Device-0x%x",cou32DeviceHandle));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostBTAddr(trUserContext..
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostBTAddr(trUserContext rUserContext, 
                                               const t_U32 cou32DeviceHandle,
                                               const t_Char* pcocAddr)
{
   (t_Void)pcocAddr;
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostBTAddr:Device-0x%x",cou32DeviceHandle));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppStatus(trUserContext..
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostAppStatus(trUserContext rUserContext,
                                                  const t_U32 cou32DeviceHandle,
                                                  t_U32 u32AppId,
                                                  tenAppStatus enAppStatus)
{
   (t_Void)enAppStatus;
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostAppStatus:Device-0x%x, AppID-0x%x",cou32DeviceHandle,u32AppId));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAllAppsSta(trUserContext..
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostAllAppsStatuses(trUserContext rUserContext,
                                                        const t_U32 cou32DeviceHandle,
                                                        const std::map<t_U32, std::map<t_String,t_String> >& corfmapAllStatuses)
{
   (t_Void)corfmapAllStatuses;
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostAllAppsStatuses:Device-0x%x",cou32DeviceHandle));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppList(trUserContext..
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostAppList(trUserContext rUserContext,
                                                const t_U32 cou32DeviceHandle,
                                                const std::vector<t_U32>& corfvecAppList )
{
   (t_Void)corfvecAppList;
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostAppList: Device-0x%x",cou32DeviceHandle));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostCertAppList(trUserContext..
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostCertAppList(trUserContext rUserContext,
                                                    const t_U32 cou32DeviceHandle,
                                                    const std::vector<t_U32>& corfvecAppList )
{
   (t_Void)corfvecAppList;
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostCertAppList: Device-0x%x",cou32DeviceHandle));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppProtcolID(trUserContext..
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostAppProtcolID(trUserContext &rfrUserContext,
         const t_U32 cou32DeviceHandle, const t_U32 u32AppID,
         t_String szProtocolID, t_Bool bIsLastApp)
{
SPI_INTENTIONALLY_UNUSED(bIsLastApp);
SPI_INTENTIONALLY_UNUSED(rfrUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostAppProtcolID: Device-0x%x, AppID-0x%x, ProtocolID = %s",
            cou32DeviceHandle, u32AppID, szProtocolID.c_str()));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostLaunchAppStatus(trUserContext...
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostLaunchAppStatus(trUserContext rUserContext,
                                                        const t_U32 cou32DeviceHandle,
                                                        t_U32 u32AppId,
                                                        t_Bool bResult)
{
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostLaunchAppStatus: Device-0x%x, AppID-0x%x,Response-%d"
      ,cou32DeviceHandle,u32AppId,bResult));
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostTerminateAppStatus(trUserContext..
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostTerminateAppStatus(trUserContext rUserContext,
                                                           const t_U32 cou32DeviceHandle,
                                                           t_U32 u32AppId,
                                                           t_Bool bResult)
{
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostTerminateAppStatus: Device-0x%x, AppID-0x%x,Response-%d"
      ,cou32DeviceHandle,u32AppId,bResult));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppInfo(trUserContext...
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostAppInfo(trUserContext rUserContext,
                                                const t_U32 cou32DeviceHandle,
                                                t_U32 u32AppId,
                                                const trApplicationInfo& corfrAppInfo)
{
   (t_Void)corfrAppInfo;
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostAppInfo: Device-0x%x, AppID-0x%x",cou32DeviceHandle,u32AppId));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppIconAttr(trUserContext...
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostAppIconAttr(trUserContext rUserContext,
                                                    const t_U32 cou32DeviceHandle,
                                                    t_U32 u32AppId,
                                                    const trIconAttributes& corfrAppIconAttr)
{
   (t_Void)corfrAppIconAttr;
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer:vPostAppIconAttr: Device-0x%x, AppID-0x%x",cou32DeviceHandle,u32AppId));
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostAppIconData(trUserContext...
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostAppIconData(trUserContext rUserContext,
                                                    const t_U32 u32DeviceHandle,
                                                    t_String szAppIconUrl,
                                                    const t_U8* pcou8BinaryData,
                                                    const t_U32 u32Len)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer::vPostAppIconData:Device-0x%x",u32DeviceHandle));
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   SPI_INTENTIONALLY_UNUSED(szAppIconUrl);
   SPI_INTENTIONALLY_UNUSED(pcou8BinaryData);
   SPI_INTENTIONALLY_UNUSED(u32Len);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostDevEventSubscriptionStatus()
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostDevEventSubscriptionStatus(const trUserContext& corfrUsrCntxt,
                                                                   const t_U32 u32DeviceHandle,
                                                                   tenEventSubscription enEventSubscription,
                                                                   t_Bool bResult)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer::vPostDevEventSubscriptionStatus:Device-0x%x,Response-%d",
      u32DeviceHandle,bResult));
   SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
   SPI_INTENTIONALLY_UNUSED(enEventSubscription);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostSetClientProfileResult()
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostSetClientProfileResult(const trUserContext& corfrUsrCntxt,
                                   const t_U32 u32DeviceHandle,
                                   t_Bool bResult)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer::vPostSetClientProfileResult:Device-0x%x,Response-%d",
      u32DeviceHandle,bResult));
   SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostAppName()
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostAppName(t_U32 u32DeviceHandle, 
                                                t_U32 u32AppHandle,
                                                t_String szAppName,
                                                const trUserContext& corfrUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(u32AppHandle);
   SPI_INTENTIONALLY_UNUSED(szAppName);
   SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMLVncRespDiscoverer::vPostSetUPnPPublicKeyResp()
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostSetUPnPPublicKeyResp(t_U32 u32DeviceHandle, 
                                                             t_Bool bXMLValidationSucceeded,
                                                             const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer::vPostSetUPnPPublicKeyResp:Device-0x%x,Response-%d",
      u32DeviceHandle,ETG_ENUM(BOOL,bXMLValidationSucceeded)));

   SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclMLVncRespDiscoverer::vPostKeyIconData(trUserContext...
***************************************************************************/
t_Void spi_tclMLVncRespDiscoverer::vPostKeyIconData(trUserContext rUserContext,
                                                    const t_U32 u32DeviceHandle,
                                                    const t_U8* pcou8BinaryData,
                                                    const t_U32 u32Len)
{
   ETG_TRACE_USR1(("spi_tclMLVncRespDiscoverer::vPostAppIconData:Device-0x%x",u32DeviceHandle));
   SPI_INTENTIONALLY_UNUSED(rUserContext);
   SPI_INTENTIONALLY_UNUSED(pcou8BinaryData);
   SPI_INTENTIONALLY_UNUSED(u32Len);
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
