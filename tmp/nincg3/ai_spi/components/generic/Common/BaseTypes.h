/*
 * MLBaseTypes.h
 *
 *  Created on: Sep 25, 2013
 *      Author:
 */

#ifndef BASETYPES_H_
#define BASETYPES_H_

#include <assert.h>
#include <string>

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

/**
 * \typedef t_S8
 * 8-bit signed integer.
 */
typedef int8_t t_S8;

/**
 * \typedef t_S16
 * 16-bit signed integer.
 */
typedef int16_t t_S16;

/**
 * \typedef t_S32
 * 32-bit signed integer.
 */
typedef int32_t t_S32;

/**
 * \typedef t_S64
 * 64-bit signed integer.
 */
typedef int64_t t_S64;

/**
 * \typedef t_U8
 * 8-bit unsigned integer.
 */
typedef uint8_t t_U8;

/**
 * \typedef t_U16
 * 16-bit unsigned integer.
 */
typedef uint16_t t_U16;

/**
 * \typedef t_U32
 * 32-bit unsigned integer.
 */
typedef uint32_t t_U32;

/**
 * \typedef t_U64
 * 64-bit unsigned integer.
 */
typedef uint64_t t_U64;

/**
 * \typedef t_PS32
 * pointer to 32-bit signed integer.
 */
typedef intptr_t t_PS32;

/**
 * \typedef t_PU32
 * .pointer to 32-bit unsigned integer
 */
typedef uintptr_t t_PU32;

/**
 * \typedef t_PCU8
 * pointer to 8-bit const unsigned integer.
 */
typedef const uint8_t* t_PCU8;

/**
 * \typedef t_Void
 * void type
 */
typedef void t_Void;

/**
 * \typedef t_Bool
 * bool type
 */
typedef bool t_Bool;

/**
 * \typedef t_PU8
 * pointer to 8-bit const unsigned integer.
 */
typedef t_U8* t_PU8;

/**
 * \typedef t_CPU8
 * const pointer to 8-bit unsigned integer.
 */
typedef const t_PU8 t_CPU8;

/**
 * \typedef t_Char
 * character type
 */
typedef char t_Char;

/**
* \typedef t_UChar
* Unsigned Char
*/
typedef unsigned char t_UChar;

/**
* \typedef t_PUChar
* Pointer to Unsigned Char
*/
typedef unsigned char* t_PUChar;

/**
* \typedef t_SChar
* Signed Char
*/
typedef signed char t_SChar;

/**
* \typedef t_PSChar
* Pointer to Signed Char
*/
typedef signed char* t_PSChar;



/* @todo
 typedef vnc_float_t ;
 typedef vnc_double_t ;*/

typedef float t_Float;
typedef double t_Double;
/**
 * \typedef
 * Single-precision floating point number.
 */
typedef long t_Long;
/**
 * \typedef
 * Double-precision floating point number.
 */

typedef const char* t_CString;


typedef const signed char* t_SString;
/**
 * \brief This is a preprocessor macro that is used to validate at runtime that the
 * integer types have been defined correctly.
 *
 * Because a mismatch in integer type sizes between the SPI application and the
 * Output Wrapper is a severe programming error, this macro uses the assert() macro
 * from the C standard library header assert.h to perform its validation.
 * Therefore, to use this macro, you must either include assert.h or define
 * your own equivalent to assert().  If you are using assert() from assert.h,
 * then please note that the behavior of MLVerifyIntegerTypes() is
 * (as with all other assertions) governed by the NDEBUG macro.
 *
 * It is recommended that you use this macro once, at application startup.
 */
#define MLVerifyIntegerTypes() \
    do \
    { \
      assert(sizeof(t_S8) == 1); \
      assert(sizeof(t_S16) == 2); \
      assert(sizeof(t_S32) == 4); \
      assert(sizeof(t_S64) == 8); \
      assert(sizeof(t_PS32) == sizeof(void *)); \
      assert(sizeof(t_U8) == 1); \
      assert(sizeof(t_U16) == 2); \
      assert(sizeof(t_U32) == 4); \
      assert(sizeof(t_U64) == 8); \
      assert(sizeof(t_PU32) == sizeof(void *)); \
    } while (0)
#ifdef __cplusplus
}

/**
 * \typedef t_String
 * string
 */
typedef std::string t_String;

//To Avoid Lint warnings:
//void unused;

#define SPI_INTENTIONALLY_UNUSED(PARAM1)                    \
{\
   (t_Void)PARAM1;\
}\
//
//#define SPI_INTENTIONALLY_UNUSED_CONSTANT(PARAM1)\
//{\
//   unused = (void)const_cast<>(PARAM1);\
//}\
//

#endif

#endif /* MLBASETYPES_H_ */
