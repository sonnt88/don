/*!
 *******************************************************************************
 * \file             spi_tclDeviceSelector.cpp
 * \brief            Handles select and deselection of device
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Handles select and deselection of device
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 16.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 10.12.2014 | Shihabudheen P M             | Changed for blocking device usage 
                                             preference updates during 
                                             select/deselect is in progress. 
 05.02.2015 |  Ramya Murthy                | Changes to set different intervals in
                                             component response timer for different SPI components

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclDevSelResp.h"
#include "spi_tclMediator.h"
#include "spi_tclFactory.h"
#include "spi_tclConnSettings.h"
#include "spi_tclSelectionStrategy.h"
#include "spi_tclDeviceSelector.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclDeviceSelector.cpp.trc.h"
#endif
#endif

static const t_U32 scou32ConnRespDummy = 1;
static const t_U32 scou32MinNumberofConnections= 1;
static t_U32 su32CompResponseTime10s = 10000;
static t_U32 su32CompResponseTime25s = 25000;
static t_U32 su32DeviceHandleMax= 0xFFFFFFFF;
//! Changing connection response time to 25 seconds as mirrorlink switch might sometime take >20 seconds
static t_U32 su32ConnCompResponseTimeinms = su32CompResponseTime25s;
static t_U32 su32MaximumRetries = 5;
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

t_U32 spi_tclDeviceSelector::m_u32CurrSelectedDevice = 0;

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::spi_tclDeviceSelector
 ***************************************************************************/
spi_tclDeviceSelector::spi_tclDeviceSelector(spi_tclDevSelResp *poRespInterface) :
                  m_poMediator(NULL),
                  m_poDevSelResp(poRespInterface),
                 /* m_u32CurrSelectedDevice(0),*/
                  m_bDeviceSwitchRequest(false),
                  m_bIsDevSelectorBusy(false),
                  m_bIsSelectionTimerRunnning(false),
                  m_enSelectedComp(e32COMPID_UNKNOWN),
                  m_rCompRespTimerID(0)
{
   ETG_TRACE_USR1(("Creating spi_tclDeviceSelector\n"));
   m_poMediator = spi_tclMediator::getInstance();
   SPI_NORMAL_ASSERT(NULL == m_poMediator);
   //bInitialize();
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::~spi_tclDeviceSelector
 ***************************************************************************/
spi_tclDeviceSelector::~spi_tclDeviceSelector()
{
   ETG_TRACE_USR1(("Destroying spi_tclDeviceSelector \n"));

   m_poDevSelResp              = NULL;
   m_poMediator                = NULL;
   m_rCompRespTimerID          = 0;
   m_bDeviceSwitchRequest      = false;
   m_bIsDevSelectorBusy        = false;
   m_bIsSelectionTimerRunnning = false;
   m_enSelectedComp            = e32COMPID_UNKNOWN;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::bInitialize
 ***************************************************************************/
t_Bool spi_tclDeviceSelector::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::bInitialize \n"));
   vRegisterCallbacks();
   //! Even if starting timer fails, initialize can return true as this
   //! doen't affect major functionality
   //! Currently timer is not used by any projects
   //! vStartSelectionTimer();
   return true;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDeviceSelector::vUnInitialize()
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vUnInitialize \n"));
   //! Add code: Currently callback deregistration not required.
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDeviceSelector::vLoadSettings(const trSpiFeatureSupport&...)
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vLoadSettings \n"));
   SPI_INTENTIONALLY_UNUSED(rfcrSpiFeatureSupp);
  //! Cancel component response timer if there is a state change from ON to OFF
   Timer* poTimer = Timer::getInstance();
   if ((0 != m_rCompRespTimerID) && (NULL != poTimer))
   {
      poTimer->CancelTimer(m_rCompRespTimerID);
   }
   //! Reset the flags on loadsettings
   vUpdateDevSelectorBusyStatus(false);
   m_enSelectedComp = e32COMPID_UNKNOWN;
   m_u32CurrSelectedDevice = 0;
   m_bDeviceSwitchRequest = false;
   su32ConnCompResponseTimeinms =su32CompResponseTime25s;
   //!load selection sequences
   vLoadSelectionSequence();
   vLoadDeselectionSequence();

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDeviceSelector::vSaveSettings()
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vSaveSettings()
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vSaveSettings \n"));

   //! Reset the component response timer to 1ms as no response is expected form connection manager
   //! after savesettings. This is because there is no updates from mediaplayer
   su32ConnCompResponseTimeinms =scou32ConnRespDummy;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vSelectDevice
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vSelectDevice(const t_U32 cou32DeviceHandle,
         tenDeviceConnectionType enDevConnType,
         tenDeviceConnectionReq enDevConnReq,
         tenEnabledInfo enDAPUsage,
         tenEnabledInfo enCDBUsage,
         tenDeviceCategory enDevCategory,
         t_Bool bIsHMITrigger,
         const trUserContext corUsrCntxt)
{
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vSelectDevice cou32DeviceHandle = 0x%x,"
            " enDevConnReq = %d \n, enDAPUsage = %d, enCDBUsage = %d Device category = %d bIsHMITrigger = %d\n",
            cou32DeviceHandle, ETG_ENUM(CONNECTION_REQ, enDevConnReq),
            ETG_ENUM(ENABLED_INFO,enDAPUsage),  ETG_ENUM(ENABLED_INFO,enCDBUsage),
            ETG_ENUM(DEVICE_CATEGORY, enDevCategory),ETG_ENUM(BOOL,bIsHMITrigger)));

   SPI_INTENTIONALLY_UNUSED(enDevConnType);
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   tenDeviceCategory enPreferredCategory = e8DEV_TYPE_UNKNOWN;
   tenDeviceSelectionMode enDevSelMode = e16DEVICESEL_AUTOMATIC;
   if(NULL != poConnSettings)
   {
      enPreferredCategory = poConnSettings->enGetTechnologyPreference();
      enDevSelMode = poConnSettings->enGetDeviceSelectionMode();
   }

   //! If the device category is provided by HMI, set the device category
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if ((NULL != poSPIFactory) && (e8DEVCONNREQ_SELECT == enDevConnReq))
   {
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         tenDeviceCategory enStoredDevCategory=  poConnMngr->enGetDeviceCategory(cou32DeviceHandle);
         ETG_TRACE_USR1(("Preferred category = %d Stored device category = %d,  Switching to Preferred category ",
                  ETG_ENUM(DEVICE_CATEGORY, enPreferredCategory),ETG_ENUM(DEVICE_CATEGORY, enStoredDevCategory)));
         //! if HMI sends the device category, use it for selecting the device
         if(e8DEV_TYPE_UNKNOWN != enDevCategory)
         {
            poConnMngr->vSetDeviceCategory(cou32DeviceHandle, enDevCategory);
         }
         else
         {
            vEvaluateSPIPreference(cou32DeviceHandle);
         }
      }
   }

   tenErrorCode enErrorCode = e8NO_ERRORS;
   if(true == bValidateSelectDeviceRequest(cou32DeviceHandle, enDevConnReq, enErrorCode))
   {
      //! Set the flag while processing the request
      vUpdateDevSelectorBusyStatus(true);
      //! Store the request info
      m_rSelectDevReqInfo.u32DeviceHandle = cou32DeviceHandle;
      m_rSelectDevReqInfo.bIsHMITrigger = bIsHMITrigger;
      m_rSelectDevReqInfo.enCDBUsage = enCDBUsage;
      m_rSelectDevReqInfo.enDAPUsage = enDAPUsage;
      m_rSelectDevReqInfo.enDevConnReq = enDevConnReq;
      m_rSelectDevReqInfo.corUsrCntxt = corUsrCntxt;


      //! Check if any other device is selected
      vEvaluateDeviceSelectionReq ();
   } // if(true == bValidateSelectDeviceRequest
   else
   {
      //! Post Result directly to HMI if processing the request is not required
      vSendSelectDeviceResultToHMI(cou32DeviceHandle, enDevConnReq, enErrorCode,
               bIsHMITrigger, corUsrCntxt);
   }// else part of if(true == bValidateSelectDeviceRequest
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vReceiveSelectDeviceResCb
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vReceiveSelectDeviceResCb(tenCompID enCompID,
         tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector:vReceiveSelectDeviceResCb: Device selection result received from component = %d, Reported ErrorCode = %d \n",
         ETG_ENUM(COMPONENT_ID,enCompID), ETG_ENUM(ERROR_CODE, enErrorCode)));

   if (enCompID == m_enSelectedComp)
   {
      //! Cancel the timer if the response is received for the current component
      Timer* poTimer = Timer::getInstance();
      if ((0 != m_rCompRespTimerID) && (NULL != poTimer))
      {
         poTimer->CancelTimer(m_rCompRespTimerID);
      }
      trDeviceSelectionInfo rSelDevInfo =
               (false == m_bDeviceSwitchRequest) ? m_rSelectDevReqInfo
                                                 : m_rDeselectDevReqInfo;
      //! Order for Selection e32COMPID_BLUETOOTH -> e32COMPID_APPMANAGER ->
      //!            e32COMPID_VIDEO -> e32COMPID_AUDIO -> e32COMPID_CONNECTIONMANAGER
      //! Order for Deselection e32COMPID_BLUETOOTH -> e32COMPID_AUDIO ->
      //!            e32COMPID_VIDEO -> e32COMPID_APPMANAGER -> e32COMPID_CONNECTIONMANAGER

      //! On Successful Select device result, send device selection request for next
      //! component

      tenDeviceCategory enDevCat = e8DEV_TYPE_UNKNOWN;
      spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
      if (NULL != poSPIFactory)
      {
         spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
         if (NULL != poConnMngr)
         {
            enDevCat = poConnMngr->enGetDeviceCategory(rSelDevInfo.u32DeviceHandle);
         }
      }

      //! Trigger device selection sequence if the previous component has returned success
      //! Trigger device deselection sequence even if the deselection for previous component failed
      if ((e8NO_ERRORS == enErrorCode) || (e8DEVCONNREQ_DESELECT == rSelDevInfo.enDevConnReq))
      {
         tenCompID enNextCompID = enGetNextComponentID(enDevCat,rSelDevInfo.enDevConnReq,enCompID);
         //! Check if selection request is sent to all components. If so then send the result to HMI
         //! and other subcomponents
         if(e32COMPID_UNKNOWN == enNextCompID)
         {
            vSendSelectDeviceResult(rSelDevInfo.u32DeviceHandle,
                                   rSelDevInfo.enDevConnReq, e8NO_ERRORS,
                                   rSelDevInfo.bIsHMITrigger, rSelDevInfo.corUsrCntxt);
         }
         else
         {
            vSendDeviceSelectionReq(rSelDevInfo, enNextCompID);
         }
      } // if (true == bSelDevRes)
      else
      {
         //! Send Error to HMI if device selection failure is returned
         vSendSelectDeviceResult(rSelDevInfo.u32DeviceHandle,
                  rSelDevInfo.enDevConnReq, enErrorCode,
                  rSelDevInfo.bIsHMITrigger, rSelDevInfo.corUsrCntxt);
      } // else case for  if (true == bSelDevRes)
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vDeviceDisconnectionCb
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vDeviceDisconnectionCb(
         const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"spi_tclDeviceSelector::vDeviceDisconnectionCb"\
            " Device-0x%x is disconnected .\n", cou32DeviceHandle));
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vDeviceDisconnectionCb:"
            " cou32DeviceHandle = 0x%x", cou32DeviceHandle));
   if (cou32DeviceHandle == m_u32CurrSelectedDevice)
   {
      //! Send device deselection request on device disconnection
      vOnAutomaticDeviceSelection(m_u32CurrSelectedDevice,
               e8DEVCONNREQ_DESELECT);
      m_u32CurrSelectedDevice = 0;
   }// if(cou32DeviceHandle == m_u32CurrSelectedDevice)
}


/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vDeviceConnectionCb
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vDeviceConnectionCb(
         const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vDeviceConnectionCb received for cou32DeviceHandle = 0x%x", cou32DeviceHandle));
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         //! Check if the Mirrorlink/Carplay/Android Auto is turned off based on the device
         tenDeviceCategory enDevCategory = poConnMngr->enGetDeviceCategory(cou32DeviceHandle);
         tenEnabledInfo enUsagePref = e8USAGE_ENABLED;
         poConnMngr->bGetDeviceUsagePreference(cou32MAX_DEVICEHANDLE, enDevCategory, enUsagePref );
         if(e8USAGE_ENABLED == enUsagePref)
         {
            vApplySelectionStrategy();
         }
      } //if (NULL != poConnMngr)
   }
}
/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vApplySelectionStrategy
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vApplySelectionStrategy()
{
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vApplySelectionStrategy : Currently Selected device = %d\n ", m_u32CurrSelectedDevice));
   t_U32 u32SelectedDevice = 0;

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();

   std::map<t_U32, trEntireDeviceInfo> mapDeviceInfo;

   if (NULL != poSPIFactory)
   {
      //! Fetch the device list from connection manager
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         poConnMngr->vGetEntireDeviceList(mapDeviceInfo);

         spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
         tenDeviceSelectionMode enSelMode = e16DEVICESEL_MANUAL;
         if (NULL != poConnSettings)
         {
            enSelMode = poConnSettings->enGetDeviceSelectionMode();
         }

         ETG_TRACE_USR4(("Automatic selection mode = %d Selection timer running state = %d", ETG_ENUM(SELECTION_MODE,
                  enSelMode), ETG_ENUM(BOOL, m_bIsSelectionTimerRunnning)));

         spi_tclSelectionStrategy oSelStrategy;

         tenDeviceCategory enSelDevCategory = e8DEV_TYPE_UNKNOWN;
         if ((e16DEVICESEL_MANUAL != enSelMode) || (true == m_bIsSelectionTimerRunnning))
         {
            t_U32 u32NoofConnectedDevices = poConnMngr->u32GetNoofConnectedDevices();
            ETG_TRACE_USR1(("spi_tclDeviceSelector::u32GetNoofConnectedDevices:  = %d \n", u32NoofConnectedDevices));
            oSelStrategy.vUpdateDeviceList(mapDeviceInfo);
            u32SelectedDevice = oSelStrategy.u32ApplySelectionStrategy();
            tenErrorCode enErrorcode = e8NO_ERRORS;

            enSelDevCategory = poConnMngr->enGetDeviceCategory(u32SelectedDevice);
            //! If the device category is unknown, use the preferred device category.
            if((e8DEV_TYPE_UNKNOWN == enSelDevCategory) && (NULL != poConnSettings))
            {
               enSelDevCategory = poConnSettings->enGetTechnologyPreference();
            }
            //! if more than one device is connected apply selection strategy again
            if ((false == bValidateSelectDeviceRequest(u32SelectedDevice, e8DEVCONNREQ_SELECT, enErrorcode, enSelDevCategory))
                  && (e8RESOURCE_BUSY != enErrorcode) && (u32NoofConnectedDevices > scou32MinNumberofConnections))
            {
               oSelStrategy.vSetDeviceSelectError(u32SelectedDevice, true);
               u32SelectedDevice = oSelStrategy.u32ApplySelectionStrategy();
               enSelDevCategory = poConnMngr->enGetDeviceCategory(u32SelectedDevice);
            }

         }
         //! Check if the previous selection of the automatically selected device failed
         t_Bool bIsSelectError = poConnMngr->bIsSelectError(u32SelectedDevice);

         //! Check if the category of the selected device is enabled
         tenEnabledInfo enUsagePref = e8USAGE_ENABLED;
         //! Commented user preference check as this is already done with user deselection flag
         poConnMngr->bGetDeviceUsagePreference(cou32MAX_DEVICEHANDLE, enSelDevCategory, enUsagePref);
        // t_Bool bUserPref = (e8PROJECTION_NOT_PREFERRED != poConnMngr->enGetUserPreference(u32SelectedDevice));
         if ((e8USAGE_ENABLED != enUsagePref) /*|| (false == bUserPref)*/)
         {
            u32SelectedDevice = 0;
         }

         //! Send device selection request on choosing device by selection strategy
         //! only if the previous activation of the device succeeded.
         //! This is required to prevent back to back selection requests for IPhone4
         if ((0 != u32SelectedDevice) && (false == bIsSelectError) && (0 == m_u32CurrSelectedDevice))
         {
            vOnAutomaticDeviceSelection(u32SelectedDevice, e8DEVCONNREQ_SELECT, enSelDevCategory);
         }
      } //if (NULL != poConnMngr)
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vOnAutomaticDeviceSelection
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vOnAutomaticDeviceSelection(
         const t_U32 cou32DeviceHandle, tenDeviceConnectionReq enDevConnReq,  tenDeviceCategory enDevCategory)
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vOnAutomaticDeviceSelection: Received %d request without user trigger for DeviceHandle = 0x%x \n",
            ETG_ENUM(CONNECTION_REQ, enDevConnReq), cou32DeviceHandle));

   tenDeviceConnectionType enDevConnType = e8UNKNOWN_CONNECTION;
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      //! Fetch the connection type from connection manager
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         enDevConnType = poConnMngr->enGetDeviceConnType(cou32DeviceHandle);
      } //if (NULL != poConnMngr)
   } // if (NULL != poSPIFactory)

   //! trigger automatic device selection
   if (0 != cou32DeviceHandle)
   {
      //! Dummy user context sent as select device is internally triggered
      trUserContext rUsrCntxt;
      vSelectDevice(cou32DeviceHandle, enDevConnType, enDevConnReq,
               e8USAGE_ENABLED, e8USAGE_ENABLED, enDevCategory, false, // Trigger is not from HMI
               rUsrCntxt);
   } // if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vOnSetUserDeselect
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vOnSetUserDeselect(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vOnSetUserDeselect: Setting user deselection flag for DeviceHandle = 0x%x \n", cou32DeviceHandle));

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      //! Fetch the connection type from connection manager
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         poConnMngr->vSetUserDeselectionFlag(cou32DeviceHandle, true);
      } //if (NULL != poConnMngr)
   } // if (NULL != poSPIFactory)
}

/***************************************************************************
 ** FUNCTION: t_U32 spi_tclDeviceSelector::u32GetSelectedDeviceHandle()
 ***************************************************************************/
t_U32 spi_tclDeviceSelector::u32GetSelectedDeviceHandle() const
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::u32GetSelectedDeviceHandle = 0x%x \n",
            m_u32CurrSelectedDevice));
   return m_u32CurrSelectedDevice;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDeviceSelector::vSetDeviceSelectionMode()
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vSetDeviceSelectionMode(tenDeviceSelectionMode enDeviceSelectionMode)
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vSetDeviceSelectionMode [DESC] Request to set the selection mode to %d \n",
            ETG_ENUM(DEVICE_SELECTION_MODE, enDeviceSelectionMode)));

   std::map<t_U32, trEntireDeviceInfo> mapDeviceInfo;
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      //! Fetch the device list from connection manager
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         poConnMngr->vGetEntireDeviceList(mapDeviceInfo);
      }
   }

   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   if (NULL != poConnSettings)
   {
      //! Trigger automatic device selection if HMI has changes from manula to automatic selection mode
      tenDeviceSelectionMode enStoredDevSelMode = poConnSettings->enGetDeviceSelectionMode();
      if ((enDeviceSelectionMode != enStoredDevSelMode))
      {
         m_enDeviceSelectionMode = enDeviceSelectionMode;
         //! Apply selection strategy if the device selection mode has changed from manual to automatic
         if (e16DEVICESEL_AUTOMATIC == enDeviceSelectionMode)
         {
            //! Store the setting if selection mode changes from manual to automatic
            poConnSettings->vSetDeviceSelectionMode(enDeviceSelectionMode);
            //! Trigger automatic selection
            //!Dummy user context
            trUserContext rUserCtxt;
            //! Choose a device for selection
            spi_tclSelectionStrategy oSelStrategy;
            oSelStrategy.vUpdateDeviceList(mapDeviceInfo);
            t_U32 u32SelectedDevice = oSelStrategy.u32GetSPICapableDevice();
            //Get the required switch type
            tenDeviceCategory enRequiredSwitch = enGetNextSwitchType(u32SelectedDevice);
            //! Send Device selection request
            vSelectDevice(u32SelectedDevice,e8USB_CONNECTED,e8DEVCONNREQ_SELECT,
                  e8USAGE_UNKNOWN,e8USAGE_UNKNOWN,enRequiredSwitch,false,rUserCtxt);
          }
         else 
         {
            poConnSettings->vSetDeviceSelectionMode(enDeviceSelectionMode);
         }
      }
   }

}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vRegisterCallbacks
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vRegisterCallbacks()
{
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vRegisterCallbacks() Entered\n"));
   /*lint -esym(40,fvOnSelectDeviceRes) fvOnSelectDeviceRes Undeclared identifier */
   /*lint -esym(40,fvOnDeviceDisconnection)fvOnDeviceDisconnection Undeclared identifier */
   /*lint -esym(40,fvAutoSelectDevice) fvAutoSelectDevice Undeclared identifier */
   /*lint -esym(40,fvSetUserDeselect) fvSetUserDeselect Undeclared identifier */
   /*lint -esym(40,fvApplySelStrategy) fvApplySelStrategy Undeclared identifier */
   /*lint -esym(40,fvApplySelStrategy) fvApplySelStrategy Undeclared identifier */
   /*lint -esym(40,_1) _1 Undeclared identifier */
   /*lint -esym(40,_2) _2 Undeclared identifier */
   //! Register for callbacks
   trSelectDeviceCallbacks rSelDevCbs;
   rSelDevCbs.fvOnSelectDeviceRes = std::bind(
            &spi_tclDeviceSelector::vReceiveSelectDeviceResCb, this,
            std::placeholders::_1, std::placeholders::_2);

   rSelDevCbs.fvOnDeviceDisconnection = std::bind(
            &spi_tclDeviceSelector::vDeviceDisconnectionCb, this,
            std::placeholders::_1);

   rSelDevCbs.fvOnDeviceConnection = std::bind(
            &spi_tclDeviceSelector::vDeviceConnectionCb, this,
            std::placeholders::_1);

   rSelDevCbs.fvAutoSelectDevice = std::bind(
            &spi_tclDeviceSelector::vOnAutomaticDeviceSelection, this,
            std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);

   rSelDevCbs.fvSetUserDeselect = std::bind(
            &spi_tclDeviceSelector::vOnSetUserDeselect, this,
            std::placeholders::_1);

   rSelDevCbs.fvApplySelStrategy = std::bind(&spi_tclDeviceSelector::vApplySelectionStrategy,
            this);

   if (NULL != m_poMediator)
   {
      m_poMediator->vRegisterCallbacks(rSelDevCbs);
   } // if (NULL != m_poMediator)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vValidateSelectDeviceRequest
 ***************************************************************************/
t_Bool spi_tclDeviceSelector::bValidateSelectDeviceRequest(
         const t_U32 cou32DeviceHandle, tenDeviceConnectionReq enDevConnReq,
         tenErrorCode &rfenErrorCode, tenDeviceCategory enDevCat)
{
   t_Bool bValidRequest = false;

   //! Get the connection status and device validity for cou32DeviceHandle
   tenDeviceConnectionStatus enConnStatus = e8DEV_NOT_CONNECTED;
   t_Bool bValidDevice = false;
   t_Bool bUSBConnected = false;
   tenEnabledInfo enDevPrjStatus = e8USAGE_ENABLED;
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         enConnStatus = poConnMngr->enGetDeviceConnStatus(cou32DeviceHandle);
         bValidDevice = poConnMngr->bIsDeviceValid(cou32DeviceHandle);
         bUSBConnected = poConnMngr->bGetUSBConnectedFlag(cou32DeviceHandle);
         //! If device category is not available, use the stored device category
         if(e8DEV_TYPE_UNKNOWN == enDevCat)
         {
            enDevCat = poConnMngr->enGetDeviceCategory(cou32DeviceHandle);
         }
         poConnMngr->bGetDevProjUsage(enDevCat, enDevPrjStatus);
      }//if (NULL != poConnMngr)
   }//if (NULL != poSPIFactory)

   //! Check if Device selector is busy with another device selection request
   if (true == m_bIsDevSelectorBusy)
   {
      rfenErrorCode = e8RESOURCE_BUSY;
   }//if (true == m_bIsDevSelectorBusy)

   //! Check if the device handle is valid
   else if ((0 == cou32DeviceHandle) || (false == bValidDevice))
   {
      rfenErrorCode = e8INVALID_DEV_HANDLE;
   } // if (0 == cou32DeviceHandle)

   //! Check if the selected device is connected
   else if (((e8DEV_NOT_CONNECTED == enConnStatus)&& (false == bUSBConnected)) && (e8DEVCONNREQ_SELECT
            == enDevConnReq))
   {
      rfenErrorCode = e8DEVICE_NOT_CONNECTED;
   } // if ((e8DEV_NOT_CONNECTED == enConnStatus) && ...

   //! Check if the device is already selected or deselected
   else if (((cou32DeviceHandle == m_u32CurrSelectedDevice)
            && (e8DEVCONNREQ_SELECT == enDevConnReq)) || ((0
            == m_u32CurrSelectedDevice) && (e8DEVCONNREQ_DESELECT
            == enDevConnReq)))
   {
      rfenErrorCode = e8NO_ERRORS;
   } // if ((cou32DeviceHandle == m_u32CurrSelectedDevice) && ..

   //! check if the particular SPI technology is enabled
   else if ((e8USAGE_ENABLED != enDevPrjStatus) && (e8USAGE_CONF_REQD != enDevPrjStatus))
   {
      rfenErrorCode = e8OPERATION_REJECTED;
   }

   else
   {
      bValidRequest = true;
   }

   if (e8NO_ERRORS != rfenErrorCode)
   {
      ETG_TRACE_ERR((" Invalid Device Selection Request  for DeviceHandle = %d Connection status = %d Errorcode = %d\n",
               cou32DeviceHandle, enConnStatus, rfenErrorCode));
   } // if (e8NO_ERRORS != enErrorCode)

   ETG_TRACE_USR2(("spi_tclDeviceSelector::bValidateSelectDeviceRequest: Device selection request validation result = %d  ",
            ETG_ENUM(BOOL,bValidRequest)));
   return bValidRequest;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vSendDeviceSelectionReq
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vSendDeviceSelectionReq(
         const trDeviceSelectionInfo &corfrSelDevinfo, tenCompID enCompID)
{
   ETG_TRACE_USR2(("spi_tclDeviceSelector::vSendDeviceSelectionReq: Sending %d request for DeviceHandle = 0x%x to component =%d \n",
            ETG_ENUM(CONNECTION_REQ,corfrSelDevinfo.enDevConnReq),
            corfrSelDevinfo.u32DeviceHandle,
            ETG_ENUM(COMPONENT_ID,enCompID)));

   m_enSelectedComp = enCompID;
   //! Start the component response timer and wait for the response
   vStartComponentResponseTimer();
   tenDeviceConnectionType enDevConnType = e8UNKNOWN_CONNECTION;
   tenDeviceCategory enDevCategory = e8DEV_TYPE_UNKNOWN;
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();

   //! Send Device Selection request to interested SPI components
   if (NULL != poSPIFactory)
   {
      //! Fetch the device category and connection type from connection manager
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         enDevCategory = poConnMngr->enGetDeviceCategory(corfrSelDevinfo.u32DeviceHandle);
         enDevConnType= poConnMngr->enGetDeviceConnType(corfrSelDevinfo.u32DeviceHandle);
      } //if (NULL != poConnMngr)

      switch (enCompID)
      {
         case e32COMPID_BLUETOOTH:
         {
            spi_tclBluetooth *poBluetooth = poSPIFactory->poGetBluetoothInstance();
            if (NULL != poBluetooth)
            {
               poBluetooth->vOnSPISelectDeviceRequest(corfrSelDevinfo.u32DeviceHandle,
                        corfrSelDevinfo.enDevConnReq);
            } //if (NULL != poBluetooth)
            break;
         }// case e32COMPID_BLUETOOTH:

         case e32COMPID_APPMANAGER:
         {
            spi_tclAppMngr *poAppMngr = poSPIFactory->poGetAppManagerInstance();
            if (NULL != poAppMngr)
            {
               poAppMngr->vSelectDevice(corfrSelDevinfo.u32DeviceHandle,
                        enDevConnType, corfrSelDevinfo.enDevConnReq, enDevCategory,
                        corfrSelDevinfo.corUsrCntxt);
            } //if(NULL != poAppMngr)
            break;
         }//case e32COMPID_APPMANAGER:

         case e32COMPID_VIDEO:
         {
            spi_tclVideo *poVideo = poSPIFactory->poGetVideoInstance();
            if (NULL != poVideo)
            {
               poVideo->vSelectDevice(corfrSelDevinfo.u32DeviceHandle,
                        corfrSelDevinfo.enDevConnReq, enDevCategory,
                        corfrSelDevinfo.corUsrCntxt);
            } //if(NULL != poVideo )
            break;
         }//case e32COMPID_VIDEO:

         case e32COMPID_AUDIO:
         {
            spi_tclAudio *poAudio = poSPIFactory->poGetAudioInstance();
            if (NULL != poAudio)
            {
               poAudio->vSelectDevice(corfrSelDevinfo.u32DeviceHandle,
                        corfrSelDevinfo.enDevConnReq, enDevCategory);
            } //if(NULL != m_poAudio)*/
            break;
         }//case e32COMPID_AUDIO:

         case e32COMPID_CONNECTIONMANAGER:
         {
            if (NULL != poConnMngr)
            {
               poConnMngr->vOnSelectDevice(corfrSelDevinfo.u32DeviceHandle,
                        enDevConnType, corfrSelDevinfo.enDevConnReq,
                        corfrSelDevinfo.enDAPUsage, corfrSelDevinfo.enCDBUsage,
                        corfrSelDevinfo.bIsHMITrigger,
                        corfrSelDevinfo.corUsrCntxt);
            } //if (NULL != poConnMngr)
            break;
         }// case e32COMPID_CONNECTIONMANAGER:

         case e32COMPID_INPUTHANDLER:
          {
             spi_tclInputHandler* poInputHandler = poSPIFactory->poGetInputHandlerInstance();
             if (NULL != poInputHandler)
               {
                 poInputHandler->vSelectDevice(corfrSelDevinfo.u32DeviceHandle,
                                 corfrSelDevinfo.enDevConnReq, enDevCategory);
               }
          }
          break;

         case e32COMPID_DATASERVICE:
          {
            spi_tclDataService* poDataService = poSPIFactory->poGetDataServiceInstance();
            if (NULL != poDataService)
            {
               poDataService->vSelectDevice(corfrSelDevinfo.u32DeviceHandle, corfrSelDevinfo.enDevConnReq,
                     enDevCategory);
            }
         }
          break;
          default:
         {
            ETG_TRACE_ERR((" Unknown component ID = %d \n", enCompID));
            break;
         }// default:
      } //switch (enCompID)
   } // if (NULL != poSPIFactory)
}


/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vEvaluateDeviceSelectionReq
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vEvaluateDeviceSelectionReq()
{
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vEvaluateDeviceSelectionReq(): Evaluating device selection request: Currently Selected Device =0x%x \n", m_u32CurrSelectedDevice));

   tenDeviceConnectionStatus enConnStatus = e8DEV_NOT_CONNECTED;
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         //! Switch Device Case:
         //! Send device disconnect if some other device is selected
         if ((e8DEVCONNREQ_SELECT == m_rSelectDevReqInfo.enDevConnReq) && (0 != m_u32CurrSelectedDevice))
         {
            ETG_TRACE_USR2((" Device %d already selected. Deselecting it ... \n", m_u32CurrSelectedDevice));

            //! Send Device deselection request
            m_bDeviceSwitchRequest = true;
            m_rDeselectDevReqInfo.u32DeviceHandle = m_u32CurrSelectedDevice;
            m_rDeselectDevReqInfo.bIsHMITrigger = true;
            m_rDeselectDevReqInfo.enDevConnReq = e8DEVCONNREQ_DESELECT;

            //! on Switch device set the user deslection flag for the device being deselected
            poConnMngr->vSetUserDeselectionFlag(m_rDeselectDevReqInfo.u32DeviceHandle, true);
            poConnMngr->vSetUserDeselectionFlag(m_rSelectDevReqInfo.u32DeviceHandle, false);

            //! Send the deselection request
            tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(m_u32CurrSelectedDevice);
            tenCompID enCompID = enGetNextComponentID(enDevCat, m_rDeselectDevReqInfo.enDevConnReq, e32COMPID_UNKNOWN);
            vSendDeviceSelectionReq(m_rDeselectDevReqInfo, enCompID);
         } // if ((e8DEVCONNREQ_SELECT == m_rSelectDevReqInfo.enDevConnReq)
         else
         {
            //! Normal Selection/Deselection Case
            //! Set user deselection flag if the device is deselected by the user
            //! via HMI. Otherwise reset the flag. this flag will be read before
            //! triggering automatic selection

            enConnStatus = poConnMngr->enGetDeviceConnStatus(m_rSelectDevReqInfo.u32DeviceHandle);

            if (e8DEV_CONNECTED == enConnStatus)
            {
               //! Set the user deselection flag if deselection is triggered as a result
               //! of user action (any HMI trigger that results in deselection)when device is connected
               //! other than the switch device case
               t_Bool bIsHMIDeselect = ((e8DEVCONNREQ_DESELECT == m_rSelectDevReqInfo.enDevConnReq) && (true
                     == m_rSelectDevReqInfo.bIsHMITrigger) && (false == m_bDeviceSwitchRequest));

               ETG_TRACE_USR4((" spi_tclDeviceSelector::vEvaluateDeviceSelectionReq() bisHMIDeselect =%d "
                  " m_rDeselectDevReqInfo.u32DeviceHandle = %d m_rSelectDevReqInfo.u32DeviceHandle = %d\n", ETG_ENUM(
                     BOOL, bIsHMIDeselect), m_rDeselectDevReqInfo.u32DeviceHandle, m_rSelectDevReqInfo.u32DeviceHandle));

               poConnMngr->vSetUserDeselectionFlag(m_rSelectDevReqInfo.u32DeviceHandle, bIsHMIDeselect);
            }
            //! Send the selection request
            tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(m_rSelectDevReqInfo.u32DeviceHandle);
            //! corrected the selection sequence for mirrorlink in case of deselect and select from HMI
            tenCompID enCompID = enGetNextComponentID(enDevCat,m_rSelectDevReqInfo.enDevConnReq, e32COMPID_UNKNOWN);
            vSendDeviceSelectionReq(m_rSelectDevReqInfo, enCompID);
         } //else case for if (e8DEVCONNREQ_SELECT ...
      }//if (NULL != poConnMngr)
   }//if (NULL != poSPIFactory)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::bDeviceSelectionComplete
 ***************************************************************************/
t_Bool spi_tclDeviceSelector::bDeviceSelectionComplete(const t_U32 cou32DeviceHandle,
         tenDeviceConnectionReq enDevConnReq, tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::bDeviceSelectionComplete: Device selection process completed for DeviceHandle =0x%x, %d request"
            "Resulting ErrorCode= %d Internal deselection required flag = %d ",
            cou32DeviceHandle, ETG_ENUM(CONNECTION_REQ, enDevConnReq),
            ETG_ENUM(ERROR_CODE, enErrorCode),
            ETG_ENUM(BOOL, m_bDeviceSwitchRequest)));

   t_Bool bProcessComplete = true;

   if (e8NO_ERRORS == enErrorCode)
   {
      m_u32CurrSelectedDevice = (e8DEVCONNREQ_SELECT == enDevConnReq)
            ? m_rSelectDevReqInfo.u32DeviceHandle : 0;
   }

   //! Check if the result is for internal deselection
   if ((e8NO_ERRORS == enErrorCode) && ((true == m_bDeviceSwitchRequest)))
   {
      bProcessComplete = false;
      m_bDeviceSwitchRequest = false;
   } // if (e8NO_ERRORS == enErrorCode)
   return bProcessComplete;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vSendSelectDeviceResult
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vSendSelectDeviceResultToHMI(const t_U32 cou32DeviceHandle,
         tenDeviceConnectionReq enDevConnReq, tenErrorCode enErrorCode, t_Bool bIsHMITrigger,
         const trUserContext &corfrUsrCtxt)
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vSendSelectDeviceResultToHMI: Sending device selection result to HMI"
            " cou32DeviceHandle =0x%x, enDevConnReq = %d, enErrorCode = %d, bIsHMITrigger = %d \n",
             cou32DeviceHandle,ETG_ENUM(CONNECTION_REQ, enDevConnReq),
             ETG_ENUM(ERROR_CODE,enErrorCode),ETG_ENUM(BOOL,bIsHMITrigger)));

   if (e8DEVCONNREQ_SELECT == enDevConnReq)
   {
      if (e8NO_ERRORS == enErrorCode)
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, "spi_tclDeviceSelector::vSendSelectDeviceResult"
                     "Selecting Device-0x%x successfully completed.\n", cou32DeviceHandle));
      }
      else
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS, "spi_tclDeviceSelector::vSendSelectDeviceResult"
                     "Selecting Device-0x%x  failed with error = %d.\n", cou32DeviceHandle, ETG_ENUM(
                           ERROR_CODE, enErrorCode)));
      }
   }

   //! Populate response code based on error
   tenResponseCode enRespCode = (e8NO_ERRORS == enErrorCode) ? e8SUCCESS
                                                             : e8FAILURE;

   //! Fetch device connection type from connection manager and
   //! bIsBTPairingReq flag from Bluetooth
   tenDeviceConnectionType enDevConnType = e8UNKNOWN_CONNECTION;
   tenDeviceCategory enDevCat = e8DEV_TYPE_UNKNOWN;
   t_Bool bIsBTPairingReq = false;
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      spi_tclBluetooth *poBluetooth = poSPIFactory->poGetBluetoothInstance();
      if ((NULL != poConnMngr) && (NULL != poBluetooth))
      {
         enDevConnType = poConnMngr->enGetDeviceConnType(cou32DeviceHandle);
         enDevCat =  poConnMngr->enGetDeviceCategory(cou32DeviceHandle);
         //! If BT pairing is required, set the flag
         if ((e8DEVCONNREQ_SELECT == m_rSelectDevReqInfo.enDevConnReq)
                  && (e8NO_ERRORS == enErrorCode))
         {
            bIsBTPairingReq = poBluetooth->bValidateBTPairingRequired();
         } // if (e8DEVCONNREQ_SELECT == m_rSelectDevReqInfo.enDevConnReq)
      }
   }

   //! Send the result to HMI if trigger is from HMI and if its is not the deslection request for switch device case
   if ((NULL != m_poDevSelResp) && (true == bIsHMITrigger) && (false == m_bDeviceSwitchRequest))
   {
      m_poDevSelResp->vPostSelectDeviceResult(cou32DeviceHandle, enDevConnType,
               enDevConnReq, enDevCat, enRespCode, enErrorCode, bIsBTPairingReq,
               corfrUsrCtxt);
   }//if ((NULL != m_poDevSelResp) && (true == bIsHMITrigger))

}


/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vSendSelectDeviceResult
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vSendSelectDeviceResult(const t_U32 cou32DeviceHandle,
         tenDeviceConnectionReq enDevConnReq, tenErrorCode enErrorCode, t_Bool bIsHMITrigger,
         const trUserContext &corfrUsrCtxt)
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vSendSelectDeviceResult: Sending %d request's result for DeviceHandle =0x%x to other components"
            "enErrorCode = %d, bIsHMITrigger = %d \n",ETG_ENUM(CONNECTION_REQ, enDevConnReq), cou32DeviceHandle,
            ETG_ENUM(ERROR_CODE,enErrorCode),ETG_ENUM(BOOL,bIsHMITrigger)));

   t_Bool bDevSelectDone = bDeviceSelectionComplete(cou32DeviceHandle, enDevConnReq,
            enErrorCode);
   if(true == bDevSelectDone)
   {
         vSendSelectDeviceResultToHMI(cou32DeviceHandle, enDevConnReq,
                  enErrorCode, bIsHMITrigger, corfrUsrCtxt);
   }

   tenResponseCode enRespCode= (e8NO_ERRORS == enErrorCode) ? e8SUCCESS: e8FAILURE;
   tenDeviceCategory enDevCategory = e8DEV_TYPE_UNKNOWN;

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      //! Fetch the device category and connection type from connection manager
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         enDevCategory = poConnMngr->enGetDeviceCategory(
               cou32DeviceHandle);
      } //if (NULL != poConnMngr)

      //! Post the select device Result to Bluetooth
      spi_tclBluetooth *poBluetooth = poSPIFactory->poGetBluetoothInstance();
      if (NULL != poBluetooth)
      {
         t_Bool bIsDeviceSwitch = ((e8DEVCONNREQ_DESELECT == enDevConnReq) && (false == bDevSelectDone));
         poBluetooth->vOnSPISelectDeviceResponse(cou32DeviceHandle,
               enDevConnReq, enRespCode, enErrorCode, bIsDeviceSwitch);
      }

      spi_tclAppMngr *poAppMngr = poSPIFactory->poGetAppManagerInstance();
      if (NULL != poAppMngr)
      {
         poAppMngr->vOnSelectDeviceResult(cou32DeviceHandle, enDevConnReq,
               enRespCode,enDevCategory);
      } //if(NULL != poAppMngr)

      spi_tclVideo *poVideo = poSPIFactory->poGetVideoInstance();
      if(NULL != poVideo)
      {
         poVideo->vOnSelectDeviceResult(cou32DeviceHandle,enDevConnReq,
            enRespCode,enDevCategory);
      }//if(NULL != poVideo)

      spi_tclAudio *poAudio = poSPIFactory->poGetAudioInstance();
      if(NULL != poAudio)
      {
         poAudio->vSelectDeviceResult(cou32DeviceHandle,enDevConnReq,
            enRespCode,enErrorCode, enDevCategory);
      }//if(NULL != poAudio)

      spi_tclInputHandler* poInputHandler = poSPIFactory->poGetInputHandlerInstance();
      if(NULL != poInputHandler)
      {
          poInputHandler->vOnSelectDeviceResult(cou32DeviceHandle,enDevConnReq,enRespCode,enDevCategory);
      }//if(NULL != poInputHandler)

      //! Post the select device Result to DataService
      spi_tclDataService *poDataService = poSPIFactory->poGetDataServiceInstance();
      if (NULL != poDataService)
      {
         poDataService->vOnSelectDeviceResult(cou32DeviceHandle,
               enDevConnReq, enRespCode, enErrorCode);
      }

      //! Post select device result to connection manager
      if (NULL != poConnMngr)
      {
         poConnMngr->vOnSelectDeviceResult(cou32DeviceHandle, enDevConnReq,
               enRespCode,enDevCategory,bIsHMITrigger);
      } //if(NULL != poConnMngr)
	  
	  spi_tclResourceMngr *poRsrcMngr = poSPIFactory->poGetRsrcMngrInstance();
      if(NULL != poRsrcMngr)
      {
         poRsrcMngr->vOnSPISelectDeviceResult(cou32DeviceHandle, enDevCategory, enDevConnReq, enRespCode, enErrorCode);
      } //if(NULL != poRsrcMngr)

      if(NULL != m_poDevSelResp)
      {
         m_poDevSelResp->vPostDeviceSelectStatus(cou32DeviceHandle, enDevCategory, enDevConnReq, enRespCode);
      }//if(NULL != m_poDevSelResp)
   }
   if(true == bDevSelectDone)
   {
         //! reset the selected component after the result is posted to HMI
         if(e8RESOURCE_BUSY != enErrorCode)
         {
            //! Remove the busy flag
            vUpdateDevSelectorBusyStatus(false);
            m_enSelectedComp = e32COMPID_UNKNOWN;
         }
   }
   else
   {
      //! If the response is for internal deselection of the device, Initiate the actual
      //! Device selection request received
      //! Send the selection request
      //! Fetch the device category from connection manager
      if(NULL != poSPIFactory)
      {
         spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
         tenDeviceCategory enDeviceCategory = e8DEV_TYPE_UNKNOWN;
         if (NULL != poConnMngr)
         {
            enDeviceCategory = poConnMngr->enGetDeviceCategory(
                  m_rSelectDevReqInfo.u32DeviceHandle);
         } //if (NULL != poConnMngr)

         //! Get the first component to receive device selection request
         tenCompID enCompID = enGetNextComponentID(enDeviceCategory, m_rSelectDevReqInfo.enDevConnReq, e32COMPID_UNKNOWN);
         vSendDeviceSelectionReq(m_rSelectDevReqInfo, enCompID);
      }
   }

   //! if internal device selection failed, trigger selection for next connected device
   if(e8FAILURE == enRespCode)
   {
      vOnAutomaticSelectionFailure(cou32DeviceHandle, enDevCategory);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vStartSelectionTimer
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vStartSelectionTimer()
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::vStartSelectionTimer() "));
   t_U32 u32TimerInterval_ms = 0;

   spi_tclConnSettings *m_poConnSettings = spi_tclConnSettings::getInstance();

   if (NULL != m_poConnSettings)
   {
      //! Start timer on start up
      //tenDeviceSelectionMode enDevSelMode = m_poConnSettings->enGetDeviceSelectionMode();
      //! Start timer before applying selection strategy on power up
      ETG_TRACE_USR2(("Starting Timer for Selection strategy \n"));
      u32TimerInterval_ms = (m_poConnSettings->u32GetDelayforStartupSel()) * 1000;

      Timer* poTimer = Timer::getInstance();
      if ((0 != u32TimerInterval_ms) && (NULL != poTimer))
      {
         timer_t rTimerID;
         //! Start timer and wait for the callback to apply selection strategy
         poTimer->StartTimer(rTimerID, u32TimerInterval_ms, u32TimerInterval_ms, this,
                  &spi_tclDeviceSelector::bSelectionTimerCb, NULL);
         m_bIsSelectionTimerRunnning = true;
      }
      else if (0 == u32TimerInterval_ms)
      {
         ETG_TRACE_ERR(("u32TimerInterval_ms is 0. Check delay in settings \n"));
      }
   }
}


/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vStartComponentResponseTimer
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vStartComponentResponseTimer()
{
   t_U32 u32TimerInterval_ms = 0;

   //! Determine the timer interval based on component
   switch (m_enSelectedComp)
   {
      case e32COMPID_AUDIO: //! Mirrorlink audio is taking more than 10 seconds to respond. Wait for 25 seconds
      case e32COMPID_BLUETOOTH:
      {
         u32TimerInterval_ms = su32CompResponseTime25s;
         break;
      }//case e32COMPID_BLUETOOTH:
      case e32COMPID_CONNECTIONMANAGER:
      {
         u32TimerInterval_ms = su32ConnCompResponseTimeinms;
         break;
      }//case e32COMPID_CONNECTIONMANAGER:
      case e32COMPID_APPMANAGER:
      case e32COMPID_VIDEO:
      default:
      {
         u32TimerInterval_ms = su32CompResponseTime10s;
         break;
      }//default:
   }//switch (m_enSelectedComp)

   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
   {
      timer_t rTimerID;
      //! Start timer and wait for the response from the component
      poTimer->StartTimer(rTimerID, u32TimerInterval_ms, 0, this,
               &spi_tclDeviceSelector::bCompRespTimerCb, NULL);
      m_rCompRespTimerID = rTimerID;
   }
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vStartComponentResponseTimer: Waiting for %d component to respond back in %d milliseconds\n ",
            ETG_ENUM(COMPONENT_ID,m_enSelectedComp), u32TimerInterval_ms));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vUpdateDevSelectorBusyStatus
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vUpdateDevSelectorBusyStatus(t_Bool bSelectorStatus)
{
   m_bIsDevSelectorBusy = bSelectorStatus;

   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if(NULL != poSPIFactory)
   {
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         poConnMngr->vSetDevSelectorBusyStatus(
            bSelectorStatus);
      } //if (NULL != poConnMngr)
   }//if(NULL != poSPIFactory)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::enGetNextComponentID
 ***************************************************************************/
tenCompID spi_tclDeviceSelector::enGetNextComponentID(tenDeviceCategory enDeviceCat,tenDeviceConnectionReq enDevConnReq, tenCompID enCurrentCompID)
{
   tenCompID enNextCompID = e32COMPID_UNKNOWN;

   if(e8DEVCONNREQ_SELECT == enDevConnReq)
   {
      if(m_mapSelectionSequence.end() != m_mapSelectionSequence.find(enDeviceCat))
      {
         ETG_TRACE_USR1((" spi_tclDeviceSelector::Device category found: size = %d ", m_mapSelectionSequence[enDeviceCat].size()));
         std::vector<tenCompID>::iterator itVecSequence;
         for(itVecSequence = m_mapSelectionSequence[enDeviceCat].begin(); itVecSequence != m_mapSelectionSequence[enDeviceCat].end(); itVecSequence++)
         {
            if(enCurrentCompID == *itVecSequence)
            {
               itVecSequence ++;
               if (itVecSequence != m_mapSelectionSequence[enDeviceCat].end())
               {
                  enNextCompID = *itVecSequence;
               }
               else
               {
                  enNextCompID = *(m_mapSelectionSequence[enDeviceCat].begin());
               }
               break;
            }
         }
      }
   }
   else if (e8DEVCONNREQ_DESELECT == enDevConnReq)
   {
      if(m_mapDeselectionSequence.end() != m_mapDeselectionSequence.find(enDeviceCat))
      {
         std::vector<tenCompID>::iterator itVecSequence;
         for(itVecSequence = m_mapDeselectionSequence[enDeviceCat].begin(); itVecSequence != m_mapDeselectionSequence[enDeviceCat].end(); itVecSequence++)
         {
            if(enCurrentCompID == *itVecSequence)
            {
               itVecSequence ++;
               if (itVecSequence != m_mapDeselectionSequence[enDeviceCat].end())
               {
                  enNextCompID = *itVecSequence;
               }
               else
               {
                  enNextCompID = *(m_mapDeselectionSequence[enDeviceCat].begin());
               }
               break;
            }
         }
      }
   }

   ETG_TRACE_USR1((" spi_tclDeviceSelector::enGetNextComponentID Device Category = %d, Selection Request = %d,"
         " Next ComponentID = %d enCurrentCompID = %d\n", ETG_ENUM(DEVICE_CATEGORY,enDeviceCat),
         ETG_ENUM(CONNECTION_REQ, enDevConnReq), ETG_ENUM(COMPONENT_ID,enNextCompID), ETG_ENUM(COMPONENT_ID,enCurrentCompID)));
   return enNextCompID;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vLoadSelectionSequence
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vLoadSelectionSequence()
{
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vLoadSelectionSequence entered \n"));
   //! Mirrorlink selection sequence
   tenCompID azenMirrorlinkSelectionSequence[] = { e32COMPID_CONNECTIONMANAGER, e32COMPID_BLUETOOTH,
         e32COMPID_APPMANAGER, e32COMPID_VIDEO, e32COMPID_INPUTHANDLER, e32COMPID_DATASERVICE, e32COMPID_AUDIO,
         e32COMPID_UNKNOWN };
   std::vector<tenCompID> vecMLSelectionSequence ( azenMirrorlinkSelectionSequence, (azenMirrorlinkSelectionSequence
         + ((sizeof(azenMirrorlinkSelectionSequence) / sizeof(tenCompID)))) );
   m_mapSelectionSequence[e8DEV_TYPE_MIRRORLINK] = vecMLSelectionSequence;
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vecMLSelectionSequence size = %d \n", vecMLSelectionSequence.size()));

   //! Carplay Selection Sequence
   tenCompID azenCarplaySelectionSequence[] = { e32COMPID_BLUETOOTH,
         e32COMPID_APPMANAGER, e32COMPID_VIDEO, e32COMPID_AUDIO, e32COMPID_CONNECTIONMANAGER,
         e32COMPID_UNKNOWN };
   std::vector < tenCompID > vecCarplaySelectionSequence ( azenCarplaySelectionSequence, (azenCarplaySelectionSequence
         + ((sizeof(azenCarplaySelectionSequence) / sizeof(tenCompID)))) );
   m_mapSelectionSequence[e8DEV_TYPE_DIPO] = vecCarplaySelectionSequence;

   //! Android Auto Selection Sequence
   tenCompID azenAndroidAutoSelectionSequence[] = { e32COMPID_CONNECTIONMANAGER, e32COMPID_BLUETOOTH,
         e32COMPID_APPMANAGER, e32COMPID_VIDEO, e32COMPID_INPUTHANDLER, e32COMPID_DATASERVICE, e32COMPID_AUDIO,
         e32COMPID_UNKNOWN };
   std::vector < tenCompID > vecAAPSelectionSequence ( azenAndroidAutoSelectionSequence, (azenAndroidAutoSelectionSequence
         + ((sizeof(azenAndroidAutoSelectionSequence) / sizeof(tenCompID))) ));
   m_mapSelectionSequence[e8DEV_TYPE_ANDROIDAUTO] = vecAAPSelectionSequence;

   //! mySPIN selection sequence TBD

}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vLoadDeselectionSequence
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vLoadDeselectionSequence()
{
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vLoadDeselectionSequence entered \n"));
   //! Mirrorlink deselection sequence
   tenCompID azenMirrorlinkDeSelectionSequence[] = { e32COMPID_BLUETOOTH,e32COMPID_AUDIO,
         e32COMPID_INPUTHANDLER, e32COMPID_DATASERVICE, e32COMPID_VIDEO, e32COMPID_APPMANAGER, e32COMPID_CONNECTIONMANAGER,
         e32COMPID_UNKNOWN };
   std::vector<tenCompID> vecMLSelectionSequence ( azenMirrorlinkDeSelectionSequence, (azenMirrorlinkDeSelectionSequence
         + ((sizeof(azenMirrorlinkDeSelectionSequence) / sizeof(tenCompID)))) );
   m_mapDeselectionSequence[e8DEV_TYPE_MIRRORLINK] = vecMLSelectionSequence;

   //! Carplay deselection Sequence
   tenCompID azenCarplayDeSelectionSequence[] = { e32COMPID_BLUETOOTH,
         e32COMPID_AUDIO, e32COMPID_VIDEO, e32COMPID_APPMANAGER, e32COMPID_CONNECTIONMANAGER,
         e32COMPID_UNKNOWN };
   std::vector < tenCompID > vecCarplaySelectionSequence ( azenCarplayDeSelectionSequence, (azenCarplayDeSelectionSequence
         + ((sizeof(azenCarplayDeSelectionSequence) / sizeof(tenCompID)))));
   m_mapDeselectionSequence[e8DEV_TYPE_DIPO] = vecCarplaySelectionSequence;

   //! Android Auto deselection Sequence
   tenCompID azenAndroidAutoDeSelectionSequence[] = { e32COMPID_CONNECTIONMANAGER, e32COMPID_BLUETOOTH,
         e32COMPID_APPMANAGER, e32COMPID_VIDEO, e32COMPID_INPUTHANDLER, e32COMPID_DATASERVICE, e32COMPID_AUDIO,
         e32COMPID_UNKNOWN };
   std::vector < tenCompID > vecAAPSelectionSequence ( azenAndroidAutoDeSelectionSequence, (azenAndroidAutoDeSelectionSequence
         + ((sizeof(azenAndroidAutoDeSelectionSequence) / sizeof(tenCompID)))) );
   m_mapDeselectionSequence[e8DEV_TYPE_ANDROIDAUTO] = vecAAPSelectionSequence;
}


/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vOnAutomaticSelectionFailure
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vOnAutomaticSelectionFailure(const t_U32 cou32DeviceID, tenDeviceCategory enDeviceCat)
{
   ETG_TRACE_USR1((" spi_tclDeviceSelector::vOnAutomaticSelectionFailure entered for Device category = %d \n", ETG_ENUM(DEVICE_CATEGORY, enDeviceCat)));
   static t_U32 u32RetryCount = 0;
   if(su32MaximumRetries != u32RetryCount)
   {
      //! Set the support for enDeviceCat as not supported
      spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
      if(NULL != poSPIFactory)
      {
         spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
         if (NULL != poConnMngr)
         {
            poConnMngr->vSetSPISupport(cou32DeviceID, enDeviceCat, e8SPI_NOTSUPPORTED);
         } //if (NULL != poConnMngr)
      }//if(NULL != poSPIFactory)

      tenDeviceCategory enNextSwitch = enGetNextSwitchType(cou32DeviceID);
      //! Dummy usercontext
      trUserContext rUserContext;
      if(e8DEV_TYPE_UNKNOWN != enNextSwitch)
      {
         vSelectDevice(cou32DeviceID, e8USB_CONNECTED, e8DEVCONNREQ_SELECT, e8USAGE_DISABLED,
                  e8USAGE_DISABLED,enNextSwitch, false, rUserContext);
      }
      else
      {
         t_U32 u32DeviceHandle = u32GetNextDeviceForSelection(cou32DeviceID);
         //! Get the switch type for new device handle
         if( 0 != u32DeviceHandle)
         {
            enNextSwitch = enGetNextSwitchType(u32DeviceHandle);
            vSelectDevice(u32DeviceHandle, e8USB_CONNECTED, e8DEVCONNREQ_SELECT,
                     e8USAGE_DISABLED, e8USAGE_DISABLED,enNextSwitch, false, rUserContext);
         }
      }
      u32RetryCount ++;
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::enGetNextSwitchType
 ***************************************************************************/
tenDeviceCategory spi_tclDeviceSelector::enGetNextSwitchType(const t_U32 cou32DeviceHandle)
{
   tenDeviceCategory enDeviceCat = e8DEV_TYPE_UNKNOWN;
   //! Get the device information
   trDeviceInfo rDeviceinfo;
   tenDeviceCategory enTechPref = e8DEV_TYPE_UNKNOWN;
   //! Check for the currently enabled SPI technologies
   tenEnabledInfo enMLEnabled = e8USAGE_DISABLED;
   tenEnabledInfo enAAPEnabled = e8USAGE_DISABLED;
   tenEnabledInfo enCarplayEnabled = e8USAGE_DISABLED;
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if(NULL != poFactory)
   {
      spi_tclConnMngr *poConnMngr = poFactory->poGetConnMngrInstance();
      if(NULL != poConnMngr)
      {
         poConnMngr->bGetDevProjUsage(e8DEV_TYPE_MIRRORLINK, enMLEnabled);
         poConnMngr->bGetDevProjUsage(e8DEV_TYPE_ANDROIDAUTO, enAAPEnabled);
         poConnMngr->bGetDevProjUsage(e8DEV_TYPE_DIPO, enCarplayEnabled);
         poConnMngr->bGetDeviceInfo(cou32DeviceHandle, rDeviceinfo);
         std::vector<tenDeviceCategory> vecTechnologyPreference;
         poConnMngr->vGetTechnologyPreference(su32DeviceHandleMax, vecTechnologyPreference);
         if(true != vecTechnologyPreference.empty())
         {
            enTechPref = *(vecTechnologyPreference.begin());
         }
      }
   }

   t_Bool bAAPEnabled = ((e8USAGE_ENABLED == enAAPEnabled) && (e8SPI_NOTSUPPORTED != rDeviceinfo.rProjectionCapability.enMirrorlinkSupport));
   t_Bool bMlEnabled = ((e8USAGE_ENABLED == enMLEnabled) && (e8SPI_NOTSUPPORTED != rDeviceinfo.rProjectionCapability.enAndroidAutoSupport));;
   t_Bool bCarplayEnabled = ((e8USAGE_ENABLED == enCarplayEnabled) && (e8SPI_NOTSUPPORTED != rDeviceinfo.rProjectionCapability.enCarplaySupport));
   //! For apple devices, perform selection for carplay
   if(true == bCarplayEnabled)
   {
      enDeviceCat = e8DEV_TYPE_DIPO;
   }
   else
   {
      //! Provide the next switch type based on preference and the device support
      if ((true == bMlEnabled) && (true == bAAPEnabled))
      {
         enDeviceCat = enTechPref;
      }
      else if (true == bAAPEnabled)
      {
         enDeviceCat = e8DEV_TYPE_ANDROIDAUTO;
      }
      else if (true == bMlEnabled)
      {
         enDeviceCat = e8DEV_TYPE_MIRRORLINK;
      }
   }

   ETG_TRACE_USR1((" spi_tclDeviceSelector::enGetNextSwitchType for Device %d is %d \n", cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDeviceCat)));
   return enDeviceCat;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::u32GetNextDeviceForSelection
 ***************************************************************************/
t_U32 spi_tclDeviceSelector::u32GetNextDeviceForSelection(const t_U32 cou32DeviceHandle)
{
   t_U32 u32DeviceforNextSelection = 0;
   t_Bool bIsSelectError = false;
   t_Bool bIsUserDeseleted = false;
   //! Get connected devices
   std::vector<trDeviceInfo> vecDeviceInfoList;
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if(NULL != poFactory)
   {
      spi_tclConnMngr *poConnMngr = poFactory->poGetConnMngrInstance();
      if(NULL != poConnMngr)
      {
         poConnMngr->vGetDeviceList(vecDeviceInfoList,false,true);         //! Check if the previous selection of the automatically selected device failed
      }

      //! Select the next connected device in the list
      std::vector<trDeviceInfo>::iterator itvecDeviceInfoList;
      for(itvecDeviceInfoList=vecDeviceInfoList.begin(); itvecDeviceInfoList!= vecDeviceInfoList.end(); itvecDeviceInfoList++)
      {
         //! Check if the connected device supports atleast on eof the SPi technology
         if((cou32DeviceHandle != itvecDeviceInfoList->u32DeviceHandle) &&
                  ((e8SPI_NOTSUPPORTED != itvecDeviceInfoList->rProjectionCapability.enAndroidAutoSupport)||
                  (e8SPI_NOTSUPPORTED != itvecDeviceInfoList->rProjectionCapability.enMirrorlinkSupport)))
         {
            u32DeviceforNextSelection = itvecDeviceInfoList->u32DeviceHandle;
            if(NULL != poConnMngr)
            {
               bIsSelectError = poConnMngr->bIsSelectError(cou32DeviceHandle);
               bIsUserDeseleted = poConnMngr->bGetUserDeselectionFlag(cou32DeviceHandle);
            }
            if((false == bIsSelectError) && (false == bIsUserDeseleted))
            {
               break;
            }
            else
            {
               u32DeviceforNextSelection = 0;
            }
         }
      }
   }
   ETG_TRACE_USR1((" spi_tclDeviceSelector::u32GetNextDeviceForSelection : currently selected device = %d, Next device for selection = %d\n",
            cou32DeviceHandle, u32DeviceforNextSelection));
   return u32DeviceforNextSelection;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::vEvaluateSPIPreference
 ***************************************************************************/
t_Void spi_tclDeviceSelector::vEvaluateSPIPreference(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("[PARAM] spi_tclDeviceSelector::vEvaluateSPIPreference DeviceHandle = 0x%x \n", cou32DeviceHandle));
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   tenDeviceCategory enPreferredCategory = e8DEV_TYPE_UNKNOWN;
   if (NULL != poConnSettings)
   {
      enPreferredCategory = poConnSettings->enGetTechnologyPreference();
   }

   //! If the device category is unknown check the preferred category
   spi_tclFactory *poSPIFactory = spi_tclFactory::getInstance();
   if (NULL != poSPIFactory)
   {
      spi_tclConnMngr *poConnMngr = poSPIFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         tenDeviceCategory enStoredDevCategory = poConnMngr->enGetDeviceCategory(cou32DeviceHandle);
         ETG_TRACE_USR4(("Preferred category = %d Stored device category = %d,  Switching to Preferred category ", ETG_ENUM(DEVICE_CATEGORY, enPreferredCategory), ETG_ENUM(DEVICE_CATEGORY, enStoredDevCategory)));
         //! if HMI sends the device category, use it for selecting the device
         if ((enPreferredCategory != enStoredDevCategory) && (e8DEV_TYPE_DIPO != enStoredDevCategory))
         {
            if ((e8SPI_NOTSUPPORTED != poConnMngr->enGetSPISupport(cou32DeviceHandle, enPreferredCategory)))
            {
               poConnMngr->vSetDeviceCategory(cou32DeviceHandle, enPreferredCategory);
               ETG_TRACE_USR4(("[DESC] Setting Device category to %d \n",
                        ETG_ENUM(DEVICE_CATEGORY, enPreferredCategory)));
            }
         }
      }
   }
}

//!Static
/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::bSelectionTimerCb
 ***************************************************************************/
t_Bool spi_tclDeviceSelector::bSelectionTimerCb(timer_t rTimerID, t_Void *pvObject,
         const t_Void *pvUserData)
{
   SPI_INTENTIONALLY_UNUSED(pvUserData);
   ETG_TRACE_USR1((" spi_tclDeviceSelector::bSelectionTimerCb entered \n"));
   spi_tclDeviceSelector* poDevSelector =
            static_cast<spi_tclDeviceSelector*>(pvObject);

   //!  stop the timer
   if (NULL != poDevSelector)
   {
      poDevSelector->m_bIsSelectionTimerRunnning = false;

      Timer* poTimer = Timer::getInstance();
      if (NULL != poTimer)
      {
         poTimer->CancelTimer(rTimerID);
      }
   }//if (NULL != poDevSelector)
   return true;
}
//!Static
/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::bCompRespTimerCb
 ***************************************************************************/
t_Bool spi_tclDeviceSelector::bCompRespTimerCb(timer_t rTimerID, t_Void *pvObject,
         const t_Void *pvUserData)
{
   SPI_INTENTIONALLY_UNUSED(pvUserData);
   ETG_TRACE_ERR(("spi_tclDeviceSelector::bCompRespTimerCb : Component did not respond back for "
         "device selection request in specified time: Triggering cleanup.. \n"));
   spi_tclDeviceSelector* poDevSelector =
            static_cast<spi_tclDeviceSelector*>(pvObject);
   Timer* poTimer = Timer::getInstance();

   //!  If the select device response is not received before timer expiry
   //! then assume the response from the component as false. this is done to
   //! avoid device selector to be blocked in busy state.
   if ((NULL != poDevSelector) && (NULL != poTimer))
   {
      poDevSelector->vReceiveSelectDeviceResCb(poDevSelector->m_enSelectedComp, e8SELECTION_FAILED);
      poTimer->CancelTimer(rTimerID);
   }
   return true;
}

//!Static
/***************************************************************************
 ** FUNCTION:  spi_tclDeviceSelector::u32GetSelectedDeviceId
 ***************************************************************************/
t_U32 spi_tclDeviceSelector::u32GetSelectedDeviceId()
{
   ETG_TRACE_USR1(("spi_tclDeviceSelector::u32GetSelectedDeviceId()  m_u32CurrSelectedDevice = 0x%x\n",
            m_u32CurrSelectedDevice));
   return m_u32CurrSelectedDevice;
}

//lint ?restore
