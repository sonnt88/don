/*!
 *******************************************************************************
 * \file             spi_tclSelectionStrategy.h
 * \brief            Implements Selection Strategy for automatic device selection
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Implements Selection Strategy for automatic device selection
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#include <algorithm>
#include "spi_tclSelectionStrategy.h"
#include "spi_tclDevHistory.h"
#include "spi_tclConnSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclSelectionStrategy.cpp.trc.h"
#endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::spi_tclSelectionStrategy
 ***************************************************************************/
spi_tclSelectionStrategy::spi_tclSelectionStrategy(): m_poConnSettings(NULL)
{
   ETG_TRACE_USR1((" spi_tclSelectionStrategy::spi_tclSelectionStrategy() entered \n"));
   m_poConnSettings = spi_tclConnSettings::getInstance();
   SPI_NORMAL_ASSERT(NULL == m_poConnSettings);
}

/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::~spi_tclSelectionStrategy
 ***************************************************************************/
spi_tclSelectionStrategy::~spi_tclSelectionStrategy()
{
   ETG_TRACE_USR1(("spi_tclSelectionStrategy::~spi_tclSelectionStrategy() entered \n"));
   m_poConnSettings = NULL;
}

/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::u32ApplySelectionStrategy
 ***************************************************************************/
t_U32 spi_tclSelectionStrategy::u32ApplySelectionStrategy()
{
   //! Apply selection strategy based on device selection mode
   t_U32 u32SelectedDevice = 0;

   tenDeviceSelectionMode enSelMode = e16DEVICESEL_MANUAL;

   //! Read the project specific setting for device selection Mode
   if (NULL != m_poConnSettings)
   {
      enSelMode = m_poConnSettings->enGetDeviceSelectionMode();
   } // if (NULL != m_poConnSettings)

   //! Switch to appropriate selection mode
   switch (enSelMode)
   {
      case e16DEVICESEL_MANUAL:
      case e16DEVICESEL_SEMI_AUTOMATIC:
      {
         break;
      }
      //! Commented out unused selection strategies
      /*
      case e16DEVICESEL_AUTO_FOR_SINGLEDEVICE:
      {
         //! Select the only device in the list
         if (1 == m_mapDeviceInfo.size())
         {
            u32SelectedDevice = m_mapDeviceInfo.begin()->first;
         }
         break;
      }
      case e16DEVICESEL_AUTO_FOR_N_DEVICES:
      {
         u32SelectedDevice = u32GetAutoSelectedDevice();
         break;
      }

      case e16DEVICESEL_RESTORE_LAST_DEVICE_ON_STARTUP:
      {
         u32SelectedDevice = u32GetLastUsedDevice();
         break;
      }

      case e16DEVICESEL_RESTORE_LAST_DEVICE_ALWAYS:
      {
         u32SelectedDevice = u32GetLastUsedDevice();
         break;
      }*/

      case e16DEVICESEL_AUTOMATIC:
      {
         u32SelectedDevice = u32GetLastConnectedDevice();
         break;
      }

      default:
      {
         u32SelectedDevice = 0;
         break;
      }
   } // switch (enSelMode)

   ETG_TRACE_USR1(("u32ApplySelectionStrategy: enSelMode = %d Selected Device= 0x%x \n",
            ETG_ENUM(SELECTION_MODE, enSelMode),u32SelectedDevice));

   return u32SelectedDevice;

}

/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::vUpdateDeviceList
 ***************************************************************************/
t_Void spi_tclSelectionStrategy::vUpdateDeviceList(
         std::map<t_U32, trEntireDeviceInfo> &rfrmapDeviceInfoList)
{
   /*lint -esym(40,rDeviceInfo) rDeviceInfo Undeclared identifier */
   /*lint -esym(40,u32DeviceHandle) u32DeviceHandle Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclSelectionStrategy::vUpdateDeviceList entered \n"));
   std::vector<trEntireDeviceInfo> vecDevicelist;
   //! copy map to a vector for sorting
   for (auto itMapDevList = rfrmapDeviceInfoList.begin();
            itMapDevList != rfrmapDeviceInfoList.end(); ++itMapDevList)
   {
      vecDevicelist.push_back(itMapDevList->second);
   }
   //! Sorts device list in the order Connected Devices (arranged as per selection time) -> Not connected
   //! stable_sort is used preserves the relative order of the elements with equivalent values
   //! The last argument trEntireDeviceInfo() is a call for overloaded function call operator
   //! for comparison. Used by std::sort algorithm to compare objects of type trEntireDeviceInfo
   std::stable_sort(vecDevicelist.begin(), vecDevicelist.end(),
            trEntireDeviceInfo());
   //! copy back to map
   for (auto itMapDevList = vecDevicelist.begin();
            itMapDevList != vecDevicelist.end(); ++itMapDevList)
   {
      m_mapDeviceInfo.insert(std::pair<t_U32, trEntireDeviceInfo> (itMapDevList->rDeviceInfo.u32DeviceHandle, *itMapDevList));
   }

}

/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::vSetDeviceSelectError
 ***************************************************************************/
t_Void spi_tclSelectionStrategy::vSetDeviceSelectError(const t_U32 cou32DeviceHanlde, t_Bool bIsError)
{
   ETG_TRACE_USR1((" vSetDeviceSelectError: setting device select error for Devicehandle = %d to %d \n", cou32DeviceHanlde, ETG_ENUM(BOOL, bIsError)));
   /*lint -esym(40,bIsSelectError) bIsSelectError Undeclared identifier */
   auto itMapDevInfo = m_mapDeviceInfo.find(cou32DeviceHanlde);
   if ((m_mapDeviceInfo.end() != itMapDevInfo))
   {
      m_mapDeviceInfo[cou32DeviceHanlde].bIsSelectError = bIsError;
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::u32GetSPICapableDevice
 ***************************************************************************/
t_U32 spi_tclSelectionStrategy::u32GetSPICapableDevice() const
{
   t_U32 u32Devicehandle = 0;
   //! Iterate through the device list and find the device for
    //! Select known device or device capable of supporting on eof the SPi technology
    for (auto itmap = m_mapDeviceInfo.begin(); itmap != m_mapDeviceInfo.end(); itmap++)
    {
       if (((e8DEV_CONNECTED == itmap->second.rDeviceInfo.enDeviceConnectionStatus)
                || (true == itmap->second.bIsUSBConnected))
                && (false == itmap->second.bIsUserDeselected)
                && (false == itmap->second.bIsSelectError)
                && (true == itmap->second.rDeviceInfo.bDeviceUsageEnabled))
       {
          u32Devicehandle = itmap->second.rDeviceInfo.u32DeviceHandle;
          break;
       } // if (m_mapDeviceInfo.end() != itFoundDevice)
    } // for (auto itvec = rvecDeviceInfo.begin()...
   ETG_TRACE_USR1(("u32GetSPICapableDevice: SPI capable device found : Device handle %d \n", u32Devicehandle));
   return u32Devicehandle;
}

/***************************************************************************
 *********************************PRIVATE ***********************************
 ***************************************************************************/
/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::u32GetAutoSelectedDevice
 ***************************************************************************/
t_U32 spi_tclSelectionStrategy::u32GetAutoSelectedDevice() const
{
	/*lint -esym(40,rDeviceInfo) rDeviceInfo Undeclared identifier */
	/*lint -esym(40,u32DeviceHandle) rDeviceInfo Undeclared identifier */
	/*lint -esym(40,enDeviceConnectionStatus) enDeviceConnectionStatus Undeclared identifier */
	/*lint -esym(40,second) second Undeclared identifier */
   t_U32 u32SelectedDevice = 0;
   tenSelModePriority enPriority = e8PRIORITY_DEVICELIST_HISTORY;
   tenDeviceConnectionType enPrefConn = e8UNKNOWN_CONNECTION;
   tenDeviceCategory enPrefDevice = e8DEV_TYPE_UNKNOWN;

   //! Retrieve project specific settings for device selection
   if (NULL != m_poConnSettings)
   {
      enPriority =
               m_poConnSettings->enGetSelectionModePriority();
      //! only USB connection is used as of now
      enPrefConn = e8USB_CONNECTED;
      enPrefDevice =
               m_poConnSettings->enGetTechnologyPreference();
   }

   ETG_TRACE_USR2(("u32GetAutoSelectedDevice: enPriority = %d, "
   "enPrefDevice = %d, enPrefConn = %d \n",
            enPriority, enPrefDevice, enPrefConn));

   spi_tclDevHistory oDeviceHistory;
   std::vector<trEntireDeviceInfo> rvecDeviceInfo;

   //! Get the previously used devices to select device automatically
   oDeviceHistory.bGetDeviceHistoryFromdb(rvecDeviceInfo,
            enPriority,
            enPrefDevice,
            enPrefConn);

   //! Iterate through the device list and find the device for
   //! selection based on project requirements
   for (auto itvec = rvecDeviceInfo.begin(); itvec != rvecDeviceInfo.end();
            itvec++)
   {
      auto itFoundDevice =
               m_mapDeviceInfo.find(itvec->rDeviceInfo.u32DeviceHandle);
      if ((m_mapDeviceInfo.end() != itFoundDevice)
               && (e8DEV_CONNECTED == itFoundDevice->second.rDeviceInfo.enDeviceConnectionStatus))
      {
         u32SelectedDevice = itvec->rDeviceInfo.u32DeviceHandle;
         break;
      } // if (m_mapDeviceInfo.end() != itFoundDevice)
   }// for (auto itvec = rvecDeviceInfo.begin()...

   ETG_TRACE_USR4(("spi_tclSelectionStrategy::u32GetAutoSelectedDevice :Selected Device = 0x%x \n",
            u32SelectedDevice));
   return u32SelectedDevice;
}

/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::u32GetLastUsedDevice
 ***************************************************************************/
t_U32 spi_tclSelectionStrategy::u32GetLastUsedDevice() const
{
	/*lint -esym(40,rDeviceInfo) rDeviceInfo Undeclared identifier */
	/*lint -esym(40,enDeviceConnectionStatus) enDeviceConnectionStatus Undeclared identifier */
	/*lint -esym(40,second) second Undeclared identifier */
	/*lint -esym(40,bDeviceUsageEnabled) bDeviceUsageEnabled Undeclared identifier */
   //! Fetch the last selected device from history
   spi_tclDevHistory oDevHistory;
   t_U32 u32DeviceHandle = oDevHistory.u32GetLastSelectedDevice();
   //! If the last used device is not connected or if the device usage flag is not enabled
   //! then don't select device automatically
   auto itMapDevInfo = m_mapDeviceInfo.find(u32DeviceHandle);
   if ((m_mapDeviceInfo.end() == itMapDevInfo)
            || (e8DEV_CONNECTED != itMapDevInfo->second.rDeviceInfo.enDeviceConnectionStatus)
            || (false == itMapDevInfo->second.rDeviceInfo.bDeviceUsageEnabled))
   {
      u32DeviceHandle = 0;
   }

   ETG_TRACE_USR4(("spi_tclSelectionStrategy::u32GetLastUsedDevice: Devices list size = %d   "
            "Last Selected Device  = 0x%x\n",
            m_mapDeviceInfo.size(), u32DeviceHandle));
   return u32DeviceHandle;
}

/***************************************************************************
 ** FUNCTION:  spi_tclSelectionStrategy::u32GetLastConnectedDevice
 ***************************************************************************/
t_U32 spi_tclSelectionStrategy::u32GetLastConnectedDevice() const
{
	/*lint -esym(40,rDeviceInfo) rDeviceInfo Undeclared identifier */
	/*lint -esym(40,u32DeviceHandle) u32DeviceHandle Undeclared identifier */
	/*lint -esym(40,enDeviceConnectionStatus) enDeviceConnectionStatus Undeclared identifier */
	/*lint -esym(40,second) second Undeclared identifier */
	/*lint -esym(40,bDeviceUsageEnabled) bDeviceUsageEnabled Undeclared identifier */
	/*lint -esym(40,bIsUSBConnected) bIsUSBConnected Undeclared identifier */
	/*lint -esym(40,bIsUserDeselected) bIsUserDeselected Undeclared identifier */
	/*lint -esym(40,bDeviceUsageEnabled) bDeviceUsageEnabled Undeclared identifier */

   t_U32 u32LastConnectedDevice = 0;
   //! Iterate through the device list and find the device for
   //! selection based on project requirements
   //! Select the recently connected device for automatic selection
   //! if the device is not deselected by user and if the Device usage flag is enabled
   //! and if the device validation is successful
   for (auto itmap = m_mapDeviceInfo.begin(); itmap != m_mapDeviceInfo.end(); itmap++)
   {
      if (((e8DEV_CONNECTED == itmap->second.rDeviceInfo.enDeviceConnectionStatus)
               || (true == itmap->second.bIsUSBConnected))
               && (false == itmap->second.bIsUserDeselected)
               && (true == itmap->second.rDeviceInfo.bDeviceUsageEnabled)
               && (false == itmap->second.bIsSelectError))
      {
         u32LastConnectedDevice = itmap->second.rDeviceInfo.u32DeviceHandle;
         break;
      } // if (m_mapDeviceInfo.end() != itFoundDevice)
   } // for (auto itvec = rvecDeviceInfo.begin()...

   ETG_TRACE_USR4(("u32GetLastConnectedDevice :Last connected Device = %d \n", u32LastConnectedDevice));
   return u32LastConnectedDevice;
}


//lint –restore
