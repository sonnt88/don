/*!
 *******************************************************************************
 * \file             spi_tclConnMngrResp.cpp
 * \brief            Response from ConnMngr class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Response  to HMI from ConnMngr class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.01.2013 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclConnMngrResp.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclConnMngrResp.cpp.trc.h"
#endif
#endif

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngrResp::spi_tclConnMngrResp
 ***************************************************************************/
spi_tclConnMngrResp::spi_tclConnMngrResp()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngrResp::~spi_tclConnMngrResp
 ***************************************************************************/
spi_tclConnMngrResp::~spi_tclConnMngrResp()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclConnMngrResp::vPostDeviceStatusInfo
 ***************************************************************************/
t_Void spi_tclConnMngrResp::vPostDeviceStatusInfo(tenDeviceStatusInfo enDeviceStatusInfo)
{
   ETG_TRACE_USR1((" %s not implemented in derived class \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(enDeviceStatusInfo);
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngrResp::vPostDeviceConnectionStatus
 ***************************************************************************/
t_Void spi_tclConnMngrResp::spi_tclConnMngrResp::vPostDeviceConnectionStatus(
         const t_U32 cou32DeviceHandle,
         tenMirrorLinkConnectionStatus enMLConnStatus,
         tenMirrorLinkConnectionErrorInfo enMLConnErrorInfo)
{
   ETG_TRACE_USR1((" %s not implemented in derived class \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(enMLConnStatus);
   SPI_INTENTIONALLY_UNUSED(enMLConnErrorInfo);

}

