/*!
 *******************************************************************************
 * \file             spi_tclDevSelResp.h
 * \brief            Response to HMI from DeviceSelector class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Response to HMI from DeviceSelector class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 16.01.2013 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclDevSelResp.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclDevSelResp.cpp.trc.h"
#endif
#endif

/***************************************************************************
 ** FUNCTION:  spi_tclDevSelResp::spi_tclDevSelResp
 ***************************************************************************/
spi_tclDevSelResp::spi_tclDevSelResp()
{

}
/***************************************************************************
 ** FUNCTION:  spi_tclDevSelResp::~spi_tclDevSelResp
 ***************************************************************************/
spi_tclDevSelResp::~spi_tclDevSelResp()
{

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDevSelResp::vPostSelectDeviceResult
 ***************************************************************************/
t_Void spi_tclDevSelResp::vPostSelectDeviceResult(tenResponseCode enRespCode,
         tenErrorType enErrorType, const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1((" %s not implemented in derived class \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(enRespCode);
   SPI_INTENTIONALLY_UNUSED(enErrorType);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDevSelResp::vPostDeviceSelectStatus
 ***************************************************************************/
t_Void spi_tclDevSelResp::vPostDeviceSelectStatus(t_U32 u32DeviceHandle, 
                                                  tenDeviceCategory enDevCategory,
                                                  tenDeviceConnectionReq enDevConnReq,
                                                  tenResponseCode enRespCode)
{
   ETG_TRACE_USR1((" %s not implemented in derived class \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(enDevCategory);
   SPI_INTENTIONALLY_UNUSED(enDevConnReq);
   SPI_INTENTIONALLY_UNUSED(enRespCode);
}

