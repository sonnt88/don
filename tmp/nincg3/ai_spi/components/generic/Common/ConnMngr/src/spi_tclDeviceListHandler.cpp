/*!
 *******************************************************************************
 * \file             spi_tclDeviceListHandler.cpp
 * \brief            Handles Device List
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Handles SPI Devices list
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 25.01.2016 | Rachana L Achar              | Logiscope improvements

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <time.h>
#include <sstream>
#include <algorithm>
#include "spi_tclDeviceListHandler.h"
#include "spi_tclDevHistory.h"
#include "Datapool.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclDeviceListHandler.cpp.trc.h"
#endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::spi_tclDeviceListHandler
 ***************************************************************************/
spi_tclDeviceListHandler::spi_tclDeviceListHandler()
{
   ETG_TRACE_USR1((" spi_tclDeviceListHandler::spi_tclDeviceListHandler() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::~spi_tclDeviceListHandler
 ***************************************************************************/
spi_tclDeviceListHandler::~spi_tclDeviceListHandler()
{
   ETG_TRACE_USR1((" spi_tclDeviceListHandler::~spi_tclDeviceListHandler() entered \n"));
   vClearDeviceList();
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bRestoreDeviceList
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bRestoreDeviceList()
{
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
	/*lint -esym(40,u32DeviceHandle)u32DeviceHandle Undeclared identifier */
   std::vector<trEntireDeviceInfo> rvecDevHistory;
   t_Bool bRetVal = bGetDeviceHistory(rvecDevHistory);

   //! Initially populate device list with the entries from device history
   if (true == bRetVal)
   {
      m_oDeviceListLock.s16Lock();
      for (auto itDevHist = rvecDevHistory.begin();
               itDevHist != rvecDevHistory.end(); ++itDevHist)
      {
         m_mapDeviceInfoList.insert(std::pair<t_U32, trEntireDeviceInfo> (
               itDevHist->rDeviceInfo.u32DeviceHandle, *itDevHist));

      } //for (auto itDevHist = rvecDevHistory.begin()...
      m_oDeviceListLock.vUnlock();
   } // if (true == bInit)

   ETG_TRACE_USR1(("spi_tclDeviceListHandler::bRestoreDeviceList: Restoring Device list from history" \
            "Device History size = %d Return Value = %d \n",
            rvecDevHistory.size(), ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bSaveDeviceList
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::bSaveDeviceList()
{
   ETG_TRACE_USR1((" spi_tclDeviceListHandler::bSaveDeviceList() : Saving device list persistently \n"));

   //! Save Device list persistently
   //! Write the data to local variable and then write to db as the process
   //! of writing data to database might be time consuming
   spi_tclDevHistory oDeviceHistory;
   std::map<t_U32, trEntireDeviceInfo> mapDeviceInfoList;
   m_oDeviceListLock.s16Lock();
   mapDeviceInfoList = m_mapDeviceInfoList;
   m_oDeviceListLock.vUnlock();
   oDeviceHistory.vSaveDeviceList(mapDeviceInfoList);
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vClearDeviceList
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vClearDeviceList()
{
   ETG_TRACE_USR1((" spi_tclDeviceListHandler::vClearDeviceList() entered \n"));
   m_oDeviceListLock.s16Lock();
   m_mapDeviceInfoList.clear();
   m_oDeviceListLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::enAddDeviceToList
 ***************************************************************************/
tenDeviceStatusInfo spi_tclDeviceListHandler::enAddDeviceToList(const t_U32 cou32DeviceHandle,
         const trDeviceInfo &rfrDeviceInfo)
{

	/*lint -esym(40,bDeviceUsageEnabled)bDeviceUsageEnabled Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,enDeviceConnectionStatus)enDeviceConnectionStatus Undeclared identifier */
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,szDeviceName)szDeviceName Undeclared identifier */
	/*lint -esym(40,bSelectedDevice)bSelectedDevice Undeclared identifier */
	/*lint -esym(40,bIsUserDeselected)bIsUserDeselected Undeclared identifier */
	/*lint -esym(40,u32AccessIndex)u32AccessIndex Undeclared identifier */
	/*lint -esym(40,enUserPreference)enUserPreference Undeclared identifier */
	/*lint -esym(40,bDeviceValidated)bDeviceValidated Undeclared identifier */
	/*lint -esym(40,enDeviceConnectionStatus)enDeviceConnectionStatus Undeclared identifier */


   tenDeviceStatusInfo enDeviceListChange = e8DEVICE_ADDED;

   if (0 != cou32DeviceHandle)
   {
      trEntireDeviceInfo rEntireDeviceInfo;
      rEntireDeviceInfo.bDeviceValidated = false;
      rEntireDeviceInfo.bIsDeviceUsed = false;
      rEntireDeviceInfo.rDeviceInfo = rfrDeviceInfo;

      //! Store the device in the device list
      m_oDeviceListLock.s16Lock();

      //! Check if the device already exists. If the device exists
      //! update the device with new info. Restore the device
      //! usage enabled flag
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
        //! Check if the device name or connection status of existing device has changed
        //! No update is required if the device name and connection status remains same
        //! If connection status changes to connected, device update will be sent with e8DEVICE_ADDED
         if((true ==(itMapDevList->second).rDeviceInfo.bDeviceUsageEnabled) &&
                  (rEntireDeviceInfo.rDeviceInfo.enDeviceConnectionStatus == (itMapDevList->second).rDeviceInfo.enDeviceConnectionStatus))
         {
            //! Send DEVICE_CHANGED if the device name has changed
            enDeviceListChange = (rEntireDeviceInfo.rDeviceInfo.szDeviceName != (itMapDevList->second).rDeviceInfo.szDeviceName)
                  ? e8DEVICE_CHANGED: e8DEVICE_STATUS_NOT_KNOWN;
         }
         //! Update the device name only if its not empty
         if(true == (rEntireDeviceInfo.rDeviceInfo.szDeviceName.empty()))
         {
            rEntireDeviceInfo.rDeviceInfo.szDeviceName =
                  (itMapDevList->second).rDeviceInfo.szDeviceName;
         }
         rEntireDeviceInfo.rDeviceInfo.bDeviceUsageEnabled =
               (itMapDevList->second).rDeviceInfo.bDeviceUsageEnabled;
         rEntireDeviceInfo.rDeviceInfo.bSelectedDevice =
               (itMapDevList->second).rDeviceInfo.bSelectedDevice;
         rEntireDeviceInfo.bIsUserDeselected =
               (itMapDevList->second).bIsUserDeselected;
         rEntireDeviceInfo.bIsSelectError =
               (itMapDevList->second).bIsSelectError;
         rEntireDeviceInfo.u32AccessIndex =
               (itMapDevList->second).u32AccessIndex;
         rEntireDeviceInfo.enUserPreference =
               (itMapDevList->second).enUserPreference;
         //! If the reported device category is unknown, use the old device category
         if(e8DEV_TYPE_UNKNOWN == rEntireDeviceInfo.rDeviceInfo.enDeviceCategory)
         {
            rEntireDeviceInfo.rDeviceInfo.enDeviceCategory =
                  (itMapDevList->second).rDeviceInfo.enDeviceCategory;
         }
      } // if (m_mapDeviceInfoList.end() != itMapDevList)

      m_mapDeviceInfoList[cou32DeviceHandle] = rEntireDeviceInfo;

      m_oDeviceListLock.vUnlock();
   }// if( 0!= cou32DeviceHandle)

   ETG_TRACE_USR1(("spi_tclDeviceListHandler::enAddDeviceToList: Adding Device 0x%x to list due to change in DeviceStatus = %d \n",
            cou32DeviceHandle, ETG_ENUM(DEVICE_STATUS_INFO, enDeviceListChange)));
   return enDeviceListChange;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vRemoveDeviceFromList
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vRemoveDeviceFromList(
         const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vRemoveDeviceFromList: "
            "Removing Device from list  = 0x%x  \n", cou32DeviceHandle));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         m_mapDeviceInfoList.erase(itMapDevList);
      } // if (m_mapDeviceInfoList.end() != itMapDevList)

      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vGetDeviceList
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vGetDeviceList(
         std::vector<trDeviceInfo>& rfvecDeviceInfoList,
         t_Bool bCertifiedOnly, t_Bool bConnectedOnly)
{
	/*lint -esym(40,bDeviceValidated) bDeviceValidated Undeclared identifier */
	/*lint -esym(40,enDeviceCategory) enDeviceCategory Undeclared identifier */
	
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vGetDeviceList: Devices reported should be Certified = %d "
            "Connected = %d  DeviceList size = %d\n",
            bCertifiedOnly, bConnectedOnly, m_mapDeviceInfoList.size()));

   m_oDeviceListLock.s16Lock();
   //! Populate device list based on bCertifiedOnly and bConnectedOnly
   //! if bCertifiedOnly = true, check if device is certified
   //! if bConnectedOnly = true, check if device is in connected state
   //! if both are false add all devices to device list
   //! ----------------------------------------------------------------------------
   //! !bCertifiedOnly || |   !bConnectedOnly ||                    |     Result
   //!     bDeviceValidated  |enDeviceConnectionStatus == e8DEV_CONNECTED |(add to list)
   //!   true                |    true                                    |   true
   //!   true                |    false                                   |   false
   //!   false               |    true                                    |   false
   //!   false               |    false                                   |   false

   std::vector<trEntireDeviceInfo> vecEntireDeviceInfoList;
   for (auto itMapDevList = m_mapDeviceInfoList.begin();
            itMapDevList != m_mapDeviceInfoList.end(); ++itMapDevList)
   {
      trEntireDeviceInfo& rfrDeviceInfoListItem = (itMapDevList->second);
      //! If the device friendly name is not available, update HMi with manufacturer name
      if((true == rfrDeviceInfoListItem.rDeviceInfo.szDeviceName.empty()) && 
	     (false == rfrDeviceInfoListItem.rDeviceInfo.szDeviceManufacturerName.empty()))
      {
         rfrDeviceInfoListItem.rDeviceInfo.szDeviceName = rfrDeviceInfoListItem.rDeviceInfo.szDeviceManufacturerName;
      }

      t_Bool bDeviceCertified = (rfrDeviceInfoListItem).bDeviceValidated;
      t_Bool bDeviceConnected = (e8DEV_CONNECTED
               == rfrDeviceInfoListItem.rDeviceInfo.enDeviceConnectionStatus);
      //! Invalidate the device if the device is deleted by user and is not yet connected.
      //! This is required in the following use case: If user disconnects and  deletes the device from the list,
      //! add device will still be shown as HMI doen't have information on device connection status
      t_Bool bDeviceValid = ((true == rfrDeviceInfoListItem.rDeviceInfo.bDeviceUsageEnabled)||
            ((false == rfrDeviceInfoListItem.rDeviceInfo.bDeviceUsageEnabled) && (true ==bDeviceConnected)));
      t_Bool bValidityCondMet = (true == bDeviceValid) && ((false == bCertifiedOnly) || (true == bDeviceCertified));
      t_Bool bConnectionCondMet = (false == bConnectedOnly) || (true == bDeviceConnected);
      if ((true == bValidityCondMet) && (true == bConnectionCondMet))
      {
         vecEntireDeviceInfoList.push_back((rfrDeviceInfoListItem));
      }   //if ((true == bValidityCondMet) && (true == bConnectionCondMet))
   }
   m_oDeviceListLock.vUnlock();

   vSortDeviceList(vecEntireDeviceInfoList, rfvecDeviceInfoList);

   //! Display device list
   vDisplayDeviceList(rfvecDeviceInfoList);
}

/***************************************************************************
** FUNCTION:  spi_tclDeviceListHandler::vDisplayDeviceList(const std::vec...
***************************************************************************/
t_Void spi_tclDeviceListHandler::vDisplayDeviceList(
	   const std::vector<trDeviceInfo>& corfvecDeviceInfoList) const
{
    ETG_TRACE_USR1(("spi_tclDeviceListHandler::vDisplayDeviceList entered"));
    // Iterate through all the devices in the given device list
    for (t_U32 u32DevCount = 0; u32DevCount < corfvecDeviceInfoList.size(); ++u32DevCount)
    {
        const trDeviceInfo& corfrDeviceInfo = corfvecDeviceInfoList[u32DevCount];
        //Display device information of each device
        ETG_TRACE_USR4(("  Device information List  Index = %d", u32DevCount));
        ETG_TRACE_USR4((" DeviceHandle= %u (0x%x)",corfrDeviceInfo.u32DeviceHandle,
            corfrDeviceInfo.u32DeviceHandle));
        ETG_TRACE_USR4((" Device Name = %s", corfrDeviceInfo.szDeviceName.c_str()));
        ETG_TRACE_USR4((" Device Model Name = %s", corfrDeviceInfo.szDeviceModelName.c_str()));
        ETG_TRACE_USR4((" Device Manufacturer Name = %s",
            corfrDeviceInfo.szDeviceManufacturerName.c_str()));
        ETG_TRACE_USR4((" Bluetooth Address = %s", corfrDeviceInfo.szBTAddress.c_str()));
        ETG_TRACE_USR4((" Device Category = %d",
            ETG_ENUM(DEVICE_CATEGORY,corfrDeviceInfo.enDeviceCategory)));
        ETG_TRACE_USR4((" Device Connection Status = %d",
            ETG_ENUM(CONNECTION_STATUS, corfrDeviceInfo.enDeviceConnectionStatus)));
        ETG_TRACE_USR4((" Device Connection Type = %d",
            ETG_ENUM(CONNECTION_TYPE, corfrDeviceInfo.enDeviceConnectionType)));
        ETG_TRACE_USR4((" Mirrorlink Version : %d.%d",
            corfrDeviceInfo.rVersionInfo.u32MajorVersion,
            corfrDeviceInfo.rVersionInfo.u32MinorVersion));
        ETG_TRACE_USR4((" Device Usage Enabled: %d",
            ETG_ENUM(BOOL,corfrDeviceInfo.bDeviceUsageEnabled)));
        ETG_TRACE_USR4((" Device Selection Flag: %d",
            ETG_ENUM(BOOL,corfrDeviceInfo.bSelectedDevice)));
        ETG_TRACE_USR4((" DAP Support: %d",
            ETG_ENUM(BOOL,corfrDeviceInfo.bDAPSupport)));
        ETG_TRACE_USR4((" Device Type: %d",
            ETG_ENUM(DEVICE_TYPE,corfrDeviceInfo.rProjectionCapability.enDeviceType)));
        ETG_TRACE_USR4((" USBPortType: %d",
            ETG_ENUM(USB_PORT_TYPE,corfrDeviceInfo.rProjectionCapability.enUSBPortType)));			
        ETG_TRACE_USR4((" Mirrorlink Support: %d",
            ETG_ENUM(SPI_SUPPORT,corfrDeviceInfo.rProjectionCapability.enMirrorlinkSupport)));
        ETG_TRACE_USR4((" Android Auto support: %d",
            ETG_ENUM(DEVICE_TYPE,corfrDeviceInfo.rProjectionCapability.enAndroidAutoSupport)));
        ETG_TRACE_USR4((" Carplay Support : %d",
            ETG_ENUM(DEVICE_TYPE,corfrDeviceInfo.rProjectionCapability.enCarplaySupport)));
    }//for (t_U32 u32DevCount = 0; u32DevCount < corfvecDeviceInfoList.size(); u32DevCount++)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vGetEntireDeviceList
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vGetEntireDeviceList(
         std::map<t_U32, trEntireDeviceInfo> &rfrmapDeviceInfoList)
{
   m_oDeviceListLock.s16Lock();
   rfrmapDeviceInfoList = m_mapDeviceInfoList;
   m_oDeviceListLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclDeviceListHandler:: vGetEntireDeviceList: DeviceList size = %d ",
            rfrmapDeviceInfoList.size()));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetUserDeselectionFlag
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetUserDeselectionFlag(const t_U32 cou32DeviceHandle,
         t_Bool bState)
{
	/*lint -esym(40,bIsUserDeselected)bIsUserDeselected Undeclared identifier */
	/*lint -esym(40,second)second Undeclared identifier */
	
   ETG_TRACE_USR1(("spi_tclDeviceListHandler:: vSetUserDeselectionFlag cou32DeviceHandle =0x%x,  bState = %d \n",
            cou32DeviceHandle, ETG_ENUM(BOOL,bState)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         (itMapDevList->second).bIsUserDeselected = bState;
      }
      m_oDeviceListLock.vUnlock();

      spi_tclDevHistory oDeviceHistory;
      oDeviceHistory.vSetUserDeselectionFlag(cou32DeviceHandle, bState);
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bGetUserDeselectionFlag
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bGetUserDeselectionFlag(const t_U32 cou32DeviceHandle)
{
   t_Bool bState = false;
   if (0 != cou32DeviceHandle)
   {
	   /*lint -esym(40,bIsUserDeselected)bIsUserDeselected Undeclared identifier */
	   /*lint -esym(40,second)second Undeclared identifier */
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         bState =(itMapDevList->second).bIsUserDeselected;
      }
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)      
   ETG_TRACE_USR1(("spi_tclDeviceListHandler:: bGetUserDeselectionFlag cou32DeviceHandle =0x%x,  bState = %d \n",
            cou32DeviceHandle, ETG_ENUM(BOOL,bState)));
   return bState;
}

/***************************************************************************
** FUNCTION:  spi_tclDeviceListHandler::u32GetNoofConnectedDevices
***************************************************************************/
t_U32 spi_tclDeviceListHandler::u32GetNoofConnectedDevices()
{
   t_U32 u32NoofConnectedDevices = 0;
   /*lint -esym(40,second)second Undeclared identifier */
   /*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
   /*lint -esym(40,enDeviceConnectionStatus)bSelectedDevice Undeclared identifier */
   m_oDeviceListLock.s16Lock();
   for (auto itMapDevList = m_mapDeviceInfoList.begin();
            itMapDevList != m_mapDeviceInfoList.end(); ++itMapDevList)
   {
      if (e8DEV_CONNECTED == (itMapDevList->second).rDeviceInfo.enDeviceConnectionStatus)
      {
         u32NoofConnectedDevices++;
      } // if (e8DEV_CONNECTED == (itMapDevList->second).rDeviceInfo.enDeviceConnectionStatus)
   }
   m_oDeviceListLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::u32GetNoofConnectedDevices = %d \n", u32NoofConnectedDevices));
   return u32NoofConnectedDevices;
}


/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceValidity
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetDeviceValidity(
         const t_U32 cou32DeviceHandle, t_Bool bDeviceValid)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,bDeviceValidated) bDeviceValidated Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetDeviceValidity: cou32DeviceHandle = 0x%x "
            "bValidDevice = %d \n", cou32DeviceHandle, ETG_ENUM(BOOL,bDeviceValid)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         (itMapDevList->second).bDeviceValidated = bDeviceValid;
      }
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bIsDeviceValid
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bIsDeviceValid(const t_U32 cou32DeviceHandle)
{
   t_Bool bValidDevice = false;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         bValidDevice = true;
      }
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)

   ETG_TRACE_USR2(("spi_tclDeviceListHandler::bIsDeviceValid  = %d \n",
            ETG_ENUM(BOOL,bValidDevice)));
   return bValidDevice;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bGetDeviceInfo
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bGetDeviceInfo(const t_U32 cou32DeviceHandle,
         trDeviceInfo &rfrDeviceInfo)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
   t_Bool bDevFound = false;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         bDevFound = true;
         rfrDeviceInfo = itMapDevList->second.rDeviceInfo;
      }
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)
   ETG_TRACE_USR4(("spi_tclDeviceListHandler::bGetDeviceInfo: u32DeviceHandle = 0x%x  bDevFound = %d\n",
            cou32DeviceHandle, ETG_ENUM(BOOL,bDevFound)));
   return bDevFound;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceName
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetDeviceName(const t_U32 cou32DeviceHandle, t_String &rfrszDeviceName)
{
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetDeviceName: cou32DeviceHandle = 0x%x  rfrszDeviceName = %s\n",
            cou32DeviceHandle, rfrszDeviceName.c_str()));
   if ((0 != cou32DeviceHandle) && (false == rfrszDeviceName.empty()))
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         itMapDevList->second.rDeviceInfo.szDeviceName = rfrszDeviceName;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)

      m_oDeviceListLock.vUnlock();
      spi_tclDevHistory oDeviceHistory;
      oDeviceHistory.vSetDeviceName(cou32DeviceHandle, rfrszDeviceName);
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vAddDeviceToHistory
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vAddDeviceToHistory(
         const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,u32AccessIndex)u32AccessIndex Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vAddDeviceToHistory: Adding Device = 0x%x to history \n",
            cou32DeviceHandle));

   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      //! Fetch the device info for requested handle and write to history
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         spi_tclDevHistory oDevHistory;
         itMapDevList->second.u32AccessIndex = (oDevHistory.u32GetMaxAccessIndex()) + 1 ;
         trEntireDeviceInfo rfrEntireDevInfo = itMapDevList->second;
         m_oDeviceListLock.vUnlock();
         //! Set the device connection status to disconnected before writing to history
         rfrEntireDevInfo.rDeviceInfo.enDeviceConnectionStatus
                  = e8DEV_NOT_CONNECTED;
         //! Write the device history to a database
         oDevHistory.bAddtoHistorydb(rfrEntireDevInfo);
         oDevHistory.vDisplayDevHistorydb();
      }
      else
      {
         m_oDeviceListLock.vUnlock();
         ETG_TRACE_ERR(("Unable to Add to History: Device 0x%x not found in the list \n",
                  cou32DeviceHandle));
      } // if (m_mapDeviceInfoList.end() != itMapDevList)
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vRemoveDeviceFromHistory
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vRemoveDeviceFromHistory(
         const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vRemoveDeviceFromHistory: "
            "Removing Device from history = 0x%x  \n", cou32DeviceHandle));
   if (0 != cou32DeviceHandle)
   {
      spi_tclDevHistory oDevHistory;
      oDevHistory.bDeleteFromHistorydb(cou32DeviceHandle);
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bGetDeviceHistory
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bGetDeviceHistory(
         std::vector<trEntireDeviceInfo>& rfvecDevHistory) const
{
   spi_tclDevHistory oDevHistory;
   t_Bool bRetDevHistroy = oDevHistory.bGetDeviceHistoryFromdb(rfvecDevHistory);
   oDevHistory.vDisplayDevHistorydb();
   ETG_TRACE_USR4(("spi_tclDeviceListHandler::bGetDeviceHistory: Device hsistory size = %d \n",
            rfvecDevHistory.size()));
   return bRetDevHistroy;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::enGetDeviceCategory
 ***************************************************************************/
tenDeviceCategory spi_tclDeviceListHandler::enGetDeviceCategory(
         const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,enDeviceCategory)enDeviceCategory Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
   tenDeviceCategory enDevCat = e8DEV_TYPE_UNKNOWN;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         enDevCat = itMapDevList->second.rDeviceInfo.enDeviceCategory;
      } //if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)

   ETG_TRACE_USR4(("spi_tclDeviceListHandler::enGetDeviceCategory: Device Handle = 0x%x  "
            "has Category = %d \n", cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDevCat)));
   return enDevCat;
}


/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceCategory
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetDeviceCategory(const t_U32 cou32DeviceHandle, tenDeviceCategory enDeviceCategory)
{
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetDeviceCategory: "
            "Device 0x%x's category set to %d  \n", cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDeviceCategory)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         itMapDevList->second.rDeviceInfo.enDeviceCategory = enDeviceCategory;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::enGetDeviceConnType
 ***************************************************************************/
tenDeviceConnectionType spi_tclDeviceListHandler::enGetDeviceConnType(
         const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,enDeviceConnectionType)enDeviceConnectionType Undeclared identifier */

   tenDeviceConnectionType enDevConnType = e8UNKNOWN_CONNECTION;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         enDevConnType
                  = itMapDevList->second.rDeviceInfo.enDeviceConnectionType;
      } //if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   }// if (0 != cou32DeviceHandle)

   ETG_TRACE_USR4(("spi_tclDeviceListHandler::enGetDeviceConnType Device Handle = 0x%x  "
            "has ConnType = %d \n", cou32DeviceHandle,  ETG_ENUM(CONNECTION_TYPE,enDevConnType)));
   return enDevConnType;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::enGetDeviceConnStatus
 ***************************************************************************/
tenDeviceConnectionStatus spi_tclDeviceListHandler::enGetDeviceConnStatus(
         const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
   tenDeviceConnectionStatus enDevConnStatus = e8DEV_NOT_CONNECTED;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         enDevConnStatus
                  = itMapDevList->second.rDeviceInfo.enDeviceConnectionStatus;
      } //if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)

   ETG_TRACE_USR2(("spi_tclDeviceListHandler::enGetDeviceConnStatus Device Handle = 0x%x  "
            "ConnStatus = %d \n", cou32DeviceHandle, ETG_ENUM(CONNECTION_STATUS,enDevConnStatus)));
   return enDevConnStatus;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vGetBTAddress
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vGetBTAddress(const t_U32 cou32DeviceHandle,
         t_String &rfszBTAddress)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,szBTAddress)szBTAddress Undeclared identifier */

   m_oDeviceListLock.s16Lock();
   auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
   if (m_mapDeviceInfoList.end() != itMapDevList)
   {
      rfszBTAddress
               = itMapDevList->second.rDeviceInfo.szBTAddress;
   } //if (m_mapDeviceInfoList.end() != itMapDevList)
   m_oDeviceListLock.vUnlock();

   ETG_TRACE_USR4(("spi_tclDeviceListHandler::vGetBTAddress Device Handle = 0x%x  "
            "szBTAddress = %s \n", cou32DeviceHandle, rfszBTAddress.c_str()));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceSelection
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetDeviceSelection(
         const t_U32 cou32DeviceHandle, t_Bool bSelectDev)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
	/*lint -esym(40,bIsDeviceUsed)bIsDeviceUsed	 Undeclared identifier */
	/*lint -esym(40,bSelectedDevice)bSelectedDevice Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetDeviceSelection: "
            "cou32DeviceHandle = 0x%x, bSelectDev = %d \n",
            cou32DeviceHandle, ETG_ENUM(BOOL, bSelectDev)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         itMapDevList->second.rDeviceInfo.bSelectedDevice = bSelectDev;
         if (true == bSelectDev)
         {
            itMapDevList->second.bIsDeviceUsed = true;
         }
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)

      m_oDeviceListLock.vUnlock();
      spi_tclDevHistory oDeviceHistory;
      oDeviceHistory.vSetSelectedDevice(cou32DeviceHandle, bSelectDev);
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::u32GetSelectedDevice
 ***************************************************************************/
t_U32 spi_tclDeviceListHandler::u32GetSelectedDevice()
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
	/*lint -esym(40,bSelectedDevice)bSelectedDevice Undeclared identifier */
	/*lint -esym(40,first)first Undeclared identifier */
   t_U32 u32DeviceHandle = 0;
   m_oDeviceListLock.s16Lock();
   for (auto itMapDevList = m_mapDeviceInfoList.begin();
            itMapDevList != m_mapDeviceInfoList.end(); ++itMapDevList)
   {
      if (true == (itMapDevList->second).rDeviceInfo.bSelectedDevice)
      {
         u32DeviceHandle = itMapDevList->first;
         break;
      } // if (true == (itMapDevList->second).rDeviceInfo.bSelectedDevice)
   }
   m_oDeviceListLock.vUnlock();
   ETG_TRACE_USR4(("spi_tclDeviceListHandler::u32GetSelectedDevice SelectedDevice  = 0x%x  ",
            u32DeviceHandle));
   return u32DeviceHandle;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bWasDeviceSelected
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bWasDeviceSelected(
         const t_U32 cou32DeviceHandle)
{
   //! Device was previously selected(used) if it was written to History
   spi_tclDevHistory oDeviceHistory;
   return oDeviceHistory.bFindDevice(cou32DeviceHandle);
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceConnectionStatus
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetDeviceConnectionStatus(
         const t_U32 cou32DeviceHandle, tenDeviceConnectionStatus enConnStatus)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,enDeviceConnectionStatus)enDeviceConnectionStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetDeviceConnectionStatus: "
            "cou32DeviceHandle = 0x%x, enConnStatus = %d \n",
            cou32DeviceHandle,  ETG_ENUM(CONNECTION_STATUS,enConnStatus)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         itMapDevList->second.rDeviceInfo.enDeviceConnectionStatus
                  = enConnStatus;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bIsDeviceConnected
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bIsDeviceConnected(
         const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
   t_Bool bDevConnected = false;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         bDevConnected = (e8DEV_CONNECTED
                  == itMapDevList->second.rDeviceInfo.enDeviceConnectionStatus);
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)
   ETG_TRACE_USR2(("spi_tclDeviceListHandler::bIsDeviceConnected: cou32DeviceHandlee = 0x%x bDevConnected? %d ",
            cou32DeviceHandle, ETG_ENUM(BOOL, bDevConnected)));
   return bDevConnected;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetDAPSupport
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetDAPSupport(const t_U32 cou32DeviceHandle,
         t_Bool bDAPSupported)
{
	/*lint -esym(40,bDAPSupport)bDAPSupport Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetDAPSupport: cou32DeviceHandle = 0x%x bIsDAPSupported = %d\n",
            cou32DeviceHandle, ETG_ENUM(BOOL,bDAPSupported)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         itMapDevList->second.rDeviceInfo.bDAPSupport = bDAPSupported;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bIsDAPSupported
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bIsDAPSupported(const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
	/*lint -esym(40,bDAPSupport)bDAPSupport Undeclared identifier */
   t_Bool bDAPSupport = false;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         bDAPSupport = itMapDevList->second.rDeviceInfo.bDAPSupport;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } //  if (0 != cou32DeviceHandle)
   ETG_TRACE_USR2((" spi_tclDeviceListHandler::bIsDAPSupported: cou32DeviceHandle = 0x%x bDAPSupport = %d\n",
                     cou32DeviceHandle, ETG_ENUM(BOOL,bDAPSupport)));
   return bDAPSupport;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDeviceListHandler::vSetDeviceUsagePreference
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetDeviceUsagePreference(
      const t_U32 cou32DeviceHandle, tenEnabledInfo enEnabledInfo)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
	/*lint -esym(40,bDeviceUsageEnabled)bDeviceUsageEnabled Undeclared identifier */
	/*lint -esym(40,enDeviceConnectionStatus)enDeviceConnectionStatus Undeclared identifier */
   if (0 != cou32DeviceHandle)
   {
      t_Bool bDevConnected = false;
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         itMapDevList->second.rDeviceInfo.bDeviceUsageEnabled = (enEnabledInfo
               == e8USAGE_ENABLED);
         bDevConnected = (e8DEV_CONNECTED
                           == itMapDevList->second.rDeviceInfo.enDeviceConnectionStatus);
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();

      spi_tclDevHistory oDeviceHistory;
      oDeviceHistory.vSetDeviceUsagePreference(cou32DeviceHandle, enEnabledInfo);

   } //  if (0 != cou32DeviceHandle)

   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetDeviceUsagePreference for device 0x%x is set to %d \n",
               cou32DeviceHandle,  ETG_ENUM(ENABLED_INFO,enEnabledInfo)));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclDeviceListHandler::vGetDeviceUsagePreference
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vGetDeviceUsagePreference(
         const t_U32 cou32DeviceHandle, tenEnabledInfo& rfenEnabledInfo)
{
	/*lint -esym(40,second)second Undeclared identifier */
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         rfenEnabledInfo =
                  (true == itMapDevList->second.rDeviceInfo.bDeviceUsageEnabled) ?
                           e8USAGE_ENABLED : e8USAGE_DISABLED;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } // if (0 != cou32DeviceHandle)

   ETG_TRACE_USR4(("spi_tclDeviceListHandler::bGetDeviceUsagePreference: cou32DeviceHandle = 0x%x"
             "enUsagePref = %d \n",
            cou32DeviceHandle,  ETG_ENUM(ENABLED_INFO,rfenEnabledInfo)));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bSetDevProjUsage
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bSetDevProjUsage(tenDeviceCategory enSPIType,
      tenEnabledInfo enSPIState)
{
	/*lint -esym(40,second)second Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler:: bSetDevProjUsage enSPIType =%d,  bSPIState = %d \n",
            ETG_ENUM(DEVICE_CATEGORY,enSPIType), ETG_ENUM(BOOL, enSPIState)));

   t_Bool bRetVal = false;
   //Store setting in datapool.
   Datapool oDatapool;
   if(e8DEV_TYPE_DIPO == enSPIType)
   {
      bRetVal = oDatapool.bWriteDipoEnableSetting(enSPIState);
   }
   else if (e8DEV_TYPE_MIRRORLINK == enSPIType)
   {
      bRetVal = oDatapool.bWriteMLEnableSetting(enSPIState);
   }
   else if(e8DEV_TYPE_ANDROIDAUTO == enSPIType)
   {
      bRetVal = oDatapool.bWriteAAPEnableSetting(enSPIState);
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bGetDevProjUsage
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bGetDevProjUsage(tenDeviceCategory enSPIType,
         tenEnabledInfo &rfrEnabledInfo)
{
   t_Bool bRetVal = true;

   //Read setting from datapool.
   Datapool oDatapool;
   switch(enSPIType)
   {
      case e8DEV_TYPE_DIPO:
      {
         rfrEnabledInfo= oDatapool.bReadDipoEnableSetting();
      }
      break;
      case e8DEV_TYPE_MIRRORLINK:
      {
         rfrEnabledInfo= oDatapool.bReadMLEnableSetting();
      }
      break;
      case e8DEV_TYPE_ANDROIDAUTO:
      {
         rfrEnabledInfo= oDatapool.bReadAAPEnableSetting();
      }
      break;
      default:
      {
         rfrEnabledInfo= e8USAGE_DISABLED;
      }
      break;
   }

   ETG_TRACE_USR1(("spi_tclDeviceListHandler::bGetDevProjUsage  enDevCat = %d rfrEnabledInfo = %d  \n",
            ETG_ENUM(DEVICE_CATEGORY,enSPIType), ETG_ENUM(ENABLED_INFO,rfrEnabledInfo)));
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclDeviceListHandler::vSetSelectError
***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetSelectError(const t_U32 cou32DeviceHandle, t_Bool bIsError)
{
	/*lint -esym(40,second)second Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetSelectError cou32DeviceHandle  = 0x%x,  bIsError - %d ",
            cou32DeviceHandle, ETG_ENUM(BOOL, bIsError)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {  
         itMapDevList->second.bIsSelectError =  bIsError;;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } // if (0 != cou32DeviceHandle
}

/***************************************************************************
** FUNCTION:  spi_tclDeviceListHandler::bIsSelectError
***************************************************************************/
t_Bool spi_tclDeviceListHandler::bIsSelectError(const t_U32 cou32DeviceHandle)
{ 
	/*lint -esym(40,second)second Undeclared identifier */
   t_Bool bIsError = false;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         bIsError = itMapDevList->second.bIsSelectError;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } // if (0 != cou32DeviceHandle 
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::bIsSelectError cou32DeviceHandle  = 0x%x,  bIsError - %d  ",
            cou32DeviceHandle, ETG_ENUM(BOOL, bIsError)));
   return bIsError;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::u32GetLastSelectedDevice
 ***************************************************************************/
t_U32 spi_tclDeviceListHandler::u32GetLastSelectedDevice()
{
   spi_tclDevHistory oDeviceHistory;
   return oDeviceHistory.u32GetLastSelectedDevice();
}
      
/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vClearPrivateData
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vClearPrivateData()
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vClearPrivateData: Clearing device list \n"));
   m_oDeviceListLock.s16Lock();
   //! @Note: Erasing element and then incrementing iterator will result in incrementing invalid iterator
   //! Hence post increment operator is used for erase
   for (auto itMapDevList = m_mapDeviceInfoList.begin();
            itMapDevList != m_mapDeviceInfoList.end(); /*No Increment*/)
   {
      if (e8DEV_CONNECTED != (itMapDevList->second).rDeviceInfo.enDeviceConnectionStatus)
      {
         //! @Note the post increment Increments the iterator but returns the original value for use by erase
         m_mapDeviceInfoList.erase(itMapDevList++);
      } // if (e8DEV_CONNECTED == (itMapDevList->second).rDeviceInfo.enDeviceConnectionStatus)
      else
      {
         ++itMapDevList; //! Can use pre-increment in this case to make sure you have the efficient version
      }
   }
   m_oDeviceListLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bGetUSBConnectedFlag
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bGetUSBConnectedFlag(const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,bIsUSBConnected)bIsUSBConnected Undeclared identifier */
   t_Bool bRetval = false;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         bRetval = itMapDevList->second.bIsUSBConnected;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } // if (0 != cou32DeviceHandle
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::bGetUSBConnectedFlag cou32DeviceHandle = 0x%x USBConnected = %d\n", cou32DeviceHandle, ETG_ENUM(BOOL, bRetval)));
   return bRetval;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vClearPrivateData
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetUSBConnectedFlag(const t_U32 cou32DeviceHandle, t_Bool bUSBConnected)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,bIsUSBConnected)bIsUSBConnected Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetUSBConnectedFlag cou32DeviceHandle = 0x%x USBConnected = %d\n",
         cou32DeviceHandle, ETG_ENUM(BOOL, bUSBConnected)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         itMapDevList->second.bIsUSBConnected =  bUSBConnected;;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } // if (0 != cou32DeviceHandle
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::enGetUserPreference
 ***************************************************************************/
tenUserPreference spi_tclDeviceListHandler::enGetUserPreference(const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,enUserPreference)enUserPreference Undeclared identifier */
   tenUserPreference enUserPref = e8PREFERENCE_NOTKNOWN;
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         ETG_TRACE_USR4(("spi_tclDeviceListHandler::enGetUserPreference found\n"));
         enUserPref = itMapDevList->second.enUserPreference;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } // if (0 != cou32DeviceHandle
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::enGetUserPreference cou32DeviceHandle = 0x%x enUserPref = %d\n",
         cou32DeviceHandle, ETG_ENUM(USER_PREFERENCE, enUserPref)));
   return enUserPref;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetUserPreference
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetUserPreference(const t_U32 cou32DeviceHandle, tenUserPreference enUserPref)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,enUserPreference)enUserPreference Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetUserPreference cou32DeviceHandle = 0x%x enUserPref = %d\n",
         cou32DeviceHandle,ETG_ENUM(USER_PREFERENCE, enUserPref)));
   if (0 != cou32DeviceHandle)
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         ETG_TRACE_USR4(("spi_tclDeviceListHandler::enGetUserPreference found\n"));
         itMapDevList->second.enUserPreference =  enUserPref;;
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   } // if (0 != cou32DeviceHandle
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::enGetSPISupport
 ***************************************************************************/
tenSPISupport spi_tclDeviceListHandler::enGetSPISupport(const t_U32 cou32DeviceHandle,
         tenDeviceCategory enSPIType)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
	/*lint -esym(40,enMirrorlinkSupport)enMirrorlinkSupport Undeclared identifier */
	/*lint -esym(40,enAndroidAutoSupport)enAndroidAutoSupport Undeclared identifier */
	/*lint -esym(40,enCarplaySupport)enCarplaySupport Undeclared identifier */
   tenSPISupport enSPISupport = e8SPI_SUPPORT_UNKNOWN;
   if (true == bIsDeviceValid(cou32DeviceHandle))
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         switch (enSPIType)
         {
            case e8DEV_TYPE_MIRRORLINK:
            {
               enSPISupport = (itMapDevList->second).rDeviceInfo.rProjectionCapability.enMirrorlinkSupport;
               break;
            }
            case e8DEV_TYPE_ANDROIDAUTO:
            {
               enSPISupport = (itMapDevList->second).rDeviceInfo.rProjectionCapability.enAndroidAutoSupport;
               break;
            }
            case e8DEV_TYPE_DIPO:
            {
               enSPISupport = (itMapDevList->second).rDeviceInfo.rProjectionCapability.enCarplaySupport;
               break;
            }
            default:
            {
               enSPISupport = e8SPI_SUPPORT_UNKNOWN;
               break;
            }
         }
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   }
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::enGetSPISupport Device ID = %d SPI Type = %d Support =%d \n",
            cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enSPIType), ETG_ENUM(SPI_SUPPORT, enSPISupport)));
   return enSPISupport;
}


/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSetSPISupport
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSetSPISupport(const t_U32 cou32DeviceHandle,
         tenDeviceCategory enSPIType, tenSPISupport enSPISupport)
{
	/*lint -esym(40,second)second Undeclared identifier */
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
	/*lint -esym(40,enMirrorlinkSupport)enMirrorlinkSupport Undeclared identifier */
	/*lint -esym(40,enAndroidAutoSupport)enAndroidAutoSupport Undeclared identifier */
	/*lint -esym(40,enCarplaySupport)enCarplaySupport Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSetSPISupport Device ID = %d SPI Type = %d Support =%d \n",
         cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY,enSPIType), ETG_ENUM(SPI_SUPPORT, enSPISupport)));
   if (true == bIsDeviceValid(cou32DeviceHandle))
   {
      m_oDeviceListLock.s16Lock();
      auto itMapDevList = m_mapDeviceInfoList.find(cou32DeviceHandle);
      if (m_mapDeviceInfoList.end() != itMapDevList)
      {
         switch (enSPIType)
         {
            case e8DEV_TYPE_MIRRORLINK:
            {
               (itMapDevList->second).rDeviceInfo.rProjectionCapability.enMirrorlinkSupport = enSPISupport;
               break;
            }
            case e8DEV_TYPE_ANDROIDAUTO:
            {
               (itMapDevList->second).rDeviceInfo.rProjectionCapability.enAndroidAutoSupport = enSPISupport;
               break;
            }
            case e8DEV_TYPE_DIPO:
            {
               (itMapDevList->second).rDeviceInfo.rProjectionCapability.enCarplaySupport = enSPISupport;
               break;
            }
            default:
            {
               //! TODO Error mSg
               break;
            }
         }
      } //  if (m_mapDeviceInfoList.end() != itMapDevList)
      m_oDeviceListLock.vUnlock();
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vGetUnauthorizedDevicesList
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vGetUnauthorizedDevicesList(std::vector<trDeviceAuthInfo> &rfrvecDevAuthInfo)
{
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vGetUnauthorizedDevicesList \n"));
   m_oUnauthDeviceListLock.s16Lock();
   std::map<t_U32, trUnauthDeviceInfo>::iterator itmapDevAuth;
   for(itmapDevAuth= m_mapDevAuthInfo.begin(); itmapDevAuth != m_mapDevAuthInfo.end(); itmapDevAuth++)
   {
      rfrvecDevAuthInfo.push_back((itmapDevAuth->second).rAuthInfo);
   }
   m_oUnauthDeviceListLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vAddUnauthorizedDeviceToList
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vAddUnauthorizedDeviceToList(const trUnauthDeviceInfo &rfrDevAuthInfo)
{
   m_oUnauthDeviceListLock.s16Lock();
   m_mapDevAuthInfo[rfrDevAuthInfo.rAuthInfo.u32DeviceHandle] = rfrDevAuthInfo;
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vAddUnauthorizedDeviceToList DeviceID = 0x%x List size = %d\n",
         rfrDevAuthInfo.rAuthInfo.u32DeviceHandle, m_mapDevAuthInfo.size()));
   m_oUnauthDeviceListLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vRemoveUnauthorizedDevice
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vRemoveUnauthorizedDevice(const t_U32 cou32DeviceHandle)
{
   m_oUnauthDeviceListLock.s16Lock();
   m_mapDevAuthInfo.erase(cou32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vRemoveUnauthorizedDevice DeviceID = 0x%x List size = %d \n",
         cou32DeviceHandle, m_mapDevAuthInfo.size()));
   m_oUnauthDeviceListLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::bGetUnauthorizedDeviceInfo
 ***************************************************************************/
t_Bool spi_tclDeviceListHandler::bGetUnauthorizedDeviceInfo(const t_U32 cou32DeviceHandle, trUnauthDeviceInfo &rfrDevAuthInfo)
{
   t_Bool bRetVal = false;
   m_oUnauthDeviceListLock.s16Lock();
   if(m_mapDevAuthInfo.end() != m_mapDevAuthInfo.find(cou32DeviceHandle))
   {
      bRetVal =  true;
      rfrDevAuthInfo = m_mapDevAuthInfo[cou32DeviceHandle];
   }
   m_oUnauthDeviceListLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vGetUnauthorizedDeviceInfo DeviceID = 0x%x bRetVal = %d\n", cou32DeviceHandle,  ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}
 
/***************************************************************************
 ** FUNCTION:  spi_tclDeviceListHandler::vSortDeviceList
 ***************************************************************************/
t_Void spi_tclDeviceListHandler::vSortDeviceList(
         std::vector<trEntireDeviceInfo>& rfvecEntireDeviceInfoList,
         std::vector<trDeviceInfo>& rfvecDeviceInfoList)
{
	/*lint -esym(40,rDeviceInfo)rDeviceInfo Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclDeviceListHandler::vSortDeviceList : Sorting device list in the order"
         " of last selected and connected devices \n"));
   //! Sorts device list in the order Connected Devices (arranged as per selection time) -> Not connected
   //! stable_sort is used preserves the relative order of the elements with equivalent values
   //! The last argument trEntireDeviceInfo() is a call for overloaded function call operator
   //! for comparison. Used by std::sort algorithm to compare objects of type trEntireDeviceInfo
   std::stable_sort(rfvecEntireDeviceInfoList.begin(), rfvecEntireDeviceInfoList.end(),
            trEntireDeviceInfo());

   //! Copy the device info from the sorted device list
   std::vector<trEntireDeviceInfo>::iterator itvecEntireList;
   for (itvecEntireList = rfvecEntireDeviceInfoList.begin();
            itvecEntireList != rfvecEntireDeviceInfoList.end(); itvecEntireList++)
   {
      rfvecDeviceInfoList.push_back(itvecEntireList->rDeviceInfo);
   }
}
//lint –restore
