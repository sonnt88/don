/*!
 *******************************************************************************
 * \file             spi_tclConnSettings.cpp
 * \brief            Project specific settings for Connection Management
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Project specific settings for Connection Management
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 24.11.2014 | Shiva Kumar Gurija           | XML Validation

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "FileHandler.h"
#include "crc.h"
#include "StringHandler.h"
#include "XmlDocument.h"
#include "XmlReader.h"
#include "spi_tclConnSettings.h"
#include "Datapool.h"
#include "spi_tclConfigReader.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclConnSettings.cpp.trc.h"
#endif
#endif

#ifdef VARIANT_S_FTR_ENABLE_GM
static const t_Char scoczGMXmlConfigFile[] = "/opt/bosch/gm/policy.xml";
#else
static const t_Char scoczG3GXmlConfigFile[] = "/opt/bosch/spi/xml/policy.xml";
#endif
static const t_U8 scou8DefaultHistorySize = 10;
static const t_U8 scou8DefaultTimeOnStartup = 30;


using namespace shl::xml;
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclConnSettings::~spi_tclConnSettings
 ***************************************************************************/
spi_tclConnSettings::~spi_tclConnSettings()
{

}
/***************************************************************************
 ** FUNCTION:  tenEnabledInfo spi_tclConnSettings::bIntializeConnSettings
 ***************************************************************************/
t_Void spi_tclConnSettings::vIntializeConnSettings()
{
   const t_Char* pczConfigFilePath = NULL;
#ifdef VARIANT_S_FTR_ENABLE_GM
   pczConfigFilePath = scoczGMXmlConfigFile;
#else
   pczConfigFilePath = scoczG3GXmlConfigFile;
#endif

   //Check the validity of the xml file
   spi::io::FileHandler oPolicySettingsFile(pczConfigFilePath,
            spi::io::SPI_EN_RDONLY);
   if (true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument oXmlDoc(pczConfigFilePath);
      tclXmlReader oXmlReader(&oXmlDoc, this);
      oXmlReader.bRead("CONNMNGR");
   } // if (true == oPolicySettingsFile.bIsValid())

   vDisplayConnSettings();
   vReadConfigurationData();
}

/***************************************************************************
 ** FUNCTION:  tenMirrolinkSupport spi_tclConnSettings::enGetMirrorlinkSupport
 ***************************************************************************/
tenEnabledInfo spi_tclConnSettings::enGetMirrorlinkSupport() const
{
   return m_enMLEnabled;
}

/***************************************************************************
 ** FUNCTION:  tenDiPoSupport spi_tclConnSettings::enGetDiPoSupport
 ***************************************************************************/
tenEnabledInfo spi_tclConnSettings::enGetDiPoSupport() const
{
   return m_enDiPoEnabled;
}

/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclConnSettings::u32GetDelayforStartupSel
 ***************************************************************************/
t_U32 spi_tclConnSettings::u32GetDelayforStartupSel() const
{
   return m_u32DelayForSelection;
}

/***************************************************************************
 ** FUNCTION:  tenDeviceSelectionMode spi_tclConnSettings::enGetDeviceSelectionMode
 ***************************************************************************/
tenDeviceSelectionMode spi_tclConnSettings::enGetDeviceSelectionMode() const
{
   return m_enSelMode;
}

/***************************************************************************
 ** FUNCTION:  t_Void vSetDeviceSelectionMode()
 ***************************************************************************/
t_Void spi_tclConnSettings::vSetDeviceSelectionMode(tenDeviceSelectionMode enDevSelectionMode)
{
   m_enSelMode = enDevSelectionMode;
   Datapool oDatapool;
   oDatapool.bWriteSelectionMode(enDevSelectionMode);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConnSettings::enGetMaxMLVersionSupported
 ***************************************************************************/
t_Void spi_tclConnSettings::vGetMaxMLVersionSupported(
         trVersionInfo &rfrVerInfo) const
{
   rfrVerInfo = m_rMaxVer;
}

/***************************************************************************
 ** FUNCTION:  tenSupportedConnectionTypes spi_tclConnSettings::enGetSupportedConnectionTypes
 ***************************************************************************/
tenSupportedConnectionTypes spi_tclConnSettings::enGetSupportedConnectionTypes() const
{
   return m_enSupportedConnType;
}

/***************************************************************************
 ** FUNCTION:   tenEnabledInfo spi_tclConnSettings::enGetDeviceAuthEnableInfo()
 ***************************************************************************/
tenEnabledInfo spi_tclConnSettings::enGetDeviceAuthEnableInfo() const
{
   return m_enDeviceAuthEnableInfo;
}

/***************************************************************************
 ** FUNCTION:  tenDeviceCategory spi_tclConnSettings::enGetTechnologyPreference
 ***************************************************************************/
tenDeviceCategory spi_tclConnSettings::enGetTechnologyPreference() const
{
   tenDeviceCategory enTechnologyPref = e8DEV_TYPE_ANDROIDAUTO;
   Datapool oDatapool;
   oDatapool.bReadTechnologyPreference(enTechnologyPref);
   return enTechnologyPref;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConnSettings::vSetTechnologyPreference
 ***************************************************************************/
t_Void spi_tclConnSettings::vSetTechnologyPreference(tenDeviceCategory enTechnologyPref)
{
   Datapool oDatapool;
   oDatapool.bWriteTechnologyPreference(enTechnologyPref);
}

/***************************************************************************
 ** FUNCTION:  tenML10Support spi_tclConnSettings::enGetML10Support
 ***************************************************************************/
tenML10Support spi_tclConnSettings::enGetML10Support() const
{

   return m_enML10Support;
}

/***************************************************************************
 ** FUNCTION:  tenEnabledInfo spi_tclConnSettings::enGetCertificateType
 ***************************************************************************/
tenCertificateType spi_tclConnSettings::enGetCertificateType() const
{
   return m_enCertificateType;
}

/***************************************************************************
 ** FUNCTION:  tenSelModePriority spi_tclConnSettings::enGetSelectionModePriority
 ***************************************************************************/
tenSelModePriority spi_tclConnSettings::enGetSelectionModePriority()
{
   return m_enSelPrority;
}

/***************************************************************************
 ** FUNCTION:  tenDAPPreference spi_tclConnSettings::enGetDAPPreference
 ***************************************************************************/
tenDAPPreference spi_tclConnSettings::enGetDAPPreference()
{
   return m_enDAPPref;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConnSettings::vGetPersistentStoragePath
 ***************************************************************************/
t_Void spi_tclConnSettings::vGetPersistentStoragePath(
         t_String &rfszPersStoragePath)
{
   rfszPersStoragePath = m_szPerStoragePath;
}

/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclConnSettings::u32GetDeviceHistorySize
 ***************************************************************************/
t_U32 spi_tclConnSettings::u32GetDeviceHistorySize()
{
   return m_u32DevHistorySize;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclConnSettings::bIsXMLValidationEnabled
***************************************************************************/
t_Bool spi_tclConnSettings::bIsXMLValidationEnabled()  const
{
   //Add code
   return m_bXMLValidationEnabled;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclConnSettings::bIsEarlyRoleSwitchRequired
 ***************************************************************************/
t_Bool spi_tclConnSettings::bIsEarlyRoleSwitchRequired() const
{
   return m_bDiPoEarlyRoleSwitch;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclConnSettings::vGetHeadUnitInfo
 ***************************************************************************/
t_Void spi_tclConnSettings::vGetHeadUnitInfo(trHeadUnitInfo &rfrHeadUnitInfo)
{
   rfrHeadUnitInfo = m_rHeadUnitInfo;
}


/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclConnSettings::vGetDefaultProjUsageSettings
 ***************************************************************************/
t_Void spi_tclConnSettings::vGetDefaultProjUsageSettings(tenEnabledInfo &enEnabledInfo, tenDeviceCategory enSPIType)
{
   spi_tclConfigReader *poConfigReader= spi_tclConfigReader::getInstance();
   if(NULL != poConfigReader)
   {
      poConfigReader->vGetDefaultProjUsageSettings(enSPIType, enEnabledInfo);
   }
}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclConnSettings::spi_tclConnSettings
 ***************************************************************************/
spi_tclConnSettings::spi_tclConnSettings() :
                  m_enDiPoEnabled(e8USAGE_ENABLED),
                  m_enMLEnabled(e8USAGE_ENABLED),
                  m_u32DelayForSelection(scou8DefaultTimeOnStartup),
                  m_enSelMode(e16DEVICESEL_MANUAL),
                  m_enSupportedConnType(e8SUPPOTSCONN_USB),
                  m_enDeviceAuthEnableInfo(e8USAGE_DISABLED),
                  m_enSelPrority(e8PRIORITY_DEVICELIST_HISTORY),
                  m_enDAPPref(e8DAPPREF_ON_DEVICECONNECTION),
                  m_enML10Support(e8ML10_ALLDEVICES),
                  m_szPerStoragePath("/opt/bosch/DeviceHistory.db"),
                  m_u32DevHistorySize(scou8DefaultHistorySize),
                  m_bXMLValidationEnabled(false),
                  m_bDiPoEarlyRoleSwitch(false),
                  m_enCertificateType(e8_CERTIFICATETYPE_DEVELOPER)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_rMaxVer.u32MajorVersion = 1;
   m_rMaxVer.u32MinorVersion = 1;
   //! Values initialized to default values (Used if xml policy not found)
}

/*************************************************************************
 ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
 *************************************************************************/
t_Bool spi_tclConnSettings::bXmlReadNode(xmlNodePtr poNode)
{
   t_S32 s32Value = 0;
   t_Bool bRetVal = false;
   t_String szNodeName;

   if (NULL != poNode)
   {
      szNodeName = (const char *) (poNode->name);
   } // if (NULL != poNode)

   if ("DIPO_SUPPORT" == szNodeName)
   {
      bRetVal = bGetAttribute("SUPPORTTYPE", poNode, s32Value);
      m_enDiPoEnabled = ((s32Value >= 0) && (s32Value <= e8USAGE_ENABLED))?
                             (tenEnabledInfo) s32Value: e8USAGE_ENABLED;
   } // if ("DIPO_SUPPORT" == szNodeName)

   else if ("MIRRORLINK_SUPPORT" == szNodeName)
   {
      bRetVal = bGetAttribute("SUPPORTTYPE", poNode, s32Value);
      m_enMLEnabled = ((s32Value >= 0) && (s32Value <= e8USAGE_ENABLED))?
                          (tenEnabledInfo) s32Value: e8USAGE_ENABLED;
   } // if ("MIRRORLINK_SUPPORT" == szNodeName)

   else if ("SELECTION_DELAY_ON_STARTUP" == szNodeName)
   {
      bRetVal = bGetAttribute("DELAYTIME_IN_SEC", poNode, s32Value);
      m_u32DelayForSelection = (s32Value >= 0) ? s32Value : 0;
   } //if ("SELECTION_DELAY_ON_STARTUP" == szNodeName)

   else if ("MAXIMUM_ML_VERSION_SUPPORTED" == szNodeName)
   {
      bRetVal = bGetAttribute("MAJOR", poNode, s32Value);
      m_rMaxVer.u32MajorVersion = (s32Value >= 0) ? s32Value : 0;
      bRetVal = bGetAttribute("MINOR", poNode, s32Value);
      m_rMaxVer.u32MinorVersion = (s32Value >= 0) ? s32Value : 0;
   } //if ("MAXIMUM_ML_VERSION_SUPPORTED" == szNodeName)

   else if ("DEVICE_SELECTION_MODE" == szNodeName)
   {
     //! Read the device selection mode from datapool. if the value is unknown then read it from policy.xml (virgin start)
     Datapool oDatapool;
     tenDeviceSelectionMode enStoredSelectionMode;
     oDatapool.bReadSelectionMode(enStoredSelectionMode);
     if(e16DEVICESEL_UNKNOWN == enStoredSelectionMode)
     {
        bRetVal = bGetAttribute("SELECTION_MODE", poNode, s32Value);
        m_enSelMode = ((s32Value >= 0) && (s32Value <= e16DEVICESEL_SEMI_AUTOMATIC))?
                                  (tenDeviceSelectionMode) s32Value: e16DEVICESEL_AUTOMATIC;
     }
     else
     {
        bRetVal = true;
        m_enSelMode = enStoredSelectionMode;
     }
   } //if ("DEVICE_SELECTION_MODE" == szNodeName)

   else if ("DEVICE_AUTH" == szNodeName)
   {
      bRetVal = bGetAttribute("DEVICE_AUTH_ENABLE", poNode, s32Value);
      m_enDeviceAuthEnableInfo = ((s32Value >= 0) && (s32Value <= e8USAGE_ENABLED))?
                                  (tenEnabledInfo) s32Value: e8USAGE_DISABLED;
   } // if ("PREFERRED_CONNECTION_TYPE" == szNodeName)

   else if ("PREFERRED_DEVICE_TYPE" == szNodeName)
   {
      bRetVal = bGetAttribute("DEVICE_TYPE", poNode, s32Value);
      tenDeviceCategory enTechnologyPref = ((s32Value >= 0) && (s32Value <= e8DEV_TYPE_ANDROIDAUTO)) ?
                                  (tenDeviceCategory) s32Value: e8DEV_TYPE_DIPO;
      //! If the preferred technology setting is static, read it from xml 
     if(e8DEV_TYPE_UNKNOWN != enTechnologyPref)
     {
      Datapool oDatapool;
      oDatapool.bWriteTechnologyPreference(enTechnologyPref);
     }
   } //if ("PREFERRED_DEVICE_TYPE" == szNodeName)

   else if ("DAP_PREFERENCE" == szNodeName)
   {
      bRetVal = bGetAttribute("DAP_PREF", poNode, s32Value);
      m_enDAPPref = ((s32Value >= 0) && (s32Value <= e8DAPPREF_ON_REQUEST))?
                        (tenDAPPreference) s32Value: e8DAPPREF_ON_DEVICECONNECTION;
   } //if ("DAP_PREFERENCE" == szNodeName)

   else if ("ML10_SUPPORT" == szNodeName)
   {
      bRetVal = bGetAttribute("SUPPORTTYPE", poNode, s32Value);
      m_enML10Support = ((s32Value >= 0) && (s32Value <= e8ML10_NOT_SUPPORTED))?
                       (tenML10Support) s32Value : e8ML10_DAPCERTIFIED;
   } //if ("ML10_SUPPORT" == szNodeName)

   else if ("CERTIFICATES" == szNodeName)
   {
      bRetVal = bGetAttribute("CERTIFICATE_TYPE", poNode, s32Value);
      m_enCertificateType = ((s32Value >= 0) && (s32Value<= e8_CERTIFICATETYPE_FFS)) ?
                                    (tenCertificateType) s32Value : e8_CERTIFICATETYPE_DEVELOPER;
   } //if ("MULTISESSION_SUPPORT" == szNodeName)

   else if ("PERSISTENT_STORAGE" == szNodeName)
   {
      bRetVal = bGetAttribute("STORAGE_PATH", poNode, m_szPerStoragePath);
   } //if ("PERSISTENT_STORAGE" == szNodeName)

   else if ("DEVICE_HISTORY_SIZE" == szNodeName)
   {
      bRetVal = bGetAttribute("MAXIMUM_SIZE", poNode, s32Value);
      m_u32DevHistorySize = (s32Value >= 0) ? s32Value : scou8DefaultHistorySize;
   } //if ("PERSISTENT_STORAGE" == szNodeName)
   else if("ENABLE_XML_VALIDATION" == szNodeName)
   {
      bRetVal = bGetAttribute("BOOL",poNode,m_bXMLValidationEnabled);
   }//else if("ENABLE_XML_VALIDATION" == szNodeName)
   else if("DIPO_EARLY_ROLESWITCH" == szNodeName)
   {
      bRetVal = bGetAttribute("BOOL",poNode,m_bDiPoEarlyRoleSwitch);
   }//else if("ENABLE_XML_VALIDATION" == szNodeName)

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConnSettings::vDisplayConnSettings
 ***************************************************************************/
t_Void spi_tclConnSettings::vDisplayConnSettings()
{
   ETG_TRACE_USR2(("/****************Connection Settings ******************/\n"));
   ETG_TRACE_USR2((" DiPo Support Type = %d",m_enDiPoEnabled));
   ETG_TRACE_USR2((" Mirrorlink Support = %d",m_enMLEnabled));
   ETG_TRACE_USR2((" Selection delay on startup = %d",m_u32DelayForSelection));
   ETG_TRACE_USR2((" Max ML Version Support Major = %d Minor = %d",
            m_rMaxVer.u32MajorVersion, m_rMaxVer.u32MinorVersion ));
   ETG_TRACE_USR2((" Device Selection Mode = %d",m_enSelMode));
   ETG_TRACE_USR2((" Device Authorization required = %d",m_enDeviceAuthEnableInfo));
   ETG_TRACE_USR2((" DAP Preference = %d ",m_enDAPPref));
   ETG_TRACE_USR2((" Mirrorlink 1.0 support = %d",m_enML10Support));
   ETG_TRACE_USR2((" Persistent Storage Path  = %s",m_szPerStoragePath.c_str()));
   ETG_TRACE_USR2((" Device History Size Limit= %d ",m_u32DevHistorySize));
   ETG_TRACE_USR2((" XML Validation Enabled= %d", ETG_ENUM(BOOL,m_bXMLValidationEnabled) ));
   ETG_TRACE_USR2((" Early role switch required= %d \n", ETG_ENUM(BOOL,m_bDiPoEarlyRoleSwitch) ));
   ETG_TRACE_USR2((" Certificates type = %d \n", ETG_ENUM(CERTIFICATE_TYPE,m_enCertificateType) ));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclConnSettings::vReadConfigurationData
 ***************************************************************************/
t_Void spi_tclConnSettings::vReadConfigurationData()
{
   ETG_TRACE_USR2(("spi_tclConnSettings::vReadConfigurationData()\n"));

   spi_tclConfigReader *poConfigReader= spi_tclConfigReader::getInstance();
   if(NULL != poConfigReader)
   {
      m_rHeadUnitInfo.enDrivePosition = poConfigReader->enGetDriveSideInfo();
      trVehicleInfo rfrVehicleInfo;
      poConfigReader->vGetVehicleInfoDataAAP(rfrVehicleInfo);
      m_rHeadUnitInfo.szVehicleManufacturer = rfrVehicleInfo.szManufacturer;
      m_rHeadUnitInfo.szVehicleModelName = rfrVehicleInfo.szModel;
      m_rHeadUnitInfo.szModelYear = poConfigReader->szGetModelYear();
      m_rHeadUnitInfo.szSoftwareVersion = poConfigReader->szGetSoftwareVersion();
      m_rHeadUnitInfo.szHUModelName =  poConfigReader->szGetHeadUnitModelName();
      m_rHeadUnitInfo.szHUManufacturer =  poConfigReader->szGetHeadUnitManufacturerName();
      m_rHeadUnitInfo.szVehicleID=poConfigReader->szGetVehicleId();
   }

   ETG_TRACE_USR2((" Drive position  = %d",ETG_ENUM(DRIVE_SIDE_TYPE, m_rHeadUnitInfo.enDrivePosition)));
   ETG_TRACE_USR2((" Head unit Manufacturer name  = %s",m_rHeadUnitInfo.szHUManufacturer.c_str()));
   ETG_TRACE_USR2((" Head unit Model name  = %s",m_rHeadUnitInfo.szHUModelName.c_str()));
   ETG_TRACE_USR2((" Serial number  = %s", m_rHeadUnitInfo.szVehicleID.c_str()));
   ETG_TRACE_USR2((" Software Version  = %s", m_rHeadUnitInfo.szSoftwareVersion.c_str()));
   ETG_TRACE_USR2((" Vehicle Manufacturer  = %s", m_rHeadUnitInfo.szVehicleManufacturer.c_str()));
   ETG_TRACE_USR2((" Vehicle Model  = %s", m_rHeadUnitInfo.szVehicleModelName.c_str()));

}

