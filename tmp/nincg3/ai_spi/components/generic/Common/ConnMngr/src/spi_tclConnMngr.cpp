/*!
 *******************************************************************************
 * \file             spi_tclConnMngr.cpp
 * \brief            Core implementation for Connection Management
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Core implementation for Connection Management
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 13.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
 31.07.2014 |  Ramya Murthy (RBEI/ECP2)    | SPI feature configuration via LoadSettings()
 10.12.2014 | Shihabudheen P M             | Changed for blocking device usage 
                                             preference updates during 
                                             select/deselect is in progress. 
 05.11.2014 |  Ramya Murthy (RBEI/ECP2)    | Added callback for Application metadata.
 06.05.2015  |Tejaswini HB                 |Lint Fix
 12.05.2015 |  Shiva Kumar G (RBEI/ECP2)   | Changes to continue with Craplay initialization,
                                              Even if the Mirrorlink initialization is failed.
 25.01.2016 | Rachana L Achar              | Logiscope improvements

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "sstream"
#include "FileHandler.h"
#include "spi_tclConnection.h"
#include "spi_tclDeviceListHandler.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
#include "spi_tclMLConnection.h"
#endif
#include "spi_tclDiPoConnection.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
#include "spi_tclAautoConnection.h"
#endif
#include "spi_tclConnMngrResp.h"
#include "spi_tclConnMngr.h"
#include "spi_tclMediator.h"
#include "spi_tclConnSettings.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclConnMngr.cpp.trc.h"
#endif
#endif

static const t_U32 scou32InvalidDeviceHandle = 0;

using namespace spi::io;


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::spi_tclConnMngr
 ***************************************************************************/
spi_tclConnMngr::spi_tclConnMngr(spi_tclConnMngrResp *poRespInterface) :
        		  m_poDeviceListHandler(NULL),
              m_poConnMngrResp(poRespInterface),
              m_bIsDevSelectorBusy(false)
{
   ETG_TRACE_USR1((" spi_tclConnMngr::spi_tclConnMngr() entered \n"));

   for (t_U8 u8Index = 0; u8Index < e8_CONN_INDEX_SIZE; u8Index++)
   {
      m_bSPIState[u8Index] = false;
      m_poConnHandlers[u8Index] = NULL;
   }
   SPI_NORMAL_ASSERT(NULL == m_poConnMngrResp);
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::~spi_tclConnMngr
 ***************************************************************************/
spi_tclConnMngr::~spi_tclConnMngr()
{
   ETG_TRACE_USR1((" spi_tclConnMngr::~spi_tclConnMngr()  entered \n"));

   m_poConnMngrResp = NULL;
   m_poDeviceListHandler = NULL;
   m_bIsDevSelectorBusy  = false;

   for (t_U8 u8Index = 0; u8Index < e8_CONN_INDEX_SIZE; u8Index++)
   {
      m_bSPIState[u8Index] = false;
      m_poConnHandlers[u8Index] = NULL;
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclConnMngr::bInitialize
 ***************************************************************************/
t_Bool spi_tclConnMngr::bInitialize()
{
   t_Bool bRetConnInit = true;

   //! Create Device List handler
   m_poDeviceListHandler = new spi_tclDeviceListHandler;
   SPI_NORMAL_ASSERT(NULL == m_poDeviceListHandler);

#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
   //! Create Mirrorlink connection
   m_poConnHandlers[e8_CONN_ML_INDEX] = new spi_tclMLConnection;
   SPI_NORMAL_ASSERT(NULL == m_poConnHandlers[e8_CONN_ML_INDEX]);
#endif

   //! Create DiPo connection
   m_poConnHandlers[e8_CONN_DIPO_INDEX] = new spi_tclDiPoConnection;
   SPI_NORMAL_ASSERT(NULL == m_poConnHandlers[e8_CONN_DIPO_INDEX]);

   //! Create AAP connection
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO   
   m_poConnHandlers[e8_CONN_AAUTO_INDEX] = new spi_tclAautoConnection;
   SPI_NORMAL_ASSERT(NULL == m_poConnHandlers[e8_CONN_AAUTO_INDEX]);
#endif

   bRetConnInit = (NULL != m_poDeviceListHandler);

   vRegisterCallbacks();

   for (t_U8 u8Index = 0; u8Index < e8_CONN_INDEX_SIZE; u8Index++)
   {
      //! Set the bInit to false, if all the SPI technologies cannot be initialized
      //! else return true if one of the SPI technologies can be initialized
      if (NULL != m_poConnHandlers[u8Index])
      {
         bRetConnInit = (m_poConnHandlers[u8Index]->bInitializeConnection()) || bRetConnInit;
      }
   } // for(t_U32 u32Index = 0; u32Index < e8_CONN_INDEX_SIZE; u32Index++)

   ETG_TRACE_USR4(("spi_tclConnMngr::bInitialize bRetConnInit= %d \n",
         ETG_ENUM(BOOL,bRetConnInit)));
   return bRetConnInit;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclConnMngr::bUnInitialize
 ***************************************************************************/
t_Bool spi_tclConnMngr::bUnInitialize()
{
   ETG_TRACE_USR1((" spi_tclConnMngr::bUnInitialize() entered \n"));

   for (t_U8 u8Index = 0; u8Index < e8_CONN_INDEX_SIZE; u8Index++)
   {
      if (NULL != m_poConnHandlers[u8Index])
      {
         m_poConnHandlers[u8Index]->vUnInitializeConnection();
      } // if(NULL != m_poConnHandlers[e8_CONN_ML_INDEX])

      RELEASE_MEM(m_poConnHandlers[u8Index]);

   } // for(t_U32 u32Index = 0; u32Index < e8_CONN_INDEX_SIZE; u32Index++)

   RELEASE_MEM(m_poDeviceListHandler);

   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConnMngr::vLoadSettings(const trSpiFeatureSupport&...)
***************************************************************************/
t_Void spi_tclConnMngr::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1((" spi_tclConnMngr::vLoadSettings() entered \n"));

   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();

   tenEnabledInfo enProjEnabledInfo = e8USAGE_ENABLED;
   if (NULL != poConnSettings)
   {
      poConnSettings->vIntializeConnSettings();

      trHeadUnitInfo rfrHeadUnitInfo;
      poConnSettings->vGetHeadUnitInfo(rfrHeadUnitInfo);
      //! Get the default settings for SPI Tecnology setting
      poConnSettings->vGetDefaultProjUsageSettings(enProjEnabledInfo);

      tenCertificateType enCertificateType = poConnSettings->enGetCertificateType();
      //! TODO extend for other technologies if needed
      if (NULL != m_poConnHandlers[e8_CONN_AAUTO_INDEX])
      {
         m_poConnHandlers[e8_CONN_AAUTO_INDEX]->vOnLoadSettings(rfrHeadUnitInfo, enCertificateType);
      } // if(NULL != m_poConnHandlers[e8_CONN_AAUTO_INDEX])
   } //if (NULL != poConnSettings)

   if (NULL != m_poDeviceListHandler)
   {
      t_Bool bRetDevListInit = m_poDeviceListHandler->bRestoreDeviceList();
      tenEnabledInfo enCurrentProjSetting;
      //! Store the default setting to datapool on the first startup after virgin flash 
      m_poDeviceListHandler->bGetDevProjUsage(e8DEV_TYPE_DIPO, enCurrentProjSetting);
      if(e8USAGE_UNKNOWN == enCurrentProjSetting)
      {
         m_poDeviceListHandler->bSetDevProjUsage(e8DEV_TYPE_DIPO, enProjEnabledInfo);
      }

      m_poDeviceListHandler->bGetDevProjUsage(e8DEV_TYPE_ANDROIDAUTO, enCurrentProjSetting);
      if(e8USAGE_UNKNOWN == enCurrentProjSetting)
      {
         m_poDeviceListHandler->bSetDevProjUsage(e8DEV_TYPE_ANDROIDAUTO, enProjEnabledInfo);
      }

      m_poDeviceListHandler->bGetDevProjUsage(e8DEV_TYPE_MIRRORLINK, enCurrentProjSetting);
      if(e8USAGE_UNKNOWN == enCurrentProjSetting)
      {
         m_poDeviceListHandler->bSetDevProjUsage(e8DEV_TYPE_MIRRORLINK, enProjEnabledInfo);
      }

      ETG_TRACE_USR4(("spi_tclConnMngr::vLoadSettings bRetDevListInit = %d \n",
            bRetDevListInit));
   } //if (NULL != m_poDeviceListHandler)

   //! Start device detection for  Mirrorlink,DiPo .. connections.
   if ((true == rfcrSpiFeatureSupp.bMirrorLinkSupported()) && (NULL != m_poConnHandlers[e8_CONN_ML_INDEX]))
   {
      m_poConnHandlers[e8_CONN_ML_INDEX]->bStartDeviceDetection();
   }
   if ((true == rfcrSpiFeatureSupp.bDipoSupported()) && (NULL != m_poConnHandlers[e8_CONN_DIPO_INDEX]))
   {
      m_poConnHandlers[e8_CONN_DIPO_INDEX]->bStartDeviceDetection();
   }
   if ((true == rfcrSpiFeatureSupp.bAndroidAutoSupported()) && (NULL != m_poConnHandlers[e8_CONN_AAUTO_INDEX]))
   {
      m_poConnHandlers[e8_CONN_AAUTO_INDEX]->bStartDeviceDetection();
   }
   //! Store the settings
   m_rSPIFeatureSupport = rfcrSpiFeatureSupp;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConnMngr::vSaveSettings()
***************************************************************************/
t_Void spi_tclConnMngr::vSaveSettings()
{
   ETG_TRACE_USR1((" spi_tclConnMngr::vSaveSettings() entered \n"));
   //! Currently device list need not be saved again on save settings as
   //! devices will be dynamically added to list on selection
   if (NULL != m_poConnHandlers[e8_CONN_DIPO_INDEX])
   {
      m_poConnHandlers[e8_CONN_DIPO_INDEX]->vOnSaveSettings();
   } // if(NULL != m_poConnHandlers[e8_CONN_ML_INDEX])
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vClearDeviceHistory
 ***************************************************************************/
t_Void spi_tclConnMngr::vClearDeviceHistory()const
{
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   t_String szStoragePath;
   if(NULL != poConnSettings)
   {
      poConnSettings->vGetPersistentStoragePath(szStoragePath);
   }
   std::stringstream ssHistoryFile;
   ssHistoryFile << szStoragePath.c_str()<<"DeviceHistory.db";
   FileHandler oFilehandler(ssHistoryFile.str().c_str(), SPI_EN_REMOVE);
   if(NULL!= m_poDeviceListHandler)
   {
      m_poDeviceListHandler->vClearPrivateData();
   }

   //! Send Device list change
   if (NULL != m_poConnMngrResp)
   {
      m_poConnMngrResp->vPostDeviceStatusInfo(scou32InvalidDeviceHandle,
            e8UNKNOWN_CONNECTION, e8DEVICE_REMOVED);
   } //if (NULL != m_poConnMngrResp)

   ETG_TRACE_USR1(("spi_tclConnMngr::vClearDeviceHistory(): Deleting Devicehistory database at ssHistoryFile = %s \n",
            ssHistoryFile.str().c_str()));
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bIsDeviceValid
 ***************************************************************************/
t_Bool spi_tclConnMngr::bIsDeviceValid(const t_U32 cou32DeviceHandle)
{
   t_Bool bValidDevice = false;
   if(NULL != m_poDeviceListHandler)
   {
      bValidDevice = m_poDeviceListHandler->bIsDeviceValid(cou32DeviceHandle);
   }
   return bValidDevice;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bGetDeviceInfo
 ***************************************************************************/
t_Bool spi_tclConnMngr::bGetDeviceInfo(const t_U32 cou32DeviceHandle,
         trDeviceInfo &rfrDeviceInfo)
{
   t_Bool bDevFound = false;
   if (NULL != m_poDeviceListHandler)
   {
      bDevFound = m_poDeviceListHandler->bGetDeviceInfo(cou32DeviceHandle,
               rfrDeviceInfo);
   } //if (NULL != m_poDeviceListHandler);
   return bDevFound;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vGetDeviceList
 ***************************************************************************/
t_Void spi_tclConnMngr::vGetDeviceList(
         std::vector<trDeviceInfo>& rfvecDeviceInfoList,
         t_Bool bCertifiedOnly, t_Bool bConnectedOnly)
{
   rfvecDeviceInfoList.clear();

   if (NULL != m_poDeviceListHandler)
   {
      m_poDeviceListHandler->vGetDeviceList(rfvecDeviceInfoList,
               bCertifiedOnly,
               bConnectedOnly);
   } //if (NULL != m_poDeviceListHandler)
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vGetEntireDeviceList
 ***************************************************************************/
t_Void spi_tclConnMngr::vGetEntireDeviceList(
         std::map<t_U32, trEntireDeviceInfo> &rfrmapDeviceInfoList)
{
   if(NULL != m_poDeviceListHandler)
   {
      m_poDeviceListHandler->vGetEntireDeviceList(rfrmapDeviceInfoList);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vOnSelectDevice
 ***************************************************************************/
t_Void spi_tclConnMngr::vOnSelectDevice(const t_U32 cou32DeviceHandle,
         tenDeviceConnectionType enDevConnType,
         tenDeviceConnectionReq enDevSelReq, tenEnabledInfo enDAPUsage,
         tenEnabledInfo enCDBUsage, t_Bool bIsHMITrigger,
         const trUserContext corUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclConnMngr:: vOnSelectDevice : cou32DeviceHandle =%d enDevSelReq = %d ",
    cou32DeviceHandle, ETG_ENUM(CONNECTION_REQ,enDevSelReq)));
   ETG_TRACE_USR1(("spi_tclConnMngr:: vOnSelectDevice: enDAPUsage = %d  enCDBUsage = %d bIsHMITrigger = %d", 
    enDAPUsage, enCDBUsage, ETG_ENUM(BOOL,bIsHMITrigger)));

   if (NULL != m_poDeviceListHandler)
   {
      tenDeviceCategory enDevCat = m_poDeviceListHandler->enGetDeviceCategory(cou32DeviceHandle);
      if (e8DEV_TYPE_UNKNOWN != enDevCat)
      {
         tenConnPointersIndex enIndex = enGetConnectionIndex(enDevCat);

         if (true == bIsHMITrigger)
         {
            //Set user preference
			(e8DEV_TYPE_ANDROIDAUTO == enDevCat) ? 
             m_poDeviceListHandler->vSetUserPreference(cou32DeviceHandle, e8ANDROID_AUTO_PREFERRED) :
             m_poDeviceListHandler->vSetUserPreference(cou32DeviceHandle, e8PREFERENCE_NOTKNOWN);
         }//if (true == bIsHMITrigger)
		 
         if (NULL != m_poConnHandlers[enIndex])
         {
            m_poConnHandlers[enIndex]->vOnSelectDevice(cou32DeviceHandle,
               enDevConnType, enDevSelReq, enDAPUsage, enCDBUsage,
               bIsHMITrigger, corUsrCntxt);
         }
      }//if (e8DEV_TYPE_UNKNOWN != enDevCat)

      //! on device deselection, result is posted directly to HMI because
      //! device disconnection status might go before the inactive status for
      //! carplay when reverse role switch is requested.
      if(e8DEVCONNREQ_DESELECT == enDevSelReq)
      {
          //! Set Currently selected device to default value if select device
          //! request is received for device deselection
          m_poDeviceListHandler->vSetDeviceSelection(cou32DeviceHandle, false);
          //! Store the user preference. Will be used during successive selections
          if (true == bIsHMITrigger)
          {
             (e8DEV_TYPE_ANDROIDAUTO == enDevCat) ?
              m_poDeviceListHandler->vSetUserPreference(cou32DeviceHandle, e8ANDROID_AUTO_NOTPREFERRED) :
              m_poDeviceListHandler->vSetUserPreference(cou32DeviceHandle, e8PROJECTION_NOT_PREFERRED);
          } //if (true == bIsHMITrigger)

		  //! Send Device list change
          if (NULL != m_poConnMngrResp)
          {
             m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle, e8USB_CONNECTED, e8DEVICE_CHANGED);
          } //if (NULL != m_poConnMngrResp)
      }
   } //if (NULL != m_poDeviceListHandler)
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::enGetConnectionIndex
 ***************************************************************************/
tenConnPointersIndex spi_tclConnMngr::enGetConnectionIndex(
       tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1((" spi_tclConnMngr::enGetConnectionIndex entered"));
   tenConnPointersIndex enIndex = e8_CONN_ML_INDEX;
   //Assign connection pointer index based on the device technology
   switch(enDevCat)
   {
      case e8DEV_TYPE_DIPO: enIndex = e8_CONN_DIPO_INDEX; break;
      case e8DEV_TYPE_ANDROIDAUTO: enIndex = e8_CONN_AAUTO_INDEX; break;
	  case e8DEV_TYPE_MIRRORLINK:
      default: enIndex = e8_CONN_ML_INDEX;
   }
   return enIndex;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConnMngr::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclConnMngr::vOnSelectDeviceResult(const t_U32 cou32DeviceHandle,
   const tenDeviceConnectionReq enDevSelReq,
   const tenResponseCode coenRespCode,
   tenDeviceCategory enDevCat, t_Bool bIsHMITrigger)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vOnSelectDeviceResult : cou32DeviceHandle =%d "
   "enDevSelReq = %d coenRespCode = %d",
   cou32DeviceHandle, ETG_ENUM(CONNECTION_REQ,enDevSelReq),
   ETG_ENUM(RESPONSE_CODE,coenRespCode)));

   tenDeviceConnectionStatus enConnStatus = e8DEV_NOT_CONNECTED;
   if (NULL != m_poDeviceListHandler)
   {
      enConnStatus = m_poDeviceListHandler->enGetDeviceConnStatus(cou32DeviceHandle);

      if ((e8DEVCONNREQ_SELECT == enDevSelReq) && (e8SUCCESS == coenRespCode))
      {
         //! Add selected device to history
         //! Store the currently selected device
         m_poDeviceListHandler->vSetDeviceSelection(cou32DeviceHandle, true);
         m_poDeviceListHandler->vAddDeviceToHistory(cou32DeviceHandle);

         //! Send Device list change
         if (NULL != m_poConnMngrResp)
         {
            m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle, e8USB_CONNECTED, e8DEVICE_CHANGED);
         } //if (NULL != m_poConnMngrResp)
      } //if (NULL != m_poDeviceListHandler)

      //! Post the device selection result to subcomponents. TODO: Post for Mirrorlink too
      if ((e8DEV_TYPE_DIPO == enDevCat) && (NULL != m_poConnHandlers[e8_CONN_DIPO_INDEX]))
      {
         m_poConnHandlers[e8_CONN_DIPO_INDEX]->vOnSelectDeviceResult(cou32DeviceHandle, enDevSelReq, coenRespCode,
               enDevCat, bIsHMITrigger);
      }
      else if ((e8DEV_TYPE_ANDROIDAUTO == enDevCat) && (NULL != m_poConnHandlers[e8_CONN_AAUTO_INDEX]))
      {
         m_poConnHandlers[e8_CONN_AAUTO_INDEX]->vOnSelectDeviceResult(cou32DeviceHandle, enDevSelReq, coenRespCode,
               enDevCat, bIsHMITrigger);
      }

      //! Set the flag if Device selection fails so that further auto selections are not triggered
      if (e8DEVCONNREQ_SELECT == enDevSelReq)
      {
         t_Bool bIsError = (e8FAILURE == coenRespCode);
         vSetSelectError(cou32DeviceHandle, bIsError);
      }
      //! Reset the device connection if device is still connected.
      else if ((e8DEVCONNREQ_DESELECT == enDevSelReq) && (e8DEV_CONNECTED == enConnStatus))
      {
         //! TODO check if reset is required for Mirrorlink device
         //! Reset is currently required for Android Auto as the device doesn't switches to default USB profile
         //! on receiving ByeBye message
         if (e8DEV_TYPE_ANDROIDAUTO == enDevCat)
         {
            bRequestDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_UNKNOWN);
         }
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSetUserDeselectionFlag
 ***************************************************************************/
t_Void spi_tclConnMngr::vSetUserDeselectionFlag(const t_U32 cou32DeviceHandle, t_Bool bState)
{
   if (NULL != m_poDeviceListHandler)
   {
      m_poDeviceListHandler->vSetUserDeselectionFlag(cou32DeviceHandle, bState);
   }
}
/***************************************************************************
** FUNCTION:  spi_tclConnMngr::bGetUserDeselectionFlag
***************************************************************************/
t_Bool spi_tclConnMngr::bGetUserDeselectionFlag(const t_U32 cou32DeviceHandle)
{
   t_Bool bUserDeselection =  false;
   if (NULL != m_poDeviceListHandler)
   {
      bUserDeselection = m_poDeviceListHandler->bGetUserDeselectionFlag(cou32DeviceHandle);
   }
   return bUserDeselection;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bSetDevProjUsage
 ***************************************************************************/
t_Bool spi_tclConnMngr::bSetDevProjUsage(tenDeviceCategory enSPIType, tenEnabledInfo enSPIState)
{
   ETG_TRACE_USR1(("spi_tclConnMngr:: bSetDevProjUsage: Changing the overall setting for %d to State = %d \n",
         ETG_ENUM(DEVICE_CATEGORY,enSPIType), ETG_ENUM(ENABLED_INFO, enSPIState)));

   //! To be used in future (If Mirrorlink/DiPo on/off fails)
   t_Bool bSetState = false;

   //Initialize or uninitialize connections base on the request
   if (e8DEV_TYPE_MIRRORLINK == enSPIType)
   {
      if (NULL != m_poConnHandlers[e8_CONN_ML_INDEX])
      {
         bSetState = m_poConnHandlers[e8_CONN_ML_INDEX]->bSetDevProjUsage(enSPIState);
      }
   } //if (e8DEV_TYPE_MIRRORLINK == enSPIType)
   else if (e8DEV_TYPE_DIPO == enSPIType)
   {
      if (NULL != m_poConnHandlers[e8_CONN_DIPO_INDEX])
      {
         bSetState = m_poConnHandlers[e8_CONN_DIPO_INDEX]->bSetDevProjUsage(enSPIState);
      }
   } //if(e8DEV_TYPE_DIPO == enSPIType)
   else if (e8DEV_TYPE_ANDROIDAUTO == enSPIType)
   {
      if (NULL != m_poConnHandlers[e8_CONN_AAUTO_INDEX])
      {
         bSetState = m_poConnHandlers[e8_CONN_AAUTO_INDEX]->bSetDevProjUsage(enSPIState);
      }
   } //if(e8DEV_TYPE_ANDROIDAUTO == enSPIType)

   if(NULL != m_poDeviceListHandler)
   {
      bSetState = (m_poDeviceListHandler->bSetDevProjUsage(enSPIType, enSPIState)) && (bSetState);
   }
   //! Apply selection strategy when Mirrorlink/Carplay setting is turned ON
   if(e8USAGE_ENABLED == enSPIState)
   {
      spi_tclMediator *poMediator = spi_tclMediator::getInstance();
      if(NULL != poMediator)
      {
         poMediator->vApplySelectionStrategy();
      }
   }

   return bSetState;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bGetDevProjUsage
 ***************************************************************************/
t_Bool spi_tclConnMngr::bGetDevProjUsage(tenDeviceCategory enSPIType,
         tenEnabledInfo &rfenEnabledInfo) const
{
   t_Bool bRetVal = false;

   //! Feature is disabled if the project calibration does not support specific SPI technology
   if((false == m_rSPIFeatureSupport.bMirrorLinkSupported()) && (e8DEV_TYPE_MIRRORLINK == enSPIType))
   {
      rfenEnabledInfo = e8USAGE_DISABLED;
   }
   else if((false == m_rSPIFeatureSupport.bAndroidAutoSupported()) && (e8DEV_TYPE_ANDROIDAUTO == enSPIType))
   {
      rfenEnabledInfo = e8USAGE_DISABLED;
   }
   else if((false == m_rSPIFeatureSupport.bDipoSupported()) && (e8DEV_TYPE_DIPO == enSPIType))
   {
      rfenEnabledInfo = e8USAGE_DISABLED;
   }
   else if(NULL != m_poDeviceListHandler)
   {
      bRetVal = m_poDeviceListHandler->bGetDevProjUsage(enSPIType, rfenEnabledInfo);
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::enGetDeviceCategory
 ***************************************************************************/
tenDeviceCategory spi_tclConnMngr::enGetDeviceCategory(
         const t_U32 cou32DeviceHandle) const
{
   tenDeviceCategory enDevCat = e8DEV_TYPE_UNKNOWN;
   if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
   {
      enDevCat = m_poDeviceListHandler->enGetDeviceCategory(cou32DeviceHandle);
   } //if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
   return enDevCat;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSetDeviceCategory
 ***************************************************************************/
t_Void spi_tclConnMngr::vSetDeviceCategory(const t_U32 cou32DeviceHandle, tenDeviceCategory enDeviceCategory)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vSetDeviceCategory entered with Devicehandle = %d and category =%d", cou32DeviceHandle, enDeviceCategory));
   if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
   {
      m_poDeviceListHandler->vSetDeviceCategory(cou32DeviceHandle, enDeviceCategory);
   } //if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::enGetDeviceConnStatus
 ***************************************************************************/
tenDeviceConnectionStatus spi_tclConnMngr::enGetDeviceConnStatus(
         const t_U32 cou32DeviceHandle) const
{
   tenDeviceConnectionStatus enConnStatus = e8DEV_NOT_CONNECTED;
   if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
   {
      enConnStatus =
               m_poDeviceListHandler->enGetDeviceConnStatus(cou32DeviceHandle);
   } //if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))

   return enConnStatus;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::enGetDeviceConnType
 ***************************************************************************/
tenDeviceConnectionType spi_tclConnMngr::enGetDeviceConnType(
         const t_U32 cou32DeviceHandle) const
{
   tenDeviceConnectionType enConnType = e8UNKNOWN_CONNECTION;
   if (NULL != m_poDeviceListHandler)
   {
      enConnType =
               m_poDeviceListHandler->enGetDeviceConnType(cou32DeviceHandle);
   } //if(NULL != m_poDeviceListHandler)
   return enConnType;
}

/***************************************************************************
** FUNCTION:  spi_tclConnMngr::u32GetNoofConnectedDevices
***************************************************************************/
t_U32 spi_tclConnMngr::u32GetNoofConnectedDevices()
{
   t_U32 u32NoofDevices = 0;
   if (NULL != m_poDeviceListHandler)
   {
      u32NoofDevices= m_poDeviceListHandler->u32GetNoofConnectedDevices();
   } //if(NULL != m_poDeviceListHandler)
   return u32NoofDevices;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vGetBTAddress
 ***************************************************************************/
t_Void spi_tclConnMngr::vGetBTAddress(const t_U32 cou32DeviceHandle,
         t_String &rfszBTAddress) const
{
   if (NULL != m_poDeviceListHandler)
   {
      m_poDeviceListHandler->vGetBTAddress(cou32DeviceHandle, rfszBTAddress);
   } //if(NULL != m_poDeviceListHandler)
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vRemoveDevice
 ***************************************************************************/
t_Void spi_tclConnMngr::vRemoveDevice(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" spi_tclConnMngr::vRemoveDevice: Deleting DeviceHandle = 0x%x from the list \n",
            cou32DeviceHandle));
   if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
   {
      m_poDeviceListHandler->vRemoveDeviceFromList(cou32DeviceHandle);
      m_poDeviceListHandler->vRemoveDeviceFromHistory(cou32DeviceHandle);

      //! Send Device list change
      if (NULL != m_poConnMngrResp)
      {
         m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle,
                  e8USB_CONNECTED, e8DEVICE_REMOVED);
      } //if (NULL != m_poConnMngrResp)
   }//if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclConnMngr::vSetDeviceUsagePreference
 ***************************************************************************/
t_Void spi_tclConnMngr::vSetDeviceUsagePreference(const t_U32 cou32DeviceHandle,
         tenDeviceCategory enDeviceCategory, tenEnabledInfo enUsagePref,
         const trUserContext &rfrUsrCntxt)
{
   ETG_TRACE_USR1((" spi_tclConnMngr::vSetDeviceUsagePreference: cou32DeviceHandle =0x%x,"
   " enDeviceCategory =%d, enUsagePref=%d \n", cou32DeviceHandle,
   ETG_ENUM(DEVICE_CATEGORY,enDeviceCategory),  ETG_ENUM(ENABLED_INFO,enUsagePref)));

   tenErrorCode enErrorCode = e8NO_ERRORS;

   //! if cou32DeviceHandle = 0xFFFFFFFF, Overall setting for the specified
   //! service is changed
   if ((cou32MAX_DEVICEHANDLE == cou32DeviceHandle) && (NULL!= m_poDeviceListHandler))
   {
      if(true == m_bIsDevSelectorBusy)
      {
         enErrorCode = e8RESOURCE_BUSY;
      }
      else
      {
         t_U32 u32SelectedDevice = m_poDeviceListHandler->u32GetSelectedDevice();
         tenDeviceCategory enSelectedDeviceCategory = enGetDeviceCategory(u32SelectedDevice);
         //! check if dev projection usage was set to off during a session
         if ((0 != u32SelectedDevice) && (enUsagePref == e8USAGE_DISABLED) && (enDeviceCategory == enSelectedDeviceCategory))
         {
            //! Send Device deselection to SPI components if device projection
            //! setting is disabled during a mirrorlink/carplay session
            spi_tclMediator *poMediator = spi_tclMediator::getInstance();
            if (NULL != poMediator)
            {
               poMediator->vPostAutoDeviceSelection(u32SelectedDevice, e8DEVCONNREQ_DESELECT);
            } // if (NULL != poMediator)
         }
         bSetDevProjUsage(enDeviceCategory, enUsagePref);
      }
   }
   //! To set the device usage preference flag
   else
   {
      if(NULL != m_poDeviceListHandler)
      {
         enDeviceCategory = m_poDeviceListHandler->enGetDeviceCategory(cou32DeviceHandle);
         if ((0 == cou32DeviceHandle) || (false
               == m_poDeviceListHandler->bIsDeviceValid(cou32DeviceHandle)))
         {
            enErrorCode = e8INVALID_DEV_HANDLE;
         }
         else if((cou32DeviceHandle == m_poDeviceListHandler->u32GetSelectedDevice())
             || (true == m_bIsDevSelectorBusy))
         {
            enErrorCode = e8OPERATION_REJECTED;
         }
         else
         {
            m_poDeviceListHandler->vSetDeviceUsagePreference(cou32DeviceHandle,
                     enUsagePref);
            //! Send Device list change
            if (NULL != m_poConnMngrResp)
            {
               m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle,
                        e8USB_CONNECTED, e8DEVICE_CHANGED);
            } //  if (NULL != m_poConnMngrResp)
         } // if ((0 == cou32DeviceHandle) || ...
      } //if (NULL != m_poDeviceListHandler)
   } //if(cou32MAX_DEVICEHANDLE == cou32DeviceHandle)

   if (NULL != m_poConnMngrResp)
   {
      m_poConnMngrResp->vPostDeviceUsagePrefResult(cou32DeviceHandle,
            enErrorCode, enDeviceCategory, enUsagePref, rfrUsrCntxt);
   }//if (NULL != m_poConnMngrResp)
}

/***************************************************************************
 ** FUNCTION: t_Bool spi_tclConnMngr::bGetDeviceUsagePreference
 ***************************************************************************/
t_Bool spi_tclConnMngr::bGetDeviceUsagePreference(const t_U32 cou32DeviceHandle,
         tenDeviceCategory enDeviceCategory,
         tenEnabledInfo& rfenUsagePref) const
{
   t_Bool bRetVal = false;
   if (cou32MAX_DEVICEHANDLE == cou32DeviceHandle)
   {
      //! Currently no error is posted if fetching Device Projection Usage fails
      bRetVal = bGetDevProjUsage(enDeviceCategory, rfenUsagePref);
   }
   else
   {
      if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
      {
         bRetVal = true;
         m_poDeviceListHandler->vGetDeviceUsagePreference(cou32DeviceHandle,
                  rfenUsagePref);
      }// if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSetSelectError
 ***************************************************************************/
t_Void spi_tclConnMngr::vSetSelectError(const t_U32 cou32DeviceHandle, t_Bool bIsError)
{
   if (NULL != m_poDeviceListHandler)
   {
      tenDeviceCategory enDeviceCategory = enGetDeviceCategory(cou32DeviceHandle);
      if ((e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) && (true == bIsError))
      {
         m_poDeviceListHandler->vSetSPISupport(cou32DeviceHandle,
               e8DEV_TYPE_ANDROIDAUTO, e8SPI_NOTSUPPORTED);
      }

   //! Set the Error flag to prevent further automatic selections
      m_poDeviceListHandler->vSetSelectError(cou32DeviceHandle, bIsError);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bIsSelectError
 ***************************************************************************/
t_Bool spi_tclConnMngr::bIsSelectError(const t_U32 cou32DeviceHandle)
{
   t_Bool bIsError = false;
   if (NULL != m_poDeviceListHandler)
   {
      bIsError = m_poDeviceListHandler->bIsSelectError(cou32DeviceHandle);
   }
   return bIsError;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclConnMngr::bIsDiPoRoleSwitchRequired(tU32 ..)
***************************************************************************/
t_Bool spi_tclConnMngr::bIsDiPoRoleSwitchRequired(const t_U32 cou32DeviceID, const trUserContext corUsrCntxt)
{
   tenEnabledInfo enDeviceAuthEnabled = e8USAGE_DISABLED;
   tenEnabledInfo enProjectionStatus = e8USAGE_ENABLED;
   //! Is the projection status set to "user confirmation needed"?
   bGetDevProjUsage(e8DEV_TYPE_DIPO, enProjectionStatus);

   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();

   if((NULL != m_poDeviceListHandler) && (NULL != poConnSettings))
   {
      //! Check if Authorization is  Required
      enDeviceAuthEnabled = poConnSettings->enGetDeviceAuthEnableInfo();
      //! Is it a unknown device?
      t_Bool bDeviceValid =  m_poDeviceListHandler->bIsDeviceValid(cou32DeviceID);

      if(((e8USAGE_ENABLED == enDeviceAuthEnabled) && (e8USAGE_DISABLED != enProjectionStatus))
            && ((false== bDeviceValid) || (e8USAGE_CONF_REQD == enProjectionStatus)))
      {
         ETG_TRACE_USR1(("Device authorization is required. Waiting for user input. \n"));
         trUnauthDeviceInfo rUnauthDevInfo;
         rUnauthDevInfo.rAuthInfo.u32DeviceHandle = cou32DeviceID;
         rUnauthDevInfo.rAuthInfo.enDeviceType = e8_APPLE_DEVICE;
         rUnauthDevInfo.rAuthInfo.enUserAuthorizationStatus = e8_USER_AUTH_UNAUTHORIZED;
         rUnauthDevInfo.rUserContext = corUsrCntxt;
         m_poDeviceListHandler->vAddUnauthorizedDeviceToList(rUnauthDevInfo);
         //! Trigger Device authorization request
         //! Wait for confirmation from user before proceeding with selection
         if(NULL != m_poConnMngrResp)
         {
            std::vector<trDeviceAuthInfo> rvecDevAuthInfo;
            m_poDeviceListHandler->vGetUnauthorizedDevicesList(rvecDevAuthInfo);
            m_poConnMngrResp->vRequestDeviceAuthorization(rvecDevAuthInfo);
         }
      }
      else
      {
         ETG_TRACE_USR1(("Device authorization not required. Proceeding with Connection\n"));
         //! If device authorization is not required, proceed with connection
         vSendRoleSwitchResponse(cou32DeviceID, corUsrCntxt);
      }
   }

  // ETG_TRACE_USR1(("spi_tclConnMngr::bIsDiPoRoleSwitchRequired: Early role switch for Carplay Device = 0x%x needed? = %d\n",
  //          cou32DeviceID, ETG_ENUM(BOOL, bRetVal)));
   return true;
}
 

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSetDevSelectorBusyStatus
 ***************************************************************************/
t_Void spi_tclConnMngr::vSetDevSelectorBusyStatus(t_Bool bDevSelStatus)
{
    ETG_TRACE_USR1(("spi_tclConnMngr::vSetDevSelectorBusyStatus: bDevSelStatus = %d \n",
            ETG_ENUM( BOOL, bDevSelStatus)));
    m_bIsDevSelectorBusy = bDevSelStatus;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::enGetUserPreference
 ***************************************************************************/
tenUserPreference spi_tclConnMngr::enGetUserPreference(const t_U32 cou32DeviceHandle)
{
   tenUserPreference enUserPref = e8PREFERENCE_NOTKNOWN;
   if (NULL != m_poDeviceListHandler)
   {
      enUserPref = m_poDeviceListHandler->enGetUserPreference(cou32DeviceHandle);
   }
   return enUserPref;
}
/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSetUserPreference
 ***************************************************************************/
t_Void spi_tclConnMngr::vSetUserPreference(const t_U32 cou32DeviceHandle, tenUserPreference enUserPref)
{
   if (NULL != m_poDeviceListHandler)
   {
      m_poDeviceListHandler->vSetUserPreference(cou32DeviceHandle, enUserPref);
   }
}
/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bGetUSBConnectedFlag
 ***************************************************************************/
t_Bool spi_tclConnMngr::bGetUSBConnectedFlag(const t_U32 cou32DeviceHandle)
{
   t_Bool bRetVal = false;
   if (NULL != m_poDeviceListHandler)
   {
      bRetVal = m_poDeviceListHandler->bGetUSBConnectedFlag(cou32DeviceHandle);
   }
   return bRetVal;
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclConnMngr:: vSetTechnologyPreference()
***************************************************************************/
t_Void spi_tclConnMngr::vSetTechnologyPreference(const t_U32 cou32DeviceHandle,
      std::vector<tenDeviceCategory> vecTechnologyPreference) const
{
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   tenDeviceCategory enPreferredTech = e8DEV_TYPE_ANDROIDAUTO;
   if(0 != vecTechnologyPreference.size())
   {
      enPreferredTech = *(vecTechnologyPreference.begin());
   }

   if ((NULL != poConnSettings) && (cou32MAX_DEVICEHANDLE == cou32DeviceHandle))
   {
      ETG_TRACE_USR1(("spi_tclConnMngr::vSetTechnologyPreference : Setting preferred technology to %d , preference size = %d \n",
         ETG_ENUM(DEVICE_CATEGORY,enPreferredTech), vecTechnologyPreference.size()));
      poConnSettings->vSetTechnologyPreference(enPreferredTech);
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclConnMngr::vSetTechnologyPreference: Not supported! Currently only "
            "overall setting (Devicehanlde = 0xFFFFFFFF is supported)\n"));
   }
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclConnMngr:: vGetTechnologyPreference()
***************************************************************************/
t_Void spi_tclConnMngr::vGetTechnologyPreference(const t_U32 cou32DeviceHandle,
      std::vector<tenDeviceCategory> &rfrvecTechnologyPreference) const
{
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   tenDeviceCategory enPreferredTech = e8DEV_TYPE_ANDROIDAUTO;
   if ((NULL != poConnSettings) && (cou32MAX_DEVICEHANDLE == cou32DeviceHandle))
   {
      enPreferredTech = poConnSettings->enGetTechnologyPreference();
      rfrvecTechnologyPreference.push_back(enPreferredTech);
      ETG_TRACE_USR1(("spi_tclConnMngr::vGetTechnologyPreference :  preferred technology is %d \n",
         ETG_ENUM(DEVICE_CATEGORY,enPreferredTech)));
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclConnMngr::vGetTechnologyPreference: Not supported! Currently only"
            " overall setting (Devicehanlde = 0xFFFFFFFF is supported)\n"));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSetDeviceAuthorization
 ***************************************************************************/
t_Void spi_tclConnMngr::vSetDeviceAuthorization(const t_U32 cou32DeviceHandle,
      tenUserAuthorizationStatus enUserAuthStatus)
{
   ETG_TRACE_USR2(("spi_tclConnMngr::vSetDeviceAuthorization: DeviceHandle = 0x%x "
            "Authorization status = %d \n", cou32DeviceHandle,  ETG_ENUM(AUTHORIZATION_STATUS,enUserAuthStatus)));

   if(NULL != m_poDeviceListHandler)
   {
      trUnauthDeviceInfo rfrUnauthDevInfo;
      if (true == m_poDeviceListHandler->bGetUnauthorizedDeviceInfo(cou32DeviceHandle, rfrUnauthDevInfo))
      {
         //! Proceed with device connection if user has authorized the device
         if (e8_USER_AUTH_AUTHORIZED == enUserAuthStatus)
         {
            //! Proceed with connection if device type is android
            if (e8_ANDROID_DEVICE == rfrUnauthDevInfo.rAuthInfo.enDeviceType)
            {
               vProceedwithConnection(cou32DeviceHandle, rfrUnauthDevInfo.rDeviceInfo, e8DEV_TYPE_ANDROIDAUTO);
            }

            //! inform role switch required request to DiPo connection
            if ((NULL != m_poConnHandlers[e8_CONN_DIPO_INDEX]))
            {
               m_poConnHandlers[e8_CONN_DIPO_INDEX]->vSetRoleSwitchRequestedInfo(cou32DeviceHandle);
            }

            //! Proceed with role switch if device type is Apple
            //! Send the role switch required response to mediaplayer
            if (NULL != m_poConnMngrResp)
            {
               m_poConnMngrResp->vPostDipoRoleSwitchResponse(true, cou32DeviceHandle,  rfrUnauthDevInfo.rUserContext);
            }

         }
         else if ((e8_USER_AUTH_UNAUTHORIZED == enUserAuthStatus) && (e8_APPLE_DEVICE
               == rfrUnauthDevInfo.rAuthInfo.enDeviceType))
         {
            //! Send the role switch not required response to mediaplayer
            if (NULL != m_poConnMngrResp)
            {
               m_poConnMngrResp->vPostDipoRoleSwitchResponse(false, cou32DeviceHandle,  rfrUnauthDevInfo.rUserContext);
            }
         }
      }
      //! Remove the device from unauthorized list
      m_poDeviceListHandler->vRemoveUnauthorizedDevice(cou32DeviceHandle);
   }
}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vUpdateDAPStatusCb
 ***************************************************************************/
t_Void spi_tclConnMngr::vUpdateDAPStatusCb(const t_U32 cou32DeviceHandle,
         tenDAPStatus enDAPStatus)
{
   ETG_TRACE_USR2(("spi_tclConnMngr::vUpdateDAPStatusCb: cou32DeviceHandle = 0x%x "
            "tenDAPStatus = %d \n", cou32DeviceHandle,  ETG_ENUM(DAP_STATUS,enDAPStatus)));

   if ((NULL != m_poConnMngrResp) && (0 != cou32DeviceHandle))
   {
      tenDeviceConnectionType enDevConnType = e8UNKNOWN_CONNECTION;
      if (NULL != m_poDeviceListHandler)
      {
         enDevConnType =
                  m_poDeviceListHandler->enGetDeviceConnType(cou32DeviceHandle);
         if(e8DAP_IN_PROGRESS == enDAPStatus)
         {
            m_poDeviceListHandler->vSetDAPSupport(cou32DeviceHandle, true);
         }
         else if(e8DAP_SUCCESS == enDAPStatus)
         {
            ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"DAP successfully completed for Device-0x%x\n"
               , cou32DeviceHandle));

            m_poDeviceListHandler->vSetDeviceValidity(cou32DeviceHandle, true);
         }
         else if(e8DAP_FAILED == enDAPStatus)
         {
            ETG_TRACE_USR2_CLS((TR_CLASS_SMARTPHONEINT_CTS,"DAP failed for Device-0x%x and will not be shown to user\n"
               , cou32DeviceHandle));

            m_poDeviceListHandler->vSetDeviceValidity(cou32DeviceHandle, false);
         }
      } // if (NULL != m_poDeviceListHandler)

      m_poConnMngrResp->vPostDAPStatusInfo(cou32DeviceHandle,
               enDevConnType,
               enDAPStatus);
   } // if (NULL != m_poConnMngrResp) && ...

}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vOnDeviceConnectionCb
 ***************************************************************************/
t_Void spi_tclConnMngr::vOnDeviceConnectionCb(const t_U32 cou32DeviceHandle,
         const trDeviceInfo& corfrDeviceInfo, tenDeviceCategory enReportingDiscoverer)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vOnDeviceConnectionCb: cou32DeviceHandle = 0x%x \n",
            cou32DeviceHandle));

   //! Add device to unknown device list (only Android devices capable of aoap and apple devices as of now)
   //! Mirrorlink devices are not yet supported
   //! Apple device are added when role switch required request is issued by mediaplayer
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();

   if((NULL != m_poDeviceListHandler) && (NULL != poConnSettings))
   {
      tenEnabledInfo enDeviceAuthEnabled = e8USAGE_DISABLED;
      //! Authorization Required?
      enDeviceAuthEnabled = poConnSettings->enGetDeviceAuthEnableInfo();
      tenEnabledInfo enProjectionStatus = e8USAGE_ENABLED;
      //! Is the projection status set to "user confirmation needed"?
      bGetDevProjUsage(e8DEV_TYPE_ANDROIDAUTO, enProjectionStatus);
      //! Is it a unknown device? Not required for RNAIVI. Enable this check if required in future
      //t_Bool bDeviceValid =  m_poDeviceListHandler->bIsDeviceValid(cou32DeviceHandle);
      //! Does the device support aoap
      t_Bool bIsAOAPDevice = (e8SPI_SUPPORTED == corfrDeviceInfo.rProjectionCapability.enAndroidAutoSupport) && (e8DEV_TYPE_ANDROIDAUTO == enReportingDiscoverer);
      //! Is the device already reported by the other discoverer
      m_oLockReportedDiscoverer.s16Lock();
      t_Bool bIsDeviceAlreadyReported = (m_mapReportedDiscoverer.end() != m_mapReportedDiscoverer.find(cou32DeviceHandle));
      m_oLockReportedDiscoverer.vUnlock();

      ETG_TRACE_USR1(("Device authorization check enDeviceAuthEnabled = %d, bIsAOAPDevice = %d, "
            "enReportingDiscoverer = %d bIsDeviceAlreadyReported = %d\n",
            enDeviceAuthEnabled, ETG_ENUM(BOOL, bIsAOAPDevice),
            ETG_ENUM(DEVICE_CATEGORY, enReportingDiscoverer), ETG_ENUM(BOOL, bIsDeviceAlreadyReported)));
      if(((e8USAGE_ENABLED == enDeviceAuthEnabled) && (e8USAGE_DISABLED != enProjectionStatus)) &&
            ((e8USAGE_CONF_REQD == enProjectionStatus))
            && ((true == bIsAOAPDevice) || (true == bIsDeviceAlreadyReported))&&
            (e8DEV_TYPE_ANDROIDAUTO != corfrDeviceInfo.enDeviceCategory))
      {
         ETG_TRACE_USR1(("Device authorization is required. Waiting for user input. \n"));
         trUnauthDeviceInfo rUnauthDevInfo;
         rUnauthDevInfo.rDeviceInfo = corfrDeviceInfo;
         rUnauthDevInfo.rAuthInfo.u32DeviceHandle = cou32DeviceHandle;
         rUnauthDevInfo.rAuthInfo.enDeviceType = e8_ANDROID_DEVICE;
         rUnauthDevInfo.rAuthInfo.enUserAuthorizationStatus = e8_USER_AUTH_UNAUTHORIZED;
         m_poDeviceListHandler->vAddUnauthorizedDeviceToList(rUnauthDevInfo);
         //! Trigger Device authorization request
         //! Wait for confirmation from user before proceeding with selection
         if(NULL != m_poConnMngrResp)
         {
            std::vector<trDeviceAuthInfo> rvecDevAuthInfo;
            m_poDeviceListHandler->vGetUnauthorizedDevicesList(rvecDevAuthInfo);
            m_poConnMngrResp->vRequestDeviceAuthorization(rvecDevAuthInfo);
         }
      }
      else
      {
         ETG_TRACE_USR1(("Device authorization not required. Proceeding with Connection\n"));
         //! If device authorization is not required, proceed with connection
         vProceedwithConnection(cou32DeviceHandle, corfrDeviceInfo, enReportingDiscoverer);
      }
   }

}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vOnDeviceDisconnectionCb
 ***************************************************************************/
t_Void spi_tclConnMngr::vOnDeviceDisconnectionCb(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vOnDeviceDisconnectionCb: DeviceHandle = 0x%x Devicecategory = %d\n",
            cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDevCat)));

   m_oLockReportedDiscoverer.s16Lock();
   //! Remove the device from the map on disconnection
   if (m_mapReportedDiscoverer.end() != m_mapReportedDiscoverer.find(cou32DeviceHandle))
   {
      ETG_TRACE_USR4(("Erasing Device 0x%x from the reported discoverer list\n", cou32DeviceHandle));
      m_mapReportedDiscoverer.erase(cou32DeviceHandle);
   }
   m_oLockReportedDiscoverer.vUnlock();

   //! Check if an unknown device was disconnected
   if ((NULL != m_poDeviceListHandler) && (NULL != m_poConnMngrResp))
   {
      trUnauthDeviceInfo rDeviceInfo;
      if((true == m_poDeviceListHandler->bGetUnauthorizedDeviceInfo(cou32DeviceHandle, rDeviceInfo))
            && ((e8DEV_TYPE_ANDROIDAUTO == enDevCat) || (e8DEV_TYPE_DIPO == enDevCat)))
      {
         ETG_TRACE_USR1(("spi_tclConnMngr::vOnDeviceDisconnectionCb: Removing unauthorized device = 0x%x from list\n",
               cou32DeviceHandle));
         std::vector<trDeviceAuthInfo> rvecDevAuthInfo;
         m_poDeviceListHandler->vRemoveUnauthorizedDevice(cou32DeviceHandle);
         //! Update HMI with new device list to inform device disconnection
         m_poDeviceListHandler->vGetUnauthorizedDevicesList(rvecDevAuthInfo);
         m_poConnMngrResp->vRequestDeviceAuthorization(rvecDevAuthInfo);
      }
   }

   m_oDeviceDisconnection.s16Lock();

   //! Check the discoveer reporting the device disconnection Ex Mirrorlink/Android Auto
   //! This is to prevent false device disconnections reported by Mirrorlink
   //! during Android Auto session

   if ((NULL != m_poDeviceListHandler))
   {
      tenDeviceCategory enStoredDevCat = m_poDeviceListHandler->enGetDeviceCategory(cou32DeviceHandle);
      if ((0 != cou32DeviceHandle) && ((enDevCat == enStoredDevCat) || (e8DEV_TYPE_UNKNOWN == enStoredDevCat)))
      {
         ETG_TRACE_USR2(("DeviceHandle = 0x%x having Devicecategory = %d is disconnected from system\n", cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDevCat)));
         if (true == m_poDeviceListHandler->bGetUSBConnectedFlag(cou32DeviceHandle) && (e8DEV_TYPE_DIPO != enDevCat))
         {
            m_poDeviceListHandler->vSetUSBConnectedFlag(cou32DeviceHandle, false);
         }

         if (true == m_poDeviceListHandler->bIsDeviceConnected(cou32DeviceHandle))
         {
            //! check if selected device was disconnected
            if (cou32DeviceHandle == m_poDeviceListHandler->u32GetSelectedDevice())
            {
               //! Set the device state to  not selected
               m_poDeviceListHandler->vSetDeviceSelection(cou32DeviceHandle, false);
            }
            else
            {
               //! Remove the device from list if its was never selected
               if (false == m_poDeviceListHandler->bWasDeviceSelected(cou32DeviceHandle))
               {
                  m_poDeviceListHandler->vRemoveDeviceFromList(cou32DeviceHandle);
               }
            }

            //! Set the connection status to not connected
            m_poDeviceListHandler->vSetDeviceConnectionStatus(cou32DeviceHandle, e8DEV_NOT_CONNECTED);

            //! Send Device disconnection to SPI components
            spi_tclMediator *poMediator = spi_tclMediator::getInstance();
            if (NULL != poMediator)
            {
               poMediator->vPostDeviceDisconnection(cou32DeviceHandle);
            } // if (NULL != poMediator)

            //! Send Device list change
            if (NULL != m_poConnMngrResp)
            {
               m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle, e8USB_CONNECTED, e8DEVICE_REMOVED);
            } //if (NULL != m_poConnMngrResp)
         }
      }
      else
      {
         ETG_TRACE_ERR(("Invalid Device : DeviceHandle = 0x%x is not connected!\n", cou32DeviceHandle));
      } //if((NULL != m_poDeviceListHandler) && ...
   }

   m_oDeviceDisconnection.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSelectDeviceResultCb
 ***************************************************************************/
t_Void spi_tclConnMngr::vSelectDeviceResultCb(tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vSelectDeviceResultCb enErrorCode = %d \n",
            ETG_ENUM(ERROR_CODE,enErrorCode)));

   //! Post Select device result
   spi_tclMediator *poMediator = spi_tclMediator::getInstance();
   if (NULL != poMediator)
   {
      poMediator->vPostSelectDeviceRes(e32COMPID_CONNECTIONMANAGER,enErrorCode);
   } //if (NULL != poMediator)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclConnMngr::vApplicationMediaMetaDataCb(...
***************************************************************************/

t_Void spi_tclConnMngr::vApplicationMediaMetaDataCb(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
   const trUserContext& rfcorUsrCntxt)
{
   //! Forward media metadata to ConnMngr response interface
   if (NULL != m_poConnMngrResp)
   {
      m_poConnMngrResp->vPostApplicationMediaMetaData(rfcorApplicationMediaMetaData, rfcorUsrCntxt);
   } //if (NULL != m_poConnMngrResp)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclConnMngr::vApplicationPhoneDataCb(...
***************************************************************************/

t_Void spi_tclConnMngr::vApplicationPhoneDataCb(const trAppPhoneData& rfcorApplicationPhoneData,
   const trUserContext& rfcorUsrCntxt)
{
   //! Forward phone data including phone call metadata to connnMngr response interface
   if (NULL != m_poConnMngrResp)
   {
      m_poConnMngrResp->vPostApplicationPhoneData(rfcorApplicationPhoneData, rfcorUsrCntxt);
   } //if (NULL != m_poConnMngrResp)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclConnMngr::vApplicationMediaPlaytimeCb(...
***************************************************************************/

t_Void spi_tclConnMngr::vApplicationMediaPlaytimeCb(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
      const trUserContext& rfcorUsrCntxt)
{
   //! Forward current playing media playtime to ConnMngr response interface
   if (NULL != m_poConnMngrResp)
   {
      m_poConnMngrResp->vPostApplicationMediaPlaytime(rfcorApplicationMediaPlaytime, rfcorUsrCntxt);
   } //if (NULL != m_poConnMngrResp)
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vUpdateDeviceNameCb
 ***************************************************************************/
t_Void spi_tclConnMngr::vUpdateDeviceNameCb(t_U32 cou32DeviceHandle, t_String szDeviceName)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vUpdateDeviceNameCb: Updating Device = %d  with new name = %s \n",
         cou32DeviceHandle, szDeviceName.c_str()));
   if (NULL != m_poDeviceListHandler)
   {
      trDeviceInfo rDeviceInfo;
      t_Bool bDevFound = m_poDeviceListHandler->bGetDeviceInfo(cou32DeviceHandle, rDeviceInfo);

      /*
       * 1. vUpdateDeviceNameCb will be called from BT and GAL receiver modules
       * 2. Initially before the updates received from modules Structure will hold default Model name
       * 3. Update only if device name (rDeviceInfo.szDeviceName) is same as model name (rDeviceInfo.szDeviceModelName)
       * 	and received name (szDeviceName) is same as device name (rDeviceInfo.szDeviceName) and received name (szDeviceName) is not empty
       * 4. As GAL receiver update is received first followed by BT update.
       */
      t_Bool bDeviceNameUpdate = ((rDeviceInfo.szDeviceModelName == rDeviceInfo.szDeviceName) && (szDeviceName != rDeviceInfo.szDeviceName)
    		  	  	  	  	  	  	  	  && !(szDeviceName.empty()));

      //! Send Device list change only if there is a change in device name
      if ((NULL != m_poConnMngrResp) && (true == bDevFound) && (true == bDeviceNameUpdate))
      {
         m_poDeviceListHandler->vSetDeviceName(cou32DeviceHandle, szDeviceName);
         m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle, rDeviceInfo.enDeviceConnectionType,
               e8DEVICE_CHANGED);
      } //if (NULL != m_poConnMngrResp)
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vRegisterCallbacks
 ***************************************************************************/
t_Void spi_tclConnMngr::vRegisterCallbacks()
{
   /*lint -esym(40,fvUpdateDAPStatus) fvUpdateDAPStatus Undeclared identifier */
   /*lint -esym(40,fvDeviceConnection)fvDeviceConnection Undeclared identifier */
   /*lint -esym(40,fvDeviceDisconnection) fvDeviceDisconnection identifier */
   /*lint -esym(40,fvSelectDeviceResult)fvSelectDeviceResult Undeclared identifier */
   /*lint -esym(40,fvAppMediaMetaData) fvAppMediaMetaData Undeclared identifier */
   /*lint -esym(40,fvAppMediaPlaytime) fvAppMediaPlaytime Undeclared identifier */
   /*lint -esym(40,fvAppPhoneData) fvAppPhoneData Undeclared identifier */
   /*lint -esym(40,_1) _1 Undeclared identifier */
   /*lint -esym(40,_2) _2 Undeclared identifier */
   /*lint -esym(40,_3) _3 Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclConnMngr::vRegisterCallbacks() entered \n"));

   trConnCallbacks rConnMngrCallbacks;

   //! Populate the callback structure
   rConnMngrCallbacks.fvUpdateDAPStatus =
            std::bind(&spi_tclConnMngr::vUpdateDAPStatusCb,
                     this,
                     SPI_FUNC_PLACEHOLDERS_2);

   rConnMngrCallbacks.fvDeviceConnection =
            std::bind(&spi_tclConnMngr::vOnDeviceConnectionCb,
                     this,
                     SPI_FUNC_PLACEHOLDERS_3);

   rConnMngrCallbacks.fvDeviceDisconnection =
            std::bind(&spi_tclConnMngr::vOnDeviceDisconnectionCb,
                     this,
                     SPI_FUNC_PLACEHOLDERS_2);

   rConnMngrCallbacks.fvSelectDeviceResult =
            std::bind(&spi_tclConnMngr::vSelectDeviceResultCb,
                     this,
                     SPI_FUNC_PLACEHOLDERS_1);

   rConnMngrCallbacks.fvAppMediaMetaData =
            std::bind(&spi_tclConnMngr::vApplicationMediaMetaDataCb,
                     this,
                     SPI_FUNC_PLACEHOLDERS_2);

   rConnMngrCallbacks.fvAppMediaPlaytime =
            std::bind(&spi_tclConnMngr::vApplicationMediaPlaytimeCb,
                     this,
                     SPI_FUNC_PLACEHOLDERS_2);

   rConnMngrCallbacks.fvAppPhoneData =
            std::bind(&spi_tclConnMngr::vApplicationPhoneDataCb,
                     this,
                     SPI_FUNC_PLACEHOLDERS_2);

   rConnMngrCallbacks.fvSelectDeviceError =
            std::bind(&spi_tclConnMngr::vSetSelectError,
                     this,
                     SPI_FUNC_PLACEHOLDERS_2);

   rConnMngrCallbacks.fvSetDeviceName =
		   std::bind(&spi_tclConnMngr::vUpdateDeviceNameCb,
				   this,
				   SPI_FUNC_PLACEHOLDERS_2);

   //! Register for callbacks with SPI connections
   for (t_U32 u32Index = 0; u32Index < e8_CONN_INDEX_SIZE; u32Index++)
   {
      if (NULL != m_poConnHandlers[u32Index])
      {
         m_poConnHandlers[u32Index]->vRegisterCallbacks(rConnMngrCallbacks);
      } // if(NULL != m_poConnHandlers[e8_CONN_ML_INDEX])
   } // for(t_U32 u32Index = 0; u32Index < e8_CONN_INDEX_SIZE; u32Index++)

   spi_tclMediator *poMediator = spi_tclMediator::getInstance();
   if (NULL != poMediator)
   {
      trConnMngrCallback rConnMngrCb;
      rConnMngrCb.fvSetBTDeviceName = std::bind(&spi_tclConnMngr::vUpdateDeviceNameCb,
                     this, SPI_FUNC_PLACEHOLDERS_2);
      poMediator->vRegisterCallbacks(rConnMngrCb);
   }
}


/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vHandleGenericDevices
 ***************************************************************************/
t_Void spi_tclConnMngr::vHandleGenericDevices(const t_U32 cou32DeviceHandle,
         const trDeviceInfo& corfrDeviceInfo, tenDeviceCategory enReportingDiscoverer)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vHandleGenericDevices(): Device connection reported by Discoverer = %d \n",
            ETG_ENUM(DEVICE_CATEGORY, enReportingDiscoverer)));

   //! Check if both the technologies are enabled. If so then wait for both the discoverers to report the device
   t_Bool bProcessingRequired = bIsProcessingRequired(cou32DeviceHandle,corfrDeviceInfo, enReportingDiscoverer);

   if (NULL != m_poDeviceListHandler)
   {
      //! Send the device connection status for known devices on USB connection when automatic selection is not triggered
      t_U32 u32SelectedDevice = m_poDeviceListHandler->u32GetSelectedDevice();
      t_Bool bIsUserDeselected = m_poDeviceListHandler->bGetUserDeselectionFlag(cou32DeviceHandle);
      tenDeviceCategory enDeviceCategory = m_poDeviceListHandler->enGetDeviceCategory(cou32DeviceHandle);
      if ((true == m_poDeviceListHandler->bIsDeviceValid(cou32DeviceHandle)) && ((e8DEV_TYPE_ANDROIDAUTO == enDeviceCategory) || (e8DEV_TYPE_MIRRORLINK == enDeviceCategory))
            &&(false == bProcessingRequired))
      {
         tenEnabledInfo enDevUsageEnabled= e8USAGE_ENABLED;
         m_poDeviceListHandler->vGetDeviceUsagePreference(cou32DeviceHandle, enDevUsageEnabled);
         tenEnabledInfo enDevPrjUsageEnabled= e8USAGE_ENABLED;
         m_poDeviceListHandler->bGetDevProjUsage(enDeviceCategory, enDevPrjUsageEnabled);
         t_Bool bSelectError = m_poDeviceListHandler->bIsSelectError(cou32DeviceHandle);
         //! Send Device list change if the device is deleted / setting is OFF / some other device is active / user has deselected the device
         if ((NULL != m_poConnMngrResp) && ((e8USAGE_DISABLED == enDevUsageEnabled) || (0 != u32SelectedDevice)
               || (true == bIsUserDeselected) || (e8USAGE_DISABLED == enDevPrjUsageEnabled) || (true == bSelectError)))
         {
            ETG_TRACE_USR1(("Updating device connection status of known device to HMI\n"));
            m_poDeviceListHandler->vSetUSBConnectedFlag(cou32DeviceHandle, true);
            m_poDeviceListHandler->vSetDeviceConnectionStatus(cou32DeviceHandle, e8DEV_CONNECTED);
            m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle, e8USB_CONNECTED, e8DEVICE_ADDED);
         } //if (NULL != m_poConnMngrResp)
      }

      //! Following processing is required only if both Android Auto and Mirrorlink are enabled
      //! Presently android auto is preferred over Mirrorlink
      if (true == bProcessingRequired)
      {
         vHandleSPITechnologySwitch(cou32DeviceHandle, corfrDeviceInfo, enReportingDiscoverer);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::enGetDeviceSwitchType
 ***************************************************************************/
tenDeviceCategory spi_tclConnMngr::enGetDeviceSwitchType(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::enGetDeviceSwitchType() Device Handle = %d \n", cou32DeviceHandle));
   tenDeviceCategory enRequiredSwitch = e8DEV_TYPE_UNKNOWN;

   //! check preferred technology on a newly connected device

   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   tenDeviceCategory enPrefDeviceCategory = e8DEV_TYPE_ANDROIDAUTO;
   if (NULL != poConnSettings)
   {
      enPrefDeviceCategory = poConnSettings->enGetTechnologyPreference();
   } //if (NULL != poConnSettings)


   //! Check for the currently enabled SPI technologies
   tenEnabledInfo enMLEnabled = e8USAGE_DISABLED;
   tenEnabledInfo enAAPEnabled = e8USAGE_DISABLED;
   tenSPISupport enMLSupport = e8SPI_SUPPORT_UNKNOWN;
   tenSPISupport enAAPSupport = e8SPI_SUPPORT_UNKNOWN;
   bGetDevProjUsage(e8DEV_TYPE_MIRRORLINK, enMLEnabled);
   bGetDevProjUsage(e8DEV_TYPE_ANDROIDAUTO, enAAPEnabled);

   if (NULL != m_poDeviceListHandler)
   {
      //! Get device specific support for specified SPI technology
      //! Check if the device already exists in the list and supported technology is known
      enMLSupport = m_poDeviceListHandler->enGetSPISupport(cou32DeviceHandle, e8DEV_TYPE_MIRRORLINK);
      enAAPSupport = m_poDeviceListHandler->enGetSPISupport(cou32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO);
   }

   //! Removed check for not supported flag as this will invalidate the device even if device switch fails once
   t_Bool bMLSupported = (((e8USAGE_ENABLED == enMLEnabled) || (e8USAGE_CONF_REQD == enMLEnabled)) /*&& (e8SPI_NOTSUPPORTED != enMLSupport)*/);
   t_Bool bAAPSupported = ((e8USAGE_ENABLED == enAAPEnabled) || (e8USAGE_CONF_REQD == enAAPEnabled))/* && (e8SPI_NOTSUPPORTED != enAAPSupport)*/;

   //! If device supports both Mirrorlink and Android auto  or both the capabilities are unknown
   //! then choose switch based on preference
   if((true == bAAPSupported) && (e8DEV_TYPE_ANDROIDAUTO == enPrefDeviceCategory))
   {
      enRequiredSwitch = e8DEV_TYPE_ANDROIDAUTO;
   }
   else if((true == bMLSupported) && (e8DEV_TYPE_MIRRORLINK == enPrefDeviceCategory))
   {
      enRequiredSwitch = e8DEV_TYPE_MIRRORLINK;
   }
   // else if one of them is supported then choose that
   else if(true == bAAPSupported)
   {
      enRequiredSwitch = e8DEV_TYPE_ANDROIDAUTO;
   }
   else
   {
      enRequiredSwitch = e8DEV_TYPE_MIRRORLINK;
   }

   ETG_TRACE_USR1(("spi_tclConnMngr::enGetDeviceSwitchType(): Device Support for AOAP =%d  Mirrorlink =%d "
         "Preferred device category =%d, Required device switch = %d\n",
            ETG_ENUM(BOOL,bAAPSupported), ETG_ENUM(BOOL,bMLSupported),
            ETG_ENUM(DEVICE_CATEGORY, enPrefDeviceCategory), ETG_ENUM(DEVICE_CATEGORY, enRequiredSwitch)));
   return enRequiredSwitch;

}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bIsProcessingRequired
 ***************************************************************************/
t_Bool spi_tclConnMngr::bIsProcessingRequired(const t_U32 cou32DeviceHandle,
         const trDeviceInfo& corfrDeviceInfo,
         tenDeviceCategory enReportingDiscoverer)
{
   t_Bool bRetVal = false;
   t_Bool bIsDeviceReported = true;

   //! Check for the currently enabled SPI technologies
   tenEnabledInfo enMLEnabled = e8USAGE_DISABLED;
   tenEnabledInfo enAAPEnabled = e8USAGE_DISABLED;
   bGetDevProjUsage(e8DEV_TYPE_MIRRORLINK, enMLEnabled);
   bGetDevProjUsage(e8DEV_TYPE_ANDROIDAUTO, enAAPEnabled);

   //! Store the details of the discoverer which reported the device
   m_oLockReportedDiscoverer.s16Lock();
   if ((m_mapReportedDiscoverer.end() == m_mapReportedDiscoverer.find(cou32DeviceHandle)))
   {
      ETG_TRACE_USR4(("Storing the device as this is the first discoverer to report the device "));
      ETG_TRACE_USR4((" Device Type: %d", ETG_ENUM(DEVICE_TYPE,corfrDeviceInfo.rProjectionCapability.enDeviceType)));
      m_mapReportedDiscoverer[cou32DeviceHandle] = corfrDeviceInfo;
      bIsDeviceReported = false;
   }
   //! Replace the device info with the one from android auto discoverer as it contains complete information
   else if (e8DEV_TYPE_ANDROIDAUTO == enReportingDiscoverer)
   {
      ETG_TRACE_USR4(("Replacing device details with that reported by Android Auto discoverer \n"));
      ETG_TRACE_USR4((" Device Type: %d", ETG_ENUM(DEVICE_TYPE,corfrDeviceInfo.rProjectionCapability.enDeviceType)));
      m_mapReportedDiscoverer[cou32DeviceHandle] = corfrDeviceInfo;
   }
   m_oLockReportedDiscoverer.vUnlock();

   //! Check if SPI technologies are enabled in calibration and settings menu
   t_Bool bIsMirrorlinkEnabled = (((e8USAGE_ENABLED == enMLEnabled) || (e8USAGE_CONF_REQD == enMLEnabled)) && (true
            == m_rSPIFeatureSupport.bMirrorLinkSupported()));
   t_Bool bIsAndroidAutoEnabled = (((e8USAGE_ENABLED == enAAPEnabled) || (e8USAGE_CONF_REQD == enAAPEnabled))&& (true
            == m_rSPIFeatureSupport.bAndroidAutoSupported()));

   if(NULL != m_poDeviceListHandler)
   {
      t_Bool bIsUserDeselected = m_poDeviceListHandler->bGetUserDeselectionFlag(cou32DeviceHandle);
      tenEnabledInfo enDevUsageEnabled= e8USAGE_ENABLED;
      m_poDeviceListHandler->vGetDeviceUsagePreference(cou32DeviceHandle, enDevUsageEnabled);
      t_Bool bSelectError = m_poDeviceListHandler->bIsSelectError(cou32DeviceHandle);

      t_U32 u32SelectedDevice = m_poDeviceListHandler->u32GetSelectedDevice();
      //! Proceed only if user has not deselected the device, previous activation has not failed, no other device is selected
      //! and the device is not deleted.
      if ((false == bIsUserDeselected) && (false == bSelectError) && (0 == u32SelectedDevice) && (e8USAGE_ENABLED == enDevUsageEnabled))
      {
         //! If both Mirrorlink and android Auto are enabled, wait for both the discoverers to report device
         //! and check for the preference
         if ((true == bIsAndroidAutoEnabled) && (true == bIsMirrorlinkEnabled))
         {
            ETG_TRACE_USR2(("Both Mirrorlink and Andoroid Auto are enabled, wait for both the discoverers "));
            bRetVal = bIsDeviceReported;
         }

         //! If only Android Auto is enabled then proceeed with Android Auto switch
         else if ((true == bIsAndroidAutoEnabled) && (e8DEV_TYPE_ANDROIDAUTO == enReportingDiscoverer))
         {
             tenSPISupport enSPISupport = m_poDeviceListHandler->enGetSPISupport(cou32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO);
            //! Check if the device supports AOAP before proceeding with the switch
            if (((e8SPI_NOTSUPPORTED != corfrDeviceInfo.rProjectionCapability.enAndroidAutoSupport) ||
               (e8SPI_SUPPORTED == enSPISupport))
               && (e8SPI_NOTSUPPORTED != enSPISupport))
            {
               ETG_TRACE_USR2(("Only Android Auto is enabled. Proceed with AOAP switch"));
               t_Bool bAAPswitch = bEvaluateDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO, corfrDeviceInfo);
               bRetVal = false;
               ETG_TRACE_USR1(("AOAP Switch for Device Handle = 0x%x: Result = %d  \n", cou32DeviceHandle, ETG_ENUM(BOOL,
                        bAAPswitch)));
            }
         }

         //! If only Mirrorlink is enabled then proceeed with Mirrorlink switch
         else if ((true == bIsMirrorlinkEnabled) && (e8DEV_TYPE_MIRRORLINK == enReportingDiscoverer))
         {
            ETG_TRACE_USR2(("Only Mirrorlink is enabled. Proceed with Mirrorlink switch"));
            t_Bool bMLSwitch = bEvaluateDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_MIRRORLINK, corfrDeviceInfo);
            bRetVal = false;
            ETG_TRACE_USR1((" Mirrorlink Switch for Device Handle = 0x%x result: %d  \n", cou32DeviceHandle, ETG_ENUM(BOOL,
                     bMLSwitch)));
         }
      }
   }
   ETG_TRACE_USR2(("Calibration : Mirrorlink Feature Support = %d Android Auto Feature Support = %d  \n",
         m_rSPIFeatureSupport.bMirrorLinkSupported(), m_rSPIFeatureSupport.bAndroidAutoSupported()));

   ETG_TRACE_USR1(("Settings: Device Handle = 0x%x IsMirrorlinkEnabled = %d bIsAndroidAutoEnabled - %d Processing required = %d \n",
         cou32DeviceHandle, ETG_ENUM(BOOL, bIsMirrorlinkEnabled), ETG_ENUM(BOOL, bIsAndroidAutoEnabled), ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bRequestDeviceSwitch
 ***************************************************************************/
t_Bool spi_tclConnMngr::bRequestDeviceSwitch(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::bRequestDeviceSwitch() Device Handle = %d  switchtype = %d\n", cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDevCat)));
   t_Bool bRetVal = false;
   switch(enDevCat)
   {
      case e8DEV_TYPE_ANDROIDAUTO:
      {
         if(NULL != m_poConnHandlers[e8_CONN_AAUTO_INDEX])
         {
            bRetVal = m_poConnHandlers[e8_CONN_AAUTO_INDEX]->bRequestDeviceSwitch(cou32DeviceHandle,
                  e8DEV_TYPE_ANDROIDAUTO);
         }
         break;
      }
      case e8DEV_TYPE_MIRRORLINK:
      {
         if (NULL != m_poConnHandlers[e8_CONN_ML_INDEX])
         {
            bRetVal = m_poConnHandlers[e8_CONN_ML_INDEX]->bRequestDeviceSwitch(cou32DeviceHandle,
                  e8DEV_TYPE_MIRRORLINK);
         }
         break;
      }
      case e8DEV_TYPE_DIPO:
      {
         ETG_TRACE_ERR(("spi_tclConnMngr::bRequestDeviceSwitch: not yet supported for this device category \n"));
         break;
      }
      //! In this case, Request will be forwarded to AOAP USB discoverer to reset the USB device
      case e8DEV_TYPE_UNKNOWN:
      {
         if (NULL != m_poConnHandlers[e8_CONN_AAUTO_INDEX])
         {
            m_poConnHandlers[e8_CONN_AAUTO_INDEX]->bRequestDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_UNKNOWN);
         }
         break;
      }
      default:
      {
         ETG_TRACE_ERR(("spi_tclConnMngr::bRequestDeviceSwitch: unknown switch type \n"));
         break;
      }
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::bEvaluateDeviceSwitch
 ***************************************************************************/
t_Bool spi_tclConnMngr::bEvaluateDeviceSwitch(const t_U32 cou32DeviceHandle,
         tenDeviceCategory enDevCat, const trDeviceInfo& corfrDeviceInfo)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::bEvaluateDeviceSwitch: Device Handle = 0x%x Requested switch type = %d\n", cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, enDevCat)));
   t_Bool bRetVal = false;
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   tenDeviceSelectionMode enDevSelMode = e16DEVICESEL_AUTOMATIC;
   if(NULL != poConnSettings)
   {
      enDevSelMode = poConnSettings->enGetDeviceSelectionMode();
   }

   if (NULL != m_poDeviceListHandler)
   {
      //! Check the device selection mode before switching the device to SPI mode
      switch (enDevSelMode)
      {
         //! if the device selection mode is manual_usb then send all reported USB devices to HMI
         case e16DEVICESEL_MANUAL:
         {
            bRetVal = true;
            tenDeviceStatusInfo enDevStatus = m_poDeviceListHandler->enAddDeviceToList(cou32DeviceHandle,
                     corfrDeviceInfo);
            //! Send Device list change only if there is a change in device status
            if ((NULL != m_poConnMngrResp) && (e8DEVICE_STATUS_NOT_KNOWN != enDevStatus))
            {
               m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle, e8USB_CONNECTED, enDevStatus);
            } //if (NULL != m_poConnMngrResp)
         }
            break;
         case e16DEVICESEL_AUTOMATIC:
         case e16DEVICESEL_SEMI_AUTOMATIC:
         {
            bRetVal = bRequestDeviceSwitch(cou32DeviceHandle, enDevCat);
         }
            break;
         default:
         {
            ETG_TRACE_ERR(("spi_tclConnMngr::bEvaluateDeviceSwitch: DeviceSelectionMode %d is not supported\n", enDevSelMode));
         }
            break;
      }
   }
   return bRetVal;
}


/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vHandleSPITechnologySwitch
 ***************************************************************************/
t_Void spi_tclConnMngr::vHandleSPITechnologySwitch(const t_U32 cou32DeviceHandle, const trDeviceInfo& corfrDeviceInfo,
      tenDeviceCategory enReportingDiscoverer)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vHandleSPITechnologySwitch() Device Handle = %d supports more than one technology "
         "Triggering USB profile switch to device based on preference\n", cou32DeviceHandle));
   trDeviceInfo rDeviceInfo = corfrDeviceInfo;
   m_oLockReportedDiscoverer.s16Lock();
   if(m_mapReportedDiscoverer.end() != m_mapReportedDiscoverer.find(cou32DeviceHandle))
   {
      rDeviceInfo = m_mapReportedDiscoverer[cou32DeviceHandle];
   }
   m_mapReportedDiscoverer.erase(cou32DeviceHandle);
   m_oLockReportedDiscoverer.vUnlock();

   if (NULL != m_poDeviceListHandler)
   {
      t_U32 u32SelectedDevice = m_poDeviceListHandler->u32GetSelectedDevice();
      t_Bool bIsUserDeselected = m_poDeviceListHandler->bGetUserDeselectionFlag(cou32DeviceHandle);
      tenDeviceCategory enRequiredSwitch = enGetDeviceSwitchType(cou32DeviceHandle);

      //!  Check if the device is selected already or not deselected by user or if other device is already active
      if ((false == bIsUserDeselected) && (0 == u32SelectedDevice))
      {
         //! Request for device switch based on the preference
         t_Bool bAAPswitch = false;
         t_Bool bMLSwitch = false;
         switch (enRequiredSwitch)
         {
            case e8DEV_TYPE_ANDROIDAUTO:
            {
               //! Check if AOAP is supported by the USB device or if it is a known device
               if (((e8SPI_SUPPORTED == rDeviceInfo.rProjectionCapability.enAndroidAutoSupport) || (e8DEV_TYPE_MIRRORLINK
                     == enReportingDiscoverer)) || (e8SPI_SUPPORTED == m_poDeviceListHandler->enGetSPISupport(
                     cou32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO)))
               {
                  bAAPswitch = bEvaluateDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO, rDeviceInfo);
               }
               //! If AAP switch is successful, then don't trigger for other technologies
               if(false == bAAPswitch)
               {
                  //! If the AOAP switch fails then store the information and
                  //! check support for other SPI technologies
                  m_poDeviceListHandler->vSetSPISupport(cou32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO, e8SPI_NOTSUPPORTED);
                  //! Try Mirrorlink switch as AOAP switch failed
                  bMLSwitch = bEvaluateDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_MIRRORLINK, rDeviceInfo);
               }
               break;
            }
            case e8DEV_TYPE_MIRRORLINK:
            {
               bMLSwitch = bEvaluateDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_MIRRORLINK, rDeviceInfo);
               if (false == bMLSwitch)
               {
                  //! TODO check if switch in progress is required
                  //! If the switch fails then store the information
                  m_poDeviceListHandler->vSetSPISupport(cou32DeviceHandle, e8DEV_TYPE_MIRRORLINK, e8SPI_NOTSUPPORTED);
                  // Try AOAP switch as Mirrorlink switch failed
                  if (((e8SPI_SUPPORTED == rDeviceInfo.rProjectionCapability.enAndroidAutoSupport) || (e8DEV_TYPE_MIRRORLINK
                        == enReportingDiscoverer)))
                  {
                     bAAPswitch = bEvaluateDeviceSwitch(cou32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO, rDeviceInfo);
                  }
               }
            }
               break;
            default:
            {
               break;
            }
         }
         ETG_TRACE_USR1(("spi_tclConnMngr::vHandleSPITechnologySwitch() AOAP switch Result =%d  Mirrorlink switch result=%d\n",
               ETG_ENUM(BOOL,bAAPswitch), ETG_ENUM(BOOL, bMLSwitch)));
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vProceedwithConnection
 ***************************************************************************/
t_Void spi_tclConnMngr::vProceedwithConnection(const t_U32 cou32DeviceHandle,
      const trDeviceInfo& corfrDeviceInfo, tenDeviceCategory enReportingDiscoverer)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vProceedwithConnection () "));

   //! Check if the device type is already determined. if not handle it as a generic device
   if((e8DEV_TYPE_UNKNOWN == corfrDeviceInfo.enDeviceCategory) && (0 != cou32DeviceHandle))
   {
      vHandleGenericDevices(cou32DeviceHandle, corfrDeviceInfo, enReportingDiscoverer);
   }
   else if ((NULL != m_poDeviceListHandler) && (0 != cou32DeviceHandle))
   {
      ETG_TRACE_USR2(("DeviceHandle = 0x%x of device category %d is connected to the system \n",
               cou32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, corfrDeviceInfo.enDeviceCategory)));

      //! Add Device to Device List
      tenDeviceStatusInfo enDevStatus = m_poDeviceListHandler->enAddDeviceToList(cou32DeviceHandle,
               corfrDeviceInfo);
      //! Set the support for the reported technology. This will be used for successive connections
      m_poDeviceListHandler->vSetSPISupport(cou32DeviceHandle, enReportingDiscoverer, e8SPI_SUPPORTED);


      //! Send Device list change only if there is a change in device status
      if ((NULL != m_poConnMngrResp) && (e8DEVICE_STATUS_NOT_KNOWN != enDevStatus))
      {
         m_poConnMngrResp->vPostDeviceStatusInfo(cou32DeviceHandle,
                  corfrDeviceInfo.enDeviceConnectionType,
                  enDevStatus);
      } //if (NULL != m_poConnMngrResp)

     //! inform subcomponents about new device connection
     //! Ex: for automatic selection
    //! this update will trigger only if the device move from DICONNECTED state to CONNECTED state.
      t_U32 u32SelectedDevice = m_poDeviceListHandler->u32GetSelectedDevice();
      if ((0 == u32SelectedDevice) /*&& (e8DEV_CONNECTED != enDevConnStatus)*/)
      {
         spi_tclMediator *poMediator = spi_tclMediator::getInstance();

         if(NULL != poMediator)
         {
            poMediator->vPostDeviceConnection(cou32DeviceHandle);
         }
      }      // if (0 == u32SelectedDevice)
      else if((cou32DeviceHandle == u32SelectedDevice) && (e8DEVICE_CHANGED == enDevStatus))
      {
         //! Update device history if the selected device changes
         m_poDeviceListHandler->vAddDeviceToHistory(cou32DeviceHandle);
      }
   }
   else
   {
      ETG_TRACE_ERR(("Invalid Device: cou32DeviceHandle = 0x%x \n",
               cou32DeviceHandle));
   }   //if((NULL != m_poDeviceListHandler) && ...
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSendRoleSwitchResponse
 ***************************************************************************/
t_Void spi_tclConnMngr::vSendRoleSwitchResponse(const t_U32 cou32DeviceID, const trUserContext corUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vSendRoleSwitchResponse () Device Handle = 0x%x ", cou32DeviceID));

      //! Result to the role switch required query is based on 4 conditions.
      // 1. Check whether any other device is already active.-> False
      // 2. Check whether carplay is turned off in settings -> False
      // 3. Check if user has explicitly deselected the device -> False
      // 4. Check if the device is unknown. -> True
      // 5. Check project specific settings
      // If one device is already active, then no need to trigger a automatic role switch.

   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   t_Bool bRetVal = false;
   if((NULL != m_poDeviceListHandler) && (NULL != poConnSettings))
   {
      tenEnabledInfo enCarPlayUsagePref = e8USAGE_ENABLED;
      bGetDeviceUsagePreference(cou32MAX_DEVICEHANDLE, e8DEV_TYPE_DIPO , enCarPlayUsagePref );
      tenEnabledInfo enDevUsagePref = e8USAGE_ENABLED;
      m_poDeviceListHandler->vGetDeviceUsagePreference(cou32DeviceID, enDevUsagePref);
      if((true == poConnSettings->bIsEarlyRoleSwitchRequired())&&
         (e8USAGE_ENABLED == enCarPlayUsagePref) &&
         (e8USAGE_ENABLED == enDevUsagePref) &&
         (false == m_bIsDevSelectorBusy) &&
         (0 == m_poDeviceListHandler->u32GetSelectedDevice()) &&
         (e16DEVICESEL_MANUAL != poConnSettings->enGetDeviceSelectionMode()))
      {

         if((false == bIsSelectError(cou32DeviceID)) &&
            (false == m_poDeviceListHandler->bGetUserDeselectionFlag(cou32DeviceID)))
         {
            bRetVal = true;
         }//if(((false == bIsSelectError(cou32DeviceID))
      } //if((e8USAGE_ENABLED == enCarPlayUsagePref).....
   } // if(NULL != m_poDeviceListHandler)

   //! inform mediaplayer request to DiPo connection
   if ((NULL != m_poConnHandlers[e8_CONN_DIPO_INDEX]) && (true == bRetVal))
   {
      m_poConnHandlers[e8_CONN_DIPO_INDEX]->vSetRoleSwitchRequestedInfo(cou32DeviceID);
   }
   //! Send the response to mediaplayer
   if(NULL != m_poConnMngrResp)
   {
      m_poConnMngrResp->vPostDipoRoleSwitchResponse(bRetVal,cou32DeviceID, corUsrCntxt);
   }
   ETG_TRACE_USR1(("spi_tclConnMngr::vSendRoleSwitchResponse () Device Handle = 0x%x Early role switch required = %d ",
         cou32DeviceID, ETG_ENUM(BOOL, bRetVal)));
}


/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::enGetSPISupport
 ***************************************************************************/
tenSPISupport spi_tclConnMngr::enGetSPISupport(const t_U32 cou32DeviceHandle, tenDeviceCategory enSPIType)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vSetSPISupport () Device Handle = 0x%x ", cou32DeviceHandle));
   tenSPISupport enSPISupport = e8SPI_SUPPORT_UNKNOWN;
   if (NULL != m_poDeviceListHandler)
   {
      //! Get device specific support for specified SPI technology
      enSPISupport = m_poDeviceListHandler->enGetSPISupport(cou32DeviceHandle, enSPIType);
   }
   return enSPISupport;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr::vSetSPISupport
 ***************************************************************************/
t_Void spi_tclConnMngr::vSetSPISupport(const t_U32 cou32DeviceHandle, tenDeviceCategory enSPIType,
         tenSPISupport enSPISupport)
{
   ETG_TRACE_USR1(("spi_tclConnMngr::vSetSPISupport () Device Handle = 0x%x ", cou32DeviceHandle));
   if (NULL != m_poDeviceListHandler)
   {
      //! Get device specific support for specified SPI technology
      m_poDeviceListHandler->vSetSPISupport(cou32DeviceHandle, enSPIType, enSPISupport);
   }
}

//lint ?restore
