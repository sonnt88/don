/*!
 *******************************************************************************
 * \file             spi_tclDevHistory.cpp
 * \brief            Handles Device History
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Handles storage of Device History for SPI Devices
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <sstream>
#include "spi_tclDevHistory.h"
#include "spi_tclConnSettings.h"
#include "DirHandler.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclDevHistory.cpp.trc.h"
#endif
#endif

using namespace shl::io;
static const char* coczSqliteNotAnError = "not an error";
static const t_S8 scos8ReadUntilNullTermination = -1;
static const t_U8 scou8ColDeviceHandle = 0;
static const t_U8 scou8ColDeviceName = 1;
static const t_U8 scou8ColDeviceCategory = 2;
static const t_U8 scou8ColDeviceModelName = 3;
static const t_U8 scou8ColDeviceManufacturerName = 4;
//static const t_U8 scou8ColDeviceConnectionStatus = 5;
static const t_U8 scou8ColDeviceConnectionType = 6;
static const t_U8 scou8ColMajorVersion = 7;
static const t_U8 scou8ColMinorVersion = 8;
static const t_U8 scou8ColDAPSupport = 9;
//static const t_U8 scou8ColSelectedDevice = 10;
static const t_U8 scou8ColDeviceUsed = 11;
static const t_U8 scou8ColBTAddress = 12;
static const t_U8 scou8ColAccessIndex = 13;
//static const t_U8 scou8ColUserDeslected = 14;
static const t_U8 scou8ColAAPSupport = 15;
static const t_U8 scou8ColMLSupport = 16;
static const t_U8 scou8ColDevicetype = 17;
static const t_U8 scou8FirstParameter = 1;

static Lock m_oDBOperationInProgress;


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::spi_tclDevHistory
 ***************************************************************************/
spi_tclDevHistory::spi_tclDevHistory() :
         m_poDevHistorydb(NULL), m_bisDBOpen(false)
{
   ETG_TRACE_USR1((" spi_tclDevHistory::spi_tclDevHistory() entered \n"));

   m_oDBOperationInProgress.s16Lock();

   //! Fetch the project specific storage path
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   t_String szStorageDir;
   if(NULL != poConnSettings)
   {
      poConnSettings->vGetPersistentStoragePath(szStorageDir);
   }// if(NULL != poConnSettings)
   ETG_TRACE_USR1(("spi_tclDevHistory()::szStorageDir =  %s  \n", szStorageDir.c_str()));

   if (true == bCheckPathValidity(szStorageDir))
   {
      std::stringstream ssDatabasePath;
      ssDatabasePath << szStorageDir << "DeviceHistory.db";

      //!<stringstream>.str() returns a temporary string object that's destroyed at the end of the full expression.
      //!Hence to extend the lifetime of the string stream, a reference string is created instead
      //! of <stringstream>.str().c_str().
      const t_String rfszDatabasePath = ssDatabasePath.str();
      //! Open sqlite DataBase
      t_S32 s32DbOpeRes = sqlite3_open(rfszDatabasePath.c_str(), &m_poDevHistorydb);
      if (SQLITE_OK == s32DbOpeRes)
      {
         m_bisDBOpen = true;
      } //if (SQLITE_OK == s32DbOpeRes)
      else
      {
         ETG_TRACE_ERR(("Opening database failed:m_poDevHistorydb = %p "
                  "Error = %s \n", m_poDevHistorydb, sqlite3_errmsg(m_poDevHistorydb)));
         sqlite3_close(m_poDevHistorydb);
      }

      //! Create table
      if (false == bCreateDevHistoryTable())
      {
         ETG_TRACE_ERR(("Creation of Device History Table failed \n"));
      } //if(false == bCreateDevHistoryTable())
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::~spi_tclDevHistory
 ***************************************************************************/
spi_tclDevHistory::~spi_tclDevHistory()
{
   ETG_TRACE_USR1((" spi_tclDevHistory::~spi_tclDevHistory() m_bisDBOpen = %d \n", ETG_ENUM(BOOL, m_bisDBOpen)));
   //! Close the sqlite database
   if (true == m_bisDBOpen)
   {
      sqlite3_close(m_poDevHistorydb);
   } //if (true == m_bisDBOpen)
   m_poDevHistorydb = NULL;
   m_oDBOperationInProgress.vUnlock();
}


/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::u32GetNumOfDevices
 ***************************************************************************/
t_U32 spi_tclDevHistory::u32GetNumOfDevices()
{
   t_U32 u32DBSize =0;

   sqlite3_stmt *poStatement = NULL;

   //! construct query to count number of records in database
   t_Char czQueryofRecords[] = "SELECT COUNT(*) FROM DeviceHistory";

   //! Prepare sqlite statement
   if (sqlite3_prepare_v2(m_poDevHistorydb,
            czQueryofRecords,
            scos8ReadUntilNullTermination,
            &poStatement,
            NULL) == SQLITE_OK)
   {
      //! Step through the sqlite query
      //! SQLITE_ROW is returned if a row matches the query
      if (SQLITE_ROW == sqlite3_step(poStatement))
      {
         // Fetch the number of records in database
         u32DBSize = sqlite3_column_int(poStatement, 0);
      }

      //! Finalize the sqlite statement
      sqlite3_finalize(poStatement);
   } //if (sqlite3_prepare_v2(m_poDevHistorydb,

   //! Check for error returned from sqlite library
   t_String szError = sqlite3_errmsg(m_poDevHistorydb);
   if (szError != coczSqliteNotAnError)
   {
      ETG_TRACE_ERR((" %s  \n", szError.c_str()));
   } // if (szError != coczSqliteNotAnError)

   ETG_TRACE_USR1(("spi_tclDevHistory::u32GetNumOfDevices  u32DbSize = %d\n", u32DBSize));
   return u32DBSize;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::bAddtoHistorydb
 ***************************************************************************/
t_Bool spi_tclDevHistory::bAddtoHistorydb(trEntireDeviceInfo& rfrDevInfo)
{
   ETG_TRACE_USR1(("spi_tclDevHistory::bAddtoHistorydb  u32DeviceHandle = %d\n",
            rfrDevInfo.rDeviceInfo.u32DeviceHandle));

   //! No need to check if Maintain DB results in success or failure
   bMaintainDbSize();

   t_Bool bRetVal = false;

   //! Use SQL mprintf instead of string stream to avoid conflicts in sql query
   //! while using special characters in strings(Ex Phone Name) such as ' " etc.
   t_Char *pczSQLQuery =
            sqlite3_mprintf(
                     "INSERT OR REPLACE INTO DeviceHistory VALUES(%d,'%q', %d,"
                     " '%q','%q', %d, %d, %d, %d, %d, %d, %d,'%q', %d, %d, %d, %d, %d)",
                     rfrDevInfo.rDeviceInfo.u32DeviceHandle,
                     rfrDevInfo.rDeviceInfo.szDeviceName.c_str(),
                     rfrDevInfo.rDeviceInfo.enDeviceCategory,
                     rfrDevInfo.rDeviceInfo.szDeviceModelName.c_str(),
                     rfrDevInfo.rDeviceInfo.szDeviceManufacturerName.c_str(),
                     rfrDevInfo.rDeviceInfo.enDeviceConnectionStatus,
                     rfrDevInfo.rDeviceInfo.enDeviceConnectionType,
                     rfrDevInfo.rDeviceInfo.rVersionInfo.u32MajorVersion,
                     rfrDevInfo.rDeviceInfo.rVersionInfo.u32MinorVersion,
                     rfrDevInfo.rDeviceInfo.bDAPSupport,
                     rfrDevInfo.rDeviceInfo.bSelectedDevice,
                     rfrDevInfo.rDeviceInfo.bDeviceUsageEnabled,
                     rfrDevInfo.rDeviceInfo.szBTAddress.c_str(), rfrDevInfo.u32AccessIndex,
                     rfrDevInfo.bIsUserDeselected,
                     rfrDevInfo.rDeviceInfo.rProjectionCapability.enAndroidAutoSupport,
                     rfrDevInfo.rDeviceInfo.rProjectionCapability.enMirrorlinkSupport,
                     rfrDevInfo.rDeviceInfo.rProjectionCapability.enDeviceType);

   //! Send the query request
   if(NULL != pczSQLQuery)
   {
      bRetVal = bExecuteQuery(pczSQLQuery);
      sqlite3_free(pczSQLQuery);
   }

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::bDeleteFromHistorydb
 ***************************************************************************/
t_Bool spi_tclDevHistory::bDeleteFromHistorydb(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" spi_tclDevHistory::bDeleteFromHistorydb cou32DeviceHandle = 0x%x \n",
            cou32DeviceHandle));

   //! Construct string stream with the device info for adding to database
   std::ostringstream ssDeleteQuery;
   ssDeleteQuery << "DELETE FROM DeviceHistory WHERE u32DeviceHandle ="
            << cou32DeviceHandle ;

   //! Send the query request
   return bExecuteQuery(ssDeleteQuery.str().c_str());
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::bGetDeviceHistoryFromdb
 ***************************************************************************/
t_Bool spi_tclDevHistory::bGetDeviceHistoryFromdb(
         std::vector<trEntireDeviceInfo>& rfrDeviceInfo,
         tenSelModePriority enPriority,
         tenDeviceCategory enDevTypePref,
         tenDeviceConnectionType enConnModePref)
{
   ETG_TRACE_USR1((" spi_tclDevHistory::bGetDeviceHistoryFromdb entered \n"));

   std::ostringstream ssSelectDevHistory;

   //! Assign the second preference for device selection
   tenDeviceConnectionType enConnSecondPref =
            (e8USB_CONNECTED == enConnModePref) ?
                                                  e8WIFI_CONNECTED :
                                                  e8USB_CONNECTED;

   tenDeviceCategory enDevSecondPref =
            (e8DEV_TYPE_MIRRORLINK == enDevTypePref) ?
                                                       e8DEV_TYPE_DIPO :
                                                       e8DEV_TYPE_MIRRORLINK;

   //! Construct Query based on priority( change the order of the database
   //! based on the priority)
   switch (enPriority)
   {
      case e8PRIORITY_DEVICELIST_HISTORY:
         {
         ETG_TRACE_USR4((" %s \n", ssSelectDevHistory.str().c_str()));
         ssSelectDevHistory
         << "SELECT * FROM DeviceHistory ORDER BY u32AccessIndex,"
                  "case when enDeviceConnectionType=" << enConnModePref
                  << " THEN 0 "
                           "when enDeviceConnectionType=" << enConnSecondPref
                  << " THEN 1 end, "
                           "case when enDeviceCategory=" << enDevTypePref
                  << " THEN 0 "
                           "when enDeviceCategory=" << enDevSecondPref
                  << " THEN 1 end";
         break;
      }
      case e8PRIORITY_CONNMODE_PREFERENCE:
         {
         ETG_TRACE_USR4((" %s \n", ssSelectDevHistory.str().c_str()));
         ssSelectDevHistory << "SELECT * FROM DeviceHistory ORDER BY " <<
                  "case when enDeviceConnectionType=" << enConnModePref
                  << " THEN 0 "
                           "when enDeviceConnectionType=" << enConnSecondPref
                  << " THEN 1 end, "
                           "u32AccessIndex,"
                           "case when enDeviceCategory=" << enDevTypePref
                  << " THEN 0 "
                           "when enDeviceConnectionType=" << enDevSecondPref
                  << " THEN 1 end";
         break;
      }

      case e8PRIORITY_DEVICETYPE_PREFERENCE:
         {
         ETG_TRACE_USR4((" %s \n", ssSelectDevHistory.str().c_str()));
         ssSelectDevHistory << "SELECT * FROM DeviceHistory ORDER BY " <<
                  "case when enDeviceCategory=" << enDevTypePref << " THEN 0 "
                           "when enDeviceCategory=" << enDevSecondPref
                  << " THEN 1 end, "
                           "u32AccessIndex,"
                           "case when enDeviceConnectionType=" << enConnModePref
                  << " THEN 0 "
                           "when enDeviceConnectionType=" << enConnSecondPref
                  << " THEN 1 end";
         break;
      }

      default:
      {
         ETG_TRACE_ERR((" Unknown Priority for Selection \n"));
         break;
      }
   }//switch (enPriority)

   return bPopulateDeviceList(rfrDeviceInfo, ssSelectDevHistory.str());
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::bFindDevice
 ***************************************************************************/
t_Bool spi_tclDevHistory::bFindDevice(t_U32 u32Key)
{
   t_Bool bRetFindkey = false;
   sqlite3_stmt *poStatement = NULL;

   //! construct query to select all devices from database matching the key
   t_Char czFindKey[] = "SELECT * FROM DeviceHistory WHERE U32DeviceHandle = ?";

   //! Prepare sqlite statement
   if (sqlite3_prepare_v2(m_poDevHistorydb,  /* Database handle */
            czFindKey,                       /* SQL statement, UTF-8 encoded */
            scos8ReadUntilNullTermination,   /* Maximum length of ssFindKey in bytes. */
            &poStatement,                    /* OUT: Statement handle */
            NULL) == SQLITE_OK)              /* OUT: Pointer to unused portion of ssFindKey */
   {
      //! bind the integer arguments (prevents injection attack on database)
      sqlite3_bind_int(poStatement, scou8FirstParameter, u32Key);

      //! Step through the sqlite query
      //! SQLITE_ROW is returned if a row matches the query
      if (SQLITE_ROW == sqlite3_step(poStatement))
      {
         bRetFindkey = true;
      }

      //! Finalize the sqlite statement
      sqlite3_finalize(poStatement);
   } //if (sqlite3_prepare_v2(m_poDevHistorydb,

   //! Check for error returned from sqlite library
   t_String szError = sqlite3_errmsg(m_poDevHistorydb);
   if (szError != coczSqliteNotAnError)
   {
      bRetFindkey = true;
      ETG_TRACE_ERR((" %s  \n", szError.c_str()));
   } // if (szError != coczSqliteNotAnError)
   ETG_TRACE_USR1(("spi_tclDevHistory::bFindDevice:  u32Key = %d bRetFindkey = %d\n",
            u32Key, ETG_ENUM(BOOL, bRetFindkey)));
   return bRetFindkey;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::vSetSelectedDevice
 ***************************************************************************/
t_Void spi_tclDevHistory::vSetSelectedDevice(const t_U32 cou32DeviceHandle,
         t_Bool bIsDevSelected)
{
   //! Construct query to change bSelectedDevice
   //! column of the requested device
   std::ostringstream ssbIsDeviceSelected;
   ssbIsDeviceSelected << bIsDevSelected;
   vSetValue(cou32DeviceHandle, "bSelectedDevice", ssbIsDeviceSelected.str());

   ETG_TRACE_USR1(("spi_tclDevHistory::vSetSelectedDevice cou32DeviceHandle = 0x%x, "\
            " bIsDevSelected = %d\n", cou32DeviceHandle,ETG_ENUM(BOOL, bIsDevSelected)));
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::vSetDeviceName
 ***************************************************************************/
t_Void spi_tclDevHistory::vSetDeviceName(const t_U32 cou32DeviceHandle, t_String &rfrszDeviceName)
{
   ETG_TRACE_USR1(("spi_tclDevHistory:: vSetDeviceName cou32DeviceHandle =0x%x,  rfrszDeviceName = %s \n",
            cou32DeviceHandle, rfrszDeviceName.c_str()));
   //! Construct query to change DeviceName
   //! column of the requested device
   vSetValue(cou32DeviceHandle, "szDeviceName", rfrszDeviceName);
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::vSetUserDeselectionFlag
 ***************************************************************************/
t_Void spi_tclDevHistory::vSetUserDeselectionFlag(const t_U32 cou32DeviceHandle, t_Bool bState)
{
   ETG_TRACE_USR1(("spi_tclDevHistory:: vSetUserDeselectionFlag cou32DeviceHandle =0x%x,  bState = %d \n",
            cou32DeviceHandle, ETG_ENUM(BOOL, bState)));
   //! Construct query to change bIsUserDeselected
   //! column of the requested device
   std::ostringstream ssbIsUserDeselected;
   ssbIsUserDeselected << bState;
   vSetValue(cou32DeviceHandle, "bIsUserDeselected", ssbIsUserDeselected.str());
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::u32GetSelectedDevice
 ***************************************************************************/
t_U32 spi_tclDevHistory::u32GetLastSelectedDevice()
{
   sqlite3_stmt *poStatement = NULL;

   //! construct query to select all devices from database matching the key
   t_Char czGetLastSelectedDevice[] = "SELECT U32DeviceHandle, MAX(u32AccessIndex) FROM DeviceHistory WHERE  bIsUserDeselected = ?";
   t_U32 u32DeviceHandle = 0;

   //! Prepare sqlite statement
   if (sqlite3_prepare_v2(m_poDevHistorydb,
            czGetLastSelectedDevice,
            scos8ReadUntilNullTermination,
            &poStatement,
            NULL) == SQLITE_OK)
   {
      //! bind the integer arguments (prevents injection attack on database)
      sqlite3_bind_int(poStatement, scou8FirstParameter, 0);

      //! Step through the sqlite query
      //! SQLITE_ROW is returned if a row matches the query
      if (SQLITE_ROW == sqlite3_step(poStatement))
      {
         // Fetch the device handle of the selected device
         u32DeviceHandle = sqlite3_column_int(poStatement, scou8ColDeviceHandle);
      }

      //! Finalize the sqlite statement
      sqlite3_finalize(poStatement);
   } //if (sqlite3_prepare_v2(m_poDevHistorydb,

   //! Check for error returned from sqlite library
   t_String szError = sqlite3_errmsg(m_poDevHistorydb);
   if (szError != coczSqliteNotAnError)
   {
      u32DeviceHandle = 0;
      ETG_TRACE_ERR((" %s  \n", szError.c_str()));
   } // if (szError != coczSqliteNotAnError)

   ETG_TRACE_USR1((" spi_tclDevHistory::u32GetLastSelectedDevice = 0x%x \n", u32DeviceHandle));
   return u32DeviceHandle;
}


/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::vSaveDeviceList
 ***************************************************************************/
t_Void spi_tclDevHistory::vSaveDeviceList(std::map<t_U32, trEntireDeviceInfo> &m_mapDeviceList)
{
	/*lint -esym(40,bIsDeviceUsed)bIsDeviceUsed Undeclared identifier */
	/*lint -esym(40,second) second Undeclared identifier */
	
   ETG_TRACE_USR1((" spi_tclDevHistory::vSaveDeviceList entered \n"));
   std::map<t_U32, trEntireDeviceInfo>::iterator itMapList;
   for (itMapList = m_mapDeviceList.begin();
            itMapList != m_mapDeviceList.end(); itMapList++)
   {
      if (true == itMapList->second.bIsDeviceUsed)
      {
	     trEntireDeviceInfo trEntireDevInfo=itMapList->second;
         bAddtoHistorydb(trEntireDevInfo);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::u32GetMaxAccessIndex
 ***************************************************************************/
t_U32 spi_tclDevHistory::u32GetMaxAccessIndex()
{
   sqlite3_stmt *poStatement = NULL;

   //! construct query to select all devices from database matching the key
   t_Char czGetLastSelectedDevice[] = "SELECT U32DeviceHandle, MAX(u32AccessIndex) FROM DeviceHistory";

   t_U32 u32MaxAccessIndex = 0;

   //! Prepare sqlite statement
   if (sqlite3_prepare_v2(m_poDevHistorydb,
            czGetLastSelectedDevice,
            scos8ReadUntilNullTermination,
            &poStatement,
            NULL) == SQLITE_OK)
   {
      //! Step through the sqlite query
      //! SQLITE_ROW is returned if a row matches the query
      if (SQLITE_ROW == sqlite3_step(poStatement))
      {
         // Fetch the AccessIndex of last selected device
         u32MaxAccessIndex = sqlite3_column_int(poStatement, 1);

         ETG_TRACE_USR1((" spi_tclDevHistory::DeviceHandle = %d \n", sqlite3_column_int(poStatement, 0)));

         ETG_TRACE_USR1((" spi_tclDevHistory::u32GetMaxAccessIndex = %d \n", u32MaxAccessIndex));
      }

      //! Finalize the sqlite statement
      sqlite3_finalize(poStatement);
   } //if (sqlite3_prepare_v2(m_poDevHistorydb,

   //! Check for error returned from sqlite library
   t_String szError = sqlite3_errmsg(m_poDevHistorydb);
   if (szError != coczSqliteNotAnError)
   {
      u32MaxAccessIndex = 0;
      ETG_TRACE_ERR((" %s  \n", szError.c_str()));
   } // if (szError != coczSqliteNotAnError)

   ETG_TRACE_USR1((" spi_tclDevHistory::u32GetMaxAccessIndex = %d \n", u32MaxAccessIndex));
   return u32MaxAccessIndex;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::vSetDeviceUsagePreference
 ***************************************************************************/
t_Void spi_tclDevHistory::vSetDeviceUsagePreference(
      const t_U32 cou32DeviceHandle, tenEnabledInfo enEnabledInfo)
{
   ETG_TRACE_USR1((" spi_tclDevHistory::vSetDeviceUsagePreference = 0x%x enEnabledInfo = %d \n",
         cou32DeviceHandle,  ETG_ENUM(ENABLED_INFO,enEnabledInfo)));
   //! Construct query to change DeviceUsagePreference
   //! column of the requested device
   std::ostringstream ssbIsDeviceUsageEnabled;
   t_Bool bState = (e8USAGE_ENABLED == enEnabledInfo);
   ssbIsDeviceUsageEnabled << bState;
   vSetValue(cou32DeviceHandle, "bDeviceUsageEnabled", ssbIsDeviceUsageEnabled.str());
}

/***************************************************************************
 *********************************PRIVATE **********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::vDisplayDevHistorydb
 ***************************************************************************/
t_Bool spi_tclDevHistory::bCreateDevHistoryTable()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Construct Query to create table for Device History
   t_String szDeviceHistoryTable =
            " CREATE TABLE IF NOT EXISTS DeviceHistory(                                            \
                        U32DeviceHandle            INT PRIMARY KEY     NOT NULL,                   \
                        szDeviceName               TEXT                NOT NULL,                   \
                        enDeviceCategory           INT,                                            \
                        szDeviceModelName          TEXT                NOT NULL,                   \
                        szDeviceMaunufacturerName  TEXT                NOT NULL,                   \
                        enDeviceConnectionStatus   INT,                                            \
                        enDeviceConnectionType     INT,                                            \
                        trVersionMajor             INT,                                            \
                        trVersionMinor             INT,                                            \
                        bDAPSupport                INT,                                            \
                        bSelectedDevice            INT,                                            \
                        bDeviceUsageEnabled        INT,                                            \
                        szBTAddress                INT,                                            \
                        u32AccessIndex             INT,                                            \
                        bIsUserDeselected          INT,                                            \
                        bAAPSupport                INT,                                            \
                        bMLSupport                 INT,                                            \
                        enDeviceType               INT                                             \
                        )";


   //! Send the query request
   return bExecuteQuery(szDeviceHistoryTable.c_str());
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::vDisplayDevHistorydb
 ***************************************************************************/
t_Void spi_tclDevHistory::vDisplayDevHistorydb()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Construct Query to fetch device history table
   t_String szDisplayQuery = "SELECT * FROM DeviceHistory;";

   //! Send the query request
   bExecuteQuery(szDisplayQuery, &spi_tclDevHistory::s32DisplayDevHistoryCb);
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::bExecuteQuery
 ***************************************************************************/
t_Bool spi_tclDevHistory::bExecuteQuery(t_String szQuery,
         fs32SqliteCallback* pfSqliteCb)
{
   t_Bool bRetExecQuery = false;

   if (true == m_bisDBOpen)
   {
      //! Pointer to sqlite error message
      char *pczErrorMsg = NULL;

      //! Execute the requested query
      t_U32 u32ExecRet = sqlite3_exec(m_poDevHistorydb, //Database
               szQuery.c_str(),                       // Requested query
               pfSqliteCb,                            // Callback function
               NULL,                                  //  1st argument to callback (NOT USED)
               &pczErrorMsg);                         // Error message pointer

      //! Check for error returned from sqlite library
      if ((u32ExecRet != SQLITE_OK) && (NULL != pczErrorMsg))
      {
         ETG_TRACE_ERR(("bExecuteQuery::SQL error: %s\n", pczErrorMsg));
         //! free memory allocated by sqlite for error message
         sqlite3_free(pczErrorMsg);
      }
      else
      {
         bRetExecQuery = true;
      } //if ((u32ExecRet != SQLITE_OK) && (NULL != pczErrorMsg))
   } // if (true == m_bisDBOpen)
   ETG_TRACE_USR4((" spi_tclDevHistory::bExecuteQuery bRetExecQuery = %d  \n",
            ETG_ENUM(BOOL,bRetExecQuery)));
   return bRetExecQuery;
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::vSetValue
 ***************************************************************************/
t_Void spi_tclDevHistory::vSetValue(const t_U32 cou32DeviceHandle,
         t_String szColName, t_String szColValue)
{
   ETG_TRACE_USR2(("spi_tclDevHistory::vSetValue: cou32DeviceHandle =0x%x, szColName = %s ",
            cou32DeviceHandle, szColName.c_str()));
   ETG_TRACE_USR2(("szColValue =%s \n", szColValue.c_str()));

   t_Char *pczSQLQuery = sqlite3_mprintf("UPDATE DeviceHistory SET %q = %q WHERE u32DeviceHandle = %d",
            szColName.c_str(), szColValue.c_str(), cou32DeviceHandle);

   //! Send the query request
   if(NULL != pczSQLQuery)
   {
      bExecuteQuery(pczSQLQuery);
      sqlite3_free(pczSQLQuery);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclDevHistory::bPopulateDeviceList
 ***************************************************************************/
t_Bool spi_tclDevHistory::bPopulateDeviceList(
         std::vector<trEntireDeviceInfo> &rfrDeviceInfo,
         t_String szDevListQuery)
{
   ETG_TRACE_USR1((" spi_tclDevHistory::bPopulateDeviceList entered \n"));
   t_Bool bRetVal = true;
   sqlite3_stmt *poStatement = NULL;

   //! Prepare sqlite statement
   if (sqlite3_prepare_v2(m_poDevHistorydb,
            szDevListQuery.c_str(),
            scos8ReadUntilNullTermination,
            &poStatement,
            NULL) == SQLITE_OK)
   {
      //! Step through the sqlite query
      //! SQLITE_ROW is returned if a row matches the query
      while (SQLITE_ROW == sqlite3_step(poStatement))
      {
         //! construct device info from the database
         trEntireDeviceInfo rDevInfo;
         rDevInfo.rDeviceInfo.u32DeviceHandle = sqlite3_column_int(poStatement,
                  scou8ColDeviceHandle);
         rDevInfo.rDeviceInfo.szDeviceName =
                  (const char*) sqlite3_column_text(poStatement, scou8ColDeviceName);
         rDevInfo.rDeviceInfo.enDeviceCategory =
                  (tenDeviceCategory) sqlite3_column_int(poStatement, scou8ColDeviceCategory);
         rDevInfo.rDeviceInfo.szDeviceModelName =
                  (const char*) sqlite3_column_text(poStatement, scou8ColDeviceModelName);
         rDevInfo.rDeviceInfo.szDeviceManufacturerName =
                  (const char*) sqlite3_column_text(poStatement, scou8ColDeviceManufacturerName);
         rDevInfo.rDeviceInfo.enDeviceConnectionStatus = e8DEV_NOT_CONNECTED;
         rDevInfo.rDeviceInfo.enDeviceConnectionType =
                  (tenDeviceConnectionType) sqlite3_column_int(poStatement, scou8ColDeviceConnectionType);
         rDevInfo.rDeviceInfo.rVersionInfo.u32MajorVersion =
                  sqlite3_column_int(poStatement, scou8ColMajorVersion);
         rDevInfo.rDeviceInfo.rVersionInfo.u32MinorVersion =
                  sqlite3_column_int(poStatement, scou8ColMinorVersion);
         rDevInfo.rDeviceInfo.bDAPSupport =
                  sqlite3_column_int(poStatement, scou8ColDAPSupport);
         rDevInfo.rDeviceInfo.bSelectedDevice = 0;
         rDevInfo.rDeviceInfo.bDeviceUsageEnabled =
               sqlite3_column_int(poStatement, scou8ColDeviceUsed);
         rDevInfo.rDeviceInfo.szBTAddress = (const char*) sqlite3_column_text(poStatement, scou8ColBTAddress);
         rDevInfo.u32AccessIndex =
                  sqlite3_column_int(poStatement, scou8ColAccessIndex);
         //! Donot restore user device deselection info across powercycles
         rDevInfo.bIsUserDeselected = false;
         rDevInfo.rDeviceInfo.rProjectionCapability.enAndroidAutoSupport =
                  (tenSPISupport) sqlite3_column_int(poStatement, scou8ColAAPSupport);
         rDevInfo.rDeviceInfo.rProjectionCapability.enMirrorlinkSupport =
                  (tenSPISupport) sqlite3_column_int(poStatement, scou8ColMLSupport);
         rDevInfo.rDeviceInfo.rProjectionCapability.enDeviceType =
                  (tenDeviceType) sqlite3_column_int(poStatement, scou8ColDevicetype);
         rfrDeviceInfo.push_back(rDevInfo);
      }      // while (SQLITE_ROW == sqlite3_step(poStatement))

      //! Finalize the sqlite statement
      sqlite3_finalize (poStatement);
   }      //if (sqlite3_prepare_v2(m_poDevHistorydb,...

   //! Check for error returned from sqlite library
   t_String szError = sqlite3_errmsg(m_poDevHistorydb);
   if (szError != coczSqliteNotAnError)
   {
      bRetVal = false;
      ETG_TRACE_ERR(("spi_tclDevHistory::bPopulateDeviceLis: Error: %s  \n", szError.c_str()));
   } // if (szError != coczSqliteNotAnError)
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclDevHistory::bMaintainDbSize
 ***************************************************************************/
t_Bool spi_tclDevHistory::bMaintainDbSize()
{
   t_Bool bRetVal = false;
   t_U32 u32DBSize = u32GetNumOfDevices();

   //! Fetch the project specific Device History size
   spi_tclConnSettings *poConnSettings = spi_tclConnSettings::getInstance();
   t_U32 u32DevHistSize = 0;
   if(NULL != poConnSettings)
   {
      u32DevHistSize = poConnSettings->u32GetDeviceHistorySize();
   }// if(NULL != poConnSettings)

   if(u32DBSize >= u32DevHistSize)
   {
      //! construct query to delete the oldest entry
      std::ostringstream ssQueryDelEntry;
      ssQueryDelEntry << "DELETE FROM DeviceHistory WHERE u32AccessIndex=(SELECT MIN(u32AccessIndex) FROM DeviceHistory)";
      bRetVal = bExecuteQuery(ssQueryDelEntry.str().c_str());
   }

   ETG_TRACE_USR1(("bMaintainDbSize  u32DBSize = %d u32DevHistSize = %d bRetVal = %d  \n",
            u32DBSize, u32DevHistSize,ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  static t_S32 spi_tclDevHistory::s32DisplayDevHistoryCb
 ***************************************************************************/
t_S32 spi_tclDevHistory::s32DisplayDevHistoryCb(t_Void *pvNotUsed,
         t_S32 s32ArgCountMax, t_Char **ppczArgv, t_Char **ppczColName)
{
   SPI_INTENTIONALLY_UNUSED(pvNotUsed);
   ETG_TRACE_USR1((" Device History \n"));
   //! Check for negative argument count as it is a signed value
   if (s32ArgCountMax > 0)
   {
      for (t_S32 s32ArgCount = 0; s32ArgCount < s32ArgCountMax; s32ArgCount++)
      {
         //! Print the values returned by sqlite
         if ((NULL != ppczColName) && (NULL != ppczArgv))
         {
            std::ostringstream ssDispValue;
            ssDispValue << ppczColName[s32ArgCount] << "\t" ;
            ssDispValue << (ppczArgv[s32ArgCount] ? ppczArgv[s32ArgCount] : "NULL") ;
            ETG_TRACE_USR4((" %s \n ", ssDispValue.str().c_str()));
         } // if ((NULL != ppczColName) && (NULL != ppczArgv))
      } //for (t_S32 s32ArgCount = 0; s32ArgCount < s32ArgCountMax; s32ArgCount++)
      ETG_TRACE_USR4(("\n"));
   } // if (s32ArgCountMax > 0)
   return 0;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclDevHistory::bCheckPathValidity
 ***************************************************************************/
t_Bool spi_tclDevHistory::bCheckPathValidity(t_String &rfrszStorageDir)
{
   t_Bool bRetVal = true;

   //! Check if the directory is valid
   DirHandler oDirHandler(rfrszStorageDir.c_str());
   if (false == oDirHandler.bIsValid())
   {
      ETG_TRACE_USR2(("spi_tclDevHistory::bCheckPathValidity: Directory not" \
               " present. Creating .. %s\n", rfrszStorageDir.c_str()));
      //! Try creating directory . (Recursive creation not attempted)
      DirHandler oNewDir("/");
      bRetVal = oNewDir.bMkDir(rfrszStorageDir.c_str());
   }

   ETG_TRACE_USR1((" spi_tclDevHistory::bCheckPathValidity bRetVal=%d \n",ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}

//lint -restore
