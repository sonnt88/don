/*!
 *******************************************************************************
 * \file             spi_tclConnection.cpp
 * \brief            Base Connection class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class for Connection classes. Provides basic connection
 interface to be implemented by derived classes
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.01.2013 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclConnection.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclConnection.cpp.trc.h"
#endif
#endif

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::spi_tclConnection
 ***************************************************************************/
spi_tclConnection::spi_tclConnection()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::~spi_tclConnection
 ***************************************************************************/
spi_tclConnection::~spi_tclConnection()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::bInitializeConnection
 ***************************************************************************/
t_Bool spi_tclConnection::bInitializeConnection()
{
   ETG_TRACE_USR1((" %s not implemented in derived class \n", __PRETTY_FUNCTION__));

   return false;
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::vUnInitializeConnection
 ***************************************************************************/
t_Void spi_tclConnection::vUnInitializeConnection()
{
   ETG_TRACE_USR1((" %s not implemented in derived class \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::vOnAddDevicetoList
 ***************************************************************************/
t_Void spi_tclConnection::vOnAddDevicetoList(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" %s not implemented in derived class \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);

}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::bOnServiceONOFF
 ***************************************************************************/

t_Void spi_tclConnection::bOnServiceONOFF()
{
   ETG_TRACE_USR1((" %s not implemented in derived class \n", __PRETTY_FUNCTION__));

}
