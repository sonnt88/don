/*!
 *******************************************************************************
 * \file             spi_tclDeviceSelector.h
 * \brief            Handles select and deselection of device
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Handles select and deselection of device
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 16.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 10.12.2014 | Shihabudheen P M             | Changed for blocking device usage 
                                             preference updates during 
                                             select/deselect is in progress. 

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLDEVICESELECTOR_H_
#define SPI_TCLDEVICESELECTOR_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_ConnMngrTypeDefines.h"
#include "Timer.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

//! Forward declarations
class spi_tclMediator;
class spi_tclDevSelResp;

/*!
 * \class spi_tclConnection
 * \brief Handles selection  and deselection of device
 */

class spi_tclDeviceSelector
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::spi_tclDeviceSelector
       ***************************************************************************/
      /*!
       * \fn     spi_tclDeviceSelector(spi_tclDevSelResp *poRespInterface)
       * \brief  Parameterized Constructor
       * \param  poRespInterface: Pointer to response interface class
       * \sa      ~spi_tclDeviceSelector()
       **************************************************************************/
      spi_tclDeviceSelector(spi_tclDevSelResp *poRespInterface);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::~spi_tclDeviceSelector
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclDeviceSelector()
       * \brief  Destructor
       * \sa     spi_tclDeviceSelector()
       **************************************************************************/
      virtual ~spi_tclDeviceSelector();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclDeviceSelector::bInitialize()
       ***************************************************************************/
      /*!
       * \fn      bInitialize()
       * \brief   Method to Initialize Device selector: Register for callbacks
       * \retval  true: if the initialization of Device selector is successful,
       *          false: on failure
       * \sa      bUnInitialize()
       **************************************************************************/
      virtual t_Bool bInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclDeviceSelector::vUnInitialize()
       ***************************************************************************/
      /*!
       * \fn      vUnInitialize()
       * \brief   Method to UnInitialize Device selector: Unregister callbacks.
       * \sa      bInitialize()
       **************************************************************************/
      virtual t_Void vUnInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclDeviceSelector::vLoadSettings(const trSpiFeatureSupport&...)
       ***************************************************************************/
      /*!
       * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
       * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
       * \sa      vSaveSettings()
       **************************************************************************/
      virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclDeviceSelector::vSaveSettings()
       ***************************************************************************/
      /*!
       * \fn      vSaveSettings()
       * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
       * \sa      vLoadSettings()
       **************************************************************************/
      virtual t_Void vSaveSettings();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vSelectDevice
       ***************************************************************************/
      /*!
       * \fn     vSelectDevice()
       * \brief  Interface to select or deselect a particular device
       *         specified by device handle
       * \param  cou32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevConnType   : Identifies the Connection Type.
       * \param  enDevConnReq    : Identifies the Connection Request.
       * \param  enDAPUsage      : Identifies Usage of DAP for the selected ML device.
       *              This value is not considered for de-selection of device.
       * \param  enCDBUsage      : Identifies Usage of CDB for the selected ML device.
       *              This value is not considered for de-selection of device
       * \param  bIsHMITrigger   : true if HMI has triggered select device
       * \param  corUsrCntxt      : User Context Details.
       **************************************************************************/
      t_Void vSelectDevice(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceConnectionReq enDevConnReq,
               tenEnabledInfo enDAPUsage,
               tenEnabledInfo enCDBUsage,
               tenDeviceCategory enDevCategory,
               t_Bool bIsHMITrigger,
               const trUserContext corUsrCntxt);


      /***************************************************************************
      ** FUNCTION: t_U32 spi_tclDeviceSelector::u32GetSelectedDeviceHandle()
      ***************************************************************************/
      /*!
      * \fn     u32GetSelectedDeviceHandle()
      * \brief  It provides the Device Handle of currently selected device, if any.
      *         If no device is selected, returns 0xFFFFFFFF.
      * \return Device Handle of Selected Device.
      **************************************************************************/
      t_U32 u32GetSelectedDeviceHandle() const;

     /***************************************************************************
      ** FUNCTION: t_U32 spi_tclDeviceSelector::u32GetSelectedDeviceId()
      ***************************************************************************/
      /*!
      * \fn     u32GetSelectedDeviceId()
      * \brief  It provides the Device Handle of currently selected device, if any.
      *         If no device is selected, returns 0xFFFFFFFF.
      * \return Device Handle of Selected Device.
      **************************************************************************/
      static t_U32 u32GetSelectedDeviceId();

      /***************************************************************************
       ** FUNCTION:  t_Void vSetDeviceSelectionMode()
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetDeviceSelectionMode(
       * \brief   Method to set the device selection mode to automatic/manual. Changes
       *          will take into effect on successive connection
       * \param   enSelectionMode : Device selection mode @see tenDeviceSelectionMode
       * \retval  t_Void
       **************************************************************************/
      t_Void vSetDeviceSelectionMode(tenDeviceSelectionMode enSelectionMode);

   private:

      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      /***************************************************************************
      ** FUNCTION: spi_tclDeviceSelector(const spi_tclDeviceSelector &corfobjRhs)
      ***************************************************************************/
      /*!
      * \fn      spi_tclDeviceSelector(const spi_tclDeviceSelector &corfobjRhs)
      * \brief   Copy constructor not implemented hence made protected to prevent
      *          misuse
      **************************************************************************/
      spi_tclDeviceSelector(const spi_tclDeviceSelector &corfobjRhs);

      /***************************************************************************
      ** FUNCTION: const spi_tclDeviceSelector & operator=(const spi_tclDeviceSelector &corfobjRhs);
      ***************************************************************************/
      /*!
      * \fn      const spi_tclDeviceSelector & operator=(const spi_tclDeviceSelector &corfobjRhs);
      * \brief   assignment operator not implemented hence made protected to
      *          prevent misuse
      **************************************************************************/
      const spi_tclDeviceSelector & operator=(
         const spi_tclDeviceSelector &corfobjRhs);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  Registers callbacks with mediator
       **************************************************************************/
      t_Void vRegisterCallbacks();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vValidateSelectDeviceRequest
       ***************************************************************************/
      /*!
       * \fn     vValidateSelectDeviceRequest
       * \brief  validates the selected device request
       * \param  cou32DeviceHandle : Device handle for which the device selection
       *         has to be validated
       * \param  enDevConnReq: Specifies whether the request was received for device
       *         selection or deselection
       * \param  rfenErrorCode: [OUT] returns the error code for the select device
       *         request
       * \retval true: if the device selection request id valid else false
       **************************************************************************/
      t_Bool bValidateSelectDeviceRequest(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionReq enDevConnReq, tenErrorCode &rfenErrorCode,
               tenDeviceCategory enDevCat = e8DEV_TYPE_UNKNOWN);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vReceiveSelectDeviceResCb
       ***************************************************************************/
      /*!
       * \fn     vReceiveSelectDeviceResCb()
       * \brief  Callback to receive response for Select Device
       * \param  enCompID: Component ID which posts select device result
       * \param  bSelDevRes: true if Selecting Device by enCompID is success.
       *         otherwise false
       **************************************************************************/
      t_Void vReceiveSelectDeviceResCb(tenCompID enCompID, tenErrorCode enErrorCode);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vDeviceDisconnectionCb
       ***************************************************************************/
      /*!
       * \fn     vDeviceDisconnectionCb()
       * \brief  Callback function for Device Disconnection
       * \param  cou32DeviceHandle: Device handle of the disconnected device
       **************************************************************************/
      t_Void vDeviceDisconnectionCb(const t_U32 cou32DeviceHandle) ;

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vDeviceConnectionCb
       ***************************************************************************/
      /*!
       * \fn     vDeviceConnectionCb()
       * \brief  Callback function for Device Connection
       * \param  cou32DeviceHandle: Device handle of the Connected device
       **************************************************************************/
      t_Void vDeviceConnectionCb(const t_U32 cou32DeviceHandle) ;

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vOnAutomaticDeviceSelection
       ***************************************************************************/
      /*!
       * \fn     vOnAutomaticDeviceSelection()
       * \brief  Called when a device is selected or deselected automatically
       * \param  cou32DeviceHandle: Device handle
       * \param  enDevConnReq: informs if the device has to be selected or unselected
       * \param  enDeviceCategory: Device category
       **************************************************************************/
      t_Void vOnAutomaticDeviceSelection(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionReq enDevConnReq, tenDeviceCategory enDevCategory = e8DEV_TYPE_UNKNOWN);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vApplySelectionStrategy
       ***************************************************************************/
      /*!
       * \fn     vApplySelectionStrategy()
       * \brief  Called to trigger device selection automatically
       **************************************************************************/
      t_Void vApplySelectionStrategy();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vOnSetUserDeselect
       ***************************************************************************/
      /*!
       * \fn     vOnSetUserDeselect()
       * \brief  Called when a device is deselected due to user action
       * \param  cou32DeviceHandle: Device handle
       **************************************************************************/
      t_Void vOnSetUserDeselect(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vSendDeviceSelectionReq
       ***************************************************************************/
      /*!
       * \fn     vSendDeviceSelectionReq()
       * \brief  Sends select device call to other SPI components
       * \param  corfrSelDevinfo : Structure containing information about the device
       *                           to be selected
       * \param  enCompID: Component ID which posts select device result
       **************************************************************************/
      t_Void vSendDeviceSelectionReq(const trDeviceSelectionInfo &corfrSelDevinfo,
               tenCompID enCompID);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vEvaluateDeviceSelectionReq
       ***************************************************************************/
      /*!
       * \fn     vEvaluateDeviceSelectionReq()
       * \brief  Checks if some other device is selected. If so first deselect the
       *         already selected device and then send select device request
       **************************************************************************/
      t_Void vEvaluateDeviceSelectionReq();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::bDeviceSelectionComplete
       ***************************************************************************/
      /*!
       * \fn     bDeviceSelectionComplete()
       * \brief  Evaluates whether the select device request is complete or not
       * \param  cou32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevConnReq    : Identifies the Connection Request.
       * \param  enErrorCode     : Returns the error code
       * \retval true: if the processing of select device request is complete.
       *         false: If further actions are needed to complete current select
       *                device request
       **************************************************************************/
      t_Bool bDeviceSelectionComplete(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionReq enDevConnReq, tenErrorCode enErrorCode);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vSendSelectDeviceResult
       ***************************************************************************/
      /*!
       * \fn     vSendSelectDeviceResult()
       * \brief  Sends select device result to interested SPI components
       * \param  corfrSelDevinfo : Structure containing information about the device
       *                           to be selected
       * \param  cou32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevConnReq    : Identifies the Connection Request.
       * \param  enErrorCode     : Returns the error code
       * \param  bIsHMITrigger   : true if HMI has triggered select device
       * \param  corUsrCntxt     : User Context Details.
       **************************************************************************/
      t_Void vSendSelectDeviceResult(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionReq enDevConnReq, tenErrorCode enErrorCode,
               t_Bool bIsHMITrigger,
               const trUserContext &corfrUsrCtxt);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vSendSelectDeviceResultToHMI
       ***************************************************************************/
      /*!
       * \fn     vSendSelectDeviceResultToHMI()
       * \brief  Sends select device result to HMI
       * \param  corfrSelDevinfo : Structure containing information about the device
       *                           to be selected
       * \param  cou32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevConnReq    : Identifies the Connection Request.
       * \param  enErrorCode     : Returns the error code
       * \param  bIsHMITrigger   : true if HMI has triggered select device
       * \param  corUsrCntxt     : User Context Details.
       **************************************************************************/
      t_Void vSendSelectDeviceResultToHMI(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionReq enDevConnReq, tenErrorCode enErrorCode,
               t_Bool bIsHMITrigger,
               const trUserContext &corfrUsrCtxt);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vStartSelectionTimer
       ***************************************************************************/
      /*!
       * \fn     vStartSelectionTimer
       * \brief  Starts timer on starup to restore the last selected device
       **************************************************************************/
      t_Void vStartSelectionTimer();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vStartComponentResponseTimer
       ***************************************************************************/
      /*!
       * \fn     vStartComponentResponseTimer
       * \brief  Starts timer to monitor response from SPI components
       **************************************************************************/
      t_Void vStartComponentResponseTimer();

      /***************************************************************************
      ** FUNCTION:  spi_tclDeviceSelector::vUpdateDevSelectorBusyStatus(t_Bool)
      ***************************************************************************/
      /*!
      * \fn     vUpdateDevSelectorBusyStatus
      * \brief  Starts timer to monitor response from SPI components
      * \param  bSelectorStatus :[IN] True if selector is busy, false otherwise.
      **************************************************************************/
      t_Void vUpdateDevSelectorBusyStatus(t_Bool bSelectorStatus);
	  
      /***************************************************************************
      ** FUNCTION:  spi_tclDeviceSelector::enGetNextComponentID(t_Bool)
      ***************************************************************************/
      /*!
      * \fn     enGetNextComponentID
      * \brief  Provides the next component to be selected 
      * \param  enDeviceCat   : Device category
      * \param  enDevConnReq   : Device selection request
      * \param  enCurrentCompID:ID of Component whose selection has just completed
      **************************************************************************/
      tenCompID enGetNextComponentID(tenDeviceCategory enDeviceCat,
                    tenDeviceConnectionReq enDevConnReq, tenCompID enCurrentCompID);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vLoadSelectionSequence
       ***************************************************************************/
      /*!
       * \fn     vLoadSelectionSequence
       * \brief  Loads device selector selection sequence. to be called on loadsettings
       **************************************************************************/
      t_Void vLoadSelectionSequence();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vLoadDeselectionSequence
       ***************************************************************************/
      /*!
       * \fn     vLoadDeselectionSequence
       * \brief  Loads device selector deselection sequence. to be called on loadsettings
       **************************************************************************/
      t_Void vLoadDeselectionSequence();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vOnAutomaticSelectionFailure
       ***************************************************************************/
      /*!
       * \fn     vOnAutomaticSelectionFailure
       * \brief  Called when internal device selection fails 
       * \param  cou32DeviceID: Device id
       * \param enDeviceCat: Device category 
       **************************************************************************/
      t_Void vOnAutomaticSelectionFailure(const t_U32 cou32DeviceID, tenDeviceCategory enDeviceCat);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vLoadDeselectionSequence
       ***************************************************************************/
      /*!
       * \fn     enGetNextSwitchType
       * \brief  returns the next device switch to be performed
       * \param  cou32DeviceID: Device id
       **************************************************************************/
      tenDeviceCategory enGetNextSwitchType(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::u32GetNextDeviceForSelection
       ***************************************************************************/
      /*!
       * \fn     u32GetNextDeviceForSelection
       * \brief  Provides next device for selection
       * \param  cou32DeviceID: Device id
       **************************************************************************/
      t_U32 u32GetNextDeviceForSelection(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::vEvaluateSPIPreference
       ***************************************************************************/
      /*!
       * \fn     vEvaluateSPIPreference
       * \brief  Evaluates the SPI technology to which the device selection has to be applied
       * \param  cou32DeviceID: Device id
       **************************************************************************/
      t_Void vEvaluateSPIPreference(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::bSelectionTimerCb
       ***************************************************************************/
      /*!
       * \fn     bSelectionTimerCb
       * \brief  called on expiry of selection timer
       * \param  rTimerID: ID of the timer which has expired
       * \param  pvObject: pointer to object passed while starting the timer
       * \param  pvUserData: data passed during start of the timer
       **************************************************************************/
      static t_Bool bSelectionTimerCb(timer_t rTimerID, t_Void *pvObject,
               const t_Void *pvUserData);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceSelector::bCompRespTimerCb
       ***************************************************************************/
      /*!
       * \fn     bCompRespTimerCb
       * \brief  called on expiry of component response timer
       * \param  rTimerID: ID of the timer which has expired
       * \param  pvObject: pointer to object passed while starting the timer
       * \param  pvUserData: data passed during start of the timer
       **************************************************************************/
      static t_Bool bCompRespTimerCb(timer_t rTimerID, t_Void *pvObject,
               const t_Void *pvUserData);

      //! pointer to Mediator for posting messages
      spi_tclMediator *m_poMediator;

      //! pointer to Device selection response
      spi_tclDevSelResp *m_poDevSelResp;

      //! Device to be deselected
      t_Bool m_bDeviceSwitchRequest;

      //! Currently selected device
      static t_U32 m_u32CurrSelectedDevice;

      //! Store current select device request
      trDeviceSelectionInfo m_rSelectDevReqInfo;

      //! Store info of device to be deselected
      trDeviceSelectionInfo m_rDeselectDevReqInfo;

      //! Set when busy with another request for device selection
      t_Bool m_bIsDevSelectorBusy;

      //! Flag to indicate if startup timer is running
      t_Bool m_bIsSelectionTimerRunnning;

      //! Stores the current component ID
      tenCompID m_enSelectedComp;

      //! Stores the component response timer ID
      timer_t m_rCompRespTimerID;

      //! Stores selection sequence
      std::map<tenDeviceCategory, std::vector<tenCompID>> m_mapSelectionSequence;

      //! Stores deselection sequence
      std::map<tenDeviceCategory, std::vector<tenCompID>> m_mapDeselectionSequence;

      //! Current selection mode set by HMI
      tenDeviceSelectionMode m_enDeviceSelectionMode;
};

/*! } */
#endif /* SPI_TCLDEVICESELECTOR_H_ */
