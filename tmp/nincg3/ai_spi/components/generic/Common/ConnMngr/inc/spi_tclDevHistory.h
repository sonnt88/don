
/*!
 *******************************************************************************
 * \file             spi_tclDevHistory.h
 * \brief            Handles Device History
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Handles storage of Device History for SPI Devices
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/


#ifndef SPITCLDEVHISTORYDB_H_
#define SPITCLDEVHISTORYDB_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include <map>
#include "Lock.h"
#include <sqlite3.h>
#include "spi_ConnMngrTypeDefines.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

//! Callback function in the format required for sqlite3 library
typedef t_S32 fs32SqliteCallback(t_Void *pvNotUsed, t_S32 u32ArgCountMax,
         t_Char **ppczArgv, t_Char **ppczColName);

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclDevHistory
 * \brief Handles storage of Device History for SPI Devices
 */

class spi_tclDevHistory
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::spi_tclDevHistory
       ***************************************************************************/
      /*!
       * \fn     spi_tclDevHistory()
       * \brief  Default Constructor
       * \sa      ~spi_tclDevHistory()
       **************************************************************************/
      spi_tclDevHistory();


      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::~spi_tclDevHistory
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclDevHistory()
       * \brief   Destructor
       * \sa     spi_tclDevHistory()
       **************************************************************************/
      ~spi_tclDevHistory();

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::u32GetNumOfDevices
       ***************************************************************************/
      /*!
       * \fn     u32GetNumOfDevices
       * \brief  returnd number of devices maintained in database
       * \retval No of devices in database
       **************************************************************************/
      t_U32 u32GetNumOfDevices();


      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::bAddtoHistorydb
       ***************************************************************************/
      /*!
       * \fn     bAddtoHistorydb
       * \brief  Adds device to History maintained in database
       * \param rfrDevInfo: Device Info of the device to be added to history
       **************************************************************************/
      t_Bool bAddtoHistorydb(trEntireDeviceInfo &rfrDevInfo);


      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::bDeleteFromHistorydb
       ***************************************************************************/
      /*!
       * \fn     bDeleteFromHistorydb
       * \brief  Deletes device from History maintained in database
       * \param cou32DeviceHandle: Device Handle of the device to be deleted from history
       **************************************************************************/
      t_Bool bDeleteFromHistorydb(const t_U32 cou32DeviceHandle);


      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::bGetDeviceHistoryFromdb
       ***************************************************************************/
      /*!
       * \fn     bGetDeviceHistoryFromdb
       * \brief  Adds device to History maintained in database
       * \param  rfrDeviceInfo:[OUT] Container to be populated with device history
       * \param  enPriority: Priority for device selection
       * \param  enDevTypePref: Preferred device type
       * \param  enConnModePref: Preferred connection mode
       **************************************************************************/
      t_Bool bGetDeviceHistoryFromdb(std::vector<trEntireDeviceInfo> &rfrDeviceInfo,
               tenSelModePriority enPriority = e8PRIORITY_DEVICELIST_HISTORY,
               tenDeviceCategory enDevTypePref = e8DEV_TYPE_DIPO,
               tenDeviceConnectionType enConnModePref = e8USB_CONNECTED);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::vDisplayDevHistorydb
       ***************************************************************************/
      /*!
       * \fn     vDisplayDevHistorydb
       * \brief  Displays Device History
       **************************************************************************/
      t_Void vDisplayDevHistorydb();

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::bFindDevice
       ***************************************************************************/
      /*!
       * \fn     bFindDevice
       * \brief  Returns true if the key specified is found in the database
       * \param  u32Key: Key to be searched for in database
       * \retval : true if found in database
       **************************************************************************/
      t_Bool bFindDevice(t_U32 u32Key);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::vSetSelectedDevice
       ***************************************************************************/
      /*!
       * \fn     vSetSelectedDevice
       * \brief  Sets the selected device
       * \param  cou32DeviceHandle: Device handle of the device to be selected
       * \param  bIsDevSelected: true if the device is the selected device
       **************************************************************************/
      t_Void vSetSelectedDevice(const t_U32 cou32DeviceHandle,
               t_Bool bIsDevSelected);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::vSetDeviceName
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceName
       * \brief  Sets the device name of the device identified by device handle
       * \param cou32DeviceHandle: Device Handle of the device to be added to history
       * \param rfrszDeviceName : Device Name to be set
       **************************************************************************/
      t_Void vSetDeviceName(const t_U32 cou32DeviceHandle, t_String &rfrszDeviceName);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::vSetUserDeselectionFlag
       ***************************************************************************/
      /*!
       * \fn     vSetUserDeselectionFlag
       * \brief  Sets the flag when user deselects the device from HMI
       * \param  cou32DeviceHandle :Device Handle
       * \param  bState : indicates the value of UserDeselectionFlag
       **************************************************************************/
      t_Void vSetUserDeselectionFlag(const t_U32 cou32DeviceHandle, t_Bool bState);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::u32GetSelectedDevice
       ***************************************************************************/
      /*!
       * \fn     vSetSelectedDevice
       * \brief  Returns the selected device
       * \retval Device handle of the selected device
       **************************************************************************/
      t_U32 u32GetLastSelectedDevice();

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::vSaveDeviceList
       ***************************************************************************/
      /*!
       * \fn     vSaveDeviceList
       * \brief  Write the current device list to history. to be called at the
       *         end of power cycle
       * \param  m_mapDeviceList: map containing the current device list
       **************************************************************************/
      t_Void vSaveDeviceList(std::map<t_U32, trEntireDeviceInfo> &m_mapDeviceList);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::u32GetMaxAccessIndex
       ***************************************************************************/
      /*!
       * \fn     u32GetMaxAccessIndex
       * \brief  returnd maximum AccessIndex in the database
       * \retval maximum AccessIndex in the database
       **************************************************************************/
      t_U32 u32GetMaxAccessIndex();

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::vSetDeviceUsagePreference
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceUsagePreference
       * \brief  stores device usage preference
       * \param  cou32DeviceHandle :Device Handle
       * \param  enEnabledInfo :indicates Device usage enabled information
       **************************************************************************/
      t_Void vSetDeviceUsagePreference(
            const t_U32 cou32DeviceHandle, tenEnabledInfo enEnabledInfo);

   private:
      /***************************************************************************
       *********************************PRIVATE **********************************
       ***************************************************************************/
      /***************************************************************************
      ** FUNCTION: spi_tclDevHistory(const spi_tclDevHistory &corfobjRhs)
      ***************************************************************************/
      /*!
      * \fn      spi_tclDevHistory(const spi_tclDevHistory &corfobjRhs)
      * \brief   Copy constructor not implemented hence made protected to prevent
      *          misuse
      **************************************************************************/
      spi_tclDevHistory(const spi_tclDevHistory &corfobjRhs);

      /***************************************************************************
      ** FUNCTION: const spi_tclDevHistory & operator=(const spi_tclDevHistory &corfobjRhs);
      ***************************************************************************/
      /*!
      * \fn      const spi_tclDevHistory & operator=(const spi_tclDevHistory &corfobjRhs);
      * \brief   assignment operator not implemented hence made protected to
      *          prevent misuse
      **************************************************************************/
      const spi_tclDevHistory & operator=(
         const spi_tclDevHistory &corfobjRhs);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::bCreateDevHistoryTable
       ***************************************************************************/
      /*!
       * \fn     bCreateDevHistoryTable
       * \brief  Creates table to store device history in database
       * \retval : true if found table is successfully created
       **************************************************************************/
      t_Bool bCreateDevHistoryTable();

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::bExecuteQuery
       ***************************************************************************/
      /*!
       * \fn     bExecuteQuery
       * \brief  Execute General sqlite query
       * \param  szQuery: string containing sqlite query
       * \param  pfSqliteCb: Callback for query results. Default NULL
       * \retval : true if Query executed successfully
       **************************************************************************/
      t_Bool bExecuteQuery(std::string szQuery, fs32SqliteCallback* pfSqliteCb = NULL );

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::vSetValue
       ***************************************************************************/
      /*!
       * \fn     vSetValue
       * \brief  Sets the value of a desired entry in the table
       * \param  cou32DeviceHandle: Device handle whose values are to be modified
       * \param  szColName: Name of the column to be modified
       * \param  szColValue: New value of the entry
       **************************************************************************/
      t_Void vSetValue(const t_U32 cou32DeviceHandle, t_String szColName,
               t_String szColValue);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::bPopulateDeviceList
       ***************************************************************************/
      /*!
       * \fn     bPopulateDeviceList
       * \brief  Populate device list structure from sqlite database
       * \param  rfrDeviceInfo: Reference to the device list structure
       * \param  szDevListQuery: Query to fetch device list
       * \retval true : if the structure was successfully populated with values
       *                in database
       *         false : on failure
       **************************************************************************/
      t_Bool bPopulateDeviceList(std::vector<trEntireDeviceInfo> &rfrDeviceInfo,
               t_String szDevListQuery);

      /***************************************************************************
       ** FUNCTION:  spi_tclDevHistory::bMaintainDbSize
       ***************************************************************************/
      /*!
       * \fn     bMaintainDbSize
       * \brief  MAintains device history size to the size mention in policy.xml
       * \retval true : if Database size is maintained to specified limit
       *         false : on failure
       **************************************************************************/
      t_Bool bMaintainDbSize();

      /***************************************************************************
       ** FUNCTION:  static t_S32 spi_tclDevHistory::s32DisplayDevHistoryCb
       ***************************************************************************/
      /*!
       * \fn     s32DisplayDevHistoryCb
       * \brief  Displays data. Called by sqlite library for  select table request
       **************************************************************************/
      static t_S32 s32DisplayDevHistoryCb(t_Void *pvNotUsed, t_S32 s32ArgCountMax,
               t_Char **ppczArgv, t_Char **ppczColName);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclDevHistory::bCheckPathValidity
       ***************************************************************************/
      /*!
       * \fn     bCheckPathValidity
       * \brief  check if the directory is valid
       **************************************************************************/
      t_Bool bCheckPathValidity(t_String &rfrszStorageDir);
	  
      //! Sqlite database handler
      sqlite3 *m_poDevHistorydb;

      //! flag to store status of database
      t_Bool m_bisDBOpen;

      //! Lock to prevent simultaneous access to database.
      //! TODO issue seen only in MY17. To be checked if multithreading is supported for sqlite
      //static Lock m_oDBOperationInProgress;

};

/*! } */
#endif /* SPITCLDEVHISTORYDB_H_ */
