/*!
 *******************************************************************************
 * \file             spi_tclDevSelResp.h
 * \brief            Response to HMI from DeviceSelector class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Response to HMI from DeviceSelector class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 16.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 27.09.2014 |  Shihabudheen P M            | Added vPostDeviceSelectStatus()

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLDEVSELRESP_H_
#define SPI_TCLDEVSELRESP_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclConnSettings
 * \brief Response to HMI from DeviceSelector class
 */
class spi_tclDevSelResp
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclDevSelResp::spi_tclDevSelResp
       ***************************************************************************/
      /*!
       * \fn     spi_tclDevSelResp()
       * \brief  Default Constructor
       * \sa      ~spi_tclDevSelResp()
       **************************************************************************/
      spi_tclDevSelResp()
      {

      }

      /***************************************************************************
       ** FUNCTION:  spi_tclDevSelResp::~spi_tclDevSelResp
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclDevSelResp()
       * \brief  Destructor
       * \sa     spi_tclDevSelResp()
       **************************************************************************/
      virtual ~spi_tclDevSelResp()
      {

      }

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclDevSelResp::vPostSelectDeviceResult
       ***************************************************************************/
      /*!
       * \fn     vPostSelectDeviceResult
       * \brief  It provides the result of  select device request
       * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
       * \param  [IN] enDevConnType   : Identifies the Connection Type.
       * \param  [IN] enDevConnReq	  : Identifies the Connection Request.
       * \param  [IN] enDevCat    : Identifies the device category
       * \param  [IN] enRespCode  : Provides result from the operation.
       * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
       *              Set to NO_ERROR for successful operation.
       * \param  [IN] bIsPairingReq  : Set when BT pairing is required. 
	   *         (Not valid for deselection)
       * \param  [IN] rcUsrCntxt  : User Context Details.
       * \sa     spi_tclCmdInterface::vSelectDevice
       **************************************************************************/
      virtual t_Void vPostSelectDeviceResult(t_U32 u32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceConnectionReq enDevConnReq, 
               tenDeviceCategory enDevcat,
               tenResponseCode enRespCode,
               tenErrorCode enErrorCode, 
               t_Bool bIsPairingReq,
               const trUserContext rcUsrCntxt)
      {

      }


      /***************************************************************************
       ** FUNCTION: t_Void spi_tclDevSelResp::vPostDeviceSelectStatus
       ***************************************************************************/
      /*!
       * \fn     vPostDeviceSelectStatus
       * \brief  Respond with the device selection status.
       * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
       * \param  [IN] enDevCategory   : Device category.
       * \param  [IN] enDevConnReq	  : Identifies the Connection Request.
       * \param  [IN] enRespCode      : Response code
       **************************************************************************/
      virtual t_Void vPostDeviceSelectStatus(t_U32 u32DeviceHandle, 
         tenDeviceCategory enDevCategory,
         tenDeviceConnectionReq enDevConnReq,
         tenResponseCode enRespCode)
      {
      }
};


#endif /* SPI_TCLDEVSELRESP_H_ */
