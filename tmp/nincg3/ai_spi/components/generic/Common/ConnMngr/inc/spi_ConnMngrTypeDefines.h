/*!
 *******************************************************************************
 * \file             spi_ConnMngrTypeDefines.h
 * \brief            Base Connection class
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Type defines used in Connection Manager
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.02.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLCONNMNGRTYPEDEFINES_H_
#define SPI_TCLCONNMNGRTYPEDEFINES_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "SPITypes.h"

//! Supported Connection Media Types
enum tenSupportedConnectionTypes
{
   e8SUPPOTSCONN_USB = 1,
   e8SUPPOTSCONN_WIFI = 2,
   e8SUPPOTSCONN_USB_OR_WIFI = 3,
   e8SUPPOTSCONN_USB_AND_WIFI = 4
};

//! Mirrorlink 1.0 support
enum tenML10Support
{
   e8ML10_ALLDEVICES = 1,
   e8ML10_DAPCERTIFIED = 2,
   e8ML10_NOT_SUPPORTED = 3
};

//! Priority for autoselection
enum tenSelModePriority
{
   e8PRIORITY_DEVICELIST_HISTORY = 1,
   e8PRIORITY_CONNMODE_PREFERENCE = 2,
   e8PRIORITY_DEVICETYPE_PREFERENCE = 3
};

//! Multi session  support
enum tenMultiSessionSupport
{
   e8MULTISESSION_NOT_SUPPORTED = 0,
   e8MULTISESSION_SUPPORTED = 1
};

//! DAP Preference
enum tenDAPPreference
{
   e8DAPPREF_UNKNOWN = 0,
   e8DAPPREF_ON_DEVICECONNECTION = 1,
   e8DAPPREF_ON_REQUEST = 2
};

/*!
 * \brief Index to various SPI Connections
 */
enum tenConnPointersIndex
{
   e8_CONN_ML_INDEX = 0,
   e8_CONN_DIPO_INDEX = 1,
   e8_CONN_AAUTO_INDEX = 2,

   //! Please add connection pointers above this in consecutive order
   //! This is required to maintain the size of enum
   e8_CONN_INDEX_SIZE
};

//! Determines type of SPI Service
typedef enum
{
   e8SPISERVICE_UNKNOWN = 0,
   e8SPISERVICE_MIRRORLINK = 1,
   e8SPISERVICE_DIPO = 2
} tenSPIServiceType;

//! Specifies user preferred SPI technology
enum tenUserPreference
{
   e8PREFERENCE_NOTKNOWN = 0,
   e8ANDROID_AUTO_NOTPREFERRED = 1,
   e8ANDROID_AUTO_PREFERRED = 2,
   e8MIRRORLINK_NOT_PREFERRED = 3,
   e8MIRRORLINK_PREFERRED = 4,
   e8PROJECTION_NOT_PREFERRED = 5
};

/*!
 * Stores device info along with extra fields needed for
 * connection management
 */
struct trEntireDeviceInfo
{
      trDeviceInfo rDeviceInfo;
      t_Bool bDeviceValidated;
      t_Bool bIsDeviceUsed;
      t_U32 u32AccessIndex;
      t_Bool bIsUserDeselected;
      t_Bool bIsSelectError;
      t_Bool bIsUSBConnected;
      tenUserPreference enUserPreference;
      trEntireDeviceInfo() :
         bDeviceValidated(false), bIsDeviceUsed(false), u32AccessIndex(0), bIsUserDeselected(false), bIsSelectError(
               false), bIsUSBConnected(false), enUserPreference(e8PREFERENCE_NOTKNOWN)
      {

      }
      //! overloads the function call operator, operator(), it becomes a FunctionObject type.
      //! Many standard algorithms, from std::sort to std::accumulate accept objects
      //! of such types to customize behavior.
      bool operator()(trEntireDeviceInfo const & corfrLhs, trEntireDeviceInfo const & corfrRhs) const
      {
         t_Bool bRetVal =
                  //! Check for Connection status
                  ((corfrLhs.rDeviceInfo.enDeviceConnectionStatus
                  > corfrRhs.rDeviceInfo.enDeviceConnectionStatus)
                  //! Check for recently selected devices in connected list
                  || ((corfrLhs.rDeviceInfo.enDeviceConnectionStatus
                           == corfrRhs.rDeviceInfo.enDeviceConnectionStatus)
                           && (corfrLhs.u32AccessIndex > corfrRhs.u32AccessIndex)));
         return bRetVal;
      }
};

//! Holds the information for device selection
struct trDeviceSelectionInfo
{
      t_U32 u32DeviceHandle;
      tenDeviceConnectionReq enDevConnReq;
      tenEnabledInfo enCDBUsage;
      tenEnabledInfo enDAPUsage;
      t_Bool bIsHMITrigger;
      trUserContext corUsrCntxt;

      trDeviceSelectionInfo():u32DeviceHandle(0), enDevConnReq(e8DEVCONNREQ_DESELECT),
               enCDBUsage(e8USAGE_DISABLED), enDAPUsage(e8USAGE_DISABLED),
               bIsHMITrigger(false)
      {

      }
};

//! Unauthorized device info
struct trUnauthDeviceInfo
{
      trDeviceAuthInfo rAuthInfo;
      trDeviceInfo rDeviceInfo;
      trUserContext rUserContext;
};

/*! } */
#endif /* SPI_TCLCONNMNGRTYPEDEFINES_H_ */
