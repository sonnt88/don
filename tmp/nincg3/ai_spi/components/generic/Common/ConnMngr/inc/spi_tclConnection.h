/*!
 *******************************************************************************
 * \file             spi_tclConnection.h
 * \brief            Base Connection class
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class for Connection classes. Provides basic connection
                 interfaces to be implemented by derived classes.
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 05.11.2014 | Ramya Murthy                 | Added callback for Application metadata.

 \endverbatim
 ******************************************************************************/
#ifndef SPI_TCLCONNECTION_H_
#define SPI_TCLCONNECTION_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <functional> //! C++11
#include "SPITypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

//! Callback signatures definitions: To be registered by the Creator of this class object
typedef std::function<void(t_U32, const trDeviceInfo&, tenDeviceCategory)> tfvDeviceConnection;
typedef std::function<void(t_U32, tenDAPStatus)> tfvUpdateDAPStatus;
typedef std::function<void(t_U32, tenDeviceCategory)> tfvDeviceDisconnection;
typedef std::function<void(tenErrorCode)> tfvSelectDeviceResult;
typedef std::function<void(const t_U32, t_Bool)> tfvSelectDeviceError;
typedef std::function<void(const trAppMediaMetaData&, const trUserContext&)> tfvAppMediaMetaData;
typedef std::function<void(const trAppPhoneData&, const trUserContext&)> tfvAppPhoneData;
typedef std::function<void(const trAppMediaPlaytime&, const trUserContext&)> tfvAppMediaPlaytime;
typedef std::function<void(t_U32, t_String)> tfvSetDeviceName;

/*!
 * \brief Structure holding the callbacks to be registered by the
 * Creator of this class object
 */
struct trConnCallbacks
{
      //! Informs when a new device is detected
      tfvDeviceConnection fvDeviceConnection;

      //! Property called when there is change in device status
      tfvUpdateDAPStatus fvUpdateDAPStatus;

      //! Informs when a Device is disconnected
      tfvDeviceDisconnection fvDeviceDisconnection;

      //!Inform the select device result
      tfvSelectDeviceResult fvSelectDeviceResult;

      //!Inform the application media metadata
      tfvAppMediaMetaData fvAppMediaMetaData;

      //!Inform the application phone data that also include phone call metadata
      tfvAppPhoneData fvAppPhoneData;

      //!Inform the application current playtime track time
      tfvAppMediaPlaytime fvAppMediaPlaytime;

      //! Sets error flag to prevent further automatic selection
      tfvSelectDeviceError fvSelectDeviceError;

      //! Sets the device name
      tfvSetDeviceName fvSetDeviceName;

      trConnCallbacks() :
                        fvDeviceConnection(NULL),
                        fvUpdateDAPStatus(NULL),
                        fvDeviceDisconnection(NULL),
                        fvSelectDeviceResult(NULL),
                        fvAppMediaMetaData(NULL),
                        fvAppPhoneData(NULL),
                        fvAppMediaPlaytime(NULL),
                        fvSelectDeviceError(NULL),
                        fvSetDeviceName(NULL)
      {

      }
};
/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclConnection
 * \brief Base class for Connection classes. Provides basic connection
 *         interface to be implemented by derived classes
 */

class spi_tclConnection
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::spi_tclConnection
       ***************************************************************************/
      /*!
       * \fn     spi_tclConnection()
       * \brief  Default Constructor
       * \sa      ~spi_tclConnection()
       **************************************************************************/
      spi_tclConnection()
      {

      }

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::~spi_tclConnection
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclConnection()
       * \brief  virtual Destructor
       * \sa     spi_tclConnection()
       **************************************************************************/
      virtual ~spi_tclConnection()
      {

      }

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::bInitializeConnection
       ***************************************************************************/
      /*!
       * \fn     bInitializeConnection()
       * \brief  required initializations.
       *         Mandatory interface
       * \retval returns true on successful initialization and false on failure
       **************************************************************************/
      virtual t_Bool bInitializeConnection() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vOnLoadSettings
       ***************************************************************************/
      /*!
       * \fn     vOnLoadSettings()
       * \brief  Called on loadsettings
       *         Optional interface
       * \param rfrheadUnitInfo : Head unit information
       * \param enCertificateType : CertificateType to be used for authentication
       **************************************************************************/
      virtual t_Void vOnLoadSettings(trHeadUnitInfo &rfrheadUnitInfo, tenCertificateType enCertificateType)
      {

      }

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vOnSaveSettings
       ***************************************************************************/
      /*!
       * \fn     vOnSaveSettings()
       * \brief  Called on savesettings
       *         Optional interface
       * \retval none
       **************************************************************************/
      virtual t_Void vOnSaveSettings()
      {

      }

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::bStartDeviceDetection
       ***************************************************************************/
      /*!
       * \fn     bStartDeviceDetection()
       * \brief  Starts device detection
       * \retval returns true on successful detection and false on failure
       **************************************************************************/
      virtual t_Bool bStartDeviceDetection() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vUnInitializeConnection
       ***************************************************************************/
      /*!
       * \fn     vUnInitializeConnection()
       * \brief  Uninitialization of sdk's etc
       *         Mandatory interface
       **************************************************************************/
      virtual t_Void vUnInitializeConnection() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  interface for the creator class to register for the required
       *        callbacks.
       *         Mandatory interface
       * \param rfrConnCallbacks : reference to the callback structure
       *        populated by the caller
       **************************************************************************/
      virtual t_Void vRegisterCallbacks(trConnCallbacks &rfrConnCallbacks) = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vOnAddDevicetoList
       ***************************************************************************/
      /*!
       * \fn     vOnAddDevicetoList()
       * \brief  called when a new device has to be added to device list
       *         Optional interface
       * \param  cou32DeviceHandle: Device handle of the device to be added to
       *         the device list
       **************************************************************************/
      virtual t_Void vOnAddDevicetoList(const t_U32 cou32DeviceHandle)
      {

      }

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::bSetDevProjUsage
       ***************************************************************************/
      /*!
       * \fn     bSetDevProjUsage()
       * \brief  Called when the SPI featured is turned ON or OFF by the user.
       *         Mandatory interface
       * \param  enServiceStatus : Sets the particular SPI service ON or OFF
       **************************************************************************/
      virtual t_Bool bSetDevProjUsage(tenEnabledInfo enServiceStatus) = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vOnSelectDevice
       ***************************************************************************/
      /*!
       * \fn     vOnSelectDevice
       * \brief  Called when a device is selected by the user. Optional interface
       * \param  u32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevConnType   : Identifies the Connection Type.
       * \param  enDevSelReq    : Identifies the Connection Request.
       * \param  enDAPUsage      : Identifies Usage of DAP for the selected ML device.
       *              This value is not considered for de-selection of device.
       * \param  enCDBUsage      : Identifies Usage of CDB for the selected ML device.
       *              This value is not considered for de-selection of device
       * \param  bIsHMITrigger   : true if HMI has triggered select device
       * \param  corUsrCntxt      : User Context Details.
       **************************************************************************/
      virtual t_Void vOnSelectDevice(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceConnectionReq enDevSelReq, tenEnabledInfo enDAPUsage,
               tenEnabledInfo enCDBUsage, t_Bool bIsHMITrigger,
               const trUserContext corUsrCntxt)
      {

      }

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vSetRoleSwitchRequestedInfo
       ***************************************************************************/
      /*!
       * \fn     vSetRoleSwitchRequestedInfo
       * \brief  Function to upddate the roleswitch requested info.
       * \param  u32DeviceHandle : Uniquely identifies the target Device.
       **************************************************************************/
      virtual t_Void vSetRoleSwitchRequestedInfo(const t_U32 cou32DeviceHandle)
      {
      }

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclConnection::vOnSelectDeviceResult()
      ***************************************************************************/
      /*!
      * \fn      t_Void vOnSelectDeviceResult
      * \brief   To perform the actions that are required, after the select device is
      *           successful/failed
      * \pram    cou32DeviceHandle  : [IN] Uniquely identifies the target Device.
      * \pram    enDevSelReq : [IN] Identifies the Connection Request.
      * \pram    coenRespCode: [IN] Response code. Success/Failure
      * \pram    enDevCat    : [IN] Device Category. ML/DiPo
      * \pram    bIsHMITrigger : [IN] Indicates if the trigger is from user
      * \retval  t_Void
      **************************************************************************/
      virtual t_Void vOnSelectDeviceResult(const t_U32 cou32DeviceHandle,
         const tenDeviceConnectionReq coenConnReq,
         const tenResponseCode coenRespCode,
         tenDeviceCategory enDevCat, t_Bool bIsHMITrigger)
      {
      }

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::bRequestDeviceSwitch
       ***************************************************************************/
      /*!
       * \fn     bRequestDeviceSwitch
       * \brief  Function to request device to switch to SPI mode or normal USB mode
       * \param  u32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevCat : Indicates SPI or device mode.
       *          @Note: e8DEV_TYPE_UNKNOWN : Indicates switch to USB mode
       **************************************************************************/
      virtual t_Bool bRequestDeviceSwitch(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat)
      {

      }

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

       /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vOnDeviceConnection
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceConnection()
       * \brief  Callback to be called on new device detection by the derived class
       * \param cou32DeviceHandle: Device handle of the detected device
       * \param corfrDeviceInfo: reference to the structure containing device info
       **************************************************************************/
      virtual t_Void vOnDeviceConnection(const t_U32 cou32DeviceHandle,
               const trDeviceInfo &corfrDeviceInfo) = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vOnDeviceDisconnection
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceDisconnection()
       * \brief Callback to be called on device disconnection by the derived class
       * \param cou32DeviceHandle: Device Handle of the disconnected device
       **************************************************************************/
      virtual t_Void vOnDeviceDisconnection(const t_U32 cou32DeviceHandle) = 0;

};
/*! } */
#endif // SPI_TCLCONNECTION_H_
