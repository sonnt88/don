/*!
 *******************************************************************************
 * \file             spi_tclConnMngrResp.h
 * \brief            Response to HMI from ConnMngr class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Response  to HMI  from ConnMngr class.
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 10.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 05.11.2014 |  Ramya Murthy                | Added Application metadata response

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLCONNMNGRRESP_H_
#define SPI_TCLCONNMNGRRESP_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"


/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclConnSettings
 * \brief Response  to HMI  from ConnMngr class
 */
class spi_tclConnMngrResp
{

   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngrResp::spi_tclConnMngrResp
       ***************************************************************************/
      /*!
       * \fn     spi_tclConnMngrResp()
       * \brief  Default Constructor
       * \sa      ~spi_tclConnMngrResp()
       **************************************************************************/
      spi_tclConnMngrResp()
      {

      }

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngrResp::~spi_tclConnMngrResp
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclConnMngrResp()
       * \brief  Virtual Destructor
       * \sa     spi_tclConnMngrResp()
       **************************************************************************/
      virtual ~spi_tclConnMngrResp()
      {

      }


      /***************************************************************************
       ** FUNCTION: t_Void spi_tclConnMngrResp::vPostDeviceStatusInfo
       ***************************************************************************/
      /*!
       * \fn     vPostDeviceStatusInfo(t_U32 u32DeviceHandle,
       *             tenDeviceConnectionType enDevConnType, tenDeviceStatusInfo enDeviceStatus)
       * \brief  It notifies the client on change in any device attributes.
       *         The client can retrieve the detailed information
       *         via the methods provided.
       * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
       * \param  [IN] enDevConnType   : Identifies the Connection Type.
       * \param  [IN] enDeviceStatus  : enum value which stores device status
       **************************************************************************/
      virtual t_Void vPostDeviceStatusInfo(t_U32 u32DevHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceStatusInfo enDeviceStatus)
      {

      }

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngrResp::vPostDAPStatusInfo
      ***************************************************************************/
      /*!
      * \fn     vPostDAPStatusInfo
      * \brief  It notifies the client about DAP authentication progress information
      *            for a Mirror Link device.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] enDevConnType   : Identifies the Connection Type.
      * \param  [IN] enDAPStatus : DAP Authentication Progress Status.
      **************************************************************************/
      virtual t_Void vPostDAPStatusInfo(t_U32 u32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDAPStatus enDAPStatus)
      {

      }

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclConnMngrResp::vPostDeviceUsagePrefResult
       ***************************************************************************/
      /*!
       * \fn     vPostDeviceUsagePrefResult
       * \brief  It provides the result for Device Usage Preference Set Request
       * \param  coU32DeviceHandle: Device handle for which the device usage
       *         preference was set
       * \param  enErrorCode: Error code if setting device usage preference fails
       * \param  enDeviceCategory: Device Category
       * \param  enUsagePref: indicates whether the device usage preference is enabled or not
       * \param  corfrUsrCtxt : User context
       **************************************************************************/
      virtual t_Void vPostDeviceUsagePrefResult(const t_U32 coU32DeviceHandle,
            tenErrorCode enErrorCode, tenDeviceCategory enDeviceCategory,
            tenEnabledInfo enUsagePref, const trUserContext &corfrUsrCtxt)
      {

      }

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngrResp::vPostApplicationMediaMetaData(t_U32& ...
      ***************************************************************************/
      /*!
      * \fn     vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationMediaMetaData : Contains the media metadata information
      *              related to an application.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      virtual t_Void vPostApplicationMediaMetaData(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
         const trUserContext& rfcorUsrCntxt)
      {

      }
      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngrResp::vPostApplicationPhoneData(...
      ***************************************************************************/
      /*!
      * \fn     vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationPhoneData : Contains the phone related information.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      virtual t_Void vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
         const trUserContext& rfcorUsrCntxt)
      {

      }
      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngrResp::vPostApplicationMediaPlaytime(...
      ***************************************************************************/
      /*!
      * \fn     vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationMediaPlaytime : Contains the media play time of current playing track.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      virtual t_Void vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
         const trUserContext& rfcorUsrCntxt)
      {

      }

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngrResp::vRequestDeviceAuthorization
      ***************************************************************************/
      /*!
      * \fn     vRequestDeviceAuthorization
      * \brief  Interface to send notification to user to authorize the device
      * \param  [IN] rfrvecDevAuthInfo : List of device authorization info
      * \sa
      **************************************************************************/
      virtual t_Void vRequestDeviceAuthorization(std::vector<trDeviceAuthInfo> &rfrvecDevAuthInfo)
      {

      }

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngrResp::vPostDipoRoleSwitchResponse
      ***************************************************************************/
      /*!
      * \fn     vPostDipoRoleSwitchResponse
      * \brief  Post response to DIPO role switch request
      * \param  [IN] bRoleSwitchRequired : true if role switch is required. otherwise false
      * \param  [IN] rfcorUsrCntxt : User context
      * \sa
      **************************************************************************/
      virtual t_Void vPostDipoRoleSwitchResponse(t_Bool bRoleSwitchRequired,const t_U32 cou32DeviceHandle,
            const trUserContext& rfcorUsrCntxt)
      {

      }

};
#endif // SPI_TCLCONNMNGRRESP_H_
