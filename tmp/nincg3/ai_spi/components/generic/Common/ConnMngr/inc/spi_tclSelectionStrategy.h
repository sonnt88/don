/*!
 *******************************************************************************
 * \file             spi_tclSelectionStrategy.h
 * \brief            Implements Selection Strategy for automatic device selection
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Implements Selection Strategy for automatic device selection
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLSELECTIONSTRATEGY_H_
#define SPI_TCLSELECTIONSTRATEGY_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include <map>
#include "spi_ConnMngrTypeDefines.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

//! Forward declaration
class spi_tclConnSettings;

/*!
 * \class spi_tclDiPoConnection
 * \brief Implements Selection Strategy for automatic device selection
 */
class spi_tclSelectionStrategy
{

   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::spi_tclSelectionStrategy
       ***************************************************************************/
      /*!
       * \fn     spi_tclSelectionStrategy()
       * \brief  Default Constructor
       * \sa      ~spi_tclSelectionStrategy()
       **************************************************************************/
      spi_tclSelectionStrategy();

      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::~spi_tclSelectionStrategy
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclSelectionStrategy()
       * \brief  Destructor
       * \sa     spi_tclSelectionStrategy()
       **************************************************************************/
      ~spi_tclSelectionStrategy();

      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::u32ApplySelectionStrategy
       ***************************************************************************/
      /*!
       * \fn     u32ApplySelectionStrategy
       * \brief  Apply algorithms for device selection and returns a device
       *         handle on automatic device selection. vUpdateDeviceList must be called
       *         before applying selection strategy
       * \retval Device handle of the automatically selected device. returns 0 if
       *         no device is  selected automatically
       **************************************************************************/
      t_U32 u32ApplySelectionStrategy();

      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::vUpdateDeviceList
       ***************************************************************************/
      /*!
       * \fn     vUpdateDeviceList
       * \brief  Update device list
       * \param rfrmapDeviceInfoList:Map containing device list
       **************************************************************************/
      t_Void vUpdateDeviceList(std::map<t_U32, trEntireDeviceInfo> &rfrmapDeviceInfoList);

      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::vSetDeviceSelectError
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceSelectError
       * \brief  Used to set error flag if the validity check for auto selected device fails
       * \param  cou32DeviceHanlde containing device list
       * \bIsError indicated if device validity for selection failed
       **************************************************************************/
      t_Void vSetDeviceSelectError(const t_U32 cou32DeviceHanlde, t_Bool bIsError);

      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::u32GetSPICapableDevice
       ***************************************************************************/
      /*!
       * \fn     u32GetSPICapableDevice
       * \brief  Returns device handle of device which might support one of the SPI
       *         technology
       **************************************************************************/
      t_U32 u32GetSPICapableDevice() const;


   private:
      /***************************************************************************
       *********************************PRIVATE ***********************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION: spi_tclSelectionStrategy(const spi_tclSelectionStrategy &corfobjRhs)
       ***************************************************************************/
      /*!
       * \fn      spi_tclSelectionStrategy(const spi_tclSelectionStrategy &corfobjRhs)
       * \brief   Copy constructor not implemented hence made protected to prevent
       *          misuse
       **************************************************************************/
      spi_tclSelectionStrategy(const spi_tclSelectionStrategy &corfobjRhs);

      /***************************************************************************
       ** FUNCTION: const spi_tclSelectionStrategy & operator=(
       **           const spi_tclSelectionStrategy &corfobjRhs);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclSelectionStrategy & operator=(
       *          const spi_tclSelectionStrategy &corfobjRhs);
       * \brief   assignment operator not implemented hence made protected to
       *          prevent misuse
       **************************************************************************/
      const spi_tclSelectionStrategy & operator=(
               const spi_tclSelectionStrategy &corfobjRhs);

      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::u32GetAutoSelectedDevice
       ***************************************************************************/
      /*!
       * \fn     u32GetAutoSelectedDevice
       * \brief  Returns device handle of selected device based on priority. returns 0
       *         if recently connected device is not found
       * \retval Device handle of the recently connected device. returns 0 if
       *         no device is  selected automatically
       **************************************************************************/
      t_U32 u32GetAutoSelectedDevice() const;

      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::u32GetLastUsedDevice
       ***************************************************************************/
      /*!
       * \fn     u32GetLastUsedDevice
       * \brief  Returns device handle of device which was being used during the
       *         last power cycle
       * \retval Device handle of the recently connected device. returns 0 if
       *         no device is  selected automatically
       **************************************************************************/
      t_U32 u32GetLastUsedDevice() const;


      /***************************************************************************
       ** FUNCTION:  spi_tclSelectionStrategy::u32GetLastConnectedDevice
       ***************************************************************************/
      /*!
       * \fn     u32GetLastConnectedDevice
       * \brief  Returns device handle of device which is last connected
       * \retval Device handle of the last connected device. returns 0 if no valid
       *         device is connected
       **************************************************************************/
      t_U32 u32GetLastConnectedDevice() const;

      //! Pointer to connection settings (Project specific )
      spi_tclConnSettings *m_poConnSettings;

      //! Temporarily stores the connected devices list
      std::map<t_U32, trEntireDeviceInfo> m_mapDeviceInfo;

};
/*! } */
#endif // SPI_TCLSELECTIONSTRATEGY_H_
