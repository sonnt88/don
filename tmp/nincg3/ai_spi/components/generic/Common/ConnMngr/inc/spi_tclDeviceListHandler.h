/*!
 *******************************************************************************
 * \file             spi_tclDeviceListHandler.h
 * \brief            Handles Device List
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Handles Device List and Device History for SPI Devices
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 25.01.2016 |  Rachana L Achar             | Logiscope improvements
 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLDEVICELISTHANDLER_H_
#define SPI_TCLDEVICELISTHANDLER_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <map>
#include "spi_ConnMngrTypeDefines.h"
#include "Lock.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

//! Forward declarations
class spi_tclSelectionStrategy;

/*!
 * \class spi_tclDeviceListHandler
 * \brief Handles Device List and Device History for SPI Devices
 */
class spi_tclDeviceListHandler
{

   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::spi_tclDeviceListHandler
       ***************************************************************************/
      /*!
       * \fn     spi_tclDeviceListHandler()
       * \brief  Default Constructor
       * \sa      ~spi_tclDeviceListHandler()
       **************************************************************************/
      spi_tclDeviceListHandler();


      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::~spi_tclDeviceListHandler
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclDeviceListHandler()
       * \brief   Destructor
       * \sa     spi_tclDeviceListHandler()
       **************************************************************************/
      ~spi_tclDeviceListHandler();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bRestoreDeviceList
       ***************************************************************************/
      /*!
       * \fn     bRestoreDeviceList
       * \brief  Reads database to restore device history to device list
       * \retval true: if the history was added successfully to device list
       *         false: on Error
       **************************************************************************/
      t_Bool bRestoreDeviceList();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bSaveDeviceList
       ***************************************************************************/
      /*!
       * \fn     bSaveDeviceList
       * \brief  Saves the device list to persistent memory device list
       **************************************************************************/
      t_Void bSaveDeviceList();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vClearDeviceList
       ***************************************************************************/
      /*!
       * \fn     vClearDeviceList
       * \brief  clears Device List
       **************************************************************************/
      t_Void vClearDeviceList();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::enAddDeviceToList
       ***************************************************************************/
      /*!
       * \fn     enAddDeviceToList
       * \brief  Adds device to device list
       * \param cou32DeviceHandle: Device Handle to be added to device list
       * \param rfrDeviceInfo: Device Info of the device to be added to list
       **************************************************************************/
      tenDeviceStatusInfo enAddDeviceToList(const t_U32 cou32DeviceHandle,
               const trDeviceInfo &rfrDeviceInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vRemoveDeviceFromList
       ***************************************************************************/
      /*!
       * \fn     vRemoveDeviceFromList
       * \brief  Removes device to device list
       * \param  coU32DeviceHandle:  Device Handle to be removed from device list
       **************************************************************************/
      t_Void vRemoveDeviceFromList(const t_U32 coU32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vGetDeviceList
       ***************************************************************************/
      /*!
       * \fn     vGetDeviceList
       * \brief  interface to get SPI devices list
       * \param  rfvecDeviceInfoList: reference to the device list which will
       *         be populated with the detected SPI devices
       * \param  bCertifiedOnly: if true, only certified devices will be reported.
       *         if false, all detected devices will be reported
       * \param  bConnectedOnly: if true only connected devices will be returned
       **************************************************************************/
      t_Void vGetDeviceList(std::vector<trDeviceInfo>& rfvecDeviceInfoList,
               t_Bool bCertifiedOnly = false, t_Bool bConnectedOnly = false);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vGetEntireDeviceList
       ***************************************************************************/
      /*!
       * \fn     vGetEntireDeviceList
       * \brief  interface to get SPI devices list with complete device info
       * \param  rfrmapDeviceInfoList: reference to the device list which will
       *         be populated with the detected SPI devices
       **************************************************************************/
      t_Void vGetEntireDeviceList(std::map<t_U32, trEntireDeviceInfo> &rfrmapDeviceInfoList);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetUserDeselectionFlag
       ***************************************************************************/
      /*!
       * \fn     vSetUserDeselectionFlag
       * \brief  Sets the flag when user deselects the device from HMI
       * \param  cou32DeviceHandle :Device Handle
       * \param  bState : indicates the value of UserDeselectionFlag
       **************************************************************************/
      t_Void vSetUserDeselectionFlag(const t_U32 cou32DeviceHandle, t_Bool bState);

       /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bGetUserDeselectionFlag
       ***************************************************************************/
      /*!
       * \fn     bGetUserDeselectionFlag
       * \brief  returns the flag when user deselects the device from HMI
       * \param  cou32DeviceHandle :Device Handle
       * \retval  bState : indicates the value of UserDeselectionFlag
       **************************************************************************/
      t_Bool bGetUserDeselectionFlag(const t_U32 cou32DeviceHandle);

      /***************************************************************************
      ** FUNCTION:  spi_tclDeviceListHandler::u32GetNoofConnectedDevices
      ***************************************************************************/
     /*!
      * \fn     u32GetNoofConnectedDevices
      * \brief  returns no of connected devices
      * \retval  t_U32 : indicates the no of connected devices
      **************************************************************************/
     t_U32 u32GetNoofConnectedDevices();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceValidity
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceValidity
       * \brief  Sets the validity of the device (based on certification)
       * \param  u32DeviceHandle : Device handle of which the validity has to be set
       * \param  bDeviceValid: true if device is certified else false
       **************************************************************************/
      t_Void vSetDeviceValidity(const t_U32 cou32DeviceHandle,
               t_Bool bDeviceValid);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bIsDeviceValid
       ***************************************************************************/
      /*!
       * \fn     bIsDeviceValid
       * \brief  Interface to get device information for a particular device handle
       * \param  cou32DeviceHandle: Device handle of the  device
       * \retval true: if the device is valid otherwise false
       **************************************************************************/
      t_Bool bIsDeviceValid(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bGetDeviceInfo
       ***************************************************************************/
      /*!
       * \fn     bGetDeviceInfo
       * \brief  Interface to get device information for a particular device handle
       * \param  u32DeviceHandle: Device handle of the  device
       * \param  rfrDeviceInfo: Device information for the requested device
       **************************************************************************/
      t_Bool bGetDeviceInfo(t_U32 u32DeviceHandle,
               trDeviceInfo &rfrDeviceInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceName
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceName
       * \brief  Sets the device name of the device identified by device handle
       * \param cou32DeviceHandle: Device Handle of the device to be added to history
       * \param rfrszDeviceName : Device Name to be set
       **************************************************************************/
      t_Void vSetDeviceName(const t_U32 cou32DeviceHandle, t_String &rfrszDeviceName);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vAddDeviceToHistory
       ***************************************************************************/
      /*!
       * \fn     vAddDeviceToHistory
       * \brief  Adds device to history
       * \param cou32DeviceHandle: Device Handle of the device to be added to history
       **************************************************************************/
      t_Void vAddDeviceToHistory(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vRemoveDeviceFromHistory
       ***************************************************************************/
      /*!
       * \fn     vRemoveDeviceFromHistory
       * \brief  removes device from history
       * \param  cou32DeviceHandle: Device Handle of the device to be removed from history
       **************************************************************************/
      t_Void vRemoveDeviceFromHistory(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bGetDeviceHistory
       ***************************************************************************/
      /*!
       * \fn     bGetDeviceHistory
       * \brief  Interface to get device history
       * \param  rfvecDevHistory: reference to vector containing device history
       **************************************************************************/
      t_Bool bGetDeviceHistory(std::vector<trEntireDeviceInfo> &rfvecDevHistory) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::enGetDeviceCategory
       ***************************************************************************/
      /*!
       * \fn     enGetDeviceCategory
       * \brief  Returns Device category for the provided device ID
       * \param  cou32DeviceHandle: Device Handle for which device
       *         category is requested
       **************************************************************************/
      tenDeviceCategory enGetDeviceCategory(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceCategory
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceCategory
       * \brief  Sets the Device category for the device
       * \param  cou32DeviceHandle :Device Handle
       * \param  enDeviceCategory : indicates the device category to be used for selection
       **************************************************************************/
      t_Void vSetDeviceCategory(const t_U32 cou32DeviceHandle, tenDeviceCategory enDeviceCategory);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::enGetDeviceConnType
       ***************************************************************************/
      /*!
       * \fn     enGetDeviceConnType
       * \brief  Returns Device connection type for the provided device ID
       * \param  cou32DeviceHandle: Device Handle for which device
       *         category is requested
       **************************************************************************/
      tenDeviceConnectionType enGetDeviceConnType(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::enGetDeviceConnStatus
       ***************************************************************************/
      /*!
       * \fn     enGetDeviceConnStatus
       * \brief  Returns Device connection status for the provided device ID
       * \param  cou32DeviceHandle: Device Handle for which device
       *         category is requested
       **************************************************************************/
      tenDeviceConnectionStatus enGetDeviceConnStatus(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vGetBTAddress
       ***************************************************************************/
      /*!
       * \fn     vGetBTAddress
       * \brief  Gets the BT Address of the device : cou32DeviceHandle
       * \param  cou32DeviceHandle: Device handle whose connection status is requested
       * \param  rfszBTAddress: [OUT] BT address for the requested device handle
       **************************************************************************/
      t_Void vGetBTAddress(const t_U32 cou32DeviceHandle, t_String &rfszBTAddress);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceSelection
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceSelection
       * \brief  Sets the bDeviceSelection flag if the device was selected during
       *         a power cycle
       * \param  cou32DeviceHandle: Device Handle for which device
       *         selection is requested
       **************************************************************************/
      t_Void vSetDeviceSelection(const t_U32 cou32DeviceHandle, t_Bool bSelectDev);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::u32GetSelectedDevice
       ***************************************************************************/
      /*!
       * \fn     u32GetSelectedDevice
       * \brief  returns the currently selected device
       * \retval Device Handle of the currently selected device. O if not found
       **************************************************************************/
      t_U32 u32GetSelectedDevice();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bWasDeviceSelected
       ***************************************************************************/
      /*!
       * \fn     bWasDeviceSelected
       * \brief  Returns whether a device was selected or not previously.
       * \param  cou32DeviceHandle: Device Handle for which device
       *         selection is requested
       **************************************************************************/
      t_Bool bWasDeviceSelected(const t_U32 cou32DeviceHandle);


      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetDeviceConnectionStatus
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceConnectionStatus
       * \brief  Sets the Device Connection status flag for cou32DeviceHandle
       * \param  cou32DeviceHandle: Device Handle for which device
       *         Connection is requested
       **************************************************************************/
      t_Void vSetDeviceConnectionStatus(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionStatus enConnStatus);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bIsDeviceConnected
       ***************************************************************************/
      /*!
       * \fn     bIsDeviceConnected
       * \brief  Returns whether a device was Connected or not previously
       * \param  cou32DeviceHandle: Device Handle for which device
       *         selection is requested
       **************************************************************************/
      t_Bool bIsDeviceConnected(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetDAPSupport
       ***************************************************************************/
      /*!
       * \fn     vSetDAPSupport
       * \brief  Sets the DAP Supported field for cou32DeviceHandle
       * \param  cou32DeviceHandle: Device Handle
       **************************************************************************/
      t_Void vSetDAPSupport(const t_U32 cou32DeviceHandle, t_Bool bDAPSupported);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bIsDAPSupported
       ***************************************************************************/
      /*!
       * \fn     bIsDAPSupported
       * \brief  Returns the DAP Support status of cou32DeviceHandle
       * \param  cou32DeviceHandle: Device Handle
       * \retval true: If device supports DAP. false otherwise
       **************************************************************************/
      t_Bool bIsDAPSupported(const t_U32 cou32DeviceHandle);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclDeviceListHandler::vSetDeviceUsagePreference
      ***************************************************************************/
      /*!
      * \fn     vSetDeviceUsagePreference
      * \brief  Interface to set the preference for the usage of the connected
      *         Mirror Link/DiPO device. This can be set for individual devices or
      *         for all the connected Mirror Link/DiPO devices.
      * \param  cou32DeviceHandle  : Unique handle which identifies the device.
      *              If the value is 0xFFFFFFFF, then this function sets the overall
      *              preference usage for Mirror Link/DiPO.
      * \param  enEnabledInfo    : Enable Information.
      **************************************************************************/
      t_Void vSetDeviceUsagePreference(const t_U32 cou32DeviceHandle,
               tenEnabledInfo enEnabledInfo) ;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclDeviceListHandler::vGetDeviceUsagePreference
      ***************************************************************************/
      /*!
      * \fn     vGetDeviceUsagePreference
      * \brief  Interface to get the preference for the usage of the
      *         connected Mirror Link/iPod Out device during startup.
      * \param  cou32DeviceHandle  : Unique handle which identifies the device.
      *              If the value is 0xFFFFFFFF, then this function sets the overall
      *              preference usage for Mirror Link/DiPO.
      * \param  [OUT] rfenEnabledInfo : Enable Information.
      **************************************************************************/
      t_Void vGetDeviceUsagePreference(const t_U32 cou32DeviceHandle,
               tenEnabledInfo& rfenEnabledInfo) ;

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bSetDevProjUsage
       ***************************************************************************/
      /*!
       * \fn     bSetDevProjUsage
       * \brief  Called when the SPI feature is turned ON or OFF by the user
       * \param  enSPIType: indicates the SPI Type (Mirrorlink, Dipo ..)
       * \param  enSPIState : indicates the service status of enSPIType
       **************************************************************************/
      t_Bool bSetDevProjUsage(tenDeviceCategory enSPIType, tenEnabledInfo enSPIState);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bGetDevProjUsage
       ***************************************************************************/
      /*!
       * \fn     bGetDevProjUsage
       * \brief  Returns the present SPI Feature Stata
       * \param  enSPIType: SPI service type for which state is
       *         requested (Mirrorlink, DiPo ..)
       * \param  rfrEnabledInfo: [OUT]Indicates if enSPIType is enabled or not
       * \retval true if enSPIType is enabled.
       *         false if enSPIType is disabled
       **************************************************************************/
      t_Bool bGetDevProjUsage(tenDeviceCategory enSPIType, tenEnabledInfo &rfrEnabledInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::u32GetLastSelectedDevice
       ***************************************************************************/
      /*!
       * \fn     u32GetLastSelectedDevice
       * \brief  Returns the last selected device
       * \retval Device handle of the selected device
       **************************************************************************/
      t_U32 u32GetLastSelectedDevice();

	  /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetSelectError
       ***************************************************************************/
      /*!
       * \fn     vSetSelectError
       * \brief  set to true if the error occurs on device side during selection
	    *         Ex: Role switch failure
       * \param cou32DeviceHandle: Device handle of the selected device
       * \parama bIsError value of the device error flag
       **************************************************************************/
      t_Void vSetSelectError(const t_U32 cou32DeviceHandle, t_Bool bIsError);

	  /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bIsSelectError
       ***************************************************************************/
      /*!
       * \fn     bIsSelectError
       * \brief  Returns true if the error occurs on device side during selection
	   *         Ex: Role switch failure
       * \param cou32DeviceHandle: Device handle of the selected device
       * \retval value of the device error flag
       **************************************************************************/
      t_Bool bIsSelectError(const t_U32 cou32DeviceHandle);
      
      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vClearPrivateData
       ***************************************************************************/
      /*!
       * \fn     vClearPrivateData
       * \brief  Clears previously connected devices from the list
       **************************************************************************/
      t_Void vClearPrivateData();

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bGetUSBConnectedFlag
       ***************************************************************************/
      /*!
       * \fn     bGetUSBConnectedFlag
       * \brief  Returns true if when USB connection for the device is active
       * \param cou32DeviceHandle: Device handle
       * \retval true if when USB connection for the device is active else false
       **************************************************************************/
      t_Bool bGetUSBConnectedFlag(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vClearPrivateData
       ***************************************************************************/
      /*!
       * \fn     vClearPrivateData
       * \brief  Set USB connection for the device (active/inactive)
       * \param cou32DeviceHandle: Device handle
       * \param bUSBConnected: Indicates whether USB connection is active or not
       **************************************************************************/
      t_Void vSetUSBConnectedFlag(const t_U32 cou32DeviceHandle, t_Bool bUSBConnected);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::enGetUserPreference
       ***************************************************************************/
      /*!
       * \fn     enGetUserPreference
       * \brief  Returns user preference SPI technology for the device
       * \param cou32DeviceHandle: Device handle
       **************************************************************************/
      tenUserPreference enGetUserPreference(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetUserPreference
       ***************************************************************************/
      /*!
       * \fn     vSetUserPreference
       * \brief   Sets user preference SPI technology for the device
       * \param cou32DeviceHandle: Device handle
       * \param  enUserPref: user preference SPI technology for the device
       **************************************************************************/
      t_Void vSetUserPreference(const t_U32 cou32DeviceHandle, tenUserPreference enUserPref);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::enGetSPISupport
       ***************************************************************************/
      /*!
       * \fn     enGetSPISupport
       * \brief  Get SPI technology support for indicated device handle and category
       * \param  cou32DeviceHandle: Device handle
       * \param  enSPIType: SPI technology supporet to be checked
       * \retval Indicates if this SPi technology is supported
       **************************************************************************/
      tenSPISupport enGetSPISupport(const t_U32 cou32DeviceHandle, tenDeviceCategory enSPIType);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSetSPISupport
       ***************************************************************************/
      /*!
       * \fn     vSetSPISupport
       * \brief  Sets SPI technology support for indicated device handle and category
       * \param  cou32DeviceHandle: Device handle
       * \param  enSPIType: SPI technology supporet to be checked
       * \param enSPISupport: indicates SPi technology is supported
       **************************************************************************/
      t_Void vSetSPISupport(const t_U32 cou32DeviceHandle, tenDeviceCategory enSPIType,
               tenSPISupport enSPISupport);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vGetUnauthorizedDevicesList
       ***************************************************************************/
      /*!
       * \fn     vGetUnauthorizedDevicesList
       * \brief
       * \param  rfrvecDevAuthInfo: List of Unauthorized devices
       **************************************************************************/
      t_Void vGetUnauthorizedDevicesList(std::vector<trDeviceAuthInfo> &rfrvecDevAuthInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vAddUnauthorizedDeviceToList
       ***************************************************************************/
      /*!
       * \fn     vAddUnauthorizedDeviceToList
       * \brief
       * \param rfrDevAuthInfo: Unauthorized device to be added to list
       **************************************************************************/
      t_Void vAddUnauthorizedDeviceToList(const trUnauthDeviceInfo &rfrDevAuthInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vRemoveUnauthorizedDevice
       ***************************************************************************/
      /*!
       * \fn     vRemoveUnauthorizedDevice
       * \brief
       * \param  cou32DeviceHandle: Device handle
       **************************************************************************/
      t_Void vRemoveUnauthorizedDevice(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::bGetUnauthorizedDeviceInfo
       ***************************************************************************/
      /*!
       * \fn     bGetUnauthorizedDeviceInfo
       * \brief
       * \param  cou32DeviceHandle: Device handle
       **************************************************************************/
      t_Bool bGetUnauthorizedDeviceInfo(const t_U32 cou32DeviceHandle, trUnauthDeviceInfo &rfrDevAuthInfo);

   private: 

      /***************************************************************************
      ** FUNCTION: spi_tclDeviceListHandler(const spi_tclDeviceListHandler &corfobjRhs)
      ***************************************************************************/
      /*!
      * \fn      spi_tclDeviceListHandler(const spi_tclDeviceListHandler &corfobjRhs)
      * \brief   Copy constructor not implemented hence made protected to prevent
      *          misuse
      **************************************************************************/
      spi_tclDeviceListHandler(const spi_tclDeviceListHandler &corfobjRhs);

      /***************************************************************************
      ** FUNCTION: const spi_tclDeviceListHandler & operator=(
      **           const spi_tclDeviceListHandler &corfobjRhs);
      ***************************************************************************/
      /*!
      * \fn      const spi_tclDeviceListHandler & operator=(
      *          const spi_tclDeviceListHandler &corfobjRhs);
      * \brief   assignment operator not implemented hence made protected to
      *          prevent misuse
      **************************************************************************/
      const spi_tclDeviceListHandler & operator=(
         const spi_tclDeviceListHandler &corfobjRhs);

      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vSortDeviceList
       ***************************************************************************/
      /*!
       * \fn     vSortDeviceList
       * \brief  Sorts SPI devices list in the order Connected, Selected
       * \param  rfvecDeviceInfoList: reference to the device list which will
       *         be populated with the detected SPI devices
       **************************************************************************/
      t_Void vSortDeviceList(std::vector<trEntireDeviceInfo>& rfvecEntireDeviceInfoList,
               std::vector<trDeviceInfo>& rfvecDeviceInfoList);
			   
      /***************************************************************************
       ** FUNCTION:  spi_tclDeviceListHandler::vDisplayDeviceList(const std::vec...
       ***************************************************************************/
      /*!
       * \fn     vDisplayDeviceList
       *         (const std::vector<trDeviceInfo>& corfvecDeviceInfoList) const
       * \brief  Display device information of all SPI devices
       * \param  corfvecDeviceInfoList: reference to the device list whose device
       *         information has to be displayed
       **************************************************************************/	  
      t_Void vDisplayDeviceList(const std::vector<trDeviceInfo>& corfvecDeviceInfoList) const;

      //! map containing Device list
      std::map<t_U32, trEntireDeviceInfo> m_mapDeviceInfoList;

      //! Lock to protect Device Info in th list
      Lock m_oDeviceListLock;

      //! map to store unauthorized devices
      std::map<t_U32, trUnauthDeviceInfo> m_mapDevAuthInfo;

      //! Lock to protect Unauthorized Device list
      Lock m_oUnauthDeviceListLock;
};
/*! } */
#endif // SPI_TCLDEVICELISTHANDLER_H_
