/*!
 *******************************************************************************
 * \file             spi_tclConnSettings.h
 * \brief            Project specific settings for Connection Management
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Project specific settings for Connection Management
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 24.11.2014 | Shiva Kumar Gurija           | XML Validation

 \endverbatim
 ******************************************************************************/

#ifndef SPI_CONNSETTINGS_H_
#define SPI_CONNSETTINGS_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_ConnMngrTypeDefines.h"
#include "GenericSingleton.h"
#include "Xmlable.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclConnSettings
 * \brief Project specific settings for Connection Management
 */
class spi_tclConnSettings: public GenericSingleton<spi_tclConnSettings>, public shl::xml::tclXmlReadable
{
      public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclConnSettings::~spi_tclConnSettings
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclConnSettings()
       * \brief  Destructor
       * \sa     spi_tclConnSettings()
       **************************************************************************/
      ~spi_tclConnSettings();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclConnSettings::bIntializeConnSettings
       ***************************************************************************/
      /*!
       * \fn     t_Void vIntializeConnSettings()
       * \brief  Reads settings from xml file and store it internally
       **************************************************************************/
      t_Void vIntializeConnSettings();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclConnSettings::vDisplayConnSettings
       ***************************************************************************/
      /*!
       * \fn     t_Void vDisplayConnSettings()
       * \brief  Displays Connection settings from xml file
       **************************************************************************/
      t_Void vDisplayConnSettings();

      /***************************************************************************
       ** FUNCTION:  tenEnabledInfo spi_tclConnSettings::enGetMirrorlinkSupport
       ***************************************************************************/
      /*!
       * \fn     tenEnabledInfo enGetMirrorlinkSupport()
       * \brief  returns mirrorlink support
       **************************************************************************/
      tenEnabledInfo enGetMirrorlinkSupport() const;

      /***************************************************************************
       ** FUNCTION:  tenEnabledInfo spi_tclConnSettings::enGetDiPoSupport
       ***************************************************************************/
      /*!
       * \fn     tenEnabledInfo enGetDiPoSupport()
       * \brief  returns DiPo support
       **************************************************************************/
      tenEnabledInfo enGetDiPoSupport() const;

      /***************************************************************************
       ** FUNCTION:  t_U32 spi_tclConnSettings::u32GetDelayforStartupSel
       ***************************************************************************/
      /*!
       * \fn     t_U32 u32GetDelayforStartupSel()
       * \brief  returns Device Selection Method
       **************************************************************************/
      t_U32 u32GetDelayforStartupSel() const;

      /***************************************************************************
       ** FUNCTION:  tenDeviceSelectionMode spi_tclConnSettings::enGetDeviceSelectionMode
       ***************************************************************************/
      /*!
       * \fn     tenDeviceSelectionMode enGetDeviceSelectionMode()
       * \brief  returns Device Selection Mode
       **************************************************************************/
      tenDeviceSelectionMode enGetDeviceSelectionMode() const;

      /***************************************************************************
       ** FUNCTION:  t_Void vSetDeviceSelectionMode()
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetDeviceSelectionMode(
       * \brief   Method to set the device selection mode to automatic/manual. Changes
       *          will take into effect on successive connection
       * \param   enSelectionMode : Device selection mode @see tenDeviceSelectionMode
       * \retval  t_Void
       **************************************************************************/
      t_Void vSetDeviceSelectionMode(tenDeviceSelectionMode enSelectionMode);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclConnSettings::vGetMaxMLVersionSupported
       ***************************************************************************/
      /*!
       * \fn     t_Void vGetMaxMLVersionSupported()
       * \brief  returns Maximum version of mirrorlink supported
       **************************************************************************/
      t_Void vGetMaxMLVersionSupported(trVersionInfo &rfrVerInfo) const;

      /***************************************************************************
       ** FUNCTION:  tenSupportedConnectionTypes spi_tclConnSettings::enGetSupportedConnectionTypes
       ***************************************************************************/
      /*!
       * \fn     tenSupportedConnectionTypes enGetSupportedConnectionTypes()
       * \brief  returns mSupported Connection Mode types
       **************************************************************************/
      tenSupportedConnectionTypes enGetSupportedConnectionTypes() const;

      /***************************************************************************
       ** FUNCTION:   tenEnabledInfo spi_tclConnSettings::enGetDeviceAuthEnableInfo()
       ***************************************************************************/
      /*!
       * \fn     tenEnabledInfo enGetDeviceAuthEnableInfo()
       * \brief  returns Device authorization enable info
       **************************************************************************/
      tenEnabledInfo enGetDeviceAuthEnableInfo() const;

      /***************************************************************************
       ** FUNCTION:  tenDeviceCategory spi_tclConnSettings::enGetTechnologyPreference
       ***************************************************************************/
      /*!
       * \fn     tenDeviceCategory enGetTechnologyPreference()
       * \brief  returns Device type preference
       **************************************************************************/
      tenDeviceCategory enGetTechnologyPreference() const;

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclConnSettings::vSetTechnologyPreference
       ***************************************************************************/
      /*!
       * \fn     t_Void vSetTechnologyPreference()
       * \brief  returns Device type preference
       **************************************************************************/
      t_Void vSetTechnologyPreference(tenDeviceCategory enTechnologyPref);

      /***************************************************************************
       ** FUNCTION:  tenML10Support spi_tclConnSettings::enGetML10Support
       ***************************************************************************/
      /*!
       * \fn     tenML10Support enGetML10Support()
       * \brief  returns Mirrorlink 1.0 support
       **************************************************************************/
      tenML10Support enGetML10Support() const;

      /***************************************************************************
       ** FUNCTION:  tenEnabledInfo spi_tclConnSettings::enGetCertificateType
       ***************************************************************************/
      /*!
       * \fn     tenEnabledInfo enGetCertificateType()
       * \brief  returns Certificate type
       **************************************************************************/
      tenCertificateType enGetCertificateType() const;

      /***************************************************************************
       ** FUNCTION:  tenSelModePriority spi_tclConnSettings::enGetSelectionModePriority
       ***************************************************************************/
      /*!
       * \fn     tenSelModePriority enGetSelectionModePriority()
       * \brief  returns Priority of selection mode
       **************************************************************************/
      tenSelModePriority enGetSelectionModePriority();

      /***************************************************************************
       ** FUNCTION:  tenDAPPreference spi_tclConnSettings::enGetDAPPreference
       ***************************************************************************/
      /*!
       * \fn     tenDAPPreference enGetDAPPreference()
       * \brief  returns DAP Preference
       **************************************************************************/
      tenDAPPreference enGetDAPPreference();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclConnSettings::vGetPersistentStoragePath
       ***************************************************************************/
      /*!
       * \fn     t_Void vGetPersistentStoragePath()
       * \brief  returns path for persistent Storage
       **************************************************************************/
      t_Void vGetPersistentStoragePath(t_String &rfszPersStoragePath);

      /***************************************************************************
       ** FUNCTION:  t_U32 spi_tclConnSettings::u32GetDeviceHistorySize
       ***************************************************************************/
      /*!
       * \fn     t_U32 u32GetDeviceHistorySize()
       * \brief  returns the Maximum size of Device History Database
       **************************************************************************/
      t_U32 u32GetDeviceHistorySize();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclConnSettings::bIsXMLValidationEnabled
       ***************************************************************************/
      /*!
       * \fn     t_Bool bIsXMLValidationEnabled()
       * \brief  Method to check whether the XML validation is enabled or not
       * \retval t_Bool
       **************************************************************************/
      t_Bool bIsXMLValidationEnabled() const;

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclConnSettings::bIsEarlyRoleSwitchRequired
       ***************************************************************************/
      /*!
       * \fn     t_Bool bIsEarlyRoleSwitchRequired()
       * \brief  Method to check if Carplay early role switch needed
       * \retval t_Bool
       **************************************************************************/
      t_Bool bIsEarlyRoleSwitchRequired() const;

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclConnSettings::vGetHeadUnitInfo
       ***************************************************************************/
      /*!
       * \fn     t_Bool vGetHeadUnitInfo()
       * \brief  Method to retrieve head unit information
       * \retval t_Bool
       **************************************************************************/
      t_Void vGetHeadUnitInfo(trHeadUnitInfo &rfrHeadUnitInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclConnSettings::vGetDefaultProjUsageSettings
       ***************************************************************************/
      /*!
       * \fn     t_Bool vGetDefaultProjUsageSettings()
       * \brief  Method to retrieve default setting for projection usage
       * \param [OUT] : returns the current value of device projection enable
       * \enSPIType  : indicates the type of SPI technology. e8DEV_TYPE_UNKNOWN indicates default value for any SPI technology
       * \retval t_Void
       **************************************************************************/
      t_Void vGetDefaultProjUsageSettings(tenEnabledInfo &enEnabledInfo, tenDeviceCategory enSPIType = e8DEV_TYPE_UNKNOWN);

   private:
      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclConnSettings::spi_tclConnSettings
       ***************************************************************************/
      /*!
       * \fn     spi_tclConnSettings()
       * \brief  Default Constructor
       * \sa      ~spi_tclConnSettings()
       **************************************************************************/
      spi_tclConnSettings();

      /*************************************************************************
       ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
       *************************************************************************/
      /*!
       * \fn     virtual bool bXmlReadNode(xmlNode *poNode)
       * \brief  virtual function to read data from a xml node
       * \param  poNode : [IN] pointer to xml node
       * \retval bool : true if success, false otherwise.
       *************************************************************************/
      virtual t_Bool bXmlReadNode(xmlNodePtr poNode);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclConnSettings::vReadConfigurationData
       ***************************************************************************/
      /*!
       * \fn     t_Void vReadConfigurationData()
       * \brief  Reads configuration data from the configuration handlers specific to the projects
       **************************************************************************/
      t_Void vReadConfigurationData();

      //! Generic singleton class (to access private constructor)
      friend class GenericSingleton<spi_tclConnSettings> ;

      //! Following member variables store the settings read from xml file

      //! DiPo enable info
      tenEnabledInfo m_enDiPoEnabled;

      //! ML Enable Info
      tenEnabledInfo m_enMLEnabled;

      //! Delay before automatic device selection on starup
      t_U32 m_u32DelayForSelection;

      //! Selection mode for automatic device selection
      tenDeviceSelectionMode m_enSelMode;

      //! Maximum Mirrorlink version supported
      trVersionInfo m_rMaxVer;

      //! Supported Connection Types (USB, Wi-Fi etc)
      tenSupportedConnectionTypes m_enSupportedConnType;

      //! Device Authorization Enabled
      tenEnabledInfo m_enDeviceAuthEnableInfo;

      //! Selection mode Priority
      tenSelModePriority m_enSelPrority;

      //! Preferred attestation mode
      tenDAPPreference m_enDAPPref;

      //! Type of Mirrorlink 1.0 Support
      tenML10Support m_enML10Support;

      //! Storage path for Device History
      t_String m_szPerStoragePath;

      //! Size of Device History
      t_U32 m_u32DevHistorySize;

      //! XML validation 
      t_Bool m_bXMLValidationEnabled;

      //! setting to indicate if Carplay early role switch needed
      t_Bool m_bDiPoEarlyRoleSwitch;

      //! Head unit information
      trHeadUnitInfo m_rHeadUnitInfo;

      //! Certificate type
      tenCertificateType m_enCertificateType;
};
/*! } */
#endif // SPI_CONNSETTINGS_H_
