/*!
 *******************************************************************************
 * \file             spi_tclConnMngr.h
 * \brief            Core implementation for Connection Management
 * \addtogroup       Connectivity
 * \{
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Core implementation for Connection Management. Handles different
 SPI connections (Ex DiPo, Mirrorlink)
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
 31.07.2014 |  Ramya Murthy (RBEI/ECP2)    | SPI feature configuration via LoadSettings()
 10.12.2014 | Shihabudheen P M             | Changed for blocking device usage 
                                             preference updates during 
                                             select/deselect is in progress. 
 05.11.2014 |  Ramya Murthy (RBEI/ECP2)    | Added callback for Application metadata.
 25.01.2016 | Rachana L Achar              | Logiscope improvements

 \endverbatim
 ******************************************************************************/

#ifndef SPI_CONNMNGR_H_
#define SPI_CONNMNGR_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "Lock.h"
#include "spi_ConnMngrTypeDefines.h"
#include "spi_tclLifeCycleIntf.h"

//! Forward declarations
class spi_tclDeviceListHandler;
class spi_tclConnMngrResp;
class spi_tclConnection;

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclConnMngr
 * \brief Core implementation for Connection Management. Handles different
 *        SPI connections (Ex DiPo, Mirrorlink)
 */
class spi_tclConnMngr : public spi_tclLifeCycleIntf
{

   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::spi_tclConnMngr
       ***************************************************************************/
      /*!
       * \fn     spi_tclConnMngr()
       * \brief  parameterized Constructor
       * \param  poRespInterface: pointer to Response interface to post asynchronous
       *         responses
       * \sa     ~spi_tclConnMngr()
       **************************************************************************/
      spi_tclConnMngr(spi_tclConnMngrResp *poRespInterface);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::~spi_tclConnMngr
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclConnMngr()
       * \brief  Destructor
       * \sa     spi_tclConnMngr()
       **************************************************************************/
      ~spi_tclConnMngr();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclConnMngr::bInitialize()
       ***************************************************************************/
      /*!
       * \fn      bInitialize()
       * \brief   Method to Initialize connection Manager: Start device detection,
       *          Register for callbacks
       * \retval  true: if the initialization of connection manager is successful,
       *          false: on failure
       * \sa      bUnInitialize()
       **************************************************************************/
      virtual t_Bool bInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclConnMngr::bUnInitialize()
       ***************************************************************************/
      /*!
       * \fn      bUnInitialize()
       * \brief   Method to UnInitialize connection Manager: Stop device detection.
       * \sa      bInitialize()
       **************************************************************************/
      virtual t_Bool bUnInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclConnMngr::vLoadSettings(const trSpiFeatureSupport&...)
       ***************************************************************************/
      /*!
       * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
       * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
       * \sa      vSaveSettings()
       **************************************************************************/
      virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclConnMngr::vSaveSettings()
       ***************************************************************************/
      /*!
       * \fn      vSaveSettings()
       * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
       * \sa      vLoadSettings()
       **************************************************************************/
      virtual t_Void vSaveSettings();

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vClearDeviceHistory
       ***************************************************************************/
      /*!
       * \fn     vClearDeviceHistory
       * \brief  Clears device history persistently stored on target
       **************************************************************************/
      t_Void vClearDeviceHistory()const;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::bIsDeviceValid
       ***************************************************************************/
      /*!
       * \fn     bIsDeviceValid
       * \brief  Interface to get device information for a particular device handle
       * \param  cou32DeviceHandle: Device handle of the  device
       * \retval true: if the device is valid otherwise false
       **************************************************************************/
      t_Bool bIsDeviceValid(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::bGetDeviceInfo
       ***************************************************************************/
      /*!
       * \fn     bGetDeviceInfo
       * \brief  Interface to get device information for a particular device handle
       * \param  cou32DeviceHandle: Device handle of the  device
       * \param  rfrDeviceInfo: [OUT] Device information for the requested device
       * \retval true: if the device is valid otherwise false
       **************************************************************************/
      t_Bool bGetDeviceInfo(const t_U32 cou32DeviceHandle,
               trDeviceInfo &rfrDeviceInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vGetDeviceList
       ***************************************************************************/
      /*!
       * \fn     vGetDeviceList
       * \brief  interface to get SPI devices list
       * \param  rfvecDeviceInfoList:[OUT] reference to the device list which will
       *         be populated with the detected SPI devices
       * \param  bCertifiedOnly: if true, only certified devices will be reported.
       *         if false, all detected devices will be reported
       * \param  bConnectedOnly: if true only connected devices will be returned
       **************************************************************************/
      t_Void vGetDeviceList(std::vector<trDeviceInfo>& rfvecDeviceInfoList,
               t_Bool bCertifiedOnly = false, t_Bool bConnectedOnly =
                        false);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vGetEntireDeviceList
       ***************************************************************************/
      /*!
       * \fn     vGetEntireDeviceList
       * \brief  interface to get SPI devices list with complete device info
       * \param  rfrmapDeviceInfoList: reference to the device list which will
       *         be populated with the detected SPI devices
       **************************************************************************/
      t_Void vGetEntireDeviceList(std::map<t_U32, trEntireDeviceInfo> &rfrmapDeviceInfoList);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vOnSelectDevice
       ***************************************************************************/
      /*!
       * \fn     vOnSelectDevice
       * \brief  Called when a device is selected/ deselected by the user.
       * \param  u32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevConnType   : Identifies the Connection Type.
       * \param  enDevSelReq    : Identifies the Connection Request.
       * \param  enDAPUsage      : Identifies when DAP has to be triggered
       *                           for the selected ML device.
       *                           This value is not considered for
       *                           de-selection of device.
       * \param  enCDBUsage      : Identifies Usage of CDB for the selected ML device.
       *                           This value is not considered for
       *                           de-selection of device
       * \param  bIsHMITrigger   : true if HMI has triggered select device event
       * \param  corUsrCntxt     : User Context Details.
       **************************************************************************/
      t_Void vOnSelectDevice(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceConnectionReq enDevSelReq, tenEnabledInfo enDAPUsage,
               tenEnabledInfo enCDBUsage, t_Bool bIsHMITrigger,
               const trUserContext corUsrCntxt);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclConnMngr::vOnSelectDeviceResult()
      ***************************************************************************/
      /*!
      * \fn      t_Void vOnSelectDeviceResult
      * \brief   To perform the actions that are required, after the select device is
      *           successful
      * \pram    cou32DeviceHandle  : [IN] Uniquely identifies the target Device.
      * \pram    enDevSelReq : [IN] Identifies the Connection Request.
      * \pram    coenRespCode: [IN] Response code. Success/Failure
      * \pram    enDevCat    : [IN] Device Category. ML/DiPo
      * \param  bIsHMITrigger   : true if HMI has triggered select device
      * \retval  t_Void
      **************************************************************************/
      t_Void vOnSelectDeviceResult(const t_U32 cou32DeviceHandle,
         const tenDeviceConnectionReq coenConnReq,
         const tenResponseCode coenRespCode,
         tenDeviceCategory enDevCat, t_Bool bIsHMITrigger);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vSetUserDeselectionFlag
       ***************************************************************************/
      /*!
       * \fn     vSetUserDeselectionFlag
       * \brief  Sets the flag when user deselects the device from HMI
       * \param  cou32DeviceHandle :Device Handle
       * \param  bState : indicates the value of UserDeselectionFlag
       **************************************************************************/
      t_Void vSetUserDeselectionFlag(const t_U32 cou32DeviceHandle, t_Bool bState);

      /***************************************************************************
      ** FUNCTION:  spi_tclConnMngr::bGetUserDeselectionFlag
      ***************************************************************************/
     /*!
      * \fn     bGetUserDeselectionFlag
      * \brief  returns the flag as true when user deselects the device from HMI
      * \param  cou32DeviceHandle :Device Handle
      * \retval  bState : indicates the value of UserDeselectionFlag
      **************************************************************************/
     t_Bool bGetUserDeselectionFlag(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::bSetDevProjUsage
       ***************************************************************************/
      /*!
       * \fn     bSetDevProjUsage
       * \brief  Called when the SPI feature is turned ON or OFF by the user
       * \param  enSPIType: indicates the SPI Type (Mirrorlink, Dipo ..)
       * \param  enSPIState : indicates the service status of enSPIType
       * \retval true: if the requested service state was switched
       *         false: on failure
       **************************************************************************/
      t_Bool bSetDevProjUsage(tenDeviceCategory enSPIType, tenEnabledInfo enSPIState);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::bGetDevProjUsage
       ***************************************************************************/
      /*!
       * \fn     bGetDevProjUsage
       * \brief  Returns the present SPI Feature State
       * \param  enSPIType: SPI service type for which state is
       *         requested (Mirrorlink, DiPo ..)
       * \param  rfenEnabledInfo:  [OUT]Indicates if enSPIType is enabled or not
       **************************************************************************/
      t_Bool bGetDevProjUsage(tenDeviceCategory enSPIType, tenEnabledInfo &rfenEnabledInfo) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::enGetDeviceCategory
       ***************************************************************************/
      /*!
       * \fn     enGetDeviceCategory
       * \brief  returns the category of the device specified by cou32DeviceHandle
       * \param  cou32DeviceHandle: Device handle whose category is requested
       * \retval e8DEV_TYPE_DIPO : if cou32DeviceHandle is DiPo capable
       *         e8DEV_TYPE_MIRRORLINK: if cou32DeviceHandle is Mirrorlink capable
       *         e8DEV_TYPE_UNKNOWN : if cou32DeviceHandle type is not known
       **************************************************************************/
      tenDeviceCategory enGetDeviceCategory(
               const t_U32 cou32DeviceHandle) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vSetDeviceCategory
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceCategory
       * \brief  Sets the Device category for the device
       * \param  cou32DeviceHandle :Device Handle
       * \param  enDeviceCategory : indicates the device category to be used for selection
       **************************************************************************/
      t_Void vSetDeviceCategory(const t_U32 cou32DeviceHandle, tenDeviceCategory enDeviceCategory);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::enGetDeviceConnStatus
       ***************************************************************************/
      /*!
       * \fn     enGetDeviceConnStatus
       * \brief  returns the connection status of the device specified by cou32DeviceHandle
       * \param  cou32DeviceHandle: Device handle whose connection status is requested
       * \retval e8DEV_NOT_CONNECTED : if cou32DeviceHandle is Not connected
       *         e8DEV_CONNECTED: if cou32DeviceHandle is Connected
       **************************************************************************/
      tenDeviceConnectionStatus enGetDeviceConnStatus(
               const t_U32 cou32DeviceHandle) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::enGetDeviceConnType
       ***************************************************************************/
      /*!
       * \fn     enGetDeviceConnType
       * \brief  returns the connection type of the device specified by cou32DeviceHandle
       * \param  cou32DeviceHandle: Device handle whose connection status is requested
       **************************************************************************/
      tenDeviceConnectionType enGetDeviceConnType(
               const t_U32 cou32DeviceHandle) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vGetBTAddress
       ***************************************************************************/
      /*!
       * \fn     vGetBTAddress
       * \brief  Gets the BT Address of the device : cou32DeviceHandle
       * \param  cou32DeviceHandle: Device handle whose connection status is requested
       * \param  rfszBTAddress: [OUT] BT address for the requested device handle
       **************************************************************************/
      t_Void vGetBTAddress(const t_U32 cou32DeviceHandle, t_String &rfszBTAddress) const;

      /***************************************************************************
      ** FUNCTION:  spi_tclConnMngr::u32GetNoofConnectedDevices
      ***************************************************************************/
     /*!
      * \fn     u32GetNoofConnectedDevices
      * \brief  returns no of connected devices
      * \retval  t_U32 : indicates the no of connected devices
      **************************************************************************/
     t_U32 u32GetNoofConnectedDevices();

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vRemoveDevice
       ***************************************************************************/
      /*!
       * \fn     vRemoveDevice
       * \brief  interface to remove device from device list
       * \param  cou32DeviceHandle: Device Handle of the device to be removed
       **************************************************************************/
      t_Void vRemoveDevice(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclConnMngr::vSetDeviceUsagePreference
       ***************************************************************************/
      /*!
       * \fn     vSetDeviceUsagePreference
       * \brief  Interface to set the preference for the usage of the connected
       *         Mirror Link/DiPO device. This can be set for individual devices or
       *         for all the connected Mirror Link/DiPO devices.
       * \param  cou32DeviceHandle  : Unique handle which identifies the device.
       *         If the value is 0xFFFFFFFF, then this function sets the overall
       *         preference usage for Mirror Link/DiPO.
       * \param  enDeviceCategory : Device Type Information(Mirror Link/iPOD Out).
       * \param  enUsagePref      : Enable Information.
       * \param  rfrUsrCntxt      : User Context Details.
       **************************************************************************/
      t_Void vSetDeviceUsagePreference(const t_U32 cou32DeviceHandle,
               tenDeviceCategory enDeviceCategory,
               tenEnabledInfo enUsagePref,
               const trUserContext &rfrUsrCntxt);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclConnMngr::bGetDeviceUsagePreference
       ***************************************************************************/
      /*!
       * \fn     bGetDeviceUsagePreference
       * \brief  Interface to get the preference for the usage of the
       *         connected Mirror Link/iPod Out device during startup.
       * \param  cou32DeviceHandle  : Unique handle which identifies the device.
       *         If the value is 0xFFFFFFFF, then this function sets the overall
       *         preference usage for Mirror Link/DiPO.
       * \param  enDeviceCategory : Device Type Information(Mirror Link/iPOD Out).
       * \param  [OUT] rfenEnabledInfo : Enable Information.
       **************************************************************************/
      t_Bool bGetDeviceUsagePreference(const t_U32 cou32DeviceHandle,
               tenDeviceCategory enDeviceCategory,
               tenEnabledInfo& rfenEnabledInfo) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vSetSelectError
       ***************************************************************************/
      /*!
       * \fn     vSetSelectError
       * \brief  set to true if the error occurs on device side during selection
	    *         Ex: Role switch failure
       * \param cou32DeviceHandle: Device handle of the selected device
       * \parama bIsError value of the device error flag
       **************************************************************************/
      t_Void vSetSelectError(const t_U32 cou32DeviceHandle, t_Bool bIsError);

	  /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::bIsSelectError
       ***************************************************************************/
      /*!
       * \fn     bIsSelectError
       * \brief  Returns true if the error occurs on device side during selection
	   *         Ex: Role switch failure
       * \param cou32DeviceHandle: Device handle of the selected device
       * \retval value of the device error flag
       **************************************************************************/
      t_Bool bIsSelectError(const t_U32 cou32DeviceHandle);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclConnMngr::bIsDiPoRoleSwitchRequired
      ***************************************************************************/
      /*!
      * \fn     t_Bool bIsDiPoRoleSwitchRequired
      * \brief  Interface to check if the device specified with device id cou32DeviceID
      *         is same as the last used device
      * \param  [IN] cou32DeviceID : Device Id to be checked
      * \retval true if the device is same as last connected device otherwise false
      **************************************************************************/
      t_Bool bIsDiPoRoleSwitchRequired(const t_U32 cou32DeviceID, const trUserContext corUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclConnMngr::vSetDevSelectorStatus
      ***************************************************************************/
      /*!
      * \fn     t_Bool vSetDevSelectorStatus
      * \brief  Interface to check if the device specified with device id cou32DeviceID
      *         is same as the last used device
      * \param  [IN] bDevSelStatus : Treu if device selector is busy, false otherwise
      * \retval NONE
      **************************************************************************/
      t_Void vSetDevSelectorBusyStatus(t_Bool bDevSelStatus);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::enGetUserPreference
       ***************************************************************************/
      /*!
       * \fn     enGetUserPreference
       * \brief  Returns user preferred SPI technology for the device
       * \param cou32DeviceHandle: Device handle
       **************************************************************************/
      tenUserPreference enGetUserPreference(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vSetUserPreference
       ***************************************************************************/
      /*!
       * \fn     vSetUserPreference
       * \brief   Sets user preferred SPI technology for the device
       * \param cou32DeviceHandle: Device handle
       * \param  enUserPref: user preference SPI technology for the device
       **************************************************************************/
      t_Void vSetUserPreference(const t_U32 cou32DeviceHandle, tenUserPreference enUserPref);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::bGetUSBConnectedFlag
       ***************************************************************************/
      /*!
       * \fn     bGetUSBConnectedFlag
       * \brief  Returns true if when USB connection for the device is active
       * \param cou32DeviceHandle: Device handle
       * \retval true if when USB connection for the device is active else false
       **************************************************************************/
      t_Bool bGetUSBConnectedFlag(const t_U32 cou32DeviceHandle);

      /**************************************************************************
      ** FUNCTION   : tVoid spi_tclConnMngr:: vSetTechnologyPreference()
      ***************************************************************************/
     /*!
      * \fn      t_Void vSetTechnologyPreference()
      * \brief   To set the preferred SPI technology for devices which support more than once technology
      * \param   [IN].cou32DeviceHandle - Device handle for which the prefernce is applicable.
      *               if cou32DeviceHandle is 0xFFFF then the preference is applicable for overall setting
      * \param   [IN].vecTechnologyPreference - contains the technology preference order
      * \retval  t_Void
      **************************************************************************/
     t_Void vSetTechnologyPreference(const t_U32 cou32DeviceHandle, std::vector<tenDeviceCategory> vecTechnologyPreference) const;

     /**************************************************************************
     ** FUNCTION   : tVoid spi_tclConnMngr:: vGetTechnologyPreference()
     ***************************************************************************/
    /*!
     * \fn      t_Void vGetTechnologyPreference()
     * \brief   To get the  previously set preferred SPI technology
     * \param   [IN].cou32DeviceHandle - Device handle for which the prefernce is applicable.
     *               if cou32DeviceHandle is 0xFFFF then the preference is applicable for overall setting
     * \param   [OUT].vecTechnologyPreference - contains the technology preference order
     * \retval  t_Void
     **************************************************************************/
    t_Void vGetTechnologyPreference(const t_U32 cou32DeviceHandle, std::vector<tenDeviceCategory> &rfrvecTechnologyPreference) const;

    /***************************************************************************
     ** FUNCTION:  spi_tclConnMngr::vSetDeviceAuthorization
     ***************************************************************************/
    /*!
     * \fn     vSetDeviceAuthorization
     * \brief  To set the device authorization based on user action
     * \param  u32DeviceHandle : Uniquely identifies the target Device.
     * \param  enUserAuthStatus : Authorization status
     **************************************************************************/
    t_Void vSetDeviceAuthorization(const t_U32 cou32DeviceHandle, tenUserAuthorizationStatus enUserAuthStatus);

    /***************************************************************************
     ** FUNCTION:  spi_tclConnMngr::enGetSPISupport
     ***************************************************************************/
    /*!
     * \fn     enGetSPISupport
     * \brief  Get SPI technology support for indicated device handle and category
     * \param  cou32DeviceHandle: Device handle
     * \param  enSPIType: SPI technology supporet to be checked
     * \retval Indicates if this SPi technology is supported
     **************************************************************************/
    tenSPISupport enGetSPISupport(const t_U32 cou32DeviceHandle, tenDeviceCategory enSPIType);

    /***************************************************************************
     ** FUNCTION:  spi_tclConnMngr::vSetSPISupport
     ***************************************************************************/
    /*!
     * \fn     vSetSPISupport
     * \brief  Sets SPI technology support for indicated device handle and category
     * \param  cou32DeviceHandle: Device handle
     * \param  enSPIType: SPI technology supporet to be checked
     * \param enSPISupport: indicates SPi technology is supported
     **************************************************************************/
    t_Void vSetSPISupport(const t_U32 cou32DeviceHandle, tenDeviceCategory enSPIType,
             tenSPISupport enSPISupport);

   private: 
      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      /***************************************************************************
      ** FUNCTION: spi_tclConnMngr(const spi_tclConnMngr &corfobjRhs)
      ***************************************************************************/
      /*!
      * \fn      spi_tclConnMngr(const spi_tclConnMngr &corfobjRhs)
      * \brief   Copy constructor not implemented hence made protected to prevent
      *          misuse
      **************************************************************************/
      spi_tclConnMngr(const spi_tclConnMngr &corfobjRhs);

      /***************************************************************************
      ** FUNCTION: const spi_tclConnMngr & operator=(const spi_tclConnMngr &corfobjRhs);
      ***************************************************************************/
      /*!
      * \fn      const spi_tclConnMngr & operator=(const spi_tclConnMngr &corfobjRhs);
      * \brief   assignment operator not implemented hence made protected to
      *          prevent misuse
      **************************************************************************/
      const spi_tclConnMngr & operator=(
         const spi_tclConnMngr &corfobjRhs);

      //! Callbacks to be called from ML or DiPo connection classes
      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vUpdateDAPStatusCb
       ***************************************************************************/
      /*!
       * \fn     vUpdateDAPStatusCb
       * \brief   Callback to Notify the client about DAP authentication progress
       *          information for a Mirror Link device.
       * \param  [IN] cou32DeviceHandle : Uniquely identifies the target Device.
       * \param  [IN] enDAPStatus : DAP Authentication Progress Status.
       **************************************************************************/
      t_Void vUpdateDAPStatusCb(const t_U32 cou32DeviceHandle,
               tenDAPStatus enDAPStatus);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vOnDeviceConnectionCb
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceConnectionCb
       * \brief  Callback to be Called by ML/ DiPo class when a new device is detected.
       * \param  cou32DeviceHandle: Device Handle of the detected device
       * \param  corfrDeviceInfo: Device info for the detected device
       **************************************************************************/
      t_Void vOnDeviceConnectionCb(const t_U32 cou32DeviceHandle,
               const trDeviceInfo& corfrDeviceInfo, tenDeviceCategory enReportingDiscoverer);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vOnDeviceDisconnectionCb
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceDisconnectionCb
       * \brief  Callback to be Called by ML/ DiPo class when a device is disconnected
       * \param  cou32DeviceHandle : Device handle of the disconnected device
       **************************************************************************/
      t_Void vOnDeviceDisconnectionCb(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat);

       /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vSelectDeviceResultCb
       ***************************************************************************/
      /*!
       * \fn     vSelectDeviceResultCb
       * \brief  Callback to update the select device result
       * \param  bSelDevResult : Select device result
       * \param  enConnreq: Indicated whether it is a selection or deselection request
       **************************************************************************/
      t_Void vSelectDeviceResultCb(tenErrorCode enErrorCode);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngr::vApplicationMediaMetaDataCb(...
      ***************************************************************************/
      /*!
      * \fn     vApplicationMediaMetaDataCb(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationMediaMetaData : Contains the media metadata information
      *              related to an application.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      t_Void vApplicationMediaMetaDataCb(const trAppMediaMetaData& rfcorApplicationMediaMetaData,
         const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngr::vApplicationPhoneDataCb(...
      ***************************************************************************/
      /*!
      * \fn     vApplicationPhoneDataCb(const trAppPhoneData& rfcorApplicationPhoneData,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationPhoneData : Contains the phone related information.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      t_Void vApplicationPhoneDataCb(const trAppPhoneData& rfcorApplicationPhoneData,
         const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclConnMngr::vApplicationMediaPlaytimeCb(...
      ***************************************************************************/
      /*!
      * \fn     vApplicationMediaPlaytimeCb(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to notify application media metadata to the client.
      * \param  [IN] rfcorApplicationMediaPlaytime : Contains the media play time of current playing track.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa
      **************************************************************************/
      t_Void vApplicationMediaPlaytimeCb(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
         const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vUpdateDeviceNameCb
       ***************************************************************************/
      /*!
       * \fn     vUpdateDeviceNameCb
       * \brief  updates the device name of the device identified by device handle
       * \param cou32DeviceHandle: Device Handle of the device to be added to history
       * \param rfrszDeviceName : Device Name to be set
       **************************************************************************/
      t_Void vUpdateDeviceNameCb(t_U32 cou32DeviceHandle, t_String szDeviceName);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks
       * \brief  Registers for callbacks to ML/DiPo classes
       **************************************************************************/
      t_Void vRegisterCallbacks();

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vHandleGenericDevices
       ***************************************************************************/
      /*!
       * \fn     vHandleGenericDevices
       * \brief  Handle Generic USB device connections (defines preference for connections)
       * \param  cou32DeviceHandle: Device handle
       * \param  corfrDeviceInfo: Device information
       **************************************************************************/
      t_Void vHandleGenericDevices(const t_U32 cou32DeviceHandle,
               const trDeviceInfo& corfrDeviceInfo, tenDeviceCategory enReportingDiscoverer);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::enGetDeviceSwitchType
       ***************************************************************************/
      /*!
       * \fn     enGetDeviceSwitchType
       * \brief  returns the type of switch needed for the specified device handle
       * \param  cou32DeviceHandle: Device handle
       * \retval type of switch needed
       **************************************************************************/
      tenDeviceCategory enGetDeviceSwitchType(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::bIsProcessingRequired
       ***************************************************************************/
      /*!
       * \fn     bIsProcessingRequired
       * \brief  returns true if further processing of the required
       *          before triggering SPI switch. If only one technology is enabled
       *          then the funtion will trigger appropriate switch
       * \param  cou32DeviceHandle: Device handle
       * \param  corfrDeviceInfo: Device information
       * \param  enReportingDiscoverer: Discoverer that reported the device
       * \retval true if further processing is needed
       **************************************************************************/
      t_Bool bIsProcessingRequired(const t_U32 cou32DeviceHandle,
               const trDeviceInfo& corfrDeviceInfo, tenDeviceCategory enReportingDiscoverer);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::bRequestDeviceSwitch
       ***************************************************************************/
      /*!
       * \fn     bRequestDeviceSwitch
       * \brief  Function to request device to switch to SPI mode or normal USB mode
       * \param  u32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevCat : Indicates SPI or device mode.
       *          @Note: e8DEV_TYPE_UNKNOWN : Indicates switch to USB mode
       **************************************************************************/
      t_Bool bRequestDeviceSwitch(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vEvaluateDeviceSwitch
       ***************************************************************************/
      /*!
       * \fn     vEvaluateDeviceSwitch
       * \brief  Evaluates if the device has to be switched to SPI mode or not
       * \param  u32DeviceHandle : Uniquely identifies the target Device.
       * \param  enDevCat : Indicates the device switch type (SPI or default mode).
       *          @Note: e8DEV_TYPE_UNKNOWN : Indicates switch to USB mode
       * \param corfrDeviceInfo: Device information
       **************************************************************************/
      t_Bool bEvaluateDeviceSwitch(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat,
               const trDeviceInfo& corfrDeviceInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vHandleSPITechnologySwitch
       ***************************************************************************/
      /*!
       * \fn     vHandleSPITechnologySwitch
       * \brief  Function to handle SPI technology switch when device supports more than one technology
       * \param  u32DeviceHandle : Uniquely identifies the target Device.
       * \param  corfrDeviceInfo : Device information
       **************************************************************************/
      t_Void vHandleSPITechnologySwitch(const t_U32 cou32DeviceHandle, const trDeviceInfo& corfrDeviceInfo,
            tenDeviceCategory enReportingDiscoverer);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vProceedwithConnection
       ***************************************************************************/
      /*!
       * \fn     vProceedwithConnection
       * \brief  Continues with connection process after authorization
       * \param  cou32DeviceHandle: Device Handle of the detected device
       * \param  corfrDeviceInfo: Device info for the detected device
       * \param  enReportingDiscoverer: Discoverer reporting device connection
       **************************************************************************/
      t_Void vProceedwithConnection(const t_U32 cou32DeviceHandle,
            const trDeviceInfo& corfrDeviceInfo, tenDeviceCategory enReportingDiscoverer);

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::vSendRoleSwitchResponse
       ***************************************************************************/
      /*!
       * \fn     vSendRoleSwitchResponse
       * \brief  Send response to role switch request
       * \param  cou32DeviceHandle: Device Handle of the detected device
       **************************************************************************/
      t_Void vSendRoleSwitchResponse(const t_U32 cou32DeviceHandle, const trUserContext corUsrCntxt);
	  
      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr::enGetConnectionIndex
       ***************************************************************************/
      /*!
       * \fn     enGetConnectionIndex(tenDeviceCategory enDevCat)
       * \brief  Get the connection index based on the device category
       * \param  enDevCat: [IN}Device category. AndroidAuto/Carplay/ML
       * \retval Connection pointer index of the selected device
       **************************************************************************/
      tenConnPointersIndex enGetConnectionIndex(tenDeviceCategory enDevCat);

      //! Pointer to the object handling device list of SPI devices
      spi_tclDeviceListHandler *m_poDeviceListHandler;

      //! Pointer to response interface for posting asynchronous responses
      spi_tclConnMngrResp *m_poConnMngrResp;

      //! array of connection pointers
      spi_tclConnection *m_poConnHandlers[e8_CONN_INDEX_SIZE];

      //! Stores the current status of SPI Service
      t_Bool m_bSPIState[e8_CONN_INDEX_SIZE];

      //! Indicates whether the device selector is busy or not.
      t_Bool m_bIsDevSelectorBusy;

      //! Lock to handle critical section for device disconnection reported from different discoverers
      Lock m_oDeviceDisconnection;

      //! Stores reported discoverer
      std::map<t_U32, trDeviceInfo> m_mapReportedDiscoverer;

      //! Protects m_mapReportedDiscoverer
      Lock m_oLockReportedDiscoverer;

      //! Stores the SPI technology enabled in calibration
      trSpiFeatureSupport m_rSPIFeatureSupport;

};

/*! } */
#endif // SPI_CONNMNGR_H_
