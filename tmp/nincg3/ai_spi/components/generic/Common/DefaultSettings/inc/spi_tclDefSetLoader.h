  /*!
 *******************************************************************************
 * \file         spi_tclDefSetLoader.cpp
 * \brief        Default settings loader class loads the default values
                 at the time of flashing a software on the target.
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Default settings loader class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 04.03.2014 |  Chaitra Srinivasa                | Initial Version

 
 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLDEFSETLOADER_H_
#define _SPI_TCLDEFSETLOADER_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "SPITypes.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/*!
* \class spi_tclDefSetLoader
* \brief This is the main Default settings loader class loads the default values
         at the time of flashing a software on the target.
*/
class spi_tclDefSetLoader 
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDefSetLoader::spi_tclDefSetLoader()
   ***************************************************************************/
   /*!
   * \fn      spi_tclDefSetLoader()
   * \brief   Default Constructor
   ***************************************************************************/
   spi_tclDefSetLoader();

   /***************************************************************************
   ** FUNCTION:  spi_tclDefSetLoader::~spi_tclDefSetLoader();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclDefSetLoader()
   * \brief   Virtual Destructor
   ***************************************************************************/
   virtual ~spi_tclDefSetLoader();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclDefSetLoader::bInitialize()
    ***************************************************************************/
   /*!
    * \fn      bInitialize()
    * \brief   Method to Initialize
    * \sa      bUnInitialize()
    **************************************************************************/
   virtual t_Bool bInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclDefSetLoader::bUnInitialize()
    ***************************************************************************/
   /*!
    * \fn      bUnInitialize()
    * \brief   Method to UnInitialize
    * \sa      bInitialize()
    **************************************************************************/
   virtual t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDefSetLoader::vRestoreSettings()
    ***************************************************************************/
   /*!
    * \fn      vRestoreSettings()
    * \brief   vRestoreSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vRestoreSettings();

    /***************************************************************************
    ** FUNCTION: t_Void spi_tclDefSetLoader::vLoadSettings()
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings()
    * \brief   vLoadSettings Method. Invoked during  NORMAL->OFF state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vLoadSettings(/*const trSpiFeatureSupport& */);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclDefSetLoader::vSaveSettings()
    ***************************************************************************/
   /*!
    * \fn      vSaveSettings()
    * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
    * \sa      vLoadSettings()
    **************************************************************************/
   virtual t_Void vSaveSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefSetLoader::vSetMLNotiSettingVal()
   ***************************************************************************/
   /*!
   * \fn      vSetMLNotiSettingVal(t_Bool bMLNotiSettingVal)
   * \brief   Called to set the default value to notification setting.
   * \param   [IN] bMLNotiSettingVal: default value for notification setting
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetMLNotiSettingVal(t_Bool bMLNotiSettingVal);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefSetLoader::vSetMLLinkEnableSetVal()
   ***************************************************************************/
   /*!
   * \fn      vSetMLLinkEnableSetVal(tenEnabledInfo enMLLinkEnableSetVal)
   * \brief   Called to set the default value to enable ML usage.
   * \param   [IN] enMLLinkEnableSetVal: default value for enabling ML link setting
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetMLLinkEnableSetVal(tenEnabledInfo enMLLinkEnableSetVal);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefSetLoader::vSetDipoEnableSetVal()
   ***************************************************************************/
   /*!
   * \fn      vSetDipoEnableSetVal(tenEnabledInfo enDipoEnableSetVal)
   * \brief   Called to set the default value to enable Dipo usage.
   * \param   [IN] enDipoEnableSetVal: default value for enabling Dipo setting
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetDipoEnableSetVal(tenEnabledInfo enDipoEnableSetVal);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefSetLoader::vSetAAPEnableSetVal()
   ***************************************************************************/
   /*!
   * \fn      vSetAAPEnableSetVal(tenEnabledInfo enAAPEnableSetVal)
   * \brief   Called to set the default value to enable AAP usage.
   * \param   [IN] enAAPEnableSetVal: default value for enabling AAP setting
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetAAPEnableSetVal(tenEnabledInfo enAAPEnableSetVal);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefSetLoader::vSetSteeringWheelPos()
   ***************************************************************************/
   /*!
   * \fn      vSetSteeringWheelPos(tenDriveSideInfo enSteeringWheelPos)
   * \brief   Called to set the default value to steering wheel position.
   * \param   [IN] enSteeringWheelPos: default value for steering wheel position
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSteeringWheelPos(tenDriveSideInfo enSteeringWheelPos);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefSetLoader::vSetTechPrefVal()
   ***************************************************************************/
   /*!
   * \fn      vSetTechPrefVal(tenDeviceCategory enTechPrefVal)
   * \brief   Called to set the default value to technology preference.
   * \param   [IN] enTechPrefVal: default value for technology preference
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetTechPrefVal(tenDeviceCategory enTechPrefVal);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefSetLoader::vSetSelectMode()
   ***************************************************************************/
   /*!
   * \fn      vSetSelectMode(tenDeviceSelectionMode enSelectMode)
   * \brief   Called to set the default value to selection mode.
   * \param   [IN] enSelectMode: default value for selection mode
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSelectMode(tenDeviceSelectionMode enSelectMode);


   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
    ** FUNCTION: spi_tclDefSetLoader(const spi_tclDefSetLoader &rfcoDefSetLoader)
    ***************************************************************************/
   /*!
    * \fn      spi_tclDefSetLoader(const spi_tclDefSetLoader &rfcoDefSetLoader)
    * \brief   Copy constructor not implemented hence made private
    **************************************************************************/
   spi_tclDefSetLoader(const spi_tclDefSetLoader &rfcoDefSetLoader);

   /***************************************************************************
    ** FUNCTION: const spi_tclDefSetLoader & operator=(
    **                                 const spi_tclDefSetLoader &rfcoDefSetLoader);
    ***************************************************************************/
   /*!
    * \fn      const spi_tclDefSetLoader & operator=(const spi_tclDefSetLoader &rfcoDefSetLoader);
    * \brief   assignment operator not implemented hence made private
    **************************************************************************/
   const spi_tclDefSetLoader & operator=(
            const spi_tclDefSetLoader &rfcoDefSetLoader);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefSetLoader::vLoadDefaultSetting()
   ***************************************************************************/
   /*!
   * \fn      vLoadDefaultSetting()
   * \brief   Called to load the default value to datapool.
   * \retval  None
   **************************************************************************/
   t_Void vLoadDefaultSetting();   			

   /***************************************************************************
   * ! Data members
   ***************************************************************************/


};

#endif // _SPI_TCLDEFSETLOADER_H_

