/***********************************************************************/
/*!
* \file  spi_tclDefaultSettings.h
* \brief Class to get the Default Settings
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Default Settings
AUTHOR:         Chaitra Srinivasa
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
03.03.2016  | Chaitra Srinivasa     | Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLDEFAULTSETTINGS_H_
#define _SPI_TCLDEFAULTSETTINGS_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "GenericSingleton.h"
#include "Xmlable.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclDefaultSettings
* \brief Class to get the Default Settings
****************************************************************************/
class spi_tclDefaultSettings: public GenericSingleton<spi_tclDefaultSettings>,
    public shl::xml::tclXmlReadable
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDefaultSettings::~spi_tclDefaultSettings()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclDefaultSettings()
   * \brief   Destructor
   * \sa      spi_tclDefaultSettings()
   **************************************************************************/
   ~spi_tclDefaultSettings();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDefaultSettings::bGetMLNotiSettingVal()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bGetMLNotiSettingVal()
   * \brief  To get the default ML Notification Setting Value
   * \retval t_Bool
   **************************************************************************/
   t_Bool bGetMLNotiSettingVal() const;

   /***************************************************************************
   ** FUNCTION:  tenEnabledInfo spi_tclDefaultSettings::enGetMLLinkEnableSetVal()
   ***************************************************************************/
   /*!
   * \fn     tenEnabledInfo enGetMLLinkEnableSetVal()
   * \brief  To get the default ML Link Enable Setting Value
   * \retval tenEnabledInfo
   **************************************************************************/
   tenEnabledInfo enGetMLLinkEnableSetVal() const;

   /***************************************************************************
   ** FUNCTION:  tenEnabledInfo spi_tclDefaultSettings::enGetDipoEnableSetVal()
   ***************************************************************************/
   /*!
   * \fn     tenEnabledInfo enGetDipoEnableSetVal()
   * \brief  To get the default Dipo Enable Setting Value
   * \retval tenEnabledInfo
   **************************************************************************/
   tenEnabledInfo enGetDipoEnableSetVal() const;

   /***************************************************************************
   ** FUNCTION:  tenEnabledInfo spi_tclDefaultSettings::enGetAAPEnableSetVal()
   ***************************************************************************/
   /*!
   * \fn     tenEnabledInfo enGetAAPEnableSetVal()
   * \brief  To get the default AAP Enable Setting Value
   * \retval tenEnabledInfo
   **************************************************************************/
   tenEnabledInfo enGetAAPEnableSetVal() const;

   /***************************************************************************
   ** FUNCTION:  tenDriveSideInfo  spi_tclDefaultSettings::enGetSteeringWheelPos()
   ***************************************************************************/
   /*!
   * \fn     tenDriveSideInfo  enGetSteeringWheelPos()
   * \brief  To get the default Steering Wheel Position
   * \retval tenDriveSideInfo 
   **************************************************************************/
   tenDriveSideInfo enGetSteeringWheelPos() const;

   /***************************************************************************
   ** FUNCTION:  tenDeviceCategory spi_tclDefaultSettings::enGetTechPrefVal()
   ***************************************************************************/
   /*!
   * \fn     tenDeviceCategory  enGetTechPrefVal()
   * \brief  To get the default Selection Mode
   * \retval tenDeviceCategory 
   **************************************************************************/
   tenDeviceCategory enGetTechPrefVal() const;

   /***************************************************************************
   ** FUNCTION:  tenDeviceSelectionMode spi_tclDefaultSettings::enGetSelectMode()
   ***************************************************************************/
   /*!
   * \fn     tenDriveSideInfo  enGetSelectMode()
   * \brief  To get the default Selection Mode
   * \retval tenDeviceSelectionMode  
   **************************************************************************/
   tenDeviceSelectionMode enGetSelectMode() const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDefaultSettings::vReadAppSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vReadAppSettings()
   * \brief  To read the settings from XML
   * \retval t_Void
   **************************************************************************/
   t_Void vReadAppSettings();


   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<spi_tclDefaultSettings>;


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDefaultSettings::spi_tclDefaultSettings()
   ***************************************************************************/
   /*!
   * \fn      spi_tclDefaultSettings()
   * \brief   Default Constructor
   * \sa      ~spi_tclDefaultSettings()
   **************************************************************************/
   spi_tclDefaultSettings();

   /***************************************************************************
   ** FUNCTION:  spi_tclDefaultSettings& spi_tclDefaultSettings::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDefaultSettings& operator= (const spi_tclDefaultSettings &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
  spi_tclDefaultSettings& operator= (const spi_tclDefaultSettings &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclDefaultSettings::spi_tclDefaultSettings(const ..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDefaultSettings(const spi_tclDefaultSettings &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDefaultSettings(const spi_tclDefaultSettings &corfrSrc);

   /*************************************************************************
   ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
   *************************************************************************/
   /*!
   * \fn     virtual bool bXmlReadNode(xmlNode *poNode)
   * \brief  virtual function to read data from a xml node
   * \param  poNode : [IN] pointer to xml node
   * \retval bool : true if success, false otherwise.
   *************************************************************************/
   virtual t_Bool bXmlReadNode(xmlNodePtr poNode);

   /*************************************************************************
    ** Data Members
    *************************************************************************/

   /*
    * \brief   ML Notification Setting Value
    */
   t_Bool m_enMLNotiSettingVal;

   /*
    * \brief   ML Link Enable Setting Value
    */
   tenEnabledInfo m_enMLLinkEnableSetVal;

   /*
    * \brief   Dipo Enable Setting Value
    */
   tenEnabledInfo m_enDipoEnableSetVal;

   /*
    * \brief   AAP Enable Setting Value
    */
   tenEnabledInfo m_enAAPEnableSetVal;

   /*
    * \brief   Steering Wheel Position
    */
   tenDriveSideInfo m_enSteeringWheelPos;

   /*
    * \brief   Technology Preference Value
    */
   tenDeviceCategory m_enTechPrefVal;

   /*
    * \brief   Selection Mode
    */
   tenDeviceSelectionMode m_enSelectMode;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclDefaultSettings

#endif //_SPI_TCLDEFAULTSETTINGS_H_
