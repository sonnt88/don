/***********************************************************************/
/*!
* \file  spi_tclDefaultSettings.h
* \brief Class to get the Default Settings
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Default Settings
AUTHOR:         Chaitra Srinivasa
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
03.03.2016  | Chaitra Srinivasa     | Initial Version

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "FileHandler.h"
#include "XmlDocument.h"
#include "XmlReader.h"
#include "spi_tclDefaultSettings.h"
#include "SPITypes.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DEFSET
      #include "trcGenProj/Header/spi_tclDefaultSettings.cpp.trc.h"
   #endif
#endif

#ifdef VARIANT_S_FTR_ENABLE_GM
   static const t_Char* pczConfigFilePath = "/opt/bosch/gm/policy.xml";
#else
   static const t_Char* pczConfigFilePath = "/opt/bosch/spi/xml/policy.xml";
#endif

using namespace shl::xml;
/***************************************************************************
** FUNCTION:  spi_tclDefaultSettings::spi_tclDefaultSettings()
***************************************************************************/
spi_tclDefaultSettings::spi_tclDefaultSettings()
   : m_enMLNotiSettingVal(false),
     m_enMLLinkEnableSetVal(e8USAGE_ENABLED),
     m_enDipoEnableSetVal(e8USAGE_ENABLED),
     m_enAAPEnableSetVal(e8USAGE_ENABLED),
     m_enSteeringWheelPos(e8UNKNOWN_DRIVE_SIDE),
     m_enTechPrefVal(e8DEV_TYPE_ANDROIDAUTO),
     m_enSelectMode(e16DEVICESEL_UNKNOWN)
{
   ETG_TRACE_USR1((" %s entered", __PRETTY_FUNCTION__));
   vReadAppSettings();
}

/***************************************************************************
** FUNCTION:  spi_tclDefaultSettings::~spi_tclDefaultSettings()
***************************************************************************/
spi_tclDefaultSettings::~spi_tclDefaultSettings()
{
   ETG_TRACE_USR1((" %s entered", __PRETTY_FUNCTION__));
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDefaultSettings::bGetMLNotiSettingVal()
***************************************************************************/
t_Bool spi_tclDefaultSettings::bGetMLNotiSettingVal() const
{
   ETG_TRACE_USR2(("[DESC]: Mirrorlink notification settings default value %u \n",
                ETG_ENUM(BOOL, m_enMLNotiSettingVal)));
   return m_enMLNotiSettingVal;
}

/***************************************************************************
** FUNCTION:  tenEnabledInfo spi_tclDefaultSettings::enGetMLLinkEnableSetVal()
***************************************************************************/
tenEnabledInfo spi_tclDefaultSettings::enGetMLLinkEnableSetVal() const
{
   ETG_TRACE_USR2(("[DESC]: Mirrorlink enable settings default value %u \n",
               ETG_ENUM(tenEnabledInfo, m_enMLLinkEnableSetVal)));
   return m_enMLLinkEnableSetVal;
}

/***************************************************************************
** FUNCTION:  tenEnabledInfo spi_tclDefaultSettings::enGetDipoEnableSetVal()
***************************************************************************/
tenEnabledInfo spi_tclDefaultSettings::enGetDipoEnableSetVal() const
{
   ETG_TRACE_USR2(("[DESC]: CarPlay enable settings default value %u \n",
              ETG_ENUM(tenEnabledInfo, m_enDipoEnableSetVal)));
   return m_enDipoEnableSetVal;
}

/***************************************************************************
** FUNCTION:  tenEnabledInfo spi_tclDefaultSettings::enGetAAPEnableSetVal()
***************************************************************************/
tenEnabledInfo spi_tclDefaultSettings::enGetAAPEnableSetVal() const
{
   ETG_TRACE_USR2(("[DESC]: AAP enable settings default value %u \n",
              ETG_ENUM(tenEnabledInfo, m_enAAPEnableSetVal)));
   return m_enAAPEnableSetVal;
}

/***************************************************************************
** FUNCTION:  tenDriveSideInfo spi_tclDefaultSettings::enGetSteeringWheelPos()
***************************************************************************/
tenDriveSideInfo spi_tclDefaultSettings::enGetSteeringWheelPos() const
{
   ETG_TRACE_USR2(("[DESC]: Steering wheel position default value %u \n",
             ETG_ENUM(tenDriveSideInfo, m_enSteeringWheelPos)));
   return m_enSteeringWheelPos;
}

/***************************************************************************
** FUNCTION:  tenDeviceCategory spi_tclDefaultSettings::enGetTechPrefVal()
***************************************************************************/
tenDeviceCategory spi_tclDefaultSettings::enGetTechPrefVal() const
{
   ETG_TRACE_USR2(("[DESC]: Technology preference default value %u \n",
            ETG_ENUM(tenDeviceCategory, m_enTechPrefVal)));
   return m_enTechPrefVal;
}

/***************************************************************************
** FUNCTION:  tenDeviceSelectionMode spi_tclDefaultSettings::enGetSelectMode()
***************************************************************************/
tenDeviceSelectionMode spi_tclDefaultSettings::enGetSelectMode() const
{
   ETG_TRACE_USR2(("[DESC]: Selection mode default value %u \n",
             ETG_ENUM(tenDeviceSelectionMode, m_enSelectMode)));
   return m_enSelectMode;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefaultSettings::vReadAppSettings()
***************************************************************************/
t_Void spi_tclDefaultSettings::vReadAppSettings()
{
   ETG_TRACE_USR1(("spi_tclDefaultSettings::vReadAppSettings entered\n"));

   //Check the validity of the xml file
   spi::io::FileHandler oPolicySettingsFile(pczConfigFilePath,
      spi::io::SPI_EN_RDONLY);
   if (true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument oXmlDoc(pczConfigFilePath);
      tclXmlReader oXmlReader(&oXmlDoc, this);

      oXmlReader.bRead("DEFSET");
   } // if (true == oPolicySettingsFile.bIsValid())
}

/*************************************************************************
** FUNCTION:  t_Bool spi_tclDefaultSettings::bXmlReadNode(xmlNode *poNode)
*************************************************************************/
t_Bool spi_tclDefaultSettings::bXmlReadNode(xmlNodePtr poNode)
{
   ETG_TRACE_USR1(("spi_tclDefaultSettings::bXmlReadNode entered"));

   t_Bool bRetVal = false;
   t_String szNodeName;
   t_S32 s32Value = 0;

   if (NULL != poNode)
   {
      szNodeName = (const t_Char *) (poNode->name);
   } // if (NULL != poNode)

   if ("ML_NOTI_SETTING" == szNodeName)
   {
      t_Bool bMLNotiSetVal = false;
      bRetVal = bGetAttribute("BOOL", poNode, bMLNotiSetVal);
      ETG_TRACE_USR2(("[DESC]: DefSet Value - %d",
                ETG_ENUM(BOOL,bMLNotiSetVal)));
   }
   
   else if ("ML_LINK_ENABLE_SETTING" ==  szNodeName)
   {
      bRetVal = bGetAttribute("VALUE", poNode, s32Value);
      m_enMLLinkEnableSetVal = static_cast<tenEnabledInfo>(s32Value);
      ETG_TRACE_USR2(("[DESC]: DefSet Value - %d",
                m_enMLLinkEnableSetVal));
   }

   else if ("DIPO_ENABLE_SETTING" == szNodeName)
   {
      bRetVal = bGetAttribute("VALUE", poNode, s32Value);
      m_enDipoEnableSetVal = static_cast<tenEnabledInfo>(s32Value);
      ETG_TRACE_USR2(("[DESC]: DefSet Value - %d",
               m_enDipoEnableSetVal));
   }

   else if ("AAP_ENABLE_SETTING" == szNodeName)
   {
      bRetVal = bGetAttribute("VALUE", poNode, s32Value);
      m_enAAPEnableSetVal = static_cast<tenEnabledInfo>(s32Value);
      ETG_TRACE_USR2(("[DESC]: DefSet Value - %d",
               m_enAAPEnableSetVal));
   }
    
   else if ("STEERING_WHEEL_POS" == szNodeName)
   {
      bRetVal = bGetAttribute("VALUE", poNode, s32Value);
      m_enSteeringWheelPos = static_cast<tenDriveSideInfo>(s32Value);
      ETG_TRACE_USR2(("[DESC]: DefSet Value - %d",
             m_enSteeringWheelPos));
   }

   else if ("TECHNOLOGY_PREF" == szNodeName)
   {
      bRetVal = bGetAttribute("VALUE", poNode, s32Value);
      m_enTechPrefVal = static_cast<tenDeviceCategory>(s32Value);
      ETG_TRACE_USR2(("[DESC]: DefSet Value - %d",
            m_enTechPrefVal));
   }    

   else if ("SEL_MODE" == szNodeName)
   {
      bRetVal = bGetAttribute("VALUE", poNode, s32Value);
      m_enSelectMode= static_cast<tenDeviceSelectionMode>(s32Value);
      ETG_TRACE_USR2(("[DESC]: DefSet Value - %d",
            m_enSelectMode));
   }  

   return bRetVal;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
