  /*!
 *******************************************************************************
 * \file         spi_tclDefSetLoader.cpp
 * \brief        Default Setting Loader class that provides interface to delegate 
                 the execution of command and handle response
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Default Setting Loader class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 03.03.2015 |  Chaitra Srinivasa                | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#include "spi_tclDefaultSettings.h"
#include "spi_tclDefSetLoader.h"
#include "Datapool.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DEFSET
      #include "trcGenProj/Header/spi_tclDefSetLoader.cpp.trc.h"
   #endif
#endif


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclDefSetLoader::spi_tclDefSetLoader();
***************************************************************************/
spi_tclDefSetLoader::spi_tclDefSetLoader()
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader() entered "));
} //!end of spi_tclDefSetLoader()

/***************************************************************************
** FUNCTION:  spi_tclDefSetLoader::~spi_tclDefSetLoader();
***************************************************************************/
spi_tclDefSetLoader::~spi_tclDefSetLoader()
{
   ETG_TRACE_USR1(("~spi_tclDefSetLoader() entered "));
} //!end of ~spi_tclDefSetLoader()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDefSetLoader::bInitialize()
***************************************************************************/
t_Bool spi_tclDefSetLoader::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::bInitialize() entered "));
   return true;
} //!end of bInitialize()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDefSetLoader::bUnInitialize()
***************************************************************************/
t_Bool spi_tclDefSetLoader::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::bUnInitialize() entered "));
   return true;
} //!end of bUnInitialize()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vRestoreSettings()
***************************************************************************/
t_Void spi_tclDefSetLoader::vRestoreSettings()
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vRestoreSettings() entered "));
   
   vLoadDefaultSetting();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vLoadSettings()
***************************************************************************/
t_Void spi_tclDefSetLoader::vLoadSettings()
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vLoadSettings() entered "));
   Datapool oDatapool;
   t_U8 u8VirginStart = oDatapool.u8ReadVirginStartSetting();
   if(u8VirginStart != 0)
   {
      vRestoreSettings();
      // Set the value to zero at the virgin start up 
      static const t_U8 u8DefaultVgStrt = 0;
      oDatapool.u8WriteVirginStartSetting(u8DefaultVgStrt);
   }	  
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vSaveSettings()
***************************************************************************/
t_Void spi_tclDefSetLoader::vSaveSettings()
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vSaveSettings() entered "));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vSetMLNotiSettingVal()
***************************************************************************/
t_Void spi_tclDefSetLoader::vSetMLNotiSettingVal(t_Bool bMLNotiSettingVal)
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::bSetMLNotiSettingVal() entered "));
   // By default set to false
   Datapool oDatapool;
   oDatapool.bWriteMLNotificationSetting(bMLNotiSettingVal);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vSetMLLinkEnableSetVal()
***************************************************************************/
t_Void spi_tclDefSetLoader::vSetMLLinkEnableSetVal(tenEnabledInfo enMLLinkEnableSetVal)
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vSetMLLinkEnableSetVal() entered "));
   // By default ML usage is enabled  
   Datapool oDatapool;
   oDatapool.bWriteMLEnableSetting(enMLLinkEnableSetVal);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vSetDipoEnableSetVal()
***************************************************************************/
t_Void spi_tclDefSetLoader::vSetDipoEnableSetVal(tenEnabledInfo enDipoEnableSetVal)
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vSetDipoEnableSetVal() entered "));
   // By default Dipo usage is enabled    
   Datapool oDatapool;
   oDatapool.bWriteDipoEnableSetting(enDipoEnableSetVal);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vSetAAPEnableSetVal()
***************************************************************************/
t_Void spi_tclDefSetLoader::vSetAAPEnableSetVal(tenEnabledInfo enAAPEnableSetVal)
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vSetAAPEnableSetVal() entered "));
   // By default the AAP usage is enabled    
   Datapool oDatapool;
   oDatapool.bWriteAAPEnableSetting(enAAPEnableSetVal);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vSetSteeringWheelPos()
***************************************************************************/
t_Void spi_tclDefSetLoader::vSetSteeringWheelPos(tenDriveSideInfo enSteeringWheelPos)
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vSetSteeringWheelPos() entered "));
   // Default steering wheel position is set as unknown drive side 
   Datapool oDatapool;
   oDatapool.bWriteDriveSideInfo(enSteeringWheelPos);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vSetTechPrefVal()
***************************************************************************/
t_Void spi_tclDefSetLoader::vSetTechPrefVal(tenDeviceCategory enTechPrefVal)
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vSetTechPrefVal() entered "));
   // Default technology preference is set as Android Auto
   Datapool oDatapool;
   oDatapool.bWriteTechnologyPreference(enTechPrefVal);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vSetSelectMode()
***************************************************************************/
t_Void spi_tclDefSetLoader::vSetSelectMode(tenDeviceSelectionMode enSelectMode)
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vSetSelectMode() entered "));
   // Default select mode is set to unknown device  
   Datapool oDatapool;
   oDatapool.bWriteSelectionMode(enSelectMode);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDefSetLoader::vLoadDefaultSetting()
***************************************************************************/
t_Void spi_tclDefSetLoader::vLoadDefaultSetting()
{
   ETG_TRACE_USR1(("spi_tclDefSetLoader::vLoadDefaultSetting() entered "));
   //loading the default values to datapool
   spi_tclDefaultSettings* poDefSet = spi_tclDefaultSettings::getInstance();

   if(poDefSet != NULL)
   {
      t_Bool bMLNotiSettingVal = poDefSet->bGetMLNotiSettingVal();
      vSetMLNotiSettingVal(bMLNotiSettingVal);

      tenEnabledInfo enMLLinkEnableSetVal = poDefSet->enGetMLLinkEnableSetVal();
      vSetMLLinkEnableSetVal(enMLLinkEnableSetVal);

      tenEnabledInfo enDipoEnableSetVal = poDefSet->enGetDipoEnableSetVal();
      vSetDipoEnableSetVal(enDipoEnableSetVal);

      tenEnabledInfo enAAPEnableSetVal = poDefSet->enGetAAPEnableSetVal();
      vSetAAPEnableSetVal(enAAPEnableSetVal);

      tenDriveSideInfo enSteeringWheelPos = poDefSet->enGetSteeringWheelPos();
      vSetSteeringWheelPos(enSteeringWheelPos);   

      tenDeviceCategory enTechPrefVal = poDefSet->enGetTechPrefVal();
      vSetTechPrefVal(enTechPrefVal);  

      tenDeviceSelectionMode enSelectMode = poDefSet->enGetSelectMode();
      vSetSelectMode(enSelectMode); 
   }	  
}
