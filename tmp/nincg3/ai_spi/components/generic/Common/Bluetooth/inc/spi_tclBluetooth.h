  /*!
 *******************************************************************************
 * \file         spi_tclBluetooth.cpp
 * \brief        Bluetooth Manager class that provides interface to delegate
                 the execution of command and handle response
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Bluetooth Manager class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 21.02.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 03.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Included BT DeviceConnection and
                                                  SPI Device Deselection implementation.
 08.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Included call handling.
 06.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Initialisation sequence implementation
 04.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Device switch cancellation handling changes.
 31.07.2014 |  Ramya Murthy (RBEI/ECP2)         | SPI feature configuration via LoadSettings()
 01.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Removed Telephone client handler
 07.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Implementation for BTPairingRequired property
 08.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Impl. of CarPlay DisableBluetooth msg handling
                                                  and SelectedDevice info access protection
 19.05.2015 |  Ramya Murthy (RBEI/ECP2)         | Added vSendBTDeviceName()
 
 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLBLUETOOTH_H_
#define _SPI_TCLBLUETOOTH_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_BluetoothTypedefs.h"
#include "spi_tclBluetoothPolicyBase.h"
#include "spi_tclLifeCycleIntf.h"
#include "spi_tclBluetoothIntf.h"
#include "Lock.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
/*!
 * \brief Index to various BT handlers
 */
enum tenBTHandlerIndex
{
   e8_BT_ML_INDEX = 0,
   e8_BT_DIPO_INDEX = 1,
   e8_BT_AAP_INDEX = 2,
   e8_BT_INDEX_UNKNOWN
};

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_U8 cou8NUM_BT_HANDLERS = 3;


/* Forward Declarations. */
class ahl_tclBaseOneThreadApp;
class spi_tclBluetoothRespIntf;
class spi_tclBluetoothDevBase;

/*!
* \class spi_tclBluetooth
* \brief This is the main Bluetooth Manager class that provides interface to delegate 
*        the execution of command and handle response.
*/
class spi_tclBluetooth : public spi_tclLifeCycleIntf, public spi_tclBluetoothIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetooth::spi_tclBluetooth(ahl_tclBaseOneThreadApp...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclBluetooth()
   * \brief   Default Constructor
   * \param   [IN] poMainApp: Pointer to main application
   * \param   [IN] poResponseIntf: Pointer to BT Response interface
   ***************************************************************************/
   spi_tclBluetooth(ahl_tclBaseOneThreadApp* poMainApp, 
         spi_tclBluetoothRespIntf* poResponseIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetooth::~spi_tclBluetooth();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclBluetooth()
   * \brief   Virtual Destructor
   ***************************************************************************/
   virtual ~spi_tclBluetooth();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclBluetooth::bInitialize()
    ***************************************************************************/
   /*!
    * \fn      bInitialize()
    * \brief   Method to Initialize
    * \sa      bUnInitialize()
    **************************************************************************/
   virtual t_Bool bInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclBluetooth::bUnInitialize()
    ***************************************************************************/
   /*!
    * \fn      bUnInitialize()
    * \brief   Method to UnInitialize
    * \sa      bInitialize()
    **************************************************************************/
   virtual t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclBluetooth::vLoadSettings(const trSpiFeatureSupport&...)
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
    * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclBluetooth::vSaveSettings()
    ***************************************************************************/
   /*!
    * \fn      vSaveSettings()
    * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
    * \sa      vLoadSettings()
    **************************************************************************/
   virtual t_Void vSaveSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when SelectDevice/DeselectDevice operation is received by SPI.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enDeviceConnReq: Connection request type for the device
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle,
         tenDeviceConnectionReq enDeviceConnReq);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq,
   *             tenResponseCode enRespCode, tenErrorCode enErrorCode)
   * \brief   Called when SelectDevice operation is complete & with the result
   * 			 of the operation.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enDeviceConnReq: Connection request type for the device
   * \param   [IN] enRespCode: Response code enumeration
   * \param   [IN] enErrorCode: Error code enumeration
   * \param   [IN] bIsDeviceSwitch: True - if a projection device switch is in progress
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenDeviceConnectionReq enDeviceConnReq,
         tenResponseCode enRespCode,
         tenErrorCode enErrorCode,
         t_Bool bIsDeviceSwitch);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetooth::bValidateBTPairingRequired()
   ***************************************************************************/
   /*!
   * \fn      bValidateBTPairingRequired()
   * \brief   Validates if BT pairing is required for currently selected device.
   *          Pairing would be required only if currently selected device is
   *          a ML Device.
   * \retval  Bool: TRUE - if BT pairing is required, else FALSE.
   **************************************************************************/
   virtual t_Bool bValidateBTPairingRequired();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vOnInvokeBTDeviceAction(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnInvokeBTDeviceAction(t_U32 u32BluetoothDevHandle,
   *             t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange)
   * \brief   Called when user selects a Bluetooth device action during switching
   *             device operation between Projection & BT device.
   * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
   * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
   * \param  [IN] enBTChange  : Enum value which stores user's selected device change.
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnInvokeBTDeviceAction(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
   		 tenBTChangeInfo enBTChange);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vOnDisableBluetooth(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnDisableBluetooth(t_String szDiPODevBTAddress)
   * \brief   Called when user selects a Bluetooth device action during switching
   *             device operation between Projection & BT device.
   * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
   * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
   * \param  [IN] enBTChange  : Enum value which stores user's selected device change.
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnDisableBluetooth(t_String szDiPODevBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vGetVehicleBTAddress()
   ***************************************************************************/
   /*!
   * \fn     vGetVehicleBTAddress()
   * \brief  Function to get the vehicle BT address.
   * \param  szVehicleBTAddress:[OUT] Vehicle BT address
   * \retval  None
   **************************************************************************/
   virtual t_Void vGetVehicleBTAddress(t_String &szVehicleBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vOnCallStatus(t_Bool bCallActive)
   ***************************************************************************/
   /*!
   * \fn      vOnCallStatus(t_Bool bCallActive)
   * \brief   Called when Telephone call status changes.
   * \param   [IN] bCallActive: true - if a call is active, else false.
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnCallStatus(t_Bool bCallActive);

   /*********Start of functions overridden from spi_tclBluetoothIntf*********/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSendSelectDeviceResult(t_Bool...)
   ***************************************************************************/
   /*!
   * \fn      vSendSelectDeviceResult(t_Bool bSuccess)
   * \brief   Interface to send result of Bluetooth connection processing during
   *          SelectDevice operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] bSelectionSuccess:
   *               TRUE - If BT processing is successful.
   *               FALSE - If BT processing is unsuccessful.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendSelectDeviceResult(t_Bool bSelectionSuccess);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSendDeselectDeviceRequest()
   ***************************************************************************/
   /*!
   * \fn      vSendDeselectDeviceRequest()
   * \brief   Interface to send a Device Deselection request.
   *          Mandatory interface to be implemented.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendDeselectDeviceRequest();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSendDeviceSwitchEvent(t_U32 ...)
   ***************************************************************************/
   /*!
   * \fn      vSendDeviceSwitchEvent(t_U32 u32BluetoothDevHandle,
   *             t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange,
   *             t_Bool bCallActive)
   * \brief   Interface to notify clients about a device switch event being performed
   *          between a BT and a Projection device, which has been triggered as a result
   *          of user action or due to device selection strategy applied by
   *          Bluetooth/SPI service.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
   * \param   [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
   * \param   [IN] enBTChange  : Indicates type of BT device change
    * \param  [IN] bSameDevice : Inidcates whether BT & Projection device are same
    *              or different devices.
   * \param   [IN] bCallActive : Provides call activity status.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendDeviceSwitchEvent(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enBTChange,
         t_Bool bSameDevice,
         t_Bool bCallActive);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSendBTPairingRequired(t_String szBTAddress)
   ***************************************************************************/
   /*!
   * \fn      vNotifyBTPairingRequired(t_String szBTAddress)
   * \brief   Notifies users when BT pairing is required for a projection device.
   *          Mandatory interface to be implemented.
   * \param   [IN] szBTAddress : BT address of device to be paired.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendBTPairingRequired(t_String szBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSendBTDeviceName(...)
   ***************************************************************************/
   /*!
   * \fn      vSendBTDeviceName(t_U32 u32ProjectionDevHandle, t_String szBTDeviceName)
   * \brief   Notifies users when BT Device name is available for active projection device
   * \param   [IN] u32ProjectionDevHandle: Unique handle of projection device
   * \param   [IN] szBTDeviceName: BT Device name of projection device
   * \retval  None
   **************************************************************************/
   t_Void vSendBTDeviceName(t_U32 u32ProjectionDevHandle,
            t_String szBTDeviceName);

   /***************************************************************************
   ** FUNCTION:  t_U32 spi_tclBluetooth::u32GetSelectedDevHandle()
   ***************************************************************************/
   /*!
   * \fn      u32GetSelectedDevHandle()
   * \brief   Interface to retrieve device handle of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   None
   * \retval  DeviceHandle of selected projection device
   **************************************************************************/
   virtual t_U32 u32GetSelectedDevHandle();

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclBluetooth::szGetSelectedDevBTAddress()
   ***************************************************************************/
   /*!
   * \fn      szGetSelectedDevBTAddress()
   * \brief   Interface to retrieve BT Address of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   None
   * \retval  BT Address of selected projection device
   **************************************************************************/
   virtual t_String szGetSelectedDevBTAddress();

   /***************************************************************************
   ** FUNCTION:  tenDeviceStatus spi_tclBluetooth::enGetSelectedDevStatus()
   ***************************************************************************/
   /*!
   * \fn      enGetSelectedDevStatus()
   * \brief   Interface to retrieve current device status of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   None
   * \retval  Device status of selected projection device
   **************************************************************************/
   virtual tenDeviceStatus enGetSelectedDevStatus();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSetSelectedDevStatus()
   ***************************************************************************/
   /*!
   * \fn      vSetSelectedDevStatus()
   * \brief   Interface to set the status of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   [IN] enDevStatus : Device status enumeration
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSelectedDevStatus(tenDeviceStatus enDevStatus);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSetSelectedDevBTAddress()
   ***************************************************************************/
   /*!
   * \fn      vSetSelectedDevBTAddress()
   * \brief   Interface to set the BT Address of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   [IN] szBTAddress : BT Address string
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSelectedDevBTAddress(t_String szBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetooth::bGetCallStatus()
   ***************************************************************************/
   /*!
   * \fn      bGetCallStatus()
   * \brief   Interface to retrieve the current call activity.
   *          Mandatory interface to be implemented.
   * \param   [IN] enDevStatus : Device status enumeration
   * \retval  None
   **************************************************************************/
   virtual t_Bool bGetCallStatus();

   /*********End of functions overridden from spi_tclBluetoothIntf***********/
   
   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
    ** FUNCTION: spi_tclBluetooth(const spi_tclBluetooth &rfcoBluetooth)
    ***************************************************************************/
   /*!
    * \fn      spi_tclBluetooth(const spi_tclBluetooth &rfcoBluetooth)
    * \brief   Copy constructor not implemented hence made private
    **************************************************************************/
   spi_tclBluetooth(const spi_tclBluetooth &rfcoBluetooth);

   /***************************************************************************
    ** FUNCTION: const spi_tclBluetooth & operator=(
    **                                 const spi_tclBluetooth &rfcoBluetooth);
    ***************************************************************************/
   /*!
    * \fn      const spi_tclBluetooth & operator=(const spi_tclBluetooth &rfcoBluetooth);
    * \brief   assignment operator not implemented hence made private
    **************************************************************************/
   const spi_tclBluetooth & operator=(
            const spi_tclBluetooth &rfcoBluetooth);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSaveSettings()
   ***************************************************************************/
   /*!
   * \fn      enGetSelDevBTHandlerIndex()
   * \brief   Provides the index of BT handler for selected projection device.
   * \param   None
   * \retval  Index of BT handler
   **************************************************************************/
   tenBTHandlerIndex enGetSelDevBTHandlerIndex();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vFetchSelectedDeviceDetails(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vFetchSelectedDeviceDetails(t_U32 u32SelectedDevHandle)
   * \brief   Method to fetch details about Selected Device.
   * \param   [IN] u32SelectedDevHandle: Unique device handle of selected device
   * \retval  None
   **************************************************************************/
   t_Void vFetchSelectedDeviceDetails(t_U32 u32SelectedDevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vClearSelectedDevInfo()
   ***************************************************************************/
   /*!
   * \fn      vClearSelectedDevInfo()
   * \brief   Method to clear information stored about Selected Device.
   * \param   None
   * \retval  None
   **************************************************************************/
   t_Void vClearSelectedDevInfo();

   /***************************************************************************
   ** FUNCTION:  tenBTChangeInfo spi_tclBluetooth::enGetSelectedDevBTChangeInfo()
   ***************************************************************************/
   /*!
   * \fn      enGetSelectedDevBTChangeInfo()
   * \brief   Provides the current BT device change information (if any device switch
   *          is in progress).
   * \param   None
   * \retval  BT device change info
   **************************************************************************/
   tenBTChangeInfo enGetSelectedDevBTChangeInfo();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetooth::vSetSelectedDevBTChangeInfo()
   ***************************************************************************/
   /*!
   * \fn      enGetSelectedDevBTChangeInfo()
   * \brief   Sets the current BT device change information for a device switch
   * \param   [IN] enBTChangeInfo : BT device change info enum
   * \retval  None
   **************************************************************************/
   t_Void vSetSelectedDevBTChangeInfo(tenBTChangeInfo enBTChangeInfo);

   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   /***************************************************************************
   ** Main application pointer
   ***************************************************************************/
   ahl_tclBaseOneThreadApp*            m_poMainApp;

   /***************************************************************************
   ** BT PolicyBase pointer
   ***************************************************************************/
   spi_tclBluetoothPolicyBase*         m_poBTPolicyBase;

   /***************************************************************************
   ** BT Response Interface pointer
   ***************************************************************************/
   spi_tclBluetoothRespIntf*           m_poBTRespIntf;

   /***************************************************************************
   ** List of pointers to Bluetooth Handlers of different device types
   ***************************************************************************/
   spi_tclBluetoothDevBase*            m_poBTHandlersLst[cou8NUM_BT_HANDLERS];

   /***************************************************************************
   ** Structure containing info about Selected Device in SPI
   ***************************************************************************/
   trSpiDeviceDetails                  m_rSelDeviceDetails;

   /***************************************************************************
   ** Lock object for Selected device details structure
   ***************************************************************************/
   Lock      m_SelDevDetailsLock;

   /***************************************************************************
   ** Flag to store current call status
   ***************************************************************************/
   t_Bool    m_bIsCallActive;

   /***************************************************************************
   ** Lock object for call status
   ***************************************************************************/
   Lock      m_oCallStatusLock;
};

#endif // _SPI_TCLBLUETOOTH_H_

