/*!
 *******************************************************************************
 * \file             spi_tclBluetoothIntf.h
 * \brief            Abstract class that specifies the interface which must be
 *                   implemented by Bluetooth Manager class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class BT  Manager
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 19.05.2015 |  Ramya Murthy (RBEI/ECP2)         | Added vSendBTDeviceName()

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef _SPI_TCLBLUETOOTHINTERFACE_H_
#define _SPI_TCLBLUETOOTHINTERFACE_H_

#include "BaseTypes.h"
#include "spi_BluetoothTypedefs.h"

/**
 * Abstract class that specifies the interface which must be
 * implemented by Bluetooth Manager class
 */

class spi_tclBluetoothIntf
{
public:

	/***************************************************************************
	*********************************PUBLIC*************************************
	***************************************************************************/

	/***************************************************************************
    ** FUNCTION:  spi_tclBluetoothIntf::spi_tclBluetoothIntf();
    ***************************************************************************/
   /*!
    * \fn      spi_tclBluetoothIntf()
    * \brief   Default Constructor
    ***************************************************************************/
   spi_tclBluetoothIntf()
   {
   }

   /***************************************************************************
    ** FUNCTION:  spi_tclBluetoothIntf::~spi_tclBluetoothIntf();
    ***************************************************************************/
   /*!
    * \fn      ~spi_tclBluetoothIntf()
    * \brief   Virtual Destructor
    ***************************************************************************/
   virtual ~spi_tclBluetoothIntf()
   {
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothIntf::vSendSelectDeviceResult(t_Bool...)
   ***************************************************************************/
   /*!
   * \fn      vSendSelectDeviceResult(t_Bool bSuccess)
   * \brief   Interface to send result of Bluetooth connection processing during
   *          SelectDevice operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] bSelectionSuccess:
   *               TRUE - If BT processing is successful.
   *               FALSE - If BT processing is unsuccessful.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendSelectDeviceResult(t_Bool bSelectionSuccess) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothIntf::vSendDeselectDeviceRequest()
   ***************************************************************************/
   /*!
   * \fn      vSendDeselectDeviceRequest()
   * \brief   Interface to send a Device Deselection request.
   *          Mandatory interface to be implemented.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendDeselectDeviceRequest() = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothIntf::vSendDeviceSwitchEvent(t_U32 ...)
   ***************************************************************************/
   /*!
   * \fn      vSendDeviceSwitchEvent(t_U32 u32BluetoothDevHandle,
   *             t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange,
   *             t_Bool bCallActive)
   * \brief   Interface to notify clients about a device switch event being performed
   *          between a BT and a Projection device, which has been triggered as a result
   *          of user action or due to device selection strategy applied by
   *          Bluetooth/SPI service.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
   * \param   [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
   * \param   [IN] enBTChange  : Indicates type of BT device change
    * \param  [IN] bSameDevice : Inidcates whether BT & Projection device are same
    *              or different devices.
   * \param   [IN] bCallActive : Provides call activity status.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendDeviceSwitchEvent(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enBTChange,
         t_Bool bSameDevice,
         t_Bool bCallActive) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothIntf::vSendBTPairingRequired(t_String szBTAddress)
   ***************************************************************************/
   /*!
   * \fn      vNotifyBTPairingRequired(t_String szBTAddress)
   * \brief   Notifies users when BT pairing is required for a projection device.
   *          Mandatory interface to be implemented.
   * \param   [IN] szBTAddress : BT address of device to be paired.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendBTPairingRequired(t_String szBTAddress) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothIntf::vSendBTDeviceName(...)
   ***************************************************************************/
   /*!
   * \fn      vSendBTDeviceName(t_U32 u32ProjectionDevHandle, t_String szBTDeviceName)
   * \brief   Notifies users when BT Device name is available for active projection device
   * \param   [IN] u32ProjectionDevHandle: Unique handle of projection device
   * \param   [IN] szBTDeviceName: BT Device name of projection device
   * \retval  None
   **************************************************************************/
   virtual t_Void vSendBTDeviceName(t_U32 u32ProjectionDevHandle,
            t_String szBTDeviceName) = 0;

   /***************************************************************************
   ** FUNCTION:  t_U32 spi_tclBluetoothIntf::u32GetSelectedDevHandle()
   ***************************************************************************/
   /*!
   * \fn      u32GetSelectedDevHandle()
   * \brief   Interface to retrieve device handle of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   None
   * \retval  DeviceHandle of selected projection device
   **************************************************************************/
   virtual t_U32 u32GetSelectedDevHandle() = 0;

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclBluetoothIntf::szGetSelectedDevBTAddress()
   ***************************************************************************/
   /*!
   * \fn      szGetSelectedDevBTAddress()
   * \brief   Interface to retrieve BT Address of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   None
   * \retval  BT Address of selected projection device
   **************************************************************************/
   virtual t_String szGetSelectedDevBTAddress() = 0;

   /***************************************************************************
   ** FUNCTION:  tenDeviceStatus spi_tclBluetoothIntf::enGetSelectedDevStatus()
   ***************************************************************************/
   /*!
   * \fn      enGetSelectedDevStatus()
   * \brief   Interface to retrieve current device status of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   None
   * \retval  Device status of selected projection device
   **************************************************************************/
   virtual tenDeviceStatus enGetSelectedDevStatus() = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothIntf::vSetSelectedDevStatus()
   ***************************************************************************/
   /*!
   * \fn      vSetSelectedDevStatus()
   * \brief   Interface to set the status of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   [IN] enDevStatus : Device status enumeration
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSelectedDevStatus(tenDeviceStatus enDevStatus) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothIntf::vSetSelectedDevBTAddress()
   ***************************************************************************/
   /*!
   * \fn      vSetSelectedDevBTAddress()
   * \brief   Interface to set the BT Address of selected projection device.
   *          Mandatory interface to be implemented.
   * \param   [IN] szBTAddress : BT Address string
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSelectedDevBTAddress(t_String szBTAddress) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothIntf::bGetCallStatus()
   ***************************************************************************/
   /*!
   * \fn      bGetCallStatus()
   * \brief   Interface to retrieve the current call activity.
   *          Mandatory interface to be implemented.
   * \param   [IN] enDevStatus : Device status enumeration
   * \retval  None
   **************************************************************************/
   virtual t_Bool bGetCallStatus() = 0;

private:

	/***************************************************************************
	*********************************PRIVATE************************************
	***************************************************************************/

};
#endif // _SPI_TCLBLUETOOTHINTERFACE_H_

