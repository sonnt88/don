/*!
 *******************************************************************************
 * \file             spi_tclBluetoothDevBase.h
 * \brief            Abstract class that specifies the Bluetooth interface which
 *                   must be implemented by device class (Mirror Link/Digital iPod out)
 *                   for handling Bluetooth connections
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class for ML and DiPO Interfaces
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 10.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 19.05.2015 |  Ramya Murthy (RBEI/ECP2)         | Added vOnBTDeviceNameUpdate()

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef _SPI_TCLBLUETOOTHDEVBASE_H_
#define _SPI_TCLBLUETOOTHDEVBASE_H_

#include "BaseTypes.h"
#include "spi_BluetoothTypedefs.h"

/*
 * Abstract class that specifies the Bluetooth interface which
 * must be implemented by device class (Mirror Link/Digital iPod out)
 * for handling Bluetooth connections
 */

class spi_tclBluetoothDevBase
{
public:

	/***************************************************************************
	*********************************PUBLIC*************************************
	***************************************************************************/

	/***************************************************************************
    ** FUNCTION:  spi_tclBluetoothDevBase::spi_tclBluetoothDevBase();
    ***************************************************************************/
   /*!
    * \fn      spi_tclBluetoothDevBase()
    * \brief   Default Constructor
    ***************************************************************************/
   spi_tclBluetoothDevBase()
   {
   }

   /***************************************************************************
    ** FUNCTION:  spi_tclBluetoothDevBase::~spi_tclBluetoothDevBase();
    ***************************************************************************/
   /*!
    * \fn      ~spi_tclBluetoothDevBase()
    * \brief   Virtual Destructor
    ***************************************************************************/
   virtual ~spi_tclBluetoothDevBase()
   {
   }

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclBluetoothDevBase::bInitialise();
    ***************************************************************************/
   /*!
    * \fn      bInitialise()
    * \brief   Method to initialises the service handler. (Performs initialisations which
    *          are not device specific.)
    *          Optional interface to be implemented.
    * \retval  t_Bool: TRUE - If ServiceHandler is initialised successfully, else FALSE.
    * \sa      bUninitialise()
    ***************************************************************************/
   virtual t_Bool bInitialise()
   {
      return true;
   }

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclBluetoothDevBase::bUninitialise();
    ***************************************************************************/
   /*!
    * \fn      bUninitialise()
    * \brief   Method to uninitialise the service handler.
    *          Optional interface to be implemented.
    * \retval  t_Bool: TRUE - If ServiceHandler is uninitialised successfully, else FALSE.
    * \sa      bInitialise()
    ***************************************************************************/
   virtual t_Bool bUninitialise()
   {
      return true;
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when SelectDevice request is received by SPI.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenResponseCode enRespCode)
   * \brief   Called when SelectDevice operation is complete, with the result
   *          of the operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enRespCode: Response code enumeration
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenResponseCode enRespCode) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when DeselectDevice request is received by SPI.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnSPIDeselectDeviceRequest(t_U32 u32ProjectionDevHandle) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenResponseCode enRespCode)
   * \brief   Called when DeselectDevice operation is complete, with the result
   *          of the operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enRespCode: Response code enumeration
   * \param   [IN] bIsDeviceSwitch: True - if a projection device switch is in progress
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnSPIDeselectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenResponseCode enRespCode,
         t_Bool bIsDeviceSwitch) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothDevBase::bCheckBTPairingRequired()
   ***************************************************************************/
   /*!
   * \fn      bCheckBTPairingRequired()
   * \brief   Validates if BT pairing is required for current selected
   *          projection device.
   *          Optional interface - to be implemented if BT Handler requires HMI
   *          to initiate BT pairing
   * \retval  Bool: TRUE - if BT pairing is required, else FALSE.
   **************************************************************************/
   virtual t_Bool bCheckBTPairingRequired()
   {
      return false;
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTDeviceSwitchAction(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
   *             t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange)
   * \brief   Called with user action when there is a device switch occurring
   *          between a Projection and a BT device.
   *          Mandatory interface to be implemented.
   * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
   * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
   * \param  [IN] enBTChange  : Indicates user's device change action
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enInitialBTChange,
         tenBTChangeInfo enFinalBTChange) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnDisableBluetooth(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnDisableBluetooth(t_String szProjectionDevBTAddr)
   * \brief   Called when Bluetooth connection should be disabled during
   *          projection session.
   *          Optional interface to be implemented.
   * \param  [IN] szProjectionDevBTAddr : BT address of active projection device
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnDisableBluetooth(t_String szProjectionDevBTAddr)
   {
      SPI_INTENTIONALLY_UNUSED(szProjectionDevBTAddr);
   }

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTConnectionResult(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTConnectionResult(t_String szBTDeviceAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Interface to receive result of a BT device connection/disconnection request.
   * \param   [IN] szBTDeviceAddress: Bluetooth address of device
   *          Mandatory interface to be implemented.
   * \param   [IN] enBTConnResult: Connection/Disconnection result.
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTConnectionResult(t_String szBTDeviceAddress,
         tenBTConnectionResult enBTConnResult) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTConnectionChanged(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTConnectionChanged(t_String szBTDeviceAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Interface to receive notification on a BT device connection/disconnection.
   *          Mandatory interface to be implemented.
   * \param   [IN] szBTDeviceAddress: Bluetooth address of device
   * \param   [IN] enBTConnResult: Connection/Disconnection result.
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTConnectionChanged(t_String szBTDeviceAddress,
         tenBTConnectionResult enBTConnResult) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTPairingResult(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTPairingResult(tenBTSetPairingMethod enPairingMethod,
   *              t_String szPairingPin)
   * \brief   Interface to receive pairing result.
   *          Optional interface to be implemented.
   * \param   [IN] enPairingMethod: Pairing method selected in pairing process
   * \param   [IN] szPairingPin: Pairing PIN number to be used for SSP
   *              Numeric Comparison method
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTPairingResult(tenBTSetPairingMethod enPairingMethod,
         t_String szPairingPin) {}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTPairingStatus(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTPairingStatus(tenBTSetPairingStatus enPairingStatus)
   * \brief   Interface to receive notification on pairing status change.
   *          Optional interface to be implemented.
   * \param   [IN] enPairingStatus: Pairing status
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTPairingStatus(tenBTSetPairingStatus enPairingStatus) {}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTDeviceNameUpdate(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTDeviceNameUpdate(t_U32 u32BTDeviceHandle,
   *              const t_String& rfszBTDeviceName)
   * \brief   Interface to receive notification on device name of BT paired devices.
   *          Optional interface to be implemented.
   * \param   [IN] szBTDeviceAddress: BT Address of paired device
   * \param   [IN] szBTDeviceName: BT Device name of paired device
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTDeviceNameUpdate(t_String szBTDeviceAddress,
            t_String szBTDeviceName) {}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTPairableMode(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTPairableMode()
   * \brief   Interface to receive notification on state of HU BT stack.
   *          Optional interface to be implemented.
   * \param   [IN] rBTPairiableModeInfo: Info on pairiable and connectable state
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTPairableMode(trBTPairiableModeInfo rBTPairiableModeInfo) {}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTPairingInfo(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTPairingInfo()
   * \brief   Interface to receive notification when there is a pairing request to HU.
   *          Optional interface to be implemented.
   * \param   [IN] rBTPairingReqInfo: Info about pairing request
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTPairingInfo(trBTPairingRequestInfo rBTPairingReqInfo) {}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothDevBase::vOnBTOnOffChanged(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTOnOffChanged()
   * \brief   Interface to receive notification when there is a  to HU.
   *          Optional interface to be implemented.
   * \param   [IN] bBluetoothOn: True if BT is switched On, else False
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnBTOnOffChanged(t_Bool bBluetoothOn) {}

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/


private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ******************************END OF PRIVATE********************************
   ***************************************************************************/

};
#endif // _SPI_TCLBLUETOOTHDEVBASE_H_

