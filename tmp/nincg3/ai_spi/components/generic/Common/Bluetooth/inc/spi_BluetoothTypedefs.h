/*!
 *******************************************************************************
 * \file             spi_BluetoothTypedefs.h
 * \brief            Type defines used by Bluetooth classes
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Type defines used by Bluetooth classes
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 25.02.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef _SPI_BLUETOOTHTYPEDEFS_H_
#define _SPI_BLUETOOTHTYPEDEFS_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <string>
#include <functional> 

#include "SPITypes.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/
//! Macro to check if a BT string address is valid.
//! Address is valid if the string is not empty.
#define IS_VALID_BT_ADDRESS(BT_ADDRESS)   (false == (BT_ADDRESS).empty())

//! Macro to check if a Projection device handle is valid
#define IS_VALID_DEVHANDLE(DEVHANDLE)  (0 != DEVHANDLE)

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_U32 cou32InvalidDeviceHandle = 0;

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

//! Identifies the requested action to Bluetooth Source
enum tenBTSetConnectionRequest
{
   //! Select (connect) action
   e8BT_REQUEST_CONNECT = 0x0,
   //! Deselect (disconnect) action
   e8BT_REQUEST_DISCONNECT = 0x1,
   //@Note: commented Delete actions since they are unused
   /*//! Delete action
   e8BT_REQUEST_DELETE = 0x2,
   //! Delete all devices action
   e8BT_REQUEST_DELETE_ALL = 0x3,
   //! Block action*/
   e8BT_REQUEST_BLOCK = 0x4,
   //! Unblock action
   e8BT_REQUEST_UNBLOCK = 0x5,
   //! Unblock with auto-connect action
   e8BT_REQUEST_UNBLOCK_AUTO_CONNECT = 0x6,
   //! Block all devices except one device action
   e8BT_REQUEST_BLOCK_ALL_EXCEPT = 0x7,
   //@Note: Please do not change above values!
   //Values are assigned as per BluetoothSettings FI values.
   e8BT_REQUEST_UNKNOWN = 0xFF
};

//! Identifies the connection result from Bluetooth Source.
enum tenBTConnectionResult
{
   //! Default value
   e8BT_RESULT_UNKNOWN = 0,
   //! Device connection success
   e8BT_RESULT_CONNECTED = 1,
   //! Device connection failure
   e8BT_RESULT_CONN_FAILED = 2,
   //! Device disconnection success
   e8BT_RESULT_DISCONNECTED = 3,
   //! Device disconnection failure
   e8BT_RESULT_DISCONN_FAILED = 4,
   //! Device block success
   e8BT_RESULT_BLOCKED = 5,
   //! Device block failure
   e8BT_RESULT_BLOCK_FAILED = 6,
   //! Device block success
   e8BT_RESULT_UNBLOCKED = 7,
   //! Device block failure
   e8BT_RESULT_UNBLOCK_FAILED = 8
};

//! Identifies the usage of different BT profiles
enum tenBTProfile
{
   //! To enable A2DP profile
   e8A2DP_ENABLED = 0,
   //! To disable A2DP profile
   e8A2DP_DISABLED = 1
};

//! Identifies Blocking type of BT device
enum tenBTDeviceBlockType
{
   //! To block a single BT device
   e8BLOCK_SINGLE_DEVICE = 0,
   //! To block all BT devices
   e8BLOCK_ALL_DEVICES = 1,
   //! To block all BT devices except one device
   e8BLOCK_ALL_EXCEPT = 2
};

//! Identifies Unblocking type of BT device
enum tenBTDeviceUnblockType
{
   //! To unblock a single BT device
   e8UNBLOCK_SINGLE_DEVICE = 0,
   //! To unblock all BT devices (without auto-reconnection)
   e8UNBLOCK_ALL_DEVICES = 1,
   //! To unblock all BT devices and start auto-reconnection strategy
   e8UNBLOCK_ALL_DEVICES_AUTO_CONNECT = 2
};

//! Indicates status of device change
enum tenDeviceStatus
{
   //! Indicates all device changes are complete (default value)
   e8DEVICE_CHANGE_COMPLETE = 0,
   //! Indicates a DeviceSwitch event b/w Projection & BT device is in progress,
   //! and SPI is waiting for user action
   e8DEVICE_SWITCH_IN_PROGRESS = 1,
   //! Indicates a device changes is being processed
   //! (as a result of user or Projection device triggered action)
   e8DEVICE_CHANGE_IN_PROGRESS = 2,
   //! Indicates a Projection device selection is in progress
   e8DEVICE_SELECTION_IN_PROGRESS = 3,
   //! Indicates a Projection device deselection is in progress
   e8DEVICE_DESELECTION_IN_PROGRESS = 4
};

enum tenDevicePairingStatus
{
   //! Indicates BT pairing is initiated & in progress
   e8DEVICE_PAIRING_NOT_STARTED = 0,
   e8DEVICE_PAIRING_REQUESTED = 1,
   e8DEVICE_PAIRING_IN_PROGRESS = 2,
   e8DEVICE_PAIRING_COMPLETE = 3

};

//! Identifies BT Pairing status types
enum tenBTSetPairingStatus
{
   e8PAIRING_SUCCESSFUL = 0x00,
   e8PAIRING_UNSUCCESSFUL = 0x01,
   e8PAIRING_TIMEOUT = 0x02,
   e8PAIRING_CONNECTIONLOST = 0x03,
   e8PAIRING_AUTH_FAILURE = 0x04,
   e8PAIRING_CANCELLED = 0x05,
   e8PAIRING_RETRY_KEEPSESSION = 0x06,
   e8PAIRING_RETRY_NEWSESSION = 0x07,
   e8PAIRING_READY_TO_PAIR = 0x08, //TODO - not yet in FI!
   e8PAIRING_IDLE = 0xFF,
   //@Note: Please do not change above values!
   //Values are assigned as per BluetoothSettings FI values.
};

enum tenBTSetPairingMethod
{
   e8PAIRING_TYPE_LEGACY = 0x00,
   e8PAIRING_TYPE_SSP_NUMERIC_COMPARISON = 0x01,
   e8PAIRING_TYPE_SSP_JUST_WORKS = 0x02,
   e8PAIRING_TYPE_CHANGE_PIN = 0x03
   //@Note: Please do not change above values!
   //Values are assigned as per BluetoothSettings FI values.
};

enum tenBTPairingAction
{
   e8PAIRING_ACCEPT = 0x00,
   e8PAIRING_REJECT = 0x01,
   e8PAIRING_CANCEL = 0x02,
   e8PAIRING_RETRY = 0x03
   //@Note: Please do not change above values!
   //Values are assigned as per BluetoothSettings FI PairingResponseType values.
};

//! Contains info about pairable & connectable states of HU
struct trBTPairiableModeInfo
{
   t_Bool bHUInPairableState;
   t_Bool bHUInConnectableState;
   t_Bool bHUInPairingOngoingState;
   t_String szBTAddress;

   trBTPairiableModeInfo() :
      bHUInPairableState(false), bHUInConnectableState(false),
      bHUInPairingOngoingState(false)
   {}
};

//! Contains info about pairable & connectable states of HU
struct trBTPairingRequestInfo
{
   t_String szRemoteDevBTAddr;
   //t_String szRemoteDevName;
   t_String szPairingPin;
   tenBTSetPairingMethod enPairingMethod;

   trBTPairingRequestInfo(): enPairingMethod(e8PAIRING_TYPE_LEGACY)
   {}
};

//! Callback signatures definition
typedef std::function<t_Void(std::string, tenBTConnectionResult)> vOnBTConnectionStatus;
typedef std::function<t_Void(tenBTSetPairingMethod, std::string)> vOnBTPairingResult;
typedef std::function<t_Void(tenBTSetPairingStatus)> vOnBTPairingStatus;
typedef std::function<t_Void(trBTPairiableModeInfo)> vOnBTPairableMode;
typedef std::function<t_Void(trBTPairingRequestInfo)> vOnBTPairingInfo;
typedef std::function<t_Void(t_Bool)> vOnBTOnOffChanged;

typedef std::function<t_Void(t_String, t_String)> vOnBTDeviceNameUpdate;

//! \brief   Structure holding the callbacks to BT Manager. Used by BT Client to notify
//!          BT Manager about Bluetooth Source events.
struct trBluetoothCallbacks
{
   //! Called when BT device result of a connection/disconnection request is received by BT Client
   vOnBTConnectionStatus fvOnBTConnectionResult;

   //! Called when BT device connected/disconnection notification is received by BT Client
   vOnBTConnectionStatus fvOnBTConnectionChanged;

   //! Called when BT On/Off status changes
   vOnBTOnOffChanged  fvOnBTOnOffChanged;

   trBluetoothCallbacks() :
      fvOnBTConnectionResult(NULL), fvOnBTConnectionChanged(NULL), fvOnBTOnOffChanged(NULL)
   {
   }
};

//! \brief   Structure holding the callbacks to BT Manager. Used by BT Client to notify
//!          BT Manager about Bluetooth Source events.
struct trBluetoothPairingCallbacks
{
   //! Called with info about HU BT stack state
   vOnBTPairableMode    fvOnBTPairableMode;

   //! Called when there is a pairing request
   vOnBTPairingInfo     fvOnBTPairingInfo;

   trBluetoothPairingCallbacks() :
      fvOnBTPairableMode(NULL), fvOnBTPairingInfo(NULL)
   {
   }
};

//! \brief   Structure holding info for BT Device name callback
struct trBTDeviceNameCbInfo
{
   //! BT Address of interested device 
   t_String szBTAddress;

   //! Callback function to get interested device's name
   vOnBTDeviceNameUpdate   fvOnBTDeviceNameUpdate;

   trBTDeviceNameCbInfo(): fvOnBTDeviceNameUpdate(NULL)
   {
   }
};

//! \brief   Structure holding a BT device information
struct trBluetoothDeviceInfo
{
   //Provides Bluetooth Address of device
   std::string szBTAddress;
   //Provides Connection status of device
   t_Bool bDeviceConnected;
   //Provides Device name
   std::string szDeviceName;

   trBluetoothDeviceInfo() :
      szBTAddress(), bDeviceConnected(false), szDeviceName()
   {
   }
};

//! \brief   Structure holding the details of a BT ChangeDeviceState MethodStart Request
struct trBTChangeDeviceRequestInfo
{
   //Provides Bluetooth Address of device
   std::string szDevBTAddress;
   //Provides BT Request type
   tenBTSetConnectionRequest enRequestType;

   trBTChangeDeviceRequestInfo(std::string szBTAddress = "",
         tenBTSetConnectionRequest enConnRequestType = e8BT_REQUEST_UNKNOWN) :
      szDevBTAddress(szBTAddress), enRequestType(enConnRequestType)
   {
   }
};

//! Contains info about a Projection device
struct trSpiDeviceDetails
{
   t_U32 u32DeviceHandle;
   t_String szBTAddress;
   tenDeviceCategory enDeviceCategory;
   tenDeviceStatus enDeviceStatus;
   tenBTChangeInfo enBTChangeInfo;

   trSpiDeviceDetails() :
      u32DeviceHandle(0), enDeviceCategory(e8DEV_TYPE_UNKNOWN),
      enDeviceStatus(e8DEVICE_CHANGE_COMPLETE),
      enBTChangeInfo(e8NO_CHANGE)
   {}
};

//! Indicates type of BT local mode
enum tenBTLocalModeType
{
   e8MODE_ON = 1,
   e8MODE_OFF = 2,
   e8MODE_AUTO = 3
};

#endif //_SPI_BLUETOOTHTYPEDEFS_H_
