/***********************************************************************/
/*!
* \file  spi_tclBluetoothSettings.h
* \brief Class to get the Bluetooth Manager Settings
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Bluetooth Manager Settings
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
08.04.2014  | Ramya Murthy          | Initial Version
24.11.2014  | Ramya Murthy          | Included BT disconnection strategy

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLBLUETOOTHSETTINGS_H_
#define _SPI_TCLBLUETOOTHSETTINGS_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "GenericSingleton.h"
#include "Xmlable.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclBluetoothSettings
* \brief Class to get the Bluetooth and Telephone services Info
****************************************************************************/
class spi_tclBluetoothSettings: public GenericSingleton<spi_tclBluetoothSettings>,
   public shl::xml::tclXmlReadable
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothSettings::~spi_tclBluetoothSettings()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclBluetoothSettings()
   * \brief   Destructor
   * \sa      spi_tclBluetoothSettings()
   **************************************************************************/
   ~spi_tclBluetoothSettings();

   /***************************************************************************
   ** FUNCTION:  t_U16 spi_tclBluetoothSettings::u16GetBluetoothServiceID()
   ***************************************************************************/
   /*!
   * \fn     t_U16 u16GetBluetoothServiceID()
   * \brief  To get the Bluetooth Service ID
   * \retval t_U16
   **************************************************************************/
   t_U16 u16GetBluetoothServiceID() const;

   /***************************************************************************
   ** FUNCTION:  t_U16 spi_tclBluetoothSettings::enGetDiPoBTDisconnStrategyType()
   ***************************************************************************/
   /*!
   * \fn     enGetDiPoBTDisconnStrategyType()
   * \brief  Provides Bluetooth disconnection mode
   * \retval t_U16
   **************************************************************************/
   tenBTDisconnectStrategy enGetDiPoBTDisconnStrategyType() const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothSettings::vReadAppSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vReadAppSettings()
   * \brief  To read the settings from XML
   * \retval t_Void
   **************************************************************************/
   t_Void vReadAppSettings();


   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<spi_tclBluetoothSettings>;


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothSettings::spi_tclBluetoothSettings()
   ***************************************************************************/
   /*!
   * \fn      spi_tclBluetoothSettings()
   * \brief   Default Constructor
   * \sa      ~spi_tclBluetoothSettings()
   **************************************************************************/
   spi_tclBluetoothSettings();

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothSettings& spi_tclBluetoothSettings::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclBluetoothSettings& operator= (const spi_tclBluetoothSettings &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclBluetoothSettings& operator= (const spi_tclBluetoothSettings &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothSettings::spi_tclBluetoothSettings(const ..
   ***************************************************************************/
   /*!
   * \fn      spi_tclBluetoothSettings(const spi_tclBluetoothSettings &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclBluetoothSettings(const spi_tclBluetoothSettings &corfrSrc);

   /*************************************************************************
   ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
   *************************************************************************/
   /*!
   * \fn     virtual bool bXmlReadNode(xmlNode *poNode)
   * \brief  virtual function to read data from a xml node
   * \param  poNode : [IN] pointer to xml node
   * \retval bool : true if success, false otherwise.
   *************************************************************************/
   virtual t_Bool bXmlReadNode(xmlNodePtr poNode);

   /*************************************************************************
    ** Data Members
    *************************************************************************/

   /*
    * \brief   Bluetooth Service ID
    */
   t_U16 m_u16BluetoothServiceID;

   /*
    * \brief   Bluetooth disconnection mode for DiPO device
    */
   tenBTDisconnectStrategy  m_enDiPoBTDisconnStrategy;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclBluetoothSettings

#endif //_SPI_TCLBLUETOOTHSETTINGS_H_
