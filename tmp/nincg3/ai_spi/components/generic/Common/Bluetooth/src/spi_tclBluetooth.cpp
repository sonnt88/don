  /*!
 *******************************************************************************
 * \file         spi_tclBluetooth.cpp
 * \brief        Bluetooth Manager class that provides interface to delegate 
                 the execution of command and handle response
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Bluetooth Manager class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 21.02.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 03.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Included BT DeviceConnection and
                                                  SPI Device Deselection implementation.
 08.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Included call handling.
 08.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Removed project variant filters.
 06.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Initialisation sequence implementation
 04.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Device switch cancellation handling changes.
 31.07.2014 |  Ramya Murthy (RBEI/ECP2)         | SPI feature configuration via LoadSettings()
 29.09.2014 |  Ramya Murthy (RBEI/ECP2)         | Fix for GMMY16-16068, to prevent disconnection
                                                  of BT devices when ML device BT Address is empty.
 01.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Removed Telephone client handler
 07.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Implementation for BTPairingRequired property
 08.10.2014 |  Ramya Murthy (RBEI/ECP2)         | Impl. of CarPlay DisableBluetooth msg handling
                                                  and SelectedDevice info access protection
 24.11.2014 |  Ramya Murthy (RBEI/ECP2)         | Implemented BT block/unblock for GM
 28.05.2015 |  Tejaswini H B(RBEI/ECP2)         | Added Lint comments to suppress C++11 Errors
 19.05.2015 |  Ramya Murthy (RBEI/ECP2)         | Added vSendBTDeviceName()
 25.06.2015 | Tejaswini HB (RBEI/ECP2)          |Featuring out Android Auto

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#include "spi_tclFactory.h"
#include "spi_tclMediator.h"
#include "spi_tclBluetoothClient.h"
#include "spi_tclBluetoothSettings.h"
#include "spi_tclBluetoothRespIntf.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
#include "spi_tclMLBluetooth.h"
#endif
#include "spi_tclDiPoBluetooth.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
#include "spi_tclAAPBluetooth.h"
#endif
#include "spi_tclBluetooth.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_BLUETOOTH
      #include "trcGenProj/Header/spi_tclBluetooth.cpp.trc.h"
   #endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclBluetooth::spi_tclBluetooth();
***************************************************************************/
spi_tclBluetooth::spi_tclBluetooth(ahl_tclBaseOneThreadApp* poMainApp,
		spi_tclBluetoothRespIntf* poResponseIntf)
      : m_poMainApp(poMainApp),
		  m_poBTPolicyBase(NULL),
        m_poBTRespIntf(poResponseIntf),
        m_bIsCallActive(false)
{
   ETG_TRACE_USR1(("spi_tclBluetooth() entered "));
   SPI_NORMAL_ASSERT(NULL == m_poMainApp);
   SPI_NORMAL_ASSERT(NULL == m_poBTRespIntf);
   //! Initialize the BT handlers
   for (t_U8 u8Index = 0; u8Index < cou8NUM_BT_HANDLERS; ++u8Index)
   {
      m_poBTHandlersLst[u8Index] = NULL;
   } //for (t_U8 u8Index = 0;...)
} //!end of spi_tclBluetooth()

/***************************************************************************
** FUNCTION:  spi_tclBluetooth::~spi_tclBluetooth();
***************************************************************************/
spi_tclBluetooth::~spi_tclBluetooth()
{
   ETG_TRACE_USR1(("~spi_tclBluetooth() entered "));
   for (t_U8 u8Index = 0; u8Index < cou8NUM_BT_HANDLERS; ++u8Index)
   {
      m_poBTHandlersLst[u8Index] = NULL;
   } //for (t_U8 u8Index = 0;...)
   m_poBTRespIntf = NULL;
   m_poBTPolicyBase = NULL;
   m_poMainApp = NULL;
} //!end of ~spi_tclBluetooth()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetooth::bInitialize()
***************************************************************************/
t_Bool spi_tclBluetooth::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclBluetooth::bInitialize() entered "));

   t_Bool bInit = false;

   //Read the settings from the XML
   spi_tclBluetoothSettings* poBTSettings = spi_tclBluetoothSettings::getInstance();
   SPI_NORMAL_ASSERT(NULL == poBTSettings);
   if (NULL != poBTSettings)
   {
      t_U16 u16BTServiceID = poBTSettings->u16GetBluetoothServiceID();

      //!Create BT related client handlers - for GM, Suzuki & G3G projects
      SPI_NORMAL_ASSERT(NULL == m_poMainApp);
      if (0 != u16BTServiceID)
      {
         if (NULL != m_poMainApp)
         {
            //!Create Bluetooth Client handler
            m_poBTPolicyBase = new spi_tclBluetoothClient(m_poMainApp, u16BTServiceID);
            SPI_NORMAL_ASSERT(NULL == m_poBTPolicyBase);

            bInit = (NULL != m_poBTPolicyBase);
         } //if (NULL != m_poMainApp)
      } //if (0 != u16BTServiceID)
      else
      {
         bInit = true;
         //!@Note: For PSA project, client handlers are not created currently!
      }

      if (NULL != m_poBTPolicyBase)
      {

#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
         m_poBTHandlersLst[e8_BT_ML_INDEX] = new spi_tclMLBluetooth(this, m_poBTPolicyBase);
         SPI_NORMAL_ASSERT(NULL == m_poBTHandlersLst[e8_BT_ML_INDEX]);
#endif

         tenBTDisconnectStrategy enDiPOBTStrategy = poBTSettings->enGetDiPoBTDisconnStrategyType();
         m_poBTHandlersLst[e8_BT_DIPO_INDEX] = new spi_tclDiPoBluetooth(this, m_poBTPolicyBase, enDiPOBTStrategy);
         SPI_NORMAL_ASSERT(NULL == m_poBTHandlersLst[e8_BT_DIPO_INDEX]);
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
         m_poBTHandlersLst[e8_BT_AAP_INDEX] = new spi_tclAAPBluetooth(this, m_poBTPolicyBase);
         SPI_NORMAL_ASSERT(NULL == m_poBTHandlersLst[e8_BT_AAP_INDEX]);

#endif

         //! Initialize the BT handlers
         for (t_U8 u8Index = 0; u8Index < cou8NUM_BT_HANDLERS; ++u8Index)
         {
            if (NULL != m_poBTHandlersLst[u8Index])
            {
               bInit = (m_poBTHandlersLst[u8Index]->bInitialise()) || bInit;
            }
         } //for (t_U8 u8Index = 0;...)
      }//if (NULL != m_poBTPolicyBase)

   }//if(NULL != poBTSettings)

   //@todo - Return value to be revisited along with startup sequence.
   return true;
} //!end of bInitialize()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetooth::bUnInitialize()
***************************************************************************/
t_Bool spi_tclBluetooth::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclBluetooth::bUnInitialize() entered "));

   t_Bool bUninitSuccess = true;

   //! Uninitialize & delete the BT handlers
   for (t_U8 u8Index = 0; u8Index < cou8NUM_BT_HANDLERS; ++u8Index)
   {
      if (NULL != m_poBTHandlersLst[u8Index])
      {
         bUninitSuccess = bUninitSuccess && (m_poBTHandlersLst[u8Index]->bUninitialise());
      }
      RELEASE_MEM(m_poBTHandlersLst[u8Index]);
   } //for (t_U8 u8Index = 0;...)

   RELEASE_MEM(m_poBTPolicyBase);

   ETG_TRACE_USR2(("spi_tclBluetooth::bUnInitialize() left with result = %u ", bUninitSuccess));
   return bUninitSuccess;
} //!end of bUnInitialize()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vLoadSettings(const trSpiFeatureSupport&...)
***************************************************************************/
t_Void spi_tclBluetooth::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vLoadSettings() entered "));
   SPI_INTENTIONALLY_UNUSED(rfcrSpiFeatureSupp);
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSaveSettings()
***************************************************************************/
t_Void spi_tclBluetooth::vSaveSettings()
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vSaveSettings() entered "));
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vOnSPISelectDeviceRequest(t_U32...)
***************************************************************************/
t_Void spi_tclBluetooth::vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle,
      tenDeviceConnectionReq enDeviceConnReq)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vOnSPISelectDeviceRequest(): "
         "DeviceHandle = 0x%x, DeviceConnectionReq = %d ",
         u32ProjectionDevHandle, ETG_ENUM(CONNECTION_REQ, enDeviceConnReq)));

   t_Bool bIsReqProcessed = false;
   if (IS_VALID_DEVHANDLE(u32ProjectionDevHandle))
   {
      if (e8DEVCONNREQ_SELECT == enDeviceConnReq)
      {
         //! Get selected device's details
         vClearSelectedDevInfo();
         vFetchSelectedDeviceDetails(u32ProjectionDevHandle);
      }

      //! Forward Select/Deselect request to respective BT handler
      tenBTHandlerIndex enBTHndlrIndex = enGetSelDevBTHandlerIndex();
      if ((e8_BT_INDEX_UNKNOWN != enBTHndlrIndex) && (NULL != m_poBTHandlersLst[enBTHndlrIndex]))
      {
         bIsReqProcessed = true;
         if (e8DEVCONNREQ_SELECT == enDeviceConnReq)
         {
            m_poBTHandlersLst[enBTHndlrIndex]->vOnSPISelectDeviceRequest(u32ProjectionDevHandle);
         }
         else
         {
            m_poBTHandlersLst[enBTHndlrIndex]->vOnSPIDeselectDeviceRequest(u32ProjectionDevHandle);
         }
      }//if ((e8_BT_INDEX_UNKNOWN != enBTHndlrIndex)&&...)
   } //if (IS_VALID_DEVHANDLE(u32ProjectionDevHandle))
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetooth::vOnSPISelectDeviceRequest: Invalid input! Nothing to be done. "));
   }

   if (false == bIsReqProcessed)
   {
      vSendSelectDeviceResult(true);
   }
} //!end of vOnSPISelectDeviceRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vOnSPISelectDeviceResponse(t_U32...)
***************************************************************************/
t_Void spi_tclBluetooth::vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
      tenDeviceConnectionReq enDeviceConnReq,
      tenResponseCode enRespCode,
      tenErrorCode enErrorCode,
      t_Bool bIsDeviceSwitch)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vOnSPISelectDeviceResponse() entered : bIsDeviceSwitch = %d ", ETG_ENUM(BOOL, bIsDeviceSwitch)));
   ETG_TRACE_USR3((" vOnSPISelectDeviceResponse: Received DeviceHandle = 0x%x ", u32ProjectionDevHandle));
   ETG_TRACE_USR3((" vOnSPISelectDeviceResponse: Received DevConnectionRequest = %d ",
         ETG_ENUM(CONNECTION_REQ, enDeviceConnReq)));
   ETG_TRACE_USR3((" vOnSPISelectDeviceResponse: Received ResponseCode = %d ", ETG_ENUM(RESPONSE_CODE, enRespCode)));
   ETG_TRACE_USR3((" vOnSPISelectDeviceResponse: Received ErrorCode = %d ", ETG_ENUM(ERROR_CODE, enErrorCode)));

   if (
      (IS_VALID_DEVHANDLE(u32ProjectionDevHandle))
      &&
      (u32ProjectionDevHandle == u32GetSelectedDevHandle()) /*Check if response is for active device*/
      )
   {
      tenBTHandlerIndex enBTHndlrIndex = enGetSelDevBTHandlerIndex();
      if ((e8_BT_INDEX_UNKNOWN != enBTHndlrIndex) && (NULL != m_poBTHandlersLst[enBTHndlrIndex]))
      {
         //! Forward Select/Deselect response to respective BT handler
         if (e8DEVCONNREQ_SELECT == enDeviceConnReq)
         {
            m_poBTHandlersLst[enBTHndlrIndex]->vOnSPISelectDeviceResponse(u32ProjectionDevHandle, enRespCode);
         }
         else
         {
            m_poBTHandlersLst[enBTHndlrIndex]->vOnSPIDeselectDeviceResponse(u32ProjectionDevHandle, enRespCode, bIsDeviceSwitch);
         }
      }//if ((e8_BT_INDEX_UNKNOWN != enBTHndlrIndex) &&...)

      //! Clear stored SelectedDevice's details if no device is active.
      if (
         ((e8DEVCONNREQ_SELECT == enDeviceConnReq) && (e8FAILURE == enRespCode))
         ||
         (e8DEVCONNREQ_DESELECT == enDeviceConnReq)
         )
      {
         vClearSelectedDevInfo();
      }
   }//if ((IS_VALID_DEVHANDLE(u32ProjectionDevHandle))&&...)
   else
   {
      ETG_TRACE_ERR((" vOnSPISelectDeviceResponse: Invalid input/No device active! Nothing to be done. "));
   }
   //! Cancel any ongoing device switch event
   if (
      ((e8DEVCONNREQ_SELECT == enDeviceConnReq) && (e8FAILURE == enRespCode))
      ||
      (e8DEVCONNREQ_DESELECT == enDeviceConnReq)
      )
   {
      vSendDeviceSwitchEvent(0, 0, e8NO_CHANGE, false, false);
   }//if (e8NO_CHANGE != enGetSelectedDevBTChangeInfo())
} //!end of vOnSPISelectDeviceResponse()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetooth::bValidateBTPairingRequired()
***************************************************************************/
t_Bool spi_tclBluetooth::bValidateBTPairingRequired()
{
   t_Bool bPairingRequired = false;

   //! Check if BT pairing is required for current active projection device
   if (IS_VALID_DEVHANDLE(u32GetSelectedDevHandle()))
   {
      tenBTHandlerIndex enBTHndlrIndex = enGetSelDevBTHandlerIndex();
      if ((e8_BT_INDEX_UNKNOWN != enBTHndlrIndex) && (NULL != m_poBTHandlersLst[enBTHndlrIndex]))
      {
         bPairingRequired = m_poBTHandlersLst[enBTHndlrIndex]->bCheckBTPairingRequired();
      }
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetooth::bValidateBTPairingRequired: No Projection device is active! "));
   }

   ETG_TRACE_USR2(("spi_tclBluetooth::bValidateBTPairingRequired() left with PairingRequired = %u ",
         ETG_ENUM(BOOL, bPairingRequired)));
   return bPairingRequired;
} //!end of bValidateBTPairingRequired()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vOnInvokeBTDeviceAction(t_U32...)
***************************************************************************/
t_Void spi_tclBluetooth::vOnInvokeBTDeviceAction(t_U32 u32BluetoothDevHandle,
		t_U32 u32ProjectionDevHandle,
		tenBTChangeInfo enBTChange)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vOnInvokeBTDeviceAction() entered:"));
   ETG_TRACE_USR2((" vOnInvokeBTDeviceAction: Received BluetoothDevHandle = 0x%x ", u32BluetoothDevHandle));
   ETG_TRACE_USR2((" vOnInvokeBTDeviceAction: Received ProjectionDevHandle = 0x%x ", u32ProjectionDevHandle));
   ETG_TRACE_USR2((" vOnInvokeBTDeviceAction: Received BTChangeInfo = %d ", ETG_ENUM(BT_CHANGE_INFO, enBTChange)));
   
   //@Note: Device switch event is notified to users via BluetoothDeviceStatus
   //property. Based on the device switch type, users can opt to continue with
   //the change (i.e, switch device), or cancel the device switch operation.

   tenBTChangeInfo enSelDevBTChange = enGetSelectedDevBTChangeInfo();
   ETG_TRACE_USR3(("spi_tclBluetooth::vOnInvokeBTDeviceAction: Current active device change = %d ", ETG_ENUM(BT_CHANGE_INFO, enSelDevBTChange)));

   //! Handle requested device switch action
   if (
      (IS_VALID_DEVHANDLE(u32BluetoothDevHandle))
      &&
      (IS_VALID_DEVHANDLE(u32ProjectionDevHandle))
      &&
      (u32ProjectionDevHandle == u32GetSelectedDevHandle()) /*If trigger is for active proj. device*/
      )
   {
      switch (enSelDevBTChange)
      {
         case e8SWITCH_BT_TO_ML:
         case e8SWITCH_ML_TO_BT:
            if (NULL != m_poBTHandlersLst[e8_BT_ML_INDEX])
            {
               m_poBTHandlersLst[e8_BT_ML_INDEX]->vOnBTDeviceSwitchAction(u32BluetoothDevHandle,
                     u32ProjectionDevHandle, enSelDevBTChange, enBTChange);
            }
            break;
         case e8SWITCH_BT_TO_DIPO:
         case e8SWITCH_DIPO_TO_BT:
            if (NULL != m_poBTHandlersLst[e8_BT_DIPO_INDEX])
            {
               m_poBTHandlersLst[e8_BT_DIPO_INDEX]->vOnBTDeviceSwitchAction(u32BluetoothDevHandle,
                     u32ProjectionDevHandle, enSelDevBTChange, enBTChange);
            }
            break;
         case e8SWITCH_BT_TO_AAP:
         case e8SWITCH_AAP_TO_BT:
            if (NULL != m_poBTHandlersLst[e8_BT_AAP_INDEX])
            {
               m_poBTHandlersLst[e8_BT_AAP_INDEX]->vOnBTDeviceSwitchAction(u32BluetoothDevHandle,
                     u32ProjectionDevHandle, enSelDevBTChange, enBTChange);
            }
            break;
         default:
            ETG_TRACE_ERR(("spi_tclBluetooth::vOnInvokeBTDeviceAction: Invalid BT change type! "));
            break;
      }

      //Clear the stored BT change info since device switch processing is complete.
      vSetSelectedDevBTChangeInfo(e8NO_CHANGE);

   } //if ((IS_VALID_DEVHANDLE(u32BluetoothDevHandle))&&...)
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetooth::vOnInvokeBTDeviceAction: Invalid input/action! Nothing to be done. "));
   }
} //!end of vOnInvokeBTDeviceAction()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vOnDisableBluetooth(t_String...)
***************************************************************************/
t_Void spi_tclBluetooth::vOnDisableBluetooth(t_String szDiPODevBTAddress)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vOnDisableBluetooth() entered: szDiPODevBTAddress = %s ", szDiPODevBTAddress.c_str()));

   //! Forward request to respective BT handler
   if (IS_VALID_DEVHANDLE(u32GetSelectedDevHandle()))
   {
      tenBTHandlerIndex enBTHndlrIndex = enGetSelDevBTHandlerIndex();
      if ((e8_BT_INDEX_UNKNOWN != enBTHndlrIndex) && (NULL != m_poBTHandlersLst[enBTHndlrIndex]))
      {
         m_poBTHandlersLst[enBTHndlrIndex]->vOnDisableBluetooth(szDiPODevBTAddress);
      }
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclBluetooth::vOnDisableBluetooth: No action taken since no Projection device is active! "));
   }
}//!end of vOnDisableBluetooth()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vGetVehicleBTAddress(t_String& ...)
***************************************************************************/
t_Void spi_tclBluetooth::vGetVehicleBTAddress(t_String& rfszVehicleBTAddress)
{
   if (NULL != m_poBTPolicyBase)
   {
      m_poBTPolicyBase->vGetVehicleBTAddress(rfszVehicleBTAddress);
   }
   ETG_TRACE_USR4(("spi_tclBluetooth::vGetVehicleBTAddress() left with : %s ",
         rfszVehicleBTAddress.c_str()));
} //!end of vGetVehicleBTAddress()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vOnCallStatus(t_Bool bCallActive)
***************************************************************************/
t_Void spi_tclBluetooth::vOnCallStatus(t_Bool bCallActive)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vOnCallStatus() entered: CallActive = %d ",
         ETG_ENUM(BOOL, bCallActive)));

   m_oCallStatusLock.s16Lock();
   m_bIsCallActive = bCallActive;
   m_oCallStatusLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSendSelectDeviceResult(t_Bool...)
***************************************************************************/
t_Void spi_tclBluetooth::vSendSelectDeviceResult(t_Bool bSelectionSuccess)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vSendSelectDeviceResult() entered: SelectionSuccess = %u ",
         ETG_ENUM(BOOL, bSelectionSuccess)));

   if (false == bSelectionSuccess)
   {
      //! Clear all selected device data since Projection device selection is canceled/failed.
      vClearSelectedDevInfo();
   }
   tenErrorCode enErrorCode = (true==bSelectionSuccess)? e8NO_ERRORS : e8SELECTION_FAILED;
   spi_tclMediator* poMediator = spi_tclMediator::getInstance();
   SPI_NORMAL_ASSERT(NULL == poMediator);
   if (NULL != poMediator)
   {
      poMediator->vPostSelectDeviceRes(e32COMPID_BLUETOOTH, enErrorCode);
   }
} //!end of vSendSelectDeviceResult()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSendDeselectDeviceRequest()
***************************************************************************/
t_Void spi_tclBluetooth::vSendDeselectDeviceRequest()
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vSendDeselectDeviceRequest() entered "));

   //! Trigger Deselection of Projection device.
   spi_tclMediator* poMediator = spi_tclMediator::getInstance();
   SPI_NORMAL_ASSERT(NULL == poMediator);
   if (NULL != poMediator)
   {
      t_U32 u32SelDevHandle = u32GetSelectedDevHandle();
      if (IS_VALID_DEVHANDLE(u32SelDevHandle))
      {
         poMediator->vPostAutoDeviceSelection(u32SelDevHandle, e8DEVCONNREQ_DESELECT);
         //! Set the user deselect flag in Conn Manager.
         //@TODO: temporary workaround since internal deselection results in cyclic select
         //with automatic selection strategy in GM.
         poMediator->vPostSetUserDeselect(u32SelDevHandle);
      }
      else
      {
         ETG_TRACE_ERR(("spi_tclBluetooth::vSendDeselectDeviceRequest: "
               "No action taken since no Projection device is active! "));
      }
   }//if (NULL != poMediator)
} //!end of vSendDeselectDeviceRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSendDeviceSwitchEvent(t_U32 ...)
***************************************************************************/
t_Void spi_tclBluetooth::vSendDeviceSwitchEvent(t_U32 u32BluetoothDevHandle,
      t_U32 u32ProjectionDevHandle,
      tenBTChangeInfo enBTChange,
      t_Bool bSameDevice,
      t_Bool bCallActive)
{
   ETG_TRACE_USR4(("spi_tclBluetooth::vSendDeviceSwitchEvent() entered: "
         "BluetoothDevHandle = 0x%x, ProjectionDevHandle = 0x%x, "
         "BTChange = %u, SameDevice = %u, CallActive = %u ",
         u32BluetoothDevHandle, u32ProjectionDevHandle,
         ETG_ENUM(BT_CHANGE_INFO, enBTChange),
         ETG_ENUM(BOOL, bSameDevice),
         ETG_ENUM(BOOL, bCallActive)));

   if (NULL != m_poBTRespIntf)
   {
      //! Store info about device change.
      vSetSelectedDevBTChangeInfo(enBTChange);

      //! Notify clients about switch between Projection & BT device.
      m_poBTRespIntf->vPostBluetoothDeviceStatus(u32BluetoothDevHandle,
            u32ProjectionDevHandle, enBTChange, bSameDevice, bCallActive);
      ETG_TRACE_USR4(("spi_tclBluetooth::Informed user about device switch. Awaiting user action. "));
   }
} //!end of vSendDeviceSwitchEvent()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSendBTPairingRequired(t_String szBTAddress)
***************************************************************************/
t_Void spi_tclBluetooth::vSendBTPairingRequired(t_String szBTAddress)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vSendBTPairingRequired() entered: szBTAddress = %s ",
         szBTAddress.c_str()));

   if ((IS_VALID_BT_ADDRESS(szBTAddress)) && (NULL != m_poBTRespIntf))
   {
      m_poBTRespIntf->vPostBTPairingRequired(szBTAddress, true);
   }
} //!end of vSendBTPairingRequired()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSendBTDeviceName(...)
***************************************************************************/
t_Void spi_tclBluetooth::vSendBTDeviceName(t_U32 u32ProjectionDevHandle,
         t_String szBTDeviceName)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vSendBTDeviceName() entered "
            "ProjectionDevHandle = %d, BTDeviceName = %s ",
            u32ProjectionDevHandle, szBTDeviceName.c_str()));

   spi_tclMediator* poMediator = spi_tclMediator::getInstance();
   SPI_NORMAL_ASSERT(NULL == poMediator);
   if ((NULL != poMediator) && (IS_VALID_DEVHANDLE(u32ProjectionDevHandle)))
   {
      poMediator->vPostBTDeviceName(u32ProjectionDevHandle, szBTDeviceName);
   }
} //!end of vSendBTPairingRequired()

/***************************************************************************
** FUNCTION:  t_U32 spi_tclBluetooth::u32GetSelectedDevHandle()
***************************************************************************/
t_U32 spi_tclBluetooth::u32GetSelectedDevHandle()
{
   //Return DeviceHandle of selected projection device
   m_SelDevDetailsLock.s16Lock();
   t_U32 u32SelDevHandle = m_rSelDeviceDetails.u32DeviceHandle;
   m_SelDevDetailsLock.vUnlock();
   return u32SelDevHandle;
} //!end of u32GetSelectedDevHandle()

/***************************************************************************
** FUNCTION:  t_String spi_tclBluetooth::szGetSelectedDevBTAddress()
***************************************************************************/
t_String spi_tclBluetooth::szGetSelectedDevBTAddress()
{
   //Return BT Address of selected projection device
   m_SelDevDetailsLock.s16Lock();
   t_String szSelDevBTAddr = m_rSelDeviceDetails.szBTAddress;
   m_SelDevDetailsLock.vUnlock();
   return szSelDevBTAddr;
} //!end of szGetSelectedDevBTAddress()

/***************************************************************************
** FUNCTION:  tenDeviceStatus spi_tclBluetooth::enGetSelectedDevStatus()
***************************************************************************/
tenDeviceStatus spi_tclBluetooth::enGetSelectedDevStatus()
{
   //Return DeviceStatus of selected projection device
   m_SelDevDetailsLock.s16Lock();
   tenDeviceStatus enSelDevStatus = m_rSelDeviceDetails.enDeviceStatus;
   m_SelDevDetailsLock.vUnlock();
   return enSelDevStatus;
} //!end of enGetSelectedDevStatus()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSetSelectedDevStatus()
***************************************************************************/
t_Void spi_tclBluetooth::vSetSelectedDevStatus(tenDeviceStatus enDevStatus)
{
   //Store the DeviceStatus of selected projection device
   m_SelDevDetailsLock.s16Lock();
   ETG_TRACE_USR1(("spi_tclBluetooth::vSetSelectedDevStatus(): "
         "current DeviceStatus = %u, new DeviceStatus = %u ",
         ETG_ENUM(DEVICE_STATUS, m_rSelDeviceDetails.enDeviceStatus),
         ETG_ENUM(DEVICE_STATUS, enDevStatus)));
   m_rSelDeviceDetails.enDeviceStatus = enDevStatus;
   m_SelDevDetailsLock.vUnlock();
} //!end of vSetSelectedDevStatus()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSetSelectedDevBTAddress()
***************************************************************************/
t_Void spi_tclBluetooth::vSetSelectedDevBTAddress(t_String szBTAddress)
{
   //Store the BT Address of selected projection device
   m_SelDevDetailsLock.s16Lock();
   m_rSelDeviceDetails.szBTAddress = szBTAddress;
   m_SelDevDetailsLock.vUnlock();
} //!end of vSetSelectedDevBTAddress()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclBluetooth::bGetCallStatus()
***************************************************************************/
t_Bool spi_tclBluetooth::bGetCallStatus()
{
   //Return the current call status of BT phone
   m_oCallStatusLock.s16Lock();
   t_Bool bCallActive = m_bIsCallActive;
   m_oCallStatusLock.vUnlock();
   return bCallActive;
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSaveSettings()
***************************************************************************/
tenBTHandlerIndex spi_tclBluetooth::enGetSelDevBTHandlerIndex()
{
   //Return the selected device's index
   m_SelDevDetailsLock.s16Lock();
   tenDeviceCategory enSelDevCat = m_rSelDeviceDetails.enDeviceCategory;
   m_SelDevDetailsLock.vUnlock();

   tenBTHandlerIndex enBTIndex = e8_BT_INDEX_UNKNOWN;
   switch (enSelDevCat)
   {
      case e8DEV_TYPE_MIRRORLINK:
         enBTIndex = e8_BT_ML_INDEX;
         break;
      case e8DEV_TYPE_DIPO:
         enBTIndex = e8_BT_DIPO_INDEX;
         break;
      case e8DEV_TYPE_ANDROIDAUTO:
         enBTIndex = e8_BT_AAP_INDEX;
         break;
      default:
         break;
   }//switch (enSelDevCat)
   return enBTIndex;
} //!end of enGetSelDevBTHandlerIndex()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vFetchSelectedDeviceDetails(t_U32...)
***************************************************************************/
t_Void spi_tclBluetooth::vFetchSelectedDeviceDetails(t_U32 u32SelectedDevHandle)
{
   ETG_TRACE_USR1(("spi_tclBluetooth::vFetchSelectedDeviceDetails() entered: DeviceHandle = 0x%x ",
         u32SelectedDevHandle));

   m_SelDevDetailsLock.s16Lock();

   //Store device handle
   m_rSelDeviceDetails.u32DeviceHandle = u32SelectedDevHandle;

   //! Get Selected ML/DiPO device's BTAddress from ConnectionManager
   spi_tclFactory* poSPIFactory = spi_tclFactory::getInstance();
   SPI_NORMAL_ASSERT(NULL == poSPIFactory);
   if (
      (NULL != poSPIFactory)
      &&
      (cou32InvalidDeviceHandle != u32SelectedDevHandle)
      )
   {
      spi_tclConnMngr* poConnMngr = poSPIFactory->poGetConnMngrInstance();
      SPI_NORMAL_ASSERT(NULL == poConnMngr);
      if (NULL != poConnMngr)
      {
         //Fetch BT address of projection device
         poConnMngr->vGetBTAddress(u32SelectedDevHandle, m_rSelDeviceDetails.szBTAddress);
         //Fetch Device category of projection device
         m_rSelDeviceDetails.enDeviceCategory = poConnMngr->enGetDeviceCategory(u32SelectedDevHandle);

         ETG_TRACE_USR4(("spi_tclBluetooth::vFetchSelectedDeviceDetails: Retrieved DeviceCategory = %d, BTAddress = %s ",
               ETG_ENUM(DEVICE_CATEGORY, m_rSelDeviceDetails.enDeviceCategory),
               m_rSelDeviceDetails.szBTAddress.c_str()));
      } //if (NULL != poConnMngr)
   } //if (NULL != poSPIFactory)

   m_SelDevDetailsLock.vUnlock();

} //!end of vFetchSelectedDeviceDetails()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vClearSelectedDevInfo()
***************************************************************************/
t_Void spi_tclBluetooth::vClearSelectedDevInfo()
{
   //Set selected device details to defaults
   m_SelDevDetailsLock.s16Lock();
   m_rSelDeviceDetails.u32DeviceHandle  = cou32InvalidDeviceHandle;
   m_rSelDeviceDetails.szBTAddress      = "";
   m_rSelDeviceDetails.enDeviceCategory = e8DEV_TYPE_UNKNOWN;
   m_rSelDeviceDetails.enDeviceStatus   = e8DEVICE_CHANGE_COMPLETE;
   m_rSelDeviceDetails.enBTChangeInfo   = e8NO_CHANGE;
   m_SelDevDetailsLock.vUnlock();
} //!end of vClearSelectedDevInfo()

/***************************************************************************
** FUNCTION:  tenBTChangeInfo spi_tclBluetooth::enGetSelectedDevBTChangeInfo()
***************************************************************************/
tenBTChangeInfo spi_tclBluetooth::enGetSelectedDevBTChangeInfo()
{
   //Return the current BT device change 
   m_SelDevDetailsLock.s16Lock();
   tenBTChangeInfo enSelDevBTInfo = m_rSelDeviceDetails.enBTChangeInfo;
   m_SelDevDetailsLock.vUnlock();
   return enSelDevBTInfo;
} //!end of enGetSelectedDevBTChangeInfo()

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetooth::vSetSelectedDevBTChangeInfo()
***************************************************************************/
t_Void spi_tclBluetooth::vSetSelectedDevBTChangeInfo(tenBTChangeInfo enBTChangeInfo)
{
   //Store the BT device change information
   m_SelDevDetailsLock.s16Lock();
   m_rSelDeviceDetails.enBTChangeInfo = enBTChangeInfo;
   m_SelDevDetailsLock.vUnlock();
} //!end of vSetSelectedDevBTChangeInfo()
//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
