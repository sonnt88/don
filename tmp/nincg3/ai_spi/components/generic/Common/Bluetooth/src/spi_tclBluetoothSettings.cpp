/***********************************************************************/
/*!
* \file  spi_tclBluetoothSettings.h
* \brief Class to get the Bluetooth Manager Settings
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Bluetooth Manager Settings
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
08.04.2014  | Ramya Murthy          | Initial Version
24.11.2014  | Ramya Murthy          | Included BT disconnection strategy

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "FileHandler.h"
#include "XmlDocument.h"
#include "XmlReader.h"
#include "spi_tclBluetoothSettings.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_BLUETOOTH
      #include "trcGenProj/Header/spi_tclBluetoothSettings.cpp.trc.h"
   #endif
#endif

#ifdef VARIANT_S_FTR_ENABLE_GM
   static const t_Char* pczConfigFilePath = "/opt/bosch/gm/policy.xml";
#else
   static const t_Char* pczConfigFilePath = "/opt/bosch/spi/xml/policy.xml";
#endif

using namespace shl::xml;
/***************************************************************************
** FUNCTION:  spi_tclBluetoothSettings::spi_tclBluetoothSettings()
***************************************************************************/
spi_tclBluetoothSettings::spi_tclBluetoothSettings()
   : m_u16BluetoothServiceID(0),
     m_enDiPoBTDisconnStrategy(e8BT_STRATEGY_DISCONNECT_ALL)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   vReadAppSettings();
}

/***************************************************************************
** FUNCTION:  spi_tclBluetoothSettings::~spi_tclBluetoothSettings()
***************************************************************************/
spi_tclBluetoothSettings::~spi_tclBluetoothSettings()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
** FUNCTION:  t_U16 spi_tclBluetoothSettings::u16GetBluetoothServiceID()
***************************************************************************/
t_U16 spi_tclBluetoothSettings::u16GetBluetoothServiceID() const
{
   ETG_TRACE_USR1(("spi_tclBluetoothSettings::u16GetBluetoothServiceID: %u \n",
      m_u16BluetoothServiceID));
   return m_u16BluetoothServiceID;
}

/***************************************************************************
** FUNCTION:  t_U16 spi_tclBluetoothSettings::enGetDiPoBTDisconnStrategyType()
***************************************************************************/
tenBTDisconnectStrategy spi_tclBluetoothSettings::enGetDiPoBTDisconnStrategyType() const
{
   ETG_TRACE_USR1(("spi_tclBluetoothSettings::enGetDiPoBTDisconnStrategyType: %u \n",
         ETG_ENUM(BT_DISCONN_MODE, m_enDiPoBTDisconnStrategy)));
   return m_enDiPoBTDisconnStrategy;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclBluetoothSettings::vReadAppSettings()
***************************************************************************/
t_Void spi_tclBluetoothSettings::vReadAppSettings()
{
   ETG_TRACE_USR1(("spi_tclBluetoothSettings::vReadAppSettings\n"));

   //Check the validity of the xml file
   spi::io::FileHandler oPolicySettingsFile(pczConfigFilePath,
      spi::io::SPI_EN_RDONLY);
   if (true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument oXmlDoc(pczConfigFilePath);
      tclXmlReader oXmlReader(&oXmlDoc, this);

      oXmlReader.bRead("BLUETOOTH");
   } // if (true == oPolicySettingsFile.bIsValid())
}

/*************************************************************************
** FUNCTION:  t_Bool spi_tclBluetoothSettings::bXmlReadNode(xmlNode *poNode)
*************************************************************************/
t_Bool spi_tclBluetoothSettings::bXmlReadNode(xmlNodePtr poNode)
{
   ETG_TRACE_USR1(("spi_tclBluetoothSettings::bXmlReadNode()"));

   t_String szAttrName;
   t_Bool bRetVal = false;
   t_String szNodeName;
   t_S32 s32Value = 0;

   if (NULL != poNode)
   {
      szNodeName = (const t_Char *) (poNode->name);
   } // if (NULL != poNode)

   if ("BLUETOOTH_SERVICE_ID" == szNodeName)
   {
      szAttrName = "VALUE";
      bRetVal = bGetAttribute(szAttrName, poNode, s32Value);
      m_u16BluetoothServiceID = static_cast<t_U16>(s32Value);
      ETG_TRACE_USR2(("spi_tclBluetoothSettings::bXmlReadNode: Bluetooth ServiceID - %d",
            m_u16BluetoothServiceID));
   }

   else if ("DIPO_BTDISCONNECT_STRATEGY" == szNodeName)
   {
      bRetVal = bGetAttribute("BTDISCONNECT_STRATEGY", poNode, s32Value);
      m_enDiPoBTDisconnStrategy = ((s32Value >= 0) && (s32Value <= e8BT_STRATEGY_BLOCK_ALL))?
           (static_cast<tenBTDisconnectStrategy>(s32Value)): (e8BT_STRATEGY_DISCONNECT_ALL);
      ETG_TRACE_USR2(("spi_tclBluetoothSettings::bXmlReadNode: Bluetooth Disconnection Strategy - %d",
            m_enDiPoBTDisconnStrategy));
   } //if ("DIPO_BTDISCONNECT_STRATEGY" == szNodeName)

   return bRetVal;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
