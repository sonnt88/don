/*!
 *******************************************************************************
 * \file             spi_tclMediator.h
 * \brief            Mediator to distribute messages among SPI components
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Mediator to distribute messages among SPI components
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 27.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMEDIATOR_H_
#define SPI_TCLMEDIATOR_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <functional>
#include "GenericSingleton.h"
#include "SPITypes.h"
#include "spi_ConnMngrTypeDefines.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	

//! Callback signatures definitions: To be registered by the SPI component
/*lint -esym(40,function)function Undeclared identifier */
//! Callback for Posting device selection result
typedef std::function<t_Void(tenCompID, tenErrorCode)> tfvOnSelectDeviceRes;
//! Callback for posting device disconnection
typedef std::function<t_Void(t_U32)> tfvOnDeviceDisconnection;
//! Callback for posting device connection
typedef std::function<t_Void(t_U32)> tfvOnDeviceConnection;
//! Callback for automatically selecting device
typedef std::function<void(t_U32, tenDeviceConnectionReq, tenDeviceCategory)> tfvAutoSelectDevice;
//! Callback for setting user deselect flag
typedef std::function<void(t_U32)> tfvSetUserDeselect;
//! Callback for applying selection strategy
typedef std::function<void()> tfvApplySelStrategy;
//! Callback for setting DAY/Night
typedef std::function<void(tenVehicleConfiguration)> tfvSetDayNightMode;
//! Callback for providing BT Device name
typedef std::function<void(t_U32, t_String)> tfvSetBTDeviceName;

/*!
* \typedef - To Update the Terminate App Response
*/
typedef std::function<t_Void(const tenCompID, const t_U32,const t_U32,tenErrorCode ,
                              const trUserContext&)> tvTerminateAppResult;
/*!
* \typedef - To Update the Launch App Response
*/
typedef std::function<t_Void(const tenCompID, const t_U32,const t_U32,const tenDiPOAppType,tenErrorCode , 
                             const trUserContext&)> tvLaunchAppResult;

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
/*!
 * \brief Structure holding the callbacks to be registered by the
 * Creator of this class object
 */
struct trSelectDeviceCallbacks
{
    /*lint -esym(40,fvOnSelectDeviceRes)fvOnSelectDeviceRes Undeclared identifier */
      //! Informs Result of the select device
      tfvOnSelectDeviceRes fvOnSelectDeviceRes;

      //! Informs when a connected device is disconnected
      tfvOnDeviceDisconnection fvOnDeviceDisconnection;

      //! Informs new device connection
      tfvOnDeviceConnection fvOnDeviceConnection;

      //! informs when a device is automatically selected
      tfvAutoSelectDevice fvAutoSelectDevice;

      //! informs user device deactivation
      tfvSetUserDeselect fvSetUserDeselect;

      //! informas when device selection is applied
      tfvApplySelStrategy fvApplySelStrategy;

      trSelectDeviceCallbacks() :
                  fvOnSelectDeviceRes(NULL),
                  fvOnDeviceDisconnection(NULL),
                  fvOnDeviceConnection(NULL),
                  fvAutoSelectDevice(NULL),
                  fvSetUserDeselect(NULL),
                  fvApplySelStrategy(NULL)
      {
         //Add code
      }
};


/*!
* \brief Structure holding the callbacks to be registered by the
* Creator of this class object
*/
struct trConnMngrCallback
{
      /*lint -esym(40,fvSetBTDeviceName)fvSetBTDeviceName Undeclared identifier */
      //! informs BT Device name of active projection device
      tfvSetBTDeviceName fvSetBTDeviceName;

      trConnMngrCallback() :
         fvSetBTDeviceName(NULL)
      {
         //Add code
      }
};

/*!
 * \brief Structure holding the callbacks to be registered by the
 * Creator of this class object
 */
struct trAppLauncherCallbacks
{
    /*lint -esym(40,fpvTerminateAppResult)fpvTerminateAppResult Undeclared identifier */
   //! Result of Terminate App request
   tvTerminateAppResult fpvTerminateAppResult;

   //! Result of Launch App request
   tvLaunchAppResult   fpvLaunchAppResult;

   trAppLauncherCallbacks():fpvTerminateAppResult(NULL),
      fpvLaunchAppResult(NULL)
   {
      //Add code
   }

};

/*!
 * \brief Structure holding the callbacks to be registered by the
 * Creator of this class object
 */
struct trAAPSensorCallback
{
      /*lint -esym(40,fvSetDayNightMode)fvSetDayNightMode Undeclared identifier */
      //! informs user about day/night mode
      tfvSetDayNightMode fvSetDayNightMode;

      trAAPSensorCallback() :
         fvSetDayNightMode(NULL)
      {
         //Add code
      }
};

/*!
 * \class spi_tclMediator
 * \brief Mediator to distribute messages among SPI components
 */
class spi_tclMediator: public GenericSingleton<spi_tclMediator>
{
   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::~spi_tclMediator
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclMediator()
       * \brief  Destructor
       **************************************************************************/
      ~spi_tclMediator();

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::vRegisterCallbacks(const trConnMngrCallback &rfrConnMngrCb)
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks(const trConnMngrCallback &rfrConnMngrCb)
       * \brief  Register for connection manager callbacks
       * \param  rfrConnMngrCb structure holding connection manager callbacks
       **************************************************************************/
      t_Void vRegisterCallbacks(const trConnMngrCallback &rfrConnMngrCb);

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::vRegisterCallbacks(const trSelectDeviceCallbacks &rSelDevCb)
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks(const trSelectDeviceCallbacks &rSelDevCb)
       * \brief  Register for connection management callbacks
       * \param  rSelDevCb structure holding select device callbacks
       **************************************************************************/
      t_Void vRegisterCallbacks(const trSelectDeviceCallbacks &rSelDevCb);

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::vRegisterCallbacks(const trAppLauncherCallbacks &corfrAppLauncherCb)
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks(const trAppLauncherCallbacks &corfrAppLauncherCb)
       * \brief  Register for Launch App callbacks
       * \param  corfrAppLauncherCb: [IN] structure holding AppLauncher callbacks
       **************************************************************************/
      t_Void vRegisterCallbacks(const trAppLauncherCallbacks& corfrAppLauncherCb);

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::vRegisterCallbacks(const trAAPSensorCallback& corfrDayNightModeCb)
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks(const trAAPSensorCallback& corfrDayNightModeCb)
       * \brief  Register for DayNight callbacks
       * \param  corfrDayNightModeCb: [IN] structure holding DayNight callbacks
       **************************************************************************/
      t_Void vRegisterCallbacks(const trAAPSensorCallback& corfrDayNightModeCb);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMediator::vPostSelectDeviceResult
       ***************************************************************************/
      /*!
       * \fn     vPostSelectDeviceResult
       * \brief  Called by SPI Component to post result for select device request
       * \param  enCompID : SPI component posting select device response
       * \param  enErrorCode   : Result of select device request
       * \sa     spi_tclCmdInterface::vSelectDevice
       **************************************************************************/
       t_Void vPostSelectDeviceRes(tenCompID enCompID, tenErrorCode enErrorCode);

       /***************************************************************************
        ** FUNCTION:  spi_tclMediator::vPostDeviceConnection
        ***************************************************************************/
       /*!
        * \fn     vPostDeviceConnection()
        * \brief  Called by SPI component when a device is Connected
        * \param u32DeviceHandle: Device Handle of the device Connected
        **************************************************************************/
       t_Void vPostDeviceConnection(const t_U32 u32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::vPostDeviceDisconnection
       ***************************************************************************/
      /*!
       * \fn     vPostDeviceDisconnection()
       * \brief  Called by SPI component when a device is disconnected
       * \param u32DeviceHandle: Device Handle of the device disconnected
       **************************************************************************/
      t_Void vPostDeviceDisconnection(const t_U32 u32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::vPostAutoDeviceSelection
       ***************************************************************************/
      /*!
       * \fn     vPostAutoDeviceSelection()
       * \brief  Called by SPI component when a device is selected automatically
       *         by applying selection strategy
       * \param u32DeviceHandle: Device Handle
       **************************************************************************/
      t_Void vPostAutoDeviceSelection(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionReq enConnReq);

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::vPostSetUserDeselect
       ***************************************************************************/
      /*!
       * \fn     vPostSetUserDeselect()
       * \brief  Called by SPI component when a device is deselected as a result of
       *         user action
       * \param u32DeviceHandle: Device Handle
       **************************************************************************/
      t_Void vPostSetUserDeselect(const t_U32 cou32DeviceHandle);


      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::vPostDayNightMode
       ***************************************************************************/
      /*!
       * \fn     vPostDayNightMode()
       * \brief  Called by SPI component to set Day/Night
       * \param enVehicleConfig: contains day/night
       **************************************************************************/
      t_Void vPostDayNightMode(tenVehicleConfiguration enVehicleConfig);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMediator::vPostLaunchAppResult
       ***************************************************************************/
      /*!
       * \fn     vPostLaunchAppResult(
       *              cons tenCompID coenCompID
       *              const t_U32 cou32DevId,
       *              const t_U32 cou32AppId,
       *              const tenDiPOAppType coenDiPOAppType,
       *              tenErrorCode enErrorCode,
       *              const trUserContext& rfrCUsrCntxt)
       * \brief  To send the Terminate applications response to the registered 
       *          classes
       * \param  coenCompID       : Uniquely identifies the target Device.
       * \param  cou32DevId       : Unique Device Id
       * \param  cou32AppId       : Application Id
       * \param  coenDiPOAppType  : DiPo App Type
       * \param  enErrorCode      : Error code
       * \param  rfrCUsrCntxt     : User Context
       **************************************************************************/
       t_Void vPostLaunchAppResult(const tenCompID coenCompID,
          const t_U32 cou32DevId,
          const t_U32 cou32AppId,
          const tenDiPOAppType coenDiPOAppType,
          tenErrorCode enErrorCode,
          const trUserContext& rfrCUsrCntxt);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclMediator::vPostTerminateAppResult
       ***************************************************************************/
      /*!
       * \fn     vPostTerminateAppResult(
       *              cons tenCompID coenCompID
       *              const t_U32 cou32DevId,
       *              const t_U32 cou32AppId,
       *              tenErrorCode enErrorCode,
       *              const trUserContext& rfrCUsrCntxt)
       * \brief  To send the Terminate applications response to the registered 
       *          classes
       * \param  coenCompID       : Uniquely identifies the target Device.
       * \param  cou32DevId       : Unique Device Id
       * \param  cou32AppId       : Application Id
       * \param  enErrorCode      : Error code
       * \param  rfrCUsrCntxt     : User Context
       **************************************************************************/
       t_Void vPostTerminateAppResult(const tenCompID coenCompID,
          const t_U32 cou32DevId,
          const t_U32 cou32AppId,
          tenErrorCode enErrorCode,
          const trUserContext& rfrCUsrCntxt);

       /***************************************************************************
        ** FUNCTION:  spi_tclMediator::vApplySelectionStrategy
        ***************************************************************************/
       /*!
        * \fn     vApplySelectionStrategy()
        * \brief  Called to trigger device selection automatically
        **************************************************************************/
       t_Void vApplySelectionStrategy();

       /***************************************************************************
        ** FUNCTION:  spi_tclMediator::vPostBTDeviceName
        ***************************************************************************/
       /*!
        * \fn     vPostBTDeviceName()
        * \brief  Called by SPI component when BT device name of active projection
        *         device becomes available
        * \param  u32DeviceHandle: Unique handle of projection device
        * \param  szBTDeviceName: BT Device name of projection device
        **************************************************************************/
       t_Void vPostBTDeviceName(t_U32 u32DeviceHandle, t_String szBTDeviceName);

      //! Generic singleton class is made friend so that the constructor is accessible
      friend class GenericSingleton<spi_tclMediator> ;
   private:

      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclMediator::spi_tclMediator
       ***************************************************************************/
      /*!
       * \fn     spi_tclMediator()
       * \brief  Default Constructor
       **************************************************************************/
      spi_tclMediator();

      //! Structure to hold select device callbacks
      std::vector<trSelectDeviceCallbacks> m_vecrSelDevCbs;

      //! AppLauncher callbacks
      trAppLauncherCallbacks m_rAppLauncherCb;

      //! DayNightMode callback
      trAAPSensorCallback m_rDayNightCb;

      //! Connection manager callbacks
      trConnMngrCallback m_rConnMngrCb;
};

#endif /* SPI_TCLMEDIATOR_H_ */
