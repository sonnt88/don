/***********************************************************************/
/*!
* \file  spi_tclAppLauncher.h
* \brief Class Responsible for Launching & Terminating Applications
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class Responsible for Launching & Terminating Applications
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAPPLAUNCHER_H_
#define _SPI_TCLAPPLAUNCHER_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclAppMngr.h"
#include "spi_tclAppLauncherRespIntf.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclAppLauncher
* \brief Class Responsible for Launching & Terminating Applications
****************************************************************************/
class spi_tclAppLauncher
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAppLauncher::spi_tclAppLauncher()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppLauncher(spi_tclAppLauncherRespIntf* poAppLauncherRespIntf)
   * \brief   Parameterized Constructor
   * \sa      ~spi_tclAppLauncher()
   **************************************************************************/
   spi_tclAppLauncher(spi_tclAppLauncherRespIntf* poAppLauncherRespIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclAppLauncher::~spi_tclAppLauncher()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclAppLauncher()
   * \brief   Destructor
   * \sa      spi_tclAppLauncher(spi_tclAppLauncherRespIntf* poAppLauncherRespIntf)
   **************************************************************************/
   virtual ~spi_tclAppLauncher();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppLauncher::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn     vLaunchApp(t_U32 u32DevId, 
   *            t_U32 u32AppId, tenDiPOAppType enDiPOAppType, t_String szTelephoneNumber, 
   *            tenEcnrSetting enEcnrSetting, const trUserContext& rcUsrCntxt)
   * \brief  It launches a remote application from the selected Mirror Link device.
   *         DeviceHandle (Unsigned Long) - Uniquely identifies the target Device.
   * \param  [IN] u32DevId  : Uniquely identifies the target Device.
   * \param  [IN] u32AppId : Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface. 
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
   *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] szTelephoneNumber : Number to be dialed if the DiPO application to be launched 
   *              is a phone application. If not valid to be used, this will be set to NULL, 
   *              zero length string. Will not be used if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] enEcnrSetting : Sets voice or server echo cancellation and noise reduction 
   *              settings if the DiPO application to be launched is a phone application. 
   *              If not valid to be used, this will be set to ECNR_NOCHANGE.
   * \param  [IN] rcUsrCntxt    : User Context Details.
   * \sa    spi_tclRespInterface::vPostLauchAppResult
   **************************************************************************/
   t_Void vLaunchApp(t_U32 u32DevId, 
      t_U32 u32AppId, 
      tenDiPOAppType enDiPOAppType, 
      t_String szTelephoneNumber, 
      tenEcnrSetting enEcnrSetting, 
      const trUserContext& rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMediator::vCbLaunchAppResult
   ***************************************************************************/
   /*!
   * \fn     vCbLaunchAppResult(
   *              cons tenCompID coenCompID
   *              const t_U32 cou32DevId,
   *              const t_U32 cou32AppId,
   *              tenErrorCode enErrorCode,
   *              const trUserContext& rfrCUsrCntxt)
   * \brief  To send the Terminate applications response to the registered 
   *          classes
   * \param  coenCompID       : Uniquely identifies the target Device.
   * \param  cou32DevId       : Unique Device Id
   * \param  cou32AppId       : Application Id
   * \param  enErrorCode      : Error code
   * \param  rfrCUsrCntxt     : User Context
   **************************************************************************/
   t_Void vCbLaunchAppResult(const tenCompID coenCompID,
      const t_U32 cou32DevId,
      const t_U32 cou32AppId,
      const tenDiPOAppType,
      tenErrorCode enErrorCode,
      const trUserContext& rfrCUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppLauncher::vTerminateApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vTerminateApp(const t_U32 cou32DeviceId,const t_U32 cou32AppId,
   *           ,const trUserContext& corfrUsrCntxt)
   * \brief   To Terminate an Application asynchronously.
   * \param   cou32DeviceId  : [IN] Device Id
   * \param   cou32AppId     : [IN] Application Id
   * \param   corfrUsrCntxt  : [IN] User context
   * \retval  t_Void
   * \sa      vLaunchApp()
   **************************************************************************/
   t_Void vTerminateApp(const t_U32 cou32DeviceId,
      const t_U32 cou32AppId,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclMediator::vCbTerminateAppResult
   ***************************************************************************/
   /*!
   * \fn     vCbTerminateAppResult(
   *              cons tenCompID coenCompID
   *              const t_U32 cou32DevId,
   *              const t_U32 cou32AppId,
   *              tenErrorCode enErrorCode,
   *              const trUserContext& rfrCUsrCntxt)
   * \brief  To send the Terminate applications response to the registered 
   *          classes
   * \param  coenCompID       : Uniquely identifies the target Device.
   * \param  cou32DevId       : Unique Device Id
   * \param  cou32AppId       : Application Id
   * \param  enErrorCode      : Error code
   * \param  rfrCUsrCntxt     : User Context
   **************************************************************************/
   t_Void vCbTerminateAppResult(const tenCompID coenCompID,
      const t_U32 cou32DevId,
      const t_U32 cou32AppId,
      tenErrorCode enErrorCode,
      const trUserContext& rfrCUsrCntxt);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/
private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

      /***************************************************************************
   ** FUNCTION:  spi_tclAppLauncher::~spi_tclAppLauncher()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppLauncher()
   * \brief   Constructor
   * \sa      ~spi_tclAppLauncher()
   **************************************************************************/
   spi_tclAppLauncher();

   /***************************************************************************
   ** FUNCTION:  spi_tclAppLauncher::vRegisterCallbacks
   ***************************************************************************/
   /*!
   * \fn     vRegisterCallbacks(const trAppLauncherCallbacks &corfrAppLauncherCb)
   * \brief  To register with mediator for asynchronous responses
   **************************************************************************/
   t_Void vRegisterCallbacks();

   //@todo - Maintain the List of Launched Applications Info
   //& terminate when the device is dis selected

   //!AppLauncher Resp Interface
   spi_tclAppLauncherRespIntf *m_poAppLauncherRespIntf;

  /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};//spi_tclAppLauncher


#endif // _SPI_TCLAPPLAUNCHER_H_ 
