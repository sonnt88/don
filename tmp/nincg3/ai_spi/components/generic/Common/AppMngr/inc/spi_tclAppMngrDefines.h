
/*!
 *******************************************************************************
 * \file    spi_tclAppMngrDefines.h
 * \brief   SPI App Mngr Defines
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   SPI App Mngr Defines
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date        |  Author               | Modifications
 16.02.2014  | Shiva Kumar Gurija    | Initial Version
 23.04.2014  | Shiva Kumar Gurija    | Updated with Notifications Impl
 26.02.2016  | Rachana L Achar       | AAP Navigation implementation
 10.03.2016  | Rachana L Achar       | AAP Notification implementation

 \endverbatim
 ******************************************************************************/
#ifndef _SPI_TCLAPPMNGRDEFINES_H_
#define _SPI_TCLAPPMNGRDEFINES_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <functional>
#include "AAPTypes.h"

struct trNavStatusData;
struct trNavNextTurnData;
struct trNavNextTurnDistanceData;
struct trNotificationData;

/*!
* \typedef - To Update the change in Application List
*/
typedef std::function<t_Void(const t_U32,tenAppStatusInfo)> tNotifyAppListChange;
/*!
* \typedef - To Update the App Icon data
*/
typedef std::function<t_Void(tenIconMimeType,const t_U8*,t_U32,const trUserContext&) > tNotifyAppIconData;
/*!
* \typedef - To Update the Terminate App Response
*/
typedef std::function<t_Void(const t_U32,const t_U32,tenErrorCode ,
                             const trUserContext&)> tNotifyTerminateAppResult;
/*!
* \typedef - To Update the Launch App Response
*/
typedef std::function<t_Void(const t_U32,const t_U32,const tenDiPOAppType,tenErrorCode ,
                             const trUserContext&)> tNotifyLaunchAppResult;

/*!
* \typedef - To send the select device response
*/
typedef std::function<t_Void(t_Bool)> tNotifySelectDeviceResult;

/*!
* \typedef - To send the select device response
*/
typedef std::function<t_Void(t_U32, t_Bool, tenDisplayContext, const trUserContext)> tPostDeviceDisplayContext;

/*!
* \typedef - To send the response of EnableNotifications request & Invoke Notification Action request
*/
typedef std::function<t_Void(t_U32, tenErrorCode,const trUserContext)> tNotifyNotificationResult;

/*!
* \typedef - To send the Notifications data to HMI
*/
typedef std::function<t_Void(t_U32,const trNotiData)> tNotificationEventInfo;

/*!
* \typedef - To update the Session Status update
*/
typedef std::function<t_Void(const t_U32,const tenDeviceCategory,const tenSessionStatus)> tNotifySessionStatus;


/*!
* \typedef - To update the Active Application Info
*/
typedef std::function<t_Void(const t_U32,const tenDeviceCategory,
                             const t_U32, const tenAppCertificationInfo)> tNotifyActiveAppInfo;

/*!
* \typedef - To update the Navigation status
*/
typedef std::function<t_Void(trNavStatusData)> tvUpdateNavStatusData;

/*!
* \typedef - To update the Navigation next turn
*/
typedef std::function<t_Void(trNavNextTurnData)> tvUpdateNavNextTurnData;

/*!
* \typedef - To update the Navigation next turn distance
*/
typedef std::function<t_Void(trNavNextTurnDistanceData)> tvUpdateNavNextTurnDistanceData;

/*!
* \typedef - To update the notification information
*/
typedef std::function<t_Void(trNotificationData)> tvUpdateNotificationData;

/*!
* \typedef struct rAppMngrCallbacks
*/
typedef struct rAppMngrCallbacks
{
   tNotifyAppListChange         fpvNotifyAppListChangeCb;
   tNotifyAppIconData           fpvCbAppIconDataResult;
   tNotifyTerminateAppResult    fpvTerminateAppResult;
   tNotifyLaunchAppResult       fpvLaunchAppResult;
   tNotifySelectDeviceResult    fpvSelectDeviceResult;
   tPostDeviceDisplayContext    fpvPostDeviceDisplayContext;
   tNotifyNotificationResult    fpvEnableNotiResult;
   tNotifyNotificationResult    fpvInvokeNotiActionResult;
   tNotificationEventInfo       fpvNotifyNoticationInfo;
   tNotifySessionStatus         fpvNotifySessionStatus;
   tNotifyActiveAppInfo         fpvNotifyActiveAppInfo;
   tvUpdateNavStatusData        fvUpdateNavStatusData;
   tvUpdateNavNextTurnData      fvUpdateNavNextTurnData;
   tvUpdateNavNextTurnDistanceData fvUpdateNavNextTurnDistanceData;
   tvUpdateNotificationData     fvUpdateNotificationData;

   rAppMngrCallbacks():fpvNotifyAppListChangeCb(NULL),
      fpvCbAppIconDataResult(NULL),
      fpvTerminateAppResult(NULL),
      fpvLaunchAppResult(NULL),
      fpvSelectDeviceResult(NULL),
      fpvPostDeviceDisplayContext(NULL),
      fpvEnableNotiResult(NULL),
      fpvInvokeNotiActionResult(NULL),
      fpvNotifyNoticationInfo(NULL),
      fpvNotifySessionStatus(NULL),
      fpvNotifyActiveAppInfo(NULL),
      fvUpdateNavStatusData(NULL),
      fvUpdateNavNextTurnData(NULL),
      fvUpdateNavNextTurnDistanceData(NULL),
      fvUpdateNotificationData(NULL)
   {
      //add code
   }

}trAppMngrCallbacks;

//! \brief Structure holding the arguments for UpdateNavStatusData function
struct trNavStatusData
{
   t_U32 u32DeviceHandle;
   tenDeviceCategory enDeviceCategory;
   tenNavAppState enNavAppState;
   
   trNavStatusData():u32DeviceHandle(0),
      enDeviceCategory(e8DEV_TYPE_UNKNOWN),
      enNavAppState(e8SPI_NAV_UNKNOWN)
   {
   }
};

//! \brief Structure holding the arguments for UpdateNavNextTurnData function
struct trNavNextTurnData
{
   t_U32 u32DeviceHandle;
   tenDeviceCategory enDeviceCategory;
   t_String szRoadName;
   tenAAPNavNextTurnSide enAAPNavNextTurnSide;
   tenAAPNavNextTurnType enAAPNavNextTurnType;
   t_String szImage;
   t_S32 s32TurnAngle;
   t_S32 s32TurnNumber;
   
   trNavNextTurnData():u32DeviceHandle(0),
      enDeviceCategory(e8DEV_TYPE_UNKNOWN),
      szRoadName(""),
      enAAPNavNextTurnSide(e8_NAV_NEXT_TURN_UNSPECIFIED),
      enAAPNavNextTurnType(e8_NAV_NEXT_TURN_UNKNOWN),
      szImage(""),
      s32TurnAngle(-1),
      s32TurnNumber(-1)
   {
   }
};

//! \brief Structure holding the arguments for UpdateNavNextTurnDistanceData function
struct trNavNextTurnDistanceData
{
   t_U32 u32DeviceHandle;
   tenDeviceCategory enDeviceCategory;
   t_S32 s32Distance;
   t_S32 s32Time;
   
   trNavNextTurnDistanceData():u32DeviceHandle(0),
      enDeviceCategory(e8DEV_TYPE_UNKNOWN),
      s32Distance(-1),
      s32Time(-1)
   {
   }
};

//! \brief Structure holding the arguments for UpdateNotificationData function
struct trNotificationData
{
   t_U32 u32DeviceHandle;
   tenDeviceCategory enDeviceCategory;
   t_String szNotifText;
   t_Bool bHasId;
   t_String szId;
   t_Bool bHasIcon;
   t_U8 *pu8Icon;
   t_U32 u32IconSize;

   trNotificationData():u32DeviceHandle(0),
      enDeviceCategory(e8DEV_TYPE_UNKNOWN),
      szNotifText(""),
      bHasId(false),
      szId(""),
      bHasIcon(false),
      pu8Icon(NULL),
      u32IconSize(0)
   {
   }
};

#endif //_SPI_TCLAPPMNGRDEFINES_H_
