/***********************************************************************/
/*!
* \file  spi_tclAppSettings.h
* \brief Class to get the App Mngr Settings
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the App Mngr Settings
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version
23.04.2014  | Shiva Kumar Gurija    | Updated with the elements for Notifications&
                                       CTS Testing
29.04.2014  | Shiva Kumar Gurija    | Changes for Locale Mapping
13.05.2014  | Ramya Murthy          | Implemented persistent Notification on/off 
                                      setting using Datapool.

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAPPMNGRSETTINGS_H_
#define _SPI_TCLAPPMNGRSETTINGS_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "GenericSingleton.h"
#include "Xmlable.h"
#include "Lock.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/*!
* \typedef struct rLocaleMap
* \brief This is used to map the Region enumeration value to the respective 
* string that we need to look for in the Application certification locale lists
*/
typedef struct rLocaleMap
{
   //! Internally defined ID to map the Enum value to the county code.
   tenRegion enRegion;
   //! Region name which is a part of Restricted/Non restricted list
   t_String szRegion;
}trLocaleMap;


/****************************************************************************/
/*!
* \class spi_tclAppSettings
* \brief Class to get the Video Layer Info
****************************************************************************/
class spi_tclAppSettings:public GenericSingleton<spi_tclAppSettings>,public shl::xml::tclXmlReadable
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAppSettings::~spi_tclAppSettings()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAppSettings()
   * \brief   Destructor
   * \sa      spi_tclAppSettings()
   **************************************************************************/
   ~spi_tclAppSettings();

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclAppSettings::szGetCCCMemberString()
   ***************************************************************************/
   /*!
   * \fn     t_String szGetCCCMemberString()
   * \brief  To get the CCC Member string
   * \retval t_String
   **************************************************************************/
   t_String szGetCCCMemberString();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppSettings::bNonCertAppsReq()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bNonCertAppsReq()
   * \brief  To get whether the Non certified Applications are required  
   *          when the application certification is enabled for the
   *          particular version of the device.
   * \retval t_Bool
   **************************************************************************/
   t_Bool bNonCertAppsReq();


   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppSettings::bFilterByLocale()
   ***************************************************************************/
   /*!
   * \fn     t_Void bFilterByLocale()
   * \brief  To Get whether Location based filtering is required
   * \retval t_Bool
   **************************************************************************/
   t_Bool bFilterByLocale();

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclAppSettings::szGetRegion()
   ***************************************************************************/
   /*!
   * \fn     t_String szGetRegion()
   * \brief  To Get the which regions CCC guidelines should be followed for the
   *          Application certification.
   * \retval t_String
   **************************************************************************/
   t_String szGetRegion();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppSettings::bEnableContentBlocking()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bEnableContentBlocking()
   * \brief  To get whether the Content Blocking is required
   * \retval t_Bool
   **************************************************************************/
   t_Bool bEnableContentBlocking();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppSettings::bEnableAppCertification()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bEnableAppCertification(const trVersionInfo& corfrVersionInfo)
   * \brief  To get whether the Application certifcation required for the 
   *          given ML version of the device.
   * \param  corfrVersionInfo : [IN] Device Version Info
   * \retval t_Bool
   **************************************************************************/
   t_Bool bEnableAppCertification(const trVersionInfo& corfrVersionInfo);
   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppSettings::vReadAppSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vReadAppSettings()
   * \brief  To read the settings from XML
   * \retval t_Void
   **************************************************************************/
   t_Void vReadAppSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppSettings::vDisplayAppSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vDisplayAppSettings()
   * \brief  To print the settings
   * \retval t_Void
   **************************************************************************/
   t_Void vDisplayAppSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppSettings::vGetIconPreferences()
   ***************************************************************************/
   /*!
   * \fn     t_Void vGetIconPreferences(trIconAttributes& rfrIconAttr)
   * \brief  To Get the Icon preferences of HMI Client
   * \param  rfrIconAttr : [IN] - Icon Preferences
   * \retval t_Void
   **************************************************************************/
   t_Void vGetIconPreferences(trIconAttributes& rfrIconAttr);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclAppSettings::bEnableCTSTest()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bEnableCTSTest()
   * \brief  To get whether the CTS test is enabled.
   * \retval t_Bool  TRUE- Enabled FALSE-Disabled
   **************************************************************************/
   t_Bool bEnableCTSTest();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppSettings::vSetMLNotificationOnOff()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
   * \brief  To Set the Notifications to On/Off
   * \param  bSetNotificationsOn : [IN] True-Set Notifications to ON
   *                                    False - Set Notifications to OFF
   * \retval t_Void 
   **************************************************************************/
   t_Void vSetMLNotificationOnOff(t_Bool bSetNotificationsOn);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclAppSettings::bGetMLNotificationEnabledInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bGetMLNotificationEnabledInfo()
   * \brief  Provides information on whether ML Notifications are enabled or
   *         disabled.
   * \retval t_Bool  TRUE- Enabled FALSE-Disabled
   **************************************************************************/
   t_Bool bGetMLNotificationEnabledInfo();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppSettings::vSetRegion(.)
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetRegion(tenRegion enRegion)
   * \brief  Interface to set the region for application certification.
   *         It gives the info of which region CCC Guidelines should be followed
   *         for the Application Certification Filtering
   * \param  [IN] enRegion : Region enumeration
   * \sa
   **************************************************************************/
   t_Void vSetRegion(tenRegion enRegion);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppSettings::vEnableTestApps()
   ***************************************************************************/
   /*!
   * \fn     t_Void vEnableTestApps(t_Bool bEnable)
   * \brief  method to enable test&certification apps (NON UI cat).
   * \param  bEnable       : [IN] TRUE-allows to display Test apps on HU
   *                              FALSE - doesn't allow
   * \retval t_Void
   **************************************************************************/
   t_Void vEnableTestApps(t_Bool bEnable);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclAppSettings::bEnableTestApps()
   ***************************************************************************/
   /*!
   * \fn     t_Void bEnableTestApps(t_Bool bEnable)
   * \brief  method to tell whether test&certification apps (NON UI cat).needs 
   *         to be enabled
   * \retval t_Bool
   **************************************************************************/
   t_Bool bEnableTestApps();


   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<spi_tclAppSettings>;


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAppSettings::spi_tclAppSettings()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppSettings()
   * \brief   Default Constructor
   * \sa      ~spi_tclAppSettings()
   **************************************************************************/
   spi_tclAppSettings();

   /***************************************************************************
   ** FUNCTION:  spi_tclAppSettings& spi_tclAppSettings::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppSettings& operator= (const spi_tclAppSettings &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclAppSettings& operator= (const spi_tclAppSettings &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAppSettings::spi_tclAppSettings(const ..
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppSettings(const spi_tclAppSettings &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclAppSettings(const spi_tclAppSettings &corfrSrc);

   /*************************************************************************
   ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
   *************************************************************************/
   /*!
   * \fn     virtual bool bXmlReadNode(xmlNode *poNode)
   * \brief  virtual function to read data from a xml node
   * \param  poNode : [IN] pointer to xml node
   * \retval bool : true if success, false otherwise.
   *************************************************************************/
   virtual t_Bool bXmlReadNode(xmlNodePtr poNode);

   //! Region - It specifies, which regions guidelines should be followed for the 
   //  App certification
   t_String m_szRegion;

   //Lock object for Region
   Lock m_oRegionLock;

   //! FilterByLocale
   t_Bool m_bFilterByLocale;

   //!CCCMember
   t_String m_szCCCMember;

   //! EnableContentBlocking
   t_Bool m_bEnableContentBlocking;

   //! FilterNonCertApps
   t_Bool m_bNonCertAppsReq;

   //! Application certification required for ML1.0 device
   t_Bool m_bEnableML10AppCertification;

   //! Application certification required for ML1.1 device
   t_Bool m_bEnableML11AppCertification;

   //! To set the whether the CTS Testing is required or project setup
   t_Bool m_bEnableCTSTest;

   //! To set the whether the Certification & test apps to shown on HU or not
   t_Bool m_bDisplayTestApps;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclAppSettings

#endif //_SPI_TCLAPPMNGRSETTINGS_H_
