/***********************************************************************/
/*!
* \file  spi_tclAppMngr.h
* \brief Main Application Manager class that provides interface to delegate 
*        the execution of command and handle response
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version
20.03.2014  | Shihabudheen P M      | Added vCbPostDeviceDisplayContext()
23.04.2014  | Shiva Kumar Gurija    | Updated with Notifications Impl
06.04.2014  | Ramya Murthy          | Initialisation sequence implementation
31.07.2014  | Ramya Murthy          | SPI feature configuration via LoadSettings()
26.02.2016  | Rachana L Achar       | AAP Navigation implementation
10.03.2016  | Rachana L Achar       | AAP Notification implementation

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAPPMNMGR_
#define _SPI_TCLAPPMNMGR_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Lock.h"
#include "spi_tclLifeCycleIntf.h"
#include "spi_tclAppMngrDefines.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
class spi_tclAppMngrDev;
class spi_tclAppMngrRespIntf;

/****************************************************************************/
/*!
* \class spi_tclAppMngr
* \brief Main Application Manager class that provides interface to delegate 
*        the execution of command and handle response
****************************************************************************/
class spi_tclAppMngr : public spi_tclLifeCycleIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngr::spi_tclAppMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppMngr(spi_tclAppMngrRespIntf* poRespIntf)
   * \brief   Parameterized Constructor
   * \sa      ~spi_tclAppMngr()
   **************************************************************************/
   spi_tclAppMngr(spi_tclAppMngrRespIntf* poRespIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngr::~spi_tclAppMngr()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclAppMngr()
   * \brief   Destructor
   * \sa      spi_tclAppMngr(spi_tclAppMngrRespIntf* poRespIntf)
   **************************************************************************/
   ~spi_tclAppMngr();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAppMngr::bInitialize()
    ***************************************************************************/
   /*!
    * \fn      bInitialize()
    * \brief   Method to Initialize
    * \sa      bUnInitialize()
    **************************************************************************/
   virtual t_Bool bInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAppMngr::bUnInitialize()
    ***************************************************************************/
   /*!
    * \fn      bUnInitialize()
    * \brief   Method to UnInitialize
    * \sa      bInitialize()
    **************************************************************************/
   virtual t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAppMngr::vLoadSettings(const trSpiFeatureSupport&...)
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
    * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclAppMngr::vSaveSettings()
    ***************************************************************************/
   /*!
    * \fn      vSaveSettings()
    * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
    * \sa      vLoadSettings()
    **************************************************************************/
   virtual t_Void vSaveSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSelectDevice(const t_U32 cou32DevId,
   *                 tenDeviceConnectionType enDevConnType,
   *                 const tenDeviceConnectionReq coenConnReq, 
   *                 tenDeviceCategory enDevCat,
   *                 const trUserContext& rfrcUsrCntxt)
   * \brief   To setup App Mngr related info when a device is selected or
   *          dis selected.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   enDevConnType : [IN] Device connection Type USB/WIFI
   * \pram    coenConnReq : [IN] Identifies the Connection Type.
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \pram    rfrcUsrCntxt: [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId,
      tenDeviceConnectionType enDevConnType,
      const tenDeviceConnectionReq coenConnReq,
      tenDeviceCategory enDevCat,
      const trUserContext& rfrcUsrCntxt);


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchApp(const t_U32 cou32DevId, 
   *           tenDeviceCategory enDevCat, 
   *           t_U32 u32AppHandle, 
   *           tenDiPOAppType enDiPOAppType, 
   *           t_String szTelephoneNumber, 
   *           tenEcnrSetting enEcnrSetting)
   * \brief   To Launch the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param  [IN] enDevCat : Device Type Information(Mirror Link/DiPO).
   * \param  [IN] u32AppHandle : Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface. 
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
   *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] szTelephoneNumber : Number to be dialed if the DiPO application to be launched 
   *              is a phone application. If not valid to be used, this will be set to NULL, 
   *              zero length string. Will not be used if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] enEcnrSetting : Sets voice or server echo cancellation and noise reduction 
   *              settings if the DiPO application to be launched is a phone application. 
   *              If not valid to be used, this will be set to ECNR_NOCHANGE.
   * \retval  t_Void
   * \sa      vTerminateApp()
   **************************************************************************/
   t_Void vLaunchApp(const t_U32 cou32DevId, 
      tenDeviceCategory enDevCat, 
      t_U32 u32AppHandle, 
      tenDiPOAppType enDiPOAppType, 
      t_String szTelephoneNumber, 
      tenEcnrSetting enEcnrSetting,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchApp(const t_U32 cou32DevId, 
   *           tenDeviceCategory enDevCat,
   *           t_U32 u32AppHandle, 
   *           const trUserContext& rfrcUsrCntxt)
   * \brief   To Launch the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   u32AppHandle : [IN]  Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface. 
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   *          rfrcUsrCntxt : [IN] user Context
   * \retval  t_Void
   * \sa      vTerminateApp()
   **************************************************************************/
   t_Void vLaunchApp(const t_U32 cou32DevId, 
      tenDeviceCategory enDevCat, 
      t_U32 u32AppHandle, 
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vTerminateApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vTerminateApp(const t_U32 cou32DevId,
   *                 const t_U32 cou32AppId,
   *                 tenDeviceCategory enDevCat,
   *                 const trUserContext& rfrcUsrCntxt)
   * \brief   To Terminate an Application asynchronously.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    cou32AppId  : [IN] Application Id
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \param   rUserContext : [IN] Context Message
   * \retval  t_Void
   * \sa      bLaunchApp()
   **************************************************************************/
   t_Void vTerminateApp(const t_U32 cou32DevId, 
      const t_U32 cou32AppId,
      tenDeviceCategory enDevCat,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vGetAppDetailsList()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetDeviceInfoList(t_U32 u32DevId,
   *         t_U32& u32NumAppInfoList,
   *         std::vector<trAppDetails>& corfvecrAppDetailsList);
   * \brief  It provides a list of UI applications supported by a device.
   * \param  u32DevId  : [IN] Device Handle
   * \param  u32NumAppInfoList : [OUT] Number of Applications in the List
   * \param  corfvecrAppDetailsList : [OUT] List of applicationinfo's of the 
   *                                 Applications supported by the device
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppDetailsList(const t_U32 u32DeviceHandle,
      t_U32& u32NumAppInfoList,
      std::vector<trAppDetails>& corfvecrAppDetailsList, 
      tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vGetAppInfo()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetAppInfo(const t_U32 cou32DevId, 
   *         const t_U32 cou32AppId,
   *         tenDeviceCategory enDevCat,
   *         trAppDetails& rfrAppDetails)
   * \brief  To Get an applications info
   * \param    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param    cou32AppId  : [IN] Application Id
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \param    rfrAppDetails : [OUT]App  Info
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppInfo(const t_U32 cou32DevId, 
      const t_U32 cou32AppId,
      tenDeviceCategory enDevCat,
      trAppDetails& rfrAppDetails);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vGetAppIconData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetAppIconData(t_String szAppIconUrl, 
   *         tenDeviceCategory enDevCat, 
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  szAppIconUrl  : [IN] Application Icon URL
   * \pram   enDevCat    : [IN] Identifies the Connection Category
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppIconData(t_String szAppIconUrl,
      tenDeviceCategory enDevCat,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vSetAppIconAttributes()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetAppIconAttributes(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
   *            tenDeviceCategory enDevCat, const IconAttributes &rfrIconAttributes, 
   *            const trUserContext& rfrcUsrCntxt);
   * \brief  sets application icon attributes for retrieval of application icons.
   * \param  u32DeviceHandle      :  [IN] Uniquely identifies the target Device.
   * \param  u32ApplicationHandle : [IN] Uniquely identifies an application on the 
   *              target device. This value will be obtained from GetAppList Interface.
   * \pram   enDevCat            : [IN] Identifies the Connection Category
   * \param  rfrIconAttributes : [IN] Icon details.
   * \param  rfrcUsrCntxt      : [IN]  User Context Details.
   **************************************************************************/
   t_Void vSetAppIconAttributes(t_U32 u32DeviceHandle,
      t_U32 u32AppHandle,
      tenDeviceCategory enDevCat,
      const trIconAttributes &rfrIconAttributes,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
   *          t_Bool bSetConfig,const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the Vehicle configurations.
   * \param  [IN] enVehicleConfig :  Identifies the Vehicle Configuration.
   * \param  [IN] bSetConfig      : Enable/Disable config
   **************************************************************************/
   t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vOnVehicleData(const trVehicleData
                                     rfrcVehicleData)
   ***************************************************************************/
   /*!
   * \fn      vOnVehicleData(const trVehicleData rfrcVehicleData)
   * \brief   Method to receive Vehicle data.
   * \param   rfrcVehicleData: [IN] Vehicle data
   * \param   bSolicited: [IN] True if the data update is for changed vehicle data, else False
   * \retval  None
   ***************************************************************************/
   t_Void vOnVehicleData(const trVehicleData rfrcVehicleData, t_Bool bSolicited);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vCbNotifyAppListChange()
   ***************************************************************************/
   /*!
   * \fn    t_Void vCbNotifyAppListChange(const t_U32 cou32DevId,
   *          tenAppStatusInfo enAppStatus)
   * \brief  To Update HMI about the change in Application list
   * \param  cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param enAppStatus : [IN] informs whther teh app list changed or default vale
   * \retval  t_Void
   **************************************************************************/
   t_Void vCbNotifyAppListChange(const t_U32 cou32DevId,
      tenAppStatusInfo enAppStatus);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vCbAppIconDataResult()
   ***************************************************************************/
   /*!
   * \fn    t_Void vCbAppIconDataResult(tenIconMimeType enMimeType,
   *          const t_U8* pcu8AppIconData,t_U32 u32Len,
   *          const trUserContext& rfrcUsrCntxt)
   * \brief   Method to send the Get App Icon data response to HMI
   * \param  enMimeType  : [IN] Icon MIME Type
   * \param  pcu8AppIconData  : [IN] Image stream
   * \param  u32Len   : [IN] data stream length
   * \param  rfrcUsrCntxt  : [IN] User context
   * \retval  t_Void
   **************************************************************************/
   t_Void vCbAppIconDataResult(tenIconMimeType enMimeType,
      const t_U8* pcu8AppIconData, 
      t_U32 u32Len,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vCbTerminateAppResult
   ***************************************************************************/
   /*!
   * \fn     vCbTerminateAppResult(
   *              const t_U32 cou32DevId,
   *              const t_U32 cou32AppId,
   *              tenErrorCode enErrorCode,
   *              const trUserContext& rfrCUsrCntxt)
   * \brief  To send the Terminate applications response to the registered 
   *          classes
   * \param  cou32DevId       : Unique Device Id
   * \param  cou32AppId       : Application Id
   * \param  enErrorCode      : Error code
   * \param  rfrCUsrCntxt     : User Context
   **************************************************************************/
   t_Void vCbTerminateAppResult(
      const t_U32 cou32DevId,
      const t_U32 cou32AppId,
      tenErrorCode enErrorCode, 
      const trUserContext& rfrCUsrCntxt);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vCbLaunchAppResult
   ***************************************************************************/
   /*!
   * \fn     vCbLaunchAppResult(
   *              const t_U32 cou32DevId,
   *              const t_U32 cou32AppId,
   *              const tenDiPOAppType,
   *              tenErrorCode enErrorCode,
   *              const trUserContext& rfrCUsrCntxt)
   * \brief  To send the Terminate applications response to the registered 
   *          classes
   * \param  cou32DevId       : Unique Device Id
   * \param  cou32AppId       : Application Id
   * \param  tenDiPOAppType   : Dipo App Type
   * \param  enErrorCode      : Error code
   * \param  rfrCUsrCntxt     : User Context
   **************************************************************************/
   t_Void vCbLaunchAppResult(
      const t_U32 cou32DevId,
      const t_U32 cou32AppId,
      const tenDiPOAppType,
      tenErrorCode enErrorCode, 
      const trUserContext& rfrCUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vCbSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn    t_Void vCbSelectDeviceResult(t_Bool bResult)
   * \brief   Method to send the select device result
   * \param  bResult  : [IN] result
   * \retval  t_Void
   **************************************************************************/
   t_Void vCbSelectDeviceResult(t_Bool bResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vDisplayAllAppsInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDisplayAllAppsInfo(const t_U32 cou32DevId,
   *            tenDeviceCategory enDevCat)
   * \brief   To Display all applications info
   * \param   cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \retval  t_Void
   **************************************************************************/
   t_Void vDisplayAllAppsInfo(const t_U32 cou32DevId,tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION:  t_U32 spi_tclAppMngr::u32GetSelectedDevice()()
   ***************************************************************************/
   /*!
   * \fn      t_U32 u32GetSelectedDevice()
   * \brief   To get the currently selected device. This is used to determine whether 
   *           the launch/terminate app has recieved for the selected device or not
   * \retval  t_U32
   **************************************************************************/
   t_U32 u32GetSelectedDevice();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppMngr::bCheckAppValidity()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bCheckAppValidity(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId,tenDeviceCategory enDevCat)
   * \brief   To check whether the application exists or not
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId  : [IN] Application Id
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bCheckAppValidity(const t_U32 cou32DevId, 
      const t_U32 cou32AppId,
      tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vDisplayAppcertificationInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId,tenDeviceCategory enDevCat)
   * \brief   To disaply th eapplication certification info 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId  : [IN] Application Id
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \retval  t_Void
   **************************************************************************/
   t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevId,
      const t_U32 cou32AppId,tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode, 
   *                 tenDeviceCategory enDevCat)
   * \brief   To perform the actions that are required, after the select device is
   *           successful
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \pram    enDevCat    : [IN] Device Category. ML/DiPo
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode,
      tenDeviceCategory enDevCat);


   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppMngr::bSetClientProfile()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bSetClientProfile(const t_U32 cou32DevHandle,
   *             const trClientProfile& corfrClientProfile, 
   *             tenDeviceCategory enDevCat
   * \brief   To set the client profiles
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   corfrClientProfile  : [IN] Client Profile to be set
   * \pram    enDevCat    : [IN] Device Category. ML/DiPo
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bSetClientProfile(const t_U32 cou32DevId,
      const trClientProfile& corfrClientProfile,
      tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngr::vSetVehicleBTAddress()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetVehicleBTAddress(const t_U32 cou32DevHandle,
   *             t_String szBTAddress, 
   *             tenDeviceCategory enDevCat,
   *             const trUserContext& corfrUsrCntxt)
   * \brief   To set the ML Client Blue tooth address
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   szBTAddress  : [IN] 12-bit BT Address
   * \pram    enDevCat    : [IN] Device Category. ML/DiPo
   * \param   corfrUsrCntxt : [IN] User context
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetVehicleBTAddress(const t_U32 cou32DevId,
      t_String szBTAddress,
      tenDeviceCategory enDevCat,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vSetMLNotificationEnabledInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetMLNotificationEnabledInfo(const t_U32 cou32DevId,
   *            t_U16 u16NumNotiEnableList,
   *            std::vector<NotificationEnable> vecrNotiEnableList,
   *            tenDeviceCategory enDevCat,
   *            const trUserContext corfrUsrCntxt)
   * \brief  Interface to set the device notification preference for
   *         applications.
   *         Set u32DeviceHandle to 0xFFFF, to indicate all devices.
   *         If notification for all the applications has to be:
   *         Enabled - Set NumNotificationEnableList to 0xFFFF
   *                   and NotificationEnableList should be empty.
   *         Disabled - Set NumNotificationEnableList to 0x00.
   *                   and NotificationEnableList should be empty.
   * \param  cou32DevId           : [IN] Uniquely identifies the Device.
   * \param  u16NumNotiEnableList : [IN] Total number of records in
   *              NotificationEnableList.
   * \param  vecrNotiEnableList   : [IN] List of NotificationEnable records.
   * \param  enDevCat             : [IN] Device Category. ML/DiPo 
   * \param  corfrUsrCntxt        : [IN] User Context Details.
   **************************************************************************/
   t_Void vSetMLNotificationEnabledInfo(const t_U32 cou32DevId,
      t_U16 u16NumNotiEnableList,
      std::vector<trNotiEnable> vecrNotiEnableList,
      tenDeviceCategory enDevCat,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vInvokeNotificationAction()
   ***************************************************************************/
   /*!
   * \fn     t_Void vInvokeNotificationAction(const t_U32 cou32DevId,
   *            t_U32 u32NotificationID, 
   *            t_U32 u32NotificationActionID,
   *            tenDeviceCategory enDevCat,
   *            const trUserContext& corfrUsrCntxt)
   * \brief  Interface to invoke the respective action for the received
   *         Notification Event.
   * \param  cou32DevId        : [IN]  Uniquely identifies the target Device.
   * \param  u32NotificationID : [IN]  Notification Identifier.
   * \param  u32NotificationActionID : [IN]  Notification action Identifier.
   * \param  enDevCat          : [IN] Device Category. ML/DiPo 
   * \param  corfrUsrCntxt     : [IN]  User Context Details.
   **************************************************************************/
   t_Void vInvokeNotificationAction(const t_U32 cou32DevId,
      t_U32 u32NotificationID,
      t_U32 u32NotificationActionID,
      tenDeviceCategory enDevCat,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vSetMLNotificationOnOff()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
   * \brief  To Set the Notifications to On/Off
   * \param  bSetNotificationsOn : [IN] True-Set Notifications to ON
   *                                    False - Set Notifications to OFF
   * \retval t_Void 
   **************************************************************************/
   t_Void vSetMLNotificationOnOff(t_Bool bSetNotificationsOn);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclAppMngr::bGetMLNotificationEnabledInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bGetMLNotificationEnabledInfo()
   * \brief  Provides information on whether ML Notifications are enabled or
   *         disabled.
   * \retval t_Bool  TRUE- Enabled FALSE-Disabled
   **************************************************************************/
   t_Bool bGetMLNotificationEnabledInfo();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vSetRegion(.)
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetRegion(tenRegion enRegion)
   * \brief  Interface to set the region for application certification.
   *         It gives the info of which region CCC Guidelines should be followed
   *         for the Application Certification Filtering
   * \param  enRegion : [IN] Region enumeration
   * \retval t_Void
   **************************************************************************/
   t_Void vSetRegion(tenRegion enRegion);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vDisplayAppListXml()
   ***************************************************************************/
   /*!
   * \fn     t_Void vDisplayAppListXml(const t_U32 cou32DevId,
   *              tenDeviceCategory enDevCat)
   * \brief  Method to retrieve the Applications XML Buffer of the device
   *          This is ML specific
   * \param  cou32DevId : [IN] Device ID
   * \param  enDevCat   : [IN] Device Category. ML/DiPo 
   * \retval t_Void
   **************************************************************************/
   t_Void vDisplayAppListXml(const t_U32 cou32DevId,
      tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vCbPostSetNotiEnabledInfoResult
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbPostSetNotiEnabledInfoResult(
   *              const t_U32 cou32DevId,
   *              tenErrorCode enErrorCode,
   *              const trUserContext& corfrCUsrCntxt)
   * \brief  To send the Set Notification Enabled Info to HMI 
   * \param  cou32DevId       : [IN]  Unique Device Id
   * \param  enErrorCode      : [IN]  Error code
   * \param  corfrCUsrCntxt   : [IN]  User Context
   **************************************************************************/
   t_Void vCbPostSetNotiEnabledInfoResult(
      const t_U32 cou32DevId,
      tenErrorCode enErrorCode, 
      const trUserContext& corfrCUsrCntxt);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vCbPostInvokeNotificationActionResult
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbPostInvokeNotificationActionResult(
   *              const t_U32 cou32DevId,
   *              tenErrorCode enErrorCode,
   *              const trUserContext& corfrCUsrCntxt)
   * \brief  To send Invoke Notification Action Result to HMI
   * \param  cou32DevId       : [IN]  Unique Device Id
   * \param  enErrorCode      : [IN]  Error code
   * \param  corfrCUsrCntxt   : [IN]  User Context
   **************************************************************************/
   t_Void vCbPostInvokeNotificationActionResult(
      const t_U32 cou32DevId,
      tenErrorCode enErrorCode, 
      const trUserContext& corfrCUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vCbPostNotificationResult
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbPostNotificationResult(
   *              const t_U32 cou32DevId,
   *              const trNotiData& corfrNotidata)
   * \brief  To send Notification Events data to HMI
   * \param  cou32DevId       : [IN]  Unique Device Id
   * \param  corfrNotidata    : [IN]  Notification Data
   * \retval t_Void
   **************************************************************************/
   t_Void vCbPostNotificationResult(
      const t_U32 cou32DevId,
      const trNotiData& corfrNotidata);

   /*[DiPO-HACK] DisplayContext*/
   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vCbPostDeviceDisplayContext
   **                   (t_U32 u32DeviceHandle, tenDisplayContext enDisplayContext,..)
   ***************************************************************************/
   /*!
   * \fn     vCbPostDeviceDisplayContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  This interface is used by Mirror Link/DiPO device to inform the client
   *              about its current display con-text.
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bDisplayFlag     : TRUE � Start Display Projection, FALSE � Stop Display Projection.
   * \param  [IN] enDisplayContext : Display context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   * \sa
   **************************************************************************/
   t_Void vCbPostDeviceDisplayContext(t_U32 u32DeviceHandle,
      t_Bool bDisplayFlag,
      tenDisplayContext enDisplayContext,
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclAppMngr::bIsLaunchAppReq()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bIsLaunchAppReq(t_U32 u32DevID,
   *                          t_U32 u32AppID,
   *                          t_U32 u32NotificationID,
   *                          t_U32 u32NotificationActionID,
   *                          tenDeviceCategory enDevCat)            
   * \brief   Checks whether the launch app is required for the user action
   *          upon receiving a Notification
   * \param   u32DevID           : [IN] Device handle to identify the MLServer
   * \param   u32AppID           : [IN] ApplicationID for which Notification was available
   * \param   u32NotificationID  : [IN] Notification identifier sent during
   *                                    notification event
   * \param   u32NotificationActionID : [IN]  ActionID corresponding to the user action
   *                                     on HMI
   * \param  enDevCat            : [IN] Device Category. ML/DiPo 
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bIsLaunchAppReq(t_U32 u32DevID,
      t_U32 u32AppID,
      t_U32 u32NotificationID,
      t_U32 u32NotificationActionID,
      tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vCbSessionStatusUpdate()
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbSessionStatusUpdate(const t_U32 cou32DevId,
   *            const tenDeviceCategory coenDevCat,
   *            const tenSessionStatus coenSessionStatus)
   * \brief  method to update the session status to HMI 
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  coenSessionStatus: [IN] Session Status
   * \retval t_Void
   **************************************************************************/
   t_Void vCbSessionStatusUpdate(const t_U32 cou32DevId,
      const tenDeviceCategory coenDevCat,
      const tenSessionStatus coenSessionStatus);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngr::vCbActiveAppInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbActiveAppInfo(const t_U32 cou32DevId,
   *            const tenDeviceCategory coenDevCat,
   *            const t_U32 cou32AppId,
   *            const tenAppCertificationInfo coenAppCertInfo)
   * \brief  method to send the Active App update to HMI 
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  cou32AppId       : [IN] AppId
   * \param  coenAppCertInfo  : [IN] Certification Status
   * \retval t_Void
   **************************************************************************/
   t_Void vCbActiveAppInfo(const t_U32 cou32DevId,
      const tenDeviceCategory coenDevCat,
      const t_U32 cou32AppId,
      const tenAppCertificationInfo coenAppCertInfo);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclAppMngr::vSendNavigationStatus(...)
    **************************************************************************/
   /*!
    * \fn     vSendNavigationStatus(const trNavStatusData& corfrNavStatusData)
    * \brief  It notifies the client whenever there is a navigation status
    *         change(ACTIVE/INACTIVE/UNAVAILABLE).
    * \param  corfrNavStatusData  : [IN] Structure containing the device handle,
    *                                    device category and navigation status
    **************************************************************************/
    t_Void vSendNavigationStatus(const trNavStatusData& corfrNavStatusData);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclAppMngr::vSendNavigationNextTurnData(...)
   ***************************************************************************/
   /*!
    * \fn     vSendNavigationNextTurnData(
    *          const trNavNextTurnData& corfrNavNexTurnData)
    * \brief  It notifies the client whenever there is a navigation
    *         next turn event information.
    * \param  corfrNavNexTurnData : [IN] Structure containing device handle,
    *                               device category, road name,
    *                               next turn details such as side,
    *                               event, image, angle and number
    **************************************************************************/
   t_Void vSendNavigationNextTurnData(const trNavNextTurnData& corfrNavNexTurnData);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclAppMngr::vSendNavigationNextTurnDistanceData(...)
   ***************************************************************************/
   /*!
    * \fn     vSendNavigationNextTurnDistanceData(
    *         const trNavNextTurnDistanceData& corfrNavNextTurnDistData)
    * \brief  It notifies the client whenever there is a change
    *         in navigation next turn distance data.
    * \param  corfrNavNextTurnDistData : [IN] Structure containing device handle,
    *                             device category, distance and time of the next turn
    **************************************************************************/
   t_Void vSendNavigationNextTurnDistanceData(
           const trNavNextTurnDistanceData& corfrNavNextTurnDistData);

    /**************************************************************************
     ** FUNCTION: t_Void spi_tclAppMngr::vSendNotificationData(...)
     *************************************************************************/
    /*!
     * \fn     vSendNotificationData(
     *          const trNotificationData& corfrNotificationData)
     * \brief  It notifies the client whenever a notification is received
     * \param  corfrNotificationData : [IN] Structure containing the device handle,
     *                                     device category and notification data
     *************************************************************************/
     t_Void vSendNotificationData(const trNotificationData& corfrNotificationData);

     /*************************************************************************
      ** FUNCTION: t_Void spi_tclAppMngr::vAckNotification(...)
      ************************************************************************/
     /*!
      * \fn     vAckNotification(const trNotificationAckData& corfrNotifAckData)
      * \brief  Interface for sending acknowledgment for a received notification
      * \param  corfrNotifAckData : [IN] Structure containing the device handle,
      *                                  device category and acknowledgment data
      ************************************************************************/
      t_Void vAckNotification(const trNotificationAckData& corfrNotifAckData);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/
private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngr::~spi_tclAppMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppMngr()
   * \brief   Constructor
   * \sa      ~spi_tclAppMngr()
   **************************************************************************/
    spi_tclAppMngr();

   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngr& spi_tclAppMngr::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppMngr& operator= (const spi_tclAppMngr &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclAppMngr& operator= (const spi_tclAppMngr &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngr::spi_tclAppMngr(const spi_tclAppMngr..
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppMngr(const spi_tclAppMngr &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclAppMngr(const spi_tclAppMngr &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclAppMngr::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterCallbacks()
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo App Mngr
   * \retval  t_Void 
   **************************************************************************/
   t_Void vRegisterCallbacks();

   /***************************************************************************
   ** FUNCTION:  t_Bool  spi_tclAppMngr::bValidateClient()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bValidateClient(t_U8 u8Index)
   * \brief   To validate the client index. check whether it is in the range of
   *          the App Mngr clients Array
   * \retval  t_Bool 
   **************************************************************************/
   t_Bool bValidateClient(t_U8 u8Index);

   //! Member Variable - pointer to the Base class for ML & DiPo AppMngrs
   spi_tclAppMngrDev* m_poAppMngrDevBase[NUM_APPMNGR_CLIENTS];

   //! AppMngr Response Intf
   spi_tclAppMngrRespIntf* m_poAppMngrRespIntf;

   //! Map to store Device connection Type.
   std::map<t_U32,tenDeviceConnectionType> m_mapDevConnType;

   //! loack variable
   Lock m_oLock;

   //! current selected device
   t_U32 m_u32SelectedDevice;

   /*[DiPO-HACK]*/
   //!Display Context status 
   t_Bool m_bDisplayContextStatus;

   /*[DiPO-HACK]*/
   //!Display Context status 
   tenDisplayContext m_enDisplayContext;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/
};//spi_tclAppMngr

#endif //_SPI_TCLAPPMNMGR_
