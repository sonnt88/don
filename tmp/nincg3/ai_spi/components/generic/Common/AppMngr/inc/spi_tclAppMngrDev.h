/***********************************************************************/
/*!
* \file  spi_tclAppMngrDev.h
* \brief Base class for MirrorLink and DiPo App Mngr 
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for MirrorLink and DiPo App Mngr 
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version
23.04.2014  | Shiva Kumar Gurija    | Updated with Notifications Impl
10.03.2016  | Rachana L Achar       | Added Notification acknowledgment interface

\endverbatim
*************************************************************************/
#ifndef _SPI_TCL_APPMNGRDEV_
#define _SPI_TCL_APPMNGRDEV_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class spi_tclAppMngrDev
* \brief Base class for MirrorLink and DiPo App Mngr 
****************************************************************************/
class spi_tclAppMngrDev
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngrDev::spi_tclAppMngrDev()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppMngrDev()
   * \brief   Default Constructor
   * \sa      ~spi_tclAppMngrDev()
   **************************************************************************/

   spi_tclAppMngrDev()
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngrDev::~spi_tclAppMngrDev()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclAppMngrDev()
   * \brief   Destructor
   * \sa      spi_tclAppMngrDev()
   **************************************************************************/
   virtual ~spi_tclAppMngrDev()
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppMngrDev::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the App Mngr related classes
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   virtual t_Bool bInitialize()=0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief   To Uninitialize all the App Mngr related classes
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   virtual t_Void vUnInitialize()=0;

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclAppMngrDev::vRegisterAppMngrCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo App Mngr
   * \param   corfrAppMngrCbs : [IN] Application Manager callabcks structure
   * \retval  t_Void 
   **************************************************************************/
   virtual t_Void vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs)=0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSelectDevice(const t_U32 cou32DevId, 
   *          const tenDeviceConnectionReq coenConnReq)
   * \brief   To Subscribe/unsubscribe for events of the currently selected device
   * \pram    cou32DevId : [IN] Unique Device Id
   * \param   coenConnReq : [IN] connected/disconnected
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSelectDevice(const t_U32 cou32DevId, 
      const tenDeviceConnectionReq coenConnReq)=0;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppMngrDev::bLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Bool bLaunchApp(const t_U32 cou32DevId, 
   *           t_U32 u32AppId,
   *           const trUserContext& rfrcUsrCntxt,
   *           tenDiPOAppType enDiPOAppType, 
   *           t_String szTelephoneNumber, 
   *           tenEcnrSetting enEcnrSetting)
   * \brief   To Launch the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param  [IN] enDevCat : Device Type Information(Mirror Link/DiPO).
   * \param  [IN] u32AppId : Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface. 
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
   *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] szTelephoneNumber : Number to be dialed if the DiPO application to be launched 
   *              is a phone application. If not valid to be used, this will be set to NULL, 
   *              zero length string. Will not be used if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] enEcnrSetting : Sets voice or server echo cancellation and noise reduction 
   *              settings if the DiPO application to be launched is a phone application. 
   *              If not valid to be used, this will be set to ECNR_NOCHANGE.
   * \retval  t_Void
   * \sa      vTerminateApp()
   **************************************************************************/
   virtual t_Void vLaunchApp(const t_U32 cou32DevId, 
      t_U32 u32AppId,
      const trUserContext& rfrcUsrCntxt, 
      tenDiPOAppType enDiPOAppType, 
      t_String szTelephoneNumber, 
      tenEcnrSetting enEcnrSetting)
   {
      //add code
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(u32AppId);
      SPI_INTENTIONALLY_UNUSED(rfrcUsrCntxt);
      SPI_INTENTIONALLY_UNUSED(enDiPOAppType);
      SPI_INTENTIONALLY_UNUSED(szTelephoneNumber);
      SPI_INTENTIONALLY_UNUSED(enEcnrSetting);
   }


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchApp(const t_U32 cou32DevId, 
   *           t_U32 u32AppId,
   *           const trUserContext& rfrcUsrCntxt)
   * \brief   To Launch the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   u32AppId    : [IN]  Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface. 
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   *          rfrcUsrCntxt : [IN] user Context
   * \retval  t_Void
   * \sa      vTerminateApp()
   **************************************************************************/
   virtual t_Void vLaunchApp(const t_U32 cou32DevId, 
      t_U32 u32AppId, 
      const trUserContext& rfrcUsrCntxt)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(u32AppId);
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vTerminateApp()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vTerminateApp(const t_U32 cou32DevId,
   *                 const t_U32 cou32AppId,
   *                 const trUserContext& rfrcUsrCntxt)=0
   * \brief   To Terminate an Application asynchronously.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    cou32AppId  : [IN] Application Id
   * \param   rUserContext : [IN] Context Message
   * \retval  t_Void
   * \sa      bLaunchApp()
   **************************************************************************/
   virtual t_Void vTerminateApp(const t_U32 cou32DevId, 
      const t_U32 u32AppId,
      const trUserContext& rfrcUsrCntxt)=0;

    /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vGetAppDetailsList()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vGetDeviceInfoList(const t_U32 cou32DevId,
   *         t_U32& u32NumAppInfoList,
   *         std::vector<trAppDetails>& corfvecrAppDetailsList)
   * \brief  It provides a list of applications supported by a device.
   * \param  cou32DevId  : [IN] Device Handle
   * \param  u32NumAppInfoList : [OUT] Number of Applications in the List
   * \param  corfvecrAppDetailsList : [OUT] List of applicationinfo's of the 
   *                                 Applications supported by the device
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vGetAppDetailsList(const t_U32 cou32DevId,
      t_U32& u32NumAppInfoList,
      std::vector<trAppDetails>& corfvecrAppDetailsList)
   {
      //add code
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(u32NumAppInfoList);
      SPI_INTENTIONALLY_UNUSED(corfvecrAppDetailsList);
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vGetAppIconData()
   ***************************************************************************/
   /*!
   * \fn    virtual t_Void vGetAppIconData(t_String szAppIconUrl, 
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  szAppIconUrl  : [IN] Application Icon data
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vGetAppIconData(t_String szAppIconUrl, 
      const trUserContext& rfrcUsrCntxt)=0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vGetAppInfo()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetAppInfo(const t_U32 cou32DevId, 
   *         const t_U32 cou32AppId,
   *         trAppDetails& rfrAppDetails)
   * \brief  To Get an application info
   * \param    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param    cou32AppId  : [IN] Application Id
   * \param    rfrAppDetails : [OUT]App  Info
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vGetAppInfo(const t_U32 cou32DevId, 
      const t_U32 cou32AppId,
      trAppDetails& rfrAppDetails)
   {
      //Add code
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(cou32AppId);
      SPI_INTENTIONALLY_UNUSED(rfrAppDetails);
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vDisplayAllAppsInfo()
   ***************************************************************************/
   /*!
   * \fn    t_Void vDisplayAllAppsInfo(const t_U32 cou32DevId)
   * \brief  To display all the stored applications info
   * \param    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vDisplayAllAppsInfo(const t_U32 cou32DevId)
   {
      //Add code
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
   }

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclAppMngrDev::vSetAppIconAttributes()
   ***************************************************************************/
   /*!
   * \fn     t_Bool vSetAppIconAttributes(t_U32 u32DevId, 
   *            t_U32 u32AppId,
   *            const IconAttributes &rfrIconAttributes)
   * \brief  sets application icon attributes for retrieval of application icons.
   * \param  u32DevId      :  [IN] Uniquely identifies the target Device.
   * \param  u32AppId  : [IN] Uniquely identifies an application on the 
   *              target device. This value will be obtained from GetAppList Interface.
   * \param  rfrIconAttributes : [IN] Icon details.
   **************************************************************************/
   virtual t_Bool bSetAppIconAttributes(t_U32 u32DevId,
      t_U32 u32AppId,
      const trIconAttributes &rfrIconAttributes)
   {
      //Add code
      SPI_INTENTIONALLY_UNUSED(u32DevId);
      SPI_INTENTIONALLY_UNUSED(u32AppId);
      SPI_INTENTIONALLY_UNUSED(rfrIconAttributes);
      return false;
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrDev::vSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
   *          t_Bool bSetConfig,const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the Vehicle configurations.
   * \param  [IN] enVehicleConfig :  Identifies the Vehicle Configuration.
   * \param  [IN] bSetConfig      : Enable/Disable config
   **************************************************************************/
   virtual t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig)=0;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppMngrDev::bCheckAppValidity()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bCheckAppValidity(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId)
   * \brief   To check whether the application exists or not
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId  : [IN] Application Id
   * \retval  t_Bool
   **************************************************************************/
   virtual t_Bool bCheckAppValidity(const t_U32 cou32DevId, 
      const t_U32 cou32AppId)=0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vDisplayAppcertificationInfo()
   ***************************************************************************/
   /*!
   * \fn      t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId)
   * \brief   To disaply the application certification info 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId  : [IN] Application Id
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vDisplayAppcertificationInfo(const t_U32 cou32DevId,
       const t_U32 cou32AppId)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(cou32AppId);
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(coenConnReq);
      SPI_INTENTIONALLY_UNUSED(coenRespCode);
   }

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAppMngrDev::bSetClientProfile()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bSetClientProfile(const t_U32 cou32DevHandle,
   *             const trClientProfile& corfrClientProfile)
   * \brief   To set the client profiles
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   corfrClientProfile  : [IN] Client Profile to be set
   * \retval  t_Bool
   **************************************************************************/
   virtual t_Bool bSetClientProfile(const t_U32 cou32DevId,
      const trClientProfile& corfrClientProfile)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(corfrClientProfile);
      return false;
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAppMngrDev::vSetVehicleBTAddress()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetVehicleBTAddress(const t_U32 cou32DevHandle,
   *             t_String szBTAddress,
   *             const trUserContext& corfrUsrCntxt)
   * \brief   To set the ML Client Blue tooth address
   * \pram    cou32DevHandle  : [IN] Uniquely identifies the target Device.
   * \param   szBTAddress  : [IN] 12-bit BT Address
   * \param   corfrUsrCntxt : [IN] User context
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSetVehicleBTAddress(const t_U32 cou32DevId,
      t_String szBTAddress,
      const trUserContext& corfrUsrCntxt)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(szBTAddress);
      SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrDev::vSetMLNotificationEnabledInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetMLNotificationEnabledInfo(const t_U32 cou32DevId,
   *            t_U16 u16NumNotiEnableList,
   *            std::vector<NotificationEnable> vecrNotiEnableList,
   *            const trUserContext corfrUsrCntxt)
   * \brief  Interface to set the device notification preference for
   *         applications.
   *         Set u32DeviceHandle to 0xFFFF, to indicate all devices.
   *         If notification for all the applications has to be:
   *         Enabled - Set NumNotificationEnableList to 0xFFFF
   *                   and NotificationEnableList should be empty.
   *         Disabled - Set NumNotificationEnableList to 0x00.
   *                   and NotificationEnableList should be empty.
   * \param  cou32DevId           : [IN] Uniquely identifies the Device.
   * \param  u16NumNotiEnableList : [IN] Total number of records in
   *              NotificationEnableList.
   * \param  vecrNotiEnableList   : [IN] List of NotificationEnable records.
   * \param  corfrUsrCntxt        : [IN] User Context Details.
   * \sa     spi_tclRespInterface::vPostSetMLNotificationEnabledInfoResult
   **************************************************************************/
   virtual t_Void vSetMLNotificationEnabledInfo(const t_U32 cou32DevId,
      t_U16 u16NumNotiEnableList,
      std::vector<trNotiEnable> vecrNotiEnableList,
      const trUserContext& corfrUsrCntxt)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(u16NumNotiEnableList);
      SPI_INTENTIONALLY_UNUSED(vecrNotiEnableList);
      SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrDev::vInvokeNotificationAction()
   ***************************************************************************/
   /*!
   * \fn     t_Void vInvokeNotificationAction(const t_U32 cou32DevId,
   *            t_U32 u32NotificationID, 
   *            t_U32 u32NotificationActionID,
   *            const trUserContext& corfrUsrCntxt)
   * \brief  Interface to invoke the respective action for the received
   *         Notification Event.
   * \param  cou32DevId        : [IN]  Uniquely identifies the target Device.
   * \param  u32NotificationID : [IN]  Notification Identifier.
   * \param  u32NotificationActionID : [IN]  Notification action Identifier.
   * \param  corfrUsrCntxt     : [IN]  User Context Details.
   **************************************************************************/
   virtual t_Void vInvokeNotificationAction(const t_U32 cou32DevId,
      t_U32 u32NotificationID,
      t_U32 u32NotificationActionID,
      const trUserContext& corfrUsrCntxt) 
   {
         SPI_INTENTIONALLY_UNUSED(cou32DevId);
         SPI_INTENTIONALLY_UNUSED(u32NotificationID);
         SPI_INTENTIONALLY_UNUSED(u32NotificationActionID);
         SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);
   }


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrDev::vSetMLNotificationOnOff()
   ***************************************************************************/
   /*!
   * \fn     t_Bool vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
   * \brief  To Set the Notifications to On/Off
   * \param  bSetNotificationsOn : [IN] True-Set Notifications to ON
   *                                    False - Set Notifications to OFF
   * \retval t_Void 
   **************************************************************************/
   virtual t_Void vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
   {
      SPI_INTENTIONALLY_UNUSED(bSetNotificationsOn);
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrDev::vDisplayAppListXml()
   ***************************************************************************/
   /*!
   * \fn     t_Void vDisplayAppListXml(const t_U32 cou32DevId)
   * \brief  Method to retrieve the Applications XML Buffer of the device
   *          This is ML specific
   * \param  cou32DevId : [IN] Device ID
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vDisplayAppListXml(const t_U32 cou32DevId)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
   }

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclAppMngrDev::bIsLaunchAppReq()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bIsLaunchAppReq(t_U32 u32DevID,
   *                          t_U32 u32AppID,
   *                          t_U32 u32NotificationID,
   *                          t_U32 u32NotificationActionID)
   *            
   * \brief   Checks whether the launch app is required for the user action
   *          upon receiving a Notification
   * \param   u32DevID           : [IN] Device handle to identify the MLServer
   * \param   u32AppID           : [IN] ApplicationID for which Notification was available
   * \param   u32NotificationID  : [IN] Notification identifier sent during
   *                                    notification event
   * \param   u32NotificationActionID : [IN]  ActionID corresponding to the user action
   *                                     on HMI
   * \retval  t_Bool
   **************************************************************************/
   virtual t_Bool bIsLaunchAppReq(t_U32 u32DevID,
      t_U32 u32AppID,
      t_U32 u32NotificationID,
      t_U32 u32NotificationActionID)
   {
      SPI_INTENTIONALLY_UNUSED(u32DevID);
      SPI_INTENTIONALLY_UNUSED(u32AppID);
      SPI_INTENTIONALLY_UNUSED(u32NotificationID);
      SPI_INTENTIONALLY_UNUSED(u32NotificationActionID);

      return false;
   }

   /**********************************************************************************
   ** FUNCTION: t_Bool spi_tclAppMngrDev::vAckNotification()
   **********************************************************************************/
   /*!
   * \fn      t_Bool vAckNotification(const trNotificationAckData& corfrNotifAckData)
   *
   * \brief   Acknowledges the receipt of notification
   * \param   u32DeviceHandle : [IN] Id of the device to be acknowledged
   * \param   corfszNotifId   : [IN] Received notification's Id
   * \retval  t_Void
   **********************************************************************************/
   virtual t_Void vAckNotification(t_U32 u32DeviceHandle, const t_String& corfszNotifId)
   {
      SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
      SPI_INTENTIONALLY_UNUSED(corfszNotifId);
   }

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/
};//spi_tclAppMngrDev

#endif //_SPI_TCL_APPMNGRDEV_
