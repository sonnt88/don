/***********************************************************************/
/*!
* \file  spi_tclAppLauncher.cpp
* \brief Class Responsible for Launching & Terminating Applications
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class Responsible for Launching & Terminating Applications
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclFactory.h"
#include "spi_tclMediator.h"
#include "spi_tclAudio.h"
#include "spi_tclVideo.h"
#include "spi_tclResourceMngr.h"
#include "spi_tclAppLauncherRespIntf.h"
#include "spi_tclAppLauncher.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclAppLauncher.cpp.trc.h"
#endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
static spi_tclFactory* spoFactory = NULL;
/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclAppLauncher::spi_tclAppLauncher()
***************************************************************************/
spi_tclAppLauncher::spi_tclAppLauncher(spi_tclAppLauncherRespIntf* poSpiRespIntf):
m_poAppLauncherRespIntf(poSpiRespIntf)
{
   ETG_TRACE_USR1(("spi_tclAppLauncher() entered\n"));
   spoFactory = spi_tclFactory::getInstance();
   SPI_NORMAL_ASSERT(NULL == spoFactory);
   //Register for callbacks with the mediator
   vRegisterCallbacks();
}

/***************************************************************************
** FUNCTION:  spi_tclAppLauncher::~spi_tclAppLauncher()
***************************************************************************/
spi_tclAppLauncher::~spi_tclAppLauncher()
{
   ETG_TRACE_USR1(("~spi_tclAppLauncher() entered\n"));

   //Destructor
   spoFactory = NULL;
   m_poAppLauncherRespIntf = NULL;
}

/***************************************************************************
** FUNCTION:  spi_tclAppLauncher::vRegisterCallbacks
***************************************************************************/
t_Void spi_tclAppLauncher::vRegisterCallbacks()
{
   trAppLauncherCallbacks rAppLauncherCbs;
   /*lint -esym(40,fpvTerminateAppResult)fpvTerminateAppResult Undeclared identifier */
   /*lint -esym(40,fpvLaunchAppResult)fpvLaunchAppResult Undeclared identifier */
   /*lint -esym(40,_1)_1 Undeclared identifier */
   /*lint -esym(40,_2)_2 Undeclared identifier */
   /*lint -esym(40,_3)_3 Undeclared identifier */
   /*lint -esym(40,_4)_4 Undeclared identifier */
   /*lint -esym(40,_5)_5 Undeclared identifier */
   /*lint -esym(40,_6)_6 Undeclared identifier */
   rAppLauncherCbs.fpvTerminateAppResult = std::bind(&spi_tclAppLauncher::vCbTerminateAppResult,
      this,SPI_FUNC_PLACEHOLDERS_5);

   rAppLauncherCbs.fpvLaunchAppResult = std::bind(&spi_tclAppLauncher::vCbLaunchAppResult,
      this,SPI_FUNC_PLACEHOLDERS_6);

   spi_tclMediator* poMediator = spi_tclMediator::getInstance();
   if (NULL != poMediator)
   {
      ETG_TRACE_USR1(("Register Callbacks"));
      poMediator->vRegisterCallbacks(rAppLauncherCbs);
   } // if (NULL != poMediator)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppLauncher::vLaunchApp()
***************************************************************************/
t_Void spi_tclAppLauncher::vLaunchApp(t_U32 u32DevId, 
                                      t_U32 u32AppId, 
                                      tenDiPOAppType enDiPOAppType, 
                                      t_String szTelephoneNumber, 
                                      tenEcnrSetting enEcnrSetting, 
                                      const trUserContext& corfrUsrCntxt)
{
	/*lint -esym(40,fpvTerminateAppResult)fpvTerminateAppResult Undeclared identifier */
	/*lint -esym(40,fpvLaunchAppResult)fpvLaunchAppResult Undeclared identifier */
	/*lint -esym(40,_1)_1 Undeclared identifier */
	/*lint -esym(40,_2)_2 Undeclared identifier */
	/*lint -esym(40,_3)_3 Undeclared identifier */
	/*lint -esym(40,_4)_4 Undeclared identifier */
	/*lint -esym(40,_5)_5 Undeclared identifier */
	/*lint -esym(40,_6)_6 Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAppLauncher::vLaunchApp: Dev-0x%x App-0x%x\n",
      u32DevId,u32AppId));

   t_Bool bSendResponse = true;
   tenErrorCode enErrorCode = e8UNKNOWN_ERROR;

   if(NULL != spoFactory)
   {
      spi_tclConnMngr* poConnMngr = spoFactory->poGetConnMngrInstance();
      spi_tclAppMngr* poAppMngr = spoFactory->poGetAppManagerInstance();

      //if the device is currently selected device, then only launch proceed further
      if( (NULL != poConnMngr)&&(NULL != poAppMngr) &&
         (u32DevId == poAppMngr->u32GetSelectedDevice()))
      {
         tenDeviceCategory enDevCat=poConnMngr->enGetDeviceCategory(u32DevId);
         //if the requested application is a part of app list, then only send launch app 
         //request
         if(true == poAppMngr->bCheckAppValidity(u32DevId,u32AppId,enDevCat))
         {
            bSendResponse = false;

            if(e8DEV_TYPE_MIRRORLINK == enDevCat)
            {
               poAppMngr->vLaunchApp(u32DevId, enDevCat, u32AppId,corfrUsrCntxt);
            }//if(e8DEV_TYPE_MIRRORLINK == enDeviceCategory)
            else
            {
               poAppMngr->vLaunchApp(u32DevId, enDevCat, u32AppId, enDiPOAppType,
                  szTelephoneNumber,enEcnrSetting,corfrUsrCntxt);
            }
         }//if(true == poAppMngr->bCheckAppValidity(u32DeviceHandle,u32AppHandle,enDevCat))
         else
         {
            //send the response with the error code invalid app handle
            enErrorCode = e8INVALID_APP_HANDLE ;
         }
      }//if((NULL != poAppMngr)
      else
      {
         //send response with the error code invalid device handle
         enErrorCode = e8INVALID_DEV_HANDLE ;
      }
   }//if(NULL != spoFactory)

   //Send the response, only if the launch app request is not sent to App Mngr.
   //If there is any error during the application launching, App Mngr will
   //send the response with the LAUNCH_FAILED error code
   if( (true == bSendResponse) && (NULL != m_poAppLauncherRespIntf) )
   {
      tenResponseCode enResponseCode = (enErrorCode==e8NO_ERRORS)?e8SUCCESS:e8FAILURE ;
      m_poAppLauncherRespIntf->vLaunchAppResult(u32DevId,u32AppId,enDiPOAppType,
         enResponseCode,enErrorCode,corfrUsrCntxt);
   }//if( (true == bSendResponse) && (NULL != m_poAppLauncherRespIntf) )

}

/***************************************************************************
** FUNCTION: t_Void spi_tclMediator::vCbLaunchAppResult
***************************************************************************/
t_Void spi_tclAppLauncher::vCbLaunchAppResult(const tenCompID coenCompID,
                                              const t_U32 cou32DevId,
                                              const t_U32 cou32AppId,
                                              const tenDiPOAppType coenDiPoAppType,
                                              tenErrorCode enErrorCode,
                                              const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAppLauncher::vCbLaunchAppResult: Dev-0x%x App-0x%x",
      cou32DevId,cou32AppId));
   //If the Application is launched successfully, Launch audio & Video for the the App.
   //and then send response
   tenResponseCode enResponseCode = (enErrorCode==e8NO_ERRORS)?e8SUCCESS:e8FAILURE ;

   //If the response is from any other component posting error currently
   if(coenCompID==e32COMPID_APPMANAGER)
   {
      if((enErrorCode == e8NO_ERRORS)&&(NULL != spoFactory))
      {
         spi_tclConnMngr* poConnMngr = spoFactory->poGetConnMngrInstance();
         spi_tclVideo* poVideo = spoFactory->poGetVideoInstance();
         spi_tclAudio* poAudio = spoFactory->poGetAudioInstance();
         spi_tclAppMngr* poAppMngr = spoFactory->poGetAppManagerInstance();
         spi_tclResourceMngr* poRscrMngr = spoFactory->poGetRsrcMngrInstance();

         if((NULL != poConnMngr) && (NULL != poVideo) && (NULL != poAudio)
        		 &&(NULL != poAppMngr) && (NULL != poRscrMngr))
         {
            tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(cou32DevId);

            //Fetch the Application Video& Audio and then send Launch Audio & Video request
            trAppDetails rAppInfo;
            poAppMngr->vGetAppInfo(cou32DevId,cou32AppId,enDevCat,rAppInfo);

            //Launch Video - If any error in launching Video - Post Error to HMI
            //! If prewarm is received for Android Auto don't launch Video
            if((!((e8DEV_TYPE_ANDROIDAUTO == enDevCat) && (e8DIPO_SIRI_PREWARN == coenDiPoAppType)))
                  && (true == poVideo->bLaunchVideo(cou32DevId, cou32AppId,enDevCat)))
            {
               //Send request to Resource manager also. Handling launch app and video reosurce management is done together,
               //to avoid synchronization issues
               poRscrMngr->vLaunchApp(cou32DevId, cou32AppId,enDevCat);


               //launch Audio, if video is launched successfully
               /* Application category check is removed. Since some phones are providing the App category as a UI for Music 
               applications also. and at the same time Navigation,Phone, Messaging applications also can have audio*/
               if(e8DEV_TYPE_MIRRORLINK == enDevCat)
               {
                  poAudio->vLaunchAudio(cou32DevId, enDevCat,e8AUD_MAIN_OUT);
               }

            }//if(true == poVideo->bLaunchVideo
            else
            {
               //Send error response to HMI
               enResponseCode = e8FAILURE ;
               //Wayland or Unknown error in launching Video
               enErrorCode = e8UNKNOWN_ERROR;
            }
         }//if((NULL != poConnMngr) && (NULL != poVideo) && (NULL != poAudio))
      }//if((bResult == true)&&(NULL != spoFactory))
   }//if(coenCompID==e32COMPID_APPMANAGER)

   //@todo - if we have any asynchronous response from Launch Audio, we should post the response 
   //after receiving the response from Audio
   //Currently we receive response only from AppMngr & response from video is synchronous
   //So posting that response directly.
   if(NULL != m_poAppLauncherRespIntf)
   {
      m_poAppLauncherRespIntf->vLaunchAppResult(cou32DevId,cou32AppId,coenDiPoAppType,
         enResponseCode,enErrorCode,corfrUsrCntxt);
   }//if(NULL != m_poAppLauncherRespIntf)
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclAppLauncher::vTerminateApp()
***************************************************************************/
t_Void spi_tclAppLauncher::vTerminateApp(const t_U32 cou32DevId,
                                         const t_U32 cou32AppId,
                                         const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAppLauncher::vTerminateApp: Dev-0x%x App-0x%x\n",
      cou32DevId,cou32AppId));

   t_Bool bSendResponse = true;
   tenErrorCode enErrorCode = e8UNKNOWN_ERROR;

   if(NULL != spoFactory)
   {
      spi_tclConnMngr* poConnMngr = spoFactory->poGetConnMngrInstance();
      spi_tclAppMngr* poAppMngr = spoFactory->poGetAppManagerInstance();
      spi_tclAudio* poAudio = spoFactory->poGetAudioInstance();

      //if the device is currently selected device, then only launch proceed further
      if((NULL != poConnMngr)&&(NULL != poAppMngr)&&(NULL != poAudio)&&
         (cou32DevId == poAppMngr->u32GetSelectedDevice()) )
      {
         tenDeviceCategory enDevCat=poConnMngr->enGetDeviceCategory(cou32DevId);
         //if the requested application is a part of app list, then only send terminate app 
         //request
         if(true == poAppMngr->bCheckAppValidity(cou32DevId,cou32AppId,enDevCat))
         {
            //if the terminate app request is sent to either Audio or App Mngr,
            //the response will be sent, after the collecting the responses from
            //App Mngr & Audio. do not send from here.
            bSendResponse = false;

            //Fetch the application info & Check the Audio Contents & Send terminate Audio Request
            trAppDetails rAppInfo;
            poAppMngr->vGetAppInfo(cou32DevId,cou32AppId,enDevCat,rAppInfo);

            //If the device is ML Device, send the Terminate Audio request, only if the application's 
            //category is MEDIA. In case if the device is DiPo device directly send the request
            if((enDevCat == e8DEV_TYPE_DIPO) ||
               (e32APP_MEDIA == (rAppInfo.enAppCategory & e32APP_MEDIA)))
            {
               //Terminate Audio
               poAudio->vTerminateAudio(cou32DevId, e8AUD_MAIN_OUT);
            }//if((enDevCat == e8DEV_TYPE_DIPO) ||

            //Terminate Application
            poAppMngr->vTerminateApp(cou32DevId,cou32AppId,enDevCat,corfrUsrCntxt);

         }//if(true == poAppMngr->bCheckAppValidity(cou32DevId,cou32AppId,enDevCat))
         else
         {
            //send the response with the error code invalid app handle
            enErrorCode = e8INVALID_APP_HANDLE ;
         }
      }//if((NULL != poConnMngr)&&(NULL != poAppMngr)&&(NULL != poAudio))
      else
      {
         //send response with the error code invalid device handle
         enErrorCode = e8INVALID_DEV_HANDLE ;
      }
   }//if(NULL != spoFactory)

   if( (true == bSendResponse) && (NULL != m_poAppLauncherRespIntf) )
   {
      tenResponseCode enResponseCode = (enErrorCode==e8NO_ERRORS)?e8SUCCESS:e8FAILURE ;
      m_poAppLauncherRespIntf->vTerminateAppResult(cou32DevId,cou32AppId,
         enResponseCode,enErrorCode,corfrUsrCntxt);
   }//if( (true == bSendResponse) && (NULL != m_poAppLauncherRespIntf) )
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclAppLauncher::vCbTerminateAppResult()
***************************************************************************/
t_Void spi_tclAppLauncher::vCbTerminateAppResult(const tenCompID coenCompID,
                                                 const t_U32 cou32DevId,
                                                 const t_U32 cou32AppId,
                                                 tenErrorCode enErrorCode,
                                                 const trUserContext& rfrCUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAppLauncher::vCbTerminateAppResult: Dev-0x%x App-0x%x\n",
      cou32DevId,cou32AppId));

   SPI_INTENTIONALLY_UNUSED(coenCompID);
   tenResponseCode enResponseCode = (enErrorCode==e8NO_ERRORS)?e8SUCCESS:e8FAILURE ;

   //Currently we receive response only from App Mngr.
   //So posting that response directly.
   //@todo - if we have any asynchronous response from Terminate Audio, we should post the response 
   //after recieving the response from App Mngr & Audio
   if(NULL != m_poAppLauncherRespIntf)
   {
      m_poAppLauncherRespIntf->vTerminateAppResult(cou32DevId,cou32AppId,
         enResponseCode,enErrorCode,rfrCUsrCntxt);
   }//if(NULL != m_poAppLauncherRespIntf)
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
