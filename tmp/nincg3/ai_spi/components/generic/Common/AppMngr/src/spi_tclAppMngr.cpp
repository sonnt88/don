/***********************************************************************/
/*!
* \file  spi_tclAppMngr.cpp
* \brief Main Application Manager class that provides interface to delegate 
*        the execution of command and handle response
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version
20.03.2014  | Shihabudheen P M      | Added vCbPostDeviceDisplayContext()
23.04.2014  | Shiva Kumar Gurija    | Updated with Notifications Impl
06.04.2014  | Ramya Murthy          | Initialisation sequence implementation
31.07.2014  | Ramya Murthy          | SPI feature configuration via LoadSettings()
25.06.2015  | Tejaswini HB          | Featuring out Android Auto
26.02.2016  | Rachana L Achar       | AAP Navigation implementation
10.03.2016  | Rachana L Achar       | AAP Notification implementation

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclRespInterface.h"
#include "spi_tclAppMngrRespIntf.h"
#include "spi_tclAppSettings.h"
#include "spi_tclAppMngrDev.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
#include "spi_tclMLAppMngr.h"
#endif
#include "spi_tclDiPoAppMngr.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO	
#include "spi_tclAAPAppMngr.h"
#endif
#include "spi_tclAppMngr.h"
#include "spi_tclMediator.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclAppMngr.cpp.trc.h"
#endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclAppMngr::spi_tclAppMngr()
***************************************************************************/
spi_tclAppMngr::spi_tclAppMngr(spi_tclAppMngrRespIntf* poRespIntf):
   m_poAppMngrRespIntf(poRespIntf), m_u32SelectedDevice(0)
{
   ETG_TRACE_USR1((" spi_tclAppMngr::spi_tclAppMngr"));

   for (t_U8 u8Index = 0; u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
   {
      m_poAppMngrDevBase[u8Index] = NULL;
   }//for(t_U8 u8Index=0; u8Index
}

/***************************************************************************
** FUNCTION:  spi_tclAppMngr::~spi_tclAppMngr()
***************************************************************************/
spi_tclAppMngr::~spi_tclAppMngr()
{
   ETG_TRACE_USR1((" spi_tclAppMngr::~spi_tclAppMngr"));

   m_mapDevConnType.clear();
   m_poAppMngrRespIntf = NULL;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAppMngr::bInitialize()
***************************************************************************/
t_Bool spi_tclAppMngr::bInitialize()
{
   m_poAppMngrDevBase[e8DEV_TYPE_DIPO] = new spi_tclDiPoAppMngr();
   SPI_NORMAL_ASSERT(NULL == m_poAppMngrDevBase[e8DEV_TYPE_DIPO]);
 
  #ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
   m_poAppMngrDevBase[e8DEV_TYPE_MIRRORLINK] = new spi_tclMLAppMngr();
   SPI_NORMAL_ASSERT(NULL == m_poAppMngrDevBase[e8DEV_TYPE_MIRRORLINK]);
#endif
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO	
   m_poAppMngrDevBase[e8DEV_TYPE_ANDROIDAUTO] = new spi_tclAAPAppMngr();
#endif

   //! Set the bInit to false, if all the SPI technologies cannot be initialized
   //! else return true if one of the SPI technologies can be initialized
   t_Bool bInit = false;
   if(NULL != m_poAppMngrDevBase[e8DEV_TYPE_DIPO])
   {
      bInit = m_poAppMngrDevBase[e8DEV_TYPE_DIPO]->bInitialize();
   } //if(NULL != m_poAppMngrDevBase[e8DEV_TYPE_DIPO])

   if(NULL != m_poAppMngrDevBase[e8DEV_TYPE_MIRRORLINK])
   {
      bInit = (m_poAppMngrDevBase[e8DEV_TYPE_MIRRORLINK]->bInitialize()) || bInit;
   }// if(NULL != m_poAppMngrDevBase[e8DEV_TYPE_MIRRORLINK])

   if(NULL != m_poAppMngrDevBase[e8DEV_TYPE_ANDROIDAUTO])
   {
      bInit = (m_poAppMngrDevBase[e8DEV_TYPE_ANDROIDAUTO]->bInitialize()) || bInit;
   }// if(NULL != m_poAppMngrDevBase[e8DEV_TYPE_MIRRORLINK])

   //Register for callbacks
   vRegisterCallbacks();

   ETG_TRACE_USR1((" spi_tclAppMngr::bInitialize bInit = %d", ETG_ENUM(BOOL, bInit)));
   return bInit;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAppMngr::bUnInitialize()
***************************************************************************/
t_Bool spi_tclAppMngr::bUnInitialize()
{
   ETG_TRACE_USR1((" spi_tclAppMngr::vUnInitialize() \n"));

   for (t_U8 u8Index = 0; u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
   {
      if (NULL != m_poAppMngrDevBase[u8Index])
      {
         m_poAppMngrDevBase[u8Index]->vUnInitialize();
      }//if((NULL != m_poVideoDevBase[
      RELEASE_MEM(m_poAppMngrDevBase[u8Index]);
   }//for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)

   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vLoadSettings(const trSpiFeatureSupport&...)
***************************************************************************/
t_Void spi_tclAppMngr::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(rfcrSpiFeatureSupp);

   //Read the settings from the XML
   spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
   if (NULL != poAppSettings)
   {
      poAppSettings->vReadAppSettings();
   }//if(NULL != poAppSettings)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vSaveSettings()
***************************************************************************/
t_Void spi_tclAppMngr::vSaveSettings()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclAppMngr::bValidateClient()
***************************************************************************/
inline t_Bool spi_tclAppMngr::bValidateClient(t_U8 u8Index)
{
   //Assert if the Index is greater than the array size
   SPI_NORMAL_ASSERT( u8Index > NUM_APPMNGR_CLIENTS);

   t_Bool bRet = ( u8Index < NUM_APPMNGR_CLIENTS) && 
      (NULL != m_poAppMngrDevBase[u8Index]);

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclAppMngr::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclAppMngr::vRegisterCallbacks()
{
	/*lint -esym(40,fpvNotifyAppListChangeCb) fpvNotifyAppListChangeCb Undeclared identifier */
	/*lint -esym(40,fpvTerminateAppResult) fpvTerminateAppResult Undeclared identifier */
	/*lint -esym(40,fpvLaunchAppResult) fpvLaunchAppResult Undeclared identifier */
	/*lint -esym(40,fpvCbAppIconDataResult) fpvCbAppIconDataResult Undeclared identifier */
	/*lint -esym(40,fpvSelectDeviceResult) fpvSelectDeviceResult Undeclared identifier */
	/*lint -esym(40,fpvPostDeviceDisplayContext) fpvPostDeviceDisplayContext Undeclared identifier */
	/*lint -esym(40,fpvEnableNotiResult) fpvEnableNotiResult Undeclared identifier */
	/*lint -esym(40,fpvInvokeNotiActionResult) fpvInvokeNotiActionResult Undeclared identifier */
	/*lint -esym(40,fpvNotifyNoticationInfo) fpvNotifyNoticationInfo Undeclared identifier */
	/*lint -esym(40,fpvNotifySessionStatus) fpvNotifySessionStatus Undeclared identifier */
	/*lint -esym(40,fpvNotifyActiveAppInfo) fpvEnableNotiResult Undeclared identifier */
	/*lint -esym(40,_1) _1 Undeclared identifier */
	/*lint -esym(40,_2) _2 Undeclared identifier */
	/*lint -esym(40,_3) _3 Undeclared identifier */
	/*lint -esym(40,_4) _4 Undeclared identifier */
	/*lint -esym(40,_5) _5 Undeclared identifier */


   ETG_TRACE_USR1(("spi_tclAppMngr:vRegisterCallbacks()"));

   trAppMngrCallbacks rAppMngrCbs;

   rAppMngrCbs.fpvNotifyAppListChangeCb = std::bind(
      &spi_tclAppMngr::vCbNotifyAppListChange, 
      this, 
      SPI_FUNC_PLACEHOLDERS_2);

   rAppMngrCbs.fpvTerminateAppResult = std::bind(
      &spi_tclAppMngr::vCbTerminateAppResult,
      this,
      SPI_FUNC_PLACEHOLDERS_4);

   rAppMngrCbs.fpvLaunchAppResult = std::bind(
      &spi_tclAppMngr::vCbLaunchAppResult,
      this,
      SPI_FUNC_PLACEHOLDERS_5);

   rAppMngrCbs.fpvCbAppIconDataResult = std::bind(
      &spi_tclAppMngr::vCbAppIconDataResult,
      this,
      SPI_FUNC_PLACEHOLDERS_4);

   rAppMngrCbs.fpvSelectDeviceResult = std::bind(
      &spi_tclAppMngr::vCbSelectDeviceResult, 
      this,SPI_FUNC_PLACEHOLDERS_1);

   rAppMngrCbs.fpvPostDeviceDisplayContext = std::bind(
      &spi_tclAppMngr::vCbPostDeviceDisplayContext,
      this, SPI_FUNC_PLACEHOLDERS_4);

   rAppMngrCbs.fpvEnableNotiResult = std::bind(
      &spi_tclAppMngr::vCbPostSetNotiEnabledInfoResult,
      this, SPI_FUNC_PLACEHOLDERS_3);

   rAppMngrCbs.fpvInvokeNotiActionResult = std::bind(
      &spi_tclAppMngr::vCbPostInvokeNotificationActionResult,
      this, SPI_FUNC_PLACEHOLDERS_3);

   rAppMngrCbs.fpvNotifyNoticationInfo = std::bind(
      &spi_tclAppMngr::vCbPostNotificationResult,
      this, SPI_FUNC_PLACEHOLDERS_2);


   rAppMngrCbs.fpvNotifySessionStatus = std::bind(
      &spi_tclAppMngr::vCbSessionStatusUpdate,this,
      SPI_FUNC_PLACEHOLDERS_3);

   rAppMngrCbs.fpvNotifyActiveAppInfo= std::bind(
      &spi_tclAppMngr::vCbActiveAppInfo,this,
      SPI_FUNC_PLACEHOLDERS_4);

   rAppMngrCbs.fvUpdateNavStatusData =
         std::bind(&spi_tclAppMngr::vSendNavigationStatus,
         this,
         SPI_FUNC_PLACEHOLDERS_1);

   rAppMngrCbs.fvUpdateNavNextTurnData =
         std::bind(&spi_tclAppMngr::vSendNavigationNextTurnData,
         this,
         SPI_FUNC_PLACEHOLDERS_1);

   rAppMngrCbs.fvUpdateNavNextTurnDistanceData =
         std::bind(&spi_tclAppMngr::vSendNavigationNextTurnDistanceData,
         this,
         SPI_FUNC_PLACEHOLDERS_1);

   rAppMngrCbs.fvUpdateNotificationData =
         std::bind(&spi_tclAppMngr::vSendNotificationData,
         this,
         SPI_FUNC_PLACEHOLDERS_1);

   for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
   {
      if(NULL != m_poAppMngrDevBase[u8Index])
      {
         m_poAppMngrDevBase[u8Index]->vRegisterAppMngrCallbacks(rAppMngrCbs);
      }//if((NULL != m_poAppMngrDevBase[
   }//for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vSelectDevice()
***************************************************************************/
t_Void spi_tclAppMngr::vSelectDevice(const t_U32 cou32DevId,
                                     tenDeviceConnectionType enDevConnType,
                                     const tenDeviceConnectionReq coenConnReq,
                                     tenDeviceCategory enDevCat,
                                     const trUserContext& rfrcUsrCntxt)
{

   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vSelectDevice:Dev-0x%x,enDevCat-%d,coenConnReq-%d",
      cou32DevId,u8Index,coenConnReq));

   SPI_INTENTIONALLY_UNUSED(rfrcUsrCntxt);
   //set the selected device. this is required to inform AppLauncher 
   //to send error response to HMI, if the Launch/terminate request has come for 
   //non selected device.
   m_u32SelectedDevice = (coenConnReq == e8DEVCONNREQ_SELECT)?cou32DevId:0;

   //Store the device connection type in the internally.
   //It is required while posting the App list change update to HMI
   m_oLock.s16Lock();
   m_mapDevConnType.insert(std::pair<t_U32,tenDeviceConnectionType>(cou32DevId,
      enDevConnType));
   m_oLock.vUnlock();

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vSelectDevice(cou32DevId,coenConnReq);
   }//if(true == bValidateClient(u8Index))

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vLaunchApp()
***************************************************************************/
t_Void spi_tclAppMngr::vLaunchApp(const t_U32 cou32DevId, 
                                  tenDeviceCategory enDevCat, 
                                  t_U32 u32AppHandle, 
                                  tenDiPOAppType enDiPOAppType, 
                                  t_String szTelephoneNumber, 
                                  tenEcnrSetting enEcnrSetting,
                                  const trUserContext& rfrcUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vLaunchApp:Dev-0x%x,App-0x%x,enDevCat-%d enDiPOAppType-%d",
      cou32DevId,u32AppHandle,ETG_ENUM(DEVICE_CATEGORY,enDevCat),enDiPOAppType));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vLaunchApp(cou32DevId,
         u32AppHandle,
         rfrcUsrCntxt,
         enDiPOAppType,
         szTelephoneNumber,
         enEcnrSetting);
   } //if(true == bValidateClient(u8Index))
   else
   {
      //invalid device handle - post error
      vCbLaunchAppResult(cou32DevId,u32AppHandle,enDiPOAppType,e8INVALID_DEV_HANDLE,
         rfrcUsrCntxt);
   }

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vLaunchApp()
***************************************************************************/
t_Void spi_tclAppMngr::vLaunchApp(const t_U32 cou32DevId, 
                                  tenDeviceCategory enDevCat, 
                                  t_U32 u32AppHandle, 
                                  const trUserContext& rfrcUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vLaunchApp:Dev-0x%x,App-0x%x,enDevCat-%d",
      cou32DevId,u32AppHandle,ETG_ENUM(DEVICE_CATEGORY,enDevCat)));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vLaunchApp(cou32DevId,
         u32AppHandle,
         rfrcUsrCntxt);
   } //if(true == bValidateClient(u8Index))
   else
   {
      //invalid device handle - post error
      vCbLaunchAppResult(cou32DevId,u32AppHandle,e8DIPO_NOT_USED,e8INVALID_DEV_HANDLE,
         rfrcUsrCntxt);
   }

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vTerminateApp()
***************************************************************************/
t_Void spi_tclAppMngr::vTerminateApp(const t_U32 cou32DevId,
                                     const t_U32 cou32AppId,
                                     tenDeviceCategory enDevCat,
                                     const trUserContext& rfrcUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vTerminateApp:Dev-0x%x,App-0x%x,enDevCat-%d",
      cou32DevId,cou32AppId,u8Index));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vTerminateApp(cou32DevId,cou32AppId,rfrcUsrCntxt);
   } //if(true == bValidateClient(u8Index))
   else
   {
      //invalid device handle - post error
      vCbTerminateAppResult(cou32DevId,cou32AppId,e8INVALID_DEV_HANDLE,rfrcUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vGetAppDetailsList()
***************************************************************************/
t_Void spi_tclAppMngr::vGetAppDetailsList(const t_U32 cou32DevId,
                                          t_U32& u32NumAppInfoList,
                                          std::vector<trAppDetails>& corfvecrAppDetailsList,
                                          tenDeviceCategory enDevCat)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vGetAppDetailsList:Dev-0x%x, enDevCat-%d",
      cou32DevId,u8Index));

   if( (m_u32SelectedDevice == cou32DevId) &&(true == bValidateClient(u8Index)) )
   {
      m_poAppMngrDevBase[u8Index]->vGetAppDetailsList(cou32DevId,
         u32NumAppInfoList,
         corfvecrAppDetailsList);
   } //if(true == bValidateClient(u8Index))

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vGetAppInfo()
***************************************************************************/
t_Void spi_tclAppMngr::vGetAppInfo(const t_U32 cou32DevId, 
                                   const t_U32 cou32AppId,
                                   tenDeviceCategory enDevCat,
                                   trAppDetails& rfrAppDetails)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vGetAppInfo: Dev-0x%x App-0x%x enDevCat-%d ",
      cou32DevId,cou32AppId,u8Index));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vGetAppInfo(cou32DevId,cou32AppId,
         rfrAppDetails);
   } //if(true == bValidateClient(u8Index))

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vDisplayAllAppsInfo()
***************************************************************************/
t_Void spi_tclAppMngr::vDisplayAllAppsInfo(const t_U32 cou32DevId,
                                           tenDeviceCategory enDevCat)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vDisplayAllAppsInfo: Dev-0x%x enDevCat-%d ",
      cou32DevId,u8Index));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vDisplayAllAppsInfo(cou32DevId);
   } //if(true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vGetAppIconData()
***************************************************************************/
t_Void spi_tclAppMngr::vGetAppIconData(t_String szAppIconUrl,
                                       tenDeviceCategory enDevCat,
                                       const trUserContext& rfrcUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vGetAppIconData:u8Index-%d IconURL-%s",
      u8Index,szAppIconUrl.c_str()));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vGetAppIconData(szAppIconUrl,rfrcUsrCntxt);
   } //if(true == bValidateClient(u8Index))
   else
   {
      //invalid handle - Post error
      vCbAppIconDataResult(e8ICON_INVALID,NULL,0,rfrcUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vSetAppIconAttributes()
***************************************************************************/
t_Void spi_tclAppMngr::vSetAppIconAttributes(t_U32 u32DevId,
                                             t_U32 u32AppId,
                                             tenDeviceCategory enDevCat,
                                             const trIconAttributes &rfrIconAttributes,
                                             const trUserContext& rfrcUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   ETG_TRACE_USR1(("spi_tclAppMngr::vSetAppIconAttributes:Dev-0x%x,App-0x%x,u8Index-%d",
      u32DevId,u32AppId,u8Index));

   tenResponseCode enRespCode = e8FAILURE;
   tenErrorCode enErrCode = e8UNSUPPORTED_OPERATION;

   if(true == bValidateClient(u8Index))
   {
      if(true == m_poAppMngrDevBase[u8Index]->bSetAppIconAttributes(u32DevId,
         u32AppId,rfrIconAttributes))
      {
         enRespCode = e8SUCCESS;
         enErrCode = e8NO_ERRORS;
      }//if(true == m_poAppMngrDevBase[u8Index]->bSetAppIconAttributes(u32DevId,
   } //if(true == bValidateClient(u8Index))
   else
   {
      enErrCode = e8INVALID_DEV_HANDLE;
   }

   if(NULL != m_poAppMngrRespIntf)
   {
      m_poAppMngrRespIntf->vPostSetAppIconAttributesResult(enRespCode,enErrCode,
         rfrcUsrCntxt);
   }//if(NULL != m_poAppMngrRespIntf)

}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vSetVehicleConfig()
***************************************************************************/
t_Void spi_tclAppMngr::vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vSetVehicleConfig:vSetVehicleConfig-0x%x",
      enVehicleConfig));

   spi_tclMediator* poMediator = spi_tclMediator::getInstance();

   if(NULL != poMediator)
   {
      if((e8_DAY_MODE == enVehicleConfig)||(e8_NIGHT_MODE == enVehicleConfig))
      {
         poMediator->vPostDayNightMode(enVehicleConfig);
      }
   }

   for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
   {
      if(NULL != m_poAppMngrDevBase[u8Index])
      {
         m_poAppMngrDevBase[u8Index]->vSetVehicleConfig(enVehicleConfig, bSetConfig);
      }//if((NULL != m_poAppMngrDevBase[
   }//for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vOnVehicleData(const trVehicleData rfrcVehicleData)
***************************************************************************/
t_Void spi_tclAppMngr::vOnVehicleData(const trVehicleData rfrcVehicleData, t_Bool bSolicited)
{
   ETG_TRACE_USR2(("[DESC]::spi_tclAppMngr::vOnVehicleData entered with ParkBrakeActive = %d, "
            "VehMovState = %d, Solicited value = %d ", rfrcVehicleData.bParkBrakeActive,
            ETG_ENUM(VEH_MOV_STATE, rfrcVehicleData.enVehMovState), ETG_ENUM(BOOL,bSolicited)));

   if(false == bSolicited)
   {
      //! Set driving status - Else if either park brake is true or vehicle movement state is parked, then it is considered as park mode.
      //! if received status for park brake is not true and vehicle movement state is not parked,
      //! then it is considered as Drive mode.
      tenVehicleConfiguration enVehicleConfig =
         ((rfrcVehicleData.bParkBrakeActive) || (e8VEHICLE_MOVEMENT_STATE_PARKED == rfrcVehicleData.enVehMovState)) ?
         (e8PARK_MODE) : (e8DRIVE_MODE);

      //! Forward the data to corresponding AppMngr
      for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
      {
         if(NULL != m_poAppMngrDevBase[u8Index])
         {
            m_poAppMngrDevBase[u8Index]->vSetVehicleConfig(enVehicleConfig,true);
         }//if((NULL != m_poAppMngrDevBase[
      }//for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
   }
}

/***************************************************************************
** FUNCTION:  t_U32 spi_tclAppMngr::u32GetSelectedDevice()()
***************************************************************************/
t_U32 spi_tclAppMngr::u32GetSelectedDevice()
{
	ETG_TRACE_USR1(("spi_tclAppMngr::u32GetSelectedDevice-0x%x",m_u32SelectedDevice));
   return m_u32SelectedDevice;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAppMngr::bCheckAppValidity()
***************************************************************************/
t_Bool spi_tclAppMngr::bCheckAppValidity(const t_U32 cou32DevId, 
                                         const t_U32 cou32AppId,
                                         tenDeviceCategory enDevCat)
{
   t_Bool bRet=false;

   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::bCheckAppValidity:Dev-0x%x,App-0x%x,enDevCat-%d",
      cou32DevId,cou32AppId,ETG_ENUM(DEVICE_CATEGORY,enDevCat)));

   if(true == bValidateClient(u8Index))
   {
      bRet = m_poAppMngrDevBase[u8Index]->bCheckAppValidity(cou32DevId,
         cou32AppId);
   } //if(true == bValidateClient(u8Index))

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vDisplayAppcertificationInfo()
***************************************************************************/
t_Void spi_tclAppMngr::vDisplayAppcertificationInfo(const t_U32 cou32DevId,
                                                    const t_U32 cou32AppId,
                                                    tenDeviceCategory enDevCat)
{
  t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::bCheckAppValidity:Dev-0x%x,App-0x%x,u8Index-%d",
      cou32DevId,cou32AppId,u8Index));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vDisplayAppcertificationInfo(cou32DevId,
         cou32AppId);
   } //if(true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAppMngr::bSetClientProfile()
***************************************************************************/
t_Bool spi_tclAppMngr::bSetClientProfile(const t_U32 cou32DevId,
                                         const trClientProfile& corfrClientProfile,
                                         tenDeviceCategory enDevCat)
{
   t_Bool bRet = false;
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::bSetClientProfile:Dev-0x%x u8Index-%d",
      cou32DevId,u8Index));

   if(true == bValidateClient(u8Index))
   {
      bRet = m_poAppMngrDevBase[u8Index]->bSetClientProfile(cou32DevId,
         corfrClientProfile);
   } //if(true == bValidateClient(u8Index))

   return bRet;

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vSetVehicleBTAddress()
***************************************************************************/
t_Void spi_tclAppMngr::vSetVehicleBTAddress(const t_U32 cou32DevId,
                                            t_String szBTAddress,
                                            tenDeviceCategory enDevCat,
                                            const trUserContext& corfrUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vSetVehicleBTAddress:Dev-0x%x,u8Index-%d",
      cou32DevId,u8Index));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vSetVehicleBTAddress(cou32DevId,
         szBTAddress.c_str(),corfrUsrCntxt);
   } //if(true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vSetMLNotificationEnabledInfo()
***************************************************************************/
t_Void spi_tclAppMngr::vSetMLNotificationEnabledInfo(const t_U32 cou32DevId,
                                                     t_U16 u16NumNotiEnableList,
                                                     std::vector<trNotiEnable> vecrNotiEnableList,
                                                     tenDeviceCategory enDevCat,
                                                     const trUserContext& corfrUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   ETG_TRACE_USR1(("spi_tclAppMngr::vSetMLNotificationEnabledInfo:Dev-0x%x,Num.Apps-%d,u8Index-%d",
      cou32DevId,u16NumNotiEnableList,u8Index));

   //this method is meant for only MirrorLink Notifications.
   //If any request comes for DiPo Device, send error response.
   if((e8DEV_TYPE_MIRRORLINK == u8Index) && (NULL != m_poAppMngrDevBase[u8Index]))
   {
      m_poAppMngrDevBase[u8Index]->vSetMLNotificationEnabledInfo(cou32DevId,u16NumNotiEnableList,
         vecrNotiEnableList,corfrUsrCntxt);
   } //if(e8DEV_TYPE_MIRRORLINK == u8Index)
   else
   {
      //Return Error
      vCbPostSetNotiEnabledInfoResult(cou32DevId,e8INVALID_DEV_HANDLE,corfrUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vInvokeNotificationAction()
***************************************************************************/
t_Void spi_tclAppMngr::vInvokeNotificationAction(t_U32 cou32DevId,
                                                 t_U32 u32NotificationID,
                                                 t_U32 u32NotificationActionID,
                                                 tenDeviceCategory enDevCat,
                                                 const trUserContext& corfrUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   ETG_TRACE_USR1(("spi_tclAppMngr::vInvokeNotificationAction:Dev-0x%x,NoitificatioID-%d,u8Index-%d",
      cou32DevId,u32NotificationID,u8Index));
   if((e8DEV_TYPE_MIRRORLINK == u8Index) && (NULL != m_poAppMngrDevBase[u8Index]))
   {
      m_poAppMngrDevBase[u8Index]->vInvokeNotificationAction(cou32DevId,u32NotificationID,
         u32NotificationActionID,corfrUsrCntxt);
   } //if(e8DEV_TYPE_MIRRORLINK == u8Index)
   else
   {
      //Return Error
      vCbPostInvokeNotificationActionResult(cou32DevId,e8INVALID_DEV_HANDLE,corfrUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vSetMLNotificationOnOff()
***************************************************************************/
t_Void spi_tclAppMngr::vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vSetMLNotificationOnOff:bSetNotificationsOn-%d",
      ETG_ENUM(BOOL,bSetNotificationsOn)));

   //Set this setting in the Settings class and inform ML & DiPo App Mngrs
   spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
   if(NULL != poAppSettings)
   {
      poAppSettings->vSetMLNotificationOnOff(bSetNotificationsOn);
   }//if(NULL != poAppSettings)

   for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)
   {
      if(NULL != m_poAppMngrDevBase[u8Index])
      {
         m_poAppMngrDevBase[u8Index]->vSetMLNotificationOnOff(bSetNotificationsOn);
      }//if((NULL != m_poAppMngrDevBase[
   }//for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)

}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAppMngr::bGetMLNotificationEnabledInfo()
***************************************************************************/
t_Bool spi_tclAppMngr::bGetMLNotificationEnabledInfo()
{
   t_Bool bRet = false;

   ETG_TRACE_USR1(("spi_tclAppMngr::bGetMLNotificationEnabledInfo()"));

   spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
   if(NULL != poAppSettings)
   {
      bRet = poAppSettings->bGetMLNotificationEnabledInfo();
   }//if(NULL != poAppSettings)

   return bRet;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vSetRegion(.)
***************************************************************************/
t_Void spi_tclAppMngr::vSetRegion(tenRegion enRegion)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vSetRegion-%d",enRegion));

   spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
   if(NULL != poAppSettings)
   {
      poAppSettings->vSetRegion(enRegion);
   }//if(NULL != poAppSettings)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vDisplayAppListXml(.)
***************************************************************************/
t_Void spi_tclAppMngr::vDisplayAppListXml(const t_U32 cou32DevId,
                                          tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vDisplayAppListXml: Dev-0x%x ",
      cou32DevId));

   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vDisplayAppListXml(cou32DevId);
   } //if(true == bValidateClient(u8Index))
}
/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vCbNotifyAppListChange()
***************************************************************************/
t_Void spi_tclAppMngr::vCbNotifyAppListChange(const t_U32 cou32DevId,
                                              tenAppStatusInfo enAppStatus)
{
	
    /*lint -esym(40,second) second Undeclared identifier */
    ETG_TRACE_USR1(("spi_tclAppMngr::vCbNotifyAppListChange:Dev-0x%x,enAppStatus-%d",
      cou32DevId,enAppStatus));
   tenDeviceConnectionType enDevConnType=e8UNKNOWN_CONNECTION;

   m_oLock.s16Lock();
   if(SPI_MAP_NOT_EMPTY(m_mapDevConnType))
   {
      auto itConnType = m_mapDevConnType.end();
      itConnType = m_mapDevConnType.find(cou32DevId);
      if(itConnType != m_mapDevConnType.end())
      {
         enDevConnType = itConnType->second;
      }//if(itConnType != m_mapDevConnType.end())
   }//if(SPI_MAP_NOT_EMPTY(m_mapDevConnType))
   m_oLock.vUnlock();

   if(NULL != m_poAppMngrRespIntf)
   {
      m_poAppMngrRespIntf->vPostAppStatusInfo(cou32DevId,enDevConnType,
         enAppStatus);
   }//if(NULL != m_poAppMngrRespIntf)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vCbAppIconDataResult()
***************************************************************************/
t_Void spi_tclAppMngr::vCbAppIconDataResult(tenIconMimeType enMimeType,
                                            const t_U8* pcu8AppIconData, 
                                            t_U32 u32Len,
                                            const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbAppIconDataResult:enMimeType-%d",
      ETG_ENUM(ICON_MIME_TYPE,enMimeType)));

   if(NULL != m_poAppMngrRespIntf)
   {
      m_poAppMngrRespIntf->vPostAppIconDataResult(enMimeType,pcu8AppIconData,
         u32Len,rfrcUsrCntxt);
   }//if(NULL != m_poAppMngrRespIntf)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vCbTerminateAppResult()
***************************************************************************/
t_Void spi_tclAppMngr::vCbTerminateAppResult(const t_U32 cou32DevId,
                                             const t_U32 cou32AppId,
                                             tenErrorCode enErrorCode,
                                             const trUserContext& rfrCUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbTerminateAppResult:Dev-0x%x App-0x%x",
      cou32DevId,cou32AppId));

   spi_tclMediator* poMediator = spi_tclMediator::getInstance();
   if (NULL != poMediator)
   {
      poMediator->vPostTerminateAppResult(e32COMPID_APPMANAGER,cou32DevId,cou32AppId,
         enErrorCode,rfrCUsrCntxt);
   } //if(NULL != poMediator)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vCbLaunchAppResult
***************************************************************************/
t_Void spi_tclAppMngr::vCbLaunchAppResult(
   const t_U32 cou32DevId,
   const t_U32 cou32AppId,
   const tenDiPOAppType coenDiPoAppType,
   tenErrorCode enErrorCode,  
   const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbLaunchAppResult:Dev-0x%x App-0x%x",
      cou32DevId,cou32AppId));

   spi_tclMediator* poMediator = spi_tclMediator::getInstance();
   if (NULL != poMediator)
   {
      poMediator->vPostLaunchAppResult(e32COMPID_APPMANAGER,cou32DevId,cou32AppId,
         coenDiPoAppType,enErrorCode,rfrcUsrCntxt);
   } //if(NULL != poMediator)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vCbSelectDeviceResult()
***************************************************************************/
t_Void spi_tclAppMngr::vCbSelectDeviceResult(t_Bool bResult)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbSelectDeviceResult:bResult-%d",
      bResult));
   tenErrorCode enErrorCode = (true==bResult)? e8NO_ERRORS : e8SELECTION_FAILED;
   spi_tclMediator *poMediator = spi_tclMediator::getInstance();
   if(NULL != poMediator)
   {
      poMediator->vPostSelectDeviceRes(e32COMPID_APPMANAGER, enErrorCode);
   }//if(NULL != poMediator)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclAppMngr::vOnSelectDeviceResult(const t_U32 cou32DevId,
                                             const tenDeviceConnectionReq coenConnReq,
                                             const tenResponseCode coenRespCode,
                                             tenDeviceCategory enDevCat)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclAppMngr::vOnSelectDeviceResult:Dev-0x%x,coenConnReq-%d enRespCode-%d u8Index-%d",
      cou32DevId,coenConnReq,coenRespCode,u8Index));

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vOnSelectDeviceResult(cou32DevId,
         coenConnReq,coenRespCode);
   } //if(true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vCbPostSetNotiEnabledInfoResult
***************************************************************************/
t_Void spi_tclAppMngr::vCbPostSetNotiEnabledInfoResult(const t_U32 cou32DevId,
                                                       tenErrorCode enErrorCode, 
                                                       const trUserContext& rfrCUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbPostSetNotiEnabledInfoResult:Dev-0x%x enErrorCode-%d",
      cou32DevId,enErrorCode));

   if(NULL != m_poAppMngrRespIntf)
   {
      tenResponseCode enRespCode = (enErrorCode==e8NO_ERRORS)?e8SUCCESS:e8FAILURE ;
      m_poAppMngrRespIntf->vPostSetMLNotificationEnabledInfoResult(cou32DevId,enRespCode,
         enErrorCode,rfrCUsrCntxt);
   }//if(NULL != m_poAppMngrRespIntf)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vCbPostInvokeNotificationActionResult
***************************************************************************/
t_Void spi_tclAppMngr::vCbPostInvokeNotificationActionResult(const t_U32 cou32DevId,
                                                             tenErrorCode enErrorCode, 
                                                             const trUserContext& rfrCUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbPostInvokeNotificationActionResult:Dev-0x%x enErrorCode-%d",
      cou32DevId,enErrorCode));

   if(NULL != m_poAppMngrRespIntf)
   {
      tenResponseCode enRespCode = (enErrorCode==e8NO_ERRORS)?e8SUCCESS:e8FAILURE ;
      m_poAppMngrRespIntf->vPostInvokeNotificationActionResult(enRespCode,
         enErrorCode,rfrCUsrCntxt);
   }//if(NULL != m_poAppMngrRespIntf)
}


/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vCbPostNotificationResult
***************************************************************************/
t_Void spi_tclAppMngr::vCbPostNotificationResult(const t_U32 cou32DevId,
                                                 const trNotiData& corfrNotidata)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbPostNotificationResult:Dev-0x%x App-%d",
      cou32DevId,corfrNotidata.u32NotiAppID));
   if(NULL != m_poAppMngrRespIntf)
   {
      m_poAppMngrRespIntf->vPostNotificationInfo(cou32DevId,corfrNotidata.u32NotiAppID,
         corfrNotidata);
   }//if(NULL != m_poAppMngrRespIntf)
}
/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclAppMngr::vCbPostDeviceDisplayContext(t_U32 u32DeviceHandle,
                                                   t_Bool bDisplayFlag,
                                                   tenDisplayContext enDisplayContext,
                                                   const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1((" spi_tclAppMngr::vCbPostDeviceDisplayContext"));
   m_enDisplayContext = enDisplayContext;
   m_bDisplayContextStatus = bDisplayFlag;
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   if(NULL != m_poAppMngrRespIntf)
   {
     // m_poAppMngrRespIntf->vPostDeviceDisplayContext(u32DeviceHandle, bDisplayFlag, enDisplayContext, rcUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAppMngr::bIsLaunchAppReq()
***************************************************************************/
t_Bool spi_tclAppMngr::bIsLaunchAppReq(t_U32 u32DevID,
                                       t_U32 u32AppID,
                                       t_U32 u32NotificationID,
                                       t_U32 u32NotificationActionID,
                                       tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1((" spi_tclAppMngr::bIsLaunchAppReq:Dev-0x%x App-0x%x Noti-0x%x NotiAction-0x%x Cat-%d",
      u32DevID,u32AppID,u32NotificationID,u32NotificationActionID,enDevCat));

   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   t_Bool bIsLaunchReq = false;

   if(true == bValidateClient(u8Index))
   {
      bIsLaunchReq = m_poAppMngrDevBase[u8Index]->bIsLaunchAppReq(u32DevID,u32AppID,
         u32NotificationID,u32NotificationActionID);
   } //if(true == bValidateClient(u8Index))

   return bIsLaunchReq;
}


/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vCbSessionStatusUpdate()
***************************************************************************/
t_Void spi_tclAppMngr::vCbSessionStatusUpdate(const t_U32 cou32DevId,
                                              const tenDeviceCategory coenDevCat,
                                              const tenSessionStatus coenSessionStatus)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbSessionStatusUpdate:Dev-0x%x Cat-%d Status-%d",
      cou32DevId,ETG_ENUM(DEVICE_CATEGORY,coenDevCat),ETG_ENUM(SESSION_STATUS,coenSessionStatus)));
   if(NULL != m_poAppMngrRespIntf)
   {
      m_poAppMngrRespIntf->vUpdateAppBlockingInfo(cou32DevId,coenDevCat,coenSessionStatus);
   }//if(NULL != m_poAppMngrRespIntf)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppMngr::vCbActiveAppInfo()
***************************************************************************/
t_Void spi_tclAppMngr::vCbActiveAppInfo(const t_U32 cou32DevId,
                                        const tenDeviceCategory coenDevCat,
                                        const t_U32 cou32AppId,
                                        const tenAppCertificationInfo coenAppCertInfo)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vCbActiveAppInfo:Dev-0x%x Cat-%d App-0x%x CertStatus-%d",
      cou32DevId,ETG_ENUM(DEVICE_CATEGORY,coenDevCat),cou32AppId,coenAppCertInfo));
   if(NULL != m_poAppMngrRespIntf)
   {
      m_poAppMngrRespIntf->vUpdateActiveAppInfo(cou32DevId,coenDevCat,cou32AppId,coenAppCertInfo);
   }//if(NULL != m_poAppMngrRespIntf)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vSendNavigationStatus()
***************************************************************************/
t_Void spi_tclAppMngr::vSendNavigationStatus(const trNavStatusData& corfrNavStatusData)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vSendNavigationStatus entered"));

   if(NULL!= m_poAppMngrRespIntf)
   {
	   m_poAppMngrRespIntf->vPostNavigationStatus(corfrNavStatusData);
   }
} //!end of vSendNavigationStatus()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vSendNavigationNextTurnData()
***************************************************************************/
t_Void spi_tclAppMngr::vSendNavigationNextTurnData(const trNavNextTurnData& corfrNavNextTurnData)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vSendNavigationNextTurnData entered"));

   if(NULL!= m_poAppMngrRespIntf)
   {
	   m_poAppMngrRespIntf->vPostNavigationNextTurnDataStatus(corfrNavNextTurnData);
   }
} //!end of vSendNavigationNextTurnData()


/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vSendNavigationNextTurnData()
***************************************************************************/
t_Void spi_tclAppMngr::vSendNavigationNextTurnDistanceData(const trNavNextTurnDistanceData& corfrNavNextTurnDistanceData)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vSendNavigationNextTurnDistanceData entered"));

   if(NULL!= m_poAppMngrRespIntf)
   {
	   m_poAppMngrRespIntf->vPostNavigationNextTurnDistanceDataStatus(corfrNavNextTurnDistanceData);
   }
} //!end of vSendNavigationNextTurnDistanceData()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vSendNotificationData()
***************************************************************************/
t_Void spi_tclAppMngr::vSendNotificationData(const trNotificationData& corfrNotificationData)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vSendNotificationData entered"));

   if(NULL!= m_poAppMngrRespIntf)
   {
      m_poAppMngrRespIntf->vPostNotificationData(corfrNotificationData);
   }
} //!end of vSendNotificationData()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vAckNotification()
***************************************************************************/
t_Void spi_tclAppMngr::vAckNotification(const trNotificationAckData& corfrNotifAckData)
{
   ETG_TRACE_USR1(("spi_tclAppMngr::vAckNotification entered"));

   t_U8 u8Index = static_cast<t_U8>(corfrNotifAckData.enDeviceCategory);

   if(true == bValidateClient(u8Index))
   {
      m_poAppMngrDevBase[u8Index]->vAckNotification(corfrNotifAckData.u32DeviceHandle, corfrNotifAckData.szNotifId);
   } //if(true == bValidateClient(u8Index))
} //!end of vAckNotification()

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
