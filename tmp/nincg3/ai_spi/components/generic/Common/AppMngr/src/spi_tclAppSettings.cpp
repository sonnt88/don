/***********************************************************************/
/*!
* \file  spi_tclAppSettings.cpp
* \brief Class to get the App Mngr Settings
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the App Mngr Settings
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version
23.04.2014  | Shiva Kumar Gurija    | Updated with the elements for Notifications&
                                       CTS Testing
29.04.2014  | Shiva Kumar Gurija    | Changes for Locale Mapping
13.05.2014  | Ramya Murthy          | Implemented persistent Notification on/off 
                                      setting using Datapool.
21.05.2014  | Shiva Kumar Gurija    | Changes to use ML1.x or above Phones
03.07.2015  | Shiva Kumar Gurija    | improvements in ML Version checking

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "FileHandler.h"
#include "XmlDocument.h"
#include "XmlReader.h"
#include "spi_tclAppSettings.h"
#include "Datapool.h"


#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclAppSettings.cpp.trc.h"
#endif
#endif

#ifdef VARIANT_S_FTR_ENABLE_GM
static const t_Char coczGMXmlConfigFile[] = "/opt/bosch/gm/policy.xml";
#else
static const t_Char coczG3GXmlConfigFile[] = "/opt/bosch/spi/xml/policy.xml";
#endif


static const t_U32 cou32IconWidth = 0x24;
static const t_U32 cou32IconHeight = 0x24;
static const t_U32 cou32IconDepth = 0x20;

/*! 
* \brief Mapped the enum values to respective region codes as mentioned in the CCC Specifications.
* refer to the document CCC-TS-024-Mirrorlink_UPnPApplicationServerService section A_ARG_TYPE_AppCertificateInfo
* for more details.
*
* e8_INVALID is used to know that the End of Array is used
*/
static const trLocaleMap saLocaleMapping[]={{e8_WORLD,"WORLD"},{e8_EU,"EU"},{e8_EPE,"EPE"},{e8_CAN,"CAN"},{e8_USA,"USA"},
{e8_AMERICA,"AMERICA"},{e8_AUS,"AUS"},{e8_KOR,"KOR"},{e8_JPN,"JPN"},{e8_CHN,"CHN"},{e8_HKG,"HKG"},{e8_TPE,"TPE"},
{e8_IND,"IND"},{e8_APAC,"APAC"},{e8_AFRICA,"AFRICA"},{e8_INVALID,""}};


using namespace shl::xml;
/***************************************************************************
** FUNCTION:  spi_tclAppSettings::spi_tclAppSettings()
***************************************************************************/
spi_tclAppSettings::spi_tclAppSettings():m_szRegion("WORLD"),m_bFilterByLocale(false),
m_szCCCMember(""),m_bEnableContentBlocking(false),m_bNonCertAppsReq(false),
m_bEnableML10AppCertification(false),m_bEnableML11AppCertification(false),
m_bEnableCTSTest(false),m_bDisplayTestApps(false)
{
   //add code
}

/***************************************************************************
** FUNCTION:  spi_tclAppSettings::~spi_tclAppSettings()
***************************************************************************/
spi_tclAppSettings::~spi_tclAppSettings()
{
   //Add code
}

/***************************************************************************
** FUNCTION:  t_String spi_tclAppSettings::szGetCCCMemberString()
***************************************************************************/
t_String spi_tclAppSettings::szGetCCCMemberString()
{

   ETG_TRACE_USR1(("spi_tclAppSettings::szGetCCCMemberString:%s\n",
      m_szCCCMember.c_str()));

   return m_szCCCMember.c_str();
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAppSettings::bFilterByLocale()
***************************************************************************/
t_Bool spi_tclAppSettings::bFilterByLocale()
{

   ETG_TRACE_USR1(("spi_tclAppSettings::bFilterByLocale:%d\n",
      ETG_ENUM(BOOL,m_bFilterByLocale)));

   return m_bFilterByLocale;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppSettings::vSetRegion(.)
***************************************************************************/
t_Void spi_tclAppSettings::vSetRegion(tenRegion enRegion)
{
   ETG_TRACE_USR1(("spi_tclAppSettings::Region:%d\n",enRegion));
   for(t_U8 u8Index=0;saLocaleMapping[u8Index].enRegion != e8_INVALID ;u8Index++)
   {
      if(saLocaleMapping[u8Index].enRegion == enRegion)
      {
         m_oRegionLock.s16Lock();
         m_szRegion = saLocaleMapping[u8Index].szRegion.c_str();
         ETG_TRACE_USR4(("spi_tclAppSettings::Region:%s",m_szRegion.c_str()));
         m_oRegionLock.vUnlock();
         break;
      }//if(saLocaleMapping[u8Index].u8Loc
   }//for(t_U8 u8Index=0;saLoca
}

/***************************************************************************
** FUNCTION:  t_String spi_tclAppSettings::szGetRegion()
***************************************************************************/
t_String spi_tclAppSettings::szGetRegion()
{
   //ETG_TRACE_USR1(("spi_tclAppSettings::szGetRegion:%s\n",m_szLocale.c_str()));
   m_oRegionLock.s16Lock();
   t_String szRegion = m_szRegion.c_str();
   m_oRegionLock.vUnlock();

   return szRegion.c_str();
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAppSettings::m_bFilterNonCertApps()
***************************************************************************/
t_Bool spi_tclAppSettings::bEnableContentBlocking()
{
   ETG_TRACE_USR1(("spi_tclAppSettings::bEnableContentBlocking:%d\n",
      ETG_ENUM(BOOL,m_bEnableContentBlocking)));

   return m_bEnableContentBlocking;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAppSettings::bNonCertAppsReq()
***************************************************************************/
t_Bool spi_tclAppSettings::bNonCertAppsReq()
{

   ETG_TRACE_USR1(("spi_tclAppSettings::bNonCertAppsReq:%d\n",
      ETG_ENUM(BOOL,m_bNonCertAppsReq)));

   return m_bNonCertAppsReq;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAppSettings::bEnableAppCertification()
***************************************************************************/
t_Bool spi_tclAppSettings::bEnableAppCertification(const trVersionInfo& corfrVersionInfo)
{
   t_Bool bEnableAppCert = true;

   //Check the certification status for ML1.1/above devices. 
   // apply the setting of ML1.1 for  ML1.1 and above devices also.
   bEnableAppCert = (bIsML10Device(corfrVersionInfo.u32MajorVersion,corfrVersionInfo.u32MinorVersion))? 
         m_bEnableML10AppCertification : m_bEnableML11AppCertification ;

   ETG_TRACE_USR1(("spi_tclAppSettings::bEnableAppCertification:%d\n",
      ETG_ENUM(BOOL,bEnableAppCert)));

   return bEnableAppCert;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAppSettings::bEnableCTSTest()
***************************************************************************/
t_Bool spi_tclAppSettings::bEnableCTSTest()
{
   ETG_TRACE_USR1(("spi_tclAppSettings::bEnableCTSTest-%d",
      ETG_ENUM(BOOL,m_bEnableCTSTest)));

   return m_bEnableCTSTest;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppSettings::vSetMLNotificationOnOff()
***************************************************************************/
t_Void spi_tclAppSettings::vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
{
   ETG_TRACE_USR1(("spi_tclAppSettings::vSetMLNotificationOnOff:bSetNotificationsOn-%d",
      ETG_ENUM(BOOL,bSetNotificationsOn)));

   //Store setting in datapool.
   Datapool oDatapool;
   oDatapool.bWriteMLNotificationSetting(bSetNotificationsOn);
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAppSettings::bGetMLNotificationEnabledInfo()
***************************************************************************/
t_Bool spi_tclAppSettings::bGetMLNotificationEnabledInfo()
{
   //Read setting from datapool.
   Datapool oDatapool;
   t_Bool bNotificationsOn = oDatapool.bReadMLNotificationSetting();
   
   ETG_TRACE_USR1(("spi_tclAppSettings::bGetMLNotificationEnabledInfo: bNotificationsOn-%d",
   ETG_ENUM(BOOL,bNotificationsOn)));
   return bNotificationsOn;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppSettings::vReadAppSettings()
***************************************************************************/
t_Void spi_tclAppSettings::vReadAppSettings()
{
   ETG_TRACE_USR1(("spi_tclAppSettings::vReadAppSettings\n"));
   const t_Char* pczConfigFilePath = NULL;
#ifdef VARIANT_S_FTR_ENABLE_GM
   pczConfigFilePath = coczGMXmlConfigFile;
#else
   pczConfigFilePath = coczG3GXmlConfigFile;
#endif

   //Check the validity of the xml file
   spi::io::FileHandler oPolicySettingsFile(pczConfigFilePath,
      spi::io::SPI_EN_RDONLY);
   if (true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument oXmlDoc(pczConfigFilePath);
      tclXmlReader oXmlReader(&oXmlDoc, this);

      oXmlReader.bRead("APPMNGR");
   } // if (true == oPolicySettingsFile.bIsValid())
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppSettings::vDisplayAppSettings()
***************************************************************************/
t_Void spi_tclAppSettings::vDisplayAppSettings()
{
   ETG_TRACE_USR4(("vDisplayAppSettings: Filter By Locale - %d",
      ETG_ENUM(BOOL,m_bFilterByLocale)));
   ETG_TRACE_USR4(("vDisplayAppSettings: CCC Member - %s",
      m_szCCCMember.c_str()));
   ETG_TRACE_USR4(("vDisplayAppSettings: Enable Content Blocking - %d",
      ETG_ENUM(BOOL,m_bEnableContentBlocking)));
   ETG_TRACE_USR4(("vDisplayAppSettings: Filter Non certified applications - %d",
      ETG_ENUM(BOOL,m_bNonCertAppsReq)));
   ETG_TRACE_USR4(("vDisplayAppSettings: Check certification status for ML1.0 devices - %d",
      ETG_ENUM(BOOL,m_bEnableML10AppCertification)));
   ETG_TRACE_USR4(("vDisplayAppSettings: Check certification status for ML1.1 devices - %d",
      ETG_ENUM(BOOL,m_bEnableML11AppCertification)));
   ETG_TRACE_USR4(("vDisplayAppSettings: Display Certification & Test apps - %d",
      ETG_ENUM(BOOL,m_bDisplayTestApps)));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppSettings::vGetIconPreferences()
***************************************************************************/
t_Void spi_tclAppSettings::vGetIconPreferences(trIconAttributes& rfrIconAttr)
{
   rfrIconAttr.u32IconWidth = cou32IconWidth;
   rfrIconAttr.u32IconHeight = cou32IconHeight;
   rfrIconAttr.u32IconDepth = cou32IconDepth;
   rfrIconAttr.enIconMimeType = e8ICON_PNG ;
   //szUrl is not required
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAppSettings::vEnableTestApps()
***************************************************************************/
t_Void spi_tclAppSettings::vEnableTestApps(t_Bool bEnable)
{
   ETG_TRACE_USR1(("spi_tclAppSettings::vEnableTestApps - %d",
      ETG_ENUM(BOOL,bEnable)));
   m_bDisplayTestApps = bEnable;
   //add code
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAppSettings::bEnableTestApps()
***************************************************************************/
t_Bool spi_tclAppSettings::bEnableTestApps()
{
   //add code
   return m_bDisplayTestApps;
}

/*************************************************************************
** FUNCTION:  t_Bool spi_tclAppSettings::bXmlReadNode(xmlNode *poNode)
*************************************************************************/
t_Bool spi_tclAppSettings::bXmlReadNode(xmlNodePtr poNode)
{
   ETG_TRACE_USR1(("spi_tclAppSettings::bXmlReadNode()"));

   t_String szAttrName;
   t_Bool bRetVal = false;
   t_String szNodeName;

   if (NULL != poNode)
   {
      szNodeName = (const t_Char *) (poNode->name);
   } // if (NULL != poNode)

   if ("FILTERBYLOCALE" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bFilterByLocale);
      ETG_TRACE_USR2(("spi_tclAppSettings::bXmlReadNode: Filter By Locale - %d",
         ETG_ENUM(BOOL,m_bFilterByLocale)));
   } //if ("FILTERBYLOCALE" == szNodeName)
   else if ("CCCMEMBER" == szNodeName)
   {
      szAttrName = "STRING";
      bRetVal = bGetAttribute(szAttrName, poNode, m_szCCCMember);
      ETG_TRACE_USR2(("spi_tclAppSettings::bXmlReadNode: CCC Member - %s",
         m_szCCCMember.c_str()));
   } //else if ("CCCMEMBER" == szNodeName)
   else if ("ENABLECB" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bEnableContentBlocking);
      ETG_TRACE_USR2(("spi_tclAppSettings::bXmlReadNode: Enable Content Blocking - %d",
         ETG_ENUM(BOOL,m_bEnableContentBlocking)));
   } //else if ("ENABLECB" == szNodeName)
   else if ("NONCERTAPPS" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bNonCertAppsReq);
      ETG_TRACE_USR2(("spi_tclAppSettings::bXmlReadNode: Filter Non certified applications - %d",
         ETG_ENUM(BOOL,m_bNonCertAppsReq)));
   } //else if ("FILTERNONCERTAPPS" == szNodeName)
   else if ("ENABLE_APPCERT" == szNodeName)
   {
      szAttrName = "ML10";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bEnableML10AppCertification);
      ETG_TRACE_USR2(("spi_tclAppSettings::bXmlReadNode: Check certification status for ML1.0 devices - %d",
         ETG_ENUM(BOOL,m_bEnableML10AppCertification)));

      szAttrName = "ML11";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bEnableML11AppCertification);
      ETG_TRACE_USR2(("spi_tclAppSettings::bXmlReadNode: Check certification status for ML1.1 devices - %d",
         ETG_ENUM(BOOL,m_bEnableML11AppCertification)));
   } //else if ("ENABLE_APPCERT" == szNodeName)
   else if("ENABLE_CTS_TEST" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bEnableCTSTest);
      ETG_TRACE_USR2(("spi_tclAppSettings::bXmlReadNode: Filter Non certified applications - %d",
         ETG_ENUM(BOOL,m_bEnableCTSTest)));
   }
   return bRetVal;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
