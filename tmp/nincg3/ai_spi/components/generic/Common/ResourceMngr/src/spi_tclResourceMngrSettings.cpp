/***********************************************************************/
/*!
* \file  spi_tclResourceMngrSettings.cpp
* \brief Class to get the Resource Manager settings from the xml.
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Resource Manager settings from the xml
AUTHOR:         Shihabudheen P M
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
22.02.1015  | Shihabudheen P M      | Initial version.   
30.11.2015  | Shihabudheen P M      | added bGetarPlayAutoLaunchFlag()


\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Trace.h"
#include <string.h>
#include <SPITypes.h>
#include "FileHandler.h"
#include "spi_tclResourceMngrSettings.h"

#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
#include "trcGenProj/Header/spi_tclResourceMngrSettings.cpp.trc.h"
#endif
#endif

#ifdef VARIANT_S_FTR_ENABLE_GM
static const t_Char cGMXmlConfigFile[] =  "/opt/bosch/gm/policy.xml";
#else
static const t_Char cG3GXmlConfigFile[] =  "/opt/bosch/spi/xml/policy.xml";
#endif


/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngrSettings::~spi_tclResourceMngrSettings()
***************************************************************************/
spi_tclResourceMngrSettings::~spi_tclResourceMngrSettings()
{
   ETG_TRACE_USR1(("spi_tclResourceMngrSettings::~spi_tclResourceMngrSettings()"
      "entered \n"));

}

//! Private
/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngrSettings::spi_tclResourceMngrSettings()
***************************************************************************/
spi_tclResourceMngrSettings::spi_tclResourceMngrSettings():
m_StartupTimeInterval(0),
m_enCarPlayAutoLaunchFlag(e8AUTOLAUNCH_DISABLED)
{
   ETG_TRACE_USR1(("spi_tclResourceMngrSettings::spi_tclResourceMngrSettings() entered \n"));
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngrSettings::vReadSettingsValues()
***************************************************************************/
t_Void spi_tclResourceMngrSettings::vReadSettingsValues()
{
   ETG_TRACE_USR1(("spi_tclResourceMngrSettings::vReadConfigValues()"
      "entered \n"));

   t_Char* szConfigFilePath = NULL;
#ifdef VARIANT_S_FTR_ENABLE_GM
   szConfigFilePath = const_cast<t_Char*>(cGMXmlConfigFile);
#else
   szConfigFilePath = const_cast<t_Char*>(cG3GXmlConfigFile);
#endif
   spi::io::FileHandler oPolicySettingsFile(szConfigFilePath,spi::io::SPI_EN_RDONLY);
   if(true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument oXmlDoc(szConfigFilePath);
      tclXmlReader oXmlReader(&oXmlDoc, this);
      oXmlReader.bRead("RESOURCEMNGR");
   }//if(true == oPolicySettingsFile.bIsValid())

}


/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngrSettings::u32GetStartUpTimeInterval()
***************************************************************************/
t_U32 spi_tclResourceMngrSettings::u32GetStartUpTimeInterval()
{
   ETG_TRACE_USR1(("spi_tclResourceMngrSettings::u32GetStartUpTimeInterval()"
      "entered \n"));
   return m_StartupTimeInterval;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngrSettings::bGetarPlayAutoLaunchFlag()
***************************************************************************/
tenAutoLaucnhFlag spi_tclResourceMngrSettings::enGetCarPlayAutoLaunchFlag()
{
	return m_enCarPlayAutoLaunchFlag;
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngrSettings::bXmlReadNode()
***************************************************************************/
t_Bool spi_tclResourceMngrSettings::bXmlReadNode(xmlNodePtr poNode)
{
   ETG_TRACE_USR1(("spi_tclResourceMngrSettings::bXmlReadNode() entered \n"));
   
   t_Bool bRetVal=false;
   t_String szNodeName;
   
   if(NULL != poNode)
   {
      szNodeName = (const char *)(poNode->name);
   }
   if("STARTUP_TIME_INTERVAL" == szNodeName)
   {
      t_S32 u32Value = 0;
      bRetVal = bGetAttribute("VALUE", poNode, u32Value);
      m_StartupTimeInterval = (t_U32)u32Value;
      ETG_TRACE_USR1(("spi_tclResourceMngrSettings: CarPlay time intervel=%d \n", 
         m_StartupTimeInterval));
   } //if(STARTUP_TIME_INTERVAL == szNodeName)
   else if ("CARPLAY_AUTOLAUNCH_FLAG" == szNodeName)
   {
	   t_Bool bAutoLaunchFlag = false;
	   bRetVal = bGetAttribute("BOOL", poNode, bAutoLaunchFlag);
	   m_enCarPlayAutoLaunchFlag = (tenAutoLaucnhFlag)bAutoLaunchFlag;
	   ETG_TRACE_USR1(("spi_tclResourceMngrSettings: CarPlay autolaunch property read value=%d \n",
			   m_enCarPlayAutoLaunchFlag));
   }
   return bRetVal;
}