/***********************************************************************/
/*!
* \file  spi_tclResourceMngr.cpp
* \brief Main Resource Manager class that provides interface to delegate 
*        the Resource management mainly of the screen and Audio
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    
AUTHOR:         Priju K Padiyath
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
05.04.2014  | Priju K Padiyath      | Initial Version
25.04.2014  | Shihabudheen P M      | Modified for audio context handling
25.06.2014  | Shihabudheen P M      | Adapted to the CarPlay design changes
20.03.2015  | Shiva Kumar G         | updated with the elements to Android Auto
20.03.2015  | Ramya Murthy          | Included AAP ResourceManager
25.06.2015  | Tejaswini HB          |Featuring out Android Auto

15.06.2015  | Shihabudheen P M      | added vSetVehicleBTAddress()
10.07.2015  | Shihabudheen P M      | Removed vOnCarPlayPluginLoaded.

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclMediator.h"
#include "spi_tclRespInterface.h"
#include "spi_tclAppMngrRespIntf.h"
#include "spi_tclResorceMngrDefines.h"
#include "spi_tclResourceMngrBase.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
#include "spi_tclAAPResourceMngr.h"
#endif
#include "spi_tclDiPOResourceMngr.h"
#include "spi_tclResourceMngr.h"
#include "spi_tclMediator.h"
#include "spi_tclResourceMngrSettings.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
#include "trcGenProj/Header/spi_tclResourceMngr.cpp.trc.h"
#endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclResourceMngr::spi_tclResourceMngr()
***************************************************************************/
spi_tclResourceMngr::spi_tclResourceMngr(spi_tclResourceMngrResp* poRespIntf) :
m_poRsrcMngrRespIntf(poRespIntf), m_bDisplayContextStatus(false), m_enDisplayContext(e8DISPLAY_CONTEXT_UNKNOWN),
m_bAudioPlayFlag(false), m_u8AudioContext(0), m_u32SelectedDevice(0), m_enDevCat(e8DEV_TYPE_UNKNOWN)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_rCurrAppState.enSpeechAppState = e8SPI_SPEECH_UNKNOWN;
   m_rCurrAppState.enPhoneAppState = e8SPI_PHONE_UNKNOWN;
   m_rCurrAppState.enNavAppState = e8SPI_NAV_UNKNOWN;

   for (t_U8 u8Index = 0; u8Index < NUM_RSRCMNGR_CLIENTS; u8Index++)
   {
      m_poRsrcMngrBase[u8Index] = NULL;
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAppMngr::~spi_tclResourceMngr()
***************************************************************************/
spi_tclResourceMngr::~spi_tclResourceMngr()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   m_poRsrcMngrRespIntf    = NULL;
   m_bDisplayContextStatus = false;
   m_enDisplayContext      = e8DISPLAY_CONTEXT_UNKNOWN;
   m_enDevCat              = e8DEV_TYPE_UNKNOWN;
   m_bAudioPlayFlag        = false;
   m_u8AudioContext        = 0;
   m_u32SelectedDevice     = 0;

   m_rCurrAppState.enSpeechAppState = e8SPI_SPEECH_UNKNOWN;
   m_rCurrAppState.enPhoneAppState = e8SPI_PHONE_UNKNOWN;
   m_rCurrAppState.enNavAppState = e8SPI_NAV_UNKNOWN;

   for (t_U8 u8Index = 0; u8Index < NUM_RSRCMNGR_CLIENTS; u8Index++)
   {
      m_poRsrcMngrBase[u8Index] = NULL;
   }
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclResourceMngr::bInitialize()
***************************************************************************/
t_Bool spi_tclResourceMngr::bInitialize()
{
   t_Bool bRet = false;

   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));


   m_poRsrcMngrBase[e8DEV_TYPE_DIPO] = new spi_tclDiPOResourceMngr();
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
   m_poRsrcMngrBase[e8DEV_TYPE_ANDROIDAUTO] = new spi_tclAAPResourceMngr();
   SPI_NORMAL_ASSERT(NULL == m_poRsrcMngrBase[e8DEV_TYPE_ANDROIDAUTO]);
#endif

   trRsrcSettings rRsrcSettings;
   spi_tclResourceMngrSettings *poRsrcMangrConf = 
      spi_tclResourceMngrSettings::getInstance();
   if(NULL != poRsrcMangrConf)
   {
      poRsrcMangrConf->vReadSettingsValues();
      rRsrcSettings.u32StartTimeInterval = 
         poRsrcMangrConf->u32GetStartUpTimeInterval();
      rRsrcSettings.enCPlayAutoLaunchFlag =
    		  poRsrcMangrConf->enGetCarPlayAutoLaunchFlag();

   }

   for(t_U8 u8Index = 0; u8Index < NUM_RSRCMNGR_CLIENTS; u8Index++)
   {
      if(NULL != m_poRsrcMngrBase[u8Index])
      {
         bRet = m_poRsrcMngrBase[u8Index]->bInitialize() || bRet;
         m_poRsrcMngrBase[u8Index]->vUpdateInitialSettings(rRsrcSettings);
      }//if(NULL != m_poRsrcMngrBase[u8Index])
   }
   //Register for callbacks
   vRegisterCallbacks();

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngr::vUnInitialize()
***************************************************************************/
t_Bool spi_tclResourceMngr::bUnInitialize()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetVal = true;

   for (t_U8 u8Index = 0; u8Index < NUM_RSRCMNGR_CLIENTS; u8Index++)
   {
      if (NULL != m_poRsrcMngrBase[u8Index])
      {
         m_poRsrcMngrBase[u8Index]->vUnInitialize();
      } //if((NULL != m_poVideoDevBase[
      RELEASE_MEM(m_poRsrcMngrBase[u8Index]);
   } //for(t_U8 u8Index=0;u8Index < NUM_APPMNGR_CLIENTS; u8Index++)

   m_poRsrcMngrRespIntf = NULL;
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngr::vUnInitialize()
***************************************************************************/
t_Void spi_tclResourceMngr::vOnSPISelectDeviceResult(t_U32 u32ProjectionDevHandle,
                                                       tenDeviceCategory enDevCat,
                                                       tenDeviceConnectionReq enDeviceConnReq,
                                                       tenResponseCode enRespCode,
                                                       tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   if((e8SUCCESS == enRespCode) && (e8DEVCONNREQ_SELECT == enDeviceConnReq))
   {
      m_u32SelectedDevice = u32ProjectionDevHandle;
      m_enDevCat = enDevCat;
   }

   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   if ((true == bValidateClient(u8Index)) && (NULL != m_poRsrcMngrBase[u8Index]))
   {
      m_poRsrcMngrBase[u8Index]->vOnSPISelectDeviceResult(u32ProjectionDevHandle,
         enDeviceConnReq, enRespCode, enErrorCode);
   }//if ((true == bValidateClient(u8Index)) &&...)
}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclResourceMngr::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclResourceMngr::vRegisterCallbacks()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   trRsrcMngrCallback rRsrcMngrCallback;

   /*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
   /*lint -esym(40,_1) _1 identifier */
   /*lint -esym(40,fvPostDeviceDisplayContext)fvPostDeviceDisplayContext Undeclared identifier */
   /*lint -esym(40,_2) _2 Undeclared identifier */
   /*lint -esym(40,_3 ) _3 Undeclared identifier */
   /*lint -esym(40,fvPostDeviceAudioContext ) fvPostDeviceAudioContext Undeclared identifier */
   /*lint -esym(40,fvPostDeviceAppState) fvPostDeviceAppState Undeclared identifier */
   /*lint -esym(40,fvUpdateSessionStatus) fvUpdateSessionStatus Undeclared identifier */
   /*lint -esym(40,fpvDeviceAuthAndAccessCb)fpvDeviceAuthAndAccessCb Undeclared identifier */


   rRsrcMngrCallback.fvPostDeviceDisplayContext =
      std::bind(&spi_tclResourceMngr::vCbPostDeviceDisplayContext,
      this,
      std::placeholders::_1,
      std::placeholders::_2,
      std::placeholders::_3);

   rRsrcMngrCallback.fvPostDeviceAudioContext =
      std::bind(&spi_tclResourceMngr::vCbDeviceAudioContext,
      this,
      std::placeholders::_1,
      std::placeholders::_2,
      std::placeholders::_3);

   rRsrcMngrCallback.fvPostDeviceAppState = 
      std::bind(&spi_tclResourceMngr::vCbDeviceAppState,
      this,
      std::placeholders::_1,
      std::placeholders::_2,
      std::placeholders::_3,
      std::placeholders::_4);

   rRsrcMngrCallback.fvUpdateSessionStatus = 
      std::bind(&spi_tclResourceMngr::vCbUpdateSessionStatus,
      this,
      std::placeholders::_1,
      std::placeholders::_2,
      std::placeholders::_3);

   rRsrcMngrCallback.fvSetDeviceAppState =
      std::bind(&spi_tclResourceMngr::vCbSetDeviceAppState,
      this,
      std::placeholders::_1,
      std::placeholders::_2,
      std::placeholders::_3);

   rRsrcMngrCallback.fpvDeviceAuthAndAccessCb = std::bind(
      &spi_tclResourceMngr::vDevAuthAndAccessInfoCb,this,
      SPI_FUNC_PLACEHOLDERS_2);

   for (t_U8 u8Index = 0; u8Index < NUM_RSRCMNGR_CLIENTS; u8Index++)
   {
      if (NULL != m_poRsrcMngrBase[u8Index])
      {
         m_poRsrcMngrBase[u8Index]->vRegRsrcMngrCallBack(rRsrcMngrCallback);
      }//if (NULL != m_poRsrcMngrBase[u8Index])
   }// end of for
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngr::vSetAccessoryDisplayContext()
***************************************************************************/
t_Void spi_tclResourceMngr::vSetAccessoryDisplayContext(const t_U32 cou32DevId,
                                                        t_Bool bDisplayFlag, 
                                                        tenDisplayContext enDisplayContext,
                                                        tenDeviceCategory enDevCat,
                                                        const trUserContext& rfrcUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   ETG_TRACE_USR4(("spi_tclResourceMngr::vSetAccessoryDisplayContext:Device ID-0x%x,DevCat-%d,Accesssory Takes Focus-%d ,AccDisplayContext = %d\n", 
      cou32DevId,u8Index,bDisplayFlag,enDisplayContext));

   if (true == bValidateClient(u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vSetAccessoryDisplayContext(cou32DevId,
         bDisplayFlag,
         enDisplayContext,
         rfrcUsrCntxt);
   } //if(true == bValidateClient(u8Index))

   if((NULL != m_poRsrcMngrBase[e8DEV_TYPE_DIPO]) &&
		   (e8DEV_TYPE_DIPO != enDevCat))
   {
	  //@Note: This  message needs to pass to the DiPOResource manager always,
	  //Since it mandatory to keep track of the latest update from HMI.
	  m_poRsrcMngrBase[e8DEV_TYPE_DIPO]->vSetAccessoryDisplayContext(cou32DevId,
	     bDisplayFlag,
	     enDisplayContext,
	     rfrcUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceMngr::vSetAccessoryDisplayMode(t_U32...
***************************************************************************/
t_Void spi_tclResourceMngr::vSetAccessoryDisplayMode(const t_U32 cu32DeviceHandle,
                                                     const tenDeviceCategory enDevCat,
                                                     const trDisplayContext corDisplayContext,
                                                     const trDisplayConstraint corDisplayConstraint,
                                                     const tenDisplayInfo coenDisplayInfo)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   ETG_TRACE_USR1(("spi_tclResourceMngr::vSetAccessoryDisplayMode() entered \n"));

   if (true == bValidateClient(u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vSetAccessoryDisplayMode(cu32DeviceHandle,
         corDisplayContext,
         corDisplayConstraint, 
         coenDisplayInfo);
   } //if(true == bValidateClient(u8Index))

   if((NULL != m_poRsrcMngrBase[e8DEV_TYPE_DIPO]) &&
		   (e8DEV_TYPE_DIPO != enDevCat))
   {
	  //@Note: This  message needs to pass to the DiPOResource manager always,
	  //Since it mandatory to keep track of the latest update from HMI.
	  m_poRsrcMngrBase[e8DEV_TYPE_DIPO]->vSetAccessoryDisplayMode(cu32DeviceHandle,
		    corDisplayContext,
			corDisplayConstraint,
			coenDisplayInfo);
   }
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vSetAccessoryAudioContext()
***************************************************************************/
t_Void spi_tclResourceMngr::vSetAccessoryAudioContext(const t_U32 cou32DevId, 
                                                      const tenAudioContext coenAudioCntxt,
                                                      t_Bool bReqFlag, 
                                                      tenDeviceCategory enSelDevCat,
                                                      const trUserContext& rfrcUsrCntxt,
                                                      tenDeviceCategory enRequestedDevCat)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(enSelDevCat);

   t_U8 u8Index = static_cast<t_U8>(enRequestedDevCat);
   // Need to update the audio context updates to CarPlay plugin even if CarPlay is not active.
   // This information needs to keep to send the device upon session startup.
   // @Note: Audio context updates should be sent only to requested DeviceCategory handler,
   // since audio context changes are updated differently for different technologies.
   if (true == bValidateClient(u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vSetAccessoryAudioContext(cou32DevId, coenAudioCntxt, bReqFlag, rfrcUsrCntxt);
   }
}


/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vSetAccessoryAppState()
***************************************************************************/
t_Void spi_tclResourceMngr::vSetAccessoryAppState(tenDeviceCategory enDevCat, 
                                                  const tenSpeechAppState enSpeechAppState, 
                                                  const tenPhoneAppState enPhoneAppState,
                                                  const tenNavAppState enNavAppState, 
                                                  const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   // TODO - update all resource managers always, even when there is no active device

   // Need to update the app state updates to CarPlay plugin even if CarPlay is not active.
   // This information needs to keep to send the device upon session startup.
   if(NULL != m_poRsrcMngrBase[e8DEV_TYPE_DIPO])
   {
      m_poRsrcMngrBase[e8DEV_TYPE_DIPO]->vSetAccessoryAppState(enSpeechAppState, enPhoneAppState, enNavAppState, rfrcUsrCntxt);
   }

   if((true == bValidateClient(u8Index)) &&(e8DEV_TYPE_DIPO != u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vSetAccessoryAppState(enSpeechAppState, enPhoneAppState, enNavAppState, rfrcUsrCntxt);
   }// if(true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::bValidateClient()
***************************************************************************/
inline t_Bool spi_tclResourceMngr::bValidateClient(t_U8 u8Index)
{
   //Assert if the Index is greater than the array size
   SPI_NORMAL_ASSERT( u8Index > NUM_RSRCMNGR_CLIENTS);

   t_Bool bRet = (u8Index < NUM_RSRCMNGR_CLIENTS)
      && (NULL != m_poRsrcMngrBase[u8Index]);

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vCbPostDeviceDisplayContext()
***************************************************************************/
t_Void spi_tclResourceMngr::vCbPostDeviceDisplayContext(t_Bool bDisplayFlag,
                                                        tenDisplayContext enDisplayContext,
                                                        const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_bDisplayContextStatus = bDisplayFlag;
   m_enDisplayContext = enDisplayContext;
   if(NULL != m_poRsrcMngrRespIntf)
   {
      m_poRsrcMngrRespIntf->vPostDeviceDisplayContext(m_u32SelectedDevice, bDisplayFlag, enDisplayContext, rcUsrCntxt);
   }//if(NULL != m_poRsrcMngrRespIntf)
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vGetDisplayContextInfo()
***************************************************************************/
t_Void spi_tclResourceMngr::vGetDisplayContextInfo(t_U32 u32DeviceHandle,
                                                   t_Bool& rfbDisplayFlag,
                                                   tenDisplayContext& rfenDisplayContext)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   rfbDisplayFlag = m_bDisplayContextStatus;
   rfenDisplayContext = m_enDisplayContext;
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vCbDeviceAudioContext()
***************************************************************************/
t_Void spi_tclResourceMngr::vCbDeviceAudioContext(t_Bool bPlayFlag, t_U8 u8AudioCntxt,
                                                  const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_bAudioPlayFlag = bPlayFlag;
   m_u8AudioContext = u8AudioCntxt;

   if(NULL != m_poRsrcMngrRespIntf)
   {
      m_poRsrcMngrRespIntf->vPostDeviceAudioContext(m_u32SelectedDevice, bPlayFlag, u8AudioCntxt, rcUsrCntxt);
   } //if(NULL != m_poRsrcMngrRespIntf)
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vGetAudioContextInfo()
***************************************************************************/
t_Void spi_tclResourceMngr::vGetAudioContextInfo(t_U32 u32DeviceHandle,
                                                 t_Bool& rfbPlayFlag,
                                                 t_U8 rfu8AudioCntxt)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   rfbPlayFlag = m_bAudioPlayFlag;
   rfu8AudioCntxt = m_u8AudioContext;
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vCbDeviceAppState()
***************************************************************************/
t_Void spi_tclResourceMngr::vCbDeviceAppState(tenSpeechAppState enSpeechAppState,
                                              tenPhoneAppState enPhoneAppState,
                                              tenNavAppState enNavAppState,
                                              const trUserContext rUserContext)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //Change the app states only for valid states and not for UNKNOWN states.
   /*m_rCurrAppState.enSpeechAppState =
            ((e8SPI_SPEECH_UNKNOWN != enSpeechAppState)? enSpeechAppState:m_rCurrAppState.enSpeechAppState);
   m_rCurrAppState.enPhoneAppState  =
            ((e8SPI_PHONE_UNKNOWN != enPhoneAppState)?enPhoneAppState:m_rCurrAppState.enPhoneAppState);
   m_rCurrAppState.enNavAppState    =
            ((e8SPI_NAV_UNKNOWN != enNavAppState)?enNavAppState:m_rCurrAppState.enNavAppState);*/

   if(NULL != m_poRsrcMngrRespIntf)
   {
      m_poRsrcMngrRespIntf->vPostDeviceAppState(enSpeechAppState, enPhoneAppState, enNavAppState, rUserContext);
   }//if(NULL != m_poRsrcMngrRespIntf)
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vCbSetDeviceAppState()
***************************************************************************/
t_Void spi_tclResourceMngr::vCbSetDeviceAppState(tenSpeechAppState enSpeechAppState,
                                              tenPhoneAppState enPhoneAppState,
                                              tenNavAppState enNavAppState)
{
   ETG_TRACE_USR1((" Resource Manager Set Device App states - Speech: %d, Phone: %d, Nav: %d , Dev Category: %d\n",
            ETG_ENUM(SPEECH_APP_STATE,enSpeechAppState), ETG_ENUM(PHONE_APP_STATE,enPhoneAppState) ,
            ETG_ENUM(NAV_APP_STATE, enNavAppState), ETG_ENUM(DEVICE_CATEGORY, m_enDevCat)));

   t_U8 u8Index = static_cast<t_U8>(m_enDevCat);

   if ((true == bValidateClient(u8Index)) && (NULL != m_poRsrcMngrBase[u8Index]))
   {
      m_poRsrcMngrBase[u8Index]->vAcquireDevAppStateLock();

      //Change the app states only for valid states and not for UNKNOWN states.
      m_rCurrAppState.enSpeechAppState =
            ((e8SPI_SPEECH_UNKNOWN != enSpeechAppState)? enSpeechAppState:m_rCurrAppState.enSpeechAppState);
      m_rCurrAppState.enPhoneAppState  =
            ((e8SPI_PHONE_UNKNOWN != enPhoneAppState)? enPhoneAppState:m_rCurrAppState.enPhoneAppState);
      m_rCurrAppState.enNavAppState    =
            ((e8SPI_NAV_UNKNOWN != enNavAppState)? enNavAppState:m_rCurrAppState.enNavAppState);

      m_poRsrcMngrBase[u8Index]->vReleaseDevAppStateLock();
   }
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vCbDeviceAppState()
***************************************************************************/
t_Void spi_tclResourceMngr::vCbUpdateSessionStatus(t_U32 u32DeviceHandle,
                              tenDeviceCategory enDevCat,
                              tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != m_poRsrcMngrRespIntf)
   {
      m_poRsrcMngrRespIntf->vUpdateSessionStatusInfo(u32DeviceHandle, enDevCat, enSessionStatus);
   }//if(NULL != m_poRsrcMngrRespIntf)
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vGetAppStateInfo()
***************************************************************************/
t_Void spi_tclResourceMngr::vGetAppStateInfo(tenSpeechAppState &enSpeechAppState, 
                                             tenPhoneAppState &enPhoneAppState, 
                                             tenNavAppState &enNavAppState)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   t_U8 u8Index = static_cast<t_U8>(m_enDevCat);

   if ((true == bValidateClient(u8Index)) && (NULL != m_poRsrcMngrBase[u8Index]))
   {
      m_poRsrcMngrBase[u8Index]->vAcquireDevAppStateLock();

      enSpeechAppState =  m_rCurrAppState.enSpeechAppState;
      enPhoneAppState = m_rCurrAppState.enPhoneAppState ;
      enNavAppState = m_rCurrAppState.enNavAppState;

      m_poRsrcMngrBase[u8Index]->vReleaseDevAppStateLock();
   }
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vOnModeChanged()
***************************************************************************/
t_Void spi_tclResourceMngr::vOnModeChanged(const t_U32 cou32DeviceHandle, trDiPOModeState & rfoDiPOModeState)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_U8 u8Index = static_cast<t_U8>(m_enDevCat);
   ETG_TRACE_USR1(("Device Category : %d", m_enDevCat));

   if(true == bValidateClient(u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vOnModeChanged(cou32DeviceHandle, rfoDiPOModeState);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceMngr::vOnAudioMsg()
***************************************************************************/
t_Void spi_tclResourceMngr::vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioAllocMsg &rfrAudioAllocMsg)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_U8 u8Index = static_cast<t_U8>(m_enDevCat);

   if(true == bValidateClient(u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vOnAudioMsg(cou32DeviceHandle, rfrAudioAllocMsg);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceMngr::vOnAudioMsg()
***************************************************************************/
t_Void spi_tclResourceMngr::vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioDuckMsg &rfrAudioDuckMsg)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_U8 u8Index = static_cast<t_U8>(m_enDevCat);

   if(true == bValidateClient(u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vOnAudioMsg(cou32DeviceHandle, rfrAudioDuckMsg);
   }
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vOnRequestUI()
***************************************************************************/
t_Void spi_tclResourceMngr::vOnRequestUI(t_Bool bRenderStatus)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_U8 u8Index = static_cast<t_U8>(m_enDevCat);

   if(true == bValidateClient(u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vOnRequestUI(bRenderStatus);
   }
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclResourceMngr::vOnSessionMsg()
***************************************************************************/
t_Void spi_tclResourceMngr::vOnSessionMsg(tenDiPOSessionState enDiPOSessionState)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if(NULL != m_poRsrcMngrBase[e8DEV_TYPE_DIPO])
   {
      //! This info needs to pass to the CarPlay resource manager 
	  // inorder to handle the Plugin loaded state.
   	  m_poRsrcMngrBase[e8DEV_TYPE_DIPO]->vOnSessionMsg(enDiPOSessionState);
   }
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngr::vSetVehicleBTAddress()
***************************************************************************/
t_Void spi_tclResourceMngr::vSetVehicleBTAddress(t_String szBtAddress)
{
	//@Note : This function
	//! Needs to invoke irrespective of the session status.
	if(NULL != m_poRsrcMngrBase[e8DEV_TYPE_DIPO])
	{
	    m_poRsrcMngrBase[e8DEV_TYPE_DIPO]->vSetVehicleBTAddress(szBtAddress);
	}//if(NULL != m_poRsrcMngrBase[e8DEV_TYPE_DIPO])
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclResourceMngr::vSetFeatureRestrictions(...)
 ***************************************************************************/
t_Void spi_tclResourceMngr::vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
      const t_U8 cou8ParkModeRestrictionInfo,const t_U8 cou8DriveModeRestrictionInfo)
{
   //! This info is required by CarPlay resource manager 
   if ((e8DEV_TYPE_DIPO == enDevCategory) && (NULL != m_poRsrcMngrBase[e8DEV_TYPE_DIPO]))
   {
      m_poRsrcMngrBase[e8DEV_TYPE_DIPO]->vSetFeatureRestrictions(cou8ParkModeRestrictionInfo,
            cou8DriveModeRestrictionInfo);
   }//if ((e8DEV_TYPE_DIPO == enDevCategory) && (NULL != m_poRsrcMngrBase[e8DEV_TYPE_DIPO]))
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclResourceMngr::vLaunchApp()
***************************************************************************/
t_Void spi_tclResourceMngr::vLaunchApp(t_U32 u32DevId,
                                       t_U32 u32AppId,
                                       tenDeviceCategory enDevCat)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   if(true == bValidateClient(u8Index))
   {
      m_poRsrcMngrBase[u8Index]->vLaunchApp(u32DevId,u32AppId);
   }//if(true == bValidateClient(u8Index))

}

/***************************************************************************
** FUNCTION: t_Void spi_tclResourceMngr::vDevAuthAndAccessInfoCb()
***************************************************************************/
t_Void spi_tclResourceMngr::vDevAuthAndAccessInfoCb(const t_U32 cou32DevId,
                                            const t_Bool cobHandsetInteractionReqd)
{
   ETG_TRACE_USR1(("spi_tclResourceMngr::vDevAuthAndAccessInfoCb:Device ID-0x%x Handset Interaction Required-%d",
      cou32DevId,ETG_ENUM(BOOL,cobHandsetInteractionReqd)));

   if(NULL != m_poRsrcMngrRespIntf)
   {
      m_poRsrcMngrRespIntf->vUpdateDevAuthAndAccessInfo(cou32DevId, cobHandsetInteractionReqd);
   }//if(NULL != m_poVideoRespIntf)
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
