/***********************************************************************/
/*!
* \file  spi_tclResourceMngr.h
* \brief Main Resource Manager class that provides interface to delegate 
*        the Resource management mainly of the screen and Audio
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    
AUTHOR:         Priju K Padiyath
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
05.04.2014  | Priju K Padiyath      | Initial Version
17.04.2014  | Shihabudheen P M      | Modofied for display context response
19.06.2014  | Shihabudheen P M      | Updated for App state resource management
25.06.2014  |  Shihabudheen P M     | Adapted to the CarPlay design changes
31.07.2014  |  Ramya Murthy         | SPI feature configuration via LoadSettings()
15.06.2015  | Shihabudheen P M      | added vSetVehicleBTAddress()

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLRESOURCEMNGR_
#define _SPI_TCLRESOURCEMNGR_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "DiPOTypes.h"
#include "AAPTypes.h"
#include "spi_tclLifeCycleIntf.h"
/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
class spi_tclResourceMngrBase;
class spi_tclResourceMngrResp;

/****************************************************************************/
/*!
* \class spi_tclResourceMngr
* \brief Main Resource Manager class that provides interface to delegate 
*        the handling of resources like the screen and the audio
****************************************************************************/
class spi_tclResourceMngr : public spi_tclLifeCycleIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngr::spi_tclResourceMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclResourceMngr(spi_tclRsrcMngrRespIntf* poRespIntf)
   * \brief   Parameterized Constructor
   * \sa      ~spi_tclResourceMngr()
   **************************************************************************/
   spi_tclResourceMngr(spi_tclResourceMngrResp* poRespIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngr::~spi_tclResourceMngr()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclResourceMngr()
   * \brief   Destructor
   * \sa      spi_tclResourceMngr(spi_tclRsrcMngrRespIntf* poRespIntf)
   **************************************************************************/
   ~spi_tclResourceMngr();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclResourceMngr::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the resource Manager related classes
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngr::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief   To Ininitialize all the resource Manager related classes
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclResourceMngr::vLoadSettings(const trSpiFeatureSupport&...)
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
    * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
   {
      SPI_INTENTIONALLY_UNUSED(rfcrSpiFeatureSupp);
   }

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngr::vSaveSettings()
   ***************************************************************************/
   /*!
   * \fn      vSaveSettings()
   * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
   * \sa      vLoadSettings()
   **************************************************************************/
   t_Void vSaveSettings()
   {
   }

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngr::vOnSPISelectDeviceResult(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResult(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq,
   *             tenResponseCode enRespCode, tenErrorCode enErrorCode)
   * \brief   Called when SelectDevice operation is complete & with the result
   * 			 of the operation.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enDevCat : Device category
   * \param   [IN] enDeviceConnReq: Connection request type for the device
   * \param   [IN] enRespCode: Response code enumeration
   * \param   [IN] enErrorCode: Error code enumeration
   * \retval  None
   **************************************************************************/
   t_Void vOnSPISelectDeviceResult(t_U32 u32ProjectionDevHandle,
      tenDeviceCategory enDevCat,
      tenDeviceConnectionReq enDeviceConnReq,
      tenResponseCode enRespCode,
      tenErrorCode enErrorCode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngr::vSetAccessoryDisplayContext()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
   *       t_Bool bDisplayFlag, tenDisplayContext enDisplayContext,
   *       tenDeviceCategory enDevCat, const trUserContext& rfrcUsrCntxt)
   *
   * \brief   To send accessory display context related info .
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   bDisplayFlag : [IN] Request flag
   * \pram    enDisplayContext : [IN] Display context
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \pram    rfrcUsrCntxt: [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
      t_Bool bDisplayFlag, 
      tenDisplayContext enDisplayContext,
      tenDeviceCategory enDevCat, 
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vSetAccessoryDisplayMode(t_U32...
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryDisplayMode()
   * \brief  Accessory display mode update request.
   * \param  [IN] cu32DeviceHandle      : Uniquely identifies the target Device.
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \param  [IN] corDisplayContext : Display context info
   * \param  [IN] corDisplayConstraint : DiDisplay constraint info
   * \param  [IN] coenDisplayInfo       : Display info flag
   * \sa
   **************************************************************************/
   t_Void vSetAccessoryDisplayMode(const t_U32 cu32DeviceHandle,
      const tenDeviceCategory enDevCat,
      const trDisplayContext corDisplayContext,
      const trDisplayConstraint corDisplayConstraint,
      const tenDisplayInfo coenDisplayInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngr::vSetAccessoryAudioContext()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetAccessoryAudioContext(const t_U32 cou32DevId, const t_U8 cu8AudioCntxt,
   *   t_Bool bReqFlag, tenDeviceCategory enDevCat, const trUserContext& rfrcUsrCntxt)
   *
   * \brief   To send accessory display context related info .
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cu8AudioCntxt : [IN] Audio Source number/base channel number
   * \pram    bReqFlag : [IN] Request flag, true for request and false for release
   * \pram    enDevCat    : [IN] Identifies the Connection Request.
   * \pram    rfrcUsrCntxt: [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryAudioContext(const t_U32 cou32DevId, const tenAudioContext coenAudioCntxt,
      t_Bool bReqFlag, tenDeviceCategory enDevCat, const trUserContext& rfrcUsrCntxt,
      tenDeviceCategory enDevCategory);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngr::vSetAccessoryAppState()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetAccessoryAppState(tenSpeechAppState enSpeechAppState, tenPhoneAppState enPhoneAppState,
   *         tenNavAppState enNavAppState
   * \brief   To set accessory app state realated info.
   * \pram    enSpeechAppState: [IN] Accessory speech state.
   * \param   enPhoneAppState : [IN] Accessory phone state
   * \pram    enNavAppState   : [IN] Accessory navigation app state
   * \pram    rfrcUsrCntxt    : [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryAppState(tenDeviceCategory enDevCat, const tenSpeechAppState enSpeechAppState, 
      const tenPhoneAppState enPhoneAppState, const tenNavAppState enNavAppState, const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vCbPostDeviceDisplayContext
   **                   (t_U32 u32DeviceHandle, tenDisplayContext enDisplayContext,..)
   ***************************************************************************/
   /*!
   * \fn     vCbPostDeviceDisplayContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  This function get called when there is a display context update
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bDisplayFlag     : TRUE � Start Display Projection, FALSE � Stop Display Projection.
   * \param  [IN] enDisplayContext : Display context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   * \sa
   **************************************************************************/
   t_Void vCbPostDeviceDisplayContext(t_Bool bDisplayFlag,
      tenDisplayContext enDisplayContext,
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vGetDisplayContextInfo
   **                   (t_U32 u32DeviceHandle, tenDisplayContext enDisplayContext,..)
   ***************************************************************************/
   /*!
   * \fn     vGetDisplayContextInfo(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,)
   * \brief  To get the dipslay context info
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [OUT] rfbDisplayFlag  : TRUE � Start Display Projection, FALSE � Stop Display Projection.
   * \param  [OUT] rfenDisplayContext : Display context of the projected device.
   * \sa
   **************************************************************************/
   t_Void vGetDisplayContextInfo(t_U32 u32DeviceHandle,
      t_Bool& rfbDisplayFlag,
      tenDisplayContext& rfenDisplayContext);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vCbPostDeviceDisplayContext
   **                   (t_U32 u32DeviceHandle, tenDisplayContext enDisplayContext,..)
   ***************************************************************************/
   /*!
   * \fn     vCbPostDeviceDisplayContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  This function get called when there is a display context update
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bPlayFlag        : TRUE � Start CarPlay playback, FALSE � native playback
   * \param  [IN] u8AudioCntxt     : Audio vontext
   * \param  [IN] rcUsrCntxt       : User Context Details.
   * \sa
   **************************************************************************/
   t_Void vCbDeviceAudioContext(t_Bool bPlayFlag, t_U8 u8AudioCntxt,
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vGetAudioContextInfo()
   ***************************************************************************/
   /*!
   * \fn     vGetAudioContextInfo(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,)
   * \brief  To get the dipslay context info
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [OUT] rfbPlayFlag     : TRUE � Start CarPlay playback, FALSE otherwise
   * \param  [OUT] rfu8AudioCntxt : Audio context of the projected device.
   * \sa
   **************************************************************************/
   t_Void vGetAudioContextInfo(t_U32 u32DeviceHandle,
      t_Bool& rfbPlayFlag, t_U8 rfu8AudioCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vCbDeviceAppState()
   ***************************************************************************/
   /*!
   * \fn     vCbDeviceAppState(tenSpeechAppState enSpeechAppState,...)
   * \brief  To post the device app states.
   * \param  [IN] enSpeechAppState: Speech state
   * \param  [IN] enPhoneAppState : Phone app state
   * \param  [IN] enNavAppState   : Nav App state
   * \param  [IN] rcUsrCntxt      : User Context Details.
   * \sa
   **************************************************************************/
   t_Void vCbDeviceAppState( tenSpeechAppState enSpeechAppState,
      tenPhoneAppState enPhoneAppState, tenNavAppState enNavAppState,
      const trUserContext rUserContext);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vCbUpdateSessionStatus()
   ***************************************************************************/
   /*!
   * \fn     vCbUpdateSessionStatus(t_U32 u32DeviceHandle, tenDeviceCategory enDevCat,...)
   * \brief  To update session info
   * \param  [IN] u32DeviceHandle  : Device handle.
   * \param  [IN] enDevCat         : Device category.
   * \param  [IN] enSessionStatus  : Session status
   * \sa
   **************************************************************************/
   t_Void vCbUpdateSessionStatus(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vCbSetDeviceAppState()
   ***************************************************************************/
   /*!
   * \fn     vCbSetDeviceAppState(tenSpeechAppState enSpeechAppState,
   *           tenPhoneAppState enPhoneAppState, tenNavAppState enNavAppState)
   * \brief  To set the device app states.
   * \param  [IN] enSpeechAppState: Speech state
   * \param  [IN] enPhoneAppState : Phone app state
   * \param  [IN] enNavAppState   : Nav App state
   * \param  [IN] rcUsrCntxt      : User Context Details.
   * \sa
   **************************************************************************/
   t_Void vCbSetDeviceAppState( tenSpeechAppState enSpeechAppState,
      tenPhoneAppState enPhoneAppState, tenNavAppState enNavAppState);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vGetAppStateInfo()
   ***************************************************************************/
   /*!
   * \fn     vGetAppStateInfo()
   * \brief  To get the device app state info.
   * \param  [OUT] enSpeechAppState  : Speech App state
   * \param  [OUT] enPhoneAppState   : Phone App state
   * \param  [OUT] enNavAppState     : Navigation App state
   * \sa
   **************************************************************************/
   t_Void vGetAppStateInfo(tenSpeechAppState &enSpeechAppState, 
      tenPhoneAppState &enPhoneAppState, 
      tenNavAppState &enNavAppState);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vOnModeChanged()
   ***************************************************************************/
   /*!
   * \fn     vOnModeChanged()
   * \brief  Function used to get the mode changed information from device
   * \param  [IN] rDiPOModeState  : DiPO mode state
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \sa
   **************************************************************************/
   t_Void vOnModeChanged(const t_U32 cou32DeviceHandle, trDiPOModeState & rfoDiPOModeState);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vOnAudioMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnAudioMsg()
   * \brief  Function used to get post audio information from device
   * \param  [IN] rfrAudioAllocMsg  : Audio info message
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \sa
   **************************************************************************/
   t_Void vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioAllocMsg &rfrAudioAllocMsg);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vOnAudioMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnAudioMsg()
   * \brief  Function used to get Audio duck information from device
   * \param  [IN] rfrAudioDuckMsg  : Audio Duck message
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \sa
   **************************************************************************/
   t_Void vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioDuckMsg &rfrAudioDuckMsg);

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngr::vOnRequestUI()
   ***************************************************************************/
   /*!
   * \fn     vOnRequestUI()
   * \brief  Function to update the RequestUI command from the device
   * \param  bRenderStatus : [IN]Render status, true to render native HMI.
   * \param  Note: bRenderStatus will always be true now.
   **************************************************************************/
   t_Void vOnRequestUI(t_Bool bRenderStatus);

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngr::vOnSessionMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnSessionMsg()
   * \brief  Function to update the session updates from the device
   * \param  enDiPOSessionState : [IN] Session state
   **************************************************************************/
   t_Void vOnSessionMsg(tenDiPOSessionState enDiPOSessionState);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngr::vSetVehicleBTAddress()
   ***************************************************************************/
   /*!
   * \fn      vSetVehicleBTAddress(t_Bool bLocDataAvailable)
   * \brief   Interface to update the vehicle BT address info update.
   * \param   szBtAddress: [IN] BT address.
   * \retval  None
   **************************************************************************/
   t_Void vSetVehicleBTAddress(t_String szBtAddress);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclResourceMngr::vSetFeatureRestrictions()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
    *          const t_U8 cou8ParkModeRestrictionInfo,
    *          const t_U8 cou8DriveModeRestrictionInfo)
    * \brief   Method to set Vehicle Park/Drive Mode Restriction
    * \param   enDevCategory : Device Category
    *          cou8ParkModeRestrictionInfo : Park mode restriction
    *          cou8DriveModeRestrictionInfo : Drive mode restriction
    * \retval  t_Void
    **************************************************************************/
   t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
         const t_U8 cou8ParkModeRestrictionInfo,const t_U8 cou8DriveModeRestrictionInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngr::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchApp(t_U32 u32DevId,
   *                 ct_U32 u32AppId,
   *                 tenDeviceCategory enDevCat)
   * \brief   To Launch the Video for the requested app 
   * \pram    u32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    u32AppId  : [IN] Application Id
   * \pram    enDevCat  : [IN] Identifies the Connection Request.
   * \retval  t_Void
   **************************************************************************/
   t_Void vLaunchApp(t_U32 u32DevId,
      t_U32 u32AppId,
      tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngr::vDevAuthAndAccessInfoCb()
   ***************************************************************************/
   /*!
   * \fn     t_Void vDevAuthAndAccessInfoCb(const t_U32 cou32DevId,
   *            const t_Bool cobHandsetInteractionReqd)
   * \brief  method to update the authorization and access to AAP projection device
   * \param  cou32DevId                 : [IN] Uniquely identifies the target Device.
   * \param  cobHandsetInteractionReqd  : [IN] Set if interaction required on Handset
   * \retval t_Void
   **************************************************************************/
   t_Void vDevAuthAndAccessInfoCb(const t_U32 cou32DevId,
            const t_Bool cobHandsetInteractionReqd);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/
private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngr::spi_tclResourceMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclResourceMngr()
   * \brief   Constructor
   * \sa      ~spi_tclResourceMngr()
   **************************************************************************/
   spi_tclResourceMngr();

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngr& spi_tclResourceMngr::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclResourceMngr& operator= (const spi_tclResourceMngr &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclResourceMngr& operator=(const spi_tclResourceMngr &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngr::spi_tclResourceMngr(const spi_tclResourceMngr..
   ***************************************************************************/
   /*!
   * \fn      spi_tclResourceMngr(const spi_tclResourceMngr &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclResourceMngr(const spi_tclResourceMngr &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclResourceMngr::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterCallbacks()
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo Resource Mngr
   * \retval  t_Void 
   **************************************************************************/
   t_Void vRegisterCallbacks();

   /***************************************************************************
   ** FUNCTION:  t_Bool  spi_tclResourceMngr::bValidateClient()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bValidateClient(t_U8 u8Index)
   * \brief   To validate the client index. check whether it is in the range of
   *          the Resource Manager clients Array
   * \retval  t_Bool
   **************************************************************************/
   inline t_Bool bValidateClient(t_U8 u8Index);

   //! Member Variable - pointer to the Base class for ML & DiPo Resource Managers
   spi_tclResourceMngrBase* m_poRsrcMngrBase[NUM_RSRCMNGR_CLIENTS];

   //! AppMngr Response Intf
   spi_tclResourceMngrResp* m_poRsrcMngrRespIntf;

   //!Display Context status 
   t_Bool m_bDisplayContextStatus;

   //!Display Context  
   tenDisplayContext m_enDisplayContext;

   //! Audio render flag
   t_Bool m_bAudioPlayFlag;

   //! Audio context of projected device
   t_U8 m_u8AudioContext;

   //! Current App state
   trDiPOAppState m_rCurrAppState;

   //! Selected device handle
   t_U32 m_u32SelectedDevice;

   //! selected device category
   tenDeviceCategory m_enDevCat;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/
};
//spi_tclResourceMngr

#endif //_SPI_TCLRESOURCEMNGR_
