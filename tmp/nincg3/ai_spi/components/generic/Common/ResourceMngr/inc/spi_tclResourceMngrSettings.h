/***********************************************************************/
/*!
* \file  spi_tclResourceMngrSettings.h
* \brief Class to get the Resource Manager settings from the xml.
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Resource Manager settings from the xml
AUTHOR:         Shihabudheen P M
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
22.02.1015  | Shihabudheen P M      | Initial version.   


\endverbatim
*************************************************************************/

#ifndef _SPI_TCLRESOURCEMNGRSETTINGS_H_
#define _SPI_TCLRESOURCEMNGRSETTINGS_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#include "GenericSingleton.h"
#include "XmlDocument.h"
#include "Xmlable.h"
#include "XmlReader.h"
#include "DiPOTypes.h"


using namespace shl::xml;

/****************************************************************************/
/*!
* \class spi_tclResourceMngrSettings
* \brief Class to get the Resource Manager settings from the xml config
****************************************************************************/

class spi_tclResourceMngrSettings:public GenericSingleton<spi_tclResourceMngrSettings>,
   public tclXmlReadable
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrSettings::~spi_tclResourceMngrSettings()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclResourceMngrSettings()
   * \brief   Destructor
   * \sa      spi_tclResourceMngrSettings()
   **************************************************************************/
   ~spi_tclResourceMngrSettings();

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrSettings::vReadSettingsValues()
   ***************************************************************************/
   /*!
   * \fn      vReadSettingsValues()
   * \brief   Function to start read the resource manager config values
   * \param   NONE
   * \retVal  NONE
   * \sa      
   **************************************************************************/
   t_Void vReadSettingsValues();

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrSettings::u32GetStartUpTimeInterval()
   ***************************************************************************/
   /*!
   * \fn      u32GetStartUpTimeInterval()
   * \brief   Function to get the CarPlay session startup time interval
   * \param   NONE
   * \retVal  t_U32 : Time interval in milli seconds.
   * \sa      
   **************************************************************************/
   t_U32 u32GetStartUpTimeInterval();

   /***************************************************************************
    ** FUNCTION:  spi_tclResourceMngrSettings::enGetCarPlayAutoLaunchFlag()
    ***************************************************************************/
    /*!
    * \fn      bGetCarPlayAutoLaunchFlag()
    * \brief   Function to get the CarPlay auto launch enable flag
    * \param   NONE
    * \retVal  tenAutoLaucnhFlag : Flag(True/False)
    * \sa
    **************************************************************************/
   tenAutoLaucnhFlag enGetCarPlayAutoLaunchFlag();

   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<spi_tclResourceMngrSettings>;


private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrSettings::spi_tclResourceMngrSettings()
   ***************************************************************************/
   /*!
   * \fn      spi_tclResourceMngrSettings()
   * \brief   Default Constructor
   * \sa      ~spi_tclResourceMngrSettings()
   **************************************************************************/
   spi_tclResourceMngrSettings();

   /*************************************************************************
   ** FUNCTION:  virtual spi_tclResourceMngrSettings::bXmlReadNode(xmlNode *poNode)
   *************************************************************************/
   /*!
   * \fn     virtual t_bool bXmlReadNode(xmlNode *poNode)
   * \brief  virtual function to read data from a xml node
   * \param  poNode : [IN] pointer to xml node
   * \retval bool : true if success, false otherwise.
   *************************************************************************/
   virtual t_Bool bXmlReadNode(xmlNodePtr poNode);


   t_U32 m_StartupTimeInterval;

   //! Indicates whether CarPlay automatic launch requirement after device selection.
   tenAutoLaucnhFlag m_enCarPlayAutoLaunchFlag;

};

#endif
