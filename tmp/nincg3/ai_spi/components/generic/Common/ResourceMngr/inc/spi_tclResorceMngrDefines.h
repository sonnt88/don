
/*!
 *******************************************************************************
 * \file    spi_tclResorceMngrDefines.h
 * \brief   SPI resource Manager Defines
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   SPI resource Manager Defines
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       | Author                       | Modifications
 05.04.2014 | Priju K Padiyath             | Initial Version
 18.04.2014 | Shihabudheen P M             | Updated for response message handling 
 25.10.2014 | Shihabudheen P M             | updated for session status update

 \endverbatim
 ******************************************************************************/
#ifndef _SPI_TCLRSRCMNGRDEFINES_H_
#define _SPI_TCLRSRCMNGRDEFINES_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <functional>
#include "SPITypes.h"
#include "DiPOTypes.h"

//! function signature for callback functions from msgrcvr to dipo resource mngr
typedef std::function<t_Void(t_Bool)> tfvOnRequetUI;
typedef std::function<t_Void(trAudioAllocMsg*)> tfvAudioAllocation;
typedef std::function<t_Void(trDiPOModeState)> tfvOnModeChanged;
typedef std::function<t_Void(t_Double, t_Double)> tfvOnAudioDuckReq;
typedef std::function<t_Void(tenDiPOSessionState)> tfvOnSessionUpdate;

//! structure to hold the function callbacks. 
struct trDiPOResMngrCallback
{
   tfvOnRequetUI fvOnRequestUI;

   tfvAudioAllocation fvAudioAllocation;

   tfvOnModeChanged  fvOnModeChanged;

   tfvOnAudioDuckReq fvOnAudioDuckReq;

   tfvOnSessionUpdate fvOnSessionUpdate;

   trDiPOResMngrCallback():
      fvOnRequestUI(NULL),
      fvAudioAllocation(NULL),
      fvOnModeChanged(NULL),
      fvOnAudioDuckReq(NULL),
      fvOnSessionUpdate(NULL)
   {
   }
};

//!function signature for callback functions from DiPO/ML resource manager to common  resource manager
typedef std::function<t_Void(t_Bool, tenDisplayContext, const trUserContext)> tfvPostDeviceDisplayContext;
typedef std::function<t_Void(t_Bool, t_U8, const trUserContext)> tfvPostDeviceAudioContext;
typedef std::function<t_Void(tenSpeechAppState, tenPhoneAppState, tenNavAppState, const trUserContext)>
   tfvPostDeviceAppState;
typedef std::function<t_Void()> tfvPostInfoMsgRequest;
typedef std::function<t_Void(t_U32, tenDeviceCategory, tenSessionStatus)> tfvUpdateSessionStatus;
typedef std::function<t_Void(tenSpeechAppState, tenPhoneAppState, tenNavAppState)> tfvSetDeviceAppState;

/*!
* \typedef - To update the device authorization and Access state
*/
typedef std::function<t_Void(const t_U32,const t_Bool)> tDeviceAuthAndAccess;
struct trRsrcMngrCallback
{
   tfvPostDeviceDisplayContext fvPostDeviceDisplayContext;

   tfvPostDeviceAudioContext fvPostDeviceAudioContext;

   tfvPostDeviceAppState fvPostDeviceAppState;

   tfvPostInfoMsgRequest fvPostInfoMsgRequest;

   tfvUpdateSessionStatus fvUpdateSessionStatus;

   tfvSetDeviceAppState fvSetDeviceAppState;

   tDeviceAuthAndAccess      fpvDeviceAuthAndAccessCb;

   trRsrcMngrCallback() : fvPostDeviceDisplayContext(NULL),
      fvPostDeviceAudioContext(NULL),
      fvPostDeviceAppState(NULL),
      fvPostInfoMsgRequest(NULL),
      fvUpdateSessionStatus(NULL),
      fvSetDeviceAppState(NULL),
      fpvDeviceAuthAndAccessCb(NULL)
   {
   }
};

#endif //_SPI_TCLRSRCMNGRDEFINES_H_
