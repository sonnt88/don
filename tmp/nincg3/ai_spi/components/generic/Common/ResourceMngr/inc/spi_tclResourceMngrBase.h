/***********************************************************************/
/*!
* \file  spi_tclResourceMngrBase.h
* \brief Base class for MirrorLink and DiPo Resource Mngr 
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for MirrorLink and DiPo Resource Mngr 
AUTHOR:         Priju K Padiyath
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
05.04.2014  | Priju K Padiyath      | Initial Version
20.03.2015  | Shiva Kumar G         | updated with the elements to Android Auto
15.06.2015  | Shihabudheen P M      | added vSetVehicleBTAddress()
10.07.2015  | Shihabudheen P M		| Removed vOnCarPlayPluginLoaded.


\endverbatim
*************************************************************************/
#ifndef _SPI_TCL_RSRCMNGRBASE_
#define _SPI_TCL_RSRCMNGRBASE_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <list>
#include "Lock.h"
#include "SPITypes.h"
#include "spi_tclResorceMngrDefines.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

struct trRsrcSettings
{
   t_U32 u32StartTimeInterval;

   tenAutoLaucnhFlag enCPlayAutoLaunchFlag;

   trRsrcSettings():u32StartTimeInterval(0), enCPlayAutoLaunchFlag(e8AUTOLAUNCH_DISABLED)
   {
   }

};
/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclResourceMngrBase
* \brief Base class for MirrorLink and DiPo Resource Manager 
****************************************************************************/
class spi_tclResourceMngrBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrBase::spi_tclResourceMngrBase()
   ***************************************************************************/
   /*!
   * \fn      spi_tclResourceMngrBase()
   * \brief   Default Constructor
   * \sa      ~spi_tclResourceMngrBase()
   **************************************************************************/

   spi_tclResourceMngrBase()
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrBase::~spi_tclResourceMngrBase()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclResourceMngrBase()
   * \brief   Destructor
   * \sa      spi_tclResourceMngrBase()
   **************************************************************************/
   virtual ~spi_tclResourceMngrBase()
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclResourceMngrBase::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the Resource Manager related classes
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   virtual t_Bool bInitialize()
   {
      return true;
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngrBase::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief   To Uninitialize all the Resource Manager related classes
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   virtual t_Void vUnInitialize()
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclResourceMngrBase::vRegRsrcMngrCallBack()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegRsrcMngrCallBack()
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo Resource Manager
   * \param   rRsrcMngrCallback : [IN] Resource Manager callbacks structure
   * \retval  t_Void 
   **************************************************************************/
   virtual t_Void vRegRsrcMngrCallBack(trRsrcMngrCallback rRsrcMngrCallback) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngrBase::vSetAccessoryDisplayContext()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
   *        t_Bool bDisplayFlag, tenDisplayContext enDisplayContext)
   * \brief   To send accessory display context related info .
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   enDevConnType : [IN] Device connection Type USB/WIFI
   * \pram    rfrcUsrCntxt: [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
      t_Bool bDisplayFlag, tenDisplayContext enDisplayContext,
      const trUserContext& rfrcUsrCntxt) = 0;


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngrBase::vSetAccessoryDisplayMode(t_U32...
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryDisplayMode()
   * \brief  Accessory display mode update request.
   * \param  [IN] cu32DeviceHandle      : Uniquely identifies the target Device.
   * \param  [IN] corDisplayContext     : Display context info
   * \param  [IN] corDisplayConstraint  : DiDisplay constraint info
   * \param  [IN] coenDisplayInfo       : Display info flag
   * \sa
   **************************************************************************/
   virtual t_Void vSetAccessoryDisplayMode(const t_U32 cu32DeviceHandle,
      const trDisplayContext corDisplayContext,
      const trDisplayConstraint corDisplayConstraint,
      const tenDisplayInfo coenDisplayInfo) 
	  {
	  }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngrBase::vSetAccessoryAudioContext()
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryAudioContext(const t_U32 cou32DevId, const t_U8 cu8AudioCntxt
   *       t_Bool bDisplayFlag, const trUserContext& rfrcUsrCntxt)
   * \brief   To send accessory audio context related info .
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cu8AudioCntxt : [IN] Audio Context
   * \pram    bReqFlag : [IN] Request/ Release flag
   * \pram    rfrcUsrCntxt: [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSetAccessoryAudioContext(const t_U32 cou32DevId, 
      const tenAudioContext coenAudioCntxt,
      t_Bool bReqFlag, const trUserContext& rfrcUsrCntxt) = 0;

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngrBase::vSetAccessoryAppState()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetAccessoryAppState(tenSpeechAppState enSpeechAppState, 
   *         tenPhoneAppState enPhoneAppState,
   *         tenNavAppState enNavAppState
   *
   * \brief   To set accessory app state realated info.
   * \pram    enSpeechAppState: [IN] Accessory speech state.
   * \param   enPhoneAppState : [IN] Accessory phone state
   * \pram    enNavAppState   : [IN] Accessory navigation app state
   * \pram    rfrcUsrCntxt    : [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSetAccessoryAppState(const tenSpeechAppState enSpeechAppState, 
      const tenPhoneAppState enPhoneAppState,
      const tenNavAppState enNavAppState, 
      const trUserContext& rfrcUsrCntxt) = 0;

  /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclResourceMngrBase::vOnSPISelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSPISelectDeviceResult()
    * \brief   Interface to receive result of SPI device selection/deselection
   * \param   u32DevID : [IN] Resource Manager callbacks structure.
   * \param   enDeviceConnReq : [IN] Select/ deselect.
   * \param   enRespCode : [IN] Response code (success/failure)
   * \param   enErrorCode : [IN] Error
   * \retval  t_Void 
   **************************************************************************/
   virtual t_Void vOnSPISelectDeviceResult(t_U32 u32DevID,
         tenDeviceConnectionReq enDeviceConnReq,
         tenResponseCode enRespCode,
         tenErrorCode enErrorCode) = 0;

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrBase::vOnModeChanged()
   ***************************************************************************/
   /*!
   * \fn     vCbOnModeChanged()
   * \brief  function to update the device state 
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \param  rfoDiPOModeState : [IN] Device context data 
   **************************************************************************/
   virtual t_Void vOnModeChanged(const t_U32 cou32DeviceHandle, trDiPOModeState &rfoDiPOModeState) {};

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngrBase::vOnAudioMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnAudioMsg()
   * \brief  Function used to get post audio information from device
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \param  [IN] rfrAudioAllocMsg  : Audio info message
   * \sa
   **************************************************************************/
   virtual t_Void vOnAudioMsg(const t_U32 cou32DeviceHandle, 
      trAudioAllocMsg &rfrAudioAllocMsg) {};

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngrBase::vOnAudioMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnAudioMsg()
   * \brief  Function used to get audio duck information from device
   * \param  [IN] cou32DeviceHandle  : Device Handle
   * \param  [IN] rfrAudioDuckMsg  : Audio Duck message
   * \sa
   **************************************************************************/
   virtual t_Void vOnAudioMsg(const t_U32 cou32DeviceHandle, trAudioDuckMsg &rfrAudioDuckMsg) {};

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrBase::vOnRequestUI()
   ***************************************************************************/
   /*!
   * \fn     vOnRequestUI()
   * \brief  function to update the RequestUI command from the device
   * \param  bRenderStatus : [IN]Render status, true to render native HMI.
   * \param  Note: bRenderStatus will always be true now.
   **************************************************************************/
   virtual t_Void vOnRequestUI(t_Bool bRenderStatus)  {}

  /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrBase::vOnSessionMsg()
   ***************************************************************************/
   /*!
   * \fn     vOnSessionMsg()
   * \brief  function to update the RequestUI command from the device
   * \param  enDiPOSessionState : [IN] Session state
   **************************************************************************/
   virtual t_Void vOnSessionMsg(tenDiPOSessionState enDiPOSessionState)  {}



   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrBase::vUpdateInitialSettings()
   ***************************************************************************/
   /*!
   * \fn     vUpdateInitialSettings()
   * \brief  Function to set the initial settings
   * \param  rRsrcSettings : [IN] Settings
   * \
   **************************************************************************/
   virtual t_Void vUpdateInitialSettings(trRsrcSettings rRsrcSettings)  {}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngrBase::vSetVehicleBTAddress()
   ***************************************************************************/
   /*!
   * \fn      vSetVehicleBTAddress(t_Bool bLocDataAvailable)
   * \brief   Interface to update the vehicle BT address info update.
   * \param   szBtAddress: [IN] BT address.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetVehicleBTAddress(t_String szBtAddress){}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclResourceMngrBase::vSetFeatureRestrictions()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSetFeatureRestrictions(const t_U8 cou8ParkModeRestrictionInfo,
    *          const t_U8 cou8DriveModeRestrictionInfo)
    * \brief   Method to set Vehicle Park/Drive Mode Restriction
    *          cou8ParkModeRestrictionInfo : Park mode restriction
    *          cou8DriveModeRestrictionInfo : Drive mode restriction
    * \retval  t_Void
    **************************************************************************/
   virtual t_Void vSetFeatureRestrictions(const t_U8 cou8ParkModeRestrictionInfo,
         const t_U8 cou8DriveModeRestrictionInfo) {}
		 
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngr::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchApp(t_U32 u32DevId,
   *                 ct_U32 u32AppId)
   * \brief   To Launch the Video for the requested app 
   * \pram    u32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    u32AppId  : [IN] Application Id
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vLaunchApp(t_U32 u32DevId,t_U32 u32AppId){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngrBase::vAcquireDevAppStateLock()
   ***************************************************************************/
   /*!
   * \fn      vAcquireDevAppStateLock()
   * \brief   Function to acquire Device App state lock.
   * \param   None
   * \retval  None
   **************************************************************************/
   t_Void vAcquireDevAppStateLock()
   {
      m_oDevAppStateLock.s16Lock();
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclResourceMngrBase::vReleaseDevAppStateLock()
   ***************************************************************************/
   /*!
   * \fn      vReleaseDevAppStateLock()
   * \brief   Function to release Device App state lock.
   * \param   None
   * \retval  None
   **************************************************************************/
   t_Void vReleaseDevAppStateLock()
   {
      m_oDevAppStateLock.vUnlock();
   }

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   //! Lock for App states
   Lock   m_oDevAppStateLock;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/
};
//spi_tclResourceMngrBase

#endif //_SPI_TCL_RSRCMNGRBASE_
