/*!
 *******************************************************************************
 * \file             spi_tclFactory.h
 * \brief            Object Factory class responsible for creation of SPI Objects
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Object Factory class responsible for creation of SPI Objects
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 13.02.2014 |  Shihabudheen P M            | Added 1.poGetMainAppInstance().
                                                  2.vInitialize()  
 04.03.2014 |  Shihabudheen P M            | Added    1.poGetIPCMsgQInstance()
                                             Modified 1.spi_tclFactory()  
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation  
 31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()   

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLMANAGER_H_
#define SPI_TCLMANAGER_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"
//@todo - Issue - bpstl ambiguous usage should be removed

#include "GenericSingleton.h"
#include "spi_tclConnMngr.h"
#include "spi_tclAppMngr.h"
#include "spi_tclVideo.h"
#include "spi_tclAudio.h"
#include "spi_tclInputHandler.h"
#include "spi_tclBluetooth.h"
#include "spi_tclDataService.h"
#include "spi_tclLifeCycleIntf.h"
#include "spi_tclResourceMngr.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

//!Forward declaration
class spi_tclRespInterface;

/*!
 * \class spi_tclConnSettings
 * \brief Response  to HMI  from ConnMngr class
 */

class spi_tclFactory: public GenericSingleton<spi_tclFactory>, public spi_tclLifeCycleIntf
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclFactory::~spi_tclFactory
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclFactory()
       * \brief  Destructor
       **************************************************************************/
      ~spi_tclFactory();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclFactory::bInitialize()
       ***************************************************************************/
      /*!
       * \fn      bInitialize()
       * \brief   Method to Initialize
       * \sa      bUnInitialize()
       **************************************************************************/
      virtual t_Bool bInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclFactory::bUnInitialize()
       ***************************************************************************/
      /*!
       * \fn      bUnInitialize()
       * \brief   Method to UnInitialize
       * \sa      bInitialize()
       **************************************************************************/
      virtual t_Bool bUnInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclFactory::vLoadSettings(const trSpiFeatureSupport&...)
       ***************************************************************************/
      /*!
       * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
       * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
       * \sa      vSaveSettings()
       **************************************************************************/
      virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclFactory::vSaveSettings()
       ***************************************************************************/
      /*!
       * \fn      vSaveSettings()
       * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
       * \sa      vLoadSettings()
       **************************************************************************/
      virtual t_Void vSaveSettings();

      /***************************************************************************
       ** FUNCTION:  spi_tclConnMngr* spi_tclFactory::poGetConnMngrInstance
       ***************************************************************************/
      /*!
       * \fn     spi_tclConnMngr* poGetConnMngrInstance()
       * \brief  returns connection manager instance
       **************************************************************************/
      spi_tclConnMngr* poGetConnMngrInstance();

      /***************************************************************************
       ** FUNCTION:  spi_tclVideo* spi_tclFactory::poGetVideoInstance
       ***************************************************************************/
      /*!
       * \fn     spi_tclVideo* poGetVideoInstance()
       * \brief  returns Video instance
       **************************************************************************/
      spi_tclVideo* poGetVideoInstance();

      /***************************************************************************
       ** FUNCTION:  spi_tclMLAppMngr* spi_tclFactory::poGetAppManagerInstance
       ***************************************************************************/
      /*!
       * \fn     spi_tclMLAppMngr* poGetAppManagerInstance()
       * \brief  returns appmanager instance
       **************************************************************************/
      spi_tclAppMngr* poGetAppManagerInstance();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudio* spi_tclFactory::poGetAudioInstance
       ***************************************************************************/
      /*!
       * \fn     spi_tclAudio* poGetAudioInstance()
       * \brief   returns audio instance
       **************************************************************************/
      spi_tclAudio* poGetAudioInstance();

      /***************************************************************************
       ** FUNCTION:  spi_tclInputHandler* spi_tclFactory::poGetInputHandlerInstance
       ***************************************************************************/
      /*!
       * \fn     spi_tclInputHandler* poGetInputHandlerInstance()
       * \brief  returns Input Handler instance
       **************************************************************************/
      spi_tclInputHandler* poGetInputHandlerInstance();
	  
      /***************************************************************************
       ** FUNCTION:  spi_tclBluetooth* spi_tclFactory::poGetBluetoothInstance
       ***************************************************************************/
      /*!
       * \fn     spi_tclBluetooth* poGetBluetoothInstance()
       * \brief   returns Bluetooth instance
       **************************************************************************/
      spi_tclBluetooth* poGetBluetoothInstance();

      /***************************************************************************
      ** FUNCTION:  spi_tclDataService* spi_tclFactory::poGetDataServiceInstance
      ***************************************************************************/
      /*!
       * \fn     spi_tclDataService* poGetDataServiceInstance()
       * \brief   returns Data Service instance
       **************************************************************************/
      spi_tclDataService* poGetDataServiceInstance();

      /***************************************************************************
       ** FUNCTION:  ahl_tclBaseOneThreadApp* spi_tclFactory::poGetMainAppInstance
       ***************************************************************************/
      /*!
       * \fn     ahl_tclBaseOneThreadApp* poGetMainAppInstance()
       * \brief  returns Main application instance.
       **************************************************************************/
      ahl_tclBaseOneThreadApp* poGetMainAppInstance();
	  
      /***************************************************************************
       ** FUNCTION:  spi_tclResourceMngr* spi_tclFactory::poGetRsrcMngrInstance
       ***************************************************************************/
      /*!
       * \fn     spi_tclResourceMngr* poGetRsrcMngrInstance()
       * \brief  returns Resource manager instance
       **************************************************************************/
       spi_tclResourceMngr* poGetRsrcMngrInstance();
   private:
      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/
      friend class GenericSingleton<spi_tclFactory>;

      /***************************************************************************
       ** FUNCTION:  spi_tclFactory::spi_tclFactory
       ***************************************************************************/
      /*!
       * \fn     spi_tclFactory()
       * \brief  Default Constructor
       **************************************************************************/
      spi_tclFactory();

      /***************************************************************************
       ** FUNCTION:  spi_tclFactory::spi_tclFactory
       ***************************************************************************/
      /*!
       * \fn     spi_tclFactory()
       * \brief  Parameterized  Constructor
       * \param  poRespInterface: pointer to response interface
       * \param  poMainAppl: pointer to main application
       **************************************************************************/
      spi_tclFactory(spi_tclRespInterface* poRespInterface,
               ahl_tclBaseOneThreadApp* poMainAppl);

      /*!
       * Pointer to tResponse interface class object
       */
      spi_tclRespInterface *m_poRespInterface;

      /*!
       * Pointer to connection manager class object
       */
      spi_tclConnMngr *m_poConnMngr;

      /*!
      * spi_tclVideo member variable
      */
      spi_tclVideo* m_poVideo;

      /*!
      * spi_tclAppManager member variable
      */
      spi_tclAppMngr* m_poAppMngr;

     /*!
      * spi_tclAudio member variable
      */
      spi_tclAudio* m_poAudio;

      /*!
       * spi_tclInputHandler member variable
       */
      spi_tclInputHandler* m_poInputHandler;
	  
      /*!
       * spi_tclBluetooth member variable
       */
      spi_tclBluetooth* m_poBluetooth;
	  
      /*!
       * spi_tclDataService member variable
       */
      spi_tclDataService* m_poDataService;

      /*!
       * spi main app pointer
       */
      ahl_tclBaseOneThreadApp* m_poMainApp;

      /*!
       * Resource manager member variable
       */
      spi_tclResourceMngr* m_poRsrcMngr;
};

#endif /* SPI_TCLMANAGER_H_ */
