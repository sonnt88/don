/*!
 *******************************************************************************
 * \file              spi_tclInputHandler.cpp
 * \brief             Main SPI input handler 
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Main input handler that creates ML and DiPo Input handlers
 AUTHOR:        Hari Priya E R (RBEI/ECP2)
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.11.2013 |  Hari Priya E R              | Initial Version
 12.03.2014 |  Hari Priya E R              | Included changes for handling ML and DiPO 
                                             Input handlers based on Device Type
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
 03.07.2014 |  Hari Priya E R              | Added changes related to Input Response interface
 31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()
 13.03.2015 |  Sameer Chandra			   | SelectDevice Implementation for AAP.
 25.06.2015 |  Tejaswini HB (RBEI/ECP2)    | Featuring out Android Auto
 17.07.2015 |  Sameer Chandra              | Added Knob Encoder Handling.

 
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/


#include "spi_tclInputHandler.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
#include "spi_tclMLInputHandler.h"
#endif
#include "spi_tclDiPoInputHandler.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
#include "spi_tclAAPInputHandler.h"
#endif
#include "spi_tclInputDevBase.h"
#include "spi_tclMediator.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_INPUTHANDLER
#include "trcGenProj/Header/spi_tclInputHandler.cpp.trc.h"
#endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
** FUNCTION:  spi_tclInputHandler::spi_tclInputHandler()
***************************************************************************/
spi_tclInputHandler::spi_tclInputHandler(spi_tclInputRespIntf* poInputRespIntf):
m_poInputRespIntf(poInputRespIntf)
{
	ETG_TRACE_USR1(("spi_tclInputHandler::spi_tclInputHandler entered \n"));

   for(t_U8 u8Index=0; u8Index<NUM_INPUT_CLIENTS;u8Index++)
   {
      m_poInputDevBase[u8Index] = NULL;
   }//for()
}

/***************************************************************************
 ** FUNCTION:  spi_tclInputHandler::~spi_tclInputHandler()
 ***************************************************************************/
spi_tclInputHandler::~spi_tclInputHandler()
{
   ETG_TRACE_USR1(("spi_tclInputHandler::~spi_tclInputHandler entered \n"));

   for(t_U8 u8Index=0;u8Index < NUM_INPUT_CLIENTS; u8Index++)
   {
      RELEASE_MEM(m_poInputDevBase[u8Index]);
   }
   m_poInputRespIntf = NULL;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclInputHandler::bInitialize()
***************************************************************************/
t_Bool spi_tclInputHandler::bInitialize()
{
   t_Bool bInit = false;
   
   if(NULL!= m_poInputRespIntf)
   {

   //! Create ML or DiPO Input handlers based on the Device Type  
   m_poInputDevBase[e8DEV_TYPE_DIPO]        =  new(std::nothrow) spi_tclDiPoInputHandler(m_poInputRespIntf);
   SPI_NORMAL_ASSERT(NULL == m_poInputDevBase[e8DEV_TYPE_DIPO]);
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
   m_poInputDevBase[e8DEV_TYPE_MIRRORLINK] = new spi_tclMLInputHandler(m_poInputRespIntf);
   SPI_NORMAL_ASSERT(NULL == m_poInputDevBase[e8DEV_TYPE_MIRRORLINK]);
#endif
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
   m_poInputDevBase[e8DEV_TYPE_ANDROIDAUTO] =  new(std::nothrow) spi_tclAAPInputHandler();
   SPI_NORMAL_ASSERT(NULL == m_poInputDevBase[e8DEV_TYPE_ANDROIDAUTO]);
#endif

   vRegisterCallbacks();
   //! Set the bInit to false, if all the SPI technologies cannot be initialized
   //! else return true if one of the SPI technologies can be initialized
   bInit = (
      ( NULL != m_poInputDevBase[e8DEV_TYPE_DIPO] ) || 
      ( NULL != m_poInputDevBase[e8DEV_TYPE_MIRRORLINK] ) ||
      ( NULL != m_poInputDevBase[e8DEV_TYPE_ANDROIDAUTO])
      );
   }

	ETG_TRACE_USR1(("spi_tclInputHandler::bInitialise bInit = %d \n", ETG_ENUM(BOOL, bInit)));
 
   return bInit;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclInputHandler::bUnInitialize()
***************************************************************************/
t_Bool spi_tclInputHandler::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclInputHandler:vUninitialise entered \n"));

   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclInputHandler::vLoadSettings(const trSpiFeatureSupport&...)
***************************************************************************/
t_Void spi_tclInputHandler::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   SPI_INTENTIONALLY_UNUSED(rfcrSpiFeatureSupp);
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclInputHandler::vSaveSettings()
***************************************************************************/
t_Void spi_tclInputHandler::vSaveSettings()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclInputHandler::vProcessTouchEvent
***************************************************************************/
t_Void spi_tclInputHandler::vProcessTouchEvent(t_U32 u32DeviceHandle,
                                               tenDeviceCategory enDevCat,
                                               trTouchData &rfrTouchData)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   if( true == bValidateClient(u8Index))
   {

      //!Forward the touch data to ML or DIPO Handler based on Device Type
      m_poInputDevBase[u8Index]->vProcessTouchEvent(u32DeviceHandle,rfrTouchData);
   } //if(( u8Index < NUM_INPUT_CLIENTS)
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclInputHandler::vProcessKeyEvents
***************************************************************************/
t_Void spi_tclInputHandler::vProcessKeyEvents(t_U32 u32DeviceHandle,
                                              tenDeviceCategory enDevCat,
                                              tenKeyMode enKeyMode, 
                                              tenKeyCode enKeyCode)
{

   ETG_TRACE_USR1(("spi_tclInputHandler::vProcessKeyEvents:DevCat-%d KeyMode -%d keyCode-%d",
      ETG_ENUM(DEVICE_CATEGORY,enDevCat),ETG_ENUM(KEY_MODE,enKeyMode), ETG_ENUM(KEY_CODE, enKeyCode)));

   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   if( true == bValidateClient(u8Index))
   {
      //!Forward the Key data to ML or DIPO Handler based on Device Type
      m_poInputDevBase[u8Index]->vProcessKeyEvents(u32DeviceHandle,
         enKeyMode,enKeyCode);
   } //if(( u8Index < NUM_INPUT_CLIENTS)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclInputHandler::vProcessKeyEvents
***************************************************************************/
t_Void spi_tclInputHandler::vSelectDevice(const t_U32 cou32DevId,
        							      const tenDeviceConnectionReq coenConnReq,
        							      const tenDeviceCategory coenDevCat)

{
	t_U8 u8Index = static_cast<t_U8>(coenDevCat);
	 ETG_TRACE_USR1(("spi_tclInputHandler:vSelectDevice:evice- 0x%x  enDevCat-%d \n"
	      ,cou32DevId,ETG_ENUM(DEVICE_CATEGORY,u8Index)));

	 if (true == bValidateClient(u8Index))
	 {
	      m_poInputDevBase[u8Index]->vSelectDevice(cou32DevId, coenConnReq);
	 } //if(true == bValidateClient(u8Index))
	else
	{
		vCbSelectDeviceResp(cou32DevId, e8INVALID_DEV_HANDLE);
	}

}
/***************************************************************************
** FUNCTION:  t_Void  spi_tclInputHandler::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclInputHandler::vRegisterCallbacks()
 {

	 /*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
	 /*lint -esym(40,_1) _1 Undeclared identifier */
	 /*lint -esym(40,_2) _2 Undeclared identifier */
    ETG_TRACE_USR1(("spi_tclInputHandler:vRegisterCallbacks() \n"));

	trInputCallbacks rInputCbs;

	rInputCbs.fvSelectDeviceResp = std::bind(&spi_tclInputHandler::vCbSelectDeviceResp,this, SPI_FUNC_PLACEHOLDERS_2);
	
	//! bind to the callback function for XDevice Key Icon data
	rInputCbs.fvKeyIconDataResp =  std::bind(&spi_tclInputHandler::vCbKeyIconDataResp, this,SPI_FUNC_PLACEHOLDERS_4);
	
	//! bind to the callback function for ML Server key capabilities
    rInputCbs.fvSrvKeyCapabilitiesInfo = std::bind(&spi_tclInputHandler::vCbServerKeyCapInfoResp, this, SPI_FUNC_PLACEHOLDERS_3);

	for (t_U8 u8Index = 0; u8Index < NUM_INPUT_CLIENTS; u8Index++)
	{
		if (NULL != m_poInputDevBase[u8Index])
		{
			m_poInputDevBase[u8Index]->vRegisterInputCallbacks(rInputCbs);
		}//if((NULL != m_poInputDevBase[

	}//for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)

}
/***************************************************************************
** FUNCTION: t_Void spi_tclInputHandler::vCbSelectDeviceResp()
***************************************************************************/
t_Void spi_tclInputHandler::vCbSelectDeviceResp(const t_U32 cou32DevId,
                                         const tenErrorCode coenErrorCode)
{
   ETG_TRACE_USR1(("spi_tclInputHandler:vCbSelectDeviceResp:Device-0x%x ErrorCode-%d \n",
      cou32DevId,ETG_ENUM(ERROR_CODE,coenErrorCode)));

   spi_tclMediator *poMediator = spi_tclMediator::getInstance();

   if(NULL != poMediator)
   {
      poMediator->vPostSelectDeviceRes(e32COMPID_INPUTHANDLER, coenErrorCode);
   }//if(NULL != poMediator)
}
/***************************************************************************
** FUNCTION:  t_Void spi_tclInputHandler::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclInputHandler::vOnSelectDeviceResult(const t_U32 cou32DevId,
                                           const tenDeviceConnectionReq coenConnReq,
                                           const tenResponseCode coenRespCode,
                                           tenDeviceCategory enDevCat)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclInputHandler::vOnSelectDeviceResult:Dev-0x%x,coenConnReq-%d enRespCode-%d u8Index-%d\n",
      cou32DevId,ETG_ENUM(CONNECTION_REQ,coenConnReq),coenRespCode,u8Index));

   if (true == bValidateClient(u8Index))
   {
		m_poInputDevBase[u8Index]->vOnSelectDeviceResult(cou32DevId,
				coenConnReq, coenRespCode);
	}//if (true == bValidateClient(u8Index))

}
/***************************************************************************
** FUNCTION:  t_Bool  spi_tclVideo::bValidateClient()
***************************************************************************/
inline t_Bool spi_tclInputHandler::bValidateClient(const t_U8 cou8Index)
{

   t_Bool bRet = (cou8Index < NUM_INPUT_CLIENTS) && (NULL
			!= m_poInputDevBase[cou8Index]);

	return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclInputHandler::vProcessKnobKeyEvents
***************************************************************************/
t_Void spi_tclInputHandler::vProcessKnobKeyEvents(t_U32 u32DeviceHandle,tenDeviceCategory enDevCat,
      t_S8 s8EncoderDeltaCnt)
{
      t_U8 u8Index = static_cast<t_U8>(enDevCat);

      if( true == bValidateClient(u8Index))
      {
         //!Forward the Key data to ML/DIPO/AAP Handler based on Device Type
         m_poInputDevBase[u8Index]->vProcessKnobKeyEvents(u32DeviceHandle,
               s8EncoderDeltaCnt);
      }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclInputHandler::vGetKeyIconData()
***************************************************************************/
t_Void spi_tclInputHandler::vGetKeyIconData(const t_U32 cou32DevId,
                                       t_String szKeyIconUrl,
                                       tenDeviceCategory enDevCat,
                                       const trUserContext& rfrcUsrCntxt)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclInputHandler::vGetKeyIconData:u8Index-%d IconURL-%s \n",
      u8Index,szKeyIconUrl.c_str()));

   if(true == bValidateClient(u8Index))
   {
         m_poInputDevBase[u8Index]->vGetKeyIconData(cou32DevId,szKeyIconUrl,enDevCat,rfrcUsrCntxt);
   }
   else
   {
      //invalid handle - Post error
      vCbKeyIconDataResp(cou32DevId,NULL,0,rfrcUsrCntxt);
   }
}
/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vCbAppIconDataResult()
***************************************************************************/
t_Void spi_tclInputHandler::vCbKeyIconDataResp(const t_U32 cou32DevId,
                                            const t_U8* pcu8KeyIconData,
                                            t_U32 u32Len,
                                            const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclInputHandler::vCbKeyIconDataResult"));

   if(NULL != m_poInputRespIntf)
   {
      //! Post KeyIconData Result
      m_poInputRespIntf->vPostKeyIconDataResult(cou32DevId,pcu8KeyIconData,
         u32Len,rfrcUsrCntxt);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAppMngr::vCbClientCapInfoResp()
***************************************************************************/
t_Void spi_tclInputHandler::vCbServerKeyCapInfoResp(const t_U32 cou32DevId,
                                                  t_U16 u16NumXDevices,
                                                  trMLSrvKeyCapabilities rSrvKeyCapabilities)
{
   ETG_TRACE_USR1(("spi_tclInputHandler::vCbServerCapInfoResp"));

   if(NULL != m_poInputRespIntf)
   {
      //! Post XDeviceKeyInfo update
      m_poInputRespIntf->vPostMLServerCapInfo(cou32DevId,u16NumXDevices,rSrvKeyCapabilities);
   }
}
//lint –restore
