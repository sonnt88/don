/*!
 *******************************************************************************
 * \file              spi_tclInputHandler.h
 * \brief             Main SPI input handler 
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Main input handler that creates ML and DiPo Input handlers
 AUTHOR:        Hari Priya E R (RBEI/ECP2)
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.11.2013 |  Hari Priya E R              | Initial Version
 12.03.2014 |  Hari Priya E R              | Included changes for handling ML and DiPO 
                                             Input handlers based on Device Type
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
 03.07.2014 |  Hari Priya E R              | Added changes related to Input Response interface
 31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()
 13.03.2015 |  Sameer Chandra		       | SelectDevice Implementation for AAP.
 25.06.2015 |  Sameer Chandra              | Added ML XDeviceKey Support for PSA
 15.07.2015 |  Sameer Chandra              | Added new method vProcessKnobKeyEvents

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLINPUTHANDLER_H_
#define SPI_TCLINPUTHANDLER_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclLifeCycleIntf.h"
#include "spi_tclInputRespIntf.h"

#define NUM_INPUT_CLIENTS 4


/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

class spi_tclInputDevBase;

/*!
 * \class spi_tclInputHandler
 * \brief Main input handler that creates ML and DiPo Input handlers
 *
 */

class spi_tclInputHandler :public spi_tclInputRespIntf, public spi_tclLifeCycleIntf
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION: spi_tclInputHandler::spi_tclInputHandler(spi_tclInputRespIntf* ..)
   ***************************************************************************/
   /*!
   * \fn      spi_tclInputHandler(spi_tclInputRespIntf* poInputRespIntf)
   * \brief   Parameterised constructor
   * \param   poInputRespIntf: [IN]Input Response Interface Pointer
   * \sa      ~spi_tclInputHandler()
   **************************************************************************/
   spi_tclInputHandler(spi_tclInputRespIntf* poInputRespIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclInputHandler::~spi_tclInputHandler()
   ***************************************************************************/
   /*!
   * \fn      spi_tclInputHandler()
   * \brief   destructor
   * \sa      spi_tclInputHandler()
   **************************************************************************/
   ~spi_tclInputHandler();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclInputHandler::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      bInitialize()
   * \brief   Method to Initialize
   * \sa      bUnInitialize()
   **************************************************************************/
   virtual t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclInputHandler::bUnInitialize()
   ***************************************************************************/
   /*!
   * \fn      bUnInitialize()
   * \brief   Method to UnInitialize
   * \sa      bInitialize()
   **************************************************************************/
   virtual t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclInputHandler::vLoadSettings(const trSpiFeatureSupport&...)
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
    * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputHandler::vSaveSettings()
   ***************************************************************************/
   /*!
   * \fn      vSaveSettings()
   * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
   * \sa      vLoadSettings()
   **************************************************************************/
   virtual t_Void vSaveSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputHandler::vProcessTouchEvent
   ***************************************************************************/
   /*!
   * \fn      vProcessTouchEvent(t_U32 u32DeviceHandle,tenDeviceCategory enDevCat,
   trTouchData &rfrTouchData)
   * \brief   Receives the Touch events and forwards its to 
   either ML or DiPO Input handlers
   * \param   u32DeviceHandle : [IN] unique identifier to ML Server
   * \pram    enDevCat        : [IN] Identifies the Connection Request.
   * \param   rfrTouchData    : [IN] reference to touch data structure which contains
   *          touch details received /ref trTouchData
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessTouchEvent(t_U32 u32DeviceHandle,tenDeviceCategory enDevCat,
      trTouchData &rfrTouchData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputHandler::vProcessKeyEvents
   ***************************************************************************/
   /*!
   * \fn      vProcessKeyEvents(t_U32 u32DeviceHandle,tenDeviceCategory enDevCat,
   tenKeyMode enKeyMode, tenKeyCode enKeyCode)
   * \brief   Receives hard key events and forwards it to either 
   ML or DiPO Input handlers
   * \param   u32DeviceHandle : [IN] unique identifier to ML Server
   * \pram    enDevCat        : [IN] Identifies the Connection Request.
   * \param   enKeyMode       : [IN] indicates keypress or keyrelease
   * \param   enKeyCode       : [IN] unique key code identifier
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessKeyEvents(t_U32 u32DeviceHandle,tenDeviceCategory enDevCat,
      tenKeyMode enKeyMode, tenKeyCode enKeyCode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputHandler::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSelectDevice(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenDeviceCategory coenDevCat,
   *                 const trUserContext& corfrcUsrCntxt)
   * \brief   To setup Video related info when a device is selected or
   *          de selected.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Type.
   * \pram    coenDevCat  : [IN] Identifies the Connection Request.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId,
        const tenDeviceConnectionReq coenConnReq,
        const tenDeviceCategory coenDevCat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputHandler::vCbSelectDeviceResp()
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbSelectDeviceResp(const t_U32 cou32DevId,
   *         const tenErrorCode coenErrorCode)
   * \brief  Method to Send the Select Device response to mediator
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenErrorCode    : [IN] Type of the error.
   * \retval t_Void
   **************************************************************************/
   t_Void vCbSelectDeviceResp(const t_U32 cou32DevId,
       const tenErrorCode coenErrorCode);
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputHandler::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful/failure
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
    t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
       const tenDeviceConnectionReq coenConnReq,
       const tenResponseCode coenRespCode,
       tenDeviceCategory enDevCat);
	   
   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputHandler::vProcessKnobKeyEvents
   ***************************************************************************/
   /*!
   * \fn      vProcessKnobKeyEvents(t_U32 u32DeviceHandle,tenDeviceCategory enDevCat,
   t_S8 s8EncoderDeltaCnt)
   * \brief   Receives Knob Encoder Delta Changes and forwards it to either
   *          ML/DiPO/AAP Input handlers
   * \param   u32DeviceHandle : [IN] unique identifier of the Server
   * \pram    enDevCat        : [IN] Identifies the Connection Request.
   * \param   s8EncoderDeltaCnt : [IN] Encoder delta counts.
   * \retval  NONE
   **************************************************************************/
   t_Void vProcessKnobKeyEvents(t_U32 u32DeviceHandle,tenDeviceCategory enDevCat,
         t_S8 s8EncoderDeltaCnt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputHandler::vGetKeyIconData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetKeyIconData(t_String szKeyIconUrl,
   *         tenDeviceCategory enDevCat,
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  szAppIconUrl  : [IN] Key Icon URL
   * \pram   enDevCat      : [IN] Identifies the Connection Category
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetKeyIconData(const t_U32 cou32DevId,
      t_String szKeyIconUrl,
      tenDeviceCategory enDevCat,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputHandler::vCbKeyIconDataResult()
   ***************************************************************************/
   /*!
   * \fn    t_Void vCbKeyIconDataResult(const t_U8* pcu8AppIconData,t_U32 u32Len,
   *          const trUserContext& rfrcUsrCntxt)
   * \brief   Method to send the Get Key Icon data response to HMI
   * \param  pcu8AppIconData  : [IN] Image stream
   * \param  u32Len   : [IN] data stream length
   * \param  rfrcUsrCntxt  : [IN] User context
   * \retval  t_Void
   **************************************************************************/
   t_Void vCbKeyIconDataResp(const t_U32 cou32DevId,
                             const t_U8* pcu8KeyIconData,
                             t_U32 u32Len, const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputHandler::vCbXDeviceKeyInfoResp()
   ***************************************************************************/
   /*!
   * \fn    t_Void vCbXDeviceKeyInfoResp(const t_U32 cou32DevId,
                                t_U16 u16NumXDevices,
                                std::vector<trXDeviceKeyDetails> vecrXDeviceKeyDetails)
   * \brief   Method to send the Get Key Icon data response to HMI
   * \param  cou32DevId  : [IN] Device ID.
   * \param  u16NumXDevices   : [IN] Number of X-Device keys
   * \param  vecrXDeviceKeyDetails  : [IN] X-Device keys details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vCbServerKeyCapInfoResp(const t_U32 cou32DevId,
                                t_U16 u16NumXDevices,
                                trMLSrvKeyCapabilities rSrvKeyCapabilities);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclInputHandler::spi_tclInputHandler(const spi_tclInputHandler..
   ***************************************************************************/
   /*!
   * \fn      spi_tclInputHandler(const spi_tclInputHandler &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclInputHandler(const spi_tclInputHandler& corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclInputHandler& spi_tclInputHandler::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclInputHandler& operator= (const spi_tclInputHandler &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclInputHandler& operator = (const spi_tclInputHandler& corfrSrc);

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclInputHandler::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterCallbacks()
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo/AAP Input Handler
   * \retval  t_Void
   **************************************************************************/
   t_Void vRegisterCallbacks();

   /***************************************************************************
   ** FUNCTION:  t_Bool  spi_tclVideo::bValidateClient()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bValidateClient(t_U8 cou8Index)
   * \brief   To validate the client index. check whether it is in the range of
   *          the Video clients Array
   * \param   cou8Index : [IN] Index in the video clients Array
   * \retval  t_Bool
   **************************************************************************/
   inline t_Bool bValidateClient(const t_U8 cou8Index);
   //! Member Variable - pointer to the Base class for ML and DiPo Input Handlers
   spi_tclInputDevBase* m_poInputDevBase[NUM_INPUT_CLIENTS];

   //!Input Response Interface Pointer
   spi_tclInputRespIntf* m_poInputRespIntf;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif /* SPI_TCLINPUTHANDLER_H_ */
