/***********************************************************************/
/*!
* \file  spi_tclInputDevBase.h
* \brief Base class for MirrorLink and DiPo Input Handler classes
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    This class is the base class that routes the input handling 
                to ML or DiPo Input Handler classes
AUTHOR:         Hari Priya E R
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
10.03.2014 |  Hari Priya E R        | Initial Version
13.03.2015 |  Sameer Chandra	    | SelectDevice Implementation for AAP.
25.06.2015 |  Sameer Chandra        | Added ML XDeviceKey Support for PSA
17.07.2015 |  Sameer Chandra        | Added new method vProcessKnobKeyEvents
\endverbatim
*************************************************************************/

#ifndef _SPI_TCLINPUTDEVBASE_H_
#define _SPI_TCLINPUTDEVBASE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
//! For Select device flow
typedef std::function<void(t_U32,tenErrorCode)>
         tfvSelectDeviceResp;
//! For Key Icon Data
typedef std::function<void(t_U32,const t_U8*,t_U32,const trUserContext&)>
         tfvNotifyKeyIconData;
//! For Server key Capabilities Info
typedef std::function<void(t_U32,t_U16,trMLSrvKeyCapabilities)>
         tfvSrvKeyCapabilitiesInfo;


struct trInputCallbacks
{
      //! Informs Select/Deselect Device Result
      tfvSelectDeviceResp fvSelectDeviceResp;
	  
      //! Informs with Icon data using URL in MS
      tfvNotifyKeyIconData fvKeyIconDataResp;
	  
      //! Informs Server Key Capabilities.
      tfvSrvKeyCapabilitiesInfo fvSrvKeyCapabilitiesInfo;

      trInputCallbacks():
         fvSelectDeviceResp(NULL),fvKeyIconDataResp(NULL),fvSrvKeyCapabilitiesInfo(NULL)
      {

      }
};
/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class spi_tclInputDevBase
* \brief 
****************************************************************************/
class spi_tclInputDevBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclInputDevBase::spi_tclInputDevBase()
   ***************************************************************************/
   /*!
   * \fn      spi_tclInputDevBase()
   * \brief   Default Constructor
   * \sa      ~spi_tclInputDevBase()
   **************************************************************************/
   spi_tclInputDevBase()
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION:  spi_tclInputDevBase::~spi_tclInputDevBase()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclInputDevBase()
   * \brief   Destructor
   * \sa      spi_tclInputDevBase()
   **************************************************************************/
   virtual ~spi_tclInputDevBase()
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclInputDevBase::vProcessTouchEvent
   ***************************************************************************/
   /*!
   * \fn      vProcessTouchEvent(t_U32 u32DeviceHandle,trTouchData &rfrTouchData)
   * \brief   Receives the Touch events and forwards it to further handlers 
   for processing
   * \param   u32DeviceHandle  : [IN] unique identifier to ML Server
   * \param   rfrTouchData     : [IN] reference to touch data structure which contains
   *          touch details received /ref trTouchData
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vProcessTouchEvent(t_U32 u32DeviceHandle,
      trTouchData &rfrTouchData)const =0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputDevBase::vProcessKeyEvents
   ***************************************************************************/
   /*!
   * \fn      vProcessKeyEvents(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
   tenKeyCode enKeyCode)
   * \brief   Receives hard key events and forwards it to 
   further handlers for processing
   * \param   u32DeviceHandle : [IN] unique identifier to ML Server
   * \param   enKeyMode       : [IN] indicates keypress or keyrelease
   * \param   enKeyCode       : [IN] unique key code identifier
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vProcessKeyEvents(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
      tenKeyCode enKeyCode) const=0;

   /***************************************************************************
   ** FUNCTION: virtual t_Void spi_tclInputDevBase::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSelectDevice(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenDeviceCategory coenDevCat,
   *                 const trUserContext& corfrcUsrCntxt)
   * \brief   To setup Video related info when a device is selected or
   *          de selected.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Type.
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq) = 0;
   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclInputDevBase::vRegisterVideoCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterVideoCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo Video
   * \param   corfrVideoCallbacks : [IN] Video callabcks structure
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vRegisterInputCallbacks(const trInputCallbacks& corfrInputCallbacks) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputDevBase::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful/failed
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(coenConnReq);
      SPI_INTENTIONALLY_UNUSED(coenRespCode);
   }
   
 /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputDevBase::vProcessKnobKeyEvents
   ***************************************************************************/
   /*!
   * \fn      vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt)
   * \brief   Receives Knob Encoder Delta Changes and forwards it to
   *          further handlers for processing
   * \param   u32DeviceHandle : [IN] unique identifier to ML Server
   * \param   s8EncoderDeltaCount : [IN] encoder delta count
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt) const=0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclInputDevBase::vGetKeyIconData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vGetKeyIconData(t_String szKeyIconUrl,
   *         tenDeviceCategory enDevCat,
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  szAppIconUrl  : [IN] Key Icon URL
   * \pram   enDevCat      : [IN] Identifies the Connection Category
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vGetKeyIconData(const t_U32 cou32DevId,
                                  t_String szKeyIconUrl,
                                  tenDeviceCategory enDevCat,
                                  const trUserContext& rfrcUsrCntxt)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(szKeyIconUrl);
      SPI_INTENTIONALLY_UNUSED(enDevCat);
      SPI_INTENTIONALLY_UNUSED(rfrcUsrCntxt);
   }
    /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif //_SPI_TCLINPUTDEVBASE_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>

