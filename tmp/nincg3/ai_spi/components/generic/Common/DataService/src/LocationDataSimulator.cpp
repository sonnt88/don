/*!
 *******************************************************************************
 * \file         LocationDataSimulator.h
 * \brief        Location data simulator class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Location data simulator class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 19.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 06.05.2015  |Tejaswini HB                 |Lint Fix

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <iostream>
#include "vector"
#include <ctime>
#include <unistd.h>

#include "FileHandler.h"
using namespace spi::io;
#include "LocationDataSimulator.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DATASERVICE
      #include "trcGenProj/Header/LocationDataSimulator.cpp.trc.h"
   #endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
enum tenGpsDataIndex
{
   Latitude = 0,
   Longitude,
   Altitude,
   Speed,
   Heading
};

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define SLEEP(TIME_IN_MS)  usleep((TIME_IN_MS) * 1000)

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_Char coczSampleLocDataFilePath[] = "/opt/bosch/gm/SampleLocData.txt";

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  LocationDataSimulator::LocationDataSimulator();
***************************************************************************/
LocationDataSimulator::LocationDataSimulator()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
} //!end of LocationDataSimulator()

/***************************************************************************
** FUNCTION:  LocationDataSimulator::~LocationDataSimulator();
***************************************************************************/
LocationDataSimulator::~LocationDataSimulator()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
} //!end of ~LocationDataSimulator()

/***************************************************************************
** FUNCTION:  t_Void LocationDataSimulator::vRegisterCallbacks()
***************************************************************************/
t_Void LocationDataSimulator::vRegisterCallbacks(const trLocDataSimulationCb& rfrLocDataCb)
{
   m_rLocDataCb = rfrLocDataCb;
}

/***************************************************************************
** FUNCTION:  t_Void LocationDataSimulator::vSimulateGpsData()
***************************************************************************/
t_Void LocationDataSimulator::vSimulateGpsData(t_U32 u32DataRateInMs)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   spi::io::FileHandler oSampleGpsDataFile(coczSampleLocDataFilePath, spi::io::SPI_EN_RDONLY);
   if (true == oSampleGpsDataFile.bIsValid())
   {
      tInputStream SampleGpsDataStream(coczSampleLocDataFilePath);
      if (SampleGpsDataStream.is_open())
      {
         while (!SampleGpsDataStream.eof())
         {
            t_String szFileLine = szGetLineInFile(SampleGpsDataStream);
            vExtractGpsData(szFileLine);
            if ((!szFileLine.empty()) && ('&' == szFileLine[0]))
            {
               SLEEP(u32DataRateInMs);
            }
         }
         SampleGpsDataStream.close();

      } //if (SampleGpsDataStream.is_open())
      else
      {
         ETG_TRACE_ERR(("ERROR: vSimulateGPSDataTransfer: SampleLocData.txt file not open!"));
      }
   } //if (true == oSampleGpsDataFile.bIsValid())
   else
   {
      ETG_TRACE_ERR(("ERROR: vSimulateGPSDataTransfer: SampleLocData.txt file is invalid!"));
   }
} //!end of LocationDataSimulator()


/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  LocationDataSimulator::vExtractGpsData();
***************************************************************************/
t_Void LocationDataSimulator::vExtractGpsData(const t_String& coszFileLine)
{
	/*lint -esym(40,fvOnSimGpsData) fvOnSimGpsData Undeclared identifier */
   /*lint -esym(746,fvOnSimGpsData) function fvOnSimGpsData() not made in the presence of a prototype */
   //!----------------------------------------------------------------------
   //! @Note: In sample GPS data file:
   //! 1) Lines containing GPS data should start with symbol &', and
   //!    data values should be separated with symbol ';'.
   //!    (Example: &42.499354;-83.358563;100;60;0 )
   //! 2) The data should be in the below order:
   //!    <Latitude>;<Longitude>;<Altitude>;<Speed>;<Heading>
   //! 3) Units of data should be:
   //!    Lat/Long - [deg], Altitude - [m], Speed - [cm/s], Heading - [deg]
   //!----------------------------------------------------------------------

   if ((!coszFileLine.empty()) && ('&' == coszFileLine[0]))
   {
      //static int LinesCount = 1;    //Commented to remove Lint Warning
      std::vector<t_String> GpsValuesList = SplitString(coszFileLine.substr(1, (coszFileLine.length() - 1)) , ';');

      trGPSData rExtractedGpsData;
      rExtractedGpsData.dLatLongResolution = (4294967296/360.0);
      rExtractedGpsData.dHeadingResolution = (256/360.0);
      rExtractedGpsData.PosixTime = time(0);

      int GpsListSize = GpsValuesList.size();
      for (int VecIndx = 0; VecIndx < GpsListSize; VecIndx++)
      {
         tenGpsDataIndex enIndex = static_cast<tenGpsDataIndex>(VecIndx);
         switch (enIndex)
         {
            case Latitude:
               rExtractedGpsData.s32Latitude = static_cast<t_S32> (dGetDoubleValue(
                     GpsValuesList[VecIndx]) * (rExtractedGpsData.dLatLongResolution));
               break;
            case Longitude:
               rExtractedGpsData.s32Longitude = static_cast<t_S32> (dGetDoubleValue(
                     GpsValuesList[VecIndx]) * (rExtractedGpsData.dLatLongResolution));
               break;
            case Altitude:
               rExtractedGpsData.s32Altitude = static_cast<t_S32> (dGetDoubleValue(
                     GpsValuesList[VecIndx]));
               break;
            case Speed:
               rExtractedGpsData.s16Speed = static_cast<t_S32> (dGetDoubleValue(
                     GpsValuesList[VecIndx]));
               break;
            case Heading:
               rExtractedGpsData.dHeading = static_cast<t_S32> (dGetDoubleValue(
                     GpsValuesList[VecIndx]));
               break;
         } //switch (enIndex)
      } //for (int VecIndx = 0; VecIndx < GpsValuesList.size(); VecIndx++)

      ETG_TRACE_USR4(("vExtractGpsData: Latitude = %d", rExtractedGpsData.s32Latitude));
      ETG_TRACE_USR4(("vExtractGpsData: Longitude = %d", rExtractedGpsData.s32Longitude));
      ETG_TRACE_USR4(("vExtractGpsData: Altitude = %d", rExtractedGpsData.s32Altitude));
      ETG_TRACE_USR4(("vExtractGpsData: Speed = %d", rExtractedGpsData.s16Speed));
      ETG_TRACE_USR4(("vExtractGpsData: Heading = %d", rExtractedGpsData.dHeading));
      ETG_TRACE_USR4(("vExtractGpsData: PosixTime = %d", rExtractedGpsData.PosixTime));
      ETG_TRACE_USR4(("vExtractGpsData: dLatLongResolution = %f", rExtractedGpsData.dLatLongResolution));
      ETG_TRACE_USR4(("vExtractGpsData: dHeadingResolution = %f", rExtractedGpsData.dHeadingResolution));

      if (NULL != m_rLocDataCb.fvOnSimGpsData)
      {
         m_rLocDataCb.fvOnSimGpsData(rExtractedGpsData);
      }
   } //if ((!coszFileLine.empty()) && ('&' == coszFileLine[0]))
}

/***************************************************************************
** FUNCTION:  LocationDataSimulator::szGetLineInFile(tInputStream& szDataFileStream)
***************************************************************************/
t_String LocationDataSimulator::szGetLineInFile(tInputStream& szDataFileStream) const
{
   t_String szFileLine = "";

   if (szDataFileStream.is_open())
   {
      getline(szDataFileStream, szFileLine);
   }
   else
   {
      ETG_TRACE_ERR(("ERROR: szGetLineInFile: File not open!"));
   }
   return szFileLine;
}

/***************************************************************************
** FUNCTION:  LocationDataSimulator::SplitString();
***************************************************************************/
std::vector<t_String> LocationDataSimulator::SplitString(const t_String& szString,
      char czDelimiter) const
{
   std::vector<t_String> ElementsList;
   tStringStream ElementsListStream(szString);
   t_String szItemInList;
   while (std::getline(ElementsListStream, szItemInList, czDelimiter))
   {
      ElementsList.push_back(szItemInList);
   }
   return ElementsList;
}

/***************************************************************************
** FUNCTION:  LocationDataSimulator::dGetDoubleValue();
***************************************************************************/
t_Double LocationDataSimulator::dGetDoubleValue(const t_String& szValue) const
{
   tInputStringStream InputStream(szValue);
   t_Double dValue = 0;
   InputStream >> dValue;
   return dValue;
}

/***************************************************************************
** FUNCTION:  LocationDataSimulator::dGetSignedIntValue();
***************************************************************************/
t_S32 LocationDataSimulator::dGetSignedIntValue(const t_String& szValue) const
{
   return atol(szValue.c_str());
}

/***************************************************************************
** FUNCTION:  LocationDataSimulator::dGetUnsignedIntValue();
***************************************************************************/
t_U32 LocationDataSimulator::dGetUnsignedIntValue(const t_String& szValue) const
{
   return atol(szValue.c_str());
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
