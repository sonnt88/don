  /*!
 *******************************************************************************
 * \file         spi_tclDataService.cpp
 * \brief        Data Service Manager class that provides interface to delegate
                 the execution of command and handle response
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Data Service Manager class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 27.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 14.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Implemented sending GPS data to
                                                  MediaPlayer client.
 06.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Initialisation sequence implementation
 13.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Implementation for: 
                                                  (1) MPlay FI extn to start/stop loc info
                                                  (2) VDSensor data integration
                                                  (3) NMEA-PASCD sentence for DiPO
 31.07.2014 |  Ramya Murthy (RBEI/ECP2)         | SPI feature configuration via LoadSettings()
 13.10.2014 |  Hari Priya E R (RBEI/ECP2)       | Added interface for Vehicle Data for PASCD
                                                  and interface with Data Service Response
 05.02.2015 |  Ramya Murthy (RBEI/ECP2)         | Added interface to set availability of LocationData
 28.03.2015 |  SHITANSHU SHEKHAR (RBEI/ECP2)    | Initialized AAP data service object and
                                                 Implemented enGetDataServiceIndex()
23.04.2015 |  Ramya Murthy (RBEI/ECP2)          | Added interface to set region code
28.05.2015 |  Tejaswini H B(RBEI/ECP2)          | Added Lint comments to suppress C++11 Errors
25.06.2015 |  Tejaswini HB (RBEI/ECP2)          |Featuring out Android Auto
30.10.2015 |  Shiva Kumar G                     | Implemented ReportEnvironment Data feature
03.12.2015 |  SHITANSHU SHEKHAR                 | Implemented vSetFeatureRestrictions() function.

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "spi_tclFactory.h"
#include "spi_tclMediator.h"
#include "spi_tclMPlayClientHandler.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
#include "spi_tclMLDataService.h"
#endif
#include "spi_tclDipoDataService.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
#include "spi_tclAAPDataService.h"
#endif
#include "LocationDataSimulator.h"
#include "spi_tclDataService.h"
#include "spi_LoopbackTypes.h"
#include "spi_tclDataServiceSettings.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DATASERVICE
      #include "trcGenProj/Header/spi_tclDataService.cpp.trc.h"
   #endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclDataService::spi_tclDataService();
***************************************************************************/
spi_tclDataService::spi_tclDataService(ahl_tclBaseOneThreadApp* poMainApp,
                              spi_tclDataServiceRespIntf* poDataSrvcRespIntf)
   : m_poMainApp(poMainApp),m_poDataSrvcRespIntf(poDataSrvcRespIntf),
     m_enSelDevCategory(e8DEV_TYPE_UNKNOWN)
{
   ETG_TRACE_USR1(("spi_tclDataService() entered "));
   SPI_NORMAL_ASSERT(NULL == m_poMainApp);
   for (t_U8 u8Index = 0; u8Index < e8_DATASVC_INDEX_SIZE; ++u8Index)
   {
      m_poDataSvcHandlersLst[u8Index] = NULL;
   }
} //!end of spi_tclDataService()

/***************************************************************************
** FUNCTION:  spi_tclDataService::~spi_tclDataService();
***************************************************************************/
spi_tclDataService::~spi_tclDataService()
{
   ETG_TRACE_USR1(("~spi_tclDataService() entered "));
   m_poDataSrvcRespIntf = NULL;
   m_poMainApp = NULL;
} //!end of ~spi_tclDataService()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDataService::bInitialize()
***************************************************************************/
t_Bool spi_tclDataService::bInitialize()
{
   //! Create Pos_Fi client handler
   SPI_NORMAL_ASSERT(NULL == m_poMainApp);
   t_Bool bInitSuccess = false;
   /*lint -esym(40,fvSubscribeForLocationData) fvSubscribeForLocationData Undeclared identifier */
   /*lint -esym(40,fvSubscribeForEnvData) fvSubscribeForEnvData Undeclared identifier */
   /*lint -esym(40,_1) _1 Undeclared identifier */
   /*lint -esym(40,_2) _2 Undeclared identifier */
   //! Create the Data Service handlers
   //! Initialize DataService manager callbacks structure
   trDataServiceCb rDataServiceCb;
   rDataServiceCb.fvSubscribeForLocationData =
      std::bind(&spi_tclDataService::vOnSubscribeForLocData,
      this,
      SPI_FUNC_PLACEHOLDERS_2);

   rDataServiceCb.fvSubscribeForEnvData = 
      std::bind(&spi_tclDataService::vSubscribeForEnvData,
      this,
      SPI_FUNC_PLACEHOLDERS_1);

#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
   m_poDataSvcHandlersLst[e8_DATASVC_ML_INDEX] = new spi_tclMLDataService(rDataServiceCb);
   SPI_NORMAL_ASSERT(NULL == m_poDataSvcHandlersLst[e8_DATASVC_ML_INDEX]);
#endif

   m_poDataSvcHandlersLst[e8_DATASVC_DIPO_INDEX] = new spi_tclDipoDataService(rDataServiceCb);
   SPI_NORMAL_ASSERT(NULL == m_poDataSvcHandlersLst[e8_DATASVC_DIPO_INDEX]);
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
   m_poDataSvcHandlersLst[e8_DATASVC_ANDROIDAUTO_INDEX] = new spi_tclAAPDataService(rDataServiceCb);
   SPI_NORMAL_ASSERT(NULL == m_poDataSvcHandlersLst[e8_DATASVC_ANDROIDAUTO_INDEX]);
#endif

   //! Initialise the Data Service handlers
   //! Set the bInit to false, if all the SPI technologies cannot be initialized
   //! else return true if one of the SPI technologies can be initialized
   for (t_U8 u8Index = 0; u8Index < e8_DATASVC_INDEX_SIZE; ++u8Index)
   {
      if (NULL != m_poDataSvcHandlersLst[u8Index])
      {
         bInitSuccess = m_poDataSvcHandlersLst[u8Index]->bInitialise() || bInitSuccess;
      }
   } //for (t_U8 u8Index = 0;...)

   ETG_TRACE_USR2(("[DESC]::spi_tclDataService::bInitialise() left with result = %u \n", ETG_ENUM(BOOL,bInitSuccess)));
   return bInitSuccess;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDataService::bUninitialise()
***************************************************************************/
t_Bool spi_tclDataService::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclDataService::bUnInitialize() entered "));

   t_Bool bUninitSuccess = true;

   //! Uninitialize & delete the Data Service handlers
   for (t_U8 u8Index = 0; u8Index < e8_DATASVC_INDEX_SIZE; ++u8Index)
   {
      if (NULL != m_poDataSvcHandlersLst[u8Index])
      {
         bUninitSuccess = bUninitSuccess && (m_poDataSvcHandlersLst[u8Index]->bUninitialise());
      }
      RELEASE_MEM(m_poDataSvcHandlersLst[u8Index]);
   } //for (t_U8 u8Index = 0;...)


   ETG_TRACE_USR2(("[DESC]::bUnInitialize() left with result = %u ", bUninitSuccess));
   return bUninitSuccess;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vLoadSettings(const trSpiFeatureSupport&...)
***************************************************************************/
t_Void spi_tclDataService::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1(("spi_tclDataService::vLoadSettings() entered "));
   SPI_INTENTIONALLY_UNUSED(rfcrSpiFeatureSupp);
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vSaveSettings()
***************************************************************************/
t_Void spi_tclDataService::vSaveSettings()
{
   ETG_TRACE_USR1(("spi_tclDataService::vSaveSettings() entered "));
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vSelectDevice()
***************************************************************************/
t_Void spi_tclDataService::vSelectDevice(const t_U32 cou32DevId, tenDeviceConnectionReq enConnReq,
      tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1(("[DESC]::spi_tclDataService::vSelectDevice() entered: "
         "DeviceHandle = %u, DevConnectionRequest = %u, Device Category = %u ",
         cou32DevId, ETG_ENUM(CONNECTION_REQ, enConnReq),
         ETG_ENUM(DEVICE_CATEGORY, enDevCat)));

   t_U8 u8ServiceLstIndx = enGetDataServiceIndex(enDevCat);

   if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
   {
      m_poDataSvcHandlersLst[u8ServiceLstIndx]->vSelectDevice(cou32DevId, enConnReq);
   }

   spi_tclMediator *poMediator = spi_tclMediator::getInstance();

   //! Data service can always return true as the service is not critical for feature to work
   if(NULL != poMediator)
   {
      poMediator->vPostSelectDeviceRes(e32COMPID_DATASERVICE, e8NO_ERRORS);
   }//if(NULL != poMediator)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vOnSelectDeviceResult(t_U32...)
***************************************************************************/
t_Void spi_tclDataService::vOnSelectDeviceResult(t_U32 u32DeviceHandle,
      tenDeviceConnectionReq enDeviceConnReq,
      tenResponseCode enRespCode,
      tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1(("[DESC]::spi_tclDataService::vOnSelectDeviceResult() entered: "
         "DeviceHandle = %u, DevConnectionRequest = %u, ResponseCode = %u,ErrorCode = %u ",
         u32DeviceHandle,
         ETG_ENUM(CONNECTION_REQ, enDeviceConnReq),
         ETG_ENUM(RESPONSE_CODE, enRespCode),
         ETG_ENUM(ERROR_CODE, enErrorCode)));

   tenDeviceCategory enDevCat = enGetDeviceCategory(u32DeviceHandle);

   //! Trigger DataService handler based on SelectDevice/Deselect device operation success
   if (e8DEV_TYPE_UNKNOWN != enDevCat)
   {
      t_U8 u8ServiceLstIndx = enGetDataServiceIndex(enDevCat);

      if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
      {
         if ((e8DEVCONNREQ_SELECT == enDeviceConnReq) && (e8NO_ERRORS == enErrorCode))
         {
            m_poDataSvcHandlersLst[u8ServiceLstIndx]->vOnSelectDeviceResult(u32DeviceHandle);
            //Store SelectedDevice category
            m_enSelDevCategory = enDevCat;
         }
         else if ((e8DEVCONNREQ_DESELECT == enDeviceConnReq) && (enDevCat == m_enSelDevCategory))
         {
            m_poDataSvcHandlersLst[u8ServiceLstIndx]->vOnDeselectDeviceResult(u32DeviceHandle);
            //Clear SelectedDevice category
            m_enSelDevCategory = e8DEV_TYPE_UNKNOWN;
         }
      } //if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
   } //if (e8DEV_TYPE_UNKNOWN != enDevCat)

} //!end of vOnSelectDeviceResult()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vStartStopLocationData(...)
***************************************************************************/
t_Void spi_tclDataService::vStartStopLocationData(t_Bool bStartLocData,
      const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList)
{
   ETG_TRACE_USR1(("[DESC]::spi_tclDataService::vStartStopLocationData() entered: "
         "bStartLocData = %u, rfcoNmeaSentencesList size = %u ",
         ETG_ENUM(BOOL, bStartLocData), rfcoNmeaSentencesList.size()));

   //! Forward request to Dipo DataService handler
   if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
   {
      t_U8 u8ServiceLstIndx = enGetDataServiceIndex(m_enSelDevCategory);

      if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
      {
         if ((true == bStartLocData) && (false == rfcoNmeaSentencesList.empty()))
         {
            m_poDataSvcHandlersLst[u8ServiceLstIndx]->vStartLocationData(
                  rfcoNmeaSentencesList);
         }
         else if (false == bStartLocData)
         {
            m_poDataSvcHandlersLst[u8ServiceLstIndx]->vStopLocationData(
                  rfcoNmeaSentencesList);
         }
         else
         {
            ETG_TRACE_ERR(("[ERR]::spi_tclDataService::vStartStopLocationData: Empty NMEA sentences list received! "));
         }
      } //if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
   } // if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)

} //!end of vStartStopLocationData()

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vOnGPSData(const trGPSData& rfrcGpsData)
***************************************************************************/
t_Void spi_tclDataService::vOnGPSData(const trGPSData& rfrcGpsData)
{
   ETG_TRACE_USR4(("[PARAM]::spi_tclDataService::vOnGPSData: "
         "Latitude = %d, Longitude = %d, Heading = %f,  Heading Direction = %d, Speed = %d, "
         "Altitude = %d, PosixTime = 0x%x, ExactTime(ms) = %d ",
         rfrcGpsData.s32Latitude, rfrcGpsData.s32Longitude, rfrcGpsData.dHeading, ETG_ENUM(HEADING_DIR,rfrcGpsData.enHeadingDir),
         rfrcGpsData.s16Speed, rfrcGpsData.s32Altitude, rfrcGpsData.PosixTime, rfrcGpsData.u16ExactTime));

   ETG_TRACE_USR4(("spi_tclDataService::vOnGPSData: "
         "SensorType = %d, TurnRate = %d, Acceleration = %d ",
         rfrcGpsData.enSensorType, rfrcGpsData.s16TurnRate, rfrcGpsData.s16Acceleration));

   //! Send data to Selected device's DataService handler
   if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
   {
      t_U8 u8ServiceLstIndx = enGetDataServiceIndex(m_enSelDevCategory);

      if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
      {
         m_poDataSvcHandlersLst[u8ServiceLstIndx]->vOnData(rfrcGpsData);
      } //if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
   } // if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vOnSensorData(const trSensorData& rfrcSensorData)
***************************************************************************/
t_Void spi_tclDataService::vOnSensorData(const trSensorData& rfrcSensorData)
{
   ETG_TRACE_USR4(("[PARAM]::spi_tclDataService::vOnSensorData: "
         "TimeStamp = %u, PosixTime = 0x%x, GnssQuality = %d, enGnssMode = %d, "
         "SatellitesUsed = %d, HDOP = %f, GeoidalSeparation = %f ",
         rfrcSensorData.u32TimeStamp, rfrcSensorData.PosixTime, rfrcSensorData.enGnssQuality, rfrcSensorData.enGnssMode,
         rfrcSensorData.u16SatellitesUsed, rfrcSensorData.dHDOP, rfrcSensorData.dGeoidalSeparation));

   //! Send data to Selected device's DataService handler
   if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
   {
      t_U8 u8ServiceLstIndx = enGetDataServiceIndex(m_enSelDevCategory);

      if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
      {
         m_poDataSvcHandlersLst[u8ServiceLstIndx]->vOnData(rfrcSensorData);
      } //if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
   } // if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vOnAccSensorData
** (const std::vector<trAccSensorData>& corfvecrAccSensorData)
***************************************************************************/
t_Void spi_tclDataService::vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
{
   //! Send data to Selected device's DataService handler
   if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
   {
      t_U8 u8ServiceLstIndx = enGetDataServiceIndex(m_enSelDevCategory);

      if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
      {
         m_poDataSvcHandlersLst[u8ServiceLstIndx]->vOnAccSensorData(corfvecrAccSensorData);
      } //if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
   } // if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vOnGyroSensorData
** (const std::vector<trGyroSensorData>& rfcoSensorData)
***************************************************************************/
t_Void spi_tclDataService::vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
{
   //! Send data to Selected device's DataService handler
   if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
   {
      t_U8 u8ServiceLstIndx = enGetDataServiceIndex(m_enSelDevCategory);

      if(NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
      {
         m_poDataSvcHandlersLst[u8ServiceLstIndx]->vOnGyroSensorData(corfvecrGyroSensorData);
      } //if (NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
   } // if (e8DEV_TYPE_UNKNOWN != m_enSelDevCategory)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vOnVehicleData(const trVehicleData rfrcVehicleData)
***************************************************************************/
t_Void spi_tclDataService::vOnVehicleData(const trVehicleData rfrcVehicleData, t_Bool bSolicited)
{
   ETG_TRACE_USR1(("[DESC]::spi_tclDataService::vOnVehicleData: PosixTime = 0x%x, "
         "GearPosition = %d, Speed = %d, ParkBrakeActive = %d, VehMovState = %d ",
         rfrcVehicleData.PosixTime, ETG_ENUM(GEARPOSITION, rfrcVehicleData.enGearPosition),
         rfrcVehicleData.s16Speed, rfrcVehicleData.bParkBrakeActive,
         ETG_ENUM(VEH_MOV_STATE, rfrcVehicleData.enVehMovState)));

   //! Send data to Selected device's DataService handler
   for (t_U8 u8Index = 0; u8Index < e8_DATASVC_INDEX_SIZE; ++u8Index)
   {
      if (NULL != m_poDataSvcHandlersLst[u8Index])
      {
         m_poDataSvcHandlersLst[u8Index]->vOnData(rfrcVehicleData,bSolicited);
      }
   } //for (t_U8 u8Index = 0;...)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vSetLocDataAvailablility(t_Bool...)
***************************************************************************/
t_Void spi_tclDataService::vSetLocDataAvailablility(t_Bool bLocDataAvailable,
      t_Bool bIsDeadReckonedData)
{
   ETG_TRACE_USR1(("spi_tclDataService::vSetLocDataAvailablility) entered "));
   trDataServiceConfigData rDataServiceConfigData;
   spi_tclDataServiceSettings* poDataSettings = spi_tclDataServiceSettings::getInstance();

   if (NULL != poDataSettings)
   {
      rDataServiceConfigData.bAcclData = poDataSettings->bGetAccelerometerDataEnabled();
      rDataServiceConfigData.bDeadReckonedData = bIsDeadReckonedData;
      rDataServiceConfigData.bEnvData = poDataSettings->bGetEnvDataSubEnabled();
      rDataServiceConfigData.bGearStatus = poDataSettings->bGetGearStatusEnabled();
      rDataServiceConfigData.bGyroData = poDataSettings->bGetGyroDataEnabled();
      rDataServiceConfigData.bLocDataAvailable = bLocDataAvailable;
	}//if (NULL != poDataSettings)
	
   ETG_TRACE_USR1(("the data populated is, "
      "ACCL = %d,DEAD_RECKON_DATA = %d,ENV_DATA = %d, GEAR_STATUS = %d, GYRO_DATA = %d ,LOC_DATA = %d ",
      rDataServiceConfigData.bAcclData,rDataServiceConfigData.bDeadReckonedData,rDataServiceConfigData.bEnvData,
      rDataServiceConfigData.bGearStatus,rDataServiceConfigData.bGyroData,rDataServiceConfigData.bLocDataAvailable));

   //! Send data to device's DataService handler
   for (t_U8 u8Index = 0; u8Index < e8_DATASVC_INDEX_SIZE; ++u8Index)
   {
      if (NULL != m_poDataSvcHandlersLst[u8Index])
      {
         m_poDataSvcHandlersLst[u8Index]->vSetSensorDataAvailablility(rDataServiceConfigData);
      }
   } //for (t_U8 u8Index = 0;...)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vSimulateGPSDataTransfer(t_U32 u32DataRateInMs)
***************************************************************************/
t_Void spi_tclDataService::vSimulateGPSDataTransfer(t_U32 u32DataRateInMs)
{
   ETG_TRACE_USR1(("spi_tclDataService::vSimulateGPSDataTransfer() entered "));

   trGPSData rSampleGpsData;
   LocationDataSimulator* poLocSimulator = LocationDataSimulator::getInstance();
   if (NULL != poLocSimulator)
   {
	   /*lint -esym(40,fvOnSimGpsData) fvOnSimGpsData Undeclared identifier */
	   /*lint -esym(40,_2) _2 Undeclared identifier */
	   //! Initialise callbacks to be handed over to
      //! Pos_Fi client handler for receiving GPS data.
      trLocDataSimulationCb rLocDataSimCb;
      rLocDataSimCb.fvOnSimGpsData =
            std::bind(&spi_tclDataService::vOnGPSData,
                      this,
                      SPI_FUNC_PLACEHOLDERS_1);

      poLocSimulator->vRegisterCallbacks(rLocDataSimCb);
      poLocSimulator->vSimulateGpsData(u32DataRateInMs);
   } //if (NULL != poLocSimulator)
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  tenDeviceCategory spi_tclDataService::enGetDeviceCategory(t_U32...)
***************************************************************************/
tenDeviceCategory spi_tclDataService::enGetDeviceCategory(t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1(("[DESC]::spi_tclDataService::enGetDeviceCategory() entered: u32DeviceHandle = %u ", u32DeviceHandle));

   //! Get Selected device's category type from ConnectionManager
   tenDeviceCategory enDeviceCategory = e8DEV_TYPE_UNKNOWN;
   spi_tclFactory* poSPIFactory = spi_tclFactory::getInstance();
   SPI_NORMAL_ASSERT(NULL == poSPIFactory);
   if (NULL != poSPIFactory)
   {
      spi_tclConnMngr* poConnMngr = poSPIFactory->poGetConnMngrInstance();
      SPI_NORMAL_ASSERT(NULL == poConnMngr);
      if (NULL != poConnMngr)
      {
         enDeviceCategory = poConnMngr->enGetDeviceCategory(u32DeviceHandle);
      } //if (NULL != poConnMngr)
   } //if (NULL != poSPIFactory)

   return enDeviceCategory;
}

/***************************************************************************
**FUNCTION: tenDataServicesIndex spi_tclDataService::enGetDataServiceIndex(tenDeviceCategory)
***************************************************************************/
tenDataServicesIndex spi_tclDataService::enGetDataServiceIndex(tenDeviceCategory enDataCategory)
{
   tenDataServicesIndex enDataSvcIndex = e8_DATASVC_ML_INDEX; //Initialise to avoid Lint warning;
   switch(enDataCategory)
   {
      case e8DEV_TYPE_DIPO :
         enDataSvcIndex = e8_DATASVC_DIPO_INDEX;
         break;
      case e8DEV_TYPE_MIRRORLINK :
         enDataSvcIndex = e8_DATASVC_ML_INDEX;
         break;
      case e8DEV_TYPE_ANDROIDAUTO :
         enDataSvcIndex = e8_DATASVC_ANDROIDAUTO_INDEX;
         break;
      default :
         ETG_TRACE_ERR(("[ERR]::spi_tclDataService::enGetDataServiceIndex() Invalid data received"));
         break;
   }//switch(enDataCategory)

   return enDataSvcIndex;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclDataService::vSetRegion(...)
***************************************************************************/
t_Void spi_tclDataService::vSetRegion(tenRegion enRegion)
{
   ETG_TRACE_USR1(("spi_tclDataService::vSetRegion() entered: Region = %u ", enRegion));

   for (t_U8 u8Index = 0; u8Index < e8_DATASVC_INDEX_SIZE; ++u8Index)
   {
      if (NULL != m_poDataSvcHandlersLst[u8Index])
      {
         m_poDataSvcHandlersLst[u8Index]->vSetRegion(enRegion);
      }
   } //for (t_U8 u8Index = 0;...)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDataService::vOnSubscribeForLocData(t_Bool...)
 ***************************************************************************/
t_Void spi_tclDataService::vOnSubscribeForLocData(t_Bool bSubscribe,
                                                  tenLocationDataType enLocDataType)
{
   ETG_TRACE_USR1(("[DESC]::spi_tclDataService::vOnSubscribeForLocData() entered: bSubscribe = %u, enLocDataType = %u ",
      ETG_ENUM(BOOL, bSubscribe), ETG_ENUM(LOCATION_DATA_TYPE, enLocDataType)));
	if(NULL!= m_poDataSrvcRespIntf)
   {
      m_poDataSrvcRespIntf->vPostSubscribeForLocData(bSubscribe,enLocDataType);
   }

}


/***************************************************************************
** FUNCTION:  spi_tclAAPDataService::vReportEnvironmentData()
***************************************************************************/
t_Void spi_tclDataService::vReportEnvironmentData(t_Bool bValidTempUpdate,
                                                  t_Double dTemp,
                                                  t_Bool bValidPressureUpdate, 
                                                  t_Double dPressure)
{
   for (t_U8 u8Index = 0; u8Index < e8_DATASVC_INDEX_SIZE; ++u8Index)
   {
      if (NULL != m_poDataSvcHandlersLst[u8Index])
      {
         m_poDataSvcHandlersLst[u8Index]->vReportEnvironmentData(bValidTempUpdate,dTemp,
            bValidPressureUpdate,dPressure);
      }
   } //for (t_U8 u8Index = 0;...)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataService::vSubscribeForEnvData()
***************************************************************************/
t_Void spi_tclDataService::vSubscribeForEnvData(t_Bool bSubscribe)
{
   ETG_TRACE_USR1(("[DESC]::spi_tclDataService::vSubscribeForEnvData: Subscribed-%d ",
      ETG_ENUM(BOOL, bSubscribe)));

   if(NULL!= m_poDataSrvcRespIntf)
   {
      m_poDataSrvcRespIntf->vSubscribeForEnvData(bSubscribe);
   }

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclDataService::vSetFeatureRestrictions(...)
 ***************************************************************************/
t_Void spi_tclDataService::vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
      const t_U8 cou8ParkModeRestrictionInfo,const t_U8 cou8DriveModeRestrictionInfo)
{
   ETG_TRACE_USR1(("[DESC]::spi_tclDataService::vSetFeatureRestrictions() Entered with Device Category = %d",
         ETG_ENUM( DEVICE_CATEGORY, enDevCategory)));

   t_U8 u8ServiceLstIndx = enGetDataServiceIndex(enDevCategory);
   if(NULL != m_poDataSvcHandlersLst[u8ServiceLstIndx])
   {
      //send park/drive restriction to corresponding device category
      m_poDataSvcHandlersLst[u8ServiceLstIndx]->vSetFeatureRestrictions(cou8ParkModeRestrictionInfo,
            cou8DriveModeRestrictionInfo);
   }
}

//lint –restore


///////////////////////////////////////////////////////////////////////////////
// <EOF>
