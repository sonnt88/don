/***********************************************************************/
/*!
* \file  spi_tclDataServiceSettings.cpp
* \brief Class to get the DataService Settings
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the DataService Settings
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
26.05.2015  | Ramya Murthy          | Initial Version

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "FileHandler.h"
#include "XmlDocument.h"
#include "XmlReader.h"
#include "spi_tclDataServiceSettings.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DATASERVICE
      #include "trcGenProj/Header/spi_tclDataServiceSettings.cpp.trc.h"
   #endif
#endif

#ifdef VARIANT_S_FTR_ENABLE_GM
   static const t_Char* pczConfigFilePath = "/opt/bosch/gm/policy.xml";
#else
   static const t_Char* pczConfigFilePath = "/opt/bosch/spi/xml/policy.xml";
#endif

using namespace shl::xml;
/***************************************************************************
** FUNCTION:  spi_tclDataServiceSettings::spi_tclDataServiceSettings()
***************************************************************************/
spi_tclDataServiceSettings::spi_tclDataServiceSettings()
   : m_bCdbGPSSeviceEnabled(false),
     m_bCdbLocationSeviceEnabled(false),
     m_bSubForEnvData(false)
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings() entered "));
   vReadAppSettings();
}

/***************************************************************************
** FUNCTION:  spi_tclDataServiceSettings::~spi_tclDataServiceSettings()
***************************************************************************/
spi_tclDataServiceSettings::~spi_tclDataServiceSettings()
{
   ETG_TRACE_USR1(("~spi_tclDataServiceSettings() entered "));
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetCdbGPSSeviceEnabled()
***************************************************************************/
t_Bool spi_tclDataServiceSettings::bGetCdbGPSSeviceEnabled() const
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings::bGetCdbGPSSeviceEnabled: %u ",
         ETG_ENUM(BOOL, m_bCdbGPSSeviceEnabled)));
   return m_bCdbGPSSeviceEnabled;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetCdbLocationSeviceEnabled()
***************************************************************************/
t_Bool spi_tclDataServiceSettings::bGetCdbLocationSeviceEnabled() const
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings::bGetCdbLocationSeviceEnabled: %u ",
            ETG_ENUM(BOOL, m_bCdbLocationSeviceEnabled)));
   return m_bCdbLocationSeviceEnabled;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetEnvDataSubEnabled()
***************************************************************************/
t_Bool spi_tclDataServiceSettings::bGetEnvDataSubEnabled() const
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings:::bGetEnvDataSubEnabled() entered "));
   ETG_TRACE_USR2(("[DESC]:The value for enabling environment status data is = %d ",
   m_bSubForEnvData));
   return m_bSubForEnvData;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetGearStatusDisable()
***************************************************************************/
t_Bool spi_tclDataServiceSettings::bGetGearStatusEnabled() const
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings:::bGetGearStatusEnabled() entered "));
   ETG_TRACE_USR2(("[DESC]:The value for enabling gear status data is = %d ",
   m_bSubForGearStatus));
   return m_bSubForGearStatus;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetAccelerometerDataDisable()
***************************************************************************/
t_Bool spi_tclDataServiceSettings::bGetAccelerometerDataEnabled() const
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings:::bGetAccelerometerDataEnabled() entered "));
   ETG_TRACE_USR2(("[DESC]:The value for enabling accelerometer data is = %d ",
   	m_bSubForAcclData));
   return m_bSubForAcclData;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetGyroDataDisabled()
***************************************************************************/
t_Bool spi_tclDataServiceSettings::bGetGyroDataEnabled() const
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings::bGetGyroDataEnabled() entered "));
   ETG_TRACE_USR2(("[DESC]:The value for enabling Gyro data is = %d ",
   m_bSubForGyroData));
   return m_bSubForGyroData;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclDataServiceSettings::vReadAppSettings()
***************************************************************************/
t_Void spi_tclDataServiceSettings::vReadAppSettings()
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings::vReadAppSettings() entered "));

   //Check the validity of the xml file
   spi::io::FileHandler oPolicySettingsFile(pczConfigFilePath, spi::io::SPI_EN_RDONLY);
   if (true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument oXmlDoc(pczConfigFilePath);
      tclXmlReader oXmlReader(&oXmlDoc, this);

      oXmlReader.bRead("DATASERVICE");
   } // if (true == oPolicySettingsFile.bIsValid())
}

/*************************************************************************
** FUNCTION:  t_Bool spi_tclDataServiceSettings::bXmlReadNode(xmlNode *poNode)
*************************************************************************/
t_Bool spi_tclDataServiceSettings::bXmlReadNode(xmlNodePtr poNode)
{
   ETG_TRACE_USR1(("spi_tclDataServiceSettings::bXmlReadNode() entered "));

   t_String szAttrName;
   t_Bool bRetVal = false;
   t_String szNodeName;

   if (NULL != poNode)
   {
      szNodeName = (const t_Char *) (poNode->name);
   } // if (NULL != poNode)

   if ("CDB_GPS_SERVICE" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bCdbGPSSeviceEnabled);
      ETG_TRACE_USR4(("bXmlReadNode(): CDB GPS Service enabled = %d ", ETG_ENUM(BOOL,m_bCdbGPSSeviceEnabled)));
   }
   else if ("CDB_LOCATION_SERVICE" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bCdbLocationSeviceEnabled);
      ETG_TRACE_USR4(("bXmlReadNode(): CDB Location Service enabled = %d ", ETG_ENUM(BOOL,m_bCdbLocationSeviceEnabled)));
   } //if ("CDB_LOCATION_SERVICE" == szNodeName)
   else if ("ENVIRONMENT_DATA_SUB" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bSubForEnvData);
      ETG_TRACE_USR4(("bXmlReadNode(): Subscribe For Environment Data = %d ", ETG_ENUM(BOOL,m_bSubForEnvData)));
   } //if ("CDB_LOCATION_SERVICE" == szNodeName)
   else if ("GEAR_STATUS" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bSubForGearStatus);
      ETG_TRACE_USR4(("bXmlReadNode(): Subscribe For gear status = %d ", ETG_ENUM(BOOL,m_bSubForGearStatus)));
   }
   else if ("ACCELEROMETER_DATA" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode,  m_bSubForAcclData);
      ETG_TRACE_USR4(("bXmlReadNode(): Subscribe For accelerometer data = %d ", ETG_ENUM(BOOL,m_bSubForAcclData)));
   }
   else if ("GYRO_DATA" == szNodeName)
   {
      szAttrName = "BOOL";
      bRetVal = bGetAttribute(szAttrName, poNode, m_bSubForGyroData);
      ETG_TRACE_USR4(("bXmlReadNode(): Subscribe For gyro Data = %d ", ETG_ENUM(BOOL,m_bSubForGyroData)));
   }

   return bRetVal;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
