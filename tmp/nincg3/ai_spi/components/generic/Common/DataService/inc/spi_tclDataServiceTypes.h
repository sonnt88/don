  /*!
 *******************************************************************************
 * \file         spi_tclDataServiceTypes.h
 * \brief        Data Service type defines
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Data Service type defines
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 01.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 14.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Updated & merged all DataService Manager
                                                  callbacks to a single structure.
 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLDATASERVICETYPES_H
#define _SPI_TCLDATASERVICETYPES_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <functional>

#include "SPITypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

//! Callback signature definitions
typedef std::function<t_Void(t_Bool, tenLocationDataType)> vSubscribeForData;

typedef std::function<t_Void(t_Bool)> tvSubscribeForEnvData;

//! \brief   Structure holding the callbacks to DataService manager.
struct trDataServiceCb
{
   //! Called by ML/Dipo DataService handler to subscribe for Location Data notifications.
   vSubscribeForData     fvSubscribeForLocationData;

   tvSubscribeForEnvData fvSubscribeForEnvData;

   trDataServiceCb(): fvSubscribeForLocationData(NULL),
      fvSubscribeForEnvData(NULL)
   {
   }
};

#endif // _SPI_TCLDATASERVICETYPES_H

