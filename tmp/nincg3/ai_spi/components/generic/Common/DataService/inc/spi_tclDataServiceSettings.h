/***********************************************************************/
/*!
* \file  spi_tclDataServiceSettings.h
* \brief Class to get the DataService Settings
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the DataService Settings
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
26.05.2015  | Ramya Murthy          | Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLDATASERVICESETTINGS_H_
#define _SPI_TCLDATASERVICESETTINGS_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "GenericSingleton.h"
#include "Xmlable.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclDataServiceSettings
* \brief Class to get the Bluetooth and Telephone services Info
****************************************************************************/
class spi_tclDataServiceSettings: public GenericSingleton<spi_tclDataServiceSettings>,
   public shl::xml::tclXmlReadable
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDataServiceSettings::~spi_tclDataServiceSettings()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclDataServiceSettings()
   * \brief   Destructor
   * \sa      spi_tclDataServiceSettings()
   **************************************************************************/
   ~spi_tclDataServiceSettings();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetCdbGPSSeviceEnabled()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bGetCdbGPSSeviceEnabled()
   * \brief  Interface to read whether GPS CDB service is enabled
   * \retval t_Bool
   **************************************************************************/
   t_Bool bGetCdbGPSSeviceEnabled() const;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetCdbLocationSeviceEnabled()
   ***************************************************************************/
   /*!
   * \fn     bGetCdbLocationSeviceEnabled()
   * \brief  Interface to read whether Location CDB service is enabled
   * \retval t_Bool
   **************************************************************************/
   t_Bool bGetCdbLocationSeviceEnabled() const;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataServiceSettings::vReadAppSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vReadAppSettings()
   * \brief  To read the settings from XML
   * \retval t_Void
   **************************************************************************/
   t_Void vReadAppSettings();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetEnvDataSubEnabled()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bGetEnvDataSubEnabled() const
   * \brief  Interface to get whether to subscribe for Environment data or not
   * \retval t_Bool
   **************************************************************************/
   t_Bool bGetEnvDataSubEnabled() const;

   /***************************************************************************
     ** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetGearStatusEnabled()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bGetGearStatusDisable() const
   * \brief  Interface to get whether to subscribe for Gear Status data or not
   * \retval t_Bool
   **************************************************************************/
   t_Bool bGetGearStatusEnabled() const;

   /***************************************************************************
     ** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetAccelerometerDataEnabled()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bGetAccelerometerDataDisable() const
   * \brief  Interface to get whether to subscribe for Accelerometer data or not
   * \retval t_Bool
   **************************************************************************/
   t_Bool bGetAccelerometerDataEnabled() const;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclDataServiceSettings::bGetGyroDataEnabled()
   ***************************************************************************/
   /*!
   * \fn     t_Bool bGetGyroDataEnabled() const
   * \brief  Interface to get whether to subscribe for Gyroscope data or not
   * \retval t_Bool
   **************************************************************************/
   t_Bool bGetGyroDataEnabled() const;

   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<spi_tclDataServiceSettings>;


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDataServiceSettings::spi_tclDataServiceSettings()
   ***************************************************************************/
   /*!
   * \fn      spi_tclDataServiceSettings()
   * \brief   Default Constructor
   * \sa      ~spi_tclDataServiceSettings()
   **************************************************************************/
   spi_tclDataServiceSettings();

   /***************************************************************************
   ** FUNCTION:  spi_tclDataServiceSettings& spi_tclDataServiceSettings::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDataServiceSettings& operator= (const spi_tclDataServiceSettings &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDataServiceSettings& operator= (const spi_tclDataServiceSettings &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclDataServiceSettings::spi_tclDataServiceSettings(const ..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDataServiceSettings(const spi_tclDataServiceSettings &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDataServiceSettings(const spi_tclDataServiceSettings &corfrSrc);

   /*************************************************************************
   ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
   *************************************************************************/
   /*!
   * \fn     virtual bool bXmlReadNode(xmlNode *poNode)
   * \brief  virtual function to read data from a xml node
   * \param  poNode : [IN] pointer to xml node
   * \retval bool : true if success, false otherwise.
   *************************************************************************/
   virtual t_Bool bXmlReadNode(xmlNodePtr poNode);

   /*************************************************************************
    ** Data Members
    *************************************************************************/

   /*
    * \brief   GPS service info
    */
   t_Bool   m_bCdbGPSSeviceEnabled;

   /*
    * \brief   Location service info
    */
   t_Bool   m_bCdbLocationSeviceEnabled;

   t_Bool   m_bSubForEnvData;

   t_Bool   m_bSubForGearStatus;

   t_Bool   m_bSubForAcclData;

   t_Bool   m_bSubForGyroData;


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclDataServiceSettings

#endif //_SPI_TCLDATASERVICESETTINGS_H_
