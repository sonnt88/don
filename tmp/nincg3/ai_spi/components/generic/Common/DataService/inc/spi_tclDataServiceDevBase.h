/*!
 *******************************************************************************
 * \file             spi_tclDataServiceDevBase.h
 * \brief            Abstract class that specifies the Data Service interface which
 *                   must be implemented by device class (Mirror Link/Digital iPod out)
 *                   for communication with SDK
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class for ML and DiPO Interfaces
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 27.03.2014 |  Ramya Murthy  (RBEI/ECP2)        | Initial Version
 13.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Interface revised to adapt to MPlay FI
                                                  extn to start/stop loc info, and VDSensor 
                                                  data integration in loc data.
 13.10.2014 |  Hari Priya E R (RBEI/ECP2)       | Added interface for Vehicle Data for PASCD.
 05.02.2015 |  Ramya Murthy (RBEI/ECP2)         | Added interface to set availability of LocationData
 28.03.2015 |  SHITANSHU SHEKHAR (RBEI/ECP2)    | Added interface to set vehicle config
 23.04.2015 |  Ramya Murthy (RBEI/ECP2)         | Added interface to set region code

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLDATASERVICEDEVBASE_H
#define SPI_TCLDATASERVICEDEVBASE_H

#include "BaseTypes.h"
#include "spi_tclDataServiceTypes.h"

/**
 * Abstract class that specifies the Data Service interface which
 * must be implemented by device class (Mirror Link/Digital iPod out)
 * for communication with SDK
 */

class spi_tclDataServiceDevBase
{
public:

	/***************************************************************************
	*********************************PUBLIC*************************************
	***************************************************************************/

	/***************************************************************************
    ** FUNCTION:  spi_tclDataServiceDevBase::spi_tclDataServiceDevBase();
    ***************************************************************************/
   /*!
    * \fn      spi_tclDataServiceDevBase()
    * \brief   Default Constructor
    ***************************************************************************/
   spi_tclDataServiceDevBase()
   {
   }

   /***************************************************************************
    ** FUNCTION:  spi_tclDataServiceDevBase::~spi_tclDataServiceDevBase();
    ***************************************************************************/
   /*!
    * \fn      ~spi_tclDataServiceDevBase()
    * \brief   Virtual Destructor
    ***************************************************************************/
   virtual ~spi_tclDataServiceDevBase()
   {
   }

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclDataServiceDevBase::bInitialise();
    ***************************************************************************/
   /*!
    * \fn      bInitialise()
    * \brief   Method to initialises the service handler. (Performs initialisations which
    *          are not device specific.)
    *          Mandatory interface to be implemented.
    * \retval  t_Bool: TRUE - If ServiceHandler is initialised successfully, else FALSE.
    * \sa      bUninitialise()
    ***************************************************************************/
   virtual t_Bool bInitialise() = 0;

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclDataServiceDevBase::bUninitialise();
    ***************************************************************************/
   /*!
    * \fn      bUninitialise()
    * \brief   Method to uninitialise the service handler.
    *          Mandatory interface to be implemented.
    * \retval  t_Bool: TRUE - If ServiceHandler is uninitialised successfully, else FALSE.
    * \sa      bInitialise()
    ***************************************************************************/
   virtual t_Bool bUninitialise() = 0;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vSelectDevice(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vSelectDevice
    * \brief   Called when a device is selection request is received
    * \param   [IN] cou32DevId: Unique handle of selected device
    * \param   [IN] enConnReq: Indicated the category of the device
    * \retval  None
    **************************************************************************/

   virtual t_Void vSelectDevice(const t_U32 cou32DevId,
         tenDeviceConnectionReq enConnReq){};

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vOnSelectDeviceResult(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vOnSelectDeviceResult(t_U32 u32DeviceHandle)
    * \brief   Called when a device is selected.
    *          Mandatory interface to be implemented.
    * \param   [IN] u32DeviceHandle: Unique handle of selected device
    * \retval  None
    **************************************************************************/
   virtual t_Void vOnSelectDeviceResult(t_U32 u32DeviceHandle) = 0;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vOnDeselectDeviceResult(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vOnDeselectDeviceResult(t_U32 u32DeviceHandle)
    * \brief   Called when currently selected device is de-selected.
    *          Mandatory interface to be implemented.
    * \param   [IN] u32DeviceHandle: Unique handle of selected device
    * \retval  None
    **************************************************************************/
   virtual t_Void vOnDeselectDeviceResult(t_U32 u32DeviceHandle) = 0;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vStartLocationData(...)
    ***************************************************************************/
   /*!
    * \fn      vStartLocationData(
    *             const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList)
    * \brief   Called to start sending LocationData to selected device.
    *          Mandatory interface to be implemented.
    * \param   [IN] rfcoNmeaSentencesList: List of NMEA sentences to be sent in
    *             location data.
    * \retval  None
    **************************************************************************/
   virtual t_Void vStartLocationData(
         const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList){}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vStopLocationData(...)
    ***************************************************************************/
   /*!
    * \fn      vStopLocationData()
    * \brief   Called to stop sending LocationData to selected device.
    *          Mandatory interface to be implemented.
    * \param   [IN] rfcoNmeaSentencesList: List of NMEA sentences which should
    *             be stopped in location data.
    * \retval  None
    **************************************************************************/
   virtual t_Void vStopLocationData(
         const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   *          Optional interface to be implemented.
   * \param   rfcorGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vOnData(const trSensorData&...)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vOnAccSensorData
   ** (const std::vector<trAccSensorData>& corfvecrAccSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   corfvecrAccSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
   {
	   SPI_INTENTIONALLY_UNUSED(corfvecrAccSensorData);
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vOnGyroSensorData
   ** (const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   corfvecrGyroSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   {
	   SPI_INTENTIONALLY_UNUSED(corfvecrGyroSensorData);
   }

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vOnData(
                 const trVehicleData& rfcoVehicleData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trVehicleData& rfcoVehicleData)
   * \brief   Method to receive Vehicle data.
   *          Optional interface to be implemented.
   * \param   rfcoVehicleData: [IN] Vehicle data
   * \param   bSolicited: [IN] True if the data update is for changed vehicle data, else False
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trVehicleData& rfcoVehicleData, t_Bool bSolicited){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataServiceDevBase::vSetLocDataAvailablility(...)
   ***************************************************************************/
   /*!
   * \fn      vSetLocDataAvailablility(t_Bool bLocDataAvailable)
   * \brief   Interface to set the availability of LocationData
   * \param   rfrDataServiceConfigData
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSensorDataAvailablility(const trDataServiceConfigData& rfrDataServiceConfigData){}

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDataServiceDevBase::vSetRegion(...)
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetRegion(tenRegion enRegion)
   * \brief  Interface to set the current region
   * \param  [IN] enRegion : Region enumeration
   * \sa
   **************************************************************************/
   virtual t_Void vSetRegion(tenRegion enRegion) {}

   /***************************************************************************
   ** FUNCTION:  spi_tclDataServiceDevBase::vReportEnvironmentData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
   *                                   t_Bool bValidPressureUpdate, t_Double dPressure)
   * \brief Use this call to report Environment data to Phone
   * \param bValidTempUpdate : [IN] Temp update is valid
   * \param dTemp : [IN] Temp in Celsius
   * \param bValidPressureUpdate: [IN] Pressure update is valid
   * \param dPressure : [IN] Pressure in KPA
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
      t_Bool bValidPressureUpdate, t_Double dPressure){}

   /***************************************************************************
    ** FUNCTION:  t_Void vSetFeatureRestrictions()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSetFeatureRestrictions(const t_U8 cou8ParkModeRestrictionInfo,
    *          const t_U8 cou8DriveModeRestrictionInfo)
    * \brief   Method to set Vehicle Park/Drive Mode Restriction
    * \param   cou8ParkModeRestrictionInfo : Park mode restriction
    *          cou8DriveModeRestrictionInfo : Drive mode restriction
    * \retval  t_Void
    **************************************************************************/

   virtual t_Void vSetFeatureRestrictions(const t_U8 cou8ParkModeRestrictionInfo,
         const t_U8 cou8DriveModeRestrictionInfo){}

private:

	/***************************************************************************
	*********************************PRIVATE************************************
	***************************************************************************/

};
#endif // SPI_TCLDATASERVICEDEVBASE_H

