/*!
 *******************************************************************************
 * \file         LocationDataSimulator.h
 * \brief        Location data simulator class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Location data simulator class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 19.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef _LOCDATASIMULATOR_H
#define _LOCDATASIMULATOR_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <fstream>
#include <sstream>

#include "SPITypes.h"
#include "GenericSingleton.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef std::ifstream         tInputStream;
typedef std::stringstream     tStringStream;
typedef std::istringstream    tInputStringStream;

//! Callback signatures definition
typedef std::function<t_Void(trGPSData)>        vOnSimGpsData;

//! \brief   Structure holding the callbacks to subscriber.
struct trLocDataSimulationCb
{
   //! Called to send simulated GPS data at defined interval.
   vOnSimGpsData     fvOnSimGpsData;

   trLocDataSimulationCb() : fvOnSimGpsData(NULL)
   {
   }
};

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \class LocationDataSimulator
* \brief Simulates location data provider (i.e, provides sample location data
*        for testing LocationService)
*/
class LocationDataSimulator : public GenericSingleton<LocationDataSimulator>
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::~LocationDataSimulator();
   ***************************************************************************/
   /*!
   * \fn      ~LocationDataSimulator()
   * \brief   Virtual Destructor
   ***************************************************************************/
   virtual ~LocationDataSimulator();

   /***************************************************************************
   ** FUNCTION:  t_Void LocationDataSimulator::vRegisterCallbacks()
   ***************************************************************************/
   t_Void vRegisterCallbacks(const trLocDataSimulationCb& rfrLocDataCb);

   /***************************************************************************
   ** FUNCTION:  t_Void LocationDataSimulator::vSimulateGpsData()
   ***************************************************************************/
   t_Void vSimulateGpsData(t_U32 u32DataRateInMs);


private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   friend class GenericSingleton<LocationDataSimulator>;

   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::LocationDataSimulator()
   ***************************************************************************/
   /*!
   * \fn     LocationDataSimulator()
   * \brief  Default Constructor
   ***************************************************************************/
   LocationDataSimulator();

   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::vExtractGpsData();
   ***************************************************************************/
   t_Void vExtractGpsData(const t_String& coszFileLine);

   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::szGetLineInFile(tInputStream& szDataFileStream)
   ***************************************************************************/
   t_String szGetLineInFile(tInputStream& szDataFileStream) const;

   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::vSimulateGPSDataTransfer();
   ***************************************************************************/
   std::vector<t_String> SplitString(const t_String& szString, char czDelimiter) const;

   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::dGetDoubleValue();
   ***************************************************************************/
   t_Double dGetDoubleValue(const t_String& szValue) const;

   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::dGetSignedIntValue();
   ***************************************************************************/
   t_S32 dGetSignedIntValue(const t_String& szValue) const;

   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::dGetUnsignedIntValue();
   ***************************************************************************/
   t_U32 dGetUnsignedIntValue(const t_String& szValue) const;


   /***************************************************************************
   ** FUNCTION:  LocationDataSimulator::operator=(const LocationDataSimulator&...)
   ***************************************************************************/
   /*!
   * \fn     operator=(const LocationDataSimulator& rfcooDataService)
   * \brief  Assignment operator, not implemented.
   ***************************************************************************/
   LocationDataSimulator& operator=(const LocationDataSimulator& rfcooLocDataSimulator);


   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   trLocDataSimulationCb   m_rLocDataCb;

};

#endif // _LOCDATASIMULATOR_H

