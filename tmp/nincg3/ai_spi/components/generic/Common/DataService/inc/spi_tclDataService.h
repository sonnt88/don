  /*!
 *******************************************************************************
 * \file         spi_tclDataService.h
 * \brief        Data Service Manager class that provides interface to delegate
                 the execution of command and handle response
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Data Service Manager class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 27.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 14.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Implemented sending GPS data to
                                                  MediaPlayer client.
 06.04.2014 |  Ramya Murthy (RBEI/ECP2)         | Initialisation sequence implementation
 13.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Implementation for: 
                                                  (1) MPlay FI extn to start/stop loc info
                                                  (2) VDSensor data integration
                                                  (3) NMEA-PASCD sentence for DiPO
 31.07.2014 |  Ramya Murthy (RBEI/ECP2)         | SPI feature configuration via LoadSettings()
 13.10.2014 |  Hari Priya E R (RBEI/ECP2)       | Added interface for Vehicle Data for PASCD and
                                                  interface with Data Service Response
 05.02.2015 |  Ramya Murthy (RBEI/ECP2)         | Added interface to set availability of LocationData
 28.03.2015 |  SHITANSHU SHEKHAR (RBEI/ECP2)    | Added index for ANDROIDAUTO and
                                                   declared following methods :
                                                   enGetDataServiceIndex()
23.04.2015  |  Ramya Murthy (RBEI/ECP2)         | Added interface to set region code
30.10.2015  | Shiva Kumar G                     | Implemented ReportEnvironment Data feature

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLDATASERVICE_H
#define _SPI_TCLDATASERVICE_H

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclDataServiceTypes.h"
#include "spi_tclLifeCycleIntf.h"
#include "spi_tclDataServiceRespIntf.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
/*!
 * \brief Index to various DataService handlers
 */
enum tenDataServicesIndex
{
   e8_DATASVC_ML_INDEX = 0,
   e8_DATASVC_DIPO_INDEX = 1,
   e8_DATASVC_ANDROIDAUTO_INDEX = 2,

   //! Please add new device types above this in consecutive order
   //! This is required to maintain the size of enum
   e8_DATASVC_INDEX_SIZE
};


/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/* Forward Declarations. */
class spi_tclDataServiceDevBase;

/*!
* \class spi_tclDataService
* \brief This is the main Data Service Manager class that provides interface
*        to delegate the execution of command and handle response.
*/
class spi_tclDataService : public spi_tclLifeCycleIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclDataService::spi_tclDataService(ahl_tclBaseOneThreadApp...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclDataService(ahl_tclBaseOneThreadApp* poMainApp,
              spi_tclDataServiceRespIntf* poDataSrvcRespIntf)
   * \brief  Parametrized Constructor
   * \param  poMainApp : [IN] Pointer to main app.
   * \param  poDataSrvcRespIntf : [IN]Pointer to Data Service Response Interface
   ***************************************************************************/
   spi_tclDataService(ahl_tclBaseOneThreadApp* poMainApp,
      spi_tclDataServiceRespIntf* poDataSrvcRespIntf);

   /***************************************************************************
   ** FUNCTION:  spi_tclDataService::~spi_tclDataService();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclDataService()
   * \brief   Virtual Destructor
   ***************************************************************************/
   virtual ~spi_tclDataService();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclDataService::bInitialize()
    ***************************************************************************/
   /*!
    * \fn      bInitialize()
    * \brief   Method to initialise the DataService manager.
   * \retval   t_Bool: TRUE - If service manager is initialised successfully, else FALSE.
    * \sa      bUnInitialize()
    **************************************************************************/
   virtual t_Bool bInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclDataService::bUnInitialize()
    ***************************************************************************/
   /*!
    * \fn      bUnInitialize()
   * \brief    Method to uninitialise the DataService manager
   * \retval   t_Bool: TRUE - If service manager is uninitialised successfully, else FALSE.
    * \sa      bInitialize()
    **************************************************************************/
   virtual t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataService::vLoadSettings(const trSpiFeatureSupport&...)
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
    * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclDataService::vSaveSettings()
    ***************************************************************************/
   /*!
    * \fn      vSaveSettings()
    * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
    * \sa      vLoadSettings()
    **************************************************************************/
   virtual t_Void vSaveSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSelectDevice(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenDeviceCategory coenDevCat,
   *                 const trUserContext& corfrcUsrCntxt)
   * \brief   To setup Video related info when a device is selected or
   *          de selected.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    enConnReq : [IN] Identifies the Connection Type.
   * \pram    enDevCat  : [IN] Identifies the Connection Request.
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSelectDevice(const t_U32 cou32DevId, tenDeviceConnectionReq enConnReq,
         tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vOnSelectDeviceResult(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSelectDeviceResult(t_U32 u32DeviceHandle,
   *             tenDeviceConnectionReq enDevConnReq,
   *             tenResponseCode enRespCode, tenErrorCode enErrorCode)
   * \brief   Called when SelectDevice operation is complete & with the result
   * 			  of the operation.
   * \param   [IN] u32DeviceHandle: Unique handle of selected device
   * \param   [IN] enDeviceConnReq: Connection request type for the device
   * \param   [IN] enRespCode: Response code enumeration
   * \param   [IN] enErrorCode: Error code enumeration
   * \retval  None
   **************************************************************************/
   virtual t_Void vOnSelectDeviceResult(t_U32 u32DeviceHandle,
         tenDeviceConnectionReq enDeviceConnReq,
         tenResponseCode enRespCode,
         tenErrorCode enErrorCode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vStartStopLocationData(...)
   ***************************************************************************/
   /*!
   * \fn      vStartStopLocationData()
   * \brief   Called when SelectDevice operation is complete & with the result
   *          of the operation.
   * \param   [IN] u32DeviceHandle: Unique handle of selected device
   * \param   [IN] enDeviceConnReq: Connection request type for the device
   * \param   [IN] enRespCode: Response code enumeration
   * \param   [IN] enErrorCode: Error code enumeration
   * \retval  None
   **************************************************************************/
   virtual t_Void vStartStopLocationData(t_Bool bStartLocData,
         const std::vector<tenNmeaSentenceType>& rfcoNmeaSentencesList);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vOnGPSData(const trGPSData& rfrcGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnGPSData(const trGPSData& rfrcGpsData)
   * \brief   Method to receive GPS location data.
   * \param   rfrcGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnGPSData(const trGPSData& rfrcGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vOnSensorData(const trSensorData& rfrcSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnSensorData(const trSensorData& rfrcSensorData)
   * \brief   Method to receive Sensor data.
   * \param   rfrcSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnSensorData(const trSensorData& rfrcSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vOnAccSensorData
   ** (const std::vector<trAccSensorData>& corfvecrAccSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
   * \brief   Method to receive Sensor data.
   * \param   corfvecrAccSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vOnGyroSensorData
   ** (const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
   * \brief   Method to receive Sensor data.
   * \param   corfvecrGyroSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vOnVehicleData(const trVehicleData 
                                     rfrcVehicleData)
   ***************************************************************************/
   /*!
   * \fn      vOnVehicleData(const trVehicleData rfrcVehicleData)
   * \brief   Method to receive Vehicle data.
   * \param   rfrcVehicleData: [IN] Vehicle data
   * \param   bSolicited: [IN] True if the data update is for changed vehicle data, else False
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnVehicleData(const trVehicleData rfrcVehicleData, t_Bool bSolicited);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vSetLocDataAvailablility(t_Bool...)
   ***************************************************************************/
   /*!
   * \fn      vSetLocDataAvailablility(t_Bool bLocDataAvailable)
   * \brief   Interface to set the availability of LocationData
   * \param   [IN] bLocDataAvailable:
   *              If true - Location data is available
   *              If false - Location data is unavailable
   * \param   [IN] bIsDeadReckonedData:
   *              True - if Location data is dead reckoned data, else False
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetLocDataAvailablility(t_Bool bLocDataAvailable, t_Bool bIsDeadReckonedData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclDataService::vSimulateGPSDataTransfer(t_U32 u32DataRateInMs)
   ***************************************************************************/
   /*!
   * \fn      vSimulateGPSDataTransfer(t_U32 u32DataRateInMs)
   * \brief   Method to start simulation of GPS data. This triggers vOnGpsData()
   *          at the rate of u32DataRateInMs, until sample GPS data is completely sent.
   * \param   u32DataRateInMs: [IN] Rate at which sample GPS data is required.
   * \retval  None
   ***************************************************************************/
   t_Void vSimulateGPSDataTransfer(t_U32 u32DataRateInMs);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclDataService::vSetRegion(...)
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetRegion(tenRegion enRegion)
   * \brief  Interface to set the current region
   * \param  [IN] enRegion : Region enumeration
   * \sa
   **************************************************************************/
   virtual t_Void vSetRegion(tenRegion enRegion);

   /***************************************************************************
   ** FUNCTION:  spi_tclDataService::vReportEnvironmentData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
   *                                   t_Bool bValidPressureUpdate, t_Double dPressure)
   * \brief Use this call to report Environment data to Phone
   * \param bValidTempUpdate : [IN] Temp update is valid
   * \param dTemp : [IN] Temp in Celsius
   * \param bValidPressureUpdate: [IN] Pressure update is valid
   * \param dPressure : [IN] Pressure in KPA
   * \retval t_Void
   **************************************************************************/
   t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
      t_Bool bValidPressureUpdate, t_Double dPressure);

   /***************************************************************************
    ** FUNCTION:  t_Void vSetFeatureRestrictions()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
    *          const t_U8 cou8ParkModeRestrictionInfo,
    *          const t_U8 cou8DriveModeRestrictionInfo)
    * \brief   Method to set Vehicle Park/Drive Mode Restriction
    * \param   enDevCategory : Device Category
    *          cou8ParkModeRestrictionInfo : Park mode restriction
    *          cou8DriveModeRestrictionInfo : Drive mode restriction
    * \retval  t_Void
    **************************************************************************/

   t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
         const t_U8 cou8ParkModeRestrictionInfo,const t_U8 cou8DriveModeRestrictionInfo);

private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tenDeviceCategory spi_tclDataService::enGetDeviceCategory(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      enGetDeviceCategory(t_U32 u32DeviceHandle)
   * \brief   Retrieves the category of a projection device
   * \param   [IN] u32DeviceHandle: Unique handle of Projection device.
   * \retval  tenDeviceCategory: Category of projection device
   **************************************************************************/
   tenDeviceCategory enGetDeviceCategory(t_U32 u32DeviceHandle);


   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataService::getDataServiceIndex(...)
    ***************************************************************************/
   /*!
   * \fn      getDataServiceIndex(tenDeviceCategory);
   * \brief   Used to get data service index
   * \param   [IN] enDataCategory : Category of device
   * \retval  tenDataServicesIndex : Data service index returned
   **************************************************************************/
   tenDataServicesIndex enGetDataServiceIndex(tenDeviceCategory enDataCategory);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataService::vOnSubscribeForLocData(t_Bool...)
    ***************************************************************************/
   /*!
   * \fn      vOnSubscribeForLocData(t_Bool bSubscribe, tenLocationDataType enLocDataType)
   * \brief   Subscribes/Unsubscribes for location data notifications from
   *              service-provider (such as POS_FI).
   * \param   [IN] bSubscribe:
   *               TRUE - To subscribe for location data notifications.
   *               FALSE - To unsubscribe for location data notifications.
   * \param   [IN] enLocDataType: Type of data to be subscribed/unsubscribed
   * \retval  None
   **************************************************************************/
   t_Void vOnSubscribeForLocData(t_Bool bSubscribe, tenLocationDataType enLocDataType);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataService::vSubscribeForEnvData()
    ***************************************************************************/
   /*!
   * \fn      t_Void vSubscribeForEnvData(t_Bool bSubscribe)
   * \brief   Subscribe/Unsubscribe for Environment updates 
   * \param   [IN] bSubscribe: TRUE - Subscribe
   *                           FALSE - Unsubscribe
   * \retval  None
   **************************************************************************/
   t_Void vSubscribeForEnvData(t_Bool bSubscribe);

   /***************************************************************************
   ** FUNCTION:  spi_tclDataService::spi_tclDataService()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDataService()
   * \brief  Default Constructor, not implemented.
   ***************************************************************************/
   spi_tclDataService();

   /***************************************************************************
   ** FUNCTION:  spi_tclDataService::operator=(const spi_tclDataService&...)
   ***************************************************************************/
   /*!
   * \fn     operator=(const spi_tclDataService& rfcooDataService)
   * \brief  Assignment operator, not implemented.
   ***************************************************************************/
   spi_tclDataService& operator=(const spi_tclDataService& rfcooDataService);


   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   /***************************************************************************
   ** Main application pointer
   ***************************************************************************/
   ahl_tclBaseOneThreadApp*         m_poMainApp;


   /***************************************************************************
   ** List of pointers to DataService Handlers of different device types
   ***************************************************************************/
   spi_tclDataServiceDevBase*       m_poDataSvcHandlersLst[e8_DATASVC_INDEX_SIZE];

   /***************************************************************************
   ** Category type of selected device
   ***************************************************************************/
   tenDeviceCategory                m_enSelDevCategory;

   /***************************************************************************
   ** Data Service Response Interface Pointer
   ***************************************************************************/
   spi_tclDataServiceRespIntf*      m_poDataSrvcRespIntf;

};

#endif // _SPI_TCLDATASERVICE_H

