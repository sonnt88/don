/***********************************************************************/
/*!
* \file  spi_tclVideo.h
* \brief Main Video class that provides interface to delegate 
*        the execution of command and handle response
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
06.01.2014  | Hari Priya E R        | Changes for Layer Sync handling
21.01.2014  | Shiva Kumar Gurija    | Updated with Video Response Interface
03.04.2013  | Shiva Kumar Gurija    | Device status messages
                                      API's in CmdInterface
06.04.2014  |  Ramya Murthy         | Initialisation sequence implementation
25.05.2014  |  Hari Priya E R       | Removed function for setting screen variant
16.07.2014  | Shiva Kumar Gurija    | Implemented SessionStatusInfo update
31.07.2014  |  Ramya Murthy         | SPI feature configuration via LoadSettings()
06.11.2014  |  Hari Priya E R       | Added changes to set Client key capabilities
03.07.2015  |   Shiva Kumar G       | Dynamic display configuration

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVIDEO_H_
#define _SPI_TCLVIDEO_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclVideoIntf.h"
#include "spi_tclLifeCycleIntf.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
class ahl_tclBaseOneThreadApp;
class spi_tclVideoDevBase;
class spi_tclVideoRespInterface;
class spi_tclVideoPolicyBase;


/****************************************************************************/
/*!
* \class spi_tclVideo
* \brief 
****************************************************************************/
class spi_tclVideo:public spi_tclVideoIntf, public spi_tclLifeCycleIntf
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/
   /***************************************************************************
   ** FUNCTION:  spi_tclVideo::spi_tclVideo()
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideo()
   * \brief   Default Constructor
   * \sa      ~spi_tclVideo()
   **************************************************************************/
   spi_tclVideo(ahl_tclBaseOneThreadApp* poMainApp,
      spi_tclVideoRespInterface* poVideoRespIntf);


   /***************************************************************************
   ** FUNCTION:  spi_tclVideo::~spi_tclVideo()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclVideo()
   * \brief   Destructor
   * \sa      spi_tclVideo()
   **************************************************************************/
   virtual ~spi_tclVideo();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclVideo::bInitialize()
    ***************************************************************************/
   /*!
    * \fn      bInitialize()
    * \brief   Method to Initialize
    * \sa      bUnInitialize()
    **************************************************************************/
   virtual t_Bool bInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclVideo::bUnInitialize()
    ***************************************************************************/
   /*!
    * \fn      bUnInitialize()
    * \brief   Method to UnInitialize
    * \sa      bInitialize()
    **************************************************************************/
   virtual t_Bool bUnInitialize();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclVideo::vLoadSettings(const trSpiFeatureSupport&...)
    ***************************************************************************/
   /*!
    * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
    * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
    * \sa      vSaveSettings()
    **************************************************************************/
   virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclVideo::vSaveSettings()
    ***************************************************************************/
   /*!
    * \fn      vSaveSettings()
    * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
    * \sa      vLoadSettings()
    **************************************************************************/
   virtual t_Void vSaveSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideo::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSelectDevice(const t_U32 cou32DevId, 
   *                 const tenDeviceConnectionReq coenConnReq, 
   *                 const tenDeviceCategory coenDevCat,
   *                 const trUserContext& corfrcUsrCntxt)
   * \brief   To setup Video related info when a device is selected or
   *          de selected.
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Type.
   * \pram    coenDevCat  : [IN] Identifies the Connection Request.
   * \pram    corfrcUsrCntxt: [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId, 
      const tenDeviceConnectionReq coenConnReq, 
      const tenDeviceCategory coenDevCat,
      const trUserContext& corfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclVideo::bLaunchVideo()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bLaunchVideo(const t_U32 cou32DevId,
   *                 const t_U32 cou32AppId,
   *                 const tenDeviceCategory coenDevCat,
   *                 const tenEnabledInfo coenSelection)
   * \brief   To Launch the Video for the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    cou32AppId  : [IN] Application Id
   * \pram    coenDevCat  : [IN] Identifies the Connection Request.
   * \pram    coenSelection  : [IN] Enable/disable the video
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bLaunchVideo(const t_U32 cou32DevId,
      const t_U32 cou32AppId,
      const tenDeviceCategory coenDevCat,
      const tenEnabledInfo coenSelection=e8USAGE_ENABLED);


   /***************************************************************************
   ** FUNCTION:  t_U32  spi_tclVideo::vStartVideoRendering()
   ***************************************************************************/
   /*!
   * \fn      t_Void vStartVideoRendering(const t_Bool cobStartVideoRendering)
   * \brief   Method send request to ML/DiPo Video eithr to start or stop
   *          Video Rendering
   * \pram    cobStartVideoRendering : [IN] True - Start Video rendering
   *                                      False - Stop Video rendering
   * \retval  t_Void 
   * \sa      vCbUpdateVideoRenderStatus()
   **************************************************************************/
   t_Void vStartVideoRendering(const t_Bool cobStartVideoRendering);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vGetVideoSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vGetVideoSettings(const t_U32 cou32DevId,
   *                                  trVideoAttributes& rfrVideoAttr,
   *                                  const tenDeviceCategory coenDevCat)
   * \brief  To get the current Video Settings.
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  rfrVideoAttr     : Video Attributes
   * \pram   coenDevCat       : [IN] Identifies the Connection Request.
   * \retval t_Void
   **************************************************************************/
   t_Void vGetVideoSettings(const t_U32 cou32DevId,
      trVideoAttributes& rfrVideoAttr,
      const tenDeviceCategory coenDevCat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vSetScreenSize()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetScreenSize(const trScreenAttributes& corfrScreenAttributes,
   *                               const tenDeviceCategory coenDevCat,
   *                               const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the screen size of Head Unit.
   * \param  corScreenAttributes : [IN] Screen Setting attributes.
   * \param  coenDevCat          : [IN] Device ID
   * \param  corfrcUsrCntxt      : [IN] User Context Details.
   * \retval t_Void
   **************************************************************************/
   t_Void vSetScreenSize(const trScreenAttributes& corfrScreenAttributes,
	   const tenDeviceCategory coenDevCat,
	   const trUserContext& corfrcUsrCntxt);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vSetVideoBlockingMode()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVideoBlockingMode(const t_U32 cou32DevId,
   *         const tenBlockingMode coenBlockingMode,
   *         const tenDeviceCategory coenDevCat,
   *         const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the display blocking mode.
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenBlockingMode : [IN] Identifies the Blocking Mode.
   * \param  coenDevCat       : [IN] device category
   * \param  corfrcUsrCntxt   : [IN] User context
   * \retval t_Void
   * \sa     vCbSetVideoBlockingMode()
   **************************************************************************/
   t_Void vSetVideoBlockingMode(const t_U32 cou32DevId,
      const tenBlockingMode coenBlockingMode,
      const tenDeviceCategory coenDevCat,
      const trUserContext& corfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vSetOrientationMode()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetOrientationMode(const t_U32 cou32DevId,
   *         const tenOrientationMode coenOrientationMode,
   *         const tenDeviceCategory coenDevCat,
   *         const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  cou32DevId          : [IN] Uniquely identifies the target Device.
   * \param  coenOrientationMode : [IN] Orientation Mode Value.
   * \pram   coenDevCat          : [IN] Identifies the Connection Request.
   * \param  corfrcUsrCntxt      : [IN] User Context Details.
   * \retval t_Void
   * \sa     vCbSetOrientationMode()
   **************************************************************************/
   t_Void vSetOrientationMode(const t_U32 cou32DevId,
      const tenOrientationMode coenOrientationMode,
      const tenDeviceCategory coenDevCat,
      const trUserContext& corfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
   *          t_Bool bSetConfig,
   *          const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the Vehicle configurations.
   * \param  [IN] enVehicleConfig :  Identifies the Vehicle Configuration.
   * \param  [IN] bSetConfig      : Enable/Disable config
   * \param  [IN] corfrcUsrCntxt  : User Context Details.
   **************************************************************************/
   t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideo::vOnVehicleData(const trVehicleData
                                     rfrcVehicleData)
   ***************************************************************************/
   /*!
   * \fn      vOnVehicleData(const trVehicleData rfrcVehicleData)
   * \brief   Method to receive Vehicle data.
   * \param   rfrcVehicleData: [IN] Vehicle data
   * \param   bSolicited: [IN] True if the data update is for changed vehicle data, else False
   * \retval  None
   ***************************************************************************/
   t_Void vOnVehicleData(const trVehicleData rfrcVehicleData, t_Bool bSolicited);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideo::vCbVideoRenderStatus()
   ***************************************************************************/
   /*!
   * \fn      t_Void vCbVideoRenderStatus(const t_Bool cobVideoRenderingStarted,
   *          const tenDeviceCategory coenDevCat) const
   * \brief   To Update the Video rendering status
   * \pram    cobVideoRenderingStarted  : [IN] Uniquely identifies the target Device.
   * \pram    coenDevCat   : [IN] Identifies the Connection Request.
   * \retval  t_Void
   * \sa      vStartVideoRendering()
   **************************************************************************/
   t_Void vCbVideoRenderStatus(const t_Bool cobVideoRenderingStarted,
      const tenDeviceCategory coenDevCat) const;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vCbSetOrientationMode()
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbSetOrientationMode(const t_U32 cou32DevId,
   *         const tenErrorCode coenErrorCode,
   *         const trUserContext& corfrcUsrCntxt,
   *         const tenDeviceCategory coenDevCat)
   * \brief  Method to be triggered after the Orientation Mode is set
   * \param  cou32DevId          : [IN] Uniquely identifies the target Device.
   * \param  coenErrorCode    : [IN] Type of the error.
   * \param  corfrcUsrCntxt   : [IN] User Context Details.
   * \pram   coenDevCat       : [IN] Identifies the Connection Request.
   * \retval t_Void
   * \sa     vSetOrientationMode()
   **************************************************************************/
   t_Void vCbSetOrientationMode(const t_U32 cou32DevId,
      const tenErrorCode coenErrorCode,
      const trUserContext& corfrcUsrCntxt,
      const tenDeviceCategory coenDevCat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vCbSelectDeviceResp()
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbSelectDeviceResp(const t_U32 cou32DevId,
   *         const tenErrorCode coenErrorCode)
   * \brief  Method to Send the Select Device response to mediator
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenErrorCode    : [IN] Type of the error.
   * \retval t_Void
   **************************************************************************/
   t_Void vCbSelectDeviceResp(const t_U32 cou32DevId,
      const tenErrorCode coenErrorCode);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vSetPixelFormat()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetPixelFormat
   * \brief  Method to set pixel format of the video dynamically
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  coenPixelFormat  : [IN] Pixel format to be set
   * \retval t_Void
   **************************************************************************/
   t_Void vSetPixelFormat(const t_U32 cou32DevId,
               const tenDeviceCategory coenDevCat,
               const tenPixelFormat coenPixelFormat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vSetPixelResolution()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution,
   *             const tenDeviceCategory coenDevCat)
   * \brief  Method to set pixel resolution
   * \param  coenPixResolution: [IN] Client display resolution
   * \param  coenDevCat       : [IN] Device category
   * \retval t_Void
   **************************************************************************/
   t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution,
      const tenDeviceCategory coenDevCat);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vCbSessionStatusUpdate()
   ***************************************************************************/
   /*!
   * \fn     t_Void vCbSessionStatusUpdate(const t_U32 cou32DevId,
   *            const tenDeviceCategory coenDevCat,
   *            const tenSessionStatus coenSessionStatus)
   * \brief  method to update the session status to HMI 
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  coenSessionStatus: [IN] Session Status
   * \retval t_Void
   **************************************************************************/
   t_Void vCbSessionStatusUpdate(const t_U32 cou32DevId,
      const tenDeviceCategory coenDevCat,
      const tenSessionStatus coenSessionStatus);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideo::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful/failure
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode,
      tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideo::vSetContAttestFlag()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetContAttestFlag(t_U8 u8ContAttestFlag)
   * \brief   To enable/disable content attestation.Enabling or disableing should be done
   *          before the device is selected for the session
   * \pram    u8ContAttestFlag  : [IN] TRUE - Enable attestation
   *                                  FALSE - disable attestation
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetContAttestFlag(t_U8 u8ContAttestFlag);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideo::vSetClientCapabilities(const tr...)
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetClientCapabilities(
                   const trClientCapabilities& corfrClientCapabilities)
   * \brief   To set the client capabilities
   * \param    corfrClientCapabilities: [IN]Client Capabilities
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetClientCapabilities(
      const trClientCapabilities& corfrClientCapabilities);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideo::vSetScreenAttr()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetScreenAttr(const trVideoConfigData& corfrVideoConfig,
   *                               const tenDeviceCategory coenDevCat)
   * \brief  Interface to set the screen attributes of Head Unit.
   * \param  corfrVideoConfig   : [IN] Screen Setting attributes.
   * \param  coenDevCat          : [IN] Device ID
   * \retval t_Void
   **************************************************************************/
   t_Void vSetScreenAttr(const trVideoConfigData& corfrVideoConfig,
      const tenDeviceCategory coenDevCat);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclVideo::vTriggerVideoFocusCb()
    ***************************************************************************/
    /*!
    * \fn      t_Void vTriggerVideoFocusCb(t_S32 s32Focus, t_S32 s32Reason,
    *             tenDeviceCategory enDevCat)
    * \brief   Method to trigger video focus callback to resource management
    * \param   s32Focus  : [IN] Video Focus Mode
    * \param   s32Reason : [IN] Video Focus Reason
    * \param   enDevCat  : [IN] Device ID
    * \retval  t_Void
    **************************************************************************/
    t_Void vTriggerVideoFocusCb(t_S32 s32Focus, t_S32 s32Reason,
       tenDeviceCategory enDevCat);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclVideo& spi_tclVideo::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideo& operator= (const spi_tclVideo &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclVideo& operator= (const spi_tclVideo &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclVideo::spi_tclVideo(const spi_tclVideo..
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideo(const spi_tclVideo &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclVideo(const spi_tclVideo &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclVideo::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterCallbacks()
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo Video
   * \retval  t_Void 
   **************************************************************************/
   t_Void vRegisterCallbacks();

   /***************************************************************************
   ** FUNCTION:  t_Bool  spi_tclVideo::bValidateClient()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bValidateClient(t_U8 cou8Index)
   * \brief   To validate the client index. check whether it is in the range of
   *          the Video clients Array
   * \param   cou8Index : [IN] Index in the video clients Array
   * \retval  t_Bool 
   **************************************************************************/
   t_Bool bValidateClient(const t_U8 cou8Index);

   //! Member Variable - pointer to the Base class for ML Video and DiPo Video
   spi_tclVideoDevBase* m_poVideoDevBase[NUM_VIDEO_CLIENTS];

   //! Member variable - Video Resp Intf
   spi_tclVideoRespInterface* m_poVideoRespIntf;

   //! Member Variable -  Video Policy Base class*
   spi_tclVideoPolicyBase* m_poVideoPolicyBase;

   //! Member Variable -  ahl_tclBaseOneThreadApp*
   ahl_tclBaseOneThreadApp* m_poMainApp;

   //! Member Variable - Currently selected device category
   tenDeviceCategory m_enSelectedDeviceCategory;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclVideo

#endif //_SPI_TCLVIDEO_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>
