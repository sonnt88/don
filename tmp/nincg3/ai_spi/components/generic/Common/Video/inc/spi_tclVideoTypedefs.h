/*!
 *******************************************************************************
 * \file              spi_tclVideoTypedefs.h
 * \brief             SPI Video related defines
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   SPI Video related defines
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 21.01.2014 | Shiva kumar Gurija           | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLVIDEOTYPEDEFS_H_
#define SPI_TCLVIDEOTYPEDEFS_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <functional>
#include "SPITypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
/*!
* \typedef - For Update Video rendering Status
*/
typedef std::function<t_Void(t_Bool,const tenDeviceCategory)> tVideoRenderStatusCb;

/*!
* \typedef - To update the response to SetOrientationMode asynchronously
*/
typedef std::function<t_Void(const t_U32,const tenErrorCode,
                             const trUserContext&,const tenDeviceCategory)> tSetOrientationModeCb;
/*!
* \typedef - To update the response to Select Device
*/
typedef std::function<t_Void(const t_U32,const tenErrorCode)> tSelectDeviceCb;

/*!
* \typedef - To update the Session Status update
*/
typedef std::function<t_Void(const t_U32,const tenDeviceCategory,const tenSessionStatus)> tNotifySessionStatus;


/*!
* \typedef struct rVideoCallbacks 
* \brief Structure of Video related callbacks
*/
typedef struct rVideoCallbacks
{
   tVideoRenderStatusCb      fpvVideoRenderStatusCb;
   tSetOrientationModeCb     fpvSetOrientationModeCb;
   tSelectDeviceCb           fpvSelectDeviceCb;
   tNotifySessionStatus      fpvNotifySessionStatus;

   rVideoCallbacks():fpvVideoRenderStatusCb(NULL),
      fpvSetOrientationModeCb(NULL),
      fpvSelectDeviceCb(NULL),
      fpvNotifySessionStatus(NULL)
   {
      //add code
   }

} trVideoCallbacks;


#endif //SPI_TCLVIDEOTYPEDEFS_H_