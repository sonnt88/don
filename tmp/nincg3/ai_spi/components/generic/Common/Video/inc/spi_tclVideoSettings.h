/***********************************************************************/
/*!
* \file  spi_tclVideoSettings.h
* \brief Class to get the Video Layer Info
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Video Layer Info
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
13.1.2014   | Vinoop U              | Implemented parser for extracting 
                                       video settings from xml file
14.05.2014  | Shiva Kumar G         | Changes for 8.2" & 10.2" Screens handling
28.05.2014  | Hari Priya E R        | Removed reading of screen size from policy and included
                                      changes to read from EOL handling class
02.04.2015  | Shiva Kumar G         | Extended for Andriod Auto
25.06.2015  | Shiva kaumr G         | Dynamic display configuration

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVIDEOSETTINGS_H_
#define _SPI_TCLVIDEOSETTINGS_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#include "GenericSingleton.h"
#include "XmlDocument.h"
#include "Xmlable.h"
#include "XmlReader.h"

using namespace shl::xml;

/****************************************************************************/
/*!
* \class spi_tclVideoSettings
* \brief Class to get the Video Layer Info
****************************************************************************/

class spi_tclVideoSettings:public GenericSingleton<spi_tclVideoSettings> ,public tclXmlReadable
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoSettings::~spi_tclVideoSettings()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclVideoSettings()
   * \brief   Destructor
   * \sa      spi_tclVideoSettings()
   **************************************************************************/
   ~spi_tclVideoSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideoSettings::vGetScreenOffset(t_U32& u32Scre...
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetScreenOffset(t_U32& u32Screen_X_Offset,
   *          t_U32& u32Screen_Y_Offset)
   * \brief   To Get the Screen Offsets
   * \param   u32Screen_X_Offset : [OUT] X Offset
   * \param   u32Screen_Y_Offset : [OUT] Y Offset
   * \retval  t_Void
   * \sa
   **************************************************************************/
   t_Void vGetScreenOffset(trScreenOffset &rfoScreenOffset) const;


   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclVideoSettings::bIsServerSideScalingRequired()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bIsServerSideScalingRequired()
   * \brief   To Check whether the server side scaling required
   * \retval  t_Bool : false if the scaling is not required, else true
   **************************************************************************/
   t_Bool bIsServerSideScalingRequired();

   /***************************************************************************
   ** FUNCTION:  t_U8 spi_tclVideoSettings::u8EnableTouchInputEvents()()
   ***************************************************************************/
   /*!
   * \fn      short t_U32 u8EnableTouchInputEvents()()
   * \brief   To Check whether the touch input handling is required
   * \retval  t_U8 0x0 -   if the touch input handling is not done using wayland
   *                       0x1 - if the Touch Input Handling is done via wayland
   **************************************************************************/
   t_U8 u8EnableTouchInputEvents() ;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoSettings::vSetScreenSize()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetScreenSize(const trScreenAttributes& corfrScreenAttributes)
   * \brief  Interface to set the screen size of Head Unit.
   * \param  corScreenAttributes : [IN] Screen Setting attributes.
   * \retval t_Void
   **************************************************************************/
   t_Void vSetScreenSize(const trScreenAttributes& corfrScreenAttributes);

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoSettings::vReadVideoSettings()
   ***************************************************************************/
   /*!
   * \fn      t_Void vReadVideoSettings() const;
   * \brief   To Initialize XML Reader
   * \retval  t_Void
   **************************************************************************/
   t_Void vReadVideoSettings();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoSettings::vGetVideoSettingsData()
   ***************************************************************************/
   /*!
   * \fn     t_Void vGetVideoSettingsData()
   * \brief  To print the video settings data
   * \retval  t_Void
   **************************************************************************/
   t_Void vDisplayVideoSettings();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideoSettings::vSetContAttestFlag()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetContAttestFlag(t_U8 u8ContAttestFlag)
   * \brief   To enable/disable content attestation.Enabling or disableing should be done
   *          before the device is selected for the session
   * \pram    u8ContAttestFlag  : [IN] TRUE - Enable attestation
   *                                  FALSE - disable attestation
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetContAttestFlag(t_U8 u8ContAttestFlag);

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclVideoSettings::szGetVideoCodecType()
   ***************************************************************************/
   /*!
   * \fn      t_String szGetVideoCodecType()
   * \brief   To get the video codec type
   *          before the device is selected for the session
   * \retval  t_String
   **************************************************************************/
   t_String szGetVideoCodecType();

   /***************************************************************************
   ** FUNCTION:  t_U8 spi_tclVideoSettings::u8GetMaxUnackedFrames()
   ***************************************************************************/
   /*!
   * \fn      t_U8 u8GetMaxUnackedFrames()
   * \brief   To get the max number of frames that can be recieved with out ack
   * \retval  t_U8 
   **************************************************************************/
   t_U8 u8GetMaxUnackedFrames();

   /***************************************************************************
   ** FUNCTION:  t_U16 spi_tclVideoSettings::u8GetMaxUnackedFrames()
   ***************************************************************************/
   /*!
   * \fn      t_Void vGetPixelDenisty()
   * \brief   To get the Pixel density
   * \retval  t_U16 
   **************************************************************************/
   t_U16 u16GetPixelDenisty(tenDeviceCategory enDevCat);

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclVideoSettings::bGetAutoStartProjection()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bGetAutoStartProjection()
   * \brief   To get whether the auto projection start is enabled or not
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bGetAutoStartProjection();

   /***************************************************************************
   ** FUNCTION:  t_U8 spi_tclVideoSettings::u8GetFramesPerSec()
   ***************************************************************************/
   /*!
   * \fn      t_U8 u8GetFramesPerSec()
   * \brief   To get the frame per pixel rate
   * \retval  t_U8 
   **************************************************************************/
   t_U8 u8GetFramesPerSec();

   /***************************************************************************
   ** FUNCTION:  t_U8 spi_tclVideoSettings::u8GetContAttestSignFlag()
   ***************************************************************************/
   /*!
   * \fn      t_U8 u8GetContAttestSignFlag()
   * \brief   To get the Content attestation info signed flag
   * \retval  t_U8 
   **************************************************************************/
   t_U8 u8GetContAttestSignFlag();

   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<spi_tclVideoSettings>;

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoSettings::spi_tclVideoSettings()
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoSettings()
   * \brief   Default Constructor
   * \sa      ~spi_tclVideoSettings()
   **************************************************************************/
   spi_tclVideoSettings();

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoSettings& spi_tclVideoSettings::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoSettings& operator= (const spi_tclVideoSettings &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclVideoSettings& operator= (const spi_tclVideoSettings &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoSettings::spi_tclVideoSettings(const ..
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoSettings(const spi_tclVideoSettings &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclVideoSettings(const spi_tclVideoSettings &corfrSrc);

   /*************************************************************************
   ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
   *************************************************************************/
   /*!
   * \fn     virtual t_bool bXmlReadNode(xmlNode *poNode)
   * \brief  virtual function to read data from a xml node
   * \param  poNode : [IN] pointer to xml node
   * \retval bool : true if success, false otherwise.
   *************************************************************************/
   virtual t_Bool bXmlReadNode(xmlNodePtr poNode);


   t_Bool m_bServerSideScalingReq;
   t_U8 m_u8TouchInputEvents;

   trScreenOffset m_rScreenOffset;

   t_String m_szVideoCodecType;
   t_U8 m_u8MaxUnAckedFrames;
   t_U8 m_u8Fps;
   trDisplayDpi m_rDisplayDpi;
   t_Bool m_bAutoStartProjection;
   t_U8 m_u8ContentAttestSignFlag;


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclVideoSettings

#endif //_SPI_TCLVIDEOSETTINGS_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>


