/***********************************************************************/
/*!
* \file  spi_tclVideoDevBase.h
* \brief Base class for MirrorLink Vodeo andDiPo Video classes
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for MirrorLink Vodeo andDiPo Video classes
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
21.01.2014  | Shiva Kumar Gurija    | Updated with Video Response Interface
03.04.2013  | Shiva Kumar Gurija    | Device status messages
06.11.2014  |  Hari Priya E R       | Added changes to set Client key capabilities
API's in CmdInterface

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVIDEODEVBASE_H_
#define _SPI_TCLVIDEODEVBASE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class spi_tclVideoDevBase
* \brief 
****************************************************************************/
class spi_tclVideoDevBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoDevBase::spi_tclVideoDevBase()
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoDevBase()
   * \brief   Default Constructor
   * \sa      ~spi_tclVideoDevBase()
   **************************************************************************/
   spi_tclVideoDevBase()
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoDevBase::~spi_tclVideoDevBase()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclVideoDevBase()
   * \brief   Destructor
   * \sa      spi_tclVideoDevBase()
   **************************************************************************/
   virtual ~spi_tclVideoDevBase()
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclVideoDevBase::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the Video related classes
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   virtual t_Bool bInitialize()=0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideoDevBase::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief   To Uninitialize all the Video related classes
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   virtual t_Void vUninitialize()=0;

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclVideoDevBase::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo Video
   * \param   corfrVideoCallbacks : [IN] Video callabcks structure
   * \retval  t_Void 
   **************************************************************************/
   virtual t_Void vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideoDevBase::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSelectDevice(const t_U32 cou32DevId,
   *          const tenDeviceConnectionReq coenConnReq)
   * \brief   To Initialize/UnInitialize Video setup for thr currently selected device
   * \pram    cou32DevId : [IN] Unique Device Id
   * \param   coenConnReq : [IN] connected/disconnected
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq) = 0;

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclVideoDevBase::bLaunchVideo()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bLaunchVideo(const t_U32 cou32DevId,
   *                 const t_U32 cou32AppId,
   *                 const tenEnabledInfo coenSelection)
   * \brief   To Launch the Video for the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    cou32AppId  : [IN] Application Id
   * \pram    coenSelection  : [IN] Enable/disable the video
   * \retval  t_Bool
   **************************************************************************/
   virtual t_Bool bLaunchVideo(const t_U32 cou32DevId,
      const t_U32 cou32AppId,
      const tenEnabledInfo coenSelection=e8USAGE_ENABLED) = 0;

   /***************************************************************************
   ** FUNCTION:  t_U32  spi_tclVideoDevBase::vStartVideoRendering()
   ***************************************************************************/
   /*!
   * \fn      t_Void vStartVideoRendering(t_Bool bStartVideoRendering)
   * \brief   Method send request to ML/DiPo Video eithr to start or stop
   *          Video Rendering
   * \pram    bStartVideoRendering : [IN] True - Start Video rendering
   *                                      False - Stop Video rendering
   * \retval  t_Void 
   **************************************************************************/
   virtual t_Void vStartVideoRendering(t_Bool bStartVideoRendering) = 0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoDevBase::vGetVideoSettings()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vGetVideoSettings(const t_U32 cou32DevId,
   *                                  trVideoAttributes& rfrVideoAttributes
   * \brief  To get the current Video Settings.
   * \param  cou32DevId         : [IN] Uniquely identifies the target Device.
   * \param  rfrVideoAttributes : [OUT]includes screen size & orientation.
   * \retval t_Void
   * \sa
   **************************************************************************/
   virtual t_Void vGetVideoSettings(const t_U32 cou32DevId,
      trVideoAttributes& rfrVideoAttributes)=0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoDevBase::vSetScreenSize()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vSetServerAspectRatio(const tenScreenAspectRatio& corfenScrAspRatio)
   * \brief  Interface to set the screen aspect ratio of Head Unit.
   * \param  corfenScrAspRatio : [IN] screen aspect ratio
   * \retval t_Void
   * \sa
   **************************************************************************/
   virtual t_Void vSetServerAspectRatio(const tenScreenAspectRatio& corfenScrAspRatio){}

   /***************************************************************************
   ** FUNCTION: tenErrorCode spi_tclVideoDevBase::enSetVideoBlockingMode()
   ***************************************************************************/
   /*!
   * \fn     virtual tenErrorCode enSetVideoBlockingMode(const t_U32 cou32DevId,
   *         const tenBlockingMode coenBlockingMode)
   * \brief  Interface to set the display blocking mode.
   * \param  cou32DevId             : [IN] Uniquely identifies the target Device.
   * \param  coenBlockingMode       : [IN] Identifies the Blocking Mode.
   * \retval tenErrorCode
   **************************************************************************/
   virtual tenErrorCode enSetVideoBlockingMode(const t_U32 cou32DevId,
      const tenBlockingMode coenBlockingMode){}

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoDevBase::vSetOrientationMode()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vSetOrientationMode(const t_U32 cou32DevId,
   *                                    const tenOrientationMode coenOrientationMode,
   *                                    const trUserContext& corfrUsrCntxt)= 0
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  cou32DevId          : [IN] Uniquely identifies the target Device.
   * \param  coenOrientationMode : [IN] Orientation Mode Value.
   * \param  corfrUsrCntxt       : [IN] User Context
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vSetOrientationMode(const t_U32 cou32DevId,
      const tenOrientationMode coenOrientationMode,
      const trUserContext& corfrUsrCntxt)= 0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoDevBase::vSetVehicleConfig()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
   *          t_Bool bSetConfig,const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the Vehicle configurations.
   * \param  [IN] enVehicleConfig :  Identifies the Vehicle Configuration.
   * \param  [IN] bSetConfig      : Enable/Disable config
   **************************************************************************/
   virtual t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      t_Bool bSetConfig)
   {
      SPI_INTENTIONALLY_UNUSED(enVehicleConfig);
      SPI_INTENTIONALLY_UNUSED(bSetConfig);
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoDevBase::vSetPixelFormat()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetPixelFormat
   * \brief  Method to set pixel format of the video dynamically
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  coenPixelFormat  : [IN] Pixel format to be set
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vSetPixelFormat(const t_U32 cou32DevId,
               const tenDeviceCategory coenDevCat,
               const tenPixelFormat coenPixelFormat){}

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoDevBase::vSetPixelResolution()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution)
   * \brief  Method to set pixel resolution
   * \param  coenPixResolution: [IN] Client display resolution
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vSetPixelResolution(const tenPixelResolution coenPixResolution)
   {
      SPI_INTENTIONALLY_UNUSED(coenPixResolution);
   }

      /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideoDevBase::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful/failed
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode)
   {
      SPI_INTENTIONALLY_UNUSED(cou32DevId);
      SPI_INTENTIONALLY_UNUSED(coenConnReq);
      SPI_INTENTIONALLY_UNUSED(coenRespCode);
   }

    /***************************************************************************
   ** FUNCTION:  t_Void spi_tclVideoDevBase::vSetClientCapabilities(const trClient...)
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetClientCapabilities(
                   const trClientCapabilities& corfrClientCapabilities)
   * \brief   To set the client capabilities
   * \param    corfrClientCapabilities: [IN]Client Capabilities
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities)
   {
      SPI_INTENTIONALLY_UNUSED(corfrClientCapabilities);
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoDevBase::vSetScreenAttr()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vSetScreenAttr(const trVideoConfigData& corfrVideoConfig)
   * \brief  Interface to set the screen attributes of Head Unit.
   * \param  corfrVideoConfig   : [IN] Screen Setting attributes.
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vSetScreenAttr(const trVideoConfigData& corfrVideoConfig)
   {
      SPI_INTENTIONALLY_UNUSED(corfrVideoConfig);
   }

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclVideoDevBase::vTriggerVideoFocusCb()
    ***************************************************************************/
    /*!
    * \fn      virtual t_Void vTriggerVideoFocusCb(t_S32 s32Focus, t_S32 s32Reason)
    * \brief   Method to trigger video focus callback to resource management
    * \param   s32Focus  : [IN] Video Focus Mode
    * \param   s32Reason : [IN] Video Focus Reason
    * \retval  t_Void
    **************************************************************************/
    virtual t_Void vTriggerVideoFocusCb(t_S32 s32Focus, t_S32 s32Reason)
    {
      SPI_INTENTIONALLY_UNUSED(s32Focus);
      SPI_INTENTIONALLY_UNUSED(s32Reason);
    }

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclVideoDevBase

#endif //_SPI_TCLVIDEODEVBASE_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>

