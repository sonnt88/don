/***********************************************************************/
/*!
* \file  spi_tclVideo.cpp
* \brief Main Video class that provides interface to delegate 
*        the execution of command and handle response
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
06.01.2014  | Hari Priya E R        |Changes for Layer Sync handling
21.01.2014  | Shiva Kumar Gurija    | Updated with Video Response Interface
03.04.2013  | Shiva Kumar Gurija    | Device status messages
                                      API's in CmdInterface
06.04.2014  | Ramya Murthy          | Initialisation sequence implementation
28.05.2014  | Hari Priya E R        | Removed function that sets screen variant
16.07.2014  | Shiva Kumar Gurija    | Implemented SessionStatusInfo update
31.07.2014  |  Ramya Murthy         | SPI feature configuration via LoadSettings()
06.11.2014  |  Hari Priya E R       | Added changes to set Client key capabilities
03.07.2015  |   Shiva Kumar G       | Dynamic display configuration

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <map>
#include <gst/gst.h>
#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"
#include "Trace.h"
#include "spi_tclVideoTypedefs.h"
#include "spi_tclVideoRespInterface.h"
#include "spi_tclVideoPolicyBase.h"
#include "spi_tclVideoSettings.h"
#include "spi_tclVideoPolicy.h"
#include "spi_tclVideoIntf.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
#include "spi_tclMLVideo.h"
#endif
#include "spi_tclDiPoVideo.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
#include "spi_tclAAPVideo.h"
#endif
#include "spi_tclVideo.h"
#include "spi_tclMediator.h"


#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VIDEO
#include "trcGenProj/Header/spi_tclVideo.cpp.trc.h"
#endif
#endif


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclVideo::spi_tclVideo(ahl_tclBaseOneThreadApp* poMainApp)
***************************************************************************/
spi_tclVideo::spi_tclVideo(ahl_tclBaseOneThreadApp* poMainApp,
                           spi_tclVideoRespInterface* poVideoRespIntf):
   m_poVideoRespIntf(poVideoRespIntf),
   m_poVideoPolicyBase(NULL),
   m_poMainApp(poMainApp),
   m_enSelectedDeviceCategory(e8DEV_TYPE_UNKNOWN)
{
   ETG_TRACE_USR1(("spi_tclVideo() entered "));

   SPI_NORMAL_ASSERT(NULL == m_poVideoRespIntf);

   for (t_U8 u8Index = 0; u8Index < NUM_VIDEO_CLIENTS; u8Index++)
   {
      m_poVideoDevBase[u8Index] = NULL;
   }//for(t_U8 u8Index=0; u8In
}

/***************************************************************************
** FUNCTION:  spi_tclVideo::~spi_tclVideo()
***************************************************************************/
spi_tclVideo::~spi_tclVideo()
{
   //Destructor
   ETG_TRACE_USR1(("~spi_tclVideo() entered "));

   m_poVideoRespIntf          = NULL;
   m_poVideoPolicyBase        = NULL;
   m_poMainApp                = NULL;
   m_enSelectedDeviceCategory = e8DEV_TYPE_UNKNOWN;

   for (t_U8 u8Index = 0; u8Index < NUM_VIDEO_CLIENTS; u8Index++)
   {
      m_poVideoDevBase[u8Index] = NULL;
   }//for(t_U8 u8Index=0; u8In
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclVideo::bInitialize()
***************************************************************************/
t_Bool spi_tclVideo::bInitialize()
{
   t_Bool bInit = false;

   //! Initialize gstreamer
   gst_init(NULL, NULL);
   m_poVideoDevBase[e8DEV_TYPE_DIPO] = new spi_tclDiPoVideo();
   SPI_NORMAL_ASSERT(NULL == m_poVideoDevBase[e8DEV_TYPE_DIPO]);
   #ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
   m_poVideoDevBase[e8DEV_TYPE_MIRRORLINK] = new spi_tclMLVideo();
   SPI_NORMAL_ASSERT(NULL == m_poVideoDevBase[e8DEV_TYPE_MIRRORLINK]);
#endif
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
m_poVideoDevBase[e8DEV_TYPE_ANDROIDAUTO] = new spi_tclAAPVideo();
 SPI_NORMAL_ASSERT(NULL == m_poVideoDevBase[e8DEV_TYPE_ANDROIDAUTO]);
#endif
   m_poVideoPolicyBase = new spi_tclVideoPolicy(m_poMainApp,this);
   SPI_NORMAL_ASSERT(NULL == m_poVideoPolicyBase);

    //! Set the bInit to false, if all the SPI technologies cannot be initialized
    //! else return true if one of the SPI technologies can be initialized

   for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)
   {
      if(NULL != m_poVideoDevBase[u8Index])
      {
         bInit =  m_poVideoDevBase[u8Index]->bInitialize() || bInit;
      }//if((NULL != m_poVideoDevBase[
   } //for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)

   bInit = (NULL != m_poVideoPolicyBase) && bInit;

   //Register With ML Video & DiPo Video for asynchronous responses
   vRegisterCallbacks();

   //Read the Project specific settings from XML and store it internally
   spi_tclVideoSettings* poVideoSettings = spi_tclVideoSettings::getInstance();
   if(NULL != poVideoSettings)
   {
      poVideoSettings->vReadVideoSettings();
   }//if(NULL != poVideoSettings)
   ETG_TRACE_USR1(("spi_tclVideo:bInitialize()  bInit = %d", ETG_ENUM(BOOL, bInit)));
   return bInit;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclVideo::bUnInitialize()
***************************************************************************/
t_Bool spi_tclVideo::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclVideo:vUninitialize() "));

   //Destroy objects
   RELEASE_MEM(m_poVideoPolicyBase);

   for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)
   {
      if (NULL != m_poVideoDevBase[u8Index])
      {
         m_poVideoDevBase[u8Index]->vUninitialize();
      }//if((NULL != m_poVideoDevBase[
      RELEASE_MEM(m_poVideoDevBase[u8Index]);
   }

   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideo::vLoadSettings(const trSpiFeatureSupport&...)
***************************************************************************/
t_Void spi_tclVideo::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1(("spi_tclVideo:vLoadSettings() "));
   SPI_INTENTIONALLY_UNUSED(rfcrSpiFeatureSupp);
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideo::vSaveSettings()
***************************************************************************/
t_Void spi_tclVideo::vSaveSettings()
{
   ETG_TRACE_USR1(("spi_tclVideo:vSaveSettings() "));
   //Add code
}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclVideo::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclVideo::vRegisterCallbacks()
{
   /*lint -esym(40,fpvVideoRenderStatusCb) fpvVideoRenderStatusCb Undeclared identifier */
   /*lint -esym(40,fpvVideoRenderStatusCb) fpvVideoRenderStatusCb Undeclared identifier */
   /*lint -esym(40,fpvSetOrientationModeCb) fpvSetOrientationModeCb Undeclared identifier */
   /*lint -esym(40,fpvSelectDeviceCb) fpvSelectDeviceCb Undeclared identifier */
   /*lint -esym(40,fpvNotifySessionStatus)fpvNotifySessionStatus Undeclared identifier */
   /*lint -esym(40,_1)_1 Undeclared identifier */
   /*lint -esym(40,_2)_2 Undeclared identifier */
   /*lint -esym(40,_3)_3 Undeclared identifier */

   ETG_TRACE_USR1(("spi_tclVideo:vRegisterCallbacks() "));

   trVideoCallbacks rVideoCbs;

   rVideoCbs.fpvVideoRenderStatusCb = std::bind(
      &spi_tclVideo::vCbVideoRenderStatus, this, 
      SPI_FUNC_PLACEHOLDERS_2);

   rVideoCbs.fpvSetOrientationModeCb = std::bind(
      &spi_tclVideo::vCbSetOrientationMode, this, 
      SPI_FUNC_PLACEHOLDERS_4);

   rVideoCbs.fpvSelectDeviceCb = std::bind(
   &spi_tclVideo::vCbSelectDeviceResp,this,
      SPI_FUNC_PLACEHOLDERS_2);

   rVideoCbs.fpvNotifySessionStatus = std::bind(
      &spi_tclVideo::vCbSessionStatusUpdate,this,
      SPI_FUNC_PLACEHOLDERS_3);

   for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)
   {
      if(NULL != m_poVideoDevBase[u8Index])
      {
         m_poVideoDevBase[u8Index]->vRegisterCallbacks(rVideoCbs);
      }//if((NULL != m_poVideoDevBase[
   }//for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)

}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclVideo::bValidateClient()
***************************************************************************/
inline t_Bool spi_tclVideo::bValidateClient(const t_U8 cou8Index)
{
   //Assert if the Index is greater than the array size
   SPI_NORMAL_ASSERT( cou8Index == NUM_VIDEO_CLIENTS);

   t_Bool bRet = ( cou8Index < NUM_VIDEO_CLIENTS) && 
      (NULL != m_poVideoDevBase[cou8Index]);

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideo::vSelectDevice()
***************************************************************************/
t_Void spi_tclVideo::vSelectDevice(const t_U32 cou32DevId, 
                                   const tenDeviceConnectionReq coenConnReq,
                                   const tenDeviceCategory coenDevCat,
                                   const trUserContext& corfrcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(corfrcUsrCntxt);

   //We need the current selected device info to send render video request to
   //Either ML or DiPo. This request comes from SPI_VideoPolicy
   //It's required to enable/disable the layer, when HMI requests.

   //Sequence of requests like ML device (X) connected, DiPo device (Y) Connected & ML device(X) 
   //disconnected. must be avoided in SelectDevice class. First ML disconnection info should be 
   //sent and then DiPo connection should be sent
   m_enSelectedDeviceCategory = coenDevCat;

   t_U8 u8Index = static_cast<t_U8>(coenDevCat);
   ETG_TRACE_USR1(("spi_tclVideo:vSelectDevice:Device ID- 0x%x  Device Category-%d "
      ,cou32DevId,ETG_ENUM(DEVICE_CATEGORY,u8Index)));

   if(true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vSelectDevice(cou32DevId,coenConnReq);
   } //if(true == bValidateClient(u8Index))
   else
   {
      vCbSelectDeviceResp(cou32DevId,e8INVALID_DEV_HANDLE);
   }

}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclVideo::bLaunchVideo()
***************************************************************************/
t_Bool spi_tclVideo::bLaunchVideo(const t_U32 cou32DevId,
                                  const t_U32 cou32AppId,
                                  const tenDeviceCategory coenDevCat,
                                  const tenEnabledInfo coenSelection) 
{
   SPI_INTENTIONALLY_UNUSED(coenSelection);

   t_Bool bRet = false;
   t_U8 u8Index = static_cast<t_U8>(coenDevCat);

   ETG_TRACE_USR1(("spi_tclVideo:bLaunchVideo::Device ID-0x%x  App ID-0x%x  Device Category-%d ",
      cou32DevId,cou32AppId,ETG_ENUM(DEVICE_CATEGORY,u8Index)));

   if(true == bValidateClient(u8Index))
   {
      bRet = m_poVideoDevBase[u8Index]->bLaunchVideo(cou32DevId,cou32AppId);
   } //if(true == bValidateClient(u8Index))

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclVideo::vStartVideoRendering()
***************************************************************************/
t_Void spi_tclVideo::vStartVideoRendering(const t_Bool cobStartVideoRendering)
{
   ETG_TRACE_USR1(("spi_tclVideo:vStartVideoRendering: Enable SPI Screen -%d ",
      ETG_ENUM(BOOL,cobStartVideoRendering)));

   t_U8 u8Index = static_cast<t_U8>(m_enSelectedDeviceCategory);

   if(true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vStartVideoRendering(cobStartVideoRendering);
   } //if(true == bValidateClient(u8Index))
   else
   {
      //Invalid device - If the request is to stop writing frame buffers,
      //send the response as true. Else send the error response
      t_Bool bResult = (false==cobStartVideoRendering);
      vCbVideoRenderStatus(bResult,m_enSelectedDeviceCategory);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vGetVideoSettings()
***************************************************************************/
t_Void spi_tclVideo::vGetVideoSettings(const t_U32 cou32DevId,
                                       trVideoAttributes& rfrVideoAttr,
                                       const tenDeviceCategory coenDevCat) 
{
   t_U8 u8Index = static_cast<t_U8>(coenDevCat);
   ETG_TRACE_USR1(("spi_tclVideo:vGetVideoSettings:Device ID-0x%x Device Category-%d "
      ,cou32DevId,ETG_ENUM(DEVICE_CATEGORY,u8Index)));

   if(true == bValidateClient(u8Index))
   {
      //get the settings
      m_poVideoDevBase[u8Index]->vGetVideoSettings(cou32DevId,rfrVideoAttr);
   } //if(true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vSetScreenSize()
***************************************************************************/
t_Void spi_tclVideo::vSetScreenSize(const trScreenAttributes& corfrScreenAttributes,
                                    const tenDeviceCategory coenDevCat,
                                    const trUserContext& corfrcUsrCntxt) 
{
   ETG_TRACE_USR1(("spi_tclVideo:vSetScreenSize"));
   SPI_INTENTIONALLY_UNUSED(corfrcUsrCntxt);
   //Set the screen size for both the ML & DIPO
   //Screen size will not change always.
   //It can be used to set the screen width&Height on start up
   t_U8 u8Index = static_cast<t_U8>(coenDevCat);

   spi_tclVideoSettings* poVideoSettings = spi_tclVideoSettings::getInstance();
   if(NULL != poVideoSettings)
   { 
      poVideoSettings -> vSetScreenSize(corfrScreenAttributes);
   } //if(NULL != poVideoSettings)

   if(true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vSetServerAspectRatio(corfrScreenAttributes.enScreenAspectRatio);
   } //if(true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vSetVideoBlockingMode()
***************************************************************************/
t_Void spi_tclVideo::vSetVideoBlockingMode(const t_U32 cou32DevId,
                                           const tenBlockingMode coenBlockingMode,
                                           const tenDeviceCategory coenDevCat,
                                           const trUserContext& corfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclVideo:vSetVideoBlockingMode:Device ID-0x%x  BlockingMode-%d ",
      cou32DevId,ETG_ENUM(BLOCKING_MODE,coenBlockingMode)));

   tenErrorCode enErrorcode = e8INVALID_DEV_HANDLE;
   t_U8 u8Index = static_cast<t_U8>(coenDevCat);
   
   if(true == bValidateClient(u8Index))
   {
      enErrorcode = m_poVideoDevBase[u8Index]->enSetVideoBlockingMode(cou32DevId,coenBlockingMode);
   } //if(true == bValidateClient(u8Index))

   //Post the Message to RespIntf
   if(NULL != m_poVideoRespIntf)
   {
      tenResponseCode enResponseCode = (enErrorcode == e8NO_ERRORS)?e8SUCCESS:e8FAILURE;
      m_poVideoRespIntf->vPostSetVideoBlockingModeResult(enResponseCode,enErrorcode,corfrcUsrCntxt);
   }//if(NULL != m_poVideoRespIntf)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vSetOrientationMode()
***************************************************************************/
t_Void spi_tclVideo::vSetOrientationMode(const t_U32 cou32DevId,
                                         const tenOrientationMode coenOrientationMode,
                                         const tenDeviceCategory coenDevCat,
                                         const trUserContext& corfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclVideo:vSetOrientationMode:Device ID-0x%x  Orientation Mode-%d ",
      cou32DevId,ETG_ENUM(ORIENTATION_MODE,coenOrientationMode)));

   t_U8 u8Index = static_cast<t_U8>(coenDevCat);
   if(true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vSetOrientationMode(cou32DevId,coenOrientationMode,corfrcUsrCntxt);
   } //if(true == bValidateClient(u8Index))
   else
   {
      //Report invalid device handle error
      vCbSetOrientationMode(cou32DevId,e8INVALID_DEV_HANDLE,corfrcUsrCntxt,e8DEV_TYPE_UNKNOWN);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vSetVehicleConfig()
***************************************************************************/
t_Void spi_tclVideo::vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
                                       t_Bool bSetConfig)
{
   ETG_TRACE_USR1(("spi_tclVideo:vSetVehicleConfig()-%d bSetConfig-%d \n",
      enVehicleConfig,ETG_ENUM(BOOL,bSetConfig)));

   for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)
   {
      if(NULL != m_poVideoDevBase[u8Index])
      {
         m_poVideoDevBase[u8Index]->vSetVehicleConfig(enVehicleConfig,bSetConfig);
      }//if(NULL != m_poVideoDevBase[u8Index])
   } //if(( u8Index < NUM_VIDEO_CLIENTS)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideo::vOnVehicleData(const trVehicleData rfrcVehicleData)
***************************************************************************/
t_Void spi_tclVideo::vOnVehicleData(const trVehicleData rfrcVehicleData, t_Bool bSolicited)
{
   ETG_TRACE_USR2(("[DESC]::spi_tclVideo::vOnVehicleData entered with ParkBrakeActive = %d, "
            "VehMovState = %d, Solicited value = %d ", rfrcVehicleData.bParkBrakeActive,
            ETG_ENUM(VEH_MOV_STATE, rfrcVehicleData.enVehMovState), ETG_ENUM(BOOL,bSolicited)));

   if(false == bSolicited)
   {
      //! Set driving status - Else if either park brake is true or vehicle movement state is parked, then it is considered as park mode.
      //! if received status for park brake is not true and vehicle movement state is not parked,
      //! then it is considered as Drive mode.
      tenVehicleConfiguration enVehicleConfig =
         ((rfrcVehicleData.bParkBrakeActive) || (e8VEHICLE_MOVEMENT_STATE_PARKED == rfrcVehicleData.enVehMovState)) ?
         (e8PARK_MODE) : (e8DRIVE_MODE);

      //! Forward the data to corresponding Video handler
      for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)
      {
         if(NULL != m_poVideoDevBase[u8Index])
         {
            m_poVideoDevBase[u8Index]->vSetVehicleConfig(enVehicleConfig,true);
         }//if(NULL != m_poVideoDevBase[u8Index])
      } //if(( u8Index < NUM_VIDEO_CLIENTS)
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideo::vCbVideoRenderStatus()
***************************************************************************/
t_Void spi_tclVideo::vCbVideoRenderStatus(const t_Bool cobVideoRenderingStarted,
                                          const tenDeviceCategory coenDevCat) const
{
   ETG_TRACE_USR1(("spi_tclVideo:vCbUpdateVideoRenderStatus: Video rendering started - %d ",
      ETG_ENUM(BOOL,cobVideoRenderingStarted)));

   SPI_INTENTIONALLY_UNUSED(coenDevCat);

   if(NULL != m_poVideoPolicyBase )
   {
      m_poVideoPolicyBase ->vUpdateVideoRenderingStatus(cobVideoRenderingStarted);
   } //if(NULL != m_poVideoPolicyBase )

}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vCbSetOrientationMode()
***************************************************************************/
t_Void spi_tclVideo::vCbSetOrientationMode(const t_U32 cou32DevId,
                                           const tenErrorCode coenErrorCode,
                                           const trUserContext& corfrcUsrCntxt,
                                           const tenDeviceCategory coenDevCat)
{
   ETG_TRACE_USR1(("spi_tclVideo:vCbSetOrientationMode ErrorCode-%d ",
      ETG_ENUM(ERROR_CODE,coenErrorCode)));

   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   SPI_INTENTIONALLY_UNUSED(coenDevCat);

   //Post the Message to RespIntf
   if(NULL != m_poVideoRespIntf)
   {
      tenResponseCode enResponseCode = (coenErrorCode == e8NO_ERRORS)?e8SUCCESS:e8FAILURE;
      m_poVideoRespIntf->vPostSetOrientationModeResult(enResponseCode,coenErrorCode,corfrcUsrCntxt);
   }//if(NULL != m_poVideoRespIntf)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vCbSelectDeviceResp()
***************************************************************************/
t_Void spi_tclVideo::vCbSelectDeviceResp(const t_U32 cou32DevId,
                                         const tenErrorCode coenErrorCode)
{
   ETG_TRACE_USR1(("spi_tclVideo:vCbSelectDeviceResp:Device ID -0x%x ErrorCode-%d ",
      cou32DevId,ETG_ENUM(ERROR_CODE,coenErrorCode)));

   spi_tclMediator *poMediator = spi_tclMediator::getInstance();
   if(NULL != poMediator)
   {
      poMediator->vPostSelectDeviceRes(e32COMPID_VIDEO, coenErrorCode);
   }//if(NULL != poMediator)
}


/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vSetPixelFormat()
***************************************************************************/
t_Void spi_tclVideo::vSetPixelFormat(const t_U32 cou32DevId,
         const tenDeviceCategory coenDevCat,
         const tenPixelFormat coenPixelFormat)
{
   ETG_TRACE_USR1(("spi_tclVideo::vSetPixelFormat:Device ID-0x%x Device Category = %d PixelFormat-%d ",
      cou32DevId, ETG_ENUM(DEVICE_CATEGORY , coenDevCat), ETG_ENUM(PIXEL_FORMAT, coenPixelFormat)));
   t_U8 u8Index = static_cast<t_U8>(coenDevCat);

   if (true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vSetPixelFormat(cou32DevId, coenDevCat,
               coenPixelFormat);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vSetPixelResolution()
***************************************************************************/
t_Void spi_tclVideo::vSetPixelResolution(const tenPixelResolution coenPixResolution,
                                         const tenDeviceCategory coenDevCat)
{
   ETG_TRACE_USR1(("spi_tclVideo::vSetPixelResolution: Pixel Resolution = %d ",
      ETG_ENUM(PIXEL_RESOLTION,coenPixResolution)));

   t_U8 u8Index = static_cast<t_U8>(coenDevCat);
   if (true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vSetPixelResolution(coenPixResolution);
   }//if (true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vCbSessionStatusUpdate()
***************************************************************************/
t_Void spi_tclVideo::vCbSessionStatusUpdate(const t_U32 cou32DevId,
                                            const tenDeviceCategory coenDevCat,
                                            const tenSessionStatus coenSessionStatus)
{
   ETG_TRACE_USR1(("spi_tclVideo::vCbSessionStatusUpdate:Device ID-0x%x Device Category-%d Session Status-%d",
      cou32DevId,ETG_ENUM(DEVICE_CATEGORY,coenDevCat),ETG_ENUM(SESSION_STATUS,coenSessionStatus)));

   //if there is the change in the session status, update HMI
   if(NULL != m_poVideoRespIntf) 
   {
      m_poVideoRespIntf->vSendSessionStatusInfo(cou32DevId,coenDevCat,coenSessionStatus);
   }//if(NULL != m_poVideoRespIntf)

   spi_tclMediator *poMediator = spi_tclMediator::getInstance();
   if( (NULL != poMediator) && (e8_SESSION_ERROR == coenSessionStatus) )
   {
      //Error during the session, terminate the session gracefully.
      poMediator->vPostAutoDeviceSelection(cou32DevId,e8DEVCONNREQ_DESELECT);
   }//if(e8_SESSION_ERROR == coenSessionStatus)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideo::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclVideo::vOnSelectDeviceResult(const t_U32 cou32DevId,
                                           const tenDeviceConnectionReq coenConnReq,
                                           const tenResponseCode coenRespCode,
                                           tenDeviceCategory enDevCat)
{
   t_U8 u8Index = static_cast<t_U8>(enDevCat);

   ETG_TRACE_USR1(("spi_tclVideo::vOnSelectDeviceResult:Device ID-0x%x,Selection Type-%d RespCode-%d Index-%d ",
      cou32DevId,ETG_ENUM(CONNECTION_REQ,coenConnReq),coenRespCode,u8Index));

   if (true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vOnSelectDeviceResult(cou32DevId,
         coenConnReq,coenRespCode);
   }//if (true == bValidateClient(u8Index))

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideoSettings::vSetContAttestFlag()
***************************************************************************/
t_Void spi_tclVideo::vSetContAttestFlag(t_U8 u8ContAttestFlag)
{

   ETG_TRACE_USR1(("spi_tclVideo::vSetContAttestFlag:ContAttestFlag-%d",
      u8ContAttestFlag));

   spi_tclVideoSettings* poVideoSettings = spi_tclVideoSettings::getInstance();
   if(NULL != poVideoSettings)
   {
      poVideoSettings->vSetContAttestFlag(u8ContAttestFlag);
   }//if(NULL != poVideoSettings)

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideo::vSetClientCapabilities(const trClient...)
***************************************************************************/
t_Void spi_tclVideo::vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities)
{
   ETG_TRACE_USR1(("spi_tclVideo::vSetClientCapabilities entered "));

   for(t_U8 u8Index=0;u8Index < NUM_VIDEO_CLIENTS; u8Index++)
   {
      if(NULL != m_poVideoDevBase[u8Index])
      {
         m_poVideoDevBase[u8Index]->vSetClientCapabilities(corfrClientCapabilities);
      }//if(NULL != m_poVideoDevBase[u8Index])
   } //if(( u8Index < NUM_VIDEO_CLIENTS)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideo::vSetScreenAttr()
***************************************************************************/
t_Void spi_tclVideo::vSetScreenAttr(const trVideoConfigData& corfrVideoConfig,
                                    const tenDeviceCategory coenDevCat)
{
   ETG_TRACE_USR1(("spi_tclVideo:vSetScreenAttr"));

   t_U8 u8Index = static_cast<t_U8>(coenDevCat);
   if(true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vSetScreenAttr(corfrVideoConfig);
   } //if(true == bValidateClient(u8Index))
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclVideo::vTriggerVideoFocusCb()
***************************************************************************/
t_Void spi_tclVideo::vTriggerVideoFocusCb(t_S32 s32Focus, t_S32 s32Reason,
                                          tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1(("spi_tclVideo:vTriggerVideoFocusCb"));
   t_U8 u8Index = static_cast<t_U8>(enDevCat);
   if(true == bValidateClient(u8Index))
   {
      m_poVideoDevBase[u8Index]->vTriggerVideoFocusCb(s32Focus,s32Reason);
   } //if(true == bValidateClient(u8Index))
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>

