/***********************************************************************/
/*!
* \file  spi_tclVideoSettings.cpp
* \brief Class to get the Video Layer Info
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class to get the Video Layer Info
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar G         | Initial Version
13.1.2014   | Vinoop U              | Implemented parser for reading video settings 
                                       from xml file
14.05.2014  | Shiva Kumar G         | Changes for 8.2" & 10.2" Screens handling
28.05.2014  | Hari Priya E R        | Removed reading of screen size from policy and included
                                      changes to read from EOL handling class
02.04.2015  | Shiva Kumar G         | Extended for Andriod Auto
25.06.2015  | Shiva kaumr G         | Dynamic display configuration

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Trace.h"
#include <string.h>
#include <SPITypes.h>
#include "FileHandler.h"

#include "spi_tclVideoSettings.h"
#include "spi_tclConfigReader.h"

#define SEVENT_TWENTY_PIXELS 720

#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VIDEO
#include "trcGenProj/Header/spi_tclVideoSettings.cpp.trc.h"
#endif
#endif

#ifdef VARIANT_S_FTR_ENABLE_GM
static const t_Char cosGMXmlConfigFile[] =  "/opt/bosch/gm/policy.xml";
#else
static const t_Char cosG3GXmlConfigFile[] =  "/opt/bosch/spi/xml/policy.xml";
#endif

/***************************************************************************
** FUNCTION:  spi_tclVideoSettings::spi_tclVideoSettings()
***************************************************************************/
spi_tclVideoSettings::spi_tclVideoSettings():m_bServerSideScalingReq(false),
m_u8TouchInputEvents(0),
m_szVideoCodecType("MEDIA_CODEC_VIDEO_H264_BP"),
m_u8MaxUnAckedFrames(4),
m_u8Fps(30),
m_bAutoStartProjection(false),
m_u8ContentAttestSignFlag(0)
{
   ETG_TRACE_USR1(("spi_tclVideoSettings() entered "));
   //add code
}

/***************************************************************************
** FUNCTION:  spi_tclVideoSettings::InitializeXmlReader()
***************************************************************************/
t_Void spi_tclVideoSettings::vReadVideoSettings()
{
   t_Char* szConfigFilePath = NULL;
#ifdef VARIANT_S_FTR_ENABLE_GM
   szConfigFilePath = const_cast<t_Char*>(cosGMXmlConfigFile);
#else
   szConfigFilePath = const_cast<t_Char*>(cosG3GXmlConfigFile);
#endif
   spi::io::FileHandler oPolicySettingsFile(szConfigFilePath,spi::io::SPI_EN_RDONLY);
   if(true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument oXmlDoc(szConfigFilePath);
      tclXmlReader oXmlReader(&oXmlDoc, this);
      oXmlReader.bRead("VIDEO");
   }//if(true == oPolicySettingsFile.bIsValid())
}

/***************************************************************************
** FUNCTION:  spi_tclVideoSettings::~spi_tclVideoSettings()
***************************************************************************/
spi_tclVideoSettings::~spi_tclVideoSettings()
{
   ETG_TRACE_USR1(("~spi_tclVideoSettings() entered "));
   //Add code
}

/************************************u32GetSurfaceId************************
** FUNCTION:  t_Void spi_tclVideoSettings::vGetScreenOffset(t_U32& u32Scre...
***************************************************************************/
t_Void spi_tclVideoSettings::vGetScreenOffset(
   trScreenOffset &rfoScreenOffset) const
{
   rfoScreenOffset = m_rScreenOffset;
   //Add code
   ETG_TRACE_USR1(("spi_tclVideoSettings:vGetScreenOffsets- X:%d  Y:%d ",
      m_rScreenOffset.u32Screen_X_Offset,m_rScreenOffset.u32Screen_Y_Offset));
}


/***************************************************************************
** FUNCTION:  t_Bool spi_tclVideoSettings::bIsServerSideScalingRequired()
***************************************************************************/
t_Bool spi_tclVideoSettings::bIsServerSideScalingRequired()
{
   ETG_TRACE_USR1(("spi_tclVideoSettings:bIsServerSideScalingRequired-%d ",
      ETG_ENUM(BOOL,m_bServerSideScalingReq)));
   //Add code
   return m_bServerSideScalingReq;
}

/***************************************************************************
** FUNCTION:  short t_U32 spi_tclVideoSettings::u8EnableTouchInputEvents()
***************************************************************************/
t_U8 spi_tclVideoSettings::u8EnableTouchInputEvents()
{
   ETG_TRACE_USR1(("spi_tclVideoSettings:u8EnableTouchInputEvents-%d ",
      m_u8TouchInputEvents));
   //Add code
   return m_u8TouchInputEvents;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideoSettings::vSetScreenSize()
***************************************************************************/
t_Void spi_tclVideoSettings::vSetScreenSize(const trScreenAttributes& corfrScreenAttributes)
{
   //Add code
   ETG_TRACE_USR1(("spi_tclVideoSettings::vSetScreenSize: Height-%d Width-%d ",
     corfrScreenAttributes.u32ScreenWidth,corfrScreenAttributes.u32ScreenHeight));
}

/***************************************************************************
** FUNCTION:  t_U8 spi_tclVideoSettings::u8GetContAttestSignFlag()
***************************************************************************/
t_U8 spi_tclVideoSettings::u8GetContAttestSignFlag()
{
   ETG_TRACE_USR1(("spi_tclVideoSettings:u8GetContAttestSignFlag-%d ",
      m_u8ContentAttestSignFlag));
   //Add code
   return m_u8ContentAttestSignFlag;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclVideoSettings::vGetVideoSettingsData()
***************************************************************************/
t_Void spi_tclVideoSettings::vDisplayVideoSettings()
{
   //Display all the settings on TTFis
   ETG_TRACE_USR1(("spi_tclVideoSettings::vDisplayVideoSettings()"));
   ETG_TRACE_USR4((" X_OFFSET = %d",m_rScreenOffset.u32Screen_X_Offset));
   ETG_TRACE_USR4((" Y_OFFSET = %d",m_rScreenOffset.u32Screen_Y_Offset));
   ETG_TRACE_USR4((" Touch = %d",m_u8TouchInputEvents));
   ETG_TRACE_USR4((" SERVER_SIDE_SCALING = %d",ETG_ENUM(BOOL,m_bServerSideScalingReq)));
   ETG_TRACE_USR4((" VideoCodecType = %s",m_szVideoCodecType.c_str()));
   ETG_TRACE_USR4((" MaxUnackedFrames = %d",m_u8MaxUnAckedFrames));
   ETG_TRACE_USR4((" PixelDenisty For 480p= %d",m_rDisplayDpi.u16Dpi480p));
   ETG_TRACE_USR4((" PixelDenisty For 720p= %d",m_rDisplayDpi.u16Dpi720p));
   ETG_TRACE_USR4((" AutoStartProjection Enabled = %d",ETG_ENUM(BOOL,m_bAutoStartProjection)));
   ETG_TRACE_USR4((" Content Attestation Signature Flag - %d ",m_u8ContentAttestSignFlag));
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclVideoSettings::vSetContAttestFlag()
***************************************************************************/
t_Void spi_tclVideoSettings::vSetContAttestFlag(t_U8 u8ContAttestFlag)
{
   ETG_TRACE_USR1(("spi_tclVideoSettings:vSetContAttestFlag:u8ContAttestFlag-%d",
      u8ContAttestFlag));

   //set the content attestation flag 
   m_u8ContentAttestSignFlag = u8ContAttestFlag ;

}

/***************************************************************************
** FUNCTION:  t_String spi_tclVideoSettings::szGetVideoCodecType()
***************************************************************************/
t_String spi_tclVideoSettings::szGetVideoCodecType()
{
   ETG_TRACE_USR1(("spi_tclVideoSettings:szGetVideoCodecType()-%s ",
      m_szVideoCodecType.c_str()));
   //Add code
   return m_szVideoCodecType.c_str();
}

/***************************************************************************
** FUNCTION:  t_U8 spi_tclVideoSettings::u8GetMaxUnackedFrames()
***************************************************************************/
t_U8 spi_tclVideoSettings::u8GetMaxUnackedFrames()
{
   ETG_TRACE_USR1(("spi_tclVideoSettings:u8GetMaxUnackedFrames()-%d ",
      m_u8MaxUnAckedFrames));
   //Add code
   return m_u8MaxUnAckedFrames;
}

/***************************************************************************
** FUNCTION:  t_U16 spi_tclVideoSettings::u16GetPixelDenisty()
***************************************************************************/
t_U16 spi_tclVideoSettings::u16GetPixelDenisty(tenDeviceCategory enDevCat)
{
   spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();

   ETG_TRACE_USR1(("spi_tclVideoSettings:vGetPixelDenisty() Dev Category -[%d] ",enDevCat ));

   trVideoConfigData rVideoConfigData;
   poConfigReader->vGetVideoConfigData(enDevCat,rVideoConfigData);

   // By default set the display to 480 Pixels DPI
   t_U16 u16DisplayDpi  = m_rDisplayDpi.u16Dpi480p;

   if (SEVENT_TWENTY_PIXELS == rVideoConfigData.u32ResolutionSupported)
   {
      // In case it is 10.2 inch screen set the DPI wrt 720p
      u16DisplayDpi = m_rDisplayDpi.u16Dpi720p;
   }


   ETG_TRACE_USR1(("spi_tclVideoSettings:vGetPixelDenisty()-[%d] ",u16DisplayDpi ));

return u16DisplayDpi;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclVideoSettings::bGetAutoStartProjection()
***************************************************************************/
t_Bool spi_tclVideoSettings::bGetAutoStartProjection()
{
   ETG_TRACE_USR1(("spi_tclVideoSettings: bGetAutoStartProjection Enabled-%d ",
      ETG_ENUM(BOOL,m_bAutoStartProjection)));
   //Add code
   return m_bAutoStartProjection;
}

/***************************************************************************
** FUNCTION:  t_U8 spi_tclVideoSettings::u8GetFramesPerSec()
***************************************************************************/
t_U8 spi_tclVideoSettings::u8GetFramesPerSec()
{
   ETG_TRACE_USR1(("spi_tclVideoSettings:u8GetFramesPerSec()-%d ",
      m_u8Fps));
   //Add code
   return m_u8Fps;
}


/*************************************************************************
** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
*************************************************************************/
t_Bool spi_tclVideoSettings::bXmlReadNode(xmlNodePtr poNode)
{
   t_Bool bRetVal=false;
   t_String szNodeName;
   t_S32 u32iValue=0;

   if(NULL != poNode)
   {
      szNodeName = (const char *)(poNode->name);

      if("SCREEN_OFFSET"==szNodeName)
      {
         bRetVal = bGetAttribute("X_OFFSET", poNode, u32iValue);
         m_rScreenOffset.u32Screen_X_Offset = (t_U32)u32iValue;
         ETG_TRACE_USR2((" X_OFFSET = %d",m_rScreenOffset.u32Screen_X_Offset));

         bRetVal = bGetAttribute("Y_OFFSET", poNode, u32iValue);
         m_rScreenOffset.u32Screen_Y_Offset = (t_U32)u32iValue;
         ETG_TRACE_USR2((" Y_OFFSET = %d",m_rScreenOffset.u32Screen_Y_Offset));
      }//else if("SCREEN_OFFSET"==szNodeName)
      else if("ENABLE_TOUCH"==szNodeName)
      {
         bRetVal = bGetAttribute("VALUE", poNode, u32iValue);
         m_u8TouchInputEvents=(t_U8)u32iValue;
         ETG_TRACE_USR2((" Touch = %d",m_u8TouchInputEvents));
      }//else if("ENABLE_TOUCH"==szNodeName)
      else if("SERVER_SIDE_SCALING"==szNodeName)
      {
         bRetVal = bGetAttribute("BOOL", poNode, m_bServerSideScalingReq);
         ETG_TRACE_USR2((" SERVER_SIDE_SCALING = %d",ETG_ENUM(BOOL,m_bServerSideScalingReq)));
      }//else if("SERVER_SIDE_SCALING"==szNodeName)
      else if("VIDEO_CODEC"==szNodeName)
      {
         bRetVal = bGetAttribute("CODEC_TYPE", poNode, m_szVideoCodecType);
         ETG_TRACE_USR2((" VIDEO_CODEC TYPE = %s",m_szVideoCodecType.c_str()));
      }//else if("VIDEO_CODEC"==szNodeName)
      else if("MAX_UNACKED_FRAMES"==szNodeName)
      {
         bRetVal = bGetAttribute("VALUE", poNode, u32iValue);
         m_u8MaxUnAckedFrames = (t_U8)u32iValue;
         ETG_TRACE_USR2((" MAX_UNACKED_FRAMES = %d",m_u8MaxUnAckedFrames));
      }  //else if("MAX_UNACKED_FRAMES"==szNodeName)
      else if("DPI_DENSITY"==szNodeName)
      {
      	bRetVal = bGetAttribute("FOUR_EIGHTY", poNode, u32iValue);
      	m_rDisplayDpi.u16Dpi480p = (t_U16)u32iValue;
      	ETG_TRACE_USR2((" DPI_DENSITY FOR 480P = %d",m_rDisplayDpi.u16Dpi480p));

      	bRetVal = bGetAttribute("SEVEN_TWENTY", poNode, u32iValue);
     	m_rDisplayDpi.u16Dpi720p = (t_U16)u32iValue;
      	ETG_TRACE_USR2((" DPI_DENSITY FOR 720P= %d",m_rDisplayDpi.u16Dpi720p));
      }//else if("DPI_DENSITY"==szNodeName)
      else if("FRAMES_PER_SEC"==szNodeName)
      {
         bRetVal = bGetAttribute("VALUE", poNode, u32iValue);
         m_u8Fps = (t_U8)u32iValue;
         ETG_TRACE_USR2((" FRAMES_PER_SEC = %d",m_u8Fps));
      }//else if("FRAMES_PER_SEC"==szNodeName)
      else if("AUTO_START_PROJECTION"==szNodeName)
      {
         bRetVal = bGetAttribute("BOOL", poNode, m_bAutoStartProjection);
         ETG_TRACE_USR2((" AUTO_START_PROJECTION = %d",ETG_ENUM(BOOL,m_bAutoStartProjection)));
      }//else if("AUTO_START_PROJECTION"==szNodeName)
      else if("CONTENT_ATTESTATION_SIGNFLAG"==szNodeName)
      {
         bRetVal = bGetAttribute("VALUE", poNode, u32iValue);
         m_u8ContentAttestSignFlag = (t_U8)u32iValue ;
         ETG_TRACE_USR2((" CONTENT_ATTEST_SIGNFLAG = %d",m_u8ContentAttestSignFlag));
      }
   }//if(NULL != poNode)
   return bRetVal;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
