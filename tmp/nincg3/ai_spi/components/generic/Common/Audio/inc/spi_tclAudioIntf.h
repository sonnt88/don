/*!
 *******************************************************************************
 * \file             spi_tclAudioIntf.h
 * \brief            Interface class that will be used to send responses 
                     from Audio policy to the Audio Manager
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Interface Audio class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 28.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version
 16.11.2013 |  Raghavendra S (RBEI/ECP2)			   | Interface Redefinition for Audio
                                                         Response from Audio Manager

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAUDIOINTF_H
#define SPI_TCLAUDIOINTF_H

#include "SPITypes.h"
/**
 *  class definitions.
 */

/**
 * Interface class that will be used to send responses 
 *  from Audio policy to the Audio Manager
 */
class spi_tclAudioIntf 
{
public:
	 /***************************************************************************
     *********************************PUBLIC*************************************
     ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAudioIntf::spi_tclAudioIntf();
    ***************************************************************************/
    /*!
    * \fn      spi_tclAudioIntf()
    * \brief   Default Constructor
    **************************************************************************/
   spi_tclAudioIntf(){}

	/***************************************************************************
    ** FUNCTION:  spi_tclAudioIntf::~spi_tclAudioIntf();
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclAudioIntf()
    * \brief   Virtual Destructor
    **************************************************************************/
   virtual ~spi_tclAudioIntf(){}

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioIntf::vOnRouteAllocateResult(tU8, ..)
    ***************************************************************************/
    /*!
    * \fn      vOnRouteAllocateResult(tU8 u8SourceNum, trAudSrcInfo& rfrSrcInfo)
    * \brief   Notification from the Audio Manager on Completion of Route Alloc. 
	*          Implement Source component specific actions on Allocation of Audio Route.
	*          Mandatory Interface to be implemented.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
   *          [rfrSrcInfo]: Reference to structure containing details of the allocated source.
	* \retval  NONE
    **************************************************************************/
	virtual t_Bool bOnRouteAllocateResult(t_U8 u8SourceNum , trAudSrcInfo& rfrSrcInfo) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioIntf::vOnRouteDeAllocateResult(tU8)
    ***************************************************************************/
    /*!
    * \fn      vOnRouteDeAllocateResult(tU8 u8SourceNum)
    * \brief   Notification from the Audio Manager on Completion of Route Dealloc. 
	*          Implement Source component specific actions on Deallocation of Audio Route.
	*          Mandatory Interface to be implemented.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vOnRouteDeAllocateResult(t_U8 u8SourceNum) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioIntf::vOnStartSourceActivity(tU8)
    ***************************************************************************/
    /*!
    * \fn      vOnStartSourceActivity(tU8 u8SourceNum)
    * \brief   Trigger from the Audio Manager to Start play of Audio from Device. 
	*          Mandatory Interface to be implemented.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vOnStartSourceActivity(t_U8 u8SourceNum) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioIntf::vOnStopSourceActivity(tU8)
    ***************************************************************************/
    /*!
    * \fn      vOnStopSourceActivity(tU8 u8SourceNum)
    * \brief   Trigger from the Audio Manager to Start play of Audio from Device
	*          Mandatory Interface to be implemented.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vOnStopSourceActivity(t_U8 u8SourceNum) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioIntf::vOnNewMuteState(tU8, t_Bool)
    ***************************************************************************/
    /*!
    * \fn      bOn(tU8 u8SourceNum)
    * \brief   Notification from the Audio Manager on change in Mute State. 
	*          Implement Source component specific actions on change in Mute State.
	*          Optional Interface to be implemented if required.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	*          [bMute]: TRUE if Source Mute is active, FALSE Otherwise
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vOnNewMuteState(t_U8 u8SourceNum, t_Bool bMute){}

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioIntf::bOnReqAVActivationResult(tU8, t_Bool)
    ***************************************************************************/
    /*!
    * \fn     bOnReqAVActivationResult
    * \brief  Application specific function after which RequestAVAct
    *          result can be processed.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
    * \retval  true if the application processing is successful false otherwise
    **************************************************************************/
   virtual t_Bool bOnReqAVActivationResult(const t_U8 u8SourceNum){return true;}

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioIntf::bOnReqAVDeActivationResult(tU8, t_Bool)
    ***************************************************************************/
    /*!
    * \fn     bOnReqAVDeActivationResult
    * \brief  Application specific function after which RequestAVDeAct
    *          result can be processed.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
    * \retval  true if the application processing is successful false otherwise
    **************************************************************************/

   virtual t_Bool bOnReqAVDeActivationResult(const t_U8 u8SourceNum){};

   /***************************************************************************
    ** FUNCTION: t_Bool spi_tclAudioIntf::vOnAudioError()
    ***************************************************************************/
   /*!
    * \fn    t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError)
    * \brief  Interface to set the audio error.
    * \param  cou8SourceNum       : [IN] Source number of the audio source
    * \param  enAudioError : [IN] Audio Error
    **************************************************************************/
   virtual t_Void vOnAudioError(const t_U8 cou8SourceNum, tenAudioError enAudioError);

};

#endif // SPI_TCLAUDIOINTF_H

