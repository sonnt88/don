/*!
 *******************************************************************************
 * \file             spi_tclAudioInBase.h
 * \brief            Base class for Audio Input Handling
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class for Audio Input Handling
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 21.03.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAUDIOINBASE_H_
#define SPI_TCLAUDIOINBASE_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <functional>

//! Includes for glib dbus bindings
#include <glib.h>
#include <dbus/dbus-glib.h>

#include "BaseTypes.h"
#include "Threadable.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

//! \brief:  DataSet values for SPI
enum tenAudioInDataSet
{
   //! Enum values should match with the config IDS in ECNR configuration
   e32_AUDIOIN_DATASET_UNKNOWN = 0,
   e32_AUDIOIN_DATASET_PHONE_NB = 1,
   e32_AUDIOIN_DATASET_PHONE_WB = 2,
   e32_AUDIOIN_DATASET_VR_NB = 3,
   e32_AUDIOIN_DATASET_VR_WB = 4,
   e32_AUDIOIN_DATASET_VR_MIC_ONLY = 5
};

//! \brief: Functor to receive Audio In Result
typedef std::function<t_Void(t_Bool)> tfvAudioInRes;

//! \brief: Callbacks for AudioIn
struct trAudioInCbs
{
      tfvAudioInRes fvIntializeAudioInCb;
      tfvAudioInRes fvUninitializeAudioInCb;
      tfvAudioInRes fvStartAudioInCb;
      tfvAudioInRes fvStopAudioInCb;
      tfvAudioInRes fvSetAlsaDeviceCb;
      tfvAudioInRes fvSetAudioInConfigCb;

      trAudioInCbs() :
                        fvIntializeAudioInCb(NULL),
                        fvUninitializeAudioInCb(NULL),
                        fvStartAudioInCb(NULL),
                        fvStopAudioInCb(NULL),
                        fvSetAlsaDeviceCb(NULL),
                        fvSetAudioInConfigCb(NULL)
      {

      }
};

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAudioInBase
 * \brief Base class for Audio Input Handling
 */

class spi_tclAudioInBase: public shl::thread::Threadable
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::spi_tclAudioInBase
       ***************************************************************************/
      /*!
       * \fn     spi_tclAudioInBase()
       * \brief  Default Constructor
       * \sa      ~spi_tclAudioInBase()
       **************************************************************************/
      spi_tclAudioInBase();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::~spi_tclAudioInBase
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclAudioInBase()
       * \brief  virtual Destructor
       * \sa     spi_tclAudioInBase()
       **************************************************************************/
      virtual ~spi_tclAudioInBase();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  Register callbacks for AudioIn responses
       * \param  corfrAudioInCbs: reference to structure holding the AudioIn callbacks
       **************************************************************************/
      t_Void vRegisterCallbacks(const trAudioInCbs &corfrAudioInCbs);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vSubscribeForAudioIn
       ***************************************************************************/
      /*!
       * \fn     vSubscribeForAudioIn()
       * \brief  Subscribe for the AudioIn service
       * \note   Pure virtual: To be implemented by derived class
       **************************************************************************/
      virtual t_Void vSubscribeForAudioIn() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vInitializeAudioIn
       ***************************************************************************/
      /*!
       * \fn     vInitializeAudioIn()
       * \brief  Initialize the AudioIn service for SPI application with the given
       *         data set. This method has to be called on successful route
       *         allocation for SPI Audio In.
       * \param  enAudioDataSet: Dataset with which the audio route has to be
       *         initialized
       * \note   Pure virtual: To be implemented by derived class
       **************************************************************************/
      virtual t_Void vInitializeAudioIn(tenAudioInDataSet enAudioDataSet) = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vUnitializeAudioIn
       ***************************************************************************/
      /*!
       * \fn     vUnitializeAudioIn()
       * \brief  Uninitialize the AudioIn service for SPI application. This method
       *         has to be called on successful deallocation of SPI audioin route
       * \note   Pure virtual: To be implemented by derived class
       **************************************************************************/
      virtual t_Void vUnitializeAudioIn() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vStartAudioIn
       ***************************************************************************/
      /*!
       * \fn     vStartAudioIn()
       * \brief  Start AudioIn streaming. This method has to be called on receiving
       *         source activity ON for SPI Audioin route
       * \note   Pure virtual: To be implemented by derived class
       **************************************************************************/
      virtual t_Void vStartAudioIn() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vStopAudioIn
       ***************************************************************************/
      /*!
       * \fn     vStopAudioIn()
       * \brief  Stop AudioIn streaming. This method has to be called on
       *         receiving source activity OFF for SPI Audioin route
       * \note   Pure virtual: To be implemented by derived class
       **************************************************************************/
      virtual t_Void vStopAudioIn() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vSetAudioInConfig
       ***************************************************************************/
      /*!
       * \fn     vSetAudioInConfig()
       * \brief  Sets the Audio configuration datasets dynamically.
       * \param  enAudioDataSet: AudioIn configuration Data set to be used
       * \note   Pure virtual: To be implemented by derived class
       **************************************************************************/
      virtual t_Void vSetAudioInConfig(tenAudioInDataSet enAudioDataSet) = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vSetWBAudio
       ***************************************************************************/
      /*!
       * \fn     vSetWBAudio()
       * \brief  Sets usage of Wideband Audio
       * \param  bSettoWB: Indicates Wide band audio usage
       * \note   Pure virtual: To be implemented by derived class
       **************************************************************************/
      virtual t_Void vSetWBAudio(t_Bool bSettoWB) = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vSetAlsaDevice
       ***************************************************************************/
      /*!
       * \fn     vSetAlsaDevice()
       * \brief  Sets the ECNR Alsa Device
       * \param  u32AlsaDeviceSelector: Alsa Device Selector
       * \param  szAlsaDeviceName: Alsa Device Name
       * \note   Pure virtual: To be implemented by derived class
       **************************************************************************/
      virtual t_Void vSetAlsaDevice(t_U32 u32AlsaDeviceSelector, t_String szAlsaDeviceName) = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vResultInitializeAudioInRes
       ***************************************************************************/
      /*!
       * \fn     vResultInitializeAudioInRes()
       * \brief  Send the AudioIn initialization result to registered callbacks
       * \param  bResult: true on successful initialization otherwise false
       **************************************************************************/
      t_Void vResultInitializeAudioInRes(t_Bool bResult);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vResultUnitializeAudioInRes
       ***************************************************************************/
      /*!
       * \fn     vResultUnitializeAudioInRes()
       * \brief  Send the AudioIn Uninitialization result to registered callbacks
       * \param  bResult: true on successful uninitialization otherwise false
       **************************************************************************/
      t_Void vResultUnitializeAudioInRes(t_Bool bResult);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vResultStartAudioInRes
       ***************************************************************************/
      /*!
       * \fn     vResultStartAudioInRes()
       * \brief  Send the AudioIn Start result to registered callbacks
       * \param  bResult: true on successful start of audioin otherwise false
       **************************************************************************/
      t_Void vResultStartAudioInRes(t_Bool bResult);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vResultStopAudioInRes
       ***************************************************************************/
      /*!
       * \fn     vResultStopAudioInRes()
       * \brief  Send the AudioIn Stop result to registered callbacks
       * \param  bResult: true on successful stop of audioin otherwise false
       **************************************************************************/
      t_Void vResultStopAudioInRes(t_Bool bResult);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vResultSetAlsaDeviceRes
       ***************************************************************************/
      /*!
       * \fn     vResultSetAlsaDeviceRes()
       * \brief  Send the Set Alsa device result to registered callbacks
       * \param  bResult: true on successful stop of audioin otherwise false
       **************************************************************************/
      t_Void vResultSetAlsaDeviceRes(t_Bool bResult);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vResultSetAudioConfigRes
       ***************************************************************************/
      /*!
       * \fn     vResultSetAudioConfigRes()
       * \brief  Send the audio configuration result to registered callbacks
       * \param  bResult: true on successful stop of audioin otherwise false
       **************************************************************************/
      t_Void vResultSetAudioConfigRes(t_Bool bResult);

   private:

      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/

      /*************************************************************************
       **  FUNCTION : spi_tclAudioInBase::spi_tclAudioInBase(const spi_tclAudioInBase &corfAudioInBase)
       *************************************************************************/
      /*!
       * \fn     spi_tclAudioInBase(const spi_tclAudioInBase &corfAudioInBase)
       * \brief  Copy constructor: Made private to prevent unintended usage
       *         of Default copy constructor
       * \param  corfAudioInBase : [IN] reference to AudioInBase class
       *************************************************************************/
      spi_tclAudioInBase(const spi_tclAudioInBase &corfAudioInBase);

      /*************************************************************************
       **  FUNCTION : spi_tclAudioInBase& operator=(const spi_tclAudioInBase &corfAudioInBase)
       *************************************************************************/
      /*!
       * \fn     spi_tclAudioInBase& operator=(const spi_tclAudioInBase &corfAudioInBase)
       * \brief  Overloaded function: Made private to prevent unintended usage of
       *         default assignment operator
       * \param  corfAudioInBase : [IN] reference to AudioInBase class
       *************************************************************************/
      spi_tclAudioInBase& operator=(const spi_tclAudioInBase &corfAudioInBase);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioInBase::vExecute
       ***************************************************************************/
      /*!
       * \fn     vExecute()
       * \brief  Executes in a seperate thread. This creates a Mainloop which waits
       *         for DBus asynchronous responses/signals
       **************************************************************************/
      t_Void vExecute();

      //! Pointer to MainLoop which waits for the responses, signals from DBus
      GMainLoop *m_poMainLoop;

      //! Structure holding the registered callbacks for AudioIn
      trAudioInCbs m_rAudioInCbs;

};

#endif /* SPI_TCLAUDIOINBASE_H_ */
