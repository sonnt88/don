/*!
 *******************************************************************************
 * \file             spi_tclAudioSettings.h
 * \brief            Settings class provides the interface to 
                     retrieve the project policy configurations
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   SmartPhone Integration
 DESCRIPTION:    Settings Audio class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 28.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version
 18.11.2013 |  Raghavendra S (RBEI/ECP2)               | Redefinition of Interface
 27/01/2014 |  Vinoop(RBEI/ECP2)                       |Implemented parser for extracting 
                                                          Audio settings from xml file
14.08.2015  | Shiva Kumar Gurija                       | Extensions for ML Dynamic Audio

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAUDIOSETTINGS_H
#define SPI_TCLAUDIOSETTINGS_H

#include "SPITypes.h"
#include "GenericSingleton.h"

#include "XmlDocument.h"
#include "Xmlable.h"
#include "XmlReader.h"

using namespace shl::xml;

/**
 *  class definitions.
 */

/**
 * This is the Settings class provides the interface to 
   retrieve the project policy configurations
 */

class spi_tclAudioSettings: public GenericSingleton<spi_tclAudioSettings> ,
         public tclXmlReadable
{
   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioSettings::~spi_tclAudioSettings()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclAudioSettings()
       * \brief   Destructor
       **************************************************************************/
      virtual ~spi_tclAudioSettings();

      /*!
       * \brief   Generic Singleton class
       */
      friend class GenericSingleton<spi_tclAudioSettings> ;

      /***************************************************************************
       ** FUNCTION:  t_U8 spi_tclAudioSettings::u8GetSourceNumber(tenAudioDir)
       ***************************************************************************/
      /*!
       * \fn      u8GetSourceNumber( tenAudioDir enAudDir)
       * \brief   Function to get the Source Number for Audio Link & selected device
       *          category.
       * \param   [enAudDir]: Audio Link Direction
       * \retval  t_U8 value: Source Number
       **************************************************************************/
      t_U8 u8GetSourceNumber(tenAudioDir enAudDir) const;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclAudioSettings::vGetRTPPayloadType
       (t_U32& rfu32RTPPayload)
       ***************************************************************************/
      /*!
       * \fn      t_Void vGetRTPPayloadType(t_U32& u32RTPPayload)
       * \brief   Method to get the RTPPayload type
       * \param   [rfu32RTPPayload]: Reference to the RTP Payload type
       * \retval  NONE
       **************************************************************************/
      t_Void vGetRTPPayloadType(t_U32& rfu32RTPPayload) const;

      //TO BE CHECKED

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclAudioSettings::vGetAudioIPL
       (tU32& rfu32AudioIPL)
       ***************************************************************************/
      /*!
       * \fn      t_Void vGetAudioIPL(tU32& rfu32AudioIPL)
       * \brief   Method to get the Audio IPL
       * \param   [rfu32AudioIPL]: Reference to the Audio IPL
       * \retval  t_Bool value: TRUE if Setting is Valid, FALSE Otherwise
       **************************************************************************/
      t_Void vGetAudioIPL(t_U32& rfu32AudioIPL) const;

       /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudioSettings::bGetAudioBlockingTriggerBasedOnCat()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bGetAudBlockTriggerBasedOnCat()
       * \brief   Method to get the Audio Blocking enable/disable trigger based on 
       *          Application category or not. This feature works only in conjunction with the 
       *          Dynamic Audio feature
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bGetAudBlockTriggerBasedOnCat() const;

       /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudioSettings::bGetAudBlockTriggerBasedOnGlobaleMute()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bGetAudBlockTriggerBasedOnGlobaleMute()
       * \brief   Method to get the Audio Blocking enable/disable trigger based on 
       *          Global mute is enabled or not
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bGetAudBlockTriggerBasedOnGlobaleMute() const;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclAudioSettings::vGetAudioSettingsData()
       ***************************************************************************/
      /*!
       * \fn     t_Void vGetAudioSettingsData()
       * \brief  To print the audio settings data
       * \retval  t_Void
       **************************************************************************/
      t_Void vDisplayAudioSettings();

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclAudioSettings::vGetAudioPipeConfig()
       ***************************************************************************/
      /*!
       * \fn     t_Void vGetAudioPipeConfig()
       * \brief  provides audio configuration for the specified audio steam
       * \param  [OUT]rfrmapAudioPipeConfig : returns audio configuration as string
       **************************************************************************/
      t_Void vGetAudioPipeConfig(tmapAudioPipeConfig &rfmapAudioPipeConfig);
	  
      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudioSettings::bGetMLDynAudioSupport()
       ***************************************************************************/
      /*!
       * \fn     t_Bool bGetMLDynAudioSupport() const
       * \brief  To Get whether ML Audio is supported or not.
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bGetMLDynAudioSupport() const;

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclAudioSettings::bGetAAPMediaStreamRecEnabled()
      ***************************************************************************/
      /*!
       * \fn     t_Bool bGetAAPMediaStreamRecEnabled() const
       * \brief  To Get whether AAP Media audio stream recording is enabled or not.
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bGetAAPMediaStreamRecEnabled() const;

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclAudioSettings::bGetAAPGuidanceStreamRecEnabled()
      ***************************************************************************/
      /*!
       * \fn     t_Bool bGetAAPGuidanceStreamRecEnabled() const
       * \brief  To Get whether AAP Guidance audio stream recording is enabled or not.
       * \retval  t_Bool
       **************************************************************************/
      t_Bool bGetAAPGuidanceStreamRecEnabled() const;

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioSettings::spi_tclAudioSettings()
       ***************************************************************************/
      /*!
       * \fn      spi_tclAudioSettings()
       * \brief   Default Constructor
       **************************************************************************/
      spi_tclAudioSettings();

      /*************************************************************************
       ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
       *************************************************************************/
      /*!
       * \fn     virtual bool bXmlReadNode(xmlNode *poNode)
       * \brief  virtual function to read data from a xml node
       * \param  poNode : [IN] pointer to xml node
       * \retval bool : true if success, false otherwise.
       *************************************************************************/
      virtual bool bXmlReadNode(xmlNodePtr poNode);

      /***************************************************************************
       ** FUNCTION:  spi_tclVideoSettings::vReadAudioSettings()
       ***************************************************************************/
      /*!
       * \retval  t_Void
       **************************************************************************/
      t_Void vReadAudioSettings();

      t_U8 m_au8Sources[e8AUD_INVALID];
      t_U32 m_RTPPayload;
      t_U32 m_AudioIPL;

      //! Member variable to store whether the Audio Blocking based on Application Category feature is enabled or not
      t_Bool m_bAudBlocking_AppCat;
      //! Member variable to store whether the Audio Blocking based on Global Mute feature is enabled or not
      t_Bool m_bAudBlocking_GlobalMute;

      tmapAudioPipeConfig  m_mapszAudioPipeConfig;
      t_Bool m_bEnableMLDynAudio;

      //! Indicates whether Media stream audio recording is enabled.
      t_Bool m_EnableMediaStreamRec;
      //! Indicates whether Guidance stream audio recording is enabled.
      t_Bool m_EnableGuidanceStreamRec;

      /***************************************************************************
       ****************************END OF PRIVATE *********************************
       ***************************************************************************/

}; //spi_tclAudioSettings

#endif //_SPI_TCLAUDIOSETTINGS_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>

