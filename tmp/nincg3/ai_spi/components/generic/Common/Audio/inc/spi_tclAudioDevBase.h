/*!
 *******************************************************************************
 * \file             spi_tclAudioDevBase.h
 * \brief            Abstract class that specifies the interface which must be 
 *                   implemented by device class (Mirror Link/Digital iPod out) 
 *                   for communication with SDK
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class for ML and DiPO Interfaces
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 28.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version
 18.11.2013 |  Raghavendra S (RBEI/ECP2)               | Redefinition of Interface

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAUDIODEVBASE_H
#define SPI_TCLAUDIODEVBASE_H

#include "SPITypes.h"

/**
 *  Type Defintions.
 */
//! Callback signatures definitions: To be registered by the Creator of this class object
typedef std::function<void(t_U32, tenDeviceConnectionReq, t_Bool)>
         tfvSelectDeviceResp;
typedef std::function<void(tenAudioDir, t_Bool)> tfvStartAudioResp;
typedef std::function<void(tenAudioDir, t_Bool)> tfvStopAudioResp;
typedef std::function<void(t_U32, tenDeviceCategory,tenAudioDir,tenAudioSamplingRate)> tfvLaunchAudReq;
typedef std::function<void(t_U32, tenAudioDir)> tfvTerminateAudReq;
typedef std::function<void(tenAudioDir)> tfvStartAudioIn;
typedef std::function<void(tenAudioDir)> tfvStopAudioIn;
typedef std::function<bool(const t_U16,const t_U8,const tenDuckingType, tenDeviceCategory)> tfbSetAudioDucking;

/*!
 * \brief Structure holding the callbacks to be registered by the
 * Creator of this class object
 */
struct trAudioCallbacks
{
      //! Informs Select/Deselect Device Result
      tfvSelectDeviceResp fvSelectDeviceResp;

      //! Informs when Audio has started playing
      tfvStartAudioResp fvStartAudioResp;

      //! Informs when Audio has stopped playing
      tfvStopAudioResp fvStopAudioResp;

      //! Informs to allocate Audio Channel
      tfvLaunchAudReq fvLaunchAudioReq;

      //! Informs to deallocate Audio channel
      tfvTerminateAudReq fvTerminateAudioReq;

      //! Informs when ecnr has to be started
      tfvStartAudioIn fvStartAudioIn;

      //! Informs when ecnr has to be stopped
      tfvStopAudioIn fvStopAudioIn;

      //! informs when ducking/unducking has to be performed.
      tfbSetAudioDucking fbSetAudioDucking;

      trAudioCallbacks() :
         fvSelectDeviceResp(NULL), fvStartAudioResp(NULL),
                fvStopAudioResp(NULL), fvLaunchAudioReq(NULL), fvTerminateAudioReq(NULL),
                fvStartAudioIn(NULL), fvStopAudioIn(NULL),fbSetAudioDucking(NULL)
      {

      }
};

/**
 * Abstract class that specifies the interface which must be 
 * implemented by device class (Mirror Link/Digital iPod out) 
 * for communication with SDK
 */

class spi_tclAudioDevBase
{

   protected:

      /***************************************************************************
       *********************************PROTECTED**********************************
       ***************************************************************************/

   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioDevBase::spi_tclAudioDevBase();
       ***************************************************************************/
      /*!
       * \fn      spi_tclAudioDevBase()
       * \brief   Default Constructor
       **************************************************************************/
      spi_tclAudioDevBase()
      {
      }

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioDevBase::~spi_tclAudioDevBase();
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAudioDevBase()
       * \brief   Virtual Destructor
       **************************************************************************/
      virtual ~spi_tclAudioDevBase()
      {
      }

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioDevBase::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  Interface for the creator class to register for the required
       *         callbacks.
       * \param  rfrAudioCallbacks : reference to the callback structure
       *         populated by the caller
       **************************************************************************/
      virtual t_Void vRegisterCallbacks(trAudioCallbacks &rfrAudioCallbacks) = 0;

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioDevBase::bInitializeAudioPlayback(t_U32)
       ***************************************************************************/
      /*!
       * \fn      bInitializeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
       * \brief   Perform necessary actions to prepare for an Audio Playback.
       *          Function will be called prior to a Play Command from Audio Manager.
       *          Optional Interface to be implemented by Device Class.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device
	   * \param   [enAudDir]: Audio route being allocated
       * \retval  Bool value
       **************************************************************************/
      virtual t_Bool bInitializeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
      {
         return true;
      }

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioDevBase::vSetAudioPipeConfig
       ***************************************************************************/
      /*!
       * \fn      vSetAudioPipeConfig
       * \brief   sets audio pipeline configuration for alsa devices 
       *          Optional Interface to be implemented by Device Class.
       * \param   rfrmapAudioPipeConfig:map containing audio pipeline configs
       **************************************************************************/
      virtual t_Void vSetAudioPipeConfig(tmapAudioPipeConfig &rfrmapAudioPipeConfig)
      {
      }

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioDevBase::bStartAudio(t_U32,t_String,tenAudioLink)
       ***************************************************************************/
      /*!
       * \fn      bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev
       *          tenAudioLink enLink)
       * \brief   Start Streaming of Audio from the CE Device to the Audio Output
       *          Device assigned by the Audio Manager for the Source.
       *          Mandatory Interface to be implemented.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *		  [szAudioDev] : ALSA Audio Device
       *          [enAudDir]   :Specify the Audio Direction(Alternate or Main Audio).
       * \retval  Bool value
       **************************************************************************/
      virtual t_Bool bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
              tenAudioDir enAudDir) = 0;

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioDevBase::bStartAudio(t_U32,t_String, t_String,
	   **					tenAudioDir)
       ***************************************************************************/
      /*!
       * \fn      bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
       *          t_String szInputAudioDev, tenAudioDir enAudDir)
       * \brief   Overloaded method to handle audio stream for Phone and VR.
       *          Start Streaming of Audio from the CE Device to the Audio Output
       *          Device assigned by the Audio Manager for the Source.
       *          Mandatory Interface to be implemented.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *          [szOutputAudioDev]: Output ALSA Audio Device
       *          [szInputAudioDev] : Input ALSA Audio Device
	   *		  [enAudDir]        : Specify the Audio Direction(Phone or VR Audio).
       * \retval  Bool value
       **************************************************************************/
      virtual t_Bool bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
              t_String szInputAudioDev, tenAudioDir enAudDir) 
	  {
			  return false;
	  }

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudioDevBase::vStopAudio(t_U32, tenAudioDir)
       ***************************************************************************/
      /*!
       * \fn      vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
       * \brief   Stop Streaming of Audio from the CE Device to the Audio Output
       *          Device assigned by the Audio Manager for the Source.
       *          Mandatory Interface to be implemented.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *          [enAudDir]  :Specify the Audio Direction.
       * \retval  None
       **************************************************************************/
      virtual t_Void vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir) = 0;

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioDevBase::bFinalizeAudioPlayback(t_U32,tenAudioDir)
       ***************************************************************************/
      /*!
       * \fn      bFinalizeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
       * \brief   Perform necessary actions on completion of Audio Playback.
       *          Function will be called after to a Stop Command from Audio Manager.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
	   * \param   [enAudDir]: Audio route being deallocated
       * \retval  Bool value
       **************************************************************************/
      virtual t_Bool bFinalizeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
      {
         return true;
      }

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioDevBase::bSelectAudioDevice(t_U32)
       ***************************************************************************/
      /*!
       * \fn      bSelectAudioDevice(t_U32 u32DeviceId CbAudioResp cbSelectDevResp)
       * \brief   Perfom necessary actions specific to a device selection like
       *          obtaining audio capabilities of device, supported modes etc
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *		   [cbSelectDevResp]: Callback function provided to notify response
       *          for Select Device.
       * \retval  Bool value
       **************************************************************************/
      virtual t_Bool bSelectAudioDevice(t_U32 u32DeviceId) = 0;

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioDevBase::vDeselectAudioDevice(t_U32)
       ***************************************************************************/
      /*!
       * \fn      vDeselectAudioDevice(t_U32 u32DeviceId)
       * \brief   Perfom necessary actions specific to a device on deselection.
       *          Optional Interface for implementation.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       * \retval  Bool value
       **************************************************************************/
      virtual t_Void vDeselectAudioDevice(t_U32 u32DeviceId) = 0;

      /***************************************************************************
      ** FUNCTION:  t_Void  spi_tclAudioDevBase::vUpdateDeviceSelection()
      ***************************************************************************/
      /*!
      * \fn      t_Void vUpdateDeviceSelection()
      * \brief   To update the device selection.
      * \param   u32DevID : [IN] Device ID.
      * \param   enDevCat : [IN] Category of the device
      * \param   enDeviceConnReq : [IN] Select/ deselect.
      * \param   enRespCode : [IN] Response code (success/failure)
      * \param   enErrorCode : [IN] Error
      * \retval  t_Void
      **************************************************************************/
      virtual t_Void vUpdateDeviceSelection(t_U32 u32DevID, tenDeviceCategory enDevCat,
                              tenDeviceConnectionReq enDeviceConnReq,
                              tenResponseCode enRespCode, tenErrorCode enErrorCode){}

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioDevBase::bIsAudioLinkSupported(t_U32,
       *                                            tenAudioLink)
       ***************************************************************************/
      /*!
       * \fn      bIsAudioLinkSupported(t_U32 u32DeviceId)
       * \brief   Perfom necessary actions specific to a device on de-selection.
       *          Optional Interface for implementation.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *          [enLink]: Specify the Audio Link Type for which Capability is
       *          requested. Mandatory interface to be implemented.
       * \retval  Bool value, TRUE if Supported, FALSE otherwise
       **************************************************************************/
      virtual t_Bool bIsAudioLinkSupported(t_U32 u32DeviceId,
               tenAudioLink enLink) = 0;
	   /***************************************************************************
	   ** FUNCTION: t_Bool spi_tclAudioDevBase::bSetAudioBlockingMode()
	   ***************************************************************************/
	   /*!
	   * \fn     virtual t_Bool bSetAudioBlockingMode(const t_U32 cou32DevId,
	   *         const tenBlockingMode coenBlockingMode)
	   * \brief  Interface to set the audio blocking mode.
	   * \param  cou32DevId             : [IN] Uniquely identifies the target Device.
	   * \param  coenBlockingMode       : [IN] Identifies the Blocking Mode.
	   * \retval t_Bool
	   **************************************************************************/
	   virtual t_Bool bSetAudioBlockingMode(const t_U32 cou32DevId,
	      const tenBlockingMode coenBlockingMode) {} //TODO

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudioDevBase::vOnAudioError()
       ***************************************************************************/
      /*!
       * \fn    t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError)
       * \brief  Interface to set the audio error.
       * \param  enAudDir       : [IN] Uniquely identifies the target Device.
       * \param  enAudioError : [IN] Audio Error
       **************************************************************************/
      virtual t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError) = 0;

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/
};
#endif // SPI_TCLAUDIODEVBASE_H
