/*!
 *******************************************************************************
 * \file          spi_tclAudioSrcInfo.h
 * \brief         Audio source class that provides interface to store and retrieve
 *                Source details
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Source Information class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 27.03.2014 |  Deepti Samant (RBEI/ECP2)               | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAUDIOSRCINFO_H
#define SPI_TCLAUDIOSRCINFO_H

#include <map>
#include "SPITypes.h"

using namespace std;

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/
//! \brief   Structure holding Audio Source information
struct trAudioSrcProperties
{
      //Provides Device ID
      t_U32 u32DeviceId;
      //Provides Device Category
      tenDeviceCategory enDeviceCategory;
      //Set if this source is active currently
      t_Bool bActiveSrc;
      //Provides Audio Direction
      tenAudioDir enAudDir;
      //Provides Audio Sampling Rate
      tenAudioSamplingRate enAudSampleRate;
      //Audio Source Info for from Route Allocation
      trAudSrcInfo rAudSrcInfo;
      trAudioSrcProperties() :
         u32DeviceId(0), enDeviceCategory(e8DEV_TYPE_UNKNOWN),
                  bActiveSrc(false), enAudDir(e8AUD_INVALID), enAudSampleRate(e8AUD_SAMPLERATE_DEFAULT)
      {
      }
};

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef map<t_U8, trAudioSrcProperties>::iterator AudSrcInfoMapItr;

/**
 *  class definitions.
 */
/**
 * This is the main audio class that provides interface to store
 * and retrieve Audio Source details.
 */
class spi_tclAudioSrcInfo
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioSrcInfo::spi_tclAudioSrcInfo();
       ***************************************************************************/
      /*!
       * \fn      spi_tclAudioSrcInfo()
       * \brief   Default Constructor
       **************************************************************************/
      spi_tclAudioSrcInfo();

      /***************************************************************************
       ** FUNCTION:  spi_tclAudioSrcInfo::~spi_tclAudioSrcInfo();
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAudioSrcInfo()
       * \brief   Virtual Destructor
       **************************************************************************/
      virtual ~spi_tclAudioSrcInfo();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudioSrcInfo::bIsSrcActive(const t_U8 cu8SrcNum)
       ***************************************************************************/
      /*!
       * \fn     t_Bool bIsSrcActive(const t_U8 cu8SrcNum);
       * \brief  Returns the active flag of an Audio Source
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \retval  Bool value
       **************************************************************************/
      t_Bool bIsSrcActive(const t_U8 cu8SrcNum);

      /***************************************************************************
       ** FUNCTION:  t_Bool bGetAudSrcInfo(const t_U8 cu8SrcNum);
       ***************************************************************************/
      /*!
       * \fn     const trAudSrcInfo& bGetAudSrcInfo(const t_U8 cu8SrcNum,
       *          trAudSrcInfo& rfrSrcInfo);
       * \brief  Returns the Source Information of an Audio Source
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \param   [OUT]rfrSrcInfo - Reference to trAudSrcInfo structure
       * \retval  t_Bool value
       **************************************************************************/
      t_Bool bGetAudSrcInfo(const t_U8 cu8SrcNum, trAudSrcInfo& rfrSrcInfo);

      /***************************************************************************
       ** FUNCTION:  tenDeviceCategory enGetDeviceCategory(const t_U8 cu8SrcNum);
       ***************************************************************************/
      /*!
       * \fn     tenDeviceCategory enGetDeviceCategory(const t_U8 cu8SrcNum);
       * \brief  Returns Device Category of an Audio Source
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \retval  const Reference to tenDeviceCategory enum
       **************************************************************************/
      tenDeviceCategory enGetDeviceCategory(const t_U8 cu8SrcNum);

      /***************************************************************************
       ** FUNCTION:  tenAudioDir enGetDeviceCategory(const t_U8 cu8SrcNum);
       ***************************************************************************/
      /*!
       * \fn     tenAudioDir enGetAudDirection(const t_U8 cu8SrcNum);
       * \brief  Returns Audio Direction of an Audio Source
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \retval  const Reference to tenAudioDir enum
       **************************************************************************/
      tenAudioDir enGetAudDirection(const t_U8 cu8SrcNum);

      /***************************************************************************
       ** FUNCTION:  tenAudioSamplingRate enGetAudSamplingRate(const t_U8 cu8SrcNum);
       ***************************************************************************/
      /*!
       * \fn     tenAudioDir enGetAudSamplingRate(const t_U8 cu8SrcNum);
       * \brief  Returns Audio sampling Rate of an Audio Source
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \retval  const Reference to tenAudioSamplingRate enum
       **************************************************************************/
      tenAudioSamplingRate enGetAudSamplingRate(const t_U8 cu8SrcNum);

      /***************************************************************************
       ** FUNCTION:  t_U32 u32GetDeviceID(const t_U8 cu8SrcNum);
       ***************************************************************************/
      /*!
       * \fn     t_U32 enGetAudDirection(const t_U8 cu8SrcNum);
       * \brief  Returns Device ID
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \retval  t_U32 Value - Device ID
       **************************************************************************/
      t_U32 u32GetDeviceID(const t_U8 cu8SrcNum);

      /***************************************************************************
       ** FUNCTION:  t_Void vSetDeviceInfo(const t_U32 cu32DeviceID,
       *             tenDeviceCategory enDeviceCat);
       ***************************************************************************/
      /*!
       * \fn     t_Void vSetDeviceInfo(const t_U32 cu32DeviceID,
       *          tenDeviceCategory enDeviceCat);
       * \brief   Sets Device Information into the map
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \param   [IN]u32DeviceID - Device Identifier
       * \param   [IN]enDeviceCat - Device Category Enum Value
       * \param   [IN]enAudDir    - Audio Direction Enum Value
       * \param   [IN]enAudSampleRate - Audio Sample Rate Enum Value
       * \retval  t_Void
       **************************************************************************/
      t_Void vSetDeviceInfo(const t_U8 cu8SrcNum, t_U32 u32DeviceID,
               tenDeviceCategory enDeviceCat, tenAudioDir enAudDir,
               tenAudioSamplingRate enAudSampleRate);

      /***************************************************************************
       ** FUNCTION:  t_Void vSetAudSrcInfo(trAudSrcInfo& rfrSrcInfo);
       ***************************************************************************/
      /*!
       * \fn     t_Void vSetAudSrcInfo(trAudSrcInfo& rfrSrcInfo);
       * \brief   Sets Audio Source Information retrieved from route allocation into the map
       * \param   [IN]cu8SrcNum  - Audio Source Number
       * \param   [IN]rfrSrcInfo - Reference to trAudSrcInfo structure
       * \retval  t_Void
       **************************************************************************/
      t_Void vSetAudSrcInfo(const t_U8 cu8SrcNum, trAudSrcInfo& rfrSrcInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void vSetActiveSrcFlag(const t_U8 cu8SrcNum, t_Bool bActiveSrcFlag);
       ***************************************************************************/
      /*!
       * \fn     t_Void vSetActiveSrcFlag(const t_U8 cu8SrcNum, t_Bool bActiveSrcFlag);
       * \brief   Sets/resets active source flag of an audio source into the map
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \param   [IN]bActiveSrcFlag - Set/Reset Flag
       * \retval  t_Void value
       **************************************************************************/
      t_Void vSetActiveSrcFlag(const t_U8 cu8SrcNum, t_Bool bActiveSrcFlag);
	  
      /***************************************************************************
       ** FUNCTION:  t_Void vSetSamplingRate(const t_U8 cu8SrcNum..
       ***************************************************************************/
      /*!
       * \fn     t_Void vSetSamplingRate
       * \brief   Sets sampling rate for particular source
       * \param   [IN]cu8SrcNum - Audio Source Number
       * \param   [IN]enAudioSamplingRate - sampling rate to be set
       * \retval  t_Void value
       **************************************************************************/
      t_Void vSetSamplingRate(const t_U8 cu8SrcNum, tenAudioSamplingRate enAudioSamplingRate);
	  
      /***************************************************************************
       ** FUNCTION:  t_Void vEraseAudSrcInfo(const t_U8 cu8SrcNum);
       ***************************************************************************/
      /*!
       * \fn     t_Void vEraseAudSrcInfo(const t_U8 cu8SrcNum);
       * \brief   Erases Audio Source Information identified by source number from the map
       * \param   [IN]cu8SrcNum  - Audio Source Number
       * \retval  t_Void
       **************************************************************************/
      t_Void vEraseAudSrcInfo(const t_U8 cu8SrcNum);

      /***************************************************************************
       ** FUNCTION:  t_Void vClearAudSrcInfo();
       ***************************************************************************/
      /*!
       * \fn     t_Void vClearAudSrcInfo();
       * \brief   Clears all Audio Source Information
       * \retval  t_Void
       **************************************************************************/
      t_Void vClearAudSrcInfo();

	  /***************************************************************************
       ** FUNCTION:  t_Void vClearDeviceInfo();
       ***************************************************************************/
      /*!
       * \fn     t_Void vClearDeviceInfo();
       * \brief   Clears all Device Information
       * \retval  t_Void
       **************************************************************************/
      t_Void vClearDeviceInfo();

      /***************************************************************************
       ** FUNCTION:  t_Bool bGetActiveSrc();
       ***************************************************************************/
      /*!
       * \fn     t_Bool bGetActiveSrc();
       * \brief   Gives the currently activated source
       * \retval  true if active source is found
       **************************************************************************/
      t_Bool bGetActiveSrc(t_U8 &rfu8ActiveSrc);

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/
      /***************************************************************************
       ** Map containing information about all sources
       ***************************************************************************/
      map<t_U8, trAudioSrcProperties> m_mapAudioSrcInfo;
};

#endif // SPI_TCLAUDIOSRCINFO_H
