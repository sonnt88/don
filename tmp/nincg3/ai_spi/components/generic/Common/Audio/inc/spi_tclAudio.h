/*!
 *******************************************************************************
 * \file          spi_tclAudio.h
 * \brief         Main Audio class that provides interface to delegate 
                  the execution of command and handle response
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Main Audio class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                         | Modifications
 28.10.2013 |  Hari Priya E R(RBEI/ECP2)      | Initial Version
 18.11.2013 |  Raghavendra S (RBEI/ECP2)      | Redefinition of Interfaces
 03.01.2014 |  Hari Priya E R(RBEI/ECP2)      | Included variant handling for main
 06.04.2014 |  Ramya Murthy                   | Initialisation sequence implementation
 10.06.2014 |  Ramya Murthy                   | Audio policy redesign implementation.
 31.07.2014 | Ramya Murthy                    | SPI feature configuration via LoadSettings()

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAUDIO_H
#define SPI_TCLAUDIO_H

#include "Lock.h"
#include "SPITypes.h"
#include "spi_tclLifeCycleIntf.h"
#include "spi_tclAudioSrcInfo.h"

#define NUM_AUD_CLIENTS 4

/**
 *  class definitions.
 */
class spi_tclAudioPolicyBase;
class spi_tclAudioDevBase;
class spi_tclAudioInBase;
namespace shl
{
   namespace thread
   {
      class FunctionThreader;
   }
}
//! describes the current audioin state
enum tenAudioInState
{
   e8_AUDIOIN_UNITIAILZED = 0,
   e8_AUDIOIN_INITIALIZED = 1,
   e8_AUDIOIN_STARTED = 2,
   e8_AUDIOIN_STOPPED = 3
};
/**
 * This is the main audio class that provides interface to delegate 
 * the execution of command and handle response.
 */
class spi_tclAudio: public spi_tclLifeCycleIntf
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAudio::spi_tclAudio(spi_tclAudioPolicyBase* poPolicyBase);
       ***************************************************************************/
      /*!
       * \fn      spi_tclAudio()
       * \brief   Default Constructor
       **************************************************************************/
      spi_tclAudio(spi_tclAudioPolicyBase* poPolicyBase);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudio::~spi_tclAudio();
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAudio()
       * \brief   Virtual Destructor
       **************************************************************************/
      virtual ~spi_tclAudio();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudio::bInitialize()
       ***************************************************************************/
      /*!
       * \fn      bInitialize()
       * \brief   Method to perform initialization related to Audio. Invoked on Startup.
       * \sa      bUnInitialize()
       **************************************************************************/
      virtual t_Bool bInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudio::bUnInitialize()
       ***************************************************************************/
      /*!
       * \fn      bUnInitialize()
       * \brief   Method to perform termination related to Audio. Invoked during Shutdown.
       * \sa      bInitialize()
       **************************************************************************/
      virtual t_Bool bUnInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vLoadSettings(const trSpiFeatureSupport&...)
       ***************************************************************************/
      /*!
       * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
       * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
       * \sa      vSaveSettings()
       **************************************************************************/
      virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclAudio::vSaveSettings()
       ***************************************************************************/
      /*!
       * \fn      vSaveSettings()
       * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
       * \sa      vLoadSettings()
       **************************************************************************/
      virtual t_Void vSaveSettings();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vSelectDevice(tU32,tenDeviceCategory)
       ***************************************************************************/
      /*!
       * \fn      vSelectDevice(t_U32 u32DeviceId, tenDeviceCategory eDevCat)
       * \brief   Method to enable/disable a device selection.
       * \param   [u32DeviceId]: Source Number corresponding to the Audio Source.
       *          [enDevConnReq]: Specifies the Selection request for Device
       *          [eDevCat]: Provides the Category
       * \retval  NONE
       **************************************************************************/
      t_Void vSelectDevice(t_U32 u32DeviceId, tenDeviceConnectionReq enDevConnReq,
                        tenDeviceCategory eDevCat) ;

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclAudio::vSelectDeviceResult(t_U32...)
      ***************************************************************************/
      /*!
      * \fn      vSelectDeviceResult(t_U32 u32DeviceId,
      *             tenDeviceConnectionReq enDevConnReq,
      *             tenResponseCode enRespCode, tenErrorCode enErrorCode)
      * \brief   Called when SelectDevice operation is complete & with the result
      *         of the operation.
      * \param   [IN] u32DeviceId: Unique handle of selected device
      * \param   [IN] enDeviceConnReq: Connection request type for the device
      * \param   [IN] enRespCode: Response code enumeration
      * \param   [IN] enErrorCode: Error code enumeration
      * \retval  None
      **************************************************************************/
      t_Void vSelectDeviceResult(t_U32 u32DeviceId,
            tenDeviceConnectionReq enDeviceConnReq,
            tenResponseCode enRespCode,
            tenErrorCode enErrorCode,
            tenDeviceCategory enDevCategory);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vLaunchAudio(tU32,tenDeviceCategory, tenAudioDir, ..)
       ***************************************************************************/
      /*!
       * \fn      vLaunchAudio(t_U32 u32DeviceId, tenDeviceCategory enDevCat)
       *                       tenAudioDir enAudDir, tenAudioSamplingRate enAudSampleRate)
       * \brief   Initiates the Playback of Audio for the Device specified.
       *          Selection of the device must be completed before Audio playback
       *          can be initiated.
       * \param   [u32DeviceId]: Source Number corresponding to the Audio Source.
       *          [eDevCat]: Provides the Category
       *          [enAudDir]: Specifies the Direction of Audio Stream from
       *          Application being launched.
       *          [enAudSampleRate]: Specifies the Audio Sample Rate.
       * \retval  NONE
       **************************************************************************/
      t_Void vLaunchAudio(t_U32 u32DeviceId, tenDeviceCategory enDevCat,
               tenAudioDir enAudDir = e8AUD_INVALID, tenAudioSamplingRate enAudSampleRate = e8AUD_SAMPLERATE_DEFAULT) ;

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vTerminateAudio(t_U32,tenAudioDir)
       ***************************************************************************/
      /*!
       * \fn      vTerminateAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
       * \brief   Trigger to end the Playback of Audio for the Device specified.
       *          Initiates the request to Audio Manager to Stop Audio streaming
       *          from the selected device on Audio Output device.
       * \param   [u32DeviceId]: Source Number corresponding to the Audio Source.
       *          [enAudDir]: Audio direction for which route is allocated
       * \retval  NONE
       **************************************************************************/
      t_Void vTerminateAudio(t_U32 u32DeviceId, tenAudioDir enAudDir);


      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vOnRouteAllocateResult(tU8, trAudSrcInfo&)
       ***************************************************************************/
      /*!
       * \fn      vOnRouteAllocateResult(tU8 u8SourceNum, trAudSrcInfo& rfrSrcInfo)
       * \brief   Notification from the Audio Manager on Completion of Route Alloc.
       *          Implement Source component specific actions on Allocation of Audio Route.
       *          Mandatory Interface to be implemented.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       *          [rfrSrcInfo]: Reference to structure containing details of the allocated source.
       * \retval  NONE
       **************************************************************************/
      virtual t_Bool bOnRouteAllocateResult(t_U8 u8SourceNum,
               trAudSrcInfo& rfrSrcInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vOnRouteDeAllocateResult(t_U8)
       ***************************************************************************/
      /*!
       * \fn      vOnRouteDeAllocateResult(t_U8 u8SourceNum)
       * \brief   Notification from the Audio Manager on Completion of Route Dealloc.
       *          Implement Source component specific actions on Deallocation of Audio Route.
       *          Mandatory Interface to be implemented.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vOnRouteDeAllocateResult(t_U8 u8SourceNum);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vOnStartSourceActivity(t_U8,t_Bool)
       ***************************************************************************/
      /*!
       * \fn      vOnStartSourceActivity(t_U8 u8SourceNum,t_Bool bResult)
       * \brief   Trigger from the Audio Manager to Start play of Audio from Device.
       *          Mandatory Interface to be implemented.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	   * \param   bResult      : Result
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vOnStartSourceActivity(t_U8 u8SourceNum,t_Bool bResult=true);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vOnStopSourceActivity(t_U8)
       ***************************************************************************/
      /*!
       * \fn      vOnStopSourceActivity(t_U8 u8SourceNum)
       * \brief   Trigger from the Audio Manager to Start play of Audio from Device
       *          Mandatory Interface to be implemented.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vOnStopSourceActivity(t_U8 u8SourceNum);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vOnNewMuteState(t_U8, t_Bool)
       ***************************************************************************/
      /*!
       * \fn      vOnNewMuteState(t_U8 u8SourceNum)
       * \brief   Notification from the Audio Manager on change in Mute State.
       *          Implement Source component specific actions on change in Mute State.
       *          Optional Interface to be implemented if required.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       *          [bMute]: TRUE if Source Mute is active, FALSE Otherwise
       * \retval  NONE
       **************************************************************************/
      virtual t_Void vOnNewMuteState(t_U8 u8SourceNum, t_Bool bMute)
      {
      }

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::bOnReqAVDeActivationResult
       ***************************************************************************/
      /*!
       * \fn      bOnReqAVDeActivationResult
       * \brief   Application specific function after which RequestAVDeAct
       *          result can be processed.
       * \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
       *          Source Number will be defined for Audio Source by the Audio Component.
       * \retval  true if the application processing is successful false otherwise
       **************************************************************************/
      virtual t_Bool bOnReqAVDeActivationResult(const t_U8 u8SourceNum);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudio::vOnAudioError()
       ***************************************************************************/
      /*!
       * \fn    t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError)
       * \brief  Interface to set the audio error.
       * \param  cou8SourceNum       : [IN] Source number of the audio source
       * \param  enAudioError : [IN] Audio Error
       **************************************************************************/
      virtual t_Void vOnAudioError(const t_U8 cou8SourceNum, tenAudioError enAudioError);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAudio::bGetAudSrcInfo(const t_U8 cu8SourceNum,
       **            trAudSrcInfo& rfrSrcInfo )
       ***************************************************************************/
      /*!
       * \fn      t_Bool bGetAudSrcInfo(const t_U8 cu8SourceNum, trAudSrcInfo& rfrSrcInfo)
       * \brief   Function To retrieve the Source Info from The Audio Policy
       * \param   [cu8SourceNum]: Source Number
       * \param   [rfrSrcInfo]: Reference to trAudSrcInfo structure
       * \retval  t_Bool Value
       **************************************************************************/
      t_Bool bGetAudSrcInfo(const t_U8 cu8SourceNum, trAudSrcInfo& rfrSrcInfo);


	   /***************************************************************************
	   ** FUNCTION: t_Bool spi_tclAudio::bSetAudioBlockingMode()
	   ***************************************************************************/
	   /*!
	   * \fn     t_Bool bSetAudioBlockingMode(const t_U32 cou32DevId,
	   *         const tenBlockingMode coenBlockingMode,
	   *         tenDeviceCategory enDevCat) const
	   * \brief  Interface to set the audio blocking mode.
	   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
	   * \param  coenBlockingMode : [IN] Identifies the Blocking Mode.
	   * \param  enDevCat         : [IN] device category
	   * \retval t_Bool
	   **************************************************************************/
	   t_Bool bSetAudioBlockingMode(const t_U32 cou32DevId,
	      const tenBlockingMode coenBlockingMode,
	      tenDeviceCategory enDevCat) const;

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclAudio::bSetAudioSrcAvailability()
      ***************************************************************************/
      /*!
      * \fn     t_Bool bSetAudioSrcAvailability
      * \brief  Interface to set the source availability.
      * \param  enAudioDir       : [IN] Direction for which source availability applies
      * \param  bAvail           : [IN] Indicates whether source is available or not
      * \retval t_Bool
      **************************************************************************/
	   t_Bool bSetAudioSrcAvailability(tenAudioDir enAudioDir, t_Bool bAvail);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclAudio::vSetServiceAvailable()
      ***************************************************************************/
      /*!
      * \fn     vSetServiceAvailable
      * \brief  Interface to set the source availability.
      * \param  bAvail           : [IN] Indicates whether source is available or not
      * \retval t_Bool
      **************************************************************************/
	   t_Void vSetServiceAvailable(t_Bool bAvail);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclAudio::bSetAudioDucking()
      ***************************************************************************/
      /*!
      * \fn     bSetAudioDucking
      * \brief  Interface to set audio ducking ON/OFF.
      * \param  cou16RampDuration: Ramp duration in milliseconds
      * \param  cou8VolumeLevelindB: Volume level in dB
      * \param  coenDuckingType: Ducking/ Unducking
      * \param  enDevCat : Device Category
      **************************************************************************/
	   t_Bool bSetAudioDucking(const t_U16 cou16RampDuration, const t_U8 cou8VolumeLevelindB,
            const tenDuckingType coenDuckingType,tenDeviceCategory enDevCat = e8DEV_TYPE_DIPO);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudio::vInitializeAudioIn()
       ***************************************************************************/
      /*!
       * \fn    t_Void vInitializeAudioIn
       * \brief  Interface to initialize audio in
       * \param  enAudDir       : [IN]Audio direction
       * \param  enSamplingRate : [IN] Sampling rate of the audio stream
       **************************************************************************/
	   t_Void vInitializeAudioIn(tenAudioDir enAudDir, tenAudioSamplingRate enSamplingRate);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudio::vSetAlsaDevice()
       ***************************************************************************/
      /*!
       * \fn    t_Void vSetAlsaDevice
       * \brief  Interface to set alsa device
       * \param  enAudDir       : [IN]Audio direction
       **************************************************************************/
	   t_Void vSetAlsaDevice(tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudio::vUninitializeAudioIn()
       ***************************************************************************/
      /*!
       * \fn    t_Void vUninitializeAudioIn
       * \brief  Interface to Uninitialize audio in
       * \param  enAudDir       : [IN]Audio direction
       **************************************************************************/
	   t_Void vUninitializeAudioIn(tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudio::vStartAudioIn()
       ***************************************************************************/
      /*!
       * \fn    t_Void vStartAudioIn
       * \brief  Interface to start audio in
       * \param  enAudDir       : [IN]Audio direction
       **************************************************************************/
	   t_Void vStartAudioIn(tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAudio::vStopAudioIn()
       ***************************************************************************/
      /*!
       * \fn    t_Void vStopAudioIn
       * \brief  Interface to stop audio in
       * \param  enAudDir       : [IN]Audio direction
       **************************************************************************/
	   t_Void vStopAudioIn(tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  spi_tclAudio::vSetAudioInConfig
       ***************************************************************************/
      /*!
       * \fn     vSetAudioInConfig(tenAudioDir enAudDir, tenAudioSamplingRate enSamplingRate)
       * \brief  Interface to change audio dataset configuration
       * \sa     spi_tclAudioInBase::vSetAudioConfig
       **************************************************************************/
      t_Void vSetAudioInConfig(tenAudioDir enAudDir, tenAudioSamplingRate enSamplingRate);

	   /***************************************************************************
	    ** FUNCTION:  t_Void spi_tclAudio::vSendAudioStatusChange(...)
	    ***************************************************************************/
	    /*!
	    * \fn      vSendAudioStatusChange(tenAudioStatus enAudioStatus)
	    * \brief   Interface to provide audio status change info
	    * \param   [enAudioStatus]: Current audio status
	    * \retval  t_Void
	    **************************************************************************/
	   t_Void vSendAudioStatusChange(tenAudioStatus enAudioStatus);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclAudio::vRestoreLastMediaAudSrc()
       ***************************************************************************/
      /*!
       * \fn     t_Void vRestoreLastMediaAudSrc
       * \brief  Interface to restore last stored audio source.
       * \param  NONE
       * \retval NONE
       **************************************************************************/
      t_Void vRestoreLastMediaAudSrc();

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vRegisterCallbacks()
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  Registers for callbacks to ML/DiPo Audio classes
       * \param  NONE
       * \retval NONE
       **************************************************************************/
      t_Void vRegisterCallbacks();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vCbSelectDeviceResp(tU16, t_Bool)
       ***************************************************************************/
      /*!
       * \fn      vCbSelectDeviceResp(tU16 u32DeviceId, t_Bool bResult)
       * \brief   Callback function invoked by ML Audio or DiPo Audio to send
       *          result back to Connection Manager for device selection/de-selection.
       * \param   [u32DeviceId]: Device ID of the selected device.
       * \param   [enDevConnReq]: Connection request (Select/Deselect).
       * \param   [bResult]: Indicates Success or failure of source activity ON
       *                     operation.
       * \retval  NONE
       **************************************************************************/
      t_Void vCbSelectDeviceResp(t_U32 u32DeviceId,
               tenDeviceConnectionReq enDevConnReq, t_Bool bResult);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vCbStartSrcActivity(tenAudioDir, t_Bool)
       ***************************************************************************/
      /*!
       * \fn      vCbStartSrcActivity(tenAudioDir enAudDir, t_Bool bResult)
       * \brief   Callback function invoked by ML Audio or DiPo Audio to send
       *          result back to ARL.
       * \param   [enAudDir]: Audio Direction for which route is allocated.
       * \param   [bResult]: Indicates Success or failure of source activity ON
       *                     operation.
       * \retval  NONE
       **************************************************************************/
      t_Void vCbStartSrcActivity(tenAudioDir enAudDir, t_Bool bResult);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAudio::vCbStopSrcActivity(tenAudioDir, t_Bool)
       ***************************************************************************/
      /*!
       * \fn      vCbStopSrcActivity(tenAudioDir enAudDir, t_Bool bResult)
       * \brief   Callback function invoked by ML Audio or DiPo Audio to send
       *          result back to ARL.
       * \param   [enAudDir]: Audio Direction for which route is allocated.
       * \param   [bResult]: Indicates Success or failure of source activity OFF
       *                     operation.
       * \retval  NONE
       **************************************************************************/
      t_Void vCbStopSrcActivity(tenAudioDir enAudDir, t_Bool bResult);

      //! Audio Policy class
      spi_tclAudioPolicyBase* m_poAudioPolicyBase;

      //! Member Variable - pointer to the Base class for ML Audio and DiPo Audio
      spi_tclAudioDevBase* m_poAudDevHandlers[NUM_AUD_CLIENTS];

      //! spi_tclAudioSrcInfo class Instance
      spi_tclAudioSrcInfo m_oAudioSrcInfo;

      //! Handler for AudioIn interface
      spi_tclAudioInBase* m_poAudioInHandler;

      //! Stores Audio sources
      t_U8 m_u8AudSources[e8AUD_INVALID];

      //! Pointer to GMainLoop thread
      shl::thread::FunctionThreader* m_poGMainLoopThread;

      //!Current audioin state
      tenAudioInState m_enAudioInState;

      //! Lock to protect critical section of ECNR calls
      Lock m_oAudioInLock;
};

#endif // SPI_TCLAUDIO_H
