/*!
 *******************************************************************************
 * \file             spi_tclAudioPolicyBase.cpp
 * \brief            Base Class for Audio Policy
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Implementation for Mirror Link
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 29.10.2013 |  Hari Priya E R(RBEI/ECP2)        | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioPolicyBase.cpp.trc.h"
#endif

#include "spi_tclAudioPolicyBase.h"
#include "spi_tclAudio.h"


/***************************************************************************
** FUNCTION:  spi_tclAudioPolicyBase::spi_tclAudioPolicyBase();
***************************************************************************/
spi_tclAudioPolicyBase::spi_tclAudioPolicyBase(spi_tclAudio* poAudio)
{
	ETG_TRACE_USR1(("spi_tclAudioPolicyBase::spi_tclAudioPolicyBase()"));
}

/***************************************************************************
** FUNCTION:  spi_tclAudioPolicyBase::~spi_tclAudioPolicyBase();
***************************************************************************/
spi_tclAudioPolicyBase::~spi_tclAudioPolicyBase()
{
	ETG_TRACE_USR1(("spi_tclAudioPolicyBase::~spi_tclAudioPolicyBase()"));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudioPolicyBase::bOnSrcActivityStart()
***************************************************************************/
t_Void spi_tclAudioPolicyBase::bOnSrcActivityStart()
{
	ETG_TRACE_USR1(("spi_tclAudioPolicyBase::bOnSrcActivityStart()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudioPolicyBase::bSrcActivityResult()
***************************************************************************/
t_Void spi_tclAudioPolicyBase::bSrcActivityResult()
{
	ETG_TRACE_USR1(("spi_tclAudioPolicyBase::bSrcActivityResult()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudioPolicyBase::bOnAllocateResult()
***************************************************************************/
t_Void spi_tclAudioPolicyBase::bOnAllocateResult()
{
	ETG_TRACE_USR1(("spi_tclAudioPolicyBase::bOnAllocateResult()"));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudioPolicyBase::bOnDeAllocateResult()
***************************************************************************/
t_Void spi_tclAudioPolicyBase::bOnDeAllocateResult()
{
	ETG_TRACE_USR1(("spi_tclAudioPolicyBase::bOnDeAllocateResult()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudioPolicyBase::vOnError()
 ***************************************************************************/
t_Void spi_tclAudioPolicyBase::vOnError()
{
	ETG_TRACE_USR1(("spi_tclAudioPolicyBase::bOnReqAVDeActResult()"));
}



