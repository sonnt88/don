/*!
 *******************************************************************************
 * \file             spi_tclAudioInBase.cpp
 * \brief            Base class for Audio Input Handling
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class for Audio Input Handling
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 21.03.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/
#include <pthread.h>
#include "ThreadNamer.h"
#include "spi_tclAudioInBase.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioInBase.cpp.trc.h"
#endif
#endif

//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::spi_tclAudioInBase
 ***************************************************************************/
spi_tclAudioInBase::spi_tclAudioInBase(): m_poMainLoop(NULL)
{
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::~spi_tclAudioInBase
 ***************************************************************************/
spi_tclAudioInBase::~spi_tclAudioInBase()
{
   //! Stop the Main loop
   g_main_loop_quit(m_poMainLoop);

   //! Release the memory of m_poMainLoop.
   g_main_loop_unref(m_poMainLoop);
   m_poMainLoop = NULL;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::vRegisterCallbacks
 ***************************************************************************/
t_Void spi_tclAudioInBase::vRegisterCallbacks(const trAudioInCbs &corfrAudioInCbs)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_rAudioInCbs = corfrAudioInCbs;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::vResultInitializeAudioInRes
 ***************************************************************************/
t_Void spi_tclAudioInBase::vResultInitializeAudioInRes(t_Bool bResult)
{
	/*lint -esym(40,fvIntializeAudioInCb) fvIntializeAudioInCb Undeclared identifier */
   if (NULL != m_rAudioInCbs.fvIntializeAudioInCb)
   {
      (m_rAudioInCbs.fvIntializeAudioInCb)(bResult);
   }//if (NULL != m_rAudioInCbs.fvIntializeAudioInCb)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::vResultUnitializeAudioInRes
 ***************************************************************************/
t_Void spi_tclAudioInBase::vResultUnitializeAudioInRes(t_Bool bResult)
{
	/*lint -esym(40,fvUninitializeAudioInCb) fvUninitializeAudioInCb Undeclared identifier */
   if (NULL != m_rAudioInCbs.fvUninitializeAudioInCb)
   {
      (m_rAudioInCbs.fvUninitializeAudioInCb)(bResult);
   }// if (NULL != m_rAudioInCbs.fvUninitializeAudioInCb)
}
/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::vResultStartAudioInRes
 ***************************************************************************/
t_Void spi_tclAudioInBase::vResultStartAudioInRes(t_Bool bResult)
{
	/*lint -esym(40,fvStartAudioInCb) fvStartAudioInCb Undeclared identifier */
   if (NULL != m_rAudioInCbs.fvStartAudioInCb)
   {
      (m_rAudioInCbs.fvStartAudioInCb)(bResult);
   }//if (NULL != m_rAudioInCbs.fvStartAudioInCb)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::vResultStopAudioInRes
 ***************************************************************************/
t_Void spi_tclAudioInBase::vResultStopAudioInRes(t_Bool bResult)
{
	/*lint -esym(40,fvStopAudioInCb)fvStopAudioInCb Undeclared identifier */
	
   if (NULL != m_rAudioInCbs.fvStopAudioInCb)
   {
      (m_rAudioInCbs.fvStopAudioInCb)(bResult);
   }//if (NULL != m_rAudioInCbs.fvStopAudioInCb)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::vResultSetAlsaDeviceRes
 ***************************************************************************/
t_Void spi_tclAudioInBase::vResultSetAlsaDeviceRes(t_Bool bResult)
{
	/*lint -esym(40,fvSetAlsaDeviceCb)fvSetAlsaDeviceCb Undeclared identifier */
   if (NULL != m_rAudioInCbs.fvSetAlsaDeviceCb)
   {
      (m_rAudioInCbs.fvSetAlsaDeviceCb)(bResult);
   }//if (NULL != m_rAudioInCbs.fvSetAlsaDeviceCb)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::vResultSetAudioConfigRes
 ***************************************************************************/
t_Void spi_tclAudioInBase::vResultSetAudioConfigRes(t_Bool bResult)
{
	/*lint -esym(40,fvSetAudioInConfigCb)fvSetAudioInConfigCb Undeclared identifier */
   if (NULL != m_rAudioInCbs.fvSetAudioInConfigCb)
   {
      (m_rAudioInCbs.fvSetAudioInConfigCb)(bResult);
   }//if (NULL != m_rAudioInCbs.fvSetAudioInConfigCb)
}


/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAudioInBase::vExecute
 ***************************************************************************/
t_Void spi_tclAudioInBase::vExecute()
{

   ETG_TRACE_USR1((" spi_tclAudioInBase::vExecute %d \n",pthread_self()));
   DEFINE_THREAD_NAME("AudioIn");
   //! This function is used to initialise the glib type system
   //! To be removed when using glib version >= 2.36 (Current version 2.35)
   // g_type_init();

   dbus_g_thread_init();

   vSubscribeForAudioIn();

   //! Create the main loop instance, uses default context
   m_poMainLoop = g_main_loop_new(NULL, FALSE);

   if (NULL != m_poMainLoop)
   {
      //!Start Main loop
      g_main_loop_run(m_poMainLoop);
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclAudioInBase::vStartMainLoop: g_main_loop_new failed \n"));
   }
}

//lint –restore
