/*!
 *******************************************************************************
 * \file          spi_tclAudioSrcInfo.cpp
 * \brief         Audio source class that provides interface to store and retrieve
 *                Source details
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Source Information class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 27.03.2014 |  Deepti Samant (RBEI/ECP2)               | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAudioSrcInfo.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioSrcInfo.cpp.trc.h"
#endif

/***************************************************************************
 ** FUNCTION:  spi_tclAudioSrcInfo::spi_tclAudioSrcInfo();
 ***************************************************************************/
spi_tclAudioSrcInfo::spi_tclAudioSrcInfo()

{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_mapAudioSrcInfo.clear();
}//End of spi_tclAudioSrcInfo::spi_tclAudioSrcInfo()

/***************************************************************************
 ** FUNCTION:  spi_tclAudioSrcInfo::~spi_tclAudioSrcInfo();
 ***************************************************************************/
spi_tclAudioSrcInfo::~spi_tclAudioSrcInfo()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_mapAudioSrcInfo.clear();
}//End of spi_tclAudioSrcInfo::~spi_tclAudioSrcInfo()

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAudioSrcInfo::bIsSrcActive(const t_U8 cu8SrcNum)
 ***************************************************************************/
t_Bool spi_tclAudioSrcInfo::bIsSrcActive(const t_U8 cu8SrcNum)
{
   t_Bool bIsActive = false;

   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      bIsActive = (itMapAudio->second).bActiveSrc;
   }//End of if (AudSrcInfoMapItr != m_mapAudioSrcInfo.end())

   ETG_TRACE_USR4(("spi_tclAudioSrcInfo::bIsSrcActive: cu8SrcNum = %d "
         "bActiveSrc flag %d \n", cu8SrcNum, bIsActive));
   return bIsActive;

}//End of t_Bool bIsSrcActive(const t_U8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAudioSrcInfo::bGetAudSrcInfo(const t_U8 cu8SrcNum,
 **            trAudSrcInfo& rfrSrcInfo)
 ***************************************************************************/
t_Bool spi_tclAudioSrcInfo::bGetAudSrcInfo(const t_U8 cu8SrcNum,
         trAudSrcInfo& rfrSrcInfo)
{
   ETG_TRACE_USR4(("spi_tclAudioSrcInfo::crfrGetAudSrcInfo: cu8SrcNum %d\n", cu8SrcNum));

   t_Bool bRetVal = false;

   AudSrcInfoMapItr itMapAudio = m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      rfrSrcInfo = (itMapAudio->second).rAudSrcInfo;
      bRetVal = true;
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())

   return bRetVal;

}//End of const trAudSrcInfo& crfrGetAudSrcInfo(const t_U8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:  tenDeviceCategory spi_tclAudioSrcInfo::enGetDeviceCategory
 **            (const t_U8 cu8SrcNum)
 ***************************************************************************/
tenDeviceCategory spi_tclAudioSrcInfo::enGetDeviceCategory(const t_U8 cu8SrcNum)
{
   tenDeviceCategory enDevCategory = e8DEV_TYPE_UNKNOWN;

   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      enDevCategory = (itMapAudio->second).enDeviceCategory;
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())

   ETG_TRACE_USR4(("spi_tclAudioSrcInfo::enGetDeviceCategory: cu8SrcNum %d "
         "enDevCategory = %d \n", cu8SrcNum, ETG_ENUM(DEVICE_CATEGORY,enDevCategory)));
   return enDevCategory;

}//End of tenDeviceCategory enGetDeviceCategory(const t_U8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:  tenAudioDir spi_tclAudioSrcInfo::enGetAudDirection
 **            (const t_U8 cu8SrcNum)
 ***************************************************************************/
tenAudioDir spi_tclAudioSrcInfo::enGetAudDirection(const t_U8 cu8SrcNum)
{
   tenAudioDir enAudDir = e8AUD_INVALID;

   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      enAudDir = (itMapAudio->second).enAudDir;
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())

   ETG_TRACE_USR4(("spi_tclAudioSrcInfo::enGetAudDirection: cu8SrcNum %d "
         "enAudDir = %d \n", cu8SrcNum, ETG_ENUM(AUDIO_DIRECTION,enAudDir)));
   return enAudDir;

}//End of tenAudioDir enGetDeviceCategory(const t_U8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:  tenAudioSamplingRate spi_tclAudioSrcInfo::enGetAudSamplingRate
 **            (const t_U8 cu8SrcNum)
 ***************************************************************************/
tenAudioSamplingRate spi_tclAudioSrcInfo::enGetAudSamplingRate(const t_U8 cu8SrcNum)
{
   tenAudioSamplingRate enAudSampleRate = e8AUD_SAMPLERATE_DEFAULT;

   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      enAudSampleRate = (itMapAudio->second).enAudSampleRate;
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())

   ETG_TRACE_USR4(("spi_tclAudioSrcInfo::enGetAudSamplingRate: cu8SrcNum %d "
         "enAudSampleRate = %d \n", cu8SrcNum, ETG_ENUM(AUDIO_SAMPLERATE, enAudSampleRate)));

   return enAudSampleRate;

}//End of tenAudioSamplingRate spi_tclAudioSrcInfo::enGetAudSamplingRate(const t_U8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclAudioSrcInfo::u32GetDeviceID(const t_U8 cu8SrcNum)
 ***************************************************************************/
t_U32 spi_tclAudioSrcInfo::u32GetDeviceID(const t_U8 cu8SrcNum)
{
   t_U32 u32DevID = 0;

   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      u32DevID = (itMapAudio->second).u32DeviceId;
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())

   ETG_TRACE_USR4(("spi_tclAudioSrcInfo::u32GetDeviceID: cu8SrcNum %d "
         "u32DevID = %d \n", cu8SrcNum, u32DevID));
   return u32DevID;
}//End of t_U32 u32GetDeviceID(const t_U8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:    t_Void spi_tclAudioSrcInfo::vSetDeviceInfo(const t_U8 cu8SrcNum,
 **      t_U32 u32DeviceID, tenDeviceCategory enDeviceCat, tenAudioDir enAudDir,
 **      tenAudioSamplingRate enAudSampleRate);
 ***************************************************************************/
t_Void spi_tclAudioSrcInfo::vSetDeviceInfo(const t_U8 cu8SrcNum,
         t_U32 u32DeviceID, tenDeviceCategory enDeviceCat, tenAudioDir enAudDir,
         tenAudioSamplingRate enAudSampleRate)
{
   ETG_TRACE_USR2(("spi_tclAudioSrcInfo::vSetDeviceInfo: cu8SrcNum %d enDeviceCat = %d"
            "enAudDir = %d \n", cu8SrcNum, ETG_ENUM(DEVICE_CATEGORY,enDeviceCat),
            ETG_ENUM(AUDIO_DIRECTION,enAudDir)));
   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      ETG_TRACE_USR4(("spi_tclAudioSrcInfo::vSetDeviceInfo: Src Info exists \n"));
      (itMapAudio->second).u32DeviceId = u32DeviceID;
      (itMapAudio->second).enDeviceCategory = enDeviceCat;
      (itMapAudio->second).enAudSampleRate = enAudSampleRate;
      (itMapAudio->second).enAudDir = enAudDir;
   }
   else
   {
      trAudioSrcProperties rAudSrcProperties;
      rAudSrcProperties.u32DeviceId = u32DeviceID;
      rAudSrcProperties.enDeviceCategory = enDeviceCat;
      rAudSrcProperties.enAudDir = enAudDir;
      rAudSrcProperties.enAudSampleRate = enAudSampleRate;
      m_mapAudioSrcInfo.insert(pair<t_U8, trAudioSrcProperties> (cu8SrcNum,
               rAudSrcProperties));
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())
}//End of t_Void vSetDeviceInfo(const t_U8 cu8SrcNum, ..)

/***************************************************************************
 ** FUNCTION:    t_Void spi_tclAudioSrcInfo::vSetAudSrcInfo(const t_U8 cu8SrcNum,
 **               trAudSrcInfo& rfrSrcInfo);
 ***************************************************************************/
t_Void spi_tclAudioSrcInfo::vSetAudSrcInfo(const t_U8 cu8SrcNum,
         trAudSrcInfo& rfrSrcInfo)
{
   ETG_TRACE_USR2(("spi_tclAudioSrcInfo::vSetAudSrcInfo: cu8SrcNum %d\n", cu8SrcNum));

   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      (itMapAudio->second).rAudSrcInfo = rfrSrcInfo;
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())
   else
   {
      trAudioSrcProperties rAudSrcProperties;
      rAudSrcProperties.rAudSrcInfo = rfrSrcInfo;
      m_mapAudioSrcInfo.insert(pair<t_U8, trAudioSrcProperties> (cu8SrcNum,
         rAudSrcProperties));
   }
}//End of t_Void vSetAudSrcInfo(const t_U8 cu8SrcNum, ..)

/***************************************************************************
 ** FUNCTION:    t_Void spi_tclAudioSrcInfo::vSetActiveSrcFlag
 **               (const t_U8 cu8SrcNum, bActiveSrcFlag);
 ***************************************************************************/
t_Void spi_tclAudioSrcInfo::vSetActiveSrcFlag(const t_U8 cu8SrcNum,
         t_Bool bActiveSrcFlag)
{
   ETG_TRACE_USR1((" vSetActiveSrcFlag: cu8SrcNum %d, bActiveSrcFlag = %d\n",
         cu8SrcNum, bActiveSrcFlag));

   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      (itMapAudio->second).bActiveSrc = bActiveSrcFlag;
      ETG_TRACE_USR4((" vSetActiveSrcFlag  bActiveSrc %d \n",
                        (itMapAudio->second).bActiveSrc));
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())
}//End of t_Void vSetActiveSrcFlag(const t_U8 cu8SrcNum, bActiveSrcFlag)

/***************************************************************************
 ** FUNCTION:  t_Void vSetSamplingRate(const t_U8 cu8SrcNum..
 ***************************************************************************/
t_Void spi_tclAudioSrcInfo::vSetSamplingRate(const t_U8 cu8SrcNum, tenAudioSamplingRate enAudioSamplingRate)
{
   AudSrcInfoMapItr itMapAudio = m_mapAudioSrcInfo.find(cu8SrcNum);
   if (itMapAudio != m_mapAudioSrcInfo.end())
   {
      (itMapAudio->second).enAudSampleRate = enAudioSamplingRate;
      ETG_TRACE_USR4((" vSetSamplingRate  enAudioSamplingRate %d \n", (itMapAudio->second).enAudSampleRate));
   }//End of if (itMapAudio != m_mapAudioSrcInfo.end())
}

/***************************************************************************
 ** FUNCTION:  t_Void vEraseAudSrcInfo(const t_U8 cu8SrcNum);
 ***************************************************************************/
t_Void spi_tclAudioSrcInfo::vEraseAudSrcInfo(const t_U8 cu8SrcNum)
{
   ETG_TRACE_USR1((" vEraseAudSrcInfo: cu8SrcNum %d\n", cu8SrcNum));
   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.find(cu8SrcNum);
   if(m_mapAudioSrcInfo.end() != itMapAudio)
   {
      m_mapAudioSrcInfo.erase(itMapAudio);
   }// End of if(m_mapAudioSrcInfo.end() != itMapAudio)
}//End of t_Void vEraseAudSrcInfo(const t_U8 cu8SrcNum)

/***************************************************************************
 ** FUNCTION:  t_Void vClearAudSrcInfo();
 ***************************************************************************/
t_Void spi_tclAudioSrcInfo::vClearAudSrcInfo()
{
   ETG_TRACE_USR1((" vClearAudSrcInfo: Entered\n"));
   m_mapAudioSrcInfo.clear();
}

/***************************************************************************
 ** FUNCTION:  t_Void vClearDeviceInfo();
 ***************************************************************************/
t_Void spi_tclAudioSrcInfo::vClearDeviceInfo()
{
   ETG_TRACE_USR1((" vClearDeviceInfo: Entered"));
   AudSrcInfoMapItr itMapAudio= m_mapAudioSrcInfo.begin();

   while (itMapAudio != m_mapAudioSrcInfo.end())
   {
      itMapAudio->second.u32DeviceId = 0;
      itMapAudio->second.enDeviceCategory = e8DEV_TYPE_UNKNOWN;
      itMapAudio++;
   }//while (itMapAudio != m_mapAudioSrcInfo.end())
}

/***************************************************************************
 ** FUNCTION:  t_Void bGetActiveSrc();
 ***************************************************************************/
t_Bool spi_tclAudioSrcInfo::bGetActiveSrc(t_U8 &rfu8ActiveSrc)
{
   t_Bool bRetval = false;
   for (AudSrcInfoMapItr itMapSrcInfo = m_mapAudioSrcInfo.begin(); itMapSrcInfo
            != m_mapAudioSrcInfo.end(); itMapSrcInfo++)
   {
      if (true == itMapSrcInfo->second.bActiveSrc)
      {
         bRetval = true;
         rfu8ActiveSrc = itMapSrcInfo->first;
         break;
      }
   }
   ETG_TRACE_USR1(("spi_tclAudioSrcInfo bGetActiveSrc: rfu8ActiveSrc = %d bRetval = %d \n",
            rfu8ActiveSrc, ETG_ENUM(BOOL, bRetval)));
   return bRetval;
}
