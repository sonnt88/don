/*!
 *******************************************************************************
 * \file         spi_tclAudioDevBase.cpp
 * \brief        Main Audio class that provides interface to delegate 
                 the execution of command and handle response
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base class for ML and DiPO Interfaces
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 28.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#define VNC_USE_STDINT_H

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioDevBase.cpp.trc.h"
#endif

#include "spi_tclAudioDevBase.h"


/***************************************************************************
** FUNCTION:  spi_tclAudioDevBase::spi_tclAudioDevBase();
***************************************************************************/
spi_tclAudioDevBase::spi_tclAudioDevBase()
{
	ETG_TRACE_USR1(("spi_tclAudio::spi_tclAudio()"));
}

/***************************************************************************
** FUNCTION:  spi_tclAudioDevBase::~spi_tclAudioDevBase();
***************************************************************************/
spi_tclAudioDevBase::~spi_tclAudioDevBase()
{
	ETG_TRACE_USR1(("spi_tclAudio::~spi_tclAudio()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudioDevBase::vInitAudio(t_U32 u32DeviceId,
fp fpInitAudioResp)
***************************************************************************/
t_Void spi_tclAudioDevBase::vInitAudio(t_U32 u32DeviceId,fp fpInitAudioResp)
{
	ETG_TRACE_USR1(("spi_tclAudio::vInitAudio()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudioDevBase::vPrepareStartAudioStreaming
                                           (t_Char* pcDeviceName)
***************************************************************************/
t_Void spi_tclAudioDevBase::vPrepareStartAudioStreaming(t_Char* pcDeviceName)
{
	ETG_TRACE_USR1(("spi_tclAudio::vPrepareStartAudioStreaming()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudioDevBase::vStartAudioStreaming(
fp fpStartAudioStreamingResp)
***************************************************************************/
t_Void spi_tclAudioDevBase::vStartAudioStreaming(fp fpStartAudioStreamingResp)
{
	ETG_TRACE_USR1(("spi_tclAudio::vStartAudioStreaming()"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudioDevBase::vvPrepareStopAudioStreaming
                                           (t_Char* pcDeviceName)
***************************************************************************/
t_Void spi_tclAudioDevBase::vPrepareStopAudioStreaming(t_Char* pcDeviceName)
{
	ETG_TRACE_USR1(("spi_tclAudio::vPrepareStopAudioStreaming()"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAudioDevBase::vStopAudioStreaming()
***************************************************************************/
t_Void spi_tclAudioDevBase::vStopAudioStreaming()
{
	ETG_TRACE_USR1(("spi_tclAudio::vStopAudioStreaming()"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAudioDevBase::vDeInitAudio(fp fpDeInitAudioResp)
***************************************************************************/
t_Void spi_tclAudioDevBase::vDeInitAudio(fp fpDeInitAudioResp)
{
	ETG_TRACE_USR1(("spi_tclAudio::vDeInitAudio()"));
}




