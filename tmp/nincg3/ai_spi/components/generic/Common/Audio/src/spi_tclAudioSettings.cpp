/*!
 *******************************************************************************
 * \file             spi_tclAudioSettings.cpp
 * \brief            Settings class provides the interface to
 retrieve the project policy configurations
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Settings Audio class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 28.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version
 21.11.2013 |  Raghavendra S (RBEI/ECP2)               | Implementation of redefined
                                                        Audio Settings Interface
 27/01/2014 | Vinoop (RBEI/ECP2)                       | Implemented parser for extracting
                                                        Audio settings from xml file
 23.10.2014 | Hari Priya E R(RBEI/ECP2)                | Included policy value to 
                                                        enable/disable audio blocking
 14.08.2015 | Shiva Kumar Gurija                       | Extensions for ML Dynamic Audio
 24.02.2016 | Ramya Murthy                             | Included default audio type for CarPlay

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "SPITypes.h"
#include "spi_tclAudioSettings.h"
#include "FileHandler.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioSettings.cpp.trc.h"
#endif

using namespace std;
#ifdef VARIANT_S_FTR_ENABLE_GM
static const t_Char cosGMXmlConfigFile[] = "/opt/bosch/gm/policy.xml";
#else
static const t_Char cosG3GXmlConfigFile[] = "/opt/bosch/spi/xml/policy.xml";
#endif

/***************************************************************************
 ** FUNCTION:  spi_tclAudioSettings::spi_tclAudioSettings();
 ***************************************************************************/
spi_tclAudioSettings::spi_tclAudioSettings() :m_RTPPayload(0),
m_AudioIPL(0),m_bAudBlocking_AppCat(false),m_bAudBlocking_GlobalMute(false),m_bEnableMLDynAudio(false),
m_EnableMediaStreamRec(false), m_EnableGuidanceStreamRec(false)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   for (t_U8 u8AudSources = 0; u8AudSources < e8AUD_INVALID; u8AudSources++)
   {
      m_au8Sources[u8AudSources] = 0;
   }
   vReadAudioSettings();

}//spi_tclAudioSettings::spi_tclAudioSettings()

/***************************************************************************
 ** FUNCTION:  spi_tclVideoSettings::vReadAudioSettings()
 ***************************************************************************/
void spi_tclAudioSettings::vReadAudioSettings()
{
   t_Char* szConfigFilePath = NULL;
#ifdef VARIANT_S_FTR_ENABLE_GM
   szConfigFilePath = const_cast<t_Char*>(cosGMXmlConfigFile);
#else
   szConfigFilePath = const_cast<t_Char*> (cosG3GXmlConfigFile);
#endif

   spi::io::FileHandler oPolicySettingsFile(szConfigFilePath,
            spi::io::SPI_EN_RDONLY);
   if (true == oPolicySettingsFile.bIsValid())
   {
      tclXmlDocument *poXmlDoc = new tclXmlDocument(szConfigFilePath);
      SPI_NORMAL_ASSERT(NULL == poXmlDoc);

      tclXmlReader* poXmlReader = new tclXmlReader(poXmlDoc, this);
      SPI_NORMAL_ASSERT(NULL == poXmlReader);
      if (NULL != poXmlReader)
      {
         poXmlReader->bRead("AUDIO");
      }
      RELEASE_MEM(poXmlReader);
      RELEASE_MEM(poXmlDoc);
   }
}
//spi_tclVideoSettings::vReadAudioSettings()

/***************************************************************************
 ** FUNCTION:  spi_tclAudioSettings::~spi_tclAudioSettings();
 ***************************************************************************/
spi_tclAudioSettings::~spi_tclAudioSettings()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}//spi_tclAudioSettings::~spi_tclAudioSettings()

/***************************************************************************
 ** FUNCTION: t_U8 spi_tclAudioSettings::u8GetSourceNumber(tenAudioDir enAudDir)const
 ***************************************************************************/
t_U8 spi_tclAudioSettings::u8GetSourceNumber(tenAudioDir enAudDir) const
{
   t_U8 u8SrcNum = 0;

   if (e8AUD_INVALID > enAudDir)
   {
      u8SrcNum = m_au8Sources[enAudDir];
   }

   return u8SrcNum;

} //t_U8 spi_tclAudioSettings::u8GetSourceNumber(tenAudioDir enAudDir)const

/***************************************************************************
 ** FUNCTION: t_Void vGetRTPPayloadType(tU32& rfu32RTPPayload)
 ***************************************************************************/
t_Void spi_tclAudioSettings::vGetRTPPayloadType(t_U32& rfu32RTPPayload) const
{
   ETG_TRACE_USR1((" spi_tclAudioSettings::bGetRTPPayloadType %d \n", m_RTPPayload));
   rfu32RTPPayload = m_RTPPayload;
}//t_Bool vGetRTPPayloadType(tU32& rfu32RTPPayload)

/***************************************************************************
 ** FUNCTION: t_Void vGetRTPPayloadType(tU32& rfu32RTPPayload)
 ***************************************************************************/
t_Void spi_tclAudioSettings::vGetAudioIPL(t_U32& rfu32AudioIPL) const
{
   ETG_TRACE_USR1((" spi_tclAudioSettings::bGetAudioIPL %d \n", m_AudioIPL));
   rfu32AudioIPL = m_AudioIPL;
}//t_Bool vGetAudioIPL(tU32& rfu32AudioIPL)

/***************************************************************************
 ** FUNCTION: t_Bool bGetAudioBlockingTrigger()
 ***************************************************************************/
t_Bool spi_tclAudioSettings::bGetAudBlockTriggerBasedOnCat() const
{
   ETG_TRACE_USR1((" spi_tclAudioSettings::bGetAudBlockTriggerBasedOnCat: Enabled- %d ",
      ETG_ENUM(BOOL,m_bAudBlocking_AppCat)));

   return m_bAudBlocking_AppCat;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudioSettings::bGetAudBlockTriggerBasedOnGlobaleMute()
***************************************************************************/
t_Bool spi_tclAudioSettings::bGetAudBlockTriggerBasedOnGlobaleMute() const
{
   ETG_TRACE_USR1((" spi_tclAudioSettings::bGetAudBlockTriggerBasedOnGlobalMute: Enabled- %d ",
      ETG_ENUM(BOOL,m_bAudBlocking_GlobalMute)));

   return m_bAudBlocking_GlobalMute;
}

/***************************************************************************
 ** FUNCTION: t_Bool vGetAudioSettingsData()
 ***************************************************************************/
t_Void spi_tclAudioSettings::vDisplayAudioSettings()
{
   for (t_U8 u8AudSources = 0; u8AudSources < e8AUD_INVALID; u8AudSources++)
   {
      ETG_TRACE_USR4(("Source Number  = %d ",m_au8Sources[u8AudSources]));
   }

   ETG_TRACE_USR4((" RTP Payload = %d",m_RTPPayload));
   ETG_TRACE_USR4((" Audio IPL = %d ",m_AudioIPL));
   ETG_TRACE_USR4((" Audio Blocking Based On AppCat Enabled = %d ",ETG_ENUM(BOOL,m_bAudBlocking_AppCat)));
   ETG_TRACE_USR4((" Audio Blocking Based On Global Mute Enabled = %d ",ETG_ENUM(BOOL,m_bAudBlocking_GlobalMute)));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclAudioSettings::vGetAudioPipeConfig()
 ***************************************************************************/
t_Void spi_tclAudioSettings::vGetAudioPipeConfig(tmapAudioPipeConfig &rfmapAudioPipeConfig)
{
   ETG_TRACE_USR1((" spi_tclAudioSettings::vGetAudioPipeConfig  \n"));
   rfmapAudioPipeConfig = m_mapszAudioPipeConfig;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudioSettings::bGetMLDynAudioSupport()
***************************************************************************/
t_Bool spi_tclAudioSettings::bGetMLDynAudioSupport() const
{
   //Add code
   return m_bEnableMLDynAudio;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudioSettings::bGetAAPMediaStreamRecEnabled()
***************************************************************************/
t_Bool spi_tclAudioSettings::bGetAAPMediaStreamRecEnabled() const
{
   //Add code
   return m_EnableMediaStreamRec;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudioSettings::bGetAAPGuidanceStreamRecEnabled()
***************************************************************************/
t_Bool spi_tclAudioSettings::bGetAAPGuidanceStreamRecEnabled() const
{
   //Add code
   return m_EnableGuidanceStreamRec;
}

/*************************************************************************
 ** FUNCTION:  virtual bXmlReadNode(xmlNode *poNode)
 *************************************************************************/
t_Bool spi_tclAudioSettings::bXmlReadNode(xmlNodePtr poNode)
{
   t_String szattribName;
   t_S32 u32iValue = 0;
   t_Bool bRetVal = false;
   t_String szNodeName;
   if (NULL != poNode)
   {
      szNodeName = (const char *) (poNode->name);
   }//if (NULL != poNode)

   if ("SOURCE_NUM" == szNodeName)
   {
      //get entertain source number
      szattribName = "SRC_ENT";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_ENT  = %d\n",u32iValue));
      m_au8Sources[e8AUD_MAIN_OUT] = u32iValue;

      //get Phone source number
      szattribName = "SRC_PHONE";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_PHONE  = %d\n",u32iValue));
      m_au8Sources[e8AUD_PHONE_IN] = u32iValue;

      //get voice rec source number
      szattribName = "SRC_VR";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_VR  = %d\n",u32iValue));
      m_au8Sources[e8AUD_VR_IN] = u32iValue;

      //get MIX with out duck source number
      szattribName = "SRC_MIX";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_MIX  = %d\n",u32iValue));
      m_au8Sources[e8AUD_MIX_OUT] = u32iValue;

      //get ALERT source number
      szattribName = "SRC_ALERT";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_ALERT  = %d\n",u32iValue));
      m_au8Sources[e8AUD_ALERT_OUT] = u32iValue;


      //get  MIX with Duck number
      szattribName = "SRC_DUCK";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_DUCK  = %d\n",u32iValue));
      m_au8Sources[e8AUD_DUCK] = u32iValue;


      //get  temporary number
      szattribName = "SRC_TRANSIENT";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_TRANSIENT  = %d ",u32iValue));
      m_au8Sources[e8AUD_TRANSIENT] = u32iValue;


      //get  ML stereo source number
      szattribName = "SRC_STEREO_MIX";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_STEREO_MIX  = %d ",u32iValue));
      m_au8Sources[e8AUD_STEREO_MIX_OUT] = u32iValue;

      //get default source number
      szattribName = "SRC_DEFAULT";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4(("SRC_DEFAULT  = %d\n",u32iValue));
      m_au8Sources[e8AUD_DEFAULT] = u32iValue;

   }//if ("SOURCE_NUM" == szNodeName)

   else if ("RTP_PAYLOAD" == szNodeName)
   {
      //get the RTP Payload types supported details
      szattribName = "PAYLOAD";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4((" RTP Payload = %d\n",u32iValue));
      m_RTPPayload = u32iValue;

   }//else if ("RTP_PAYLOAD" == szNodeName)
   else if ("AUDIO_IPL" == szNodeName)
   {
      //get the Initial playback latency 
      szattribName = "IPL";
      bRetVal = bGetAttribute(szattribName, poNode, u32iValue);
      ETG_TRACE_USR4((" Audio IPL = %d\n",u32iValue));
      m_AudioIPL = u32iValue;
   }//else if ("AUDIO_IPL" == szNodeName)
   else if ("AUDIO_BLOCKING_CAT" == szNodeName)
   {
      //get whether teh audio blocking needs to enabled, based on app category
      szattribName = "BOOL";
      bRetVal = bGetAttribute(szattribName, poNode, m_bAudBlocking_AppCat);
      ETG_TRACE_USR4((" Audio Blocking Based On App Category = %d\n",ETG_ENUM(BOOL,m_bAudBlocking_AppCat)));
   }//else if ("AUDIO_BLOCKING_CAT" == szNodeName)
   else if("AUDIO_BLOCKING_GLOBAL_MUTE" == szNodeName)
   {
      //get whether teh audio blocking needs to enabled, based on global mute trigger
      szattribName = "BOOL";
      bRetVal = bGetAttribute(szattribName, poNode, m_bAudBlocking_GlobalMute);
      ETG_TRACE_USR4((" Audio Blocking Based On Global Mute Enabled = %d",ETG_ENUM(BOOL,m_bAudBlocking_GlobalMute)));
   }//else if("AUDIO_BLOCKING_GLOBAL_MUTE" == szNodeName)
   else if ("AUDIO_CONFIGURATION" == szNodeName)
   {
      //get the audio configurations
      szattribName = "MAIN_AUDIO_24kHz";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_24kHz]);
      ETG_TRACE_USR4((" Audio configuration  MAIN_AUDIO_24kHz = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_24kHz].c_str()));
      szattribName = "MAIN_AUDIO_16kHz";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_16kHz]);
      ETG_TRACE_USR4((" Audio configuration  MAIN_AUDIO_16kHz = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_16kHz].c_str()));
      szattribName = "MAIN_AUDIO_8kHz";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_8kHz]);
      ETG_TRACE_USR4((" Audio configuration  MAIN_AUDIO_8kHz = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_8kHz].c_str()));
      szattribName = "MAIN_AUDIO_ALERT";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_ALERT]);
      ETG_TRACE_USR4((" Audio configuration  MAIN_AUDIO_ALERT = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_ALERT].c_str()));
      szattribName = "MAIN_AUDIO_MEDIA_BROWSING";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_MEDIA_BROWSING]);
      ETG_TRACE_USR4((" Audio configuration  MAIN_AUDIO_MEDIA_BROWSING = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_MEDIA_BROWSING].c_str()));
      szattribName = "MAIN_AUDIO_MEDIA_STANDALONE";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_MEDIA_STANDALONE]);
      ETG_TRACE_USR4((" Audio configuration  MAIN_AUDIO_MEDIA_STANDALONE = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_MEDIA_STANDALONE].c_str()));
      szattribName = "MAIN_AUDIO_SPEECH";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_SPEECH]);
      ETG_TRACE_USR4((" Audio configuration  MAIN_AUDIO_SPEECH = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_SPEECH].c_str()));
      szattribName = "MAIN_AUDIO_DUMMY";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_DUMMY]);
      ETG_TRACE_USR4((" Audio configuration  MAIN_AUDIO_DUMMY = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_DUMMY].c_str()));
      szattribName = "ALTERNATE_AUDIO";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_ALTERNATEAUDIO]);
      ETG_TRACE_USR4((" Audio configuration  ALTERNATE_AUDIO = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_ALTERNATEAUDIO].c_str()));
      szattribName = "AUDIO_IN_24kHz";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_AUDIOIN_24kHz]);
      ETG_TRACE_USR4((" Audio configuration  AUDIO_IN_24kHz = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_AUDIOIN_24kHz].c_str()));
      szattribName = "AUDIO_IN_16kHz";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_AUDIOIN_16kHz]);
      ETG_TRACE_USR4((" Audio configuration  AUDIO_IN_16kHz = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_AUDIOIN_16kHz].c_str()));
      szattribName = "AUDIO_IN_8kHz";
      bRetVal = bGetAttribute(szattribName, poNode, m_mapszAudioPipeConfig[e8AUDIOCONFIG_AUDIOIN_8kHz]);
      ETG_TRACE_USR4((" Audio configuration  AUDIO_IN_8kHz = %s\n", m_mapszAudioPipeConfig[e8AUDIOCONFIG_AUDIOIN_8kHz].c_str()));
   }//else if ("AUDIO_CONFIGURATION" == szNodeName)
   else if("ML_DYN_AUDIO" == szNodeName)
   {
      //get whether the dynamic audio source swicth for ML is enabled
      szattribName = "BOOL";
      bRetVal = bGetAttribute(szattribName, poNode, m_bEnableMLDynAudio);
      ETG_TRACE_USR4((" ML Dynamic Audio Enabled - %d",ETG_ENUM(BOOL,m_bEnableMLDynAudio)));
   }//else if("ML_DYN_AUDIO" == szNodeName)
   else if("AAP_AUD_ENDPOINT_REC" == szNodeName)
   {
      szattribName = "MEDIA_STREAM";
      bRetVal = bGetAttribute(szattribName, poNode, m_EnableMediaStreamRec);
      ETG_TRACE_USR4((" AAP Media Stream Recording Enabled - %d",ETG_ENUM(BOOL,m_EnableMediaStreamRec)));

      szattribName = "GUIDANCE_STREAM";
      bRetVal = bGetAttribute(szattribName, poNode, m_EnableGuidanceStreamRec);
      ETG_TRACE_USR4((" AAP Guidance Stream Recording Enabled - %d",ETG_ENUM(BOOL,m_EnableGuidanceStreamRec)));
   }//else if("AAP_AUD_ENDPOINT_REC" == szNodeName)
   return bRetVal;
}
