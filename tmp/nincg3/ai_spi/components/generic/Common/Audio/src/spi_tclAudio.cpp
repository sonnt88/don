/*!
 *******************************************************************************
 * \file             spi_tclAudio.cpp
 * \brief            Main Audio class that provides interface to delegate 
                     the execution of command and handle response
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Main Audio class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                         | Modifications
 28.10.2013 |  Hari Priya E R(RBEI/ECP2)      | Initial Version
 18.11.2013 |  Raghavendra S (RBEI/ECP2)      | Redefinition of Interfaces
 03.01.2014 |  Hari Priya E R(RBEI/ECP2)      | Included variant handling for main
 06.04.2014 |  Ramya Murthy                   | Initialisation sequence implementation
 10.06.2014 |  Ramya Murthy                   | Audio policy redesign implementation.
 31.07.2014 | Ramya Murthy                    | SPI feature configuration via LoadSettings()
 06.05.2015 | Tejaswini H B                    | Lint Errors and warning fixes
 25.06.2015 | Tejaswini HB (RBEI/ECP2)        |Featuring out Android Auto
 07.07.2015 | Ramya Murthy                    | Fix for deallocating maun audio source on device disconnection,
                                                during active phone call or speech.
 18.08.2015 | Shiva Kumar Gurija              | Added elements for ML Dynamic Audio
 24.02.2016 |  Ramya Murthy                   | Implementation of default audio type for CarPlay

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "spi_tclMediator.h"
#include "spi_tclAudioDevBase.h"
#include "spi_tclAudioIn.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
#include "spi_tclMLAudio.h"
#endif
#include "spi_tclDiPoAudio.h"
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
#include "spi_tclAAPAudio.h"
#endif
#include "spi_tclAudioSettings.h"
#include "spi_tclAudioPolicyBase.h"
#include "spi_tclAudio.h"
#include "FunctionThreader.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudio.cpp.trc.h"
#endif

#define AUD_MAX_SOURCES e8AUD_INVALID
#define AUD_MAX_SAMPLERATE e8AUD_SAMPLERATE_MAX

const t_String gszAudDeviceName[AUD_MAX_SOURCES][AUD_MAX_SAMPLERATE] =
                                    {
                                    {"AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out"},                                   //MAIN_OUT
                                    {"AdevAudioInfoMonoOut", "AdevAudioInfoMonoOut","AdevAudioInfoMonoOut","AdevAudioInfoMonoOut"}, //MIX_OUT
                                    {"AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out"},                                   //ALERT_OUT
                                    {"AdevSPIInOut", "AdevSPIInOut_8", "AdevSPIInOut_16", "AdevSPIInOut"},                          //PHONE_IN
                                    {"AdevSPIInOut", "AdevSPIInOut_8", "AdevSPIInOut_16", "AdevSPIInOut"},                          //VR_IN
                                    {"AdevAudioInfoMonoOut", "AdevAudioInfoMonoOut","AdevAudioInfoMonoOut","AdevAudioInfoMonoOut"}, //DUCK
                                    {"AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out"},                                   // SPI_TRANSIENT
                                    {"AdevSPIInfo", "AdevSPIInfo", "AdevSPIInfo", "AdevSPIInfo"},                                    // SPI_STEREO MIX
                                    {"AdevSPIInOut", "AdevSPIInOut_8", "AdevSPIInOut_16", "AdevSPIInOut"}                          //DEFAULT
                                    };

const t_String gszECNRAudDeviceName[AUD_MAX_SOURCES][AUD_MAX_SAMPLERATE] =
                                    {
                                    {"AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out"},                                    //MAIN_OUT
                                    {"AdevAudioInfoMonoOut", "AdevAudioInfoMonoOut","AdevAudioInfoMonoOut","AdevAudioInfoMonoOut"},  //MIX_OUT
                                    {"AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out"},                                    //ALERT_OUT
                                    {"AdevECNRInOut", "AdevECNRInOut_8", "AdevECNRInOut_16", "AdevECNRInOut"},                       //PHONE_IN
                                    {"AdevECNRInOut", "AdevECNRInOut_8", "AdevECNRInOut_16", "AdevECNRInOut"},                       //VR_IN
                                    {"AdevAudioInfoMonoOut", "AdevAudioInfoMonoOut","AdevAudioInfoMonoOut","AdevAudioInfoMonoOut"},  //DUCK
                                    {"AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out", "AdevEnt1Out"},                                    // SPI_TRANSIENT
                                    {"AdevSPIInfo", "AdevSPIInfo", "AdevSPIInfo", "AdevSPIInfo"},                                     // SPI_STEREO MIX
                                    {"AdevECNRInOut", "AdevECNRInOut_8", "AdevECNRInOut_16", "AdevECNRInOut"}                       //DEFAULT
                                    };

#define AUD_ECNR_MIC_OUT 2
#define AUD_ECNR_RECV_IN 3
#define AUD_ECNR_RECV_OUT 4

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 ** FUNCTION:  spi_tclAudio::spi_tclAudio();
 ***************************************************************************/
spi_tclAudio::spi_tclAudio(spi_tclAudioPolicyBase* poPolicyBase) :
   m_poAudioPolicyBase(poPolicyBase),
   m_poAudioInHandler(NULL), m_poGMainLoopThread(NULL), m_enAudioInState(e8_AUDIOIN_UNITIAILZED)
{
   ETG_TRACE_USR1(("spi_tclAudio::spi_tclAudio entered \n"));
   SPI_NORMAL_ASSERT(NULL == m_poAudioPolicyBase);

   for (t_U8 u8AudClients = 0; u8AudClients < NUM_AUD_CLIENTS; u8AudClients++)
   {
      m_poAudDevHandlers[u8AudClients] = NULL;
   }

   for (t_U8 u8AudSources = 0; u8AudSources < e8AUD_INVALID; u8AudSources++)
   {
      m_u8AudSources[u8AudSources] = 0;
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudio::~spi_tclAudio();
 ***************************************************************************/
spi_tclAudio::~spi_tclAudio()
{
   ETG_TRACE_USR1(("spi_tclAudio::~spi_tclAudio entered \n"));
   m_poAudioPolicyBase = NULL;
   m_poAudioInHandler  = NULL;
   m_poGMainLoopThread = NULL;
   m_enAudioInState    = e8_AUDIOIN_UNITIAILZED;

   for (t_U8 u8AudClients = 0; u8AudClients < NUM_AUD_CLIENTS; u8AudClients++)
   {
      m_poAudDevHandlers[u8AudClients] = NULL;
   }

   for (t_U8 u8AudSources = 0; u8AudSources < e8AUD_INVALID; u8AudSources++)
   {
      m_u8AudSources[u8AudSources] = 0;
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAudio::bInitialize()
 ***************************************************************************/
t_Bool spi_tclAudio::bInitialize()
{
   t_Bool bInit = false;

   //! Create handlers
   m_poAudDevHandlers[e8DEV_TYPE_DIPO] = new spi_tclDiPoAudio();
   SPI_NORMAL_ASSERT(NULL == m_poAudDevHandlers[e8DEV_TYPE_DIPO]);

#ifdef VARIANT_S_FTR_ENABLE_SPI_MIRRORLINK
   m_poAudDevHandlers[e8DEV_TYPE_MIRRORLINK] = new spi_tclMLAudio();
   SPI_NORMAL_ASSERT(NULL == m_poAudDevHandlers[e8DEV_TYPE_MIRRORLINK]);
#endif
#ifdef VARIANT_S_FTR_ENABLE_SPI_ANDROIDAUTO
   m_poAudDevHandlers[e8DEV_TYPE_ANDROIDAUTO] = new spi_tclAAPAudio();
   SPI_NORMAL_ASSERT(NULL == m_poAudDevHandlers[e8DEV_TYPE_ANDROIDAUTO]);
#endif

   //! Audio In Handler for Echo Cancellation and Noise Reduction
   m_poAudioInHandler = new spi_tclAudioIn();
   SPI_NORMAL_ASSERT(NULL == m_poAudioInHandler);

   vRegisterCallbacks();

   //! Create a new thread for GMainLoop
   if (NULL != m_poAudioInHandler)
   {
      m_poGMainLoopThread = new shl::thread::FunctionThreader(
            m_poAudioInHandler, true);
   }
   SPI_NORMAL_ASSERT(NULL == m_poGMainLoopThread);
   //! Set the bInit to false, if all the SPI technologies cannot be initialized
   //! else return true if one of the SPI technologies can be initialized
   //! If Mirrorlink supports AudioIn in future modify the logic accordingly
   bInit = ((NULL != m_poAudDevHandlers[e8DEV_TYPE_MIRRORLINK]) ||
         (NULL != m_poAudDevHandlers[e8DEV_TYPE_DIPO]) || (NULL != m_poAudDevHandlers[e8DEV_TYPE_ANDROIDAUTO])) &&
         ((NULL != m_poAudioInHandler) &&
         (NULL != m_poGMainLoopThread));

   ETG_TRACE_USR1(("spi_tclAudio::bInitialize  Mirrorlink handle = %p Carplay handle = %p bInit = %d\n",
   m_poAudDevHandlers[e8DEV_TYPE_MIRRORLINK],  m_poAudDevHandlers[e8DEV_TYPE_DIPO], ETG_ENUM(BOOL, bInit)));
   return bInit;
}//spi_tclAudio::bInitialize()

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAudio::bUnInitialize()
 ***************************************************************************/
t_Bool spi_tclAudio::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclAudio::bUnInitialize entered \n"));

   for (t_U8 u8AudDevCnt = 0; u8AudDevCnt < NUM_AUD_CLIENTS; u8AudDevCnt++)
   {
      RELEASE_MEM(m_poAudDevHandlers[u8AudDevCnt]);
   }
   RELEASE_MEM(m_poGMainLoopThread);
   RELEASE_MEM(m_poAudioInHandler);

   return true;
}//spi_tclAudio::bUnInitialize()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudio::vLoadSettings(const trSpiFeatureSupport&...)
***************************************************************************/
t_Void spi_tclAudio::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1(("spi_tclAudio::vLoadSettings entered \n"));
   SPI_INTENTIONALLY_UNUSED(rfcrSpiFeatureSupp);

   spi_tclAudioSettings* poAudSettings = spi_tclAudioSettings::getInstance();
   if (NULL != poAudSettings)
   {
      for (t_U8 u8AudSources = 0; u8AudSources < e8AUD_INVALID; u8AudSources++)
      {
         m_u8AudSources[u8AudSources] = poAudSettings->u8GetSourceNumber(
               static_cast<tenAudioDir> (u8AudSources));
      }
      //! Read and set audio pipeleine configuration for alsa devices
      tmapAudioPipeConfig mapAudioPipeConfig;
      poAudSettings->vGetAudioPipeConfig(mapAudioPipeConfig);

      for (t_U8 u8AudClients = 0; u8AudClients < NUM_AUD_CLIENTS; u8AudClients++)
      {
         if(NULL != m_poAudDevHandlers[u8AudClients])
         {
            m_poAudDevHandlers[u8AudClients]->vSetAudioPipeConfig(mapAudioPipeConfig);
         }
      }
   } //if(NULL != poAudSettings)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudio::vSaveSettings()
***************************************************************************/
t_Void spi_tclAudio::vSaveSettings()
{
   ETG_TRACE_USR1(("spi_tclAudio::vSaveSettings entered \n"));
   //Add code
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudio::vRegisterCallbacks
 ***************************************************************************/
t_Void spi_tclAudio::vRegisterCallbacks()
{
   ETG_TRACE_USR1(("spi_tclAudio::vRegisterCallbacks entered \n"));
   /*lint -esym(40,fvSelectDeviceResp) Undeclared identifier */
   /*lint -esym(40,fvStartAudioResp) fvStartAudioResp Undeclared identifier */
   /*lint -esym(40,fvStopAudioResp) fvStopAudioResp Undeclared identifier */
   /*lint -esym(40,fvLaunchAudioReq)fvLaunchAudioReq  Undeclared identifier */
   /*lint -esym(40,fvTerminateAudioReq)fvTerminateAudioReq Undeclared identifier */
   /*lint -esym(40,_1)_1 Undeclared identifier */
   /*lint -esym(40,_2)_2 Undeclared identifier */
   /*lint -esym(40,_3)_1 Undeclared identifier */
   /*lint -esym(40,_4)_2 Undeclared identifier */
   

   trAudioCallbacks rAudCallbacks;

   //! Populate the callback structure
   rAudCallbacks.fvSelectDeviceResp
            = std::bind(&spi_tclAudio::vCbSelectDeviceResp,
                     this,
                     std::placeholders::_1,
                     std::placeholders::_2,
                     std::placeholders::_3);

   rAudCallbacks.fvStartAudioResp
            = std::bind(&spi_tclAudio::vCbStartSrcActivity,
                     this,
                     std::placeholders::_1,
                     std::placeholders::_2);

   rAudCallbacks.fvStopAudioResp = std::bind(&spi_tclAudio::vCbStopSrcActivity,
            this,
            std::placeholders::_1,
            std::placeholders::_2);

   rAudCallbacks.fvLaunchAudioReq = std::bind(&spi_tclAudio::vLaunchAudio,
            this,
            std::placeholders::_1,
            std::placeholders::_2,
            std::placeholders::_3,
            std::placeholders::_4);

   rAudCallbacks.fvTerminateAudioReq = std::bind(&spi_tclAudio::vTerminateAudio,
            this,
            std::placeholders::_1,
            std::placeholders::_2);

   rAudCallbacks.fvStartAudioIn = std::bind(&spi_tclAudio::vStartAudioIn,
            this,
            std::placeholders::_1);

   rAudCallbacks.fvStopAudioIn = std::bind(&spi_tclAudio::vStopAudioIn,
            this,
            std::placeholders::_1);

   rAudCallbacks.fbSetAudioDucking = std::bind(&spi_tclAudio::bSetAudioDucking,
      this,
      std::placeholders::_1,
      std::placeholders::_2,
      std::placeholders::_3,
      std::placeholders::_4);

   //! Register for callbacks with SPI connections
   for (t_U8 u8AudDevCnt = 0; u8AudDevCnt < NUM_AUD_CLIENTS; u8AudDevCnt++)
   {
      if (NULL != m_poAudDevHandlers[u8AudDevCnt])
      {
         m_poAudDevHandlers[u8AudDevCnt]->vRegisterCallbacks(rAudCallbacks);
      } // if(NULL != m_poAudDevHandlers[u8AudDevCnt])
   } // for(t_U8 u8AudDevCnt = 0; u8AudDevCnt < NUM_AUD_CLIENTS ; u8AudDevCnt++)
}

/***************************************************************************
 ** FUNCTION:  static t_Void vCbSelectDeviceResp(t_U32 u32DeviceId,..)
 ***************************************************************************/
t_Void spi_tclAudio::vCbSelectDeviceResp(t_U32 u32DeviceId,
         tenDeviceConnectionReq enDevConnReq, t_Bool bResult)
{
   ETG_TRACE_USR1((" spi_tclAudio::vCbSelectDeviceResp bResult %d \n",
            ETG_ENUM(BOOL,bResult)));
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   SPI_INTENTIONALLY_UNUSED(enDevConnReq);

   tenErrorCode enErrorCode = (true==bResult)? e8NO_ERRORS : e8SELECTION_FAILED;
   //! Post Select device result
   spi_tclMediator *poMediator = spi_tclMediator::getInstance();
   if (NULL != poMediator)
   {
      poMediator->vPostSelectDeviceRes(e32COMPID_AUDIO, enErrorCode);
   } //if (NULL != poMediator)
   // On Selection/Deselection of the Device, clear the Device Information
   m_oAudioSrcInfo.vClearDeviceInfo();
}//t_Void vCbSelectDeviceResp(t_U32 u32DeviceId,..)

/***************************************************************************
 ** FUNCTION:  static t_Void vCbStartSrcActivity(tenAudioDir enAudDir,..)
 ***************************************************************************/
t_Void spi_tclAudio::vCbStartSrcActivity(tenAudioDir enAudDir, t_Bool bResult)
{
   ETG_TRACE_USR1((" spi_tclAudio::vCbStartSrcActivity enAudDir = %d bResult %d \n",
         ETG_ENUM(AUDIO_DIRECTION,enAudDir), ETG_ENUM(BOOL,bResult)));

   if (NULL != m_poAudioPolicyBase)
   {
      m_poAudioPolicyBase->vStartSourceActivityResult(m_u8AudSources[enAudDir],
            bResult);
   }

   //! Call ECNR StartAudio() if the device category is not Android Auto.
   //! In case of Android Auto, AudioIn has to be started after receiving microphone request
   //! THis is because for Android Auto, the phone can request open and close of microphone any number
   //! of times during a session
   tenDeviceCategory enDevCat = m_oAudioSrcInfo.enGetDeviceCategory(m_u8AudSources[enAudDir]);

   if(e8DEV_TYPE_ANDROIDAUTO != enDevCat)
   {
      vStartAudioIn(enAudDir);
   }

}//t_Void spi_tclAudio::vCbStartSrcActivity(t_U32 u32DeviceId...)


/***************************************************************************
 ** FUNCTION:  static t_Void vCbStopSrcActivity(tenAudioDir enAudDir,..)
 ***************************************************************************/
t_Void spi_tclAudio::vCbStopSrcActivity(tenAudioDir enAudDir, t_Bool bResult)
{
   ETG_TRACE_USR1(("spi_tclAudio::vCbStopSrcActivity enAudDir = %d bResult %d \n",
         ETG_ENUM(AUDIO_DIRECTION,enAudDir), ETG_ENUM(BOOL,bResult)));

   if (NULL != m_poAudioPolicyBase)
   {
      m_poAudioPolicyBase->vStopSourceActivityResult(m_u8AudSources[enAudDir], bResult);
   }

   //! Call ECNR StopAudio()if the device category is not Android Auto.
   //! In case of Android Auto, AudioIn has to be started after receiving microphone request
   //! THis is because for Android Auto, the phone can request open and close of microphone any number
   //! of times during a session

   tenDeviceCategory enDevCat = m_oAudioSrcInfo.enGetDeviceCategory(m_u8AudSources[enAudDir]);
   if(e8DEV_TYPE_ANDROIDAUTO != enDevCat)
   {
      vStopAudioIn(enAudDir);
   }
}//t_Void spi_tclAudio::vCbStopSrcActivity(tenAudioDir enAudDir...)

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAudio::bOnRouteAllocateResult(t_U8, trAudSrcInfo&)
 ***************************************************************************/
t_Bool spi_tclAudio::bOnRouteAllocateResult(t_U8 u8SourceNum,
         trAudSrcInfo& rfrSrcInfo)
{
   ETG_TRACE_USR1(("spi_tclAudio::bOnRouteAllocateResult u8SourceNum = %d ", u8SourceNum));
   ETG_TRACE_USR4(("spi_tclAudio::bOnRouteAllocateResult MainAudDevName out = %s ",
         rfrSrcInfo.rMainAudDevNames.szOutputDev.c_str()));
   ETG_TRACE_USR4(("spi_tclAudio::bOnRouteAllocateResult MainAudDevName in = %s ",
         rfrSrcInfo.rMainAudDevNames.szInputDev.c_str()));
   ETG_TRACE_USR4(("spi_tclAudio::bOnRouteAllocateResult EcnrAudDevName out = %s ",
         rfrSrcInfo.rEcnrAudDevNames.szOutputDev.c_str()));
   ETG_TRACE_USR4(("spi_tclAudio::bOnRouteAllocateResult EcnrAudDevName in = %s ",
         rfrSrcInfo.rEcnrAudDevNames.szInputDev.c_str()));

   //Store the details here in Audio Source class if the Source is not active
   if(false == m_oAudioSrcInfo.bIsSrcActive(u8SourceNum))
   {
       m_oAudioSrcInfo.vSetAudSrcInfo(u8SourceNum, rfrSrcInfo);
   } //if(false == m_oAudioSrcInfo.bIsSrcActive(u8SourceNum))

   t_Bool bRetVal = false;

   tenDeviceCategory enDevCat = (m_oAudioSrcInfo.enGetDeviceCategory(
         u8SourceNum));

   if (e8DEV_TYPE_UNKNOWN != enDevCat)
   {
      t_U8 u8DevCategory = static_cast<t_U8> (enDevCat);
      tenAudioDir enAudDir = m_oAudioSrcInfo.enGetAudDirection(u8SourceNum);

      if (NULL != m_poAudDevHandlers[u8DevCategory])
      {
         m_poAudDevHandlers[u8DevCategory]->bInitializeAudioPlayback(m_oAudioSrcInfo.u32GetDeviceID(u8SourceNum),enAudDir);
      }

      tenAudioSamplingRate enAudSampleRate = m_oAudioSrcInfo.enGetAudSamplingRate(u8SourceNum);
      vInitializeAudioIn(enAudDir, enAudSampleRate);
      vSetAlsaDevice(enAudDir);
      bRetVal = true;
   }
   ETG_TRACE_USR4(("spi_tclAudio::bOnRouteAllocateResult bRetVal = %d \n", bRetVal));
   return bRetVal;
}//spi_tclAudio::bOnRouteAllocateResult(t_U8, trAudSrcInfo&)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudio::vOnRouteDeAllocateResult(t_U8 u8SourceNum)
 ***************************************************************************/
t_Void spi_tclAudio::vOnRouteDeAllocateResult(t_U8 u8SourceNum)
{
   ETG_TRACE_USR1((" spi_tclAudio::vOnRouteDeAllocateResult u8SourceNum = %d\n", u8SourceNum));

   t_U8 u8DevCategory =
            static_cast<t_U8> (m_oAudioSrcInfo.enGetDeviceCategory(u8SourceNum));
   t_U32 u32DevID = m_oAudioSrcInfo.u32GetDeviceID(u8SourceNum);
   tenAudioDir enAudDir = m_oAudioSrcInfo.enGetAudDirection(u8SourceNum);

   if (NULL != m_poAudDevHandlers[u8DevCategory])
   {
      m_poAudDevHandlers[u8DevCategory]->bFinalizeAudioPlayback(u32DevID,enAudDir);
   }

   // Call ECNR Destroy and un initialize
   vUninitializeAudioIn(enAudDir);
}//spi_tclAudio::vOnRouteDeAllocateResult(t_U8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudio::vOnStartSourceActivity(t_U8 u8SourceNum)
 ***************************************************************************/
t_Void spi_tclAudio::vOnStartSourceActivity(t_U8 u8SourceNum, t_Bool bResult)
{
   ETG_TRACE_USR1((" spi_tclAudio::vOnStartSourceActivity u8SourceNum= %d  \n", u8SourceNum));

   trAudSrcInfo rSrcInfo;
   t_U8 u8DevCategory =
            static_cast<t_U8> (m_oAudioSrcInfo.enGetDeviceCategory(u8SourceNum));

   t_Bool bRetVal       = m_oAudioSrcInfo.bGetAudSrcInfo(u8SourceNum, rSrcInfo);
   tenAudioDir enAudDir = m_oAudioSrcInfo.enGetAudDirection(u8SourceNum);
   tenAudioSamplingRate enAudSampleRate = m_oAudioSrcInfo.enGetAudSamplingRate(u8SourceNum);
   t_U32 u32DevID       = m_oAudioSrcInfo.u32GetDeviceID(u8SourceNum);

   //Route is allocated for SPI - Set the ActiveSrcFlag to TRUE
   m_oAudioSrcInfo.vSetActiveSrcFlag(u8SourceNum, true);

   if ((true == bRetVal) && (NULL != m_poAudDevHandlers[u8DevCategory]) && (0 != u32DevID))
   {
      if ((e8AUD_PHONE_IN == enAudDir) || (e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir))
      {
         bRetVal = m_poAudDevHandlers[u8DevCategory]->bStartAudio(u32DevID,
                  (gszAudDeviceName[enAudDir][enAudSampleRate]).c_str(),
                  (gszAudDeviceName[enAudDir][enAudSampleRate]).c_str(), enAudDir);
      }//if ((e8AUD_PHONE_IN == enAudDir) || (e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir))
      else if( (e8AUD_MAIN_OUT == enAudDir) || (e8AUD_MIX_OUT == enAudDir) || 
         (e8AUD_ALERT_OUT == enAudDir) ||(e8AUD_DUCK == enAudDir) || 
         (e8AUD_TRANSIENT == enAudDir) || ( e8AUD_STEREO_MIX_OUT == enAudDir) )
      {
         bRetVal = m_poAudDevHandlers[u8DevCategory]->bStartAudio(u32DevID,
                  rSrcInfo.rMainAudDevNames.szOutputDev,
                  enAudDir);

      }//else if((e8AUD_MAIN_OUT == enAudDir) || (e8AUD_MIX_OUT == enAudDir) ...)
   }//if ((true == bRetVal) && (NULL != m_poAudDevHandlers[u8DevCategory]))

   if ((NULL != m_poAudioPolicyBase) && (false == bRetVal) && (true == bResult))
   {
      m_poAudioPolicyBase->vStartSourceActivityResult(u8SourceNum, bRetVal);
   }
}//spi_tclAudio::vOnStartSourceActivity(t_U8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudio::vOnStopSourceActivity(t_U8 u8SourceNum)
 ***************************************************************************/
t_Void spi_tclAudio::vOnStopSourceActivity(t_U8 u8SourceNum)
{
   ETG_TRACE_USR1((" spi_tclAudio::vOnStopSourceActivity u8SourceNum= %d  \n", u8SourceNum));

   t_U8 u8DevCategory =
            static_cast<t_U8> (m_oAudioSrcInfo.enGetDeviceCategory(u8SourceNum));

   t_U32 u32DevID = m_oAudioSrcInfo.u32GetDeviceID(u8SourceNum);
   tenAudioDir enAudDir = m_oAudioSrcInfo.enGetAudDirection(u8SourceNum);

   //Set the ActiveSrcFlag to FALSE since source is deallocated
   m_oAudioSrcInfo.vSetActiveSrcFlag(u8SourceNum, false);

   if (NULL != m_poAudDevHandlers[u8DevCategory])
   {
      m_poAudDevHandlers[u8DevCategory]->vStopAudio(u32DevID, enAudDir);
   }
   else if (NULL != m_poAudioPolicyBase)
   {
      m_poAudioPolicyBase->vStopSourceActivityResult(u8SourceNum, true);
   }
}//spi_tclAudio::vOnStopSourceActivity(t_U8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudio::vSelectDevice(t_U32 u32DeviceId,
 tenDeviceConnectionReq enDevConnReq, tenDeviceCategory eDevCat)
 ***************************************************************************/
t_Void spi_tclAudio::vSelectDevice(t_U32 u32DeviceId,
         tenDeviceConnectionReq enDevConnReq, tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR4(("spi_tclAudio::vSelectDevice u32DeviceId = %d enDevConnReq = %d\n",
            u32DeviceId, ETG_ENUM(CONNECTION_REQ,enDevConnReq) ));

   t_U8 u8DevCategory = static_cast<t_U8> (enDevCat);

   if (NULL != m_poAudDevHandlers[u8DevCategory])
   {
      (e8DEVCONNREQ_SELECT == enDevConnReq) ?
      ((t_Void)(m_poAudDevHandlers[u8DevCategory]->bSelectAudioDevice(u32DeviceId))) :
      (m_poAudDevHandlers[u8DevCategory]->vDeselectAudioDevice(u32DeviceId));

      //@Note:
      //1. Audio source info is cleared on Device Select/Deselect Callback
      //2. Deactivation of audio channels on device deselection is moved to technology handlers

   }// if (NULL != m_poAudDevHandlers[u8DevCategory])
}//spi_tclAudio::vSelectDevice(t_U32 u32DeviceId, tenEnabledInfo...)

/***************************************************************************
** FUNCTION:  t_Void spi_tclAudio::vSelectDeviceResult(t_U32...)
***************************************************************************/
t_Void spi_tclAudio::vSelectDeviceResult(t_U32 u32DeviceId,
      tenDeviceConnectionReq enDeviceConnReq,
      tenResponseCode enRespCode,
      tenErrorCode enErrorCode,
      tenDeviceCategory enDevCategory)
{
   ETG_TRACE_USR1(("spi_tclAudio::vSelectDeviceResult "
         "DeviceHandle = %d, DevConnReq %d, ResponseCode %d, ErrorCode %d ",
         u32DeviceId, ETG_ENUM(CONNECTION_REQ, enDeviceConnReq),
         ETG_ENUM(RESPONSE_CODE, enRespCode), ETG_ENUM(ERROR_CODE, enErrorCode)));

   if ((e8DEV_TYPE_ANDROIDAUTO == enDevCategory)
         && (NULL != m_poAudDevHandlers[e8DEV_TYPE_ANDROIDAUTO]))
   {
      m_poAudDevHandlers[e8DEV_TYPE_ANDROIDAUTO]->vUpdateDeviceSelection(u32DeviceId, enDevCategory,
               enDeviceConnReq, enRespCode, enErrorCode);
   } // if (NULL != m_poAudDevHandlers[u8DevCategory])

   //! Set source to unavailable after  device is deselected/disconnected(SUZUKI-22901).
   if((e8DEVCONNREQ_DESELECT == enDeviceConnReq) && (NULL != m_poAudioPolicyBase))
   {
	   m_poAudioPolicyBase->bSetSrcAvailability(m_u8AudSources[e8AUD_MAIN_OUT], false);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudio::vLaunchAudio(t_U32 u32DeviceId,
 tenDeviceCategory enDevCat, tenAudioDir enAudDir, tenAudioSampleRate enAudSampleRate)
 ***************************************************************************/
t_Void spi_tclAudio::vLaunchAudio(t_U32 u32DeviceId,
         tenDeviceCategory enDevCat, tenAudioDir enAudDir, tenAudioSamplingRate enAudSampleRate)
{
   ETG_TRACE_USR1(("spi_tclAudio::vLaunchAudio u32DeviceId= %d, enDevCat =%d, enAudDir %d enAudSampleRate = %d",
         u32DeviceId, ETG_ENUM(DEVICE_CATEGORY,enDevCat), ETG_ENUM(AUDIO_DIRECTION,enAudDir), ETG_ENUM(SAMPLING_RATE, enAudSampleRate)));

   t_U8 u8SrcNum = (e8AUD_INVALID > enAudDir)? (m_u8AudSources[enAudDir]):(0);
   ETG_TRACE_USR4(("spi_tclAudio::vLaunchAudio Is SrcActive = %d ",
         ETG_ENUM(BOOL, m_oAudioSrcInfo.bIsSrcActive(u8SrcNum))));

   if ((0 != u8SrcNum) && (NULL != m_poAudioPolicyBase))
   {
      m_oAudioSrcInfo.vSetDeviceInfo(u8SrcNum, u32DeviceId, enDevCat, enAudDir, enAudSampleRate);

      if (false == m_oAudioSrcInfo.bIsSrcActive(u8SrcNum))
      {
         t_Bool bRetVal = m_poAudioPolicyBase->bRequestAudioActivation(u8SrcNum);
         ETG_TRACE_USR4((" spi_tclAudio::vLaunchAudio: Is AudioActivation successful = %d ", ETG_ENUM(BOOL, bRetVal)));

      }// if (false == m_oAudioSrcInfo.bIsSrcActive(u8SrcNum))
      else if ((e8DEV_TYPE_MIRRORLINK == enDevCat) || (e8DEV_TYPE_ANDROIDAUTO == enDevCat))
      {
         //Since the Source is already active, get the Source Info and use the existing APIs to mock the Audio Sequence
         //@Note: This is not required for CarPlay since it is taken care when SPI receives Audio device info from Mediaplayer
         trAudSrcInfo rSrcInfo;
         bGetAudSrcInfo(u8SrcNum,rSrcInfo);
         bOnRouteAllocateResult(u8SrcNum,rSrcInfo);
         vOnStartSourceActivity(u8SrcNum, false);
      } //end of if-else; if (false == m_oAudioSrcInfo.bIsSrcActive(u8SrcNum))
   }
}//spi_tclAudio::vLaunchAudio(t_U32 u32DeviceId,tenDeviceCategory...)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudio::vTerminateAudio(t_U32 u32DeviceId,
                tenAudioDir enAudDir)
 ***************************************************************************/
t_Void spi_tclAudio::vTerminateAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
{
   ETG_TRACE_USR1((" spi_tclAudio::vTerminateAudio u32DeviceId= %d, enAudDir %d \n",
         u32DeviceId, ETG_ENUM(AUDIO_DIRECTION,enAudDir)));

   /*Not required to check if the source is active for sending deactivation request.
   Reason: If Audio Manager is in process of restoring source, deactivation request needs to be triggered
   as the source active flag will not be set yet.*/
   if ((NULL != m_poAudioPolicyBase) && (e8AUD_INVALID != enAudDir))
   {
       if(e8AUD_MIX_OUT == enAudDir)
       {
           /*SPI route is going to be deactivated for Alternate channel - Set the ActiveSrcFlag to FALSE
           This will resolve the issue if back to back requests for MIX channel is triggered by iPhone.*/
           m_oAudioSrcInfo.vSetActiveSrcFlag(m_u8AudSources[enAudDir], false);
       }//End of if(e8AUD_MIX_OUT == enAudDir)

      (t_Void) m_poAudioPolicyBase->bRequestAudioDeactivation(m_u8AudSources[enAudDir]);
   }//if (NULL != m_poAudioPolicyBase)
}//spi_tclAudio::vTerminateAudio(t_U32 u32DeviceId,tenAudioDir enAudDir)

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAudio::bGetAudSrcInfo(const t_U8 cu8SourceNum,
 **            trAudSrcInfo& rfrSrcInfo)
 ***************************************************************************/
t_Bool spi_tclAudio::bGetAudSrcInfo(const t_U8 cu8SourceNum, trAudSrcInfo& rfrSrcInfo)
{
   t_Bool bRetVal = m_oAudioSrcInfo.bGetAudSrcInfo(cu8SourceNum, rfrSrcInfo);
   ETG_TRACE_USR3((" bGetAudSrcInfo left with result = %u ", bRetVal));
   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudio::bSetAudioBlockingMode()
***************************************************************************/
t_Bool spi_tclAudio::bSetAudioBlockingMode(const t_U32 cou32DevId,
                                           const tenBlockingMode coenBlockingMode,
                                           tenDeviceCategory enDevCat) const
{
   ETG_TRACE_USR1((" spi_tclAudio::bSetAudioBlockingMode:coenBlockingMode-%d ",coenBlockingMode));

   t_Bool bRet = false;
   t_U8 u8DevCat = static_cast<t_U8>(enDevCat);

   if((u8DevCat < NUM_AUD_CLIENTS) && (NULL != m_poAudDevHandlers[u8DevCat]) )
   {
      bRet = m_poAudDevHandlers[u8DevCat]->bSetAudioBlockingMode(cou32DevId,coenBlockingMode);
   }//if(u8DevCat < NUM_AUD_CLIENTS) && (NULL != m_poAudDevHandlers[u8DevCat])

   ETG_TRACE_USR4((" bSetAudioBlockingMode:Result-%d ",ETG_ENUM(BOOL,bRet)));

   return bRet;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudio::bSetAudioSrcAvailability()
***************************************************************************/
t_Bool spi_tclAudio::bSetAudioSrcAvailability(tenAudioDir enAudioDir, t_Bool bAvail)
{
   ETG_TRACE_USR1((" spi_tclAudio::bSetAudioSrcAvailability:enAudioDir-%d  bAvail = %d ",
         ETG_ENUM(AUDIO_DIRECTION,enAudioDir), bAvail));
   t_Bool bRetVal = false;
   t_U8 u8SrcNum = (e8AUD_INVALID > enAudioDir)? (m_u8AudSources[enAudioDir]):(0);
   if((NULL != m_poAudioPolicyBase) && (0 != u8SrcNum))
   {
      bRetVal = m_poAudioPolicyBase->bSetSrcAvailability(u8SrcNum, bAvail);
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudio::vSetServiceAvailable()
***************************************************************************/
t_Void spi_tclAudio::vSetServiceAvailable(t_Bool bAvail)
{
   ETG_TRACE_USR1(("spi_tclAudio::vSetServiceAvailable: bAvail = %d \n", bAvail));
   if(NULL != m_poAudioPolicyBase)
   {
      m_poAudioPolicyBase->vSetServiceAvailable(bAvail);
   }
}
/***************************************************************************
** FUNCTION: t_Bool spi_tclAudio::bSetAudioDucking()
***************************************************************************/
t_Bool spi_tclAudio::bSetAudioDucking(const t_U16 cou16RampDuration,const
      t_U8 cou8VolumeLevelindB, const tenDuckingType coenDuckingType,tenDeviceCategory enDevCat)
{
   ETG_TRACE_USR1(("spi_tclAudio::bSetAudioDucking: cou16RampDuration = %d "
         "cou8VolumeLevelindB = %d coenDuckingType = %d ", cou16RampDuration,
         cou8VolumeLevelindB, coenDuckingType));

   t_Bool bRetVal = false;
   t_U8 u8SrcNum = (enDevCat == e8DEV_TYPE_MIRRORLINK)? m_u8AudSources[e8AUD_STEREO_MIX_OUT]:
      m_u8AudSources[e8AUD_DUCK]; 

   //! Send request for Audio ducking only if e8AUD_MIX_OUT is active
   if (NULL != m_poAudioPolicyBase)
   {
      bRetVal = m_poAudioPolicyBase->bSetAudioDucking(u8SrcNum, cou16RampDuration,
               cou8VolumeLevelindB, coenDuckingType);
   }

   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAudio::vInitializeAudioIn()
***************************************************************************/
t_Void spi_tclAudio::vInitializeAudioIn(tenAudioDir enAudDir, tenAudioSamplingRate enSamplingRate)
{
   ETG_TRACE_USR1((" spi_tclAudio::vInitializeAudioIn: m_enAudioInState = %d enAudDir = %d  enSamplingRate =%d ",
         ETG_ENUM(AUDIOIN_STATE, m_enAudioInState), ETG_ENUM(AUDIO_DIRECTION,enAudDir), 
         ETG_ENUM(SAMPLING_RATE, enSamplingRate)));
   m_oAudioInLock.s16Lock();
   if ((e8_AUDIOIN_UNITIAILZED == m_enAudioInState) && 
      ((e8AUD_PHONE_IN == enAudDir) || (e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir))
      && (NULL != m_poAudioInHandler))
   {
      t_String szALsaDevice = "";
      t_U8 u8SrcNum = (e8AUD_INVALID > enAudDir) ? (m_u8AudSources[enAudDir]) : (0);
      m_oAudioSrcInfo.vSetSamplingRate(u8SrcNum, enSamplingRate);
      tenAudioInDataSet enAudioDataSet = e32_AUDIOIN_DATASET_UNKNOWN;
      tenDeviceCategory enDevCat = m_oAudioSrcInfo.enGetDeviceCategory(u8SrcNum);
      if (e8AUD_PHONE_IN == enAudDir)
      {
         enAudioDataSet = (e8AUD_SAMPLERATE_8KHZ == enSamplingRate) ? e32_AUDIOIN_DATASET_PHONE_NB
               : e32_AUDIOIN_DATASET_PHONE_WB;
      }
      else if (e8AUD_VR_IN == enAudDir)
      {
         //! In case of Android Auto, ECNR is used only for gain control for microphone
         //! Whereas output is directly routed to speaker. so a different dataset is required
         if (e8DEV_TYPE_ANDROIDAUTO == enDevCat)
         {
            enAudioDataSet = e32_AUDIOIN_DATASET_VR_MIC_ONLY;
         }
         else
         {
            enAudioDataSet = (e8AUD_SAMPLERATE_8KHZ == enSamplingRate) ? e32_AUDIOIN_DATASET_VR_NB
               : e32_AUDIOIN_DATASET_VR_WB;
         }
      }
      else if (e8AUD_DEFAULT == enAudDir)
      {
         //! Default audio type is without ECNR
         enAudioDataSet = e32_AUDIOIN_DATASET_VR_MIC_ONLY;
      }
      
      m_poAudioInHandler->vInitializeAudioIn(enAudioDataSet);

      m_enAudioInState = e8_AUDIOIN_INITIALIZED;
   }
   m_oAudioInLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAudio::vSetAlsaDevice()
***************************************************************************/
t_Void spi_tclAudio::vSetAlsaDevice(tenAudioDir enAudDir)
{
   ETG_TRACE_USR1(("spi_tclAudio::vSetAlsaDevice: m_enAudioInState = %d enAudDir = %d", 
      ETG_ENUM(AUDIOIN_STATE, m_enAudioInState), ETG_ENUM(AUDIO_DIRECTION,enAudDir)));

   m_oAudioInLock.s16Lock();
   if ((e8_AUDIOIN_UNITIAILZED != m_enAudioInState) &&
      ((e8AUD_PHONE_IN == enAudDir) || (e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir)) &&
      (NULL != m_poAudioInHandler))
   {
      t_String szALsaDevice = "";
      t_U8 u8SrcNum = m_u8AudSources[enAudDir];
      tenAudioSamplingRate enAudSampleRate = m_oAudioSrcInfo.enGetAudSamplingRate(u8SrcNum);
      //! Alsa Devices to be set to ECNR is different for GM and G3G projects
      if (e8AUD_PHONE_IN == enAudDir)
      {
         #ifdef VARIANT_S_FTR_ENABLE_GM
         szALsaDevice = "AdevSPIVoiceOut";
         #else
         szALsaDevice = "AdevVoiceOut";
         #endif
      }
      else if ((e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir))
      {
         #ifdef VARIANT_S_FTR_ENABLE_GM
         szALsaDevice = "AdevSPIAcousticoutSpeech";
         #else
         szALsaDevice = "AdevAcousticoutSpeech";
         #endif
      }
      /*Set Audio Device changes.*/
      m_poAudioInHandler->vSetAlsaDevice(AUD_ECNR_MIC_OUT, gszECNRAudDeviceName[enAudDir][enAudSampleRate]);
      m_poAudioInHandler->vSetAlsaDevice(AUD_ECNR_RECV_IN, gszECNRAudDeviceName[enAudDir][enAudSampleRate]);
      m_poAudioInHandler->vSetAlsaDevice(AUD_ECNR_RECV_OUT, szALsaDevice);
      m_enAudioInState = e8_AUDIOIN_INITIALIZED;
   }
   m_oAudioInLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAudio::vUninitializeAudioIn()
***************************************************************************/
t_Void spi_tclAudio::vUninitializeAudioIn(tenAudioDir enAudDir)
{
   ETG_TRACE_USR1((" spi_tclAudio::vUninitializeAudioIn: m_enAudioInState = %d enAudDir = %d  ",
         ETG_ENUM(AUDIOIN_STATE, m_enAudioInState), ETG_ENUM(AUDIO_DIRECTION,enAudDir)));
   //Call ECNR StopAudio()
   m_oAudioInLock.s16Lock();
   if ((e8_AUDIOIN_UNITIAILZED != m_enAudioInState) &&
      ((e8AUD_PHONE_IN == enAudDir) || (e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir)) &&
      (NULL != m_poAudioInHandler))
   {
      // Call ECNR Destroy and uninitialize
      m_poAudioInHandler->vUnitializeAudioIn();
      m_enAudioInState = e8_AUDIOIN_UNITIAILZED;
   }
   m_oAudioInLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAudio::vStartAudioIn()
***************************************************************************/
t_Void spi_tclAudio::vStartAudioIn(tenAudioDir enAudDir)
{
   ETG_TRACE_USR1((" spi_tclAudio::vStartAudioIn: m_enAudioInState = %d enAudDir = %d  ",
         ETG_ENUM(AUDIOIN_STATE, m_enAudioInState), ETG_ENUM(AUDIO_DIRECTION,enAudDir)));
   //Call ECNR StartAudio()
   m_oAudioInLock.s16Lock();
   if ((e8_AUDIOIN_INITIALIZED == m_enAudioInState) && 
      ((e8AUD_PHONE_IN == enAudDir) || (e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir)) &&
      (NULL != m_poAudioInHandler))
   {
      m_poAudioInHandler->vStartAudioIn();
      m_enAudioInState = e8_AUDIOIN_STARTED;
   }
   m_oAudioInLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAudio::vStopAudioIn()
***************************************************************************/
t_Void spi_tclAudio::vStopAudioIn(tenAudioDir enAudDir)
{
   ETG_TRACE_USR1((" spi_tclAudio::vStopAudioIn: m_enAudioInState = %d enAudDir = %d ",
         ETG_ENUM(AUDIOIN_STATE, m_enAudioInState), ETG_ENUM(AUDIO_DIRECTION,enAudDir)));
   //Call ECNR StopAudio()
   m_oAudioInLock.s16Lock();
   if ((e8_AUDIOIN_STARTED == m_enAudioInState) &&
      ((e8AUD_PHONE_IN == enAudDir) || (e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir)) &&
      (NULL != m_poAudioInHandler))
   {
      m_poAudioInHandler->vStopAudioIn();
      m_enAudioInState = e8_AUDIOIN_INITIALIZED;
   }
   m_oAudioInLock.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudio::vSetAudioInConfig
 ***************************************************************************/
t_Void spi_tclAudio::vSetAudioInConfig(tenAudioDir enAudDir, tenAudioSamplingRate enSamplingRate)
{
   ETG_TRACE_USR1((" spi_tclAudio::vSetAudioInConfig: enSamplingRate = %d  \n", ETG_ENUM(SAMPLING_RATE, enSamplingRate)));

   tenAudioInDataSet enAudioDataSet = e32_AUDIOIN_DATASET_UNKNOWN;

   if (e8AUD_PHONE_IN == enAudDir)
   {
      enAudioDataSet = (e8AUD_SAMPLERATE_8KHZ == enSamplingRate) ? e32_AUDIOIN_DATASET_PHONE_NB
            : e32_AUDIOIN_DATASET_PHONE_WB;
   }
   else if (e8AUD_VR_IN == enAudDir)
   {
      enAudioDataSet = (e8AUD_SAMPLERATE_8KHZ == enSamplingRate) ? e32_AUDIOIN_DATASET_VR_NB
            : e32_AUDIOIN_DATASET_VR_WB;
   }
   else if (e8AUD_DEFAULT == enAudDir)
   {
      //! Default audio type is without ECNR
      enAudioDataSet = e32_AUDIOIN_DATASET_VR_MIC_ONLY;
   }

   m_oAudioInLock.s16Lock();
   //Call ECNR vSetAudioConfig()
   if ((e8_AUDIOIN_INITIALIZED == m_enAudioInState) &&
      ((e8AUD_PHONE_IN == enAudDir) || (e8AUD_VR_IN == enAudDir) || (e8AUD_DEFAULT == enAudDir)) &&
      (NULL != m_poAudioInHandler))
   {
      m_poAudioInHandler->vSetAudioInConfig(enAudioDataSet);
   }
   m_oAudioInLock.vUnlock();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAudio::vRestoreLastMainAudSrc()
***************************************************************************/
t_Void spi_tclAudio::vRestoreLastMediaAudSrc()
{
   ETG_TRACE_USR1((" spi_tclAudio::vRestoreLastMainAudSrc"));

   if (NULL != m_poAudioPolicyBase)
   {
      m_poAudioPolicyBase->vRestoreLastMediaAudSrc();
   }//End of if (NULL != m_poAudioPolicyBase)
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudio::bOnReqAVDeActivationResult()
***************************************************************************/
t_Bool spi_tclAudio::bOnReqAVDeActivationResult(const t_U8 cou8SourceNum)
{
   ETG_TRACE_USR1((" spi_tclAudio::bOnReqAVDeActivationResult: u8SourceNum = %d", cou8SourceNum));
   m_oAudioSrcInfo.vSetActiveSrcFlag(cou8SourceNum, false);
   return true;
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclAudio::vOnAudioError()
***************************************************************************/
t_Void spi_tclAudio::vOnAudioError(const t_U8 cou8SourceNum, tenAudioError enAudioError)
{
   ETG_TRACE_USR1((" spi_tclAudio::vOnAudioError: u8SourceNum = %d enAudioError =%d ",
         cou8SourceNum, ETG_ENUM(AUDIO_ERROR,enAudioError)));

   t_U8 u8DevCategory =
            static_cast<t_U8> (m_oAudioSrcInfo.enGetDeviceCategory(cou8SourceNum));

   t_U32 u32DevID = m_oAudioSrcInfo.u32GetDeviceID(cou8SourceNum);
   SPI_INTENTIONALLY_UNUSED(u32DevID);
   tenAudioDir enAudDir = m_oAudioSrcInfo.enGetAudDirection(cou8SourceNum);

   if ((NULL != m_poAudDevHandlers[u8DevCategory]))
   {
      m_poAudDevHandlers[u8DevCategory]->vOnAudioError(enAudDir, enAudioError);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAudio::vSendAudioStatusChange(...)
 ***************************************************************************/
t_Void spi_tclAudio::vSendAudioStatusChange(tenAudioStatus enAudioStatus)
{
   //! Forward audio status to policy
   if (NULL != m_poAudioPolicyBase)
   {
      m_poAudioPolicyBase->vSendAudioStatusChange(enAudioStatus);
   }
}
//lint –restore

///////////////////////////////////////////////////////////////////////////////
// <EOF>
