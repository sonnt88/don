/*!
 *******************************************************************************
 * \file             spi_tclMediator.h
 * \brief            Mediator to distribute messages among SPI components
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Mediator to distribute messages among SPI components
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclMediator.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclMediator.cpp.trc.h"
#endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 ** FUNCTION:  spi_tclMediator::~spi_tclMediator
 ***************************************************************************/
spi_tclMediator::~spi_tclMediator()
{

}
/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vRegisterCallbacks(const trConnMngrCallback &rfrConnMngrCb)
 ***************************************************************************/
t_Void spi_tclMediator::vRegisterCallbacks(const trConnMngrCallback &rfrConnMngrCb)
{
   ETG_TRACE_USR1(("spi_tclMediator::vRegisterCallbacks for device selection"));
   m_rConnMngrCb = rfrConnMngrCb;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vRegisterCallbacks
 ***************************************************************************/
t_Void spi_tclMediator::vRegisterCallbacks(const trSelectDeviceCallbacks &rSelDevCb)
{
   ETG_TRACE_USR1(("spi_tclMediator::vRegisterCallbacks for connection management"));
   m_vecrSelDevCbs.push_back(rSelDevCb);
}

/***************************************************************************
** FUNCTION:  spi_tclMediator::vRegisterCallbacks
***************************************************************************/
t_Void spi_tclMediator::vRegisterCallbacks(const trAppLauncherCallbacks& corfrAppLauncherCb)
{
   ETG_TRACE_USR1(("spi_tclMediator::vRegisterCallbacks for app launcher"));
   m_rAppLauncherCb = corfrAppLauncherCb;
}

/***************************************************************************
** FUNCTION:  spi_tclMediator::vRegisterCallbacks
***************************************************************************/
t_Void spi_tclMediator::vRegisterCallbacks(const trAAPSensorCallback& corfrDayNightModeCb)
{
   ETG_TRACE_USR1(("spi_tclMediator::vRegisterCallbacks for sensor"));
   m_rDayNightCb = corfrDayNightModeCb;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vPostSelectDeviceRes
 ***************************************************************************/
t_Void spi_tclMediator::vPostSelectDeviceRes(tenCompID enCompID, tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   std::vector<trSelectDeviceCallbacks>::iterator itCbs;
   for (itCbs = m_vecrSelDevCbs.begin(); itCbs != m_vecrSelDevCbs.end();
            itCbs++)
   {
      if (NULL != itCbs->fvOnSelectDeviceRes)
      {
         itCbs->fvOnSelectDeviceRes(enCompID, enErrorCode);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vPostDeviceDisconnection
 ***************************************************************************/
t_Void spi_tclMediator::vPostDeviceDisconnection(const t_U32 u32DeviceHandle)
{
	/*lint -esym(40,fvOnDeviceDisconnection) fvOnDeviceDisconnection Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   std::vector<trSelectDeviceCallbacks>::iterator itCbs;
   for (itCbs = m_vecrSelDevCbs.begin(); itCbs != m_vecrSelDevCbs.end();
            itCbs++)
   {
      if (NULL != itCbs->fvOnDeviceDisconnection)
      {
         itCbs->fvOnDeviceDisconnection(u32DeviceHandle);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vPostDeviceConnection
 ***************************************************************************/
t_Void spi_tclMediator::vPostDeviceConnection(const t_U32 u32DeviceHandle)
{
	/*lint -esym(40,fvOnDeviceConnection) fvOnDeviceConnection Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   std::vector<trSelectDeviceCallbacks>::iterator itCbs;
   for (itCbs = m_vecrSelDevCbs.begin(); itCbs != m_vecrSelDevCbs.end();
            itCbs++)
   {
      if (NULL != itCbs->fvOnDeviceConnection)
      {
         itCbs->fvOnDeviceConnection(u32DeviceHandle);
      }
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMediator::vPostLaunchAppResult
***************************************************************************/
t_Void spi_tclMediator::vPostLaunchAppResult(const tenCompID coenCompID,
                                             const t_U32 cou32DevId,
                                             const t_U32 cou32AppId,
                                             const tenDiPOAppType coenDiPOAppType,
                                             tenErrorCode enErrorCode,
                                             const trUserContext& rfrcUsrCntxt)
{
	/*lint -esym(40,fpvLaunchAppResult) fpvLaunchAppResult Undeclared identifier */
    ETG_TRACE_USR1(("spi_tclMediator::vPostLaunchAppResult:Dev-0x%x App-0x%x",
      cou32DevId,cou32AppId));

   if(NULL != m_rAppLauncherCb.fpvLaunchAppResult)
   {
      (m_rAppLauncherCb.fpvLaunchAppResult)(coenCompID,cou32DevId,cou32AppId,
         coenDiPOAppType,enErrorCode,rfrcUsrCntxt);
   }//if(NULL != m_rAppLauncherCb.fpvLaunchAppResult)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclMediator::vPostTerminateAppResult
***************************************************************************/
t_Void spi_tclMediator::vPostTerminateAppResult(const tenCompID coenCompID,
                                                const t_U32 cou32DevId,
                                                const t_U32 cou32AppId,
                                                tenErrorCode enErrorCode,
                                                const trUserContext& rfrCUsrCntxt)
{
	/*lint -esym(40,fpvTerminateAppResult) fpvTerminateAppResult Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclMediator::vPostTerminateAppResult:Dev-0x%x App-0x%x",
      cou32DevId,cou32AppId));

   if(NULL != m_rAppLauncherCb.fpvTerminateAppResult)
   {
      (m_rAppLauncherCb.fpvTerminateAppResult)(coenCompID,cou32DevId,cou32AppId,
         enErrorCode,rfrCUsrCntxt);
   }//if(NULL != m_rAppLauncherCb.fpvTerminateAppResult)
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vPostAutoDeviceSelection
 ***************************************************************************/
t_Void spi_tclMediator::vPostAutoDeviceSelection(const t_U32 cou32DeviceHandle,
         tenDeviceConnectionReq enConnReq)
{
	/*lint -esym(40,fvAutoSelectDevice) fvAutoSelectDevice Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   std::vector<trSelectDeviceCallbacks>::iterator itCbs;
   for (itCbs = m_vecrSelDevCbs.begin(); itCbs != m_vecrSelDevCbs.end();
            itCbs++)
   {
      if (NULL != itCbs->fvAutoSelectDevice)
      {
         itCbs->fvAutoSelectDevice(cou32DeviceHandle,
                  enConnReq, e8DEV_TYPE_UNKNOWN);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vPostSetUserDeselect
 ***************************************************************************/
t_Void spi_tclMediator::vPostSetUserDeselect(const t_U32 cou32DeviceHandle)
{
	/*lint -esym(40,fvSetUserDeselect) fvSetUserDeselect Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   std::vector<trSelectDeviceCallbacks>::iterator itCbs;
   for (itCbs = m_vecrSelDevCbs.begin(); itCbs != m_vecrSelDevCbs.end();
            itCbs++)
   {
      if (NULL != itCbs->fvSetUserDeselect)
      {
         itCbs->fvSetUserDeselect(cou32DeviceHandle);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vPostBTDeviceName
 ***************************************************************************/
t_Void spi_tclMediator::vPostBTDeviceName(t_U32 u32DeviceHandle,
         t_String szBTDeviceName)
{
   /*lint -esym(40,fvSetBTDeviceName) fvSetBTDeviceName Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != m_rConnMngrCb.fvSetBTDeviceName)
   {
      m_rConnMngrCb.fvSetBTDeviceName(u32DeviceHandle, szBTDeviceName);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vApplySelectionStrategy
 ***************************************************************************/
t_Void spi_tclMediator::vApplySelectionStrategy()
{

	/*lint -esym(40,fvApplySelStrategy)fvApplySelStrategy Undeclared identifier */
   ETG_TRACE_USR1((" vApplySelectionStrategy entered \n"));
   std::vector<trSelectDeviceCallbacks>::iterator itCbs;
   for (itCbs = m_vecrSelDevCbs.begin(); itCbs != m_vecrSelDevCbs.end();
            itCbs++)
   {
      if (NULL != itCbs->fvApplySelStrategy)
      {
         itCbs->fvApplySelStrategy();
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::vPostDayNightMode
 ***************************************************************************/
t_Void spi_tclMediator::vPostDayNightMode(tenVehicleConfiguration enVehicleConfig)
{

	/*lint -esym(40,fvSetDayNightMode)fvSetDayNightMode Undeclared identifier */
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   if (NULL != m_rDayNightCb.fvSetDayNightMode)
   {
      m_rDayNightCb.fvSetDayNightMode(enVehicleConfig);
   }
}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMediator::spi_tclMediator
 ***************************************************************************/
spi_tclMediator::spi_tclMediator()
{

}
//lint –restore
