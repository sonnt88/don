/*!
 *******************************************************************************
 * \file             spi_tclFactory.cpp
 * \brief            Object Factory class responsible for creation of SPI Objects
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Object Factory class responsible for creation of SPI Objects
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.01.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 13.02.2014 |  Shihabudheen P M            | Added 1.poGetMainAppInstance().
                                                  2.vInitialize()
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
 10.06.2014 |  Ramya Murthy                | Audio policy redesign implementation.
 31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()
 05.06.1990| Tejaswini HB                  | Added Lint comments to suppress C++11 errors
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclRespInterface.h"
#include "spi_tclFactory.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclFactory.cpp.trc.h"
#endif
#endif

#define LOAD_SETTINGS(COMPONENT_PTR, FEATURESUPPLIST) \
   if (NULL != COMPONENT_PTR)                         \
   {                                                  \
      COMPONENT_PTR->vLoadSettings(FEATURESUPPLIST);  \
   }

#define SAVE_SETTINGS(COMPONENT_PTR)   \
   if (NULL != COMPONENT_PTR)          \
   {                                   \
      COMPONENT_PTR->vSaveSettings();  \
   }



//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	


/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclFactory::~spi_tclFactory
 ***************************************************************************/
spi_tclFactory::~spi_tclFactory()
{
   ETG_TRACE_USR1(("spi_tclFactory::~spi_tclFactory() entered\n"));

   m_poMainApp       = NULL;
   m_poRespInterface = NULL;
   m_poConnMngr      = NULL;
   m_poVideo         = NULL;
   m_poAppMngr       = NULL;
   m_poAudio         = NULL;
   m_poInputHandler  = NULL;
   m_poBluetooth     = NULL;
   m_poDataService   = NULL;
   m_poRsrcMngr      = NULL;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclFactory::bInitialize()
 ***************************************************************************/
t_Bool spi_tclFactory::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclFactory::bInitialize() entered\n"));

   //! Create component managers
   if ((NULL != m_poRespInterface) && (NULL != m_poMainApp))
   {
      m_poConnMngr = new spi_tclConnMngr(m_poRespInterface);
      SPI_NORMAL_ASSERT(NULL== m_poConnMngr);

      m_poAppMngr = new spi_tclAppMngr(m_poRespInterface);
      SPI_NORMAL_ASSERT(NULL == m_poAppMngr);

      m_poVideo = new spi_tclVideo(m_poMainApp, m_poRespInterface);
      SPI_NORMAL_ASSERT(NULL == m_poVideo);

      m_poAudio = new spi_tclAudio(m_poRespInterface);
      SPI_NORMAL_ASSERT(NULL == m_poAudio);

      m_poInputHandler = new spi_tclInputHandler(m_poRespInterface);
      SPI_NORMAL_ASSERT(NULL == m_poInputHandler);

      m_poBluetooth = new spi_tclBluetooth(m_poMainApp, m_poRespInterface);
      SPI_NORMAL_ASSERT(NULL == m_poBluetooth);

      m_poDataService = new spi_tclDataService(m_poMainApp,m_poRespInterface);
      SPI_NORMAL_ASSERT(NULL == m_poDataService);
	  
      m_poRsrcMngr = new spi_tclResourceMngr(m_poRespInterface);
      SPI_NORMAL_ASSERT(NULL == m_poRsrcMngr);
   }

   //! Initialize component managers
   t_Bool bInit = ((NULL != m_poConnMngr)
         && (NULL != m_poAppMngr) && (NULL != m_poVideo)
         && (NULL != m_poAudio) && (NULL != m_poInputHandler)
         && (NULL != m_poBluetooth) && (NULL != m_poDataService)
         && (NULL != m_poRsrcMngr)
         && (m_poConnMngr->bInitialize())
         && (m_poAppMngr->bInitialize())
         && (m_poVideo->bInitialize())
         && (m_poAudio->bInitialize())
         && (m_poInputHandler->bInitialize())
         && (m_poBluetooth->bInitialize())
         && (m_poDataService->bInitialize())
		   && (m_poRsrcMngr->bInitialize()));

   return bInit;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclFactory::bUnInitialize()
 ***************************************************************************/
t_Bool spi_tclFactory::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclFactory::bUnInitialize() entered\n"));

   //! Uninitialize component managers
   t_Bool bUnInit = ((NULL != m_poConnMngr)
         && (NULL != m_poAppMngr) && (NULL != m_poVideo)
         && (NULL != m_poAudio) && (NULL != m_poInputHandler)
         && (NULL != m_poBluetooth) && (NULL != m_poDataService)
         && (NULL != m_poRsrcMngr)
         && (m_poDataService->bUnInitialize())
         && (m_poBluetooth->bUnInitialize())
         && (m_poInputHandler->bUnInitialize())
         && (m_poAudio->bUnInitialize())
         && (m_poVideo->bUnInitialize())
         && (m_poAppMngr->bUnInitialize())
         && (m_poConnMngr->bUnInitialize())
         && (m_poRsrcMngr->bUnInitialize()));

   //! Release resources
   RELEASE_MEM(m_poRsrcMngr);
   RELEASE_MEM(m_poDataService);
   RELEASE_MEM(m_poBluetooth);
   RELEASE_MEM(m_poInputHandler);
   RELEASE_MEM(m_poAudio);
   RELEASE_MEM(m_poVideo);
   RELEASE_MEM(m_poAppMngr);
   RELEASE_MEM(m_poConnMngr);

   return bUnInit;
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclFactory::vLoadSettings(const trSpiFeatureSupport&...)
 ***************************************************************************/
t_Void spi_tclFactory::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   LOAD_SETTINGS(m_poConnMngr, rfcrSpiFeatureSupp);
   LOAD_SETTINGS(m_poAppMngr, rfcrSpiFeatureSupp);
   LOAD_SETTINGS(m_poVideo, rfcrSpiFeatureSupp);
   LOAD_SETTINGS(m_poAudio, rfcrSpiFeatureSupp);
   LOAD_SETTINGS(m_poInputHandler, rfcrSpiFeatureSupp);
   LOAD_SETTINGS(m_poBluetooth, rfcrSpiFeatureSupp);
   LOAD_SETTINGS(m_poDataService, rfcrSpiFeatureSupp);
   LOAD_SETTINGS(m_poRsrcMngr, rfcrSpiFeatureSupp);
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclFactory::vSaveSettings()
 ***************************************************************************/
t_Void spi_tclFactory::vSaveSettings()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   SAVE_SETTINGS(m_poConnMngr);
   SAVE_SETTINGS(m_poAppMngr);
   SAVE_SETTINGS(m_poVideo);
   SAVE_SETTINGS(m_poAudio);
   SAVE_SETTINGS(m_poInputHandler);
   SAVE_SETTINGS(m_poBluetooth);
   SAVE_SETTINGS(m_poDataService);
   SAVE_SETTINGS(m_poRsrcMngr);
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnMngr* spi_tclFactory::poGetConnMngrInstance
 ***************************************************************************/
spi_tclConnMngr* spi_tclFactory::poGetConnMngrInstance()
{
   return m_poConnMngr;
}

/***************************************************************************
 ** FUNCTION:  spi_tclVideo* spi_tclFactory::poGetVideoInstance
 ***************************************************************************/
spi_tclVideo* spi_tclFactory::poGetVideoInstance()
{
   return m_poVideo;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLAppMngr* spi_tclFactory::poGetAppManagerInstance
 ***************************************************************************/
spi_tclAppMngr* spi_tclFactory::poGetAppManagerInstance()
{
   return m_poAppMngr;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAudio* spi_tclFactory::poGetAudioInstance
 ***************************************************************************/
spi_tclAudio* spi_tclFactory::poGetAudioInstance()
{
   return m_poAudio;
}

/***************************************************************************
 ** FUNCTION:  spi_tclInputHandler* spi_tclFactory::poGetInputHandlerInstance
 ***************************************************************************/
spi_tclInputHandler* spi_tclFactory::poGetInputHandlerInstance()
{
   return m_poInputHandler;
}

/***************************************************************************
** FUNCTION:  spi_tclBluetooth* spi_tclFactory::poGetBluetoothInstance
***************************************************************************/
spi_tclBluetooth* spi_tclFactory::poGetBluetoothInstance()
{
   return m_poBluetooth;
}

/***************************************************************************
** FUNCTION:  spi_tclDataService* spi_tclFactory::poGetDataServiceInstance
***************************************************************************/
spi_tclDataService* spi_tclFactory::poGetDataServiceInstance()
{
   return m_poDataService;
}

/***************************************************************************
** FUNCTION:  ahl_tclBaseOneThreadApp* spi_tclFactory::poGetMainAppInstance
***************************************************************************/
ahl_tclBaseOneThreadApp* spi_tclFactory::poGetMainAppInstance()
{
   return m_poMainApp;
}	 
 
/***************************************************************************
 ** FUNCTION:  spi_tclFactory::poGetRsrcMngrInstance()
 ***************************************************************************/
spi_tclResourceMngr* spi_tclFactory::poGetRsrcMngrInstance()
{
   return m_poRsrcMngr;
}

/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/
spi_tclFactory::spi_tclFactory() :
   m_poRespInterface(NULL), m_poConnMngr(NULL), m_poVideo(NULL),
   m_poAppMngr(NULL), m_poAudio(NULL), m_poInputHandler(NULL),
   m_poBluetooth(NULL), m_poDataService(NULL), m_poMainApp(NULL), m_poRsrcMngr(NULL)
{
   ETG_TRACE_USR1(("spi_tclFactory::spi_tclFactory() entered\n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclFactory::spi_tclFactory
 ***************************************************************************/
spi_tclFactory::spi_tclFactory(spi_tclRespInterface* poRespInterface,
         ahl_tclBaseOneThreadApp* poMainAppl) :
   m_poRespInterface(poRespInterface), m_poConnMngr(NULL), m_poVideo(NULL),
   m_poAppMngr(NULL), m_poAudio(NULL), m_poInputHandler(NULL),
   m_poBluetooth(NULL), m_poDataService(NULL), m_poMainApp(poMainAppl),  m_poRsrcMngr(NULL)
{
   ETG_TRACE_USR1(("spi_tclFactory::spi_tclFactory(..) entered\n"));
}

//lint –restore