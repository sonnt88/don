/***********************************************************************/
/*!
* \file  spi_tclCmdMsgQThreadable.h
* \brief implements MsgQthreader for SPI Commands
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    implements MsgQthreader for SPI Commands
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.03.2014  | Pruthvi Thej Nagaraju | Initial Version

\endverbatim
*************************************************************************/

#ifndef SPI_TCLCMDMSGQTHREADER_H_
#define SPI_TCLCMDMSGQTHREADER_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "MsgQThreadable.h"
#include "BaseTypes.h"


using namespace shl::thread;

//!Forward declarations
class spi_tclCmdDispatcher;

/****************************************************************************/
/*!
* \class spi_tclCmdMsgQThreadable
* \brief implements threading based on MsgQthreader for VNC Wrappers
*
* Responsible for calling the respective dispatchers and to allocate memory
* when a message arrives on Q
*
****************************************************************************/

class spi_tclCmdMsgQThreadable : public MsgQThreadable
{
   public:

      /***************************************************************************
      ** FUNCTION:  spi_tclCmdMsgQThreadable::spi_tclCmdMsgQThreadable()
      ***************************************************************************/
      /*!
      * \fn      spi_tclCmdMsgQThreadable()
      * \brief   Default Constructor
      * \sa      ~spi_tclCmdMsgQThreadable()
      **************************************************************************/
      spi_tclCmdMsgQThreadable();

      /***************************************************************************
      ** FUNCTION:  spi_tclCmdMsgQThreadable::~spi_tclCmdMsgQThreadable()
      ***************************************************************************/
      /*!
      * \fn      ~spi_tclCmdMsgQThreadable()
      * \brief   Destructor
      * \sa      spi_tclCmdMsgQThreadable()
      **************************************************************************/
      ~spi_tclCmdMsgQThreadable();

   protected:
      /***************************************************************************
      ** FUNCTION:  spi_tclCmdMsgQThreadable::vExecute
      ***************************************************************************/
      /*!
      * \fn      t_Void vExecute(tShlMessage *poMessage)
      * \brief   Responsible for posting the message to respective dispatchers
      * \param   poMessage : message received from MsgQ for dispatching
      **************************************************************************/
      virtual t_Void vExecute(tShlMessage *poMessage);

      /***************************************************************************
      ** FUNCTION:  spi_tclCmdMsgQThreadable::tShlMessage* poGetMsgBuffer(size_t )
      ***************************************************************************/
      /*!
      * \fn      tShlMessage* poGetMsgBuffer(size_t )
      * \brief  This function will be called for requesting the storage allocation for received
      *           message
      * \param siBuffer: size of the buffer to be allocated for the received message
      **************************************************************************/
      virtual tShlMessage* poGetMsgBuffer(size_t siBuffer);

   private:

      //! Pointers to Message Dispatchers
      spi_tclCmdDispatcher *m_poCmdDispatcher;
};


#endif /* SPI_TCLCMDMSGQTHREADER_H_ */
