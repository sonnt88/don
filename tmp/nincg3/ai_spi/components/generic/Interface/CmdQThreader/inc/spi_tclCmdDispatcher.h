/***********************************************************************/
/*!
 * \file  spi_tclCmdDispatcher.h
 * \brief Message Dispatcher for SPI Command Messages.
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for SPI Command Messages. Implemented using
 double dispatch mechanism
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                			| Modification
 18.03.2014  | Pruthvi Thej Nagaraju 			| Initial Version
 10.03.2016  | Rachana L Achar                  | Added dispatcher for AAP Notification event


 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLCMDDISPATCHER_H_
#define SPI_TCLCMDDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"

/**************Forward Declarations******************************************/
class spi_tclCmdDispatcher;
class spi_tclRespInterface;
class ahl_tclBaseOneThreadApp;
class spi_tclDeviceSelector;
class spi_tclAppLauncher;
class spi_tclAppMngr;
class spi_tclConnMngr;
class spi_tclVideo;
class spi_tclAudio;
class spi_tclInputHandler;
class spi_tclBluetooth;
class spi_tclResourceMngr;
class spi_tclDataService;

/*! \note : Dynamic memory allocation is required where non basic types such as
 *  STL containers are used because these types are not allocated on stack and hence
 *  data held by these types are not passed via message queue. To avoid illegal
 *  memory access, such types can be allocated memory in vAllocateMsg() and released
 *  in vDeAllocateMsg(). If only basic types are used, these function can be left
 *  blank.
 */
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/****************************************************************************/
/*! \class CmdSelectDevice
 * \sa spi_tclCmdInterface::vSelectDevice
 ****************************************************************************/
class CmdSelectDevice: public trMsgBase
{
   public:
      spi_tclDeviceSelector* m_pDeviceSelector;
      t_U32 m_u32DeviceHandle;
      tenDeviceConnectionType m_enDevConnType;
      tenDeviceConnectionReq m_enDevConnReq;
      tenEnabledInfo m_enDAPUsage;
      tenEnabledInfo m_enCDBUsage;
      tenDeviceCategory m_enDevCat;
      t_Bool m_bIsHMITrigger;

      /***************************************************************************
       ** FUNCTION:  CmdSelectDevice::CmdSelectDevice
       ***************************************************************************/
      /*!
       * \fn      CmdSelectDevice()
       * \brief   Default constructor
       **************************************************************************/
      CmdSelectDevice();

      /***************************************************************************
       ** FUNCTION:  CmdSelectDevice::CmdSelectDevice
       ***************************************************************************/
      /*!
       * \fn      CmdSelectDevice
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdSelectDevice(spi_tclDeviceSelector* m_pDeviceSelector, t_U32 u32DeviceHandle,
               tenDeviceConnectionType enDevConnType, tenDeviceConnectionReq enDevConnReq,
               tenEnabledInfo enDAPUsage, tenEnabledInfo enCDBUsage, tenDeviceCategory enDevCat,
               t_Bool bIsHMITrigger);

      /***************************************************************************
       ** FUNCTION:  CmdSelectDevice::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdSelectDevice::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdSelectDevice::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdLaunchApp
 * \sa spi_tclCmdInterface::vLaunchApp
 ****************************************************************************/
class CmdLaunchApp: public trMsgBase
{
   public:
      spi_tclAppLauncher *m_pAppLauncher;
      t_U32 m_u32DeviceHandle;
      t_U32 m_u32AppHandle;
      tenDiPOAppType m_enDiPOAppType;
      t_String *m_pszTelephoneNumber;
      tenEcnrSetting m_enEcnrSetting;

      /***************************************************************************
       ** FUNCTION:  CmdLaunchApp::CmdLaunchApp
       ***************************************************************************/
      /*!
       * \fn      CmdLaunchApp()
       * \brief   Default constructor
       **************************************************************************/
      CmdLaunchApp();

      /***************************************************************************
       ** FUNCTION:  CmdLaunchApp::CmdLaunchApp
       ***************************************************************************/
      /*!
       * \fn      CmdLaunchApp()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdLaunchApp(spi_tclAppLauncher *pAppLauncher, t_U32 u32DeviceHandle, t_U32 u32AppHandle,
               tenDiPOAppType enDiPOAppType, t_String szTelephoneNumber,
               tenEcnrSetting enEcnrSetting);

      /***************************************************************************
       ** FUNCTION:  CmdLaunchApp::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdLaunchApp::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdLaunchApp::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*! \class CmdTerminateApp
 * \sa spi_tclCmdInterface::vTerminateApp
 ****************************************************************************/
class CmdTerminateApp: public trMsgBase
{
   public:
      spi_tclAppLauncher *m_pAppLauncher;
      t_U32 m_u32DeviceHandle;
      t_U32 m_u32AppHandle;

      /***************************************************************************
       ** FUNCTION:  CmdTerminateApp::CmdTerminateApp
       ***************************************************************************/
      /*!
       * \fn      CmdTerminateApp()
       * \brief   Default constructor
       **************************************************************************/
      CmdTerminateApp();

      /***************************************************************************
       ** FUNCTION:  CmdTerminateApp::CmdTerminateApp
       ***************************************************************************/
      /*!
       * \fn      CmdTerminateApp()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdTerminateApp(spi_tclAppLauncher *pAppLauncher, t_U32 u32DeviceHandle, t_U32 u32AppHandle);

      /***************************************************************************
       ** FUNCTION:  CmdTerminateApp::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdTerminateApp::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdTerminateApp::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdAppIconData
 * \sa spi_tclCmdInterface::vGetAppIconData
 ****************************************************************************/
class CmdAppIconData: public trMsgBase
{
   public:
      spi_tclAppMngr *m_pAppMngr;
      tenDeviceCategory m_enDevCat;
      t_String *m_pszAppIconURL;

      /***************************************************************************
       ** FUNCTION:  CmdAppIconData::CmdAppIconData
       ***************************************************************************/
      /*!
       * \fn      CmdAppIconData()
       * \brief   Default constructor
       **************************************************************************/
      CmdAppIconData();

      /***************************************************************************
       ** FUNCTION:  CmdAppIconData::CmdAppIconData
       ***************************************************************************/
      /*!
       * \fn      CmdAppIconData()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAppIconData(spi_tclAppMngr *pAppMngr, tenDeviceCategory enDevCat, t_String szAppIconURL);

      /***************************************************************************
       ** FUNCTION:  CmdAppIconData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAppIconData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdAppIconData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*! \class CmdAppIconAttributes
 * \sa spi_tclCmdInterface::vSetAppIconAttributes
 ****************************************************************************/
class CmdAppIconAttributes: public trMsgBase
{
   public:
      spi_tclAppMngr *m_pAppMngr;
      t_U32 m_u32DeviceHandle;
      t_U32 m_u32AppHandle;
      tenDeviceCategory m_enDevCat;
      trIconAttributes *m_prIconAttributes;

      /***************************************************************************
       ** FUNCTION:  CmdAppIconAttributes::CmdAppIconAttributes
       ***************************************************************************/
      /*!
       * \fn      CmdAppIconAttributes()
       * \brief   Default constructor
       **************************************************************************/
      CmdAppIconAttributes();

      /***************************************************************************
       ** FUNCTION:  CmdAppIconAttributes::CmdAppIconAttributes
       ***************************************************************************/
      /*!
       * \fn      CmdAppIconAttributes()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAppIconAttributes(spi_tclAppMngr *pAppMngr, t_U32 u32DeviceHandle, t_U32 u32AppHandle,
               tenDeviceCategory enDevCat, const trIconAttributes &rfrIconAttributes);

      /***************************************************************************
       ** FUNCTION:  CmdAppIconAttributes::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAppIconAttributes::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdAppIconAttributes::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*! \class CmdDeviceUsagePreference
 * \sa spi_tclCmdInterface::vSetDeviceUsagePreference
 ****************************************************************************/
class CmdDeviceUsagePreference: public trMsgBase
{
   public:
      spi_tclConnMngr *m_poConnMngr;
      t_U32 m_u32DeviceHandle;
      tenDeviceCategory m_enDeviceCategory;
      tenEnabledInfo m_enEnabledInfo;

      /***************************************************************************
       ** FUNCTION:  CmdDeviceUsagePreference::CmdDeviceUsagePreference
       ***************************************************************************/
      /*!
       * \fn      CmdDeviceUsagePreference()
       * \brief   Default constructor
       **************************************************************************/
      CmdDeviceUsagePreference();

      /***************************************************************************
       ** FUNCTION:  CmdDeviceUsagePreference::CmdDeviceUsagePreference
       ***************************************************************************/
      /*!
       * \fn      CmdDeviceUsagePreference()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdDeviceUsagePreference(spi_tclConnMngr *poConnMngr, t_U32 u32DeviceHandle,
               tenDeviceCategory enDeviceCategory, tenEnabledInfo enEnabledInfo);

      /***************************************************************************
       ** FUNCTION:  CmdDeviceUsagePreference::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdDeviceUsagePreference::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdDeviceUsagePreference::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdMLNotificationEnabledInfo
 * \sa spi_tclCmdInterface::vSetMLNotificationEnabledInfo
 ****************************************************************************/
class CmdMLNotificationEnabledInfo: public trMsgBase
{
   public:
      spi_tclAppMngr *m_pAppMngr;
      t_U32 m_u32DeviceHandle;
      t_U32 m_u32NumNotificationEnableList;
      tenDeviceCategory m_enDevCat;
      std::vector<trNotiEnable> *m_pvecrNotificationEnableList;

      /***************************************************************************
       ** FUNCTION:  CmdMLNotificationEnabledInfo::CmdMLNotificationEnabledInfo
       ***************************************************************************/
      /*!
       * \fn      CmdMLNotificationEnabledInfo()
       * \brief   Default constructor
       **************************************************************************/
      CmdMLNotificationEnabledInfo();

      /***************************************************************************
       ** FUNCTION:  CmdMLNotificationEnabledInfo::CmdMLNotificationEnabledInfo
       ***************************************************************************/
      /*!
       * \fn      CmdMLNotificationEnabledInfo()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdMLNotificationEnabledInfo(spi_tclAppMngr *poAppMngr,
               t_U32 u32DeviceHandle, t_U32 u32NumNotificationEnableList,
               tenDeviceCategory enDevCat,
               std::vector<trNotiEnable> vecrNotificationEnableList);

      /***************************************************************************
       ** FUNCTION:  CmdMLNotificationEnabledInfo::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdMLNotificationEnabledInfo::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdMLNotificationEnabledInfo::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*! \class CmdSetMLNotificationOnOff
 * \sa spi_tclCmdInterface::vSetMLNotificationOnOff
 ****************************************************************************/
class CmdSetMLNotificationOnOff: public trMsgBase
{
   public:

      spi_tclAppMngr *m_poAppMngr;
      t_Bool m_bNotificationState;

      /***************************************************************************
       ** FUNCTION:  CmdSetMLNotificationOnOff::CmdSetMLNotificationOnOff
       ***************************************************************************/
      /*!
       * \fn      CmdSetMLNotificationOnOff()
       * \brief   Default constructor
       **************************************************************************/
      CmdSetMLNotificationOnOff();

      /***************************************************************************
       ** FUNCTION:  CmdSetMLNotificationOnOff::CmdSetMLNotificationOnOff
       ***************************************************************************/
      /*!
       * \fn      CmdSetMLNotificationOnOff()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdSetMLNotificationOnOff(spi_tclAppMngr *poAppMngr, t_Bool bNotificationState);

      /***************************************************************************
       ** FUNCTION:  CmdSetMLNotificationOnOff::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdSetMLNotificationOnOff::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdSetMLNotificationOnOff::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*! \class CmdInvokeNotificationAction
 * \sa spi_tclCmdInterface::vInvokeNotificationAction
 ****************************************************************************/
class CmdInvokeNotificationAction: public trMsgBase
{
   public:
      spi_tclAppMngr *m_poAppMngr;
      t_U32 m_u32DeviceHandle;
      t_U32 m_u32AppHandle;
      t_U32 m_u32NotificationID;
      t_U32 m_u32NotificationActionID;
      tenDeviceCategory m_enDevCat;

      /***************************************************************************
       ** FUNCTION:  CmdInvokeNotificationAction::CmdInvokeNotificationAction
       ***************************************************************************/
      /*!
       * \fn      CmdInvokeNotificationAction()
       * \brief   Default constructor
       **************************************************************************/
      CmdInvokeNotificationAction();

      /***************************************************************************
       ** FUNCTION:  CmdInvokeNotificationAction::CmdInvokeNotificationAction
       ***************************************************************************/
      /*!
       * \fn      CmdInvokeNotificationAction()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdInvokeNotificationAction(spi_tclAppMngr* poAppMngr,t_U32 u32DeviceHandle, t_U32 u32AppHandle,
               t_U32 u32NotificationID, t_U32 u32NotificationActionID,tenDeviceCategory enDevCat);

      /***************************************************************************
       ** FUNCTION:  CmdInvokeNotificationAction::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdInvokeNotificationAction::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdInvokeNotificationAction::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdOrientationMode
 * \sa spi_tclCmdInterface::vSetOrientationMode
 ****************************************************************************/
class CmdOrientationMode: public trMsgBase
{
   public:
      spi_tclVideo *m_poVideo;
      t_U32 m_u32DeviceHandle;
      tenDeviceCategory m_enDevCat;
      tenOrientationMode m_enOrientationMode;

      /***************************************************************************
       ** FUNCTION:  CmdOrientationMode::CmdOrientationMode
       ***************************************************************************/
      /*!
       * \fn      CmdOrientationMode()
       * \brief   Default constructor
       **************************************************************************/
      CmdOrientationMode();

      /***************************************************************************
       ** FUNCTION:  CmdOrientationMode::CmdOrientationMode
       ***************************************************************************/
      /*!
       * \fn      CmdOrientationMode()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdOrientationMode(spi_tclVideo *poVideo, t_U32 u32DeviceHandle, tenDeviceCategory enDevCat,
               tenOrientationMode enOrientationMode);

      /***************************************************************************
       ** FUNCTION:  CmdOrientationMode::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdOrientationMode::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdOrientationMode::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdScreenSize
 * \sa spi_tclCmdInterface::vSetScreenSize
 ****************************************************************************/
class CmdScreenSize: public trMsgBase
{
   public:

      spi_tclVideo *m_poVideo;
      tenDeviceCategory m_enDevCat;
      trScreenAttributes m_rScreenAttributes;

      /***************************************************************************
       ** FUNCTION:  CmdScreenSize::CmdScreenSize
       ***************************************************************************/
      /*!
       * \fn      CmdScreenSize()
       * \brief   Default constructor
       **************************************************************************/
      CmdScreenSize();

      /***************************************************************************
       ** FUNCTION:  CmdScreenSize::CmdScreenSize
       ***************************************************************************/
      /*!
       * \fn      CmdScreenSize()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdScreenSize(spi_tclVideo *poVideo, tenDeviceCategory enDevCat,
               const trScreenAttributes corScreenAttributes);

      /***************************************************************************
       ** FUNCTION:  CmdScreenSize::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdScreenSize::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdScreenSize::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdSetVideoBlocking
 * \sa spi_tclCmdInterface::vSetVideoBlockingMode
 ****************************************************************************/
class CmdSetVideoBlocking: public trMsgBase
{
   public:
      spi_tclVideo *m_poVideo;
      tenBlockingType m_enBlockingType;
      t_U32 m_u32DeviceHandle;
      tenDeviceCategory m_enDevCat;
      tenBlockingMode m_enBlockingMode;

      /***************************************************************************
       ** FUNCTION:  CmdSetVideoBlocking::CmdSetVideoBlocking
       ***************************************************************************/
      /*!
       * \fn      CmdSetVideoBlocking()
       * \brief   Default constructor
       **************************************************************************/
      CmdSetVideoBlocking();

      /***************************************************************************
       ** FUNCTION:  CmdSetVideoBlocking::CmdSetVideoBlocking
       ***************************************************************************/
      /*!
       * \fn      CmdSetVideoBlocking()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdSetVideoBlocking(spi_tclVideo *poVideo, tenBlockingType enBlockingType, t_U32 u32DeviceHandle,
               tenDeviceCategory enDevCat, tenBlockingMode enBlockingMode);

      /***************************************************************************
       ** FUNCTION:  CmdSetVideoBlocking::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdSetVideoBlocking::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdSetVideoBlocking::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*! \class CmdSetAudioBlocking
 * \sa spi_tclCmdInterface::vSetAudioBlockingMode
 ****************************************************************************/
class CmdSetAudioBlocking: public trMsgBase
{
   public:
      spi_tclAudio *m_poAudio;
      tenBlockingType m_enBlockingType;
      t_U32 m_u32DeviceHandle;
      tenDeviceCategory m_enDevCat;
      tenBlockingMode m_enBlockingMode;

      /***************************************************************************
       ** FUNCTION:  CmdSetAudioBlocking::CmdSetAudioBlocking
       ***************************************************************************/
      /*!
       * \fn      CmdSetAudioBlocking()
       * \brief   Default constructor
       **************************************************************************/
      CmdSetAudioBlocking();

      /***************************************************************************
       ** FUNCTION:  CmdSetAudioBlocking::CmdSetAudioBlocking
       ***************************************************************************/
      /*!
       * \fn      CmdSetAudioBlocking()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdSetAudioBlocking(spi_tclAudio *poAudio, tenBlockingType enBlockingType, t_U32 u32DeviceHandle,
               tenDeviceCategory enDevCat, tenBlockingMode enBlockingMode);

      /***************************************************************************
       ** FUNCTION:  CmdSetAudioBlocking::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdSetAudioBlocking::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdSetAudioBlocking::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*! \class CmdSetVehicleConfig
 ****************************************************************************/
class CmdSetVehicleConfig: public trMsgBase
{
   public:
      spi_tclVideo *m_poVideo;
      spi_tclAppMngr *m_poAppMngr;
      t_Bool m_bSetConfig;
      tenVehicleConfiguration m_enVehicleConfig;

      /***************************************************************************
       ** FUNCTION:  CmdSetVehicleConfig::CmdSetVehicleConfig
       ***************************************************************************/
      /*!
       * \fn      CmdSetVehicleConfig()
       * \brief   Default constructor
       **************************************************************************/
      CmdSetVehicleConfig();

      /***************************************************************************
       ** FUNCTION:  CmdSetVehicleConfig::CmdSetVehicleConfig
       ***************************************************************************/
      /*!
       * \fn      CmdSetVehicleConfig()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdSetVehicleConfig(spi_tclVideo *poVideo, spi_tclAppMngr *poAppMngr, t_Bool bSetConfig,
               tenVehicleConfiguration enVehicleConfig);

      /***************************************************************************
       ** FUNCTION:  CmdSetVehicleConfig::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdSetVehicleConfig::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdSetVehicleConfig::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdTouchEvent
 * \sa spi_tclCmdInterface::vSendTouchEvent
 ****************************************************************************/
class CmdTouchEvent: public trMsgBase
{
   public:
      spi_tclInputHandler *m_pInputHandler;
      t_U32 m_u32DeviceHandle;
      tenDeviceCategory m_enDevCat;
      trTouchData *m_prTouchData;

      /***************************************************************************
       ** FUNCTION:  CmdTouchEvent::CmdTouchEvent
       ***************************************************************************/
      /*!
       * \fn      CmdTouchEvent()
       * \brief   Default constructor
       **************************************************************************/
      CmdTouchEvent();

      /***************************************************************************
       ** FUNCTION:  CmdTouchEvent::CmdTouchEvent
       ***************************************************************************/
      /*!
       * \fn      CmdTouchEvent()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdTouchEvent(spi_tclInputHandler *pInputHandler, t_U32 u32DeviceHandle,
            tenDeviceCategory enDevCat, trTouchData &rfrTouchData);

      /***************************************************************************
       ** FUNCTION:  CmdTouchEvent::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdTouchEvent::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdTouchEvent::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*! \class CmdKeyEvent
 * \sa spi_tclCmdInterface::vSendKeyEvent
 ****************************************************************************/
class CmdKeyEvent: public trMsgBase
{
   public:
      spi_tclInputHandler *m_pInputHandler;
      t_U32 m_u32DeviceHandle;
      tenDeviceCategory m_enDevCat;
      tenKeyMode m_enKeyMode;
      tenKeyCode m_enKeyCode;

      /***************************************************************************
       ** FUNCTION:  CmdKeyEvent::CmdKeyEvent
       ***************************************************************************/
      /*!
       * \fn      CmdKeyEvent()
       * \brief   Default constructor
       **************************************************************************/
      CmdKeyEvent();

      /***************************************************************************
       ** FUNCTION:  CmdKeyEvent::CmdKeyEvent
       ***************************************************************************/
      /*!
       * \fn      CmdKeyEvent()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdKeyEvent(spi_tclInputHandler *pInputHandler, t_U32 u32DeviceHandle,
            tenDeviceCategory enDevCat, tenKeyMode enKeyMode,
            tenKeyCode enKeyCode);

      /***************************************************************************
       ** FUNCTION:  CmdKeyEvent::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdKeyEvent::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdKeyEvent::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdClientCapabilities
 * \sa spi_tclCmdInterface::vSetClientCapabilities
 ****************************************************************************/
class CmdClientCapabilities: public trMsgBase
{
   public:
      spi_tclVideo *m_poVideo;
     trClientCapabilities m_rClientCapabilities;

      /***************************************************************************
       ** FUNCTION:  CmdClientCapabilities::CmdClientCapabilities
       ***************************************************************************/
      /*!
       * \fn      CmdClientCapabilities()
       * \brief   Default constructor
       **************************************************************************/
      CmdClientCapabilities();

      /***************************************************************************
       ** FUNCTION:  CmdClientCapabilities::CmdClientCapabilities
       ***************************************************************************/
      /*!
       * \fn      CmdClientCapabilities()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdClientCapabilities(spi_tclVideo *poVideo,trClientCapabilities rClientCapabilities);

      /***************************************************************************
       ** FUNCTION:  CmdClientCapabilities::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdKeyEvent::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdKeyEvent::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*! \class CmdAccessoryDisplayContext
 * \sa spi_tclCmdInterface::vSetAccessoryDisplayContext
 ****************************************************************************/
class CmdAccessoryDisplayContext: public trMsgBase
{
   public:
      spi_tclResourceMngr *m_pResourceMngr;
      t_U32 m_u32DeviceHandle;
      t_Bool m_bDisplayFlag;
      tenDisplayContext m_enDisplayContext;

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryDisplayContext::CmdAccessoryDisplayContext
       ***************************************************************************/
      /*!
       * \fn      CmdAccessoryDisplayContext()
       * \brief   Default constructor
       **************************************************************************/
      CmdAccessoryDisplayContext();

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryDisplayContext::CmdAccessoryDisplayContext
       ***************************************************************************/
      /*!
       * \fn      CmdAccessoryDisplayContext()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAccessoryDisplayContext(spi_tclResourceMngr *pResourceMngr,
               t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
               tenDisplayContext enDisplayContext);

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryDisplayContext::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryDisplayContext::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryDisplayContext::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdAccessoryAudioContext
 ****************************************************************************/
class CmdAccessoryAudioContext: public trMsgBase
{
   public:
      spi_tclResourceMngr *m_pResourceMngr;
      t_U32 m_u32DeviceHandle;
      t_U8 m_u8AudioCntxt;
      t_Bool m_bReqFlag;

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAudioContext::CmdAccessoryAudioContext
       ***************************************************************************/
      /*!
       * \fn      CmdAccessoryAudioContext()
       * \brief   Default constructor
       **************************************************************************/
      CmdAccessoryAudioContext();

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAudioContext::CmdAccessoryAudioContext
       ***************************************************************************/
      /*!
       * \fn      CmdAccessoryAudioContext()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAccessoryAudioContext(spi_tclResourceMngr *pResourceMngr, t_U32 u32DeviceHandle,t_U8 u8AudioCntxt,
               t_Bool m_bReqFlag);

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAudioContext::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAudioContext::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAudioContext::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*! \class CmdAccessoryAppStateContext
 ****************************************************************************/
class CmdAccessoryAppStateContext: public trMsgBase
{
   public:
      spi_tclResourceMngr *m_pResourceMngr;
      tenNavAppState m_enNavAppState;
      tenPhoneAppState m_enPhoneAppState;
      tenSpeechAppState m_enSpeechAppState;

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAppStateContext::CmdAccessoryAppStateContext
       ***************************************************************************/
      /*!
       * \fn      CmdAccessoryAppStateContext()
       * \brief   Default constructor
       **************************************************************************/
      CmdAccessoryAppStateContext();

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAppStateContext::CmdAccessoryAppStateContext
       ***************************************************************************/
      /*!
       * \fn      CmdAccessoryAppStateContext()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAccessoryAppStateContext(spi_tclResourceMngr *pResourceMngr, tenSpeechAppState enSpeechAppState,
      tenPhoneAppState enPhoneAppState,
      tenNavAppState enNavAppState);

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAppStateContext::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAppStateContext::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdAccessoryAppStateContext::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdSetRegion
 * \sa spi_tclCmdInterface::vDeAllocate
 ****************************************************************************/
class CmdSetRegion: public trMsgBase
{
   public:
      spi_tclAppMngr *m_poAppMngr;
      tenRegion m_enRegion;

      /***************************************************************************
       ** FUNCTION:  CmdSetRegion::CmdSetRegion
       ***************************************************************************/
      /*!
       * \fn      CmdSetRegion()
       * \brief   Default constructor
       **************************************************************************/
      CmdSetRegion();

      /***************************************************************************
       ** FUNCTION:  CmdSetRegion::CmdSetRegion
       ***************************************************************************/
      /*!
       * \fn      CmdSetRegion()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdSetRegion(spi_tclAppMngr *poAppMngr,tenRegion enRegion);

      /***************************************************************************
       ** FUNCTION:  CmdSetRegion::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdSetRegion::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdSetRegion::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};



/****************************************************************************/
/*! \class CmdAllocateAudioRoute
 * \sa spi_tclCmdInterface::vAllocate
 ****************************************************************************/
class CmdAllocateAudioRoute: public trMsgBase
{
   public:
      spi_tclAudio *m_poAudio;
      trAudSrcInfo *m_prAudSrcInfo;
      t_U8 m_u8SourceNum;

      /***************************************************************************
       ** FUNCTION:  CmdAllocateAudioRoute::CmdAllocateAudioRoute
       ***************************************************************************/
      /*!
       * \fn      CmdAllocateAudioRoute()
       * \brief   Default constructor
       **************************************************************************/
      CmdAllocateAudioRoute();

      /***************************************************************************
       ** FUNCTION:  CmdAllocateAudioRoute::CmdAllocateAudioRoute
       ***************************************************************************/
      /*!
       * \fn      CmdAllocateAudioRoute()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAllocateAudioRoute(spi_tclAudio *poAudio,const t_U8 cou8SourceNum, trAudSrcInfo &rfrAudSrcInfo);

      /***************************************************************************
       ** FUNCTION:  CmdAllocateAudioRoute::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAllocateAudioRoute::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() ;

      /***************************************************************************
       ** FUNCTION:  CmdAllocateAudioRoute::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*! \class CmdDeAllocateAudioRoute
 * \sa spi_tclCmdInterface::vDeAllocate
 ****************************************************************************/
class CmdDeAllocateAudioRoute: public trMsgBase
{
   public:
      spi_tclAudio *m_poAudio;
      t_U8 m_u8SourceNum;

      /***************************************************************************
       ** FUNCTION:  CmdDeAllocateAudioRoute::CmdDeAllocateAudioRoute
       ***************************************************************************/
      /*!
       * \fn      CmdDeAllocateAudioRoute()
       * \brief   Default constructor
       **************************************************************************/
      CmdDeAllocateAudioRoute();

      /***************************************************************************
       ** FUNCTION:  CmdDeAllocateAudioRoute::CmdDeAllocateAudioRoute
       ***************************************************************************/
      /*!
       * \fn      CmdDeAllocateAudioRoute()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdDeAllocateAudioRoute(spi_tclAudio *poAudio,const t_U8 cou8SourceNum);

      /***************************************************************************
       ** FUNCTION:  CmdDeAllocateAudioRoute::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdDeAllocateAudioRoute::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdDeAllocateAudioRoute::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdStartAudioSrcActivity
 ****************************************************************************/
class CmdStartAudioSrcActivity: public trMsgBase
{
   public:
      spi_tclAudio *m_poAudio;
      t_U8 m_u8SourceNum;

      /***************************************************************************
       ** FUNCTION:  CmdStartAudioSrcActivity::CmdStartAudioSrcActivity
       ***************************************************************************/
      /*!
       * \fn      CmdStartAudioSrcActivity()
       * \brief   Default constructor
       **************************************************************************/
      CmdStartAudioSrcActivity();

      /***************************************************************************
       ** FUNCTION:  CmdStartAudioSrcActivity::CmdStartAudioSrcActivity
       ***************************************************************************/
      /*!
       * \fn      CmdStartAudioSrcActivity()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdStartAudioSrcActivity(spi_tclAudio *poAudio, const t_U8 cou8SourceNum);

      /***************************************************************************
       ** FUNCTION:  CmdStartAudioSrcActivity::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdStartAudioSrcActivity::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdStartAudioSrcActivity::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*! \class CmdStopAudioSrcActivity
 ****************************************************************************/
class CmdStopAudioSrcActivity: public trMsgBase
{
   public:
      spi_tclAudio *m_poAudio;
      t_U8 m_u8SourceNum;

      /***************************************************************************
       ** FUNCTION:  CmdStopAudioSrcActivity::CmdStopAudioSrcActivity
       ***************************************************************************/
      /*!
       * \fn      CmdStopAudioSrcActivity()
       * \brief   Default constructor
       **************************************************************************/
      CmdStopAudioSrcActivity();

      /***************************************************************************
       ** FUNCTION:  CmdStopAudioSrcActivity::CmdStopAudioSrcActivity
       ***************************************************************************/
      /*!
       * \fn      CmdStopAudioSrcActivity()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdStopAudioSrcActivity(spi_tclAudio *poAudio, const t_U8 cou8SourceNum);

      /***************************************************************************
       ** FUNCTION:  CmdStopAudioSrcActivity::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdStopAudioSrcActivity::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdStopAudioSrcActivity::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*! \class CmdReqAVDeactResult
 ****************************************************************************/
class CmdReqAVDeactResult: public trMsgBase
{
   public:
      spi_tclAudio *m_poAudio;
      t_U8 m_u8SourceNum;

      /***************************************************************************
       ** FUNCTION:  CmdReqAVDeactResult::CmdReqAVDeactResult
       ***************************************************************************/
      /*!
       * \fn      CmdReqAVDeactResult()
       * \brief   Default constructor
       **************************************************************************/
      CmdReqAVDeactResult();

      /***************************************************************************
       ** FUNCTION:  CmdReqAVDeactResult::CmdReqAVDeactResult
       ***************************************************************************/
      /*!
       * \fn      CmdReqAVDeactResult()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdReqAVDeactResult(spi_tclAudio *poAudio, const t_U8 cou8SourceNum);

      /***************************************************************************
       ** FUNCTION:  CmdReqAVDeactResult::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdReqAVDeactResult::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdReqAVDeactResult::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdAudioError
 ****************************************************************************/
class CmdAudioError: public trMsgBase
{
   public:
      spi_tclAudio *m_poAudio;
      t_U8 m_u8SourceNum;
      tenAudioError m_enAudioError;

      /***************************************************************************
       ** FUNCTION:  CmdAudioError::CmdAudioError
       ***************************************************************************/
      /*!
       * \fn      CmdAudioError()
       * \brief   Default constructor
       **************************************************************************/
      CmdAudioError();

      /***************************************************************************
       ** FUNCTION:  CmdAudioError::CmdAudioError
       ***************************************************************************/
      /*!
       * \fn      CmdAudioError()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAudioError(spi_tclAudio *poAudio,
               const t_U8 cou8SourceNum, tenAudioError enAudioError);

      /***************************************************************************
       ** FUNCTION:  CmdAudioError::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAudioError::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdAudioError::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*! \class CmdBTDeviceAction
 ****************************************************************************/
class CmdBTDeviceAction: public trMsgBase
{
   public:
      spi_tclBluetooth *m_poBluetooth;
      t_U32 m_u32BluetoothDevHandle;
      t_U32 m_u32ProjectionDevHandle;
      tenBTChangeInfo m_enBTChange;

      /***************************************************************************
       ** FUNCTION:  CmdBTDeviceAction::CmdBTDeviceAction
       ***************************************************************************/
      /*!
       * \fn      CmdBTDeviceAction()
       * \brief   Default constructor
       **************************************************************************/
      CmdBTDeviceAction();

      /***************************************************************************
       ** FUNCTION:  CmdBTDeviceAction::CmdBTDeviceAction
       ***************************************************************************/
      /*!
       * \fn      CmdBTDeviceAction()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdBTDeviceAction(spi_tclBluetooth *poBluetooth, t_U32 u32BluetoothDevHandle,
               t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange);

      /***************************************************************************
       ** FUNCTION:  CmdBTDeviceAction::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdBTDeviceAction::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdBTDeviceAction::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};


/****************************************************************************/
/*! \class CmdVehicleBTAddress
 ****************************************************************************/
class CmdVehicleBTAddress: public trMsgBase
{
   public:
      spi_tclAppMngr *m_poAppMngr;
      t_U32 m_u32DeviceHandle;
      t_String *m_pszBTAddress;
      tenDeviceCategory m_enDevCat;

      /***************************************************************************
       ** FUNCTION:  CmdVehicleBTAddress::CmdVehicleBTAddress
       ***************************************************************************/
      /*!
       * \fn      CmdVehicleBTAddress()
       * \brief   Default constructor
       **************************************************************************/
      CmdVehicleBTAddress():m_poAppMngr(NULL),m_u32DeviceHandle(0),m_pszBTAddress(NULL),m_enDevCat(e8DEV_TYPE_UNKNOWN)
      {
         //default
      }

      /***************************************************************************
       ** FUNCTION:  CmdVehicleBTAddress::CmdVehicleBTAddress
       ***************************************************************************/
      /*!
       * \fn      CmdVehicleBTAddress()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdVehicleBTAddress(spi_tclAppMngr *poAppMngr, t_U32 u32DeviceHandle,t_String szBTAddress,
               tenDeviceCategory enDevCat);

      /***************************************************************************
       ** FUNCTION:  CmdVehicleBTAddress::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdVehicleBTAddress::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdVehicleBTAddress::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};


/****************************************************************************/
/*! \class CmdCallStatus
 ****************************************************************************/
class CmdCallStatus: public trMsgBase
{
   public:
      spi_tclBluetooth *m_poBluetooth;
      t_Bool m_bCallActive;

      /***************************************************************************
       ** FUNCTION:  CmdCallStatus::CmdCallStatus
       ***************************************************************************/
      /*!
       * \fn      CmdCallStatus()
       * \brief   Default constructor
       **************************************************************************/
      CmdCallStatus():m_poBluetooth(NULL),m_bCallActive(false)
      {
         //default
      }

      /***************************************************************************
       ** FUNCTION:  CmdCallStatus::CmdCallStatus
       ***************************************************************************/
      /*!
       * \fn      CmdCallStatus()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdCallStatus(spi_tclBluetooth *poBluetooth, t_Bool bCallActive);

      /***************************************************************************
       ** FUNCTION:  CmdCallStatus::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdCallStatus::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  CmdCallStatus::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};


/****************************************************************************/
/*! \class CmdGPSData
 ****************************************************************************/
class CmdGPSData: public trMsgBase
{
   public:
      spi_tclDataService *m_poDataService;
      trGPSData m_rGPSData;

      /***************************************************************************
       ** FUNCTION:  CmdGPSData::CmdGPSData
       ***************************************************************************/
      /*!
       * \fn      CmdGPSData()
       * \brief   Default constructor
       **************************************************************************/
      CmdGPSData():m_poDataService(NULL)
      {
         //default
      }

      /***************************************************************************
       ** FUNCTION:  CmdGPSData::CmdGPSData
       ***************************************************************************/
      /*!
       * \fn      CmdGPSData()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdGPSData(spi_tclDataService *poDataService, const trGPSData &rfcorGPSData);

      /***************************************************************************
       ** FUNCTION:  CmdGPSData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdGPSData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  CmdGPSData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};


/***************************************************************************/
/*! \class CmdSensorData
 ****************************************************************************/
class CmdSensorData: public trMsgBase
{
   public:
      spi_tclDataService *m_poDataService;
      trSensorData m_rSensorData;

      /***************************************************************************
       ** FUNCTION:  CmdSensorData::CmdSensorData
       ***************************************************************************/
      /*!
       * \fn      CmdSensorData()
       * \brief   Default constructor
       **************************************************************************/
      CmdSensorData():m_poDataService(NULL)
      {
         //default
      }

      /***************************************************************************
       ** FUNCTION:  CmdSensorData::CmdSensorData
       ***************************************************************************/
      /*!
       * \fn      CmdSensorData()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdSensorData(spi_tclDataService *poDataService, const trSensorData &rfcorSensorData);

      /***************************************************************************
       ** FUNCTION:  CmdSensorData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdSensorData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  CmdSensorData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/***************************************************************************/
/*! \class CmdAccSensorData
 ****************************************************************************/
class CmdAccSensorData: public trMsgBase
{
   public:
      spi_tclDataService *m_poDataService;
      std::vector<trAccSensorData> *m_pVecrAccSensorData;

      /***************************************************************************
       ** FUNCTION:  CmdAccSensorData::CmdAccSensorData
       ***************************************************************************/
      /*!
       * \fn      CmdAccSensorData()
       * \brief   Default constructor
       **************************************************************************/
      CmdAccSensorData():m_poDataService(NULL), m_pVecrAccSensorData(NULL)
      {
         //default
      }

      /***************************************************************************
       ** FUNCTION:  CmdAccSensorData::CmdAccSensorData
       ***************************************************************************/
      /*!
       * \fn      CmdAccSensorData()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAccSensorData(spi_tclDataService *poDataService, const std::vector<trAccSensorData>& corfvecrAccSensorData);

      /***************************************************************************
       ** FUNCTION:  CmdAccSensorData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAccSensorData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdAccSensorData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/***************************************************************************/
/*! \class CmdGyroSensorData
 ****************************************************************************/
class CmdGyroSensorData: public trMsgBase
{
   public:
      spi_tclDataService *m_poDataService;
      std::vector<trGyroSensorData> *m_pVecrGyroSensorData;

      /***************************************************************************
       ** FUNCTION:  CmdGyroSensorData::CmdGyroSensorData
       ***************************************************************************/
      /*!
       * \fn      CmdGyroSensorData()
       * \brief   Default constructor
       **************************************************************************/
      CmdGyroSensorData():m_poDataService(NULL), m_pVecrGyroSensorData(NULL)
      {
         //default
      }

      /***************************************************************************
       ** FUNCTION:  CmdGyroSensorData::CmdGyroSensorData
       ***************************************************************************/
      /*!
       * \fn      CmdGyroSensorData()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdGyroSensorData(spi_tclDataService *poDataService, const std::vector<trGyroSensorData>& corfvecrGyroSensorData);

      /***************************************************************************
       ** FUNCTION:  CmdGyroSensorData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdGyroSensorData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdGyroSensorData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/***************************************************************************/
/*! \class CmdVehicleData
 ****************************************************************************/
class CmdVehicleData: public trMsgBase
{
   public:
      spi_tclDataService *m_poDataService;
      spi_tclVideo *m_poVideo;
      spi_tclAppMngr *m_poAppMngr;
      trVehicleData m_rVehicleData;
      t_Bool m_bSolicited;

      /***************************************************************************
       ** FUNCTION:  CmdVehicleData::CmdVehicleData
       ***************************************************************************/
      /*!
       * \fn      CmdVehicleData()
       * \brief   Default constructor
       **************************************************************************/
      CmdVehicleData():m_poDataService(NULL), m_poVideo(NULL), m_poAppMngr(NULL), m_bSolicited(false)
      {
         //default
      }

      /***************************************************************************
       ** FUNCTION:  CmdVehicleData::CmdVehicleData
       ***************************************************************************/
      /*!
       * \fn      CmdVehicleData()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdVehicleData(spi_tclDataService *poDataService, spi_tclVideo *poVideo, spi_tclAppMngr *poAppMngr,
            const trVehicleData &rfcorVehiceData, t_Bool bSolicited);

      /***************************************************************************
       ** FUNCTION:  CmdVehicleData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdVehicleData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  CmdVehicleData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*! \class CmdKnobKeyEvent
 * \sa spi_tclCmdInterface::vSendKnobKeyEvent
 ****************************************************************************/
class CmdKnobKeyEvent: public trMsgBase
{
   public:
      spi_tclInputHandler *m_pInputHandler;
      t_U32 m_u32DeviceHandle;
      tenDeviceCategory m_enDevCat;
      t_S8 m_s8EncoderDeltaCnt;

      /***************************************************************************
       ** FUNCTION:  CmdKnobKeyEvent::CmdKnobKeyEvent
       ***************************************************************************/
      /*!
       * \fn      CmdKnobKeyEvent()
       * \brief   Default constructor
       **************************************************************************/
      CmdKnobKeyEvent();

      /***************************************************************************
       ** FUNCTION:  CmdKnobKeyEvent::CmdKeyEvent
       ***************************************************************************/
      /*!
       * \fn      CmdKnobKeyEvent()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdKnobKeyEvent(spi_tclInputHandler *pInputHandler, t_U32 u32DeviceHandle,
            tenDeviceCategory enDevCat, t_S8 s8EncoderDeltaCnt);

      /***************************************************************************
       ** FUNCTION:  CmdKnobKeyEvent::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdKnobKeyEvent::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg() {}

      /***************************************************************************
       ** FUNCTION:  CmdKnobKeyEvent::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg() {}
};

/****************************************************************************/
/*! \class CmdKeyIconData
 * \sa spi_tclCmdInterface::vGetKeyIconData
 ****************************************************************************/
class CmdKeyIconData: public trMsgBase
{
   public:
      t_U32  m_cou32DevId;
      spi_tclInputHandler *m_pInputHandler;
      tenDeviceCategory m_enDevCat;
      t_String *m_pszKeyIconURL;

      /***************************************************************************
       ** FUNCTION:  CmdKeyIconData::CmdKeyIconData
       ***************************************************************************/
      /*!
       * \fn      CmdKeyIconData()
       * \brief   Default constructor
       **************************************************************************/
      CmdKeyIconData();

      /***************************************************************************
       ** FUNCTION:  CmdKeyIconData::CmdKeyIconData
       ***************************************************************************/
      /*!
       * \fn      CmdKeyIconData()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdKeyIconData(t_U32 u32DeviceHandle,spi_tclInputHandler *poInputHandler, tenDeviceCategory enDevCat, t_String szAppIconURL);

      /***************************************************************************
       ** FUNCTION:  CmdKeyIconData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdKeyIconData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  CmdKeyIconData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};


/****************************************************************************/
/*! \class CmdSetFeatRestrData
 ****************************************************************************/
class CmdSetFeatRestrData: public trMsgBase
{
   public:
      spi_tclDataService* m_poDataService;
      spi_tclResourceMngr* m_poResourceMngr;
      tenDeviceCategory m_enDevCategory;
      t_U8 m_u8ParkModeRestrictionInfo;
      t_U8 m_u8DriveModeRestrictionInfo;

      /***************************************************************************
       ** FUNCTION:  CmdSetFeatRestrData::CmdSetFeatRestrData
       ***************************************************************************/
      /*!
       * \fn      CmdSetFeatRestrData()
       * \brief   Default constructor
       **************************************************************************/
      CmdSetFeatRestrData();

      /***************************************************************************
       ** FUNCTION:  CmdSetFeatRestrData::CmdSetFeatRestrData
       ***************************************************************************/
      /*!
       * \fn      CmdSetFeatRestrData()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdSetFeatRestrData(spi_tclDataService* poDataService, spi_tclResourceMngr* poResourceMngr,
            tenDeviceCategory enDevCategory, t_U8 u8ParkModeRestrictionInfo,
            t_U8 u8DriveModeRestrictionInfo);

      /***************************************************************************
       ** FUNCTION:  CmdSetFeatRestrData::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdSetFeatRestrData::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  CmdSetFeatRestrData::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*! \class CmdAckNotificationEvent
 ****************************************************************************/
class CmdAckNotificationEvent: public trMsgBase
{
   public:
      spi_tclAppMngr *m_poAppMngr;
      trNotificationAckData m_rNotifAckData;

      /***************************************************************************
       ** FUNCTION:  CmdAckNotificationEvent::CmdAckNotificationEvent
       ***************************************************************************/
      /*!
       * \fn      CmdAckNotificationEvent()
       * \brief   Default constructor
       **************************************************************************/
      CmdAckNotificationEvent();

      /***************************************************************************
       ** FUNCTION:  CmdAckNotificationEvent::CmdAckNotificationEvent
       ***************************************************************************/
      /*!
       * \fn      CmdAckNotificationEvent()
       * \brief   Parameterized constructor
       **************************************************************************/
      CmdAckNotificationEvent(spi_tclAppMngr* poAppMngr,
          const trNotificationAckData& corfrNotifAckData);

      /***************************************************************************
       ** FUNCTION:  CmdAckNotificationEvent::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param  poCmdDispatcher : pointer to Message dispatcher for SPI Commands
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclCmdDispatcher* poCmdDispatcher);

      /***************************************************************************
       ** FUNCTION:  CmdAckNotificationEvent::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  CmdAckNotificationEvent::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class spi_tclCmdDispatcher
 * \brief Message Dispatcher for SPI Command Messages
 ****************************************************************************/
class spi_tclCmdDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::spi_tclCmdDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclCmdDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclCmdDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::~spi_tclCmdDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclCmdDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclCmdDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSelectDevice* poSelectDevice)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdSelectDevice* poSelectDevice)
       * \brief   Handles Messages of CmdSelectDevice type
       * \param   poSelectDevice : pointer to CmdSelectDevice.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdSelectDevice* poSelectDevice);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdLaunchApp* poLaunchApp)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdLaunchApp* poLaunchApp)
       * \brief   Handles Messages of CmdLaunchApp type
       * \param   poLaunchApp : pointer to CmdLaunchApp.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdLaunchApp* poLaunchApp);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdTerminateApp* poTerminateApp)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdTerminateApp* poTerminateApp)
       * \brief   Handles Messages of CmdTerminateApp type
       * \param   poTerminateApp : pointer to CmdTerminateApp.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdTerminateApp* poTerminateApp);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAppIconData* poAppIconData)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAppIconData* poAppIconData)
       * \brief   Handles Messages of CmdAppIconData type
       * \param   poAppIconData : pointer to CmdAppIconData.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAppIconData* poAppIconData);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAppIconAttributes* poAppIconAttributes)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAppIconAttributes* poAppIconAttributes)
       * \brief   Handles Messages of CmdAppIconAttributes type
       * \param   poAppIconAttributes : pointer to CmdAppIconAttributes.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAppIconAttributes* poAppIconAttributes);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdDeviceUsagePreference*
       **  poDeviceUsagePreference)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdDeviceUsagePreference* poDeviceUsagePreference)
       * \brief   Handles Messages of CmdDeviceUsagePreference type
       * \param   poDeviceUsagePreference : pointer to CmdDeviceUsagePreference.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdDeviceUsagePreference* poDeviceUsagePreference);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdMLNotificationEnabledInfo*
       ** poMLNotificationEnabledInfo)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdMLNotificationEnabledInfo* poMLNotificationEnabledInfo)
       * \brief   Handles Messages of CmdMLNotificationEnabledInfo type
       * \param   poMLNotificationEnabledInfo : pointer to CmdMLNotificationEnabledInfo.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdMLNotificationEnabledInfo* poMLNotificationEnabledInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetMLNotificationOnOff*
       ** poSetMLNotificationOnOff)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdSetMLNotificationOnOff* poSetMLNotificationOnOff)
       * \brief   Handles Messages of CmdSetMLNotificationOnOff type
       * \param   poSetMLNotificationOnOff : pointer to CmdSetMLNotificationOnOff.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdSetMLNotificationOnOff* poSetMLNotificationOnOff);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdInvokeNotificationAction*
       ** poInvokeNotificationAction)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdInvokeNotificationAction* poInvokeNotificationAction)
       * \brief   Handles Messages of CmdInvokeNotificationAction type
       * \param   poInvokeNotificationAction : pointer to CmdInvokeNotificationAction.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdInvokeNotificationAction* poInvokeNotificationAction);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdOrientationMode* poOrientationMode)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdOrientationMode* poOrientationMode)
       * \brief   Handles Messages of CmdOrientationMode type
       * \param   poOrientationMode : pointer to CmdOrientationMode.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdOrientationMode* poOrientationMode);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdScreenSize* poScreenSize)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdScreenSize* poScreenSize)
       * \brief   Handles Messages of CmdScreenSize type
       * \param   poScreenSize : pointer to CmdScreenSize.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdScreenSize* poScreenSize);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetVideoBlocking* poSetVideoBlocking)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdSetVideoBlocking* poSetVideoBlocking)
       * \brief   Handles Messages of CmdSetVideoBlocking type
       * \param   poSetVideoBlocking : pointer to CmdSetVideoBlocking.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdSetVideoBlocking* poSetVideoBlocking);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetAudioBlocking* poSetVideoBlocking)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdSetAudioBlocking* poSetVideoBlocking)
       * \brief   Handles Messages of CmdSetAudioBlocking type
       * \param   poSetVideoBlocking : pointer to CmdSetAudioBlocking.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdSetAudioBlocking* poSetVideoBlocking);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetVehicleConfig* poSetVehicleConfig)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdSetVehicleConfig* poSetVehicleConfig)
       * \brief   Handles Messages of CmdSetVehicleConfig type
       * \param   poSetVehicleConfig : pointer to CmdSetVehicleConfig.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdSetVehicleConfig* poSetVehicleConfig);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdTouchEvent* poTouchEvent)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdTouchEvent* poTouchEvent)
       * \brief   Handles Messages of CmdTouchEvent type
       * \param   poTouchEvent : pointer to CmdTouchEvent.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdTouchEvent* poTouchEvent);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdKeyEvent* poKeyEvent)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdKeyEvent* poKeyEvent)
       * \brief   Handles Messages of CmdKeyEvent type
       * \param   poKeyEvent : pointer to CmdKeyEvent.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdKeyEvent* poKeyEvent);

       /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdClientCapabilities* poClientCapabilities)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdClientCapabilities* poClientCapabilities)
       * \brief   Handles Messages of CmdClientCapabilities type
       * \param   poClientCapabilities : pointer to CmdClientCapabilities.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdClientCapabilities* poClientCapabilities);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAccessoryDisplayContext*
       ** poAccessoryDisplayContext)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAccessoryDisplayContext* poAccessoryDisplayContext)
       * \brief   Handles Messages of CmdAccessoryDisplayContext type
       * \param   poAccessoryDisplayContext : pointer to CmdAccessoryDisplayContext.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAccessoryDisplayContext* poAccessoryDisplayContext);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAccessoryAudioContext*)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAccessoryAudioContext* poBTAddress)
       * \brief   Handles Messages of CmdAccessoryAudioContext type
       * \param   poBTAddress : pointer to CmdAccessoryAudioContext.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAccessoryAudioContext* poAccessoryAudioCtxt);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAccessoryAppStateContext*)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAccessoryAppStateContext* poBTAddress)
       * \brief   Handles Messages of CmdAccessoryAppStateContext type
       * \param   poBTAddress : pointer to CmdAccessoryAppStateContext.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAccessoryAppStateContext* poAccessoryAppStateCtxt);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetRegion*
       ** poSetRegion)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdSetRegion* poSetRegion)
       * \brief   Handles Messages of CmdSetRegion type
       * \param   poSetRegion : pointer to CmdSetRegion.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdSetRegion* poSetRegion);


      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAllocateAudioRoute*
       ** poAllocateAudioRoute)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAllocateAudioRoute* poAllocateAudioRoute)
       * \brief   Handles Messages of CmdAllocateAudioRoute type
       * \param   poAllocateAudioRoute : pointer to CmdAllocateAudioRoute.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAllocateAudioRoute* poAllocateAudioRoute);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdDeAllocateAudioRoute*
       ** poDeAllocateAudioRoute)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdDeAllocateAudioRoute* poDeAllocateAudioRoute)
       * \brief   Handles Messages of CmdDeAllocateAudioRoute type
       * \param   poDeAllocateAudioRoute : pointer to CmdDeAllocateAudioRoute.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdDeAllocateAudioRoute* poDeAllocateAudioRoute);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdStartAudioSrcActivity*
       ** poAudioSrcActivity)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdStartAudioSrcActivity* poAudioSrcActivity)
       * \brief   Handles Messages of CmdStartAudioSrcActivity type
       * \param   poAudioSrcActivity : pointer to CmdStartAudioSrcActivity.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdStartAudioSrcActivity* poAudioSrcActivity);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdStopAudioSrcActivity*
       ** poAudioSrcActivity)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdStopAudioSrcActivity* poAudioSrcActivity)
       * \brief   Handles Messages of CmdStopAudioSrcActivity type
       * \param   poAudioSrcActivity : pointer to CmdStopAudioSrcActivity.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdStopAudioSrcActivity* poAudioSrcActivity);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdReqAVDeactResult*
       ** poAudioSrcActivity)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdReqAVDeactResult* poAudioSrcActivity)
       * \brief   Handles Messages of CmdReqAVDeactResult type
       * \param   poAudioSrcActivity : pointer to CmdReqAVDeactResult.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdReqAVDeactResult* poReqAVDeactResult);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAudioError*
       ** poAudioError)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAudioError* poAudioError)
       * \brief   Handles Messages of CmdAudioError type
       * \param   poAudioSrcActivity : pointer to CmdAudioError.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAudioError* poAudioError);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdBTDeviceAction*
       ** poBTDeviceAction)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdBTDeviceAction* poBTDeviceAction)
       * \brief   Handles Messages of CmdBTDeviceAction type
       * \param   poBTDeviceAction : pointer to CmdBTDeviceAction.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdBTDeviceAction* poBTDeviceAction);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdVehicleBTAddress*
       ** poVehicleBTAddress)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdVehicleBTAddress* poBTDeviceAction)
       * \brief   Handles Messages of CmdVehicleBTAddress type
       * \param   poBTDeviceAction : pointer to CmdVehicleBTAddress.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdVehicleBTAddress* poVehicleBTAddress);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdCallStatus*
       ** poCallStatus)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdCallStatus* poBTDeviceAction)
       * \brief   Handles Messages of CmdCallStatus type
       * \param   poCallStatus : pointer to CmdCallStatus.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdCallStatus* poCallStatus);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmCallStatusndleCmdMsg(CmdGPSData*
       ** poGPSData)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdGPSData* poBTDeviceAction)
       * \brief   Handles Messages of CmdGPSData type
       * \param   poGPSData : pointer to CmdGPSData.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdGPSData* poGPSData);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSensorData*
       ** poSensorData)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdSensorData* poBTDeviceAction)
       * \brief   Handles Messages of CmdSensorData type
       * \param   poSensorData : pointer to CmdSensorData.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdSensorData* poSensorData);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAccSensorData*
       ** poAccSensorData)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAccSensorData* poAccSensorData)
       * \brief   Handles Messages of CmdAccSensorData type
       * \param   poAccSensorData : pointer to CmdAccSensorData.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAccSensorData* poAccSensorData);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdGyroSensorData*
       ** poGyroSensorData)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdGyroSensorData* poGyroSensorData)
       * \brief   Handles Messages of poGyroSensorData type
       * \param   poGyroSensorData : pointer to poGyroSensorData.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdGyroSensorData* poGyroSensorData);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdVehicleData*
       ** poVehicleData)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdVehicleData* poBTDeviceAction)
       * \brief   Handles Messages of CmdVehicleData type
       * \param   poVehicleData : pointer to CmdVehicleData.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdVehicleData* poVehicleData);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdKnobKeyEvent* poKnobKeyEvent)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdKnobKeyEvent* poKeyEvent)
       * \brief   Handles Messages of CmdKnobKeyEvent type
       * \param   poKeyEvent : pointer to CmdKeyEvent.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdKnobKeyEvent* poKnobKeyEvent);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdKeyIconData* poKeyIconData)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdKeyIconData* poKeyIconData)
       * \brief   Handles Messages of CmdKeyIconData type
       * \param   poKeyIconData : pointer to CmdAppIconData.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdKeyIconData* poKeyIconData);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetFeatRestrData* poSetFeatRestrData)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdSetFeatRestrData* poSetFeatRestrData)
       * \brief   Handles Messages of CmdSetFeatRestrData type
       * \param   poSetFeatRestrData : pointer to CmdSetFeatRestrData.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdSetFeatRestrData* poSetFeatRestrData);

      /***************************************************************************
       ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAckNotificationEvent* poAckNotificationEvent)
       ***************************************************************************/
      /*!
       * \fn      vHandleCmdMsg(CmdAckNotificationEvent* poAckNotificationEvent)
       * \brief   Handles Messages of CmdAckNotificationEvent type
       * \param   poAckNotificationEvent : [IN] pointer to CmdAckNotificationEvent.
       **************************************************************************/
      t_Void vHandleCmdMsg(CmdAckNotificationEvent* poAckNotificationEvent);
	  
   private:

};

#endif /* SPI_TCLCMDDISPATCHER_H_ */
