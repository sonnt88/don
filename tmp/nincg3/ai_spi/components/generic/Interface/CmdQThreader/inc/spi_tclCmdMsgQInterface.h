/***********************************************************************/
/*!
 * \file  spi_tclCmdMsgQInterface.h
 * \brief interface for writing commands to Message Q to use the MsgQ based
 *        threading model for Dispatching commands received by SPI
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    interface for writing commands to Message Q to use the MsgQ based
                 threading model for Dispatching commands received by SPI
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 17.03.2014  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/

#ifndef SPI_TCLCMDMSGQINTERFACE_H_
#define SPI_TCLCMDMSGQINTERFACE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include "GenericSingleton.h"
#include "MsgQThreader.h"

//!Forward declarations
class spi_tclCmdMsgQThreadable;
class trMsgBase;

/****************************************************************************/
/*!
* \class spi_tclCmdMsgQInterface
* \brief interface for writing commands to Message Q to use the MsgQ based
         threading model for Dispatching commands received by SPI
****************************************************************************/
class spi_tclCmdMsgQInterface : public GenericSingleton<spi_tclCmdMsgQInterface>
{
   public:

      /***************************************************************************
      ** FUNCTION:  spi_tclCmdMsgQInterface::spi_tclCmdMsgQInterface()
      ***************************************************************************/
      /*!
      * \fn      spi_tclCmdMsgQInterface()
      * \brief   Default Constructor
      * \sa      ~spi_tclCmdMsgQInterface()
      **************************************************************************/
      spi_tclCmdMsgQInterface();

      /***************************************************************************
      ** FUNCTION:  spi_tclCmdMsgQInterface::~spi_tclCmdMsgQInterface()
      ***************************************************************************/
      /*!
      * \fn      ~spi_tclCmdMsgQInterface()
      * \brief   Destructor
      * \sa      spi_tclCmdMsgQInterface()
      **************************************************************************/
      ~spi_tclCmdMsgQInterface();

      /***************************************************************************
      ** FUNCTION:  spi_tclCmdMsgQInterface::bWriteMsgToQ
      ***************************************************************************/
      /*!
      * \fn      bWriteMsgToQ(trMsgBase *prMsgBase, t_U32 u32MsgSize)
      * \brief   Write Message to Q for dispatching the message by a seperate thread
      * \param   prMsgBase : Pointer to Base Message type
      * \param   u32MsgSize :  size of the message to be written to the MsgQ
      **************************************************************************/
      t_Bool bWriteMsgToQ(trMsgBase *prMsgBase, t_U32 u32MsgSize);

      //! Base Singleton class
      friend class GenericSingleton<spi_tclCmdMsgQInterface> ;

   private:

      //! Pointer to the overridden MsgQThreadable class
      spi_tclCmdMsgQThreadable *m_poCmdMsgQThreadable;

      //! pointer to MsgQThreader
      ::shl::thread::MsgQThreader *m_poCmdMsgQThreader;

};


#endif /* SPI_TCLCMDMSGQINTERFACE_H_ */
