/***********************************************************************/
/*!
* \file  spi_tclCmdMsgQThreadable.cpp
* \brief implements MsgQthreader for SPI Commands
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    implements MsgQthreader for SPI Commands
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.03.2014  | Pruthvi Thej Nagaraju | Initial Version
14.07.2015  | Ramkumar Muniraj      | Resolved lint warnings

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclCmdMsgQThreadable.h"
#include "spi_tclCmdDispatcher.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclCmdMsgQThreadable.cpp.trc.h"
   #endif
#endif

#define COMMAND_ACTION(COMMAND_ID, COMMAND_MESSAGE)                           \
   case COMMAND_ID:                                                           \
   {                                                                          \
      COMMAND_MESSAGE *poCmdMsg = static_cast<COMMAND_MESSAGE*> (prMsgBase);  \
      poCmdMsg->vDispatchMsg(m_poCmdDispatcher);                              \
      break;                                                                  \
   }



/***************************************************************************
 ** FUNCTION:  spi_tclCmdMsgQThreadable::spi_tclCmdMsgQThreadable()
 ***************************************************************************/
spi_tclCmdMsgQThreadable::spi_tclCmdMsgQThreadable(): m_poCmdDispatcher(NULL)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_poCmdDispatcher = new spi_tclCmdDispatcher();
   SPI_NORMAL_ASSERT(NULL == m_poCmdDispatcher);
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdMsgQThreadable::~spi_tclCmdMsgQThreadable()
 ***************************************************************************/

spi_tclCmdMsgQThreadable::~spi_tclCmdMsgQThreadable()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   RELEASE_MEM(m_poCmdDispatcher);
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdMsgQThreadable::vExecute
 ***************************************************************************/

t_Void spi_tclCmdMsgQThreadable::vExecute(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      //! Cast the received message buffer to Base message type
      trMsgBase *prMsgBase = static_cast<trMsgBase*>(poMessage->pvBuffer);
      if (NULL != prMsgBase)
      {
         t_U32 u32CommandID = prMsgBase->u32GetServiceID();
         ETG_TRACE_USR1(("spi_tclCmdMsgQThreadable::vExecute: COMMAND ID = %d \n", u32CommandID));

         //! Dispatch the message based on Command ID
         switch (u32CommandID)
         {
            COMMAND_ACTION(e32COMMANDID_SELECTDEVICE, CmdSelectDevice);
            COMMAND_ACTION(e32COMMANDID_LAUNCHAPP, CmdLaunchApp);
            COMMAND_ACTION(e32COMMANDID_TERMINATEAPP, CmdTerminateApp);
            COMMAND_ACTION(e32COMMANDID_GETAPPICONDATA, CmdAppIconData);
            COMMAND_ACTION(e32COMMANDID_SETAPPICONATTR, CmdAppIconAttributes);
            COMMAND_ACTION(e32COMMANDID_SETDEVICEUSAGEPREF, CmdDeviceUsagePreference);
            COMMAND_ACTION(e32COMMANDID_SETORIENTATIONMODE, CmdOrientationMode);
            COMMAND_ACTION(e32COMMANDID_SETSCREENSIZE, CmdScreenSize);
            COMMAND_ACTION(e32COMMANDID_SETVIDEOBLOCKINGMODE, CmdSetVideoBlocking);
            COMMAND_ACTION(e32COMMANDID_SETAUDIOBLOCKINGMODE, CmdSetAudioBlocking);
            COMMAND_ACTION(e32COMMANDID_SETVEHICLECONFIG, CmdSetVehicleConfig);
            COMMAND_ACTION(e32COMMANDID_SENDTOUCHEVENT, CmdTouchEvent);
            COMMAND_ACTION(e32COMMANDID_SENDKEYEVENT, CmdKeyEvent);
            COMMAND_ACTION(e32COMMANDID_ALLOCATEAUDIOROUTE, CmdAllocateAudioRoute);
            COMMAND_ACTION(e32COMMANDID_DEALLOCATEAUDIOROUTE, CmdDeAllocateAudioRoute);
            COMMAND_ACTION(e32COMMANDID_AUDIOSRCACTIVITY, CmdStartAudioSrcActivity);
            COMMAND_ACTION(e32COMMANDID_BTDEVICEACTION, CmdBTDeviceAction);
            COMMAND_ACTION(e32COMMANDID_BTADDRESS, CmdVehicleBTAddress);
            COMMAND_ACTION(e32COMMANDID_MLNOTIONOFF, CmdSetMLNotificationOnOff);
            COMMAND_ACTION(e32COMMANDID_SETACCESSORY_DISPLAYCONTEXT, CmdAccessoryDisplayContext);
            COMMAND_ACTION(e32COMMANDID_SETACCESSORY_AUDIOCONTEXT, CmdAccessoryAudioContext);
            COMMAND_ACTION(e32COMMANDID_SETACCESSORY_APPSTATE, CmdAccessoryAppStateContext);
            COMMAND_ACTION(e32COMMANDID_SETREGION, CmdSetRegion);
            COMMAND_ACTION(e32COMMANDID_AUDIO_ROUTEALLOCATE_RESULT, CmdAllocateAudioRoute);
            COMMAND_ACTION(e32COMMANDID_AUDIO_ROUTEDEALLOCATE_RESULT, CmdDeAllocateAudioRoute);
            COMMAND_ACTION(e32COMMANDID_AUDIO_STARTSOURCEACTIVITY, CmdStartAudioSrcActivity);
            COMMAND_ACTION(e32COMMANDID_AUDIO_STOPSOURCEACTIVITY, CmdStopAudioSrcActivity);
            COMMAND_ACTION(e32COMMANDID_AUDIO_REQAV_DEACTIVATION_RESULT, CmdReqAVDeactResult);
            COMMAND_ACTION(e32COMMANDID_AUDIO_ERROR, CmdAudioError);
            COMMAND_ACTION(e32COMMANDID_MLNOTIFICATION_ENABLED_INFO, CmdMLNotificationEnabledInfo);
            COMMAND_ACTION(e32COMMANDID_INVOKE_NOTIACTION,CmdInvokeNotificationAction);
            COMMAND_ACTION(e32COMMANDID_SETCLIENTCAPABILITIES,CmdClientCapabilities);
            COMMAND_ACTION(e32COMMANDID_CALLSTATUS,CmdCallStatus);
            COMMAND_ACTION(e32COMMANDID_SENSORDATA,CmdSensorData);
            COMMAND_ACTION(e32COMMANDID_GPSDATA,CmdGPSData);
            COMMAND_ACTION(e32COMMANDID_VEHICLEDATA,CmdVehicleData);            
            COMMAND_ACTION(e32COMMANDID_GETKEYICONDATA, CmdKeyIconData);
            COMMAND_ACTION(e32COMMANDID_SENDKNOBKEYEVENT, CmdKnobKeyEvent);
            COMMAND_ACTION(e32COMMANDID_ACCSENSORDATA, CmdAccSensorData);
            COMMAND_ACTION(e32COMMANDID_GYROSENSORDATA, CmdGyroSensorData);
            COMMAND_ACTION(e32COMMANDID_SETFEATURERESTRICTIONDATA, CmdSetFeatRestrData);
            default:
            {
               ETG_TRACE_ERR(("spi_tclCmdMsgQThreadable::vExecute: Invalid Command ID \n"));
               break;
            }
         } // switch (u32CommandID)
      } // if (NULL != prMsgBase)

      t_U8 *pU8Buffer = static_cast<t_PU8> (poMessage->pvBuffer);
      RELEASE_ARRAY_MEM(pU8Buffer);
   }
   RELEASE_MEM(poMessage);
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdMsgQThreadable::~spi_tclCmdMsgQThreadable()
 ***************************************************************************/
tShlMessage* spi_tclCmdMsgQThreadable::poGetMsgBuffer(size_t siBuffer)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   tShlMessage* poMessage = new tShlMessage;

   if (NULL != poMessage)
   {
      if(0 < siBuffer)
      {
         //! Allocate the requested memory
         poMessage->pvBuffer = new t_U8[siBuffer];
      }
      else
      {
         poMessage->pvBuffer = NULL;
      } // if(0 < siBuffer)

      if (NULL != poMessage->pvBuffer)
      {
         poMessage->size = siBuffer;
      }
      else
      {
         //! Free the message as internal allocation failed.
         delete poMessage;
         poMessage = NULL;
      } //   if (NULL != poMessage->pvBuffer)
   } // if (NULL != poMessage)

   return poMessage;
}
