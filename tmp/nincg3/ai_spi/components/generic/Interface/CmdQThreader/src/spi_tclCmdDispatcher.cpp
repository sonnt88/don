/***********************************************************************/
/*!
 * \file  spi_tclCmdDispatcher.cpp
 * \brief Message Dispatcher for SPI Command Messages.
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for SPI Command Messages. Implemented using
                 double dispatch mechanism
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                			| Modification
 18.03.2014  | Pruthvi Thej Nagaraju 			| Initial Version
 26.05.2015  | Tejaswini H B(RBEI/ECP2)         | Added Lint comments to suppress C++11 Errors
 15.07.2015  | Sameer Chandra                   | Knob key implementation 
 10.03.2016  | Rachana L Achar                  | Added dispatcher for AAP Notification event

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "spi_tclCmdDispatcher.h"
#include "spi_tclDeviceSelector.h"
#include "spi_tclAppLauncher.h"
#include "spi_tclAppMngr.h"
#include "spi_tclAudio.h"
#include "spi_tclVideo.h"
#include "spi_tclInputHandler.h"
#include "spi_tclConnMngr.h"
#include "spi_tclBluetooth.h"
#include "spi_tclDataService.h"
#include "spi_tclRespInterface.h"
#include "spi_tclResourceMngr.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VNCWRAPPER
#include "trcGenProj/Header/spi_tclCmdDispatcher.cpp.trc.h"
#endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleCmdMsg(this);                    \
   }                                                        \
   vDeAllocateMsg();                                        \
}                                                           \


/****************************************************************************/
/*! \class CmdSelectDevice
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdSelectDevice::CmdSelectDevice
 ***************************************************************************/
CmdSelectDevice::CmdSelectDevice() :
                  m_pDeviceSelector(NULL),
                  m_u32DeviceHandle(0),
                  m_enDevConnType(e8UNKNOWN_CONNECTION),
                  m_enDevConnReq(e8DEVCONNREQ_SELECT),
                  m_enDAPUsage(e8USAGE_DISABLED),
                  m_enCDBUsage(e8USAGE_DISABLED),
                  m_enDevCat(e8DEV_TYPE_UNKNOWN),
                  m_bIsHMITrigger(false)
{

}

/***************************************************************************
 ** FUNCTION:  CmdSelectDevice::CmdSelectDevice
 ***************************************************************************/
CmdSelectDevice::CmdSelectDevice(spi_tclDeviceSelector* pDeviceSelector,
      t_U32 u32DeviceHandle, tenDeviceConnectionType enDevConnType,
      tenDeviceConnectionReq enDevConnReq, tenEnabledInfo enDAPUsage,
      tenEnabledInfo enCDBUsage, tenDeviceCategory enDevCat,t_Bool bIsHMITrigger):
      m_pDeviceSelector(NULL), m_u32DeviceHandle(0), m_enDevConnType(e8UNKNOWN_CONNECTION),
      m_enDevConnReq(e8DEVCONNREQ_SELECT), m_enDAPUsage(e8USAGE_DISABLED),
      m_enCDBUsage(e8USAGE_DISABLED), m_enDevCat(e8DEV_TYPE_UNKNOWN), m_bIsHMITrigger(bIsHMITrigger)
{
   vAllocateMsg();
   m_pDeviceSelector = pDeviceSelector;
   m_u32DeviceHandle = u32DeviceHandle;
   m_enDevConnType = enDevConnType;
   m_enDevConnReq = enDevConnReq;
   m_enDAPUsage = enDAPUsage;
   m_enCDBUsage = enCDBUsage;
   m_enDevCat = enDevCat;
}

/***************************************************************************
 ** FUNCTION:  CmdSelectDevice::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdSelectDevice, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdLaunchApp
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdLaunchApp::CmdLaunchApp
 ***************************************************************************/
CmdLaunchApp::CmdLaunchApp() :
                  m_pAppLauncher(NULL),
                  m_u32DeviceHandle(0),
                  m_u32AppHandle(0),
                  m_enDiPOAppType(e8DIPO_NOT_USED),
                  m_pszTelephoneNumber(NULL),
                  m_enEcnrSetting(e8ECNR_NOCHANGE)
{
}

/***************************************************************************
 ** FUNCTION:  CmdLaunchApp::CmdLaunchApp
 ***************************************************************************/
CmdLaunchApp::CmdLaunchApp(spi_tclAppLauncher *pAppLauncher,
      t_U32 u32DeviceHandle, t_U32 u32AppHandle, tenDiPOAppType enDiPOAppType,
      t_String szTelephoneNumber, tenEcnrSetting enEcnrSetting) : m_pAppLauncher(NULL),
      m_u32DeviceHandle(0), m_u32AppHandle(0), m_enDiPOAppType(e8DIPO_NOT_USED),
      m_pszTelephoneNumber(NULL), m_enEcnrSetting(e8ECNR_NOCHANGE)
{
   vAllocateMsg();
   m_pAppLauncher = pAppLauncher;
   m_u32DeviceHandle = u32DeviceHandle;
   m_u32AppHandle = u32AppHandle;
   m_enDiPOAppType = enDiPOAppType;
   if(NULL != m_pszTelephoneNumber)
   {
      *(m_pszTelephoneNumber) = szTelephoneNumber;
   }
   m_enEcnrSetting = enEcnrSetting;
}

/***************************************************************************
 ** FUNCTION:  CmdLaunchApp::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdLaunchApp, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdLaunchApp::vAllocateMsg
 ***************************************************************************/
t_Void CmdLaunchApp::vAllocateMsg()
{
   m_pszTelephoneNumber = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_pszTelephoneNumber);
}

/***************************************************************************
 ** FUNCTION:  CmdLaunchApp::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdLaunchApp::vDeAllocateMsg()
{
   RELEASE_MEM(m_pszTelephoneNumber);
}

/****************************************************************************/
/*! \class CmdTerminateApp
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdTerminateApp::CmdTerminateApp
 ***************************************************************************/
CmdTerminateApp::CmdTerminateApp() :
         m_u32DeviceHandle(0), m_u32AppHandle(0)
{
}

/***************************************************************************
 ** FUNCTION:  CmdTerminateApp::CmdTerminateApp
 ***************************************************************************/
CmdTerminateApp::CmdTerminateApp(spi_tclAppLauncher *pAppLauncher,
      t_U32 u32DeviceHandle, t_U32 u32AppHandle) : m_u32DeviceHandle(0), m_u32AppHandle(0)
{
   vAllocateMsg();
   m_pAppLauncher = pAppLauncher;
   m_u32DeviceHandle = u32DeviceHandle;
   m_u32AppHandle = u32AppHandle;
}

/***************************************************************************
 ** FUNCTION:  CmdTerminateApp::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdTerminateApp, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdAppIconData
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAppIconData::CmdAppIconData
 ***************************************************************************/
CmdAppIconData::CmdAppIconData() :
   m_pAppMngr(NULL), m_enDevCat(e8DEV_TYPE_UNKNOWN),m_pszAppIconURL(NULL)
{
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconData::CmdAppIconData
 ***************************************************************************/
CmdAppIconData::CmdAppIconData(spi_tclAppMngr *pAppMngr,
      tenDeviceCategory enDevCat, t_String szAppIconURL): m_pAppMngr(NULL),
      m_enDevCat(e8DEV_TYPE_UNKNOWN), m_pszAppIconURL(NULL)
{
   vAllocateMsg();
   m_pAppMngr = pAppMngr;
   m_enDevCat = enDevCat;
   if(NULL != m_pszAppIconURL)
   {
      *(m_pszAppIconURL) = szAppIconURL;
   }
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAppIconData, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdAppIconData::vAllocateMsg
 ***************************************************************************/
t_Void CmdAppIconData::vAllocateMsg()
{
   m_pszAppIconURL = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_pszAppIconURL);
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconData::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdAppIconData::vDeAllocateMsg()
{
   RELEASE_MEM(m_pszAppIconURL);
}

/****************************************************************************/
/*! \class CmdAppIconAttributes
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAppIconAttributes::CmdAppIconAttributes
 ***************************************************************************/
CmdAppIconAttributes::CmdAppIconAttributes() :
   m_pAppMngr(NULL), m_u32DeviceHandle(0), m_u32AppHandle(0), m_enDevCat(
         e8DEV_TYPE_UNKNOWN),m_prIconAttributes(NULL)
{
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconAttributes::CmdAppIconAttributes
 ***************************************************************************/
CmdAppIconAttributes::CmdAppIconAttributes(spi_tclAppMngr *pAppMngr,
      t_U32 u32DeviceHandle, t_U32 u32AppHandle, tenDeviceCategory enDevCat,
      const trIconAttributes& rfrIconAttributes):m_pAppMngr(NULL), m_u32DeviceHandle(0),
      m_u32AppHandle(0), m_enDevCat(e8DEV_TYPE_UNKNOWN), m_prIconAttributes(NULL)
{
   vAllocateMsg();
   m_pAppMngr = pAppMngr;
   m_u32DeviceHandle = u32DeviceHandle;
   m_u32AppHandle = u32AppHandle;
   m_enDevCat = enDevCat;
   if (NULL != m_prIconAttributes)
   {
      *(m_prIconAttributes) = rfrIconAttributes;
   }
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconAttributes::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAppIconAttributes, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdAppIconAttributes::vAllocateMsg
 ***************************************************************************/
t_Void CmdAppIconAttributes::vAllocateMsg()
{
   m_prIconAttributes = new trIconAttributes;
   SPI_NORMAL_ASSERT(NULL == m_prIconAttributes);
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconAttributes::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdAppIconAttributes::vDeAllocateMsg()
{
   RELEASE_MEM(m_prIconAttributes);
}

/****************************************************************************/
/*! \class CmdDeviceUsagePreference
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdDeviceUsagePreference::CmdDeviceUsagePreference
 ***************************************************************************/
CmdDeviceUsagePreference::CmdDeviceUsagePreference() :
   m_poConnMngr(NULL), m_u32DeviceHandle(0), m_enDeviceCategory(
         e8DEV_TYPE_UNKNOWN), m_enEnabledInfo(e8USAGE_DISABLED)
{

}

/***************************************************************************
 ** FUNCTION:  CmdDeviceUsagePreference::CmdDeviceUsagePreference
 ***************************************************************************/
CmdDeviceUsagePreference::CmdDeviceUsagePreference(spi_tclConnMngr *poConnMngr,
      t_U32 u32DeviceHandle, tenDeviceCategory enDeviceCategory,
      tenEnabledInfo enEnabledInfo) : m_poConnMngr(NULL), m_u32DeviceHandle(0),
      m_enDeviceCategory(e8DEV_TYPE_UNKNOWN), m_enEnabledInfo(e8USAGE_DISABLED)
{
   vAllocateMsg();
   m_poConnMngr = poConnMngr;
   m_u32DeviceHandle = u32DeviceHandle;
   m_enDeviceCategory = enDeviceCategory;
   m_enEnabledInfo = enEnabledInfo;
}

/***************************************************************************
 ** FUNCTION:  CmdDeviceUsagePreference::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdDeviceUsagePreference, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdMLNotificationEnabledInfo
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdMLNotificationEnabledInfo::CmdMLNotificationEnabledInfo
 ***************************************************************************/
CmdMLNotificationEnabledInfo::CmdMLNotificationEnabledInfo() :
                  m_pAppMngr(NULL),
                  m_u32DeviceHandle(0),
                  m_u32NumNotificationEnableList(0),
                  m_pvecrNotificationEnableList(NULL)
{
}

/***************************************************************************
 ** FUNCTION:  CmdMLNotificationEnabledInfo::CmdMLNotificationEnabledInfo
 ***************************************************************************/
CmdMLNotificationEnabledInfo::CmdMLNotificationEnabledInfo(
         spi_tclAppMngr *poAppMngr, t_U32 u32DeviceHandle,
         t_U32 u32NumNotificationEnableList, tenDeviceCategory enDevCat,
         std::vector<trNotiEnable> vecrNotificationEnableList): m_pAppMngr(NULL), m_u32DeviceHandle(0),
         m_u32NumNotificationEnableList(0), m_enDevCat(e8DEV_TYPE_UNKNOWN), m_pvecrNotificationEnableList(NULL)
{
   vAllocateMsg();
   m_pAppMngr = poAppMngr;
   m_u32DeviceHandle = u32DeviceHandle;
   m_u32NumNotificationEnableList = u32NumNotificationEnableList;
   m_enDevCat = enDevCat;
   if(NULL != m_pvecrNotificationEnableList)
   {
      ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
      (*m_pvecrNotificationEnableList) = vecrNotificationEnableList;
   }
}

/***************************************************************************
 ** FUNCTION:  CmdMLNotificationEnabledInfo::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdMLNotificationEnabledInfo, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdMLNotificationEnabledInfo::vAllocateMsg
 ***************************************************************************/
t_Void CmdMLNotificationEnabledInfo::vAllocateMsg()
{
   m_pvecrNotificationEnableList = new std::vector<trNotiEnable>;
   SPI_NORMAL_ASSERT(NULL == m_pvecrNotificationEnableList);
}

/***************************************************************************
 ** FUNCTION:  CmdMLNotificationEnabledInfo::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdMLNotificationEnabledInfo::vDeAllocateMsg()
{
   RELEASE_MEM(m_pvecrNotificationEnableList);
}

/****************************************************************************/
/*! \class CmdSetMLNotificationOnOff
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdSetMLNotificationOnOff::CmdSetMLNotificationOnOff
 ***************************************************************************/
CmdSetMLNotificationOnOff::CmdSetMLNotificationOnOff() : m_poAppMngr(NULL), m_bNotificationState(false)
{
}

/***************************************************************************
 ** FUNCTION:  CmdSetMLNotificationOnOff::CmdSetMLNotificationOnOff
 ***************************************************************************/
CmdSetMLNotificationOnOff::CmdSetMLNotificationOnOff(spi_tclAppMngr *poAppMngr,
         t_Bool bNotificationState) : m_poAppMngr(NULL), m_bNotificationState(false)
{
   vAllocateMsg();
   m_poAppMngr = poAppMngr;
   m_bNotificationState = bNotificationState;
}

/***************************************************************************
 ** FUNCTION:  CmdSetMLNotificationOnOff::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdSetMLNotificationOnOff, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdInvokeNotificationAction
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdInvokeNotificationAction::CmdInvokeNotificationAction
 ***************************************************************************/
CmdInvokeNotificationAction::CmdInvokeNotificationAction() :
                  m_poAppMngr(NULL),
                  m_u32DeviceHandle(0),
                  m_u32AppHandle(0),
                  m_u32NotificationID(0),
                  m_u32NotificationActionID(0),
                  m_enDevCat(e8DEV_TYPE_UNKNOWN)
{
}

/***************************************************************************
 ** FUNCTION:  CmdInvokeNotificationAction::CmdInvokeNotificationAction
 ***************************************************************************/
CmdInvokeNotificationAction::CmdInvokeNotificationAction(spi_tclAppMngr* poAppMngr,
                     t_U32 u32DeviceHandle, 
                     t_U32 u32AppHandle,
                     t_U32 u32NotificationID, 
                     t_U32 u32NotificationActionID,
                     tenDeviceCategory enDevCat): m_poAppMngr(NULL),
                     m_u32DeviceHandle(0), m_u32AppHandle(0),
                     m_u32NotificationID(0), m_u32NotificationActionID(0),
                     m_enDevCat(e8DEV_TYPE_UNKNOWN)
{
   vAllocateMsg();
   m_poAppMngr = poAppMngr;
   m_u32DeviceHandle = u32DeviceHandle;
   m_u32AppHandle = u32AppHandle;
   m_u32NotificationID = u32NotificationID;
   m_u32NotificationActionID = u32NotificationActionID;
   m_enDevCat = enDevCat;
   
}

/***************************************************************************
 ** FUNCTION:  CmdInvokeNotificationAction::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdInvokeNotificationAction, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdOrientationMode
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdOrientationMode::CmdOrientationMode
 ***************************************************************************/
CmdOrientationMode::CmdOrientationMode() :
                  m_poVideo(NULL),
                  m_u32DeviceHandle(0),
                  m_enDevCat(e8DEV_TYPE_UNKNOWN),
                  m_enOrientationMode(e8INVALID_MODE)
{
}

/***************************************************************************
 ** FUNCTION:  CmdOrientationMode::CmdOrientationMode
 ***************************************************************************/
CmdOrientationMode::CmdOrientationMode(spi_tclVideo *poVideo,
      t_U32 u32DeviceHandle, tenDeviceCategory enDevCat,
      tenOrientationMode enOrientationMode) : m_poVideo(NULL),
      m_u32DeviceHandle(0), m_enDevCat(e8DEV_TYPE_UNKNOWN), m_enOrientationMode(e8INVALID_MODE)
{
   vAllocateMsg();
   m_poVideo = poVideo;
   m_u32DeviceHandle = u32DeviceHandle;
   m_enDevCat = enDevCat;
   m_enOrientationMode = enOrientationMode;
}

/***************************************************************************
 ** FUNCTION:  CmdOrientationMode::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdOrientationMode, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdScreenSize
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdScreenSize::CmdScreenSize
 ***************************************************************************/
CmdScreenSize::CmdScreenSize() :
   m_poVideo(NULL), m_enDevCat(e8DEV_TYPE_UNKNOWN)
{
}

/***************************************************************************
 ** FUNCTION:  CmdScreenSize::CmdScreenSize
 ***************************************************************************/
CmdScreenSize::CmdScreenSize(spi_tclVideo *poVideo, tenDeviceCategory enDevCat,
      const trScreenAttributes corScreenAttributes) : m_poVideo(NULL), m_enDevCat(e8DEV_TYPE_UNKNOWN)
{
   vAllocateMsg();
   m_poVideo = poVideo;
   m_enDevCat = enDevCat;
   m_rScreenAttributes = corScreenAttributes;
}

/***************************************************************************
 ** FUNCTION:  CmdScreenSize::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdScreenSize, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdSetVideoBlocking
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdSetVideoBlocking::CmdSetVideoBlocking
 ***************************************************************************/
CmdSetVideoBlocking::CmdSetVideoBlocking() :
                  m_poVideo(NULL),
                  m_enBlockingType(),
                  m_u32DeviceHandle(0),
                  m_enDevCat(e8DEV_TYPE_UNKNOWN),
                  m_enBlockingMode(e8DISABLE_BLOCKING)
{
}

/***************************************************************************
 ** FUNCTION:  CmdSetVideoBlocking::CmdSetVideoBlocking
 ***************************************************************************/
CmdSetVideoBlocking::CmdSetVideoBlocking(spi_tclVideo *poVideo,
      tenBlockingType enBlockingType, t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat, tenBlockingMode enBlockingMode) : m_poVideo(NULL),
      m_enBlockingType(), m_u32DeviceHandle(0), m_enDevCat(e8DEV_TYPE_UNKNOWN),
      m_enBlockingMode(e8DISABLE_BLOCKING)
{
   vAllocateMsg();
   m_poVideo = poVideo;
   m_enBlockingType = enBlockingType;
   m_u32DeviceHandle = u32DeviceHandle;
   m_enDevCat = enDevCat;
   m_enBlockingMode = enBlockingMode;
}

/***************************************************************************
 ** FUNCTION:  CmdSetVideoBlocking::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdSetVideoBlocking, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdSetAudioBlocking
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdSetAudioBlocking::CmdSetAudioBlocking
 ***************************************************************************/
CmdSetAudioBlocking::CmdSetAudioBlocking() :
                  m_poAudio(NULL),
                  m_enBlockingType(),
                  m_u32DeviceHandle(0),
                  m_enDevCat(e8DEV_TYPE_UNKNOWN),
                  m_enBlockingMode(e8DISABLE_BLOCKING)
{
}

/***************************************************************************
 ** FUNCTION:  CmdSetAudioBlocking::CmdSetAudioBlocking
 ***************************************************************************/
CmdSetAudioBlocking::CmdSetAudioBlocking(spi_tclAudio *poAudio,
         tenBlockingType enBlockingType, t_U32 u32DeviceHandle,
         tenDeviceCategory enDevCat, tenBlockingMode enBlockingMode) : m_poAudio(NULL),
         m_enBlockingType(), m_u32DeviceHandle(0), m_enDevCat(e8DEV_TYPE_UNKNOWN),
         m_enBlockingMode(e8DISABLE_BLOCKING)
{
   vAllocateMsg();
   m_poAudio = poAudio;
   m_enBlockingType = enBlockingType;
   m_u32DeviceHandle = u32DeviceHandle;
   m_enDevCat = enDevCat;
   m_enBlockingMode = enBlockingMode;
}

/***************************************************************************
 ** FUNCTION:  CmdSetAudioBlocking::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdSetAudioBlocking, spi_tclCmdDispatcher)



/****************************************************************************/
/*! \class CmdSetVehicleConfig
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdSetVehicleConfig::CmdSetVehicleConfig
 ***************************************************************************/
CmdSetVehicleConfig::CmdSetVehicleConfig() :
                  m_poVideo(NULL),
                  m_poAppMngr(NULL),
                  m_bSetConfig(false)
{
}

/***************************************************************************
 ** FUNCTION:  CmdSetVehicleConfig::CmdSetVehicleConfig
 ***************************************************************************/
CmdSetVehicleConfig::CmdSetVehicleConfig(spi_tclVideo *poVideo,
         spi_tclAppMngr *poAppMngr, t_Bool bSetConfig,
         tenVehicleConfiguration enVehicleConfig) : m_poVideo(NULL),
         m_poAppMngr(NULL), m_bSetConfig(false)
{
   vAllocateMsg();
   m_poVideo = poVideo;
   m_poAppMngr = poAppMngr;
   m_bSetConfig = bSetConfig;
   m_enVehicleConfig = enVehicleConfig;
}

/***************************************************************************
 ** FUNCTION:  CmdSetVehicleConfig::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdSetVehicleConfig, spi_tclCmdDispatcher)



/****************************************************************************/
/*! \class CmdTouchEvent
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdTouchEvent::CmdTouchEvent
 ***************************************************************************/
CmdTouchEvent::CmdTouchEvent() :
   m_pInputHandler(NULL), m_u32DeviceHandle(0), m_enDevCat(e8DEV_TYPE_UNKNOWN),
      m_prTouchData(NULL)
{
}

/***************************************************************************
 ** FUNCTION:  CmdTouchEvent::CmdTouchEvent
 ***************************************************************************/
CmdTouchEvent::CmdTouchEvent(spi_tclInputHandler *pInputHandler,
      t_U32 u32DeviceHandle, tenDeviceCategory enDevCat,
      trTouchData &rfrTouchData): m_pInputHandler(NULL), m_u32DeviceHandle(0),
      m_enDevCat(e8DEV_TYPE_UNKNOWN), m_prTouchData(NULL)
{
   vAllocateMsg();
   m_pInputHandler = pInputHandler;
   m_u32DeviceHandle = u32DeviceHandle;
   m_enDevCat = enDevCat;
   if(NULL != m_prTouchData)
   {
      *(m_prTouchData) = rfrTouchData;
   }
}

/***************************************************************************
 ** FUNCTION:  CmdTouchEvent::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdTouchEvent, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdTouchEvent::vAllocateMsg
 ***************************************************************************/
t_Void CmdTouchEvent::vAllocateMsg()
{
   m_prTouchData = new trTouchData;
   SPI_NORMAL_ASSERT(NULL == m_prTouchData);
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconAttributes::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdTouchEvent::vDeAllocateMsg()
{
   RELEASE_MEM(m_prTouchData);
}

/****************************************************************************/
/*! \class CmdKeyEvent
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdKeyEvent::CmdKeyEvent
 ***************************************************************************/
CmdKeyEvent::CmdKeyEvent() :
                  m_pInputHandler(NULL),
                  m_u32DeviceHandle(0),
                  m_enKeyMode(e8KEY_RELEASE),
                  m_enKeyCode(e32ITU_KEY_0)
{
}
/***************************************************************************
 ** FUNCTION:  CmdKeyEvent::CmdKeyEvent
 ***************************************************************************/
CmdKeyEvent::CmdKeyEvent(spi_tclInputHandler *pInputHandler,
      t_U32 u32DeviceHandle, tenDeviceCategory enDevCat, tenKeyMode enKeyMode,
      tenKeyCode enKeyCode) : m_pInputHandler(NULL),
      m_u32DeviceHandle(0), m_enKeyMode(e8KEY_RELEASE), m_enKeyCode(e32ITU_KEY_0)
{
   vAllocateMsg();
   m_pInputHandler = pInputHandler;
   m_u32DeviceHandle = u32DeviceHandle;
   m_enDevCat = enDevCat;
   m_enKeyMode = enKeyMode;
   m_enKeyCode = enKeyCode;
}

/***************************************************************************
 ** FUNCTION:  CmdKeyEvent::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdKeyEvent, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdClientCapabilities
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdClientCapabilities::CmdClientCapabilities
 ***************************************************************************/
CmdClientCapabilities::CmdClientCapabilities(): m_poVideo(NULL)
{
}
/***************************************************************************
 ** FUNCTION:  CmdClientCapabilities::CmdClientCapabilities
 ***************************************************************************/
CmdClientCapabilities::CmdClientCapabilities(spi_tclVideo *poVideo,
                                             trClientCapabilities rClientCapabilities) : m_poVideo(NULL)
{
   vAllocateMsg();
   m_poVideo = poVideo;
   m_rClientCapabilities = rClientCapabilities;

}

/***************************************************************************
 ** FUNCTION:  CmdClientCapabilities::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdClientCapabilities, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdAccessoryDisplayContext
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAccessoryDisplayContext::CmdAccessoryDisplayContext
 ***************************************************************************/
CmdAccessoryDisplayContext::CmdAccessoryDisplayContext() : m_pResourceMngr(NULL),
         m_u32DeviceHandle(0), m_bDisplayFlag(false), m_enDisplayContext(e8DISPLAY_CONTEXT_UNKNOWN)
{
}

/***************************************************************************
 ** FUNCTION:  CmdAccessoryDisplayContext::CmdAccessoryDisplayContext
 ***************************************************************************/
CmdAccessoryDisplayContext::CmdAccessoryDisplayContext(
         spi_tclResourceMngr *pResourceMngr, t_U32 u32DeviceHandle,
         t_Bool bDisplayFlag, tenDisplayContext enDisplayContext) : m_pResourceMngr(NULL),
         m_u32DeviceHandle(0), m_bDisplayFlag(false), m_enDisplayContext(e8DISPLAY_CONTEXT_UNKNOWN)
{
   vAllocateMsg();
   m_pResourceMngr = pResourceMngr;
   m_u32DeviceHandle = u32DeviceHandle;
   m_bDisplayFlag = bDisplayFlag;
   m_enDisplayContext = enDisplayContext;
}

/***************************************************************************
 ** FUNCTION:  CmdAccessoryDisplayContext::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAccessoryDisplayContext, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdAccessoryAudioContext
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAccessoryAudioContext::CmdAccessoryAudioContext
 ***************************************************************************/
CmdAccessoryAudioContext::CmdAccessoryAudioContext() :
                  m_pResourceMngr(NULL),
                  m_u32DeviceHandle(0),
                  m_u8AudioCntxt(0),
                  m_bReqFlag(false)
{
   //default
}

/***************************************************************************
 ** FUNCTION:  CmdAccessoryAudioContext::CmdAccessoryAudioContext
 ***************************************************************************/
CmdAccessoryAudioContext::CmdAccessoryAudioContext(
         spi_tclResourceMngr *pResourceMngr, t_U32 u32DeviceHandle,
         t_U8 u8AudioCntxt, t_Bool bReqFlag) : m_pResourceMngr(NULL),
         m_u32DeviceHandle(0), m_u8AudioCntxt(0), m_bReqFlag(false)
{
   vAllocateMsg();
   m_pResourceMngr = pResourceMngr;
   m_u32DeviceHandle = u32DeviceHandle;
   m_u8AudioCntxt = u8AudioCntxt;
   m_bReqFlag = bReqFlag;
}

/***************************************************************************
 ** FUNCTION:  CmdAccessoryAudioContext::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAccessoryAudioContext, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdAccessoryAppStateContext
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAccessoryAppStateContext::CmdAccessoryAppStateContext
 ***************************************************************************/
CmdAccessoryAppStateContext::CmdAccessoryAppStateContext() :
                  m_pResourceMngr(NULL),
                  m_enNavAppState(e8SPI_NAV_UNKNOWN),
                  m_enPhoneAppState(e8SPI_PHONE_UNKNOWN),
                  m_enSpeechAppState(e8SPI_SPEECH_UNKNOWN)
{

}

/***************************************************************************
 ** FUNCTION:  CmdAccessoryAppStateContext::CmdAccessoryAppStateContext
 ***************************************************************************/
CmdAccessoryAppStateContext::CmdAccessoryAppStateContext(
         spi_tclResourceMngr *pResourceMngr, tenSpeechAppState enSpeechAppState,
         tenPhoneAppState enPhoneAppState, tenNavAppState enNavAppState) : m_pResourceMngr(NULL),
         m_enNavAppState(e8SPI_NAV_UNKNOWN), m_enPhoneAppState(e8SPI_PHONE_UNKNOWN), m_enSpeechAppState(e8SPI_SPEECH_UNKNOWN)
{
   vAllocateMsg();
   m_pResourceMngr = pResourceMngr;
   m_enNavAppState = enNavAppState;
   m_enPhoneAppState = enPhoneAppState;
   m_enSpeechAppState = enSpeechAppState;
}

/***************************************************************************
 ** FUNCTION:  CmdAccessoryAppStateContext::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAccessoryAppStateContext, spi_tclCmdDispatcher)



/****************************************************************************/
/*! \class CmdSetRegion
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdSetRegion::CmdSetRegion
 ***************************************************************************/
CmdSetRegion::CmdSetRegion() : m_poAppMngr(NULL), m_enRegion(e8_INVALID)
{
}

/***************************************************************************
 ** FUNCTION:  CmdSetRegion::CmdSetRegion
 ***************************************************************************/
CmdSetRegion::CmdSetRegion(spi_tclAppMngr *poAppMngr,tenRegion enRegion): m_poAppMngr(NULL),
         m_enRegion(e8_INVALID)
{
   vAllocateMsg();
   m_poAppMngr = poAppMngr;
   m_enRegion = enRegion;
}

/***************************************************************************
 ** FUNCTION:  CmdSetRegion::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdSetRegion, spi_tclCmdDispatcher)





/****************************************************************************/
/*! \class CmdAllocateAudioRoute
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAllocateAudioRoute::CmdAllocateAudioRoute
 ***************************************************************************/
CmdAllocateAudioRoute::CmdAllocateAudioRoute() :
         m_poAudio(NULL),m_prAudSrcInfo(NULL),m_u8SourceNum(0)
{
}

/***************************************************************************
 ** FUNCTION:  CmdAllocateAudioRoute::CmdAllocateAudioRoute
 ***************************************************************************/
CmdAllocateAudioRoute::CmdAllocateAudioRoute(spi_tclAudio *poAudio,
         const t_U8 cou8SourceNum, trAudSrcInfo &rfrAudSrcInfo): m_poAudio(NULL),
         m_prAudSrcInfo(NULL), m_u8SourceNum(0)
{
   vAllocateMsg();
   m_poAudio = poAudio;
   m_u8SourceNum = cou8SourceNum;
   if (NULL != m_prAudSrcInfo)
   {
      (*m_prAudSrcInfo) = rfrAudSrcInfo;
   }
}

/***************************************************************************
 ** FUNCTION:  CmdAllocateAudioRoute::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAllocateAudioRoute, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdAllocateAudioRoute::vAllocateMsg
 ***************************************************************************/
t_Void CmdAllocateAudioRoute::vAllocateMsg()
{
   m_prAudSrcInfo = new trAudSrcInfo;
   SPI_NORMAL_ASSERT(NULL == m_prAudSrcInfo);
}

/***************************************************************************
 ** FUNCTION:  CmdAllocateAudioRoute::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdAllocateAudioRoute::vDeAllocateMsg()
{
   RELEASE_MEM(m_prAudSrcInfo);
}


/****************************************************************************/
/*! \class CmdDeAllocateAudioRoute
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdDeAllocateAudioRoute::CmdDeAllocateAudioRoute
 ***************************************************************************/
CmdDeAllocateAudioRoute::CmdDeAllocateAudioRoute() :
         m_poAudio(NULL), m_u8SourceNum(0)
{
}

/***************************************************************************
 ** FUNCTION:  CmdDeAllocateAudioRoute::CmdDeAllocateAudioRoute
 ***************************************************************************/
CmdDeAllocateAudioRoute::CmdDeAllocateAudioRoute(spi_tclAudio *poAudio,
         const t_U8 cou8SourceNum) : m_poAudio(NULL), m_u8SourceNum(0)
{
   vAllocateMsg();
   m_poAudio = poAudio;
   m_u8SourceNum = cou8SourceNum;
}

/***************************************************************************
 ** FUNCTION:  CmdDeAllocateAudioRoute::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdDeAllocateAudioRoute, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdStartAudioSrcActivity
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdStartAudioSrcActivity::CmdStartAudioSrcActivity
 ***************************************************************************/
CmdStartAudioSrcActivity::CmdStartAudioSrcActivity() : m_poAudio(NULL), m_u8SourceNum(0)
{
}

/***************************************************************************
 ** FUNCTION:  CmdStartAudioSrcActivity::CmdStartAudioSrcActivity
 ***************************************************************************/
CmdStartAudioSrcActivity::CmdStartAudioSrcActivity(spi_tclAudio *poAudio,
         const t_U8 cou8SourceNum) : m_poAudio(NULL), m_u8SourceNum(0)
{
   vAllocateMsg();
   m_poAudio = poAudio;
   m_u8SourceNum = cou8SourceNum;
}

/***************************************************************************
 ** FUNCTION:  CmdStartAudioSrcActivity::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdStartAudioSrcActivity, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdStopAudioSrcActivity
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdStopAudioSrcActivity::CmdStopAudioSrcActivity
 ***************************************************************************/
CmdStopAudioSrcActivity::CmdStopAudioSrcActivity() : m_poAudio(NULL), m_u8SourceNum(0)
{
}

/***************************************************************************
 ** FUNCTION:  CmdStopAudioSrcActivity::CmdStopAudioSrcActivity
 ***************************************************************************/
CmdStopAudioSrcActivity::CmdStopAudioSrcActivity(spi_tclAudio *poAudio,
         const t_U8 cou8SourceNum) : m_poAudio(NULL), m_u8SourceNum(0)
{
   vAllocateMsg();
   m_poAudio = poAudio;
   m_u8SourceNum = cou8SourceNum;
}

/***************************************************************************
 ** FUNCTION:  CmdStopAudioSrcActivity::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdStopAudioSrcActivity, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdReqAVDeactResult
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdReqAVDeactResult::CmdReqAVDeactResult
 ***************************************************************************/
CmdReqAVDeactResult::CmdReqAVDeactResult() : m_poAudio(NULL), m_u8SourceNum(0)
{
}

/***************************************************************************
 ** FUNCTION:  CmdReqAVDeactResult::CmdReqAVDeactResult
 ***************************************************************************/
CmdReqAVDeactResult::CmdReqAVDeactResult(spi_tclAudio *poAudio,
         const t_U8 cou8SourceNum) : m_poAudio(NULL), m_u8SourceNum(0)
{
   vAllocateMsg();
   m_poAudio = poAudio;
   m_u8SourceNum = cou8SourceNum;
}

/***************************************************************************
 ** FUNCTION:  CmdReqAVDeactResult::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdReqAVDeactResult, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdAudioError
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAudioError::CmdAudioError
 ***************************************************************************/
CmdAudioError::CmdAudioError() : m_poAudio(NULL), m_u8SourceNum(0), m_enAudioError(e8_AUDIOERROR_NONE)
{
}

/***************************************************************************
 ** FUNCTION:  CmdAudioError::CmdAudioError
 ***************************************************************************/
CmdAudioError::CmdAudioError(spi_tclAudio *poAudio,
         const t_U8 cou8SourceNum, tenAudioError enAudioError) : m_poAudio(NULL),
         m_u8SourceNum(0), m_enAudioError(e8_AUDIOERROR_NONE)
{
   vAllocateMsg();
   m_poAudio = poAudio;
   m_u8SourceNum = cou8SourceNum;
   m_enAudioError = enAudioError;
}

/***************************************************************************
 ** FUNCTION:  CmdAudioError::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAudioError, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdBTDeviceAction
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdBTDeviceAction::CmdBTDeviceAction
 ***************************************************************************/
CmdBTDeviceAction::CmdBTDeviceAction() :
                  m_poBluetooth(NULL),
                  m_u32BluetoothDevHandle(0),
                  m_u32ProjectionDevHandle(0),
                  m_enBTChange(e8NO_CHANGE)
{
}

/***************************************************************************
 ** FUNCTION:  CmdBTDeviceAction::CmdBTDeviceAction
 ***************************************************************************/
CmdBTDeviceAction::CmdBTDeviceAction(spi_tclBluetooth *poBluetooth,
         t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange) : m_poBluetooth(NULL),
         m_u32BluetoothDevHandle(0), m_u32ProjectionDevHandle(0), m_enBTChange(e8NO_CHANGE)
{
   vAllocateMsg();
   m_poBluetooth = poBluetooth;
   m_u32BluetoothDevHandle = u32BluetoothDevHandle;
   m_u32ProjectionDevHandle = u32ProjectionDevHandle;
   m_enBTChange = enBTChange;
}

/***************************************************************************
 ** FUNCTION:  CmdBTDeviceAction::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdBTDeviceAction, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdVehicleBTAddress
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdVehicleBTAddress::CmdVehicleBTAddress
 ***************************************************************************/
 CmdVehicleBTAddress::CmdVehicleBTAddress(spi_tclAppMngr *poAppMngr,
      t_U32 u32DeviceHandle,
      t_String szBTAddress,
      tenDeviceCategory enDevCat):m_poAppMngr(NULL), m_u32DeviceHandle(0),
      m_pszBTAddress(NULL), m_enDevCat(e8DEV_TYPE_UNKNOWN)
{
   vAllocateMsg();
   m_poAppMngr = poAppMngr;
   m_u32DeviceHandle = u32DeviceHandle;
   if(NULL != m_pszBTAddress)
   {
      (*m_pszBTAddress) = szBTAddress;
   }
   m_enDevCat = enDevCat;
}

/***************************************************************************
 ** FUNCTION:  CmdVehicleBTAddress::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdVehicleBTAddress, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdVehicleBTAddress::vAllocateMsg
 ***************************************************************************/
t_Void CmdVehicleBTAddress::vAllocateMsg()
{
   m_pszBTAddress = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_pszBTAddress);
}

/***************************************************************************
 ** FUNCTION:  CmdVehicleBTAddress::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdVehicleBTAddress::vDeAllocateMsg()
{
   RELEASE_MEM(m_pszBTAddress);
}


/****************************************************************************/
/*! \class CmdCallStatus
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdCallStatus::CmdCallStatus
 ***************************************************************************/
 CmdCallStatus::CmdCallStatus(spi_tclBluetooth *poBluetooth, t_Bool bCallActive)
                : m_poBluetooth(NULL), m_bCallActive(false)
{
   vAllocateMsg();
   m_poBluetooth = poBluetooth;
   m_bCallActive = bCallActive;
}

/***************************************************************************
 ** FUNCTION:  CmdCallStatus::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdCallStatus, spi_tclCmdDispatcher)


/****************************************************************************/
/*! \class CmdGPSData
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdGPSData::CmdGPSData
 ***************************************************************************/
 CmdGPSData::CmdGPSData(spi_tclDataService *poDataService, const trGPSData &rfcorGPSData) :
             m_poDataService(NULL)
{
   vAllocateMsg();
   m_poDataService = poDataService;
   m_rGPSData = rfcorGPSData;
}

/***************************************************************************
 ** FUNCTION:  CmdGPSData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdGPSData, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdSensorData
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdSensorData::CmdSensorData
 ***************************************************************************/
 CmdSensorData::CmdSensorData(spi_tclDataService *poDataService, const trSensorData &rfcorSensorData):
                   m_poDataService(NULL)
{
   vAllocateMsg();
   m_poDataService = poDataService;
   m_rSensorData = rfcorSensorData;
}

/***************************************************************************
 ** FUNCTION:  CmdSensorData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdSensorData, spi_tclCmdDispatcher)

/****************************************************************************/
/*! \class CmdAccSensorData
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAccSensorData::CmdAccSensorData
 ***************************************************************************/
CmdAccSensorData::CmdAccSensorData(spi_tclDataService *poDataService,
         const std::vector<trAccSensorData>& corfvecrAccSensorData): m_poDataService(NULL),
         m_pVecrAccSensorData(NULL)
{
   vAllocateMsg();
   m_poDataService = poDataService;
   if(NULL != m_pVecrAccSensorData)
   {
      (*m_pVecrAccSensorData) = corfvecrAccSensorData;
   }
}

/***************************************************************************
 ** FUNCTION:  CmdAccSensorData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAccSensorData, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdAccSensorData::vAllocateMsg
 ***************************************************************************/
t_Void CmdAccSensorData::vAllocateMsg()
{
   m_pVecrAccSensorData = new std::vector<trAccSensorData>;
   SPI_NORMAL_ASSERT(NULL == m_pVecrAccSensorData);
}

/***************************************************************************
 ** FUNCTION:  CmdAccSensorData::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdAccSensorData::vDeAllocateMsg()
{
   RELEASE_MEM(m_pVecrAccSensorData);
}

/****************************************************************************/
/*! \class CmdGyroSensorData
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdGyroSensorData::CmdGyroSensorData
 ***************************************************************************/
CmdGyroSensorData::CmdGyroSensorData(spi_tclDataService *poDataService,
         const std::vector<trGyroSensorData>& corfvecrGyroSensorData): m_poDataService(NULL),
         m_pVecrGyroSensorData(NULL)
{
   vAllocateMsg();
   m_poDataService = poDataService;
   if(NULL != m_pVecrGyroSensorData)
   {
      (*m_pVecrGyroSensorData) = corfvecrGyroSensorData;
   }
}

/***************************************************************************
 ** FUNCTION:  CmdGyroSensorData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdGyroSensorData, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdGyroSensorData::vAllocateMsg
 ***************************************************************************/
t_Void CmdGyroSensorData::vAllocateMsg()
{
	m_pVecrGyroSensorData = new std::vector<trGyroSensorData>;
   SPI_NORMAL_ASSERT(NULL == m_pVecrGyroSensorData);
}

/***************************************************************************
 ** FUNCTION:  CmdGyroSensorData::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdGyroSensorData::vDeAllocateMsg()
{
   RELEASE_MEM(m_pVecrGyroSensorData);
}

/****************************************************************************/
/*! \class CmdSensorData
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdVehicleData::CmdVehicleData
 ***************************************************************************/
 CmdVehicleData::CmdVehicleData(spi_tclDataService *poDataService, spi_tclVideo *poVideo,
       spi_tclAppMngr *poAppMngr ,const trVehicleData &rfcorVehicleData,
       t_Bool bSolicited) : m_poDataService(NULL), m_poVideo(NULL), m_poAppMngr(NULL), m_bSolicited(false)
{
   vAllocateMsg();
   m_poDataService = poDataService;
   m_poVideo = poVideo;
   m_poAppMngr = poAppMngr;
   m_rVehicleData = rfcorVehicleData;
   m_bSolicited = bSolicited;
}

/***************************************************************************
 ** FUNCTION:  CmdVehicleData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdVehicleData, spi_tclCmdDispatcher)


/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::spi_tclCmdDispatcher
 ***************************************************************************/
spi_tclCmdDispatcher::spi_tclCmdDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::~spi_tclCmdDispatcher
 ***************************************************************************/
spi_tclCmdDispatcher::~spi_tclCmdDispatcher()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSelectDevice* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdSelectDevice* poSelectDevice)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poSelectDevice) && (NULL != poSelectDevice->m_pDeviceSelector))
   {
      trUserContext rUsrCtxt;
      poSelectDevice->vGetUserContext(rUsrCtxt);
      poSelectDevice->m_pDeviceSelector->vSelectDevice(
            poSelectDevice->m_u32DeviceHandle, poSelectDevice->m_enDevConnType,
            poSelectDevice->m_enDevConnReq, poSelectDevice->m_enCDBUsage,
            poSelectDevice->m_enCDBUsage, poSelectDevice->m_enDevCat, poSelectDevice->m_bIsHMITrigger, rUsrCtxt);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdLaunchApp* poCmdLaunchApp)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdLaunchApp* poLaunchApp)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poLaunchApp) && (NULL != poLaunchApp->m_pAppLauncher)
            && (NULL != poLaunchApp->m_pszTelephoneNumber))
   {
      trUserContext rUsrCtxt;
      poLaunchApp->vGetUserContext(rUsrCtxt);
      poLaunchApp->m_pAppLauncher->vLaunchApp(poLaunchApp->m_u32DeviceHandle,
            poLaunchApp->m_u32AppHandle, poLaunchApp->m_enDiPOAppType,
            poLaunchApp->m_pszTelephoneNumber->c_str(),
            poLaunchApp->m_enEcnrSetting, rUsrCtxt);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdTerminateApp* poTerminateApp)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdTerminateApp* poTerminateApp)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poTerminateApp) && (NULL != poTerminateApp->m_pAppLauncher))
   {
      trUserContext rUsrCtxt;
      poTerminateApp->vGetUserContext(rUsrCtxt);
      poTerminateApp->m_pAppLauncher->vTerminateApp(
            poTerminateApp->m_u32DeviceHandle, poTerminateApp->m_u32AppHandle,
            rUsrCtxt);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAppIconData* poAppIconData)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdAppIconData* poAppIconData)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAppIconData) && (NULL != poAppIconData->m_pAppMngr)
            && (NULL != poAppIconData->m_pszAppIconURL))
   {
      trUserContext rUsrCtxt;
      poAppIconData->vGetUserContext(rUsrCtxt);
      poAppIconData->m_pAppMngr->vGetAppIconData(
            poAppIconData->m_pszAppIconURL->c_str(), poAppIconData->m_enDevCat,
            rUsrCtxt);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAppIconAttributes* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdAppIconAttributes* poAppIconAttributes)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAppIconAttributes) && (NULL != poAppIconAttributes->m_pAppMngr)
            && (NULL != poAppIconAttributes->m_prIconAttributes))
   {
      trUserContext rUsrCtxt;
      poAppIconAttributes->vGetUserContext(rUsrCtxt);
      poAppIconAttributes->m_pAppMngr->vSetAppIconAttributes(
            poAppIconAttributes->m_u32DeviceHandle,
            poAppIconAttributes->m_u32AppHandle,
            poAppIconAttributes->m_enDevCat,
            *(poAppIconAttributes->m_prIconAttributes), rUsrCtxt);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdDeviceUsagePreference* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdDeviceUsagePreference* poDeviceUsagePreference)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poDeviceUsagePreference) && (NULL
         != poDeviceUsagePreference->m_poConnMngr))
   {
      trUserContext rUsrCtxt;
      poDeviceUsagePreference->vGetUserContext(rUsrCtxt);
      poDeviceUsagePreference->m_poConnMngr->vSetDeviceUsagePreference(
            poDeviceUsagePreference->m_u32DeviceHandle,
            poDeviceUsagePreference->m_enDeviceCategory,
            poDeviceUsagePreference->m_enEnabledInfo, rUsrCtxt);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdMLNotificationEnabledInfo* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdMLNotificationEnabledInfo* poNotiEnableInfo)
{
   if ((NULL != poNotiEnableInfo)
            && (NULL != poNotiEnableInfo->m_pAppMngr)
            && (NULL != poNotiEnableInfo->m_pvecrNotificationEnableList))
   {
      trUserContext rUsrCtxt;
      poNotiEnableInfo->vGetUserContext(rUsrCtxt);

      poNotiEnableInfo->m_pAppMngr->vSetMLNotificationEnabledInfo(
               poNotiEnableInfo->m_u32DeviceHandle,
               poNotiEnableInfo->m_u32NumNotificationEnableList,
               *(poNotiEnableInfo->m_pvecrNotificationEnableList),
               poNotiEnableInfo->m_enDevCat,rUsrCtxt );
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetMLNotificationOnOff* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdSetMLNotificationOnOff* poSetMLNotificationOnOff)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poSetMLNotificationOnOff) && (NULL != poSetMLNotificationOnOff->m_poAppMngr))
   {
      poSetMLNotificationOnOff->m_poAppMngr->vSetMLNotificationOnOff(
               poSetMLNotificationOnOff->m_bNotificationState);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdInvokeNotificationAction* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdInvokeNotificationAction* poInvokeNotificationAction)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poInvokeNotificationAction)&&(NULL != poInvokeNotificationAction->m_poAppMngr))
   {
      trUserContext rUsrCtxt;
      poInvokeNotificationAction->vGetUserContext(rUsrCtxt);
      poInvokeNotificationAction->m_poAppMngr->vInvokeNotificationAction(
         poInvokeNotificationAction->m_u32DeviceHandle,
         poInvokeNotificationAction->m_u32NotificationID,
         poInvokeNotificationAction->m_u32NotificationActionID,
         poInvokeNotificationAction->m_enDevCat,
         rUsrCtxt);
   }//if ((NULL != poInvokeNotificatio
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdOrientationMode* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdOrientationMode* poOrientationMode)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poOrientationMode) && (NULL != poOrientationMode->m_poVideo))
   {
      trUserContext rUsrCtxt;
      poOrientationMode->vGetUserContext(rUsrCtxt);
      poOrientationMode->m_poVideo->vSetOrientationMode(
            poOrientationMode->m_u32DeviceHandle,
            poOrientationMode->m_enOrientationMode,
            poOrientationMode->m_enDevCat, rUsrCtxt);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdScreenSize* poScreenSize)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdScreenSize* poScreenSize)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poScreenSize) && (NULL != poScreenSize->m_poVideo))
   {
      trUserContext rUsrCtxt;
      poScreenSize->vGetUserContext(rUsrCtxt);
      poScreenSize->m_poVideo->vSetScreenSize(poScreenSize->m_rScreenAttributes,
            poScreenSize->m_enDevCat, rUsrCtxt);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetVideoBlocking* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetVideoBlocking* poBlockingMode)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   // TODO : Evaluate audio or video blocking when both are used
   if ((NULL != poBlockingMode) && (NULL != poBlockingMode->m_poVideo))
   {
      trUserContext rUsrCtxt;
      poBlockingMode->vGetUserContext(rUsrCtxt);
         poBlockingMode->m_poVideo->vSetVideoBlockingMode(
               poBlockingMode->m_u32DeviceHandle,
               poBlockingMode->m_enBlockingMode, poBlockingMode->m_enDevCat,
               rUsrCtxt);
   }
}
/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetAudioBlocking* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetAudioBlocking* poBlockingMode)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR1((" poBlockingMode->m_poAudio = %p \n", poBlockingMode->m_poAudio));
   if ((NULL != poBlockingMode) && (NULL != poBlockingMode->m_poAudio))
   {
      trUserContext rUsrCtxt;
      poBlockingMode->vGetUserContext(rUsrCtxt);
      poBlockingMode->m_poAudio->bSetAudioBlockingMode(
               poBlockingMode->m_u32DeviceHandle,
               poBlockingMode->m_enBlockingMode, poBlockingMode->m_enDevCat);
   }
}
/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetVehicleConfig* ..)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetVehicleConfig* poSetVehicleConfig)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   // TODO : Evaluate audio or video blocking when both are used
   if ((NULL != poSetVehicleConfig) )
   {
//      trUserContext rUsrCtxt;
//      poSetVehicleConfig->vGetUserContext(rUsrCtxt);
      if (NULL != poSetVehicleConfig->m_poVideo)
      {
         poSetVehicleConfig->m_poVideo->vSetVehicleConfig(
                  poSetVehicleConfig->m_enVehicleConfig,
                  poSetVehicleConfig->m_bSetConfig);
      }

      if (NULL != poSetVehicleConfig->m_poAppMngr)
      {
         poSetVehicleConfig->m_poAppMngr->vSetVehicleConfig(
                  poSetVehicleConfig->m_enVehicleConfig,
                  poSetVehicleConfig->m_bSetConfig);
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdTouchEvent* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdTouchEvent* poTouchEvent)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poTouchEvent) && (NULL != poTouchEvent->m_pInputHandler)
            && (NULL != poTouchEvent->m_prTouchData))
   {
      trUserContext rUsrCtxt;
      poTouchEvent->vGetUserContext(rUsrCtxt);
      poTouchEvent->m_pInputHandler->vProcessTouchEvent(
            poTouchEvent->m_u32DeviceHandle, poTouchEvent->m_enDevCat,
            *(poTouchEvent->m_prTouchData));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdKeyEvent* poKeyEvent)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdKeyEvent* poKeyEvent)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poKeyEvent) && (NULL != poKeyEvent->m_pInputHandler))
   {
      trUserContext rUsrCtxt;
      poKeyEvent->vGetUserContext(rUsrCtxt);
      poKeyEvent->m_pInputHandler->vProcessKeyEvents(
            poKeyEvent->m_u32DeviceHandle, poKeyEvent->m_enDevCat,
            poKeyEvent->m_enKeyMode, poKeyEvent->m_enKeyCode);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdClientCapabilities* poClientCapabilities)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdClientCapabilities* poClientCapabilities)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poClientCapabilities) && (NULL != poClientCapabilities->m_poVideo))
   {
      trUserContext rUsrCtxt;
      poClientCapabilities->vGetUserContext(rUsrCtxt);
      poClientCapabilities->m_poVideo->vSetClientCapabilities(
           poClientCapabilities->m_rClientCapabilities);
   }
}


/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAccessoryDisplayContext* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdAccessoryDisplayContext* poAccessoryDisplayContext)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAccessoryDisplayContext))
   {
      trUserContext rUsrCtxt;
      poAccessoryDisplayContext->vGetUserContext(rUsrCtxt);
      // TODO : Add code when used
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAccessoryAudioContext* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdAccessoryAudioContext* poAccessoryAudioContext)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAccessoryAudioContext))
   {
      trUserContext rUsrCtxt;
      poAccessoryAudioContext->vGetUserContext(rUsrCtxt);
      // TODO : Add code when used
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAccessoryAppStateContext* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdAccessoryAppStateContext* poAccessoryAppStateContext)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAccessoryAppStateContext))
   {
      trUserContext rUsrCtxt;
      poAccessoryAppStateContext->vGetUserContext(rUsrCtxt);
      // TODO : Add code when used
   }
}
/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetRegion* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdSetRegion* poSetRegion)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poSetRegion) && (NULL
         != poSetRegion->m_poAppMngr))
   {
      poSetRegion->m_poAppMngr->vSetRegion( poSetRegion->m_enRegion);
   }
}
/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAllocateAudioRoute* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdAllocateAudioRoute* poAllocateAudioRoute)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAllocateAudioRoute) && (NULL
         != poAllocateAudioRoute->m_poAudio) &&
         (NULL != poAllocateAudioRoute->m_prAudSrcInfo))
   {
      poAllocateAudioRoute->m_poAudio->bOnRouteAllocateResult(
               poAllocateAudioRoute->m_u8SourceNum,
               *(poAllocateAudioRoute->m_prAudSrcInfo));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdDeAllocateAudioRoute* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdDeAllocateAudioRoute* poDeAllocateAudioRoute)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poDeAllocateAudioRoute) && (NULL
         != poDeAllocateAudioRoute->m_poAudio))
   {
      poDeAllocateAudioRoute->m_poAudio->vOnRouteDeAllocateResult(
               poDeAllocateAudioRoute->m_u8SourceNum);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdStartAudioSrcActivity* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdStartAudioSrcActivity* poAudioSrcActivity)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAudioSrcActivity) && (NULL != poAudioSrcActivity->m_poAudio))
   {
      poAudioSrcActivity->m_poAudio->vOnStartSourceActivity(
               poAudioSrcActivity->m_u8SourceNum);
   }
}


/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdStopAudioSrcActivity* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdStopAudioSrcActivity* poAudioSrcActivity)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAudioSrcActivity) && (NULL != poAudioSrcActivity->m_poAudio))
   {
      poAudioSrcActivity->m_poAudio->vOnStopSourceActivity(
               poAudioSrcActivity->m_u8SourceNum);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdReqAVDeactResult* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdReqAVDeactResult* poReqAVDeactResult)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poReqAVDeactResult) && (NULL != poReqAVDeactResult->m_poAudio))
   {
      poReqAVDeactResult->m_poAudio->bOnReqAVDeActivationResult(
               poReqAVDeactResult->m_u8SourceNum);
   }
}
/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAudioError* ...)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(
      CmdAudioError* poAudioError)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poAudioError) && (NULL != poAudioError->m_poAudio))
   {
      poAudioError->m_poAudio->vOnAudioError(poAudioError->m_u8SourceNum, poAudioError->m_enAudioError);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdBTDeviceAction* poBTDeviceAction)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdBTDeviceAction* poBTDeviceAction)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poBTDeviceAction) && (NULL != poBTDeviceAction->m_poBluetooth))
   {
      poBTDeviceAction->m_poBluetooth->vOnInvokeBTDeviceAction(
               poBTDeviceAction->m_u32BluetoothDevHandle,
               poBTDeviceAction->m_u32ProjectionDevHandle,
               poBTDeviceAction->m_enBTChange);
   }
}

/***************************************************************************
** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(C)
***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdVehicleBTAddress* poBTAddress)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poBTAddress) && (NULL != poBTAddress->m_poAppMngr) && (NULL != poBTAddress->m_pszBTAddress))
   {
      trUserContext rUsrCtxt;
      poBTAddress->vGetUserContext(rUsrCtxt);
      poBTAddress->m_poAppMngr->vSetVehicleBTAddress(poBTAddress->m_u32DeviceHandle,
         *(poBTAddress->m_pszBTAddress),poBTAddress->m_enDevCat,rUsrCtxt);
   }//if ((NULL != poBTAddress) && (NULL != poBTAddress->m_poAppMngr))
}


/***************************************************************************
** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(C)
***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdCallStatus* poCallStatus)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poCallStatus) && (NULL != poCallStatus->m_poBluetooth))
   {
      poCallStatus->m_poBluetooth->vOnCallStatus(poCallStatus->m_bCallActive);
   }//if ((NULL != poCallStatus) && (NULL != poCallStatus->m_poBluetooth))
}

/***************************************************************************
** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(C)
***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdGPSData* poGPSData)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poGPSData) && (NULL != poGPSData->m_poDataService))
   {
      poGPSData->m_poDataService->vOnGPSData(poGPSData->m_rGPSData);
   }//if ((NULL != poGPSData) && (NULL != poGPSData->m_poDataService))
}

/***************************************************************************
** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(C)
***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdSensorData* poSensorData)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poSensorData) && (NULL != poSensorData->m_poDataService))
   {
      poSensorData->m_poDataService->vOnSensorData(poSensorData->m_rSensorData);
   }// if ((NULL != poSensorData) && (NULL != poSensorData->m_poDataService))
}

/***************************************************************************
** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(C)
***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdAccSensorData* poAccSensorData)
{
   ETG_TRACE_USR1(("spi_tclCmdDispatcher::vHandleCmdMsg CmdAccSensorData entered \n"));
   if ((NULL != poAccSensorData) && (NULL != poAccSensorData->m_poDataService))
   {
	   poAccSensorData->m_poDataService->vOnAccSensorData(*(poAccSensorData->m_pVecrAccSensorData));
   }// if ((NULL != poAccSensorData) && (NULL != poAccSensorData->m_poDataService))
}

/***************************************************************************
** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(C)
***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdGyroSensorData* poGyroSensorData)
{
   ETG_TRACE_USR1((" spi_tclCmdDispatcher::vHandleCmdMsg CmdGyroSensorData entered \n"));
   if ((NULL != poGyroSensorData) && (NULL != poGyroSensorData->m_poDataService))
   {
	   poGyroSensorData->m_poDataService->vOnGyroSensorData(*(poGyroSensorData->m_pVecrGyroSensorData));
   }// if ((NULL != poGyroSensorData) && (NULL != poGyroSensorData->m_poDataService))
}

/***************************************************************************
** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(C)
***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdVehicleData* poVehicleData)
{
   ETG_TRACE_USR1((" spi_tclCmdDispatcher::vHandleCmdMsg CmdVehicleData entered"));
   if(NULL != poVehicleData)
   {
      if(NULL != poVehicleData->m_poDataService)
      {
         poVehicleData->m_poDataService->vOnVehicleData(poVehicleData->m_rVehicleData,poVehicleData->m_bSolicited);
      }
      if (NULL != poVehicleData->m_poVideo)
      {
         poVehicleData->m_poVideo->vOnVehicleData(poVehicleData->m_rVehicleData,poVehicleData->m_bSolicited);
      }
      if (NULL != poVehicleData->m_poAppMngr)
      {
         poVehicleData->m_poAppMngr->vOnVehicleData(poVehicleData->m_rVehicleData,poVehicleData->m_bSolicited);
      }
   }//if ((NULL != poVehicleData) && (NULL != poVehicleData->m_poDataService))
}
/****************************************************************************/
/*! \class CmdKnobKeyEvent
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdKnobKeyEvent::CmdKeyEvent
 ***************************************************************************/
CmdKnobKeyEvent::CmdKnobKeyEvent() :
                  m_pInputHandler(NULL),
                  m_u32DeviceHandle(0),
                  m_s8EncoderDeltaCnt(0)
{
}
/***************************************************************************
 ** FUNCTION:  CmdKnobKeyEvent::CmdKnobKeyEvent
 ***************************************************************************/
CmdKnobKeyEvent::CmdKnobKeyEvent(spi_tclInputHandler *pInputHandler,
      t_U32 u32DeviceHandle, tenDeviceCategory enDevCat, t_S8 s8EncoderDeltaCnt)
{
   vAllocateMsg();
   m_pInputHandler = pInputHandler;
   m_u32DeviceHandle = u32DeviceHandle;
   m_enDevCat = enDevCat;
   m_s8EncoderDeltaCnt = s8EncoderDeltaCnt;

}

/***************************************************************************
 ** FUNCTION:  CmdKnobKeyEvent::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdKnobKeyEvent, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdKnobKeyEvent* poKnobKeyEvent)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdKnobKeyEvent* poKnobKeyEvent)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != poKnobKeyEvent) && (NULL != poKnobKeyEvent->m_pInputHandler))
   {
      trUserContext rUsrCtxt;
      poKnobKeyEvent->vGetUserContext(rUsrCtxt);
      poKnobKeyEvent->m_pInputHandler->vProcessKnobKeyEvents(
            poKnobKeyEvent->m_u32DeviceHandle, poKnobKeyEvent->m_enDevCat,
            poKnobKeyEvent->m_s8EncoderDeltaCnt);
   }
}

/****************************************************************************/
/*! \class CmdKeyIconData
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdKeyIconData::CmdKeyIconData
 ***************************************************************************/
CmdKeyIconData::CmdKeyIconData() :
      m_cou32DevId(0),m_pInputHandler(NULL), m_enDevCat(e8DEV_TYPE_UNKNOWN),
      m_pszKeyIconURL(NULL)
{
}

/***************************************************************************
 ** FUNCTION:  CmdKeyIconData::CmdKeyIconData
 ***************************************************************************/
CmdKeyIconData::CmdKeyIconData(t_U32 u32DeviceHandle,spi_tclInputHandler *pInputHandler,
      tenDeviceCategory enDevCat, t_String szKeyIconURL):m_cou32DevId(0), m_pInputHandler(NULL),
      m_enDevCat(e8DEV_TYPE_UNKNOWN), m_pszKeyIconURL(NULL)
{
   vAllocateMsg();
   m_cou32DevId = u32DeviceHandle;
   m_pInputHandler = pInputHandler;
   m_enDevCat = enDevCat;
   if(NULL != m_pszKeyIconURL)
   {
      *(m_pszKeyIconURL) = szKeyIconURL;
   }
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdKeyIconData, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  CmdKeyIconData::vAllocateMsg
 ***************************************************************************/
t_Void CmdKeyIconData::vAllocateMsg()
{
   m_pszKeyIconURL = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_pszKeyIconURL);
}

/***************************************************************************
 ** FUNCTION:  CmdKeyIconData::vDeAllocateMsg
 ***************************************************************************/
t_Void CmdKeyIconData::vDeAllocateMsg()
{
   RELEASE_MEM(m_pszKeyIconURL);
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdKeyIconData* pokeyIconData)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdKeyIconData* pokeyIconData)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if ((NULL != pokeyIconData) && (NULL != pokeyIconData->m_pInputHandler)
            && (NULL != pokeyIconData->m_pszKeyIconURL))
   {
      trUserContext rUsrCtxt;
      pokeyIconData->vGetUserContext(rUsrCtxt);
      pokeyIconData->m_pInputHandler->vGetKeyIconData(pokeyIconData->m_cou32DevId,
            pokeyIconData->m_pszKeyIconURL->c_str(), pokeyIconData->m_enDevCat,
            rUsrCtxt);
   }
}

/****************************************************************************/
/*! \class CmdSetFeatRestrData
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdSetFeatRestrData::CmdSetFeatRestrData
 ***************************************************************************/
CmdSetFeatRestrData::CmdSetFeatRestrData() :
      m_poDataService(NULL), m_poResourceMngr(NULL),
      m_enDevCategory(e8DEV_TYPE_UNKNOWN),
      m_u8ParkModeRestrictionInfo(0),
      m_u8DriveModeRestrictionInfo(0)
{
}

/***************************************************************************
 ** FUNCTION:  CmdSetFeatRestrData::CmdSetFeatRestrData
 ***************************************************************************/
CmdSetFeatRestrData::CmdSetFeatRestrData(spi_tclDataService* poDataService,
      spi_tclResourceMngr* poResourceMngr, tenDeviceCategory enDevCategory,
      t_U8 u8ParkModeRestrictionInfo, t_U8 u8DriveModeRestrictionInfo):
      m_poDataService(poDataService), m_poResourceMngr(poResourceMngr),
      m_enDevCategory(enDevCategory),
      m_u8ParkModeRestrictionInfo(u8ParkModeRestrictionInfo),
      m_u8DriveModeRestrictionInfo(u8DriveModeRestrictionInfo)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  CmdAppIconData::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdSetFeatRestrData, spi_tclCmdDispatcher)

/***************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetFeatRestrData* poSetFeatRestrData)
 ***************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdSetFeatRestrData* poSetFeatRestrData)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   if (NULL != poSetFeatRestrData)
   {
      //! Forward the data to DataService manager
      if (NULL != poSetFeatRestrData->m_poDataService)
      {
         poSetFeatRestrData->m_poDataService->vSetFeatureRestrictions(
               poSetFeatRestrData->m_enDevCategory,
               poSetFeatRestrData->m_u8ParkModeRestrictionInfo,
               poSetFeatRestrData->m_u8DriveModeRestrictionInfo);
      }
      //! Forward the data to Resource manager
      if (NULL != poSetFeatRestrData->m_poResourceMngr)
      {
         poSetFeatRestrData->m_poResourceMngr->vSetFeatureRestrictions(
               poSetFeatRestrData->m_enDevCategory,
               poSetFeatRestrData->m_u8ParkModeRestrictionInfo,
               poSetFeatRestrData->m_u8DriveModeRestrictionInfo);
      }
   }//if (NULL != poSetFeatRestrData)
}

/****************************************************************************/
/*! \class CmdAckNotificationEvent
 ****************************************************************************/
/***************************************************************************
 ** FUNCTION:  CmdAckNotificationEvent::CmdAckNotificationEvent
 ***************************************************************************/
CmdAckNotificationEvent::CmdAckNotificationEvent() :
      m_poAppMngr(NULL)
{
}

/***************************************************************************
 ** FUNCTION:  CmdAckNotificationEvent::CmdAckNotificationEvent
 ***************************************************************************/
CmdAckNotificationEvent::CmdAckNotificationEvent(spi_tclAppMngr* poAppMngr,
        const trNotificationAckData& corfrNotifAckData) :
        m_poAppMngr(poAppMngr), m_rNotifAckData(corfrNotifAckData)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  CmdAckNotificationEvent::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(CmdAckNotificationEvent, spi_tclCmdDispatcher)

/**************************************************************************************************
 ** FUNCTION:  spi_tclCmdDispatcher::vHandleCmdMsg(CmdAckNotificationEvent* poAckNotificationEvent)
 **************************************************************************************************/
t_Void spi_tclCmdDispatcher::vHandleCmdMsg(CmdAckNotificationEvent* poAckNotificationEvent)
{
   ETG_TRACE_USR1((" %s entered", __PRETTY_FUNCTION__));
   if ((NULL != poAckNotificationEvent) && (NULL != poAckNotificationEvent->m_poAppMngr))
   {
      //! Acknowledge notification through App Manager
      poAckNotificationEvent->m_poAppMngr->vAckNotification(poAckNotificationEvent->m_rNotifAckData);
   }//if (NULL != poAckNotificationEvent)
}
