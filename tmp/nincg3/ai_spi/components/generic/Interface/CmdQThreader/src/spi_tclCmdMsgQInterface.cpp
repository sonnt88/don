/***********************************************************************/
/*!
 * \file  spi_tclCmdMsgQInterface.cpp
 * \brief interface for writing commands to Message Q to use the MsgQ based
 *        threading model for Dispatching commands received by SPI
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    interface for writing commands to Message Q to use the MsgQ based
                 threading model for Dispatching commands received by SPI
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 17.03.2014  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "MsgQThreader.h"
#include "spi_tclCmdMsgQThreadable.h"
#include "spi_tclCmdMsgQInterface.h"
#include "MessageQueue.h"
#include "SPITypes.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclCmdMsgQInterface.cpp.trc.h"
   #endif
#endif

using namespace shl::thread;

/***************************************************************************
 ** FUNCTION:  spi_tclCmdMsgQInterface::spi_tclCmdMsgQInterface()
 ***************************************************************************/
spi_tclCmdMsgQInterface::spi_tclCmdMsgQInterface() :
   m_poCmdMsgQThreadable(NULL), m_poCmdMsgQThreader(NULL)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_poCmdMsgQThreadable = new spi_tclCmdMsgQThreadable();
   SPI_NORMAL_ASSERT(NULL == m_poCmdMsgQThreadable);
   m_poCmdMsgQThreader = new MsgQThreader(m_poCmdMsgQThreadable, true);
   SPI_NORMAL_ASSERT(NULL == m_poCmdMsgQThreader);
   if(NULL != m_poCmdMsgQThreader)
   {
      m_poCmdMsgQThreader->vSetThreadName("CommandQ");
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdMsgQInterface::~spi_tclCmdMsgQInterface()
 ***************************************************************************/
spi_tclCmdMsgQInterface::~spi_tclCmdMsgQInterface()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   RELEASE_MEM(m_poCmdMsgQThreader);
   RELEASE_MEM(m_poCmdMsgQThreadable);
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdMsgQInterface::bWriteMsgToQ
 ***************************************************************************/
t_Bool spi_tclCmdMsgQInterface::bWriteMsgToQ(trMsgBase *prMsgBase,
         t_U32 u32MsgSize)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   t_Bool bRetQ = false;

   if (NULL != m_poCmdMsgQThreader)
   {
      //! Get the MsgQ form threader and push the message to Q
      MessageQueue *poMsgQ = m_poCmdMsgQThreader->poGetMessageQueu();
      if ((NULL != poMsgQ) && (NULL != prMsgBase))
      {
         t_S32 s32RetMsgQ = poMsgQ->s16Push(static_cast<t_Void*>(prMsgBase), u32MsgSize);
         bRetQ = (0 == s32RetMsgQ);
      } // if ((NULL != poMsgQ) && (NULL != prMsgBase))
   } //if (NULL != m_poCmdMsgQThreader)

   if(bRetQ == false)
   {
      ETG_TRACE_ERR(("spi_tclCmdMsgQInterface::bWriteMsgToQ: Write to MsgQ failed  \n"));
   }
   return bRetQ;
}
