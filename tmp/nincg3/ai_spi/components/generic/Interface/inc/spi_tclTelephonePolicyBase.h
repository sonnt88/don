/*!
 *******************************************************************************
 * \file             spi_tclTelephonePolicyBase.h
 * \brief            Base Class for Telephone Policy
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base Class for Telephone Policy
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 06.03.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLTELEPHONEPOLICYBASE_H
#define SPI_TCLTELEPHONEPOLICYBASE_H
#include "BaseTypes.h"

/**
 *  class definitions.
 */


/**
 * This class is the base class for the Telephone policy class which implements the
 * Telephone interface for different projects.
 */
class spi_tclTelephonePolicyBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclTelephonePolicyBase::spi_tclTelephonePolicyBase();
   ***************************************************************************/
   /*!
   * \fn      spi_tclTelephonePolicyBase()
   * \brief   Parameterised Constructor
   * \param   NONE
   **************************************************************************/
   spi_tclTelephonePolicyBase(){}

   /***************************************************************************
   ** FUNCTION:  spi_tclTelephonePolicyBase::~spi_tclTelephonePolicyBase();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclTelephonePolicyBase()
   * \brief   Virtual Destructor
   **************************************************************************/
   virtual ~spi_tclTelephonePolicyBase(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephonePolicyBase::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for interested properties to Telephone FBlock.
   **************************************************************************/
   virtual t_Void vRegisterForProperties() {};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephonePolicyBase::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Registers for interested properties to Telephone FBlock.
   **************************************************************************/
   virtual t_Void vUnregisterForProperties() {};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclTelephonePolicyBase::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterCallbacks(trTelephoneCallbacks rTelephoneCallbacks)
   * \brief   Interface to register for Telephone events callbacks
   *          Optional interface to be implemented.
   * \param   [IN] rTelephoneCallbacks: Callbacks structure
   * \retval  None
   **************************************************************************/
   virtual t_Void vRegisterCallbacks(trTelephoneCallbacks rTelephoneCallbacks) {};

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclTelephonePolicyBase::bGetCallActivity()
   ***************************************************************************/
   /*!
   * \fn      bGetCallActivity()
   * \brief   Request from Bluetooth Manager to Telephone Client for current call
   *          activity.
   *          Optional interface to be implemented.
   * \retval  Bool value: TRUE - if call activity is ongoing, else FALSE.
   **************************************************************************/
   virtual t_Bool bGetCallActivity() const { return false; };


};

#endif //SPI_TCLTELEPHONEPOLICYBASE_H
