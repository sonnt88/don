/***********************************************************************/
/*!
* \file  spi_tclAppLauncherRespIntf.h
* \brief To Send Launch App & Terminate App response to HMI
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    To Send Launch App & Terminate App response to HMI
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
16.02.2014  | Shiva Kumar Gurija    | Initial Version
14.12.2015  | Rachana L Achar	    | Modified the prototype of vLaunchAppResult method

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAPPLAUNCHERRESPINTF_
#define _SPI_TCLAPPLAUNCHERRESPINTF_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclAppLauncherRespIntf
* \brief To Send Launch App & Terminate App response to HMI
****************************************************************************/
class spi_tclAppLauncherRespIntf
{
   public:

         /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAppLauncherRespIntf::spi_tclAppLauncherRespIntf()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppLauncherRespIntf()
   * \brief   Default Constructor
   * \sa      ~spi_tclAppLauncherRespIntf()
   **************************************************************************/
      spi_tclAppLauncherRespIntf()
      {
         //Add code
      }

   /***************************************************************************
   ** FUNCTION:  spi_tclAppLauncherRespIntf::~spi_tclAppLauncherRespIntf()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclAppLauncherRespIntf()
   * \brief   Destructor
   * \sa      spi_tclAppLauncherRespIntf()
   **************************************************************************/
      virtual ~spi_tclAppLauncherRespIntf()
      {
         //Add code
      }
      /***************************************************************************
      ** FUNCTION: t_Void spi_tclAppLauncherRespIntf::vLaunchAppResult()
      ***************************************************************************/
      /*!
      * \fn     vLaunchAppResult(t_U32 u32DeviceHandle, t_U32 u32AppHandle, 
      *            tenDiPOAppType enDiPOAppType, tenResponseCode enResponseCode, 
	   *			     tenErrorCode enErrorCode, const trUserContext& corfrUsrCntxt)
      * \brief  It provides result of a remote application launch in selected Mirror Link device.
      * \param  [IN] u32DeviceHandle      : Uniquely identifies the target Device.
      * \param  [IN] u32AppHandle : Uniquely identifies an Application on
      *              the target Device. This value will be obtained from AppList Interface.
      *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
      * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
      *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
      * \param  [IN] enResponseCode  :  Provides result from the operation.
      * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
      *              Set to NO_ERROR for successful operation.
	   * \param  [IN] corfrUsrCntxt	 : User Context Details.
      * \sa      spi_tclCmdInterface::vLaunchApp
      **************************************************************************/
      virtual t_Void vLaunchAppResult(t_U32 u32DeviceHandle, 
               t_U32 u32AppHandle, 
               tenDiPOAppType enDiPOAppType,
               tenResponseCode enResponseCode, 
               tenErrorCode enErrorCode, 
               const trUserContext& corfrUsrCntxt)=0;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclAppLauncherRespIntf::vTerminateAppResult()
      ***************************************************************************/
      /*!
      * \fn     vTerminateAppResult(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
      *             tenResponseCode enResponseCode, tenErrorCode enErrorCode,
      * 				const trUserContext rcUsrCntxt)
      * \brief  It terminates the remote application running on the Mirror Link device.
      * \param  [IN] enResponseCode  :  Provides result from the operation.
      * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
      *              Set to NO_ERROR for successful operation.
      * \param  [IN] u32DeviceHandle		  : Unique handle of the device.
      * \param  [IN] u32AppHandle : Unique handle of the application to be terminated.
      * \param  [IN] rcUsrCntxt : User Context Details.
      * \sa     spi_tclCmdInterface::vTerminateApp
      **************************************************************************/
      virtual t_Void vTerminateAppResult(t_U32 u32DeviceHandle, 
               t_U32 u32AppHandle, 
               tenResponseCode enResponseCode, 
               tenErrorCode enErrorCode, 
               const trUserContext rcUsrCntxt)=0;

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/
private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ** FUNCTION:  spi_tclAppLauncherRespIntf& spi_tclAppLauncherRespIntf::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppLauncherRespIntf& operator= (const spi_tclAppLauncherRespIntf &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclAppLauncherRespIntf& operator= (const spi_tclAppLauncherRespIntf &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAppLauncherRespIntf::spi_tclAppLauncherRespIntf(const 
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppLauncherRespIntf(const spi_tclAppLauncherRespIntf &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclAppLauncherRespIntf(const spi_tclAppLauncherRespIntf &corfrSrc);


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/
};//spi_tclAppLauncherRespIntf

#endif//_SPI_TCLAPPLAUNCHERRESPINTF_
