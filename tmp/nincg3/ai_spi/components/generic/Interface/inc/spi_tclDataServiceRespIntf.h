/*!
*******************************************************************************
* \file   spi_tclDataServiceRespIntf.h
* \brief  SPI Response interface for the Data Service
*******************************************************************************
\verbatim
PROJECT:       Gen3
SW-COMPONENT:  Smart Phone Integration
DESCRIPTION:   provides SPI response interface for the Project specific layer
COPYRIGHT:     &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
28.10.2013 |  Hari Priya E R              | Initial Version
\endverbatim
******************************************************************************/

#ifndef SPI_TCLDATASERVICERESPINTERFACE_H_
#define SPI_TCLDATASERVICERESPINTERFACE_H_

/******************************************************************************
| includes:
| 1)RealVNC sdk - includes
| 2)Typedefines
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \class spi_tclDataServiceRespIntf
* \brief This class provides response interface for the SPI Data Service 
*/
class spi_tclDataServiceRespIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/
   
   /***************************************************************************
   ** FUNCTION: spi_tclDataServiceRespIntf::spi_tclDataServiceRespIntf()
   ***************************************************************************/
   /*!
   * \fn     spi_tclDataServiceRespIntf()
   * \brief  Default Constructor
   * \param  NONE
   * \sa     ~spi_tclDataServiceRespIntf
   **************************************************************************/
   spi_tclDataServiceRespIntf()
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION: spi_tclDataServiceRespIntf::~spi_tclDataServiceRespIntf()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclDataServiceRespIntf()
   * \brief  Virtual Destructor
   * \sa     spi_tclDataServiceRespIntf
   **************************************************************************/
   virtual ~spi_tclDataServiceRespIntf()
   {
      //add code
   }

/***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataServiceRespIntf::vPostSubscribeForLocData(t_Bool...)
    ***************************************************************************/
   /*!
   * \fn      vPostSubscribeForLocData(t_Bool bSubscribe, tenLocationDataType enLocDataType)
   * \brief   Subscribes/Unsubscribes for location data notifications from
   *              service-provider (such as POS_FI).
   * \param   [IN] bSubscribe:
   *               TRUE - To subscribe for location data notifications.
   *               FALSE - To unsubscribe for location data notifications.
   * \param   [IN] enLocDataType: Type of data to be subscribed/unsubscribed
   * \retval  None
   **************************************************************************/
   virtual t_Void vPostSubscribeForLocData(t_Bool bSubscribe, 
      tenLocationDataType enLocDataType) = 0;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclDataServiceRespIntf::vSubscribeForEnvData()
    ***************************************************************************/
   /*!
   * \fn      t_Void vSubscribeForEnvData(t_Bool bSubscribe)
   * \brief   Subscribe/Unsubscribe for Environment updates 
   * \param   [IN] bSubscribe: TRUE - Subscribe
   *                           FALSE - Unsubscribe
   * \retval  None
   **************************************************************************/
   virtual t_Void vSubscribeForEnvData(t_Bool bSubscribe){}

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/
   /***************************************************************************
   ** FUNCTION:  spi_tclDataServiceRespIntf& spi_tclDataServiceRespIntf::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDataServiceRespIntf& operator= (const spi_tclDataServiceRespIntf &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDataServiceRespIntf& operator= (const spi_tclDataServiceRespIntf &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclDataServiceRespIntf::spi_tclDataServiceRespIntf(const ..
   ***************************************************************************/
   /*!
   * \fn      spi_tclDataServiceRespIntf(const spi_tclDataServiceRespIntf &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclDataServiceRespIntf(const spi_tclDataServiceRespIntf &corfrSrc);



   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};//spi_tclDataServiceRespIntf



#endif //SPI_TCLDATASERVICERESPINTERFACE_H_