/*!
*******************************************************************************
* \file   spi_tclInputRespIntf.h
* \brief  SPI Response interface for the Input Handler
*******************************************************************************
\verbatim
PROJECT:       Gen3
SW-COMPONENT:  Smart Phone Integration
DESCRIPTION:   provides SPI response interface for the Project specific layer
COPYRIGHT:     &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
27.06.2013 |  Hari Priya E R              | Initial Version
25.06.2015 |  Sameer Chandra              | New Interfaces for XDevice Keys
\endverbatim
******************************************************************************/

#ifndef SPI_TCLINPUTRESPINTERFACE_H_
#define SPI_TCLINPUTRESPINTERFACE_H_

/******************************************************************************
| includes:
| 1)RealVNC sdk - includes
| 2)Typedefines
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \class spi_tclInputRespIntf
* \brief This class provides response interface for the SPI Input handler 
*/
class spi_tclInputRespIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/
   
   /***************************************************************************
   ** FUNCTION: spi_tclInputRespIntf::spi_tclInputRespIntf()
   ***************************************************************************/
   /*!
   * \fn     spi_tclInputRespIntf()
   * \brief  Default Constructor
   * \param  NONE
   * \sa     ~spi_tclInputRespIntf
   **************************************************************************/
   spi_tclInputRespIntf()
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION: spi_tclInputRespIntf::~spi_tclInputRespIntf()
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclInputRespIntf()
   * \brief  Virtual Destructor
   * \sa     spi_tclInputRespIntf
   **************************************************************************/
   virtual ~spi_tclInputRespIntf()
   {
      //add code
   }

      /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputRespIntf::vPostSendTouchEvent
   **                (tenResponseCode enResponseCode,tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSendTouchEvent(tenResponseCode enResponseCode,tenErrorCode enErrorCode
   *     const trUserContext rcUsrCntxt)
   * \brief  Interface to set the Touch or Pointer events.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSendTouchEvent
   **************************************************************************/
   virtual t_Void vPostSendTouchEvent(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt)
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputRespIntf::vPostSendKeyEvent
   **                (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSendKeyEvent(tenResponseCode enResponseCode,tenErrorCode enErrorCode,
   *         const trUserContext rcUsrCntxt)
   * \brief   Interface to set the key events.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *          Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt : User Context Details.
   * \sa     spi_tclCmdInterface::vSendKeyEvent
   **************************************************************************/
   virtual t_Void vPostSendKeyEvent(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt)
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputRespIntf::vPostKeyIconDataResult
   **                   (, t_Char* pczAppIconData, ..)
   ***************************************************************************/
   /*!
   * \fn     vPostKeyIconDataResult(tenIconMimeType enIconMimeType,
   *            t_Char* pczAppIconData,t_U32 u32Len, const trUserContext rcUsrCntxt)
   * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
   * \param  [IN] cou32DevId : Device Handle
   * \param  [IN] pczKeyIconData : Byte Data Stream from the icon image file.
   * \param  [IN] u32Len : data stream length
   * \param  [IN] rcUsrCntxt         : User Context Details.
   * \sa     spi_tclCmdInterface::vGetKeyIconData
   **************************************************************************/
   virtual t_Void vPostKeyIconDataResult(const t_U32 cou32DevId,
            const t_U8* pczKeyIconData,
            t_U32 u32Len,
            const trUserContext rcUsrCntxt)
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclInputRespIntf::vPostMLServerCapInfo
   **                   (, t_Char* pczAppIconData, ..)
   ***************************************************************************/
   /*!
   * \fn     vPostKeyIconDataResult(tenIconMimeType enIconMimeType,
   *            t_Char* pczAppIconData,t_U32 u32Len, const trUserContext rcUsrCntxt)
   * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
   * \param  [IN] cou32DevId : Device Handle
   * \param  [IN] u16NumXDevices : Number of X-Device Keys supported.
   * \param  [IN] vecrXDeviceKeyDetail : XDevice Key Details.
   **************************************************************************/
   virtual t_Void vPostMLServerCapInfo(const t_U32 cou32DevId,
            t_U16 u16NumXDevices,
            trMLSrvKeyCapabilities rSrvKeyCapabilities)
   {
      //Add code
   }

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};//spi_tclInputRespIntf



#endif //SPI_TCLINPUTRESPINTERFACE_H_