/*!
 *******************************************************************************
 * \file             spi_tclClientHandlerBase.h
 * \brief            Base Class for client handlers in SPI
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base Class for client handlers in SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 27.06.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLCLIENTHANDLERBASE_H
#define SPI_TCLCLIENTHANDLERBASE_H

#include "BaseTypes.h"

/**
 *  class definitions.
 */


/**
 * This class is the base class for client-handlers used in SPI.
 */
class spi_tclClientHandlerBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclClientHandlerBase::spi_tclClientHandlerBase();
   ***************************************************************************/
   /*!
   * \fn      spi_tclClientHandlerBase()
   * \brief   Default Constructor
   * \param   None
   **************************************************************************/
   spi_tclClientHandlerBase(){}

   /***************************************************************************
   ** FUNCTION:  spi_tclClientHandlerBase::~spi_tclClientHandlerBase();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclClientHandlerBase()
   * \brief   Virtual Destructor
   **************************************************************************/
   virtual ~spi_tclClientHandlerBase(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclClientHandlerBase::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for all interested properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vUnregisterForProperties()
   **************************************************************************/
   virtual t_Void vRegisterForProperties() = 0;

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclClientHandlerBase::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Un-registers all subscribed properties to respective service
   *          Mandatory interface to be implemented.
   * \sa      vRegisterForProperties()
   **************************************************************************/
   virtual t_Void vUnregisterForProperties() = 0;

};

#endif //SPI_TCLCLIENTHANDLERBASE_H
