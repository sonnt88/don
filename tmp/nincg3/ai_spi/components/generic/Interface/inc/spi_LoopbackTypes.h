/*!
 *******************************************************************************
 * \file              spi_LoopbackTypes.h
 * \brief             Loopback type defines
 *******************************************************************************
 \verbatim
 PROJECT:        G3G
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Loopback type defines
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 18.05.2014 |  Ramya Murthy			       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_LOOPBACKTYPES_H_
#define _SPI_LOOPBACKTYPES_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#include "BaseTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

//! Internal function ID defines (for loopback messages)

//! To have a maximum separation of the public and the internal FIDs, we start the
//! internal IDs nearly at the end of the possible range (0xFFFF) and then count
//! them down, i.e. we start with 0xFFF0 and then use next as 0xFFEF, 0xFFEE, etc.
static const tU16 SPI_C_U16_IFID_SYSSTATE_DAYNIGHTMODE         = 0xFFF0;
static const tU16 SPI_C_U16_IFID_CNTRSTCKHMI_DAYNIGHTMODE      = 0xFFEF;
static const tU16 SPI_C_U16_IFID_ONSTAR_C2_CALLSTATE           = 0xFFEE;
static const tU16 SPI_C_U16_IFID_ONSTAR_ACCEPTCALLERROR        = 0xFFED;
static const tU16 SPI_C_U16_IFID_ONSTAR_HANGUPERROR            = 0xFFEC;
static const tU16 SPI_C_U16_IFID_ONSTAR_TBTACTIVESTATE         = 0xFFEB;
static const tU16 SPI_C_U16_IFID_SPEECHREC_BTNEVENTRESULT      = 0xFFEA;
static const tU16 SPI_C_U16_IFID_SPEECHREC_SRSTATUS            = 0xFFE9;
static const tU16 SPI_C_U16_IFID_NAV_GUIDANCESTATUS            = 0xFFE8;
static const tU16 SPI_C_U16_IFID_GMLAN_GPSCONFIG               = 0xFFE7;
static const tU16 SPI_C_U16_IFID_SYSSTATE_TIMEOFDAY            = 0xFFE6;
static const tU16 SPI_C_U16_IFID_MPLAY_REQAUDIODEV_RESULT      = 0xFFE5;
static const tU16 SPI_C_U16_IFID_ONSTAR_C1_CALLSTATE           = 0xFFE4;
static const tU16 SPI_C_U16_IFID_ONSTAR_EMERGENCY_CALLSTATE    = 0xFFE3;
static const tU16 SPI_C_U16_IFID_ONSTAR_SETTINGS_UPDATE        = 0xFFE2;
static const tU16 SPI_C_U16_IFID_DATA_SERVICE_SUBSCRIBE        = 0xFFE1;
static const tU16 SPI_C_U16_IFID_SYSSTATE_VALETMODE            = 0xFFE0;
static const tU16 SPI_C_U16_IFID_VEHICLE_BTADDRESS_UPDATE = 0xFFDE;
static const tU16 SPI_C_U16_IFID_VINDIGITS10TO17 = 0xFFDD;


//! Internal event ID's
//! (lowest 6 Byte are available 0xXX000001 for bPostEvent)
const t_U32 SPI_C_U32_EVENT_ID_DEFSET_READEOL             = 0x00000001;
const t_U32 SPI_C_U32_EVENT_ID_DEFSET_CLEARPRIVATEDATA    = 0x00000002;
const t_U32 SPI_C_U32_EVENT_ID_DEFSET_CUSTOMER            = 0x00000003;
const t_U32 SPI_C_U32_EVENT_ID_DEFSET_TEF                 = 0x00000004;


/*********************************************************/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef gm_tclU8Message       tLbSysStaDayNightMode;
typedef gm_tclEmptyMessage    tLbSysStaTimeOfDay;
typedef gm_tclU8Message       tLbCntrStckHmiDayNightMode;
typedef gm_tclU8Message       tLbOnstarCallState;
typedef gm_tclEmptyMessage    tLbOnstarAcceptCallError; //! empty since its only used to indicate error
typedef gm_tclEmptyMessage    tLbOnstarHangUpError;     //! empty since its only used to indicate error
typedef gm_tclU8Message       tLbOnstarTBTActiveState;
typedef gm_tclU8Message       tLbSpeechRecBtnEventResult;
typedef gm_tclU8Message       tLbSpeechRecStatus;
typedef gm_tclU8Message       tLbNavGuidanceStatus;
typedef gm_tclU8Message       tLbGMLANGPSConfig;
typedef gm_tclStreamMessage   tLbMplayAudioDevice;
typedef gm_tclU8Message       tLbOnStarC1CallState;
typedef gm_tclU8Message       tLbOnStarEmergencyCallState;
typedef gm_tclU8Message       tLbOnStarDataSettingsUpdate;
typedef gm_tclU16Message      tLbDataServiceSubscribeMsg;
typedef gm_tclStreamMessage   tLbOnVehicleBTAdressUpdate;
typedef gm_tclU8Message       tLbVINDigits10To17;
typedef gm_tclU8Message       tLbSysStaValetModeEnabled;


#endif /* _SPI_LOOPBACKTYPES_H_ */

