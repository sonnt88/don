/*!
 *******************************************************************************
 * \file             spi_tclAudioPolicyBase.h
 * \brief            Base Class for Audio Policy
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base Class for Audio Policy
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 29.10.2013 |  Hari Priya E R(RBEI/ECP2)        | Initial Version
 16.11.2013 |  Raghavendra S (RBEI/ECP2)        | Interface Redefinition for Generic
                                                  Audio Policy Base
 10.06.2014 |  Ramya Murthy                     | Audio policy redesign implementation.

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAUDIOPOLICYBASE_H
#define SPI_TCLAUDIOPOLICYBASE_H
#include "SPITypes.h"

/**
 *  class definitions.
 */


/**
 * This class is the base class for the audio policy which implements the Audio interface 
 * based on IIL or ARL.
 */
class spi_tclAudioPolicyBase 
{
public:
	 /***************************************************************************
     *********************************PUBLIC*************************************
     ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAudioPolicyBase::spi_tclAudioPolicyBase();
    ***************************************************************************/
    /*!
    * \fn      spi_tclAudioPolicyBase()
    * \brief   Parameterised Constructor
	* \param   NONE
    **************************************************************************/
   spi_tclAudioPolicyBase(){}

	/***************************************************************************
    ** FUNCTION:  spi_tclAudioPolicyBase::~spi_tclAudioPolicyBase();
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclAudioPolicyBase()
    * \brief   Virtual Destructor
    **************************************************************************/
   virtual ~spi_tclAudioPolicyBase(){}

	/***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAudioPolicyBase::bRequestAudioActivation(t_U8)
    ***************************************************************************/
    /*!
    * \fn      bRequestAudioActivation(t_U8 u8SourceNum)
    * \brief   Request to the Audio Manager by Component for Starting Audio Playback. 
	*          Mandatory Interface to be implemented as per Project Audio Policy.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
   virtual t_Bool bRequestAudioActivation(t_U8 u8SourceNum) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAudioPolicyBase::bRequestAudioDeactivation(t_U8)
    ***************************************************************************/
    /*!
    * \fn      bRequestAudioDeactivation(t_U8 u8SourceNum)
    * \brief   Request to the Audio Manager by Component for Stopping Audio Playback.
	*          Mandatory Interface to be implemented as per Project Audio Policy.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual t_Bool bRequestAudioDeactivation(t_U8 u8SourceNum) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAudioPolicyBase::bPauseAudioActivity(t_U8)
    ***************************************************************************/
    /*!
    * \fn      bPauseAudioActivity(t_U8 u8SourceNum)
    * \brief   Request to the Audio Manager by Component for Pausing Audio Playback.
	*          Optional Interface to be implemented if supported and required.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual t_Bool bPauseAudioActivity(t_U8 u8SourceNum) {return false;};

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioPolicyBase::vOnStartAllocate(t_U8* pu8MsgAlloc)
    ***************************************************************************/
    /*!
    * \fn      vOnStartAllocate(t_U8* pu8MsgAlloc)
    * \brief   Command from the external Audio Manager to the Source component
	*          to Allocate the Audio Route for required for playback. 
	*		   Optional Interface to be implemented.
	* \param   [pu8MsgSrcAct]: Message Data containing Allocate Command.
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vOnStartAllocate(t_U8* pu8MsgAlloc) {};

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioPolicyBase::vOnStartDeAllocate(t_U8* pu8MsgDeAlloc)
    ***************************************************************************/
    /*!
    * \fn      vOnStartDeAllocate(t_U8* pu8MsgDeAlloc)
    * \brief   Command from the external Audio Manager to the Source component
	*          to DeAllocate the Audio Route after playback. 
	*		   Optional Interface to be implemented.
	* \param   [pu8MsgSrcAct]: Message Data containing DeAllocate Command.
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vOnStartDeAllocate(t_U8* pu8MsgDeAlloc) {};

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioPolicyBase::vOnSourceActivity(t_U8* pu8MsgSrcAct)
    ***************************************************************************/
    /*!
    * \fn      vOnSourceActivity(t_U8* pu8MsgSrcAct)
    * \brief   Command from the external Audio Manager to the Source component
	*          to carry out action associated with Source Activity On/Off. 
	*		   Optional Interface to be implemented.
	* \param   [pu8MsgSrcAct]: Message Data containing Source Activity Command.
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vOnSourceActivity(t_U8* pu8MsgSrcAct) {};

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioPolicyBase::vOnRequestSourceInfo(t_U8*)
    ***************************************************************************/
    /*!
    * \fn      vOnRequestSourceInfo(t_U8* pu8MsgSrcAct)
    * \brief   Request from the external Audio Manager to the get the attributes
	*          associated with Audio Source. 
	*		   Optional Interface to be implemented.
	* \param   [pu8MsgSrcAct]: Message Data containing Source Info Request.
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vOnRequestSourceInfo(t_U8* pu8MsgSrcInfo) {};

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioPolicyBase::vStartSourceActivityResult(t_U8, t_Bool)
    ***************************************************************************/
    /*!
    * \fn      vStartSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
    * \brief   Acknowledgement from the Source Component to Audio Manager indicating
	*          Successful Start of Audio Playback on the allocated route. 
	*		   Mandatory Interface to be implemented.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          [bError]: true for Error Condition, false otherwise
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vStartSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioPolicyBase::vStopSourceActivityResult(t_U8, t_Bool)
    ***************************************************************************/
    /*!
    * \fn      vStopSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
    * \brief   Acknowledgement from the Source Component to Audio Manager indicating
	*          Successful Stop of Audio Playback on the allocated route. 
	*		   Mandatory Interface to be implemented.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          [bError]: true for Error Condition, false otherwise
	* \retval  NONE
    **************************************************************************/
	virtual t_Void vStopSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioPolicyBase::vPauseSourceActivityResult(t_U8, t_Bool)
    ***************************************************************************/
    /*!
    * \fn      vPauseSourceActivityResult(t_U8 u8SourceNum, t_Bool bError)
    * \brief   Acknowledgement from the Source Component to Audio Manager indicating
	*          Successful Pause of Audio Playback on the allocated route. 
	*		   Optional Interface to be implemented if supported and required
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          [bError]: true for Error Condition, false otherwise
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual t_Void vPauseSourceActivityResult(t_U8 u8SourceNum, t_Bool bError = false){};

	/***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAudioPolicyBase::bSetSrcAvailability(t_U8,t_Bool)
    ***************************************************************************/
    /*!
    * \fn      bSetSrcAvailability(t_U8 u8SourceNum,t_Bool bAvail)
    * \brief   Register the Availability of State of Source with Audio Manager. 
	*   	   Optional Interface to be implemented if supported and required
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          [bAvail]: true is Source Available, false if Unavailable
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual t_Bool bSetSrcAvailability(t_U8 u8SourceNum, t_Bool bAvail = true) = 0;

	/***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAudioPolicyBase::vSetServiceAvailable(t_U8,t_Bool)
    ***************************************************************************/
    /*!
    * \fn      vSetServiceAvailable(t_U8 u8SourceNum,t_Bool bAvail)
    * \brief   Set service availability for audio
	* \param   [bAvail]: true is Source Available, false if Unavailable
    **************************************************************************/
	virtual t_Void vSetServiceAvailable(t_Bool bAvail){};

	/***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAudioPolicyBase::bSetSourceMute(t_U8)
    ***************************************************************************/
    /*!
    * \fn      bSetSourceMuteOn(t_U8 u8SourceNum)
    * \brief   Request to Audio Manager to Mute the Source Audio.
	*          Optional Interface to be implemented if supported and required.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual t_Bool bSetSourceMute(t_U8 u8SourceNum){return false;};

	/***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAudioPolicyBase::bSetSourceDemute(t_U8)
    ***************************************************************************/
    /*!
    * \fn      t_Bool bSetSourceDemute(t_U8 u8SourceNum)
    * \brief   Request to Audio Manager to Demute the Source Audio. 
	*		   Optional Interface to be implemented if supported and required.
	* \param   [u8SourceNum]: Source Number corresponding to the Audio Source.
	*          Source Number will be defined for Audio Source by the Audio Component.
	* \retval  Bool value
    **************************************************************************/
	virtual t_Bool bSetSourceDemute(t_U8 u8SourceNum) {return false;};

   /***************************************************************************
   ** FUNCTION: t_Bool spi_tclAudioPolicyBase::bSetAudioDucking()
   ***************************************************************************/
   /*!
   * \fn     bSetAudioDucking
   * \brief  Interface to set audio ducking ON/OFF.
   * \param  cou8SrcNum: Source Number(Used only for GM)
   * \param  cou16RampDuration: Ramp duration in milliseconds
   * \param  cou8VolumeLevelindB: Volume level in dB
   * \param  coenDuckingType: Ducking/ Unducking
   **************************************************************************/
	virtual t_Bool bSetAudioDucking(const t_U8 cou8SrcNum, const t_U16 cou16RampDuration,
	         const t_U8 cou8VolumeLevelindB, const tenDuckingType coenDuckingType) {return false;};

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAudioPolicyBase::vSendAudioStatusChange(...)
    ***************************************************************************/
    /*!
    * \fn      vSendAudioStatusChange(tenAudioStatus enAudioStatus)
    * \brief   Interface to provide audio status change info
    * \param   [enAudioStatus]: Current audio status
    * \retval  t_Void
    **************************************************************************/
   virtual t_Void vSendAudioStatusChange(tenAudioStatus enAudioStatus){};

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclAudioPolicyBase::vRestoreLastMediaAudSrc()
    ***************************************************************************/
   /*!
    * \fn     t_Void vRestoreLastMediaAudSrc
    * \brief  Interface to restore last stored audio source.
    * \param  NONE
    * \retval NONE
    **************************************************************************/
   virtual t_Void vRestoreLastMediaAudSrc() {};

};
#endif //SPI_TCLAUDIOPOLICYBASE_H
