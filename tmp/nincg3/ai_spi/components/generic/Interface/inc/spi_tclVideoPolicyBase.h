/***********************************************************************/
/*!
* \file  spi_tclVideoPolicyBase.h
* \brief Base class for all Project specific Layer management interfaces
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for all Project specific Layer management interfaces
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.10.2013  | Shiva Kumar Gurija    | Initial Version
21.01.2014  | Shiva Kumar Gurija    | Added an element to update Video Render status 
Rendering Status

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLVIDEOPOLICYBASE_H_
#define _SPI_TCLVIDEOPOLICYBASE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class spi_tclVideoPolicyBase
* \brief This is to give response to HMI for the requested calls.
****************************************************************************/
class spi_tclVideoPolicyBase 
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoPolicyBase::spi_tclVideoPolicyBase()
   ***************************************************************************/
   /*!
   * \fn      spi_tclVideoPolicyBase()
   * \brief   Default Constructor
   * \sa      ~spi_tclVideoPolicyBase()
   **************************************************************************/
   spi_tclVideoPolicyBase()
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION:  spi_tclVideoPolicyBase::~spi_tclVideoPolicyBase()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclVideoPolicyBase()
   * \brief   Destructor
   * \sa      spi_tclVideoPolicyBase()
   **************************************************************************/
   virtual ~spi_tclVideoPolicyBase()
   {
      //Add code
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoPolicyBase::vUpdateVideoRenderingStatus()
   ***************************************************************************/
   /*!
   * \fn     t_Void vUpdateVideoRenderingStatus(t_Bool bRenderingStarted)
   * \brief  Method to update the video rendering status
   * \param  bRenderingStarted : [IN] true - Video rendering is started
   *                                  false - Video rendering is stopped
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vUpdateVideoRenderingStatus(t_Bool bRenderingStarted)
   {
      SPI_INTENTIONALLY_UNUSED(bRenderingStarted);
      //Add code
   }


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //spi_tclVideoPolicyBase

#endif //_SPI_TCLVIDEOPOLICYBASE_H_


///////////////////////////////////////////////////////////////////////////////
// <EOF>



