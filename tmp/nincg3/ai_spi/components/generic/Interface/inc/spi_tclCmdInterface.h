/*!
 *******************************************************************************
 * \file              spi_tclCmdInterface.h
 * \brief             SPI command interface for the Project specific layer
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   provides SPI command interface for the Project specific layer
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 13.09.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 30.11.2013 |  Ramya Murthy                | (1) Modified vGetDeviceInfoList() &
                                             vGetAppList() : Included new formal 
                                             arguments for fetching data.
                                             (2) Included method definition 
                                             for u32GetSelectedDeviceHandle()
 09.12.2013 |  Hari Priya                  | Included changes for input handler
 10.12.2013 |  Ramya Murthy                | (1) Corrected parameter data types in 
                                             vGetAppList() & vGetAppIconData().
                                             (2) Included ProjectedDisplayContext,
                                             DeviceUsagePreference, AppMetadata
                                             & MLNotificationEnabledInfo related
                                             methods.
 21.01.2014 |  Shiva kumar Gurija          | Enabled Video related Methods
 06.02.2014 |  Ramya Murthy                | Adapted to SPI HMI API document v1.5 changes
 11.02.2014 |  Hari Priya E R              | Included Method to send the screen variant
 17.03.2014 |  Pruthvi Thej Nagaraju       | Added separate thread to receive commands
 06.04.2014 |  Ramya Murthy                | Initialisation sequence implementation
 25.05.2014 |  Hari Priya E R              | Removed function for setting screen variant
 10.06.2014 |  Ramya Murthy                | Audio policy redesign implementation.
19.06.2014 | Shihabudheen P M              | Modified for app state resource arbitration 
23.06.2014  |  Shihabudheen P M            | Modified for DiPO device msg receiver.
 31.07.2014 |  Ramya Murthy                | SPI feature configuration via LoadSettings()
 01.10.2014 |  Ramya Murthy                | Added interface to receive call status
 14.10.2014 |  Hari Priya E R              | Added interface to receive Location data
 05.02.2015 |  Ramya Murthy                | Added interface to set availability of LocationData
 25.06.2015 |  Sameer Chandra              | Added ML XDeviceKey Support for PSA
 15.07.2015 |  Sameer Chandra              | Knob Key Implementation
 07.03.2016 |  Chaitra Srinivasa           | Default settings
 10.03.2016 |  Rachana L Achar             | Added interface for notification acknowledgment
 
 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLCMDINTERFACE_H_
#define SPI_TCLCMDINTERFACE_H_
/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "GenericSingleton.h"
#include "spi_tclLifeCycleIntf.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

class spi_tclRespInterface;
class ahl_tclBaseOneThreadApp;
class spi_tclFactory;
class spi_tclDeviceSelector;
class spi_tclAppLauncher;
class spi_tclDiPODeviceMsgRcvr;
class spi_tclCmdMsgQInterface;
class spi_tclDefSetLoader;

/*!
 * \class spi_tclCmdInterface
 * \brief This class provides command interface for the project specific layer to
 * implement mirrorlink and IPODout components
 */
class spi_tclCmdInterface: public GenericSingleton<spi_tclCmdInterface>,
   public spi_tclLifeCycleIntf
{
   public:

      /***************************************************************************
      *********************************PUBLIC*************************************
      ***************************************************************************/

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::~spi_tclCmdInterface()
      ***************************************************************************/
      /*!
      * \fn     ~spi_tclCmdInterface()
      * \brief  destructor
      * \sa     spi_tclCmdInterface
      **************************************************************************/
      virtual ~spi_tclCmdInterface();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclCmdInterface::bInitialize()
       ***************************************************************************/
      /*!
       * \fn      bInitialize()
       * \brief   Method to Initialize
       * \sa      bUnInitialize()
       **************************************************************************/
      virtual t_Bool bInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclCmdInterface::bUnInitialize()
       ***************************************************************************/
      /*!
       * \fn      bUnInitialize()
       * \brief   Method to UnInitialize
       * \sa      bInitialize()
       **************************************************************************/
      virtual t_Bool bUnInitialize();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclCmdInterface::vLoadSettings(const trSpiFeatureSupport&...)
       ***************************************************************************/
      /*!
       * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
       * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
       * \sa      vSaveSettings()
       **************************************************************************/
      virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp);

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclCmdInterface::vSaveSettings()
       ***************************************************************************/
      /*!
       * \fn      vSaveSettings()
       * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
       * \sa      vLoadSettings()
       **************************************************************************/
      virtual t_Void vSaveSettings();

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vRestoreSettings()
      ***************************************************************************/
      /*!
      * \fn     vRestoreSettings()
      * \brief  Interface to restore settings in SPI.
      * \param  None
      * \sa
      **************************************************************************/
      t_Void vRestoreSettings();

      /***************************************************************************
      ** FUNCTION: t_U32 spi_tclCmdInterface::u32GetSelectedDeviceHandle()
      ***************************************************************************/
      /*!
      * \fn     u32GetSelectedDeviceHandle()
      * \brief  It provides the Device Handle of currently selected device, if any.
      *         If no device is selected, returns 0xFFFFFFFF.
      * \param  None.
      * \return Device Handle of Selected Device. 
      **************************************************************************/
      t_U32 u32GetSelectedDeviceHandle() const;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vGetDeviceInfoList(t_U32& u32Num...
      ***************************************************************************/
      /*!
      * \fn     vGetDeviceInfoList(t_U32& u32NumDevices,
      *		       std::vector<trDeviceInfo>& corfvecrDeviceInfoList)
      * \brief  It provides a list of connected devices with the information for 
      *		    each device. In case of error, Number of devices will be set
      *         to 0xFF and List will be empty.
      * \param	[OUT] rfu32NumDevices : Total number of devices for which the 
      *               information provided.
      * \param  [OUT] rfvecrDeviceInfoList : List of DeviceInfo records.
      **************************************************************************/
      t_Void vGetDeviceInfoList(t_U32& rfu32NumDevices,
               std::vector<trDeviceInfo>& rfvecrDeviceInfoList);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSelectDevice(t_U32 u32DeviceHandle,..)
      ***************************************************************************/
      /*!
      * \fn     vSelectDevice(t_U32 u32DeviceHandle, DeviceConnectionType rDevConnType,
      *            DeviceConnectionReq rDevConnReq, const trUserContext& rfcorUsrCntxt);
      * \brief  It provides a mechanism to select/deselect a device from 
      *         Mirror Link/DiPO device manager with which a session can be established.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] enDevConnType   : Identifies the Connection Type.
      * \param  [IN] enDevConnReq	 : Identifies the Connection Request.
      * \param  [IN] enDAPUsage	    : Identifies Usage of DAP for the selected ML device. 
      *              This value is not considered for de-selection of device.
      * \param  [IN] enCDBUsage		 : Identifies Usage of CDB for the selected ML device. 
      *              This value is not considered for de-selection of device
      * \param  [IN] rfcorUsrCntxt		 : User Context Details.
      * \param  [IN] bIsHMITrigger   : True - If device Selection or Deselection is
      *               triggered due to a user action, else False.
      * \sa     spi_tclRespInterface::vPostSelectDeviceResult
      **************************************************************************/
      t_Void vSelectDevice(t_U32 u32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceConnectionReq enDevConnReq,
               tenEnabledInfo enDAPUsage,
               tenEnabledInfo enCDBUsage,
               tenDeviceCategory enDevCat,
               const trUserContext& rfcorUsrCntxt,
               t_Bool bIsHMITrigger = true);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vLaunchApp(t_U32 u32DeviceHandle...
      ***************************************************************************/
      /*!
      * \fn     vLaunchApp(t_U32 u32DeviceHandle, tenDeviceCategory enDeviceCategory, 
	  *            t_U32 u32AppHandle, tenDiPOAppType enDiPOAppType, t_String szTelephoneNumber, 
	  *            tenEcnrSetting enEcnrSetting, const trUserContext& rfcorUsrCntxt)
      * \brief  It launches a remote application from the selected Mirror Link device.
      *         DeviceHandle (Unsigned Long) - Uniquely identifies the target Device.
      * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
      * \param  [IN] enDeviceCategory : Device Type Information(Mirror Link/DiPO).
      * \param  [IN] u32AppHandle : Uniquely identifies an Application on
      *              the target Device. This value will be obtained from AppList Interface. 
      *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
      * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
      *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
      * \param  [IN] szTelephoneNumber : Number to be dialed if the DiPO application to be launched 
      *              is a phone application. If not valid to be used, this will be set to NULL, 
      *              zero length string. Will not be used if DeviceCategory = DEV_TYPE_MIRRORLINK.
      * \param  [IN] enEcnrSetting : Sets voice or server echo cancellation and noise reduction 
      *              settings if the DiPO application to be launched is a phone application. 
      *              If not valid to be used, this will be set to ECNR_NOCHANGE.
      * \param  [IN] rfcorUsrCntxt    : User Context Details.
      * \sa    spi_tclRespInterface::vPostLauchAppResult
      **************************************************************************/
      t_Void vLaunchApp(t_U32 u32DeviceHandle, 
               tenDeviceCategory enDeviceCategory, 
               t_U32 u32AppHandle, 
               tenDiPOAppType enDiPOAppType, 
               t_String szTelephoneNumber, 
               tenEcnrSetting enEcnrSetting, 
               const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vTerminateApp(t_U32 u32DeviceHandle, ..)
      ***************************************************************************/
      /*!
      * \fn     vTerminateApp(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
      *            const trUserContext& rfcorUsrCntxt);
      * \brief  It terminates the remote application running on the Mirror Link device.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] u32AppHandle : Uniquely identifies an Application on
      *              the target Device. This value will be obtained from AppList Interface.
      *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
      * \param  [IN] rfcorUsrCntxt		 : User Context Details.
      * \sa     spi_tclRespInterface::vPostTerminateAppResult
      **************************************************************************/
      t_Void vTerminateApp(t_U32 u32DeviceHandle, 
               t_U32 u32AppHandle,
               const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vGetAppList(t_U32 u32DeviceHandle...
      ***************************************************************************/
      /*!
      * \fn     vGetAppList(t_U32 u32DeviceHandle, t_U32& rfu32NumAppDetailsList,
      *		       const std::vector<trAppInfo>& rfvecrAppDetailsList,
      *		       const trUserContext& rfcorUsrCntxt)
      * \brief  It retrieves list of supported applications for the Device Handle provided
      *         In case of error, Number of records in AppInfoList will be
      *         set to 0xFF and AppInfoList will be empty.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param	[OUT] rfu32NumAppDetailsList : Total number of records in AppDetailsList.
      * \param  [OUT] rfvecrAppDetailsList   : List of AppDetails records.
      * \sa     spi_tclRespInterface::vPostGetAppListResult
      **************************************************************************/
      t_Void vGetAppList(t_U32 u32DeviceHandle,
               t_U32& rfu32NumAppDetailsList,
               std::vector<trAppDetails>& rfvecrAppDetailsList);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vGetAppIconData(t_String szAppIconURL,..)
      ***************************************************************************/
      /*!
      * \fn     vGetAppIconData(t_String szAppIconURL, const trUserContext& rfcorUsrCntxt)
      * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
      * \param  [IN] szAppIconURL : Application Icon URL for which the icon data
      *              is required.
      * \param  [IN] rfcorUsrCntxt   : User Context Details.
      * \sa     spi_tclRespInterface::vPostGetAppIconData
      **************************************************************************/
      t_Void vGetAppIconData(t_String szAppIconURL,
               const trUserContext& rfcorUsrCntxt) const;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetAppIconAttributes(t_U32 u32DeviceHandle..)
      ***************************************************************************/
      /*!
      * \fn     vSetAppIconAttributes(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
	  *            IconAttributes &rfrIconAttributes, const trUserContext& rfcorUsrCntxt);
      * \brief  sets application icon attributes for retrieval of application icons.
      * \param  [IN] u32DeviceHandle   : Uniquely identifies the target Device.
      * \param  [IN] u32ApplicationHandle : Uniquely identifies an application on the 
      *              target device. This value will be obtained from GetAppList Interface.
      * \param  [IN] rfrIconAttributes : Icon details.
      * \param  [IN] rfcorUsrCntxt        : User Context Details.
      * \sa     spi_tclRespInterface::vPostSetAppIconAttributesResult
      **************************************************************************/
      t_Void vSetAppIconAttributes(t_U32 u32DeviceHandle,
               t_U32 u32AppHandle,
               const trIconAttributes &rfrIconAttributes,
               const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetDeviceUsagePreference(t_U32...
      ***************************************************************************/
      /*!
      * \fn     vSetDeviceUsagePreference(t_U32 u32DeviceHandle, DeviceCategory enDeviceCategory,
      *            EnabledInfo enEnabledInfo, const trUserContext& rfcorUsrCntxt);
      * \brief  Interface to set the preference for the usage of the connected 
      *         Mirror Link/DiPO device. This can be set for individual devices or 
      *         for all the connected Mirror Link/DiPO devices.
      * \param  [IN] u32DeviceHandle  : Unique handle which identifies the device. 
      *              If the value is 0xFFFFFFFF, then this function sets the overall 
      *              preference usage for Mirror Link/DiPO.
      * \param  [IN] enDeviceCategory : Device Type Information(Mirror Link/iPOD Out).
      * \param  [IN] enEnabledInfo    : Enable Information.
      * \param  [IN] rfcorUsrCntxt        : User Context Details.
      * \sa     
      **************************************************************************/
      t_Void vSetDeviceUsagePreference(t_U32 u32DeviceHandle,
               tenDeviceCategory enDeviceCategory,
               tenEnabledInfo enEnabledInfo,
               const trUserContext& rfcorUsrCntxt) const;

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclCmdInterface::bGetDeviceUsagePreference
      **                   (t_U32 u32DeviceHandle, tenDeviceCategory enDeviceCategory,..)
      ***************************************************************************/
      /*!
      * \fn     bGetDeviceUsagePreference(DeviceCategory enDeviceCategory,
      *            tenDeviceCategory enDeviceCategory, tenEnabledInfo& rfenEnabledInfo);
      * \brief  Interface to get the preference for the usage of the
      *         connected Mirror Link/iPod Out device during startup.
      * \param  [IN] u32DeviceHandle  : Unique handle which identifies the device. 
      *              If the value is 0xFFFFFFFF, then this function sets the overall 
      *              preference usage for Mirror Link/DiPO.
      * \param  [IN] enDeviceCategory : Device Type Information(Mirror Link/iPOD Out).
      * \param  [OUT] rfenEnabledInfo : Enable Information.
      * \sa     
      **************************************************************************/
      t_Bool bGetDeviceUsagePreference(t_U32 u32DeviceHandle,
               tenDeviceCategory enDeviceCategory,
               tenEnabledInfo& rfenEnabledInfo) const;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetMLNotificationEnabledInfo
      **                   (t_U32 u32DeviceHandle, t_U32 u32NumNotificationEnableList,..)
      ***************************************************************************/
      /*!
      * \fn     vSetMLNotificationEnabledInfo(t_U32 u32DeviceHandle,
      *            t_U16 u16NumNotificationEnableList,
      *            std::vector<NotificationEnable> NotificationEnableList,
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to set the device notification preference for
      *         applications (only for Mirror Link devices).
      *         Set u32DeviceHandle to 0xFFFF, to indicate all devices.
      *         If notification for all the applications has to be:
      *         Enabled - Set NumNotificationEnableList to 0xFFFF
      *                   and NotificationEnableList should be empty.
      *         Disabled - Set NumNotificationEnableList to 0x00.
      *                   and NotificationEnableList should be empty.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] u16NumNotificationEnableList : Total number of records in
      *              NotificationEnableList.
      * \param  [IN] vecrNotificationEnableList   : List of NotificationEnable records.
      * \param  [IN] rfcorUsrCntxt      : User Context Details.
      * \sa     spi_tclRespInterface::vPostSetMLNotificationEnabledInfoResult
      **************************************************************************/
      t_Void vSetMLNotificationEnabledInfo(t_U32 u32DeviceHandle,
               t_U16 u16NumNotificationEnableList,
               std::vector<trNotiEnable> vecrNotificationEnableList,
               const trUserContext& rfcorUsrCntxt) const;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetMLNotificationOnOff()
      ***************************************************************************/
      /*!
      * \fn     t_Void vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
      * \brief  To Set the Notifications to On/Off
      * \param  bSetNotificationsOn : [IN] True - Set Notifications to ON
      *                                    False - Set Notifications to OFF
      * \retval t_Void 
      **************************************************************************/
      t_Void vSetMLNotificationOnOff(t_Bool bSetNotificationsOn);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclCmdInterface::bGetMLNotificationEnabledInfo()
      ***************************************************************************/
      /*!
      * \fn     t_Bool bGetMLNotificationEnabledInfo(t_U32 u32DeviceHandle)
      * \brief  Provides information on whether ML Notifications are enabled or
      *         disabled.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \retval t_Bool  TRUE- Enabled FALSE-Disabled
      **************************************************************************/
      t_Bool bGetMLNotificationEnabledInfo() const;
      
      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vInvokeNotificationAction
      **                   (t_U32 u32DeviceHandle, t_U32 u32AppHandle,..)
      ***************************************************************************/
      /*!
      * \fn     vInvokeNotificationAction(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
      *            t_U32 u32NotificationID, t_U32 u32NotificationActionID,
      *            const trUserContext& rfcorUsrCntxt);
      * \brief  Interface to invoke the respective action for the received
      *         Notification Event.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] u32AppHandle    : Handle uniquely identifies an application on the device.
      * \param  [IN] u32NotificationID : Notification Identifier.
      * \param  [IN] u32NotificationActionID : Notification action Identifier.
      * \param  [IN] rfcorUsrCntxt      : User Context Details.
      * \sa     spi_tclRespInterface::vPostInvokeNotificationActionResult
      **************************************************************************/
      t_Void vInvokeNotificationAction(t_U32 u32DeviceHandle,
               t_U32 u32AppHandle,
               t_U32 u32NotificationID,
               t_U32 u32NotificationActionID,
               const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vGetVideoSettings
      **                   (const t_U32 cou32DeviceHandle, trVideoAttributes& rfrVideoAttr)
      ***************************************************************************/
      /*!
      * \fn     vGetVideoSettings(const t_U32 cou32DeviceHandle, trVideoAttributes& rfrVideoAttr)
      * \brief  It provides current Video Settings.
      * \param  [IN] cou32DeviceHandle : Uniquely identifies the target Device.
      * \param  [OUT] rfrVideoAttr   : Provides current Video settings. In case of 
      *              error in retrieving the attributes, corresponding values will 
      *              be set to INVALID.
      **************************************************************************/
      t_Void vGetVideoSettings(const t_U32 cou32DeviceHandle,
               trVideoAttributes& rfrVideoAttr);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetOrientationMode
      **                (const t_U32 cou32DeviceHandle, tenOrientationMode enOrientationMode,..)
      ***************************************************************************/
      /*!
      * \fn     vSetOrientationMode(const t_U32 cou32DeviceHandle,
      *            OrientationMode enOrientationMode, const trUserContext& rfcorUsrCntxt);
      * \brief  Interface to set the orientation mode of the projected display.
      * \param  [IN] cou32DeviceHandle   : Uniquely identifies the target Device.
      * \param  [IN] enOrientationMode : Orientation Mode Value.
      * \param  [IN] rfcorUsrCntxt        : User Context Details.
      * \sa      spi_tclRespInterface::vPostSetOrientationModeResult
      **************************************************************************/
      t_Void vSetOrientationMode(const t_U32 cou32DeviceHandle,
               tenOrientationMode enOrientationMode,
               const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetScreenSize
      **                   (trScreenAttributes& corfrScreenAttributes,..)
      ***************************************************************************/
      /*!
      * \fn     vSetScreenSize(ScreenAttributes& corfrScreenAttributes,
      *                     const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to set the screen size of Head Unit.
      * \param  [IN] corfrScreenAttributes : Screen Setting attributes.
      * \param  [IN] rfcorUsrCntxt            : User Context Details.
      **************************************************************************/
      t_Void vSetScreenSize(const trScreenAttributes& corfrScreenAttributes,
               const trUserContext& rfcorUsrCntxt) const;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetVideoBlockingMode
      **                (const t_U32 cou32DeviceHandle, tenBlockingMode enBlockingMode,..)
      ***************************************************************************/
      /*!
      * \fn     vSetVideoBlockingMode(const t_U32 cou32DeviceHandle,
      *             tenBlockingMode enBlockingMode, const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to set the display blocking mode.
      * \param  [IN] cou32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] enBlockingMode  : Identifies the Blocking Mode.
      * \param  [IN] rfcorUsrCntxt      : User Context Details.
      * \sa      spi_tclRespInterface::vPostSetVideoBlockingModeResult
      **************************************************************************/
      t_Void vSetVideoBlockingMode(const t_U32 cou32DeviceHandle,
               tenBlockingMode enBlockingMode,
               const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclCmdInterface::bSetAudioBlockingMode
      **                (t_U32 u32DeviceHandle, tenBlockingMode enBlockingMode)
      ***************************************************************************/
      /*!
      * \fn     bSetAudioBlockingMode(t_U32 u32DeviceHandle, BlockingMode enBlockingMode)
      * \brief  Interface to set the audio blocking mode.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] enBlockingMode  : Identifies the Blocking Mode.
      * \sa     spi_tclRespInterface::vPostSetAudioBlockingModeResult
      **************************************************************************/
      t_Bool bSetAudioBlockingMode(t_U32 u32DeviceHandle,
               tenBlockingMode enBlockingMode);
            
      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetVehicleConfig()
      ***************************************************************************/
      /*!
      * \fn     t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
      *          t_Bool bSetConfig,const trUserContext& corfrUsrCntxt)
      * \brief  Interface to set the Vehicle configurations.
      * \param  [IN] enVehicleConfig :  Identifies the Vehicle Configuration.
      * \param  [IN] bSetConfig      : Enable/Disable config
      * \param  [IN] corfrUsrCntxt   : User Context Details.
      **************************************************************************/
      t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
         t_Bool bSetConfig,const trUserContext& corfrUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vGetVehicleMode()
      ***************************************************************************/
      /*!
      * \fn     vGetVehicleMode(tenVehicleConfiguration& rfenVehicleConfig)
      * \brief  Interface to get the Mode of the vehicle.
      * \param  [IN] rfenVehicleConfig :  Identifies the Vehicle configuration.
      * \sa
      **************************************************************************/
      t_Void vGetVehicleMode(tenVehicleConfiguration& rfenVehicleConfig); 

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSendTouchEvent(t_U32 u32DeviceHandle..)
      ***************************************************************************/
      /*!
      * \fn     vSendTouchEvent(t_U32 u32DeviceHandle, 
      *    trTouchData& rfrTouchData, const trUserContext& rfcorUsrCntxt);
      * \brief  Interface to set the Touch or Pointer events.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] rfrTouchData  : Contains Touch information.
      * \param  [IN] rfcorUsrCntxt		 : User Context Details.
      * \sa     spi_tclRespInterface::vPostSendTouchEvent
      **************************************************************************/
      t_Void vSendTouchEvent(t_U32 u32DeviceHandle, 
               trTouchData& rfrTouchData,
               const trUserContext& rfcorUsrCntxt) const;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSendKeyEvent(t_U32 u32DeviceHandle..)
      ***************************************************************************/
      /*!
      * \fn     vSendKeyEvent(t_U32 u32DeviceHandle, KeyMode enKeyMode,KeyCode enKeyCode)
      * \brief   Interface to set the key events.
      * \param  [IN] u32DeviceHandle : Handle uniquely identifies a device.
      * \param  [IN] enKeyMode  :  Key Mode Value.
      * \param  [IN] enKeyCode  : Key Code Value.
      * \param  [IN] rfcorUsrCntxt : User Context Details.
      * \sa
      **************************************************************************/
      t_Void vSendKeyEvent(t_U32 u32DeviceHandle, 
               tenKeyMode enKeyMode,
               tenKeyCode enKeyCode, 
               const trUserContext& rfcorUsrCntxt) const;

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetAccessoryDisplayContext(t_U32...
      ***************************************************************************/
      /*!
      * \fn     vSetAccessoryDisplayContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
      *              tenDisplayContext enDisplayContext, const trUserContext& rfcorUsrCntxt);
      * \brief  This interface is used by Mirror Link/DiPO device to inform the client
      *              about its current display con-text.
      * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
      * \param  [IN] bDisplayFlag     : TRUE � Start Display Projection, FALSE � Stop Display Projection.
      * \param  [IN] enDisplayContext : Display context of the projected device.
      * \param  [IN] rfcorUsrCntxt       : User Context Details.
      * \sa
      **************************************************************************/
      t_Void vSetAccessoryDisplayContext(t_U32 u32DeviceHandle,
         t_Bool bDisplayFlag,
         tenDisplayContext enDisplayContext,
         const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetAccessoryDisplayMode(t_U32...
      ***************************************************************************/
      /*!
      * \fn     vSetAccessoryDisplayMode()
      * \brief  Accessory display mode update request.
      * \param  [IN] cu32DeviceHandle      : Uniquely identifies the target Device.
      * \param  [IN] corDisplayContext : Display context info
      * \param  [IN] corDisplayConstraint : Display constraint info
      * \param  [IN] coenDisplayInfo       : Display info flag
      * \sa
      **************************************************************************/
      t_Void vSetAccessoryDisplayMode(const t_U32 cu32DeviceHandle,
         const trDisplayContext corDisplayContext,
         const trDisplayConstraint corDisplayConstraint,
         const tenDisplayInfo coenDisplayInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclCmdInterface::vSetAccessoryAudioContext()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetAccessoryAudioContext(const t_U32 cou32DevId, const t_U8 cu8AudioCntxt,
   *   t_Bool bReqFlag, tenDeviceCategory enDevCat, const trUserContext& rfrcUsrCntxt)
   *
   * \brief   To send accessory display context related info .
   * \param   cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cu8AudioCntxt : [IN] Audio Source number/base channel number
   * \param   bReqFlag : [IN] Request flag, true for request and false for release
   * \param   rfrcUsrCntxt: [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryAudioContext(t_U32 u32DeviceHandle, const tenAudioContext coenAudioCntxt,
      t_Bool bReqFlag, const trUserContext& rfrcUsrCntxt,
      tenDeviceCategory enDevCategory);

  /***************************************************************************
   ** FUNCTION:  t_Void spi_tclCmdInterface::vSetAccessoryAppState()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetAccessoryAppState(tenSpeechAppState enSpeechAppState, tenPhoneAppState enPhoneAppState,
   *         tenNavAppState enNavAppState
   *
   * \brief   To set accessory app state realated info.
   * \param   enSpeechAppState: [IN] Uniquely identifies the target Device.
   * \param   enPhoneAppState : [IN] Audio Source number/base channel number
   * \param   enPhoneAppState : [IN] Request flag, true for request and false for release
   * \param   rfrcUsrCntxt    : [IN] User Context Details.
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryAppState(const tenSpeechAppState enSpeechAppState, const tenPhoneAppState enPhoneAppState,
      const tenNavAppState enNavAppState, const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclCmdInterface::vGetDeviceDisplayContext(t_U32...
   ***************************************************************************/
   /*!
   * \fn     vGetDeviceDisplayContext(t_U32 u32DeviceHandle, t_Bool& rfbDisplayFlag,
   *              tenDisplayContext& rfenDisplayContext);
   * \brief  This interface is used by Mirror Link/DiPO device to inform the client
   *              about its current display con-text.
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [OUT] rfbDisplayFlag     : TRUE � Start Display Projection, FALSE � Stop Display Projection.
   * \param  [OUT] rfenDisplayContext : Display context of the projected device.
   * \sa
   **************************************************************************/
   t_Void vGetDeviceDisplayContext(t_U32 u32DeviceHandle,
      t_Bool& rfbDisplayFlag,
      tenDisplayContext& rfenDisplayContext);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclCmdInterface::vGetAppStateInfo()
   ***************************************************************************/
   /*!
   * \fn     vGetAppStateInfo()
   * \brief  To get the device app state info.
   * \param  [OUT] enSpeechAppState  : Speech App state
   * \param  [OUT] enPhoneAppState   : Phone App state
   * \param  [OUT] enNavAppState     : Navigation App state
   * \sa
   **************************************************************************/
   t_Void vGetAppStateInfo(tenSpeechAppState &enSpeechAppState, tenPhoneAppState 
      &enPhoneAppState, tenNavAppState &enNavAppState) const;

    /**************************************************************************
    ** FUNCTION   : tVoid spi_tclCmdInterface:: vSetTechnologyPreference()
    ***************************************************************************/
   /*!
    * \fn      tVoid vSetTechnologyPreference()
    * \brief   To set the preferred SPI technology for devices which support more than once technology
    * \param   [IN].cou32DeviceHandle - Device handle for which the prefernce is applicable.
    *               if cou32DeviceHandle is 0xFFFF then the preference is applicable for overall setting
    * \param   [IN].vecTechnologyPreference - contains the technology preference order
    * \retval  tVoid
    **************************************************************************/
   t_Void vSetTechnologyPreference(const t_U32 cou32DeviceHandle, std::vector<tenDeviceCategory> vecTechnologyPreference) const;

   /**************************************************************************
   ** FUNCTION   : tVoid spi_tclCmdInterface:: vGetTechnologyPreference()
   ***************************************************************************/
  /*!
   * \fn      tVoid vGetTechnologyPreference()
   * \brief   To get the  previously set preferred SPI technology
   * \param   [IN].cou32DeviceHandle - Device handle for which the prefernce is applicable.
   *               if cou32DeviceHandle is 0xFFFF then the preference is applicable for overall setting
   * \param   [OUT].vecTechnologyPreference - contains the technology preference order
   * \retval  tVoid
   **************************************************************************/
  t_Void vGetTechnologyPreference(const t_U32 cou32DeviceHandle, std::vector<tenDeviceCategory> &rfrvecTechnologyPreference) const;

  /***************************************************************************
   ** FUNCTION:  spi_tclCmdInterface::vSetDeviceAuthorization
   ***************************************************************************/
  /*!
   * \fn     vSetDeviceAuthorization
   * \brief  To set the device authorization based on user action
   * \param  u32DeviceHandle : Uniquely identifies the target Device.
   * \param  enUserAuthStatus : Authorization status
   **************************************************************************/
  t_Void vSetDeviceAuthorization(const t_U32 cou32DeviceHandle, tenUserAuthorizationStatus enUserAuthStatus);


      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vAllocate(t_PUChar pu8MsgAlloc)
      ***************************************************************************/
      /*!
      * \fn      vAllocate(t_PUChar pu8MsgAlloc)
      * \brief   Command from the external Audio Manager to the Source component
      *          to Allocate the Audio Route for required for playback.
      * \param   [pu8MsgAlloc]: Message Data containing Allocate Command.
      * \retval  NONE
      **************************************************************************/
      t_Void vAllocate(t_PUChar pu8MsgAlloc);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vDeAllocate(t_PUChar pu8MsgDeAlloc)
      ***************************************************************************/
      /*!
      * \fn      vDeAllocate(t_PUChar pu8MsgDeAlloc)
      * \brief   Command from the external Audio Manager to the Source component
      *          to Allocate the Audio Route for required for playback.
      * \param   [pu8MsgDeAlloc]: Message Data containing Allocate Command.
      * \retval  NONE
      **************************************************************************/
      t_Void vDeAllocate(t_PUChar pu8MsgDeAlloc);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vSrcActivity(t_PUChar pu8MsgSrcAct)
      ***************************************************************************/
      /*!
      * \fn      vSrcActivity(t_PUChar pu8MsgSrcAct)
      * \brief   Command from the external Audio Manager to the Source component
      *          to carry out action associated with Source Activity On/Off.
      * \param   [pu8MsgSrcAct]: Message Data containing Source Activity Command.
      * \retval  NONE
      **************************************************************************/
      t_Void vSrcActivity(t_PUChar pu8MsgSrcAct);

      /***************************************************************************
      ** FUNCTION:  t_Bool spi_tclCmdInterface::bGetAudSrcInfo(const t_U8..)
      ***************************************************************************/
      /*!
      * \fn      t_Bool bGetAudSrcInfo(const t_U8 cu8SourceNum)
      * \brief   Command To retrieve the Source Info from The Audio Policy
      * \param   [rfrSrcInfo]: Data containing Source Info.
      * \param   [rfrSrcInfo]: Reference to trAudSrcInfo structure
      * \retval  t_Bool Value
      **************************************************************************/
      t_Bool bGetAudSrcInfo(const t_U8 cu8SourceNum, trAudSrcInfo& rfrSrcInfo);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vInvokeBluetoothDeviceAction
      **                   (t_U32 u32BTDevHandle, t_U32 u32ProjectionDevHandle,..)
      ***************************************************************************/
      /*!
      * \fn     vInvokeNotificationAction(t_U32 u32BTDevHandle
      *            t_U32 u32ProjectionDevHandle, tenBTStatusInfo enBTStatus);
      * \brief  Interface to invoke the respective Bluetooth device switch action.
      * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
      * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
      * \param  [IN] enBTChange  : Enum value which stores BT device status
      **************************************************************************/
      t_Void vInvokeBluetoothDeviceAction(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enBTChange);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetClientCapabilities
      **            (const trClientCapabilities& corfrClientCapabilities,..)
      ***************************************************************************/
      /*!
      * \fn     vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities, 
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to set client capabilities
      * \param  [IN] rClientCapabilities : Capabilities of the client.
      * \param  [IN] rfcorUsrCntxt : User Context Details.
      * \sa
      **************************************************************************/
      t_Void vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities, 
         const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetVehicleBTAddress(.)
      ***************************************************************************/
      /*!
      * \fn     vSetVehicleBTAddress(std::string szBTAddress, 
      *            const trUserContext& rfcorUsrCntxt)
      * \brief  Interface to set vehicle BT Address
      * \param  [IN] szBTAddress : 12bit BT Address
      * \param  [IN] rfcorUsrCntxt : User Context Details.
      * \sa
      **************************************************************************/
      t_Void vSetVehicleBTAddress(std::string szBTAddress, 
         const trUserContext& rfcorUsrCntxt);

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSetRegion(.)
      ***************************************************************************/
      /*!
      * \fn     t_Void vSetRegion(tenRegion enRegion)
      * \brief  Interface to set the region for application certification.
      *         It gives the info of which region CCC Guidelines should be followed
      *         for the Application Certification Filtering
      * \param  [IN] enRegion : Region enumeration
      * \sa
      **************************************************************************/
      t_Void vSetRegion(tenRegion enRegion);

      /***************************************************************************
      ** FUNCTION: t_Bool spi_tclCmdInterface::bIsDiPoRoleSwitchRequired
      ***************************************************************************/
      /*!
      * \fn     t_Bool bIsDiPoRoleSwitchRequired
      * \brief  Interface to check if the device specified with device id cou32DeviceID
      *         is same as the last used device
      * \param  [IN] cou32DeviceID : Device Id to be checked
      * \retval true if the device is same as last connected device otherwise false
      **************************************************************************/
      t_Bool bIsDiPoRoleSwitchRequired(const t_U32 cou32DeviceID, const trUserContext corUsrCntxt);

      /**************************Start of Audio methods**************************/

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnRouteAllocateResult(const tU8, ..)
      ***************************************************************************/
      /*!
      * \fn      vOnRouteAllocateResult(const tU8 cu8SourceNum, trAudSrcInfo& rfrSrcInfo)
      * \brief   Notification from the Audio Manager on Completion of Route Alloc.
      *          Implement Source component specific actions on Allocation of Audio Route.
      *          Mandatory Interface to be implemented.
      * \param   [cu8SourceNum]: Source Number corresponding to the Audio Source.
      *          Source Number will be defined for Audio Source by the Audio Component.
      *          [rfrSrcInfo]: Reference to structure containing details of the allocated source.
      * \retval  NONE
      **************************************************************************/
      virtual t_Bool bOnRouteAllocateResult(const t_U8 cu8SourceNum , trAudSrcInfo& rfrSrcInfo);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnRouteDeAllocateResult(const tU8)
      ***************************************************************************/
      /*!
      * \fn      vOnRouteDeAllocateResult(const tU8 cu8SourceNum)
      * \brief   Notification from the Audio Manager on Completion of Route Dealloc.
      *          Implement Source component specific actions on Deallocation of Audio Route.
      *          Mandatory Interface to be implemented.
      * \param   [cu8SourceNum]: Source Number corresponding to the Audio Source.
      *          Source Number will be defined for Audio Source by the Audio Component.
      * \retval  NONE
      **************************************************************************/
      virtual t_Void vOnRouteDeAllocateResult(const t_U8 cu8SourceNum);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnStartSourceActivity(const tU8)
      ***************************************************************************/
      /*!
      * \fn      vOnStartSourceActivity(const tU8 cu8SourceNum)
      * \brief   Trigger from the Audio Manager to Start play of Audio from Device.
      *          Mandatory Interface to be implemented.
      * \param   [cu8SourceNum]: Source Number corresponding to the Audio Source.
      * \retval  NONE
      **************************************************************************/
      virtual t_Void vOnStartSourceActivity(const t_U8 cu8SourceNum);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnStopSourceActivity(const tU8)
      ***************************************************************************/
      /*!
      * \fn      vOnStopSourceActivity(const tU8 cu8SourceNum)
      * \brief   Trigger from the Audio Manager to Start play of Audio from Device
      *          Mandatory Interface to be implemented.
      * \param   [cu8SourceNum]: Source Number corresponding to the Audio Source.
      * \retval  NONE
      **************************************************************************/
      virtual t_Void vOnStopSourceActivity(t_U8 cu8SourceNum);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnNewMuteState(const tU8, t_Bool)
      ***************************************************************************/
      /*!
      * \fn      bOn(const tU8 cu8SourceNum)
      * \brief   Notification from the Audio Manager on change in Mute State.
      *          Implement Source component specific actions on change in Mute State.
      *          Optional Interface to be implemented if required.
      * \param   [cu8SourceNum]: Source Number corresponding to the Audio Source.
      *          Source Number will be defined for Audio Source by the Audio Component.
      *          [bMute]: TRUE if Source Mute is active, FALSE Otherwise
      * \retval  NONE
      **************************************************************************/
      virtual t_Void vOnNewMuteState(t_U8 cu8SourceNum, t_Bool bMute){}

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::bOnReqAVActivationResult(const tU8, t_Bool)
      ***************************************************************************/
      /*!
      * \fn     bOnReqAVActivationResult
      * \brief  Application specific function after which RequestAVAct
      *          result can be processed.
      * \param   [cu8SourceNum]: Source Number corresponding to the Audio Source.
      *          Source Number will be defined for Audio Source by the Audio Component.
      * \retval  true if the application processing is successful false otherwise
      **************************************************************************/
      virtual t_Bool bOnReqAVActivationResult(const t_U8 cu8SourceNum){return true;}

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::bOnReqAVDeActivationResult(const tU8, t_Bool)
      ***************************************************************************/
      /*!
      * \fn     bOnReqAVDeActivationResult
      * \brief  Application specific function after which RequestAVDeAct
      *          result can be processed.
      * \param   [cu8SourceNum]: Source Number corresponding to the Audio Source.
      *          Source Number will be defined for Audio Source by the Audio Component.
      * \retval  true if the application processing is successful false otherwise
      **************************************************************************/
      virtual t_Bool bOnReqAVDeActivationResult(const t_U8 cu8SourceNum);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnAudioError(const tU8)
      ***************************************************************************/
      /*!
      * \fn      vOnAudioError
      * \brief   Error Notification from the Audio Manager
      * \param   [cu8SourceNum]: Source Number corresponding to the Audio Source.
      * \param   [enError] :Audio Error code
      * \retval  NONE
      **************************************************************************/
      virtual t_Void vOnAudioError( const t_U8 cu8SourceNum, tenAudioError enAudioError);

      /***************************************************************************
      ** FUNCTION:  tenDeviceCategory spi_tclCmdInterface::enGetDeviceCategory(t_U32)
      ***************************************************************************/
      /*!
      * \fn      enGetDeviceCategory(t_U32 u32DeviceHandle)
      * \brief   Interface to get the device category of a device
      * \param   [u32DeviceHandle]: Device Handle
      * \retval  Device category of device
      **************************************************************************/
      tenDeviceCategory enGetDeviceCategory(t_U32 u32DeviceHandle) const;

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnCallStatus(t_Bool bCallActive)
      ***************************************************************************/
      /*!
      * \fn      vOnCallStatus(t_Bool bCallActive)
      * \brief   Interface to receive Telephone call status changes.
      * \param   [IN] bCallActive: true - if a call is active, else false.
      * \retval  None
      **************************************************************************/
      t_Void vOnCallStatus(t_Bool bCallActive);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnGPSData(const trGPSData& rfcoGPSData)
      ***************************************************************************/
      /*!
      * \fn      vOnGPSData(const trGPSData& rfcoGPSData)
      * \brief   Interface to receive the GPS Data.
      * \param   [IN] rfcoGPSData: GPS Data.
      * \retval  None
      **************************************************************************/
      t_Void vOnGPSData(const trGPSData& rfcoGPSData);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnSensorData(const trSensorData& rfcoSensorData)
      ***************************************************************************/
      /*!
      * \fn      vOnSensorData(const trSensorData& rfcoSensorData)
      * \brief   Interface to receive the Sensor Data.
      * \param   [IN] rfcoSensorData: Sensor Data
      * \retval  None
      **************************************************************************/
      t_Void vOnSensorData(const trSensorData& rfcoSensorData);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnAccSensorData
      ** (const std::vector<trAccSensorData>& corfvecrSensorData)
      ***************************************************************************/
      /*!
      * \fn      vOnAccSensorData(const std::vector<trAccSensorData>& rfcoSensorData)
      * \brief   Interface to receive the Acceleration Sensor Data.
      * \param   [IN] corfvecrSensorData: Acceleration Sensor Data
      * \retval  None
      **************************************************************************/
      t_Void vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrSensorData);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnGyroSensorData
      ** (const std::vector<trGyroSensorData>& rfcoSensorData)
      ***************************************************************************/
      /*!
      * \fn      vOnGyroSensorData(const std::vector<trGyroSensorData>& rfcoSensorData)
      * \brief   Interface to receive the gyro Sensor Data.
      * \param   [IN] rfcoSensorData: Gyro Sensor Data
      * \retval  None
      **************************************************************************/
      t_Void vOnGyroSensorData(const std::vector<trGyroSensorData>& rfcoSensorData);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vOnVehicleData(const trVehicleData& rfcoVehicleData)
      ***************************************************************************/
      /*!
      * \fn      vOnVehicleData(const trVehicleData& rfcoVehicleData)
      * \brief   Interface to receive the Vehicle Data.
      * \param   [IN] rVehicleData: Vehicle Data.
      * \retval  None
      **************************************************************************/
      t_Void vOnVehicleData(const trVehicleData& rfcoVehicleData, t_Bool bSolicited);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vSetLocDataAvailablility(t_Bool...)
      ***************************************************************************/
      /*!
      * \fn      vSetLocDataAvailablility(t_Bool bLocDataAvailable)
      * \brief   Interface to set the availability of LocationData
      * \param   [IN] bLocDataAvailable:
      *              If true - Location data is available
      *              If false - Location data is unavailable
      * \param   [IN] bIsDeadReckonedData:
      *              True - if Location data is dead reckoned data, else False
      * \retval  None
      **************************************************************************/
      t_Void vSetLocDataAvailablility(t_Bool bLocDataAvailable, t_Bool bIsDeadReckonedData);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vSetVehicleBTAddress(t_Bool...)
      ***************************************************************************/
      /*!
      * \fn      vSetVehicleBTAddress(t_Bool bLocDataAvailable)
      * \brief   Interface to update the vehicle BT address info update.
      * \param   [IN] szBtAddress: BT address.
      * \retval  None
      **************************************************************************/
      t_Void vSetVehicleBTAddress(t_String szBtAddress);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclCmdInterface::vSetDispAttr(...)
      ***************************************************************************/
      /*!
      * \fn      t_Void vSetDispAttr(const trDisplayAttributes& corfrDispLayerAttr)
      * \brief   Interface to set the HMI screen attributes
      * \param   [IN] corfrDispLayerAttr:Screen Attributes
      * \retval  None
      **************************************************************************/
      t_Void vSetDispAttr(const trDisplayAttributes& corfrDispLayerAttr);


      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vGetKeyIconData(t_String szAppIconURL,..)
      ***************************************************************************/
      /*!
      * \fn     vGetAppIconData(t_String szAppIconURL, const trUserContext& rfcorUsrCntxt)
      * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
      * \param  [IN] szAppIconURL : Application Icon URL for which the icon data
      *              is required.
      * \param  [IN] rfcorUsrCntxt   : User Context Details.
      * \sa     spi_tclRespInterface::vPostGetAppIconData
      **************************************************************************/
      t_Void vGetKeyIconData(t_U32 u32DeviceHandle,t_String szKeyIconURL,
               const trUserContext& rfcorUsrCntxt) const;
			   
	  /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::vSendKnobKeyEvent(t_U32 u32DeviceHandle..)
      ***************************************************************************/
      /*!
      * \fn     vSendKnobKeyEvent(t_U32 u32DeviceHandle,t_S8 s8EncodeDeltaCnts,
                      const trUserContext& rfcorUsrCntxt)
      * \brief   Interface to set the key events.
      * \param  [IN] u32DeviceHandle : Handle uniquely identifies a device.
      * \param  [IN] s8EncodeDeltaCnts  :  Delta Change in the encoder position
      * \param  [IN] rfcorUsrCntxt : User Context Details.
      * \sa
      **************************************************************************/
      t_Void vSendKnobKeyEvent(t_U32 u32DeviceHandle,t_S8 s8EncodeDeltaCnts,
                      const trUserContext& rfcorUsrCntxt) const;

   /***************************************************************************
   ** FUNCTION:  spi_tclCmdInterface::vReportEnvironmentData()
   ***************************************************************************/
   /*!
   * \fn    t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
   *                                   t_Bool bValidPressureUpdate, t_Double dPressure)
   * \brief Use this call to report Environment data to Phone
   * \param bValidTempUpdate : [IN] Temp update is valid
   * \param dTemp : [IN] Temp in Celsius
   * \param bValidPressureUpdate: [IN] Pressure update is valid
   * \param dPressure : [IN] Pressure in KPA
   * \retval t_Void
   **************************************************************************/
   t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
      t_Bool bValidPressureUpdate, t_Double dPressure);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclCmdInterface::vSetFeatureRestrictions()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
    *          const t_U8 cou8ParkModeRestrictionInfo,
    *          const t_U8 cou8DriveModeRestrictionInfo)
    * \brief   Method to set Vehicle Park/Drive Mode Restriction
    * \param   enDevCategory : Device Category
    *          cou8ParkModeRestrictionInfo : Park mode restriction
    *          cou8DriveModeRestrictionInfo : Drive mode restriction
    * \retval  t_Void
    **************************************************************************/

   t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
         const t_U8 cou8ParkModeRestrictionInfo,const t_U8 cou8DriveModeRestrictionInfo);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclCmdInterface::vSetDeviceSelectionMode()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSetDeviceSelectionMode(
    * \brief   Method to set the device selection mode to automatic/manual. Changes
    *          will take into effect on successive connection
    * \param   enDeviceSelectionMode : Device selection mode @see tenDeviceSelectionMode
    * \retval  t_Void
    **************************************************************************/
   t_Void vSetDeviceSelectionMode(tenDeviceSelectionMode enDeviceSelectionMode);

      /***************************************************************************
    ** FUNCTION:  t_Void spi_tclCmdInterface::vAckNotification(...)
    ***************************************************************************/
   /*!
    * \fn      t_Void vAckNotification(
    *           const trNotificationAckData& corfrNotifAckData)
    * \brief   Method to send the acknowledgment for receiving the notification
    * \param   corfrNotifAckData : [IN] Structure containing the Id and
    *                              category of the device to be acknowledged
    *                              and the id of the notification received
    * \retval  t_Void
    ***************************************************************************/
   t_Void vAckNotification(const trNotificationAckData& corfrNotifAckData);

      /***************************************************************************
      *********************************PRIVATE************************************
      ***************************************************************************/
      friend class GenericSingleton<spi_tclCmdInterface>;


   private:

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::spi_tclCmdInterface();
      ***************************************************************************/
      /*!
      * \fn      spi_tclCmdInterface()
      * \brief   default constructor
      * \sa     ~spi_tclCmdInterface
      **************************************************************************/
      spi_tclCmdInterface() {}

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclCmdInterface::spi_tclCmdInterface(..);
      ***************************************************************************/
      /*!
      * \fn     spi_tclCmdInterface(spi_tclRespInterface* poSpiRespIntf)
      * \brief  Constructor with Parameter
      * \param  [IN] poSpiRespIntf : Pointer to spi_tclRespInterface class.
      * \param  [IN] poMainAppl : Pointer to ahl_tclBaseOneThreadApp class.
      * \sa     ~spi_tclCmdInterface
      **************************************************************************/
      spi_tclCmdInterface(spi_tclRespInterface* poSpiRespIntf, 
               ahl_tclBaseOneThreadApp* poMainAppl);

      //! C++ Response Interface Pointer
      spi_tclRespInterface* m_poSpiRespIntf;

      //! Pointer to Main application
      ahl_tclBaseOneThreadApp* m_poMainApp;

      //! Pointer to factory creating SPI components
      spi_tclFactory* m_poFactory;

      //! pointer to object responsible for device selection
      spi_tclDeviceSelector* m_poDevSelector;

     //! Pointer object of Appplication launch/terminate class
	  spi_tclAppLauncher* m_poLaunchApp;

     //! Pointer object to the DiPo device msg receiver
     spi_tclDiPODeviceMsgRcvr *m_poDeviceMsgRcvr;

	 //! Pointer to command dispatcher
     spi_tclCmdMsgQInterface* m_poCmdMsqQ;

	 //! Pointer to default settings loader
	 spi_tclDefSetLoader* m_poDefSet;
};

#endif /* SPI_TCLCMDINTERFACE_H_ */
