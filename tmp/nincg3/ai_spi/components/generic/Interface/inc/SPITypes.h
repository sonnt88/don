/*!
*******************************************************************************
* \file              SPITypes.h
* \brief             common types used for mirrorlink and IPODout
*******************************************************************************
\verbatim
PROJECT:       GM Gen3
SW-COMPONENT:  Smart Phone Integration
DESCRIPTION:   provides definition of common types used
for mirrorlink and IPODout
COPYRIGHT:     &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
13.09.2013 |  Pruthvi Thej Nagaraju       | Initial Version
07.01.2014 |  Priju K Padiyath            | Updated with types and data structure for SPI message mediator
21.04.2014 |  Shiva Kumar G               | Updated with the elements for Content Attestation
28.04.2014 |  Shihabudheen P M            | Updated with types for audio context handling
28.10.2014 |  Hari Priya E R (RBEI/ECP2)  | Updated with types for Vehicle Data
25.06.2015 |  Shiva kaumr G               | Dynamic display configuration
25.06.2015 |  Sameer Chandra              | Added ML XDeviceKey Support for PSA
03.07.2015 |  Shiva Kumar Gurija          | improvements in ML Version checking
17.07.2015 |  Sameer Chandra              | Added new command ID for knob Encoder and respectiv
                                            keycodes
10.08.2015 |  Vinoop U                    | Updated VehicleConfiguration with Right 
                                              and left and drive enums
18.08.2015 |  Shiva Kumar Gurija          | Added elements for ML Dynamic Audio
05.02.2016 |  Rachana L Achar             | Removed tenAAPKeycode and tenAAPkeyCodes
10.03.2016 | Rachana L Achar              | Added structure for Notification Acknowledgment data

\endverbatim
******************************************************************************/

#ifndef SPITYPES_H_
#define SPITYPES_H_

#include <functional>
#include <vector>
#include <map>
#include <list>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "BaseTypes.h"

static const t_U32 BUFFERSIZE = 256; //TODO Check buffer size
static const t_U32 MAXKEYSIZE = 256;
static const t_U32 cou32MAX_DEVICEHANDLE = 0xFFFFFFFF;
#define ARR_SIZE 3

#define ORIGIN 0
#define AUDLINK_ENUM_SIZE     4

#define SINGLE_TOUCH 1
#define DOUBLE_TOUCH 2

#define STR_LENGTH 250

#define NUM_APPMNGR_CLIENTS 4
#define NUM_VIDEO_CLIENTS 4
#define NUM_RSRCMNGR_CLIENTS 4

//TODO consider using templates to avoid misuse of macros
#define RELEASE_MEM(VAR)      \
   if(NULL!=VAR)              \
   {                          \
      delete VAR;             \
      VAR= NULL;              \
   }

#define RELEASE_ARRAY_MEM(VAR)  \
   if(NULL!=VAR)                \
   {                            \
      delete[] VAR;             \
      VAR= NULL;                \
   }

#define CPY_TO_USRCNTXT(RCVDMSG, USRCNTXT)\
         USRCNTXT.u32FuncID    = RCVDMSG->u16GetFunctionID( );\
         USRCNTXT.u32SrcAppID  = RCVDMSG->u16GetSourceAppID( );\
         USRCNTXT.u32DestAppID = RCVDMSG->u16GetTargetAppID( );\
         USRCNTXT.u32RegID     = RCVDMSG->u16GetRegisterID( );\
         USRCNTXT.u32CmdCtr    = RCVDMSG->u16GetCmdCounter( );\
         USRCNTXT.u32ServcID   = RCVDMSG->u16GetServiceID( );\

//@note: most_fi_tcl_String and midw_fi_tclString szGet() function performs deep copy.
//Hence this macro extracts the FI string data to a std::string object & de-allocates the deep-copied data.
#define GET_STRINGDATA_FROM_FI_STRINGOBJ(FI_STRING_OBJ, CHAR_SET, STD_STRING_OBJ)  \
{                                                                                  \
   t_Char* poszFiString = static_cast<t_Char*>(FI_STRING_OBJ.szGet(CHAR_SET));     \
   STD_STRING_OBJ = "";                               \
   if (NULL != poszFiString)                          \
   {                                                  \
      STD_STRING_OBJ.assign(poszFiString);            \
      delete[] poszFiString;                          \
      poszFiString = NULL;                            \
   }                                                  \
}

//! Macro to place 1 place holders for call back registration
#define SPI_FUNC_PLACEHOLDERS_1 std::placeholders::_1
//! Macro to place 2 place holders for call back registration
#define SPI_FUNC_PLACEHOLDERS_2 std::placeholders::_1,std::placeholders::_2
//! Macro to place 3 place holders for call back registration
#define SPI_FUNC_PLACEHOLDERS_3 std::placeholders::_1,std::placeholders::_2,std::placeholders::_3 
//! Macro to place 4 place holders for call back registration
#define SPI_FUNC_PLACEHOLDERS_4 std::placeholders::_1,std::placeholders::_2,std::placeholders::_3,\
   std::placeholders::_4 
//! Macro to place 5 place holders for call back registration
#define SPI_FUNC_PLACEHOLDERS_5 std::placeholders::_1,std::placeholders::_2,std::placeholders::_3,\
   std::placeholders::_4,std::placeholders::_5
//! Macro to place 6 place holders for call back registration
#define SPI_FUNC_PLACEHOLDERS_6 std::placeholders::_1,std::placeholders::_2,std::placeholders::_3,\
   std::placeholders::_4,std::placeholders::_5,std::placeholders::_6
//! Macro to place 7 place holders for call back registration
#define SPI_FUNC_PLACEHOLDERS_7 std::placeholders::_1,std::placeholders::_2,std::placeholders::_3,\
   std::placeholders::_4,std::placeholders::_5,std::placeholders::_6,std::placeholders::_7

//! Macro to check whether the map is empty
#define SPI_MAP_NOT_EMPTY(var)   var.size()>0

//! Macro to check whether the vector is empty
#define SPI_VECTOR_EMPTY(var)   var.size()>0

//Defines for Mirror link versions
static const t_U32 scou32ML_Major_Version_1 = 1;
static const t_U32 scou32ML_Minor_Version_0 = 0;
static const t_U32 scou32ML_Minor_Version_1 = 1;
static const t_U32 scou32ML_Invalid_Major_Version = 0;

inline t_Bool bIsML10Device(t_U32 u32MajorVer,t_U32 u32MinorVer)
{
   return ((scou32ML_Major_Version_1==u32MajorVer)&&(scou32ML_Minor_Version_0==u32MinorVer));
}

inline t_Bool bIsML11Device(t_U32 u32MajorVer,t_U32 u32MinorVer)
{
   return ((scou32ML_Major_Version_1==u32MajorVer)&&(scou32ML_Minor_Version_1==u32MinorVer));
}

inline t_Bool bIsInvalidMLMajorVersion(t_U32 u32MajorVer)
{
   return (scou32ML_Invalid_Major_Version == u32MajorVer);
}

inline t_Bool bIsML11OrAbove(t_U32 u32MajorVer,t_U32 u32MinorVer)
{
   return ((scou32ML_Major_Version_1<=u32MajorVer)&&(scou32ML_Minor_Version_1<=u32MinorVer));
}

//! Indicates type of SPI feature supported
struct trSpiFeatureSupport
{
public:
   trSpiFeatureSupport() : u8FeatureSupport(0)
   {
   }
   t_Bool bMirrorLinkSupported() const
   {
      return (0 != (u8FeatureSupport & e8MIRRORLINK_FEATURE_SUPPORT));
   }
   t_Void vSetMirrorLinkSupport(t_Bool bMirrorLinkSupported)
   {
      u8FeatureSupport = (true == bMirrorLinkSupported) ?
            (u8FeatureSupport | e8MIRRORLINK_FEATURE_SUPPORT) :
            (u8FeatureSupport & (~e8MIRRORLINK_FEATURE_SUPPORT));
   }
   t_Bool bDipoSupported() const
   {
      return (0 != (u8FeatureSupport & e8DIPO_FEATURE_SUPPORT));
   }
   t_Void vSetDipoSupport(t_Bool bDipoSupported)
   {
      u8FeatureSupport = (true == bDipoSupported) ?
            (u8FeatureSupport | e8DIPO_FEATURE_SUPPORT) :
            (u8FeatureSupport & (~e8DIPO_FEATURE_SUPPORT));
   }
   t_Bool bAndroidAutoSupported() const
   {
      return (0 != (u8FeatureSupport & e8ANDROID_AUTO_FEATURE_SUPPORT));
   }
   t_Void vSetAndroidAutoSupport(t_Bool bAndroidAutoSupported)
   {
      u8FeatureSupport = (true == bAndroidAutoSupported) ?
            (u8FeatureSupport | e8ANDROID_AUTO_FEATURE_SUPPORT) :
            (u8FeatureSupport & (~e8ANDROID_AUTO_FEATURE_SUPPORT));
   }
private:
   enum
   {
      e8MIRRORLINK_FEATURE_SUPPORT = 0x1,
      e8DIPO_FEATURE_SUPPORT = 0x2,
      e8ANDROID_AUTO_FEATURE_SUPPORT = 0x4
      //! Take care of enum values as bitwise logic is used to enable/disable a feature
      //! Ex the next enum value should be 0x8 i.e 00001000
   };
   t_U8 u8FeatureSupport;
};

static const std::string coszUnKnownAppName = "";

//! To Identify Event subscription request.
// Tells whether it subscribe or unsubscribe request/result
typedef enum
{
   e8SUBSCRIBE = 0,
   e8UNSUBSCRIBE = 1
}tenEventSubscription;

//! Identifies result of an operation
typedef enum
{
   //! Identifies an unsuccessful Operation.
   e8FAILURE = 0,
   //! Identifies a successful Operation.
   e8SUCCESS = 1
} tenResponseCode;

//! Identifies different Error Codes
typedef enum
{
   //! No Error
   e8NO_ERRORS = 0x00,
   //! Unknown Error
   e8UNKNOWN_ERROR = 0x01,
   //! Invalid Argument
   e8INVALID_ARGUMENT = 0x02,
   //!Invalid Device Handle
   e8INVALID_DEV_HANDLE = 0x03,
   //!Application Handle Invalid
   e8INVALID_APP_HANDLE = 0x04,
   //!Resource Busy
   e8RESOURCE_BUSY = 0x05,
   //!Operation Rejected
   e8OPERATION_REJECTED = 0x06,
   //!Application Launch failed
   e8LAUNCH_FAILED = 0x07,
   //!Application Termination failed
   e8TERMINATE_FAILED = 0x08,
   //!Unsupported Key
   e8UNSUPPORTED_KEY = 0x09,
   //!Unsupported Operation
   e8UNSUPPORTED_OPERATION = 0x0A,
   //!Invalid Service Handle
   e8INVALID_SERVICE_HANDLE = 0x0B,
   //!Device Selection Failed
   e8SELECTION_FAILED = 0x0C,
   //! Device not connected
   e8DEVICE_NOT_CONNECTED = 0x0D

   //! \note : Error Codes will be extended in future as required.

} tenErrorCode;

//! Identifies the different types of devices
typedef enum
{
   //! Default value for Initialization
   e8DEV_TYPE_UNKNOWN = 0,
   //! Digital iPod Out Device
   e8DEV_TYPE_DIPO = 1,
   //! Mirror Link Device
   e8DEV_TYPE_MIRRORLINK = 2,
   //! Android Audio Device
   e8DEV_TYPE_ANDROIDAUTO = 3

   //Device category has the dependency in Video.
   //If there is any change in enum values. tcl_Video must be revisisted.
   //or else it can cause crashes because of array out of bounds error
} tenDeviceCategory;

//! Device Type (Android, Apple etc)
enum tenDeviceType
{
   e8_UNKNOWN_DEVICE = 0x00,
   e8_ANDROID_DEVICE = 0x01,
   e8_APPLE_DEVICE = 0x02
};

enum tenUSBPortType
{
	e8_PORT_TYPE_NOT_KNOWN = 0,
	e8_PORT_TYPE_OTG = 1,
	e8_PORT_TYPE_NON_OTG = 2
};

//! Indicated support to SPI technologies
enum tenSPISupport
{
   e8SPI_SUPPORT_UNKNOWN = 0,
   e8SPI_SUPPORTED = 1,
   e8SPI_NOTSUPPORTED = 2
};

//! Indicates device connection mode
enum tenDeviceConnectionMode
{
   e8DEVICE_MODE_NONE = 0,
   e8DEVICE_MODE_MIRRORLINK = 1,
   e8DEVICE_MODE_ANDROIDAUTO =2,
   e8DEVICE_MODE_CARPLAY =3
};

//! Identifies Device connection Statuses
typedef enum
{
   /*!
   * Device is connected but not available to start a Mirror Link
   * session (USB Device/ Wi-Fi Au-thenticated devices).
   * Also the Default Status value at Startup.
   */
   e8DEV_NOT_CONNECTED = 0,

   /*!
   * Device is connected and available to
   * start a Mirror Link session or an Application has al-ready
   * been launched on the device (USB De-vice/Wi-Fi Authenticated devices)
   */
   e8DEV_CONNECTED = 1
   
} tenDeviceConnectionStatus;

//! Identifies the device connection types
typedef enum
{
   //! Unknown Connection Type.
   e8UNKNOWN_CONNECTION = 0,
   //! Device connected via USB.
   e8USB_CONNECTED = 1,
   //! Device connected via Wi-Fi.
   e8WIFI_CONNECTED = 2
} tenDeviceConnectionType;

//! Identifies Connection Information of the devices
typedef enum
{
   /*!
   * Default value(Startup case).
   */
   e8DEVICE_STATUS_NOT_KNOWN = 0,

   /*!
   * Device List has been modified with addition of a device.
   */
   e8DEVICE_ADDED = 1,

   /*!
   * Device List has been modified with removal of an existing device.
   */
   e8DEVICE_REMOVED = 2,

   /*!
   * Properties of an existing device updated in the Device List.
   */
   e8DEVICE_CHANGED = 3
} tenDeviceStatusInfo;

//! Identifies Status Information of device applications.
typedef enum
{
   /*!
   * Default value(Startup case).
   */
   e8APP_STATUS_NOT_KNOWN = 0,

   /*!
   * Application List has been updated with addition/removal of devices and on change in drive modes.
   */
   e8APP_LIST_CHANGED = 1,
} tenAppStatusInfo;

//! Session status
enum tenSessionStatus
{
   //! Indicates the Default status
   e8_SESSION_UNKNOWN = 0,
   //! Indicates the Device is selected but a session has not been initiated
   e8_SESSION_INACTIVE = 1,
   //! Indicates the Session has started and the remote application active in foreground
   e8_SESSION_ACTIVE = 2,
   //! Indicates the Session has started and the remote application is active in background
   e8_SESSION_SUSPENDED = 3,
   //! Indicates any internal error during an active session
   e8_SESSION_ERROR = 4,
   //! Indicates that the Non Mirror link application is launched by the user
   e8_SESSION_SUSPENDED_NON_ML_APP = 5,
   //! Indicates that the Non drive certified application is launched by the user in drive mode
   e8_SESSION_SUSPENDED_NON_DRIVE_APP = 6,
};

//! Handset Interaction Status
enum tenHandsetInteractionStatus
{
   //! Indicates the Default status
   e8_HANDSET_INTERACTION_UNKNOWN = 0,
   //! Indicates that handset interaction is required to proceed with projection
   e8_HANDSET_INTERACTION_REQUIRED = 1,
   //! Indicates that handset interaction is not required to proceed with projection
   e8_HANDSET_INTERACTION_NOT_REQUIRED = 2
};

//! Identifies the Icon Mime Types
typedef enum
{
   //! Invalid Icon Format.
   e8ICON_INVALID = 0x00,
   //! Icon image in PNG format.
   e8ICON_PNG = 0x01,
   //! Icon image in JPEG format.
   e8ICON_JPEG = 0x02,
} tenIconMimeType;

//! Identifies the Orientation Modes
typedef enum
{
   //! Invalid Mode for display
   e8INVALID_MODE = 0x00,
   //! Portrait Mode for display
   e8PORTRAIT_MODE = 0x01,
   //! Landscape Mode for display. Default Mode
   e8LANDSCAPE_MODE = 0x02,
} tenOrientationMode;

//! Identifies the Blocking Modes
typedef enum
{
   //! Disable Audio/Video content blocking.
   e8DISABLE_BLOCKING = 0x00,
   //! Enable Audio/Video blocking.
   e8ENABLE_BLOCKING = 0x01,
} tenBlockingMode;

//! Identifies the Vehicle configurations
typedef enum
{
   //! Vehicle is in Non-Driving Mode
   e8PARK_MODE = 0x00,
   //! Vehicle is in Driving Mode
   e8DRIVE_MODE = 0x01,
   //! Day Mode
   e8_DAY_MODE = 0x02,
   //! Night mode
   e8_NIGHT_MODE = 0x03,
   //! Right Hand Drive
   e8_RIGHT_HAND_DRIVE = 0x04,
   //! Left Hand Drive
   e8_LEFT_HAND_DRIVE = 0x05,
   //! To enable or disable Key Lock ( pointer events & key events from the Phone)
   e8_KEY_LOCK = 0x06,
   //! To enable or disable device lick during ML session.
   e8_DEVICE_LOCK = 0x07,
   //! To Enable/Disable the Screen Saver
   e8_SCREEN_SAVER = 0x08,
   //! To enable/disable Voice recognition
   e8_VOICE_RECOG = 0x09,
   //! Top Enable/disable MicroPhone In
   e8_MICROPHONE_IN = 0x0A

}tenVehicleConfiguration;

//! Identifies the Touch Modes
typedef enum
{
   //! Touch pressure is Zero
   e8TOUCH_RELEASE = 0x00,
   //! Touch Pressure is Non-Zero
   e8TOUCH_PRESS = 0x01,
   //! Touch pressure is Non-Zero but is not
   //! the first Press (introduced for AndroidAuto)
   e8TOUCH_MOVED = 0x02,
   //! Non primary touch-point press [AAP specific]
   e8TOUCH_MULTI_PRESS = 0x03,
   //! Non primary touch-point release [AAP specific]
   e8TOUCH_MULTI_RELEASE = 0x04

} tenTouchMode;

//! Identifies the Key Modes
typedef enum
{
   //! Key is Released
   e8KEY_RELEASE = 0x00,
   //! Key is Pressed
   e8KEY_PRESS = 0x01,
} tenKeyMode;

//! Identifies the Usage Preference
enum tenEnabledInfo
{
   //! Disables the chosen setting
   e8USAGE_DISABLED = 0x00,
   //! Enables the chosen setting
   e8USAGE_ENABLED = 0x01,
   //! Wait for user confirmation
   e8USAGE_CONF_REQD = 0x02,
   //! Value on virgin flash
   e8USAGE_UNKNOWN = 0xFF
} ;

//! Identifies the various screen aspect ratios
typedef enum
{
   //! Unknown Aspect Ratio
   e8ASPECT_UNKNOWN = 0x00,
   //! 4:3 Aspect Ratio
   e8ASPECT_4_3 = 0x01,
   //! 16:9 Aspect Ratio
   e8ASPECT_16_9 = 0x02,
} tenScreenAspectRatio;

//! Identifies the Connection Request Type
typedef enum
{
   //! Device Connect
   e8DEVCONNREQ_SELECT = 0x00,
   //! Device Disconnect
   e8DEVCONNREQ_DESELECT = 0x01,
} tenDeviceConnectionReq;

//! Identifies the Key Codes
typedef enum
{
   //! 0, ' '
   e32ITU_KEY_0 = 0x30000100,
   //! 1, '.', ','
   e32ITU_KEY_1 = 0x30000101,
   //! 2, a, b, c
   e32ITU_KEY_2 = 0x30000102,
   //! 3, d, e, f
   e32ITU_KEY_3 = 0x30000103,
   //! 4, g, h, i
   e32ITU_KEY_4 = 0x30000104,
   //! 5, j, k, l
   e32ITU_KEY_5 = 0x30000105,
   //! 6, m, n, 0
   e32ITU_KEY_6 = 0x30000106,
   //! 7, p,q, r, s
   e32ITU_KEY_7 = 0x30000107,
   //! 8, t, u, v
   e32ITU_KEY_8 = 0x30000108,
   //! 9, w, x, y, z
   e32ITU_KEY_9 = 0x30000109,
   //! *, +
   e32ITU_KEY_ASTERIX = 0x3000010A,
   //!#, shift
   e32ITU_KEY_POUND = 0x3000010B,
   //! Take a phone call
   e32DEV_PHONE_CALL = 0x30000200,
   //! End phone call
   e32DEV_PHONE_END = 0x30000201,
   //! Left soft key
   e32DEV_SOFT_LEFT = 0x30000202,
   //! Middle soft key
   e32DEV_SOFT_MIDDLE = 0x30000203,
   //! Right soft key
   e32DEV_SOFT_RIGHT = 0x30000204,
   //! Shortcut to the Application listing
   e32DEV_APPLICATION = 0x30000205,
   //! Ok
   e32DEV_OK = 0x30000206,
   //! Delete (Backspace)
   e32DEV_DELETE = 0x30000207,
   //! Zoom in
   e32DEV_ZOOM_IN = 0x30000208,
   //! Zoom out
   e32DEV_ZOOM_OUT = 0x30000209,
   //! Clear
   e32DEV_CLEAR = 0x3000020A,
   //! Go one step forward
   e32DEV_FORWARD = 0x3000020B,
   //! Go one step backward
   e32DEV_BACKWARD = 0x3000020C,
   //! Shortcut to the Home Screen
   e32DEV_HOME = 0x3000020D,
   //! Shortcut to the search function
   e32DEV_SEARCH = 0x3000020E,
   //! Shortcut to the (application) menu
   e32DEV_MENU = 0x3000020F,
   //! Shortcut to the (application) menu
   e32DEV_PTT = 0x30000210,
   //! Start media playing
   e32MULTIMEDIA_PLAY = 0x30000400,
   //! Pause media playing
   e32MULTIMEDIA_PAUSE = 0x30000401,
   //! Stop media playing
   e32MULTIMEDIA_STOP = 0x30000402,
   //! Forward
   e32MULTIMEDIA_FORWARD = 0x30000403,
   //! Rewind
   e32MULTIMEDIA_REWIND = 0x30000404,
   //! Go to next track in playlist
   e32MULTIMEDIA_NEXT = 0x30000405,
   //! Go to previous track in playlist
   e32MULTIMEDIA_PREVIOUS = 0x30000406,
   //! Mute the audio stream at source
   e32MULTIMEDIA_MUTE = 0x30000407,
   //! Un-mute the audio stream
   e32MULTIMEDIA_UNMUTE = 0x30000408,
   //! Take a Photo
   e32MULTIMEDIA_PHOTO = 0x30000409,
   //! Flash key
   e32DEV_PHONE_FLASH = 0x3000040A,
   //! App Key Code for Media
   e32APP_KEYCODE_MEDIA = 0x3000040B,
   //! App Key Code for Telephony
   e32APP_KEYCODE_TELEPHONY  = 0x3000040C,
   //! App Key Code for Navigation
   e32APP_KEYCODE_NAVIGATION = 0x3000040D,
   //!Invalid Key Code
   e32INVALID_KEY = 0xFFFFFFFF
} tenKeyCode;

//! @note: Key Codes will be extended in future as required.

//! Identifies the application categories
typedef enum
{
   //! Unknown Application
   e32APPUNKNOWN = 0x00000000,
   //! General UI Framework
   e32APPUI = 0x00010000,
   //! Home screen/Start-up screen
   e32APP_UI_HOMESCREEN = 0x00010001,
   //! Menu
   e32APP_UI_MENU = 0x00010002,
   //! Notification
   e32APP_UI_NOTIFICATION = 0x00010003,
   //! Application Listing
   e32APP_UI_APPLICATIONLISTING = 0X00010004,
   //! Settings
   e32APP_UI_SETTINGS = 0X00010005,
   //! General Phone Call Application
   e32APP_PHONE = 0X00020000,
   //! Contact List
   e32APP_PHONE_CONTACTLIST = 0X00020001,
   //! Call Log
   e32APP_PHONE_CALLLOG = 0X00020002,
   //! General Media Applications
   e32APP_MEDIA = 0X00030000,
   //! Music
   e32APP_MEDIA_MUSIC = 0X00030001,
   //! Video
   e32APP_MEDIA_VIDEO = 0X00030002,
   //! Gaming
   e32APP_MEDIA_GAMING = 0X00030003,
   //! Image
   e32APP_MEDIA_IMAGE = 0X00030004,
   //! General Messaging Applications
   e32APP_MESSAGING = 0X00040000,
   //! SMS
   e32APP_MESSAGING_SMS = 0X00040001,
   //! MMS
   e32APP_MESSAGING_MMS = 0X00040002,
   //! Email
   e32APP_MESSAGING_EMAIL = 0X00040003,
   //! General Navigation Application
   e32APP_NAVIGATION = 0X00050000,
   //! General Browser Application
   e32APP_BROWSER = 0X00060000,
   //! Application Store
   e32APP_BROWSER_APPLICATIONSTORE = 0X00060001,
   //! General Productivity Application
   e32APP_PRODUCTIVITY = 0X00070000,
   //! Document Viewer
   e32APP_PRODUCTIVITY_DOCVIEWER = 0X00070001,
   //! Document Editor
   e32APP_PRODUCTIVITY_DOCEDITOR = 0X00070002,
   //! General Information
   e32APP_INFORMATION = 0X00080000,
   //! News
   e32APP_INFORMATION_NEWS = 0X00080001,
   //! Weather
   e32APP_INFORMATION_WEATHER = 0X00080002,
   //! Stocks
   e32APP_INFORMATION_STOCKS = 0X00080003,
   //! Travel
   e32APP_INFORMATION_TRAVEL = 0X00080004,
   //! Sports
   e32APP_INFORMATION_SPORTS = 0X00080005,
   //! Clock
   e32APP_INFORMATION_CLOCK = 0X00080006,
   //! General Social Networking Application
   e32APP_SOCIALNETWORKING = 0X00090000,
   //! General Personal Information Management(PIM)
   e32APP_PIM = 0X000A0000,
   //! Calendar
   e32APP_PIM_CALENDAR = 0X000A0001,
   //! Notes
   e32APP_PIM_NOTES = 0X000A0002,
   //! General UI less Application
   e32APP_NOUI = 0XF0000000,
   //! No UI Audio Server Application
   e32APP_NOUI_AUDIOSERVER = 0XF0000001,
   //! No UI Audio Client Application
   e32APP_NOUI_AUDIOCLIENT = 0XF0000002,
   //! Voice Command Engine
   e32APP_NOUI_VOICECMDENGINE = 0XF0000010,
   //! Conversational Audio
   e32APP_NOUI_CONVERSATIONAL_AUDIO = 0XF0000020,
   //! Switch to Mirror Link Client Native UI
   e32APP_SWITCHTO_CLIENT_NATIVE_UI = 0XF000FFFF,
   //! Mask for the Test&Certification Apps
   e32APP_CERT_TEST = 0XFFFE0000,
   //! General System
   e32APP_SYSTEM = 0XFFFF0000,
   //! PIN Input for Device Unlock
   e32APP_SYSTEM_INPUT_UNLOCK_PIN = 0XFFFF0001,
   //! Bluetooth PIN code Input
   e32APP_SYSTEM_INPUT_BTPIN = 0XFFFF0002,
   //! Other Password Input
   e32APP_SYSTEM_INPUT_OTHERPWD = 0XFFFF000F,
   //! Voice Command Confirmation
   e32APP_SYSTEM_VOICECMD_CONFIRMATION = 0XFFFF0010,
} tenAppCategory;

//! Identifies the content categories of an application
typedef enum
{
   //! Text Content
   e32CONTENT_TEXT = 0X00000001,
   //! Video Content
   e32CONTENT_VIDEO = 0X00000002,
   //! Image Content
   e32CONTENT_IMAGE = 0X00000004,
   //! Graphics Vector Content
   e32CONTENT_GRAPHICSVECTOR = 0X00000080,
   //! 3D Graphics Content
   e32CONTENT_GRAPHICS3D = 0X00000010,
   //! User Interface(e.g. ApplicationMenu)
   e32CONTENT_UI = 0X00000020,
   //! Car Mode (Application UI is complying with all rules for a restricted driving mode)
   e32CONTENT_CARMODE = 0X00010000,
   //! Miscellaneous Content
   e32CONTENT_MISC = 0X80000000,
} tenAppDisplayCategory;

//! Identifies the audio categories of an application
typedef enum
{
   //! Phone Audio
   e32PHONE_AUDIO = 0X00000001,
   //! Media Audio Out
   e32MEDIA_AUDIOOUT = 0X00000002,
   //! Media Audio In
   e32MEDIA_AUDIOIN = 0X00000004,
   //! Voice Command Out
   e32VOICE_COMMANDOUT = 0X00000080,
   //! Voice Command In
   e32VOICE_COMMANDIN = 0X00000010,
   //! Miscellaneous Content
   e32MISC_CONTENT = 0X80000000,
} tenAppAudioCategory;

//! Identifies the trust levels
typedef enum
{
   //! No Trust
   e8UNKNOWN = 0X00,
   //! Trust the User
   e8USER_CONFIGURATION = 0X40,
   //! Trust the application
   e8SELF_REGISTERED_APPLICATION = 0X60,
   //! Trust the VNC and UPnP MirrorLink server
   e8REGISTERED_APPLICATION = 0X80,
   //! Trust a 3rd party certification entity
   e8APPLICATION_CERTIFICATE = 0XA0,
} tenTrustLevel;

typedef enum
{
   //Certification status for ML1.0 devices & Not certified & Only CCC certified but cant be used in
   //Park mode & drive mode
   e8NOT_CERTIFIED = 0x00,
   // Don't use BASE CERTIFIED. App should be either NON CERTIFIED or PARK/DRIVE CERTIFIED,When we inform HMI.
   // If this is used, Updating Application List to HMi and App Filtering needs to be revisited.  
   // We still have this element in the SPI Interface. Commented just to prevent the usage.
   //CCC/CCC-Member certified
   //e8BASE_CERTIFIED = 0x01,
   //Only certified in Park mode
   e8NONDRIVE_CERTIFIED_ONLY = 0x02,
   //Only used in drive mode
   e8DRIVE_CERTIFIED_ONLY = 0x03,
   // Can be used in both the park mode & drive mode
   e8DRIVE_CERTIFIED = 0x04,
}tenAppCertificationInfo;

//! Identifies type of filter to apply for applications
typedef enum
{
   //Park mode apps
   e8APP_CERT_FILTER_PARK_MODE = 0,
   //Drive mode apps
   e8APP_CERT_FILTER_DRIVE_MODE = 1
}tenAppCertificationFilter;

//! identifies App certfying entity
typedef enum
{
   e8UNKNOWN_ENTITY = 0,
   e8CCC_CERTIFIED =1,
   e8CCC_MEMBER_CERTIFIED=2
}tenAppCertificationEntity;

//! Identifies the Application Status Information
typedef enum
{
   //! Application running in Foreground.
   e8FOREGROUND = 0x00,
   //! Application running in Background
   e8BACKGROUND = 0x01,
   //! Application Not Running
   e8NOTRUNNING = 0x02,
} tenAppStatus;

//! Identifies the display context of HMI or the projected device
typedef enum
{
   //! Context information not available.
   e8DISPLAY_CONTEXT_UNKNOWN = 0x00,
   //! Display context is speech recognition.
   e8DISPLAY_CONTEXT_SPEECH_REC = 0x01,
   //! Display context is phone display.
   e8DISPLAY_CONTEXT_PHONE = 0x02,
   //! Display context is navigation display.
   e8DISPLAY_CONTEXT_NAVIGATION = 0x03,
   //! Display context is media display.
   e8DISPLAY_CONTEXT_MEDIA = 0x04,
   //! Display context is informational.
   e8DISPLAY_CONTEXT_INFORMATIONAL = 0x05,
   //! Display context is emergency display.
   e8DISPLAY_CONTEXT_EMERGENCY = 0x06,
   //! Display context is a speaking display.
   e8DISPLAY_CONTEXT_SPEAKING = 0x07,
   //! Display Context is Turn-by-Type navigation display.
   e8DISPLAY_CONTEXT_TBT_NAVIGATION = 0x08,
   //! Display Context is sleep mode. System will wake up on incoming calls.
   e8DISPLAY_CONTEXT_SLEEP_MODE = 0x09,
   //! Display Context is sleep mode. System will not wake up on any event.
   e8DISPLAY_CONTEXT_STANDBY_MODE = 0x0A,
   //! Display Context is screen saver.
   e8DISPLAY_CONTEXT_SCREEN_SAVER = 0x0B
} tenDisplayContext;

//! Identifies the location data usage request.
typedef enum
{
   //! Unknown Data Service. Default Value at StartUp.
   e8UNKNOWN_DATA_SERVICE = 0x00,
   //! GPS Data Service.
   e8GPS_DATA_SERVICE = 0x01,
   //! Location Data Service.
   e8LOCATION_DATA_SERVICE = 0x02,

} tenDataServiceType;

//! Identifies the DAP Authentication progress information.
typedef enum
{
   //! Unknown DAP Status. Default Value at Startup.
   e8DAP_UNKNOWN = 0x00,
   //! DAP Authentication Not supported.
   e8DAP_NOT_SUPPORTED = 0x01,
   //! DAP Authentication In Progress.
   e8DAP_IN_PROGRESS = 0x02,
   //! DAP Authentication Successful.
   e8DAP_SUCCESS = 0x03,
   //! DAP Authentication failed.
   e8DAP_FAILED = 0x04,

} tenDAPStatus;

//! Identifies the the application to be launched on a DiPO device.
typedef enum
{
   //! Default Value at Startup.
   e8DIPO_NOT_USED = 0x00,
   //! DiPO HomeScreen.
   e8DIPO_NO_URL = 0x01,
   //! DiPO Maps Application.
   e8DIPO_MAPS = 0x02,
   //! DiPO MobilePhone Application.
   e8DIPO_MOBILEPHONE = 0x03,
   //! DiPO Telephone Number Application.
   e8DIPO_TEL_NUMBER = 0x04,
   //! DiPO Siri Application for Pre Warning.
   e8DIPO_SIRI_PREWARN = 0x05,
   //! DiPO Siri Application for Button Down Event.
   e8DIPO_SIRI_BUTTONDOWN = 0x06,
   //! DiPO Siri Application Button Up Event.
   e8DIPO_SIRI_BUTTONUP = 0x07,
   //! DiPO music application
   e8DIPO_MUSIC = 0x08,
   //! DiPO now playing application.
   e8DIPO_NOWPLAYING = 0x09
} tenDiPOAppType;

//! Identifies the ECNR configuration for launching DiPO application.
typedef enum
{
   //! Settings unchanged for this dial event.
   e8ECNR_NOCHANGE = 0x00,
   //! For Telephone call.
   e8ECNR_VOICE = 0x01,
   //! For Voice Recognition.
   e8ECNR_SERVER = 0x02,

} tenEcnrSetting;

//! Identifies the change in Bluetooth device
enum tenBTChangeInfo
{
   //! Default value
	e8NO_CHANGE = 0,
   //! Switching from BT to ML device
   e8SWITCH_BT_TO_ML = 1,
   //! Switching from BT to DiPO device
   e8SWITCH_BT_TO_DIPO = 2,
   //! Switching from ML to BT device
   e8SWITCH_ML_TO_BT = 3,
   //! Switching from DiPO to BT device
   e8SWITCH_DIPO_TO_BT = 4,
   //! Switching from BT to AAP device
   e8SWITCH_BT_TO_AAP = 5,
   //! Switching from AAP to BT device
   e8SWITCH_AAP_TO_BT = 6
};

//! Indicates BT device disconnection strategy
enum tenBTDisconnectStrategy
{
   //! All BT devices will be disconnected based on active SPI device type (ML/DiPO)
   e8BT_STRATEGY_DISCONNECT_ALL = 0,
   //! Active SPI device will be BT disconnected & further connection/pairing blocked
   e8BT_STRATEGY_BLOCK_SINGLE = 1,
   //! All BT devices will be disconnected & further BT connection/pairing blocked
   e8BT_STRATEGY_BLOCK_ALL = 2
};

enum tenCallStatus
{
   e8IDLE = 0x00,
   e8DIALING = 0x01,
   e8ACTIVE = 0x02,
   e8ON_HOLD = 0x03,
   e8DISCONNECTING = 0x04,
   e8BUSY = 0x05,
   e8CONFERENCE = 0x06,
   e8IN_VOICEMAIL = 0x07,
   e8RINGTONE = 0x08
   //@Note: Please do not change above values!
   //Values are assigned as per Telephone FI values.
};

//! This enumeration identifies State of the current playing song
enum tenMediaPlayBackState
{
   //! Current track is stopped playing
   e8PLAYBACK_STOPPED = 0x00,
   //! Current track is in playing state
   e8PLAYBACK_PLAYING = 0x01,
   //! Current track is in paused state
   e8PLAYBACK_PAUSED = 0x02,
   //! Current track is seeking forward
   e8PLAYBACK_SEEKFORWARD = 0x03,
   //! Current track is seeking backward
   e8PLAYBACK_SEEKBACKWARD = 0x04,
   //! Identifies Default value(Startup case) or status is not known
   e8PLAYBACK_NOT_KNOWN = 0xFF
};

//! This enumeration identifies shuffle state of the song
enum tenMediaPlayBackShuffleState
{
   //! Shuffle is off
   e8SHUFFLE_OFF = 0x00,
   //! Shuffle is on for Songs
   e8SHUFFLE_SONGS = 0x01,
   //! Shuffle is on for albums
   e8SHUFFLE_ALBUMS = 0x02,
   //! Shuffle is on in the device, however whether it is for songs or albums etc is upto the device specific
   e8SHUFFLE_ON = 0x03,
   //! Identifies Default value(Startup case) or status is not known
   e8SHUFFLE_NOT_KNOWN = 0xFF
};

//! This enumeration identifies Repeat state of the song
enum tenMediaPlayBackRepeatState
{
   //! Repeat is off
   e8REPEAT_OFF = 0x00,
   //! Repeat is on for one song
   e8REPEAT_ONE = 0x01,
   //! Repeat is on for all the songs
   e8REPEAT_ALL = 0x02,
   //! Identifies Default value(Startup case) or status is not known
   e8REPEAT_NOT_KNOWN = 0xFF
};

//! This enumeration identifies current playing media type
enum tenMediaPlayBackMediaType
{
   //! Its a music app
   e8MEDIATYPE_MUSIC = 0x00,
   //! It is pod cast<
   e8MEDIATYPE_PODCAST = 0x01,
   //! Its a audio book.
   e8MEDIATYPE_AUDIOBOOK = 0x02,
   //! Its a iTunes.
   e8MEDIATYPE_ITUESU = 0x03,
   //! Identifies Default value(Startup case) or status is not known
   e8MEDIATYPE_NOT_KNOWN = 0xFF
};


//! This enumeration identifies current playing media type
enum tenSignalStrength
{
   //! Zero bars to be shown on display
   e8ZERO_BARS = 0x00,
   //! One bar to be shown on display
   e8ONE_BAR = 0x01,
   //! Two bars to be shown on display
   e8TWO_BARS = 0x02,
   //! Three bars to be shown on display
   e8THREE_BARS = 0x03,
   //! Four bars to be shown on display
   e8FOUR_BARS = 0x04,
   //! Five bars to be shown on display
   e8FIVE_BARS = 0x05,
   //! Identifies Default value(Startup case) or status is not known
   e8SIGNAL_STRENGTH_NOT_KNOWN = 0xFF
};

//! This enumeration identifies registration status of the SIM on phone side
enum tenRegistrationStatus
{
   //! Not registered
   e8NOT_REGISTERED = 0x01,
   e8SEARCHING = 0x02,
   e8DENIED = 0x03,
   e8REGISTERED_HOME = 0x04,
   e8REGISTERED_ROAMING = 0x05,
   e8EMERGENCY_CALLS_ONLY = 0x06,
   //! Identifies Default value(Startup case) or status is not known
   e8PHONE_REGISTRATION_NOT_KNOWN = 0xFF
};
//! This enumeration identifies AirPlane status on the phone
enum tenAirPlaneModeStatus
{
   //! AirPlane is off
   e8AIRPLANE_OFF = 0x00,
   //! AirPlne is on
   e8AIRPLANE_ON = 0x01,
   //! Identifies Default value(Startup case) or status is not known
   e8PHONE_AIRPLANE_MODE_NOT_KNOWN = 0xFF
};

//! This enumeration identifies AirPlane status on the phone
enum tenPhoneMuteStatus
{
   //! mute is off
   e8PHONE_MUTE_OFF = 0x00,
   //! mute is on
   e8PHONE_MUTE_ON = 0x01,
   //! Identifies Default value(Startup case) or status is not known
   e8PHONE_MUTESTATE_NOT_KNOWN = 0xFF
};

//! This enumeration identifies call direction from the phone
enum tenPhoneCallDirection
{
   //! call is Incoming
   e8CALL_INCOMING = 0x01,
   //! call is Outgoing
   e8CALL_OUTGOING = 0x02,
   //! Identifies Default value(Startup case) or status is not known
   e8PHONE_CALL_DIRECTION_NOT_KNOWN = 0xFF
};

//! This enumeration identifies call state on the phone
//! This enum is used in populating phone call metadata
enum tenPhoneCallState
{
   //! Phone call is disconnected or no active call
   e8CALL_DISCONNECTED = 0x00,
   e8CALL_SENDING = 0x01,
   //! Phone call is ringing.
   e8CALL_RINGING = 0x02,
   //! Phone call is connecting
   e8CALL_CONNECTING = 0x03,
   //! Phone call is active
   e8CALL_ACTIVE = 0x04,
   //! Phone call is on hold.
   e8CALL_HELD = 0x05,
   //! Phone call is disconnecting.
   e8CALL_DISCONNECTING = 0x06,
   //! Identifies Default value(Startup case) or status is not known
   e8PHONE_CALL_STATE_NOT_KNOWN = 0xFF
};

enum tenCallStateUpdateService
{
   //! call Service : Telephony
   e8CALLSTATE_SERVICE_TELEPHONY = 0x00,
   //! call Service : Face Time Audio
   e8CALLSTATE_SERVICE_FACETIMEAUDIO = 0x01,
   //! call Service : Face Time Video
   e8CALLSTATE_SERVICE_FACETIMEVIDEO = 0x02,
   //! call Service : Unknown
   e8CALLSTATE_SERVICE_UNKNOWN = 0xFF
};

enum tenCallStateUpdateDisconnectReason
{
   //! call disconnect reason update : Call Ended
   e8CALLSTATE_DISCONNECT_REASON_ENDED = 0x00,
   //! call disconnect reason update : Call Declined
   e8CALLSTATE_DISCONNECT_REASON_DECLINED = 0x01,
   //! call disconnect reason update : Call Failed
   e8CALLSTATE_DISCONNECT_REASON_FAILED = 0x02,
   //! call disconnect reason update : Unknown
   e8CALLSTATE_DISCONNECT_REASON_UNKNOWN = 0xFF
};

//! Call info structure
struct trTelCallStatusInfo
{
   t_U16 u16CallInstance;
   tenCallStatus  enCallStatus;

   trTelCallStatusInfo():
      u16CallInstance(0), enCallStatus(e8IDLE)
   {
   }
};

//! Telephone Client Callback signatures definition
typedef std::function<t_Void(t_Bool)> tvOnTelephoneCallActivity;
typedef std::function<t_Void(const std::vector<trTelCallStatusInfo>&) > tvOnTelephoneCallStatus;

//! \brief   Structure holding the callbacks for Telephone events.
//!          Used by Telephone Client to notify subscriber about Telephone events.
struct trTelephoneCallbacks
{
   //! Called when there is a change in Telephone call activity
   tvOnTelephoneCallActivity fvOnTelephoneCallActivity;

   //! Called with the details of each CallInstance
   tvOnTelephoneCallStatus fvOnTelephoneCallStatus;

   trTelephoneCallbacks() :
      fvOnTelephoneCallActivity(NULL), fvOnTelephoneCallStatus(NULL)
   {
   }
};

/************structures***************************/
//! \brief This section lists the different structures used by the interfaces noted below
//! This provides Mirror Link version information and contains the following elements
struct trVersionInfo
{
      //! Major Version
      t_U32 u32MajorVersion; //TODO update in API doc
      //! Minor Version
      t_U32 u32MinorVersion;
      //! Patch Version (Set to NULL if not available)
      t_U32 u32PatchVersion;

//! Initialise structure members to 0
      trVersionInfo() :
         u32MajorVersion(0), u32MinorVersion(0), u32PatchVersion(0)
      {

      }
//! Assignment operator
      trVersionInfo& operator=(const trVersionInfo& rfcorVersionInfo)
      {
         if (&rfcorVersionInfo != this)
         {
            u32MajorVersion = rfcorVersionInfo.u32MajorVersion;
            u32MinorVersion = rfcorVersionInfo.u32MinorVersion;
            u32PatchVersion = rfcorVersionInfo.u32PatchVersion;
         }//if (&rfcorVersionInfo != this)
         return *this;
      }
} ;

//!Contains Key Capabilities
struct trKeyCapabilities
{
      /*BitField*/
      //! Contains support information for knob keys.
      t_U32 u32KnobKeySupport;
      /*BitField*/
      //! Contains support information for various device keys.
      t_U32 u32DeviceKeySupport;
      /*BitField*/
      //! Contains support information for various multimedia keys.
      t_U32 u32MultimediaKeySupport;
      /*BitField*/
      //! Contains support information for various miscellaneous keys.
      t_U32 u32MiscKeySupport;
      /*BitField*/
      //! Contains support pointer or touch support
      t_U32 u32PointerTouchSupport;

//! Initialise structure members to 0
      trKeyCapabilities() :
         u32KnobKeySupport(0), u32DeviceKeySupport(0), u32MultimediaKeySupport(0),
               u32MiscKeySupport(0), u32PointerTouchSupport(0)
      {

      }
//! Assignment operator
      trKeyCapabilities& operator=(const trKeyCapabilities& rfcoKeyCapabilities)
      {
         if (&rfcoKeyCapabilities != this)
         {
            u32KnobKeySupport = rfcoKeyCapabilities.u32KnobKeySupport;
            u32DeviceKeySupport = rfcoKeyCapabilities.u32DeviceKeySupport;
            u32MultimediaKeySupport = rfcoKeyCapabilities.u32MultimediaKeySupport;
            u32MiscKeySupport = rfcoKeyCapabilities.u32MiscKeySupport;
            u32PointerTouchSupport = rfcoKeyCapabilities.u32PointerTouchSupport;
         }//if (&rfcoKeyCapabilities != this)
         return *this;
      }
} ;

//!Contains Audio Capabilities
struct trAudioCapabilities
{
      /*BitField*/
      //! Contains Bluetooth HFP support information.
      t_Bool bBTHFPSupport;
      /*BitField*/
      //! Contains Bluetooth A2DP support information.
      t_Bool bBTA2DPSupport;
      /*BitField*/
      //! Contains RTPIn support information.
      t_Bool bRTPInSupport;
      /*BitField*/
      //! Contains RTPOut support information.
      t_Bool bRTPOutSupport;

//! Initialise structure members to 0
      trAudioCapabilities():
         bBTHFPSupport(false), bBTA2DPSupport(false),
         bRTPInSupport(false), bRTPOutSupport(false)
      {

      }
//! Assignment operator
      trAudioCapabilities& operator=(const trAudioCapabilities& rfcorAudioCapabilities)
      {
         if (&rfcorAudioCapabilities != this)
         {
            bBTHFPSupport = rfcorAudioCapabilities.bBTHFPSupport;
            bBTA2DPSupport = rfcorAudioCapabilities.bBTA2DPSupport;
            bRTPInSupport = rfcorAudioCapabilities.bRTPInSupport;
            bRTPOutSupport = rfcorAudioCapabilities.bRTPOutSupport;
         }//if (&rfcorAudioCapabilities != this)
         return *this;
      }

      t_Void vInitialize()
      {
         bBTHFPSupport = false;
         bBTA2DPSupport = false;
         bRTPInSupport = false;
         bRTPOutSupport = false;
      }
} ;

//!Contains Display Capabilities
struct trDisplayCapabilities
{
      /*BitField*/
      //! Supported Frame Buffer configuration data.
      t_U16 u16FrameBufferConfiguration;
      /*BitField*/
      //! Supported Pixel Formats.
      t_U32 u32PixelFormat;
//! Initialise structure members to 0
      trDisplayCapabilities() :
         u16FrameBufferConfiguration(0), u32PixelFormat(0)
      {

      }
//! Assignment operator
      trDisplayCapabilities& operator=(const trDisplayCapabilities& rfcorDisplayCapabilities)
      {
         if (&rfcorDisplayCapabilities != this)
         {
            u16FrameBufferConfiguration = rfcorDisplayCapabilities.u16FrameBufferConfiguration;
            u32PixelFormat = rfcorDisplayCapabilities.u32PixelFormat;
         }//if (&rfcorDisplayCapabilities != this)
         return *this;
      }
} ;

//!Contains server capabilities
struct trServerCapabilities
{
      //! Provides Key Capabilities
      trKeyCapabilities rKeyCapabilities;
      //! Provides Audio Capabilities
      trAudioCapabilities rAudioCapabilities;
      //! Provides Display Capabilities
      trDisplayCapabilities rDisplayCapabilities;
//! Default constructor
      trServerCapabilities()
      {

      }
//! Assignment operator
      trServerCapabilities& operator=(const trServerCapabilities& rfcoServerCapabilities)
      {
         if (&rfcoServerCapabilities != this)
         {
            rKeyCapabilities = rfcoServerCapabilities.rKeyCapabilities;
            rAudioCapabilities = rfcoServerCapabilities.rAudioCapabilities;
            rDisplayCapabilities = rfcoServerCapabilities.rDisplayCapabilities;
         }//if (&rfcoServerCapabilities != this)
         return *this;
      }
} ;

//! This provides information about projection details

struct trProjectionCapability
{
	//! Port type. Indicates OTG or non OTG port
	tenUSBPortType enUSBPortType;
      //! Device type. indicates the OS running on the device
      tenDeviceType enDeviceType;
      //! Carplay capability
      tenSPISupport enCarplaySupport;
      //! Android Auto capability
      tenSPISupport enAndroidAutoSupport;
      //! Mirrorlink capability
      tenSPISupport enMirrorlinkSupport;

    trProjectionCapability() :
    	                       enUSBPortType(e8_PORT_TYPE_NOT_KNOWN),
                        enDeviceType(e8_UNKNOWN_DEVICE),
                        enCarplaySupport(e8SPI_SUPPORT_UNKNOWN),
                        enAndroidAutoSupport(e8SPI_SUPPORT_UNKNOWN),
                        enMirrorlinkSupport(e8SPI_SUPPORT_UNKNOWN)
      {

    }

    //! Note: Please add corresponding elements to assignment operator
    trProjectionCapability& operator=(const trProjectionCapability& rfcoProjectionCapability)
    {
    	if(&rfcoProjectionCapability != this)
    	{
    		enUSBPortType = rfcoProjectionCapability.enUSBPortType;
    		enDeviceType = rfcoProjectionCapability.enDeviceType;
    		enCarplaySupport = rfcoProjectionCapability.enCarplaySupport;
    		enAndroidAutoSupport = rfcoProjectionCapability.enAndroidAutoSupport;
    		enMirrorlinkSupport = rfcoProjectionCapability.enMirrorlinkSupport;
    	}
    	return *this;
    }

};

//! This provides attributes of the connected Device
struct trDeviceInfo
{
      //! Unique device identifier
      t_U32 u32DeviceHandle;
      //! Name of the Device
      std::string szDeviceName;
      //! Type of the Device
      tenDeviceCategory enDeviceCategory;
      //! Model Name of the Device.
      std::string szDeviceModelName;
      //! Manufacturer Name of the Device.
      std::string szDeviceManufacturerName;
      //! Connection Status of the device
      tenDeviceConnectionStatus enDeviceConnectionStatus;
      //! Connection Type of the device
      tenDeviceConnectionType enDeviceConnectionType;
      //! Session information of the devices
      tenSessionStatus enSessionStatus;
      //! Contains version based on the Device Category(MirrorLink/iPodOut)
      trVersionInfo rVersionInfo;
      //! Contains Device connection mode
      tenDeviceConnectionMode enDeviceConnectionMode;
      //! Device can be used for projection only if this value is set to TRUE. 
      //! This value is set to TRUE by default if the client has to enable a device for projection.
      t_Bool bDeviceUsageEnabled;
      //! This value is set to TRUE if a device is automatically selected to initiate a ses-sion, FALSE otherwise. 
      //! Specifically used in case of startup.
      t_Bool bSelectedDevice;
      //! Set to TRUE if device sup-ports DAP, FALSE other-wise.
      t_Bool bDAPSupport;
      //! Bluetooth address of the device, Set to NULL if not available.
      std::string szBTAddress;
      //! provides information about projection details
      trProjectionCapability rProjectionCapability;

      //! Note: Please add corresponding elements to assignment operator
      trDeviceInfo() :
                        u32DeviceHandle(0),
                        enDeviceCategory(e8DEV_TYPE_UNKNOWN),
                        enDeviceConnectionStatus(e8DEV_NOT_CONNECTED),
                        enDeviceConnectionType(e8UNKNOWN_CONNECTION),
                        enSessionStatus(e8_SESSION_UNKNOWN),
                        bDeviceUsageEnabled(true),
                        bSelectedDevice(false),
                        bDAPSupport(false)
      {
      }

      trDeviceInfo& operator=(const trDeviceInfo& rfcoDevInfo)
      {
         if (&rfcoDevInfo != this)
         {
            u32DeviceHandle = rfcoDevInfo.u32DeviceHandle;
            szDeviceName = rfcoDevInfo.szDeviceName;
            enDeviceCategory = rfcoDevInfo.enDeviceCategory;
            szDeviceModelName = rfcoDevInfo.szDeviceModelName;
            szDeviceManufacturerName = rfcoDevInfo.szDeviceManufacturerName;
            enDeviceConnectionStatus = rfcoDevInfo.enDeviceConnectionStatus;
            enDeviceConnectionType = rfcoDevInfo.enDeviceConnectionType;
            enSessionStatus= rfcoDevInfo.enSessionStatus;
            rVersionInfo = rfcoDevInfo.rVersionInfo;
            bDeviceUsageEnabled = rfcoDevInfo.bDeviceUsageEnabled;
            bSelectedDevice = rfcoDevInfo.bSelectedDevice;
            bDAPSupport = rfcoDevInfo.bDAPSupport;
            szBTAddress = rfcoDevInfo.szBTAddress;
            rProjectionCapability = rfcoDevInfo.rProjectionCapability;

         }//if (&rfcoDevInfo != this)
         return *this;
      }
};

//! This provides information about the Remoting Info used to interact with the application after it is launched and contains the following elements
struct trAppRemotingInfo
{
      //! Protocol Identifier of the Remoting protocol that will be used to access the application.
      std::string szRemotingProtocolID;

      //! Format of the data being transferred using the Remoting Protocol.
      std::string szRemotingFormat;
      //! Direction of the content stream.
      std::string szRemotingDirection;
      //! Audio Initial Playback Latency.
      t_U32 u32RemotingAudioIPL;
      //! Audio Maximum Playback Latency.
      t_U32 u32RemotingAudioMPL;

      //! Initialise structure members to 0
      trAppRemotingInfo() :
      u32RemotingAudioIPL(0), u32RemotingAudioMPL(0)
      {

      }
      //Assignment operator
      trAppRemotingInfo& operator=(const trAppRemotingInfo& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            szRemotingProtocolID = corfrSrc.szRemotingProtocolID.c_str();
            szRemotingFormat = corfrSrc.szRemotingFormat.c_str();
            szRemotingDirection = corfrSrc.szRemotingDirection.c_str();
            u32RemotingAudioIPL = corfrSrc.u32RemotingAudioIPL;
            u32RemotingAudioMPL =corfrSrc.u32RemotingAudioMPL;
         }
         return *this;
      }

} ;

//! This provides Application Display Information and contains the following elements
struct trAppAudioInfo
{
      //! Application Audio Content Type.
      std::string szAppAudioType;

      //! Application Audio Content Category.
      tenAppAudioCategory enAppAudioCategory;
      //! Application Audio Content Trust Level.
      tenTrustLevel enTrustLevel;
      //! Application Audio Content Rules.
      t_U32 u32AppAudioRules;

//! Initialise structure members to 0
      trAppAudioInfo() :
         u32AppAudioRules(0)
      {

      }
      //Assignment operator
      trAppAudioInfo& operator=(const trAppAudioInfo& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            szAppAudioType = corfrSrc.szAppAudioType.c_str();
            enAppAudioCategory = corfrSrc.enAppAudioCategory;
            enTrustLevel = corfrSrc.enTrustLevel;
            u32AppAudioRules =corfrSrc.u32AppAudioRules;
         }
         return *this;
      }

} ;

//! This contains Icon attributes.
struct trIconAttributes
{
      //! Width of the icon
      t_U32 u32IconWidth;
      //! Height of the icon
      t_U32 u32IconHeight;
      //! Depth of the icon
      t_U32 u32IconDepth;
      //! Mime Type of the icon
      tenIconMimeType enIconMimeType;
      //! URL of the icon.Data for the icon can be retrieved via GetApplicationIcon Interface
      std::string szIconURL;
      //! Initialise structure members to 0
      trIconAttributes() :
      u32IconWidth(0), u32IconHeight(0), u32IconDepth(0)
      {

      }
      //Assignment operator
      trIconAttributes& operator=(const trIconAttributes& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            u32IconWidth = corfrSrc.u32IconWidth;
            u32IconHeight = corfrSrc.u32IconHeight;
            u32IconDepth = corfrSrc.u32IconDepth;
            enIconMimeType =corfrSrc.enIconMimeType;
            szIconURL = corfrSrc.szIconURL.c_str();
         }
         return *this;
      }

} ;

//! This provides Application Display Information and contains the following elements
struct trAppDisplayInfo
{
      //! Application Display Content Category.
      tenAppDisplayCategory enAppDisplayCategory;
      //! Application Display Content Trust Level.
      tenTrustLevel enTrustLevel;
      //! Application Display Content Rules.
      t_U32 u32AppDisplayRules;
      //! Application Display Content Orientation.
      std::string szAppDisplayOrientation;

//! Initialise structure members to 0
      trAppDisplayInfo() :
         u32AppDisplayRules(0)
      {

      }
      trAppDisplayInfo& operator=(const trAppDisplayInfo& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            enAppDisplayCategory = corfrSrc.enAppDisplayCategory;
            enTrustLevel = corfrSrc.enTrustLevel;
            u32AppDisplayRules = corfrSrc.u32AppDisplayRules;
            szAppDisplayOrientation =corfrSrc.szAppDisplayOrientation.c_str();
         }
         return *this;
      }
} ;

//! This provides detailed application information and contains the following elements
struct trAppDetails
{
      //! Unique application identifier on the device.
      t_U32 u32AppHandle;

      //! String  Equivalent App ID
      t_U32 u32ConvAppHandle;

      //! Name of the application.
      std::string szAppName;
      //! Status of the application.
      tenAppStatus enAppStatus;
      //! Application Variant.
      std::string szAppVariant;
      //! Application Provider Name.
      std::string szAppProviderName;
      //! Application Provider URL.
      std::string szAppProviderURL;
      //! Application Description.
      std::string szAppDescription;
      //! Allowed Profiles for the application. //TODO
      std::vector<std::string> AppAllowedProfiles;
      //! URL of application certificate.
      std::string szAppCertificateURL;
      //! Application Category.
      tenAppCategory enAppCategory;
      //! Application Trust Level.
      tenTrustLevel enTrustLevel;
      //! Application Display Information
      trAppDisplayInfo rAppDisplayInfo;
      //! Application Audio Information
      trAppAudioInfo rAppAudioInfo;
      //! Application Remoting Information
      trAppRemotingInfo rAppRemotingInfo;
      //! Number of Application Icons
      t_U32 u32NumAppIcons;
      //! Application Icon Details. //TODO
      std::vector<trIconAttributes> tvecAppIconList;
      //! Notification support info
      t_Bool bNotificationSupport;

      tenAppCertificationInfo enAppCertificationInfo;
      tenAppCertificationEntity enAppCertificationEntity;
	  
      //! Initialise structure members to 0
      trAppDetails() :
      u32AppHandle(0),u32ConvAppHandle(0),u32NumAppIcons(0), bNotificationSupport(false)
      {

      }
      //Assignment operator
      trAppDetails& operator=(const trAppDetails& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            //copy info from corfrSrc
            u32AppHandle = corfrSrc.u32AppHandle;
            u32ConvAppHandle = corfrSrc.u32ConvAppHandle;
            szAppName = corfrSrc.szAppName.c_str();
            enAppStatus = corfrSrc.enAppStatus;
            szAppVariant =corfrSrc.szAppVariant.c_str();
            szAppProviderName = corfrSrc.szAppProviderName.c_str();
            szAppProviderURL = corfrSrc.szAppProviderURL.c_str();
            szAppDescription = corfrSrc.szAppDescription.c_str();
            AppAllowedProfiles =corfrSrc.AppAllowedProfiles;
            szAppCertificateURL = corfrSrc.szAppCertificateURL.c_str();
            enAppCategory = corfrSrc.enAppCategory;
            enTrustLevel = corfrSrc.enTrustLevel;
            rAppDisplayInfo =corfrSrc.rAppDisplayInfo;
            rAppAudioInfo = corfrSrc.rAppAudioInfo;
            rAppRemotingInfo = corfrSrc.rAppRemotingInfo;
            u32NumAppIcons = corfrSrc.u32NumAppIcons;
            tvecAppIconList = corfrSrc.tvecAppIconList;
            enAppCertificationInfo = corfrSrc.enAppCertificationInfo;
            bNotificationSupport = corfrSrc.bNotificationSupport;
         }
         return *this;
      }

} ;

//! This provides information of the application
struct trAppInfo
{
      //! Unique device identifier
      t_U32 u32DeviceHandle;
      //! Number of applications on the device.
      t_U32 u32NumOfApps;
      //! std::vector<>Application Details //TODO
      std::vector<trAppDetails> tvecAppDetailsList;

//! Initialise structure members to 0
      trAppInfo() :
         u32DeviceHandle(0), u32NumOfApps(0)
      {

      }

} ;


//! This provides session status information for a device and contains the following elements
struct trSessionStatusInfo
{
      //! Unique device Identifier.
      t_U32 u32DeviceHandle;
      //! Current Session Status
      tenSessionStatus enSessionStatus;

//! Initialise structure members to 0
      trSessionStatusInfo() :
         u32DeviceHandle(0)
      {

      }
} ;

//! This identifies the X and Y Co-ordinates and contains the following elements
struct trTouchCoordinates
{
      //! Touch Mode information
      tenTouchMode enTouchMode;
      //! X Co-ordinate value
      t_S32 s32XCoordinate;
      //! Y Co-ordinate value
      t_S32 s32YCoordinate;
      //!Touch Identifier
      t_U8 u8Identifier;//TODO : added update API document

//! Initialise structure members to 0
      trTouchCoordinates() :
         s32XCoordinate(0), s32YCoordinate(0), u8Identifier(0)
      {

      }
} ;

//! This identifies Touch information and contains the following elements
struct trTouchInfo
{
      //! Information for various Touch points.
      std::vector<trTouchCoordinates> tvecTouchCoordinatesList;
      //! TimeStamp
      t_U32 u32TimeStamp; //TODO : added update API document
      t_U32 u32Pressure; //TODO : added update API document

//! Initialise structure members to 0
      trTouchInfo() :
         u32TimeStamp(0), u32Pressure(0)
      {

      }
} ;

//! This identifies Single/Multi-Touch information and contains the following elements
struct trTouchData
{
      //! Number of Touch Descriptors. In case of Single touch//! This value is 1
      t_U32 u32TouchDescriptors;
      //! Touch Information. In case of Single touch list has only one entry
      std::vector<trTouchInfo> tvecTouchInfoList;

//! Initialise structure members to 0
      trTouchData() :
         u32TouchDescriptors(0)
      {

      }
} ;

//! Provides Attributes to scale
struct trScalingAttributes
{
   //! New Height of the screen
   t_U32 u32ScreenHeight;
   //! New width of the screen
   t_U32 u32ScreenWidth;
   //! New X axis start coordinate
   t_S32 s32XStartCoordinate;
   //! New Y axis start coordinate
   t_S32 s32YStartCoordinate;
   //! Scaling value for phone width
   t_Float fWidthScaleValue;
   //! Scaling value of phone height
    t_Float fHeightScalingValue;


    trScalingAttributes():
   u32ScreenHeight(0), u32ScreenWidth(0),
      s32XStartCoordinate(0),s32YStartCoordinate(0),
      fWidthScaleValue(1.0),fHeightScalingValue(1.0)
   {

   }

} ;

//!Provides screen attributes
struct trScreenAttributes
{
      //! Height of the screen
      t_U32 u32ScreenHeight;
      //! Width of the screen
      t_U32 u32ScreenWidth;
      //! Aspect Ratio.
      tenScreenAspectRatio enScreenAspectRatio;

//! Initialise structure members to 0
      trScreenAttributes() :
         u32ScreenHeight(0), u32ScreenWidth(0)
      {

      }
} ;

//! Identifies the attributes associated with Video.
struct trVideoAttributes
{
      //! Orientation Mode.
      tenOrientationMode enOrientationMode;
      //! Screen size of the CE device.
      trScreenAttributes rScreenAttributes;

//! Default Constructor
      trVideoAttributes()
      {

      }
} ;

//!It identifies the application IDs for which notification has to be enabled / disabled.
struct trNotiEnable
{
      //! Unique Application Identifier
      t_U32 u32AppHandle;
      //! Enable/Disable Info
      tenEnabledInfo enEnabledInfo;

//! Initialise structure members to 0
      trNotiEnable() :
         u32AppHandle(0)
      {

      }
} ;

//! Provides Notification Action details
struct trNotiAction
{
      //! Notification action Identifier
      t_U32 u32NotiActionID;
      //! Notification action Name
      std::string szNotiActionName;
      //! Number of Notification Action Icons
      t_U32 u32NotiActionIconCount;
      //! Launch the application on this particular user action             
      t_Bool bLaunchAppReq;
      //! Details of Notification action icons
      std::vector<trIconAttributes> tvecNotiActionIconList;

//! Initialize structure members to 0
      trNotiAction() :
         u32NotiActionID(0), u32NotiActionIconCount(0),
         bLaunchAppReq(false)
      {

      }
} ;

//!Provides detailed information for a Notification Event
struct trNotiData
{
      //! Notification Event ID
      t_U32 u32NotiID;
      //! Title of the notification
      std::string szNotiTitle;
      //! Body of the Notification
      std::string szNotiBody;
      //! Number of Notification Icons
      t_U32 u32NotiIconCount;
      //! Details of Notification icons
      std::vector<trIconAttributes> tvecNotiIconList;
      //! Application ID of the notification
      t_U32 u32NotiAppID;
      //! Details of Notification Actions
      std::vector<trNotiAction> tvecNotiActionList;

//! Initialise structure members to 0
      trNotiData() :
         u32NotiID(0), u32NotiIconCount(0), u32NotiAppID(0)
      {

      }
};

//!Structure containing Details of supported XdeviceKeys
struct trXDeviceKeyDetails
{
   //! X-Device Key Id (Internal to SPI)
   t_U8 u8KeyId;

   //!X-Device key Name
   std::string szXDeviceName;

   //!X-Device Manadatory Status
   t_Bool bIsMandatory;

   //!X-Device Symbol Value
   tenKeyCode enSymbolValue;

   //!List of Icons for each X-Device key
   std::vector<trIconAttributes> tvecKeyIconList;
   //! Initialise structure members to 0
   trXDeviceKeyDetails() :
      bIsMandatory(false),enSymbolValue(e32INVALID_KEY),u8KeyId(0)
   {

   }
   //Assignment operator
   trXDeviceKeyDetails& operator=(const trXDeviceKeyDetails& corfrSrc)
   {
     if( &corfrSrc != this)
     {
        //copy info from corfrSrc
        bIsMandatory  = corfrSrc.bIsMandatory;
        szXDeviceName = corfrSrc.szXDeviceName;
        enSymbolValue = corfrSrc.enSymbolValue;
        tvecKeyIconList = corfrSrc.tvecKeyIconList;
     }
     return *this;
   }

} ;

//!Contains client capabilities
struct trClientCapabilities
{
      //! Provides Key Capabilities
      trKeyCapabilities rKeyCapabilities;

//!Default Constructor
      trClientCapabilities()
      {
      }
} ;

//!Contains Server Key capabilities
struct trMLSrvKeyCapabilities
{
      //! Provides Key Capabilities
      trKeyCapabilities rKeyCapabilities;

      //! Provides XDevice Keys Capabilities
      std::vector<trXDeviceKeyDetails> vecrXDeviceKeyInfo;

      //!Default Constructor
      trMLSrvKeyCapabilities()
      {
      }
} ;
/*
//!Provides application specific metadata information associated with currently active application (audio/ video).
struct trAppMetaData
{
   //! Validity flag of Metadata
   t_Bool bMetadataValid;
   //! Name of the artist.
   std::string szArtist;
   //! Title of currently active application.
   std::string szTitle;
   //! Name of the album.
   std::string szAlbum;
   //! Name of the Genre.
   std::string szGenre;
   //! Total Play time of the media object in seconds.
   t_U32 u32TotalPlayTime;
   //! Elapsed play time of the media object in seconds.
   t_U32 u32ElapsedPlayTime;
   //! Track number
   t_U32 u32TrackNumber;
   //! Name of the composer
   std::string szComposerName;
   //! URL for image data.
   std::string szImageUrl;
   //! Image size in bytes.
   t_U32 u32ImageSize;
   //! MIME type of the image pointed by ImageUrl.
   std::string szImageMIMEType;
   //! Name of the application.
   std::string szAppName;
   //! Uutgoing caller name, in-coming caller ID or phone number.
   std::string szPhoneCaller;
   //! Formatted string of phone call information.
   std::string szPhoneCallInfo;

//! Initialise structure members to 0
   trAppMetaData() :
      bMetadataValid(false), u32TotalPlayTime(0), u32ElapsedPlayTime(0),
      u32ImageSize(0), u32TrackNumber(0)
   {

   }

};
*/

//!Provides Media application specific metadata information associated with currently active application (audio/ video).
struct trAppMediaMetaData
{
   //! Validity flag of Metadata
   t_Bool bMediaMetadataValid;
   //! Name of the application.
   std::string szAppName;
   //! Title of currently active application.
   std::string szTitle;
   //! Name of the artist.
   std::string szArtist;
   //! Name of the album.
   std::string szAlbum;
   //! Name of the albumartist.
   std::string szAlbumArtist;
   //! Name of the Genre.
   std::string szGenre;
   //! Name of the composer
   std::string szComposerName;
   //! Track number
   t_U32 u32TrackNumber;
   //! Album Track Count
   t_U32 u32AlbumTrackCount;
   //! Album Disc number
   t_U32 u32AlbumDiscNumber;
   //! Album Disc Count
   t_U32 u32AlbumDiscCount;
   //! Chapter Count
   t_U32 u32ChapterCount;
   //! MIME type of the image pointed by ImageUrl.
   std::string szImageMIMEType;
   //! Image size in bytes.
   t_U32 u32ImageSize;
   //! URL for image data
   std::string szImageUrl;
   //! Album Art
   std::string szAlbumArt;
   //! Current Playback state of the song
   tenMediaPlayBackState enMediaPlayBackState;
   //! Shuffle state
   tenMediaPlayBackShuffleState enMediaPlayBackShuffleState;
   //! Repeat state of the song
   tenMediaPlayBackRepeatState enMediaPlayBackRepeatState;
   //! Current playing media type.
   tenMediaPlayBackMediaType enMediaPlayBackMediaType;
   //! Now playing an iTunes ad.
   t_Bool bITunesRadioAd;
   //! Name of the now playing iTunes Radio station name.
   std::string szITunesRadioStationName;
   //! Indicates what is media rating. It is can 0...5
   t_U8 u8MediaRating;
   //! Initialise structure members to default values
   trAppMediaMetaData() :
	   bMediaMetadataValid(false), u32TrackNumber(0xFFFFFFFF),u32AlbumTrackCount(0xFFFFFFFF),
      u32AlbumDiscNumber(0xFFFFFFFF),u32AlbumDiscCount(0xFFFFFFFF),u32ChapterCount(0xFFFFFFFF),
      u32ImageSize(0), enMediaPlayBackState(e8PLAYBACK_NOT_KNOWN),
      enMediaPlayBackShuffleState(e8SHUFFLE_NOT_KNOWN),bITunesRadioAd(false),
      enMediaPlayBackRepeatState(e8REPEAT_NOT_KNOWN),enMediaPlayBackMediaType(e8MEDIATYPE_NOT_KNOWN),u8MediaRating(0xFF)
   {

   }
};

//! This structure contains Phone call specific metadata information
struct trPhoneCallMetaData
{
   //! Phone number
   std::string szPhoneNumber;
   //! Name of the caller in the Phone contacts.
   std::string szDisplayName;
   //! Identifies the state of the call
   tenPhoneCallState enPhoneCallState;
   //! Incoming or outgoing call
   tenPhoneCallDirection enPhoneCallDirection;
   //! Call duration in secs
   t_U32 u32CallDuration;
   //! Call UUID
   std::string szCallUUID;
   //! If caller label is in contacts, than mobile, work or home would be populated.
   std::string szCallerLabel;
   //! Notification support info
   t_Bool bConferencedCall;
   //! Conference group number
   t_U8 u8ConferenceGroup;
   //! Call service of the current device
   tenCallStateUpdateService enCallStateUpdateService;
   //! Call disconnect reason of the current device
   tenCallStateUpdateDisconnectReason enCallStateUpdateDisconnectReason;
   //! Initialise structure members to default vaules
   trPhoneCallMetaData() :
      enPhoneCallState(e8PHONE_CALL_STATE_NOT_KNOWN),enPhoneCallDirection(e8PHONE_CALL_DIRECTION_NOT_KNOWN),
      u32CallDuration(0xFFFFFFFF), bConferencedCall(false), u8ConferenceGroup(0),
      enCallStateUpdateService(e8CALLSTATE_SERVICE_UNKNOWN),
      enCallStateUpdateDisconnectReason(e8CALLSTATE_DISCONNECT_REASON_UNKNOWN)
   {
   }
};

//!Data to hold Phone application information like, call status, signal strength to the client.
struct trAppPhoneData
{
   //! Validity flag of Phone Metadata
   t_Bool bPhoneMetadataValid;
   //! std::vector<>Phone call details Details
   std::vector<trPhoneCallMetaData> tvecPhoneCallMetaDataList;
   //! Signal Strength on the Phone
   tenSignalStrength enSignalStrength;
   //! Registration status of the phone connection
   tenRegistrationStatus enRegistrationStatus;
   //! AirPlane Mode status on the phone
   tenAirPlaneModeStatus enAirPlaneModeStatus;
   //! Mute status of the call..
   tenPhoneMuteStatus enMute;
   //! Carrier Name on the phone.
   std::string szCarrierName;
   //! Current call count
   t_U8 u8CurrentCallCount;
   //! Initialise structure members to default vaules
   trAppPhoneData() :
	   bPhoneMetadataValid(false), enSignalStrength(e8SIGNAL_STRENGTH_NOT_KNOWN),
	   enRegistrationStatus(e8PHONE_REGISTRATION_NOT_KNOWN),
	   enAirPlaneModeStatus(e8PHONE_AIRPLANE_MODE_NOT_KNOWN),enMute(e8PHONE_MUTESTATE_NOT_KNOWN),
	   u8CurrentCallCount(0)
   {

   }
};

//!Structure to hold playtime of the current playing media song.
struct trAppMediaPlaytime
{
   //! Total play time of the media object in seconds.
   t_U32 u32TotalPlayTime;
   //! Elapsed play time of the media object in seconds.
   t_U32 u32ElapsedPlayTime;
   //! Initialise structure members to default vaules
   trAppMediaPlaytime() :
      u32TotalPlayTime(0), u32ElapsedPlayTime(0)
   {

   }
};

/*****************************************************
*****************************************************
********************* MLTypeDefines******************
*****************************************************
*****************************************************/

/*****************************************************
**************** COMMON ******************************
*****************************************************/
#define MAX_KEYSIZE 256
/*****************************************************
**************DISCOVERY******************************
*****************************************************/

/***
* \brief Errors encountered in discovery SDK
*/
/***
* The value for the event handle that indicates the handle is "inactive".
*/
static const t_S32 cs32MLDiscovererNoEventHandle = -1;
/***
* No error occured. Operation completed successfully.
*/
static const t_S32 cs32MLDiscoveryErrorNone = 0;
/***
* One of the parameters passed in the request is not valid.
*/
static const t_S32 cs32MLDiscoveryErrorInvalidParameter = 1;
/***
* Can't load the library needed for the Discoverer.
*/
static const t_S32 cs32MLDiscoveryErrorLibraryNotLoaded = 2;
/***
* Not enough memory to complete the request.
*/
static const t_S32 cs32MLDiscoveryErrorOutOfMemory = 3;
/***
* The requested item can not be found.
*/
static const t_S32 cs32MLDiscoveryErrorNotFound = 4;
/***
* An unhandled exception occured in the SDK thread,
* or an unexpected event occured in the SDK main thread loop.
*/
static const t_S32 cs32MLDiscoveryErrorBadEvent = 5;
/***
* The Discoverer has not been loaded.
*/
static const t_S32 cs32MLDiscoveryErrorDiscovererNotLoaded = 6;
/***
* The item has not been initialized.
*/
static const t_S32 cs32MLDiscoveryErrorNotInitialized = 7;
/***
* The Discoverer is stopping.
*/
static const t_S32 cs32MLDiscoveryErrorStopping = 8;
/***
* The requested operation is not supported.
*/
static const t_S32 cs32MLDiscoveryErrorNotSupported = 9;
/***
* An item already existst.
*/
static const t_S32 cs32MLDiscoveryErrorAlreadyExists = 10;
/***
* Access to a resource is not allowed.
*/
static const t_S32 cs32MLDiscoveryErrorAccessDenied = 11;
/***
* The request can't be completed because initialization failed.
*/
static const t_S32 cs32MLDiscoveryErrorUnableToInitialize = 12;
/***
* The request has been cancelled. This can happen because of a
* call to Discoverer Stop.
*/
static const t_S32 cs32MLDiscoveryErrorCancelled = 13;
/***
* The request could not be completed before the timer expiry.
*/
static const t_S32 cs32MLDiscoveryErrorTimedOut = 14;
/***
* The request could not be completed because the remote
* side returned an error..
*/
static const t_S32 cs32MLDiscoveryFailed = 15;
/***
* The request to operate on an entity could not be completed
* because the entity is not known.
*/
static const t_S32 cs32MLDiscoveryErrorUnkownEntity = 16;
/***
* Start of range of third-party Discoverer-specific error codes.
* The range from this value onwards is reserved for the use of
* thid parties developing Discoverers.
* It is intended for error conditions that are specific to particular
* Discoverer implementations and that do not map closely to error codes
* defined in the 0 to (VNCDiscoverySDKErrorVENDOR - 1) range.
*/
static const t_S32 cs32MLDiscoveryErrorVENDOR = 0x10000;


//Disc specific

/***
* ID for an unmatched request.
*/
static const t_U32 cu32MLDiscoveryNoRequestId = 0;
/***
* No timeout shall be enforced.
*/
static const t_S32 cs32MLDiscoveryNoTimeout = -1;
/***
* The call has to be instant, so the callback should
* be called before the request method completes.
*/
static const t_S32 cs32MLDiscoveryInstantCall = 0;
/***
* The Discoverer status is not defined. Most likely because
* the Discoverer is not started.
*/
static const t_S32 cs32MLDiscoveryDiscovererStatusUndefined = 0;
/***
* The Discoverer is waiting for new devices to appear.
*/
static const t_S32 cs32MLDiscoveryDiscovererStatusWaiting = 1;
/***
* The Discoverer is scanning one, or more, new devices.
*/
static const t_S32 cs32MLDiscoveryDiscovererStatusScanning = 2;

/***
* UnKnown Discoverer Error for unmatched defined error.
*/
static const t_S32 cs32MLDiscoveryUnknownError = -2;

/***
* VNC Discovery SDK Error.
*/
typedef t_S32 MLDiscoveryError;

/***
* VNCDiscoverySDK Timeout
*/
typedef t_S32 MLDiscoverySDKTimeoutMicroseconds;

/***
* Vector to store Application List
*/
typedef std::vector<t_U32> tvecMLApplist;

/***
* This provides the connected Mirrorlink Device attributes.
 */
struct trMLDeviceInfo
{
      //! brief Unique Device Name of the device provided by VNC (UUID).
      t_String szDeviceUUID;

      //! brief Friendly name of the device
      t_String szFriendlyDeviceName;

      //! Device Model Name
      t_String szDeviceModelName;

      //! Device Manufacturer Name
      t_String szDeviceManufacturerName;

      //! brief vnccmd of the device
      t_String szVnccmd;

      //! BT Address
      t_String szBTAddress;

      //! brief Type of the Device - MirrorLink
      tenDeviceCategory enDeviceCategory;

      //! brief Connection Status of the device
      tenDeviceConnectionStatus enDeviceConnectionStatus;

      //! brief Connection Type of the device
      tenDeviceConnectionType enDeviceConnectionType;

      //! brief Contains version based on the Device Category
      trVersionInfo rVersionInfo;

      trMLDeviceInfo() :
                        enDeviceCategory(e8DEV_TYPE_UNKNOWN),
                        enDeviceConnectionStatus(e8DEV_NOT_CONNECTED),
                        enDeviceConnectionType(e8UNKNOWN_CONNECTION)
      {

      }
      trMLDeviceInfo& operator=(const trMLDeviceInfo& corfrSrc)
      {
         if (&corfrSrc != this)
         {
            szDeviceUUID = corfrSrc.szDeviceUUID.c_str();
            szFriendlyDeviceName = corfrSrc.szFriendlyDeviceName.c_str();
            szDeviceManufacturerName = corfrSrc.szDeviceManufacturerName.c_str();
            szDeviceModelName = corfrSrc.szDeviceModelName.c_str();
            szVnccmd = corfrSrc.szVnccmd.c_str();
            szBTAddress = corfrSrc.szBTAddress.c_str();
            enDeviceCategory = corfrSrc.enDeviceCategory;
            enDeviceConnectionStatus = corfrSrc.enDeviceConnectionStatus;
            enDeviceConnectionType = corfrSrc.enDeviceConnectionType;
            rVersionInfo.u32MajorVersion = corfrSrc.rVersionInfo.u32MajorVersion;
            rVersionInfo.u32MinorVersion = corfrSrc.rVersionInfo.u32MinorVersion;
         }//if( &corfrSrc != this)
         return *this;
      }
} ;

//! This provides certification info of a certified entity of an application
struct trAppCertInfo
{
   //Certified Entity name
   std::string szCertEntityName;
   //Restricted list of application (Drive Mode)
   std::vector<std::string> vecCertEntityRestrictedList;
   //Non restricted list of application (Park Mode)
   std::vector<std::string> vecCertEntityNonRestrictedList;

   trAppCertInfo()
   {
   }

   trAppCertInfo& operator=(const trAppCertInfo& corfrSrc)
   {
      if(&corfrSrc != this)
      {
         szCertEntityName = corfrSrc.szCertEntityName.c_str();
         vecCertEntityRestrictedList = corfrSrc.vecCertEntityRestrictedList;
         vecCertEntityNonRestrictedList = corfrSrc.vecCertEntityNonRestrictedList;
      }//if( &corfrSrc != this)
      return *this;
   }
};

//! This provides application information
struct trApplicationInfo
{
   //Application Details
   trAppDetails rAppdetails;
   //Number of Application certified entities
   t_U8 u8AppCertEntityCount;
   //Application certification info of each cerified entity
   std::vector<trAppCertInfo> vecAppCertInfo;

   //Initialize structure members
   trApplicationInfo():u8AppCertEntityCount(0)
   {
      //Add 
   }
   trApplicationInfo& operator=(const trApplicationInfo& corfrSrc)
   {
      if( &corfrSrc != this)
      {
         //copy info from corfrSrc
         rAppdetails = corfrSrc.rAppdetails;
         u8AppCertEntityCount = corfrSrc.u8AppCertEntityCount;
         vecAppCertInfo = corfrSrc.vecAppCertInfo;
      }//if( &corfrSrc != this)
      return *this;
   }
} ;

//! Used to store List applications supported by the device & DAP certification Info
struct trMLDeviceAppInfo
{
   trVersionInfo rVersionInfo;
   std::vector<trAppDetails> vecAppDetailsList;
};

//! This provides ScreenSize information
struct trScreenSize
{
   t_U32 u32Screen_Height;
   t_U32 u32Screen_Width;
//! Initialise structure members to 0
   trScreenSize():u32Screen_Height(0),
      u32Screen_Width(0)
   {
   }
   trScreenSize& operator=(const trScreenSize& corfrSrc)
   {
      if( &corfrSrc != this)
      {
         u32Screen_Height = corfrSrc.u32Screen_Height;
         u32Screen_Width = corfrSrc.u32Screen_Width;
      }//if( &corfrSrc != this)
      return *this;
   }
};

//! This provides ScreenOffset information
struct trScreenOffset
{
   t_U32 u32Screen_X_Offset;
   t_U32 u32Screen_Y_Offset;
//! Initialise structure members to 0
   trScreenOffset():u32Screen_X_Offset(0),
      u32Screen_Y_Offset(0)
   {
   }
   trScreenOffset& operator=(const trScreenOffset& corfrSrc)
   {
      if( &corfrSrc != this)
      {
         u32Screen_X_Offset = corfrSrc.u32Screen_X_Offset;
         u32Screen_Y_Offset = corfrSrc.u32Screen_Y_Offset;
      }//if( &corfrSrc != this)
      return *this;
   }
};


// "0" is the default profile ID, that should be supported by any server
static const t_String cszClientProfileId = "0";
static const t_U32 cou32MLClientMajorversion = 1;
static const t_U32 cou32MLClientMinorVersion = 1;
static const t_Bool cobMLNotiUISupport = true;
static const t_U8 cou8MLNotiMaxActions = 3;

static const t_U8 cou8DefaultMLNotiActions = 2;

/*****************************************************
**************DAP************************************
*****************************************************/

/**
* \brief Errors in DAPSDK
*/
typedef enum
{
   /**
   * \brief The operation was successful.
   */
   e32MLDAP_ERRORNONE = 0,

   /**
   * \brief The MirrorLink server does not implement the component that it
   * was asked to attest in the call to VNCDAPClientAttestationRequest().
   *
   * Returned by the DAP server.
   */
   e32MLDAP_RESULT_COMPONENT_NOTIMPLEMENTED = 1,

   /**
   * \brief The DAP server does not recognize the trust root certificate
   * that was provided in the last call to VNCDAPClientSetTrustRoot().
   *
   * Returned by the DAP server.
   */
   e32MLDAP_RESULT_UNKNOWN_TRUSTROOT = 2,

   /**
   * \brief The DAP server does not implement the version of the DAP
   * protocol that was specified when VNCDAPClientCreate() was called.
   *
   * The MirrorLink client application can avoid this error by specifying
   * the version number that was returned by the MirrorLink server in its
   * TmServerDevice XML.
   *
   * Returned by the DAP server.
   */
   e32MLDAP_RESULT_VERSION_UNSUPPORTED = 3,

   /**
   * \brief The DAP server does not recognize the component ID that was
   * specified in the call to VNCDAPClientAttestationRequest().
   *
   * Returned by the DAP server.
   */
   e32MLDAP_RESULT_UNKNOWN_COMPONENT = 4,

   /**
   * \brief Attestation failed, due to some server-side error.
   *
   * Returned by the DAP server.
   */
   e32MLDAP_RESULT_ATTESTATIONFAILED = 5,

   /**
   * \brief Lowest error value that can be returned by the DAP SDK but that
   * does not originate with a DAP server.
   */
   e32MLDAP_ERROR_LOCAL = 1000,

   /**
   * \brief The operation failed for an unspecified reason.
   *
   * Whereever possible, the DAP SDK will attempt to provide a more specific
   * error code.
   */
   e32MLDAP_ERROR_FAILED = e32MLDAP_ERROR_LOCAL,

   /**
   * \brief The application called a DAP SDK API with an invalid parameter.
   */
   e32MLDAP_ERROR_INVALIDPARAMETER = e32MLDAP_ERROR_LOCAL + 1,

   /**
   * \brief A dynamic memory allocation failed.
   */
   e32MLDAP_ERROR_OUTOFMEMORY = e32MLDAP_ERROR_LOCAL + 2,

   /**
   * \brief The DAP protocol version requested by a call to
   * VNCDAPClientCreate() is not supported by this release of the DAP SDK.
   */
   e32MLDAP_ERROR_SERVER_VERSION_NOTSUPPORTED = e32MLDAP_ERROR_LOCAL + 3,

   /**
   * \brief The DAP SDK license provided in the call to
   * VNCDAPClientAddLicense() is not valid.
   */
   e32MLDAP_ERROR_LICENSENOTVALID = e32MLDAP_ERROR_LOCAL + 4,

   /**
   * \brief The DAP SDK is not licensed to perform the requested operation.
   *
   * This could be because:
   *
   *  - The application has not called VNCDAPClientAddLicense()
   *  - The license added with VNCDAPClientAddLicense() has expired
   *  - The license added with VNCDAPClientAddLicense() does not grant
   *    license to perform the requested operation
   */
   e32MLDAP_ERROR_FEATURE_NOTLICENSED = e32MLDAP_ERROR_LOCAL + 5,

   /**
   * \brief The application called VNCDAPClientAttestationRequest() before it
   * made a successful call to VNCDAPClientSetServerURL().
   */
   e32MLDAP_ERROR_NO_SERVERURL = e32MLDAP_ERROR_LOCAL + 6,

   /**
   * \brief The application called VNCDAPClientAttestationRequest() before it
   * made a successful call to VNCDAPClientSetTrustRoot().
   */
   e32MLDAP_ERROR_NO_TRUSTROOT = e32MLDAP_ERROR_LOCAL + 7,

   /**
   * \brief An OS error occurred while communicating with the DAP server.
   */
   e32MLDAP_ERROR_NETWORKERROR = e32MLDAP_ERROR_LOCAL + 8,

   /**
   * \brief The DAP server returned an &lt;attestationResponse&gt; that could
   * not be parsed.
   *
   * This could be because:
   *
   *  - The XML is not well-formed
   *  - The XML is not a valid &lt;attestationResponse&gt;
   *  - A CDATA element that should contain Base-64-encoded data (e.g.
   *    &lt;deviceCertificate&gt;) does not contain valid Base-64-encoded
   *    data
   */
   e32MLDAP_ERROR_INVALID_ATTESTATION_RESPONSE = e32MLDAP_ERROR_LOCAL + 9,

   /**
   * \brief An X.509 certificate is not valid.
   *
   * When returned by VNCDAPClientSetTrustRoot(), this error indicates that
   * the data provided by the application is not a valid X.509 certificate.
   *
   * When returned by VNCDAPClientAttestationRequest(), this error indicates
   * that the DAP server returned a device certificate or a manufacturer
   * certificate that is not a valid X.509 certificate.
   */
   e32MLDAP_ERROR_INVALID_CERTIFICATE = e32MLDAP_ERROR_LOCAL + 10,

   /**
   * \brief The DAP server returned an &lt;attestationResponse&gt; that was
   * syntactically valid, and which contained valid X.509 certificates, but
   * which failed one or more validation checks.
   *
   * This could be because:
   *
   *  - The device certificate does not have the required
   *    tcp-kp-AIKCertificate Extended Key Usage X.509 v3 extension
   *  - A manufacturer certificate does not have the CA:TRUE Basic
   *    Constraints X.509 v3 extension
   *  - Either the device certificate or a manufacturer certificate has
   *    expired
   *  - The device certificate and the manufacturer certificates do not form
   *    a chain of trust that ends in the trust root provided by the last
   *    successful call to VNCDAPClientSetTrustRoot()
   *  - A &lt;quoteSignature&gt; contained in an &lt;attestation&gt; is not a
   *    valid SHA1-with-RSA signature over the corresponding
   *    &lt;quoteInfo&gt;
   *  - The application specified a particular component ID (i.e. not
   *    ::VNCDAPComponentAll) in its call to
   *    VNCDAPClientAttestationRequest(), but that component ID is not
   *    attested in the response from the DAP server
   *
   * Check the log for a detailed desription of the failure.
   */
   e32MLDAP_ERROR_SERVER_NOTTRUSTED = e32MLDAP_ERROR_LOCAL + 11,

   /**
   * \brief The configured timeout elapsed before a complete
   * &lt;attestationResponse&gt; was received from the server.
   *
   * To configure the timeout for attestation requests, use
   * VNCDAPClientSetTimeout().
   */
   e32MLDAP_ERROR_TIMEDOUT = e32MLDAP_ERROR_LOCAL + 12,

   /**
   * unknown error
   */
   e32MLDAP_UNKNOWNERROR = 99

} tenMLDAPError;

/** \brief An array of bytes. */
struct trMLDAPByteArray
{
      /** \brief Pointer to the start of the array. */
      const t_U8 *pcou8Data;

      /** \brief Length of the array. */
      size_t siLength;

//! Initialise structure members to 0
      trMLDAPByteArray() :
         pcou8Data(NULL), siLength(0)
      {

      }
} ;
/**
 * \brief A single &lt;attestation&gt; in a parsed &lt;attestationResponse&gt;.
 */
struct trMLDAPAttestation
{
   /**
   * \brief The ID of the MirrorLink component that is attested by this
   * attesation.
   */
   const t_Char *pcoczComponentId;

   /** \brief The MirrorLink server's URL for this component. */
   const t_Char *pcoczUrl;

   /** \brief The application public key for this component. */
   trMLDAPByteArray rApplicationPublicKey;

      /** \brief The application public key for this component, Base-64 encoded. */
      const t_Char *pcoczApplicationPublicKeyBase64;
//! Initialise structure members to 0
      trMLDAPAttestation() :
         pcoczComponentId(NULL), pcoczUrl(NULL),
               pcoczApplicationPublicKeyBase64(NULL)
      {
      }

} ;

/** \brief A parsed &lt;attestationResponse&gt;. */
struct trMLDAPAttestationResponse
{
   /**
   * \brief Array of pointers to received &lt;attestation&gt;s.
   */
   const trMLDAPAttestation *pcorAttestations;

   /**
   * \brief Size of the attestations array.
   */
   size_t siAttestationCount;

   /**
   * \brief Received device certificate, as a DER-encoded byte array (the SDK
   * performs the Base-64 decoding).
   *
   * As part of VNCDAPClientAttestationRequest(), the SDK validates the
   * device certificate using the manufacturer certificates and the trust
   * root. It is not necessary for the application to repeat this step.
   */
   trMLDAPByteArray rDeviceCertificate;

   /**
   * \brief Array of pointers to received manufacturer certificates, each of
   * which is a DER-encoded byte array (the SDK performs the Base-64
   * decoding).
   */
   trMLDAPByteArray *prManufacturerCertificates;

      /**
       * \brief Size of the manufacturerCertificates array.
       */
      size_t siManufacturerCertificateCount;

//! Initialise structure members to 0
      trMLDAPAttestationResponse() :
         pcorAttestations(NULL), siAttestationCount(0),
               prManufacturerCertificates(NULL),
               siManufacturerCertificateCount(0)
      {

      }

} ;

typedef enum
{
   e8MLATTESTATION_SUCCESSFUL = 0,
   e8MLCOMPONENT_NOTEXISTING = 1,
   e8MLVERSION_NOTSUPPORTED = 2,
   e8MLUNKNOWN_TRUSTROOT = 3,
   e8MLUNKNOWN_COMPONENTID = 4,
   e8MLATTESTATION_FAILED = 5,
   e8MLATTESTATION_DIFFERENT_URL = 6,
   e8MLXML_VALIDATION_SUCCESSFUL = 7,
   e8MLXML_VALIDATION_FAILED =8
} tenMLAttestationResult;

typedef enum
{
   e8TERMINALMODE_VNCSERVER = 0,
   e8TERMINALMODE_UPNPSERVER = 1,
   e8TERMINALMODE_RTPSERVER = 2,
   e8TERMINALMODE_RTPCLIENT = 3,
   e8TERMINALMODE_CDBENDPOINT = 4,
   e8TERMINALMODE_MLDEVICE = 5,
   e8TERMINALMODE_UNKNOWN = 99
} tenMLComponentID;

struct trMLVersion
{
      t_U32 u32MajorVersion;
      t_U32 u32MinorVersion;
      t_U32 u32PatchVersion;
//! Initialise structure members to 0
      trMLVersion() :
         u32MajorVersion(0), u32MinorVersion(0), u32PatchVersion(0)
      {

      }

};

/*****************************************************
**************VIEWER*********************************
*****************************************************/

/**
* \brief Describes a point in two dimensions.
*
* The origin, (0, 0), is the point at the top left of the framebuffer.
*
* \see MLRectangle
*/
struct trMLPoint
{
   /** The X-coordinate of the point. */
   t_U16 u16x;
   /** The Y-coordinate of the point. */
   t_U16 u16y;
   //! Initialise structure members to 0
   trMLPoint() :
   u16x(0), u16y(0)
   {
   }
   //Assignment operator
   trMLPoint& operator=(const trMLPoint& corfrSrc)
   {
      if( &corfrSrc != this)
      {
         u16x = corfrSrc.u16x;
         u16y = corfrSrc.u16y;
      }
      return *this;
   }
} ;

/**
 * \brief Describes a rectangular region.
 *
 * The rectangle contains the pixels from topLeft inclusive through to
 * bottomRight non-inclusive.  That is, there are
 * (bottomRight.x - topLeft.x) * (bottomRight.y - topLeft.y) pixels in the
 * rectangle.
 *
 * \see MLPoint
 */
struct trMLRectangle
{
      /** The point inside the rectangle at its top-left corner. */
      trMLPoint rTopLeft;
      /** The point just outside the rectangle at its bottom-right corner. */
      trMLPoint rBottomRight;
//! Default Constructor
      trMLRectangle()
      {

      }
      //Assignment operator
      trMLRectangle& operator=(const trMLRectangle& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            rTopLeft = corfrSrc.rTopLeft;
            rBottomRight = corfrSrc.rBottomRight;
         }
         return *this;
      }

} ;

/**
* \brief Constants which define the reason
* for blocking a framebuffer
*
* Use a combination of these values to inform the server why the viewer
* application has chosen to block an application's visual content.  For
* example, the viewer application may have decided (based on the application's
* category and content category) that the application does not comply with the
* current driver distraction policy, or the viewer application may not be
* visible to the end user at this time.
*
*/
typedef enum
{
   /**
   * The application's content category has been disallowed (e.g. by the
   * driver distraction policy).
   */
   e16MLCONTENTCATEGORY_NOTALLOWED = 0X0001,
   /**
   * The application category has been disallowed (e.g. by the driver
   * distraction policy).
   */
   e16MLAPPLICATIONCATEGORY_NOTALLOWED = 0X0002,
   /**
   * The server's trust in the content category that it reported is not
   * sufficient to satisfy the viewer application.
   */
   e16MLCONTENT_NOTTRUSTED = 0X0004,
   /**
   * The server's trust in the application category that it reported is not
   * sufficient to satisfy the viewer application.
   */
   e16MLAPPLICATION_NOTTRUSTED = 0X0008,
   /**
   * The server application has not followed the content rules that were
   * communicated to the server via UPnP.
   */
   e16MLCONTENTRULES_NOTFOLLOWED = 0X0010,
   /** The server application has been disallowed based on its unique ID. */
   e16MLAPPLICATION_UNIQUEID_NOTALLOWED = 0X0020,
   /** The viewer application is not in focus. */
   e16MLUI_NOT_INFOCUS = 0X0100,
   /** The UI of the viewer application is not visible to the user. */
   e16MLUI_NOTVISIBLE = 0X0200,
} tenMLFramebufferBlockReason;

/**
* \brief Constants which define the reason
* for blocking audio
*
* Use a combination of these values to inform the server why the viewer
* application has chosen to block the server's audio content.
*
*/
typedef enum
{
   e16MLAUDIO_UNBLOCK = 0x0000,
   /**
   * The application's category has been disallowed (e.g. by the driver
   * distraction policy).
   */
   e16MLAUDIO_APPCATEGORY_NOTALLOWED = 0X0001,
   /**
   * The server's trust in the application category that it reported is not
   * sufficient to satisfy the viewer application.
   */
   e16MLAUDIO_APP_NOTTRUSTED = 0X0002,
   /** The server application has been disallowed based on its unique ID. */
   e16MLAUDIO_APP_UNIQUEID_NOTALLOWED = 0X0004,
   /** The user has muted all audio. */
   e16MLGLOBALLY_MUTED = 0X0008,
   /** The user has muted a particular audio stream. */
   e16MLSTREAM_MUTED = 0X0010,
} tenMLAudioBlockReason;

/**
* \brief Describes the level of trust that the MirrorLink server has in the
* Context Information that it is providing.
*
* For further information, see Appendix B, 'Application Context Information',
* in the MirrorLink specification.
*
* \see MLContextInformation
*/
typedef enum
{
   /** The server has no trust in the reported information. */
   e16UNKNOWN = 0X00,
   /** The provided data is under the control of the user. */
   e16USER_CONFIGURATION = 0X40,
   /** The provided data is under the control of the application. */
   e16SELF_REGISTERED_APPLICATION = 0X60,
   /**
   * The provided data is under sole control of the ML and UPnP server.
   * The application is known to them and has been uniquely identified.
   */
   e16REGISTERED_APPLICATION = 0X80,
   /**
   * The provided data is under sole control of the ML and UPnP server.
   * The data is derived from a valid application certificate.
   */
   e16APPLICATION_CERTIFICATE = 0XA0
} tenMLTrustLevel;

/**
* \brief Structure holding a decoded Context Information rectangle that has
* been received from the server.
*
*/
struct trMLContextInfo
{
   /**
   * The unique ID of the application that has drawn to the relevant part of
   * the framebuffer.
   */
   t_U32 u32ApplicationUniqueId;
   /**
   * The server's level of trust that the information in the
   * applicationCategory field is correct.
   */
   tenMLTrustLevel enApplicationCategoryTrustLevel;
   /**
   * The server's level of trust that the information in the contentCategory
   * field is correct.
   */
   tenMLTrustLevel enContentCategoryTrustLevel;
   /**
   * The category and sub-category into which the application falls.
   */
   tenAppCategory enApplicationCategory;
   /**
   * The category of the content that the application iCRCBWrappers presenting.
   */
   tenAppDisplayCategory enContentCategory;
   /**
   * A bit-field of the content rules, negotiated in the MirrorLink UPnP
   * stream, with which the application has complied.
   */
   t_U32 u32ContentRulesFollowed;
};

struct trMLServerEventConfiguration
{
      /**
       * ISO 639-1 language code for the server's keyboard layout.
       *
       * The SDK normalizes the case of this field so that the value is always
       * lowercase.
       */
      t_Char czKeyboardLanguage[ARR_SIZE];
      /**
       * ISO 3166-1 country code for the server's keyboard layout.
       *
       * For example, if the server has an American keyboard layout, then
       * keyboardLanguage will be 'en' and keyboardCountry will be 'US'.
       *
       * The SDK normalizes the case of this field so that the value is always
       * uppercase.
       */
      t_Char czKeyboardCountry[ARR_SIZE];
      /**
       * ISO 639-1 language code for the server's user interface language.
       *
       * The SDK normalizes the case of this field so that the value is always
       * lowercase.
       */
      t_Char czUiLanguage[ARR_SIZE];
      /**
       * ISO 3166-1 country code for the server's user interface language.
       *
       * For example, if the server's user interface is US English, then
       * uiLanguage will be 'en' and uiCountry will be 'US'.
       *
       * The SDK normalizes the case of this field so that the value is always
       * uppercase.
       */
      t_Char czUiCountry[ARR_SIZE];
      /**
       * Indicates the server's support for knob key input.
       *
       * The value is a combination of the values in the \ref VNCKnobKeySupport
       * enumeration.  For example, if the VNCKnobKeySupport0PushZ bit is set,
       * then the server will respond to KeyEvents of type
       * XK_Knob_2D_shift_push(0).
       */
      t_U32 u32knobKeySupport;
      /**
       * Indicates the server's support for MirrorLink device key input.
       *
       * The value is a combination of the values in the \ref VNCDeviceKeySupport
       * enumeration.  For example, if the VNCDeviceKeySupportSoftLeft bit is
       * set, then the server will respond to KeyEvents of type
       * XK_Device_Soft_left.
       */
      t_U32 u32deviceKeySupport;
      /**
       * Indicates the server's support for multimedia key input.
       *
       * The value is a combination of the values in the
       * \ref VNCMultimediaKeySupport enumeration.  For example, if the
       * VNCMultimediaKeySupportPlay bit is set, then the server will respond to
       * KeyEvents of type XK_Multimedia_Play.
       */
      t_U32 u32multimediaKeySupport;
      /**
       * Indicates the server's support for miscellaneous MirrorLink key
       * input.
       *
       * The value is a combination of the values in the \ref VNCMiscKeySupport
       * enumeration.  For example, if the VNCMiscKeySupportITU bit is
       * set, then the server will respond to KeyEvents with ITU keypad key
       * symbols (XK_ITU_Key_0 etc).
       */
      t_U32 u32miscKeySupport;
      /**
       * Indicates the server's support for pointer / touchscreen input.
       *
       * The value is a combination of the values in the \ref VNCPointerSupport
       * enumeration.  For example, if the VNCPointerSupportPointerButton1 bit
       * is set, then the server will respond to PointerEvents with the
       * VNCPointerDeviceButtonLeft bit set.
       */
      t_U32 u32pointerSupport;
//! Initialise structure members to 0
      trMLServerEventConfiguration():u32knobKeySupport(0),u32deviceKeySupport(0),u32multimediaKeySupport(0),
                               u32miscKeySupport(0),u32pointerSupport(0)
  {
     memset(czKeyboardLanguage,0,ARR_SIZE);
     memset(czKeyboardCountry,0,ARR_SIZE);
     memset(czUiLanguage,0,ARR_SIZE);
     memset(czUiCountry,0,ARR_SIZE);
  }

} ;

typedef trMLServerEventConfiguration trMLClientEventConfiguration;

struct trMLServerDisplayConfiguration
{
   /**
   * The major version number of the MirrorLink specification followed by
   * the server.
   */
   t_Char cserverMajorVersion;
   /**
   * The minor version number of the MirrorLink specification followed by
   * the server.
   */
   t_Char cserverMinorVersion;
   /**
   * A combination of \ref VNCFramebufferConfiguration values describing the
   * server's capabilities with respect to its framebuffer.
   */
   t_U16 u16framebufferConfiguration;
   /**
   * The width of each pixel in the server display relative to its height.
   *
   * This has no effect on the Viewer SDK, but you may wish to take note of
   * it when displaying the framebuffer to the user.
   */
   t_U16 u16relativePixelWidth;
   /**
   * The height of each pixel in the server display relative to its width.
   *
   * This has no effect on the Viewer SDK, but you may wish to take note of
   * it when displaying the framebuffer to the user.
   */
   t_U16 u16relativePixelHeight;
   /**
   * A combination of \ref VNCPixelFormatSupport values that detail the
   * server's capabilities with respect to its framebuffer.
   */
   t_U32 u32pixelFormatSupport;

//! Initialise structure members to 0
   trMLServerDisplayConfiguration() :
         cserverMajorVersion(0), cserverMinorVersion(0),
               u16framebufferConfiguration(0), u16relativePixelWidth(0),
               u16relativePixelHeight(0), u32pixelFormatSupport(0)
      {
      }

} ;

struct trMLClientDisplayConfiguration
{
   /**
   * The major version number of the MirrorLink specification followed by
   * the client.  This may not exceed the major version provided by the
   * server.
   *
   * Pre-filled to 1.
   */
   t_U8 u8clientMajorVersion;
   /**
   * The major version number of the MirrorLink specification followed by
   * the client.  This may not exceed the minor version provided by the
   * server.
   *
   * Pre-filled to 0.
   */
   t_U8 u8clientMinorVersion;
   /**
   * A combination of \ref VNCFramebufferConfiguration values describing
   * which of the server's advertised framebuffer capabilities it intends to
   * use.
   *
   * Pre-filled to
   * (VNCFramebufferConfigurationUpScaling |
   *  VNCFramebufferConfigurationDownScaling),
   * with the addition of
   * VNCFramebufferConfigurationServerSideOrientationSwitch if the server has
   * advertised that it supports it.
   */
   t_U16 u16framebufferConfiguration;
   /**
   * The width in pixels of the area in which the viewer application will
   * display the framebuffer.
   *
   * Pre-filled to the width of the server display in pixels.  However, you
   * should always override this with the actual width of the viewer-side
   * display, if at all possible.
   */
   t_U16 u16clientDisplayWidthPixels;
   /**
   * The height in pixels of the area in which the viewer application will
   * display the framebuffer.
   *
   * Pre-filled to the height of the server display in pixels.  However, you
   * should always override this with the actual height of the viewer-side
   * display, if at all possible.
   */
   t_U16 u16clientDisplayHeightPixels;
   /**
   * The width in millimeters of the area in which the viewer application
   * will display the framebuffer.
   *
   * Pre-filled to 0.
   */
   t_U16 u16clientDisplayWidthMillimeters;
   /**
   * The height in millimeters of the area in which the viewer application
   * will display the framebuffer.
   *
   * Pre-filled to 0.
   */
   t_U16 u16clientDisplayHeightMillimeters;
   /**
   * The expected distance between the viewer display and the user in
   * millimeters.
   *
   * In-car applications may be able to provide the server with a good
   * estimate for this value.
   *
   * Pre-filled to 0.
   */
   t_U16 u16clientDistanceFromUserMillimeters;

   /**
   * A combination of \ref VNCPixelFormatSupport values that detail the
   * pixel formats that are supported by the client for use with the
   * Transform encoding.
   *
   * The application should not normally alter the value of this field.
   */
   t_U32 u32pixelFormatSupport;

   /**
   * A combination of \ref VNCResizeFactor values that detail the resize
   * factores that are supported by the client for use with the Transform
   * encoding.
   *
   * The application should not normally alter the value of this field.
   */
   t_U32 u32resizeFactors;

//! Initialise structure members to 0
   trMLClientDisplayConfiguration() :
         u8clientMajorVersion(0), u8clientMinorVersion(0),
                  u16framebufferConfiguration(0),
                  u16clientDisplayWidthPixels(0),
               u16clientDisplayHeightPixels(0),
               u16clientDisplayWidthMillimeters(0),
               u16clientDisplayHeightMillimeters(0),
               u16clientDistanceFromUserMillimeters(0),
               u32pixelFormatSupport(0), u32resizeFactors(0)
      {
      }

} ;

struct trMLPixelFormat
{
   /**
   * The number of bits of pixel data used for each pixel.  Must be either 8,
   * 16, or 32.
   *
   * Your application's framebuffer must be aligned so that the address of
   * each pixel is a multiple of (bitsPerPixel / 8).
   */
   t_U8 u8bitsPerPixel;

   /**
   * The number of bits in each pixel that are significant (i.e. the number
   * of colors that can be represented).  Must be less than or equal to
   * bitsPerPixel.  (For example, if 32 bits are used to represent pixels
   * that have 8 bits each for red, green and blue, then bitsPerPixel is 32
   * and depth is 24.)
   *
   * Non-signficiant bits are cleared by the SDK.
   */
   t_U8 u8depth;

   /**
   * If non-zero, then the pixels are big-endian.  Otherwise, they are
   * little-endian.
   */
   t_U8 u8bigEndianFlag;

   /**
   * If non-zero, then the pixel data contains actual pixel values.
   * Otherwise, the pixel data contains indices into the accompanying color
   * map.
   */
   t_U8 u8trueColorFlag;

   /**
   * The maximum value of the red component of each pixel.
   */
   t_U16 u16redMax;

   /**
   * The maximum value of the green component of each pixel.
   */
   t_U16 u16greenMax;

   /**
   * The maximum value of the blue component of each pixel.
   */
   t_U16 u16blueMax;

   /**
   * The shift value of the red component of each pixel.
   */
   t_U8 u8redShift;

   /**
   * The shift value of the green component of each pixel.
   */
   t_U8 u8greenShift;

   /**
   * The shift value of the blue component of each pixel.
   */
   t_U8 u8blueShift;

//! Initialise structure members to 0
   trMLPixelFormat() :
         u8bitsPerPixel(0), u8depth(0), u8bigEndianFlag(0), u8trueColorFlag(0),
               u16redMax(0), u16greenMax(0), u16blueMax(0), u8redShift(0),
               u8greenShift(0), u8blueShift(0)
      {
      }
};

//! Session Status enumerations
enum tenSessionProgress
{
   e16_SESSION_PROGRESS_NONE = 0x0000,
   //!viewer thread is about to start listening for an incoming connection. 
   e16_SESSION_PROGRESS_LISTENING = 0x0100,
   //! viewer thread is about to perform a name lookup (e.g. using DNS)
   e16_SESSION_PROGRESS_PERFORMING_NAME_LOOKUP = 0x0200,
   //! viewer thread is about to start connecting outwards.
   e16_SESSION_PROGRESS_CONNECTING = 0x0300,
   //! viewer thread is about to start negotiating with an HTTP or SOCKS proxy.
   e16_SESSION_PROGRESS_NEGOTIATING_WITH_PROXY = 0x0400,
   //! viewer thread is about to start negotiating with a VNC Data Relay. 
   e16_SESSION_PROGRESS_NEGOTIATING_WITH_DATARELAY = 0x0500 ,
   //! viewer thread is about to start negotiating with Barry. 
   e16_SESSION_PROGRESS_NEGOTIATING_WITH_BARRY = 0x0510,
   //! viewer thread is about to start negotiating with adb (the Android Debug Bridge).
   e16_SESSION_PROGRESS_NEGOTIATING_WITH_ADB = 0x0520,
   //! viewer thread has finished negotiating with a VNC Data Relay
   e16_SESSION_PROGRESS_WAITING_FOR_VNCSERVER = 0x0600,
   //! viewer thread has established a connection to the VNC Server and the RFB protocol begins.
   e16_SESSION_PROGRESS_NEGOTIATING_WITH_VNCSERVER = 0x0700,
   //! Notified if one or more VNCViewerSkinFetchCallback()s has been registered
   e16_SESSION_PROGRESS_FETCHINGSKIN = 0x0800,
   //!  normal RFB handshake is complete, and the viewer is negotiating MirrorLink features with the server
   e16_SESSION_PROGRESS_ML_HANDSHAKE = 0x0900,
   //the session is fully established
   e16_SESSION_PROGRESS_SESSION_ESTABLISHED = 0xf000
};

/********************Viewer Errors***********************************/ //////////////
/**
* Placeholder value for variable initialization.
*/
static const t_S32 cs32MLViewerErrorNone = 0;

/**
* The server hostname could not be resolved.
*/
static const t_S32 cs32MLViewerErrorDNSFailure = 1;

/**
* The server network is unreachable.
*/
static const t_S32 cs32MLViewerErrorNetworkUnreachable = 2;

/**
* The server host is unreachable.
*/
static const t_S32 cs32MLViewerErrorHostUnreachable = 3;

/**
* The connection attempt timed out.
*/
static const t_S32 cs32MLViewerErrorConnectionTimedOut = 4;

/**
* The server host refused a TCP connection.
*/
static const t_S32 cs32MLViewerErrorConnectionRefused = 5;

/**
* The bearer successfully established a connection to the server, but the
* the RFB version offered by the VNC Server is not supported.
*/
static const t_S32 cs32MLViewerErrorUnsupportedProtocolVersion = 6;

/**
* The credentials provided via MLViewerAsynchronousCredentialsResult()
* are incorrect (or you did not provide a
* VNCViewerAsynchronousCredentialsCallback at all).
*/
static const t_S32 cs32MLViewerErrorAuthenticationFailure = 7;

/**
* Your VNCViewerAsynchronousVerifyServerIdentityResult() rejected the
* server public key.
*/
static const t_S32 cs32MLViewerErrorServerIdentityVerificationFailed = 8;

/**
* The connection to the server was lost.
*/
static const t_S32 cs32MLViewerErrorDisconnected = 9;

/**
* VNCViewerReset() was called (either explicitly or from
* VNCViewerDestroy()).
*/
static const t_S32 cs32MLViewerErrorReset = 11;

/**
* The server gracefully closed the RFB session.
*/
static const t_S32 cs32MLViewerErrorConnectionClosed = 12;

/**
* Attempted to listen on a TCP port number that was already in use.
*/
static const t_S32 cs32MLViewerErrorAddressInUse = 13;

/**
* The security type that has been negotiated requires an RSA key pair.
*/
static const t_S32 cs32MLViewerErrorRSAKeysRequired = 14;

/**
* The Viewer SDK license does not grant permission to connect to this VNC
* server.
*/
static const t_S32 cs32MLViewerErrorNotLicensedForServer = 15;

/**
* You called a viewer method that may not be called before the
* VNCViewerServerInitCallback() is notified.
*/
static const t_S32 cs32MLViewerErrorIllegalBeforeServerInit = 16;

/**
* A protocol error occurred when interacting with a VNC Data Relay.
*/
static const t_S32 cs32MLViewerErrorDataRelayProtocolError = 17;

/**
* The command string passed to VNCViewerProcessCommandString() has an
* invalid format.
*/
static const t_S32 cs32MLViewerErrorInvalidCommandString = 18;

/**
* The Data Relay reported that the session ID in the channel details
* string passed to VNCViewerProcessCommandString() is not valid
* (probably because the channel lease has already expired).
*/
static const t_S32 cs32MLViewerErrorUnknownDataRelaySessionId = 19;

/**
* The Data Relay channel lease expired while waiting for the server to
* connect to the Data Relay.
*/
static const t_S32 cs32MLViewerErrorDataRelayChannelTimeout = 20;

/**
* The VNC Server did not offer any of the security types that were
* configured with VNCViewerSetSecurityTypes().
*/
static const t_S32 cs32MLViewerErrorNoMatchingSecurityTypes = 21;

/**
* A parameter supplied to a Viewer SDK API call is not valid.
*/
static const t_S32 cs32MLViewerErrorInvalidParameter = 23;

/**
* Either the mobile device skin file could not be opened, or it is not a
* valid zip archive.
*
* If you encounter this error during mobile device skin development, it is
* recommended that you enable Viewer SDK logging to determine the cause.
*
* \see VNCViewerSetSkin(), VNCParameterSkinDTD, VNCParameterLog,
* VNCViewerLogCallback()
*/
static const t_S32 cs32MLViewerErrorInvalidSkinArchive = 24;

/**
* \brief There is an error in the skin XML in the mobile device skin.
*
* This may indicate that:
*
*  - the skin XML is not well-formed
*  - the skin XML is not valid according to the configured DTD
*  - the skin DTD itself is not valid
*  - the skin XML is well-formed and valid, but not semantically correct
*    for use as part of a mobile device skin
*
* If you encounter this error during mobile device skin development, it is
* recommended that you enable Viewer SDK logging to determine the cause.
*
* \see VNCViewerSetSkin(), VNCParameterSkinDTD, VNCParameterLog,
* VNCViewerLogCallback()
*/
static const t_S32 cs32MLViewerErrorInvalidSkinXml = 25;

/**
* An image in a mobile device skin archive could not be loaded.
*/
static const t_S32 cs32MLViewerErrorInvalidSkinImage = 26;

/**
* \brief General non-specific failure code.
*
* Where-ever possible, the Viewer SDK attempts to provide a more specific
* error code.  If you do encounter this error, it is recommended that you
* enable Viewer SDK logging to determine the cause.
*
* \see VNCParameterLog, VNCViewerLogCallback()
*/
static const t_S32 cs32MLViewerErrorFailed = 27;

/**
* Permission was denied by the operating system.
*/
static const t_S32 cs32MLViewerErrorPermissionDenied = 28;

/**
* The command string selected a bearer that could not be loaded.
*
* More detailed information can be found in the log.
*
* Often this means that the viewer SDK library was simply unable to find
* the bearer libraries. You can explicitly provide a directory for it
* to search, using VNCViewerSetParameter and VNCParameterBearerDirectory.
*
* \see VNCParameterLog, VNCViewerLogCallback(), VNCViewerSetParameter(),
* VNCParameterBearerDirectory
*/
static const t_S32 cs32MLViewerErrorBearerLoadFailed = 29;

/**
* This API function is not implemented for the current viewer
* platform.
*
* \see VNCParameterLog, VNCViewerLogCallback()
*/
static const t_S32 cs32MLViewerErrorNotImplemented = 30;

/**
* The bearer failed to establish a connection due to an authentication
* failure.
*
* Note that this error indicates a transport-level failure, rather than an
* application-level failure.  That is, the failure originates from within
* a pluggable bearer implementation, rather than with the Viewer SDK.
*/
static const t_S32 cs32MLViewerErrorBearerAuthenticationFailed = 31;

/**
* The specified device is not connected via USB, or the VNC Server on
* the device is not ready to begin a USB connection.
*/
static const t_S32 cs32MLViewerErrorUSBNotConnected = 32;

/**
* The bearer was unable to an load an underlying software library (e.g.
* OEM software for driving a particular type of communications), and
* cannot proceed without it.
*/
static const t_S32 cs32MLViewerErrorUnderlyingLibraryNotFound = 33;

/**
* The application called an API that may not be called while the viewer
* thread is running.
*/
static const t_S32 cs32MLViewerErrorIllegalWhileRunning = 34;

/**
* The application called an API that may not be called unless the viewer
* thread is running.
*/
static const t_S32 cs32MLViewerErrorIllegalWhileNotRunning = 35;

/**
* An extension message cannot be sent because the server has not enabled
* the extension.
*/
static const t_S32 cs32MLViewerErrorExtensionNotEnabled = 36;

/**
* The operation is not supported by the version of RFB that has been
* negotiated for the session.
*/
static const t_S32 cs32MLViewerErrorNoProtocolSupport = 37;

/**
* The operation failed because the thing being added or registered already
* exists.
*/
static const t_S32 cs32MLViewerErrorAlreadyExists = 38;

/**
* The operation failed because there is insufficient space available to
* buffer the requested data.
*/
static const t_S32 cs32MLViewerErrorInsufficientBufferSpace = 39;

/**
* Reserved for future use.
*/
static const t_S32 cs32MLViewerErrorReserved1 = 40;

/**
* A license could not be added because it is not valid.
*/
static const t_S32 cs32MLViewerErrorLicenseNotValid = 41;

/**
* A feature of the SDK cannot be used because it is not licensed.
*/
static const t_S32 cs32MLViewerErrorFeatureNotLicensed = 42;

/**
* The bearer successfully established a connection to the server; but the
* server is using some protocol other than RFB (i.e. it is not a VNC
* Server).
*/
static const t_S32 cs32MLViewerErrorNotAnRFBServer = 43;

/**
* The port specified by the 'p' field of the VNC Command String does not
* represent a valid port.
*/
static const t_S32 cs32MLViewerErrorBadPort = 44;

/**
* There was an error reading an RFB packet
*/
static const t_S32 cs32MLViewerErrorRFBProtocolParsingError = 45;

/**
* A static configuration required by the bearer has not been provided.
*/
static const t_S32 cs32MLViewerErrorBearerConfigurationNotProvided = 46;

/**
* A static configuration provided for the bearer is invalid.
*/
static const t_S32 cs32MLViewerErrorBearerConfigurationInvalid = 47;

/**
* An operation was attempted for which the MirrorLink VNC Server public
* key is required, but VNCParameterMirrorLinkVNCServerPublicKey was not
* set.
*
* The MirrorLink VNC Server public key should be obtained using MirrorLink
* Device Attestation Protocol for the TerminalMode:VNC-Server component.
* VNC DAP SDK may be used to perform this step.
*/
static const t_S32 cs32MLViewerErrorMirrorLinkMLServerPublicKeyRequired = 48;

/**
* MirrorLink content attestation failed.
*/
static const t_S32 cs32MLViewerErrorContentAttestationFailed = 49;

typedef t_S32 MLViewerError;


/**
* \typedef FrameBuffer Configuration enumeration
*/
typedef enum
{
   /**
   *The server is capable of switching orientations, as instructed by an 
   *appropriate DeviceStatusRequest message
   */
   MLFramebufferConfigurationServerSideOrientationSwitch        = 0x0001,
   /**
   *The server is capable of rotating its framebuffer, as instructed by 
   *an appropriate DeviceStatusRequest message. 
   */
   MLFramebufferConfigurationServerSideRotation                 = 0x0002,
   /**
   *The server is capable of upscaling its framebuffer if its own dimensions are 
   *less than those of the client display
   */
   MLFramebufferConfigurationUpScaling                          = 0x0004,
   /**
   *The server is capable of downscaling its framebuffer if its own 
   *dimensions are greater than those of the client display
   */
   MLFramebufferConfigurationDownScaling                        = 0x0008,
   /**
   *The server is capable of suppressing empty update rectangles, so that 
   *the viewer application does not have to deal with them.
   */
   MLFramebufferConfigurationReplaceEmptyUpdates                = 0x0010,
   /**
   *The server supports FramebufferAlternativeText messages
   */
   MLFramebufferConfigurationSupportsFramebufferAlternativeText = 0x0020
}tenFrameBufferConfiguration;

/**
* \typedef Content attestation Flag enumeration
*/
typedef enum
{
   //!disable content Attestation
   e8ATTEST_DISABLE = 0x00,
   //! Attest Application Context Info 
   e8ATTEST_CONTEXT_INFO = 0x01,
   //! Attest Frame buffer updates
   e8ATTEST_FRAMEBUFFER_UPDATES = 0x02,
   //! Attest updated Pixels
   e8ATTEST_UPDATED_PIXELS = 0x04
} tenContentAttestationFlag;

/**
* \typedef Content attestation failure reason enumeration
*/
typedef enum
{
   //! Content Attestation is not implemented in the ML server
   e32MLContentAttestationFailureErrorNotImplemented = 0x00000001, 
   //! No session Key is provided. It indicates a bug in server implementation
   e32MLContentAttestationFailureErrorNoSessionKey = 0x00000002, 
   //!ML Server returned other error
   e32MLContentAttestationFailureErrorOther = 0x00000004, 
   //! ML server returned Unrecognized error
   e32MLContentAttestationFailureErrorUnknown = 0x00000008,
   //! Content Attestation response was not received in a timely manner
   e32MLContentAttestationFailureNoResponse = 0x00010000, 
   //! Attestation Response was received but not signed
   e32MLContentAttestationFailureNoSignature = 0x00020000, 
   //! Attestation response doesn't contain hash of frame buffer
   e32MLContentAttestationFailureFramebufferNotAttested = 0x00040000, 
   //! Attestation response doesn't contain hash of Context information
   e32MLContentAttestationFailureContextInformationNotAttested = 0x00080000,
   //! Attestation response doesn't contain the number of updated pixels
   e32MLContentAttestationFailurePixelCountNotAttested = 0x00100000
}tenContentAttestationFailureReason;

/*****************************************************
********************DATA SERVICE**********************
*****************************************************/
/**
* \brief Defines the type of data service
*
* CDBService is low level data exchange protocol
* which is abstracted and provided as SBPService.
* SBPService must be preferred for implementing
* data service
*/
typedef enum
{
   e8MLCDB_SERVICE = 0, e8MLSPB_SERVICE = 1
} tenMLServiceType;

//!UID's of GPS data
static const t_U32 cou32GPS_NMEA_OBJECT_UID               = 0x0aac4540;
static const t_U32 cou32GPS_NMEA_DATA_FIELD_UID           = 0x144a776f;
static const t_U32 cou32GPS_NMEA_TIMESTAMP_FIELD_UID      = 0x59413fd1;
static const t_U32 cou32GPS_NMEA_DESC_OBJECT_UID          = 0x9d08b19d;
static const t_U32 cou32GPS_NMEA_DESC_SUPP_SENTENCES_UID  = 0x6e72b167;

//!GPS NMEA sentence types
static const t_U32 cou32GPS_SUPPORT_GGA = 0x00000001;
static const t_U32 cou32GPS_SUPPORT_GLL = 0x00000002;
static const t_U32 cou32GPS_SUPPORT_GSA = 0x00000004;
static const t_U32 cou32GPS_SUPPORT_GSV = 0x00000008;
static const t_U32 cou32GPS_SUPPORT_RMC = 0x00000010;
static const t_U32 cou32GPS_SUPPORT_VTG = 0x00000020;
static const t_U32 cou32GPS_SUPPORT_GST = 0x00000040;

//!UID's of Location Update Data
static const t_U32 cou32LOCATION_GEOLOCATION_OBJECT_UID                  = 0x572a6461;
static const t_U32 cou32LOCATION_GEOLOCATION_COORD_FIELD_UID             = 0xbad026d0;
static const t_U32 cou32LOCATION_GEOLOCATION_LATITUDE_FIELD_UID          = 0x64f8f3f1;
static const t_U32 cou32LOCATION_GEOLOCATION_LONGITUDE_FIELD_UID         = 0x7581892a;
static const t_U32 cou32LOCATION_GEOLOCATION_ALTITUDE_FIELD_UID          = 0x970e9047;
static const t_U32 cou32LOCATION_GEOLOCATION_ACCURACY_FIELD_UID          = 0x5ec654de;
static const t_U32 cou32LOCATION_GEOLOCATION_ALTITUDEACCURACY_FIELD_UID  = 0xc28b9440;
static const t_U32 cou32LOCATION_GEOLOCATION_HEADING_FIELD_UID           = 0x813c675d;
static const t_U32 cou32LOCATION_GEOLOCATION_SPEED_FIELD_UID             = 0x23234962;
static const t_U32 cou32LOCATION_GEOLOCATION_TIMESTAMP_FIELD_UID         = 0x59413fd1;

//!GPS service name
static const t_Char* const copczGPS_SERVICE_NAME = "com.mirrorlink.GPS";
//!Location Updatate service name
static const t_Char* const copczLOCATION_SERVICE_NAME = "com.mirrorlink.location";

static const t_U8 u8CDB_VERSION_MAJOR = 0x01;
static const t_U8 u8CDB_VERSION_MINOR = 0x01;

typedef enum
{
   e8CDB_GPS_SERVICE = 0x00,
   e8CDB_LOC_SERVICE = 0x01,
   //e8CDB_LOCATION_SERVICE, //@Note: currently not supported
   //! Please add new CDB service types above this in consecutive order
   //! This is required to maintain the size of enum
   e8CDB_SERVICE_INDEX_SIZE,
   e8CDB_UNKNOWN_SERVICE
} tenCDBServiceType;

struct trCdbServiceConfig
{
   /*!
   * Service to be configured
   */
   tenCDBServiceType enServiceType;

   /*!
   * CDB service activation value:
   - if "ServiceEnabled" value is set to "TRUE" the appropriate CDB service shall be available on the CDB bus
   - if "ServiceEnabled" value is set to "FALSE" the appropriate CDB service shall be not available on the CDB bus
   */
   t_Bool bServiceEnabled;

   //! Initialise structure members to 0
   trCdbServiceConfig() :
      enServiceType(e8CDB_UNKNOWN_SERVICE), bServiceEnabled(false)
   {

   }

   t_Bool operator==(const trCdbServiceConfig& rConfig)
   {
      //compare all members
      return ((rConfig.enServiceType == enServiceType) && (rConfig.bServiceEnabled == bServiceEnabled));
   }
} ;

/** NMEA object carrying NMEA sentence @mandatory @UID: 0x0AAC4540 */
struct trNMEA
{
   //!@UID: 0x144A776F
   t_S8* ps8Data;

   //!UTC time of data @UID: 0x59413FD1
   t_U64 u64TimeStamp; 

//! Initialise structure members to 0
   trNMEA() : ps8Data(NULL), u64TimeStamp(0)
   {
   }

} ;

/** gives configuration information of NMEA object @mandatory @UID: 0x9D08B19D*/
struct trNMEADescription
{
   /** command separated list of supported sentences like GGA,GLL,GSA,GSV,RMC,VTG,GST @UID: 0x6E72B167*/
   t_U32 u32SupportedSentences; 
   
} ;

/** STRUCTURE holding location coordinate. */
struct trCoordinates
{
   /** geographic coordinates @unit: decimal degrees @optional: NaN @UID: 0x64F8F3F1 @range: -90 to 90 with North positive */
   t_Double dLatitude;
      /** geographic coordinates @unit: decimal degrees @optional: NaN @UID: 0x7581892A @range: -180 to 180 with East positive */
   t_Double dLongitude;
   /** height of the position above the [WGS84] ellipsoid @unit: meters @optional: NaN @UID: 0x970E9047 */
   t_Double dAltitude;
   /** accuracy level of the latitude and longitude coordinates @unit: meters @optional: NaN @UID: 0x5EC654DE */
   t_Double dLongLatAccuracy;
   /** accuracy level of the altitude information @unit: meters @optional: NaN @UID: 0xC28B9440 */
   t_Double dAltitudeAccuracy;
   /** direction of travel @unit: degrees, where 0° = heading < 360°,
      counting clockwise relative to the true north @optional: NaN @UID: 0x813C675D */
   t_Double dHeading;
   /** magnitude of the horizontal component of current velocity @unit: meters per second @optional: NaN @UID: 0x23234962 */
   t_Double dSpeed;

   //! Initialise structure members to 0
   trCoordinates() :
      dLatitude(0), dLongitude(0), dAltitude(0), 
      dLongLatAccuracy(0), dAltitudeAccuracy(0), 
      dHeading(0), dSpeed(0)
   {
   
   }

} ;

/** high-level location object @mandatory @UID: 0x572A6461 */
struct trGeoLocation
{
   //!Location coordinate structure @UID 0xBAD026D0
   trCoordinates rCoord;
   //!UTC time when the position was acquired. @mandatory @UID: 0x59413FD1
   t_U64 u64TimeStamp;   
   //!sensor info
   t_U16 u16Sensordata; 

   //! Initialise members to default
   trGeoLocation() :
   u64TimeStamp(0),u16Sensordata(0)
   {

   }

} ;

//! Posix time (Time since the Epoch (00:00:00 UTC, January 1, 1970),
//! measured in seconds
typedef t_S32 t_PosixTime;

//! Indicates the direction of the Heading caluculation
enum tenHeadingDir
{
   //! Clockwise direction
   e8CLOCKWISE = 0,
   //!Anti-clockwise Direction
   e8ANTICLOCKWISE = 1
};

//! Indicates Sensor type available in the vehicle
enum tenSensorType
{
   e8SENSOR_TYPE_UNKNOWN = 0,
   e8SENSOR_TYPE_COMBINED_LEFT_RIGHT_WHEEL = 1
};

/* Structure holding Location data */
struct trPositionData
{
   //! Time in Posix format
   t_PosixTime PosixTime;
   //! Precision of Posix time (in milliseconds) of received data
   t_U16 u16ExactTime;
   //! Latitude (in decimal) of vehicle position
   t_S32 s32Latitude;
   //! Longitude (in decimal) of vehicle position
   t_S32 s32Longitude;
   //! Resolution of Latitude/Longitude data
   //! @Note: Indicates scaling factor of Latitude/Longitude values.
   //! Actual Lat/Long value = <LatLong_in_decimal> / <Resolution>
   t_Double dLatLongResolution;
   //! Altitude (in meters)
   t_S32 s32Altitude;
   //! Heading (in decimal) of vehicle
   t_Double dHeading;
   //! Resolution of Heading data
   //! @Note: Indicates scaling factor of Heading value.
   //! Actual Heading value = <Heading_in_decimal> / <Resolution>
   t_Double dHeadingResolution;
   //! Speed (in centimeter per second) of vehicle
   t_S16 s16Speed;
   //! Value that indicates the Heading Direction-Clockwise or anti-clockwise
   tenHeadingDir enHeadingDir;
   //! Sensor type used
   tenSensorType enSensorType;
   //! Turn rate (in centidegree per second)
   t_S16 s16TurnRate;
   //! Acceleration (cm/s)
   t_S16 s16Acceleration;

   //! Data availability flags
   t_Bool bLatitudeAvailable;
   t_Bool bLongitudeAvailable;
   t_Bool bAltitudeAvailable;
   t_Bool bHeadingAvailable;
   t_Bool bSpeedAvailable;

   //! Initialise structure members to default values
   trPositionData() : PosixTime(0), u16ExactTime(0),
         s32Latitude(0), s32Longitude(0), dLatLongResolution(1),
         s32Altitude(0), dHeading(0), dHeadingResolution(1),
         s16Speed(0), enHeadingDir(e8CLOCKWISE),
         enSensorType(e8SENSOR_TYPE_UNKNOWN),
         s16TurnRate(0),
         bLatitudeAvailable(false), bLongitudeAvailable(false),
         bAltitudeAvailable(false), bHeadingAvailable(false),
         bSpeedAvailable(false)
   {
   }
};


//! Structure for holding GPS data
typedef trPositionData trGPSData;
//! Structure for holding DeadReckoning data
typedef trPositionData trDeadReckoningData;

/* Identifies the source of NMEA data */
enum tenNmeaDataSource
{
   //! GPS as source of NMEA data
   e8NMEA_SOURCE_GPS = 0,
   //! Unknown source of NMEA data
   e8NMEA_SOURCE_UNKNOWN
};

/* Identifies the type of Location data */
enum tenLocationDataType
{
   //! Location data type is GPS data
   e8GPS_DATA = 0,
   //! Location data type is Dead Reckoning data
   e8DEADRECKONING_DATA = 1
};

/* Identifies NMEA sentence types */
enum tenNmeaSentenceType
{
   e8NMEA_GPGGA = 0,
   e8NMEA_GPRMC,
   e8NMEA_GPGSV,
   e8NMEA_GPHDT,
   e8NMEA_PASCD,
   e8NMEA_PAGCD,
   e8NMEA_PAACD
};

typedef enum
{
   e8GNSSQUALITY_NOFIX = 0x00,
   e8GNSSQUALITY_AUTONOMOUS = 0x01,
   e8GNSSQUALITY_DIFFERENTIAL = 0x02,
   e8GNSSQUALITY_UNKNOWN = 0x0ff
} tenGnssQuality;

typedef enum
{
   e8GNSSMODE_NOFIX = 0x01,
   e8GNSSMODE_2DFIX = 0x02,
   e8GNSSMODE_3DFIX = 0x03,
   e8GNSSMODE_UNKNOWN = 0x0255
} tenGnssMode;

//! Structure for holding sensor data
struct trSensorData
{
   //! Timestamp
   t_U32 u32TimeStamp;
   //! UTC time
   t_PosixTime PosixTime;
   //! Gnss Quality indicator
   tenGnssQuality enGnssQuality;
   //! Gnss Mode
   tenGnssMode enGnssMode;
   //! Number of Satellites used
   t_U16 u16SatellitesUsed;
   //! Horizontal dilution of precision
   t_Double dHDOP;
   //! Geoidal Separation (in meters)
   t_Double dGeoidalSeparation;
   //! Accuracy (in meters)
   t_Float fAccuracy;
   //! Speed (in meters per sec)
   t_Float fSpeed;
   //! Heading (or Bearing) (in radian)
   t_Float fHeading;
   //! Latitude (in radian)
   t_Double dLatitude;
   //! Longitude (in radian)
   t_Double dLongitude;
   //! Altitude (in meters)
   t_Float fAltitude;
   //! GyroZ (in deg/s)
   t_Float fGyroZ;
   //! GyroY (in deg/s)
   t_Float fGyroY;
   //! GyroX (in deg/s)
   t_Float fGyroX;
   //! Acceleration-Z (in m/s2)
   t_Float fAccZ;
   //! Acceleration-Y (in m/s2)
   t_Float fAccY;
   //! Acceleration-X (in m/s2)
   t_Float fAccX;

   //! Data availability flags
   t_Bool bNumSatUsedAvailable;
   t_Bool bHDOPAvailable;
   t_Bool bGeoidalSepAvailable;

   trSensorData() :
   u32TimeStamp(0), PosixTime(0), enGnssQuality(e8GNSSQUALITY_UNKNOWN),
   enGnssMode(e8GNSSMODE_UNKNOWN), u16SatellitesUsed(0), dHDOP(0.0), dGeoidalSeparation(0.0),
   fAccuracy(0),fSpeed(0),fHeading(0),dLatitude(0),dLongitude(0),fAltitude(0),
   bNumSatUsedAvailable(false), bHDOPAvailable(false), bGeoidalSepAvailable(false),
   fGyroZ(0), fGyroY(0), fGyroX(0), fAccZ(0), fAccY(0), fAccX(0)
   {

   }
} ;

//! Structure for holding Acceleration sensor data
struct trAccSensorData
{
   //! Acceleration-Z (in m/s2)
   t_Float fAccZ;
   //! Acceleration-Y (in m/s2)
   t_Float fAccY;
   //! Acceleration-X (in m/s2)
   t_Float fAccX;

   trAccSensorData() : fAccZ(0), fAccY(0), fAccX(0)
   {
	   //Nothing to do
   }
};

//! Structure for holding Gyro sensor data
struct trGyroSensorData
{
   //! GyroZ (in deg/s)
   t_Float fGyroZ;
   //! GyroY (in deg/s)
   t_Float fGyroY;
   //! GyroX (in deg/s)
   t_Float fGyroX;

   trGyroSensorData() : fGyroZ(0), fGyroY(0), fGyroX(0)
   {
	   //Nothing to do
   }
};

//Indicates the Gear Position
enum tenGearPosition
{
   e8TRANS_EST_GEAR_POS_NOT_SUPPORTED = 0,
   e8TRANS_EST_GEAR_POS_FIRST_GEAR = 1,
   e8TRANS_EST_GEAR_POS_SECOND_GEAR = 2,
   e8TRANS_EST_GEAR_POS_THIRD_GEAR = 3,
   e8TRANS_EST_GEAR_POS_FOURTH_GEAR = 4,
   e8TRANS_EST_GEAR_POS_FIFTH_GEAR = 5,
   e8TRANS_EST_GEAR_POS_SIXTH_GEAR = 6,
   e8TRANS_EST_GEAR_POS_SEVENTH_GEAR = 7,
   e8TRANS_EST_GEAR_POS_EIGHTH_GEAR = 8,
   e8TRANS_EST_GEAR_POS_EVT_MODE1 = 10,
   e8TRANS_EST_GEAR_POS_EVT_MODE2 = 11,
   e8TRANS_EST_GEAR_POS_CVT_FORWARD_GEAR = 12,
   e8TRANS_EST_GEAR_POS_NEUTRAL_GEAR = 13,
   e8TRANS_EST_GEAR_POS_REVERSE_GEAR = 14,
   e8TRANS_EST_GEAR_POS_PARK_GEAR = 15
   //@Note: Please do not change above values!
   //Values are assigned as per GMLAN Gateway FI values
};

enum tenVehicleMovementState
{
   e8VEHICLE_MOVEMENT_STATE_PARKED = 0,
   e8VEHICLE_MOVEMENT_STATE_NEUTRAL = 1,
   e8VEHICLE_MOVEMENT_STATE_FORWARD = 2,
   e8VEHICLE_MOVEMENT_STATE_REVERSE = 3,
   e8VEHICLE_MOVEMENT_STATE_INVALID = 4
};

//Indicates the Vehicle Data for PASCD sentence
struct trVehicleData
{
   //! Posix time
   t_PosixTime PosixTime;

   //! Speed (in centimeter per second) of vehicle
   t_S16 s16Speed;

   //! Transmitted Gear Position
   tenGearPosition enGearPosition;

   //! Parking Brake
   t_Bool bParkBrakeActive;

   //! Data availability flags
   t_Bool bSpeedAvailable;

   tenVehicleMovementState enVehMovState;

   //! Initialise all members to defaults
   trVehicleData():PosixTime(0),s16Speed(0),
      enGearPosition(e8TRANS_EST_GEAR_POS_NOT_SUPPORTED), 
      bParkBrakeActive(false), bSpeedAvailable(false), enVehMovState(e8VEHICLE_MOVEMENT_STATE_INVALID)
   {
   }
};

//! Indicates the GMLNGW GPS Configuration
enum tenGPSConfig
{
   e8GMLAN_GPS_NONE = 0,
   e8GMLAN_GPS_ONSTAR = 1,
   e8GMLAN_GPS_NAVIGATION = 2
   //@Note: Please do not change above values!
   //Values are assigned as per GMLAN Gateway FI values.
};


//! Callback signatures definition for GPS Data
typedef std::function<t_Void(trGPSData)>        vOnGpsData;
typedef std::function<t_Void(trDeadReckoningData)>   vOnDeadReckoningData;

//! \brief   Structure holding the callbacks to subscriber.
//!          Used by Client-handler to notify subscriber on interested events.
struct trLocationDataCallbacks
{
   //! Called when GPS data is received by client handler.
   vOnGpsData     fvOnGpsData;

   //! Called when DeadReckoning data is received by client handler.
   vOnDeadReckoningData    fvOnDeadReckoningData;

   trLocationDataCallbacks() : fvOnGpsData(NULL), fvOnDeadReckoningData(NULL)
   {
   }
};

//! Callback signatures definition for Sensor Data
typedef std::function<t_Void(trSensorData)>        vOnSensorData;

//! Callback signatures definition for Sensor Data
typedef std::function<t_Void(const std::vector<trGyroSensorData>&)>        vOnGyroSensorData;

//! Callback signatures definition for Sensor Data
typedef std::function<t_Void(const std::vector<trAccSensorData>&)>        vOnAccSensorData;

//! \brief   Structure holding the callbacks to subscriber.
//!          Used by Client-handler to notify subscriber on interested events.
struct trSensorDataCallbacks
{
   //! Called when Sensor data is received by client handler.
   vOnSensorData     fvOnSensorData;

   //! Called when Gyro Sensor data is received by client handler.
   vOnGyroSensorData     fvOnGyroSensorData;

   //! Called when Acceleration Sensor data is received by client handler.
   vOnAccSensorData     fvOnAccSensorData;

   trSensorDataCallbacks() : fvOnSensorData(NULL), fvOnGyroSensorData(NULL),
		                     fvOnAccSensorData(NULL)
   {
   }
};

//! Callback signatures definition for Vehicle Data
typedef std::function<t_Void(trVehicleData, t_Bool)>        vOnVehicleData;


//! \brief   Structure holding the callbacks to subscriber.
//!          Used by Client-handler to notify subscriber on interested events.
struct trVehicleDataCallbacks
{
   //! Called when Vehicle data is received by client handler.
   vOnVehicleData     fvOnVehicleData;

   trVehicleDataCallbacks() : fvOnVehicleData(NULL)
   {
   }
};


//Callback for OutsideTemperature update
typedef std::function<t_Void(t_Bool,t_Double)> tvOutsideTempUpdate;


struct trEnvironmentDataCbs
{
   tvOutsideTempUpdate fvOutsideTempUpdate;

   trEnvironmentDataCbs():fvOutsideTempUpdate(NULL)
   {

   }
};


/***************************************************
**************AUDIO**********************************
*****************************************************/

/******AudioErrors**************************/
/** No error reported. */
static const t_S32 cs32MLAudioRouterErrorNone = 0;
/** Error reported by the mobile device. Bluetooth is not supported on the
* device. */
static const t_S32 cs32MLAudioRouterErrorBtNotSupported = 1;
/** Error reported by the mobile device. The mobile platform can not complete the
* request automatically. Only a manual check/connect/disconnect can be done. */
static const t_S32 cs32MLAudioRouterErrorManualOnly = 2;
/** Error reported by the mobile device. Pairing failed because of an
* authentication failure. */
static const t_S32 cs32MLAudioRouterErrorAuthFailure = 3;
/** Error reported by the mobile device. User is needed to manually complete
* the request. */
static const t_S32 cs32MLAudioRouterErrorUserNeeded = 4;
/** Error reported by the mobile device. The protocol is not supported. */
static const t_S32 cs32MLAudioRouterErrorProtocolNotSupported = 5;
/** Error reported by the mobile device. The address provided is not valid. */
static const t_S32 cs32MLAudioRouterErrorInvalidAddress = 6;
/** Error reported by the mobile device. The requested device can not be found. */
static const t_S32 cs32MLAudioRouterErrorDeviceNotFound = 7;
/** Error reported by the mobile device. The PIN needs to be entered by the
* user on the mobile device. */
static const t_S32 cs32MLAudioRouterErrorManualPinNeeded = 8;
/** Error reported by the mobile device. Can not pair automatically. The user
* must create the pairing manually. */
static const t_S32 cs32MLAudioRouterErrorManualPairingNeeded = 9;
/** Error reported by the mobile device. Can not determine the
* connected/bonded profile. */
static const t_S32 cs32MLAudioRouterErrorProfileNotDetermined = 10;
/** Error reported by the mobile device. Can not connect all the requested
* profiles; only some of them. */
static const t_S32 cs32MLAudioRouterErrorPartialConnect = 11;
/** Error reported by the mobile device. Bluetooth is switched off, so the
* request can not be completed. */
static const t_S32 cs32MLAudioRouterErrorBtOff = 12;
/** Error reported by the mobile device. The audio device is paired with the
* target device, but is not set as authorized (fully bonded). */
static const t_S32 cs32MLAudioRouterErrorNotAuthorized = 13;
/** Unable to find an item which is needed to complete the request. */
static const t_S32 cs32MLAudioRouterErrorNotFound = 100;
/** At least one of the parameters passed to the API is invalid. */
static const t_S32 cs32MLAudioRouterErrorInvalidParameter = 101;
/** Not enough memory to complete the request. */
static const t_S32 cs32MLAudioRouterErrorOutOfMemory = 102;
/** Request is not supported. */
static const t_S32 cs32MLAudioRouterErrorNotSupported = 103;
/** An item/connection/request already exists */
static const t_S32 cs32MLAudioRouterErrorAlreadyExists = 104;
/** Request can't be completed because the configuration step wasn't completed
* successfully. */
static const t_S32 cs32MLAudioRouterErrorNotConfigured = 105;
/** Can't make the request because of a disconnection. */
static const t_S32 cs32MLAudioRouterErrorDisconnected = 106;
/** Not enough buffer space to send message out to the ML server. */
static const t_S32 cs32MLAudioRouterErrorInsufficientBufferSpace = 107;
/** The VNC connection is not started, so it is not possible to send the
* request at the moment. */
static const t_S32 cs32MLAudioRouterErrorNotStarted = 108;
/** One of the string arguments is bigger than the maximum expected size. */
static const t_S32 cs32MLAudioRouterErrorTooBig = 109;
/** There is a conflict between the settings requested. */
static const t_S32 cs32MLAudioRouterErrorConflict = 110;
/** The request timed out. */
static const t_S32 cs32MLAudioRouterErrorTimedOut = 111;
/** Unable to make request because the router is shutting down. */
static const t_S32 cs32MLAudioRouterErrorShuttingDown = 112;
/** Unable to make request because no usage has been set. */
static const t_S32 cs32MLAudioRouterErrorUsageNotSet = 113;
/** The audio router extension feature is not licensed. */
static const t_S32 cs32MLAudioRouterErrorNotLicensed = 114;
/** A general, unspecified, error occurred. */
static const t_S32 cs32MLAudioRouterErrorGeneral = 0xFFFF;

typedef t_S32 MLAudioRouterError;

static const t_S32 cs32MLAudioRouterProtocolUndefined = 0;
static const t_S32 cs32MLAudioRouterProtocolBtHfp = 2;
static const t_S32 cs32MLAudioRouterProtocolBtA2dp = 4;
static const t_S32 cs32MLAudioRouterProtocolRtpOut = 8;
static const t_S32 cs32MLAudioRouterProtocolRtpIn = 16;
static const t_S32 cs32MLAudioRouterProtocolRtpVoiceCommand = 32;
static const t_S32 cs32MLAudioRouterProtocolAny = 0xFFFFFFFF;

struct trMLAudioRtpInInfo
{
      /** The URI where to send the RTP packets */
      t_String szUri;
      /** The payload types supported by the RTP client (MirrorLink server
       * application). The viewer application should use one of the payload types
       * defined here */
      t_String szPayloadTypes;
      /** The audio Initial Playback Latency requested by the RTP client
       * (MirrorLink server). This is the audioIpl as received from the server. If
       * empty, or NULL, the default value, as specified by the MirrorLink
       * specification, is assumed.*/
      t_String szAudioIpl;
      /** The audio Maximum Payload Length requested by the RTP client
       * (MirrorLink server). This is the audioMpl as received from the server. If
       * empty, or NULL, the default value, as specified by the MirrorLink
       * specification, is assumed.*/
      t_String szAudioMpl;
      //! Initialise structure members to 0
      trMLAudioRtpInInfo()
      {
      }

      //Assignment operator
      trMLAudioRtpInInfo& operator=(const trMLAudioRtpInInfo& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            szUri = corfrSrc.szUri.c_str();
            szPayloadTypes = corfrSrc.szPayloadTypes.c_str();
            szAudioIpl = corfrSrc.szAudioIpl.c_str();
            szAudioMpl = corfrSrc.szAudioMpl.c_str();
         }
         return *this;
      }

      //Copy constructor
      trMLAudioRtpInInfo(const trMLAudioRtpInInfo& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            szUri = corfrSrc.szUri.c_str();
            szPayloadTypes = corfrSrc.szPayloadTypes.c_str();
            szAudioIpl = corfrSrc.szAudioIpl.c_str();
            szAudioMpl = corfrSrc.szAudioMpl.c_str();
         }
      }
} ;

struct trMLAudioRtpOutInfo
{
      /** The URI of the RTP server. The MirrorLink client is expected to send a
       * an UDP packet containing 1 byte (of arbitrary value) to the URI (a 'hello'
       * message), before the RTP server can start streaming (see the MirrorLink
       * specification for more information. */
      t_String szUri;
      trMLAudioRtpOutInfo()
      {
      }
      //Assignment operator
      trMLAudioRtpOutInfo& operator=(const trMLAudioRtpOutInfo& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            szUri = corfrSrc.szUri.c_str();
         }
         return *this;
      }

      //Copy constructor
      trMLAudioRtpOutInfo(const trMLAudioRtpOutInfo& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            szUri = corfrSrc.szUri.c_str();
         }
      }
};

struct trMLAudioLinkInfo
{
      t_Void *pContext;
      trMLAudioRtpInInfo rRtpInExtraInfo;
      trMLAudioRtpOutInfo rRtpOutExtraInfo;
      t_Bool bRTPOutStreaming;
      t_Bool bRTPInStreaming;
      //! Initialise structure members to 0
      trMLAudioLinkInfo() :
         pContext(NULL), bRTPOutStreaming(false), bRTPInStreaming(false)
      {

      }
} ;
struct trMLRTPOutContext
{
   pthread_mutex_t mutex;
   pthread_t gst_thread;

   //GMainLoop * gst_mainloop;
   t_U32 u32payloadtype;
   t_U32 u32channels;
   t_U32 u32samplerate;
   t_U32 u32ipl_ms;
   t_U32 u32ipaddr;
   t_U32 u32port;
   t_U8 * pu8PlaybackDevice;

   //mlink_gst_rtp_out_error_callback * error_cb;
   //mlink_gst_rtp_out_data_callback * data_cb;
   t_Void * p_user_ctx;
   //DltContext * p_dlt_ctx;
} ;

//typedef trMLRTPOutContext mlink_rtp_out_context;

struct trMLRTPInContext
{
   pthread_mutex_t mutex;
   pthread_t gst_thread;

   //GMainLoop * gst_mainloop;
   t_U32 u32Payloadtype;
   t_U32 u32Channels;
   t_U32 u32Samplerate;
   t_U32 u32IPaddr;
   t_U32 u32Port;
   t_U8 * pu8CaptureDevice;

   //mlink_gst_rtp_in_error_callback * error_cb;
   t_Void * p_user_ctx;
   //DltContext * p_dlt_ctx;
} ;

//typedef trMLRTPInContext mlink_rtp_in_context;

struct trMLAppData
{
   t_U8 * pu8audiosrc;
   t_U8 * pu8audiosink;
   trMLRTPOutContext *prRTPOutCtxt;
   trMLRTPInContext *prRTPInCtxt;

   t_U32 u32rtp_payload_type;
   t_U32 u32IPL;
//! Initialise structure members to 0
   trMLAppData():
      pu8audiosrc(NULL), pu8audiosink(NULL),
      prRTPOutCtxt(NULL), prRTPInCtxt(NULL),
      u32rtp_payload_type(0), u32IPL(0)
   {

   }

} ;

struct trMLContext
{
   MLViewerError error;
   t_U16 u16wl_error;

   /* the framebuffer */
   trMLPixelFormat rFramebufferPixelFormat;
   t_U16 u16width;
   t_U16 u16height;
   t_U16 u16stride;

   trMLAppData * trAppdata;
} ;

struct trBtInfo
{
   MLAudioRouterError s32BtError;
   t_U32 u32BtEnabled;
   MLAudioRouterError s32BondedProfilesError;
   t_S32 s32BondedBtProfiles;
   MLAudioRouterError s32ConnectedProfilesError;
   t_S32 s32ConnectedBtProfiles;
   t_Char* pcBtAddress;
//! Initialise structure members to 0
   trBtInfo():
      s32BtError(0), u32BtEnabled(0), s32BondedProfilesError(0),
      s32ConnectedProfilesError(0), s32ConnectedBtProfiles(0), pcBtAddress(NULL)
   {

   }
} ;



/*****************************************************
**************** COMMON ******************************
*****************************************************/
#define MAX_KEYSIZE 256
/**
* typedef enum tenServiceId
* \Enumeration to identify different services or functionalities in the SPI core lib
*  Use it along with mediator.
*  TODO : Append new service Ids as various services are identified for SPI core
*/
/*
typedef enum 
{
   e8SPI_SERVICE_VIDEO,
   e8SPI_SERVICE_AUDIO,
   e8SPI_SERVICE_SPIMANAGER,
   e8SPI_SERVICE_INVALID
}tenServiceId;*/

/**
 * struct rUserContext
 * \brief  Structure to store the User Context info
 */
struct trUserContext
{
      /*
       * Source Application Id
       */
      t_U32 u32SrcAppID;
      /*
       * Registration Id
       */
      t_U32 u32RegID;
      /*
       * Command Counter
       */
      t_U32 u32CmdCtr;
      /*
       * Function ID
       */
      t_U32 u32FuncID;

      /*
       * Destination Application Id
       */
      t_U32 u32DestAppID;

      /*
       * Service Id
       */
      t_U32 u32ServcID;

      /*************************************************************************
       ** FUNCTION:  rUserContext::rUserContext()
       *************************************************************************/
      /*!
       * \fn    rUserContext()
       * \brief Constructor
       *************************************************************************/
      trUserContext() :
         u32SrcAppID(0), u32RegID(0), u32CmdCtr(0), u32FuncID(0),
                  u32DestAppID(0), u32ServcID(0)
      {

      }

      t_Bool operator==(const trUserContext& rfcorUserContext) const
      {
         //compare all members
         return ((rfcorUserContext.u32SrcAppID == u32SrcAppID) &&
               (rfcorUserContext.u32RegID == u32RegID) &&
               (rfcorUserContext.u32CmdCtr == u32CmdCtr) &&
               (rfcorUserContext.u32FuncID == u32FuncID) &&
               (rfcorUserContext.u32DestAppID == u32DestAppID) &&
               (rfcorUserContext.u32ServcID == u32ServcID));
      }

      t_Bool operator!=(const trUserContext& rfcorUserContext) const
      {
         //use equality operator
         return !((*this).operator==(rfcorUserContext));
      }

      trUserContext& operator=(const trUserContext& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            u32SrcAppID = corfrSrc.u32SrcAppID;
            u32RegID = corfrSrc.u32RegID;
            u32CmdCtr = corfrSrc.u32CmdCtr;
            u32FuncID = corfrSrc.u32FuncID;
            u32DestAppID = corfrSrc.u32DestAppID;
            u32ServcID = corfrSrc.u32ServcID;
         }//if( &corfrSrc != this)
         return *this;
      }


} ;

//! Empty context
static const trUserContext corEmptyUsrContext;

/**
* struct rAppContext
* \brief  Structure to store the application Context info
*/

struct trAppContext
{
      //! Response ID
      t_U32 u32ResID;

      //! Service ID
      t_U32 u32ServiceID;

      //! Initialise structure members to 0
      trAppContext() : u32ResID(0), u32ServiceID(0){ }

      //Assignment operator
      trAppContext& operator=(const trAppContext& corfrSrc)
      {
         if( &corfrSrc != this)
         {
            u32ResID = corfrSrc.u32ResID;
            u32ServiceID = corfrSrc.u32ServiceID;
         }//if( &corfrSrc != this)
         return *this;
      }
} ;

/*
struct trAppContext
{
   //! Response ID
   t_U32 u32ResID;

   //! Service ID list
   std::list<t_U32> lstReceiverServiceId;

   rAppContext() : u32ResID(0)
   {
   }
} ;
*/
/**
* struct rMsgContext
* \brief  Structure to store the complete message Context info
*/
struct trMsgContext
{
   trUserContext rUserContext;
   trAppContext rAppContext;
//! Default Constructor
   trMsgContext() { }

   //Assignment operator
   trMsgContext& operator=(const trMsgContext& corfrSrc)
   {
      if( &corfrSrc != this)
      {
         rUserContext = corfrSrc.rUserContext;
         rAppContext = corfrSrc.rAppContext;
      } //if( &corfrSrc != this)
      return *this;
   }

} ;


//! Identifies the Audio Streaming Link 
typedef enum
{
   e8LINK_RTPOUT = 0,
   e8LINK_RTPIN = 1,
   e8LINK_BTA2DP = 2,
   e8LINK_BTHFP = 3
} tenAudioLink;


/***************************************************************************************************************************
   AudioDir                   Purpose                                   GM-AudioSource        G3G-AudioSource
----------------------------------------------------------------------------------------------------------------------------
   e8AUD_MAIN_OUT          To enable MAIN Audio channel                 LC_MAIN              ARL_SRC_SPI_MAIN     
   e8AUD_MIX_OUT           To enable Mono MIX channel with out Duck     LC_ALERTTONE         ARL_SRC_SPI_INFO
   e8AUD_ALERT_OUT         To play Ringtones                            LC_INCOMING_RINGTONE ARL_SRC_SPI_ALERT
   e8AUD_PHONE_IN          To enable Telephony                          LC_PHONE             ARL_SRC_SPI_PHONE
   e8AUD_VR_IN             To enable Speech Rec                         LC_SPEECH            ARL_SRC_SPI_VR
   e8AUD_DUCK              To enable Mono SPI_MIX with Duck             LC_MIXALERT          ARL_SRC_SPI_MIX & bSetAudioDucking
   e8AUD_TRANSIENT         To enable SPI_TRANSIENT                        -                  ARL_SRC_SPI_TRANSIENT
   e8AUD_STEREO_MIX        To enable Stereo SPI_MIX                       -                  ARL_SRC_SPI_ML_INFO
   e8AUD_INVALID           Native Audio Source is Active
******************************************************************************************************************************/

/****************************************************************************************************
e8AUD_MIX_OUT and e8AUD_DUCK are used to differentiate Audio channels which needs to mix SPI audio 
    with Duck and with out Duck.
------------------------------------------------------------------------------------------------------
In case of G3G:
To Mix SPI audio with MAIN audio with Ducking => SPI has to use SPI_MIX audio channel and use bSetAudioDucking API
To Mix SPI audio with MAIN audio with out Ducking => SPI just has to use SPI_MIX audio channel

In case of GM:
To Mix SPI audio with MAIN audio with Ducking => SPI has to use LC_MIXALERT audio channel
To Mix SPI audio with MAIN audio with out Ducking => SPI has to use LC_ALERTTONE audio channel
****************************************************************************************************/

//! Identifies the Audio Stream Direction 
typedef enum
{
   //! Audio Output: CE Device -> Head Unit
   e8AUD_MAIN_OUT = 0x00,
   //! Audio Output: CE Device -> Head Unit
   e8AUD_MIX_OUT  = 0x01,
   //! Audio Output: CE Device -> Head Unit
   e8AUD_ALERT_OUT = 0x02,
   //! Audio Input: CE Device <-> Head Unit
   e8AUD_PHONE_IN = 0x03,
   //! Audio Input: CE Device <-> Head Unit
   e8AUD_VR_IN = 0x04,
   //! Audio Input: CE Device -> Head Unit
   e8AUD_DUCK = 0x05,
   //! Audio Output: CE->HU; For general info updates.
   e8AUD_TRANSIENT = 0x06,
   //! Audio Stereo Mix channel without Duck CE -> HU
   e8AUD_STEREO_MIX_OUT = 0x07,
   //! Audio Input: CE Device <-> Head Unit
   e8AUD_DEFAULT = 0x08,
   //! Audio Input: Invalid for Default Value
   e8AUD_INVALID = 0x09
} tenAudioDir;


//Identifies Row and column size
typedef enum
{   e8AUD_COLUMN_SIZE = 0x04,
	e8AUD_ROW_SIZE = 0x05
}tenAudioSize;

//! Identifies the Audio  Sampling Rate
typedef enum
{
   //! Default Sample Rate
   e8AUD_SAMPLERATE_DEFAULT = 0x00,
   //! 8KHz Sample Rate
   e8AUD_SAMPLERATE_8KHZ  = 0x01,
   //! 16KHz Sample Rate
   e8AUD_SAMPLERATE_16KHZ = 0x02,
   //! 24KHz Sample Rate
   e8AUD_SAMPLERATE_24KHZ = 0x03,
   //!Add entries above this for sampling rates
   e8AUD_SAMPLERATE_MAX = 0x04
} tenAudioSamplingRate;

/**
* class trMsgBase
* \brief  Base class for all messages used for communication and data exchange between 
*  classes in Smart Phone integration
*/


class trMsgBase
{
   public:

      trMsgBase()
      {
      }

      virtual ~trMsgBase()
      {

      }

      t_U32 u32GetServiceID()
      {
         return rMsgCtxt.rAppContext.u32ServiceID;
      }

      t_Void vSetServiceID(t_U32 u32ServiceID)
      {
         rMsgCtxt.rAppContext.u32ServiceID = u32ServiceID;
      }

      t_Void vGetUserContext(trUserContext &rfrUserContext)
      {
         rfrUserContext = rMsgCtxt.rUserContext;
      }

      t_Void vSetUserContext(const trUserContext &rfrUserContext)
      {
         rMsgCtxt.rUserContext = rfrUserContext;
      }

      //virtual t_Void vAllocateMsg() = 0;

      //virtual t_Void vDeAllocateMsg() = 0;
   private:
      trMsgContext rMsgCtxt;

};

typedef enum
{
   e32MODULEID_VNCDISCOVERER = 1,
   e32MODULEID_VNCAUDIO = 2,
   e32MODULEID_VNCCDB = 3,
   e32MODULEID_VNCVIEWER = 4,
   e32MODULEID_VNCNOTIFICATIONS = 5,
   e32MODULEID_VNCDAP = 6
} tenModuleID;

//! Defines Command IDs for HMI interface
enum tenCommandID
{
   e32COMMANDID_INVALID = 0,
   e32COMMANDID_GETDEVICELIST = 1,
   e32COMMANDID_SELECTDEVICE = 2,
   e32COMMANDID_GETAPPLIST = 3,
   e32COMMANDID_LAUNCHAPP =4,
   e32COMMANDID_TERMINATEAPP = 5,
   e32COMMANDID_GETAPPICONDATA = 6,
   e32COMMANDID_SETAPPICONATTR = 7,
   e32COMMANDID_SETDEVICEUSAGEPREF = 8,
   e32COMMANDID_SETORIENTATIONMODE = 9,
   e32COMMANDID_SETSCREENSIZE = 10,
   e32COMMANDID_SETVIDEOBLOCKINGMODE = 11,
   e32COMMANDID_SETAUDIOBLOCKINGMODE = 12,
   e32COMMANDID_SETVEHICLECONFIG = 13,
   e32COMMANDID_SENDTOUCHEVENT = 14,
   e32COMMANDID_SENDKEYEVENT = 15,
   e32COMMANDID_ALLOCATEAUDIOROUTE = 16,
   e32COMMANDID_DEALLOCATEAUDIOROUTE = 17,
   e32COMMANDID_AUDIOSRCACTIVITY = 18,
   e32COMMANDID_SETSCREENVARIANT = 19,
   e32COMMANDID_BTDEVICEACTION = 20,
   e32COMMANDID_BTADDRESS = 21,
   e32COMMANDID_MLNOTIONOFF = 22,
   e32COMMANDID_SETACCESSORY_DISPLAYCONTEXT = 23,
   e32COMMANDID_SETACCESSORY_AUDIOCONTEXT = 24,
   e32COMMANDID_SETACCESSORY_APPSTATE = 25,
   e32COMMANDID_SETREGION = 26,
   e32COMMANDID_AUDIO_ROUTEALLOCATE_RESULT = 27,
   e32COMMANDID_AUDIO_ROUTEDEALLOCATE_RESULT = 28,
   e32COMMANDID_AUDIO_STARTSOURCEACTIVITY = 29,
   e32COMMANDID_AUDIO_STOPSOURCEACTIVITY = 30,
   e32COMMANDID_AUDIO_REQAV_DEACTIVATION_RESULT = 31,
   e32COMMANDID_AUDIO_ERROR = 32,
   e32COMMANDID_MLNOTIFICATION_ENABLED_INFO = 33,
   e32COMMANDID_INVOKE_NOTIACTION = 34,
   e32COMMANDID_SETCLIENTCAPABILITIES = 35,
   e32COMMANDID_CALLSTATUS = 36,
   e32COMMANDID_SENSORDATA = 37,
   e32COMMANDID_GPSDATA = 38,
   e32COMMANDID_VEHICLEDATA = 39,
   e32COMMANDID_GETKEYICONDATA = 40,
   e32COMMANDID_SENDKNOBKEYEVENT = 41,
   e32COMMANDID_ACCSENSORDATA = 42,
   e32COMMANDID_GYROSENSORDATA = 43,
   e32COMMANDID_SETFEATURERESTRICTIONDATA = 44,
   e32COMMANDID_ACKNOTIFICATIONEVENT = 45
   };


struct trInitialFramebufferDimensions
{
	t_U32 u32Width;
	t_U32 u32Height;
//! Initialise structure members to 0
	trInitialFramebufferDimensions():
    u32Width(0),u32Height(0)
	{
	}
};

struct trServerDisplayDimensions
{
	t_U32 u32Width;
	t_U32 u32Height;
//! Initialise structure members to 0
	trServerDisplayDimensions():
    u32Width(0),u32Height(0)
	{
	}
};


//! IDS for SPI components
typedef enum
{
   e32COMPID_UNKNOWN = 0,
   e32COMPID_CONNECTIONMANAGER = 1,
   e32COMPID_AUDIO = 2,
   e32COMPID_VIDEO = 3,
   e32COMPID_APPMANAGER = 4,
   e32COMPID_BLUETOOTH = 5,
   e32COMPID_DATASERVICE = 6,
   e32COMPID_INPUTHANDLER =7,
   //! Add Components above this. Please follow sequential numbering
   //! so that enum size is maintained
   e32COMPID_SIZE
}tenCompID;

struct trAudDevices
{
      t_String szOutputDev;
      t_String szInputDev;
};

struct trAudSrcInfo
{
      trAudDevices rMainAudDevNames;
      trAudDevices rEcnrAudDevNames;
};

typedef enum 
{
	e8BASESYSTEM  = 0,           //0 - Base System
	e8CONNSYSTEM  = 1,           //1 - Connectivity System -> 4.2" system
	e8COLORSYSTEM8INCH = 2,      //2 - Color HMI System    -> 8" system
	e8NAVISYSTEM8INCH = 3,       //3 - Premium Navi System -> 8" system
	e8COLORSYSTEM10DOT2INCH = 4, //4 - Color HMI System    -> 10.2" system
	e8NAVISYSTEM10DOT2INCH = 5   //5 - Premium Navi System -> 10.2" system
}tenGMSystemVariant;


//! Context info of an application
struct trVehicleBrandInfo
{
      t_U8 u8GMVehicleBrandStartRange;
      t_U8 u8GMVehicleBrandEndRange;
      t_Char szOemName[STR_LENGTH];
      t_Char szOemIconPath[STR_LENGTH];
      t_Char szManufacturer[STR_LENGTH];
      t_Char szModel[STR_LENGTH];
      t_Bool bIsRotarySupported;
};

struct trVehicleInfo
{
      t_U8 u8GMVehicleBrandStartRange;
      t_U8 u8GMVehicleBrandEndRange;
      t_String szManufacturer;
      t_String szModel;
};

typedef enum 
{
   e8VARIANT_COLOR = 0,
   e8VARIANT_NAVIGATION = 1
}tenSystemVariant;

struct trDiagEOLEntry
{
  t_U8      u8Table;
  t_U16     u16Offset;
  t_U16     u16EntryLength;
  t_U8*     pu8EntryData;
} ;


//! Context info of an application 
struct trAppContextInfo
{
   t_U32 u32AppUUID;
   t_U32 u32AppCategory;
   t_U32 u32AppCategoryTrustLevel;
   t_U32 u32AppContentCategroy;
   t_U32 u32AppContentCategoryTrustLevel;
   t_U32 u32ContentRulesFollowed;
   trMLRectangle rMLRect;
   trAppContextInfo():u32AppUUID(0),u32AppCategory(0),u32AppCategoryTrustLevel(0),u32AppContentCategroy(0),
      u32AppContentCategoryTrustLevel(0),u32ContentRulesFollowed(0)
   {
   }

   //Assignment operator
   trAppContextInfo& operator=(const trAppContextInfo& corfrSrc)
   {
      if( &corfrSrc != this)
      {
         u32AppUUID = corfrSrc.u32AppUUID;
         u32AppCategory = corfrSrc.u32AppCategory;
         u32AppCategoryTrustLevel = corfrSrc.u32AppCategoryTrustLevel;
         u32AppContentCategroy = corfrSrc.u32AppContentCategroy;
         u32AppContentCategoryTrustLevel = corfrSrc.u32AppContentCategoryTrustLevel;
         u32ContentRulesFollowed = corfrSrc.u32ContentRulesFollowed;
         rMLRect = corfrSrc.rMLRect;
      }
      return *this;
   }

} ;

//! Identifies the blocking type
enum tenBlockingType
{
   e8BLOCKINGTYPE_UNKNOWN = 0,
   e8BLOCKINGTYPE_AUDIO = 1,
   e8BLOCKINGTYPE_VIDEO = 2
};

//! MirrorLink Client Profile
struct trClientProfile
{
   //! Profile Id of the Client Profile
   t_String szClientProfileId;
   //! Client Manufacturer or OEM - This is required for fetching member certified applications
   t_String szProfileManufacturer;

   //! Icon Attributes which needs to be set
   trIconAttributes rIconPreferences;

   //! MirrorLink Client BT Address
   t_String szProfileBdAddr;

   //! various RTP payload types supported by the MLC
   t_String szRtpPayLoadTypes;

   //! ML Major version supported by the ML Client
   t_U32 u32ProfileMajorVersion;
   //! ML Minor version supported by the ML Client
   t_U32 u32ProfileMinorVersion;

   //! Support for native notification UI
   t_Bool bNotiUISupport;
   //! Maximum number of actions in notification
   t_U8 u8NotiMaxActions;
   
   //! Friendly name of the MLC
   t_String szFriendlyName;

   trClientProfile():u32ProfileMajorVersion(0),
   u32ProfileMinorVersion(0),
   bNotiUISupport(false),
   u8NotiMaxActions(0)
   {
   szClientProfileId = "0" ;
   szRtpPayLoadTypes = "99";
   }
} ;


/*! 
* \enum tenAudioContext
* \brief SPI internal audio mapping
*/
typedef enum
{
	   //! Idenify the main audio
	   e8SPI_AUDIO_MAIN = 0,

	   //! Idenify audio from iApps
	   e8SPI_AUDIO_INTERNET_APP = 1,

	   //! idenify the speech_rec audio
	   e8SPI_AUDIO_SPEECH_REC = 2,

	   //! Idenify advisor phone audio
	   e8SPI_AUDIO_ADVISOR_PHONE = 3,

	   //! Identify emergency phone audio
	   e8SPI_AUDIO_EMER_PHONE = 4,

	   //! Idenify phone audio
	   e8SPI_AUDIO_PHONE =5,

	   //! Idenitify phone incoming ringtone audio
	   e8SPI_AUDIO_INCOM_TONE = 6,

	   //! Identify traffic alert audio
	   s8SPI_AUDIO_TRAFFIC = 7,

	   //! Idenify emergency message audio
	   e8SPI_AUDIO_EMER_MSG = 8,

	   //! Identify synchronous message audio
	   e8SPI_AUDIO_SYNC_MSG =9,

	   //! Identify mix alert message audio
	   e8SPI_AUDIO_MIX_ALERT_MSG = 10,

	   //! Identify short mix alert message
	   e8SPI_AUDIO_SHORT_MIX_ALERT_MSG = 11,

	   //! Identify audio sleep mode, and it ready to wake up on incoming call.
	   e8SPI_AUDIO_SLEEP_MODE = 12,

	   //! Identify audio sleep mode, and it will not wake up on any event on the projection device.
	   e8SPI_AUDIO_STANDBY_MODE = 13,

	   //! Identify alert tone audio
	   e8SPI_AUDIO_ALERT_TONE = 14,

	   //! Identify asynchronous message audio
	   e8SPI_AUDIO_ASYNC_MSG = 15,

	   //! Identify lvm message audio
	   e8SPI_AUDIO_LVM = 16,

	   //! Identify audio cues message audio
	   e8SPI_AUDIO_CUE = 17,

	   //! Identify no audio
	   e8SPI_AUDIO_NONE = 254,

	   //! Identify unknown audio
	   e8SPI_AUDIO_UNKNOWN = 255
}tenAudioContext;

//! Audio ducking type
enum tenDuckingType
{
   e8_DUCKINGTYPE_UNDUCK = 0,
   e8_DUCKINGTYPE_DUCK = 1
};

//Structure for audio context info//
struct trAudioContext
{
	t_Bool AudioFlag;
	tenAudioContext AudioContext;
	trAudioContext() :
		AudioFlag(false)
	{
	}
} ;

/*!
 * \typedef tenRegion
 */
enum tenRegion
{
   //! Indicates All countries
   e8_WORLD = 0,
   //! Indicates European Union member states
   e8_EU = 1,
   //! Indicates Europe without countries listed separately or EU member states
   e8_EPE = 2,
   //! Indicates Canada
   e8_CAN = 3,
   //! Indicates USA
   e8_USA = 4,
   //! Indicates Americas without countries listed separately
   e8_AMERICA = 5,
   //! Indicates Australia
   e8_AUS = 6,
   //! Indicates Korea
   e8_KOR = 7,
   //! Indicates Japan
   e8_JPN = 8,
   //! Indicates China
   e8_CHN = 9,
   //! Indicates Hongkong
   e8_HKG = 10,
   //! Indicates Taiwan
   e8_TPE = 11,
   //! Indicates India
   e8_IND = 12,
   //! Indicates APAC states without countries listed separately
   e8_APAC = 13,
   //! Indicates African countries without countries listed separately
   e8_AFRICA=14,
   //! Invalid index
   e8_INVALID = 15
};

/*! 
* \enum tenSpeechAppState
* \brief SPI speech states
*/
typedef enum
{
   //! Unknown speech state/ not applicable
   e8SPI_SPEECH_UNKNOWN = 0,

   //! Indentify the end of speeking/recognizing state
   e8SPI_SPEECH_END = 1,

   //! Identify the speeking state
   e8SPI_SPEECH_SPEAKING = 2,

   //! Identify recognizing state
   e8SPI_SPEECH_RECOGNIZING = 3
}tenSpeechAppState;

/*! 
* \enum tenPhoneAppState
* \brief SPI phone states
*/
typedef enum 
{
   //! Phone state is unknown/ not applicable
   e8SPI_PHONE_UNKNOWN = 0,

   //! Phone call is active
   e8SPI_PHONE_ACTIVE = 1,

   //! Phone call is not active.
   e8SPI_PHONE_NOT_ACTIVE = 2
}tenPhoneAppState;


/*! 
* \enum tenPhoneAppState
* \brief SPI Nav states
*/
typedef enum
{
   //! Nav state is unknown
   e8SPI_NAV_UNKNOWN = 0,

   //! Nav state is active
   e8SPI_NAV_ACTIVE = 1,

   //! Nav state is not active
   e8SPI_NAV_NOT_ACTIVE = 2
}tenNavAppState;

//!Vehicle Display Data
struct trVideoConfigData
{
   tenGMSystemVariant enSysVariant;
   t_U32 u32Screen_Width;
   t_U32 u32Screen_Height;
   t_U32 u32LayerId;
   t_U32 u32SurfaceId;
   t_U32 u32TouchLayerId;
   t_U32 u32TouchSurfaceId;
   t_U32 u32Screen_Width_Millimeter;
   t_U32 u32Screen_Height_Millimeter;
   t_U32 u32ProjScreen_Width;
   t_U32 u32ProjScreen_Height;
   t_U32 u32ProjScreen_Width_Mm;
   t_U32 u32ProjScreen_Height_Mm;
   t_U32 u32ResolutionSupported;
   
   //Please make sure that config reader files for all projects are updated,
   //when this structure is extended/updated.
};

//! Pixel formats for video rendering
enum tenPixelFormat
{
   e8_PIXELFORMAT_UNKNOWN = 0,
   e8_PIXELFORMAT_ARGB888 = 1,
   e8_PIXELFORMAT_RGB565 = 2
};


//! DayNight modes as per SystemState FBlock property
enum tenSystemStateDayNightMode
{
   e8DAY_MODE = 0,
   e8NIGHT_MODE = 1,
   //@Note: Please do not change values!
   //Values are assigned as per SystemState FI values.
};

//! DayNight modes as per CenterStackHMI FBlock property
enum tenCenterStackHmiDayNightOverride
{
   e8OVERRIDE_NOT_SUPPORTED = 0,
   e8OVERRIDE_AUTOMATIC = 1,
   e8OVERRIDE_DAYMODE = 2,
   e8OVERRIDE_NIGHTMODE = 3,
   //@Note: Please do not change above values!
   //Values are assigned as per CenterStackHMI FI values.
};

//! Enum for Onstarcall status
enum tenOnstarCallState
{
   e8ONSCALL_IDLE = 0x00,
   e8ONSCALL_ACTIVE = 0x01,
   e8ONSCALL_ONHOLD = 0x02,
   e8ONSCALL_CONFERENCE = 0x03,
   e8ONSCALL_DIALING = 0x04,
   e8ONSCALL_RINGING = 0x05,
   e8ONSCALL_BUSY = 0x06,
   e8ONSCALL_CONFERENCE_ONHOLD = 0x07
   //@Note: Please do not change above values!
   //Values are assigned as per OnstarPersonalCalling FI values.
};

//! Enum for Speech Recognition status
enum tenSpeechRecStatus
{
   e8SR_ACTIVE = 0x00,
   e8SR_INACTIVE = 0x01,
   e8SR_SUSPEND = 0x02,
   e8SR_INITIALIZING = 0x03
   //@Note: Please do not change above values!
   //Values are assigned as per SpeechHMI FI values.
};

//! SR Button event result
enum tenSpeechRecBtnEventResult
{
   e8NEW_SESSION_NOT_STARTED = 0,
   e8NEW_SESSION_STARTED = 1
};

//! Enum for Navigation Guidance
enum tenGuidanceStatus
{
   e8GUIDANCE_INACTIVE = 0x00,
   e8GUIDANCE_ACTIVE = 0x01,
   e8GUIDANCE_SUSPENDED = 0x02
   //@Note: Please do not change above values!
   //Values are assigned as per Navigation FI values.
};

//! defines audio errors
enum tenAudioError
{
   e8_AUDIOERROR_NONE = 0,
   e8_AUDIOERROR_UNKNOWN = 1,
   e8_AUDIOERROR_AVACTIVATION = 2,
   e8_AUDIOERROR_AVDEACTIVATION = 3,
   e8_AUDIOERROR_ALLOCATE = 4,
   e8_AUDIOERROR_DEALLOCATE = 5,
   e8_AUDIOERROR_STARTSOURCEACT = 6,
   e8_AUDIOERROR_STOPSOURCEACT = 7,
   e8_AUDIOERROR_GENERIC = 8
};
//! Pixel resolutions
enum tenPixelResolution
{
   e8_PIXEL_RES_800x480 = 0,
   e8_PIXEL_RES_640x320 = 1,
   e8_PIXEL_RES_320x240 = 2,
   e8_PIXEL_RES_1200x720 = 3
};

//! Indicates Drive side of vehicle
enum tenDriveSideInfo
{
   e8UNKNOWN_DRIVE_SIDE = 0,
   e8LEFT_HAND_DRIVE = 1,
   e8RIGHT_HAND_DRIVE = 2
};

//!Indicates the GM Vehicle Brand
enum tenVehicleBrand
{
   e8BUICK = 0x01,
   e8CADILLAC = 0x02,
   e8CHEVROLET = 0x03,
   e8GMC = 0x05,
   e8HOLDEN = 0x06,
   e8OPEL = 0x09,
   e8VAUXHALL = 0x0E,
   e8SGMBUICK = 0x15
};

enum tenAudioStatus
{
   e8AUDIO_STATUS_UNKNOWN = 0,
   e8AUDIO_STATUS_MEDIA_SETUP = 1,
   e8AUDIO_STATUS_MEDIA_TEARDOWN = 2,
   e8AUDIO_STATUS_MEDIA_RELEASE = 3,
   e8AUDIO_STATUS_RELEASE_AUDIO_DEVICE = 4,
};

enum tenTouchReleaseValidity
{
   e8P1Release = 0,
   e8P2Release = 1,
   e8P1P2Release = 2
};

//! Onstar Data setting event enum.
enum tenOnstarSettingsEvents
{
   e8_ONSTAR_FU_RESEST = 0x00,
   e8_ONSTAR_CALL_CONNECTING = 0x01,
   e8_ONSTAR_CALL_CONNECTED = 0x02,
   e8_ONSTAR_CALL_ENDED = 0x03,
   e8_ONSTAR_CALL_CONNECTION_FAILED =0x04,
   e8_ONSTAR_SHOWIN_VEHICLE_ALERT_DISPLAY = 0x05,
   e8_ONSTAR_ENDIN_VEHICLE_ALERT_DISPLAY = 0x06,
   e8_ONSTAR_DESTINATION_ENTRY_ASSIST =0x07,
   e8_ONSTAR_LANGUAGE_CHANGE = 0x09,
   e8_ONSTAR_SHOW_MIN_DIGITS = 0x0A,
   e8_ONSTAR_ACQ_SETTINGS = 0x0B,
   e8_ONSTAR_END_UAC_DISPLAY = 0x0C,
   e8_ONSTAR_INCOMING_CALL = 0x0D,
   e8_ONSTAR_SHOW_AUDIO_MESSAGE_TEXT = 0x0E,
   e8_ONSTAR_WELCOME_TEXT = 0x0F,
   e8_ONSTAR_XM_DEACTIVATED_TEXT = 0x11,
   e8_ONSTAR_MINUTES_TEXT = 0x12,
   e8_ONSTAR_MESSAGE_TEXT_END = 0x14,
   e8_ONSTAR_ACTIVATE_MESSAGE_DIALOGUE = 0x1B,
   e8_ONSTAR_ACTIVATE_1BUTTON_MESSAGE_DIALOGUE = 0x1C,
   e8_ONSTAR_ACTIVATE_2BUTTON_MESSAGE_DIALOGUE = 0x1D,
   e8_ONSTAR_TITILED_MENU_TEMPLATE = 0x1E,
   e8_ONSTAR_DEACTIVATE_MENU = 0x1F,
   e8_ONSTAR_TTY_FUNCTION_RESTRICTION = 0x20,
   e8_ONSTAR_PHONE_BUTTON_PRESSED = 0x21,
   e8_ONSTAR_CANCEL_NAVIGATION_ROUTE = 0x22
};

enum tenBTActivityStatus
{
  //! There is no call on Bluetooth
  e8_BT_NOACTIVITY  = 0x0,

  //! There is a ongoing BT call
  e8_BT_CALLONGOING = 0x1,

  //! There is a BT call which has been handled
  e8_BT_CALLHANDLED = 0x2
};

enum tenBTCallAction
{
  //! Accept the active incoming call
  e8_BT_CALLACCEPT = 0x0,

  //! End,Hanup or cancel any BT call
  e8_BT_CALLEND    = 0x1
};

typedef enum
{

  e8_ACTION_DOWN = 0,
  e8_ACTION_UP = 1,
  e8_ACTION_MOVED = 2,
  e8_ACTION_POINTER_DOWN = 5,
  e8_ACTION_POINTER_UP = 6,
  e8_ACTION_INVALID

}tenAAPPointerAction;

struct trAAPGestureMode
{
  //! SPI Touch Action
  tenTouchMode enTouchMode;
  //! Co-responding AAP Pointer Action
  tenAAPPointerAction enAAPPointerAction;

};


typedef enum 
{
   e8_DISPLAY_CONTEXT = 0,
   e8_DISPLAY_CONSTRAINT = 1
}__attribute__ ((packed, aligned (1))) tenDisplayInfo;


struct trDisplayContext
{
   tenDisplayContext enDisplayContext;
   t_Bool bDisplayFlag;
};

typedef enum
{
    e8DIPO_TRANSFERTYPE_NA       = 0,
    e8DIPO_TRANSFERTYPE_TAKE     = 1,
    e8DIPO_TRANSFERTYPE_UNTAKE   = 2,
    e8DIPO_TRANSFERTYPE_BORROW   = 3,
    e8DIPO_TRANSFERTYPE_UNBORROW = 4
} __attribute__ ((packed, aligned (1))) tenDiPOTransferType;


typedef enum
{
    e8DIPO_TRANSFERPRIO_NA            = 0,
    e8DIPO_TRANSFERPRIO_NICETOHAVE    = 1,
    e8DIPO_TRANSFERPRIO_USERINITIATED = 2
} __attribute__ ((packed, aligned (1))) tenDiPOTransferPriority;

typedef enum
{
    e8DIPO_CONSTRAINT_NA            = 0,
    e8DIPO_CONSTRAINT_ANYTIME       = 1,
    e8DIPO_CONSTRAINT_USERINITIATED = 2,
    e8DIPO_CONSTRAINT_NEVER         = 3
} __attribute__ ((packed, aligned (1))) tenDiPOConstraint;

/*! 
* \struct rDisplayConstraint
* \brief structure to hold video constraint data
*/
struct trDisplayConstraint
{
   //! Transfer type
   tenDiPOTransferType enTransferType;

   //! Transfer priority
   tenDiPOTransferPriority enTransferPriority;

   //! Transfer constraint
   tenDiPOConstraint enTakeConstraint;

   //! Transfer borrow constraint
   tenDiPOConstraint enBorrowConstraint;

   trDisplayConstraint& operator=(trDisplayConstraint &DiPODispConst)
   {   
      enTransferType = DiPODispConst.enTransferType;
      enTransferPriority = DiPODispConst.enTransferPriority;
      enTakeConstraint = DiPODispConst.enTakeConstraint;
      enBorrowConstraint = DiPODispConst.enBorrowConstraint;
      return *this;
   }
};

struct trDisplayLayerAttributes
{
   tenDeviceCategory enDevCat;
   t_U16 u16TouchLayerID;
   t_U16 u16TouchSurfaceID;
   t_U16 u16VideoLayerID;
   t_U16 u16VideoSurfaceID;
   t_U16 u16LayerWidth;
   t_U16 u16LayerHeight;
   t_U16 u16DisplayHeightMm;
   t_U16 u16DisplayWidthMm;

   trDisplayLayerAttributes():enDevCat(e8DEV_TYPE_UNKNOWN),u16TouchLayerID(0),
      u16TouchSurfaceID(0),u16VideoLayerID(0),u16VideoSurfaceID(0),
      u16LayerWidth(0),u16LayerHeight(0),u16DisplayHeightMm(0),
      u16DisplayWidthMm(0){}

   trDisplayLayerAttributes& operator=(const trDisplayLayerAttributes& corfrSrc)
   {
      if( &corfrSrc != this)
      {
         //copy info from corfrSrc
         enDevCat = corfrSrc.enDevCat;
         u16TouchLayerID = corfrSrc.u16TouchLayerID;
         u16TouchSurfaceID = corfrSrc.u16TouchSurfaceID;
         u16VideoLayerID = corfrSrc.u16VideoLayerID;
         u16VideoSurfaceID =corfrSrc.u16VideoSurfaceID;
         u16LayerWidth = corfrSrc.u16LayerWidth;
         u16LayerHeight = corfrSrc.u16LayerHeight;
         u16DisplayHeightMm = corfrSrc.u16DisplayHeightMm;
         u16DisplayWidthMm = corfrSrc.u16DisplayWidthMm;
      }//if( &corfrSrc != this)
      return *this;
   }//trDisplayLayerAttributes&

   t_Bool operator!=(const trDisplayLayerAttributes& corfrSrc)
   {
      t_Bool bRet=false;

      bRet = ( (enDevCat != corfrSrc.enDevCat) ||
         (u16TouchLayerID != corfrSrc.u16TouchLayerID) ||
         (u16TouchSurfaceID != corfrSrc.u16TouchSurfaceID) ||
         (u16VideoLayerID != corfrSrc.u16VideoLayerID) ||
         (u16VideoSurfaceID != corfrSrc.u16VideoSurfaceID) ||
         (u16LayerWidth != corfrSrc.u16LayerWidth) ||
         (u16LayerHeight != corfrSrc.u16LayerHeight) ||
         (u16DisplayHeightMm != corfrSrc.u16DisplayHeightMm) ||
         (u16DisplayWidthMm != corfrSrc.u16DisplayWidthMm)
         );

      return bRet;
   }//t_Bool operator!=(const trDis

};

struct trDisplayAttributes
{
   t_U16 u16ScreenHeight;
   t_U16 u16ScreenWidth;
   t_U16 u16ScreenHeightMm;
   t_U16 u16ScreenWidthMm;
   std::vector<trDisplayLayerAttributes> vecDisplayLayerAttr;

   trDisplayAttributes():u16ScreenHeight(0),u16ScreenWidth(0),u16ScreenHeightMm(0),
      u16ScreenWidthMm(0){}

   trDisplayAttributes& operator=(const trDisplayAttributes& corfrSrc)
   {
      if( &corfrSrc != this)
      {
         //copy info from corfrSrc
         u16ScreenHeight = corfrSrc.u16ScreenHeight;
         u16ScreenWidth = corfrSrc.u16ScreenWidth;
         u16ScreenHeightMm = corfrSrc.u16ScreenHeightMm;
         u16ScreenWidthMm = corfrSrc.u16ScreenWidthMm;
         vecDisplayLayerAttr =corfrSrc.vecDisplayLayerAttr;
      }//if( &corfrSrc != this)
      return *this;
   }//trDisplayAttributes& operator=(c

   t_Bool operator!=(const trDisplayAttributes& corfrSrc)
   {
      t_Bool bRet=false;

      bRet = ( (u16ScreenHeight != corfrSrc.u16ScreenHeight) ||
         (u16ScreenWidth != corfrSrc.u16ScreenWidth) ||
         (u16ScreenHeightMm != corfrSrc.u16ScreenHeightMm) ||
         (u16ScreenWidthMm != corfrSrc.u16ScreenWidthMm) ||
         (vecDisplayLayerAttr.size() != corfrSrc.vecDisplayLayerAttr.size())
         );

      if(false == bRet)
      {
         for(t_U8 u8Index=0;u8Index<vecDisplayLayerAttr.size();u8Index++)
         {
            if(true == (vecDisplayLayerAttr[u8Index] != corfrSrc.vecDisplayLayerAttr[u8Index]))
            {
               bRet=true;
               break;
            }//if(true == (vecDisplayLaye
         }//for(t_U8 u8Index=0;u8Ind
      }// if(false == bRet)
      return bRet;
   }
};

typedef enum
{
   //! If the key is of button type
   e8_BUTTON_TYPE = 0x0,

   //! If the key is of Knob type
   e8_KNOB_TYPE   = 0x1
}tenKeyType;


//! enum to define different audio configuration values
enum tenAudioStreamConfig
{
   e8AUDIOCONFIG_MAINAUDIO_24kHz = 0,
   e8AUDIOCONFIG_MAINAUDIO_16kHz = 1,
   e8AUDIOCONFIG_MAINAUDIO_8kHz = 2,
   e8AUDIOCONFIG_MAINAUDIO_MEDIA_BROWSING = 3,
   e8AUDIOCONFIG_MAINAUDIO_MEDIA_STANDALONE = 4,
   e8AUDIOCONFIG_MAINAUDIO_ALERT = 5,
   e8AUDIOCONFIG_ALTERNATEAUDIO = 6,
   e8AUDIOCONFIG_MAINAUDIO_SPEECH = 7,
   e8AUDIOCONFIG_MAINAUDIO_DUMMY = 9,
   e8AUDIOCONFIG_ALERT = 10,
   e8AUDIOCONFIG_AUDIOIN_24kHz =11,
   e8AUDIOCONFIG_AUDIOIN_16kHz =12,
   e8AUDIOCONFIG_AUDIOIN_8kHz =13,

   //! Please add new elements before ths line to ensure size is amintained
   e8AUDIOCONFIG_SIZE
};

//! Structure holding the details of the headunit
struct trHeadUnitInfo
{
      t_String szHUManufacturer;
      t_String szModelYear;
      t_String szHUModelName;
      t_String szVehicleManufacturer;
      t_String szVehicleModelName;
      tenDriveSideInfo enDrivePosition;
      t_String szSoftwareVersion;
      t_String szVehicleID;
      trHeadUnitInfo():enDrivePosition(e8UNKNOWN_DRIVE_SIDE)
      {

      }
};


typedef std::map<tenAudioStreamConfig, t_String> tmapAudioPipeConfig;

enum tenAudioChannelStatus
{
   e8_AUD_NOT_ACTIVE = 0,      // Audio channel is not active
   e8_AUD_ACT_REQUESTED = 1,   // Audio channel activation is requested
   e8_AUD_ACT_GRANTED = 2,     // Audio channel is active
   e8_AUD_DEACT_REQUESTED = 3, // Audio channel deactivation is requested
};
//! indicates the certificate type to be used (used only for android auto as of now)
enum tenCertificateType
{
   e8_CERTIFICATETYPE_DEVELOPER = 0,
   e8_CERTIFICATETYPE_SDC = 1,
   e8_CERTIFICATETYPE_FFS = 2
};

//! Enum for authorization status
enum tenUserAuthorizationStatus
{
   e8_USER_AUTH_UNKNOWN = 0x00,
   e8_USER_AUTH_UNAUTHORIZED = 0x01,
   e8_USER_AUTH_AUTHORIZED = 0x02
};

//! Device Authorization status
struct trDeviceAuthInfo
{
      t_U32 u32DeviceHandle;
      tenDeviceType enDeviceType;
      tenUserAuthorizationStatus enUserAuthorizationStatus;
      trDeviceAuthInfo():u32DeviceHandle(0), enDeviceType(e8_UNKNOWN_DEVICE),enUserAuthorizationStatus(e8_USER_AUTH_UNKNOWN)
      {

      }
};
struct trDisplayDpi
{
   t_U16 u16Dpi480p;
   t_U16 u16Dpi720p;
   t_U16 u16Dpi1080p;

   trDisplayDpi():u16Dpi480p(160),
                  u16Dpi720p(240),
                  u16Dpi1080p(240)
   {

   }
};
//! Device Selection Modes
enum tenDeviceSelectionMode
{
   e16DEVICESEL_UNKNOWN = 0,
   e16DEVICESEL_MANUAL = 1,
   e16DEVICESEL_AUTOMATIC = 2,
   e16DEVICESEL_SEMI_AUTOMATIC = 3
};

typedef enum  {
  e8_TOUCHSCREEN_CAPACITIVE = 1,
  e8_TOUCHSCREEN_RESISTIVE = 2
}tenAAPTouchScreenType;

struct trDataServiceConfigData
{
    t_Bool bLocDataAvailable;
    t_Bool bDeadReckonedData;
    t_Bool bEnvData;
    t_Bool bGearStatus;
    t_Bool bAcclData;
    t_Bool bGyroData;

    trDataServiceConfigData():bLocDataAvailable(true),bDeadReckonedData(false),bEnvData(false),bGearStatus(false),bAcclData(false),bGyroData(false)
    {

    }

};

//! \brief Structure holding notification's acknowledgement data
struct trNotificationAckData
{
   t_U32 u32DeviceHandle;
   tenDeviceCategory enDeviceCategory;
   t_String szNotifId;

   trNotificationAckData():u32DeviceHandle(0),
      enDeviceCategory(e8DEV_TYPE_UNKNOWN),
      szNotifId("")
   {
   }
};


#endif //SPITYPES_H_
