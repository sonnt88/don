/*!
 *******************************************************************************
 * \file             spi_tclBluetoothPolicyBase.h
 * \brief            Base Class for Bluetooth Policy
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Base Class for Bluetooth Policy
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 21.02.2014 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 24.11.2014 |  Ramya Murthy (RBEI/ECP2)         | Added BT block/unblock

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLBLUETOOTHPOLICYBASE_H
#define SPI_TCLBLUETOOTHPOLICYBASE_H

#include "BaseTypes.h"
#include "spi_BluetoothTypedefs.h"

/**
 *  class definitions.
 */


/**
 * This class is the base class for the Bluetooth policy class which implements the
 * Bluetooth interface for different projects.
 */
class spi_tclBluetoothPolicyBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothPolicyBase::spi_tclBluetoothPolicyBase();
   ***************************************************************************/
   /*!
   * \fn      spi_tclBluetoothPolicyBase()
   * \brief   Parameterised Constructor
   * \param   NONE
   **************************************************************************/
   spi_tclBluetoothPolicyBase(){}

   /***************************************************************************
   ** FUNCTION:  spi_tclBluetoothPolicyBase::~spi_tclBluetoothPolicyBase();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclBluetoothPolicyBase()
   * \brief   Virtual Destructor
   **************************************************************************/
   virtual ~spi_tclBluetoothPolicyBase(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vRegisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vRegisterForProperties()
   * \brief   Registers for interested properties to Bluetooth Service.
   **************************************************************************/
   virtual t_Void vRegisterForProperties() {};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vUnregisterForProperties()
   **************************************************************************/
   /*!
   * \fn      vUnregisterForProperties()
   * \brief   Registers for interested properties to Bluetooth Service.
   **************************************************************************/
   virtual t_Void vUnregisterForProperties() {};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterCallbacks(trBluetoothCallbacks rBTRespCallbacks)
   * \brief   Interface to register for BT connection callbacks.
   *          Optional interface to be implemented.
   * \param   [IN] rBTRespCallbacks: Callbacks structure
   * \retval  None
   **************************************************************************/
   virtual t_Void vRegisterCallbacks(trBluetoothCallbacks rBTRespCallbacks) {};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vRegisterPairingInfoCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterPairingInfoCallbacks(trBluetoothPairingCallbacks rBTPairInfoCallbacks)
   * \brief   Interface to register for BT pairing info callbacks.
   *          Optional interface to be implemented.
   * \param   [IN] rBTPairInfoCallbacks: Callbacks structure
   * \retval  None
   **************************************************************************/
   virtual t_Void vRegisterPairingInfoCallbacks(trBluetoothPairingCallbacks rBTPairInfoCallbacks) {};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vRegisterDeviceNameCallback()
   ***************************************************************************/
   /*!
   * \fn      vRegisterDeviceNameCallback(trBTDeviceNameCbInfo rBTDeviceNameCbInfo)
   * \brief   Interface to register for device name callback of a specific device
   *          Optional interface to be implemented.
   * \param   [IN] rBTDeviceNameCbInfo: Callback info structure
   * \retval  None
   **************************************************************************/
   virtual t_Void vRegisterDeviceNameCallback(trBTDeviceNameCbInfo rBTDeviceNameCbInfo) {};

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bSendPairingAction(...)
   ***************************************************************************/
   /*!
   * \fn      bSendPairingAction(tenBTPairingAction enAction)
   * \brief   Request from Bluetooth Manager to Bluetooth service for sending
   *          Pairing response.
   *          Optional interface to be implemented.
   * \param   [IN] enAction: Action for pairing
   * \retval  Bool value: TRUE - if response is sent successfully, else FALSE.
   **************************************************************************/
   virtual t_Bool bSendPairingAction(tenBTPairingAction enAction) { return false; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bConnectBTDevice(t_String...)
   ***************************************************************************/
   /*!
   * \fn      bConnectBTDevice(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          to connect a BT device. 
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device to be connected.
   * \retval  t_Bool: true - if Connect request is sent successully to BT Service, else false.
   **************************************************************************/
   virtual t_Bool bConnectBTDevice(const t_String& rfcoszDeviceBTAddress) { return false; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bDisconnectBTDevice(const...)
   ***************************************************************************/
   /*!
   * \fn      bDisconnectBTDevice(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          to disconnect a BT device. 
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device to be disconnected.
   * \retval  t_Bool: true - if Disconnect request is sent successully to BT Service, else false.
   **************************************************************************/
   virtual t_Bool bDisconnectBTDevice(const t_String& rfcoszDeviceBTAddress) { return false; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bBlockBTDevice(...)
   ***************************************************************************/
   /*!
   * \fn      bBlockBTDevice(tenBTDeviceBlockType enBlockType,
   *             const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          to block connection/pairing of one or more BT devices.
   *          Optional interface to be implemented.
   * \param   [IN] enBlockType: Type of blocking to be performed
   * \param   [IN] rfcoszDeviceBTAddress: BT device address of device to be blocked
   *               (ignored if Blocking type is for all devices)
   * \retval  t_Bool: true - if Block request is sent successully to BT Service, else false.
   **************************************************************************/
   virtual t_Bool bBlockBTDevice(tenBTDeviceBlockType enBlockType,
         const t_String& rfcoszDeviceBTAddress) { return false; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bUnblockBTDevice(...)
   ***************************************************************************/
   /*!
   * \fn      bUnblockBTDevice(tenBTDeviceUnblockType enUnblockType,
   *             const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          to unblock connection/pairing of one or more BT devices.
   *          Optional interface to be implemented.
   * \param   [IN] enUnblockType: Type of unblocking to be performed
   * \param   [IN] rfcoszDeviceBTAddress: BT device address of device to be unblocked
   *               (ignored if Blocking type is for all devices)
   * \retval  t_Bool: true - if Unblock request is sent successully to BT Service, else false.
   **************************************************************************/
   virtual t_Bool bUnblockBTDevice(tenBTDeviceUnblockType enUnblockType,
         const t_String& rfcoszDeviceBTAddress) { return false; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bGetPairingStatus(const...)
   ***************************************************************************/
   /*!
   * \fn      bGetPairingStatus(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          for the pairing status of a BT device.
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device.
   * \retval  Bool value: TRUE - if Pairing is required, else FALSE
   **************************************************************************/
   virtual t_Bool bGetPairingStatus(const t_String& rfcoszDeviceBTAddress) { return true; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bGetConnectionStatus(const...)
   ***************************************************************************/
   /*!
   * \fn      bGetConnectionStatus(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from the Bluetooth Manager to the Bluetooth service
   *          for the connection status of a BT device.
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device.
   * \retval  Bool value: TRUE - if Device is connected(active), else FALSE
   **************************************************************************/
   virtual t_Bool bGetConnectionStatus(const t_String& rfcoszDeviceBTAddress)  { return true; };

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclBluetoothPolicyBase::szGetConnectedDeviceBTAddress()
   ***************************************************************************/
   /*!
   * \fn      szGetConnectedDeviceBTAddress()
   * \brief   Request from Bluetooth Manager to the Bluetooth service
   *          for the BT address of the currently connected BT device.
   *          Optional interface to be implemented.
   * \retval  t_String : BT address of Connected(Active) device.
   *            If no device is connected, returns empty string.
   **************************************************************************/
   virtual t_String szGetConnectedDeviceBTAddress()  { return ""; };

   /***************************************************************************
   ** FUNCTION:  t_U32 spi_tclBluetoothPolicyBase::u32GetBTDeviceHandle(const...)
   ***************************************************************************/
   /*!
   * \fn      u32GetBTDeviceHandle(const t_String& rfcoszDeviceBTAddress)
   * \brief   Request from Bluetooth Manager to the Bluetooth Client
   *          for the BT DeviceHandle of a device.
   *          Optional interface to be implemented.
   * \param   [IN] rfcoszDeviceBTAddress: BT address of device.
   * \retval  t_U32 : BT device handle of device with BT address = rfcoszDeviceBTAddress.
   *          If the device is not found in BT DeviceList, zero is returned.
   **************************************************************************/
   virtual t_U32 u32GetBTDeviceHandle(const t_String& rfcoszDeviceBTAddress)  { return 0; };

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vGetVehicleBTAddress(const...)
   ***************************************************************************/
   /*!
   * \fn      vGetVehicleBTAddress()
   * \brief   Function to get the vehicle BT address
   * \param   szVehicleBtId : [IN]Vehicle BT address
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vGetVehicleBTAddress(t_String &szVehicleBtId) { szVehicleBtId = ""; };

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclBluetoothPolicyBase::vSetBTProfile(tenBTProfile...)
   ***************************************************************************/
   /*!
   * \fn      vSetBTProfile(tenBTProfile enBTProfile)
   * \brief   Interface to enable/disable BT profiles.
   *          Optional interface to be implemented.
   * \param   enBTProfile : [IN] Identifies BT profile to be set
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vSetBTProfile(tenBTProfile enBTProfile) {};

   /***************************************************************************
   ** FUNCTION:  t_String spi_tclBluetoothPolicyBase::szGetBTDeviceName()
   ***************************************************************************/
   /*!
   * \fn      szGetBTDeviceName()
   * \brief   Interface to fetch name of a BT device.
   * \param   [IN] rfcszDeviceBTAddress: BT address of device.
   * \retval  t_String : BT device name of requested device.
   *            If device name is not available, returns empty string.
   **************************************************************************/
   virtual t_String szGetBTDeviceName(const t_String& rfcszDeviceBTAddress)  { return ""; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bConfigureHUState(...)
   ***************************************************************************/
   /*!
   * \fn      bConfigureHUState()
   * \brief   Interface to configure HU to pairable and/or connectable state
   *          for a specific device.
   *          Optional interface to be implemented.
   * \param   [IN] enPairableMode: Indicates desired pairable state
   * \param   [IN] enConnectableMode: Indicates desired connectable state
   * \param   [IN] rfcszDeviceBTAddress: BT address of device.
   * \retval  t_Bool : True - if request to configure HU is sent successfully,
   *              else false.
   *              Also, returns false if device address is invalid.
   **************************************************************************/
   virtual t_Bool bConfigureHUState(tenBTLocalModeType enPairableMode,
         tenBTLocalModeType enConnectableMode, const t_String& rfcszDeviceBTAddress) { return false; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bSwitchBTOnOff(...)
   ***************************************************************************/
   /*!
   * \fn      bSwitchBTOnOff()
   * \brief   Interface to Switch On/Off Bluetooth in HU.
   *          Optional interface to be implemented.
   * \param   [IN] bBluetoothOnOff:
   *              True - If BT is to be switched On
   *              False - If BT is to be switched Off
   * \retval  t_Bool : True - if request to configure HU is sent successfully,
   *              else false.
   **************************************************************************/
   virtual t_Bool bSwitchBTOnOff(t_Bool bBluetoothOnOff) { return false; };

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclBluetoothPolicyBase::bGetBTOnStatus(...)
   ***************************************************************************/
   /*!
   * \fn      bGetBTOnStatus()
   * \brief   Interface to get Bluetooth On/Off status in HU.
   *          Optional interface to be implemented.
   * \param   None
   * \retval  t_Bool : True - if BT is On, else false.
   **************************************************************************/
   virtual t_Bool bGetBTOnStatus() { return false; };
};

#endif //SPI_TCLBLUETOOTHPOLICYBASE_H
