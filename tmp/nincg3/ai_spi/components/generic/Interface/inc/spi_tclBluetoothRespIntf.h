/*!
 *******************************************************************************
 * \file             spi_tclBluetoothRespIntf.h
 * \brief            Response to HMI from Bluetooth manager class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Response to HMI from Bluetooth manager class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 08.03.2014 |  Ramya Murthy                | Initial Version
 07.10.2014 |  Ramya Murthy                | Implementation for BTPairingRequired property

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLBLUETOOTHRESPINTF_H_
#define SPI_TCLBLUETOOTHRESPINTF_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclBluetoothRespIntf
 * \brief Response to HMI from Bluetooth manager class
 */
class spi_tclBluetoothRespIntf
{

   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclBluetoothRespIntf::spi_tclBluetoothRespIntf
       ***************************************************************************/
      /*!
       * \fn     spi_tclBluetoothRespIntf()
       * \brief  Default Constructor
       * \sa     ~spi_tclBluetoothRespIntf()
       **************************************************************************/
      spi_tclBluetoothRespIntf(){}

      /***************************************************************************
       ** FUNCTION:  spi_tclBluetoothRespIntf::~spi_tclBluetoothRespIntf
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclBluetoothRespIntf()
       * \brief  Virtual Destructor
       * \sa     spi_tclBluetoothRespIntf()
       **************************************************************************/
      virtual ~spi_tclBluetoothRespIntf(){}

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclBluetoothRespIntf::vPostBluetoothDeviceStatus(...)
       ***************************************************************************/
      /*!
       * \fn     vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
       *            t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTStatus,
       *            t_Bool bCallActive)
       * \brief  It notifies the client when changing from or to a BT device.
       * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
       * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
       * \param  [IN] bSameDevice : Inidcates whether BT & Projection device are same
       *              or different devices.
       * \param  [IN] enBTStatus  : Enum value which stores BT device status
       **************************************************************************/
      virtual t_Void vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
            t_U32 u32ProjectionDevHandle,
            tenBTChangeInfo enBTChange,
            t_Bool bSameDevice,
            t_Bool bCallActive) = 0;

      /***************************************************************************
       ** FUNCTION: t_Void spi_tclBluetoothRespIntf::vPostBTPairingRequired(...)
       ***************************************************************************/
      /*!
       * \fn     vPostBTPairingRequired(const t_String& rfcoszBTAddress, t_Bool bPairingRequired)
       * \brief  It notifies the client when changing from or to a BT device.
       * \param  [IN] rfcoszBTAddress  : BT Address of device.
       * \param  [IN] bPairingRequired : true - if device needs to be paired, else false.
       **************************************************************************/
      virtual t_Void vPostBTPairingRequired(const t_String& rfcoszBTAddress,
            t_Bool bPairingRequired) = 0;

};
#endif // SPI_TCLBLUETOOTHRESPINTF_H_
