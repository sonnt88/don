/*!
 *******************************************************************************
 * \file              spi_tclConfigReaderBase.h
 * \brief             Common  Base Class  SPI Config Reader for all Projects
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Common  Base Class  SPI Config Reader for all Projects
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 29.10.2015 |  Dhiraj Asopa                | Initial Version

 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLCONFIGREADERBASE_H_
#define SPI_TCLCONFIGREADERBASE_H_

#include "SPITypes.h"
/**
 * This class is the base class for config readers used in SPI.
 */

class spi_tclConfigReaderBase
{
public:

  /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/


  /***************************************************************************
   ** FUNCTION:  spi_tclConfigReader::~spi_tclConfigReaderBase()
   ***************************************************************************/
  /*!
   * \fn      virtual ~spi_tclConfigReaderBase()
   * \brief   Destructor
   * \sa      spi_tclConfigReaderBase()
   **************************************************************************/
  virtual ~spi_tclConfigReaderBase();

  /***************************************************************************
   ** FUNCTION:  t_String spi_tclConfigReaderBase::szGetVehicleId()
   ***************************************************************************/
  /*!
   * \fn      szGetVehicleId()
   * \brief   Method to get VehicleId information
   * \retval  t_String
   **************************************************************************/
  virtual t_String szGetVehicleId(){return t_String();};

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclConfigReaderBase::vGetVehicleInfoDataAAP(
                      trVehicleBrandInfo& rfrVehicleBrandInfo)
   ***************************************************************************/
  /*!
   * \fn      t_Void vGetVehicleInfoDataAAP(trVehicleBrandInfo& rfrVehicleBrandInfo)
   * \brief   Method to read the EOL value and get the Brand configuration data
               from the look up table
   * \param   rfrVideoConfig: [IN]Vehicle Brand Data
   * \retval  NONE
   **************************************************************************/
  virtual  t_Void vGetVehicleInfoDataAAP(trVehicleInfo& rfrVehicleInfo){return;}

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclConfigReaderBase::vGetDefaultProjUsageSettings
   ***************************************************************************/
  /*!
   * \fn      t_Void vGetDefaultProjUsageSettings()
   * \brief  Method to retrieve default setting for projection usage
   * \param [OUT] : returns the current value of device projection enable
   * \enSPIType  : indicates the type of SPI technology. e8DEV_TYPE_UNKNOWN indicates default value for any SPI technology
   * \retval t_Void
   **************************************************************************/
  virtual  t_Void vGetDefaultProjUsageSettings(tenDeviceCategory enSPIType, tenEnabledInfo &enEnabledInfo) {enEnabledInfo = e8USAGE_ENABLED;};

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclConfigReaderBase::vGetTouchScreenType
   ***************************************************************************/
  /*!
   * \fn      t_Void vGetTouchScreenType()
   * \brief  Method to retrieve default setting of Touch Screen Type
   * \param [OUT] : returns the value of Touch screen
   * \retval t_Void
   **************************************************************************/
  virtual  t_Void vGetTouchScreenType(tenAAPTouchScreenType &rfenScreenType){rfenScreenType = e8_TOUCHSCREEN_RESISTIVE;}

  /***************************************************************************
   ** FUNCTION:  spi_tclConfigReaderBase::spi_tclConfigReaderBase();
   ***************************************************************************/
  /*!
   * \fn      spi_tclConfigReaderBase()
   * \brief   Default Constructor
   * \param   None
   **************************************************************************/
  spi_tclConfigReaderBase();

  /***************************************************************************
   ** FUNCTION:  spi_tclConfigReaderBase& spi_tclConfigReaderBase::operator= (const..
   ***************************************************************************/
  /*!
   * \fn      spi_tclConfigReaderBase& operator= (const spi_tclConfigReaderBase &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
  spi_tclConfigReaderBase& operator= (const spi_tclConfigReaderBase &corfrSrc);

  /***************************************************************************
   ** FUNCTION:  spi_tclConfigReaderBase::spi_tclConfigReaderBase(const ..
   ***************************************************************************/
  /*!
   * \fn      spi_tclConfigReader(const spi_tclConfigReader &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
  spi_tclConfigReaderBase(const spi_tclConfigReaderBase &corfrSrc);


  /***************************************************************************
   ****************************END OF PUBLIC *********************************
   ***************************************************************************/

};

#endif  // SPI_TCLCONFIGREADERBASE_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
