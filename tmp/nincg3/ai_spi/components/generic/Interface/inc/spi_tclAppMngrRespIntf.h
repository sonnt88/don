/***********************************************************************/
/*!
* \file  spi_tclAppMngrRespIntf.h
* \brief Application Manager Response Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Mirror Link Application Manager
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
18.02.2014  | Shiva Kumar Gurija    | Initial Version
20.03.2014  | Shihabudheen P M      | Added vPostDeviceDisplayContext()
Handling Devices and App's Info
26.02.2016  | Rachana L Achar       | AAP Navigation implementation
10.03.2016  | Rachana L Achar       | AAP Notification implementation

\endverbatim
*************************************************************************/
#ifndef SPI_TCL_APPMNGR_RESPINTF
#define SPI_TCL_APPMNGR_RESPINTF

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclAppMngrDefines.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class spi_tclAppMngrRespIntf
* \brief Application Manager Response Interface
****************************************************************************/
class spi_tclAppMngrRespIntf
{
   public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngrRespIntf::spi_tclAppMngrRespIntf()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAppMngrRespIntf()
   * \brief   Constructor
   * \sa      ~spi_tclAppMngrRespIntf()
   **************************************************************************/
      spi_tclAppMngrRespIntf()
      {
         //Add code
      }
   /***************************************************************************
   ** FUNCTION:  spi_tclAppMngrRespIntf::~spi_tclAppMngrRespIntf()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAppMngrRespIntf()
   * \brief   Destructor
   * \sa      spi_tclAppMngrRespIntf()
   **************************************************************************/
      virtual ~spi_tclAppMngrRespIntf()
      {
         //Add code
      }

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostAppStatusInfo
      ***************************************************************************/
      /*!
      * \fn     vPostAppStatusInfo(t_U32 u32DeviceHandle, 
      *             tenDeviceConnectionType enDevConnType, tenAppStatusInfo enAppStatus)
      * \brief  It notifies the client upon application list change of a Mirror Link 
      *         device. The client can retrieve the detailed information of the  
      *         applications via the methods provided. If a Mirror Link device is 
      *         disconnected, Device Handle is set to 0xFFFF and DeviceConnectionType 
      *         is set to UNKNOWN_CONNECTION.
      * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
      * \param  [IN] enDevConnType   : Identifies the Connection Type.
      * \param  [IN] enAppStatus : Provides application Status Information.
      **************************************************************************/
      virtual t_Void vPostAppStatusInfo(t_U32 u32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenAppStatusInfo enAppStatus)
      {
         //Add code
      }

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclRespInterface::vPostAppIconDataResult
      **                   (tenIconMimeType enIconMimeType, t_Char* pczAppIconData, ..)
      ***************************************************************************/
      /*!
      * \fn     vPostAppIconDataResult(tenIconMimeType enIconMimeType, 
      *            t_Char* pczAppIconData,t_U32 u32Len, const trUserContext rcUsrCntxt)
      * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
      * \param  [IN] enIconMimeType :  Mime Type of the icon pointed by AppIconURL.
      *              If image is not available then this parameter would be set
      *              to NULL (zero length string).
      * \param  [IN] pczAppIconData : Byte Data Stream from the icon image file.
      *              Format of the file is de-fined by IconMimeType parameter.
      * \param  [IN] u32Len : data stream length
      * \param  [IN] rcUsrCntxt		: User Context Details.
      * \sa     spi_tclCmdInterface::vGetAppIconData
      **************************************************************************/
      virtual t_Void vPostAppIconDataResult(tenIconMimeType enIconMimeType,
               const t_U8* pczAppIconData,
               t_U32 u32Len,
               const trUserContext rcUsrCntxt)
      {
         //Add code
      }

      /***************************************************************************
      ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostSetAppIconAttributesResult
      **             (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
      ***************************************************************************/
      /*!
      * \fn     vPostSetAppIconAttributesResult(tenResponseCode enResponseCode,
      *                          tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
      * \brief  sets application icon attributes for retrieval of application icons.
      * \param  [IN] enResponseCode :  Provides result from the operation.
      * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
      *              Set to NO_ERROR for successful operation.
	   * \param  [IN] rcUsrCntxt	 : User Context Details.
      * \sa     spi_tclCmdInterface::vSetAppIconAttributes
      **************************************************************************/
      virtual t_Void vPostSetAppIconAttributesResult(tenResponseCode enResponseCode,
               tenErrorCode enErrorCode, 
               const trUserContext rcUsrCntxt)
      {
         //Add code
      }
      //SetAppIconAttrResp
      //GetAppIconUrlResp

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostSetMLNotificationEnabledInfoResult()
   ***************************************************************************/
   /*!
   * \fn     vPostSetMLNotificationEnabledInfoResult(t_U32 u32DeviceHandle, 
   *            tenResponseCode enResponseCode, tenErrorCode enErrorCode, 
   *            const trUserContext rcUsrCntxt)
   * \brief  Interface to set the device notification preference for
   *         applications (only for Mirror Link devices). If notification for
   *         all the applications has to be
   * \param  [IN] u32DeviceHandle   : Uniquely identifies the target Device.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetMLNotificationEnabledInfo
   **************************************************************************/
   virtual t_Void vPostSetMLNotificationEnabledInfoResult(t_U32 u32DeviceHandle,
      tenResponseCode enResponseCode, 
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt)
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostNotificationInfo()
   ***************************************************************************/
   /*!
   * \fn     vPostNotificationInfo(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
   *            NotificationData &rfrNotificationData)
   * \brief  Interface to provide Notification Information received from
   *         the Mirror Link server (only for Mirror Link devices).
   * \param  [IN] u32DeviceHandle   :  Handle uniquely identifies a device.
   * \param  [IN] u32AppHandle      : Handle uniquely identifies an application 
   *              on the device.
   * \param  [IN] corfrNotificationData : Provides notification event details
   * \sa     spi_tclCmdInterface::vSetMLNotificationEnabledInfo
   **************************************************************************/
   virtual t_Void vPostNotificationInfo(t_U32 u32DeviceHandle,
      t_U32 u32AppHandle, 
      const trNotiData& corfrNotificationData)
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostInvokeNotificationActionResult()
   ***************************************************************************/
   /*!
   * \fn     vPostInvokeNotificationActionResult(tenResponseCode enResponseCode,
   *            tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
   * \brief  Interface to invoke the respective action for the
   *         received Notification Event.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt		 : User Context Details.
   * \sa     spi_tclCmdInterface::vInvokeNotificationAction
   **************************************************************************/
   virtual t_Void vPostInvokeNotificationActionResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode,
      const trUserContext rcUsrCntxt)
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vUpdateAppBlockingInfo()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vUpdateAppBlockingInfo(t_U32 u32DeviceHandle, 
   *             tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus)
   * \brief  It notifies the client about the Application blocking using the 
   *          session status update
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCat   : Identifies the Device category.
   * \param  [IN] enSessionStatus : Session status
   **************************************************************************/
   virtual t_Void vUpdateAppBlockingInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus){}

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vUpdateActiveAppInfo()
   ***************************************************************************/
   /*!
   * \fn     t_Void vUpdateActiveAppInfo(const t_U32 cou32DevId,
   *            const tenDeviceCategory coenDevCat,
   *            const t_U32 cou32AppId,
   *            const tenAppCertificationInfo coenAppCertInfo)
   * \brief  method to send the Active App update to HMI 
   * \param  cou32DevId       : [IN] Uniquely identifies the target Device.
   * \param  coenDevCat       : [IN] Device category
   * \param  cou32AppId       : [IN] AppId
   * \param  coenAppCertInfo  : [IN] Certification Status
   * \retval t_Void
   **************************************************************************/
   virtual t_Void vUpdateActiveAppInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      const t_U32 cou32AppId,
      const tenAppCertificationInfo coenAppCertInfo){}

   /************************************************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostNavigationStatus(...)
   ************************************************************************************************************/
   /*!
   * \fn     vPostNavigationStatus(const trNavStatusData& corfrNavStatusData)
   * \brief  It notifies the client whenever there is a navigation status change(ACTIVE/INACTIVE/UNAVAILABLE).
   * \param  corfrNavStatusData  : [IN] Structure containing the device handle,
   *                                    device category and navigation status
   ************************************************************************************************************/
   virtual t_Void vPostNavigationStatus(const trNavStatusData& corfrNavStatusData)
   {
      SPI_INTENTIONALLY_UNUSED(corfrNavStatusData);
   }

   /*******************************************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostNavigationNextTurnDataStatus(...)
   *******************************************************************************************************/
   /*!
   * \fn     vPostNavigationNextTurnDataStatus(const trNavNextTurnData& corfrNavNexTurnData)
   * \brief  It notifies the client whenever there is a navigation next turn event information.
   * \param  corfrNavNexTurnData : [IN] Structure containing device handle, device category, road name,
   *                                    next turn details such as side, event, image, angle and number
   *******************************************************************************************************/
   virtual t_Void vPostNavigationNextTurnDataStatus(const trNavNextTurnData& corfrNavNexTurnData)
   {
      SPI_INTENTIONALLY_UNUSED(corfrNavNexTurnData);
   }

   /*******************************************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostNavigationNextTurnDistanceDataStatus(...)
   *******************************************************************************************************/
   /*!
   * \fn     vPostNavigationNextTurnDistanceDataStatus(
   *          const trNavNextTurnDistanceData& corfrNavNextTurnDistData)
   * \brief  It notifies the client whenever there is a change in navigation next turn distance data.
   * \param  corfrNavNextTurnDistData : [IN] Structure containing device handle,
   *                                         device category, distance and time of the next turn
   *******************************************************************************************************/
   virtual t_Void vPostNavigationNextTurnDistanceDataStatus(
                   const trNavNextTurnDistanceData& corfrNavNextTurnDistData)
   {
      SPI_INTENTIONALLY_UNUSED(corfrNavNextTurnDistData);
   }

   /*******************************************************************************************************
   ** FUNCTION: t_Void spi_tclAppMngrRespIntf::vPostNotificationData(...)
   *******************************************************************************************************/
   /*!
   * \fn     vPostNotificationData(
   *          const trNotificationData& corfrNotificationData)
   * \brief  It notifies the client whenever a notification is received.
   * \param  corfrNotificationData : [IN] Structure containing device handle,
   *                                      device category and notification data
   *******************************************************************************************************/
   virtual t_Void vPostNotificationData(
                   const trNotificationData& corfrNotificationData)
   {
      SPI_INTENTIONALLY_UNUSED(corfrNotificationData);
   }

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};//spi_tclAppMngrRespIntf

#endif //SPI_TCL_APPMNGR_RESPINTF
