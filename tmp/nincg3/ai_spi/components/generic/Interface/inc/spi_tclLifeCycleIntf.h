/***********************************************************************/
/*!
 * \file  spi_tclLifeCycleIntf.h
 * \brief Life Cycle Interface for all sub-component managers
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    SPI Life Cycle Interface. To handle power states
 AUTHOR:         Deepti Samant
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 26.04.2014  | Deepti Samant         | Initial Version
 31.07.2014  |  Ramya Murthy         | Changes for SPI feature configuration via LoadSettings()
 \endverbatim
 *************************************************************************/
#ifndef SPI_TCL_LIFECYCLE_INTF
#define SPI_TCL_LIFECYCLE_INTF

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
 * \class spi_tclLifeCycleIntf
 * \brief Life Cycle Interface class
 ****************************************************************************/
class spi_tclLifeCycleIntf
{
   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclLifeCycleIntf::~spi_tclLifeCycleIntf()
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclLifeCycleIntf()
       * \brief   Destructor
       **************************************************************************/
      virtual ~spi_tclLifeCycleIntf() {}

      /***************************************************************************
       ** FUNCTION:  spi_tclLifeCycleIntf::bInitialize()
       ***************************************************************************/
      /*!
       * \fn      bInitialize()
       * \brief   Method to Initialize
       * \sa      bUnInitialize()
       **************************************************************************/
      virtual t_Bool bInitialize() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclLifeCycleIntf::bUnInitialize()
       ***************************************************************************/
      /*!
       * \fn      bUnInitialize()
       * \brief   Method to UnInitialize
       * \sa      bInitialize()
       **************************************************************************/
      virtual t_Bool bUnInitialize() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclLifeCycleIntf::vLoadSettings(const trSpiFeatureSupport&...)
       ***************************************************************************/
      /*!
       * \fn      vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
       * \brief   vLoadSettings Method. Invoked during OFF->NORMAL state transition.
       * \sa      vSaveSettings()
       **************************************************************************/
      virtual t_Void vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp) = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclLifeCycleIntf::vSaveSettings()
       ***************************************************************************/
      /*!
       * \fn      vSaveSettings()
       * \brief   vSaveSettings Method. Invoked during  NORMAL->OFF state transition.
       * \sa      vLoadSettings()
       **************************************************************************/
      virtual t_Void vSaveSettings() = 0;

      /***************************************************************************
       ** FUNCTION:  spi_tclLifeCycleIntf::vOnNewAppState()
       ***************************************************************************/
      /*!
       * \fn      vOnNewAppState()
       * \brief   vOnNewAppState Method. Invoked during application state transition.
       * \sa      
       **************************************************************************/
      virtual t_Void vOnNewAppState()
      {
      }
      /***************************************************************************
       ****************************END OF PUBLIC***********************************
       ***************************************************************************/

   protected:

      /***************************************************************************
       *********************************PROTECTED**********************************
       ***************************************************************************/

      /***************************************************************************
       ****************************END OF PROTECTED********************************
       ***************************************************************************/

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ****************************END OF PRIVATE *********************************
       ***************************************************************************/

};//End Of spi_tclLifeCycleIntf

#endif //SPI_TCL_LIFECYCLE_INTF
