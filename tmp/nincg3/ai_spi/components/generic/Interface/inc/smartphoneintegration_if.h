/*!
*******************************************************************************
* \file              smartphoneintegration_if.h
* \brief             Interface for datapool component to get the user defined types
                     by SPI
*******************************************************************************
\verbatim
PROJECT:       GM Gen3
SW-COMPONENT:  Smart Phone Integration
DESCRIPTION:   Interface for datapool component to get the user defined types
               by SPI
COPYRIGHT:     &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
24.06.2014 |  Shiva Kumar G               | Initial version

\endverbatim
******************************************************************************/

#ifndef SMARTPHONEINTEGRATION_IF_H_
   #define SMARTPHONEINTEGRATION_IF_H_
   #ifdef SPI_S_IMPORT_INTERFACE_DP_SPI
      #include "../../../utils/Datapool/inc/spi_tclDatapooldefs.h"
   #endif //#ifdef SPI_S_IMPORT_INTERFACE_DP_SPI

   #if !defined(SPI_S_IMPORT_INTERFACE_DP_SPI)
      #pragma message ( "You have to define an interface for the component: SPI." )
   #endif //#if !defined(SPI_S_IMPORT_INTERFACE_DP_SPI)
#endif //#define SMARTPHONEINTEGRATION_IF_H_
