/*!
*******************************************************************************
* \file             spi_tclResourceMngrResp.h
* \brief            Response to HMI from Resource manager class
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Response  to HMI  from Resource manager class.
AUTHOR:         Priju K Padiyath
COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
05.04.2014 |  Priju K Padiyath            | Initial Version
26.04.2014 |  Shihabudheen P M            | added vPostDeviceAudioContext
19.06.2014 |  Shihabudheen P M            | added vPostDeviceAppState
25.10.2014 |  Shihabudheen P M            | added vUpdateSessionStatusInfo

\endverbatim
******************************************************************************/

#ifndef SPI_TCLRSRCMNGRRESP_H_
#define SPI_TCLRSRCMNGRRESP_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "SPITypes.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \class spi_tclConnSettings
* \brief Response  to HMI  from ConnMngr class
*/
class spi_tclResourceMngrResp
{

public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrResp::spi_tclResourceMngrResp
   ***************************************************************************/
   /*!
   * \fn     spi_tclResourceMngrResp()
   * \brief  Default Constructor
   * \sa      ~spi_tclResourceMngrResp()
   **************************************************************************/
   spi_tclResourceMngrResp()
   {
   }

   /***************************************************************************
   ** FUNCTION:  spi_tclResourceMngrResp::~spi_tclResourceMngrResp
   ***************************************************************************/
   /*!
   * \fn     ~spi_tclResourceMngrResp()
   * \brief  Virtual Destructor
   * \sa     spi_tclResourceMngrResp()
   **************************************************************************/
   virtual ~spi_tclResourceMngrResp()
   {
   }


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngrResp::vPostDeviceDisplayContext
   **                   (t_U32 u32DeviceHandle, t_Bool bDisplayFlag,..)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceDisplayContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  This interface is used by Mirror Link/DiPO device to inform the client
   *              about its current display con-text.
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bDisplayFlag     : TRUE � Start Display Projection, FALSE � Stop Display Projection.
   * \param  [IN] enDisplayContext : Display context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   virtual t_Void vPostDeviceDisplayContext(t_U32 u32DeviceHandle,
      t_Bool bDisplayFlag,
      tenDisplayContext enDisplayContext,
      const trUserContext rcUsrCntxt)
   {
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngrResp::vPostDeviceAudioContext
   **                   (t_U32 u32DeviceHandle, t_Bool bPlayFlag,..)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceAudioContext()
   * \brief  This function used to update HMI about the audio context changes
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bPlayFlag        : TRUE � Start Display Projection playback, 
   *                                 FALSE � Stop Display Projection playback.
   * \param  [IN] u8AudioContext   : Audio context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   virtual t_Void vPostDeviceAudioContext(t_U32 u32DeviceHandle,
      t_Bool bPlayFlag, t_U8 u8AudioContext,
      const trUserContext rcUsrCntxt)
   {
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngrResp::vPostDeviceAppState
   **                   (tenSpeechAppState enSpeechAppState,...)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceAppState()
   * \brief  This function used to update HMI about the App state changes
   * \param  [IN] enSpeechAppState : Speech app state
   * \param  [IN] enPhoneAppState  : Phone App state
   * \param  [IN] enNavAppState    : Navigation App state
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   virtual t_Void vPostDeviceAppState(tenSpeechAppState enSpeechAppState, 
      tenPhoneAppState enPhoneAppState,  tenNavAppState enNavAppState, 
      const trUserContext rcUsrCntxt)
   {
   }

  /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngrResp::vUpdateSessionStatusInfo
   **                   (t_U32 u32DeviceHandle,...)
   ***************************************************************************/
   /*!
   * \fn     vUpdateSessionStatusInfo()
   * \brief  Used to update the session status to HMI.
   * \param  [IN] u32DeviceHandle  : Device handle 
   * \param  [IN] enDevCat         : Device category
   * \param  [IN] enSessionStatus  : Session status.
   **************************************************************************/
   virtual t_Void vUpdateSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus)
   {
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclResourceMngrResp::vUpdateDevAuthAndAccessInfo()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vUpdateDevAuthAndAccessInfo(const t_U32 cou32DeviceHandle,
   *             const t_Bool cobHandsetInteractionReqd)
   * \brief  Notifies the authorization and access to AAP projection device.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] cobHandsetInteractionReqd   : Set if interaction required on Handset.
   **************************************************************************/
   virtual t_Void vUpdateDevAuthAndAccessInfo(const t_U32 cou32DeviceHandle,
            const t_Bool cobHandsetInteractionReqd){}

   /***************************************************************************
   *****************************END OF PUBLIC*************************************
   ***************************************************************************/

};
#endif // SPI_TCLRSRCMNGRRESP_H_
