/*!
*******************************************************************************
* \file   spi_tclVideoRespInterface.h
* \brief  SPI Response interface for the Video
*******************************************************************************
\verbatim
PROJECT:       Gen3
SW-COMPONENT:  Smart Phone Integration
DESCRIPTION:   provides SPI response interface for the Project specific layer
COPYRIGHT:     &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
13.09.2013 |  Shiva Kumar Gurija          | Initial Version

\endverbatim
******************************************************************************/

#ifndef SPI_TCLVIDEORESPINTERFACE_H_
#define SPI_TCLVIDEORESPINTERFACE_H_

/******************************************************************************
| includes:
| 1)RealVNC sdk - includes
| 2)Typedefines
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \class spi_tclVideoRespInterface
* \brief This class provides response interface for the SPI Video 
*/
class spi_tclVideoRespInterface
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   spi_tclVideoRespInterface()
   {
      //add code
   }

   virtual ~spi_tclVideoRespInterface()
   {
      //add code
   }

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoRespInterface::vPostSetOrientationModeResult()
   ***************************************************************************/
   /*!
   * \fn     vPostSetOrientationModeResult(tenResponseCode enResponseCode,
   *                  tenErrorCode enErrorCode, const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  [IN] enResponseCode : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] corfrcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetOrientationMode
   **************************************************************************/
   virtual t_Void vPostSetOrientationModeResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext& corfrcUsrCntxt)=0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoRespInterface::vPostSetVideoBlockingModeResult()
   ***************************************************************************/
   /*!
   * \fn     vPostSetVideoBlockingModeResult(tenResponseCode enResponseCode,
   *                         tenErrorCode enErrorCode, const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the display blocking mode.
   * \param  [IN] enResponseCode : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] corfrcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetVideoBlockingMode
   **************************************************************************/
   virtual t_Void vPostSetVideoBlockingModeResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext& corfrcUsrCntxt)=0;

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclVideoRespInterface::vSendSessionStatusInfo()
   ***************************************************************************/
   /*!
   * \fn     virtual t_Void vSendSessionStatusInfo(t_U32 u32DeviceHandle, 
   *             tenDeviceCategory enDevCat, tenSessionStatus enSessionStatus)
   * \brief  It notifies the client about the ML Session status updates
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevCat   : Identifies the Device category.
   * \param  [IN] enSessionStatus : Session status
   **************************************************************************/
   virtual t_Void vSendSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus){}

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};//spi_tclVideoRespInterface



#endif //SPI_TCLVIDEORESPINTERFACE_H_