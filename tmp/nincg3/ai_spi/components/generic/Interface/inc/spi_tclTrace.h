/***********************************************************************/
/*!
* \file  spi_tclTrace.cpp
* \brief Trace Commands Handler
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Trace Commands Handler
AUTHOR:         Vinoop
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
11.02.2014  |Vinoop U               | Initial Version

\endverbatim
*************************************************************************/

#ifndef SPI_TCLIMPTRACECMDHANDLER_H_
#define SPI_TCLIMPTRACECMDHANDLER_H_
/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclVideoSettings.h"
#include "spi_tclAudioSettings.h"
#include "spi_trace_macros.h"
#include "Trace.h"

#include "TraceStreamable.h"


#include "spi_tclAudio.h"
#include "spi_tclAppSettings.h"
using namespace spi::spitrace;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
enum tenMLCertiType
{
   e8_CCCCERTIFICATE = 0,
   e8_CTSCERTIFICATE = 1
};

class spi_tclTrace
{
public:
   /********************************************************************************
   ** FUNCTION   : spi_tclTrace::spi_tclTrace()
   /*******************************************************************************
   * \fn     spi_tclTrace::spi_tclTrace()
   * \brief  Constructor.
   * \retval  None.
   *******************************************************************************/
   spi_tclTrace(ahl_tclBaseOneThreadApp *poMapp);

   /***************************************************************************
   ** FUNCTION:  spi_tclTrace::~spi_tclTrace();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclTrace()
   * \brief   Virtual Destructor
   **************************************************************************/
   virtual ~spi_tclTrace();

   /********************************************************************************
   ** FUNCTION   : tVoid vProcessTraceCmd()
   /*******************************************************************************
   /*!
    * \brief   Trace Command interface handler.
    * \param   [cpu8Buffer] (->I) Pointer to the Trace command buffer
    * \retval  NONE
   *******************************************************************************/
   tVoid vProcessTraceCmd(tU8 const* const cpu8Buffer);

   /***************************************************************************
   ** FUNCTION:  virtual tVoid spi_tclTrace::vHandleTraceCmd()
   ***************************************************************************/
   /*!
   * \brief   Handles trace commands associated which are non-loopback(CCA)
   * based
   * \param   [cpu8Buffer]: Message Buffer with trace command and data
   * \retval  NONE
   **************************************************************************/
   tVoid vHandleTraceCmd(tU8 const* const cpu8Buffer);
   /********************************************************************************
   ** FUNCTION   : tVoid vDipoTrace()
   /*******************************************************************************
   /*!
    * \brief   Trace command for DiPO device connect test
    * \param   NONE
    * \retval  NONE
   *******************************************************************************/
   t_Void vDipoTrace();
   /********************************************************************************
   ** FUNCTION   : tVoid vDipoTrace()
   /*******************************************************************************
   /*!
    * \brief   Trace command to display SPI version info
    * \param   NONE
    * \retval  NONE
   *******************************************************************************/
   t_Void vDisplaySpiVersionInfo();
   
   /********************************************************************************
   ** FUNCTION   : tVoid vSetMLCertificateType()
   /*******************************************************************************
   /*!
    * \brief   Set the Mirrorlink certificate
    * \param   enMLCerti : type of mirrorlink certificate
    * \retval  NONE
   *******************************************************************************/
   t_Void vSetMLCertificateType(tenMLCertiType enMLCerti);

   /********************************************************************************
   ** FUNCTION   : tVoid vDisplayMLCertiType()
   /*******************************************************************************
   /*!
    * \brief   Trace command to display Mirrorlink certificate type
    * \param   NONE
    * \retval  NONE
   *******************************************************************************/
   t_Void vDisplayMLCertiType();

protected:
   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /*!
   * Pointer to Trace Command handler.
   */
   TraceStreamable* m_poTraceStreamer;

};

#endif
