/*!
*******************************************************************************
* \file              spi_tclRespInterface.h
* \brief             SPI Response interface for the Project specific layer
*******************************************************************************
\verbatim
PROJECT:       Gen3
SW-COMPONENT:  Smart Phone Integration
DESCRIPTION:   provides SPI response interface for the Project specific layer
COPYRIGHT:     &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
13.09.2013 |  Pruthvi Thej Nagaraju       | Initial Version
30.11.2013 |  Ramya Murthy                | Removed methods:
vPostGetDeviceInfoListResult(),
vPostGetAppListResult()
(methods from CmdInterface will be used)
10.12.2013 |  Ramya Murthy                | Addded method: 
vPostSetProjectedDisplayContextStatus()
06.02.2014 |  Ramya Murthy                | Adapted to SPI HMI API document v1.5 changes
26.04.2014 |  Shihabudheen P M            | Added vPostDeviceAudioContext
10.06.2014 |  Ramya Murthy                | Audio policy redesign implementation.
03.07.2014 |  Hari Priya E R              | Added changes related to Input Response Interface
25.10.2014 |  Shihabudheen P M            | added vUpdateSessionStatusInfo
28.10.2013 |  Hari Priya E R              | Added changes related to Data Service Response Interface
14.12.2015 |  Rachana L Achar             | Modified the prototype of vLaunchAppResult method

\endverbatim
******************************************************************************/

#ifndef SPI_TCLRESPINTERFACE_H_
#define SPI_TCLRESPINTERFACE_H_

/******************************************************************************
| includes:
| 1)RealVNC sdk - includes
| 2)Typedefines
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclConnMngrResp.h"
#include "spi_tclDevSelResp.h"
#include "spi_tclVideoRespInterface.h"
#include "spi_tclAppMngrRespIntf.h"
#include "spi_tclAppLauncherRespIntf.h"
#include "spi_tclBluetoothRespIntf.h"
#include "spi_tclResourceMngrResp.h"
#include "spi_tclAudioPolicyBase.h"
#include "spi_tclInputRespIntf.h"
#include "spi_tclDataServiceRespIntf.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \class spi_tclRespInterface
* \brief This class provides response interface for the project specific layer to
* implement mirrorlink and DIPO components
*/

class spi_tclRespInterface : public spi_tclConnMngrResp, public spi_tclDevSelResp,
   public spi_tclAppMngrRespIntf,public spi_tclAppLauncherRespIntf,
   public spi_tclVideoRespInterface, public spi_tclBluetoothRespIntf,public spi_tclResourceMngrResp,
   public spi_tclAudioPolicyBase,public spi_tclInputRespIntf,public spi_tclDataServiceRespIntf
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::spi_tclRespInterface();
   ***************************************************************************/
   /*!
   * \fn      spi_tclRespInterface()
   * \brief   default constructor
   * \sa     ~spi_tclRespInterface
   **************************************************************************/
   spi_tclRespInterface();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::~spi_tclRespInterface()
   ***************************************************************************/
   /*!
   * \fn     virtual ~spi_tclRespInterface()
   * \brief  virtualized destructor
   * \sa     spi_tclRespInterface
   **************************************************************************/
   virtual ~spi_tclRespInterface();

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostAppStatusInfo
   ***************************************************************************/
   /*!
   * \fn     vPostAppStatusInfo(t_U32 u32DeviceHandle, 
   *             tenDeviceConnectionType enDevConnType, tenAppStatusInfo enAppStatus)
   * \brief  It notifies the client upon application list change of a Mirror Link 
   *         device. The client can retrieve the detailed information of the  
   *         applications via the methods provided. If a Mirror Link device is 
   *         disconnected, Device Handle is set to 0xFFFF and DeviceConnectionType 
   *         is set to UNKNOWN_CONNECTION.
   * \param  [IN] u32DeviceHandle : Uniquely identifies the target Device.
   * \param  [IN] enDevConnType   : Identifies the Connection Type.
   * \param  [IN] enAppStatus : Provides application Status Information.
   **************************************************************************/
   virtual t_Void vPostAppStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceConnectionType enDevConnType,
      tenAppStatusInfo enAppStatus);


   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vLaunchAppResult
   **             (t_U32 u32DeviceHandle, t_U32 u32AppHandle,...)
   ***************************************************************************/
   /*!
   * \fn     vLaunchAppResult(t_U32 u32DeviceHandle, t_U32 u32AppHandle, 
   *            tenDiPOAppType enDiPOAppType, tenResponseCode enResponseCode, 
   *			     tenErrorCode enErrorCode, const trUserContext corfrUsrCntxt)
   * \brief  It provides result of a remote application launch in selected Mirror Link device.
   * \param  [IN] u32DeviceHandle      : Uniquely identifies the target Device.
   * \param  [IN] u32AppHandle : Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface.
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
   *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] enResponseCode  :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] corfrUsrCntxt	 : User Context Details.
   * \sa      spi_tclCmdInterface::vLaunchApp
   **************************************************************************/
   virtual t_Void vLaunchAppResult(t_U32 u32DeviceHandle, 
      t_U32 u32AppHandle, 
      tenDiPOAppType enDiPOAppType,
      tenResponseCode enResponseCode, 
      tenErrorCode enErrorCode, 
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vTerminateAppResult
   **             (t_U32 u32DeviceHandle, t_U32 u32AppHandle,..)
   ***************************************************************************/
   /*!
   * \fn     vTerminateAppResult(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
   *             tenResponseCode enResponseCode, tenErrorCode enErrorCode,
   * 				const trUserContext rcUsrCntxt)
   * \brief  It terminates the remote application running on the Mirror Link device.
   * \param  [IN] enResponseCode  :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] u32DeviceHandle		  : Unique handle of the device.
   * \param  [IN] u32AppHandle : Unique handle of the application to be terminated.
   * \param  [IN] rcUsrCntxt : User Context Details.
   * \sa     spi_tclCmdInterface::vTerminateApp
   **************************************************************************/
   virtual t_Void vTerminateAppResult(t_U32 u32DeviceHandle, 
      t_U32 u32AppHandle, 
      tenResponseCode enResponseCode, 
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostAppIconDataResult
   **                   (tenIconMimeType enIconMimeType, t_Char* pczAppIconData, ..)
   ***************************************************************************/
   /*!
   * \fn     vPostAppIconDataResult(tenIconMimeType enIconMimeType, 
   *            t_Char* pczAppIconData,t_U32 u32Len, const trUserContext rcUsrCntxt)
   * \brief  It retrieves icon data referenced by the AppList.AppIconXXXURLs
   * \param  [IN] enIconMimeType :  Mime Type of the icon pointed by AppIconURL.
   *              If image is not available then this parameter would be set
   *              to NULL (zero length string).
   * \param  [IN] pczAppIconData : Byte Data Stream from the icon image file.
   *              Format of the file is de-fined by IconMimeType parameter.
   * \param  [IN] u32Len : Length the data stream
   * \param  [IN] rcUsrCntxt		: User Context Details.
   * \sa     spi_tclCmdInterface::vGetAppIconData
   **************************************************************************/
   virtual t_Void vPostAppIconDataResult(tenIconMimeType enIconMimeType,
      const t_U8* pcu8AppIconData,
      t_U32 u32Len,
      const trUserContext rcUsrCntxt); //TODO

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostSetAppIconAttributesResult
   **             (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSetAppIconAttributesResult(tenResponseCode enResponseCode,
   *                          tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
   * \brief  sets application icon attributes for retrieval of application icons.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetAppIconAttributes
   **************************************************************************/
   virtual t_Void vPostSetAppIconAttributesResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostSetMLNotificationEnabledInfoResult
   **                 (t_U32 u32DeviceHandle, tenResponseCode enResponseCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSetMLNotificationEnabledInfoResult(t_U32 u32DeviceHandle, 
   *            tenResponseCode enResponseCode, tenErrorCode enErrorCode, 
   *            const trUserContext rcUsrCntxt)
   * \brief  Interface to set the device notification preference for
   *         applications (only for Mirror Link devices). If notification for
   *         all the applications has to be
   * \param  [IN] u32DeviceHandle   : Uniquely identifies the target Device.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetMLNotificationEnabledInfo
   **************************************************************************/
   virtual t_Void vPostSetMLNotificationEnabledInfoResult(t_U32 u32DeviceHandle,
      tenResponseCode enResponseCode, 
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostNotificationInfo
   **                  (t_U32 u32DeviceHandle,t_U32 u32AppHandle,..)
   ***************************************************************************/
   /*!
   * \fn     vPostNotificationInfo(t_U32 u32DeviceHandle, t_U32 u32AppHandle,
   *            NotificationData &rfrNotificationData)
   * \brief  Interface to provide Notification Information received from
   *         the Mirror Link server (only for Mirror Link devices).
   * \param  [IN] u32DeviceHandle   :  Handle uniquely identifies a device.
   * \param  [IN] u32AppHandle      : Handle uniquely identifies an application 
   *              on the device.
   * \param  [IN] corfrNotificationData : Provides notification event details
   * \sa     spi_tclCmdInterface::vSetMLNotificationEnabledInfo
   **************************************************************************/
   virtual t_Void vPostNotificationInfo(t_U32 u32DeviceHandle,
      t_U32 u32AppHandle, 
      const trNotiData& corfrNotificationData);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostInvokeNotificationActionResult(
   **               tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostInvokeNotificationActionResult(tenResponseCode enResponseCode,
   *            tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
   * \brief  Interface to invoke the respective action for the
   *         received Notification Event.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt		 : User Context Details.
   * \sa     spi_tclCmdInterface::vInvokeNotificationAction
   **************************************************************************/
   virtual t_Void vPostInvokeNotificationActionResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode,
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostSetOrientationModeResult
   **              (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSetOrientationModeResult(tenResponseCode enResponseCode,
   *                  tenErrorCode enErrorCode, const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  [IN] enResponseCode : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] corfrcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetOrientationMode
   **************************************************************************/
   virtual t_Void vPostSetOrientationModeResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext& corfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostSetVideoBlockingModeResult
   **               (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSetVideoBlockingModeResult(tenResponseCode enResponseCode,
   *                         tenErrorCode enErrorCode, const trUserContext& corfrcUsrCntxt)
   * \brief  Interface to set the display blocking mode.
   * \param  [IN] enResponseCode : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] corfrcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetVideoBlockingMode
   **************************************************************************/
   virtual t_Void vPostSetVideoBlockingModeResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext& corfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostSetAudioBlockingModeResult
   **            (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSetAudioBlockingModeResult(tenResponseCode enResponseCode,
   *            tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
   * \brief  Interface to set the audio blocking mode.
   * \param  [IN] enResponseCode : Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSetAudioBlockingMode
   **************************************************************************/
   virtual t_Void vPostSetAudioBlockingModeResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostSendTouchEvent
   **                (tenResponseCode enResponseCode,tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSendTouchEvent(tenResponseCode enResponseCode,tenErrorCode enErrorCode
   *				const trUserContext rcUsrCntxt)
   * \brief  Interface to set the Touch or Pointer events.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *              Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSendTouchEvent
   **************************************************************************/
   virtual t_Void vPostSendTouchEvent(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostSendKeyEvent
   **                (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostSendKeyEvent(tenResponseCode enResponseCode,tenErrorCode enErrorCode,
   *				const trUserContext rcUsrCntxt)
   * \brief   Interface to set the key events.
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   *          Set to NO_ERROR for successful operation.
   * \param  [IN] rcUsrCntxt	 : User Context Details.
   * \sa     spi_tclCmdInterface::vSendKeyEvent
   **************************************************************************/
   virtual t_Void vPostSendKeyEvent(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostUpdateCertificateFileResult
   **              (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
   ***************************************************************************/
   /*!
   * \fn     vPostUpdateCertificateFileResult(tenResponseCode enResponseCode,
   *             tenErrorCode enErrorCode, const trUserContext rcUsrCntxt)
   * \brief  Interface to update/modify the certificate file stored in the system
   * \param  [IN] enResponseCode :  Provides result from the operation.
   * \param  [IN] enErrorCode : Provides the Error Code in case ResponseCode==FAILURE.
   * \param  [IN] rcUsrCntxt		 : User Context Details.
   * \sa     spi_tclCmdInterface::vUpdateCertificateFile
   **************************************************************************/
   virtual t_Void vPostUpdateCertificateFileResult(tenResponseCode enResponseCode,
      tenErrorCode enErrorCode, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostDeviceDisplayContext
   **                   (t_U32 u32DeviceHandle, t_Bool bDisplayFlag,..)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceDisplayContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  This interface is used by Mirror Link/DiPO device to inform the client
   *              about its current display con-text.
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bDisplayFlag     : TRUE � Start Display Projection, FALSE � Stop Display Projection.
   * \param  [IN] enDisplayContext : Display context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   virtual t_Void vPostDeviceDisplayContext(t_U32 u32DeviceHandle,
      t_Bool bDisplayFlag,
      tenDisplayContext enDisplayContext,
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostDeviceAudioContext
   **                   (t_U32 u32DeviceHandle, t_Bool bDisplayFlag,..)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceAudioContext(t_U32 u32DeviceHandle, t_Bool bDisplayFlag,
   *         tenDisplayContext enDisplayContext, const trUserContext rcUsrCntxt);
   * \brief  this function is used to update the audio context changes to HMI
   * \param  [IN] u32DeviceHandle  : Uniquely identifies the target Device.
   * \param  [IN] bPlayFlag        : TRUE � Start Projection playback, 
   *                                 FALSE � Stop Projection playback.
   * \param  [IN] u8AudioContext   : Audio context of the projected device.
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   virtual t_Void vPostDeviceAudioContext(t_U32 u32DeviceHandle,
      t_Bool bPlayFlag, t_U8 u8AudioContext,
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vPostDeviceAppState
   **                   (tenSpeechAppState enSpeechAppState,...)
   ***************************************************************************/
   /*!
   * \fn     vPostDeviceAudioContext()
   * \brief  This function used to update HMI about the App state changes
   * \param  [IN] enSpeechAppState : Speech app state
   * \param  [IN] enPhoneAppState  : Phone App state
   * \param  [IN] enNavAppState    : Navigation App state
   * \param  [IN] rcUsrCntxt       : User Context Details.
   **************************************************************************/
   virtual t_Void vPostDeviceAppState(tenSpeechAppState enSpeechAppState, 
      tenPhoneAppState enPhoneAppState, tenNavAppState enNavAppState, 
      const trUserContext rcUsrCntxt);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclRespInterface::vUpdateSessionStatusInfo
   **                   (t_U32 u32DeviceHandle,...)
   ***************************************************************************/
   /*!
   * \fn     vUpdateSessionStatusInfo()
   * \brief  Used to update the session status to HMI.
   * \param  [IN] u32DeviceHandle  : Device handle 
   * \param  [IN] enDevCat         : Device category
   * \param  [IN] enSessionStatus  : Session status.
   **************************************************************************/
   virtual t_Void vUpdateSessionStatusInfo(t_U32 u32DeviceHandle,
      tenDeviceCategory enDevCat,
      tenSessionStatus enSessionStatus);


private:


};

#endif /* SPI_TCLRESPINTERFACE_H_ */
