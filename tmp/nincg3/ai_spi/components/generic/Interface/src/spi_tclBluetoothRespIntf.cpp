/*!
 *******************************************************************************
 * \file             spi_tclBluetoothRespIntf.cpp
 * \brief            Response to HMI from Bluetooth manager class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Response to HMI from Bluetooth manager class
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 08.03.2014 |  Ramya Murthy                | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclFactory.h"
#include "spi_tclBluetoothRespIntf.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_BLUETOOTH
      #include "trcGenProj/Header/spi_tclBluetoothRespIntf.cpp.trc.h"
   #endif
#endif

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION: t_Void spi_tclBluetoothRespIntf::spi_tclBluetoothRespIntf();
***************************************************************************/
spi_tclBluetoothRespIntf::spi_tclBluetoothRespIntf()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclBluetoothRespIntf::~spi_tclBluetoothRespIntf()
***************************************************************************/
spi_tclBluetoothRespIntf::~spi_tclBluetoothRespIntf()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclBluetoothRespIntf::vPostBluetoothDeviceStatus(...)
***************************************************************************/
t_Void spi_tclBluetoothRespIntf::vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
      t_U32 u32ProjectionDevHandle,
      tenBTChangeInfo enBTChange,
      t_Bool bCallActive)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   ETG_TRACE_USR3(("vPostBluetoothDeviceStatus: Received info: \n"));
   ETG_TRACE_USR3(("   || BluetoothDevHandle = %u \n", u32BluetoothDevHandle));
   ETG_TRACE_USR3(("   || ProjectionDevHandle = %u \n", u32ProjectionDevHandle));
   ETG_TRACE_USR3(("   || BTChangeInfo = %d \n", ETG_ENUM(BT_CHANGE_INFO, enBTChange)));
   ETG_TRACE_USR3(("   || CallActive = %u \n", bCallActive));

   //! Invoke BT action to switch device
   spi_tclFactory* poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclBluetooth* poBluetooth = poFactory->poGetBluetoothInstance();
      SPI_NORMAL_ASSERT(NULL == poBluetooth);
      if (NULL != poBluetooth)
      {
         poBluetooth->vOnInvokeBTDeviceAction(u32BluetoothDevHandle, u32ProjectionDevHandle, enBTChange);
      } //if (NULL != poBluetooth)
   } // if (NULL != poFactory)
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
