/*!
 *******************************************************************************
 * \file              spi_tclCmdInterface.cpp
 * \brief             SPI command interface for the Project specific layer
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Provides SPI command interfaces for the SPI Service.
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                  		| Modifications
 13.11.2013 |  Deepti Samant           		| Initial Version
 30.11.2013 |  Ramya Murthy            		| (1) Modified vGetDeviceInfoList() &
                                         	  vGetAppList() : Included new formal
                                         	  arguments for fetching data.
                                         	  (2) Included new methods:
                                         	  u32GetSelectedDeviceHandle(),
                                         	  bGetMLNotificationEnabledInfo()
 09.12.2013 |  Hari Priya             		| Included changes for input handler
 10.12.2013 |  Ramya Murthy           		| (1) Corrected parameter data types in
                                         	  vGetAppList() & vGetAppIconData().
                                         	  (2) Included ProjectedDisplayContext,
                                         	  DeviceUsagePreference, AppMetadata
                                         	  & MLNotificationEnabledInfo related
                                         	  methods.
 26.12.2013 |  Shiva Kumar G           		| Updated with few elements for
                                         	  HMI Interface
 21.01.2014 |  Shiva kumar Gurija    		| Updated with Video related elements
 06.02.2014 |  Ramya Murthy            		| Adapted to SPI HMI API document v1.5 changes
 17.03.2014 |  Pruthvi Thej Nagaraju   		| Added separate thread to receive commands
 06.04.2014 |  Ramya Murthy            		| Initialisation sequence implementation
 25.05.2014 |  Hari Priya E R          		| Removed function for setting screen variant
 10.06.2014 |  Ramya Murthy            		| Audio policy redesign implementation.
 19.06.2014 | Shihabudheen P M         		| Modified for app state resource arbitration
 25.06.2014 | Shihabudheen P M         		| Modified bInitialize() for CarPlay msg receiver
                                         	  creation
 31.07.2014 |  Ramya Murthy            		| SPI feature configuration via LoadSettings()
 01.10.2014 |  Ramya Murthy            		| Added interface to receive call status
 14.10.2014 |  Hari Priya E R          		| Added interface to receive Location data
 06.11.2014 |  Hari Priya E R          		| Added changes to set Client key capabilities
 05.02.2015 |  Ramya Murthy            		| Added interface to set availability of LocationData
 23.04.2015 |  Ramya Murthy                 | Added region info for DataService
 17.06.2015 |  Sameer Chandra               | Added new method vGetKeyIconData()
 25.06.2015 | Shiva kaumr G                 | Dynamic display configuration
 15.07.2015 |  Sameer Chandra               | Added new method vSendKnobKeyEvent
 04.09.2015 |  Dhiraj Asopa                 | Implemented vSetAccessoryAudioContext() function.
 03.12.2015 |  SHITANSHU SHEKHAR            | Implemented vSetFeatureRestrictions() function.
 07.03.2016 |  Chaitra Srinivasa            | Implemented default settings
 10.03.2016 |  Rachana L Achar              | Added interface for notification acknowledgment

\endverbatim
******************************************************************************/

#include <map>
#include <unistd.h>

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include "ahl_if.h"

#include "SPITypes.h"
#include "spi_tclLifeCycleIntf.h"
#include "spi_tclRespInterface.h"
#include "spi_tclFactory.h"
#include "spi_tclDeviceSelector.h"
#include "spi_tclAppLauncher.h"
#include "spi_tclBluetooth.h"
#include "spi_tclDiPODeviceMsgRcvr.h"
#include "spi_tclConfigReader.h"
#include "spi_tclCmdMsgQInterface.h"
#include "spi_tclCmdDispatcher.h"
#include "spi_tclCmdInterface.h"
#include "spi_tclDefaultSettings.h"
#include "spi_tclDefSetLoader.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_TCLCMDINTERFACE
#include "trcGenProj/Header/spi_tclCmdInterface.cpp.trc.h"
#endif
#endif


/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclCmdInterface::spi_tclCmdInterface(spi_tclRespInterface* poSpiRespIntf)
 ***************************************************************************/
spi_tclCmdInterface::spi_tclCmdInterface(spi_tclRespInterface* poSpiRespIntf, 
      ahl_tclBaseOneThreadApp* poMainAppl) :
         m_poSpiRespIntf(poSpiRespIntf), m_poMainApp(poMainAppl),
         m_poFactory(NULL), m_poDevSelector(NULL),
         m_poLaunchApp(NULL), m_poDeviceMsgRcvr(NULL),
         m_poCmdMsqQ(NULL), m_poDefSet(NULL)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::spi_tclCmdInterface() entered"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclCmdInterface::~spi_tclCmdInterface()
 ***************************************************************************/
spi_tclCmdInterface::~spi_tclCmdInterface()
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::~spi_tclCmdInterface() entered"));

   m_poMainApp       = NULL;
   m_poSpiRespIntf   = NULL;
   m_poFactory       = NULL;
   m_poDevSelector   = NULL;
   m_poLaunchApp     = NULL;
   m_poDeviceMsgRcvr = NULL;
   m_poCmdMsqQ       = NULL;
   m_poDefSet        = NULL;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclCmdInterface::bInitialize()
***************************************************************************/
t_Bool spi_tclCmdInterface::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::bInitialize() entered"));

   t_Bool bInit = true;

   if ((NULL != m_poMainApp) && (NULL != m_poSpiRespIntf))
   {
      m_poFactory = spi_tclFactory::getInstance(m_poSpiRespIntf, m_poMainApp);
      SPI_NORMAL_ASSERT(NULL == m_poFactory);

      if (NULL != m_poFactory)
      {
         bInit = m_poFactory->bInitialize();
      } //if (NULL != m_poFactory)

      m_poDevSelector = new spi_tclDeviceSelector(m_poSpiRespIntf);
      SPI_NORMAL_ASSERT(NULL == m_poDevSelector);
      if (NULL != m_poDevSelector)
      {
         bInit = bInit && m_poDevSelector->bInitialize();
      } //if (NULL != m_poFactory)

      m_poLaunchApp = new spi_tclAppLauncher(m_poSpiRespIntf);
      SPI_NORMAL_ASSERT(NULL == m_poLaunchApp);

      m_poDeviceMsgRcvr = new spi_tclDiPODeviceMsgRcvr(m_poSpiRespIntf);
      SPI_NORMAL_ASSERT(NULL == m_poDeviceMsgRcvr);

      m_poCmdMsqQ = new spi_tclCmdMsgQInterface();
      SPI_NORMAL_ASSERT(NULL == m_poCmdMsqQ);

      m_poDefSet = new spi_tclDefSetLoader();
      SPI_NORMAL_ASSERT(NULL == m_poDefSet);

   } //if (NULL != m_poSpiRespIntf)

   bInit = bInit && (NULL != m_poDevSelector) && (NULL != m_poLaunchApp)
            && (NULL != m_poDeviceMsgRcvr) && (NULL != m_poCmdMsqQ)
            && (NULL != m_poDefSet);			

   return bInit;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclCmdInterface::bUnInitialize()
***************************************************************************/
t_Bool spi_tclCmdInterface::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::bUnInitialize() entered"));

   t_Bool bUnInit = false;

   //Release resources
   RELEASE_MEM(m_poCmdMsqQ);
   RELEASE_MEM(m_poLaunchApp);
   RELEASE_MEM(m_poDefSet);   
   if (NULL != m_poDevSelector)
   {
      m_poDevSelector->vUnInitialize();
   }
   RELEASE_MEM(m_poDevSelector);
   RELEASE_MEM(m_poDeviceMsgRcvr);

   if (NULL != m_poFactory)
   {
      bUnInit = m_poFactory->bUnInitialize();
   }
   m_poFactory = NULL;
   ETG_TRACE_USR1(("spi_tclCmdInterface::bUnInitialize() Left"));
   return bUnInit;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vLoadSettings()
***************************************************************************/
t_Void spi_tclCmdInterface::vLoadSettings(
      const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vLoadSettings() entered"));

   if (NULL != m_poFactory)
   {
      m_poFactory->vLoadSettings(rfcrSpiFeatureSupp);
   }

   //!Do not set the SPI_ENT source to unavailable at startup.
   //!Audio has implemented a timer at startup to restore last active source.
   //!Whenever SPI_ENT source is ready to stream, set it to available.After Audio timer expires,
   //!Audio component will restore FM as the default source.(SUZUKI-24718)
   /*spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
   if (NULL != poAudio)
   {
      poAudio->bSetAudioSrcAvailability(e8AUD_MAIN_OUT, false);
   } */

   if (NULL != m_poDevSelector)
   {
      m_poDevSelector->vLoadSettings(rfcrSpiFeatureSupp);
   }
   
   if (NULL != m_poDefSet)
   {
      m_poDefSet->vLoadSettings();
   }
   
   ETG_TRACE_USR1(("spi_tclCmdInterface::vLoadSettings() Left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vSaveSettings()
***************************************************************************/
t_Void spi_tclCmdInterface::vSaveSettings()
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSaveSettings() entered"));
   if (NULL != m_poFactory)
   {
      m_poFactory->vSaveSettings();
   }
   if (NULL != m_poDevSelector)
   {
       m_poDevSelector->vSaveSettings();
   }
   
   if (NULL != m_poDefSet)
   {
      m_poDefSet->vSaveSettings();
   }
   
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSaveSettings() Left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vRestoreSettings()
***************************************************************************/
t_Void spi_tclCmdInterface::vRestoreSettings()
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vRestoreSettings entered"));

   if (NULL != m_poFactory)
   {
      spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
      if (OSAL_NULL != poConfigReader)
      {
         poConfigReader->vClearVehicleId();
      }
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      if (NULL != poConnMngr)
      {
         poConnMngr->vClearDeviceHistory();
      }//if (NULL != poConnMngr)
	  //load the default values on call to restore settings
      if (NULL != m_poDefSet)
      {
         m_poDefSet->vRestoreSettings();
      }
	  
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vRestoreSettings Left"));
}

/***************************************************************************
** FUNCTION: t_U32 spi_tclCmdInterface::u32GetSelectedDeviceHandle();
***************************************************************************/
t_U32 spi_tclCmdInterface::u32GetSelectedDeviceHandle() const
{
   t_U32 u32SelDevHandle = 0;
   if(NULL != m_poDevSelector)
   {
      u32SelDevHandle = m_poDevSelector->u32GetSelectedDeviceHandle();
   }
   return u32SelDevHandle;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclCmdInterface::vGetDeviceInfoList(t_U32& u32Nu...
 ***************************************************************************/
t_Void spi_tclCmdInterface::vGetDeviceInfoList(t_U32& rfu32NumDevices,
   std::vector<trDeviceInfo>& rfvecrDeviceInfoList)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetDeviceInfoList() entered "));

   if (NULL != m_poFactory)
   {
      spi_tclConnMngr *poConnMngr = m_poFactory->poGetConnMngrInstance();

      if (NULL != poConnMngr)
      {
         poConnMngr->vGetDeviceList(rfvecrDeviceInfoList);
         rfu32NumDevices = rfvecrDeviceInfoList.size();
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetDeviceInfoList() left "));

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclCmdInterface::vSelectDevice(t_U32 u32DeviceHandle,..)
 ***************************************************************************/
t_Void spi_tclCmdInterface::vSelectDevice(t_U32 u32DeviceHandle,
   tenDeviceConnectionType enDevConnType, 
   tenDeviceConnectionReq enDevConnReq,
   tenEnabledInfo enDAPUsage,
   tenEnabledInfo enCDBUsage,
   tenDeviceCategory enDevCat,
   const trUserContext& rfcorUsrCntxt,
   t_Bool bIsHMITrigger)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSelectDevice entered"));

   if (NULL != m_poDevSelector)
   {
      CmdSelectDevice oCmdSelectDevice(m_poDevSelector, u32DeviceHandle,
               enDevConnType, enDevConnReq, enDAPUsage, enCDBUsage, enDevCat, bIsHMITrigger);
      oCmdSelectDevice.vSetUserContext(rfcorUsrCntxt);
      oCmdSelectDevice.vSetServiceID(e32COMMANDID_SELECTDEVICE);
      if (NULL != m_poCmdMsqQ)
      {
         m_poCmdMsqQ->bWriteMsgToQ(&oCmdSelectDevice, sizeof(oCmdSelectDevice));
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSelectDevice left"));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclCmdInterface::vLaunchApp(t_U32 u32DeviceHandle,..)
 ***************************************************************************/
t_Void spi_tclCmdInterface::vLaunchApp(t_U32 u32DeviceHandle, 
   tenDeviceCategory enDeviceCategory, 
   t_U32 u32AppHandle, 
   tenDiPOAppType enDiPOAppType, 
   t_String szTelephoneNumber, 
   tenEcnrSetting enEcnrSetting, 
   const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vLaunchApp entered"));
   SPI_INTENTIONALLY_UNUSED(enDeviceCategory);
   if (NULL != m_poLaunchApp)
   {
      CmdLaunchApp oCmdLaunchApp(m_poLaunchApp, u32DeviceHandle, u32AppHandle,
               enDiPOAppType, szTelephoneNumber, enEcnrSetting);
      oCmdLaunchApp.vSetUserContext(rfcorUsrCntxt);
      oCmdLaunchApp.vSetServiceID(e32COMMANDID_LAUNCHAPP);
      if (NULL != m_poCmdMsqQ)
      {
         m_poCmdMsqQ->bWriteMsgToQ(&oCmdLaunchApp, sizeof(oCmdLaunchApp));
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vLaunchApp left"));
}

/***************************************************************************
 ** FUNCTION: vTerminateApp(t_U32 u32DeviceHandle, t_U32 u32ApplicationHandle,
               const trUserContext& rfcorUsrCntxt);
 ***************************************************************************/
t_Void spi_tclCmdInterface::vTerminateApp(t_U32 u32DeviceHandle, 
   t_U32 u32ApplicationHandle, 
   const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vTerminateApp entered"));
   if (NULL != m_poLaunchApp)
   {
      CmdTerminateApp oCmdTerminateApp(m_poLaunchApp, u32DeviceHandle,
               u32ApplicationHandle);
      oCmdTerminateApp.vSetUserContext(rfcorUsrCntxt);
      oCmdTerminateApp.vSetServiceID(e32COMMANDID_TERMINATEAPP);
      if (NULL != m_poCmdMsqQ)
      {
         m_poCmdMsqQ->bWriteMsgToQ(&oCmdTerminateApp, sizeof(oCmdTerminateApp));
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vTerminateApp entered"));
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclCmdInterface::vGetAppList(t_U32 u32DeviceHandl...
 ***************************************************************************/
t_Void spi_tclCmdInterface::vGetAppList(t_U32 u32DeviceHandle,
   t_U32& rfu32NumAppDetailsList,
   std::vector<trAppDetails>& rfvecrAppDetailsList)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetAppList entered"));
   ETG_TRACE_USR2(("[DESC]::vGetAppList received value of DeviceHandle -0x%x",u32DeviceHandle));

   if (NULL != m_poFactory)
   {
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      if((NULL != poAppMngr) &&(NULL != poConnMngr ))
      {
         tenDeviceCategory enDevCat=poConnMngr->enGetDeviceCategory(u32DeviceHandle);
         poAppMngr->vGetAppDetailsList(u32DeviceHandle,
            rfu32NumAppDetailsList,
            rfvecrAppDetailsList,
            enDevCat);
      }//if(NULL != m_poAppMngr )
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetAppList left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vGetAppIconData(t_String szAppIconURL,..)
***************************************************************************/
t_Void spi_tclCmdInterface::vGetAppIconData(t_String szAppIconURL,
   const trUserContext& rfcorUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetAppIconData entered"));
   if (NULL != m_poFactory)
   {
      //Get the selected device & it's category and pass to App mngr
      tenDeviceCategory enDevCat = enGetDeviceCategory(u32GetSelectedDeviceHandle());
      spi_tclAppMngr* poAppMngr = m_poFactory->poGetAppManagerInstance();

      if (NULL != poAppMngr)
      {
         CmdAppIconData oCmdAppIconData(poAppMngr, enDevCat, szAppIconURL);
         oCmdAppIconData.vSetUserContext(rfcorUsrCntxt);
         oCmdAppIconData.vSetServiceID(e32COMMANDID_GETAPPICONDATA);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oCmdAppIconData,
                     sizeof(oCmdAppIconData));
         }
      }//if (NULL != poAppMngr)
   } //if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetAppIconData left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetAppIconAttributes(t_U32 u32DeviceHandle..)
***************************************************************************/
t_Void spi_tclCmdInterface::vSetAppIconAttributes(t_U32 u32DeviceHandle,
   t_U32 u32AppHandle,
   const trIconAttributes &rfrIconAttributes,
   const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAppIconAttributes() entered "));

   if (NULL != m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      if ((NULL != poAppMngr) && (NULL != poConnMngr))
      {
         //Get the selected device & it's category and pass to App mngr
         tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(
                  u32DeviceHandle);
         CmdAppIconAttributes oCmdAppIconAttributes(poAppMngr, u32DeviceHandle,
                  u32AppHandle, enDevCat, rfrIconAttributes);
         oCmdAppIconAttributes.vSetUserContext(rfcorUsrCntxt);
         oCmdAppIconAttributes.vSetServiceID(e32COMMANDID_SETAPPICONATTR);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oCmdAppIconAttributes,
                     sizeof(oCmdAppIconAttributes));
         }
      } //if (NULL != poConnMngr)
   } //if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAppIconAttributes() left "));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetDeviceUsagePreference(t_U32 ...
***************************************************************************/
t_Void spi_tclCmdInterface::vSetDeviceUsagePreference(t_U32 u32DeviceHandle,
   tenDeviceCategory enDeviceCategory,
   tenEnabledInfo enEnabledInfo,
   const trUserContext& rfcorUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetDeviceUsagePreference() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclConnMngr *poConnMngr = m_poFactory->poGetConnMngrInstance();

      CmdDeviceUsagePreference oCmdDeviceUsagePreference(poConnMngr,
               u32DeviceHandle, enDeviceCategory, enEnabledInfo);
      oCmdDeviceUsagePreference.vSetUserContext(rfcorUsrCntxt);
      oCmdDeviceUsagePreference.vSetServiceID(e32COMMANDID_SETDEVICEUSAGEPREF);
      if (NULL != m_poCmdMsqQ)
      {
         m_poCmdMsqQ->bWriteMsgToQ(&oCmdDeviceUsagePreference,
                  sizeof(oCmdDeviceUsagePreference));
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetDeviceUsagePreference() left"));
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclCmdInterface::bGetDeviceUsagePreference
**                   (t_U32 u32DeviceHandle, tenDeviceCategory enDeviceCategory,..)
***************************************************************************/
t_Bool spi_tclCmdInterface::bGetDeviceUsagePreference(t_U32 u32DeviceHandle,
   tenDeviceCategory enDeviceCategory,
   tenEnabledInfo& rfenEnabledInfo) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::bGetDeviceUsagePreference() entered"));

   t_Bool bRetVal = false;
   if (NULL != m_poFactory)
   {
      spi_tclConnMngr *m_poConnMngr = m_poFactory->poGetConnMngrInstance();
      if (NULL != m_poConnMngr)
      {
         bRetVal = m_poConnMngr->bGetDeviceUsagePreference(u32DeviceHandle,
                  enDeviceCategory, rfenEnabledInfo);
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::bGetDeviceUsagePreference() left"));
   return bRetVal;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetMLNotificationOnOff()
***************************************************************************/
t_Void spi_tclCmdInterface::vSetMLNotificationOnOff(t_Bool bSetNotificationsOn)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetMLNotificationOnOff entered"));
   ETG_TRACE_USR2(("[DESC]::vSetMLNotificationOnOff received value of bSetNotificationsOn -%d",
      ETG_ENUM(BOOL,bSetNotificationsOn)));

   if (NULL != m_poFactory)
   {
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      if ((NULL != poAppMngr))
      {
         poAppMngr->vSetMLNotificationOnOff(bSetNotificationsOn);
      } //if (NULL != poAppMngr)
   } //if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetMLNotificationOnOff left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetMLNotificationEnabledInfo()
***************************************************************************/
t_Void spi_tclCmdInterface::vSetMLNotificationEnabledInfo(t_U32 u32DeviceHandle,
   t_U16 u16NumNotificationEnableList,
   std::vector<trNotiEnable> vecrNotificationEnableList,
   const trUserContext& rfcorUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetMLNotificationEnabledInfo entered"));
   ETG_TRACE_USR2(("[DESC]::vSetMLNotificationEnabledInfo received value of DeviceHandle-0x%x and  NotificationEnabled List-%d\n",
      u32DeviceHandle,u16NumNotificationEnableList));

   if (NULL != m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      if ((NULL != poAppMngr) && (NULL != poConnMngr))
      { 
	  //Get the device category and pass to App Mngr
         tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(
                  u32DeviceHandle);
         CmdMLNotificationEnabledInfo oMLNotiEnableInfo(poAppMngr,
                  u32DeviceHandle, u16NumNotificationEnableList, enDevCat,
                  vecrNotificationEnableList);

         oMLNotiEnableInfo.vSetUserContext(rfcorUsrCntxt);
         oMLNotiEnableInfo.vSetServiceID(
                  e32COMMANDID_MLNOTIFICATION_ENABLED_INFO);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oMLNotiEnableInfo,
                     sizeof(oMLNotiEnableInfo));
         }
      }//if ((NULL != poAppMngr)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetMLNotificationEnabledInfo left"));
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclCmdInterface::bGetMLNotificationEnabledInfo()
***************************************************************************/
t_Bool spi_tclCmdInterface::bGetMLNotificationEnabledInfo() const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::bGetMLNotificationEnabledInfo() entered"));

   t_Bool bRet = false;

   if (NULL != m_poFactory)
   {
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      if(NULL != poAppMngr)
      {
         bRet = poAppMngr->bGetMLNotificationEnabledInfo();
      }//if ((NULL != poAppMngr)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::bGetMLNotificationEnabledInfo() Left"));
   return bRet;

}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vInvokeNotificationAction
**                   (t_U32 u32DeviceHandle, t_U32 u32AppHandle,..)
***************************************************************************/
t_Void spi_tclCmdInterface::vInvokeNotificationAction(t_U32 u32DeviceHandle,
   t_U32 u32AppHandle,
   t_U32 u32NotificationID,
   t_U32 u32NotificationActionID,
   const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vInvokeNotificationAction entered"));
   ETG_TRACE_USR2(("[DESC]::vInvokeNotificationAction received value of DeviceHandle-0x%x NotificationID-0x%x NotificationActionID-0x%x ",
      u32DeviceHandle,u32NotificationID,u32NotificationActionID));

   if (NULL != m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      if ((NULL != poAppMngr) && (NULL != poConnMngr))
      { 
         //Get the device category and pass to App Mngr
         tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(
            u32DeviceHandle);

         //Check whether the launch app is required for the action.
         t_Bool bIslaunchAppreq = poAppMngr->bIsLaunchAppReq(u32DeviceHandle,
            u32AppHandle,u32NotificationID,u32NotificationActionID,enDevCat);

         CmdInvokeNotificationAction oInvokeNotiAction(poAppMngr,
            u32DeviceHandle,
            u32AppHandle,
            u32NotificationID,
            u32NotificationActionID,
            enDevCat);

         oInvokeNotiAction.vSetUserContext(rfcorUsrCntxt);
         oInvokeNotiAction.vSetServiceID(e32COMMANDID_INVOKE_NOTIACTION);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oInvokeNotiAction,
               sizeof(oInvokeNotiAction));
         }

         if((true == bIslaunchAppreq)&&(NULL != m_poLaunchApp))
         {
            //Launch App is required for this particular Notification Action. Launch the application,
            //for which the Notification is available
            t_String szTelephoneNumber; 
            CmdLaunchApp oCmdLaunchApp(m_poLaunchApp, u32DeviceHandle, u32AppHandle,
               e8DIPO_NOT_USED, szTelephoneNumber, e8ECNR_NOCHANGE);
            oCmdLaunchApp.vSetUserContext(corEmptyUsrContext);
            oCmdLaunchApp.vSetServiceID(e32COMMANDID_LAUNCHAPP);
            if (NULL != m_poCmdMsqQ)
            {
               m_poCmdMsqQ->bWriteMsgToQ(&oCmdLaunchApp, sizeof(oCmdLaunchApp));
            }//if (NULL != m_poCmdMsqQ)
         }//if((true == bIslaunchAppreq)&&(NULL != m_poLaunchApp))
      }//if ((NULL != poAppMngr)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vInvokeNotificationAction left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vGetVideoSettings
**                   (const t_U32 cou32DeviceHandle, trVideoAttributes& rfrVideoAttr)
***************************************************************************/
t_Void spi_tclCmdInterface::vGetVideoSettings(const t_U32 cou32DeviceHandle,
   trVideoAttributes& rfrVideoAttr)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetVideoSettings entered"));
   if (NULL != m_poFactory)
   {
      spi_tclVideo *poVideo = m_poFactory->poGetVideoInstance();
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();

      if((NULL != poVideo)&&(NULL != poConnMngr ))
      {
         //Get teh Device category  from Conn Mngr
         tenDeviceCategory enDevCat= poConnMngr->enGetDeviceCategory(cou32DeviceHandle);
         poVideo->vGetVideoSettings(cou32DeviceHandle, rfrVideoAttr, enDevCat);
      }//if(NULL != poVideo )
   }//if(NULL!= m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetVideoSettings left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetOrientationMode
**                (const t_U32 cou32DeviceHandle, , tenOrientationMode enOrientationMode,..)
***************************************************************************/
t_Void spi_tclCmdInterface::vSetOrientationMode(const t_U32 cou32DeviceHandle,
   tenOrientationMode enOrientationMode,
   const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetOrientationMode entered"));
   if (NULL != m_poFactory)
   {
      spi_tclVideo *poVideo = m_poFactory->poGetVideoInstance();
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();

      if((NULL != poVideo)&&(NULL != poConnMngr ))
      {
         tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(cou32DeviceHandle);
         CmdOrientationMode oOrientationMode(poVideo, cou32DeviceHandle, enDevCat,
                  enOrientationMode);
         oOrientationMode.vSetUserContext(rfcorUsrCntxt);
         oOrientationMode.vSetServiceID(e32COMMANDID_SETORIENTATIONMODE);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oOrientationMode, sizeof(oOrientationMode));
         }
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetOrientationMode left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetScreenSize
**                   (const trScreenAttributes& corfrScreenAttributes,..)
***************************************************************************/
t_Void spi_tclCmdInterface::vSetScreenSize(
   const trScreenAttributes& corfrScreenAttributes,
   const trUserContext& rfcorUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetScreenSize entered"));

   if (NULL != m_poFactory)
   {
      tenDeviceCategory enDevCat = enGetDeviceCategory(u32GetSelectedDeviceHandle());
      spi_tclVideo *poVideo = m_poFactory->poGetVideoInstance();

      if (NULL != poVideo)
      {
         CmdScreenSize oScreenSize(poVideo, enDevCat, corfrScreenAttributes);
         oScreenSize.vSetUserContext(rfcorUsrCntxt);
         oScreenSize.vSetServiceID(e32COMMANDID_SETSCREENSIZE);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oScreenSize, sizeof(oScreenSize));
         }
      }//if (NULL != poVideo)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetScreenSize left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetVideoBlockingMode
**                (const t_U32 cou32DeviceHandle, tenBlockingMode enBlockingMode,..)
***************************************************************************/
t_Void spi_tclCmdInterface::vSetVideoBlockingMode(const t_U32 cou32DeviceHandle,
   tenBlockingMode enBlockingMode,
   const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetVideoBlockingMode entered"));
   ETG_TRACE_USR2(("[DESC]::vSetVideoBlockingMode received value of Device Handle-0x%x BlockingMode-%d",
      cou32DeviceHandle,ETG_ENUM(BLOCKING_MODE,enBlockingMode)));

   if (NULL != m_poFactory)
   {
      tenDeviceCategory enDevCat = enGetDeviceCategory(cou32DeviceHandle);
      spi_tclVideo *poVideo = m_poFactory->poGetVideoInstance();

      if (NULL != poVideo)
      {
         CmdSetVideoBlocking oBlockingMode(poVideo, e8BLOCKINGTYPE_VIDEO,
                  cou32DeviceHandle, enDevCat, enBlockingMode);
         oBlockingMode.vSetUserContext(rfcorUsrCntxt);
         oBlockingMode.vSetServiceID(e32COMMANDID_SETVIDEOBLOCKINGMODE);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oBlockingMode, sizeof(oBlockingMode));
         }
      }//if (NULL != poVideo)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetVideoBlockingMode left"));
}

/***************************************************************************
** FUNCTION: t_Bool spi_tclCmdInterface::bSetAudioBlockingMode()
***************************************************************************/
t_Bool spi_tclCmdInterface::bSetAudioBlockingMode(t_U32 u32DeviceHandle,
   tenBlockingMode enBlockingMode)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::bSetAudioBlockingMode() entered "));
   t_Bool bRet=false;
   if (NULL != m_poFactory)
   {
      spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();

      if((NULL != poAudio)&&(NULL != poConnMngr ))
      {
         //Get Device category  from Conn Mngr
         tenDeviceCategory enDevCat= poConnMngr->enGetDeviceCategory(u32DeviceHandle);
         bRet=poAudio->bSetAudioBlockingMode(u32DeviceHandle, enBlockingMode, enDevCat);
      }//if(NULL != poAudio )
   }//if(NULL!= m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::bSetAudioBlockingMode() left "));
   return bRet;
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetVehicleConfig()
***************************************************************************/
t_Void spi_tclCmdInterface::vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
   t_Bool bSetConfig,const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetVehicleMode() entered"));
   ETG_TRACE_USR1(("[DESC]::vSetVehicleMode()received value of enVehicleConfig -%d ",enVehicleConfig));

   if (NULL != m_poFactory)
   {
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      spi_tclVideo *poVideo = m_poFactory->poGetVideoInstance();

      if ((NULL != poAppMngr) && (NULL != poVideo))
      {
         CmdSetVehicleConfig oVehicleConfig(poVideo, poAppMngr, bSetConfig,
                  enVehicleConfig);
         oVehicleConfig.vSetUserContext(corfrUsrCntxt);
         oVehicleConfig.vSetServiceID(e32COMMANDID_SETVEHICLECONFIG);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oVehicleConfig, sizeof(oVehicleConfig));
         }
      }//if ((NULL != poAppMngr)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetVehicleMode() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vGetVehicleMode(tenVehicleMode&...)
***************************************************************************/
t_Void spi_tclCmdInterface::vGetVehicleMode(tenVehicleConfiguration& rfenVehicleConfig)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetVehicleMode() entered"));

   SPI_INTENTIONALLY_UNUSED(rfenVehicleConfig);
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetVehicleMode() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSendTouchEvent(t_U32 u32DeviceHandle..)
***************************************************************************/
t_Void spi_tclCmdInterface::vSendTouchEvent(t_U32 u32DeviceHandle,
   trTouchData& rfrTouchData,
   const trUserContext& rfcorUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSendTouchEvent() entered"));
   if (NULL != m_poFactory)
   {
      spi_tclInputHandler *poInputHandler =
               m_poFactory->poGetInputHandlerInstance();
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      if ((NULL != poConnMngr) && (NULL != poInputHandler))
      {
         tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(
                  u32DeviceHandle);
         CmdTouchEvent oTouchEvent(poInputHandler, u32DeviceHandle, enDevCat,
                  rfrTouchData);
         oTouchEvent.vSetUserContext(rfcorUsrCntxt);
         oTouchEvent.vSetServiceID(e32COMMANDID_SENDTOUCHEVENT);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oTouchEvent, sizeof(oTouchEvent));
         }
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSendTouchEvent() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSendKeyEvent(t_U32 u32DeviceHandle..)
***************************************************************************/
t_Void spi_tclCmdInterface::vSendKeyEvent(t_U32 u32DeviceHandle, 
                                          tenKeyMode enKeyMode,
                                          tenKeyCode enKeyCode,
                                          const trUserContext& rfcorUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSendKeyEvent() entered"));
   if (NULL != m_poFactory)
   {
      spi_tclInputHandler *poInputHandler =
               m_poFactory->poGetInputHandlerInstance();
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      if ((NULL != poConnMngr) && (NULL != poInputHandler))
      {
         tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(
                  u32DeviceHandle);
         CmdKeyEvent oKeyEvent(poInputHandler, u32DeviceHandle, enDevCat,
                  enKeyMode, enKeyCode);
         oKeyEvent.vSetUserContext(rfcorUsrCntxt);
         oKeyEvent.vSetServiceID(e32COMMANDID_SENDKEYEVENT);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oKeyEvent, sizeof(oKeyEvent));
         }
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSendKeyEvent() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetAccessoryDisplayContext(t_U32...
***************************************************************************/
t_Void spi_tclCmdInterface::vSetAccessoryDisplayContext(t_U32 u32DeviceHandle,
   t_Bool bDisplayFlag,
   tenDisplayContext enDisplayContext,
   const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAccessoryDisplayContext() entered"));
      
   if (NULL != m_poFactory)
   {
      spi_tclResourceMngr *poRsrcMngr = m_poFactory->poGetRsrcMngrInstance();
      if (NULL != poRsrcMngr)
      {
         //Get the Device category  from Conn Mngr
         tenDeviceCategory enDevCat = enGetDeviceCategory(u32DeviceHandle);
         //send the message to resource manager 
         poRsrcMngr->vSetAccessoryDisplayContext(u32DeviceHandle,
               bDisplayFlag,enDisplayContext,enDevCat,rfcorUsrCntxt);
      }//if (NULL != poRsrcMngr)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAccessoryDisplayContext() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetAccessoryDisplayMode(t_U32...
***************************************************************************/
t_Void spi_tclCmdInterface::vSetAccessoryDisplayMode(const t_U32 cu32DeviceHandle,
                                                     const trDisplayContext corDisplayContext,
                                                     const trDisplayConstraint corDisplayConstraint,
                                                     const tenDisplayInfo coenDisplayInfo)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAccessoryDisplayMode() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclResourceMngr *poRsrcMngr = m_poFactory->poGetRsrcMngrInstance();
      if (NULL != poRsrcMngr)
      {
         //Get the Device category  from Conn Mngr
         tenDeviceCategory enDevCat = enGetDeviceCategory(cu32DeviceHandle);
         //send the message to resource manager 
         poRsrcMngr->vSetAccessoryDisplayMode(cu32DeviceHandle, enDevCat,
            corDisplayContext,corDisplayConstraint,coenDisplayInfo);
      }//if (NULL != poRsrcMngr)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAccessoryDisplayMode() left"));
}


/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vGetDeviceDisplayContext(t_U32...
***************************************************************************/
t_Void spi_tclCmdInterface::vGetDeviceDisplayContext(t_U32 u32DeviceHandle,
	   t_Bool& rfbDisplayFlag,
	   tenDisplayContext& rfenDisplayContext)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetDeviceDisplayContext() entered"));
   if (NULL != m_poFactory)
   {
      spi_tclResourceMngr *poResourceMngr = m_poFactory->poGetRsrcMngrInstance();
      if(NULL != poResourceMngr)
      {
         poResourceMngr->vGetDisplayContextInfo(u32DeviceHandle, rfbDisplayFlag, rfenDisplayContext);       
      }//if ((NULL != poAppMngr)
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetDeviceDisplayContext() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetAccessoryAudioContext(t_U32...
***************************************************************************/
t_Void spi_tclCmdInterface::vSetAccessoryAudioContext(t_U32 u32DeviceHandle, const tenAudioContext coenAudioCntxt,
      t_Bool bReqFlag, const trUserContext& rfrcUsrCntxt,
      tenDeviceCategory enDevCategory)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAccessoryAudioContext entered"));
   ETG_TRACE_USR1(("[DESC]::vSetAccessoryAudioContext received value of "
         "DeviceHandle %d, AudioCntxt %d, bReqFlag %d, DevCategory %d",
         u32DeviceHandle,
         ETG_ENUM(AUDIO_CONTEXT, coenAudioCntxt),
         ETG_ENUM(BOOL, bReqFlag),
         ETG_ENUM(DEVICE_CATEGORY, enDevCategory)));

   if(NULL !=m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      spi_tclResourceMngr *poRsrcMngr = m_poFactory->poGetRsrcMngrInstance();
      if((NULL != poRsrcMngr) && (NULL != poConnMngr))
      {
         //Get the Device category  from Conn Mngr
         tenDeviceCategory enDevCat= poConnMngr->enGetDeviceCategory(u32DeviceHandle);
         poRsrcMngr->vSetAccessoryAudioContext(u32DeviceHandle, coenAudioCntxt, bReqFlag, enDevCat, rfrcUsrCntxt, enDevCategory);
      }      
   } //  if(NULL !=m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAccessoryAudioContext left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetAccessoryAppState()
***************************************************************************/
t_Void spi_tclCmdInterface::vSetAccessoryAppState(const tenSpeechAppState enSpeechAppState, const tenPhoneAppState enPhoneAppState,
                                                  const tenNavAppState enNavAppState, const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAccessoryAppState() entered"));
   if (NULL != m_poFactory)
   {
      //Get the Device category
      tenDeviceCategory enDevCat = enGetDeviceCategory(u32GetSelectedDeviceHandle());
      spi_tclResourceMngr *poRsrcMngr = m_poFactory->poGetRsrcMngrInstance();
      if (NULL != poRsrcMngr)
      {
         poRsrcMngr->vSetAccessoryAppState(enDevCat, enSpeechAppState, enPhoneAppState, enNavAppState, rfrcUsrCntxt);
      }
   } //if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetAccessoryAppState() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vGetAppStateInfo()
***************************************************************************/
t_Void spi_tclCmdInterface::vGetAppStateInfo(tenSpeechAppState &enSpeechAppState, tenPhoneAppState 
                                             &enPhoneAppState, tenNavAppState &enNavAppState) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetAppStateInfo entered"));
   if(NULL != m_poFactory)
   {
      spi_tclResourceMngr *poRsrcMngr = m_poFactory->poGetRsrcMngrInstance();
      if(NULL != poRsrcMngr)
      {
         poRsrcMngr->vGetAppStateInfo(enSpeechAppState, enPhoneAppState, enNavAppState);
      }
   } //if(NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetAppStateInfo left"));
}


/**************************************************************************
** FUNCTION   : tVoid spi_tclCmdInterface:: vSetTechnologyPreference()
***************************************************************************/
t_Void spi_tclCmdInterface::vSetTechnologyPreference(const t_U32 cou32DeviceHandle,
      std::vector<tenDeviceCategory> vecTechnologyPreference) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetTechnologyPreference() entered "));
   if(NULL !=m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      if(NULL != poConnMngr)
      {
         poConnMngr->vSetTechnologyPreference(cou32DeviceHandle, vecTechnologyPreference);
      }
   } //  if(NULL !=m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetTechnologyPreference() left "));
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclCmdInterface:: vGetTechnologyPreference()
***************************************************************************/
t_Void spi_tclCmdInterface::vGetTechnologyPreference(const t_U32 cou32DeviceHandle,
      std::vector<tenDeviceCategory> &rfrvecTechnologyPreference) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetTechnologyPreference() entered"));
   if(NULL !=m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      if(NULL != poConnMngr)
      {
         poConnMngr->vGetTechnologyPreference(cou32DeviceHandle, rfrvecTechnologyPreference);
      }
   } //  if(NULL !=m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetTechnologyPreference() left"));
}
/***************************************************************************
 ** FUNCTION:  spi_tclCmdInterface::vSetDeviceAuthorization
 ***************************************************************************/
t_Void spi_tclCmdInterface::vSetDeviceAuthorization(const t_U32 cou32DeviceHandle, tenUserAuthorizationStatus enUserAuthStatus)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetDeviceAuthorization() entered"));
   ETG_TRACE_USR1(("[DESC]::vSetDeviceAuthorization() received values of  cou32DeviceHandle = %d,  enUserAuthStatus = %d\n",
         cou32DeviceHandle, ETG_ENUM(AUTHORIZATION_STATUS, enUserAuthStatus)));
   if(NULL !=m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      if(NULL != poConnMngr)
      {
         poConnMngr->vSetDeviceAuthorization(cou32DeviceHandle, enUserAuthStatus);
      }
   } //  if(NULL !=m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetDeviceAuthorization() left"));
}


/***************************************************************************
** FUNCTION:  t_Bool spi_tclCmdInterface::bGetAudSrcInfo(const t_U8 cu8SourceNum,
**             trAudSrcInfo& rfrSrcInfo)
***************************************************************************/
t_Bool spi_tclCmdInterface::bGetAudSrcInfo(const t_U8 cu8SourceNum, trAudSrcInfo& rfrSrcInfo)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetAudioSrcInfo() entered "));

   t_Bool bRetVal = false;

   if(NULL != m_poFactory)
   {
      spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
      if (NULL != poAudio)
      {
         bRetVal = poAudio->bGetAudSrcInfo(cu8SourceNum, rfrSrcInfo);
      }//End of if(NULL != poAudio)
   }//End of if(NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetAudioSrcInfo() left "));
   return bRetVal;
}//spi_tclCmdInterface::vGetAudioSrcInfo(const t_U8 cu8SourceNum,..)



/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vInvokeBluetoothDeviceAction
**                   (t_U32 u32BluetoothDevHandle,..)
***************************************************************************/
t_Void spi_tclCmdInterface::vInvokeBluetoothDeviceAction(
         t_U32 u32BluetoothDevHandle, t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enBTChange)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vInvokeBluetoothDeviceAction() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclBluetooth *poBluetooth = m_poFactory->poGetBluetoothInstance();
      if (NULL != poBluetooth)
      {
         CmdBTDeviceAction oBTDeviceAction(poBluetooth, u32BluetoothDevHandle,
                  u32ProjectionDevHandle, enBTChange);
         oBTDeviceAction.vSetServiceID(e32COMMANDID_BTDEVICEACTION);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oBTDeviceAction, sizeof(oBTDeviceAction));
         }
      }//if(NULL != poBluetooth)
   }//if(NULL!= m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vInvokeBluetoothDeviceAction() left"));
}


/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetClientCapabilities()
***************************************************************************/
t_Void spi_tclCmdInterface::vSetClientCapabilities(const trClientCapabilities& corfrClientCapabilities, 
                                                   const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetClientCapabilities() entered"));
   if (NULL != m_poFactory)
   {
      spi_tclVideo *poVideo = m_poFactory->poGetVideoInstance();
      if(NULL != poVideo)
      {
         //poVideo->vSetClientCapabilities(corfrClientCapabilities);
         CmdClientCapabilities oClientCapabilities(poVideo, corfrClientCapabilities);
         oClientCapabilities.vSetUserContext(rfcorUsrCntxt);
         oClientCapabilities.vSetServiceID(e32COMMANDID_SETCLIENTCAPABILITIES);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oClientCapabilities, sizeof(oClientCapabilities));
         }
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetClientCapabilities() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetVehicleBTAddress(.)
***************************************************************************/
t_Void spi_tclCmdInterface::vSetVehicleBTAddress(t_String szBTAddress, 
                                                 const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetVehicleBTAddress() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      if((NULL != m_poDevSelector)&&(NULL != poConnMngr )&&(NULL != poAppMngr))
      {
         t_U32 u32DevId = m_poDevSelector->u32GetSelectedDeviceHandle();
         tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(u32DevId);
         CmdVehicleBTAddress oVehicleBTAddress(poAppMngr, u32DevId,
                  szBTAddress, enDevCat);
         oVehicleBTAddress.vSetUserContext(rfcorUsrCntxt);
         oVehicleBTAddress.vSetServiceID(e32COMMANDID_BTADDRESS);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oVehicleBTAddress,
                     sizeof(oVehicleBTAddress));
         }
     }//if((NULL != m_poDevSelector)&&(NULL != poConnMngr )
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetVehicleBTAddress() left"));
}

/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vSetRegion(.)
***************************************************************************/
t_Void spi_tclCmdInterface::vSetRegion(tenRegion enRegion)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetRegion() entered"));
   ETG_TRACE_USR2(("[DESC]::vSetRegion received value of enRegion =%d",enRegion));
   if (NULL != m_poFactory)
   {
      spi_tclAppMngr *poAppMngr = m_poFactory->poGetAppManagerInstance();
      if(NULL != poAppMngr)
      {
         poAppMngr->vSetRegion(enRegion);
      }//if ((NULL != poAppMngr)

      spi_tclDataService* poDataService = m_poFactory->poGetDataServiceInstance();
      if (NULL != poDataService)
      {
         poDataService->vSetRegion(enRegion);
      }
   }//if (NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetRegion() left"));
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::bIsDiPoRoleSwitchRequired(tU32 ..)
***************************************************************************/
t_Bool spi_tclCmdInterface::bIsDiPoRoleSwitchRequired(const t_U32 cou32DeviceID, const trUserContext corUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::bIsDiPoRoleSwitchRequired() entered"));
   ETG_TRACE_USR2(("[DESC]::bIsDiPoRoleSwitchRequired received value of cou32DeviceID = %d\n",  cou32DeviceID));

   t_Bool bRoleSwitchRequired = false;
   if (NULL != m_poFactory)
   {
      spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
      if(NULL != poConnMngr)
      {
         bRoleSwitchRequired = poConnMngr->bIsDiPoRoleSwitchRequired(cou32DeviceID, corUsrCntxt);
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::bIsDiPoRoleSwitchRequired() left"));
   return bRoleSwitchRequired;

}
/**************************Start of Audio methods**************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnRouteAllocateResult(const tU8, ..)
***************************************************************************/
t_Bool spi_tclCmdInterface::bOnRouteAllocateResult(const t_U8 cu8SourceNum ,
      trAudSrcInfo& rfrSrcInfo)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::bOnRouteAllocateResult() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
      if (NULL != poAudio)
      {
         CmdAllocateAudioRoute oAllocateAudioRoute(poAudio, cu8SourceNum,
                  rfrSrcInfo);
         oAllocateAudioRoute.vSetServiceID(
                  e32COMMANDID_AUDIO_ROUTEALLOCATE_RESULT);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oAllocateAudioRoute,
                     sizeof(oAllocateAudioRoute));
         }
      } //if(NULL != poAudio)
   } //if(NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::bOnRouteAllocateResult() left"));
   return true; //dummy value, unused

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnRouteDeAllocateResult(const tU8)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnRouteDeAllocateResult(const t_U8 cu8SourceNum)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnRouteDeAllocateResult() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
      if (NULL != poAudio)
      {
         CmdDeAllocateAudioRoute oDeAllocateAudioRoute(poAudio, cu8SourceNum);
         oDeAllocateAudioRoute.vSetServiceID(
                  e32COMMANDID_AUDIO_ROUTEDEALLOCATE_RESULT);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oDeAllocateAudioRoute,
                     sizeof(oDeAllocateAudioRoute));
         }
      } //if(NULL != poAudio)
   } //if(NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnRouteDeAllocateResult() left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnStartSourceActivity(const tU8)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnStartSourceActivity(const t_U8 cu8SourceNum)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnStartSourceActivity() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
      if (NULL != poAudio)
      {
         CmdStartAudioSrcActivity oStartAudioSrcActivity(poAudio, cu8SourceNum);
         oStartAudioSrcActivity.vSetServiceID(
                  e32COMMANDID_AUDIO_STARTSOURCEACTIVITY);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oStartAudioSrcActivity,
                     sizeof(oStartAudioSrcActivity));
         }
      } //if(NULL != poAudio)
   } //if(NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnStartSourceActivity() Left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnStopSourceActivity(const tU8)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnStopSourceActivity(const t_U8 cu8SourceNum)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnStopSourceActivity() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
      if (NULL != poAudio)
      {
         CmdStopAudioSrcActivity oStopAudioSrcActivity(poAudio, cu8SourceNum);
         oStopAudioSrcActivity.vSetServiceID(
                  e32COMMANDID_AUDIO_STOPSOURCEACTIVITY);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oStopAudioSrcActivity,
                     sizeof(oStopAudioSrcActivity));
         }
      } //if(NULL != poAudio)
   } //if(NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnStopSourceActivity() left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::bOnReqAVDeActivationResult(const tU8, t_Bool)
***************************************************************************/
t_Bool spi_tclCmdInterface::bOnReqAVDeActivationResult(const t_U8 cu8SourceNum)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::bOnReqAVDeActivationResult() entered"));

   if (NULL != m_poFactory)
   {
      spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
      if (NULL != poAudio)
      {
         CmdReqAVDeactResult oAVDeactResult(poAudio, cu8SourceNum);
         oAVDeactResult.vSetServiceID(e32COMMANDID_AUDIO_REQAV_DEACTIVATION_RESULT);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oAVDeactResult, sizeof(oAVDeactResult));
         }
      } //if(NULL != poAudio)
   } //if(NULL != m_poFactory)*/
   ETG_TRACE_USR1(("spi_tclCmdInterface::bOnReqAVDeActivationResult() left"));
   return true; //dummy value, unused
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnAudioError(const tU8)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnAudioError( const t_U8 cu8SourceNum, tenAudioError enAudioError)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnAudioError() entered"));
   ETG_TRACE_USR2(("[DESC]vOnAudioError() received the values of cu8SourceNum = %d, enAudioError =%d\n",
            cu8SourceNum, ETG_ENUM(AUDIO_ERROR, enAudioError)));
   if (NULL != m_poFactory)
   {
      spi_tclAudio *poAudio = m_poFactory->poGetAudioInstance();
      if (NULL != poAudio)
      {
         CmdAudioError oAudioError(poAudio, cu8SourceNum,enAudioError );
         oAudioError.vSetServiceID(e32COMMANDID_AUDIO_ERROR);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oAudioError, sizeof(oAudioError));
         }
      } //if(NULL != poAudio)
   } //if(NULL != m_poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnAudioError() left"));
}

/**************************End of Audio methods***************************/

/***************************************************************************
** FUNCTION:  tenDeviceCategory spi_tclCmdInterface::enGetDeviceCategory(t_U32)
***************************************************************************/
tenDeviceCategory spi_tclCmdInterface::enGetDeviceCategory(t_U32 u32DeviceHandle) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::enGetDeviceCategory() entered"));
   tenDeviceCategory enDevCat = e8DEV_TYPE_UNKNOWN;
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclConnMngr *poConnMngr = poFactory->poGetConnMngrInstance();
      if(NULL != poConnMngr)
      {
         enDevCat = poConnMngr->enGetDeviceCategory(u32DeviceHandle);
      }
   }//if (NULL != poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::enGetDeviceCategory() Left"));
   return enDevCat;

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnCallStatus(t_Bool bCallActive)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnCallStatus(t_Bool bCallActive)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnCallStatus() entered"));
   //! Forward call status to BT Manager
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclBluetooth *poBluetooth = poFactory->poGetBluetoothInstance();
      if (NULL != poBluetooth)
      {
         CmdCallStatus oCallStatus(poBluetooth, bCallActive);
         oCallStatus.vSetServiceID(e32COMMANDID_CALLSTATUS);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oCallStatus, sizeof(oCallStatus));
         }
      }//if (NULL != poBluetooth)
   }//if (NULL != poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnCallStatus() Left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnGPSData(const trGPSData& rfcoGPSData)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnGPSData(const trGPSData& rfcoGPSData)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnGPSData() entered"));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
      if(NULL!= poDataService)
      {
         CmdGPSData oGPSData(poDataService, rfcoGPSData);
         oGPSData.vSetServiceID(e32COMMANDID_GPSDATA);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oGPSData, sizeof(oGPSData));
         }
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnGPSData() left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnSensorData(const trSensorData& 
rfcoSensorData)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnSensorData(const trSensorData& rfcoSensorData)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnSensorData() entered"));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
      if(NULL!= poDataService)
      {
         CmdSensorData oSensorData(poDataService, rfcoSensorData);
         oSensorData.vSetServiceID(e32COMMANDID_SENSORDATA);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oSensorData, sizeof(oSensorData));
         }
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnSensorData() Left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnAccSensorData(const
** trAccSensorData& corfvecrAccSensorData)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnAccSensorData() entered"));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
      if(NULL!= poDataService)
      {
    	  CmdAccSensorData oAccSensorData(poDataService, corfvecrAccSensorData);
    	  oAccSensorData.vSetServiceID(e32COMMANDID_ACCSENSORDATA);
    	  if (NULL != m_poCmdMsqQ)
    	  {
    		  m_poCmdMsqQ->bWriteMsgToQ(&oAccSensorData, sizeof(oAccSensorData));
    	  }
    	  //poDataService->vOnAccSensorData(corfvecrAccSensorData);
      }
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnGyroSensorData(const
** std::vector<trGyroSensorData>& corfvecrGyroSensorData)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnGyroSensorData() entered"));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
      if(NULL!= poDataService)
      {
        CmdGyroSensorData oGyroSensorData(poDataService, corfvecrGyroSensorData);
        oGyroSensorData.vSetServiceID(e32COMMANDID_GYROSENSORDATA);
        if (NULL != m_poCmdMsqQ)
        {
           m_poCmdMsqQ->bWriteMsgToQ(&oGyroSensorData, sizeof(oGyroSensorData));
        }
        //poDataService->vOnGyroSensorData(corfvecrGyroSensorData);
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnGyroSensorData() Left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vOnVehicleData(const trVehicleData& 
rfcoVehicleData)
***************************************************************************/
t_Void spi_tclCmdInterface::vOnVehicleData(const trVehicleData& rfcoVehicleData, t_Bool bSolicited)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnVehicleData() entered"));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
      spi_tclAppMngr *poAppMngr = poFactory->poGetAppManagerInstance();
      spi_tclVideo *poVideo = poFactory->poGetVideoInstance();

      if((NULL!= poDataService) && (NULL != poAppMngr) && (NULL != poVideo))
      {
         CmdVehicleData oVehicleData(poDataService, poVideo, poAppMngr, rfcoVehicleData, bSolicited);
         oVehicleData.vSetServiceID(e32COMMANDID_VEHICLEDATA);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oVehicleData, sizeof(oVehicleData));
         }
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vOnVehicleData() Left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vSetLocDataAvailablility(t_Bool...)
***************************************************************************/
t_Void spi_tclCmdInterface::vSetLocDataAvailablility(t_Bool bLocDataAvailable,
      t_Bool bIsDeadReckonedData)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetLocDataAvailablility() entered"));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
      if (NULL!= poDataService)
      {
         poDataService->vSetLocDataAvailablility(bLocDataAvailable, bIsDeadReckonedData);
      }
   }//if (NULL != poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetLocDataAvailablility() Left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vSetDispAttr()
***************************************************************************/
t_Void spi_tclCmdInterface::vSetDispAttr(const trDisplayAttributes& corfrDispLayerAttr)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetDispAttr() entered"));
   spi_tclConfigReader* poCnfgReader = spi_tclConfigReader::getInstance();
   if( (NULL != poCnfgReader) && (NULL != m_poFactory))
   {
      poCnfgReader->vSetScreenConfigs(corfrDispLayerAttr);
      spi_tclVideo *poVideo = m_poFactory->poGetVideoInstance();
      if(NULL != poVideo)
      {
         trVideoConfigData rVideoCnfgData;
         poCnfgReader->vGetVideoConfigData(e8DEV_TYPE_DIPO,rVideoCnfgData);
         //Currently commented updating carplay dynamically. this needs to be tested and 
         //then released.
         poVideo->vSetScreenAttr(rVideoCnfgData,e8DEV_TYPE_DIPO);
      }
   }//if(NULL != poCnfgReader)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetDispAttr() Left"));
}
/***************************************************************************
** FUNCTION: t_Void spi_tclCmdInterface::vGetKeyIconData(t_String szAppIconURL,..)
***************************************************************************/
t_Void spi_tclCmdInterface::vGetKeyIconData(t_U32 u32DeviceHandle,t_String szKeyIconURL,
   const trUserContext& rfcorUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetKeyIconData() entered"));
   ETG_TRACE_USR1(("[DESC]::vGetKeyIconData() received value for device handle =%d",u32DeviceHandle));

   if (NULL != m_poFactory)
   {

      tenDeviceCategory enDevCat = enGetDeviceCategory(u32DeviceHandle);

      spi_tclInputHandler* poInputHandler = m_poFactory->poGetInputHandlerInstance();

      if (NULL != poInputHandler)
      {
         CmdKeyIconData oCmdKeyIconData(u32DeviceHandle,poInputHandler, enDevCat, szKeyIconURL);
         oCmdKeyIconData.vSetUserContext(rfcorUsrCntxt);
         oCmdKeyIconData.vSetServiceID(e32COMMANDID_GETKEYICONDATA);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oCmdKeyIconData,
                     sizeof(oCmdKeyIconData));
         }
      }
   }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vGetKeyIconData() Left"));
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclCmdInterface::vSetVehicleBTAddress(t_Bool...)
***************************************************************************/
t_Void spi_tclCmdInterface::vSetVehicleBTAddress(t_String szBtAddress)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetVehicleBTAddress() entered"));
	 spi_tclFactory *poFactory = spi_tclFactory::getInstance();
	 if(NULL != poFactory)
	 {
	      spi_tclResourceMngr *poRsrcMngr = poFactory->poGetRsrcMngrInstance();
	      if (NULL != poRsrcMngr)
	      {
	    	  poRsrcMngr->vSetVehicleBTAddress(szBtAddress);
	      }//if (NULL != poRsrcMngr)
	 }//if(NULL != poFactory)
	 ETG_TRACE_USR1(("spi_tclCmdInterface::vSetVehicleBTAddress() left"));
}

/***************************************************************************
** FUNCTION:  t_Void vSendKnobKeyEvent(t_U32 u32DeviceHandle ..),
***************************************************************************/
t_Void spi_tclCmdInterface::vSendKnobKeyEvent(t_U32 u32DeviceHandle,t_S8 s8EncodeDeltaCnts,
                    const trUserContext& rfcorUsrCntxt) const
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSendKnobKeyEvent() entered"));
   if (NULL != m_poFactory)
      {
         spi_tclInputHandler *poInputHandler =
                  m_poFactory->poGetInputHandlerInstance();
         spi_tclConnMngr* poConnMngr = m_poFactory->poGetConnMngrInstance();
         if ((NULL != poConnMngr) && (NULL != poInputHandler))
         {
            tenDeviceCategory enDevCat = poConnMngr->enGetDeviceCategory(
                     u32DeviceHandle);
            CmdKnobKeyEvent oKeyEvent(poInputHandler, u32DeviceHandle, enDevCat,
                  s8EncodeDeltaCnts);
            oKeyEvent.vSetUserContext(rfcorUsrCntxt);
            oKeyEvent.vSetServiceID(e32COMMANDID_SENDKNOBKEYEVENT);
            if (NULL != m_poCmdMsqQ)
            {
               m_poCmdMsqQ->bWriteMsgToQ(&oKeyEvent, sizeof(oKeyEvent));
            }
         }
      }
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSendKnobKeyEvent() Left"));
}

/***************************************************************************
** FUNCTION:  spi_tclCmdInterface::vReportEnvironmentData()
***************************************************************************/
t_Void spi_tclCmdInterface::vReportEnvironmentData(t_Bool bValidTempUpdate,
                                                   t_Double dTemp,
                                                   t_Bool bValidPressureUpdate, 
                                                   t_Double dPressure)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vReportEnvironmentData() entered"));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
      if (NULL!= poDataService)
      {
         poDataService->vReportEnvironmentData(bValidTempUpdate,dTemp,
            bValidPressureUpdate,dPressure);
      }//if (NULL!= poDataService)
   }//if (NULL != poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vReportEnvironmentData() left"));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclCmdInterface::vSetFeatureRestrictions(...)
 ***************************************************************************/
t_Void spi_tclCmdInterface::vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
      const t_U8 cou8ParkModeRestrictionInfo,const t_U8 cou8DriveModeRestrictionInfo)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetFeatureRestrictions entered"));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
      spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();

      if (OSAL_NULL != poConfigReader)
      {
         //send restriction data to config reader
         poConfigReader->vSetFeatureRestrictions(enDevCategory,
               cou8ParkModeRestrictionInfo, cou8DriveModeRestrictionInfo);
      }

      spi_tclResourceMngr* poResourceMngr = poFactory->poGetRsrcMngrInstance();
      if ((NULL!= poDataService) && (NULL != poResourceMngr))
      {
         CmdSetFeatRestrData oSetFeatRestrData(poDataService, poResourceMngr, enDevCategory,
               cou8ParkModeRestrictionInfo, cou8DriveModeRestrictionInfo);
         oSetFeatRestrData.vSetServiceID(e32COMMANDID_SETFEATURERESTRICTIONDATA);
         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oSetFeatRestrData, sizeof(oSetFeatRestrData));
         }
      }//if ((NULL!= poDataService) && (NULL != poResourceMngr))
   }//if (NULL != poFactory)
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetFeatureRestrictions Left"));
}

/***************************************************************************
 ** FUNCTION:  t_Void vSetDeviceSelectionMode()
 ***************************************************************************/
t_Void spi_tclCmdInterface::vSetDeviceSelectionMode(tenDeviceSelectionMode enDeviceSelectionMode)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vSetDeviceSelectionMode entered"));
   if(NULL != m_poDevSelector)
   {
      m_poDevSelector->vSetDeviceSelectionMode(enDeviceSelectionMode);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void vAckNotification()
 ***************************************************************************/
t_Void spi_tclCmdInterface::vAckNotification(const trNotificationAckData& corfrNotifAckData)
{
   ETG_TRACE_USR1(("spi_tclCmdInterface::vAckNotification entered"));
   ETG_TRACE_USR2(("[DESC]:vAckNotification: Device Handle = 0x%x, Device category = %d,"
                   "Notification Id = %s", corfrNotifAckData.u32DeviceHandle,
                   ETG_ENUM(DEVICE_CATEGORY, corfrNotifAckData.enDeviceCategory),
                   corfrNotifAckData.szNotifId.c_str()));
   spi_tclFactory *poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      spi_tclAppMngr* poAppMngr = poFactory->poGetAppManagerInstance();

      if (NULL != poAppMngr)
      {
         //Create a message for AckNotificationEvent to dispatch it to AppMngr
         CmdAckNotificationEvent oAckNotificationEvent(poAppMngr, corfrNotifAckData);
         oAckNotificationEvent.vSetServiceID(e32COMMANDID_ACKNOTIFICATIONEVENT);

         if (NULL != m_poCmdMsqQ)
         {
            m_poCmdMsqQ->bWriteMsgToQ(&oAckNotificationEvent, sizeof(oAckNotificationEvent));
         } //if (NULL != m_poCmdMsqQ)
      } //if (NULL != poAppMngr)
   } //if (NULL != poFactory)
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
