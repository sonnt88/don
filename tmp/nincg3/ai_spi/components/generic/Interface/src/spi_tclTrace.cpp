/***********************************************************************/
/*!
* \file  spi_tclTrace.cpp
* \brief Trace Commands Handler
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Trace Commands Handler
AUTHOR:         Vinoop
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
11.02.2014  | Vinoop U               | Initial Version
26.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors


\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include "AAPTypes.h"
#include "spi_tclMPlayClientHandler.h"
#include "TraceStreamable.h"
#include "spi_tclFactory.h"
#include "spi_tclTrace.h"
#include "spi_tclDiPOTraceCommands.h"
#include "spi_tclImpTraceStreamable.h"
#include "spi_tclAudio.h"
#include "spi_tclResourceMngr.h"
#include "spi_tclCmdInterface.h"
#include "spi_tclConfigReader.h"
#include "FileHandler.h"


#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclTrace.cpp.trc.h"
#endif
#endif

//#define TTFIS_CMD_BYTE_LEN 2
//#define CONVERT_32(u8_data1,u8_data2,u8_data3,u8_data4) ((u8_data1) | (u8_data2 << 8) | (u8_data3 << 16) | (u8_data4 << 24))
//#define DATE_MAX_LEN 100

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/
#define CCX_SPI_iFID_TRACEMESSAGE 0xFFFF
#define DATE_MAX_LEN 100
#define CONVERT_32(u8_data1,u8_data2,u8_data3,u8_data4) ((u8_data1) | (u8_data2 << 8) | (u8_data3 << 16) | (u8_data4 << 24))
#define CONVERT_16(u8_data1,u8_data2) ((u8_data1) | (u8_data2 << 8))
#define CMD_AUDIO_ACT 0
static const char sczMLCertiPrefFile[]= "/var/opt/bosch/dynamic/spi/CertiPref.dat";
static const char sczCCCCertificate[]="CCC";
static const char sczCTSCertificate[]="CTS";

using namespace spi::io;


//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
** FUNCTION:  spi_tclTrace::spi_tclTrace(ahl_tclBaseOneThreadApp *poMapp)
******************************************************************************/
spi_tclTrace::spi_tclTrace(ahl_tclBaseOneThreadApp *poMapp)
   :m_poTraceStreamer(NULL)
{
   ETG_TRACE_USR4(("Constructor spi_tclMainApp() entered"));
   m_poTraceStreamer = new spi_tclImpTraceStreamable(poMapp);
   SPI_NORMAL_ASSERT(NULL == m_poTraceStreamer);
   // Setup the commands.
   if(NULL != m_poTraceStreamer)
   {
      ETG_TRACE_USR4(("vSetupCmds entered"));
      m_poTraceStreamer->vSetupCmds();
   }
   ETG_TRACE_USR4(("Constructor spi_tclMainApp() left"));

}  //!end of spi_tclMainApp()

/***************************************************************************
** FUNCTION:  spi_tclTrace::~spi_tclTrace()
***************************************************************************/
spi_tclTrace::~spi_tclTrace()
{
   RELEASE_MEM(m_poTraceStreamer);
   ETG_TRACE_USR1(("spi_tclTrace::~spi_tclTrace()"));
}


/******************************************************************************
** FUNCTION:  tVoid spi_tclTrace::vProcessTraceCmd(tU8 const* const cpu8B..
******************************************************************************/

tVoid spi_tclTrace::vProcessTraceCmd(tU8 const* const cpu8Buffer)
{
   //cpu8Buffer[0] - Gives you the length of the trace command
   tU8 u8MsgLen = cpu8Buffer[0];

   //ETG_TRACE_USR4(("Recieved a Trace command - %d, cpu8Buffer[1] -%d ",u8MsgLen,cpu8Buffer[1]));

   if((OSAL_NULL != cpu8Buffer) && ( 0 < u8MsgLen ))
   {
      //! Getting the FID from the 2nd and 3rd buffer for mapping
      tU16 u16Cmd    =  ((tU16)cpu8Buffer[2]|((tU16)cpu8Buffer[1]<<8));
      if(CCX_SPI_iFID_TRACEMESSAGE ==u16Cmd) //&& (OSAL_NULL != m_poTraceStreamer))
      {
         ETG_TRACE_USR2(("CCX_SPI_iFID_TRACEMESSAGE called= %d",u16Cmd));
         vHandleTraceCmd(cpu8Buffer);
      }
      else if(NULL != m_poTraceStreamer)
      {
         ETG_TRACE_USR4(("u16Cmd in the buffer %u",u16Cmd));
         m_poTraceStreamer->bStream(cpu8Buffer);

         //Commented for LINT warning Symbol bStat not subsequently referenced
         //tBool bStat=m_poTraceStreamer->bStream(cpu8Buffer);
         //ETG_TRACE_USR4(("Trace streaming status: %d",bStat));
      }
      else
      {
         ETG_TRACE_USR4(("m_poTraceStreamer is NULL"));
      }
   }
}//tVoid spi_tclTrace::vProcessTraceCmd()

/******************************************************************************
** FUNCTION:  tVoid spi_tclTrace::vHandleTraceCmd(tU8 const* const cpu8B..
******************************************************************************/
tVoid spi_tclTrace::vHandleTraceCmd(tU8 const* const cpu8Buffer)
{
	tU8 u8MsgLen = cpu8Buffer[0];
   ETG_TRACE_USR4(("spi_tclTrace::vHandleTraceCmd() entered - %d ",cpu8Buffer[3]));
    if(OSAL_NULL != cpu8Buffer)
    {
        switch(cpu8Buffer[3])
        {
        case SPI_SET_DATE_TIME:
           {
        	   if(7 <= u8MsgLen)
            {
                 tChar szDateTime[DATE_MAX_LEN] = {0};
                 //tU32 value recieved in 4bytes. arrange it and use
                 //Enter date in YYMMDDHHMM format
                 //For Ex: to set the date to 29-01-2014 12:12
                 //give the command - SPI_SET_DATE_TIME 1401291212
                 tU32 u32DateTime = CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                 sprintf(szDateTime, "date --set %d", u32DateTime);

                 ETG_TRACE_USR4(("szDateTime - %s", szDateTime));
                 //System call to set the date and time
                 system(szDateTime);
            }
           }
           break;
        case SPI_GET_VIDEO_SETTINGS:
           {
        	   if(3 <= u8MsgLen)
        	   {
        		   ETG_TRACE_FATAL(("vGetVideoSettingsData case"));
                 spi_tclVideoSettings *poVideoSettings  = spi_tclVideoSettings::getInstance();
                 if(NULL != poVideoSettings )
                 {
                	 ETG_TRACE_FATAL(("vGetVideoSettingsData entered"));
                    poVideoSettings ->vDisplayVideoSettings();
                 }
        	   }
           }
           break;
        case SPI_GET_AUDIO_SETTINGS:
           {
        	   if(3 <= u8MsgLen)
        	   {

        		   ETG_TRACE_FATAL(("vGetAudioSettingsData case"));
                 spi_tclAudioSettings *poAudioSettings  = spi_tclAudioSettings::getInstance();

                 if(NULL != poAudioSettings )
                 {
                	 ETG_TRACE_FATAL(("vGetaudioSettingsData entered"));
                    poAudioSettings ->vDisplayAudioSettings();
                 }
        	   }
           }
           break;
        case SPI_GET_APP_INFO:
           {
              spi_tclFactory* poFactory = spi_tclFactory::getInstance();
              if(
                 (8 <= u8MsgLen)
                 &&
                 (NULL != poFactory)
                 )
              {
                 spi_tclAppMngr* poAppMngr = poFactory->poGetAppManagerInstance();
                 if( NULL != poAppMngr)
                 {
                    t_U32 u32DevId = CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                    tenDeviceCategory enDevCat = static_cast<tenDeviceCategory>(cpu8Buffer[8]);
                    poAppMngr->vDisplayAllAppsInfo(u32DevId,enDevCat);
                 }//if( NULL != poAppMngr)
              }//if((5 <= u8MsgLen)
           }
           break;
		case SPI_DIPO_EXECUTE:
         {
            if(3 <= u8MsgLen)
            {
               // create trace class object
               spi_tclDiPOTraceCommands *poTraceCmd = new spi_tclDiPOTraceCommands();
               if(NULL != poTraceCmd)
               {
                  ETG_TRACE_USR4(("SPI_DIPO_EXECUTE called"));
                  // invoke the DiPO execution function
                  poTraceCmd->vDiPOExecCommands();
                  //LINT warning removal Custodial pointer poTraceCmd has not been freed or returned
                  delete poTraceCmd;
               }
            }
         }
         break;
     case SPI_DIPO_DEVICE_CONNECT:
        {
          if(3 <= u8MsgLen)
        	   {
           ETG_TRACE_USR4(("ecieved a Trace command 2"));
           vDipoTrace();
          }
        }
        break;
     case SPI_DIPO_DEVICE_SELECT_RESULT:
         {
            if (3 <= u8MsgLen)
            {
               ETG_TRACE_USR4(("Recieved a Trace command 3"));
               spi_tclFactory* poFactory = spi_tclFactory::getInstance();
               if (NULL != poFactory)
               {
                  ahl_tclBaseOneThreadApp* poMainApp = poFactory->poGetMainAppInstance();
                  if (NULL != poMainApp)
                  {
                     spi_tclMPlayClientHandler* handler = spi_tclMPlayClientHandler::getInstance(poMainApp);
                     SPI_NORMAL_ASSERT(NULL == handler);
                     if (NULL != handler)
                     {
                        handler->vDiPORoleSwitchResultTest();
                     } //if (NULL != poFactory)
                  } //if (NULL != poMainApp)
               } //if (NULL != poFactory)
            } //if (3 <= u8MsgLen)
         }
        break;
      case SPI_SET_VEHICLE_CONFIG:
         {
            spi_tclCmdInterface* poCmdIntf = spi_tclCmdInterface::getInstance();
            if( (5 <= u8MsgLen)&&(NULL != poCmdIntf) )
            {
               tenVehicleConfiguration enVehicleConfig = static_cast<tenVehicleConfiguration>(cpu8Buffer[4]);
               t_Bool bSetConfig = static_cast<t_Bool>(cpu8Buffer[5]);
               trUserContext rUsrCntxt;
               poCmdIntf->vSetVehicleConfig(enVehicleConfig,bSetConfig,rUsrCntxt);
            }//if((5 <= u8MsgLen)
         }
         break;
      case SPI_GET_APP_CERTIFICATION_INFO:
         {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((12 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclAppMngr* poAppMngr = poFactory->poGetAppManagerInstance();
               if( NULL != poAppMngr)
               {
                  t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                  t_U32 u32AppId =  CONVERT_32(cpu8Buffer[8], cpu8Buffer[9], cpu8Buffer[10], cpu8Buffer[11]);
                  tenDeviceCategory enDevCat = static_cast<tenDeviceCategory>(cpu8Buffer[12]);
                  poAppMngr->vDisplayAppcertificationInfo(u32DevId,u32AppId,enDevCat);
               }//if( NULL != poAppMngr)
            }//if((12 <= u8MsgLen)&&(NULL != poFactory))
         }
         break;
      case SPI_GET_APP_SETTINGS:
         {
            spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
            if((3 <= u8MsgLen)&&(NULL != poAppSettings))
            {
               poAppSettings->vDisplayAppSettings();
            }//if((3 <= u8MsgLen)&&(NULL != poAppSettings))
         }
         break;
      case SPI_SET_VIDEO_BLOCKING:
         {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((9 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclVideo* poVideo = poFactory->poGetVideoInstance();
               if(NULL != poVideo)
               {
                  trUserContext rUsrCntxt;
                  t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                  tenBlockingMode enBlcokingMode = static_cast<tenBlockingMode>(cpu8Buffer[8]);
                  tenDeviceCategory enDevCat = static_cast<tenDeviceCategory>(cpu8Buffer[9]);
                  poVideo->vSetVideoBlockingMode(u32DevId,enBlcokingMode,enDevCat,rUsrCntxt);
               }//if(NULL != poVideo)
            }//if((9 <= u8MsgLen)&&(NULL != poFactory))
         }
         break;
	  case SPI_AUDIO_ACT_DEACT:
         {
            ETG_TRACE_USR1(("SPI_AUDIO_ACT_DEACT called above if condition"));
            if(6 <= u8MsgLen)
            {
				spi_tclFactory* poFactory = spi_tclFactory::getInstance();
				if(NULL != poFactory)
				{
			    	/*lint -esym(40,nullptr)nullptr Undeclared identifier */
                   spi_tclAudio *poAudio = poFactory->poGetAudioInstance();
			       tenDeviceCategory enDeviceCat = static_cast<tenDeviceCategory>(cpu8Buffer[5]);
			       tenAudioDir enAudio = static_cast<tenAudioDir>(cpu8Buffer[6]);
                   if(NULL != poAudio)
                   {
			          (cpu8Buffer[4] == CMD_AUDIO_ACT) ?
                        poAudio->vLaunchAudio(2,enDeviceCat,enAudio) : poAudio->vTerminateAudio(2,enAudio);
                   }
              }               
           }
         }
         break;
     case SPI_SET_AUDIO_BLOCKING:
        {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((9 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclAudio* poAudio = poFactory->poGetAudioInstance();
               if(NULL != poAudio)
               {
                  t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                  tenBlockingMode enBlcokingMode = static_cast<tenBlockingMode>(cpu8Buffer[8]);
                  tenDeviceCategory enDevCat = static_cast<tenDeviceCategory>(cpu8Buffer[9]);
                  poAudio->bSetAudioBlockingMode(u32DevId,enBlcokingMode,enDevCat);
               }//if(NULL != poVideo)
            }//if((9 <= u8MsgLen)&&(NULL != poFactory))
        }
        break;

     case SPI_SET_AUDIO_SOURCE_AVAILABILITY:
        {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((5 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclAudio* poAudio = poFactory->poGetAudioInstance();
               if(NULL != poAudio)
               {
                  tenAudioDir enAudioDir = static_cast<tenAudioDir> (cpu8Buffer[4]);
                  t_Bool bAvailable = static_cast<t_Bool> (cpu8Buffer[5]);
                  poAudio->bSetAudioSrcAvailability(enAudioDir, bAvailable);
               }//if(NULL != poVideo)
            }//if((9 <= u8MsgLen)&&(NULL != poFactory))
        }
        break;

     case SPI_LAUNCH_AUDIO:
        {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((9 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclAudio* poAudio = poFactory->poGetAudioInstance();
               if(NULL != poAudio)
               {
                  t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                  tenDeviceCategory enDevCat = static_cast<tenDeviceCategory>(cpu8Buffer[8]);
                  tenAudioDir enAudioDir = static_cast<tenAudioDir>(cpu8Buffer[9]);
                  poAudio->vLaunchAudio(u32DevId, enDevCat, enAudioDir);
               }//if(NULL != poVideo)
            }//if((9 <= u8MsgLen)&&(NULL != poFactory))
        }
        break;
	 case SPI_SET_CLIENT_PROFILE:
        {

           spi_tclFactory* poFactory = spi_tclFactory::getInstance();
           if((11 <= u8MsgLen)&&(NULL != poFactory))
           {
              spi_tclAppMngr* poAppMngr = poFactory->poGetAppManagerInstance();
              if( NULL != poAppMngr)
              {
                 t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);

                 trClientProfile rClntProfile;

                 rClntProfile.szClientProfileId=cszClientProfileId.c_str();
                 rClntProfile.szRtpPayLoadTypes = "98,99";
                 rClntProfile.u32ProfileMajorVersion = cou32MLClientMajorversion ;
                 rClntProfile.u32ProfileMinorVersion = cou32MLClientMinorVersion ;

                 rClntProfile.rIconPreferences.u32IconWidth = cpu8Buffer[8];
                 rClntProfile.rIconPreferences.u32IconHeight = cpu8Buffer[9] ;
                 rClntProfile.rIconPreferences.u32IconDepth = cpu8Buffer[10] ;
                 rClntProfile.rIconPreferences.enIconMimeType = static_cast<tenIconMimeType>(cpu8Buffer[11]) ;

                 if(false == poAppMngr->bSetClientProfile(u32DevId,rClntProfile,e8DEV_TYPE_MIRRORLINK))
                 {
                    ETG_TRACE_ERR(("Error in Setting Client Profile to the server"));
                 }//if(false == poAppMngr->bSetClientProfile(u32DevId,rClntProfile,e8DEV_TYPE_MIRROR
              }//if( NULL != poAppMngr)
           }//if((11 <= u8MsgLen)&&(NULL != poFactory))
        }
        break;
     case SPI_START_GPS_DATA_TRANSFER:
        {
           spi_tclFactory* poFactory = spi_tclFactory::getInstance();
           if ((7 <= u8MsgLen) && (NULL != poFactory))
           {
              spi_tclDataService* poDataService = poFactory->poGetDataServiceInstance();
              if (NULL != poDataService)
              {
                 t_U32 u32DataRateInMs =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], 0, 0);
                 poDataService->vSimulateGPSDataTransfer(u32DataRateInMs);
              }//if(NULL != poDataService)
           }//if((7 <= u8MsgLen)&&(NULL != poFactory))
        }
        break;
     case SPI_DIPO_AUDIO_CONTEXT_UPDATE:
        {
            ETG_TRACE_USR1(("Within SPI_DIPO_AUDIO_CONTEXT_UPDATE "));
            tenDeviceCategory enDevCat = e8DEV_TYPE_DIPO; // Always DiPO
            trUserContext rUserContext; // Dummy
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((9 <= u8MsgLen) && (NULL != poFactory))
            {
               spi_tclResourceMngr *poResMngr = poFactory->poGetRsrcMngrInstance();
               if(NULL != poResMngr)
               {
                  t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                  t_U8 u8AudioCntxt = cpu8Buffer[8]; 
                  t_U8 u8ReqFlag = cpu8Buffer[9];

                  ETG_TRACE_USR1(("Device ID : %d", u32DevId));
                  ETG_TRACE_USR1(("Audio Context : %d", u8AudioCntxt));
                  ETG_TRACE_USR1(("Request Flag : %d", u8ReqFlag));
                  poResMngr->vSetAccessoryAudioContext(u32DevId, (tenAudioContext)u8AudioCntxt, (t_Bool)u8ReqFlag, enDevCat, rUserContext,e8DEV_TYPE_DIPO);
               }//if(NULL != poResMngr)
            }//if((9 <= u8MsgLen) && (NULL != poFactory))
        }
        break;
      case SPI_SET_NOTIFICATIONS:
         {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((4 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclAppMngr* poAppMngr = poFactory->poGetAppManagerInstance();
               if( NULL != poAppMngr)
               {
                  t_Bool bSetNotificationsOn = (t_Bool)(cpu8Buffer[4]);
                  poAppMngr->vSetMLNotificationOnOff(bSetNotificationsOn);
               }//if( NULL != poAppMngr)
            }//if((4 <= u8MsgLen)&&(NULL != poFactory))
         }
         break;
      case SPI_SET_AUDIO_DUCKING:
         {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((7 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclAudio* poAudio = poFactory->poGetAudioInstance();
               if(NULL != poAudio)
               {
                  t_U16 u16DuckingDuration = CONVERT_16(cpu8Buffer[4], cpu8Buffer[5]);
                  t_U8 u8VolumeLevelindB = cpu8Buffer[6];
                  tenDuckingType enDuckingType = static_cast<tenDuckingType> (cpu8Buffer[7]);
                  poAudio->bSetAudioDucking(u16DuckingDuration,u8VolumeLevelindB,enDuckingType);
               }//if(NULL != poAudio)
            }//if((9 <= u8MsgLen)&&(NULL != poFactory))
         }
         break;
         case SPI_GET_APPLIST_XML:
         {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((7 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclAppMngr* poAppMngr = poFactory->poGetAppManagerInstance();
               if( NULL != poAppMngr)
               {
                  t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                  poAppMngr->vDisplayAppListXml(u32DevId,e8DEV_TYPE_MIRRORLINK);
               }//if( NULL != poAppMngr)
            }//if((7 <= u8MsgLen)&&(NULL != poFactory))
         }
         break;
	     case SPI_DIPO_SET_APP_STATE:
         {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            spi_tclResourceMngr *poResMngr = poFactory->poGetRsrcMngrInstance();
            tenDeviceCategory enDevCat = e8DEV_TYPE_DIPO;
            trUserContext rUserContext;
            if((10 <= u8MsgLen) && (NULL != poFactory) &&( NULL != poResMngr))
            {
               t_U32 u32DevId = CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
               tenSpeechAppState enSpeechState = static_cast<tenSpeechAppState>(cpu8Buffer[8]);
               tenPhoneAppState enPhoneAppState = static_cast<tenPhoneAppState>(cpu8Buffer[9]);
               tenNavAppState enNavAppState = static_cast<tenNavAppState>(cpu8Buffer[10]);

               ETG_TRACE_USR1(("Device ID : %d", u32DevId));
               ETG_TRACE_USR1(("Speech State : %d", enSpeechState));
               ETG_TRACE_USR1(("Phone State  : %d", enPhoneAppState));
               ETG_TRACE_USR1(("Nav State    : %d", enNavAppState));
               poResMngr->vSetAccessoryAppState(enDevCat, enSpeechState, enPhoneAppState, enNavAppState, rUserContext);


            } // if((9 <= u8MsgLen) && (NULL != poFactory)) 
         }
         break;
        case SPI_SEND_TELEPHONEKEY_EVENT:
           {
              spi_tclCmdInterface* poCmdIntf = spi_tclCmdInterface::getInstance();
              if( (12 <= u8MsgLen)&&(NULL != poCmdIntf) )
              {
                 t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                 tenKeyMode enKeyMode = static_cast<tenKeyMode>(cpu8Buffer[8]);
                 tenKeyCode enKeyCode = static_cast<tenKeyCode> (CONVERT_32(cpu8Buffer[9], cpu8Buffer[10], cpu8Buffer[11], cpu8Buffer[12]));
                 trUserContext rUsrCntxt;
                 poCmdIntf->vSendKeyEvent(u32DevId,enKeyMode,enKeyCode,rUsrCntxt);
              }//if((12 <= u8MsgLen)
           }
           break;
	   case SPI_SET_PIXEL_FORMAT:
            {
               spi_tclFactory* poFactory = spi_tclFactory::getInstance();
               if((9 <= u8MsgLen)&&(NULL != poFactory))
               {
                  spi_tclVideo* poVideo = poFactory->poGetVideoInstance();
                  if(NULL != poVideo)
                  {
                     t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                     tenDeviceCategory enDevCat = static_cast<tenDeviceCategory>(cpu8Buffer[8]);
                     tenPixelFormat enPixelFormat = static_cast<tenPixelFormat>(cpu8Buffer[9]);
                     poVideo->vSetPixelFormat(u32DevId,enDevCat,enPixelFormat);
                  }//if(NULL != poVideo)
               }//if((9 <= u8MsgLen)&&(NULL != poFactory))
            }
            break;
      case SPI_SET_PIXEL_RESOLUTION:
         {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((5 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclVideo* poVideo = poFactory->poGetVideoInstance();
               if(NULL != poVideo)
               {
                  tenPixelResolution enPixResolution = static_cast<tenPixelResolution>(cpu8Buffer[4]);
                  tenDeviceCategory enDevCat = static_cast<tenDeviceCategory>(cpu8Buffer[5]);
                  poVideo->vSetPixelResolution(enPixResolution,enDevCat);
               }//if(NULL != poVideo)
            }//if((5 <= u8MsgLen)&&(NULL != poFactory))
         }
         break;
      case SPI_SET_LAYER_STATUS:
         {
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((4 <= u8MsgLen)&&(NULL != poFactory))
            {
               spi_tclVideo* poVideo = poFactory->poGetVideoInstance();
               if(NULL != poVideo)
               {
                  poVideo->vStartVideoRendering((t_Bool)cpu8Buffer[4]);
               }//if(NULL != poVideo)
            }//if((4 <= u8MsgLen)&&(NULL != poFactory))
         }
         break;
	  case SPI_VERSION_INFO:
        {
           if(3 <= u8MsgLen)
           {
             ETG_TRACE_USR4(("Display Version Info "));
             vDisplaySpiVersionInfo();
           }
        }
        break;

     case SPI_CHANGE_MIRRORLINK_CERTIFICATE:
        {
           if(4 <= u8MsgLen)
           {
              tenMLCertiType enMLCertiType = static_cast<tenMLCertiType>(cpu8Buffer[4]);
              vSetMLCertificateType(enMLCertiType);
           }
        }
        break;

     case SPI_GET_MIRRORLINK_CERTIFICATE_TYPE:
          {
             if(3 <= u8MsgLen)
             {
               vDisplayMLCertiType();
             }
          }
          break;

     case SPI_FEATURE_SUPPORT:
          {
             spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
             if ((3 <= u8MsgLen) && (NULL != poConfigReader))
             {
               trSpiFeatureSupport rSpiFeatureSupport;
               poConfigReader->vGetSpiFeatureSupport(rSpiFeatureSupport);
               //@Note: List is not printed here since vGetSpiFeatureSupport() fn. prints it.
             }//if(3 <= u8MsgLen)
          }
          break;

     case SPI_INVOKE_ML_NOTIFICATION_ACTION:
        {
           spi_tclCmdInterface* poCmdInterface = spi_tclCmdInterface::getInstance();
           if ((19 <= u8MsgLen)&&(NULL != poCmdInterface))
           {
              t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
              t_U32 u32AppId =  CONVERT_32(cpu8Buffer[8], cpu8Buffer[9], cpu8Buffer[10], cpu8Buffer[11]);
              t_U32 u32NotiID = CONVERT_32(cpu8Buffer[12], cpu8Buffer[13], cpu8Buffer[14], cpu8Buffer[15]);
              t_U32 u32NotiActionID = CONVERT_32(cpu8Buffer[16], cpu8Buffer[17], cpu8Buffer[18], cpu8Buffer[19]);
              poCmdInterface->vInvokeNotificationAction(u32DevId,u32AppId,u32NotiID,u32NotiActionID,corEmptyUsrContext);
           }//if ((19 <= u8MsgLen)&&(NULL != poCmdInterface))
        }
        break;
     case SPI_SET_CONTENT_ATTESTATION:
        {
           spi_tclFactory* poFactory = spi_tclFactory::getInstance();
           if((4 <= u8MsgLen)&&(NULL != poFactory))
           {
              spi_tclVideo* poVideo = poFactory->poGetVideoInstance();
              if(NULL != poVideo)
              {
                 poVideo->vSetContAttestFlag(cpu8Buffer[4]);
              }//if(NULL != poVideo)
           }//if((4 <= u8MsgLen)&&(NULL != poFactory))
        }
        break;
     case SPI_ENABLE_TEST_APPS:
        {
           spi_tclAppSettings* poAppSettings = spi_tclAppSettings::getInstance();
           if((4 <= u8MsgLen)&&(NULL != poAppSettings))
           {
              poAppSettings->vEnableTestApps((t_Bool)cpu8Buffer[4]);
           }//if((4 <= u8MsgLen)&&(NULL != poAppSettings))
        }
        break;
     case SPI_SET_ACC_DISP_CNTXT:
        {
           spi_tclCmdInterface* poCmdInterface = spi_tclCmdInterface::getInstance();
           if ((9 <= u8MsgLen)&&(NULL != poCmdInterface))
           {
              t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
              t_Bool bDisplayFlag =  (t_Bool)(cpu8Buffer[8]);
              tenDisplayContext enDisplayContext = static_cast<tenDisplayContext>(cpu8Buffer[9]);
              poCmdInterface->vSetAccessoryDisplayContext(u32DevId,bDisplayFlag,enDisplayContext,corEmptyUsrContext);
           }//if ((9 <= u8MsgLen)&&(NULL != poCmdInterface))
        }
        break;
     case SPI_TRIGGER_VIDEOFOCUSCB:
        {
           spi_tclFactory* poFactory = spi_tclFactory::getInstance();
           if((5 <= u8MsgLen)&&(NULL != poFactory))
           {
              spi_tclVideo* poVideo = poFactory->poGetVideoInstance();
              if(NULL != poVideo)
              {
                 poVideo->vTriggerVideoFocusCb(cpu8Buffer[4],cpu8Buffer[5],e8DEV_TYPE_ANDROIDAUTO);
              }//if(NULL != poVideo)
           }//if((5 <= u8MsgLen)&&(NULL != poFactory))
         }
        break;

     case SPI_SET_VEHICLE_SPEED:
        {
           spi_tclCmdInterface* poCmdIntf = spi_tclCmdInterface::getInstance();
           if( (5 <= u8MsgLen)&&(NULL != poCmdIntf) )
           {
              t_S16 s16Speed = static_cast<t_S16>(cpu8Buffer[4]);
              t_Bool bSolicited = static_cast<t_Bool>(cpu8Buffer[5]);
              trVehicleData rVehicleData;
              rVehicleData.s16Speed = s16Speed;
              poCmdIntf->vOnVehicleData(rVehicleData,bSolicited);
           }//if((5 <= u8MsgLen)
        }
        break;

    case SPI_SET_FEATURE_RESTRICTION:
       {
          spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();
          spi_tclCmdInterface* poCmdIntf = spi_tclCmdInterface::getInstance();

          if( (4 <= u8MsgLen)&&(NULL != poCmdIntf)&&(NULL != poConfigReader))
          {
             t_U8 u8DriveRestriction = static_cast<t_U8>(cpu8Buffer[4]);
             ETG_TRACE_USR1(("SPI_SET_FEATURE_RESTRICTION : u8DriveRestriction  : %d", u8DriveRestriction));

            //#ifndef VARIANT_S_FTR_ENABLE_GM
            //poConfigReader->vSetFeatureRestrictions(e8DEV_TYPE_ANDROIDAUTO, 0, u8DriveRestriction);
            //#endif

             poCmdIntf->vSetFeatureRestrictions(e8DEV_TYPE_ANDROIDAUTO, 0, u8DriveRestriction);
          }//if((4 <= u8MsgLen)
       }
       break;

    case SPI_SET_VEHICLE_MOVEMENT_STATE:
       {
          spi_tclCmdInterface* poCmdIntf = spi_tclCmdInterface::getInstance();
          if( (4 <= u8MsgLen)&&(NULL != poCmdIntf) )
          {
             trVehicleData rVehicledata;
             rVehicledata.enVehMovState = static_cast<tenVehicleMovementState>(cpu8Buffer[4]);
             ETG_TRACE_USR1(("SPI_SET_VEHICLE_MOVEMENT_STATE : VEHICLE MOVEMENT STATE  : %d",
                   ETG_ENUM(VEH_MOV_STATE, rVehicledata.enVehMovState)));
             poCmdIntf->vOnVehicleData(rVehicledata,false);
          }//if((4 <= u8MsgLen)
       }
       break;

	 case SPI_SET_ACCESSORY_AUDIO_CONTEXT_ONE_SOURCE_ACTIVE :
        {
            ETG_TRACE_USR1(("Within SPI_SET_ACCESSORY_AUDIO_CONTEXT_ONE_SOURCE_ACTIVE "));
            trUserContext rUserContext; // Dummy
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if((9 <= u8MsgLen) && (NULL != poFactory))
            {
               spi_tclResourceMngr *poResMngr = poFactory->poGetRsrcMngrInstance();
               if(NULL != poResMngr)
               {
                  t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                  tenDeviceCategory enDevCat =  (tenDeviceCategory) (CONVERT_32(cpu8Buffer[8], cpu8Buffer[9], cpu8Buffer[10], cpu8Buffer[11]));
                  t_U8 u8AudioCntxt = cpu8Buffer[12];
                  t_U8 u8ReqFlag = cpu8Buffer[13];

                  ETG_TRACE_USR1(("Device ID : %d", u32DevId));
                  ETG_TRACE_USR1(("Audio Context : %d", u8AudioCntxt));
                  ETG_TRACE_USR1(("Request Flag : %d", u8ReqFlag));

                  if (enDevCat==e8DEV_TYPE_DIPO)
                  {
                	  poResMngr->vSetAccessoryAudioContext(u32DevId, (tenAudioContext)u8AudioCntxt, (t_Bool)u8ReqFlag, enDevCat, rUserContext,e8DEV_TYPE_DIPO);
                  }
                  else if (enDevCat==e8DEV_TYPE_ANDROIDAUTO)
                  {
                	  poResMngr->vSetAccessoryAudioContext(u32DevId, (tenAudioContext)u8AudioCntxt, (t_Bool)u8ReqFlag, enDevCat, rUserContext,e8DEV_TYPE_ANDROIDAUTO);

                  }
               }//if(NULL != poResMngr)
            }//if((9 <= u8MsgLen) && (NULL != poFactory))
        }
        break;
	  case SPI_SET_ACCESSORY_AUDIO_CONTEXT_TWO_SOURCE_ACTIVE :
        {
            ETG_TRACE_USR1(("SPI_SET_ACCESSORY_AUDIO_CONTEXT_TWO_SOURCE_ACTIVE "));
            trUserContext rUserContext; // Dummy
            spi_tclFactory* poFactory = spi_tclFactory::getInstance();
            if(( 9 <= u8MsgLen) && (NULL != poFactory))
            {
               spi_tclResourceMngr *poResMngr = poFactory->poGetRsrcMngrInstance();
               if(NULL != poResMngr)
               {
                  t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
                  tenDeviceCategory enDevCat = (tenDeviceCategory) (CONVERT_32(cpu8Buffer[8], cpu8Buffer[9], cpu8Buffer[10], cpu8Buffer[11]));
                  t_U8 u8AudioCntxt = cpu8Buffer[12];
                  t_U8 u8ReqFlag = cpu8Buffer[13];
                  t_U8 u8AudioCntxtSecond = cpu8Buffer[14];
                  t_U8 u8ReqFlagSecond = cpu8Buffer[15];


                  ETG_TRACE_USR1(("Device ID : %d", u32DevId));
                  ETG_TRACE_USR1(("Audio Context : %d", u8AudioCntxt));
                  ETG_TRACE_USR1(("Request Flag : %d", u8ReqFlag));
                  ETG_TRACE_USR1(("Audio Context Second  : %d", u8AudioCntxtSecond));
                  ETG_TRACE_USR1(("Request Flag Second: %d", u8ReqFlagSecond));

                  if (enDevCat==e8DEV_TYPE_DIPO)
                  {
                	  poResMngr->vSetAccessoryAudioContext(u32DevId, (tenAudioContext)u8AudioCntxt, (t_Bool)u8ReqFlag, enDevCat, rUserContext,e8DEV_TYPE_DIPO);
                	  poResMngr->vSetAccessoryAudioContext(u32DevId, (tenAudioContext)u8AudioCntxtSecond, (t_Bool)u8ReqFlagSecond, enDevCat, rUserContext,e8DEV_TYPE_DIPO);

                  }
                  else if (enDevCat==e8DEV_TYPE_ANDROIDAUTO)
                  {
                	  poResMngr->vSetAccessoryAudioContext(u32DevId, (tenAudioContext)u8AudioCntxt, (t_Bool)u8ReqFlag, enDevCat, rUserContext,e8DEV_TYPE_ANDROIDAUTO);
                	  poResMngr->vSetAccessoryAudioContext(u32DevId, (tenAudioContext)u8AudioCntxtSecond, (t_Bool)u8ReqFlagSecond, enDevCat, rUserContext,e8DEV_TYPE_ANDROIDAUTO);

                  }
               }//if(NULL != poResMngr)
            }//if((9 <= u8MsgLen) && (NULL != poFactory))
        }
        break;

     case SPI_SET_USER_AUTHENTICATION :
        {
            ETG_TRACE_USR1(("SPI_SET_USER_AUTHENTICATION "));
            trUserContext rUserContext; // Dummy
            spi_tclCmdInterface* poCmdInterface = spi_tclCmdInterface::getInstance();
            if((8 <= u8MsgLen) && (NULL != poCmdInterface))
            {
               t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
               poCmdInterface->vSetDeviceAuthorization(u32DevId, static_cast<tenUserAuthorizationStatus> (cpu8Buffer[8]));
            }//if((9 <= u8MsgLen) && (NULL != poFactory))
        }
        break;

     case SPI_SELECT_DEVICE_EXT:
        {
            ETG_TRACE_USR1(("SPI_SELECT_DEVICE_EXT "));
            trUserContext rUserContext; // Dummy
            spi_tclCmdInterface* poCmdInterface = spi_tclCmdInterface::getInstance();
            if((10 <= u8MsgLen) && (NULL != poCmdInterface))
            {
               t_U32 u32DevId =  CONVERT_32(cpu8Buffer[4], cpu8Buffer[5], cpu8Buffer[6], cpu8Buffer[7]);
               poCmdInterface->vSelectDevice(u32DevId, static_cast<tenDeviceConnectionType> (cpu8Buffer[8]), static_cast<tenDeviceConnectionReq> (cpu8Buffer[9]),
                        e8USAGE_UNKNOWN, e8USAGE_UNKNOWN, static_cast<tenDeviceCategory> (cpu8Buffer[10]), rUserContext);
            }//if((9 <= u8MsgLen) && (NULL != poFactory))
        }
        break;

     default:
        {
           ETG_TRACE_ERR(("Trace Streamer unavailable."));
        }
        }
     }
  }//tVoid spi_tclTrace::vHandleTraceCmd()

/******************************************************************************
** FUNCTION:  tVoid spi_tclTrace::vDipoTrace()
******************************************************************************/
t_Void spi_tclTrace::vDipoTrace()
{
   ETG_TRACE_USR1(("vDipoTrace entered "));
   spi_tclFactory* poFactory = spi_tclFactory::getInstance();
   if (NULL != poFactory)
   {
      ahl_tclBaseOneThreadApp* poMainApp = poFactory->poGetMainAppInstance();
      if (NULL != poMainApp)
      {
         spi_tclMPlayClientHandler* handler = spi_tclMPlayClientHandler::getInstance(poMainApp);
         SPI_NORMAL_ASSERT(NULL == handler);
         if (NULL != handler)
         {
            ETG_TRACE_USR1(("vDipoTrace:: NOT NULL "));
            handler->vTestMediaPlayer();
         }
         else
         {
            ETG_TRACE_USR1(("vDipoTrace:: NULL "));
         }
      } //if (NULL != poMainApp)
   } //if (NULL != poFactory)
}

/******************************************************************************
** FUNCTION:  tVoid spi_tclTrace::vDisplaySpiVersionInfo()
******************************************************************************/
t_Void spi_tclTrace::vDisplaySpiVersionInfo()
{
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS,"SPI Release Label - RBEI_MAIN_TAS_GEN3_SPI_113"));
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS,"SDK version - RealVncSdk 2.9"));
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Mirrorlink Label - SPI_ML_113"));
}


/********************************************************************************
** FUNCTION   : tVoid vSetMLCertificateType()
/*******************************************************************************/
t_Void spi_tclTrace::vSetMLCertificateType(tenMLCertiType enMLCerti)
{
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Mirrorlink certificate set to %d \n", ETG_ENUM(ML_CERTIFICATE_TYPE, enMLCerti)));

   FileHandler oFileHandler(sczMLCertiPrefFile,
            SPI_EN_RDWR);

   //! Write the preferred Mirrorlink certificate type to a file
   if(e8_CCCCERTIFICATE == enMLCerti)
   {
      oFileHandler.bFWrite((t_SString) sczCCCCertificate, strlen(sczCCCCertificate));
   }
   else if(e8_CTSCERTIFICATE == enMLCerti)
   {
      oFileHandler.bFWrite((t_SString) sczCTSCertificate, strlen(sczCTSCertificate));
   }
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Please reboot the system for changes to take effect \n"));
}

/********************************************************************************
** FUNCTION   : tVoid vDisplayMLCertiType()
/*******************************************************************************/
t_Void spi_tclTrace::vDisplayMLCertiType()
{
   spi::io::FileHandler oFileHandler(
            sczMLCertiPrefFile, spi::io::SPI_EN_RDONLY);
   t_S32 s32FileSize = oFileHandler.s32GetSize();

   //! Read the file and check the mirrorlink certificate preference type
   tenMLCertiType enMLCerti = e8_CCCCERTIFICATE;

   t_Char* pczCertiPref = NULL;
   if (s32FileSize > 0)
   {
      pczCertiPref = new t_Char[s32FileSize + 1];
      if (NULL != pczCertiPref)
      {
         memset((t_Void*) pczCertiPref, '\0', s32FileSize + 1);
         oFileHandler.bFRead(pczCertiPref, s32FileSize);
      }
      //! Check if the CTS certificate is preferred. if not CCC certificate is loaded
      if ((NULL != pczCertiPref) && (0 == strncmp(pczCertiPref, sczCTSCertificate,
               sizeof(sczCTSCertificate))))
      {
         ETG_TRACE_USR2(("vDisplayMLCertiType:: pczCertiPref = %s\n", pczCertiPref));
         enMLCerti = e8_CTSCERTIFICATE;
      }

      RELEASE_ARRAY_MEM(pczCertiPref);
   }
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_CTS,"Mirrorlink certificate used is %d \n",
            ETG_ENUM(ML_CERTIFICATE_TYPE, enMLCerti)));
}
//lint –restore
