/*!
 *******************************************************************************
 * \file              spi_tclRespInterface.cpp
 * \brief             SPI Response interface for the Project specific layer
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   provides SPI response interface for the Project specific layer
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 11.22.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 30.11.2013 |  Ramya Murthy                | Removed methods definitions:
                                             vPostGetDeviceInfoListResult(),
                                             vPostGetAppListResult()
 10.12.2013 |  Ramya Murthy                | Included ProjectedDisplayContext,
                                             DeviceUsagePreference, AppMetadata
                                             & MLNotificationEnabledInfo related
                                             method definitions.
 06.02.2014 |  Ramya Murthy                | Adapted to SPI HMI API document v1.5 changes
 26.04.2014 |  Shihabudheen P M            | Added vPostDeviceAudioContext
 03.07.2014 |  Hari Priya E R              | Added changes related to Input Response Interface
 25.10.2014 |  Shihabudheen P M            | added vUpdateSessionStatusInfo
 28.10.2013 |  Hari Priya E R              | Added changes related to Data Service Response Interface
 14.12.2015 |  Rachana L Achar			   | Modified the prototype of vLaunchAppResult method

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "spi_tclRespInterface.h"
/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::spi_tclRespInterface();
 ***************************************************************************/
spi_tclRespInterface::spi_tclRespInterface()
{

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::~spi_tclRespInterface()
 ***************************************************************************/
spi_tclRespInterface::~spi_tclRespInterface()
{

}

/***************************************************************************
** FUNCTION: t_Void spi_tclRespInterface::vPostAppStatusInfo
***************************************************************************/
t_Void spi_tclRespInterface::vPostAppStatusInfo(t_U32 u32DeviceHandle,
   tenDeviceConnectionType enDevConnType,
   tenAppStatusInfo enAppStatus)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(enDevConnType);
   SPI_INTENTIONALLY_UNUSED(enAppStatus);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclRespInterface::vLaunchAppResult
**             (t_U32 u32DeviceHandle, t_U32 u32AppHandle,...)
***************************************************************************/
t_Void spi_tclRespInterface::vLaunchAppResult(t_U32 u32DeviceHandle,
   t_U32 u32AppHandle,
   tenDiPOAppType enDiPOAppType,
   tenResponseCode enResponseCode,
   tenErrorCode enErrorCode,
   const trUserContext& corfrUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(u32AppHandle);
   SPI_INTENTIONALLY_UNUSED(enDiPOAppType);
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(corfrUsrCntxt);

}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::vTerminateAppResult
 **             (t_U32 u32DeviceHandle, t_U32 u32AppHandle ..)
 ***************************************************************************/
t_Void spi_tclRespInterface::vTerminateAppResult(t_U32 u32DeviceHandle, 
   t_U32 u32AppHandle, 
   tenResponseCode enResponseCode, 
   tenErrorCode enErrorCode,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(u32AppHandle);
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclRespInterface::vPostAppIconDataResult
**                   (tenIconMimeType enIconMimeType, t_Char* pczAppIconData, ..)
***************************************************************************/
t_Void spi_tclRespInterface::vPostAppIconDataResult(tenIconMimeType enIconMimeType,
   const t_U8* pcu8AppIconData, 
   t_U32 u32Len,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enIconMimeType);
   SPI_INTENTIONALLY_UNUSED(pcu8AppIconData);
   SPI_INTENTIONALLY_UNUSED(u32Len);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::vPostSetAppIconAttributesResult
 **             (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
 ***************************************************************************/
t_Void spi_tclRespInterface::vPostSetAppIconAttributesResult(
   tenResponseCode enResponseCode, tenErrorCode enErrorCode,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::vPostSetMLNotificationEnabledInfoResult
 **                   (t_U32 u32DeviceHandle, tenResponseCode enResponseCode,..)
 ***************************************************************************/
t_Void spi_tclRespInterface::vPostSetMLNotificationEnabledInfoResult(
   t_U32 u32DeviceHandle,
   tenResponseCode enResponseCode, 
   tenErrorCode enErrorCode,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclRespInterface::vPostNotificationInfo
**                  (t_U32 u32DeviceHandle,t_U32 u32AppHandle,..)
***************************************************************************/
t_Void spi_tclRespInterface::vPostNotificationInfo(t_U32 u32DeviceHandle,
   t_U32 u32AppHandle, 
   const trNotiData& corfrNotificationData)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(u32AppHandle);
   SPI_INTENTIONALLY_UNUSED(corfrNotificationData);
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::vPostInvokeNotificationActionResult(
 **               tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
 ***************************************************************************/
t_Void spi_tclRespInterface::vPostInvokeNotificationActionResult(
   tenResponseCode enResponseCode, 
   tenErrorCode enErrorCode,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::vPostSetOrientationModeResult
 **              (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
 ***************************************************************************/
t_Void spi_tclRespInterface::vPostSetOrientationModeResult(
   tenResponseCode enResponseCode, 
   tenErrorCode enErrorCode,
   const trUserContext& corfrcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(corfrcUsrCntxt);
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::vPostSetVideoBlockingModeResult
 **               (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
 ***************************************************************************/
t_Void spi_tclRespInterface::vPostSetVideoBlockingModeResult(
   tenResponseCode enResponseCode, 
   tenErrorCode enErrorCode,
   const trUserContext& corfrcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(corfrcUsrCntxt);
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclRespInterface::vPostSetAudioBlockingModeResult
 **            (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
 ***************************************************************************/
t_Void spi_tclRespInterface::vPostSetAudioBlockingModeResult(
   tenResponseCode enResponseCode, 
   tenErrorCode enErrorCode,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclRespInterface::vPostSendTouchEvent
**                (tenResponseCode enResponseCode,tenErrorCode enErrorCode,..)
***************************************************************************/
t_Void spi_tclRespInterface::vPostSendTouchEvent(
   tenResponseCode enResponseCode,
   tenErrorCode enErrorCode,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclRespInterface::vPostSendKeyEvent
**                (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
***************************************************************************/
t_Void spi_tclRespInterface::vPostSendKeyEvent(
   tenResponseCode enResponseCode,
   tenErrorCode enErrorCode,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclRespInterface::vPostUpdateCertificateFileResult
**              (tenResponseCode enResponseCode, tenErrorCode enErrorCode,..)
***************************************************************************/
t_Void spi_tclRespInterface::vPostUpdateCertificateFileResult(
   tenResponseCode enResponseCode,
   tenErrorCode enErrorCode,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enResponseCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclRespInterface::vPostDeviceDisplayContext
**                   (t_U32 u32DeviceHandle, t_Bool bDisplayFlag,..)
***************************************************************************/
t_Void spi_tclRespInterface::vPostDeviceDisplayContext(
   t_U32 u32DeviceHandle,
   t_Bool bDisplayFlag,
   tenDisplayContext enDisplayContext,
   const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(enDisplayContext);	
   SPI_INTENTIONALLY_UNUSED(bDisplayFlag);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
** FUNCTION:t_Void spi_tclRespInterface::vPostDeviceAudioContext(t_U32 u32DeviceHandle,
                                       t_Bool bPlayFlag, t_U8 u8AudioContext,
                                       const trUserContext rcUsrCntxt)
***************************************************************************/
t_Void spi_tclRespInterface::vPostDeviceAudioContext(t_U32 u32DeviceHandle,
                                       t_Bool bPlayFlag, t_U8 u8AudioContext,
                                       const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(bPlayFlag);
   SPI_INTENTIONALLY_UNUSED(u8AudioContext);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
  
}


/***************************************************************************
** FUNCTION: t_Void vPostDeviceAppState(tenSpeechAppState enSpeechAppState, 
                                   tenPhoneAppState enPhoneAppState, tenNavAppState enNavAppState, 
                                   const trUserContext rcUsrCntxt)
***************************************************************************/
t_Void spi_tclRespInterface::vPostDeviceAppState(tenSpeechAppState enSpeechAppState, 
                                                 tenPhoneAppState enPhoneAppState,  
                                                 tenNavAppState enNavAppState, 
                                                 const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enSpeechAppState);
   SPI_INTENTIONALLY_UNUSED(enPhoneAppState);
   SPI_INTENTIONALLY_UNUSED(enNavAppState);
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
}

/***************************************************************************
** FUNCTION: t_Void vUpdateSessionStatusInfo(t_U32 u32DeviceHandle,
                                tenDeviceCategory enDevCat,
                                tenSessionStatus enSessionStatus)
***************************************************************************/
t_Void spi_tclRespInterface::vUpdateSessionStatusInfo(t_U32 u32DeviceHandle,
                                tenDeviceCategory enDevCat,
                                tenSessionStatus enSessionStatus)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   SPI_INTENTIONALLY_UNUSED(enDevCat);
   SPI_INTENTIONALLY_UNUSED(enSessionStatus);
}
