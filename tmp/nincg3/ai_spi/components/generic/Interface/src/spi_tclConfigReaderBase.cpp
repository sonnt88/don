/***********************************************************************/
/*!
 * \file  spi_tclConfigReaderBase.h
 * \brief Base Class Implementation of Config Reader for Gen3 Projects
 *************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base Class Implementation of Config Reader for Gen3 Projects
AUTHOR:         Dhiraj Asopa
COPYRIGHT:      &copy; RBEI
HISTORY:
Date        | Author                | Modification
29.10.2015  | Dhiraj Asopa          | Initial Version with implementation to get and set Vehicle Id Info.


\endverbatim
 *************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Trace.h"
#include "spi_tclConfigReaderBase.h"

#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/spi_tclConfigReaderBase.cpp.trc.h"
#endif
#endif

/***************************************************************************
 ** FUNCTION:  spi_tclConfigReaderBase::spi_tclConfigReaderBase()
 ***************************************************************************/
spi_tclConfigReaderBase::spi_tclConfigReaderBase()
{
  ETG_TRACE_USR1(("spi_tclConfigReaderBase() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclConfigReaderBase::~spi_tclConfigReaderBase()
 ***************************************************************************/
spi_tclConfigReaderBase::~spi_tclConfigReaderBase()
{
  ETG_TRACE_USR1(("~spi_tclConfigReaderBase() entered \n"));
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>


