/*!
 *******************************************************************************
 * \file             spi_tclAudioIntf.h
 * \brief            Interface class that will be used to send responses 
                     from Audio policy to the Audio Manager
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Interface Audio class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                                  | Modifications
 28.10.2013 |  Hari Priya E R(RBEI/ECP2)               | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/



#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAudioIntf.cpp.trc.h"
#endif

#include "spi_tclAudioIntf.h"


/***************************************************************************
** FUNCTION:  spi_tclAudioIntf::spi_tclAudioIntf();
***************************************************************************/
spi_tclAudioIntf::spi_tclAudioIntf()
{
	ETG_TRACE_USR1(("spi_tclAudioIntf::spi_tclAudioIntf()"));
}

/***************************************************************************
** FUNCTION:  spi_tclAudioIntf::~spi_tclAudioIntf();
***************************************************************************/
spi_tclAudioIntf::~spi_tclAudioIntf()
{
	ETG_TRACE_USR1(("spi_tclAudioIntf::~spi_tclAudioIntf()"));
}