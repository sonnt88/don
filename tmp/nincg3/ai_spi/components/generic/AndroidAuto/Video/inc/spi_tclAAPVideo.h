
/***********************************************************************/
/*!
* \file   spi_tclAAPVideo.h
* \brief  AAP Video class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Video class
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shiva Kumar Gurija    | Initial Version
04.02.2015  | Shiva Kumar Gurija    | Select Device Error Handling & Launch Video Impl
04.02.2016  | Shiva Kumar Gurija    | Moved LaunchApp handling from Video to RsrcMngr

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPVIDEO_H_
#define _SPI_TCLAAPVIDEO_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Timer.h"
#include "SPITypes.h"
#include "spi_tclVideoDevBase.h"
#include "spi_tclAAPRespVideo.h"
#include "Lock.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class    spi_tclAAPVideo
* \brief    AAP Video implementation
*           This class interacts with AAP Video wrapper to interact with google
*           API's
****************************************************************************/
class spi_tclAAPVideo:public spi_tclVideoDevBase, public spi_tclAAPRespVideo
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideo::spi_tclAAPVideo()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideo()
   * \brief   Default Constructor
   * \param   t_Void
   * \sa      ~spi_tclAAPVideo()
   **************************************************************************/
   spi_tclAAPVideo();

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideo::~spi_tclAAPVideo()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPVideo()
   * \brief   Destructor
   * \param   t_Void
   * \sa      spi_tclAAPVideo()
   **************************************************************************/
   ~spi_tclAAPVideo();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPVideo::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the AAP Video related things
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideo::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief   To Uninitialize all the AAP Video related things
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vUninitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclAAPVideo::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks)
   * \brief   To Register for the asynchronous responses that are required from
   *          AAP Video
   * \param   corfrVideoCallbacks : [IN] Video callabcks structure
   * \retval  t_Void
   **************************************************************************/
   t_Void vRegisterCallbacks(const trVideoCallbacks& corfrVideoCallbacks);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideo::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSelectDevice(const t_U32 cou32DevId,
   *          const tenDeviceConnectionReq coenConnReq)
   * \brief   To Initialize/UnInitialize Video setup for the selected device
   * \pram    cou32DevId : [IN] Unique Device Id
   * \param   coenConnReq : [IN] connected/disconnected
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId, const tenDeviceConnectionReq coenConnReq);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPVideo::bLaunchVideo()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bLaunchVideo(const t_U32 cou32DevId,
   *                 const t_U32 cou32AppId,
   *                 const tenEnabledInfo coenSelection)
   * \brief   To Launch the Video for the requested app
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    cou32AppId  : [IN] Application Id
   * \pram    coenSelection  : [IN] Enable/disable the video
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bLaunchVideo(const t_U32 cou32DevId, const t_U32 cou32AppId,
            const tenEnabledInfo coenSelection);

   /***************************************************************************
   ** FUNCTION:  t_U32  spi_tclAAPVideo::vStartVideoRendering()
   ***************************************************************************/
   /*!
   * \fn      t_Void vStartVideoRendering(t_Bool bStartVideoRendering)
   * \brief   Method to inform that the SPI Layer is enabled/disabled
   * \pram    bStartVideoRendering : [IN] True - Layer is enabled
   *                                      False - Layer is Disabled
   * \retval  t_Void
   **************************************************************************/
   t_Void vStartVideoRendering(t_Bool bStartVideoRendering);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPVideo::vGetVideoSettings()
   ***************************************************************************/
   /*!
   * \fn     t_Void vGetVideoSettings(const t_U32 cou32DevId,
   *                                  trVideoAttributes& rfrVideoAttributes
   * \brief  To get the current Video Settings.
   * \param  u32DeviceHandle    : [IN] Uniquely identifies the target Device.
   * \param  rfrVideoAttributes : [OUT]includes screen size & orientation.
   * \retval t_Void
   * \sa
   **************************************************************************/
   t_Void vGetVideoSettings(const t_U32 cou32DevId,
      trVideoAttributes& rfrVideoAttributes);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPVideo::vSetOrientationMode()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetOrientationMode(const t_U32 cou32DevId,
   *                                    const tenOrientationMode coenOrientationMode,
   *                                    const trUserContext& corfrUsrCntxt)
   * \brief  Interface to set the orientation mode of the projected display.
   * \param  cou32DevId          : [IN] Uniquely identifies the target Device.
   * \param  coenOrientationMode : [IN] Orientation Mode Value.
   * \param  corfrUsrCntxt       : [IN] User Context
   * \retval t_Void
   **************************************************************************/
   t_Void vSetOrientationMode(const t_U32 cou32DevId,
      const tenOrientationMode coenOrientationMode,
      const trUserContext& corfrUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideo::vOnSelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
   *                 const tenDeviceConnectionReq coenConnReq,
   *                 const tenResponseCode coenRespCode)
   * \brief   To perform the actions that are required, after the select device is
   *           successful
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    coenConnReq : [IN] Identifies the Connection Request.
   * \pram    coenRespCode: [IN] Response code. Success/Failure
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq,
      const tenResponseCode coenRespCode);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPVideo::vTriggerVideoFocusCb()
    ***************************************************************************/
    /*!
    * \fn      t_Void vTriggerVideoFocusCb(t_S32 s32Focus, t_S32 s32Reason)
    * \brief   Method to trigger video focus callback to resource management
    * \param   s32Focus  : [IN] Video Focus Mode
    * \param   s32Reason : [IN] Video Focus Reason
    * \retval  t_Void
    **************************************************************************/
    t_Void vTriggerVideoFocusCb(t_S32 s32Focus, t_S32 s32Reason);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideo(const spi_tclAAPVideo...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideo( const spi_tclAAPVideo& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPVideo()
   ***************************************************************************/
   spi_tclAAPVideo(const spi_tclAAPVideo& corfoSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideo& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideo& operator=(const spi_tclAAPVideo& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPVideo(const spi_tclAAPVideo& otrSrc)
   ***************************************************************************/
   spi_tclAAPVideo& operator=(const spi_tclAAPVideo& corfoSrc);

   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPVideo::bInitVideoSession()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitVideoSession(t_U32 u32DevID))
   * \brief   Method to initialize the Video resources that are required for session
   * \pram    u32DevID  : [IN] Uniquely identifies the target Device.
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bInitVideoSession(t_U32 u32DevID);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideo::vUnInitVideoSession()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUnInitVideoSession(t_U32 u32DevID)
   * \brief   Method to uninitialize the Video resources that were allocated for session
   * \pram    u32DevID  : [IN] Uniquely identifies the target Device.
   * \retval  t_Void
   **************************************************************************/
   t_Void vUnInitVideoSession(t_U32 u32DevID);

   //! Video Callbacks structure
   trVideoCallbacks m_rVideoCallbacks;

   //! Currently selected device
   t_U32 m_u32SelectedDeviceID;
   
   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/
}; //class spi_tclAAPVideo

#endif //_SPI_TCLAAPVIDEO_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
