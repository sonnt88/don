/***********************************************************************/
/*!
* \file    spi_tclAAPVideo.cpp
* \brief   AAP Video implementation
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Video implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date         | Author                 | Modification
20.03.2015   | Shiva Kumar Gurija     | Initial Version
04.02.2015   | Shiva Kumar Gurija     | Select Device Error Handling & Launch Video Impl
03.07.2015   | Shiva Kumar Gurija     | Dynamic display configuration
04.02.2016   | Shiva Kumar Gurija    | Moved LaunchApp handling from Video to RsrcMngr

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "AAPTypes.h"
#include "spi_tclVideoTypedefs.h"
#include "spi_tclConfigReader.h"
#include "spi_tclVideoSettings.h"
#include "spi_tclAAPManager.h"
#include "spi_tclAAPCmdVideo.h"
#include "spi_tclAAPVideo.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_VIDEO
      #include "trcGenProj/Header/spi_tclAAPVideo.cpp.trc.h"
   #endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static spi_tclAAPCmdVideo* spoCmdVideo = NULL;

/***************************************************************************
 ** FUNCTION:  spi_tclAAPVideo::spi_tclAAPVideo()
 ***************************************************************************/
spi_tclAAPVideo::spi_tclAAPVideo():m_u32SelectedDeviceID(0)
{
   ETG_TRACE_USR1(("spi_tclAAPVideo() entered"));

   spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      //! Register with AAP manager for Video callbacks
      poAAPManager->bRegisterObject((spi_tclAAPRespVideo*)this);
   }//if(NULL != poAAPManager)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPVideo::~spi_tclAAPVideo()
 ***************************************************************************/
spi_tclAAPVideo::~spi_tclAAPVideo()
{
   ETG_TRACE_USR1(("~spi_tclAAPVideo() entered"));
   m_u32SelectedDeviceID = 0;
   spoCmdVideo = NULL;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPVideo::bInitialize()
 ***************************************************************************/
t_Bool spi_tclAAPVideo::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPVideo::bInitialize() entered"));

   t_Bool bRet = false;

   spi_tclAAPManager* poAAPMngr = spi_tclAAPManager::getInstance();
   SPI_NORMAL_ASSERT(NULL == poAAPMngr);

   if (NULL != poAAPMngr)
   {
      spoCmdVideo = poAAPMngr->poGetVideoInstance();
      bRet = true;

   }//if (NULL != poAAPMngr)

   return bRet;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPVideo::vUninitialize()
 ***************************************************************************/
t_Void spi_tclAAPVideo::vUninitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPVideo::vUninitialize() entered"));
}

/***************************************************************************
 ** FUNCTION:  t_Void  spi_tclMLVideo::vRegisterCallbacks()
 ***************************************************************************/
t_Void spi_tclAAPVideo::vRegisterCallbacks(
         const trVideoCallbacks& corfrVideoCallbacks)
{
   ETG_TRACE_USR1(("spi_tclAAPVideo::vRegisterCallbacks() entered"));
   m_rVideoCallbacks = corfrVideoCallbacks;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPVideo::vSelectDevice()
 ***************************************************************************/
t_Void spi_tclAAPVideo::vSelectDevice(const t_U32 cou32DevId,
                                      const tenDeviceConnectionReq coenConnReq)
{
   /*lint -esym(40,fpvSelectDeviceCb) fpvSelectDeviceCb is not declared */
   ETG_TRACE_USR1(("spi_tclAAPVideo:vSelectDevice: Device ID-0x%x Selection Type - %d ",
      cou32DevId, ETG_ENUM(CONNECTION_REQ, coenConnReq)));

   tenErrorCode enErrCode = e8NO_ERRORS;
   if (e8DEVCONNREQ_SELECT == coenConnReq)
   {
      //Initialize Video session
      enErrCode = (bInitVideoSession(cou32DevId)) ? e8NO_ERRORS : e8UNKNOWN_ERROR;
   }//if( coenConnReq == e8DEVCONNREQ_SELECT )
   else
   {
      vUnInitVideoSession(cou32DevId);
   }//else

   if (NULL != m_rVideoCallbacks.fpvSelectDeviceCb)
   {
      (m_rVideoCallbacks.fpvSelectDeviceCb)(cou32DevId, enErrCode);
   }//if(NULL != m_rVideoCallbacks.fpvSelectDeviceCb)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPVideo::bInitVideoSession()
***************************************************************************/
t_Bool spi_tclAAPVideo::bInitVideoSession(t_U32 u32DevID)
{
   ETG_TRACE_USR1(("spi_tclAAPVideo::bInitVideoSession:Device ID-0x%x", u32DevID));

   t_Bool bRet = false;

   if( (m_u32SelectedDeviceID != u32DevID) && (0 != u32DevID) )
   {
      //un initilize video session for the earlier connected device, in case if the clean up was not performed.
      vUnInitVideoSession(m_u32SelectedDeviceID);

      //create the VideoSink end point using ADIT interfaces
      trAAPVideoConfig rAAPVideoConfig;

      spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();
      spi_tclVideoSettings* poSettings = spi_tclVideoSettings::getInstance();
      if( (NULL != poSettings) && (NULL != spoCmdVideo)&&(NULL != poConfigReader) )
      {
         trVideoConfigData rVideoConfigData;
         poConfigReader->vGetVideoConfigData(e8DEV_TYPE_ANDROIDAUTO,rVideoConfigData);

         rAAPVideoConfig.u32ScreenHeight = rVideoConfigData.u32ProjScreen_Height;
         rAAPVideoConfig.u16LayerID = rVideoConfigData.u32LayerId;
         rAAPVideoConfig.u16SurfaceID = rVideoConfigData.u32SurfaceId;

         rAAPVideoConfig.szVideoCodec = poSettings->szGetVideoCodecType().c_str();
         rAAPVideoConfig.u16DpiDensity = poSettings->u16GetPixelDenisty(e8DEV_TYPE_ANDROIDAUTO);
         rAAPVideoConfig.u8Fps = poSettings->u8GetFramesPerSec();
         rAAPVideoConfig.bAutoStartProjection = poSettings->bGetAutoStartProjection();
         rAAPVideoConfig.u8MaxUnAckedFrames = poSettings->u8GetMaxUnackedFrames();
         rAAPVideoConfig.u32ScreenWidth= rVideoConfigData.u32ProjScreen_Width;

         bRet = spoCmdVideo->bInitialize(rAAPVideoConfig);
         m_u32SelectedDeviceID = u32DevID;

      }//if((NULL != poSettings) && (NULL != spoCmdVideo) )

   }//if( (m_u32SelectedDeviceID != u32DevID) && (0 != u32DevID) )
   else
   {
      //return true if the currently selected device is same as already selected device.
      //return an error, if the device ID is zero(invalid device).
      bRet = ((0 != u32DevID)&&(m_u32SelectedDeviceID == u32DevID));
   }

   ETG_TRACE_USR4(("spi_tclAAPVideo::bInitVideoSession left with %d", ETG_ENUM(
      BOOL, bRet)));

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideo::vUnInitVideoSession()
***************************************************************************/
t_Void spi_tclAAPVideo::vUnInitVideoSession(t_U32 u32DevID)
{
   ETG_TRACE_USR1(("spi_tclAAPVideo::vUnInitVideoSession:Device ID-0x%x", u32DevID));
   if ( (0 != m_u32SelectedDeviceID) && (NULL != spoCmdVideo) )
   {
      spoCmdVideo->vUninitialize();
      m_u32SelectedDeviceID = 0;
   }//if (NULL != spoCmdVideo)
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPVideo::bLaunchVideo()
***************************************************************************/
t_Bool spi_tclAAPVideo::bLaunchVideo(const t_U32 cou32DevId,
                                     const t_U32 cou32AppId, 
                                     const tenEnabledInfo coenSelection)
{
   ETG_TRACE_USR1(("spi_tclAAPVideo::bLaunchVideo:Device ID-0x%x AppID-0x%x", 
      cou32DevId,cou32AppId));

   SPI_INTENTIONALLY_UNUSED(coenSelection);
   
   return true;
}

/***************************************************************************
 ** FUNCTION:  t_U32  spi_tclAAPVideo::vStartVideoRendering()
 ***************************************************************************/
t_Void spi_tclAAPVideo::vStartVideoRendering(t_Bool bStartVideoRendering)
{
	/*lint -esym(40,fpvVideoRenderStatusCb) fpvVideoRenderStatusCb identifier*/
   // Send response to the set request
   if (NULL != m_rVideoCallbacks.fpvVideoRenderStatusCb)
   {
      (m_rVideoCallbacks.fpvVideoRenderStatusCb)(bStartVideoRendering,
               e8DEV_TYPE_ANDROIDAUTO);
   }//if(NULL != m_rVideoCallbacks.fpvVideoRenderStatusCb )
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAAPVideo::vGetVideoSettings()
***************************************************************************/
t_Void spi_tclAAPVideo::vGetVideoSettings(const t_U32 cou32DevId,
                                          trVideoAttributes& rfrVideoAttributes)
{
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   SPI_INTENTIONALLY_UNUSED(rfrVideoAttributes);
   //unused
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideo::vOnSelectDeviceResult()
***************************************************************************/
t_Void spi_tclAAPVideo::vOnSelectDeviceResult(const t_U32 cou32DevId,
                                              const tenDeviceConnectionReq coenConnReq,
                                              const tenResponseCode coenRespCode)
{
   ETG_TRACE_USR1(("spi_tclAAPVideo:vOnSelectDeviceResult: Device ID-0x%x Device Selection Type - %d "
            "Response Code - %d", cou32DevId, ETG_ENUM(CONNECTION_REQ, coenConnReq),
            ETG_ENUM(RESPONSE_CODE,coenRespCode)));

   //Device selection is failed. Clear the video resources,if already allocated
   if ( (e8FAILURE == coenRespCode) && (e8DEVCONNREQ_SELECT == coenConnReq) )
   {
      vUnInitVideoSession(m_u32SelectedDeviceID);
   } //if ( (e8FAILURE == coenRespCode) && (e8DEVCONNREQ_SELECT == coenConnReq) )
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAAPVideo::vSetOrientationMode()
***************************************************************************/
t_Void spi_tclAAPVideo::vSetOrientationMode(const t_U32 cou32DevId,
                                            const tenOrientationMode coenOrientationMode,
                                            const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAAPVideo::vSetOrientationMode"));

   /*lint -esym(40,fpvSetOrientationModeCb) fpvSetOrientationModeCb Undeclared identifier */
   SPI_INTENTIONALLY_UNUSED(coenOrientationMode);
   if(NULL != m_rVideoCallbacks.fpvSetOrientationModeCb)
   {
      (m_rVideoCallbacks.fpvSetOrientationModeCb)(cou32DevId,
         e8UNSUPPORTED_OPERATION ,corfrUsrCntxt,e8DEV_TYPE_ANDROIDAUTO);
   }// if(NULL != m_rVideoCallbacks.fpvCbSetVideoBlockingMode)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideo::vTriggerVideoFocusCb()
***************************************************************************/
t_Void spi_tclAAPVideo::vTriggerVideoFocusCb(t_S32 s32Focus, t_S32 s32Reason)
{
   ETG_TRACE_USR1(("spi_tclAAPVideo::vTriggerVideoFocusCb: Focus-%d",s32Focus));
   if (NULL != spoCmdVideo)
   {
      spoCmdVideo->videoFocusCallback(s32Focus, s32Reason);
   }//if (NULL != spoCmdVideo)
}

//lint –restore


///////////////////////////////////////////////////////////////////////////////
// <EOF>
