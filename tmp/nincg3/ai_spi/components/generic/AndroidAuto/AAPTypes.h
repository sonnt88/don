/*!
 *******************************************************************************
 * \file              AAPTypes.h
 * \brief             Android Auto Types
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Common AAP Types
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.02.2015 |  Pruthvi Thej Nagaraju       | Initial Version
 24.03.2015 |  SHITANSHU SHEKHAR           | Added sensor types
 05.02.2016 |  Rachana L Achar             | Added tenAAPkeyCodes enum
 26.02.2016 |  Rachana L Achar             | Added enums for navigation status and next turn data

 \endverbatim
 ******************************************************************************/
#ifndef AAPTYPES_H_
#define AAPTYPES_H_

#include <algorithm>

#include "BaseTypes.h"
#include "SPITypes.h"


static const t_U32 scou32InvalidID = 0xFFFFFFFF;

//! Maintain the enum similar to the google auto enum DriverPosition
enum tenDriverPosition
{
   e8DRIVERPOSITION_LEFT = 0,
   e8DRIVERPOSITION_RIGHT = 1,
   e8DRIVERPOSITION_CENTER = 2
};
/*
struct trHeadUnitInfo
{
      t_String szMake;
      t_String szModel;
      t_String szYear;
      t_String szDescription;
      t_String szVersion;
      t_String szUniqueID;
      tenDriverPosition enDriverPos;

      trHeadUnitInfo() :
         enDriverPos(e8DRIVERPOSITION_LEFT)
      {

      }
};*/

struct trUSBDeviceInfo
{
      t_U32 u32VendorID;
      t_U32 u32ProductID;
      t_String szSerialNumber;
      t_String   szProduct;
      t_String   szmanufacturer;
      t_U32    u32DeviceNumber;
      t_String szSystemPath;
      t_U32    u32Interface;
      t_Bool   bIsAOAPSupported;

      trUSBDeviceInfo():u32VendorID(0), u32ProductID(0),u32DeviceNumber(0), u32Interface(0), bIsAOAPSupported(false)
      {

      }
};

struct trAAPSessionInfo
{
      t_U32 u32DeviceID;
      t_U32 u32HeadUnitID;
      trAAPSessionInfo(): u32DeviceID(scou32InvalidID), u32HeadUnitID(scou32InvalidID)
      {

      }
};

struct trAAPHeadUnitInfo
{
      t_String szManufacturer;
      t_String szModelName;
      t_String szDescription;
      t_String szVersion;
      t_String szURI;
      t_String szSerial;
      t_String szYear;
      t_String szSoftwareVersion;
      t_String szVehicleManufacturer;
      t_String szVehicleModel;
      t_U32 u32EnableAudio;
      tenDriverPosition enDriverPos;

      trAAPHeadUnitInfo():szManufacturer("BOSCH"),szModelName("G3G"), szDescription("Android Auto"),
               szVersion("1.0"),szURI("http://www.android.com/auto"),szSerial("0000000012345678"), szYear("2016"),
               szVehicleManufacturer("CAR"),szVehicleModel("CAR"),
               u32EnableAudio(0),enDriverPos(e8DRIVERPOSITION_LEFT)
      {
         //! TODO interface to get serial number?
      }
};

struct trAAPDeviceInfo
{
      trAAPSessionInfo rSessionInfo;
      trUSBDeviceInfo rUSBDeviceInfo;
      trUSBDeviceInfo rA0APDeviceInfo;
};

/*******************************************************************************************************
| **************************************   VIDEO *******************************************************
| *****************************************************************************************************/

typedef enum
{
   e8AAP_VIDEOFOCUSSTATE_UNKNOWN = 0,
   e8AAP_VIDEOFOCUSSTATE_GAIN = 1,
   e8AAP_VIDEOFOCUSSTATE_GAIN_TRANSIENT = 2,
   e8AAP_VIDEOFOCUSSTATE_LOSS = 3,
   e8AAP_VIDEOFOCUSSTATE_LOSS_TRANSIENT = 4
}tenVideoFocusState;

typedef enum
{
   e8VIDEOFOCUS_UNKNOWN = 0,
   e8VIDEOFOCUS_PROJECTED = 1,
   e8VIDEOFOCUS_NATIVE = 2
} tenVideoFocus;

typedef enum
{
   e8VIDEOFOCUS_REASON_UNKNOWN = 0,
   e8VIDEOFOCUS_REASON_PHONE_SCREEN_OFF = 1,
   e8VIDEOFOCUS_REASON_LAUNCH_NATIVE = 2

}tenVideoFocusReason;

typedef enum
{
   e8VIDEOFRAMERATETYPE_FPS_60 = 1,
   e8VIDEOFRAMERATETYPE_FPS_30 = 2
}tenVideoFrameRateType;

typedef enum
{
   e8MEDIA_CODEC_UNKNOWN = 0,
   e8MEDIA_CODEC_AUDIO_PCM = 1,
   e8MEDIA_CODEC_AUDIO_AAC_LC = 2,
   e8MEDIA_CODEC_VIDEO_H264_BP = 3,
   e8MEDIA_CODEC_AUDIO_AAC_LC_ADTS = 4
}tenMediaCodecTypes;

//! response type to MD Video Focus requests
typedef enum 
{
   e8AAP_VIDEOFOCUS_GRANT = 0,
   e8AAP_VIDEOFOCUS_DENY = 1,
   e8AAP_VIDEOFOCUS_DELAY = 2
} tenAAPMDVideoFocusReqResp;

//! Video configuration required for AAP Video
struct trAAPVideoConfig
{
   t_U16 u16LayerID;
   t_U16 u16SurfaceID;
   t_String szVideoCodec;
   t_U8 u8MaxUnAckedFrames;
   t_U32 u32ScreenWidth;
   t_U32 u32ScreenHeight;
   t_U16 u16DpiDensity;
   t_U8 u8Fps;
   t_Bool bAutoStartProjection;


   trAAPVideoConfig():u16LayerID(0),u16SurfaceID(0),szVideoCodec("MEDIA_CODEC_VIDEO_H264_BP"),
      u8MaxUnAckedFrames(4),u32ScreenWidth(800),u32ScreenHeight(480),
      u16DpiDensity(160),u8Fps(30),bAutoStartProjection(false){}
};

//It is used to hold the data mentioned in the config file,
//to derive the current HU Video focus state based on the context & display flag
struct trAAPAccVideoFocusNoti
{
   tenDisplayContext enAccDispCntxt;
   //This state is set, when HMI requests for Video Focus
   tenVideoFocusState enAccFocusReqType;
   //This state is set, when HMI releases for Video Focus
   tenVideoFocusState enAccFocusRelType;

   trAAPAccVideoFocusNoti& operator=(const trAAPAccVideoFocusNoti& corfrAAPAccVideoFocusNoti)
   {
      if(&corfrAAPAccVideoFocusNoti != this)
      {
         enAccDispCntxt = corfrAAPAccVideoFocusNoti.enAccDispCntxt;
         enAccFocusReqType = corfrAAPAccVideoFocusNoti.enAccFocusReqType;
         enAccFocusRelType = corfrAAPAccVideoFocusNoti.enAccFocusRelType;
      } //if(&corfrAAPAccVideoFocusNoti != this)
      return *this;
   }
};

struct trAAPVideoFocusState
{
   //Current MD Focus State
   tenVideoFocusState enCurMDFocusState;
   //Accessory or MD Requested Focus State
   tenVideoFocusState enReqFocusState;
   //New MD Focus State
   tenVideoFocusState enUpdatedMDFocusState;

   trAAPVideoFocusState& operator=(const trAAPVideoFocusState& corfrAAPVideoFocusState)
   {
      if( &corfrAAPVideoFocusState != this )
      {
         enCurMDFocusState = corfrAAPVideoFocusState.enCurMDFocusState;
         enReqFocusState = corfrAAPVideoFocusState.enReqFocusState;
         enUpdatedMDFocusState = corfrAAPVideoFocusState.enUpdatedMDFocusState;
      }   //if( &corfrAAPVideoFocusState != this )
      return *this;
   }
};


struct trAAPMDVideoFocusReq
{
   //Current Accessory display context
   tenDisplayContext enAccDispCntxt;
   tenAAPMDVideoFocusReqResp enMDFocusReqResp_StateLoss;
   tenAAPMDVideoFocusReqResp enMDFocusReqResp_StateLossTransient;

   trAAPMDVideoFocusReq& operator=(const trAAPMDVideoFocusReq& corfrAAPMDVideoFocusReq)
   {
      if( &corfrAAPMDVideoFocusReq != this )
      {
         enAccDispCntxt = corfrAAPMDVideoFocusReq.enAccDispCntxt;
         enMDFocusReqResp_StateLoss = corfrAAPMDVideoFocusReq.enMDFocusReqResp_StateLoss;
         enMDFocusReqResp_StateLossTransient = corfrAAPMDVideoFocusReq.enMDFocusReqResp_StateLossTransient;
      }   //if( &corfrAAPMDVideoFocusReq != this )
      return *this;
   }
};

#define STR_METADATA_LENGTH 200
struct trAAPMediaPlaybackStatus
{
      t_U32 u32State;
      t_Char cMediaSource[STR_METADATA_LENGTH];
      t_U32 u32PlaybackSeconds;
      t_Bool bShuffle;
      t_Bool bRepeat;
      t_Bool bRepeatOne;

      trAAPMediaPlaybackStatus() :
         u32State(0), u32PlaybackSeconds(0), bShuffle(false), bRepeat(false),
                  bRepeatOne(false)
      {
         memset(cMediaSource, 0, STR_METADATA_LENGTH);
      }
};

struct trAAPMediaPlaybackMetadata
{
      t_Char cSong[STR_METADATA_LENGTH];
      t_Char cAlbum[STR_METADATA_LENGTH];
      t_Char cArtist[STR_METADATA_LENGTH];
      t_Char cAlbum_art[STR_METADATA_LENGTH];
      t_Char cPlaylist[STR_METADATA_LENGTH];
      t_U32 u32DurationSeconds;
      t_U32 u32Rating;

      trAAPMediaPlaybackMetadata() :
         u32DurationSeconds(0), u32Rating(0)
      {
         memset(cSong, 0, STR_METADATA_LENGTH);
         memset(cArtist, 0, STR_METADATA_LENGTH);
         memset(cAlbum, 0, STR_METADATA_LENGTH);
         memset(cAlbum_art, 0, STR_METADATA_LENGTH);
         memset(cPlaylist, 0, STR_METADATA_LENGTH);
      }

};
/*******************************************************************************************************
| ********************************END  OF  VIDEO *******************************************************
| *****************************************************************************************************/


typedef enum
{
   e32MODULEID_AAPDISCOVERER = 1,
   e32MODULEID_AAPSESSION = 2,
   e32MODULEID_AAPAUDIO = 3,
   e32MODULEID_AAPVIDEO = 4,
   e32MODULEID_AAPTOUCH = 5,
   e32MODULEID_AAPSENSOR = 6,
   e32MODULEID_AAPBLUETOOTH = 7,
   e32MODULEID_AAPMEDIAPLAYBACK = 8,
   e32MODULEID_AAPNAVIGATIONTBT=9,
   e32MODULEID_AAPNOTIFICATION=10
} tenAAPModuleID;

typedef enum
{
   e32SESSIONID_AAPAUDIO = 10,
   e32SESSIONID_AAPVIDEO = 20,
   e32SESSIONID_AAPTOUCH = 30,
   e32SESSIONID_AAPSENSOR = 40,
   e32SESSIONID_AAPBLUETOOTH = 50,
   e32SESSIONID_AAPMEDIAPLAYBACK = 60,
   e32SESSIONID_AAPNAVIGATION=70,
   e32SESSIONID_AAPNOTIFICATION=80
} tenAAPSessionID;

enum tenBTPairingMethod
{
   e8BT_PAIRING_OOB = 1,
   e8BT_PAIRING_NUMERIC_COMPARISON = 2,
   e8BT_PAIRING_PASSKEY_ENTRY = 3,
   e8BT_PAIRING_PIN = 4
};

static const tenBTPairingMethod scenPreferredAAPBTPairingMethod = e8BT_PAIRING_NUMERIC_COMPARISON;

enum tenAAPNavFocusType {
  e8_NAV_FOCUS_NATIVE = 1,
  e8_NAV_FOCUS_PROJECTED = 2
};

enum tenAAPByeByeReason {
  e8_USER_SELECTION = 1
};


enum tenAAPVoiceSessionStatus {
   e8_VOICE_SESSION_START = 1,
   e8_VOICE_SESSION_END = 2
};

enum tenAAPDeviceAudioFocusRequest {
   e8_AUDIO_FOCUS_REQ_GAIN = 1,
   e8_AUDIO_FOCUS_REQ_GAIN_TRANSIENT = 2,
   e8_AUDIO_FOCUS_REQ_GAIN_TRANSIENT_MAY_DUCK = 3,
   e8_AUDIO_FOCUS_REQ_RELEASE = 4
};

//!Identifies various Audio streams from AAP device.
enum tenAAPAudStreamType
{
   e8_AUDIOTYPE_MEDIA = 0,
   e8_AUDIOTYPE_GUIDANCE = 1,
   e8_AUDIOTYPE_SYSTEM_AUDIO = 2,
   e8_AUDIOTYPE_VOICE = 3,
   e8_AUDIOTYPE_MICROPHONE = 4
};

enum tenAAPDeviceAudioFocusState
{
   e8_FOCUS_STATE_GAIN = 1,
   e8_FOCUS_STATE_GAIN_TRANSIENT = 2,
   e8_FOCUS_STATE_LOSS = 3,
   e8_FOCUS_STATE_LOSS_TRANSIENT_CAN_DUCK = 4,
   e8_FOCUS_STATE_LOSS_TRANSIENT = 5,
   e8_FOCUS_STATE_GAIN_MEDIA_ONLY = 6,
   e8_FOCUS_STATE_GAIN_TRANSIENT_GUIDANCE_ONLY = 7
};

enum tenAccessoryAudioFocusState
{
   e8_CAR_FOCUS_STATE_GAINED = 1,
   e8_CAR_FOCUS_STATE_GAINED_TRANSIENT = 2,
   e8_CAR_FOCUS_STATE_LOST = 3,
   e8_CAR_FOCUS_STATE_LOST_TRANSIENT = 4
};

enum tenAAPStreamState
{
	e8_AUD_STREAM_OPEN = 0,   // Playback start received. Blocked until Audio Activation.
	e8_AUD_STREAMING = 1,     // Audio Activation completed. Device configured. Audio streaming in progress.
	e8_AUD_STREAM_CLOSED = 2  // Playback stop received. Audio streaming ended.
};

enum tenAAPStreamErrorCode
{
    e8_AUDSTREAM_ERR_NONE = 0,     // No Streaming Error
	e8_AUDSTREAM_ERR_TIMEOUT = 1   // Playback start received. Time out before Audio channel is activated.
};

typedef std::function<t_Void(tenAAPDeviceAudioFocusState, t_Bool)> vSetAudioFocus;

//! \brief   Structure holding the callbacks to AAP Audio Resource Manager. 
struct trAAPAudioRsrcMngrCallbacks
{
   vSetAudioFocus fvSetAudioFocus;

   trAAPAudioRsrcMngrCallbacks() : fvSetAudioFocus(NULL)
   {
   }
};

struct trAAPAudioStreamInfo
{
      tenAAPAudStreamType enStreamType;
      tenAAPStreamState enStreamState;
      tenAAPStreamErrorCode enErrorCode;
      t_S32 s32SessionID;
      t_Bool bNoiseReductionEnabled;
      t_Bool bEchoCancellationEnabled;

      trAAPAudioStreamInfo() :
         enErrorCode(e8_AUDSTREAM_ERR_NONE), bNoiseReductionEnabled(false), bEchoCancellationEnabled(false),
               s32SessionID(0)
      {

      }

      trAAPAudioStreamInfo(tenAAPAudStreamType enType, tenAAPStreamState enState, tenAAPStreamErrorCode enError,
            t_S32 s32ID = 0, t_Bool bNREnabled = false, t_Bool bECEnabled = false) :
         enStreamType(enType), enStreamState(enState), enErrorCode(enError), s32SessionID(s32ID),
               bNoiseReductionEnabled(bNREnabled), bEchoCancellationEnabled(bECEnabled)
      {

      }
};


enum tenSensorTypes {
	e8_AAP_SENSOR_GPS                   =   1,
	e8_AAP_SENSOR_COMPASS               =   2,
	e8_AAP_SENSOR_SPEED                 =   3,
	e8_AAP_SENSOR_RPM                   =   4,
	e8_AAP_SENSOR_ODOMETER              =   5,
	e8_AAP_SENSOR_FUEL                  =   6,
	e8_AAP_SENSOR_PARKING_BRAKE         =   7,
	e8_AAP_SENSOR_GEAR                  =   8,
	e8_AAP_SENSOR_OBDII_DIAGNOSTIC_CODE =   9,
	e8_AAP_SENSOR_NIGHT_MODE            =   10,
	e8_AAP_SENSOR_ENVIRONMENT_DATA      =   11,
	e8_AAP_SENSOR_HVAC_DATA             =   12,
	e8_AAP_SENSOR_DRIVING_STATUS_DATA   =   13,
	e8_AAP_SENSOR_DEAD_RECKONING_DATA   =   14,
	e8_AAP_SENSOR_PASSENGER_DATA        =   15,
	e8_AAP_SENSOR_DOOR_DATA             =   16,
	e8_AAP_SENSOR_LIGHT_DATA            =   17,
	e8_AAP_SENSOR_TIRE_PRESSURE_DATA    =   18,
	e8_AAP_SENSOR_ACCELEROMETER_DATA    =   19,
	e8_AAP_SENSOR_GYROSCOPE_DATA        =   20,
	e8_AAP_SENSOR_GPS_SATELLITE_DATA    =   21
};

enum tenGearData
{
	e8_AAP_GEAR_NEUTRAL  = 0,
	e8_AAP_GEAR_1        = 1,
	e8_AAP_GEAR_2        = 2,
	e8_AAP_GEAR_3        = 3,
	e8_AAP_GEAR_4        = 4,
	e8_AAP_GEAR_5        = 5,
	e8_AAP_GEAR_6        = 6,
	e8_AAP_GEAR_DRIVE    = 100,
	e8_AAP_GEAR_PARK     = 101,
	e8_AAP_GEAR_REVERSE  = 102
};

enum tenDrivingStatus
{
	e8_DRIVE_STATUS_UNRESTRICTED = 0x0, 		//No restrictions
	e8_DRIVE_STATUS_NO_VIDEO = 0x1,     		//No video playback allowed
	e8_DRIVE_STATUS_NO_KEYBOARD_INPUT = 0x2, 	//No keyboard or rotary controller input allowed
	e8_DRIVE_STATUS_NO_VOICE_INPUT = 0x4, 		//No voice input allowed
	e8_DRIVE_STATUS_NO_CONFIG = 0x8, 			//No setup/configuration allowed
	e8_DRIVE_STATUS_LIMIT_MESSAGE_LEN = 0x10    //Limit displayed message length
};

enum tenDayNightMode
{
	e8_AAP_DAY_MODE = 0,
	e8_AAP_NIGHT_MODE = 1
};

struct trAAPInputConfig
{
	t_U16 u16LayerID;
	t_U16 u16SurfaceID;
	t_U32 u32DisplayWidth;
	t_U32 u32DisplayHeight;
	t_U16 u16TouchWidth;
	t_U16 u16TouchHeight;
	t_U8  u8TouchMaximum;
	t_U8  u8TriggerInterval;
	t_U8  u8EnableVerbose;
	t_Bool bIsRotaryCtrl;
	tenAAPTouchScreenType enAAPTouchScreenType;
};
//! Indicates transient audio status in accessory
enum tenAccessoryTransientAudioState
{
	e8CAR_TRANSIENT_AUD_ACTIVE = 0,
	e8CAR_TRANSIENT_AUD_INACTIVE = 1
};

//! Indicates audio resource owner
enum tenAudioResourceOwner
{
   e8AUDIO_OWNER_CAR = 0,
   e8AUDIO_OWNER_DEVICE = 1
};

//! Indicates audio context type
enum tenAudioContextType
{
   e8_AUDIO_CTXT_MAIN = 0,
   e8_AUDIO_CTXT_TRANSIENT = 1,
   e8_AUDIO_CTXT_TRANSIENT_MIX = 2
};

/*!
* \struct trAAPAudioResponseContext
* \brief structure to hold audio context data
*/
struct trAAPAudioResponseContext
{
   //! Audio focus request from Device
   tenAAPDeviceAudioFocusRequest    enDevAudFocusRequest;

   //! Current Audio focus state of Accessory
   tenAccessoryAudioFocusState      enCurAccAudFocusState;

   //! Current Audio context of Accessory
   tenAudioContextType enCurAccAudContext;

   //! New audio focus state of Device
   tenAAPDeviceAudioFocusState      eNewDevAudFocusState;

   //! New audio focus state of Accessory
   tenAccessoryAudioFocusState      enNewAccAudFocusState;

   trAAPAudioResponseContext& operator=(trAAPAudioResponseContext &rfrAAPAudioContext)
   {
      enDevAudFocusRequest = rfrAAPAudioContext.enDevAudFocusRequest;
      enCurAccAudFocusState = rfrAAPAudioContext.enCurAccAudFocusState;
      enCurAccAudContext = rfrAAPAudioContext.enCurAccAudContext;
      eNewDevAudFocusState = rfrAAPAudioContext.eNewDevAudFocusState;
      enNewAccAudFocusState = rfrAAPAudioContext.enNewAccAudFocusState;
      return *this;
   }
};

/*!
* \struct trAAPAudioNotifContext
* \brief structure to hold audio context data
*/
struct trAAPAudioNotifContext
{
   //! Current playing Audio resource owner
   tenAudioResourceOwner    enAudResourceOwner;

   //! Current playing audio context type
   tenAudioContextType      enAudContextType;

   //! Current Audio focus state of Accessory
   tenAccessoryAudioFocusState  enCurAccAudFocusState;

   //! New audio focus state of Device
   tenAAPDeviceAudioFocusState      eNewDevAudFocusState;

   //! New audio focus state of Accessory
   tenAccessoryAudioFocusState      enNewAccAudFocusState;

   trAAPAudioNotifContext& operator=(trAAPAudioNotifContext &rfrAAPAudioNotifContext)
   {
      enAudResourceOwner = rfrAAPAudioNotifContext.enAudResourceOwner;
      enAudContextType = rfrAAPAudioNotifContext.enAudContextType;
      enCurAccAudFocusState = rfrAAPAudioNotifContext.enCurAccAudFocusState;
      eNewDevAudFocusState = rfrAAPAudioNotifContext.eNewDevAudFocusState;
      enNewAccAudFocusState = rfrAAPAudioNotifContext.enNewAccAudFocusState;
      return *this;
   }
};

//! Indicates Driving mode of vehicle
enum tenDrivingMode
{
   e8_PARK_MODE = 0x0,
   e8_DRIVE_MODE = 0x1
};

// ! Navigation Next Turn Direction
enum tenAAPNavNextTurnSide
{
   e8_NAV_NEXT_TURN_LEFT = 1,
   e8_NAV_NEXT_TURN_RIGHT = 2,
   e8_NAV_NEXT_TURN_UNSPECIFIED = 3

};

// ! Navigation Next Turn Type
enum tenAAPNavNextTurnType
{
   e8_NAV_NEXT_TURN_UNKNOWN = 0,
   e8_NAV_NEXT_TURN_DEPART = 1,
   e8_NAV_NEXT_TURN_NAME_CHANGE = 2,
   e8_NAV_NEXT_TURN_SLIGHT_TURN = 3,
   e8_NAV_NEXT_TURN_TURN = 4,
   e8_NAV_NEXT_TURN_SHARP_TURN = 5,
   e8_NAV_NEXT_TURN_U_TURN = 6,
   e8_NAV_NEXT_TURN_ON_RAMP = 7,
   e8_NAV_NEXT_TURN_OFF_RAMP = 8,
   e8_NAV_NEXT_TURN_FORK = 9,
   e8_NAV_NEXT_TURN_MERGE = 10,
   e8_NAV_NEXT_TURN_ROUNDABOUT_ENTER = 11,
   e8_NAV_NEXT_TURN_ROUNDABOUT_EXIT = 12,
   e8_NAV_NEXT_TURN_ROUNDABOUT_ENTER_AND_EXIT = 13,
   e8_NAV_NEXT_TURN_STRAIGHT = 14,
   e8_NAV_NEXT_TURN_FERRY_BOAT = 16,
   e8_NAV_NEXT_TURN_FERRY_TRAIN = 17,
   e8_NAV_NEXT_TURN_DESTINATION = 19

};

typedef enum
{
  e32_KEYCODE_INVALID = 0x00000000,
  e32_KEYCODE_BACK = 0x00000004,
  e32_KEYCODE_ENTER = 0x00000042,
  e32_KEYCODE_MENU = 0x00000052,
  e32_KEYCODE_SERACH = 0x00000054,
  e32_KEYCODE_MEDIA_NEXT = 0x00000057,
  e32_KEYCODE_MEDIA_PREVIOUS = 0x00000058,
  e32_KEYCODE_ROTARY_CONTROLLER = 0x00010000,
  e32_KEYCODE_MEDIA = 0x00010001,
  e32_KEYCODE_NAVIGATION = 0x00010002,
  e32_KEYCODE_TEL = 0x00010004,
  e32_KEYCODE_PLAY = 0x0000007e,
  e32_KEYCODE_PAUSE = 0x0000007f

} tenAAPkeyCodes;

#endif //AAPTYPES_H_
