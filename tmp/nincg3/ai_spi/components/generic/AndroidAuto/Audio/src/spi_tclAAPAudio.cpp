/*!
 *******************************************************************************
 * \file             spi_tclAAPAudio.cpp
 * \brief            Implements the Audio functionality for Android Auto using
                     interface to AAP Wrapper for GAL .
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Implementation for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclAAPManager.h"
#include "spi_tclAAPCmdAudio.h"
#include "spi_tclAAPAudio.h"

#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAAPAudio.cpp.trc.h"
#endif

#define IS_SPEECH_SESSION_ACTIVE()        ((true == sbVoiceSessionActive)||(e8_AUD_STREAM_CLOSED != m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]))

static t_U32 su32CurSelectedDevID             = 0;
static t_Bool sbVoiceSessionActive            = false;

static timer_t srSpeechRecRelTimerID;
static t_Bool bSpeechRecRelTimerRunning = false;

static const t_Double scodAudioUnduck      = 1.0;
static const t_Double scodAudioDuckDefdB   = 41.0;
static const t_U16 scou16AudioRampDefDuration  = 1000;  //In msec

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::spi_tclAAPAudio()
 ***************************************************************************/
spi_tclAAPAudio::spi_tclAAPAudio() : m_poCmdAudio(NULL)
{
   ETG_TRACE_USR1(("spi_tclAAPAudio entered"));

   spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      // Initializations to be done which are not specific to device connected/selected
      m_poCmdAudio = const_cast<spi_tclAAPCmdAudio*> (poAAPManager->poGetAudioInstance());

      //! Register with AAP manager for Session & Audio callbacks
      poAAPManager->bRegisterObject((spi_tclAAPRespAudio*)this);
      poAAPManager->bRegisterObject((spi_tclAAPRespSession*)this);
   }//if(NULL != poAAPManager)

   //! Initialize audio stream states.
   for(t_U8 u8Cnt = 0; u8Cnt < cou32MaxAudioStreams; u8Cnt++)
   {
       m_aenStreamState[u8Cnt] = e8_AUD_STREAM_CLOSED;
   }//for(t_U8 u8Cnt = 0; u8Cnt < cou32MaxAudioStreams; u8Cnt++)

   //! Initialize audio channel states.
   for(t_U8 u8Cnt = 0; u8Cnt < e8AUD_INVALID; u8Cnt++)
   {
       m_aenChannelStatus[u8Cnt] = e8_AUD_NOT_ACTIVE;
   }//for(t_U8 u8Cnt = 0; u8Cnt < e8AUD_INVALID; u8Cnt++)

}//spi_tclAAPAudio::spi_tclAAPAudio()


/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::~spi_tclAAPAudio()
 ***************************************************************************/
spi_tclAAPAudio::~spi_tclAAPAudio()
{
   ETG_TRACE_USR1(("~spi_tclAAPAudio entered "));
   m_poCmdAudio = NULL;
   su32CurSelectedDevID = 0;
   bSpeechRecRelTimerRunning = false;
   sbVoiceSessionActive = false;

}//spi_tclAAPAudio::~spi_tclAAPAudio()

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::bInitializeAudioPlayback(t_U32 u32DeviceId)
 ***************************************************************************/
t_Bool spi_tclAAPAudio::bInitializeAudioPlayback(t_U32 u32DeviceId, tenAudioDir enAudDir)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   SPI_INTENTIONALLY_UNUSED(enAudDir);

   ETG_TRACE_USR1(("spi_tclAAPAudio::bInitializeAudioPlayback entered "));

   return true;

}//t_Bool spi_tclAAPAudio::bInitializeAudioPlayback(t_U32 u32DeviceId)

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::vSetAudioPipeConfig
 ***************************************************************************/
t_Void spi_tclAAPAudio::vSetAudioPipeConfig(tmapAudioPipeConfig &rfmapAudioPipeConfig)
{
   if(NULL != m_poCmdAudio)
   {
      m_poCmdAudio->vSetAudioPipeConfig(rfmapAudioPipeConfig);
   }//if(NULL != m_poCmdAudio)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::bFinalizeAudioPlayback(t_U32 u32DeviceId, tenAudioDir enAudDir)
 ***************************************************************************/
t_Bool spi_tclAAPAudio::bFinalizeAudioPlayback(t_U32 u32DeviceId, tenAudioDir enAudDir)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);

   /*lint -esym(40,tfbSetAudioDucking)fbSetAudioDucking Undeclared identifier */
   /*lint -esym(746,fbSetAudioDucking)call to function fbSetAudioDucking() not made in the presence of a prototype */

   m_oAudioStreamLock.s16Lock();

   ETG_TRACE_USR1((" spi_tclAAPAudio::bFinalizeAudioPlayback entered "));

   //! Media Source is deactivated.
   if(e8AUD_MAIN_OUT == enAudDir)
   {
       ETG_TRACE_USR4(("Media Audio Channel is deactivated."));

       //! Media Audio Channel is deactivated. Update the Audio Channel state.
       m_aenChannelStatus[enAudDir] = e8_AUD_NOT_ACTIVE;

       //! MD had setup a new Media Audio stream during the deactivation of Media Audio channel
       if (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MEDIA])
       {
           ETG_TRACE_USR4(("Media Audio Stream was opened by MD. Requesting Media Audio Channel Activation"));
           vActivateChannel(enAudDir);
       }//if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MEDIA])

   }//if(e8AUD_MAIN_OUT == enAudDir)

   //! Mix Audio Source (Guidance) is deactivated.
   else if(e8AUD_DUCK == enAudDir)
   {
      ETG_TRACE_USR4(("Mix Audio Channel is deactivated."));

      //! Mix Audio Channel is deactivated. Update the Audio Channel state.
      m_aenChannelStatus[enAudDir] = e8_AUD_NOT_ACTIVE;

      if (NULL != m_rAudCallbacks.fbSetAudioDucking)
      {
         m_rAudCallbacks.fbSetAudioDucking(scou16AudioRampDefDuration, scodAudioUnduck,
               e8_DUCKINGTYPE_UNDUCK, e8DEV_TYPE_ANDROIDAUTO);
      }//if(NULL != m_rAudCallbacks.fbSetAudioDucking)

      //! MD has setup a new Guidance Audio stream during deactivation of Mix Audio channel

      //! Case 1: Speech Session is Inactive. Speech Audio channel could either be inactive or requested for deactivation.
      //! If speech audio channel was active but Speech session was inactive, on Guidance stream playback start Speech Audio channel is deactivated
      //! and Mix Audio channel activation is requested. Refer vProcessGuidanceStart() function.
      if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])
      {
         ETG_TRACE_USR4(("Guidance Audio Stream was opened by MD. Checking the Speech Stream and Audio Channel Status"));

         //! Case 1: Speech Session has been started by MD. Speech Audio channel has been requested for Activation.
         //! Guidance stream was deactivated when Speech was inactive. Mix Audio Channel Deactivation was requested.
         //! Guidance stream was opened again during deactivation of Mix Audio channel.
         //! Activation request processing is delayed to wait for completion of deactivation request.
         //! 1 a) Speech Session was meanwhile started by MD. Speech Audio channel activation is requested.
         //! 1 b) Speech Session was started by MD. Speech Audio channel deactivation was in progress. Channel activation will be requested again
         //! after deactivation of Speech Audio channel is complete. In this scenario, Guidance will play as Speech Downstream Audio.
         if((e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]) ||
           ((true == IS_SPEECH_SESSION_ACTIVE()) && (e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN])))
         {
            ETG_TRACE_USR4(("Guidance Stream Audio will be played as Speech Session Downstream Audio. Waiting for Speech Audio Channel Activation"));
         }
         //! Speech Channel is already granted. Guidance channel is still open. This scenario shouldn't occur as Guidance stream if open will be
         //! unblocked on activation of Speech Channel.
         else if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN])
         {
            ETG_TRACE_USR4(("Guidance Stream is Open even though Speech Audio Channel is granted. Scenario shouldn't occur !!"));
            //! Unblock Guidance Stream

         }//else if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN])

         //! Speech Session has ended and Speech Audio channel deactivation request is being processed or Speech Audio Channel is not active
         //! Play Guidance Stream on Mix Audio channel. Request for Activation of Mix Audio Channel.
         else if ((e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN])||
                 (e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN]))
         {
            vActivateChannel(enAudDir);
         }//else if ((e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN])||...

      }//if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])

   }//else if(e8AUD_DUCK == enAudDir)

   //! Speech Audio Channel is deactivated.
   else if (e8AUD_VR_IN == enAudDir)
   {
       ETG_TRACE_USR4(("Speech Audio Channel is deactivated."));
       
       //! Speech Audio Channel is deactivated. Update the Audio Channel state.
       m_aenChannelStatus[enAudDir] = e8_AUD_NOT_ACTIVE;

       //! MD has started a new Speech Session during deactivation of Speech Audio channel
	   //! Speech session status is not checked here to pass PCTS cases A11, A12, A14, A100, A101
	   //! In these PCTS tests, Voice session status is maintained active but Media and Guidance streams are played. Mic is closed.
	   //! Nevertheless, shouldn't cause an issue as the channel is requested for activation once Microphone is opened.
       if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])
       {
           ETG_TRACE_USR4(("Speech Session is active. Requesting for Activation of Speech Audio Channel"));
           vActivateChannel(enAudDir);
       }//if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])

   }//else if (e8AUD_VR_IN == enAudDir)

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1((" spi_tclAAPAudio::bFinalizeAudioPlayback left"));

   return true;

}//t_Bool spi_tclAAPAudio::bFinalizeAudioPlayback(t_U32 u32DeviceId)

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::vRegisterCallbacks()
 ***************************************************************************/
t_Void spi_tclAAPAudio::vRegisterCallbacks(trAudioCallbacks &rfrAudCallbacks)
{
   ETG_TRACE_USR1((" spi_tclAAPAudio::vRegisterCallbacks entered "));
   m_rAudCallbacks = rfrAudCallbacks;
}//t_Void spi_tclAAPAudio::vRegisterCallbacks(trAudioCallbacks &rfrAudCallbacks)

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::bStartAudio(t_U32,t_String,tenAudioLink)
 ***************************************************************************/
t_Bool spi_tclAAPAudio::bStartAudio(t_U32 u32DeviceId, t_String szAudioDev,
      tenAudioDir enAudDir)
{
   /*lint -esym(40,fvStartAudioResp)fvStartAudioResp Undeclared identifier */
   /*lint -esym(40,tfbSetAudioDucking)fbSetAudioDucking Undeclared identifier */

    m_oAudioStreamLock.s16Lock();

   ETG_TRACE_USR1((" spi_tclAAPAudio::bStartAudio() entered "));
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   ETG_TRACE_USR1(("spi_tclAAPAudio::bStartAudio entered for Audio Source %d with Audio Device - %s ",
            ETG_ENUM(AUDIO_DIRECTION, enAudDir), szAudioDev.c_str()));

   tenAAPAudStreamType enAudStreamType;
   t_Bool bRetVal = bGetAAPAudioStreamStype(enAudDir, enAudStreamType);

   if ((NULL != m_poCmdAudio) && (false == szAudioDev.empty()) && (true == bRetVal))
   {
      ETG_TRACE_USR4(("Current Stream State for Activated Audio Channel is %d",
               ETG_ENUM(AUDSTREAM_STATE, m_aenStreamState[enAudStreamType])));
      m_aenChannelStatus[enAudDir] = e8_AUD_ACT_GRANTED;

      //! Start playback only if the Audio Stream from MD is blocked for Activation of Audio channel
      //! Do nothing for cases like restoration of Audio channel by Audio manager where the MD may not have activated stream
      //! When playback start is received, request for Audio channel and SPI Audio Manager will mock the necessary response to complete sequence
      if (e8_AUD_STREAM_OPEN == m_aenStreamState[enAudStreamType])
      {
         ETG_TRACE_USR4(("Starting Audio playback for AAP Stream %d", ETG_ENUM(AUDSTREAM_TYPE, enAudStreamType)));
         m_aenStreamState[enAudStreamType] = e8_AUD_STREAMING;
         bRetVal = m_poCmdAudio->bPlaybackStarted(enAudStreamType);
      }//if (true == bGetAAPAudioStreamStype(enAudDir, enAudStreamType))

      if((NULL != m_rAudCallbacks.fbSetAudioDucking) && (e8AUD_DUCK == enAudDir))
      {
         m_rAudCallbacks.fbSetAudioDucking(scou16AudioRampDefDuration, scodAudioDuckDefdB,
                  e8_DUCKINGTYPE_DUCK,e8DEV_TYPE_ANDROIDAUTO);
      }//if((NULL != m_rAudCallbacks.fbSetAudioDucking) && (e8AUD_DUCK == enAudDir))

   }//if ((NULL != m_poCmdAudio) && (false == szAudioDev.empty()))

   //! In any case, Acknowledge Start of playback to SPI Audio Manager.
   if (NULL != m_rAudCallbacks.fvStartAudioResp)
   {
      (m_rAudCallbacks.fvStartAudioResp)(enAudDir, bRetVal);
   }//if (NULL != m_rAudCallbacks.fvStartAudioResp)

   //! If the device is already deselected, request for de-activation of Audio channel
   //! This has to be initiated only after sending an acknowledgement for Source Activity (On) to Audio Mananger

   if (0 == su32CurSelectedDevID)
   {
       ETG_TRACE_USR4(("Audio Channel Activated but Device is already deselected. Deactivate Audio Channel"));
       vDeactivateChannel(enAudDir);
   }//if(0 == su32CurSelectedDevID)

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1((" spi_tclAAPAudio::bStartAudio() left "));

   return bRetVal;
}//t_Bool bStartAudio(t_U32 u32DeviceId, tString szAudioDev, tenAudioLink enLink)


/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::bStartAudio(t_U32,t_String,tenAudioLink)
 ***************************************************************************/
t_Bool spi_tclAAPAudio::bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
         t_String szInputAudioDev, tenAudioDir enAudDir)
{
   /*lint -esym(40,fvStartAudioResp)fvStartAudioResp Undeclared identifier */

   m_oAudioStreamLock.s16Lock();
   ETG_TRACE_USR1((" spi_tclAAPAudio::bStartAudio() entered ")); 
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   t_Bool bRetVal = true;

   ETG_TRACE_USR1(("spi_tclAAPAudio::bStartAudio entered for Audio Source %d",
            ETG_ENUM(AUDIO_DIRECTION, enAudDir)));
   ETG_TRACE_USR4(("Audio Output Device szOutputAudioDev =%s ", szOutputAudioDev.c_str()));
   ETG_TRACE_USR4(("Audio Input Device szInputAudioDev =%s ", szInputAudioDev.c_str()));

   if ((NULL != m_poCmdAudio) &&
            (false == szOutputAudioDev.empty()) && (false == szInputAudioDev.empty())) //TODO
   {
      //! Check if Speech Audio channel is active. Not required to check if channel was requested.
      m_aenChannelStatus[enAudDir] = e8_AUD_ACT_GRANTED;

      if (e8AUD_VR_IN == enAudDir)
      {
         //! AAP MD handles VR through separate Upstream (Microphone, HU->MD) and Downstream (Guidance/Media, MD->HU) channels.
         //! Media channel from MD for VR Downstream audio is not supported currently.
         ETG_TRACE_USR4(("Speech Audio Channel Activated. Microphone Stream State - %d, Guidance Stream State - %d",
                  ETG_ENUM(AUDSTREAM_STATE, m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]),
                  ETG_ENUM(AUDSTREAM_STATE, m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])));

         //! Unblock Microphone if it was opened and blocked for Speech Audio channel activation to be completed.
         //! Start ECNR for Microphone Input as channel is now allocated.
         //! If Microphone is still closed, do nothing. Audio Device can be configured when Microphone is opened later.

         if((e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]) &&
                  (NULL != m_rAudCallbacks.fvStartAudioIn))
         {
            m_aenStreamState[e8_AUDIOTYPE_MICROPHONE] = e8_AUD_STREAMING;
            (m_rAudCallbacks.fvStartAudioIn)(e8AUD_VR_IN);
            ETG_TRACE_USR4(("ECNR Started. Unblocking Microphone Input"));
            bRetVal = (m_poCmdAudio->bMicRequestCompleted());
         }//if((e8_AUD_STREAM_OPEN ==....) && (NULL != ...))

         //! Unblock Guidance Stream if it was opened and blocked for Speech Audio channel activation to be completed.
         //! Ensure that Audio Device is set explicitly each time as Guidance stream is can be mapped to multiple HU Audio channels.
         //! If Guidance Stream is still closed, do nothing. Audio Device can be configured if Guidance is opened later.
         //! It is also possible that Guidance stream is already active and playing before Voice session was started. Nothing can be done.

         if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])
         {
            ETG_TRACE_USR4(("Audio Device Configured.Unblocking Guidance Stream"));
            m_aenStreamState[e8_AUDIOTYPE_GUIDANCE] = e8_AUD_STREAMING;
            m_poCmdAudio->vSetAudioStreamConfig(e8_AUDIOTYPE_GUIDANCE,
                     "alsa-main-audio", e8AUDIOCONFIG_MAINAUDIO_SPEECH);
            bRetVal = (m_poCmdAudio->bPlaybackStarted(e8_AUDIOTYPE_GUIDANCE));

         }//End of if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])

      }//End of if (e8AUD_VR_IN == enAudDir)

   }//End of if if ((NULL != m_poCmdAudio) &&...))

   //! Call the registered callbacks
   if (NULL != m_rAudCallbacks.fvStartAudioResp)
   {
      (m_rAudCallbacks.fvStartAudioResp)(enAudDir, bRetVal);
   }//End of if (NULL != m_rAudCallbacks.fvStartAudioResp)

   //! Special case to handle PCTS A12 where Media is played with Speech session status being active.
   //! If Media stream is open but Microphone and Guidance streams are closed, ignore voice session status.
   //! Request for deactivation of Speech Audio channel and activate Media Audio channel.
   //! Deactivation of Speech channel to be requested only after the current activation is acknowledged. 

   if((e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MEDIA]) && (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]) && 
	   (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]) && (e8AUD_VR_IN == enAudDir))
   {
	   ETG_TRACE_USR4(("Microphone and Guidance Stream closed. Media Stream is Open"));
      ETG_TRACE_USR4(("Deactivating Speech Audio Channel and Requesting for Audio Activation of Main Audio Channel"));

      vStopSpeechRecRelTimer();

      vDeactivateChannel(e8AUD_VR_IN);

      vActivateChannel(e8AUD_MAIN_OUT);

   }//if((e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MEDIA]) &&...)
   else if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MEDIA])
   {
	   ETG_TRACE_USR4(("Media Stream cannot be played on Media Audio channel as speech session is active"));
	   ETG_TRACE_USR4(("Unblock the Media Audio Stream."));
	   m_aenStreamState[e8_AUDIOTYPE_MEDIA] = e8_AUD_STREAMING;
      if (NULL != m_poCmdAudio)
      {
         bRetVal = (m_poCmdAudio->bPlaybackStarted(e8_AUDIOTYPE_MEDIA));
      }
   }//else if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MEDIA])


   //! If the device is already deselected, request for de-activation of Audio channel
   //! This has to be initiated only after sending an acknowledgement for Source Activity (On) to Audio Mananger

   if(0 == su32CurSelectedDevID)
   {
       ETG_TRACE_USR4(("Audio Channel Activated but Device is already deselected. Deactivate Audio Channel"));
       vDeactivateChannel(enAudDir);
   }//if(0 == su32CurSelectedDevID)

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1((" spi_tclAAPAudio::bStartAudio() left"));

   return bRetVal;
}//t_Bool bStartAudio(t_U32 u32DeviceId, tString szAudioDev, tenAudioLink enLink)

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::vStopAudio(t_U32)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceId);
   /*lint -esym(40,fvStopAudioResp) fvStopAudioResp is not referenced */
   /*lint -esym(40,fvStartAudioResp)fvStartAudioResp Undeclared identifier */
   
   m_oAudioStreamLock.s16Lock();
   ETG_TRACE_USR1((" spi_tclAAPAudio::vStopAudio() entered "));

   //! Call the registered callbacks
   if (NULL != m_rAudCallbacks.fvStopAudioResp)
   {
      (m_rAudCallbacks.fvStopAudioResp)(enAudDir, true);
   }//if (NULL != m_rAudCallbacks.fvStopAudioResp)

   ETG_TRACE_USR1(("spi_tclAAPAudio::vStopAudio u32DeviceId = %d enAudDir = %d  ",
            u32DeviceId, ETG_ENUM(AUDIO_DIRECTION, enAudDir)));

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1((" spi_tclAAPAudio::vStopAudio() left "));

}//t_Void vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::bSelectAudioDevice(t_U32)
 ***************************************************************************/
t_Bool spi_tclAAPAudio::bSelectAudioDevice(t_U32 u32DeviceId)
{
    
    /*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclAAPAudio::bSelectAudioDevice entered Device ID %d", u32DeviceId));
   t_Bool bRetVal = (NULL != m_poCmdAudio) ? (m_poCmdAudio->bCreateEndpoints()):(false);

   //! su32CurSelectedDevID will be updated to selected device on completion of Selection sequence

   //! Acknowldege Device Selector with response on creation of Audio Endpoints
   if (NULL != m_rAudCallbacks.fvSelectDeviceResp)
   {
      (m_rAudCallbacks.fvSelectDeviceResp)(u32DeviceId, e8DEVCONNREQ_SELECT, bRetVal);

   }//if (NULL != m_rAudCallbacks.fvSelectDeviceResp)

   return bRetVal;
}//t_Bool spi_tclAAPAudio::bSelectAudioDevice(t_U32)

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::vDeselectAudioDevice(t_U32)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vDeselectAudioDevice(t_U32 u32DeviceId)
{
   /*lint -esym(40,fvSelectDeviceResp)fvSelectDeviceResp Undeclared identifier */

   m_oAudioStreamLock.s16Lock();
   ETG_TRACE_USR1((" spi_tclAAPAudio::vDeselectAudioDevice entered Device ID %d", u32DeviceId));

   vStopSpeechRecRelTimer();

   //! Endpoints are destroyed here because it should be before GAL receiver is destroyed.
   if(NULL != m_poCmdAudio)
   {
       m_poCmdAudio->vDestroyEndpoints();
   }//if(NULL != m_poCmdAudio)
   su32CurSelectedDevID = 0;
   
   //! Acknowldege Device Selector with response on destruction of Audio Endpoints
   if (NULL != m_rAudCallbacks.fvSelectDeviceResp)
   {
      (m_rAudCallbacks.fvSelectDeviceResp)(u32DeviceId, e8DEVCONNREQ_DESELECT, true);

   }//if (NULL != m_rAudCallbacks.fvSelectDeviceResp)

   //! Deactivate Audio Channels if any active. If the channel is requested to be deactivated, no action required.
   //! If the Audio activation request was made, check for the selected device after the channel is activated.

   if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_DUCK])
   {
       ETG_TRACE_USR1(("Device Deselected. Requesting Mix Audio Channel Deactivation"));
       vDeactivateChannel(e8AUD_DUCK);
   }//if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_DUCK])

   if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN])
   {
       ETG_TRACE_USR1(("Device Deselected. Requesting Speech Audio Channel Deactivation"));
       vDeactivateChannel(e8AUD_VR_IN);
   }//if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN])

   //! Deactivation of main audio channel is required regardless of channel state since it can be in suspended state
   ETG_TRACE_USR1(("Device Deselected. Requesting Main Audio Channel Deactivation"));
   vDeactivateChannel(e8AUD_MAIN_OUT);

   sbVoiceSessionActive = false;

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1((" spi_tclAAPAudio::vDeselectAudioDevice() left"));

}//t_Void spi_tclAAPAudio::bDeselectAudioDevice(t_U32 u32DeviceId)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vProcessMediaStart(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vProcessMediaStart(tenAudioDir enAudioDirection)
{
   m_oAudioStreamLock.s16Lock();

   ETG_TRACE_USR1(("spi_tclAAPAudio::vProcessMediaStart() entered"));

   //! Speech Channel is not active or deactivation has already been requested.
   //! Request for activation of Media channel.
   ETG_TRACE_USR4(("Voice Session State - %d, Microphone Stream State - %d, Speech Channel Status - %d, Media Channel State- %d",
            ETG_ENUM(BOOL, sbVoiceSessionActive), ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]),
            ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[e8AUD_VR_IN]),ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[enAudioDirection])));

   if((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN])||(e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]))
   {
      //Activate Media channel
      ETG_TRACE_USR4(("Speech Audio Channel not active. Requesting Activation of Media Audio Channel"));
      vActivateChannel(enAudioDirection);
   }//if((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN])||...)

   //! Case 1: Voice Session has completely ended. Microphone closed, Guidance stream stopped and MD Voice session status is Inactive.
   //! Case 2: Microphone is closed & MD Voice Session status is Inactive. Guidance stream is still active.
   //! In either case, Deactivate Speech Audio channel and Request for activation of Media channel.
   //! There may be a possibility of downstream audio on Guidance stream being truncated but then it MD's decision to start playback on Media.
   //! Due to this reason, we do not consider the state of Guidance stream to activate the Media channel
   //! If the MD is forced to play only on Guidance Channel at start of Voice session, Media playback may not start without change in audio focus.

   else if((false == sbVoiceSessionActive) && (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])
            && (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]))
   {
      ETG_TRACE_USR4(("Speech Audio Channel Active. Voice Session Inactive. Microphone closed. Guidance Stream State - %d",
               ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])));
      ETG_TRACE_USR4(("Deactivating Speech Channel and Requesting Activation of Media Audio Channel"));

      //! Stop the timer
      vStopSpeechRecRelTimer();

      //! De-Activate Speech Audio channel
      vDeactivateChannel(e8AUD_VR_IN);

      //! Request for Activation of Media Audio channel
      vActivateChannel(e8AUD_MAIN_OUT);

   }//else if((false == sbVoiceSessionActive) &&....))

   //! Special case to handle PCTS - A14, A100, A101
   //! If Microphone is closed and Guidance stream is not active, request for deactivation of Speech Audio channel
   //! In case of Guidance stream running, Speech channel is not deactivated to prevent audio playback truncation.
   //! Request for Activation of Media Audio Stream
   else if ((e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])&&
            (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]) &&
            (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]))
   {
      ETG_TRACE_USR4(("Guidance and Microphone Streams are closed. Media playback start received.Speech Audio channel is still active."));
      ETG_TRACE_USR4(("Deactivate Speech Audio channel. Request for activation of Media Audio channel"));

      //! Stop the timer
      vStopSpeechRecRelTimer();

      //! De-Activate Speech Audio channel
      vDeactivateChannel(e8AUD_VR_IN);

      //! Request for Activation of Media Audio channel
      vActivateChannel(e8AUD_MAIN_OUT);

   }//else if ((e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])&&...)

  //! Special case to handle PCTS A12
  //! Microphone is closed and Guidance stream is not active but speech channel has been requested for activation
  //! Wait for Speech Audio channel to be activated and evaluate the same condition to assess if Media Audio channel can be activated
  //! To request for Media Audio channel, Media stream should be open and Mic & Guidance stream must be closed. Voice session can be ignored.
  else if ((e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])&&
            (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]) &&
            (e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]))
  {
      ETG_TRACE_USR4(("Guidance and Microphone Streams are closed. Media playback start received.Speech Audio channel activation is in progress."));
      ETG_TRACE_USR4(("Evaluate the Audio stream condition again after Speech Channel activation is completed"));

  }//else if ((e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])&&...)

   //! Speech channel activation has been requested but yet to be granted. Now, we have request to start streaming Media from MD.
   //! Scenario may be valid as the Microphone (Open/Close)/Voice Session Status and playback start(Guidance/Media) callbacks could be from different threads.
   //! Here, we can only unblock Media Stream without requesting for activation since Audio activation would fail due to ongoing speech audio channel activation
   else if((NULL != m_poCmdAudio) && (e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]))
   {
      ETG_TRACE_USR4(("Speech Audio Channel activation request in progress. Cannot activate Media Audio Channel"));
      ETG_TRACE_USR4(("Unblocking Media Stream without activating Media Audio channel. Expect MD to terminate stream"));
      m_aenStreamState[e8_AUDIOTYPE_MEDIA] = e8_AUD_STREAMING;
      m_poCmdAudio->bPlaybackStarted(e8_AUDIOTYPE_MEDIA);

   }//else if((NULL != m_poCmdAudio) && (e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]))

   //! Speech Audio channel is active and Guidance audio is played as downstream in Voice session while Microphone is closed.
   //! Deactivation of Speech Audio channel and activation of Media Audio channel cannot is not done to prevent truncation of Guidance stream playback.
   else if((e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]) && (e8_AUD_STREAMING  == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])
          &&(e8_AUD_STREAM_CLOSED  == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]))
   {
      ETG_TRACE_USR4(("Speech Audio Channel is activated. Cannot activate Media Audio Channel now"));
      ETG_TRACE_USR4(("Media Audio channel to be requested for activation at end of Speech Session"));
   }//else if((e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]) &&...)

   //! Media stream is opened while Microphone is open and Speech Audio channel is active. (PCTS A14,A100,A101,A116,A117 cases)
   //! Deactivation of Speech Audio channel and activation of Media Audio channel is not done because user input is being sent via upstream audio.
   else if((e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]) && (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]))
   {
      ETG_TRACE_USR4(("Microphone is open and Speech Audio Channel is active. Cannot activate Media Audio Channel now"));
      ETG_TRACE_USR4(("Media Audio channel to be requested for activation at end of Speech Session"));
   }

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vProcessMediaStart() left"));

}// t_Void spi_tclAAPAudio::vProcessMediaStart(tenAudioDir enAudioDirection)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vProcessGuidanceStart(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vProcessGuidanceStart(tenAudioDir enAudioDirection)
{
    m_oAudioStreamLock.s16Lock();

   ETG_TRACE_USR1(("spi_tclAAPAudio::vProcessGuidanceStart() entered"));

   if(NULL != m_poCmdAudio)
   {
      ETG_TRACE_USR4(("Voice Session State - %d, Microphone Stream State - %d, Speech Channel Status - %d, Guidance Stream State - %d, Guidance Channel State - %d",
               ETG_ENUM(BOOL, sbVoiceSessionActive), ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]),
               ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[e8AUD_VR_IN]), ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]),
               ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[enAudioDirection])));

      //! Speech Session is active and Audio activation for Speech channel is granted.
      //! If Guidance Stream is open, Set the Audio device configuration and Unblock to start playback
      if ((true == IS_SPEECH_SESSION_ACTIVE()) && (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]) &&
               (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]))
      {
         //!Stop the timer
         vStopSpeechRecRelTimer();

         ETG_TRACE_USR4(("Speech Audio Channel is active. Unblock Guidance Start.."));

         m_aenStreamState[e8_AUDIOTYPE_GUIDANCE] = e8_AUD_STREAMING;

         //! Set the Audio device name for Guidance channel corresponding to the output device for Speech Audio Source.
         m_poCmdAudio->vSetAudioStreamConfig(e8_AUDIOTYPE_GUIDANCE, "alsa-main-audio", e8AUDIOCONFIG_MAINAUDIO_SPEECH);

         //! Unblock the Guidance stream to start playback of Voice Rec Downstream Audio
         m_poCmdAudio->bPlaybackStarted(e8_AUDIOTYPE_GUIDANCE);

      }//if ((true == IS_SPEECH_SESSION_ACTIVE()) &&...)

      //! Speech Session is active and Audio activation for Speech channel is requested.
      //! Do nothing here. Activation of Speech Audio channel will unblock the Guidance stream.
      else if((true == IS_SPEECH_SESSION_ACTIVE()) && (e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]) &&
               (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]))
      {
         ETG_TRACE_USR4(("Speech Audio Channel Activation is requested. Wait to Unblock Guidance Stream"));
      }//else if((true == IS_SPEECH_SESSION_ACTIVE()) && (e8_AUD_ACT_REQUESTED ==...)

      //! Speech Session is not active.
      else if ((false == sbVoiceSessionActive) && (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]) &&
               (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]))
      {
         ETG_TRACE_USR4(("Speech Session has ended. "));

         //! Deactivate the Speech Audio channel if it is still active.
         if (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN])
         {
            ETG_TRACE_USR4(("Speech Audio Channel is still active. Deactivate Speech Audio Channel.."));

            //!Stop the timer
            vStopSpeechRecRelTimer();

            //! Request for Deactivation of Speech Audio channel
            vDeactivateChannel(e8AUD_VR_IN);
         }

         //! Speech Audio channel activation is not in progress.
         //! Request for Activation of Audio channel to play Guidance Stream.
         if (e8_AUD_ACT_REQUESTED != m_aenChannelStatus[e8AUD_VR_IN])
         {
            vActivateChannel(enAudioDirection);
         }//if (e8_AUD_ACT_REQUESTED != m_aenChannelStatus[e8AUD_VR_IN])
         else
         {
            ETG_TRACE_USR4(("Speech Audio channel activation is in progress. Guidance stream is open. "
                  "Wait for audio activation to unblock guidance stream."));
         }
      }//else if ((false == sbVoiceSessionActive) &&...)

      //! Scenario to handle PCTS A11. Speech Channel has been deactivated due to Audio Focus release
      //! Speech session status is still active but Microphone has been closed.
      //! If Speech Audio channel is already deactivated or requested for deactivation, request for activation of Guidance Audio channel
      //! If Speech Audio channel has been requested for Activation, Guidance Stream will be played as downstream of speech audio channel
      else if (((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN])||(e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN])) &&
                     (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]))
      {
         ETG_TRACE_USR4(("Speech Audio Channel is deactivated/requested for deactivation. "));
         vActivateChannel(enAudioDirection);

      }//else if (((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN])||...)

      else if(e8_AUD_STREAMING == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])
      {
         ETG_TRACE_USR4(("Guidance Stream is already playing on Audio Channel"));
      }//else if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])
   }//End of if((NULL != m_poCmdAudio) && ...

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vProcessGuidanceStart() left"));

}// t_Void spi_tclAAPAudio::vProcessGuidanceStart(tenAudioDir enAudioDirection)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vActivateChannel(tenAudioDir enAudioDirection)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vActivateChannel(tenAudioDir enAudioDir)
{
   /*lint -esym(40,fvLaunchAudioReq)fvLaunchAudioReq Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAAPAudio::vActivateChannel() entered for Audio direction %d ",
         ETG_ENUM(AUDIO_DIRECTION, enAudioDir)));

   //! Evaluate Guidance Channel status to Request for Activation of Audio channel to play Guidance Stream.
   if ((NULL != m_poCmdAudio) && (NULL != m_rAudCallbacks.fvLaunchAudioReq))
   {
      switch (enAudioDir)
      {
         case e8AUD_MAIN_OUT:
         {
            m_aenChannelStatus[enAudioDir] = e8_AUD_ACT_REQUESTED;
            (m_rAudCallbacks.fvLaunchAudioReq)(su32CurSelectedDevID, e8DEV_TYPE_ANDROIDAUTO, e8AUD_MAIN_OUT,
                  e8AUD_SAMPLERATE_DEFAULT);
         }//case e8AUD_MAIN_OUT:
            break;
         case e8AUD_VR_IN:
         {
            m_aenChannelStatus[enAudioDir] = e8_AUD_ACT_REQUESTED;
            (m_rAudCallbacks.fvLaunchAudioReq)(su32CurSelectedDevID, e8DEV_TYPE_ANDROIDAUTO, e8AUD_VR_IN,
                  e8AUD_SAMPLERATE_16KHZ);
         }//case e8AUD_VR_IN:
            break;
         case e8AUD_DUCK:
         {
            //! Request for Activation of Guidance Audio channel if it is not active
            if (e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_DUCK])
            {
               ETG_TRACE_USR4(("Request for Activation of Guidance Audio Channel"));

               m_aenChannelStatus[e8AUD_DUCK] = e8_AUD_ACT_REQUESTED;
               //! Set the Audio device name for Guidance channel corresponding to the output device for Mix Audio Source.
               m_poCmdAudio->vSetAudioStreamConfig(e8_AUDIOTYPE_GUIDANCE, "alsa-main-audio", e8AUDIOCONFIG_ALTERNATEAUDIO);
               (m_rAudCallbacks.fvLaunchAudioReq)(su32CurSelectedDevID, e8DEV_TYPE_ANDROIDAUTO, e8AUD_DUCK,
                        e8AUD_SAMPLERATE_DEFAULT);
            }
            //! If Guidance audio channel is already active, unblock Guidance PlaybackStart
            else if (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_DUCK])
            {
               ETG_TRACE_USR4(("Guidance Audio Channel is active. Unblocking Guidance Start.."));
               //! Set the Audio device name for Guidance channel corresponding to the output device for Mix Audio Source.
               m_poCmdAudio->vSetAudioStreamConfig(e8_AUDIOTYPE_GUIDANCE, "alsa-main-audio", e8AUDIOCONFIG_ALTERNATEAUDIO);
               //! Unblock the Guidance stream
               m_aenStreamState[e8_AUDIOTYPE_GUIDANCE] = e8_AUD_STREAMING;
               m_poCmdAudio->bPlaybackStarted(e8_AUDIOTYPE_GUIDANCE);
            }
            //! If Guidance audio channel is being activated/deactivated, wait for it to complete & reevaluate.
            else
            {
               ETG_TRACE_USR4(("Guidance Audio Channel activation/deactivation is in progress. "));
               ETG_TRACE_USR4(("Evaluate the Audio stream condition again after Guidance Channel activation/deactivation is completed "));
            }
         }//case e8AUD_DUCK:
            break;
         default:
         {
            ETG_TRACE_ERR(("No action taken for invalid audio direction %d ", ETG_ENUM(AUDIO_DIRECTION, enAudioDir)));
         }//default:
            break;
      }//switch (enAudioDir)
   }//if ((NULL != m_poCmdAudio) && (NULL != m_rAudCallbacks.fvLaunchAudioReq))
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vDeactivateChannel(tenAudioDir enAudioDirection)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vDeactivateChannel(tenAudioDir enAudioDirection)
{
   /*lint -esym(40,fvTerminateAudioReq)fvTerminateAudioReq Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAAPAudio::vDeactivateChannel() entered for Audio direction %d ",
         ETG_ENUM(AUDIO_DIRECTION, enAudioDirection)));

   if (NULL != m_rAudCallbacks.fvTerminateAudioReq)
   {
      m_aenChannelStatus[enAudioDirection] = e8_AUD_DEACT_REQUESTED;
      (m_rAudCallbacks.fvTerminateAudioReq)(su32CurSelectedDevID, enAudioDirection);
   }//if (NULL != m_rAudCallbacks.fvTerminateAudioReq)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vOnAudioError(tenAudioDir, tenAudioError)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError)
{
   SPI_INTENTIONALLY_UNUSED(enAudioError);

   m_oAudioStreamLock.s16Lock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vOnAudioError() entered"));

   tenAAPAudStreamType enAudStreamType;
   t_Bool bResult = bGetAAPAudioStreamStype(enAudDir, enAudStreamType);

   ETG_TRACE_USR1(("spi_tclAAPAudio::vOnAudioError entered for Audio Source - %d and AAP Stream - %d",
            ETG_ENUM(AUDIO_DIRECTION, enAudDir), ETG_ENUM(AUDSTREAM_TYPE, enAudStreamType)));

   //! Failure to activate channel could be due to Audio Manager processing request for higher prio source.
   //! Could occur in case of timing issues in Audio activation requests from different components.

   if((true == bResult) && (NULL != m_poCmdAudio))
   {
      ETG_TRACE_USR4(("Current State of AAP Audio Stream %d is %d",
               ETG_ENUM(AUDSTREAM_TYPE, enAudStreamType),ETG_ENUM(AUDSTREAM_STATE, m_aenStreamState[enAudStreamType])));
      m_aenChannelStatus[enAudDir] = e8_AUD_NOT_ACTIVE; 

      //! CASE 1: Media Channel Activation Error
      //! Failure to activate Media could be due to Audio Manager processing request for higher prio source.
      //! 1a: Other components requesting source: Not an issue. Focus management will subsequently notify the change in Audio ownership.
      //! 1b: MD has started speech session
      //!     i)  Assumption: Playback start, Voice Session Notification and Mic Callback handled from same thread of AAP Stack
      //!         Not an issue. Another callback cannot be expected until Media playback start is unblocked.
      //!     ii) Assumption: Playback start & Mic Callback/ Voice Session Notification from different thread contexts of AAP Stack
      //!         On start of Voice session, MD to be notified that it can play only on Guidance channel.
      //! Unblock from the wait to start Media playback. Playback Stop can be expected from MD in quick time for 1a, 1b(ii) .
      if((e8_AUDIOTYPE_MEDIA == enAudStreamType) &&
               (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MEDIA]))
      {
         ETG_TRACE_USR4(("Media Audio Channel Activation Error. Unblocking Start Playback"));
         m_aenStreamState[e8_AUDIOTYPE_MEDIA] = e8_AUD_STREAMING;
         bResult = m_poCmdAudio->bPlaybackStarted(enAudStreamType);

      }//if((e8_AUDIOTYPE_MEDIA == enAudStreamType) &&...)

      //! CASE 2: Guidance (Mix) Channel Activation Error
      //! Failure to activate Guidance (Mix) could be due to Audio Manager processing request for other higher prio source.
      //! 2a: Other components requesting source: No issue. Focus management will notify change in Audio ownership.
      //! 2b: MD has started speech session. Guidance used for downstream. Activation of Speech channel requested causing failure to activate Guidance.
      else if(e8_AUDIOTYPE_GUIDANCE == enAudStreamType)
      {
         //! If Speech Session has been initiated by MD, do nothing here.
         //! Activation of Speech channel later will ensure the correct audio device is configured and Guidance playback start is unblocked/
         //! If Speech Session is inactive but Guidance stream is blocked for Audio activation, just set the dummy audio device
         if((false == IS_SPEECH_SESSION_ACTIVE()) &&
                  (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]))
         {
            ETG_TRACE_USR4(("Guidance Audio Channel Activation Error. Unblocking Start Playback"));
            m_aenStreamState[e8_AUDIOTYPE_GUIDANCE] = e8_AUD_STREAMING;
            m_poCmdAudio->vSetAudioStreamConfig(e8_AUDIOTYPE_GUIDANCE,
                                        "alsa-main-audio", e8AUDIOCONFIG_MAINAUDIO_DUMMY);
            m_poCmdAudio->bPlaybackStarted(e8_AUDIOTYPE_GUIDANCE);
         }//if(false == IS_SPEECH_SESSION_ACTIVE()) && (...))
         else if((true == IS_SPEECH_SESSION_ACTIVE()) &&
                  (e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]))
         {
            ETG_TRACE_USR4(("Guidance Audio Channel Activation Error. Speech Session Activation requested. Wait.."));
         }
      }////else if(true == IS_SPEECH_SESSION_ACTIVE()) && (...))
   }//if((true = bResult) && (NULL ! = m_poCmdAudio))

   //! CASE 3: Speech Channel Activation Error
   //! Failure to activate Speech Channel could be due to Audio manager processing request for other higher prio source.
   //! Foreseen only in case of timing issues in audio activation requests from different components
   //! Ensure that all active streams for which playback start was received is just unblocked.
   //! Focus management will notify change in Audio ownership. Voice session will be ended by MD.

   //! Voice Recognition Audio Channel is not mapped to AAP Audio Stream Types
   //! AAP Handles Upstream and Downstream as simplex channels.
   //! From Audio Management perspective, Speech channel is duplex.
   if((false == bResult) && (e8AUD_VR_IN == enAudDir) && (NULL != m_poCmdAudio))
   {
      //! Speech Channel activation failed. Clear the channel activation state.
      //! Just to ensure that the channel can be requested again if speech session is ended and started again by MD
      m_aenChannelStatus[e8AUD_VR_IN] = e8_AUD_NOT_ACTIVE;

      ETG_TRACE_USR4(("Speech Audio Channel Activation Failure. Check for Open MD Streams"));
      //! Microphone was opened. Unblock the call.
      if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])
      {
         //! NOTE!! - ECNR is not started here. Audio channel is activation is unsucessful.
         ETG_TRACE_USR4(("Speech Audio Activaton Failure - Unblocking Microphone(Open)"));
         m_aenStreamState[e8_AUDIOTYPE_MICROPHONE] = e8_AUD_STREAMING;
         m_poCmdAudio->bMicRequestCompleted();
      }//if(e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])

      //! Guidance was active and blocked for device to be configured.
      if((e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]))
      {
         ETG_TRACE_USR4(("Speech Audio Activaton Failure - Unblocking Guidance Stream"));
         m_aenStreamState[e8_AUDIOTYPE_GUIDANCE] = e8_AUD_STREAMING;

         //! Provide dummy audio device if Mix channel is not active and Unblock Guidance stream.
         if ((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_DUCK]) || (e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_DUCK]))
         {
            m_poCmdAudio->vSetAudioStreamConfig(e8_AUDIOTYPE_GUIDANCE,
                                    "alsa-main-audio", e8AUDIOCONFIG_MAINAUDIO_DUMMY);
         }
         m_poCmdAudio->bPlaybackStarted(e8_AUDIOTYPE_GUIDANCE);
      }// if((e8_AUD_STREAM_OPEN == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE])

   }//if((false = bResult) && (e8AUD_VR_IN == enAudDir) && (NULL != m_poCmdAudio))

   //! Any more error cases ? Please list them below even if not handled for now.
   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vOnAudioError() left"));

}//spi_tclAAPAudio::vOnAudioError(tenAudioDir, tenAudioError)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vPlaybackStartCb(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vPlaybackStartCb(tenAAPAudStreamType enAudStreamType,
      t_S32 s32SessionID)
{
   m_oAudioStreamLock.s16Lock();
   tenAudioDir enAudioDir = e8AUD_INVALID;

   ETG_TRACE_USR1(("spi_tclAAPAudio::vPlaybackStartCb() entered for Audio StreamType %d, SessionID %d",
            ETG_ENUM(AUDSTREAM_TYPE, enAudStreamType), s32SessionID));

   ETG_TRACE_USR4(("Voice Session State - %d, Microphone Stream State - %d, Speech Channel Status - %d, Guidance Channel Status - %d",
            ETG_ENUM(BOOL, sbVoiceSessionActive), ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]),
            ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[e8AUD_VR_IN]), ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8AUD_DUCK])));

   ETG_TRACE_USR4(("Media Stream State - %d, Media Channel Status - %d",
            ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MEDIA]),
            ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8_AUDIOTYPE_MEDIA])));

   t_Bool bRetVal = bGetAudioDir(enAudStreamType, enAudioDir);

   if(true == bRetVal)
   {
      m_aenStreamState[enAudStreamType] = e8_AUD_STREAM_OPEN;
      ETG_TRACE_USR4(("Updated %d Stream State to %d",ETG_ENUM(AUDSTREAM_TYPE, enAudStreamType),
               ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[enAudStreamType])));

      //! Check if the Audio channel deactivation is in progress for stream being opened by MD.
      //! Defer the request to activate the Audio channel until the deactivation is completed.
      if((e8_AUDIOTYPE_MEDIA == enAudStreamType) && (e8_AUD_DEACT_REQUESTED != m_aenChannelStatus[enAudioDir]))
      {
          vProcessMediaStart(enAudioDir);
      } //if((e8_AUDIOTYPE_MEDIA == enAudStreamType) && ((e8_AUD_DEACT_REQUESTED !=..))

      //! Guidance Audio channel status is not checked here since it needs to be evaluated in context of Mic Stream, Speech Audio channel and 
      //! Voice Session status. This is handled in vProcessGuidanceStart().
      else if(e8_AUDIOTYPE_GUIDANCE == enAudStreamType)
      {
          vProcessGuidanceStart(enAudioDir);
      }//else if(e8_AUDIOTYPE_GUIDANCE == enAudStreamType)

   }//if(true == bRetVal)

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vPlaybackStartCb() left"));

}//t_Void spi_tclAAPAudio::vPlaybackStartCb(tenAAPAudStreamType enAudStreamType,...)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vPlaybackStopCb(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vPlaybackStopCb(tenAAPAudStreamType enAudStreamType,
      t_S32 s32SessionID)
{
    m_oAudioStreamLock.s16Lock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vPlaybackStopCb Entered: AudStreamType %d, SessionID %d",
            ETG_ENUM(AUDSTREAM_TYPE, enAudStreamType), s32SessionID));

   ETG_TRACE_USR4(("Voice Session State - %d, Microphone Stream State - %d, Speech Channel Status - %d, Guidance Channel Status - %d",
            ETG_ENUM(BOOL, sbVoiceSessionActive), ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]),
            ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[e8AUD_VR_IN]),ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8AUD_DUCK])));

   ETG_TRACE_USR4(("Media Stream State - %d, Media Channel Status - %d",
            ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MEDIA]),
            ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8_AUDIOTYPE_MEDIA])));

   tenAudioDir enAudioDir = e8AUD_INVALID;
   t_Bool bRetVal = bGetAudioDir(enAudStreamType, enAudioDir);

   if(true == bRetVal)
   {
      m_aenStreamState[enAudStreamType] = e8_AUD_STREAM_CLOSED;
      ETG_TRACE_USR4(("Updated %d Stream State to %d",ETG_ENUM(AUDSTREAM_TYPE, enAudStreamType),
               ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[enAudStreamType])));

      //! Media Stream playback stopped. Not required to deactivate audio channel.
      if(e8_AUDIOTYPE_MEDIA == enAudStreamType)
      {
         ETG_TRACE_USR4(("Media Stream playback stopped. Audio channel is not required to be deactivated"));
      }//if(e8_AUDIOTYPE_MEDIA == enAudStreamType)

      //! Guidance Stream playback stopped.
      //! Speech Session is active. Speech channel is already activated or requested for activation.
      //! If Guidance Audio stream was played on Guidance/Mix Audio Channel, request for deactivation of Audio channel.
      //! In case, the Guidance/Mix Audio channel was already deactivated, SPI Audio Manager will not forward the request to Audio Manager
      //! However, If Guidance Audio stream was played as downstream audio of Speech Audio channel, do action required.

      else if((e8_AUDIOTYPE_GUIDANCE == enAudStreamType) && (true == IS_SPEECH_SESSION_ACTIVE())
              && (e8_AUD_ACT_GRANTED == m_aenChannelStatus[enAudioDir]))
      {
         ETG_TRACE_USR4(("Guidance Stream playback stopped. Speech Session is active. Deactivate Guidance/Mix Audio channel"));
         vDeactivateChannel(enAudioDir);

      }//if((e8_AUDIOTYPE_GUIDANCE == enAudStreamType) && (true == IS_SPEECH_SESSION_ACTIVE()))

      //! Guidance Stream playback stopped.
      //! Speech Session is not active. Speech channel is still active.
      //! Guidance may have played as downstream audio on Speech Audio Channel or Guidance/Mix Audio Channel
      //! In any case, request for deactivation of Guidance/Mix audio channel. SPI Audio Manager will not forward the request to Audio Manager if deactivated.
      //! Start the timer to hold on to Speech Audio channel, waiting for other Audio stream to be setup or Audio Focus to be released

      else if((e8_AUDIOTYPE_GUIDANCE == enAudStreamType) && (false == IS_SPEECH_SESSION_ACTIVE()) &&
               (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]))
      {
         ETG_TRACE_USR4(("Guidance Stream playback stopped. Speech Session is inactive. Speech Audio channel is active."));

         if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[enAudioDir])
         {
             ETG_TRACE_USR4(("Deactivating Mix Audio Channel that was active during Speech Session"));
             vDeactivateChannel(enAudioDir);
         }//if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[enAudioDir])

         vStartSpeechRecRelTimer();

      }//if((e8_AUDIOTYPE_GUIDANCE == enAudStreamType) && (false == IS_SPEECH_SESSION_ACTIVE()) &&...)

      //! Guidance Stream playback stopped.
      //! Speech Session has ended. Speech Audio channel is not active. Request for deactivation of Guidance/Mix Audio channel
      else if((e8_AUDIOTYPE_GUIDANCE == enAudStreamType) && (false == IS_SPEECH_SESSION_ACTIVE()) &&
               ((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN])||(e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN])))
      {
         ETG_TRACE_USR4(("Guidance Stream playback stopped. Speech Session has ended. Speech Audio channel is not active. Deactivate Guidance Audio channel"));

         if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[enAudioDir])
         {
             ETG_TRACE_USR4(("Deactivating Mix Audio Channel"));
             vDeactivateChannel(enAudioDir);
         }//if(e8_AUD_ACT_GRANTED == m_aenChannelStatus[enAudioDir])

      } //else if((e8_AUDIOTYPE_GUIDANCE == enAudStreamType) && (false == IS_SPEECH_SESSION_ACTIVE()) && ((e8_AUD_NOT_ACTIVE ==...)))

      //! Guidance Stream Playback stopped. Microphone is closed.
      //! Voice Session is ACTIVE. Media stream is open and Speech channel is activated.
      //! Start the timer to hold on to Speech Audio channel, waiting for other Audio stream to be setup.
      else if ((e8_AUDIOTYPE_GUIDANCE == enAudStreamType) && (true == sbVoiceSessionActive) &&
            (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]) &&
            (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]) &&
            (e8_AUD_STREAMING == m_aenStreamState[e8_AUDIOTYPE_MEDIA]))
      {
         ETG_TRACE_USR4(("Guidance Stream playback stopped. Speech Session has ended. Speech Audio channel is still active.."));
         vStartSpeechRecRelTimer();
      }
   }//if(true == bRetVal)

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vPlaybackStopCb() left"));

}//t_Void spi_tclAAPAudio::vPlaybackStopCb(tenAAPAudStreamType enAudStreamType,...)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vMicRequestCb(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vMicRequestCb(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled, t_Bool bEchoCancellationEnabled,
         t_S32 s32MaxUnacked)
{
    m_oAudioStreamLock.s16Lock();

   ETG_TRACE_USR1(("spi_tclAAPAudio:vMicRequestCb Entered: MicOpen %d, NREnabled %d, ECEnabled %d, UnackedFrames %d ",
            ETG_ENUM(BOOL, bMicOpen), ETG_ENUM(BOOL, bNoiseReductionEnabled),
            ETG_ENUM(BOOL, bEchoCancellationEnabled), s32MaxUnacked));

   ETG_TRACE_USR4(("Voice Session State - %d, Speech Channel Status - %d, Guidance Stream State - %d, Guidance Channel State - %d",
            ETG_ENUM(BOOL, sbVoiceSessionActive), ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[e8AUD_VR_IN]),
            ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]),ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8AUD_DUCK])));

   ETG_TRACE_USR4(("Media Stream State - %d, Media Channel Status - %d",
            ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MEDIA]),
            ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8_AUDIOTYPE_MEDIA])));

   //! Update the Audio Stream State for Microphone
   m_aenStreamState[e8_AUDIOTYPE_MICROPHONE] = (bMicOpen) ?(e8_AUD_STREAM_OPEN):(e8_AUD_STREAM_CLOSED);
   ETG_TRACE_USR4(("Updated Microphone Stream State %d",ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])));

   //! If Microphone is Opened and Speech Audio channel activation is not requested, request for Activation of Speech channel
   if((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN]) && (true == bMicOpen))
   {
      ETG_TRACE_USR4(("Microphone Opened. Request for Activation of Speech Audio Channel"));

      vStopSpeechRecRelTimer();

      vActivateChannel(e8AUD_VR_IN);

   }//if((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN]) && (true == bMicOpen))

   // Speech Audio Channel Activation has been requested. No action required here.
   // Upon Activation of Speech Audio Channel, Microphone stream will be unblocked.
   else if((e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]) && (true == bMicOpen))
   {
      ETG_TRACE_USR4(("Speech Audio Channel activation in progress. Microphone Opened. Wait for Audio Activation to Unblock Microphone Audio"));

   }//else if((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN]) && (true == bMicOpen))

   //! NOTE !! - If Speech Audio Channel deactivation is in progress, need to request Activation upon completion of earlier request - NOT HANDLED
   //! Request for Activation of Audio channel during processing of Deactivation request of same channel leads to failure.
   else if((e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]) && (true == bMicOpen))
   {
      ETG_TRACE_USR4(("Speech Audio Channel deactivation in progress. Speech Audio channel activation not requested. Scenario is not handled !!"));
   }//else if((e8_AUD_DEACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN]) && (true == bMicOpen))

   //! Speech Audio Channel is already active. Complete the Microphone Request. Update the Stream state.
   else if((NULL != m_poCmdAudio) && (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]))
   {
      m_aenStreamState[e8_AUDIOTYPE_MICROPHONE] = (bMicOpen) ? (e8_AUD_STREAMING): (e8_AUD_STREAM_CLOSED);
      ETG_TRACE_USR4(("Updated Microphone Stream State %d",ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])));
      m_poCmdAudio->bMicRequestCompleted();

      //! Start and Stop ECNR on receiving microphone request as the ECNR for Android
      //! Auto will be used only for microphone Gain control and there can be multiple requests
      //! to turn on and Off microphone during a voice session
      if ((NULL != m_rAudCallbacks.fvStartAudioIn) && (NULL != m_rAudCallbacks.fvStopAudioIn))
      {
         if(true == bMicOpen)
         {
            ETG_TRACE_USR4(("Microphone Opened. Speech Audio Channel Active. Start ECNR"));
            vStopSpeechRecRelTimer();
            (m_rAudCallbacks.fvStartAudioIn)(e8AUD_VR_IN);
         } // if(true == bMicOpen)
         else
         {
            ETG_TRACE_USR4(("Microphone Closed. Speech Audio Channel Active. Stop ECNR"));
            (m_rAudCallbacks.fvStopAudioIn)(e8AUD_VR_IN);

            //! If Speech Session status is Inactive and Guidance stream is already closed, start Speech Audio channel release timer.
            //! Ideally, this scenario shouldn't occur. Microphone is closed ahead of Guidance Stream and Voice Session Status(Inactive) Notification
            //! Considering the inconsistency of MD in sequence of events related to Speech session, this scenario is handled.

            if((false == IS_SPEECH_SESSION_ACTIVE()) && (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]))
            {
               ETG_TRACE_USR4(("Microphone Closed. Speech Session Inactive and Guidance Stream is closed. Start Speech Audio Channel release timer"));
               vStartSpeechRecRelTimer();
            }//if((false == IS_SPEECH_SESSION_ACTIVE()) && ( e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]))

         }// End of if-else: if(true == bMicOpen)
      }//if ((NULL != m_rAudCallbacks.fvStartAudioIn) && (NULL != m_rAudCallbacks.fvStopAudioIn))
   }//else if((NULL != m_poCmdAudio) && (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]))

   //! Speech Audio channel is inactive & Mic is closed. Unblock the Microphone reequest.
   //! Scenario occurs when user switches from AA VR to Native VR.
   else if((NULL != m_poCmdAudio) && (e8_AUD_ACT_GRANTED != m_aenChannelStatus[e8AUD_VR_IN]) && (false == bMicOpen))
   {
      ETG_TRACE_USR4(("Microphone Closed. Speech Audio Channel not active. Unblocking Microphone request..."));
      m_poCmdAudio->bMicRequestCompleted();
   }

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vMicRequestCb() left"));

}// t_Void spi_tclAAPAudio::vMicRequestCb(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled, t_Bool bEchoCancellationEnabled,..)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vVoiceSessionNotificationCb(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vVoiceSessionNotificationCb(tenAAPVoiceSessionStatus enVoiceSessionStatus)
{
    m_oAudioStreamLock.s16Lock();

   ETG_TRACE_USR1(("spi_tclAAPAudio:vVoiceSessionNotificationCb Entered: VoiceSessionStatus %d",
            ETG_ENUM(AAP_VOICE_SESSION_STATUS, enVoiceSessionStatus)));

   ETG_TRACE_USR4(("Speech Channel Status - %d, Guidance Stream State - %d, Microphone Stream State - %d, Guidance Channel State - %d",
            ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[e8AUD_VR_IN]),ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]),
            ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MICROPHONE]),ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8AUD_DUCK])));

   ETG_TRACE_USR4(("Media Stream State - %d, Media Channel Status - %d",
            ETG_ENUM(AUDSTREAM_STATE,m_aenStreamState[e8_AUDIOTYPE_MEDIA]),
            ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8_AUDIOTYPE_MEDIA])));

   sbVoiceSessionActive = (enVoiceSessionStatus == e8_VOICE_SESSION_START)? (true):(false);
   (true == sbVoiceSessionActive) ? (vStopSpeechRecRelTimer()):(vStartSpeechRecRelTimer());

   //! Voice Session has started. Audio activation has not been requested. Request for Activation of Speech Audio Channel.
   if ((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN])
            && (e8_VOICE_SESSION_START == enVoiceSessionStatus))
   {
      ETG_TRACE_USR4(("Speech Session Started. Request for Speech Audio Channel Activation"));
      //!Activate Speech channel either on this callback or vMicRequestCb(whichever is triggered first).
      vActivateChannel(e8AUD_VR_IN);

   }//End of if ((e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_VR_IN]) ...

   //! Voice Session active. Speech Audio Channel is already granted or requested for activation
   else if(((e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN])||(e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN])) &&
            (e8_VOICE_SESSION_START == enVoiceSessionStatus))
   {
      ETG_TRACE_USR4(("Speech Session Started. Speech Audio Channel Activation in progress/Activated"));
   }//else if(((e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8AUD_VR_IN])...)

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vVoiceSessionNotificationCb() left"));

} //spi_tclAAPAudio::vVoiceSessionNotificationCb(tenAAPVoiceSessionStatus enVoiceSessionStatus)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vAudioFocusRequestCb(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vAudioFocusRequestCb(tenAAPDeviceAudioFocusRequest enDevAudFocusRequest)
{
   m_oAudioStreamLock.s16Lock();

   ETG_TRACE_USR1(("spi_tclAAPAudio::vAudioFocusRequestCb() entered: enDevAudFocusRequest %d ", ETG_ENUM(DEVICE_AUDIOFOCUS_REQ,
                                 enDevAudFocusRequest)));

   ETG_TRACE_USR4(("Voice Session Status - %d, Speech Channel Status - %d, Guidance Stream State - %d, Guidance Channel State - %d, Microphone Stream State - %d",
         ETG_ENUM(BOOL,sbVoiceSessionActive), ETG_ENUM(AAPCHANNEL_STATE,m_aenChannelStatus[e8AUD_VR_IN]),
         ETG_ENUM(AUDSTREAM_STATE, m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]),
         ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[e8AUD_DUCK]),
         ETG_ENUM(AUDSTREAM_STATE, m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])));

   //! Check for speech channel status.
   //! This is required for if Audio Focus is released when Speech channel is not active
   //! If Speech channel activation or deactivation is requested, do not trigger another request.

   if((e8_AUDIO_FOCUS_REQ_RELEASE == enDevAudFocusRequest)
            && (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]))
   {
      ETG_TRACE_USR4(("Audio Focus released by MD. Deactivate Speech Audio Channel"));
      vStopSpeechRecRelTimer();
      vDeactivateChannel(e8AUD_VR_IN);
   }//if((e8_AUDIO_FOCUS_REQ_RELEASE == enDevAudFocusRequest)...)

   //! TBD: Deactivation of Guidance Channel

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR1(("spi_tclAAPAudio::vAudioFocusRequestCb() left"));

}//spi_tclAAPAudio::vAudioFocusRequestCb(tenAAPDeviceAudioFocusRequest enDevAudFocusRequest)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vAudStreamPlaybackErrorCb(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vAudStreamPlaybackErrorCb(trAAPAudioStreamInfo rAAPAudStreamInfo)
{
   m_oAudioStreamLock.s16Lock();

   ETG_TRACE_USR4(("spi_tclAAPAudio::vAudStreamPlaybackErrorCb() entered"));
   ETG_TRACE_USR4(("Audio Streaming Error observed for stream - %d, stream state - %d, channel state - %d with Error Code - %d",
         ETG_ENUM(AUDSTREAM_TYPE, rAAPAudStreamInfo.enStreamType),
         ETG_ENUM(AUDSTREAM_STATE,rAAPAudStreamInfo.enStreamState),
         ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[rAAPAudStreamInfo.enStreamType]), rAAPAudStreamInfo.enErrorCode));

   if (e8_AUDSTREAM_ERR_TIMEOUT == rAAPAudStreamInfo.enErrorCode)
   {
      //! Update the state of the audio stream. As there is no possibility to flag an error to stop the stream from MD, set the
      //! state to streaming. MD would send the audio stream after expiry of timer waiting for audio channel activation.
      m_aenStreamState[rAAPAudStreamInfo.enStreamType] = (e8_AUD_STREAM_OPEN == rAAPAudStreamInfo.enStreamState)
            ? e8_AUD_STREAMING : e8_AUD_STREAM_CLOSED;

      //! If Audio is streamed from MD but Audio channel is still not activated, request for activation of Audio channel if speech is inactive.
      //! Case 1: Voice session could have already ended but the Speech Audio channel could still be active. Scenario handled through short time out value to
      //! hold on for Speech Audio channel (10ms) - Not required to be addressed here.
      //! Case 2: Media stream has already timed out but Audio channel activation was not requested.
      if ((e8_AUDIOTYPE_MEDIA == rAAPAudStreamInfo.enStreamType) &&
            (e8_AUD_STREAMING == m_aenStreamState[rAAPAudStreamInfo.enStreamType])
            && (e8_AUD_NOT_ACTIVE == m_aenChannelStatus[e8AUD_MAIN_OUT]))
      {
         ETG_TRACE_USR4(("Media Audio is streamed from MD. Audio channel is not active. Requesting for activation of Media Audio channel"));
         vActivateChannel(e8AUD_MAIN_OUT);

      }//if((e8_AUDIOTYPE_MEDIA == rAAPAudStreamInfo.enStreamType) &&...)

   }//if(e8_AUDSTREAM_ERR_TIMEOUT == rAAPAudStreamInfo.enErrorCode)

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR4(("spi_tclAAPAudio::vAudStreamPlaybackErrorCb() left"));

}//vAudStreamPlaybackErrorCb(trAAPAudioStreamInfo rAAPAudStreamInfo)

//!Static
/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::bSpeechRecRelTimerCb
 ***************************************************************************/
t_Bool spi_tclAAPAudio::bSpeechRecRelTimerCb(
         timer_t rTimerID, t_Void *pvObject, const t_Void *pvUserData)
{
   ETG_TRACE_USR4(("spi_tclAAPAudio:bSpeechRecRelTimerCb entered"));

   SPI_INTENTIONALLY_UNUSED(pvUserData);
   SPI_INTENTIONALLY_UNUSED(rTimerID);

   Timer* poTimer = Timer::getInstance();
   if (NULL != poTimer)
   {
      poTimer->CancelTimer(srSpeechRecRelTimerID);
      ETG_TRACE_USR4(("Speech Rec Release Timer Stopped\n"));

      bSpeechRecRelTimerRunning = false;
   }//End of if (NULL != poTimer)

   spi_tclAAPAudio* poAAPAudio =
            static_cast<spi_tclAAPAudio*> (pvObject);

   if(NULL != poAAPAudio)
   {
      poAAPAudio->m_oAudioStreamLock.s16Lock();
      ETG_TRACE_USR4(("Speech Session Status - %d, Speech Channel Status - %d, Guidance Stream State - %d, Microphone Stream State - %d", ETG_ENUM(BOOL,
                                    sbVoiceSessionActive), ETG_ENUM(AAPCHANNEL_STATE,
                                    poAAPAudio->m_aenChannelStatus[e8AUD_VR_IN]), ETG_ENUM(AUDSTREAM_STATE,
                                    poAAPAudio->m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]), ETG_ENUM(AUDSTREAM_STATE,
                                    poAAPAudio->m_aenStreamState[e8_AUDIOTYPE_MICROPHONE])));

      //!De-activate Speech channel
      if (e8_AUD_ACT_GRANTED == poAAPAudio->m_aenChannelStatus[e8AUD_VR_IN])
      {
         poAAPAudio->vDeactivateChannel(e8AUD_VR_IN);
      }//End of if (e8_AUD_ACT_GRANTED == poAAPAudio->m_aenChannelStatus[e8AUD_VR_IN])

      //! Case 1: Media Stream is open, request for activation of Media Audio channel
      //! Case 2: Media stream is already in streaming state (due to timeout condition when source activation was not requested due to active voice session),
      //! request for activation of Media audio channel.
      if ((e8_AUD_STREAM_CLOSED != poAAPAudio->m_aenStreamState[e8_AUDIOTYPE_MEDIA])
           && (e8_AUD_NOT_ACTIVE == poAAPAudio->m_aenChannelStatus[e8AUD_MAIN_OUT]))
      {
         //! Request for Activation of Media Audio channel
         poAAPAudio->vActivateChannel(e8AUD_MAIN_OUT);
      }//End of if
      poAAPAudio->m_oAudioStreamLock.vUnlock();

   }//if(NULL != poAAPAudio)

   ETG_TRACE_USR1(("spi_tclAAPAudio::bSpeechRecRelTimerCb() left"));
   return true;

}//spi_tclAAPAudio::bSpeechRecRelTimerCb(..)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vStartSpeechRecRelTimer(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vStartSpeechRecRelTimer()
{
   //m_oAudioStreamLock.s16Lock(); -> Calling function has already acquired lock.

   ETG_TRACE_USR4(("spi_tclAAPAudio:vStartSpeechRecRelTimer entered, Timer Running State %d",
                    bSpeechRecRelTimerRunning));

   Timer* poTimer = Timer::getInstance();
   //! If Media Stream is opened and conditions for end of voice session is met, then it is not required to hold the speech
   //! audio channel active. Instead, Media Audio channel activation can be requested.
   //! Set the timeout value to 10ms if Media stream is opened, waiting for Audio channel to be activated. Else, 5 sec as usual.
   t_U16 u16TimeoutDuration = (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_MEDIA]) ?
         (5000): ((e8_AUD_ACT_REQUESTED == m_aenChannelStatus[e8_AUDIOTYPE_MEDIA])? (10) : (1000));

   if ((NULL != poTimer) && (false == bSpeechRecRelTimerRunning) &&
       (e8_AUD_STREAM_CLOSED == m_aenStreamState[e8_AUDIOTYPE_GUIDANCE]) &&
       (e8_AUD_ACT_GRANTED == m_aenChannelStatus[e8AUD_VR_IN]) &&
       ((false == IS_SPEECH_SESSION_ACTIVE()) || (e8_AUD_STREAMING == m_aenStreamState[e8_AUDIOTYPE_MEDIA])))
   {
      poTimer->StartTimer(srSpeechRecRelTimerID, u16TimeoutDuration, 0, this,
            &spi_tclAAPAudio::bSpeechRecRelTimerCb, NULL);

      ETG_TRACE_USR4(("Speech Rec Release Timer started\n"));

      bSpeechRecRelTimerRunning = true;
   }//End of if ((false == bSpeechRecRelTimerRunning) && ...

   //m_oAudioStreamLock.vUnlock();
   
   ETG_TRACE_USR4(("spi_tclAAPAudio:vStartSpeechRecRelTimer() left"));

}//t_Void spi_tclAAPAudio::vStartSpeechRecRelTimer()

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudio::vStopSpeechRecRelTimer(...)
 ***************************************************************************/
t_Void spi_tclAAPAudio::vStopSpeechRecRelTimer()
{
   // m_oAudioStreamLock.s16Lock(); -> Calling function has already acquired lock
   ETG_TRACE_USR4(("spi_tclAAPAudio:vStopSpeechRecRelTimer entered, Timer Running State %d",
                  bSpeechRecRelTimerRunning));

   Timer* poTimer = Timer::getInstance();
   if ((true == bSpeechRecRelTimerRunning) && (NULL != poTimer))
   {
      poTimer->CancelTimer(srSpeechRecRelTimerID);
      ETG_TRACE_USR4(("Speech Rec Release Timer Stopped\n"));
      bSpeechRecRelTimerRunning = false;
   }//End of if ((true == bSpeechRecRelTimerRunning) && (NULL != poTimer))

   //m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR4(("spi_tclAAPAudio:vStopSpeechRecRelTimer() left"));

}//t_Void spi_tclAAPAudio::vStopSpeechRecRelTimer()

/***************************************************************************
 ** FUNCTION:  t_Void  spi_tclAAPAudio::vUpdateDeviceSelection()
 ***************************************************************************/
t_Void spi_tclAAPAudio::vUpdateDeviceSelection(t_U32 u32DevID,
                                      tenDeviceCategory enDevCat,
                                      tenDeviceConnectionReq enDeviceConnReq,
                                      tenResponseCode enRespCode,
                                      tenErrorCode enErrorCode)
{
   m_oAudioStreamLock.s16Lock();

   SPI_INTENTIONALLY_UNUSED(enDevCat);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);
   ETG_TRACE_USR4(("spi_tclAAPAudio::vUpdateDeviceSelection entered"));

   if(e8DEVCONNREQ_SELECT == enDeviceConnReq)
   {
      su32CurSelectedDevID = (e8SUCCESS == enRespCode)?(u32DevID):(0);
      if((e8FAILURE == enRespCode) && (NULL != m_poCmdAudio))
      {
         m_poCmdAudio->vDestroyEndpoints();
         su32CurSelectedDevID = 0;

      }//if((e8FAILURE == enRespCode) && (NULL != m_poCmdAudio))

      //! Clear all the stream states.
      for(t_U8 u8Cnt = 0; u8Cnt < cou32MaxAudioStreams; u8Cnt++)
      {
          ETG_TRACE_USR4(("Stream State for %d stream is %d",ETG_ENUM(AUDSTREAM_TYPE, u8Cnt),
                                         ETG_ENUM(AUDSTREAM_STATE, m_aenStreamState[u8Cnt])));
          m_aenStreamState[u8Cnt] = e8_AUD_STREAM_CLOSED;
      }//for(t_U8 u8Cnt = 0; u8Cnt < cou32MaxAudioStreams; u8Cnt++)

      //! Clear all the channel states.
      for(t_U8 u8Cnt = 0; u8Cnt < e8AUD_INVALID; u8Cnt++)
      {
          ETG_TRACE_USR4(("Channel State for %d stream is %d",ETG_ENUM(AUDIO_DIRECTION, u8Cnt),
                                         ETG_ENUM(AAPCHANNEL_STATE, m_aenChannelStatus[u8Cnt])));
          m_aenChannelStatus[u8Cnt] = e8_AUD_NOT_ACTIVE;
      }//for(t_U8 u8Cnt = 0; u8Cnt < e8AUD_INVALID; u8Cnt++)


      ETG_TRACE_USR4(("spi_tclAAPAudio::vUpdateDeviceSelection. Selected Device ID %d", su32CurSelectedDevID));

   }//if(e8DEVCONNREQ_SELECT == enDeviceConnReq)

   m_oAudioStreamLock.vUnlock();
   ETG_TRACE_USR4(("spi_tclAAPAudio:vUpdateDeviceSelection() left"));

}//t_Void spi_tclAAPAudio::vUpdateDeviceSelection(t_U32 u32DevID,...)

/***************************************************************************
 ** FUNCTION:  t_Void  spi_tclAAPAudio::bGetAudioDir()
 ***************************************************************************/
t_Bool spi_tclAAPAudio::bGetAudioDir(tenAAPAudStreamType enAudStreamType,
      tenAudioDir& rfenAudioDir)
{
   t_Bool bValidAudDir = true;

   switch (enAudStreamType)
   {
      case e8_AUDIOTYPE_MEDIA:
      {
         rfenAudioDir = e8AUD_MAIN_OUT;
      }
         break;
      case e8_AUDIOTYPE_GUIDANCE:
      case e8_AUDIOTYPE_SYSTEM_AUDIO:
      {
         rfenAudioDir = e8AUD_DUCK;
         //rfenAudioDir = e8AUD_MIX_OUT;
      }
         break;
      case e8_AUDIOTYPE_VOICE:
      case e8_AUDIOTYPE_MICROPHONE:
      default:
      {
         ETG_TRACE_USR1((" bGetAudioDir: Unsupported Audio direction! "));
         bValidAudDir = false;
      }
      break;
   } //switch(enAudStreamType)

   ETG_TRACE_USR1((" spi_tclAAPAudio::bGetAudioDir: left with bValidAudDir = %d, Aud Dir %d ",
         ETG_ENUM(BOOL, bValidAudDir), ETG_ENUM(AUDIO_DIRECTION, rfenAudioDir)));
   return bValidAudDir;

}//t_Bool spi_tclAAPAudio::bGetAudioDir(tenAAPAudStreamType enAudStreamType,..)

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudio::bGetAAPAudioStreamStype(...)
 ***************************************************************************/
t_Bool spi_tclAAPAudio::bGetAAPAudioStreamStype(tenAudioDir enAudDir,
      tenAAPAudStreamType& rfenAAPAudioStreamType)
{
   t_Bool bValidAudDir = true;
   switch (enAudDir)
   {
      case e8AUD_MAIN_OUT:
         rfenAAPAudioStreamType = e8_AUDIOTYPE_MEDIA;
      break;
      case e8AUD_MIX_OUT:
      case e8AUD_DUCK:
         rfenAAPAudioStreamType = e8_AUDIOTYPE_GUIDANCE;
      break;
      case e8AUD_PHONE_IN:
         /*rfenAAPAudioStreamType = e8_AUDIOTYPE_MICROPHONE;
      break;*/
      case e8AUD_VR_IN:
         /*rfenAAPAudioStreamType = e8_AUDIOTYPE_VOICE;
      break;*/
      case e8AUD_ALERT_OUT:
      case e8AUD_INVALID:
      default:
      {
         ETG_TRACE_USR1((" bGetAAPAudioStreamStype: Unsupported Audio direction! "));
         bValidAudDir = false;
      }
      break;
   }
   //TODO - to be extended when all stream types are supported.

   ETG_TRACE_USR1((" spi_tclAAPAudio::bGetAAPAudioStreamStype: left with bValidAudDir = %d, StreamType %d ",
         ETG_ENUM(BOOL, bValidAudDir), ETG_ENUM(AUDSTREAM_TYPE, rfenAAPAudioStreamType)));
   return bValidAudDir;
}//t_Bool spi_tclAAPAudio::bGetAAPAudioStreamStype(tenAudioDir enAudDir,...)

//lint –restore
