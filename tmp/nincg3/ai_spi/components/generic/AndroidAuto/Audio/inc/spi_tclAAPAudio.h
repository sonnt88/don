/*!
 *******************************************************************************
 * \file             spi_tclAAPAudio.h
 * \brief            Implements the Audio functionality for Android Auto using
                     interface to AAP Wrapper for GAL .
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3 Projects
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Implementation for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#ifndef SPI_TCLAAPAUDIO_H
#define SPI_TCLAAPAUDIO_H

#include "Timer.h"
#include "AAPTypes.h"
#include "spi_tclAAPRespAudio.h"
#include "spi_tclAAPRespSession.h"
#include "spi_tclAudioDevBase.h"
#include "Lock.h"

static const t_U32 cou32MaxAudioStreams = 5;
class spi_tclAAPCmdAudio;
/**
 *  class definitions.
 */
/**
 * This class implements Audio functionality for Android Auto.
 */
class spi_tclAAPAudio: public spi_tclAudioDevBase,
   public spi_tclAAPRespAudio,public spi_tclAAPRespSession
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudio::spi_tclAAPAudio();
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPAudio()
       * \brief   Default Constructor
       **************************************************************************/
      spi_tclAAPAudio();

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudio::~spi_tclAAPAudio();
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAAPAudio()
       * \brief   Virtual Destructor
       **************************************************************************/
      virtual ~spi_tclAAPAudio();

      /*************Start of functions overridden from spi_tclAudioDevBase********/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudio::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  interface for the creator class to register for the required
       *         callbacks.
       * \param rfrAudCallbacks : reference to the callback structure
       *        populated by the caller
       **************************************************************************/
      t_Void vRegisterCallbacks(trAudioCallbacks &rfrAudCallbacks);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::bInitializeAudioPlayback(t_U32)
       ***************************************************************************/
      /*!
       * \fn      bInitializeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
       * \brief   Perform necessary actions to prepare for an Audio Playback.
       *          Function will be called prior to a Play Command from Audio Manager.
       *          Optional Interface to be implemented by Device Class.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device
	   * \param   [enAudDir]: Audio route being allocated
       * \retval  Bool value
       **************************************************************************/
      t_Bool bInitializeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir);
      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vSetAudioPipeConfig()
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetAudioPipeConfig()
       * \brief   Set the Audio pipeline congigration for alsa devices
       * \param   rfmapAudioPipeConfig: Contains audio pipeline configuration
       **************************************************************************/
      t_Void vSetAudioPipeConfig(tmapAudioPipeConfig &rfmapAudioPipeConfig);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::bStartAudio(t_U32,t_String)
       ***************************************************************************/
      /*!
       * \fn      bStartAudio(t_U32 u32DeviceId, t_String szAudioDev)
       * \brief   Start Streaming of Audio from the CE Device to the Audio Output
       *          Device assigned by the Audio Manager for the Source.
       *          Mandatory Interface to be implemented.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *         [szAudioDev]: ALSA Audio Device
       *          [enAudDir]    :Specify the Audio Direction(Alternate or Main Audio).
       * \retval  Bool value
       **************************************************************************/
      t_Bool bStartAudio(t_U32 u32DeviceId, t_String szAudioDev,
            tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::bStartAudio(t_U32,t_String, t_String,
      **             tenAudioDir)
       ***************************************************************************/
      /*!
       * \fn      bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
       *          t_String szInputAudioDev, tenAudioDir enAudDir)
       * \brief   Overloaded method to handle audio stream for Phone and VR.
       *          Start Streaming of Audio from the CE Device to the Audio Output
       *          Device assigned by the Audio Manager for the Source.
       *          Mandatory Interface to be implemented.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *          [szOutputAudioDev]: Output ALSA Audio Device
       *          [szInputAudioDev] : Input ALSA Audio Device
       *           [enAudDir]        : Specify the Audio Direction(Phone or VR Audio).
       * \retval  Bool value
       **************************************************************************/
      t_Bool bStartAudio(t_U32 u32DeviceId, t_String szOutputAudioDev,
              t_String szInputAudioDev, tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vStopAudio(t_U32)
       ***************************************************************************/
      /*!
       * \fn      vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir)
       * \brief   Stop Streaming of Audio from the CE Device to the Audio Output
       *          Device assigned by the Audio Manager for the Source.
       *          Mandatory Interface to be implemented.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *          [enAudDir]  :Specify the Audio Direction.
       * \retval  None
       **************************************************************************/
      t_Void vStopAudio(t_U32 u32DeviceId, tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::bFinalizeAudioPlayback(t_U32,tenAudioDir)
       ***************************************************************************/
      /*!
       * \fn      bFinalizeAudioPlayback(t_U32 u32DeviceId,tenAudioDir enAudDir)
       * \brief   Perform necessary actions on completion of Audio Playback.
       *          Function will be called after to a Stop Command from Audio Manager.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
	   * \param   [enAudDir]: Audio route being deallocated
       * \retval  Bool value
       **************************************************************************/
      t_Bool bFinalizeAudioPlayback(t_U32 u32DeviceId, tenAudioDir enAudDir);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::bSelectAudioDevice(t_U32)
       ***************************************************************************/
      /*!
       * \fn      bSelectAudioDevice(t_U32 u32DeviceId)
       * \brief   Perform necessary actions specific to a device selection like
       *          obtaining audio capabilities of device, supported modes etc
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       * \retval  Bool value
       **************************************************************************/
      t_Bool bSelectAudioDevice(t_U32 u32DeviceId);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vDeselectAudioDevice()
       ***************************************************************************/
      /*!
       * \fn      vDeselectAudioDevice(t_U32 uu32DeviceId
       * \brief   Perform necessary actions specific to a device on de-selection.
       *          Optional Interface for implementation.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       * \retval  NONE
       **************************************************************************/
      t_Void vDeselectAudioDevice(t_U32 u32DeviceId);

      /***************************************************************************
      ** FUNCTION:  t_Void  spi_tclAAPAudio::vUpdateDeviceSelection()
      ***************************************************************************/
      /*!
      * \fn      t_Void vUpdateDeviceSelection()
      * \brief   To update the device selection.
      * \param   u32DevID : [IN] Resource Manager callbacks structure.
      * \param   enDevCat : [IN] Category of the device
      * \param   enDeviceConnReq : [IN] Select/ deselect.
      * \param   enRespCode : [IN] Response code (success/failure)
      * \param   enErrorCode : [IN] Error
      * \retval  t_Void
      **************************************************************************/
      t_Void vUpdateDeviceSelection(t_U32 u32DevID, tenDeviceCategory enDevCat,
                              tenDeviceConnectionReq enDeviceConnReq,
                              tenResponseCode enRespCode, tenErrorCode enErrorCode);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::bIsAudioLinkSupported(t_U32,
       *                                            tenAudioLink)
       ***************************************************************************/
      /*!
       * \fn      bIsAudioLinkSupported(t_U32 u32DeviceId)
       * \brief   Perfom necessary actions specific to a device on de-selection.
       *          Optional Interface for implementation.
       * \param   [u32DeviceId]: Unique Identifier for the Connected Device.
       *          [enLink]: Specify the Audio Link Type for which Capability is
       *          requested. Mandatory interface to be implemented.
       * \retval  Bool value, TRUE if Supported, FALSE otherwise
       **************************************************************************/
      t_Bool bIsAudioLinkSupported(t_U32 u32DeviceId,
               tenAudioLink enLink)
      {
         return true;
      }

      /***************************************************************************
       ** FUNCTION: t_Bool spi_tclAAPAudio::vOnAudioError()
       ***************************************************************************/
      /*!
       * \fn    t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError)
       * \brief  Interface to set the audio error.
       * \param  enAudDir       : [IN] Uniquely identifies the target Device.
       * \param  enAudioError : [IN] Audio Error
       **************************************************************************/
      t_Void vOnAudioError(tenAudioDir enAudDir, tenAudioError enAudioError);

      /************End of functions overridden from spi_tclAudioDevBase***********/

      /***********Start of functions overridden from spi_tclAAPRespAudio**********/

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vPlaybackStartCb(...)
       ***************************************************************************/
      /*!
       * \fn      t_Void vPlaybackStartCb(tenAAPAudStreamType enAudStreamType, t_S32 s32SessionID)
       * \brief   Callback to start audio streaming from IAditAudioSinkCallbacks.
       * \sa      vPlaybackStopCb()
       **************************************************************************/
      t_Void vPlaybackStartCb(tenAAPAudStreamType enAudStreamType, t_S32 s32SessionID);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vPlaybackStopCb(...)
       ***************************************************************************/
      /*!
       * \fn      t_Void vPlaybackStopCb(tenAAPAudStreamType enAudStreamType, t_S32 s32SessionID)
       * \brief   Callback to stop audio streaming from IAditAudioSinkCallbacks.
       * \sa      vPlaybackStartCb()
       **************************************************************************/
      t_Void vPlaybackStopCb(tenAAPAudStreamType enAudStreamType, t_S32 s32SessionID);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vMicRequestCb(...)
       ***************************************************************************/
      /*!
       * \fn      t_Void vMicRequestCb(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled,
       *          t_Bool bEchoCancellationEnabled, t_S32 s32MaxUnacked)
       * \brief   Callback to request for microphone from IAditAudioSourceCallbacks.
       * \sa      None
       **************************************************************************/
      t_Void vMicRequestCb(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled,
               t_Bool bEchoCancellationEnabled, t_S32 s32MaxUnacked);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vAudStreamPlaybackErrorCb(...)
       ***************************************************************************/
      /*!
       * \fn      t_Void vAudStreamPlaybackErrorCb(trAAPAudioStreamInfo rAAPAudStreamInfo)
       * \brief   Callback to handle error from Audio Sink/Source Endpoint.
       * \sa      None
       **************************************************************************/
      t_Void vAudStreamPlaybackErrorCb(trAAPAudioStreamInfo rAAPAudStreamInfo);

      /***********End of functions overridden from spi_tclAAPRespAudio***********/

      /***********Start of functions overridden from spi_tclAAPRespSession**********/
      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::vVoiceSessionNotificationCb()
       ***************************************************************************/
      /*!
       * \fn      vVoiceSessionNotificationCb
       * \brief  CCalled when a voice session notification is received. Note that this callback only applies
       * to you if you do not always send a PTT short press to us always. If you always send PTT
       * short press to us, you should be able to ignore this call altogether.
       * \param enVoiceSessionStatus The status of the voice recognition session.
       **************************************************************************/
      t_Void vVoiceSessionNotificationCb(tenAAPVoiceSessionStatus enVoiceSessionStatus);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::vAudioFocusRequestCb()
       ***************************************************************************/
      /*!
       * \fn      vAudioFocusRequestCb
       * \brief  Called when the source wishes to acquire audio focus.
       * \param enDevAudFocusRequest Can be one of AUDIO_FOCUS_GAIN, AUDIO_FOCUS_GAIN_TRANSIENT,
       *        AUDIO_FOCUS_GAIN_TRANSIENT_MAY_DUCK, AUDIO_FOCUS_RELEASE.
       **************************************************************************/
      t_Void vAudioFocusRequestCb(tenAAPDeviceAudioFocusRequest enDevAudFocusRequest);

      /***********End of functions overridden from spi_tclAAPRespSession***********/
   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::bGetAAPAudioStreamStype()
       ***************************************************************************/
      /*!
       * \fn      bGetAAPAudioStreamStype
       * \brief  Function to convert Audio direction to AAP audio stream type.
       * \param  enAudDir : Audio Direction of type tenAudioDir.
       * \param  rfenAAPAudioStreamType : reference to AAP Audio stream type.
       * \retval  Bool value, TRUE if retrieved, FALSE otherwise
       **************************************************************************/
      t_Bool bGetAAPAudioStreamStype(tenAudioDir enAudDir,
            tenAAPAudStreamType& rfenAAPAudioStreamType);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudio::bGetAudioDir()
       ***************************************************************************/
      /*!
       * \fn      bGetAudioDir
       * \brief  Function to convert AAP Audio stream type to Audio direction.
       * \param  enAudStreamType : AAP Audio stream type.
       * \param  rfenAudioDir : reference to Audio direction type.
       * \retval  Bool value, TRUE if retrieved, FALSE otherwise
       **************************************************************************/
      t_Bool bGetAudioDir(tenAAPAudStreamType enAudStreamType,
            tenAudioDir& rfenAudioDir);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vProcessGuidanceStart(tenAudioDir enAudioDirection)
       ***************************************************************************/
      /*!
       * \fn      vProcessGuidanceStart
       * \brief  Function to process Guidance playback start request from AAP device.
       * \param  enAudioDirection : Audio direction type.
       * \retval  None
       **************************************************************************/
      t_Void vProcessGuidanceStart(tenAudioDir enAudioDirection);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vProcessMediaStart(tenAudioDir enAudioDirection)
       ***************************************************************************/
      /*!
       * \fn      vProcessMediaStart
       * \brief  Function to process Media playback start request from AAP device.
       * \param  enAudioDirection : Audio direction type.
       * \retval  None
       **************************************************************************/
      t_Void vProcessMediaStart(tenAudioDir enAudioDirection);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vStartSpeechRecRelTimer()
       ***************************************************************************/
      /*!
       * \fn     vStartSpeechRecRelTimer
       * \brief  Function to start the speech rec channel timer based on Speech channel
       *         status.
       * \param  None
       * \retval None
       **************************************************************************/
      t_Void vStartSpeechRecRelTimer();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vStopSpeechRecRelTimer()
       ***************************************************************************/
      /*!
       * \fn     vStopSpeechRecRelTimer
       * \brief  Function to stop the speech rec channel timer based on Speech channel
       *         status.
       * \param  None
       * \retval None
       **************************************************************************/
      t_Void vStopSpeechRecRelTimer();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vActivateChannel(tenAudioDir enAudioDirection)
       ***************************************************************************/
      /*!
       * \fn     vActivateChannel(tenAudioDir enAudioDirection)
       * \brief  Function to validate and request for activation of Guidance channel
       * \param  enAudioDirection : Audio direction type
       * \retval None
       **************************************************************************/
      t_Void vActivateChannel(tenAudioDir enAudioDirection);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudio::vDeactivateChannel(tenAudioDir enAudioDirection)
       ***************************************************************************/
      /*!
       * \fn     vDeactivateChannel(tenAudioDir enAudioDirection)
       * \brief  Function to request for deactivation of Guidance channel
       * \param  enAudioDirection : Audio direction type
       * \retval None
       **************************************************************************/
      t_Void vDeactivateChannel(tenAudioDir enAudioDirection);

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudio::bSpeechRecRelTimerCb
       ***************************************************************************/
      /*!
       * \fn     bSpeechRecRelTimerCb
       * \brief  called on expiry of Speech rec release timer
       * \param  rTimerID: ID of the timer which has expired
       * \param  pvObject: pointer to object passed while starting the timer
       * \param  pvUserData: data passed during start of the timer
       **************************************************************************/
      static t_Bool bSpeechRecRelTimerCb(timer_t rTimerID, t_Void *pvObject,
               const t_Void *pvUserData);

      //! Pointer to spi_tclAAPCmdAudio class
      spi_tclAAPCmdAudio* m_poCmdAudio;

      //! Structure object for Function pointers .
      //! This will be used by Audio Manager to register for response callbacks from AAP Audio
      trAudioCallbacks m_rAudCallbacks;

	  //! To remove hard coding of number of streams. 
      //! For every audio stream from MD, maintain the state of playback
	  tenAAPStreamState m_aenStreamState[cou32MaxAudioStreams];

	  //! Audio Channel Status used for every stream
	  tenAudioChannelStatus m_aenChannelStatus[e8AUD_INVALID];

	  //! Lock for Audio Streams and Activations
	  Lock    m_oAudioStreamLock;

};
#endif // SPI_TCLAAPAUDIO_H
