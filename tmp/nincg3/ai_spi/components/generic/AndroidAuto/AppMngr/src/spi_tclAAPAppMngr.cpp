/***********************************************************************/
/*!
 * \file  spi_tclAAPAppMngr.cpp
 * \brief AAP App Mngr Implementation
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    AAP App Mngr Implementation
 AUTHOR:         Shiva Kumar Gurija
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                        | Modification
 20.03.2015  | Shiva Kumar Gurija            | Initial Version
 26.05.2015  | Tejaswini H B                 | Added Lint comments to suppress C++11 Errors
 26.02.2016  | Rachana L Achar               | AAP Navigation implementation
 10.03.2016  | Rachana L Achar               | AAP Notification implementation

 \endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/

#include "spi_tclAppMngrDefines.h"
#include "spi_tclAAPRespDiscoverer.h"
#include "spi_tclAAPAppMngr.h"

using namespace std;

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPMNGR
#include "trcGenProj/Header/spi_tclAAPAppMngr.cpp.trc.h"
#endif
#endif

static const t_U32 scou32UnknownDevId = 0;
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 ** FUNCTION:  spi_tclDiPoAppMngr::spi_tclDiPoAppMngr()
 ***************************************************************************/
spi_tclAAPAppMngr::spi_tclAAPAppMngr():m_poAAPManager(NULL),
     m_u32SelectedDevId(scou32UnknownDevId)
{
   ETG_TRACE_USR1(("spi_tclAAPAppMngr::spi_tclAAPAppMngr()"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAppMngr::~spi_tclAAPAppMngr()
 ***************************************************************************/
spi_tclAAPAppMngr::~spi_tclAAPAppMngr()
{
   ETG_TRACE_USR1((" spi_tclAAPAppMngr::~spi_tclAAPAppMngr() "));
   m_poAAPManager = NULL;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAppMngr::bInitialize()
 ***************************************************************************/
t_Bool spi_tclAAPAppMngr::bInitialize()
{
   ETG_TRACE_USR1((" spi_tclAAPAppMngr::bInitialize() "));

   t_Bool bInitSuccess = false;
   m_poAAPManager = spi_tclAAPManager::getInstance();

   if((NULL != m_poAAPManager)
            &&
      (true == m_poAAPManager->bRegisterObject((spi_tclAAPRespDiscoverer*)this))
            &&
      (true == m_poAAPManager->bRegisterObject((spi_tclAAPRespNavigation*)this))
            &&
      (true == m_poAAPManager->bRegisterObject((spi_tclAAPRespNotification*)this)))
   {
	   bInitSuccess = true;
   }// if (NULL != m_poAAPManager)

   ETG_TRACE_USR2(("bInitialise() left with result = %u", bInitSuccess));
   return bInitSuccess;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAppMngr::vUnInitialize()
 ***************************************************************************/
t_Void spi_tclAAPAppMngr::vUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPDataService::bUninitialise() entered"));

   t_Bool bUnInitSuccess = false;
   if((NULL != m_poAAPManager)
       &&
      (m_poAAPManager->bUnRegisterObject((spi_tclAAPRespDiscoverer*) this))
       &&
      (m_poAAPManager->bUnRegisterObject((spi_tclAAPRespNavigation*) this))
       &&
      (m_poAAPManager->bUnRegisterObject((spi_tclAAPRespNotification*) this)))
   {
      bUnInitSuccess = true;
   }

   ETG_TRACE_USR2(("vUnInitialize() left with result = %u", bUnInitSuccess));
 }
/***************************************************************************
 ** FUNCTION:  t_Void  spi_tclAAPAppMngr::vRegisterAppMngrCallbacks()
 ***************************************************************************/
t_Void spi_tclAAPAppMngr::vRegisterAppMngrCallbacks(
         const trAppMngrCallbacks& corfrAppMngrCbs)
{
   //Add code
   m_rAppMngrCallbacks = corfrAppMngrCbs;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAppMngr::vSelectDevice()
 ***************************************************************************/
t_Void spi_tclAAPAppMngr::vSelectDevice(const t_U32 cou32DevId,
         const tenDeviceConnectionReq coenConnReq)
{
    
	/*lint -esym(40,fpvSelectDeviceResult)fpvSelectDeviceResult Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclAAPAppMngr::vSelectDevice:Dev-0x%x", cou32DevId));

   if (NULL != m_rAppMngrCallbacks.fpvSelectDeviceResult)
   {

      spi_tclAAPCmdNavigation* poCmdNavigation = (NULL != m_poAAPManager)?
      (m_poAAPManager->poGetNavigationInstance()) : (NULL);

      spi_tclAAPCmdNotification* poCmdNotification = (NULL != m_poAAPManager)?
      (m_poAAPManager->poGetNotificationInstance()) : (NULL);

         if (e8DEVCONNREQ_SELECT == coenConnReq)
         {
            m_u32SelectedDevId = cou32DevId;
          if (NULL != poCmdNavigation)
          {
            poCmdNavigation->bInitializeNavigationStatus();
         }
          if (NULL != poCmdNotification)
          {
        	  poCmdNotification->bInitializeNotification();
          }
      }
         else
         {
            m_u32SelectedDevId = 0;
          if (NULL != poCmdNavigation)
          {
            poCmdNavigation->vUninitializeNavigationStatus();
         }
          if (NULL != poCmdNotification)
          {
              poCmdNotification->vUninitializeNotification();
          }
      }

      (m_rAppMngrCallbacks.fpvSelectDeviceResult)(true);
   }//if(NULL != m_rAppMngrCallbacks.fpvSelectDeviceResult)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAppMngr::bLaunchApp()
 ***************************************************************************/
t_Void spi_tclAAPAppMngr::vLaunchApp(const t_U32 cou32DevId,
	      t_U32 u32AppId,
	      const trUserContext& rfrcUsrCntxt,
	      tenDiPOAppType enDiPOAppType,
	      t_String szTelephoneNumber,
	      tenEcnrSetting enEcnrSetting)
{
	/*lint -esym(40,fpvLaunchAppResult)fpvLaunchAppResult Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclAAPAppMngr::vLaunchApp:Dev-0x%x App-0x%x,Apptype: %d",
		   cou32DevId, u32AppId,ETG_ENUM(DIPO_APP_TYPE,enDiPOAppType)));

   SPI_INTENTIONALLY_UNUSED(enEcnrSetting);
   SPI_INTENTIONALLY_UNUSED(szTelephoneNumber);

   if (NULL != m_poAAPManager)
   {
      spi_tclAAPCmdInput* poAAPCmdInputInstance = m_poAAPManager->poGetInputInstance();

      if (NULL != poAAPCmdInputInstance)
      {

         switch (enDiPOAppType)
         {
            case e8DIPO_NOT_USED:
            case e8DIPO_NO_URL:
            {
               // Just ask for video focus.
               ETG_TRACE_USR1((" spi_tclAAPAppMngr::vLaunchApp:NO URL"));

            }
               break;
            case e8DIPO_MAPS:
            {
               //Send Key code for Navigation
               poAAPCmdInputInstance->vReportkey(cou32DevId, e8KEY_PRESS, e32APP_KEYCODE_NAVIGATION);
               poAAPCmdInputInstance->vReportkey(cou32DevId, e8KEY_RELEASE, e32APP_KEYCODE_NAVIGATION);
            }
               break;
            case e8DIPO_MOBILEPHONE:
            {
               // Send Key event for Telephony
               poAAPCmdInputInstance->vReportkey(cou32DevId, e8KEY_PRESS, e32APP_KEYCODE_TELEPHONY);
               poAAPCmdInputInstance->vReportkey(cou32DevId, e8KEY_RELEASE, e32APP_KEYCODE_TELEPHONY);
            }
               break;
            case e8DIPO_MUSIC:
            {
               //Send Key event for Music
               poAAPCmdInputInstance->vReportkey(cou32DevId, e8KEY_PRESS, e32APP_KEYCODE_MEDIA);
               poAAPCmdInputInstance->vReportkey(cou32DevId, e8KEY_RELEASE, e32APP_KEYCODE_MEDIA);
            }
               break;
            case e8DIPO_SIRI_BUTTONUP:
            {
               //Send Key event for Voice rec button down
               poAAPCmdInputInstance->vReportkey(cou32DevId, e8KEY_RELEASE, e32DEV_SEARCH);
            }
               break;
            case e8DIPO_SIRI_BUTTONDOWN:
            {
               //Send Key event for voice rec button up
               poAAPCmdInputInstance->vReportkey(cou32DevId, e8KEY_PRESS, e32DEV_SEARCH);
            }
               break;

            default:
            {
               // Unsupported Application Type
               ETG_TRACE_USR1((" spi_tclAAPAppMngr::vLaunchApp:Unsupported"));
            }
               break;
         }
		}
   }
   if (NULL != m_rAppMngrCallbacks.fpvLaunchAppResult)
   {
      (m_rAppMngrCallbacks.fpvLaunchAppResult)(cou32DevId, u32AppId,
               enDiPOAppType, e8NO_ERRORS, rfrcUsrCntxt);
   }//if( NULL != m_rAppMngrCallbacks.fpvLaunchAppResult)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAppMngr::vTerminateApp()
 ***************************************************************************/
t_Void spi_tclAAPAppMngr::vTerminateApp(const t_U32 cou32DevId,
         const t_U32 cou32AppId, const trUserContext& rfrcUsrCntxt)
{
	/*lint -esym(40,fpvTerminateAppResult) fpvTerminateAppResult is not referenced */
   ETG_TRACE_USR1((" spi_tclAAPAppMngr::vTerminateApp:Dev-0x%x App-0%x",
		   cou32DevId, cou32AppId));

   //Post the success as a Result to tcl App Mngr
   if (NULL != m_rAppMngrCallbacks.fpvTerminateAppResult)
   {
      (m_rAppMngrCallbacks.fpvTerminateAppResult)(cou32DevId, cou32AppId,
               e8NO_ERRORS, rfrcUsrCntxt);
   }//if( NULL != m_rAppMngrCallbacks.fpvTerminateAppResult )
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAppMngr::vGetAppIconData()
 ***************************************************************************/
t_Void spi_tclAAPAppMngr::vGetAppIconData(t_String szAppIconUrl,
         const trUserContext& rfrcUsrCntxt)
{
	/*lint -esym(40,fpvCbAppIconDataResult) fpvCbAppIconDataResult is not referenced */
   SPI_INTENTIONALLY_UNUSED(szAppIconUrl);
   //Post response to HMI
   if (NULL != m_rAppMngrCallbacks.fpvCbAppIconDataResult)
   {
      (m_rAppMngrCallbacks.fpvCbAppIconDataResult)(e8ICON_INVALID, NULL, 0,
               rfrcUsrCntxt);
   }//if( NULL != m_rAppMngrCallbacks.fpvCbAppIconDataResult)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAppMngr::bCheckAppValidity()
 ***************************************************************************/
t_Bool spi_tclAAPAppMngr::bCheckAppValidity(const t_U32 cou32DevId,
         const t_U32 cou32AppId)
{
	ETG_TRACE_USR1(("spi_tclAAPAppMngr::bCheckAppValidity"));
   SPI_INTENTIONALLY_UNUSED(cou32DevId);
   SPI_INTENTIONALLY_UNUSED(cou32AppId);
   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPAppMngr::vSetVehicleConfig();
***************************************************************************/
t_Void spi_tclAAPAppMngr::vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig, t_Bool bSetConfig)
{
	ETG_TRACE_USR1(("spi_tclAAPAppMngr:vSetVehicleConfig()-%d bSetConfig-%d \n", enVehicleConfig, ETG_ENUM(BOOL,
            bSetConfig)));
   SPI_INTENTIONALLY_UNUSED(bSetConfig);

   if ((NULL != m_poAAPManager) && (NULL != m_poAAPManager->poGetSensorInstance()))
   {
      switch (enVehicleConfig)
      {
         case e8_DAY_MODE:
         {
            ETG_TRACE_USR2(("spi_tclAAPAppMngr:vSetVehicleConfig()-- Day Mode Received !!! \n"));
            m_poAAPManager->poGetSensorInstance()->vReportDayNightMode(e8_AAP_DAY_MODE);
         }
            break;
         case e8_NIGHT_MODE:
         {
            ETG_TRACE_USR2(("spi_tclAAPAppMngr:vSetVehicleConfig()-- Night Mode Received !!! \n"));
            m_poAAPManager->poGetSensorInstance()->vReportDayNightMode(e8_AAP_NIGHT_MODE);
         }
            break;
         default:
         {
            //Nothing to do
         }
            break;
      }
   }
}

/***********************************************************************************************************************
** FUNCTION:  spi_tclAAPAppMngr::vNavigationStatusCallback(..)
************************************************************************************************************************/
t_Void spi_tclAAPAppMngr::vNavigationStatusCallback(tenNavAppState enNavAppState)
{
   ETG_TRACE_USR1(("spi_tclAAPAppMngr::vNavigationStatusCallback entered"));
   ETG_TRACE_USR2(("[DESC]:vNavigationStatusCallback: DeviceHandle = 0x%x,Device Category = %d,Navigation Status = %d",
          m_u32SelectedDevId,ETG_ENUM(DEVICE_CATEGORY,e8DEV_TYPE_ANDROIDAUTO),ETG_ENUM(NAVIGATION_STATUS,enNavAppState)));
   if ((NULL != m_rAppMngrCallbacks.fvUpdateNavStatusData) && (0 != m_u32SelectedDevId))
   {
	   trNavStatusData rNavStatusData;
	   rNavStatusData.u32DeviceHandle = m_u32SelectedDevId;
	   rNavStatusData.enDeviceCategory = e8DEV_TYPE_ANDROIDAUTO;
      rNavStatusData.enNavAppState = enNavAppState;
      (m_rAppMngrCallbacks.fvUpdateNavStatusData)(rNavStatusData);
   }
}

/******************************************************************************************************************************
** FUNCTION:  spi_tclAAPAppMngr::vNavigationNextTurnCallback(..)
********************************************************************************************************************************/
t_Void spi_tclAAPAppMngr::vNavigationNextTurnCallback(t_String szRoadName,tenAAPNavNextTurnSide enAAPNavNextTurnSide,
                                                      tenAAPNavNextTurnType enAAPNavNextTurnType, t_String szImage,
                                                      t_S32 s32TurnAngle, t_S32 s32TurnNumber)
{
   ETG_TRACE_USR1(("spi_tclAAPAppMngr::vNavigationNextTurnCallback entered"));
   ETG_TRACE_USR2(("[DESC]:vNavigationNextTurnCallback: DeviceHandle = 0x%x,Device Category = %d,Road Name = %s",
		   m_u32SelectedDevId ,ETG_ENUM(DEVICE_CATEGORY,e8DEV_TYPE_ANDROIDAUTO),szRoadName.c_str()));
   ETG_TRACE_USR2(("[DESC]:vNavigationNextTurnCallback: Next Turn side = %d,Next Turn Event = %d,Turn Angle =%d,"
                   "Turn Number = %d,Image = %s", ETG_ENUM(NEXTTURN_SIDE,enAAPNavNextTurnSide),
                   ETG_ENUM(NEXTTURN_EVENT,enAAPNavNextTurnType),s32TurnAngle,s32TurnNumber,szImage.c_str()));

   if ((NULL != m_rAppMngrCallbacks.fvUpdateNavNextTurnData) && (0 != m_u32SelectedDevId))
   {
	   trNavNextTurnData rNavNextTurnData;
	   rNavNextTurnData.u32DeviceHandle = m_u32SelectedDevId;
	   rNavNextTurnData.enDeviceCategory = e8DEV_TYPE_ANDROIDAUTO;
	   rNavNextTurnData.szRoadName = szRoadName;
       rNavNextTurnData.enAAPNavNextTurnSide = enAAPNavNextTurnSide;
       rNavNextTurnData.enAAPNavNextTurnType = enAAPNavNextTurnType;
       rNavNextTurnData.szImage = szImage;
       rNavNextTurnData.s32TurnAngle = s32TurnAngle;
       rNavNextTurnData.s32TurnNumber = s32TurnNumber;
      (m_rAppMngrCallbacks.fvUpdateNavNextTurnData)(rNavNextTurnData);
   }
}

/**********************************************************************************************************************************
** FUNCTION:  spi_tclAAPAppMngr::vSendNavigationNextTurnDistanceData(..)
***********************************************************************************************************************************/
t_Void spi_tclAAPAppMngr::vNavigationNextTurnDistanceCallback(
        const t_S32 cos32Distance,
        const t_S32 cos32Time)
{
   ETG_TRACE_USR1(("spi_tclAAPAppMngr::vNavigationNextTurnDistanceCallback entered"));
   ETG_TRACE_USR2(("[DESC]:vNavigationNextTurnDistanceCallback: DeviceHandle = 0x%x,Device Category = %d,Distance = %d, Time = %d",
                   m_u32SelectedDevId, ETG_ENUM(DEVICE_CATEGORY,e8DEV_TYPE_ANDROIDAUTO), cos32Distance, cos32Time));
   if ((NULL != m_rAppMngrCallbacks.fvUpdateNavNextTurnDistanceData) && (0 != m_u32SelectedDevId))
   {
	   trNavNextTurnDistanceData rNavNextTurnDistanceData;
	   rNavNextTurnDistanceData.u32DeviceHandle = m_u32SelectedDevId;
	   rNavNextTurnDistanceData.enDeviceCategory = e8DEV_TYPE_ANDROIDAUTO;
	   rNavNextTurnDistanceData.s32Distance = cos32Distance;
	   rNavNextTurnDistanceData.s32Time = cos32Time;
      (m_rAppMngrCallbacks.fvUpdateNavNextTurnDistanceData)(rNavNextTurnDistanceData);
   }
}

/***********************************************************************************************************************
** FUNCTION:  spi_tclAAPAppMngr::vNotificationSubscriptionStatusCallback(..)
************************************************************************************************************************/
t_Void spi_tclAAPAppMngr::vNotificationSubscriptionStatusCallback(t_Bool bNotifSubscribed)
{
   ETG_TRACE_USR1(("spi_tclAAPAppMngr::vNotificationSubscriptionStatusCallback entered"));
   ETG_TRACE_USR2(("[DESC]:vNotificationSubscriptionStatusCallback: DeviceHandle = 0x%x,Device Category = %d,Subscription Status = %d",
          m_u32SelectedDevId,ETG_ENUM(DEVICE_CATEGORY,e8DEV_TYPE_ANDROIDAUTO),ETG_ENUM(BOOL,bNotifSubscribed)));
}

/***********************************************************************************************************************
** FUNCTION:  spi_tclAAPAppMngr::vNotificationCallback(..)
************************************************************************************************************************/
t_Void spi_tclAAPAppMngr::vNotificationCallback(const t_String& corfszNotifText, t_Bool bHasId,
        const t_String& corfszId, t_Bool bHasIcon, t_U8 *pu8Icon, t_U32 u32IconSize)
{
   ETG_TRACE_USR1(("spi_tclAAPAppMngr::vNotificationCallback entered"));
   ETG_TRACE_USR2(("[DESC]:vNotificationCallback: DeviceHandle = 0x%x, Device Category = %d, Notification text = %s",
          m_u32SelectedDevId,ETG_ENUM(DEVICE_CATEGORY,e8DEV_TYPE_ANDROIDAUTO),corfszNotifText.c_str()));
   ETG_TRACE_USR2(("[DESC]:vNotificationCallback: Has Notification Id = %d, Notification Id = %s",
          ETG_ENUM(BOOL,bHasId), corfszId.c_str()));
   ETG_TRACE_USR2(("[DESC]:vNotificationCallback: Has Notification Icon = %d, Icon size = %d",
          ETG_ENUM(BOOL,bHasIcon), u32IconSize));

   if ((NULL != m_rAppMngrCallbacks.fvUpdateNotificationData) && (0 != m_u32SelectedDevId))
   {
      trNotificationData rNotificationData;
      rNotificationData.u32DeviceHandle = m_u32SelectedDevId;
      rNotificationData.enDeviceCategory = e8DEV_TYPE_ANDROIDAUTO;
      rNotificationData.szNotifText = corfszNotifText;
      rNotificationData.bHasId = bHasId;
      rNotificationData.szId = corfszId;
      rNotificationData.bHasIcon = bHasIcon;
      rNotificationData.pu8Icon = pu8Icon;
      rNotificationData.u32IconSize = u32IconSize;
      (m_rAppMngrCallbacks.fvUpdateNotificationData)(rNotificationData);
   }
}

/***********************************************************************************************************************
** FUNCTION:  spi_tclAAPAppMngr::vAckNotification(..)
************************************************************************************************************************/
t_Void spi_tclAAPAppMngr::vAckNotification(t_U32 u32DeviceHandle, const t_String& corfszNotifId)
{
   ETG_TRACE_USR1(("spi_tclAAPAppMngr::vAckNotification entered"));
   ETG_TRACE_USR2(("[DESC]:vAckNotification: DeviceHandle = 0x%x, Device Category = %d,"
                    "Notification Id = %s", u32DeviceHandle,
                    ETG_ENUM(DEVICE_CATEGORY,e8DEV_TYPE_ANDROIDAUTO),corfszNotifId.c_str()));

   spi_tclAAPCmdNotification* poCmdNotification = (NULL != m_poAAPManager)?
         (m_poAAPManager->poGetNotificationInstance()) : (NULL);
   if ((NULL != poCmdNotification) && (0 != u32DeviceHandle))
   {
      poCmdNotification->vAckNotification(u32DeviceHandle,corfszNotifId);
   }
}

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
