/***********************************************************************/
/*!
* \file  spi_tclAAPAppMngr.h
* \brief AAP App Mngr Implementation
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP App Mngr Implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
 20.03.2015 | Shiva Kumar Gurija    | Initial Version
 26.02.2016 | Rachana L Achar       | AAP Navigation implementation
 10.03.2016 | Rachana L Achar       | AAP Notification implementation

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAAPAPPMNGR_H_
#define _SPI_TCLAAPAPPMNGR_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclAppMngrDev.h"
#include "spi_tclAAPRespNavigation.h"
#include "spi_tclAAPRespNotification.h"
#include "spi_tclAAPManager.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclAAPAppMngr
* \brief AAP AppMngr Implementation
****************************************************************************/
class spi_tclAAPAppMngr : public spi_tclAppMngrDev, public spi_tclAAPRespNavigation,
                          public spi_tclAAPRespNotification
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAppMngr::spi_tclAAPAppMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAppMngr()
   * \brief   Default Constructor
   * \sa      ~spi_tclAAPAppMngr()
   **************************************************************************/
   spi_tclAAPAppMngr();

  /***************************************************************************
   ** FUNCTION:  spi_tclAAPAppMngr::~spi_tclAAPAppMngr()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPAppMngr()
   * \brief   Destructor
   * \sa      spi_tclAAPAppMngr()
   **************************************************************************/
   ~spi_tclAAPAppMngr();
 
   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPAppMngr::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the AAP App Mngr related things
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAppMngr::vUnInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUnInitialize()
   * \brief   To Uninitialize all the Dipo App Mngr related things
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vUnInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclAAPAppMngr::vRegisterAppMngrCallbacks()
   ***************************************************************************/
   /*!
   * \fn      t_Void vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs)
   * \brief   To Register for the asynchronous responses that are required from
   *          ML/DiPo App Mngr
   * \param   corfrAppMngrCbs : [IN] Application Manager callabcks structure
   * \retval  t_Void 
   **************************************************************************/
   t_Void vRegisterAppMngrCallbacks(const trAppMngrCallbacks& corfrAppMngrCbs);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAppMngr::vSelectDevice()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vSelectDevice(const t_U32 cou32DevId,
   *          const tenDeviceConnectionReq coenConnReq)
   * \brief   To Subscribe/unsubscribe for events of the currently selected device
   * \pram    cou32DevId : [IN] Unique Device Id
   * \param   coenConnReq : [IN] connected/disconnected
   * \retval  t_Void
   **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId,
      const tenDeviceConnectionReq coenConnReq);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPAppMngr::bLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bLaunchApp(const t_U32 cou32DevId, 
   *           t_U32 u32AppHandle, 
   *           tenDiPOAppType enDiPOAppType, 
   *           t_String szTelephoneNumber, 
   *           tenEcnrSetting enEcnrSetting)
   * \brief   To Launch the requested app 
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param  [IN] enDevCat : Device Type Information(Mirror Link/DiPO).
   * \param  [IN] u32AppHandle : Uniquely identifies an Application on
   *              the target Device. This value will be obtained from AppList Interface. 
   *              This value will be set to 0xFFFFFFFF if DeviceCategory = DEV_TYPE_DIPO.
   * \param  [IN] enDiPOAppType : Identifies the application to be launched on a DiPO device.
   *              This value will be set to NOT_USED if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] szTelephoneNumber : Number to be dialed if the DiPO application to be launched 
   *              is a phone application. If not valid to be used, this will be set to NULL, 
   *              zero length string. Will not be used if DeviceCategory = DEV_TYPE_MIRRORLINK.
   * \param  [IN] enEcnrSetting : Sets voice or server echo cancellation and noise reduction 
   *              settings if the DiPO application to be launched is a phone application. 
   *              If not valid to be used, this will be set to ECNR_NOCHANGE.
   * \retval  t_Bool
   * \sa      vTerminateApp()
   **************************************************************************/
   t_Void vLaunchApp(const t_U32 cou32DevId, 
         t_U32 u32AppId,
         const trUserContext& rfrcUsrCntxt,
         tenDiPOAppType enDiPOAppType,
         t_String szTelephoneNumber,
         tenEcnrSetting enEcnrSetting);

     /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAppMngr::vTerminateApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vTerminateApp(trUserContext rUserContext,
   *          t_U32 u32DeviceId, t_U32 u32AppId)
   * \brief   To Terminate an Application asynchronously.
   * \param   rUserContext : [IN] Context Message
   * \param   u32DeviceId  : [IN] Device Id
   * \param   u32AppId     : [IN] Application Id
   * \retval  t_Void
   * \sa      t_Bool bLaunchApp(t_U32 u32DeviceId, t_U32 u32AppId)
   **************************************************************************/
   t_Void vTerminateApp(const t_U32 cou32devId, 
      const t_U32 cou32AppId,
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAppMngr::vGetAppIconData()
   ***************************************************************************/
   /*!
   * \fn    virtual t_Void vGetAppIconData(t_String szAppIconUrl, 
   *         const trUserContext& rfrcUsrCntxt)
   * \brief  To Get the application icon data
   * \param  szAppIconUrl  : [IN] Application Icon data
   * \param  rfrcUsrCntxt  : [IN] User Context
   * \retval  t_Void
   **************************************************************************/
   t_Void vGetAppIconData(t_String szAppIconUrl, 
      const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPAppMngr::bCheckAppValidity()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bCheckAppValidity(const t_U32 cou32DevId,
   *             const t_U32 cou32AppId)
   * \brief   To check whether the application exists or not
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cou32AppId  : [IN] Application Id
   * \retval  t_Bool
   **************************************************************************/
   t_Bool bCheckAppValidity(const t_U32 cou32DevId, 
      const t_U32 cou32AppId);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPAppMngr::vSetVehicleConfig(tenVehicleConfiguration...)
   ***************************************************************************/
   /*!
   * \fn      vSetVehicleConfig(tenVehicleConfiguration...)
   * \brief   Method to set vehicle configuration
   * \param   enVehicleConfig : [IN] Vehicle config data required.
   * 		  bSetConfig      : [IN]   Boolean value to set for changing value or not
   * \retval  None
   ***************************************************************************/
   t_Void vSetVehicleConfig(tenVehicleConfiguration enVehicleConfig,
                            t_Bool bSetConfig);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPAppMngr::vNavigationStatusCallback(...)
   ***************************************************************************/
   /*!
   * \fn     vNavigationStatusCallback(tenNavAppState enNavAppState)
   * \brief  It notifies the client whenever there is a navigation status
   *         change(ACTIVE/INACTIVE/UNAVAILABLE).
   * \param  enNavAppState : [IN] Navigation Status
   **************************************************************************/
   t_Void vNavigationStatusCallback(tenNavAppState enNavAppState);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPAppMngr::vNavigationNextTurnCallback(...)
   ***************************************************************************/
   /*!
   * \fn     vNavigationNextTurnCallback(t_String szRoadName,tenAAPNavNextTurnSide enAAPNavNextTurnSide,
   *                                     tenAAPNavNextTurnType enAAPNavNextTurnType, t_String szImage,
   *                                     t_S32 s32TurnAngle, t_S32 s32TurnNumber)
   * \brief  It notifies the client whenever there is a navigation next turn event information.
   * \param  szRoadName           : [IN] Name of the Road
   * \param  enAAPNavNextTurnSide : [IN] Next Turn Side
   * \param  enAAPNavNextTurnType : [IN] Next Turn Event
   * \param  szImage              : [IN] Next Turn Image
   * \param  s32TurnAngle         : [IN] Next turn angle in degrees between the roundabout entry and exit
   * \param  s32TurnNumber        : [IN] Next turn number, counting around from the roundabout entry to the exit
   ***************************************************************************/
   t_Void vNavigationNextTurnCallback(t_String szRoadName,tenAAPNavNextTurnSide enAAPNavNextTurnSide,
                                      tenAAPNavNextTurnType enAAPNavNextTurnType, t_String szImage,
                                      t_S32 s32TurnAngle, t_S32 s32TurnNumber);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPAppMngr::vNavigationNextTurnDistanceCallback(...)
   ***************************************************************************/
   /*!
   * \fn     vNavigationNextTurnDistanceCallback(
   *          const t_S32 cos32Distance,const t_S32 cos32Time)
   * \brief  It notifies the client whenever there is a change in
   *         navigation next turn distance data.
   * \param  cos32Distance     : [IN] Distance to next turn event
   * \param  cos32Time         : [IN] Time to next turn event
   ***************************************************************************/
   t_Void vNavigationNextTurnDistanceCallback(const t_S32 cos32Distance,
                                              const t_S32 cos32Time);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPAppMngr::vNotificationSubscriptionStatusCallback(t_Bool bsubscribed)
   ***************************************************************************/
   /*!
   * \fn     t_Void vNotificationSubscriptionStatusCallback(t_Bool bSubscribed)
   * \brief  It notifies the client whenever there is a change in subscription status
   *         for notification(subscribed/unsubscribed).
   * \param  bSubscribed  : [IN] Notification subscription status, true if subscribed
   ***************************************************************************/
   t_Void vNotificationSubscriptionStatusCallback(t_Bool bNotifSubscribed);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPAppMngr::vNotificationCallback(...)
   ***************************************************************************/
   /*!
   * \fn     t_Void vNotificationCallback(const t_String& corfszNotifText,
   *          t_Bool bHasId, const t_String& corfszId, t_Bool bHasIcon,
   *          t_U8 *pu8Icon, t_U32 u32IconSize)
   * \brief  It notifies the client whenever a notification is received.
   * \param  corfszNotifText  : [IN] Notification text
   * \param  bHasId           : [IN] true if the notification has ID
   * \param  corfszId         : [IN] Notification Id
   * \param  bHasIcon         : [IN] true if the notification has an Icon
   * \param  pu8Icon          : [IN] pointer to the icon
   * \param  u32IconSize      : [IN] size of the icon
   ***************************************************************************/
   t_Void vNotificationCallback(const t_String& corfszNotifText, t_Bool bHasId,
           const t_String& corfszId, t_Bool bHasIcon, t_U8 *pu8Icon, t_U32 u32IconSize);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPAppMngr::vAckNotification(...)
   ***************************************************************************/
   /*!
   * \fn     t_Void vAckNotification(t_U32 u32DeviceHandle,
   *          const t_String& corfszNotifId)
   * \brief  Acknowledges the receipt of notification
   * \param  u32DeviceHandle : [IN] Id of the device to be acknowledged
   * \param  corfszNotifId   : [IN] Received notification's Id
   ***************************************************************************/
   t_Void vAckNotification(t_U32 u32DeviceHandle, const t_String& corfszNotifId);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/



   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAppMngr& spi_tclAAPAppMngr::operator= (const..
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAppMngr& operator= (const spi_tclAAPAppMngr &corfrSrc)
   * \brief   Assignment Operator, will not be implemented.
   * \note    This is a technique to disable the assignment operator for this class.
   *          So if an attempt for the assignment is made linker complains.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclAAPAppMngr& operator= (const spi_tclAAPAppMngr &corfrSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAppMngr::spi_tclAAPAppMngr(const spi_tclAAPAppMngr..
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAppMngr(const spi_tclAAPAppMngr &corfrSrc)
   * \brief   Copy constructor, will not be implemented.
   * \note    This is a technique to disable the Copy constructor for this class.
   * \param   corfrSrc : [IN] Source Object
   **************************************************************************/
   spi_tclAAPAppMngr(const spi_tclAAPAppMngr &corfrSrc);

   //! call back structure to end response to tclAppMngr
   trAppMngrCallbacks m_rAppMngrCallbacks;

   spi_tclAAPManager* m_poAAPManager;

   //! Device handle of the selected device
   t_U32 m_u32SelectedDevId;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};//spi_tclAAPAppMngr


#endif //_SPI_TCLAAPAPPMNGR_H_
