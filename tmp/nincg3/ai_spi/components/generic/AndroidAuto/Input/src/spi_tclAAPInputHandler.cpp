/*!
 *******************************************************************************
 * \file              spi_tclAAPInputHandler.cpp
 * \brief             SPI input handler for AAP devices
 *******************************************************************************
 \verbatim
 PROJECT:       Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Input handler class to send input events from Head Unit to
                AAP mobile device
 AUTHOR:        Sameer Chandra (RBEI/ECP2)
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 06.03.2015 |  Sameer Chandra              | Initial Version
 07.07.2015 |  Shiva Kumar G               | Dynamic display configuration
 17.07.2015	|  Sameer Chandra              | Knob Encoder Implementation
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "spi_tclAAPInputHandler.h"
#include "spi_tclAAPCmdInput.h"
#include "spi_tclConfigReader.h"


#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_INPUTHANDLER
#include "trcGenProj/Header/spi_tclAAPInputHandler.cpp.trc.h"
#endif
#endif

static const t_U8 cou8EnableVerbose   = 0;
static const t_U8 cou8TouchMaximum    = 2;
static const t_U8 cou8TriggerInterval = 50;

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::spi_tclAAPInputHandler()
***************************************************************************/
spi_tclAAPInputHandler::spi_tclAAPInputHandler():m_poAAPmanager(NULL)
{
  ETG_TRACE_USR1(("spi_tclAAPInputHandler::spi_tclAAPInputHandler entered "));

  m_poAAPmanager = spi_tclAAPManager::getInstance();
  
  if (NULL != m_poAAPmanager)
  {
      //! Register with AAP manager for Video callbacks
      m_poAAPmanager->bRegisterObject((spi_tclAAPRespVideo*)this);
  }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::~spi_tclAAPInputHandler()
***************************************************************************/
spi_tclAAPInputHandler::~spi_tclAAPInputHandler()
{
   ETG_TRACE_USR1(("spi_tclAAPInputHandler::~spi_tclAAPInputHandler entered"));
   m_poAAPmanager = NULL;

}
/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::vProcessTouchEvent()
***************************************************************************/
t_Void spi_tclAAPInputHandler::vProcessTouchEvent(t_U32 u32DeviceHandle,trTouchData &rfrTouchData)const
{
   ETG_TRACE_USR1(("spi_tclAAPInputHandler::vProcessTouchEvent entered "));

   if (NULL != m_poAAPmanager)
   {
      spi_tclAAPCmdInput
               * poAAPCmdInput =
                        const_cast<spi_tclAAPCmdInput*> (m_poAAPmanager->poGetInputInstance());

      if (NULL != poAAPCmdInput)
      {
         ETG_TRACE_USR1(("spi_tclAAPInputHandler::vProcessTouchEvent - Touch Events sent"));
         poAAPCmdInput->vReportTouch(u32DeviceHandle, rfrTouchData, m_rScalingAttributes);
      }
   }

}
/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::vProcessTouchEvent()
***************************************************************************/
t_Void spi_tclAAPInputHandler::vProcessKeyEvents(t_U32 u32DeviceHandle,tenKeyMode enKeyMode,
                        tenKeyCode enKeyCode) const
{
   ETG_TRACE_USR1(("spi_tclAAPInputHandler::vProcessTouchEvent entered "));

   if (NULL != m_poAAPmanager)
   {
      spi_tclAAPCmdInput
               * poAAPCmdInput =
                        const_cast<spi_tclAAPCmdInput*> (m_poAAPmanager->poGetInputInstance());

      if (NULL != poAAPCmdInput)
      {
         //forward the received info to the Input Command using pointer obtained.
         ETG_TRACE_USR1(("spi_tclAAPInputHandler::vProcessKeyEvents - Key Codes sent"));
         poAAPCmdInput->vReportkey(u32DeviceHandle, enKeyMode, enKeyCode);
      }
   }
}
/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::vSelectDevice()
***************************************************************************/
t_Void spi_tclAAPInputHandler::vSelectDevice(const t_U32 cou32DevId,
               const tenDeviceConnectionReq coenConnReq)
{
	/*lint -esym(40,fvSelectDeviceResp) fvSelectDeviceResp is not declared */
   ETG_TRACE_USR1(("spi_tclAAPInputHandler:vSelectDevice-0x%x Enabled - %d", cou32DevId, ETG_ENUM(CONNECTION_REQ,
            coenConnReq)));

   tenErrorCode enErrCode = e8INVALID_DEV_HANDLE;
   if (e8DEVCONNREQ_SELECT == coenConnReq && NULL != m_poAAPmanager)
   {
      //! Initialize the InputSource/WaylandInputSource endpoint.
      enErrCode = (bInitInputSession()) ? e8NO_ERRORS : e8SELECTION_FAILED;

   }
   else if (NULL != m_poAAPmanager)
   {

      spi_tclAAPCmdInput* poAAPCmdInput = 
	  			const_cast<spi_tclAAPCmdInput*> (m_poAAPmanager->poGetInputInstance());

      if (NULL != poAAPCmdInput)
      {
         enErrCode = e8NO_ERRORS;
         //! Un-initialize the InputSource endpoint.
         poAAPCmdInput->bUnInitializeInput();
      }

   }

   if (NULL != m_rInputCallbacks.fvSelectDeviceResp)
   {
      (m_rInputCallbacks.fvSelectDeviceResp)(cou32DevId, enErrCode);
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::vRegisterVideoCallbacks()
***************************************************************************/
t_Void spi_tclAAPInputHandler::vRegisterInputCallbacks(const trInputCallbacks& corfrInputCallbacks)
{
   ETG_TRACE_USR1(("spi_tclAAPInputHandler::vRegisterVideoCallbacks entered "));
   m_rInputCallbacks = corfrInputCallbacks;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::vRegisterVideoCallbacks()
***************************************************************************/
t_Void spi_tclAAPInputHandler::vOnSelectDeviceResult(const t_U32 cou32DevId,
            const tenDeviceConnectionReq coenConnReq,
            const tenResponseCode coenRespCode)
{

	 ETG_TRACE_USR1(("spi_tclAAPInputHandler::vOnSelectDeviceResult:Dev-0x%x ConnReq-%d, RespCode-%d", cou32DevId, ETG_ENUM(CONNECTION_REQ,
            coenConnReq), coenRespCode));

   if ((e8FAILURE == coenRespCode) && (e8DEVCONNREQ_SELECT == coenConnReq)
            && (NULL != m_poAAPmanager))
   {
      spi_tclAAPCmdInput
               * poAAPCmdInput =
                        const_cast<spi_tclAAPCmdInput*> (m_poAAPmanager->poGetInputInstance());

      if (NULL != poAAPCmdInput)
      {
         //! Un-initialize the InputSource endpoint.
         poAAPCmdInput->bUnInitializeInput();
      }

   }

}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::bInitInputSession()
***************************************************************************/
t_Bool spi_tclAAPInputHandler::bInitInputSession()
{
   //! Get Video settings instance for screen properties
   spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();
   t_Bool bInitStatus = false;

   if ( (NULL != m_poAAPmanager) && (NULL != poConfigReader) )
   {
      spi_tclAAPCmdInput* poAAPCmdInput =   const_cast<spi_tclAAPCmdInput*>
         (m_poAAPmanager->poGetInputInstance());

      //! Get Screen dimensinons
      if (NULL != poAAPCmdInput)
      {
         trAAPInputConfig rAAPInputConfig;

         trVideoConfigData rVideoConfigData;
         poConfigReader->vGetVideoConfigData(e8DEV_TYPE_ANDROIDAUTO,rVideoConfigData);

         trScreenSize rScreenSize;
         rAAPInputConfig.u32DisplayHeight  = rVideoConfigData.u32ProjScreen_Height;
         rAAPInputConfig.u32DisplayWidth   = rVideoConfigData.u32ProjScreen_Width;
         rAAPInputConfig.u16LayerID		    = rVideoConfigData.u32TouchLayerId;
         rAAPInputConfig.u16SurfaceID	    = rVideoConfigData.u32TouchSurfaceId;
         rAAPInputConfig.u16TouchHeight    = rVideoConfigData.u32ProjScreen_Height;
         rAAPInputConfig.u16TouchWidth     = rVideoConfigData.u32ProjScreen_Width;
         rAAPInputConfig.u8EnableVerbose   = cou8EnableVerbose;
         rAAPInputConfig.u8TouchMaximum    = cou8TouchMaximum;
         rAAPInputConfig.u8TriggerInterval = cou8TriggerInterval;
         rAAPInputConfig.bIsRotaryCtrl = poConfigReader->bGetRotaryCtrlSupport();

         tenAAPTouchScreenType rfenAAPTouchScreenType;
         poConfigReader->vGetTouchScreenType(rfenAAPTouchScreenType);

         rAAPInputConfig.enAAPTouchScreenType = rfenAAPTouchScreenType;
		 
         bInitStatus = poAAPCmdInput->bInitializeInput(rAAPInputConfig);
      }
   }

   return bInitStatus;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::vProcessKnobKeyEvents()
***************************************************************************/
t_Void spi_tclAAPInputHandler::vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt) const
{
   ETG_TRACE_USR1(("spi_tclAAPInputHandler::vProcessKnobKeyEvents entered "));

   if (NULL != m_poAAPmanager)
   {
      spi_tclAAPCmdInput* poAAPCmdInput =
                        const_cast<spi_tclAAPCmdInput*> (m_poAAPmanager->poGetInputInstance());

      if (NULL != poAAPCmdInput)
      {
         //forward the received info to the Input Command using pointer obtained.
         ETG_TRACE_USR1(("Encoder Delta Count Sent"));
         poAAPCmdInput->vReportKnobkey(u32DeviceHandle, s8EncoderDeltaCnt);
      }
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPInputHandler::vVideoConfigCallback()
***************************************************************************/
t_Void spi_tclAAPInputHandler::vVideoConfigCallback(t_S32 s32LogicalUIWidth, t_S32 s32LogicalUIHeight)
{
   ETG_TRACE_USR1(("Received vVideoConfigCallback from MD"));

   // Fetch video configurations from config reader.
   m_rScalingAttributes.s32YStartCoordinate = 0;
   m_rScalingAttributes.s32XStartCoordinate = 0;

   spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();

   if (NULL != poConfigReader)
   {
      trVideoConfigData rVideoConfigData;
      poConfigReader->vGetVideoConfigData(e8DEV_TYPE_ANDROIDAUTO,rVideoConfigData);

      //Calculate Phone Aspect ratio & Display Aspect Ratio

      t_Float fPhoneAspectRatio = (t_Float)(static_cast<t_U32>(s32LogicalUIWidth))/
                                           (static_cast<t_U32>(s32LogicalUIHeight));

      t_Float fDispAspectRatio = (t_Float)(rVideoConfigData.u32ProjScreen_Width)/
                                          (rVideoConfigData.u32ProjScreen_Height);

      if (fPhoneAspectRatio < fDispAspectRatio)
      {
         // Calculate the new width
         m_rScalingAttributes.u32ScreenWidth = (rVideoConfigData.u32ProjScreen_Height * fPhoneAspectRatio );
         m_rScalingAttributes.u32ScreenHeight = rVideoConfigData.u32ProjScreen_Height;
         //Calculate new X axis
         m_rScalingAttributes.s32XStartCoordinate = (rVideoConfigData.u32ProjScreen_Width - m_rScalingAttributes.u32ScreenWidth) / 2;
      }
      else
      {
         m_rScalingAttributes.u32ScreenHeight =  (rVideoConfigData.u32ProjScreen_Width / fDispAspectRatio);
         m_rScalingAttributes.u32ScreenWidth  =   rVideoConfigData.u32ProjScreen_Width;

         // Calculate new Y axis
         m_rScalingAttributes.s32YStartCoordinate = (rVideoConfigData.u32ProjScreen_Height - m_rScalingAttributes.u32ScreenHeight) / 2 ;
      }




      m_rScalingAttributes.fHeightScalingValue = (t_Float)(static_cast<t_U32>((s32LogicalUIHeight)))
                                                  /(m_rScalingAttributes.u32ScreenHeight);

      m_rScalingAttributes.fWidthScaleValue = (t_Float)(static_cast<t_U32>(s32LogicalUIWidth))
                                                  /(m_rScalingAttributes.u32ScreenWidth);





      ETG_TRACE_USR4(("Scaling Factor for Height [%f] , Width [%f] ,", m_rScalingAttributes.fHeightScalingValue,
                                                                      m_rScalingAttributes.fWidthScaleValue));

      ETG_TRACE_USR4(("New origin for Touch for X axis : [%d]",m_rScalingAttributes.s32XStartCoordinate));
   }
}
