/***********************************************************************/
/*!
 * \file  spi_tclAAPInputHandler.h
 * \brief SPI input handler for AAP devices
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Input Handler to report input events from Head Unit to
 AAP supported Mobile device.
 AUTHOR:         Sameer Chandra
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        |  Author                | Modification
 06.03.2015  |  Sameer Chandra        | Initial Version

 \endverbatim
 *************************************************************************/

#ifndef _SPI_TCLAAPINPUTHANDLER_H_
#define _SPI_TCLAAPINPUTHANDLER_H_

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclAAPManager.h"
#include "spi_tclInputDevBase.h"
#include "spi_tclAAPRespVideo.h"

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
 * \class spi_tclAAPInputHandler
 * \brief
 ****************************************************************************/
class spi_tclAAPInputHandler : public spi_tclInputDevBase, public spi_tclAAPRespVideo
{
public:

   /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputHandler::spi_tclAAPInputHandler()
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPInputHandler()
    * \brief   Default Constructor
    * \sa      spi_tclAAPInputHandler()
    **************************************************************************/
   spi_tclAAPInputHandler();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputHandler::~spi_tclAAPInputHandler()
    ***************************************************************************/
   /*!
    * \fn     ~spi_tclAAPInputHandler()
    * \brief   Destructor
    * \sa      ~spi_tclAAPInputHandler()
    **************************************************************************/
   ~spi_tclAAPInputHandler();

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclAAPInputHandler::vProcessTouchEvent
    ***************************************************************************/
   /*!
    * \fn      vProcessTouchEvent(t_U32 u32DeviceHandle,trTouchData &rfrTouchData)
    * \brief   Receives the Touch events and forwards it to AAP Input Source Endpoint
    * 		  wrapper.
    * \param   u32DeviceHandle  : [IN] unique identifier to AAP device
    * \param   rfrTouchData     : [IN] reference to touch data structure which contains
    *          touch details received /ref trTouchData
    * \retval  NONE
    **************************************************************************/
   t_Void vProcessTouchEvent(t_U32 u32DeviceHandle, trTouchData &rfrTouchData) const;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPInputHandler::vProcessKeyEvents
    ***************************************************************************/
   /*!
    * \fn      vProcessKeyEvents(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
    tenKeyCode enKeyCode)
    * \brief   Receives hard key events and forwards it to AAP InputSource Endpoint
    * 		  wrapper.
    * \param   u32DeviceHandle : [IN] unique identifier to AAP device
    * \param   enKeyMode       : [IN] indicates keypress or keyrelease
    * \param   enKeyCode       : [IN] unique key code identifier
    * \retval  NONE
    **************************************************************************/
   t_Void vProcessKeyEvents(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
         tenKeyCode enKeyCode) const;

   /***************************************************************************
    ** FUNCTION: virtual t_Void spi_tclAAPInputHandler::vSelectDevice()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSelectDevice(const t_U32 cou32DevId,
    *                 const tenDeviceConnectionReq coenConnReq,
    *                 const tenDeviceCategory coenDevCat,
    *                 const trUserContext& corfrcUsrCntxt)
    * \brief   To setup Video related info when a device is selected or
    *          de selected.
    * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
    * \pram    coenConnReq : [IN] Identifies the Connection Type.
    * \pram    coenDevCat  : [IN] Identifies the Connection Request.
    * \retval  t_Void
    **************************************************************************/
   t_Void vSelectDevice(const t_U32 cou32DevId,
         const tenDeviceConnectionReq coenConnReq);
   /***************************************************************************
    ** FUNCTION:  t_Void  spi_tclAAPInputHandler::vRegisterInputCallbacks()
    ***************************************************************************/
   /*!
    * \fn      t_Void vRegisterVideoCallbacks(const trInputCallbacks& corfrInputCallbacks)
    * \brief   To Register for the asynchronous responses that are required from
    *          ML/DiPo/AAP Video
    * \param   corfrVideoCallbacks : [IN] Input callabcks structure
    * \retval  t_Void
    **************************************************************************/
   t_Void vRegisterInputCallbacks(const trInputCallbacks& corfrInputCallbacks);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPInputHandler::vOnSelectDeviceResult()
    ***************************************************************************/
   /*!
    * \fn      t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
    *                 const tenDeviceConnectionReq coenConnReq,
    *                 const tenResponseCode coenRespCode)
    * \brief   To perform the actions that are required, after the select device is
    *           successful/failed
    * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
    * \pram    coenConnReq : [IN] Identifies the Connection Request.
    * \pram    coenRespCode: [IN] Response code. Success/Failure
    * \retval  t_Void
    **************************************************************************/
   t_Void vOnSelectDeviceResult(const t_U32 cou32DevId,
         const tenDeviceConnectionReq coenConnReq,
         const tenResponseCode coenRespCode);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPInputHandler::bInitInputSession()
    ***************************************************************************/
   /*!
    * \fn      t_Void bInitInputSession()
    * \brief   To initialize Input Session with display properties
    * \pram    None
    * \retval  t_Bool
    **************************************************************************/
    t_Bool bInitInputSession();
	
     /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPInputHandler::vProcessKnobKeyEvents
    ***************************************************************************/
    /*!
    * \fn    vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCount)
    * \brief   Receives knob key events and forwards it to
    further handlers for processing
    * \param   u32DeviceHandle : [IN] unique identifier to AAP Server
    * \param   s8EncoderDeltaCount : [IN] encoder delta count
    * \retval  NONE
    **************************************************************************/
    t_Void vProcessKnobKeyEvents(t_U32 u32DeviceHandle,t_S8 s8EncoderDeltaCnt) const;

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPRespVideo::vVideoConfigCallback()
    ***************************************************************************/
    /*!
    * \fn      virtual t_Void vVideoConfigCallback()
    * \brief   Method to update video configuration received from MD
    * \param   s32LogicalUIWidth : [IN] Logical UI width
    * \param   s32LogicalUIHeight: [IN] Logical UI Height
    * \retval  t_Void
    **************************************************************************/
    t_Void vVideoConfigCallback(t_S32 s32LogicalUIWidth, t_S32 s32LogicalUIHeight);

   /***************************************************************************
    ****************************END OF PUBLIC***********************************
    ***************************************************************************/
protected:

   /***************************************************************************
    *********************************PROTECTED**********************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputHandler (const spi_tclAAPInputHandler...
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPInputHandler( const spi_tclAAPInputHandler& corfoSrc)
    * \brief   Copy constructor - Do not allow the creation of copy constructor
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPInputHandler()
    ***************************************************************************/
   spi_tclAAPInputHandler(const spi_tclAAPInputHandler& corfoSrc);

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPInputHandler& operator=( const spi_tclAAPIn...
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPInputHandler& operator=(const spi_tclAAPInputHandler& corfoSrc))
    * \brief   Assignment operator
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPInputHandler(const spi_tclAAPInputHandler& otrSrc)
    ***************************************************************************/
   spi_tclAAPInputHandler& operator=(const spi_tclAAPInputHandler& corfoSrc);

   /***************************************************************************
    ****************************END OF PROTECTED********************************
    ***************************************************************************/

private:

   /***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/

   spi_tclAAPManager* m_poAAPmanager;

   //! Input Callbacks structure
   trInputCallbacks m_rInputCallbacks;

   //! Structure containing Scaling attributes
   trScalingAttributes m_rScalingAttributes;
   /***************************************************************************
    ****************************END OF PRIVATE *********************************
    ***************************************************************************/

};

#endif //_SPI_TCLAAPINPUTHANDLER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>

