  /*!
 *******************************************************************************
 * \file         spi_tclAAPBluetooth.h
 * \brief        AAP Bluetooth class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    AAP Bluetooth handler class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 04.03.2015 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 19.05.2015 |  Ramya Murthy (RBEI/ECP2)         | Added vOnBTDeviceNameUpdate()
 18.08.2015 |  Ramya Murthy (RBEI/ECP2)         | Revised BT logic based on new BTSettings interfaces
 
 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLAAPBLUETOOTH_H_
#define _SPI_TCLAAPBLUETOOTH_H_

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include "AAPTypes.h"
#include "spi_BluetoothTypedefs.h"
#include "spi_tclBluetoothDevBase.h"
#include "spi_tclAAPRespBluetooth.h"
#include "Lock.h"

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/* Forward Declarations. */
class spi_tclBluetoothIntf;
class spi_tclBluetoothPolicyBase;
class spi_tclAAPCmdBluetooth;

/*!
* \class spi_tclAAPBluetooth
* \brief This is the AAP Bluetooth class that handles the Bluetooth
*        connection logic during a AAP device session
*/
class spi_tclAAPBluetooth : public spi_tclBluetoothDevBase, public spi_tclAAPRespBluetooth
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPBluetooth::spi_tclAAPBluetooth(...)
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPBluetooth(spi_tclBluetoothIntf* poBTInterface,
   *             spi_tclBluetoothPolicyBase* poBTPolicyBase)
   * \brief   Parameterized Constructor
   * \param   [IN] poBTInterface: Pointer to Bluetooth manager interface
   * \param   [IN] poBTPolicyBase: Pointer to Bluetooth policy
   ***************************************************************************/
   spi_tclAAPBluetooth(spi_tclBluetoothIntf* poBTInterface,
         spi_tclBluetoothPolicyBase* poBTPolicyBase);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPBluetooth::~spi_tclAAPBluetooth();
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPBluetooth()
   * \brief   Virtual Destructor
   ***************************************************************************/
   virtual ~spi_tclAAPBluetooth();

   /*********Start of functions overridden from spi_tclBluetoothDevBase*******/

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bInitialise();
    ***************************************************************************/
   /*!
    * \fn      bInitialise()
    * \brief   Method to initialises the service handler. (Performs initialisations which
    *          are not device specific.)
    *          Optional interface to be implemented.
    * \retval  t_Bool: TRUE - If ServiceHandler is initialised successfully, else FALSE.
    * \sa      bUninitialise()
    ***************************************************************************/
   t_Bool bInitialise();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when SelectDevice request is received by SPI.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \retval  None
   **************************************************************************/
   t_Void vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenResponseCode enRespCode)
   * \brief   Called when SelectDevice operation is complete, with the result
   *          of the operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enRespCode: Response code enumeration
   * \retval  None
   **************************************************************************/
   t_Void vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenResponseCode enRespCode);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceRequest(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenDeviceConnectionReq enDevConnReq)
   * \brief   Called when DeselectDevice request is received by SPI.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \retval  None
   **************************************************************************/
   t_Void vOnSPIDeselectDeviceRequest(t_U32 u32ProjectionDevHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceResponse(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
   *             tenResponseCode enRespCode)
   * \brief   Called when DeselectDevice operation is complete, with the result
   *          of the operation.
   *          Mandatory interface to be implemented.
   * \param   [IN] u32ProjectionDevHandle: Unique handle of selected device
   * \param   [IN] enRespCode: Response code enumeration
   * \param   [IN] bIsDeviceSwitch: True - if a projection device switch is in progress
   * \retval  None
   **************************************************************************/
   t_Void vOnSPIDeselectDeviceResponse(t_U32 u32ProjectionDevHandle,
         tenResponseCode enRespCode,
         t_Bool bIsDeviceSwitch);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTDeviceSwitchAction(t_U32...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
   *             t_U32 u32ProjectionDevHandle, tenBTChangeInfo enBTChange)
   * \brief   Called with user action when there is a device switch occurring
   *          between a Projection and a BT device.
   *          Mandatory interface to be implemented.
   * \param  [IN] u32BluetoothDevHandle  : Uniquely identifies a Bluetooth Device.
   * \param  [IN] u32ProjectionDevHandle : Uniquely identifies a Projection Device.
   * \param  [IN] enInitialBTChange  : Ongoing device change type
   * \param  [IN] enFinalBTChange  : Indicates user's device change action
   * \retval  None
   **************************************************************************/
   t_Void vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
         t_U32 u32ProjectionDevHandle,
         tenBTChangeInfo enInitialBTChange,
         tenBTChangeInfo enFinalBTChange);

   /*********End of functions overridden from spi_tclBluetoothDevBase*********/

   /********Start of functions overridden from spi_tclAAPRespBluetooth********/

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPRespBluetooth::vPostBTPairingRequest()
    ***************************************************************************/
   /*!
    * \fn      t_Void vPostBTPairingRequest()
    * \brief   Called when Pairing request is sent by AAP device.
    *          Mandatory interface to be implemented.
    * \param   [IN] szAAPBTAddress: BT address of AAP device
    * \param   [IN] enPairingMethod: Pairing method selected by AAP device
    * \sa      None
    **************************************************************************/
   t_Void vPostBTPairingRequest(t_String szAAPBTAddress,
         tenBTPairingMethod enPairingMethod);

   /**********End of functions overridden from spi_tclAAPRespBluetooth********/

   /**************************************************************************
   ****************************END OF PUBLIC**********************************
   **************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /*********Start of functions overridden from spi_tclBluetoothDevBase*******/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTConnectionResult(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTConnectionResult(t_String szBTDeviceAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Interface to receive result of a BT device connection/disconnection request.
   * \param   [IN] szBTDeviceAddress: Bluetooth address of device
   *          Mandatory interface to be implemented.
   * \param   [IN] enBTConnResult: Connection/Disconnection result.
   * \retval  None
   **************************************************************************/
   t_Void vOnBTConnectionResult(t_String szBTDeviceAddress,
         tenBTConnectionResult enBTConnResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTConnectionChanged(t_String...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTConnectionChanged(t_String szBTDeviceAddress,
   *             tenBTConnectionResult enBTConnResult)
   * \brief   Interface to receive notification on a BT device connection/disconnection.
   *          Mandatory interface to be implemented.
   * \param   [IN] szBTDeviceAddress: Bluetooth address of device
   * \param   [IN] enBTConnResult: Connection/Disconnection result.
   * \retval  None
   **************************************************************************/
   t_Void vOnBTConnectionChanged(t_String szBTDeviceAddress,
         tenBTConnectionResult enBTConnResult);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTDeviceNameUpdate(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTDeviceNameUpdate()
   * \brief   Interface to receive notification on device name of BT paired devices.
   *          Optional interface to be implemented.
   * \param   [IN] szBTDeviceAddress: BT Address of paired device
   * \param   [IN] szBTDeviceName: BT Device name of paired device
   * \retval  None
   **************************************************************************/
   t_Void vOnBTDeviceNameUpdate(t_String szBTDeviceAddress,
         t_String szBTDeviceName);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTPairableMode(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTPairableMode()
   * \brief   Interface to receive notification on state of HU BT stack.
   *          Optional interface to be implemented.
   * \param   [IN] rBTPairiableModeInfo: Info on pairiable and connectable state
   * \retval  None
   **************************************************************************/
   t_Void vOnBTPairableMode(trBTPairiableModeInfo rBTPairiableModeInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTPairingInfo(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTPairingInfo()
   * \brief   Interface to receive notification when there is a pairing request to HU.
   *          Optional interface to be implemented.
   * \param   [IN] rBTPairingReqInfo: Info about pairing request
   * \retval  None
   **************************************************************************/
   t_Void vOnBTPairingInfo(trBTPairingRequestInfo rBTPairingReqInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTOnOffChanged(...)
   ***************************************************************************/
   /*!
   * \fn      vOnBTOnOffChanged()
   * \brief   Interface to receive notification when there is a  to HU.
   *          Optional interface to be implemented.
   * \param   [IN] bBluetoothOn: True if BT is switched On, else False
   * \retval  None
   **************************************************************************/
   t_Void vOnBTOnOffChanged(t_Bool bBluetoothOn);

   /*********End of functions overridden from spi_tclBluetoothDevBase*********/

   /***************************************************************************
   ******************************END OF PROTECTED******************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
    ** FUNCTION: spi_tclAAPBluetooth(const spi_tclAAPBluetooth &rfcoBluetooth)
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPBluetooth(const spi_tclAAPBluetooth &rfcoBluetooth)
    * \brief   Copy constructor not implemented hence made private
    **************************************************************************/
   spi_tclAAPBluetooth(const spi_tclAAPBluetooth &rfcoBluetooth);

   /***************************************************************************
    ** FUNCTION: const spi_tclAAPBluetooth & operator=(
    **                                 const spi_tclAAPBluetooth &rfcoBluetooth);
    ***************************************************************************/
   /*!
    * \fn      const spi_tclAAPBluetooth & operator=(const spi_tclAAPBluetooth &rfcoBluetooth);
    * \brief   assignment operator not implemented hence made private
    **************************************************************************/
   const spi_tclAAPBluetooth & operator=(
            const spi_tclAAPBluetooth &rfcoBluetooth);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vRegisterBTCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterBTCallbacks()
   * \brief   Registers callbacks to Bluetooth client.
   * \retval  None
   **************************************************************************/
   t_Void vRegisterBTCallbacks();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vRegisterBTPairInfoCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterBTPairInfoCallbacks()
   * \brief   Registers pairing info callbacks to Bluetooth client.
   * \retval  None
   **************************************************************************/
   t_Void vRegisterBTPairInfoCallbacks();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vRegisterBTDeviceNameCallback()
   ***************************************************************************/
   /*!
   * \fn      vRegisterBTDeviceNameCallback()
   * \brief   Registers callbacks to Bluetooth client.
   * \retval  None
   **************************************************************************/
   t_Void vRegisterBTDeviceNameCallback(const t_String& rfcszAAPBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bValidateBTDisconnectionRequired()
   ***************************************************************************/
   /*!
   * \fn      bValidateBTDisconnectionRequired()
   * \brief   Validates if disconnection of a BT device is required.
   * \retval  Bool: TRUE - if BT disconnection is required, else FALSE.
   **************************************************************************/
   t_Bool bValidateBTDisconnectionRequired();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bTriggerBTDeviceDisconnection()
   ***************************************************************************/
   /*!
   * \fn      bTriggerBTDeviceDisconnection()
   * \brief   Triggers disconnection of active BT device.
   * \retval  Bool: TRUE - if BT device disconnection is triggered, else FALSE
   **************************************************************************/
   t_Bool bTriggerBTDeviceDisconnection();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTConnectionResult(...)
   ***************************************************************************/
   /*!
   * \fn      vTriggerDeviceSwitchEvent(tenBTChangeInfo enBTChangeInfo)
   * \brief   Triggers a switch between AAP and BT device
   * \param   enBTChangeInfo [IN]: Describes type of device switch
   * \retval  None
   **************************************************************************/
   t_Void vTriggerDeviceSwitchEvent(tenBTChangeInfo enBTChangeInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vHandleBTtoAAPSwitch(tenBTChangeInfo...)
   ***************************************************************************/
   /*!
   * \fn      vHandleBTtoAAPSwitch(tenBTChangeInfo enBTChangeAction)
   * \brief   Implements logic to switch from BT to AAP device
   * \param   enBTChangeAction [IN]: Describes type of BT change
   * \retval  None
   **************************************************************************/
   t_Void vHandleBTtoAAPSwitch(tenBTChangeInfo enBTChangeAction);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vHandleBTtoAAPSwitch(tenBTChangeInfo...)
   ***************************************************************************/
   /*!
   * \fn      vHandleAAPtoBTSwitch(tenBTChangeInfo enBTChangeAction)
   * \brief   Implements logic to switch from AAP to BT device
   * \param   enBTChangeAction [IN]: Describes type of BT change
   * \retval  None
   **************************************************************************/
   t_Void vHandleAAPtoBTSwitch(tenBTChangeInfo enBTChangeAction);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vStartBTPairing()
   ***************************************************************************/
   /*!
   * \fn      vStartBTPairing(const t_String& rfcoszBTAddress)
   * \brief   Starts BT pairing of AAP device
   * \param   rfcoszBTAddress [IN]: BT address of AAP device to be paired
   * \retval  None
   **************************************************************************/
   t_Void vStartBTPairing(const t_String& rfcoszBTAddress);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vStopBTPairing()
   ***************************************************************************/
   /*!
   * \fn      vStopBTPairing()
   * \brief   Stops BT pairing of AAP device
   * \retval  None
   **************************************************************************/
   t_Void vStopBTPairing();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bIsHUInDefaultState
    ***************************************************************************/
   /*!
   * \fn      bIsHUInDefaultState()
   * \brief   Validates if HU is in default state
   * \retval  True - if HU is in default state, else False
   **************************************************************************/
   t_Bool bIsHUInDefaultState();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bIsHUInConnectableState
    ***************************************************************************/
   /*!
   * \fn      bIsHUInConnectableState()
   * \brief   Validates if HU is in connectable state for AAP device
   * \retval  True - if HU is in connectable state, else False
   **************************************************************************/
   t_Bool bIsHUInConnectableState();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bIsHUInPairableConnectableState
    ***************************************************************************/
   /*!
   * \fn      bIsHUInPairableConnectableState()
   * \brief   Validates if HU is in pairable and connectable state
   * \retval  True - if HU is in pairable and connectable state, else False
   **************************************************************************/
   t_Bool bIsHUInPairableConnectableState();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vStopBTPairing()
   ***************************************************************************/
   /*!
   * \fn      enGetPairingStatus()
   * \brief   Interface to read current pairing status of AAP device
   * \retval  tenDevicePairingStatus
   **************************************************************************/
   tenDevicePairingStatus enGetPairingStatus();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vSetPairingStatus()
   ***************************************************************************/
   /*!
   * \fn      vSetPairingStatus()
   * \brief   Interface to set pairing status of AAP device
   * \param   enPairingStatus : [IN] Pairing state enum
   **************************************************************************/
   t_Void vSetPairingStatus(tenDevicePairingStatus enPairingStatus);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vRestoreHUDefaultState()
   ***************************************************************************/
   /*!
   * \fn      vRestoreHUDefaultState()
   * \brief   Resets the HU BT stack state to default condition
   * \retval  None
   **************************************************************************/
   t_Void vRestoreHUDefaultState();

   /***************************************************************************
   ** FUNCTION:   t_Void spi_tclDiPoBluetooth::vDeselectAAPDevice()
   ***************************************************************************/
   /*!
   * \fn      vDeselectAAPDevice()
   * \brief   Triggers deselection request for AAP device
   * \retval  None
   **************************************************************************/
   t_Void vDeselectAAPDevice();

   /***************************************************************************
   ** FUNCTION:   t_Void spi_tclAAPBluetooth::vSendDevicePairingResponse()
   ***************************************************************************/
   /*!
   * \fn      vSendDevicePairingResponse(t_Bool bReadyToPair)
   * \brief   Sends a BTPairingResponse to AAP device
   * \param   bReadyToPair [IN]: True - if HU is ready to pair, else False
   * \retval  None
   **************************************************************************/
   t_Void vSendDevicePairingResponse(t_Bool bReadyToPair);

   /***************************************************************************
   ** FUNCTION:   t_Bool spi_tclAAPBluetooth::bCompareBTDevices()
   ***************************************************************************/
   /*!
   * \fn      bCompareBTDevices()
   * \brief   Compares BT Address of AA P device and other device to determine if they are same.
   * \param   None
   * \retval  True - if BT devices are same, else False.
   **************************************************************************/
   t_Bool bCompareBTDevices(const t_String& rfcszAAPBTDevAddress,
         const t_String& rfcszBTDevAddress);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vPrepareHUForPairing(...)
   ***************************************************************************/
   /*!
   * \fn      vPrepareHUForPairing()
   * \brief   Makes HU ready for pairing with AA device
   * \param   None
   * \retval  None
   **************************************************************************/
   t_Void vPrepareHUForPairing();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPBluetooth::vPrepareHUForReconnection(...)
   ***************************************************************************/
   /*!
   * \fn      vPrepareHUForReconnection()
   * \brief   Makes HU ready for BT re-connection with AA device
   * \param   None
   * \retval  None
   **************************************************************************/
   t_Void vPrepareHUForReconnection();

   /***************************************************************************
   * ! Data members
   ***************************************************************************/

   /***************************************************************************
   ** BT Manager interface pointer
   ***************************************************************************/
   spi_tclBluetoothIntf* const   m_cpoBTInterface;

   /***************************************************************************
   ** BT PolicyBase pointer
   ***************************************************************************/
   spi_tclBluetoothPolicyBase* const   m_cpoBTPolicyBase;

   /***************************************************************************
   ** Pairing state of HU
   ***************************************************************************/
   trBTPairiableModeInfo   m_rPairableModeInfo;

   /***************************************************************************
   ** Pairing status of AAP device
   ***************************************************************************/
   tenDevicePairingStatus  m_enPairingStatus;

   /***************************************************************************
   ** Lock object for device pairing status
   ***************************************************************************/
   Lock  m_oPairingStateLock;

   /***************************************************************************
   ** Lock object for HU BT stack state
   ***************************************************************************/
   Lock  m_oHUBTStateLock;

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/

};

#endif // _SPI_TCLAAPBLUETOOTH_H_

