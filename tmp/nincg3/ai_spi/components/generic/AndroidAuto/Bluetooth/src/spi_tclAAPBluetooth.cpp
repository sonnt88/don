  /*!
 *******************************************************************************
 * \file         spi_tclAAPBluetooth.cpp
 * \brief        AAP Bluetooth class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    AAP Bluetooth handler class for SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                           | Modifications
 04.03.2015 |  Ramya Murthy (RBEI/ECP2)         | Initial Version
 26.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
 18.08.2015 |  Ramya Murthy (RBEI/ECP2)         | Revised BT logic based on new BTSettings interfaces
 16.02.2016 |  Ramya Murthy (RBEI/ECP2)         | Changes to switch ON BT when AA device is activated (Fix for NCG3D-5752)

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclBluetoothIntf.h"
#include "spi_tclBluetoothPolicyBase.h"
#include "spi_tclAAPCmdBluetooth.h"
#include "spi_tclAAPManager.h"
#include "spi_tclAAPBluetooth.h"

#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_BLUETOOTH
      #include "trcGenProj/Header/spi_tclAAPBluetooth.cpp.trc.h"
   #endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/
static const tenBTSetPairingMethod scenBTSetPreferredAAPPairingMethod =
      e8PAIRING_TYPE_SSP_NUMERIC_COMPARISON;

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclAAPBluetooth::spi_tclAAPBluetooth();
***************************************************************************/
spi_tclAAPBluetooth::spi_tclAAPBluetooth(spi_tclBluetoothIntf* poBTInterface,
      spi_tclBluetoothPolicyBase* poBTPolicyBase)
      : m_cpoBTInterface(poBTInterface),
        m_cpoBTPolicyBase(poBTPolicyBase),
        m_enPairingStatus(e8DEVICE_PAIRING_NOT_STARTED)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth() entered "));
   SPI_NORMAL_ASSERT(NULL == m_cpoBTInterface);
   SPI_NORMAL_ASSERT(NULL == m_cpoBTPolicyBase);

   spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      //! Register with AAP manager for BT callbacks
      poAAPManager->bRegisterObject((spi_tclAAPRespBluetooth*)this);
   }//if(NULL != poAAPManager)

} //!end of spi_tclAAPBluetooth()

/***************************************************************************
** FUNCTION:  spi_tclAAPBluetooth::~spi_tclAAPBluetooth();
***************************************************************************/
spi_tclAAPBluetooth::~spi_tclAAPBluetooth()
{
   ETG_TRACE_USR1(("~spi_tclAAPBluetooth() entered "));
} //!end of ~spi_tclAAPBluetooth()

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bInitialise();
 ***************************************************************************/
t_Bool spi_tclAAPBluetooth::bInitialise()
{
   //! Register to BT client
   vRegisterBTPairInfoCallbacks();
   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceRequest(t_U32...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceRequest(t_U32 u32ProjectionDevHandle)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnSPISelectDeviceRequest() entered "));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);

   vRegisterBTCallbacks();

   //@Note: BT connection is handled after BT PairingRequest is received from AAP

   if (NULL != m_cpoBTInterface)
   {
      //! Set Device status - this is in order to prevent SwitchDevice during ongoing selection
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SELECTION_IN_PROGRESS);

      //! Initialize BT Endpoint
      spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
      spi_tclAAPCmdBluetooth* poCmdBluetooth = (NULL != poAAPManager)?
            (poAAPManager->poGetBluetoothInstance()) : (NULL);
      t_Bool bInitSuccess = false;

      if ((NULL != poCmdBluetooth) && (NULL != m_cpoBTPolicyBase))
      {
         t_String szVehicleBTAddress;
         m_cpoBTPolicyBase->vGetVehicleBTAddress(szVehicleBTAddress);
         bInitSuccess = poCmdBluetooth->bInitialiseBTEndpoint(
               szVehicleBTAddress, scenPreferredAAPBTPairingMethod);
      }

      //! send success result for SelectDevice
      m_cpoBTInterface->vSendSelectDeviceResult(bInitSuccess);
   }//if (NULL != m_cpoBTInterface)
} //!end of vOnSPISelectDeviceRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceResponse(t_U32...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceResponse(t_U32 u32ProjectionDevHandle,
      tenResponseCode enRespCode)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnSPISelectDeviceResponse() entered "));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);

   if (NULL != m_cpoBTInterface)
   {
      //! Clear device status
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
   }//if (NULL != m_cpoBTInterface)


   //@Note: If Selection is successful, A2DP is disabled after BTPairingRequest is received from AAP
   
   //! On selection failure, perform cleanup.
   if (e8FAILURE == enRespCode)
   {
      spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
      spi_tclAAPCmdBluetooth* poCmdBluetooth = (NULL != poAAPManager)?
            (poAAPManager->poGetBluetoothInstance()) : (NULL);
      if (NULL != poCmdBluetooth)
      {
         poCmdBluetooth->vUninitialiseBTEndpoint();
      }

      vRestoreHUDefaultState();

   }//else if (e8FAILURE == enRespCode)
} //!end of vOnSPISelectDeviceResponse()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceRequest(t_U32...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnSPIDeselectDeviceRequest(t_U32 u32ProjectionDevHandle)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnSPIDeselectDeviceRequest() entered "));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);
   if (NULL != m_cpoBTInterface)
   {
      // Set Device status - this is in order to prevent SwitchDevice during ongoing deselection
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_DESELECTION_IN_PROGRESS);
      // Nothing else to be done. Simply send success result.
      m_cpoBTInterface->vSendSelectDeviceResult(true);
   }
} //!end of vOnSPIDeselectDeviceRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnSPISelectDeviceResponse(t_U32...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnSPIDeselectDeviceResponse(t_U32 u32ProjectionDevHandle,
      tenResponseCode enRespCode,
      t_Bool bIsDeviceSwitch)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnSPIDeselectDeviceResponse() entered "));
   SPI_INTENTIONALLY_UNUSED(u32ProjectionDevHandle);
   SPI_INTENTIONALLY_UNUSED(enRespCode);
   SPI_INTENTIONALLY_UNUSED(bIsDeviceSwitch);

    // Clear device status
   if (NULL != m_cpoBTInterface)
   {
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
   }

   //! Perform cleanup
   spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
   spi_tclAAPCmdBluetooth* poCmdBluetooth = (NULL != poAAPManager)?
         (poAAPManager->poGetBluetoothInstance()) : (NULL);
   if (NULL != poCmdBluetooth)
   {
      poCmdBluetooth->vUninitialiseBTEndpoint();
   }

   //! Cancel pairing if ongoing
   vStopBTPairing();

   vRestoreHUDefaultState();

   //! Unblock all BT devices (auto-connection not required since AAP device will be BT connected)
   if (NULL != m_cpoBTPolicyBase)
   {
      m_cpoBTPolicyBase->bUnblockBTDevice(e8UNBLOCK_ALL_DEVICES, "");
   }

} //!end of vOnSPIDeselectDeviceResponse()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTDeviceSwitchAction(t_U32...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnBTDeviceSwitchAction(t_U32 u32BluetoothDevHandle,
      t_U32 u32ProjectionDevHandle,
      tenBTChangeInfo enInitialBTChange,
      tenBTChangeInfo enFinalBTChange)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnInvokeBTDeviceAction() entered: BT DevHandle = 0x%x, Projection DevHandle = 0x%x ",
         u32BluetoothDevHandle, u32ProjectionDevHandle));
   ETG_TRACE_USR4(("[PARAM]vOnInvokeBTDeviceAction: InitialBTChange = %d, enFinalBTChange = %d ",
         ETG_ENUM(BT_CHANGE_INFO, enInitialBTChange), ETG_ENUM(BT_CHANGE_INFO, enFinalBTChange)));

   //! Handle requested device switch action only if device change is in progress
   //! (i.e. if SPI had previously sent a device switch event)
   if (
      (NULL != m_cpoBTInterface)
      &&
      (e8DEVICE_SWITCH_IN_PROGRESS == m_cpoBTInterface->enGetSelectedDevStatus())
      )
   {
      //! Handle switch from BT device to AAP device.
      //! @Note: When switch from BT to AAP device is triggered, if user selects to:
      //! a) Switch to AAP device - BT device should be disconnected in order to
      //!    continue with AAP device activation.
      //! b) Cancel switch operation - AAP device selection should be canceled, and
      //!    BT connection should remain unchanged.

      if (e8SWITCH_BT_TO_AAP == enInitialBTChange)
      {
         vHandleBTtoAAPSwitch(enFinalBTChange);
      }//if (e8SWITCH_BT_TO_AAP == enInitialBTChange)
      else if (e8SWITCH_AAP_TO_BT == enInitialBTChange)
      {
         vHandleAAPtoBTSwitch(enFinalBTChange);
      }//else if (e8SWITCH_BT_TO_AAP == enInitialBTChange)
   } //if ((NULL != m_cpoBTInterface)&&...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnInvokeBTDeviceAction: Null ptr/Device Switch not in progress. "));
   }
} //!end of vOnBTDeviceSwitchAction()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vPostBTPairingRequest(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vPostBTPairingRequest(t_String szAAPBTAddress,
      tenBTPairingMethod enPairingMethod)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vPostBTPairingRequest() entered: "
         "PairingMethod = %d, AAPBTAddress = %s ",
         ETG_ENUM(AAP_BTPAIRING_METHOD, enPairingMethod), szAAPBTAddress.c_str()));

   //! Store BT address of AAP device
   //! IMPORTANT! This must be done first, since in following logic, other functions will use the stored BT address.
   tenDeviceStatus enSelectedDevStatus = e8DEVICE_CHANGE_COMPLETE;
   if (NULL != m_cpoBTInterface)
   {
      m_cpoBTInterface->vSetSelectedDevBTAddress(szAAPBTAddress);
      enSelectedDevStatus = m_cpoBTInterface->enGetSelectedDevStatus();
   }

   //! Stop any ongoing pairing session
   vStopBTPairing();

   //! Send initial response as not ready (response will be sent later based on success of pairing initiation)
   //! @Note: Pairing state of device is not relevant when sending delayed status.
   vSendDevicePairingResponse(false);

   vPrepareHUForPairing();

   //! Check if device name is available
   if (true == IS_VALID_BT_ADDRESS(szAAPBTAddress))
   {
      t_String szBTDeviceName = (NULL != m_cpoBTPolicyBase) ?
            (m_cpoBTPolicyBase->szGetBTDeviceName(szAAPBTAddress)) : ("");

      //! If device is already paired, forward BT Device name of active AAP device
      if ((false == szBTDeviceName.empty()) && (NULL != m_cpoBTInterface))
      {
         m_cpoBTInterface->vSendBTDeviceName(m_cpoBTInterface->u32GetSelectedDevHandle(), szBTDeviceName);
      }//if ((false == szBTDeviceName.empty()) && (NULL != m_cpoBTInterface))

      //! Register for device name callback (to get device name update if device is not paired,
      //! or gets unpaired in future)
      vRegisterBTDeviceNameCallback(szAAPBTAddress);
   }
} //!end of vPostBTPairingRequest()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vPrepareHUForPairing(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vPrepareHUForPairing()
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vPrepareHUForPairing() entered "));

   //! Validate next action to be taken for pairing/connection
   if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))
   {
      t_String szAAPBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();
      if (true == IS_VALID_BT_ADDRESS(szAAPBTAddress))
      {
         tenDeviceStatus enSelectedDevStatus = m_cpoBTInterface->enGetSelectedDevStatus();

         //! Switch ON Bluetooth if it is OFF
         if (false == m_cpoBTPolicyBase->bGetBTOnStatus())
         {
            ETG_TRACE_USR2(("[DESC]:vPrepareHUForPairing: BT is OFF. Switch on BT "));

            //! Set pairing status
            vSetPairingStatus(e8DEVICE_PAIRING_REQUESTED);

            m_cpoBTPolicyBase->bSwitchBTOnOff(true);
         }
         //! If BT is ON, check if AA device is paired and/or connected
         else if (true == m_cpoBTPolicyBase->bGetConnectionStatus(szAAPBTAddress)) /*If AAP device is BT connected*/
         {
            ETG_TRACE_USR2(("[DESC]:vPrepareHUForPairing: MD is BT connected. Send pairing response to MD and block other devices."));

            //Send response as ready to pair (without initiating pairing)
            vSendDevicePairingResponse(true);

            //! Disable BT A2DP profile of connected device
            m_cpoBTPolicyBase->vSetBTProfile(e8A2DP_DISABLED);

            //Block other BT devices
            if (e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus)
            {
               m_cpoBTPolicyBase->bBlockBTDevice(e8BLOCK_ALL_EXCEPT, szAAPBTAddress);
            }
         }
         else /*If AAP device is not BT connected*/
         {
            ETG_TRACE_USR2(("[DESC]:vPrepareHUForPairing: MD is not BT connected. Make HU ready for pairing."));

            //! Set pairing status
            vSetPairingStatus(e8DEVICE_PAIRING_REQUESTED);

            //! Unblock all BT devices (since pairing cannot occur when blocking is active)
            m_cpoBTPolicyBase->bUnblockBTDevice(e8UNBLOCK_ALL_DEVICES, "");

            if (e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus)
            {
               //! If other BT connections exist, trigger DeviceSwitch event to HMI.
               //! Else start BT Pairing process
               (true == bValidateBTDisconnectionRequired()) ?
                     (vTriggerDeviceSwitchEvent(e8SWITCH_BT_TO_AAP)) :
                     (vStartBTPairing(szAAPBTAddress));
            }//if (e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus)
            else
            {
               ETG_TRACE_USR2(("[DESC]:vPrepareHUForPairing: No action taken due to ongoing device change/switch "));
            }
         }
      }//if (true == IS_VALID_BT_ADDRESS(szAAPBTAddress))
   }//if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))

} //!end of vPrepareHUForPairing()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vPrepareHUForReconnection(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vPrepareHUForReconnection()
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vPrepareHUForReconnection() entered "));

   //@Note:
   //(1) This is called whenever BT is switched Off or AA BT device is disconnected.
   //(2) When BT is switched Off, AA BT disconnection is reported before BT Off.
   //    Since we cannot know whether BT is Off, we should try to make HU connectable.
   //    If it fails, we retry when BT is turned On.

   if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))
   {
      t_String szAAPBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();
      tenDeviceStatus enSelectedDevStatus = m_cpoBTInterface->enGetSelectedDevStatus();

      if ((true == IS_VALID_BT_ADDRESS(szAAPBTAddress)) && (e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus))
      {
         //! Switch ON Bluetooth if it is OFF
         if (false == m_cpoBTPolicyBase->bGetBTOnStatus())
         {
            ETG_TRACE_USR2(("[DESC]:vPrepareHUForReconnection: BT is OFF. Switch on BT. "));

            m_cpoBTPolicyBase->bSwitchBTOnOff(true);
         }
         //! If BT is ON and AA BT is disconnected, make HU ready for connection
         else if (false == m_cpoBTPolicyBase->bGetConnectionStatus(szAAPBTAddress))
         {
            ETG_TRACE_USR2(("[DESC]:vPrepareHUForReconnection: MD is not BT connected. Make HU ready for connection."));

            //! Unblock all BT devices (since pairing cannot occur when blocking is active)
            m_cpoBTPolicyBase->bUnblockBTDevice(e8UNBLOCK_ALL_DEVICES, "");

            //! If other BT connections exist, trigger DeviceSwitch event to HMI.
            //! Else start BT Pairing process
            if (((true == bIsHUInDefaultState()) || (true == bIsHUInConnectableState())) &&
                  (false == m_cpoBTPolicyBase->bConfigureHUState(e8MODE_ON, e8MODE_ON, szAAPBTAddress)))
            {
               ETG_TRACE_ERR(("[ERR]:vPrepareHUForReconnection: Could not make HU connectable to restore AAP BT! "));
            }
         }
      }//if ((true == IS_VALID_BT_ADDRESS(szAAPBTAddress)) &&...)
      else
      {
         ETG_TRACE_USR2(("[DESC]:vPrepareHUForReconnection: No action taken due to ongoing device change/switch or inactive device "));
      }
   }//if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))

} //!end of vPrepareHUForReconnection()

/***************************************************************************
*********************************PROTECTED**********************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTConnectionResult(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnBTConnectionResult(t_String szBTDeviceAddress,
      tenBTConnectionResult enBTConnResult)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnBTConnectionResult() entered: BTConnectionResult = %d, BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult), szBTDeviceAddress.c_str()));

   //! @Note: BT Device connection/disconnection result will be received only for
   //! an SPI-triggered BT device connection/disconnection.

   if (
      (NULL != m_cpoBTInterface)
      &&
      (IS_VALID_DEVHANDLE(m_cpoBTInterface->u32GetSelectedDevHandle())) /*If AA device is active*/
      )
   {
      tenDeviceStatus enSelectedDevStatus = m_cpoBTInterface->enGetSelectedDevStatus();

      switch (enBTConnResult)
      {
         case e8BT_RESULT_DISCONNECTED:
         {
            ETG_TRACE_USR2(("[DESC]:vOnBTConnectionResult: BT device successfully disconnected. "));
            //! If disconnection success result is received during:
            //! 1. BT pairing sequence of AAP - proceed with BT pairing (if pairing was triggered by phone)
            //! 2. When user cancels switch from AAP to BT device - nothing to be done.
            //! (AAP will initiate BT pairing/connection)
            if (e8DEVICE_CHANGE_IN_PROGRESS == enSelectedDevStatus)
            {
               vStartBTPairing(m_cpoBTInterface->szGetSelectedDevBTAddress());
            }
            m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
         }
            break;
         case e8BT_RESULT_DISCONN_FAILED:
         {
            ETG_TRACE_ERR(("[ERR]:vOnBTConnectionResult: BT device disconnection failed! "));
            //! If disconnection failure result is received during any of the below cases,
            //! Deselect AAP device:
            //! 1. BT pairing sequence of AAP
            //! 2. When user cancels switch from AAP to BT device
            vDeselectAAPDevice();
         }
            break;
         case e8BT_RESULT_BLOCKED:
         {
            ETG_TRACE_USR2(("[DESC]:vOnBTConnectionResult: BT devices successfully blocked! "));
            //m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
         }
            break;
         case e8BT_RESULT_BLOCK_FAILED:
         {
            ETG_TRACE_ERR(("[ERR]:vOnBTConnectionResult: BT devices blocking failed! "));
            //TODO - deselect AAP device?
         }
            break;
         case e8BT_RESULT_UNBLOCKED:
         {
            //! Occurs when Unblock All (with or without auto-connect) is successful
            ETG_TRACE_USR2(("[DESC]:vOnBTConnectionResult: BT devices successfully unblocked. "));
         }
            break;
         case e8BT_RESULT_UNBLOCK_FAILED:
         {
            //! Occurs when Unblock All (with or without auto-connect) has failed
            ETG_TRACE_ERR(("[ERR]:vOnBTConnectionResult: BT devices unblocking failed! "));
         }
            break;
         default:
            ETG_TRACE_ERR(("[ERR]:vOnBTConnectionResult: Invalid enum encountered. "));
            break;
      } //switch (enBTConnResult)
   } //if ((NULL != m_cpoBTInterface) && ...)
   else
   {
      ETG_TRACE_ERR(("[ERR]:vOnBTConnectionResult: No device is currently active "));
   }
} //!end of vOnBTConnectionResponse()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTConnectionChanged(t_String...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnBTConnectionChanged(t_String szBTDeviceAddress,
      tenBTConnectionResult enBTConnResult)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnBTConnectionChanged() entered: BTConnectionResult = %d, BTAddress = %s ",
         ETG_ENUM(BT_CONNECTION_RESULT, enBTConnResult), szBTDeviceAddress.c_str()));

   if (
      (NULL != m_cpoBTInterface)
      &&
      (NULL != m_cpoBTPolicyBase)
      &&
      (IS_VALID_DEVHANDLE(m_cpoBTInterface->u32GetSelectedDevHandle())) /*If AA device is active*/
      )
   {
      tenDeviceStatus enSelectedDevStatus = m_cpoBTInterface->enGetSelectedDevStatus();
      t_String szAAPDevBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();

      switch (enBTConnResult)
      {
         case e8BT_RESULT_CONNECTED:
         {
            //! If BT Address of AAP device is not yet known, defer action to be taken until PairingRequest is received.
            //! If BT address of AAP device is known, trigger AAP to BT switch if BT device is different, else 
			//! block other BT devices and block AVP profile of connected device.
            if (IS_VALID_BT_ADDRESS(szAAPDevBTAddress))
            {
               t_Bool bIsAAPDevice = bCompareBTDevices(szAAPDevBTAddress, szBTDeviceAddress);
               if ((false == bIsAAPDevice) && (e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus))
               {
                  vTriggerDeviceSwitchEvent(e8SWITCH_AAP_TO_BT);
               }
               else if (true == bIsAAPDevice)
               {
                  m_cpoBTPolicyBase->bBlockBTDevice(e8BLOCK_ALL_EXCEPT, szAAPDevBTAddress);

                  //! Disable BT A2DP profile of connected device
                  m_cpoBTPolicyBase->vSetBTProfile(e8A2DP_DISABLED);
               }
            }
            else
            {
               ETG_TRACE_USR2(("[DESC]:vOnBTConnectionChanged: BT device connected, but AA BT address is unknown. "
                     "Waiting for PairingRequest to validate next action. "));
            }
         }//case e8BT_RESULT_CONNECTED:
         break;
         case e8BT_RESULT_DISCONNECTED:
         {
            //! If AAP device is BT disconnected, enable Connectable state so that phone can re-connect.
            if ((e8DEVICE_CHANGE_COMPLETE == enSelectedDevStatus) &&
                  (true == bCompareBTDevices(szAAPDevBTAddress, szBTDeviceAddress))) /*If BT and AAP devices are same*/
            {
               ETG_TRACE_USR2(("[DESC]:vOnBTConnectionChanged: AAP BT connection lost! Attempting to reconnect. "));

               vPrepareHUForReconnection();
            }
         }//case e8BT_RESULT_DISCONNECTED:
         break;
         default:
         {
            ETG_TRACE_USR2(("[DESC]:vOnBTConnectionChanged: Nothing to be done. "));
         }
            break;
      }//switch (enBTConnResult)
   }//if (NULL != m_cpoBTInterface)
} //!end of vOnBTConnectionChanged()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTDeviceNameUpdate(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnBTDeviceNameUpdate(t_String szBTDeviceAddress,
         t_String szBTDeviceName)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnBTDeviceNameUpdate() entered: BTDeviceAddress = %s ",
         szBTDeviceAddress.c_str()));

   if ((false == szBTDeviceName.empty()) && (NULL != m_cpoBTInterface))
   {
      t_String szSelDevBTAddr = m_cpoBTInterface->szGetSelectedDevBTAddress();

      if (
         (IS_VALID_BT_ADDRESS(szBTDeviceAddress))
         &&
         (IS_VALID_BT_ADDRESS(szSelDevBTAddr)) /*If BT Address of active projection device is available*/
         &&
         (szBTDeviceAddress == szSelDevBTAddr) /*If BT device is active projection device*/
         )
      {
         t_U32 u32SelProjDevHandle = m_cpoBTInterface->u32GetSelectedDevHandle();
         m_cpoBTInterface->vSendBTDeviceName(u32SelProjDevHandle, szBTDeviceName.c_str());
      }
   }//if ((false == szBTDeviceName.empty()) && (NULL != m_cpoBTInterface))
} //!end of vOnBTDeviceNameUpdate()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTPairableMode(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnBTPairableMode(trBTPairiableModeInfo rBTPairiableModeInfo)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnBTPairableMode() entered: "
         "Pairable = %d, Connectable = %d, PairingOngoing = %d, BTAddress = %s ",
         ETG_ENUM(BOOL, rBTPairiableModeInfo.bHUInPairableState),
         ETG_ENUM(BOOL, rBTPairiableModeInfo.bHUInConnectableState),
         ETG_ENUM(BOOL, rBTPairiableModeInfo.bHUInPairingOngoingState),
         rBTPairiableModeInfo.szBTAddress.c_str()));

   //! Store latest pairing mode info
   m_rPairableModeInfo = rBTPairiableModeInfo;

   if (
      (NULL != m_cpoBTInterface)
      &&
      (e8DEVICE_CHANGE_COMPLETE == m_cpoBTInterface->enGetSelectedDevStatus()) /*No device change in progress*/
      )
   {
      tenDevicePairingStatus enPairingStatus = enGetPairingStatus();

      /* Scenario: Pairing is started by SPI, and now HU is in pairable & connectable state for AAP device.
       * Inform MD that HU is ready to pair. */
      if ((e8DEVICE_PAIRING_IN_PROGRESS == enPairingStatus) /*Pairing is started by SPI*/
         && (true == bIsHUInPairableConnectableState()))
      {
         ETG_TRACE_USR2(("[DESC]:vOnBTPairableMode() entered: HU is ready for pairing. Send pairing response to MD. "));
         vSendDevicePairingResponse(true);
      }
      /* Scenario: Pairing is requested by phone, but it was not started due to ongoing pairing or connection.
       * Once HU is in default state, start AAP device pairing sequence. */
      else if ((e8DEVICE_PAIRING_REQUESTED == enPairingStatus)
               && ((true == bIsHUInDefaultState()) || (true == bIsHUInConnectableState())))
      {
         ETG_TRACE_USR2(("[DESC]:vOnBTPairableMode() entered: Ongoing pairing/connection is complete. Make HU ready for pairing. "));
         vStartBTPairing(m_cpoBTInterface->szGetSelectedDevBTAddress());
      }
   }
} //!end of vOnBTPairableMode()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTPairingInfo(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnBTPairingInfo(trBTPairingRequestInfo rBTPairingReqInfo)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnBTPairingInfo() entered: BTAddress = %s ",
         rBTPairingReqInfo.szRemoteDevBTAddr.c_str()));
   ETG_TRACE_USR4(("[PARAM]:vOnBTPairingInfo: PairingMethod = %d, PairingPin = %s ",
         ETG_ENUM(BTSET_PAIRING_METHOD, rBTPairingReqInfo.enPairingMethod),
         rBTPairingReqInfo.szPairingPin.c_str()));

   if (
      (NULL != m_cpoBTInterface)
      &&
      (NULL != m_cpoBTPolicyBase)
      &&
      (IS_VALID_DEVHANDLE(m_cpoBTInterface->u32GetSelectedDevHandle())) /*If AAP device is active*/
      )
   {
      //! If pairing is started for AAP device, accept the pairing and send PIN number to MD.
      if (
         (rBTPairingReqInfo.szRemoteDevBTAddr == m_cpoBTInterface->szGetSelectedDevBTAddress())
         &&
         (scenBTSetPreferredAAPPairingMethod == rBTPairingReqInfo.enPairingMethod)
         )
      {
         if (false == m_cpoBTPolicyBase->bSendPairingAction(e8PAIRING_ACCEPT))
         {
            ETG_TRACE_ERR(("[ERR]:vOnBTPairingInfo: Error sending Accept pairing response "));
         }

         spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
         spi_tclAAPCmdBluetooth* poCmdBluetooth = (NULL != poAAPManager)?
               (poAAPManager->poGetBluetoothInstance()) : (NULL);
         if (NULL != poCmdBluetooth)
         {
            poCmdBluetooth->vSendAuthenticationData(rBTPairingReqInfo.szPairingPin);
         }
      }
      //! If pairing is started for different device, cancel the pairing.
      else
      {
         if (false == m_cpoBTPolicyBase->bSendPairingAction(e8PAIRING_CANCEL))
         {
            ETG_TRACE_ERR(("[ERR]:vOnBTPairingInfo: Error sending Cancel pairing response "));
         }
      }

      vSetPairingStatus(e8DEVICE_PAIRING_COMPLETE);

   }//if ((NULL != m_cpoBTInterface) && ...)
} //!end of vOnBTPairingInfo()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTOnOffChanged(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vOnBTOnOffChanged(t_Bool bBluetoothOn)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vOnBTOnOffChanged() entered "));

   if (
      (NULL != m_cpoBTInterface)
      &&
      (IS_VALID_DEVHANDLE(m_cpoBTInterface->u32GetSelectedDevHandle())) /*If AA device is active*/
      )
   {
      //! If BT is switched ON, proceed with BT pairing (if requested) or BT reconnection
      if (true == bBluetoothOn)
      {
         ETG_TRACE_USR2(("[DESC]:vOnBTOnOffChanged: BT is turned ON "));
         (e8DEVICE_PAIRING_REQUESTED == enGetPairingStatus()) ?
             (vPrepareHUForPairing()) : (vPrepareHUForReconnection());
      }
      //! If BT is switched OFF, prepare for BT reconnection
      else
      {
         ETG_TRACE_USR2(("[DESC]:vOnBTOnOffChanged: BT is turned OFF "));
         vPrepareHUForReconnection();
      }
   }
} //!end of vOnBTOnOffChanged()


/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vRegisterBTCallbacks()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vRegisterBTCallbacks()
{
   if (NULL != m_cpoBTPolicyBase)
   { 
	   /*lint -esym(40,fvOnBTConnectionResult) fvOnBTConnectionResult is not referenced */
	   /*lint -esym(40,_1)_1  Undeclared identifier */
	   /*lint -esym(40,_2)_2 Undeclared identifier */
	   /*lint -esym(40,fvOnBTConnectionChanged)fvOnBTConnectionChanged Undeclared identifier */
      //!Initialize BT response callbacks structure
      trBluetoothCallbacks rBluetoothCb;
      rBluetoothCb.fvOnBTConnectionResult =
         std::bind(&spi_tclAAPBluetooth::vOnBTConnectionResult,
               this,
               SPI_FUNC_PLACEHOLDERS_2);
      rBluetoothCb.fvOnBTConnectionChanged =
         std::bind(&spi_tclAAPBluetooth::vOnBTConnectionChanged,
               this,
               SPI_FUNC_PLACEHOLDERS_2);
      rBluetoothCb.fvOnBTOnOffChanged =
         std::bind(&spi_tclAAPBluetooth::vOnBTOnOffChanged,
               this,
               SPI_FUNC_PLACEHOLDERS_1);

      m_cpoBTPolicyBase->vRegisterCallbacks(rBluetoothCb);
   }//if (NULL != m_cpoBTPolicyBase)
} //!end of vRegisterBTCallbacks()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vRegisterBTPairInfoCallbacks()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vRegisterBTPairInfoCallbacks()
{
   if (NULL != m_cpoBTPolicyBase)
   {
      /*lint -esym(40,fvOnBTPairableMode) fvOnBTPairableMode is not referenced */
      /*lint -esym(40,fvOnBTPairingInfo) fvOnBTPairingInfo is not referenced */
      /*lint -esym(40,_1)_1  Undeclared identifier */
      /*lint -esym(40,_2)_2 Undeclared identifier */
      //!Initialize BT pairing info callbacks structure
      trBluetoothPairingCallbacks rBluetoothPairingCb;
      rBluetoothPairingCb.fvOnBTPairableMode =
               std::bind(&spi_tclAAPBluetooth::vOnBTPairableMode,
               this,
               SPI_FUNC_PLACEHOLDERS_1);
      rBluetoothPairingCb.fvOnBTPairingInfo =
               std::bind(&spi_tclAAPBluetooth::vOnBTPairingInfo,
               this,
               SPI_FUNC_PLACEHOLDERS_1);

      m_cpoBTPolicyBase->vRegisterPairingInfoCallbacks(rBluetoothPairingCb);
   }//if (NULL != m_cpoBTPolicyBase)
} //!end of vRegisterBTPairInfoCallbacks()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vRegisterBTDeviceNameCallback()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vRegisterBTDeviceNameCallback(const t_String& rfcszAAPBTAddress)
{
   /*lint -esym(40,fvOnBTDeviceNameUpdate)fvOnBTDeviceNameUpdate Undeclared identifier */
   //! Register callback for BT Device name
   if (NULL != m_cpoBTPolicyBase)
   {
      trBTDeviceNameCbInfo rBTDevNameCb;
      rBTDevNameCb.szBTAddress = rfcszAAPBTAddress;
      rBTDevNameCb.fvOnBTDeviceNameUpdate =
         std::bind(&spi_tclAAPBluetooth::vOnBTDeviceNameUpdate,
               this,
               SPI_FUNC_PLACEHOLDERS_2);
      m_cpoBTPolicyBase->vRegisterDeviceNameCallback(rBTDevNameCb);
   }//if (NULL != m_cpoBTPolicyBase)
} //!end of vRegisterBTDeviceNameCallback()

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPBluetooth::bValidateBTDisconnection()
***************************************************************************/
t_Bool spi_tclAAPBluetooth::bValidateBTDisconnectionRequired()
{
   //! For AAP, BT disconnection is required only if connected BT device is NOT the AAP device.
   t_Bool bDisconnectionReq = false;

   if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))
   {
      //Fetch BT Address of connected BT device (if any)
      t_String szConnBTDevAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();
      ETG_TRACE_USR4(("[PARAM]:bValidateBTDisconnectionRequired: Retrieved connected device BTAddress = %s ",
            szConnBTDevAddr.c_str()));

      //Fetch AAP device BT address
      t_String szAAPDevBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();
      ETG_TRACE_USR4(("[PARAM]:bValidateBTDisconnectionRequired: Retrieved selected AAP device BTAddress = %s ",
            szAAPDevBTAddress.c_str()));

      if (
         (IS_VALID_BT_ADDRESS(szConnBTDevAddr)) /*If a BT device is connected*/
         &&
         //@Note: If BT address of AAP device is unavailable, ignore any BT connections
         //so that the AAP device can be connected via BT.
         (IS_VALID_BT_ADDRESS(szAAPDevBTAddress))
         &&
         (szConnBTDevAddr != szAAPDevBTAddress) /*If devices are different*/
         )
      {
         bDisconnectionReq = true;
      }
   } //if ((NULL != m_cpoBTPolicyBase) && (NULL != m_cpoBTInterface))

   ETG_TRACE_USR2(("spi_tclAAPBluetooth::bValidateBTDisconnectionRequired() left with DisconnRequired = %u ",
         ETG_ENUM(BOOL, bDisconnectionReq)));
   return bDisconnectionReq;
} //!end of bValidateBTDisconnectionRequired()

/***************************************************************************
** FUNCTION:   t_Bool spi_tclAAPBluetooth::bTriggerBTDeviceDisconnection()
***************************************************************************/
t_Bool spi_tclAAPBluetooth::bTriggerBTDeviceDisconnection()
{
   //! Request disconnection of currently connected BT device (if any connection exists)
   t_Bool bDisconnectTriggered = false;

   if (NULL != m_cpoBTPolicyBase)
   {
      //! Fetch BT Address of connected device
      t_String szConnBTDevAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();
      ETG_TRACE_USR4(("[PARAM]:bTriggerBTDeviceDisconnection: Retrieved connected device BTAddress = %s ",
            szConnBTDevAddr.c_str()));

      if (IS_VALID_BT_ADDRESS(szConnBTDevAddr)) /*If a BT device is connected*/
      {
         bDisconnectTriggered = m_cpoBTPolicyBase->bDisconnectBTDevice(szConnBTDevAddr);
      }
   } //if (NULL != m_cpoBTPolicyBase)

   ETG_TRACE_USR2(("spi_tclAAPBluetooth::bTriggerBTDeviceDisconnection() left with DisconnectTriggered = %u ",
         ETG_ENUM(BOOL, bDisconnectTriggered)));
   return bDisconnectTriggered;
} //!end of vTriggerBTDeviceDisconnection()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vOnBTConnectionResult(...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vTriggerDeviceSwitchEvent(tenBTChangeInfo enBTChangeInfo)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vTriggerDeviceSwitchEvent() entered: for switch type = %d",
         ETG_ENUM(BT_CHANGE_INFO, enBTChangeInfo)));

   if ((NULL != m_cpoBTInterface) && (NULL != m_cpoBTPolicyBase))
   {
      //! Fetch AAP device handle
      t_U32 u32ProjectionDevHandle = m_cpoBTInterface->u32GetSelectedDevHandle();

      //! Fetch BT DeviceHandle of connected device
      t_String szConnBTDevAddr = m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress();
      t_U32 u32ConnBTDevHandle = m_cpoBTPolicyBase->u32GetBTDeviceHandle(szConnBTDevAddr);
      ETG_TRACE_USR4(("[PARAM]:vTriggerDeviceSwitchEvent: "
            "Retrieved connected BT DeviceHandle = 0x%x (for BTAddress = %s) ",
           u32ConnBTDevHandle, szConnBTDevAddr.c_str()));

      //! Fetch Call Status of connected Device
      //@Note: Call Status is not required to be sent in AAP to BT switch scenario,
      //since BT is the newly connected device.
      t_Bool bCallActive = (e8SWITCH_BT_TO_AAP == enBTChangeInfo)?
            (m_cpoBTInterface->bGetCallStatus()) : (false);

      //! Store info about device change.
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_SWITCH_IN_PROGRESS);

      //! Notify HMI about switch between BT to Projection device.
      m_cpoBTInterface->vSendDeviceSwitchEvent(u32ConnBTDevHandle,
           u32ProjectionDevHandle, enBTChangeInfo, false, bCallActive);

   }//if (NULL != m_cpoBTInterface)
} //!end of vTriggerDeviceSwitchEvent()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vHandleBTtoAAPSwitch(tenBTChangeInfo...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vHandleBTtoAAPSwitch(tenBTChangeInfo enBTChangeAction)
{
   if (NULL != m_cpoBTInterface)
   {
      switch (enBTChangeAction)
      {
         case e8SWITCH_BT_TO_AAP:
         {
            //! Disconnect BT device (if any connected)
            if ((NULL != m_cpoBTPolicyBase) &&
                  (true == IS_VALID_BT_ADDRESS(m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress())))
            {
               ETG_TRACE_USR2(("[DESC]:BT to AAP switch - Attempting to disconnect BT device "));
               if (true == bTriggerBTDeviceDisconnection())
               {
                  m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_IN_PROGRESS);
                  //This status will be used to validate result of BT disconnection
                  //(i.e. whether disconnection is triggered during BT to AAP switch or not)
               }
               else
               {
                  ETG_TRACE_ERR(("[ERR]:Sending BT disconn request failed! Hence deactivating AAP device. "));
                  vDeselectAAPDevice();
               }
            }
            //! Continue with AAP device pairing.
            //! No further action required if BT is disconnected when this event is received.
            else
            {
               ETG_TRACE_USR2(("[DESC]:BT to AAP switch - No action taken since there is no BT device connected. "));
               vStartBTPairing(m_cpoBTInterface->szGetSelectedDevBTAddress());
               m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
            }
         }
            break;
         case e8NO_CHANGE:
         {
            //! Deactivate AAP device
            ETG_TRACE_USR2(("[DESC]:Cancel BT to AAP switch - Hence deactivating AAP device. "));
            vDeselectAAPDevice();
         }
            break;
         default:
            ETG_TRACE_ERR(("[ERR]:Invalid user action for BT to AAP switch. "));
            break;
      }//switch (enBTChangeAction)
   }//if (NULL != m_cpoBTInterface)
} //!end of vHandleBTtoAAPSwitch()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vHandleBTtoAAPSwitch(tenBTChangeInfo...)
***************************************************************************/
t_Void spi_tclAAPBluetooth::vHandleAAPtoBTSwitch(tenBTChangeInfo enBTChangeAction)
{
   if (NULL != m_cpoBTInterface)
   {
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);

      switch (enBTChangeAction)
      {
         case e8SWITCH_AAP_TO_BT:
         {
            //! Deselect AAP device
            ETG_TRACE_USR2(("[DESC]:AAP to BT switch - Deactivating AAP device "));
            vDeselectAAPDevice();
         }
            break;
         case e8NO_CHANGE:
         {
            //! Disconnect BT device
            if ((NULL != m_cpoBTPolicyBase) &&
                  (true == IS_VALID_BT_ADDRESS(m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress())))
            {
               ETG_TRACE_USR2(("[DESC]:Cancel AAP to BT switch - Attempting to disconnect BT device "));
               if (false == bTriggerBTDeviceDisconnection())
               {
                  ETG_TRACE_ERR(("[ERR]:Sending BT disconn request failed! Hence deactivating AAP device. "));
                  vDeselectAAPDevice();
               }
            }
            //! No further action required if BT is disconnected when this event is received.
            else
            {
               ETG_TRACE_USR2(("[DESC]:Cancel AAP to BT switch - No action taken since there is no BT device connected. "));
            }
         }
            break;
         default:
            ETG_TRACE_ERR(("[ERR]:Invalid user action for AAP to BT switch. "));
            break;
      }//switch (enBTChangeAction)
   }//if (NULL != m_cpoBTInterface)
} //!end of vHandleAAPtoBTSwitch()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vStartBTPairing()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vStartBTPairing(const t_String& rfcoszBTAddress)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vStartBTPairing() entered "));

   //TODO - stop ongoing pairing session?

   //Make HU ready for pairing
   if (
      (e8DEVICE_PAIRING_REQUESTED == enGetPairingStatus())
      &&
      (true == IS_VALID_BT_ADDRESS(rfcoszBTAddress))
      &&
      (NULL != m_cpoBTPolicyBase)
      )
   {
      if (rfcoszBTAddress != m_cpoBTPolicyBase->szGetConnectedDeviceBTAddress()) /*If AAP device is not already connected*/
      {
         if ((true == bIsHUInDefaultState()) || (true == bIsHUInConnectableState()))
         {
            ETG_TRACE_USR2(("[DESC]:vStartBTPairing: HU is in default state. Configuring to pairable connectable state "));

            if (true == m_cpoBTPolicyBase->bConfigureHUState(e8MODE_ON, e8MODE_ON, rfcoszBTAddress))
            {
               vSetPairingStatus(e8DEVICE_PAIRING_IN_PROGRESS);
            }
            else
            {
               ETG_TRACE_ERR(("[ERR]:vStartBTPairing: Pairing could not be started "));
            }
         }//if (true == bHUInDefaultState)
         else if (true == bIsHUInPairableConnectableState())
         {
            vSetPairingStatus(e8DEVICE_PAIRING_IN_PROGRESS);
            vSendDevicePairingResponse(true);
            ETG_TRACE_USR2(("[DESC]:vStartBTPairing: HU is in pairable connectable state. Nothing to be done. "));
         }
         else
         {
            ETG_TRACE_USR2(("[DESC]:vStartBTPairing: Cancelling ongoing pairing "));
            if (NULL != m_cpoBTPolicyBase)
            {
               m_cpoBTPolicyBase->bSendPairingAction(e8PAIRING_CANCEL);
            }
         }
      }
      else
      {
         vSetPairingStatus(e8DEVICE_PAIRING_NOT_STARTED);
         ETG_TRACE_ERR(("[ERR]:vStartBTPairing: Pairing not started since device is already connected "));
      }
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:vStartBTPairing: Pairing not started due to pairing not requested by device/invalid address "));
   }
} //!end of vStartBTPairing()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vStopBTPairing()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vStopBTPairing()
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vStopBTPairing() entered "));

   //! Cancel pairing if it was initiated previously
   if ((e8DEVICE_PAIRING_NOT_STARTED != enGetPairingStatus()) && (NULL != m_cpoBTPolicyBase))
   {
      m_cpoBTPolicyBase->bSendPairingAction(e8PAIRING_CANCEL);
   }
   vSetPairingStatus(e8DEVICE_PAIRING_NOT_STARTED);

} //!end of vStopBTPairing()

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bIsHUInDefaultState
 ***************************************************************************/
t_Bool spi_tclAAPBluetooth::bIsHUInDefaultState()
{
   m_oHUBTStateLock.s16Lock();

   //TODO - default state is different in PSA project. Need to read from policy.
   //@Note: Not required to check BT address in default state (GMMY17-12222)
   t_Bool bHUInDefaultState = (
            (false == m_rPairableModeInfo.bHUInPairableState) && /*HU not in pairable state*/
            (false == m_rPairableModeInfo.bHUInPairingOngoingState) && /*No pairing ongoing*/
            (false == m_rPairableModeInfo.bHUInConnectableState));

   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPBluetooth::bIsHUInDefaultState() left with %d "
         "(Current state :: Pairable = %d, Connectable = %d, PairingOngoing = %d, BTAddress = %s) ",
         ETG_ENUM(BOOL, bHUInDefaultState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInPairableState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInConnectableState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInPairingOngoingState),
         m_rPairableModeInfo.szBTAddress.c_str()));

   m_oHUBTStateLock.vUnlock();

   return bHUInDefaultState;
} //!end of bIsHUInDefaultState()

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bIsHUInDefaultState
 ***************************************************************************/
t_Bool spi_tclAAPBluetooth::bIsHUInConnectableState()
{
   t_Bool bHUInConnectableState = false;

   m_oHUBTStateLock.s16Lock();

   if (NULL != m_cpoBTInterface)
   {
      t_String szAAPBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();

      //TODO - default state is different in PSA project. Need to read from policy.
      bHUInConnectableState = (
            (false == m_rPairableModeInfo.bHUInPairableState) && /*HU not in pairable state*/
            (false == m_rPairableModeInfo.bHUInPairingOngoingState) && /*No pairing ongoing*/
            (true == m_rPairableModeInfo.bHUInConnectableState) && /*HU already in connectable state for AAP*/
            (IS_VALID_BT_ADDRESS(m_rPairableModeInfo.szBTAddress)) &&
            (szAAPBTAddress == m_rPairableModeInfo.szBTAddress));
   }

   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPBluetooth::bHUInConnectableState() left with %d "
         "(Current state :: Pairable = %d, Connectable = %d, PairingOngoing = %d, BTAddress = %s) ",
         ETG_ENUM(BOOL, bHUInConnectableState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInPairableState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInConnectableState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInPairingOngoingState),
         m_rPairableModeInfo.szBTAddress.c_str()));

   m_oHUBTStateLock.vUnlock();

   return bHUInConnectableState;
} //!end of bIsHUInConnectableState()

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPBluetooth::bIsHUInPairableConnectableState
 ***************************************************************************/
t_Bool spi_tclAAPBluetooth::bIsHUInPairableConnectableState()
{
   t_Bool bHUInPairableConnectableState = false;

   m_oHUBTStateLock.s16Lock();

   if (NULL != m_cpoBTInterface)
   {
      t_String szAAPBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();

      bHUInPairableConnectableState =
            ((true == m_rPairableModeInfo.bHUInPairableState) &&
            (false == m_rPairableModeInfo.bHUInPairingOngoingState) &&
            (true == m_rPairableModeInfo.bHUInConnectableState) &&
            (IS_VALID_BT_ADDRESS(m_rPairableModeInfo.szBTAddress)) &&
            (szAAPBTAddress == m_rPairableModeInfo.szBTAddress));
   }//if (NULL != m_cpoBTInterface)

   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPBluetooth::bIsHUInPairableConnectableState() left with %d "
         "(Current state :: Pairable = %d, Connectable = %d, PairingOngoing = %d, BTAddress = %s) ",
         ETG_ENUM(BOOL, bHUInPairableConnectableState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInPairableState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInConnectableState),
         ETG_ENUM(BOOL, m_rPairableModeInfo.bHUInPairingOngoingState),
         m_rPairableModeInfo.szBTAddress.c_str()));

   m_oHUBTStateLock.vUnlock();

   return bHUInPairableConnectableState;
} //!end of bIsHUInPairableConnectableState()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::enGetPairingStatus()
***************************************************************************/
tenDevicePairingStatus spi_tclAAPBluetooth::enGetPairingStatus()
{
   m_oPairingStateLock.s16Lock();
   tenDevicePairingStatus enPairingStatus = m_enPairingStatus;
   m_oPairingStateLock.vUnlock();

   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPBluetooth::enGetPairingStatus() left with %d ",
         ETG_ENUM(SPI_BT_PAIRING_STATE, enPairingStatus)));
   return enPairingStatus;
} //!end of enGetPairingStatus()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vSetPairingStatus()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vSetPairingStatus(tenDevicePairingStatus enPairingStatus)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vSetPairingStatus() entered: %d ",
         ETG_ENUM(SPI_BT_PAIRING_STATE, enPairingStatus)));

   m_oPairingStateLock.s16Lock();
   m_enPairingStatus = enPairingStatus;
   m_oPairingStateLock.vUnlock();
} //!end of vSetPairingStatus()

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetooth::vRestoreHUDefaultState()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vRestoreHUDefaultState()
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vRestoreHUDefaultState() entered "));

   if (NULL != m_cpoBTPolicyBase)
   {
      //! If SPI has switched HU to connectable state, need to reset to default state
      //! (i.e. Pairable = OFF, Connectable = OFF, BTAddress = <empty>
      if ((true == m_rPairableModeInfo.bHUInConnectableState) //TODO - state at startup?
         &&
         (false == m_cpoBTPolicyBase->bConfigureHUState(e8MODE_OFF, e8MODE_OFF, "")))
      {
         // TODO - default for other projects needs to be considered
         ETG_TRACE_ERR(("[ERR]:vRestoreHUDefaultState: Error restoring BT Local mode! "));
      }

      //! Re-enable BT A2DP profile
      m_cpoBTPolicyBase->vSetBTProfile(e8A2DP_ENABLED);

   }//if (NULL != m_cpoBTPolicyBase)
} //!end of vRestoreHUDefaultState()

/***************************************************************************
** FUNCTION:   t_Void spi_tclAAPBluetooth::vDeselectAAPDevice()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vDeselectAAPDevice()
{
   ETG_TRACE_USR1(("spi_tclAAPBluetooth::vDeselectAAPDevice() entered "));
   if (NULL != m_cpoBTInterface)
   {
      //Clear device status
      m_cpoBTInterface->vSetSelectedDevStatus(e8DEVICE_CHANGE_COMPLETE);
      m_cpoBTInterface->vSendDeselectDeviceRequest();
   }
} //!end of vDeselectAAPDevice()

/***************************************************************************
** FUNCTION:   t_Void spi_tclAAPBluetooth::vSendDevicePairingResponse()
***************************************************************************/
t_Void spi_tclAAPBluetooth::vSendDevicePairingResponse(t_Bool bReadyToPair)
{
   if (NULL != m_cpoBTInterface)
   {
      t_String szAAPBTAddress = m_cpoBTInterface->szGetSelectedDevBTAddress();

      spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
      spi_tclAAPCmdBluetooth* poCmdBluetooth = (NULL != poAAPManager)?
            (poAAPManager->poGetBluetoothInstance()) : (NULL);

      //! Send BTDevicePairingResponse to device
      if ((true == IS_VALID_BT_ADDRESS(szAAPBTAddress)) &&
            (NULL != m_cpoBTPolicyBase) && (NULL != poCmdBluetooth))
      {
         //@Note: If HU is not ready to pair, send AAP device paired info as unpaired.
         t_Bool bAAPDevicePaired = (true == bReadyToPair) ?
               (m_cpoBTPolicyBase->bGetPairingStatus(szAAPBTAddress)) :
               (false);
         poCmdBluetooth->vSendBTPairingResponse(bReadyToPair, bAAPDevicePaired);
         ETG_TRACE_USR2(("[DESC]spi_tclAAPBluetooth::vSendDevicePairingResponse: Sending BTPairingResponse "));
      }
   }//if (NULL != m_cpoBTInterface)
} //!end of vSendDevicePairingResponse()

/***************************************************************************
** FUNCTION:   t_Bool spi_tclAAPBluetooth::bCompareBTDevices()
***************************************************************************/
t_Bool spi_tclAAPBluetooth::bCompareBTDevices(const t_String& rfcszAAPBTDevAddress,
      const t_String& rfcszBTDevAddress)
{
   return (
         (IS_VALID_BT_ADDRESS(rfcszBTDevAddress))   /*If received BT device address is valid*/
         &&
         (IS_VALID_BT_ADDRESS(rfcszAAPBTDevAddress))   /*If AAP BT device address is valid*/
         &&
         (rfcszBTDevAddress == rfcszAAPBTDevAddress)   /*If BT and AAP devices are same*/
         );
} //!end of bCompareBTDevices()

//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
