/***********************************************************************/
/*!
* \file  spi_tclAAPCmdNotification.h
* \brief Interface to interact with AAP Notification Endpoint
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface to interact with AAP Notification Endpoint
AUTHOR:         Dhiraj Asopa
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
17.02.2016  | Dhiraj Asopa          | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPCMDNOTIFICATION_H_
#define _SPI_TCLAAPCMDNOTIFICATION_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <NotificationEndpoint.h>

#include "AAPTypes.h"
#include "spi_tclAAPNotificationCbs.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/* Forward declaration */

/******************************************************************************/
/*!
* \class spi_tclAAPCmdNotification
* \brief Interface to interact with Notification Endpoint
*
* It is responsible for creation & initialization of Notification Endpoint.
*******************************************************************************/
class spi_tclAAPCmdNotification
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdNotification::spi_tclAAPCmdNotification()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPCmdNotification()
   * \brief   Default Constructor
   * \sa      ~spi_tclAAPCmdNotification()
   ***************************************************************************/
   spi_tclAAPCmdNotification();

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdNotification::~spi_tclAAPCmdNotification()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPCmdNotification()
   * \brief   Destructor
   * \sa      spi_tclAAPCmdNotification()
   ***************************************************************************/
   ~spi_tclAAPCmdNotification();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPCmdNotification::bInitializeNotification()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitializeNotification()
   * \brief   Creates and initialises an instance of Notification Endpoint
   * \retval  t_Bool  :  True if the Notification Endpoint is initialised, else False
   * \sa      vUninitializeNotification()
   ***************************************************************************/
   t_Bool bInitializeNotification();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPCmdNotification::vUninitializeNotification()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitializeNotification()
   * \brief   Uninitialises and destroys an instance of Notification Endpoint
   * \retval  t_Void
   * \sa      bInitializeNotification()
   ***************************************************************************/
   t_Void vUninitializeNotification();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPCmdNotification::vAckNotification()
   ***************************************************************************/
   /*!
   * \fn      t_Void vAckNotification(t_U32 u32DeviceHandle,
   *                  const t_String& corfszNotifId)
   * \brief   Acknowledges the receipt of notification
   * \param   u32DeviceHandle  : [IN] Id of the device to be acknowledged
   * \param   corfszNotifId    : [IN] Received notification's Id
   * \retval  t_Void
   ***************************************************************************/
   t_Void vAckNotification(t_U32 u32DeviceHandle, const t_String& corfszNotifId);


private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
    ** FUNCTION: spi_tclAAPCmdNotification(const spi_tclAAPCmdNotification &rfcoCmdNotification)
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPCmdNotification(const spi_tclAAPCmdNotification &rfcoCmdNotification)
    * \brief   Copy constructor not implemented hence made private
    **************************************************************************/
   spi_tclAAPCmdNotification(const spi_tclAAPCmdNotification &rfcoCmdNotification);

   /***************************************************************************
    ** FUNCTION: const spi_tclAAPCmdNotification & operator=(
    **                                 const spi_tclAAPCmdNotification &rfcoCmdNotification);
    ***************************************************************************/
   /*!
    * \fn      const spi_tclAAPCmdNotification & operator=(
    *             const spi_tclAAPCmdNotification &rfcoCmdNotification);
    * \brief   assignment operator not implemented hence made private
    **************************************************************************/
   const spi_tclAAPCmdNotification & operator=(
            const spi_tclAAPCmdNotification &rfcoCmdNotification);

   //! Notification Endpoint pointer
   NotificationEndpoint*   m_poNotifEndpoint;

   shared_ptr<INotificationEndpointCallbacks>  m_spoNotificationCbs;

};

#endif // _SPI_TCLAAPCMDNOTIFICATION_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
