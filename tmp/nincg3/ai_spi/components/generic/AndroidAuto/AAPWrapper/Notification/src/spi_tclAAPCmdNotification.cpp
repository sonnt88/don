/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdNotification.cpp
 * \brief             Notification  wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Notification wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.02.2016 | Dhiraj Asopa                 | Initial Version


 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/

#include "spi_tclAAPSessionDataIntf.h"
#include "spi_tclAAPCmdNotification.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPCmdNotification.cpp.trc.h"
   #endif
#endif
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
** FUNCTION:  spi_tclAAPCmdNotification::spi_tclAAPCmdNotification()
***************************************************************************/
spi_tclAAPCmdNotification::spi_tclAAPCmdNotification():
m_poNotifEndpoint(NULL)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdNotification::spi_tclAAPCmdNotification entered "));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdNotification::~spi_tclAAPCmdNotification()
***************************************************************************/
spi_tclAAPCmdNotification::~spi_tclAAPCmdNotification()
{
   ETG_TRACE_USR1((" spi_tclAAPCmdNotification::~spi_tclAAPCmdNotification entered "));
   RELEASE_MEM(m_poNotifEndpoint);
   m_spoNotificationCbs = nullptr;
}

/***************************************************************************
** FUNCTION:t_Bool spi_tclAAPCmdNotification::bInitializeNotification
***************************************************************************/
t_Bool spi_tclAAPCmdNotification::bInitializeNotification()
{
   //Registration of Endpoint and callbacks are commented for now.
   //Will be available based on the requirement.
   /*lint -esym(40,nullptr)nullptr Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclAAPCmdNotification::bInitializeNotification entered "));
   //! Get Galreciever using Session Data Interface
   spi_tclAAPSessionDataIntf oSessionDataIntf;
   shared_ptr<GalReceiver> spoGalReceiver = oSessionDataIntf.poGetGalReceiver();
   MessageRouter* poMessageRouter = (spoGalReceiver != nullptr) ? (spoGalReceiver->messageRouter()) : (NULL);
   t_Bool bRetVal = false;
   if ((NULL != poMessageRouter) && (NULL == m_poNotifEndpoint))
   {
      m_poNotifEndpoint = new (std::nothrow)NotificationEndpoint((t_U8)e32SESSIONID_AAPNOTIFICATION, poMessageRouter);
      SPI_NORMAL_ASSERT(NULL == m_poNotifEndpoint);
      if ((NULL != m_poNotifEndpoint))
      {
         //m_spoNotificationCbs = new (std::nothrow)spi_tclAAPNotificationCbs();
         //m_poNotifEndpoint->registerCallbacks(m_spoNotificationCbs);
         //bRetVal = spoGalReceiver->registerService(m_poNotifEndpoint);
         ETG_TRACE_USR4(("[DESC]:bInitializeNotification: Registration with GalReceiver : %d",ETG_ENUM(BOOL,bRetVal)));
      }
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdNotification::vUninitializeNotification()
***************************************************************************/
t_Void spi_tclAAPCmdNotification::vUninitializeNotification()
{
   /*lint -esym(40,nullptr)nullptr Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAAPCmdNotification::vUninitializeNotification() entered "));
   RELEASE_MEM(m_poNotifEndpoint);
   m_spoNotificationCbs = nullptr;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdNotification::vAckNotification()
***************************************************************************/
t_Void spi_tclAAPCmdNotification::vAckNotification(t_U32 u32DeviceHandle,
        const t_String& corfszNotifId)
{
   /*lint -esym(40,nullptr)nullptr Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAAPCmdNotification::vAckNotification() entered "));
   ETG_TRACE_USR2(("[DESC]: vAckNotification: Device Handle = 0x%x, Device category = %d"
                   "Notification Id = %s", u32DeviceHandle,
                   ETG_ENUM(DEVICE_CATEGORY,e8DEV_TYPE_ANDROIDAUTO),corfszNotifId.c_str()));

   if ((false == corfszNotifId.empty()) && (NULL != m_poNotifEndpoint))
   {
      t_U32 u32AckStatus = m_poNotifEndpoint->ackNotification(corfszNotifId);
      ETG_TRACE_USR2(("Acknowledgment status = %d", ETG_ENUM(NOTIFICATION_ACK_STATUS,u32AckStatus)));
   }
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>

