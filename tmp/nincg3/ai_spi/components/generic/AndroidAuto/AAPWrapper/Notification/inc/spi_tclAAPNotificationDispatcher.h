/***********************************************************************/
/*!
 * \file  spi_tclAAPNotificationDispatcher.h
 * \brief Message Dispatcher for Notification Messages. implemented using
 *        double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Notification Messages
 AUTHOR:         Dhiraj Asopa
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 17.02.2016  | Dhiraj Asopa          | Initial Version

 \endverbatim
 *************************************************************************/
#ifndef _SPI_TCLAAPNOTIFICATIONDISPATCHER_H_
#define _SPI_TCLAAPNOTIFICATIONDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "RespRegister.h"
#include "AAPTypes.h"

/**************Forward Declarations******************************************/
class spi_tclAAPNotificationDispatcher;

/****************************************************************************/
/*!
 * \class AAPNotificationMsgBase
 * \brief Base Message type for all Notification messages
 ****************************************************************************/
class AAPNotificationMsgBase: public trMsgBase
{
   public:

   /***************************************************************************
    ** FUNCTION:  AAPNotificationMsgBase::AAPNotificationMsgBase
    ***************************************************************************/
   /*!
    * \fn      AAPNotificationMsgBase()
    * \brief   Default constructor
    **************************************************************************/
   AAPNotificationMsgBase();

   /***************************************************************************
    ** FUNCTION:  AAPNotificationMsgBase::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPNotificationDispatcher* poNotificationDispatcher)
    * \brief   Pure virtual function to be overridden by inherited classes for
    *          dispatching the message
    * \param   poNotificationDispatcher : [IN] pointer to Message dispatcher for Notification
    **************************************************************************/
   virtual t_Void vDispatchMsg(spi_tclAAPNotificationDispatcher* poNotificationDispatcher) = 0;

   /***************************************************************************
    ** FUNCTION:  AAPNotificationMsgBase::~AAPNotificationMsgBase
    ***************************************************************************/
   /*!
    * \fn      ~AAPNotificationMsgBase()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPNotificationMsgBase()
   {

   }
};

/****************************************************************************/
/*!
 * \class AAPSubscriptionStatusMsg
 * \brief Notification subscription Status msg
 ****************************************************************************/
class AAPSubscriptionStatusMsg: public AAPNotificationMsgBase
{
   public:

   t_Bool m_bSubscribed;

   /***************************************************************************
    ** FUNCTION:  AAPSubscriptionStatusMsg::AAPSubscriptionStatusMsg
    ***************************************************************************/
   /*!
    * \fn      AAPSubscriptionStatusMsg()
    * \brief   Default constructor
    **************************************************************************/
   AAPSubscriptionStatusMsg();

   /***************************************************************************
    ** FUNCTION:  AAPSubscriptionStatusMsg::~AAPSubscriptionStatusMsg
    ***************************************************************************/
   /*!
    * \fn      ~AAPSubscriptionStatusMsg()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPSubscriptionStatusMsg(){}

   /***************************************************************************
    ** FUNCTION:  AAPSubscriptionStatusMsg::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPNotificationDispatcher* poNotificationDispatcher)
    * \brief   virtual function for dispatching the message of 'this' type
    * \param   poNotificationDispatcher : [IN] pointer to Message dispatcher for Notification
    **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPNotificationDispatcher* poNotificationDispatcher);

   /***************************************************************************
    ** FUNCTION:  AAPSubscriptionStatusMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg();

   /***************************************************************************
    ** FUNCTION:  AAPSubscriptionStatusMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg();
};


/****************************************************************************/
/*!
 * \class AAPNotificationMsg
 * \brief Notification Comes from the phone
 ****************************************************************************/
class AAPNotificationMsg: public AAPNotificationMsgBase
{
   public:

   t_String* m_poszAAPNotifText;
   t_Bool m_bAAPNotifHasId;
   t_String* m_poszAAPNotifId;
   t_Bool m_bAAPNotifHasIcon;
   t_U8* m_pu8AAPNotifIcon;
   t_U8 m_u8AAPNotifLength;


   /***************************************************************************
    ** FUNCTION:  AAPNotificationMsg::AAPNotificationMsg
    ***************************************************************************/
   /*!
    * \fn      AAPNotificationMsg()
    * \brief   Default constructor
    **************************************************************************/
   AAPNotificationMsg();

   /***************************************************************************
    ** FUNCTION:  AAPNotificationMsg::~AAPNotificationMsg
    ***************************************************************************/
   /*!
    * \fn      ~AAPNotificationMsg()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPNotificationMsg(){}

   /***************************************************************************
    ** FUNCTION:  AAPNotificationMsg::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPNotificationDispatcher* poNotificationDispatcher)
    * \brief   virtual function for dispatching the message of 'this' type
    * \param   poNotificationDispatcher : pointer to Message dispatcher for Notification
    **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPNotificationDispatcher* poNotificationDispatcher);

   /***************************************************************************
    ** FUNCTION:  AAPNotificationMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg();

   /***************************************************************************
    ** FUNCTION:  AAPNotificationMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class spi_tclAAPNotificationDispatcher
 * \brief Message Dispatcher for Notification Messages
 ****************************************************************************/
class spi_tclAAPNotificationDispatcher
{
   public:
   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNotificationDispatcher::spi_tclAAPNotificationDispatcher
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPNotificationDispatcher()
    * \brief   Default constructor
    **************************************************************************/
   spi_tclAAPNotificationDispatcher();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNotificationDispatcher::~spi_tclAAPNotificationDispatcher
    ***************************************************************************/
   /*!
    * \fn      ~spi_tclAAPNotificationDispatcher()
    * \brief   Destructor
    **************************************************************************/
   ~spi_tclAAPNotificationDispatcher();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNotificationDispatcher::vHandleNotificationMsg(AAPSubscriptionStatusMsg*...)
    ***************************************************************************/
   /*!
    * \fn      vHandleNotificationMsg(AAPSubscriptionStatusMsg* poSubsrStatus)
    * \brief   Handles Messages of AAPSubscriptionStatusMsg type
    * \param   poSubsrStatus : [IN] pointer to AAPSubscriptionStatusMsg.
    **************************************************************************/
   t_Void vHandleNotificationMsg(AAPSubscriptionStatusMsg* poSubsrStatus) const;


   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNotificationDispatcher::vHandleNotificationMsg(AAPNotificationMsg*...)
    ***************************************************************************/
   /*!
    * \fn      vHandleNotificationMsg(AAPNotificationMsg* poNotification)
    * \brief   Handles Messages of AAPNotificationMsg type
    * \param   poNotification : [IN] pointer to AAPsubscriptionStatusMsg.
    **************************************************************************/
   t_Void vHandleNotificationMsg(AAPNotificationMsg* poNotification) const;

   protected:

      /***************************************************************************
      *********************************PROTECTED**********************************
      ***************************************************************************/

      /***************************************************************************
      ** FUNCTION:  spi_tclAAPNotificationDispatcher(const spi_tclAAPNotificationDispatcher...
      ***************************************************************************/
      /*!
      * \fn      spi_tclAAPNotificationDispatcher(
      *                             const spi_tclAAPNotificationDispatcher& corfoSrc)
      * \brief   Copy constructor - Do not allow the creation of copy constructor
      * \param   corfoSrc : [IN] reference to source data interface object
      * \retval
      * \sa      spi_tclAAPNotificationDispatcher()
      ***************************************************************************/
   spi_tclAAPNotificationDispatcher(const spi_tclAAPNotificationDispatcher& corfoSrc);


      /***************************************************************************
      ** FUNCTION:  spi_tclAAPVideoDispatcher& operator=( const spi_tclAAP...
      ***************************************************************************/
      /*!
      * \fn      spi_tclAAPNotificationDispatcher& operator=(
      *                          const spi_tclAAPNotificationDispatcher& corfoSrc))
      * \brief   Assignment operator
      * \param   corfoSrc : [IN] reference to source data interface object
      * \retval
      * \sa      spi_tclAAPNotificationDispatcher(const spi_tclAAPNotificationDispatcher& otrSrc)
      ***************************************************************************/
   spi_tclAAPNotificationDispatcher& operator=(const spi_tclAAPNotificationDispatcher& corfoSrc);


      /***************************************************************************
      ****************************END OF PROTECTED********************************
      ***************************************************************************/

   private:

      /***************************************************************************
      *********************************PRIVATE************************************
      ***************************************************************************/


      /***************************************************************************
      ****************************END OF PRIVATE *********************************
      ***************************************************************************/

};

#endif /* _SPI_TCLAAPNOTIFICATIONDISPATCHER_H_ */
