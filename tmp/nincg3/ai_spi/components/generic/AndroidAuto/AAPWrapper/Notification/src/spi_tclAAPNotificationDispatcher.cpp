/***********************************************************************/
/*!
 * \file  spi_tclAAPNotificationDispatcher.cpp
 * \brief Message Dispatcher for Notification. implemented using
 *        double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Notification
 AUTHOR:         Dhiraj Asopa
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 18.02.2016  | Dhiraj Asopa         | Initial Version

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "spi_tclAAPRespNotification.h"
#include "spi_tclAAPNotificationDispatcher.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclAAPNotificationDispatcher.cpp.trc.h"
   #endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleNotificationMsg(this);           \
   }                                                        \
   vDeAllocateMsg();                                        \
}

/***************************************************************************
 ** FUNCTION:  AAPNotificationMsgBase::AAPNotificationMsgBase
 ***************************************************************************/
AAPNotificationMsgBase::AAPNotificationMsgBase()
{
   ETG_TRACE_USR1(("AAPNotificationMsgBase() entered "));
   vSetServiceID(e32MODULEID_AAPNOTIFICATION);
}

/***************************************************************************
 ** FUNCTION:  AAPSubscriptionStatusMsg::AAPSubscriptionStatusMsg
 ***************************************************************************/
AAPSubscriptionStatusMsg::AAPSubscriptionStatusMsg():
      m_bSubscribed(false)
{
   ETG_TRACE_USR1(("AAPSubscriptionStatusMsg() entered "));
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPSubscriptionStatusMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPSubscriptionStatusMsg, spi_tclAAPNotificationDispatcher);

/***************************************************************************
 ** FUNCTION:  AAPSubscriptionStatusMsg::vAllocateMsg
 ***************************************************************************/
t_Void AAPSubscriptionStatusMsg::vAllocateMsg()
{
   ETG_TRACE_USR1(("AAPSubscriptionStatusMsg::vAllocateMsg() entered "));
}

/***************************************************************************
 ** FUNCTION:  AAPSubscriptionStatusMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AAPSubscriptionStatusMsg::vDeAllocateMsg()
{
   ETG_TRACE_USR1(("AAPSubscriptionStatusMsg::vDeAllocateMsg() entered "));

}
/***************************************************************************
 ** FUNCTION:  AAPNotificationMsg::AAPNotificationMsg
 ***************************************************************************/
AAPNotificationMsg::AAPNotificationMsg():
      m_poszAAPNotifText(NULL),
      m_bAAPNotifHasId(false),
      m_poszAAPNotifId(NULL),
      m_bAAPNotifHasIcon(false),
      m_pu8AAPNotifIcon(NULL),
      m_u8AAPNotifLength(0)
{
   ETG_TRACE_USR1(("AAPNotificationMsg() entered "));
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPNotificationMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPNotificationMsg, spi_tclAAPNotificationDispatcher);

/***************************************************************************
 ** FUNCTION:  AAPNotificationMsg::vAllocateMsg
 ***************************************************************************/
t_Void AAPNotificationMsg::vAllocateMsg()
{
   ETG_TRACE_USR1(("AAPNotificationMsg::vAllocateMsg() entered "));
   m_poszAAPNotifText = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_poszAAPNotifText);
   m_poszAAPNotifId = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_poszAAPNotifId);
}

/***************************************************************************
 ** FUNCTION:  AAPNotificationMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AAPNotificationMsg::vDeAllocateMsg()
{
   ETG_TRACE_USR1(("AAPNotificationMsg::vDeAllocateMsg() entered "));
   RELEASE_MEM(m_poszAAPNotifText);
   RELEASE_MEM(m_poszAAPNotifId);
   RELEASE_MEM(m_pu8AAPNotifIcon);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPNotificationDispatcher::spi_tclAAPNotificationDispatcher
 ***************************************************************************/
spi_tclAAPNotificationDispatcher::spi_tclAAPNotificationDispatcher()
{
   ETG_TRACE_USR1(("spi_tclAAPNotificationDispatcher() entered "));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPNotificationDispatcher::~spi_tclAAPNotificationDispatcher
 ***************************************************************************/
spi_tclAAPNotificationDispatcher::~spi_tclAAPNotificationDispatcher()
{
   ETG_TRACE_USR1(("~spi_tclAAPNotificationDispatcher() entered "));
}

/*********************************************************************************************
 ** FUNCTION:  spi_tclAAPNotificationDispatcher::vHandleNotificationMsg(AAPsubscriptionStatusMsg...)
 *********************************************************************************************/
t_Void spi_tclAAPNotificationDispatcher::vHandleNotificationMsg(AAPSubscriptionStatusMsg* poSubsrStatus)const
{
   ETG_TRACE_USR1(("spi_tclAAPNotificationDispatcher::vHandleNotificationMsg entered "));
   if (NULL != poSubsrStatus)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespNotification,
         e16AAP_NOTIFICATION,
         vNotificationSubscriptionStatusCallback(poSubsrStatus->m_bSubscribed));
   } // if (NULL != poSubsrStatus)

}

/*************************************************************************************************
 ** FUNCTION:  spi_tclAAPNotificationDispatcher::vHandleNotificationMsg(AAPNotificationMsg...)
 *************************************************************************************************/
t_Void spi_tclAAPNotificationDispatcher::vHandleNotificationMsg(AAPNotificationMsg* poNotification) const
{
   ETG_TRACE_USR1(("spi_tclAAPNotificationDispatcher::vHandleNotificationMsg entered "));
   if ((NULL != poNotification) && (NULL != poNotification->m_poszAAPNotifText)
        && (NULL != poNotification->m_poszAAPNotifId))
   {
      CALL_REG_OBJECTS(spi_tclAAPRespNotification,
            e16AAP_NOTIFICATION,
            vNotificationCallback(*(poNotification->m_poszAAPNotifText),poNotification->m_bAAPNotifHasId,
                  *(poNotification->m_poszAAPNotifId), poNotification->m_bAAPNotifHasIcon,
                  poNotification->m_pu8AAPNotifIcon, poNotification->m_u8AAPNotifLength));
   } // if (NULL != poNotification)

}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
