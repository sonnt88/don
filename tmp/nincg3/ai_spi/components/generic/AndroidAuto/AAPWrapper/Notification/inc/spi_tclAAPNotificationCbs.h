/*!
 *******************************************************************************
 * \file              spi_tclAAPNotificationCbs.h
 * \brief             Notification Endpoint callbacks handler for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Notification Endpoint callbacks handler for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.02.2016 |  Dhiraj Asopa               | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLAAPNOTIFICATIONCBS_H_
#define _SPI_TCLAAPNOTIFICATIONCBS_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include <INotificationEndpointCallbacks.h>

/* This class includes a general set of INotificationEndpointCallbacks that must be set up for the Notification Endpoint */
class spi_tclAAPNotificationCbs : public INotificationEndpointCallbacks
{
public:

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclAAPNotificationCbs::spi_tclAAPNotificationCbs()
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPNotificationCbs()
    * \brief   Constructor
    * \sa      ~spi_tclAAPNotificationCbs()
    **************************************************************************/
    spi_tclAAPNotificationCbs() { }

    /***************************************************************************
     ** FUNCTION:  virtual spi_tclAAPNotificationCbs::~spi_tclAAPNotificationCbs()
     ***************************************************************************/
    /*!
     * \fn      virtual ~spi_tclAAPNotificationCbs()
     * \brief   Destructor
     * \sa      spi_tclAAPNotificationCbs()
     **************************************************************************/
    virtual ~spi_tclAAPNotificationCbs() { }

    /**********Start of functions overridden from INotificationStatusCallbacks**********/

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPNotificationCbs::subscriptionStatusCallback(...)
    ***************************************************************************/
    /*!
     * \fn      subscriptionStatusCallback (bool bSubscribed )
     * \brief   Interface to send  Sstatus of the phones subscription to the notification service changes.
     * \param   bSubscribed: [IN] Subscription status (true if the phone has subscribed to notifications,
     *          false if the phone has unsubscribed. )
     * \sa      None
     ***************************************************************************/
    void subscriptionStatusCallback (bool bsubscribed);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPNotificationCbs::notificationCallback(...)
    ***************************************************************************/
   /*!
    * \fn      notificationCallback (const string& corfszNotifText, bool bHasId, const string& corfszId,
    *                                bool bHasIcon, uint8_t *pu8Icon, size_t siLen)
    * \brief   Interface to send a new notification to Notification Endpoint.
    * \param   corfszNotifText: [IN] The text of the notification to be displayed.
    * \param   bHasId         : [IN] If true, the id parameter contains valid data
    * \param   corfszId       : [IN] An identifier for this notification.
    * \param   bHasIcon       : [IN] If true, the icon argument contains valid data.
    * \param   pu8Icon        : [IN] A pointer to the icon data (png format).
    * \param   siIconSize     : [IN] The size of the icon in bytes.
    * \sa      None
    ***************************************************************************/
    void notificationCallback (const string& corfszNotifText, bool bHasId,
          const string& corfszId, bool bHasIcon, uint8_t *pu8Icon, size_t siIconSize);

    /***************************************************************************
     ** FUNCTION:  t_Void spi_tclAAPNotificationCbs::ackCallback(...)
     ***************************************************************************/
    /*!
     * \fn      ackCallback(const string &corfszId)
     * \brief   Interface to send response when notification is acknowledged by the other end
     * \param   [IN] corfszId: The id of the notification that was acknowledged.
     * \sa      None
     ***************************************************************************/
    void ackCallback (const string &corfszId)
    {
    	SPI_INTENTIONALLY_UNUSED(corfszId);
    }

    /***********End of functions overridden from INotificationStatusCallbacks**********/


private:

    /***************************************************************************
     ** FUNCTION: spi_tclAAPNotificationCbs(const spi_tclAAPNotificationCbs &corfObject)
     ***************************************************************************/
    /*!
     * \fn      spi_tclAAPNotification(const spi_tclAAPNotification &corfObject)
     * \brief   Copy constructor not implemented hence made private
     * \param   corfObject : [IN] The reference object to be copied
     **************************************************************************/
    spi_tclAAPNotificationCbs(const spi_tclAAPNotificationCbs& corfObject);

    /***************************************************************************
     ** FUNCTION: const spi_tclAAPNotificationCbs & operator=(
     **                                 const spi_tclAAPNotificationCbs &corfObject);
     ***************************************************************************/
    /*!
     * \fn      const spi_tclAAPNotificationCbs & operator=
     *            (const spi_tclAAPNotificationCbs &corfObject);
     * \brief   assignment operator not implemented hence made private
     * \param   corfObject : [IN] The reference object to be copied
     **************************************************************************/
    const spi_tclAAPNotificationCbs& operator=(const spi_tclAAPNotificationCbs& corfObject);

};


#endif /* _SPI_TCLAAPNOTIFICATIONCBS_H_ */
