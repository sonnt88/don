
/***********************************************************************/
/*!
* \file   spi_tclAAPRespNotification
* \brief  Notification Call backs output interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Notification Call backs output interface
AUTHOR:         Dhiraj Asopa
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                    | Modification
18.02.2016  | Dhiraj Asopa              | Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAAPRESPNOTIFICATION_H_
#define _SPI_TCLAAPRESPNOTIFICATION_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "RespBase.h"
#include "AAPTypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class  spi_tclAAPRespNotification
* \brief  Notification Status is  updated to Application using this
*****************************************************************************/
class spi_tclAAPRespNotification:public RespBase
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespNotification::spi_tclAAPRespNotification()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespNotification()
   * \brief   Default Constructor
   * \sa      ~spi_tclAAPRespNotification()
   **************************************************************************/
   spi_tclAAPRespNotification():RespBase(e16AAP_NOTIFICATION){}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespNotification::~spi_tclAAPRespNotification()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPRespNotification()
   * \brief   Destructor
   * \sa      spi_tclAAPRespNotification()
   **************************************************************************/
   virtual ~spi_tclAAPRespNotification(){}

   /**************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespNotification::vNotificationSubscriptionStatusCallback
   **************************************************************************/
   /*!
   * \fn      virtual t_Void vNotificationSubscriptionStatusCallback(bAAPNotifSubscribed)
   * \brief   Callback to update the subscription status
   *           from INotificationEndpointCallbacks
   * \param   bAAPNotifSubscribed : [IN] true if the phone is subscribed for
   *                                notifications
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vNotificationSubscriptionStatusCallback(t_Bool bAAPNotifSubscribed){};

   /**************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespNotification::vNotificationCallback()
   **************************************************************************/
   /*!
   * \fn      virtual t_Void vNotificationCallback(t_String szAAPNotifText,
   *           t_Bool bAAPNotifHasId,t_String szAAPNotifId,
   *           t_Bool bAAPNotifhasIcon,t_U8* pou8AAPNotifIcon,size_t sAAPNotifSize)
   * \brief   Callback to update the notification from INotificationEndpointCallbacks
   * \param   szAAPNotifText   : [IN] Notification description text
   * \param   bAAPNotifHasId   : [IN] true if the notification has unique Id
   * \param   szAAPNotifId     : [IN] Unique Id of the notification
   * \param   bAAPNotifhasIcon : [IN] true if the notification has an icon
   * \param   pou8AAPNotifIcon : [IN] Byte data stream of the icon image file
   * \param   sAAPNotifSize    : [IN] Size of the notification icon
   * \retval  t_Void
   *************************************************************************/
   virtual t_Void vNotificationCallback(t_String szAAPNotifText, t_Bool bAAPNotifHasId,
         t_String szAAPNotifId, t_Bool bAAPNotifhasIcon,t_U8* pou8AAPNotifIcon, size_t sAAPNotifSize){};

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespNotification(const spi_tclAAPRespNotification...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespNotification(const spi_tclAAPRespNotification& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespNotification()
   ***************************************************************************/
   spi_tclAAPRespNotification(const spi_tclAAPRespNotification& corfoSrc);


   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespNotification& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespNotification& operator=(const spi_tclAAPRespNotification& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespNotification(const spi_tclAAPRespNotification& otrSrc)
   ***************************************************************************/
   spi_tclAAPRespNotification& operator=(const spi_tclAAPRespNotification& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/


}; //class spi_tclAAPRespNotification

#endif //_SPI_TCLAAPRESPNOTIFICATION_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
