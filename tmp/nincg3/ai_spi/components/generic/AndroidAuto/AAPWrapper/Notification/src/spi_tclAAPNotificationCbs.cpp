/*!
 *******************************************************************************
 * \file              spi_tclAAPNotificationCbs.cpp
 * \brief             Notification Status Endpoint callbacks handler for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Notification Endpoint callbacks handler for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.02.2016 |  Dhiraj Asopa                | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPNotificationDispatcher.h"
#include "spi_tclAAPNotificationCbs.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclAAPNotificationCbs.cpp.trc.h"
#endif
#endif


/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPNotificationCbs:: subscriptionStatusCallback (bool bsubscribed)
***************************************************************************/
void spi_tclAAPNotificationCbs::subscriptionStatusCallback (bool bsubscribed)
{
   ETG_TRACE_USR1(("spi_tclAAPNotificationCbs::subscriptionStatusCallback() entered"));
   ETG_TRACE_USR2(("[DESC]:subscriptionStatusCallback: Subscription status = %d", ETG_ENUM(BOOL, bsubscribed)));

   spi_tclAAPMsgQInterface* poMsgQInterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      AAPSubscriptionStatusMsg oNotfSubscrStatusMsg;
      oNotfSubscrStatusMsg.m_bSubscribed = bsubscribed;
      poMsgQInterface->bWriteMsgToQ(&oNotfSubscrStatusMsg, sizeof(oNotfSubscrStatusMsg));
   } //if ((NULL != poMsgQInterface)
}

/*****************************************************************************************************
** FUNCTION:  t_Void spi_tclAAPNotificationCbs:: notificationCallback (const string& corfszNotifText, bool bHasId,
**                               const string& corfszId, bool bHasIcon, uint8_t *pu8Icon, size_t siLen)
*******************************************************************************************************/
void spi_tclAAPNotificationCbs::notificationCallback (const string& corfszNotifText, bool bHasId, const string& corfszId,
       bool bHasIcon, uint8_t *pu8Icon, size_t siIconSize)
{
   ETG_TRACE_USR1(("spi_tclAAPNotificationCbs::notificationCallback() entered"));
   ETG_TRACE_USR2(("[DESC]:notificationCallback: Notification Text = %s", corfszNotifText.c_str()));
   ETG_TRACE_USR2(("[DESC]:notificationCallback: Has Notification Id = %d, Notification Id = %s", ETG_ENUM(BOOL,bHasId), corfszId.c_str()));
   ETG_TRACE_USR2(("[DESC]:notificationCallback: Has Icon = %d, Icon size = %d", ETG_ENUM(BOOL, bHasIcon), siIconSize));

   if (true == corfszNotifText.empty())
   {
	   ETG_TRACE_ERR(("[ERR]:notificationCallback: Invalid Notification text."));
   }
   spi_tclAAPMsgQInterface* poMsgQInterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      AAPNotificationMsg oNotificationMsg;
      if ( NULL != oNotificationMsg.m_poszAAPNotifText )
      {
         *(oNotificationMsg.m_poszAAPNotifText) = corfszNotifText.c_str();
      }
      oNotificationMsg.m_bAAPNotifHasId = bHasId;
      if ( NULL != oNotificationMsg.m_poszAAPNotifId)
      {
         *(oNotificationMsg.m_poszAAPNotifId) = corfszId.c_str();
      }
      oNotificationMsg.m_bAAPNotifHasIcon = bHasIcon;

      if ((true == bHasIcon) && (0 < siIconSize))
      {
    	 oNotificationMsg.m_pu8AAPNotifIcon = new (std::nothrow)t_U8(siIconSize);
    	 if (NULL != oNotificationMsg.m_pu8AAPNotifIcon)
    	 {
    		 memcpy((t_Void*)oNotificationMsg.m_pu8AAPNotifIcon, pu8Icon, siIconSize);
    	 }
      }

      oNotificationMsg.m_u8AAPNotifLength = siIconSize;
      poMsgQInterface->bWriteMsgToQ(&oNotificationMsg, sizeof(oNotificationMsg));
   }//if ((NULL != poMsgQInterface)
}



