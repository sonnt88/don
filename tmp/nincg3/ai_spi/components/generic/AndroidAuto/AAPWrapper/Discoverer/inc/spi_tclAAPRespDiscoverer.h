/***********************************************************************/
/*!
* \file  spi_tclAAPRespDiscoverer.h
* \brief AAP Discoverer Output Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Discoverer Output Interface
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                   | Modification
03.03.2015  | Pruthvi Thej Nagaraju    | Initial Version
\endverbatim
*************************************************************************/

#ifndef SPI_TCLAAPRESPDISCOVERER_H_
#define SPI_TCLAAPRESPDISCOVERER_H_

#include "RespBase.h"
#include "AAPTypes.h"
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclAAPRespDiscoverer
* \brief ML Discoverer Output Interface
*
* Detects USB devices and provides interface to switch to Android Auto
* updates all the registered clients, whenever there is
* an update on device connections
*
****************************************************************************/
class spi_tclAAPRespDiscoverer:public RespBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespDiscoverer::spi_tclAAPRespDiscoverer()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespDiscoverer()
   * \brief   Constructor
   * \sa      ~spi_tclAAPRespDiscoverer()
   **************************************************************************/
   spi_tclAAPRespDiscoverer():RespBase(e16AAP_DISCOVERER_REGID)
   {}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespDiscoverer::~spi_tclAAPRespDiscoverer()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclAAPRespDiscoverer()
   * \brief   Destructor
   * \param   t_Void
   * \sa      spi_tclAAPRespDiscoverer(RegID enRegId)
   **************************************************************************/
   virtual ~spi_tclAAPRespDiscoverer(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespDiscoverer::vPostDeviceInfo
   ***************************************************************************/
   /*!
   * \fn      t_Void vPostDeviceInfo
   * \brief   To Post the device info to SPI, when a new device is detected
   * \param   cou32DeviceHandle     : [IN] Device Id
   * \param   corfrDeviceInfo : [IN] const reference to the DeviceInfo structure.
   * \retval  t_Void
   * \sa      vPostDeviceDisconncted(const t_U32 cou32DeviceHandle)
   ***************************************************************************/
   virtual t_Void vPostDeviceInfo(
       const t_U32 cou32DeviceHandle,
       const trDeviceInfo &corfrDeviceInfo){}


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespDiscoverer::vPostDeviceDisconnected
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPostDeviceDisconnected
   * \brief   To Post the Device Id to SPI, when a device is disconnected
   * \param   rUserContext : [IN] Context information passed from the caller
   * \param   cou32DeviceHandle    : [IN] Device Id
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vPostDeviceDisconnected(const t_U32 cou32DeviceHandle){}


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespDiscoverer::spi_tclAAPRespDiscoverer()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespDiscoverer(
   *                          const spi_tclAAPRespDiscoverer& corfoSrc))
   * \brief   Parameterized Constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \sa      spi_tclAAPRespDiscoverer(RegID enRegId)
   **************************************************************************/
   spi_tclAAPRespDiscoverer(const spi_tclAAPRespDiscoverer& corfoSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespDiscoverer& operator=( const spi_tclMLV...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespDiscoverer& operator=(
   *                          const spi_tclAAPRespDiscoverer& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespDiscoverer(const spi_tclAAPRespDiscoverer& otrSrc)
   ***************************************************************************/
   spi_tclAAPRespDiscoverer& operator=(const spi_tclAAPRespDiscoverer& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

};

#endif /* SPI_TCLAAPRESPDISCOVERER_H_ */
