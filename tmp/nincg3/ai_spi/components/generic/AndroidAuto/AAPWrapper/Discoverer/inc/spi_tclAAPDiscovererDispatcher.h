/***********************************************************************/
/*!
 * \file  spi_tclAAPDiscovererDispatcher.h
 * \brief Message Dispatcher for Discoverer Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Discoverer Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.03.2015  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLAAPDISCOVERERDISPATCHER_H_
#define SPI_TCLAAPDISCOVERERDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespRegister.h"

/**************Forward Declarations******************************************/
class spi_tclAAPDiscovererDispatcher;

/****************************************************************************/
/*!
 * \class AAPDiscMsgBase
 * \brief Base Message type for all Discoverer messages
 ****************************************************************************/
class AAPDiscMsgBase: public trMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  AAPDiscMsgBase::AAPDiscMsgBase
       ***************************************************************************/
      /*!
       * \fn      AAPDiscMsgBase()
       * \brief   Default constructor
       **************************************************************************/
      AAPDiscMsgBase();

      /***************************************************************************
       ** FUNCTION:  AAPDiscMsgBase::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPDiscovererDispatcher* poDiscovererDispatcher)
       * \brief   Pure virtual function to be overridden by inherited classes for
       *          dispatching the message
       * \param poDiscovererDispatcher : pointer to Message dispatcher for Discoverer
       **************************************************************************/
      virtual t_Void vDispatchMsg(
               spi_tclAAPDiscovererDispatcher* poDiscovererDispatcher) = 0;

      /***************************************************************************
       ** FUNCTION:  AAPDiscMsgBase::~AAPDiscMsgBase
       ***************************************************************************/
      /*!
       * \fn      ~AAPDiscMsgBase()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPDiscMsgBase()
      {

      }

      /***************************************************************************
       ** FUNCTION:  MLDiscovererMsgBase::u32GetDeviceHandle
       ***************************************************************************/
      /*!
       * \fn      u32GetDeviceHandle()
       * \brief   returns the device handle
       **************************************************************************/
      virtual t_U32 u32GetDeviceHandle(){return m_u32DeviceHandle;}

      /***************************************************************************
       ** FUNCTION:  MLDiscovererMsgBase::vSetDeviceHandle
       ***************************************************************************/
      /*!
       * \fn      vSetDeviceHandle()
       * \brief   sets the device handle
       **************************************************************************/
      virtual t_Void vSetDeviceHandle(const t_U32 cou32DevHndle) {m_u32DeviceHandle = cou32DevHndle;};

   private:
      t_U32 m_u32DeviceHandle;
};


/****************************************************************************/
/*!
 * \class DeviceInfoMsg
 * \brief Discoverer attestation response message
 ****************************************************************************/
class DeviceInfoMsg: public AAPDiscMsgBase
{
   public:

      trDeviceInfo *m_prDeviceInfo;

      /***************************************************************************
       ** FUNCTION:  DeviceInfoMsg::DeviceInfoMsg
       ***************************************************************************/
      /*!
       * \fn      DeviceInfoMsg()
       * \brief   Default constructor
       **************************************************************************/
      DeviceInfoMsg();

      /***************************************************************************
       ** FUNCTION:  DeviceInfoMsg::~DeviceInfoMsg
       ***************************************************************************/
      /*!
       * \fn      ~DeviceInfoMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~DeviceInfoMsg(){}

      /***************************************************************************
       ** FUNCTION:  DeviceInfoMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPDiscovererDispatcher* poDiscovererDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
	   * \param  poDiscovererDispatcher : pointer to Message dispatcher for Discoverer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPDiscovererDispatcher* poDiscovererDispatcher);

      /***************************************************************************
       ** FUNCTION:  DeviceInfoMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  DeviceInfoMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class AAPDeviceDisconnectionMsg
 * \brief
 ****************************************************************************/
class AAPDeviceDisconnectionMsg: public AAPDiscMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  AAPDeviceDisconnectionMsg::AAPDeviceDisconnectionMsg
       ***************************************************************************/
      /*!
       * \fn      AAPDeviceDisconnectionMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPDeviceDisconnectionMsg();

      /***************************************************************************
       ** FUNCTION:  AAPDeviceDisconnectionMsg::~AAPDeviceDisconnectionMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPDeviceDisconnectionMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPDeviceDisconnectionMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPDeviceDisconnectionMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPDiscovererDispatcher* poDiscovererDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
      * \param  poDiscovererDispatcher : pointer to Message dispatcher for Discoverer
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPDiscovererDispatcher* poDiscovererDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPDeviceDisconnectionMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPDeviceDisconnectionMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};


/****************************************************************************/
/*!
 * \class spi_tclAAPDiscovererDispatcher
 * \brief Message Dispatcher for Discoverer Messages
 ****************************************************************************/
class spi_tclAAPDiscovererDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclAAPDiscovererDispatcher::spi_tclAAPDiscovererDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPDiscovererDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclAAPDiscovererDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPDiscovererDispatcher::~spi_tclAAPDiscovererDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAAPDiscovererDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclAAPDiscovererDispatcher();


      /***************************************************************************
       ** FUNCTION:  spi_tclAAPDiscovererDispatcher::vHandleDiscovererMsg(DeviceInfoMsg* poDeviceInfoMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscovererMsg(DeviceInfoMsg* poDeviceInfoMsg)
       * \brief   Handles Messages of DeviceInfoMsg type
      * \param   poLaunchDiscovererMsg : pointer to DeviceInfoMsg.
       **************************************************************************/
      t_Void vHandleDiscovererMsg(DeviceInfoMsg* poDeviceInfoMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPDiscovererDispatcher::vHandleDiscovererMsg(AAPDeviceDisconnectionMsg* poDeviceDisconnMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleDiscovererMsg(AAPDeviceDisconnectionMsg* poDeviceDisconnMsg)
       * \brief   Handles Messages of AAPDeviceDisconnectionMsg type
      * \param   poLaunchDiscovererMsg : pointer to AAPDeviceDisconnectionMsg.
       **************************************************************************/
      t_Void vHandleDiscovererMsg(AAPDeviceDisconnectionMsg* poDeviceDisconnMsg)const;

};

#endif /* SPI_TCLAAPDISCOVERERDISPATCHER_H_ */
