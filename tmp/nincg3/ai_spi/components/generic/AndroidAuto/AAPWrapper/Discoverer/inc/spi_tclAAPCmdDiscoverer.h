/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdDiscoverer.h
 * \brief             Device discovery wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device discovery wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.02.2015 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_AAPCMDDISCOVERER_H_
#define SPI_AAPCMDDISCOVERER_H_

/******************************************************************************
 | includes:
 | 1)AAP - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include <map>
#include "Lock.h"
#include "Timer.h"
#include "BaseTypes.h"
#include "MsgContext.h"
#include "AAPTypes.h"
#include "spi_usb_discoverer_types.h"
#include "spi_usb_discoverer.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
//! Stores the aoap switch progress information
struct trAOAPSwitchInfo
{
      t_U32 u32DeviceHandle;
      t_Bool bSwitchinProgress;
      trAOAPSwitchInfo():u32DeviceHandle(0), bSwitchinProgress(false)
      {

      }
};

//! Stores USB reset retrial information
struct trUSBResetInfo
{
      timer_t oTimerID;
      t_U32 u32RetryCount;
      trUSBResetInfo(): oTimerID(0), u32RetryCount(0)
      {

      }
};

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAAPCmdDiscoverer
 * \brief Device discovery wrapper for Android Auto
 */

class spi_tclAAPCmdDiscoverer
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPCmdDiscoverer::spi_tclAAPCmdDiscoverer();
       ***************************************************************************/
      /*!
       * \fn     spi_tclAAPCmdDiscoverer()
       * \brief  Default Constructor
       * \sa      ~spi_tclAAPCmdDiscoverer()
       **************************************************************************/
      spi_tclAAPCmdDiscoverer();

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclAAPCmdDiscoverer::~spi_tclAAPCmdDiscoverer()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclAAPCmdDiscoverer()
       * \brief   Destructor
       * \sa      spi_tclAAPCmdDiscoverer()
       **************************************************************************/
      virtual ~spi_tclAAPCmdDiscoverer();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdDiscoverer::bInitializeDiscoverer
       ***************************************************************************/
      /*!
       * \fn      t_Bool bInitializeDiscoverer()
       * \brief   Creates AAP discoverer and registers callbacks
       * \sa      vUnInitializeDiscoverer()
       * \retval  true if Creation of AAP Discoverer successful false otherwise
       **************************************************************************/
      t_Bool bInitializeDiscoverer();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vUnInitializeDiscoverer
       ***************************************************************************/
      /*!
       * \fn      t_Void vUnInitializeDiscoverer()
       * \brief   Destroys AAP discoverer
       * \sa      bInitializeDiscoverer()
       **************************************************************************/
      t_Void vUnInitializeDiscoverer();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdDiscoverer::bStartDeviceDiscovery
       ***************************************************************************/
      /*!
       * \fn      t_Bool bStartDeviceDiscovery()
       * \brief   Starts USB device monitoring to report new devices
       * \sa      vStopDeviceDiscovery()
       * \retval  true if device monitoring was successfully started false otherwise
       **************************************************************************/
      t_Bool bStartDeviceDiscovery();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vStopDeviceDiscovery
       ***************************************************************************/
      /*!
       * \fn      t_Void vStopDeviceDiscovery()
       * \brief   Stops monitoring USB devices
       * \sa      bStartDeviceDiscovery()
       **************************************************************************/
      t_Void vStopDeviceDiscovery();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vSetHeadUnitInfo
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetHeadUnitInfo()
       * \brief   Sets the Headunit information needed for AOAP switch
       * \param   rfrHeadUnitInfo: Reference to the structure containing head unit info
       **************************************************************************/
      t_Void vSetHeadUnitInfo(trAAPHeadUnitInfo &rfrHeadUnitInfo);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdDiscoverer::bSwitchToAOAPMode
       ***************************************************************************/
      /*!
       * \fn      t_Bool bSwitchToAOAPMode()
       * \brief   Requests the device to switch to AOAP mode.
       * \param   cou32DeviceHandle: Device handle
       * \sa      vSwitchtoUSBMode()
       * \retval  true if device was successfully switched to AOAP mode false otherwise
       **************************************************************************/
      t_Bool bSwitchToAOAPMode(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdDiscoverer::bSwitchToUSBMode
       ***************************************************************************/
      /*!
       * \fn      t_Bool bSwitchToUSBMode()
       * \brief   Resets the USB connection to reenumerate the USb device
       * \param   cou32DeviceHandle: Device handle
       * \sa      bSwitchToAOAPMode()
       * \retval  true if USB reset was successfull on the device false otherwise
       **************************************************************************/
      t_Bool bSwitchToUSBMode(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vRetryUSBReset
       ***************************************************************************/
      /*!
       * \fn      t_Void vRetryUSBReset()
       * \brief   Retries USB Reset
       * \param   cou32DeviceHandle: Device handle
       * \sa      bSwitchToUSBMode()
       **************************************************************************/
      t_Void vRetryUSBReset(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vGetSessionInfo
       ***************************************************************************/
      /*!
       * \fn      t_Void vGetSessionInfo()
       * \brief   Populates the session info required to start transport
       * \param   cou32DeviceHandle: Device handle
       * \param   rfrAAPSession: [OUT] Reference to structure to be populated with session info
       **************************************************************************/
      t_Void vGetSessionInfo(const t_U32 cou32DeviceID, trAAPSessionInfo &rfrAAPSession);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vClearSessionInfo
       ***************************************************************************/
      /*!
       * \fn      t_Void vClearSessionInfo()
       * \brief   Clears session info 
       * \param   cou32DeviceHandle: Device handle
       **************************************************************************/
      t_Void vClearSessionInfo(const t_U32 cou32DeviceID);

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION: spi_tclAAPCmdDiscoverer(const spi_tclAAPCmdDiscoverer &rfcoobjCRCBResp)
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPCmdDiscoverer(const spi_tclAAPCmdDiscoverer &rfcoobjCRCBResp)
       * \brief   Copy constructor not implemented hence made protected
       **************************************************************************/
      spi_tclAAPCmdDiscoverer(const spi_tclAAPCmdDiscoverer &rfcoobjCRCBResp);

      /***************************************************************************
       ** FUNCTION: const spi_tclAAPCmdDiscoverer & operator=(
       **                                 const spi_tclAAPCmdDiscoverer &rfcoobjCRCBResp);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclAAPCmdDiscoverer & operator=(const spi_tclAAPCmdDiscoverer &objCRCBResp);
       * \brief   assignment operator not implemented hence made protected
       **************************************************************************/
      const spi_tclAAPCmdDiscoverer & operator=(const spi_tclAAPCmdDiscoverer &rfcoobjCRCBResp);

      /***************************************************************************
       ** FUNCTION:  t_U32 spi_tclAAPCmdDiscoverer::u32GenerateUniqueDeviceID
       ***************************************************************************/
      /*!
       * \fn      t_U32 u32GenerateUniqueDeviceID()
       * \brief   Generates unique device ID by calculating crc with product vendor and serial number
       * \param   rfrUSBDeviceInfo : Contains information of the USB device
       **************************************************************************/
      t_U32 u32GenerateUniqueDeviceID(trUSBDeviceInfo &rfrUSBDeviceInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vCopyAAPDeviceInfo
       ***************************************************************************/
      /*!
       * \fn      t_Void vCopyAAPDeviceInfo()
       * \brief   Copies Android auto device info to a different structure
       **************************************************************************/
      t_Void vCopyAAPDeviceInfo(trDeviceInfo &rfrDeviceInfo, t_usbDeviceInformation *prUSBDevInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vCopyUSBDeviceInfo
       ***************************************************************************/
      /*!
       * \fn      t_Void vCopyUSBDeviceInfo()
       * \brief   Copies USB device info to a different structure
       **************************************************************************/
      t_Void vCopyUSBDeviceInfo(trUSBDeviceInfo &rfrUSBDeviceInfo, const t_usbDeviceInformation *prUSBDevInfo);

      //! AAP callbacks
      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vUSBEntityAppearedCb
       ***************************************************************************/
      /*!
       * \fn      t_Void vUSBEntityAppearedCb()
       * \brief   Callback from USB discoverer when a new USb device is detected
       * \param   pContext: context passed during registration
       * \param   prUSBDevInfo: Structure containing USB info
       * \param   s32Result: Result
       **************************************************************************/
      static t_Void vUSBEntityAppearedCb(t_Void *pContext, t_usbDeviceInformation *prUSBDevInfo, t_S32 s32Result);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vUSBEntityDisappearedCb
       ***************************************************************************/
      /*!
       * \fn      t_Void vUSBEntityDisappearedCb()
       * \brief   Callback from USB discoverer when a USB device disappears
       * \param   pContext: context passed during registration
       * \param   prUSBDevInfo: Structure containing USB info
       * \param   s32Result: Result
       **************************************************************************/
      static t_Void vUSBEntityDisappearedCb(t_Void *pContext, t_usbDeviceInformation *prUSBDevInfo, t_S32 s32Result);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vAOAPEntityAppearedCb
       ***************************************************************************/
      /*!
       * \fn      t_Void vAOAPEntityAppearedCb()
       * \brief   Callback from discoverer when a device is detected in AOAP mode
       * \param   pContext: context passed during registration
       * \param   prUSBDevInfo: Structure containing USB info
       * \param   s32Result: Result
       **************************************************************************/
      static t_Void vAOAPEntityAppearedCb(t_Void *pContext, t_usbDeviceInformation *prUSBDevInfo, t_S32 s32Result);

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPCmdDiscoverer::bUSBResetTimerCb
       ***************************************************************************/
      /*!
       * \fn     bUSBResetTimerCb
       * \brief  called on expiry of USB reset timer
       * \param  rTimerID: ID of the timer which has expired
       * \param  pvObject: pointer to object passed while starting the timer
       * \param  pvUserData: data passed during start of the timer
       **************************************************************************/
      static t_Bool bUSBResetTimerCb(timer_t rTimerID, t_Void *pvObject,
               const t_Void *pvUserData);

      //! message context utility
      static MsgContext m_oMsgContext;

      //! Discoverer pointer
      AOAP::UsbDiscoverer *m_poDiscoverer;

      //! Stores head unit information
      trAAPHeadUnitInfo m_rHeadUnitInfo;

      //! Stores session information
      std::map<t_U32, trAAPDeviceInfo> m_mapDeviceInfo;

      //! Lock to protect m_mapDeviceInfo
      Lock m_oLockDeviceInfo;

      //! Lock to protect critical section of switchAOAP mode result and AOAPEntityAppearedCb
      Lock m_oLockAOAPSwitch;

      //! Indicates if a aoap switch is already in progress.
      trAOAPSwitchInfo m_rAOAPSwitchInfo;

      //! Stores USB reset info
      std::map<t_U32, trUSBResetInfo> m_mapUSBDeviceResetInfo;

      //! Lock to protect m_mapUSBDeviceResetInfo
      Lock m_oLockUSBResetInfo;
};

#endif /* SPI_AAPCMDDISCOVERER_H_ */
