/***********************************************************************/
/*!
 * \file  spi_tclAAPDiscovererDispatcher.h
 * \brief Message Dispatcher for Discoverer Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Discoverer Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.03.2015  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/

#include "RespRegister.h"
#include "spi_tclAAPDiscovererDispatcher.h"
#include "spi_tclAAPRespDiscoverer.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
#include "trcGenProj/Header/spi_tclAAPDiscovererDispatcher.cpp.trc.h"
#endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleDiscovererMsg(this);             \
   }                                                        \
   vDeAllocateMsg();                                        \
}

/***************************************************************************
 ** FUNCTION:  DeviceInfoMsg::DeviceInfoMsg
 ***************************************************************************/
AAPDiscMsgBase::AAPDiscMsgBase(): m_u32DeviceHandle(0)
{
   vSetServiceID (e32MODULEID_AAPDISCOVERER);
}

/***************************************************************************
 ** FUNCTION:  DeviceInfoMsg::DeviceInfoMsg
 ***************************************************************************/
DeviceInfoMsg::DeviceInfoMsg() :  m_prDeviceInfo(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  DeviceInfoMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(DeviceInfoMsg, spi_tclAAPDiscovererDispatcher);

/***************************************************************************
 ** FUNCTION:  DeviceInfoMsg::vAllocateMsg
 ***************************************************************************/
t_Void DeviceInfoMsg::vAllocateMsg()
{
   m_prDeviceInfo = new trDeviceInfo;
   SPI_NORMAL_ASSERT(NULL == m_prDeviceInfo);
}

/***************************************************************************
 ** FUNCTION:  DeviceInfoMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void DeviceInfoMsg::vDeAllocateMsg()
{
   RELEASE_MEM(m_prDeviceInfo);
}

/***************************************************************************
 ** FUNCTION:  AAPDeviceDisconnectionMsg::AAPDeviceDisconnectionMsg
 ***************************************************************************/
AAPDeviceDisconnectionMsg::AAPDeviceDisconnectionMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPDeviceDisconnectionMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPDeviceDisconnectionMsg, spi_tclAAPDiscovererDispatcher);


/***************************************************************************
 ** FUNCTION:  spi_tclAAPDiscovererDispatcher::spi_tclAAPDiscovererDispatcher
 ***************************************************************************/
spi_tclAAPDiscovererDispatcher::spi_tclAAPDiscovererDispatcher()
{
   ETG_TRACE_USR1((" spi_tclAAPDiscovererDispatcher::spi_tclAAPDiscovererDispatcher() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDiscovererDispatcher::~spi_tclAAPDiscovererDispatcher
 ***************************************************************************/
spi_tclAAPDiscovererDispatcher::~spi_tclAAPDiscovererDispatcher()
{
   ETG_TRACE_USR1((" spi_tclAAPDiscovererDispatcher::~spi_tclAAPDiscovererDispatcher() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDiscovererDispatcher::vHandleDiscovererMsg(DeviceInfoMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPDiscovererDispatcher::vHandleDiscovererMsg(DeviceInfoMsg* poDeviceInfoMsg) const
{
   ETG_TRACE_USR1(("  spi_tclAAPDiscovererDispatcher::vHandleDiscovererMsg(DeviceInfoMsg* poDeviceInfoMsg) entered \n"));
   if ((NULL != poDeviceInfoMsg) && (NULL != poDeviceInfoMsg->m_prDeviceInfo))
   {
      CALL_REG_OBJECTS(spi_tclAAPRespDiscoverer,
               e16AAP_DISCOVERER_REGID,
               vPostDeviceInfo(poDeviceInfoMsg->u32GetDeviceHandle(), *(poDeviceInfoMsg->m_prDeviceInfo)));
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDiscovererDispatcher::vHandleDiscovererMsg(AAPDeviceDisconnectionMsg* poDeviceDisconnMsg)
 ***************************************************************************/
t_Void spi_tclAAPDiscovererDispatcher::vHandleDiscovererMsg(AAPDeviceDisconnectionMsg* poDeviceDisconnMsg) const
{
   ETG_TRACE_USR1((" spi_tclAAPDiscovererDispatcher::vHandleDiscovererMsg(AAPDeviceDisconnectionMsg* poDeviceDisconnMsg) entered \n"));
   if (NULL != poDeviceDisconnMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespDiscoverer,
               e16AAP_DISCOVERER_REGID,
               vPostDeviceDisconnected(poDeviceDisconnMsg->u32GetDeviceHandle()));
   } // if (NULL != poDeviceInfoMsg)
}

