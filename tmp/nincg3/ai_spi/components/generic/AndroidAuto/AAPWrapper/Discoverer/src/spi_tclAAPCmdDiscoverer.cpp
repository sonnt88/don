/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdDiscoverer.cpp
 * \brief             Device discovery wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device discovery wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.02.2015 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "crc.h"
#include <dlt/dlt.h>
#include "StringHandler.h"
#include "SPITypes.h"
#include "AoapTransport.h"
#include "AautoLogging.h"
#include "spi_tclAAPCmdDiscoverer.h"
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPDiscovererDispatcher.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclAAPCmdDiscoverer.cpp.trc.h"
#endif
#endif

using namespace adit::aauto;

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define OTG_PORT_STRING_LIMITER  "gadget"
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
static const t_S32 scos32InvalidID = 0x7FFFFFFF;
//! Maximum AOAP switch delay in milliseconds
static const t_U32 scou32MaxSwitchDelay = 10000;
static const t_U32 scou32ResetIntervalinms = 4000;
static const t_U32 scou32MaxNoofUSBResetRetries = 4;

/***************************************************************************
 ** FUNCTION:  spi_tclAAPCmdDiscoverer::spi_tclAAPCmdDiscoverer();
 ***************************************************************************/
spi_tclAAPCmdDiscoverer::spi_tclAAPCmdDiscoverer(): m_poDiscoverer(NULL)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::spi_tclAAPCmdDiscoverer() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclAAPCmdDiscoverer::~spi_tclAAPCmdDiscoverer()
 ***************************************************************************/
spi_tclAAPCmdDiscoverer::~spi_tclAAPCmdDiscoverer()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdDiscoverer::~spi_tclAAPCmdDiscoverer() \n"));
   m_poDiscoverer = NULL;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdDiscoverer::bInitializeDiscoverer
 ***************************************************************************/
t_Bool spi_tclAAPCmdDiscoverer::bInitializeDiscoverer()
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::bInitializeDiscoverer(): Initializing USB discoverer and registering callbacks \n"));
   //! register spi_usb_discoverer callbacks and create discoverer object
   SpiUsbDiscovererCallbacks rDiscovererCbs;
   rDiscovererCbs.notifyDeviceFound = &spi_tclAAPCmdDiscoverer::vUSBEntityAppearedCb;
   rDiscovererCbs.notifyDeviceRemoved = &spi_tclAAPCmdDiscoverer::vUSBEntityDisappearedCb;
   rDiscovererCbs.notifyDeviceFoundAccessoryMode = &spi_tclAAPCmdDiscoverer::vAOAPEntityAppearedCb;

   //! register  logging
   aautoRegisterAppWithDLT("AAP", "Android Auto demo");
   //! register all AAUTO DLT context of aauto/platform */
   aautoRegisterCtxtWithDLT();

   //! Create USB discoverer and register callbacks
   m_poDiscoverer = new AOAP::UsbDiscoverer(this, &rDiscovererCbs);
   SPI_NORMAL_ASSERT(NULL == m_poDiscoverer);
   t_Bool bRetval = (NULL != m_poDiscoverer);
   return bRetval;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vUnInitializeDiscoverer
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vUnInitializeDiscoverer()
{
   ETG_TRACE_USR1(("  spi_tclAAPCmdDiscoverer::vUnInitializeDiscoverer(): uninitializing USB device discoverer \n"));
   RELEASE_MEM(m_poDiscoverer)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdDiscoverer::bStartDeviceDiscovery
 ***************************************************************************/
t_Bool spi_tclAAPCmdDiscoverer::bStartDeviceDiscovery()
{
   t_Bool bResult = false;
   if(NULL != m_poDiscoverer)
   {
      //! Start monitoring USB devices
      t_S32 s32ErrorCode = m_poDiscoverer->startMonitoring();
      bResult = (0 == s32ErrorCode);
   }
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::bStartDeviceDiscovery(): Started USB device discovery: Result = %d \n",
            ETG_ENUM(BOOL, bResult)));
   return bResult;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vStopDeviceDiscovery
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vStopDeviceDiscovery()
{
   t_Bool bResult = false;
   if(NULL != m_poDiscoverer)
   {
      //! Stop monitoring USB devices
      t_S32 s32ErrorCode = m_poDiscoverer->stopMonitoring();
      bResult = (0 == s32ErrorCode);
   }
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vStopDeviceDiscovery(): Stopping USB device discovery: Result = %d \n", ETG_ENUM(BOOL, bResult)));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vSetHeadUnitInfo
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vSetHeadUnitInfo(trAAPHeadUnitInfo & rfrHeadUnitInfo)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vSetHeadUnitInfo entered \n"));
   //! Store the headunit information
   m_rHeadUnitInfo = rfrHeadUnitInfo;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdDiscoverer::bSwitchToAOAPMode
 ***************************************************************************/
t_Bool spi_tclAAPCmdDiscoverer::bSwitchToAOAPMode(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::bSwitchToAOAPMode: Switching DeviceID = 0x%x to AOAP mode \n", cou32DeviceHandle));
   t_Bool bResult = false;

   //! Populate AOAP device information
   aoapDeviceInfo_t rAoapInfo;
   rAoapInfo.aoapAccessoryInfo.manufacturer = m_rHeadUnitInfo.szManufacturer.c_str();
   rAoapInfo.aoapAccessoryInfo.modelName = m_rHeadUnitInfo.szModelName.c_str();
   rAoapInfo.aoapAccessoryInfo.description = m_rHeadUnitInfo.szDescription.c_str();
   rAoapInfo.aoapAccessoryInfo.version = m_rHeadUnitInfo.szVersion.c_str();
   rAoapInfo.aoapAccessoryInfo.uri = m_rHeadUnitInfo.szURI.c_str();
   rAoapInfo.aoapAccessoryInfo.serial = m_rHeadUnitInfo.szSerial.c_str();
   rAoapInfo.aoapAccessoryInfo.enableAudio = m_rHeadUnitInfo.u32EnableAudio;

   //! Display Accessory info
   ETG_TRACE_USR4((" Device Accessory Information "));
   ETG_TRACE_USR4((" manufacturer-%s, ", rAoapInfo.aoapAccessoryInfo.manufacturer));
   ETG_TRACE_USR4((" modelName-%s, ", rAoapInfo.aoapAccessoryInfo.modelName));
   ETG_TRACE_USR4((" description-%s, ", rAoapInfo.aoapAccessoryInfo.description));
   ETG_TRACE_USR4((" version-%s, ", rAoapInfo.aoapAccessoryInfo.version));
   ETG_TRACE_USR4((" uri-%s, ", rAoapInfo.aoapAccessoryInfo.uri));
   ETG_TRACE_USR4((" serial-%s, ", rAoapInfo.aoapAccessoryInfo.serial));
   ETG_TRACE_USR4((" enableAudio-%d, ", rAoapInfo.aoapAccessoryInfo.enableAudio));

   aoapTransportInfo_t rTransportInfo;
   rTransportInfo.aoapAccessoryId = scos32InvalidID;
   rTransportInfo.aoapDeviceId = scos32InvalidID;

   //! Search for the device in the list and populate the required entries
   m_oLockDeviceInfo.s16Lock();
   std::map<t_U32, trAAPDeviceInfo>::iterator  itMapDevList = m_mapDeviceInfo.find(cou32DeviceHandle);
   if (m_mapDeviceInfo.end() != itMapDevList)
   {
      rAoapInfo.vendorId = (itMapDevList->second).rUSBDeviceInfo.u32VendorID;
      rAoapInfo.productId = (itMapDevList->second).rUSBDeviceInfo.u32ProductID;
      rAoapInfo.pSerial = strdup((itMapDevList->second).rUSBDeviceInfo.szSerialNumber.c_str());

      m_rAOAPSwitchInfo.bSwitchinProgress = true;
      m_rAOAPSwitchInfo.u32DeviceHandle = cou32DeviceHandle;
      //! Switch the device to AOAP mode
      ETG_TRACE_USR1((" Device to be switched to aoap mode found in the list and has VendorID=0x%x, ProductID = 0x%x and serial number = %s\n",
               rAoapInfo.vendorId, rAoapInfo.productId, rAoapInfo.pSerial));
      AoapDevice oAoapDevice;
      //! TODO Blocking call. to be checked if it is ok

      m_oLockAOAPSwitch.s16Lock();
      t_S32 s32Error = oAoapDevice.switchDevice(&rAoapInfo, &rTransportInfo, scou32MaxSwitchDelay);
      bResult = (0 == s32Error);

      if (true == bResult)
      {
         //! Store the session info. This information will be used in session class
         (itMapDevList->second).rSessionInfo.u32HeadUnitID = rTransportInfo.aoapAccessoryId;
         (itMapDevList->second).rSessionInfo.u32DeviceID = rTransportInfo.aoapDeviceId;
      }
      m_oLockAOAPSwitch.vUnlock();
   }
   m_oLockDeviceInfo.vUnlock();

   m_rAOAPSwitchInfo.bSwitchinProgress = bResult;
   ETG_TRACE_USR1((" Switching to AOAP mode: Result = %d for DeviceID = %d, AccessoryID=%d\n",
            ETG_ENUM(BOOL, bResult), rTransportInfo.aoapDeviceId, rTransportInfo.aoapAccessoryId));
   return bResult;
}


/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdDiscoverer::bSwitchToUSBMode
 ***************************************************************************/
t_Bool spi_tclAAPCmdDiscoverer::bSwitchToUSBMode(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::bSwitchToUSBMode : Switching DeviceID = 0x%x  to USB mode\n", cou32DeviceHandle));
   t_Bool bRetval = false;
   m_oLockDeviceInfo.s16Lock();
   t_Bool bValidDevice = false;
   //! Fetch the device information to reset the USB connection
   t_usbDeviceInformation rA0APDeviceInfo;
   if (m_mapDeviceInfo.end() != m_mapDeviceInfo.find(cou32DeviceHandle))
   {
      bValidDevice = true;
      rA0APDeviceInfo.aoapSupported =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.bIsAOAPSupported;
      rA0APDeviceInfo.devNum =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.u32DeviceNumber;
      rA0APDeviceInfo.interface =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.u32Interface;
      rA0APDeviceInfo.manufacturer =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.szmanufacturer;
      rA0APDeviceInfo.product =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.szProduct;
      rA0APDeviceInfo.productId =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.u32ProductID;
      rA0APDeviceInfo.serial =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.szSerialNumber;
      rA0APDeviceInfo.sysPath =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.szSystemPath;
      rA0APDeviceInfo.vendorId =  m_mapDeviceInfo[cou32DeviceHandle].rA0APDeviceInfo.u32VendorID;
   }
   m_oLockDeviceInfo.vUnlock();

   if (true == bValidDevice)
   {
      if (NULL != m_poDiscoverer)
      {
         //! The following call will trigger a software reset on USB BUS
         bRetval = m_poDiscoverer->resetDevice(&rA0APDeviceInfo);
         ETG_TRACE_USR4((" spi_tclAAPCmdDiscoverer::Issuing reset command: Result = %d for the Systempath = %s ", ETG_ENUM(BOOL,
                     bRetval), rA0APDeviceInfo.sysPath.c_str()));
      }
   }
   return bRetval;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vRetryUSBReset
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vRetryUSBReset(const t_U32 cou32DeviceHandle)
{
   //! If USB reset has failed. try to reset the device for scou32NoofUSBResetRetries times
   //! at an interval of scou32ResetIntervalinms seconds
   //! Start the reset timer only for the first trial of USB reset

   //! Check if the device is valid by checking the device list
   m_oLockDeviceInfo.s16Lock();
   t_Bool bValidDevice = false;
   if (m_mapDeviceInfo.end() != m_mapDeviceInfo.find(cou32DeviceHandle))
   {
      bValidDevice = true;
   }
   m_oLockDeviceInfo.vUnlock();

   //! Check if the device is not already under USB reset retrial.
   //! If the USB reset retrail is already in progress for the device, don't start new timer
   m_oLockUSBResetInfo.s16Lock();
   if (((true == bValidDevice))&&(m_mapUSBDeviceResetInfo.end() != m_mapUSBDeviceResetInfo.find(cou32DeviceHandle)))
   {
      bValidDevice = false;
   }
   m_oLockUSBResetInfo.vUnlock();

   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vRetryUSBReset : Retrying USB reset for device = 0x%x bValidDevice = %d\n",
         cou32DeviceHandle, ETG_ENUM(BOOL, bValidDevice)));

   Timer* poTimer = Timer::getInstance();
   if ((NULL != poTimer) && (true == bValidDevice))
   {
      //! Start the USB reset retrial timer. On the expirty of this timer, USB reset will be attempted
      timer_t rTimerID;
      poTimer->StartTimer(rTimerID, scou32ResetIntervalinms, scou32ResetIntervalinms, this,
            &spi_tclAAPCmdDiscoverer::bUSBResetTimerCb, NULL);

      //! Store the USB reset retrial details
      trUSBResetInfo rUSBResetInfo;
      rUSBResetInfo.oTimerID = rTimerID;
      rUSBResetInfo.u32RetryCount = 0;
      ETG_TRACE_USR4((" Started USB reset Retrail timer for device = 0x%x with Timer ID = %d\n", cou32DeviceHandle,rTimerID));
      m_oLockUSBResetInfo.s16Lock();
      m_mapUSBDeviceResetInfo.insert(std::pair<t_U32, trUSBResetInfo>(cou32DeviceHandle, rUSBResetInfo));
      m_oLockUSBResetInfo.vUnlock();
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vGetSessionInfo
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vGetSessionInfo(const t_U32 cou32DeviceID, trAAPSessionInfo &rfrAAPSession)
{
   //! Returns session information for the Device handle cou32DeviceID
   m_oLockDeviceInfo.s16Lock();
   if(m_mapDeviceInfo.end() != m_mapDeviceInfo.find(cou32DeviceID))
   {
      rfrAAPSession.u32DeviceID = m_mapDeviceInfo[cou32DeviceID].rSessionInfo.u32DeviceID;
      rfrAAPSession.u32HeadUnitID = m_mapDeviceInfo[cou32DeviceID].rSessionInfo.u32HeadUnitID;
   }
   m_oLockDeviceInfo.vUnlock();
   ETG_TRACE_USR4(("spi_tclAAPCmdDiscoverer::vGetSessionInfo: Session information for Device = %d :  DeviceID = %d, HeadUnit ID = %d \n",
            cou32DeviceID, rfrAAPSession.u32DeviceID, rfrAAPSession.u32HeadUnitID));
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vClearSessionInfo
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vClearSessionInfo(const t_U32 cou32DeviceID)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vClearSessionInfo: Clearing aoap session information for DeviceID = 0x%x \n", cou32DeviceID));

   m_oLockDeviceInfo.s16Lock();
   if(m_mapDeviceInfo.end() != m_mapDeviceInfo.find(cou32DeviceID))
   {
      m_mapDeviceInfo[cou32DeviceID].rSessionInfo.u32DeviceID = scou32InvalidID;
      m_mapDeviceInfo[cou32DeviceID].rSessionInfo.u32HeadUnitID = scou32InvalidID;
   }
   m_oLockDeviceInfo.vUnlock();
}

/***************************************************************************
 ** FUNCTION:  t_U32 spi_tclAAPCmdDiscoverer::u32GenerateUniqueDeviceID
 ***************************************************************************/
t_U32 spi_tclAAPCmdDiscoverer::u32GenerateUniqueDeviceID(trUSBDeviceInfo & rfrUSBDeviceInfo)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdDiscoverer::u32GenerateUniqueDeviceID  ProductID=0x%x VendorID= 0x%x SerialNumber=%s\n",
            rfrUSBDeviceInfo.u32ProductID, rfrUSBDeviceInfo.u32VendorID, rfrUSBDeviceInfo.szSerialNumber.c_str() ));
   t_U32 u32UniqueID = 0;

   //! TODO : Need confirmation from google for finding the unique device ID
   t_String szUniqueID = rfrUSBDeviceInfo.szSerialNumber;
   u32UniqueID = u16CalcCRC16((const t_UChar*)szUniqueID.c_str(),szUniqueID.size());
   ETG_TRACE_USR1(("Generated unique Device ID=0x%x for the unique string =%s\n",
            u32UniqueID, szUniqueID.c_str()));
   return u32UniqueID;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vCopyAAPDeviceInfo
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vCopyAAPDeviceInfo(trDeviceInfo &rfrDeviceInfo, t_usbDeviceInformation *prUSBDevInfo)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vCopyAAPDeviceInfo entered \n"));
   if(NULL != prUSBDevInfo)
   {
      trUSBDeviceInfo rUSBDevInfo;
      vCopyUSBDeviceInfo(rUSBDevInfo, prUSBDevInfo);
      rfrDeviceInfo.u32DeviceHandle = u32GenerateUniqueDeviceID(rUSBDevInfo);
      rfrDeviceInfo.enDeviceConnectionStatus = e8DEV_CONNECTED;
      rfrDeviceInfo.szDeviceManufacturerName = (prUSBDevInfo->product).c_str();
      rfrDeviceInfo.enDeviceCategory = e8DEV_TYPE_ANDROIDAUTO;
      rfrDeviceInfo.enDeviceConnectionType = e8USB_CONNECTED;
      rfrDeviceInfo.rProjectionCapability.enAndroidAutoSupport = e8SPI_SUPPORTED;
      rfrDeviceInfo.rProjectionCapability.enDeviceType = e8_ANDROID_DEVICE;
	  rfrDeviceInfo.rProjectionCapability.enUSBPortType = e8_PORT_TYPE_NOT_KNOWN;
      if(!(prUSBDevInfo->sysPath).empty())
    	  rfrDeviceInfo.rProjectionCapability.enUSBPortType =
    			  (prUSBDevInfo->sysPath).find(OTG_PORT_STRING_LIMITER) >= 0 ? e8_PORT_TYPE_OTG : e8_PORT_TYPE_NON_OTG;	  
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vCopyAAPDeviceInfo
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vCopyUSBDeviceInfo(trUSBDeviceInfo &rfrUSBDeviceInfo,const  t_usbDeviceInformation *prUSBDevInfo)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vCopyUSBDeviceInfo entered \n"));
   if(NULL != prUSBDevInfo)
   {
      rfrUSBDeviceInfo.szSerialNumber = prUSBDevInfo->serial;
      rfrUSBDeviceInfo.u32ProductID = prUSBDevInfo->productId;
      rfrUSBDeviceInfo.u32VendorID = prUSBDevInfo->vendorId;
      rfrUSBDeviceInfo.szProduct = prUSBDevInfo->product;
      rfrUSBDeviceInfo.szSystemPath = prUSBDevInfo->sysPath;
      rfrUSBDeviceInfo.szmanufacturer = prUSBDevInfo->manufacturer;
      rfrUSBDeviceInfo.u32DeviceNumber = prUSBDevInfo->devNum;
      rfrUSBDeviceInfo.u32Interface = prUSBDevInfo->interface;
      rfrUSBDeviceInfo.bIsAOAPSupported = prUSBDevInfo->aoapSupported;
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vUSBEntityAppearedCb
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vUSBEntityAppearedCb(t_Void *pContext, t_usbDeviceInformation *prUSBDevInfo, t_S32 s32Result)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vUSBEntityAppearedCb: New USB device detected pContext = %p, prUSBDevInfo - %d, s32Result = %d \n",
         pContext, prUSBDevInfo, s32Result));
   if ((NULL != prUSBDevInfo) && (NULL != pContext))
   {
      ETG_TRACE_USR4(("Detected USB device has: AOAP support =%d, DeviceNo- %d, Interface-%d, productId- 0x%x, vendorId=0x%x, Product-%s ",
               ETG_ENUM(BOOL,prUSBDevInfo->aoapSupported), prUSBDevInfo->devNum, prUSBDevInfo->interface,  prUSBDevInfo->productId,
               prUSBDevInfo->vendorId, (prUSBDevInfo->product).c_str()));
      ETG_TRACE_USR4((" Manufacturer-%s, ", (prUSBDevInfo->manufacturer).c_str()));
      ETG_TRACE_USR4((" SystemPath-%s,  \n", (prUSBDevInfo->sysPath).c_str()));

      spi_tclAAPCmdDiscoverer* poCmdDisc = static_cast<spi_tclAAPCmdDiscoverer*> (pContext);
      trUSBDeviceInfo rUSBDeviceInfo;
      poCmdDisc->vCopyUSBDeviceInfo(rUSBDeviceInfo, prUSBDevInfo);
      t_U32 u32DeviceHandle = poCmdDisc->u32GenerateUniqueDeviceID(rUSBDeviceInfo);
      trAAPDeviceInfo rAAPDeviceINfo;
      rAAPDeviceINfo.rUSBDeviceInfo = rUSBDeviceInfo;
      //! Store the device USB information
      poCmdDisc->m_oLockDeviceInfo.s16Lock();
      poCmdDisc->m_mapDeviceInfo[u32DeviceHandle] = rAAPDeviceINfo;
      poCmdDisc->m_oLockDeviceInfo.vUnlock();

      //! Send the AOAP device information to the response interface
      DeviceInfoMsg oDevInfoMsg;
      oDevInfoMsg.vSetDeviceHandle(u32DeviceHandle);
      if (NULL != oDevInfoMsg.m_prDeviceInfo)
      {
         (oDevInfoMsg.m_prDeviceInfo)->u32DeviceHandle = u32DeviceHandle;
         (oDevInfoMsg.m_prDeviceInfo)->szDeviceManufacturerName = (prUSBDevInfo->manufacturer).c_str();
         //! Use the device category as Unknown until aoap switch is confirmed
         (oDevInfoMsg.m_prDeviceInfo)->enDeviceCategory = e8DEV_TYPE_UNKNOWN;

         (oDevInfoMsg.m_prDeviceInfo)->enDeviceConnectionStatus = e8DEV_CONNECTED;
         (oDevInfoMsg.m_prDeviceInfo)->enDeviceConnectionType = e8USB_CONNECTED;
         if (true == prUSBDevInfo->aoapSupported)
         {
            (oDevInfoMsg.m_prDeviceInfo)->rProjectionCapability.enAndroidAutoSupport = e8SPI_SUPPORTED;
            (oDevInfoMsg.m_prDeviceInfo)->rProjectionCapability.enDeviceType = e8_ANDROID_DEVICE;
         }
         else
         {
            (oDevInfoMsg.m_prDeviceInfo)->rProjectionCapability.enAndroidAutoSupport = e8SPI_NOTSUPPORTED;
            (oDevInfoMsg.m_prDeviceInfo)->rProjectionCapability.enDeviceType = e8_UNKNOWN_DEVICE;
         }
      }
	  
	  (oDevInfoMsg.m_prDeviceInfo)->rProjectionCapability.enUSBPortType = e8_PORT_TYPE_NOT_KNOWN;
      if(!(prUSBDevInfo->sysPath).empty())
      {
		  //! Extract information for device is device connected to OTG port
		  (oDevInfoMsg.m_prDeviceInfo)->rProjectionCapability.enUSBPortType =
							  (prUSBDevInfo->sysPath).find(OTG_PORT_STRING_LIMITER) >= 0 ? e8_PORT_TYPE_OTG :e8_PORT_TYPE_NON_OTG;

		  ETG_TRACE_USR4(("OTG port supported =%d  \n", ETG_ENUM(USB_PORT_TYPE, (oDevInfoMsg.m_prDeviceInfo)->rProjectionCapability.enUSBPortType)));
      }

      spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
      if (NULL != poMsgQinterface)
      {
         poMsgQinterface->bWriteMsgToQ(&oDevInfoMsg, sizeof(oDevInfoMsg));
      }//if (NULL != poMsgQinterface)
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vUSBEntityDisappearedCb
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vUSBEntityDisappearedCb(t_Void *pContext,
         t_usbDeviceInformation *prUSBDevInfo, t_S32 s32Result)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vUSBEntityDisappearedCb USB device was removed pContext = %p, prUSBDevInfo - %d, s32Result = %d \n",
            pContext, prUSBDevInfo, s32Result));

   if ((NULL != prUSBDevInfo) && (NULL != pContext))
   {
      spi_tclAAPCmdDiscoverer* poCmdDisc = static_cast<spi_tclAAPCmdDiscoverer*> (pContext);

      ETG_TRACE_USR4(("Removed device has DeviceNo- %d, Interface-%d, productId- 0x%x, vendorId=0x%x. AOAP Switch is in Progress = %d for Device = 0x%x",
            prUSBDevInfo->devNum, prUSBDevInfo->interface, prUSBDevInfo->productId, prUSBDevInfo->vendorId,
            ETG_ENUM(BOOL,poCmdDisc->m_rAOAPSwitchInfo.bSwitchinProgress),  poCmdDisc->m_rAOAPSwitchInfo.u32DeviceHandle));

      //! Copy USB device info and calculate the device handle
      trUSBDeviceInfo rUSBDeviceInfo;
      poCmdDisc->vCopyUSBDeviceInfo(rUSBDeviceInfo,prUSBDevInfo);
      t_U32 u32DeviceHandle = poCmdDisc->u32GenerateUniqueDeviceID(rUSBDeviceInfo);


      Timer* poTimer = Timer::getInstance();
      //! Cancel the timer if the USB reset is successful and also delete the USB reset details for the device
      poCmdDisc->m_oLockUSBResetInfo.s16Lock();
      ETG_TRACE_USR4(("No of devices under USB Reset = %d \n", poCmdDisc->m_mapUSBDeviceResetInfo.size()));
      std::map<t_U32, trUSBResetInfo>::iterator itMapUSBDev = poCmdDisc->m_mapUSBDeviceResetInfo.find(u32DeviceHandle);
      if (itMapUSBDev != poCmdDisc->m_mapUSBDeviceResetInfo.end())
      {
         trUSBResetInfo rUSBResetInfo = itMapUSBDev->second;
         if ((0 != rUSBResetInfo.oTimerID) && (NULL != poTimer))
         {
            ETG_TRACE_USR4(("USB reset has succeeded for device 0x%x or the device is disconnected by user."
               " Cancelling USB reset retrial timer\n ", itMapUSBDev->first));
            poTimer->CancelTimer(rUSBResetInfo.oTimerID);
            //! Clear USB reset device info as the USB reset as succeeded
            if (itMapUSBDev != poCmdDisc->m_mapUSBDeviceResetInfo.end())
            {
               poCmdDisc->m_mapUSBDeviceResetInfo.erase(itMapUSBDev);
            }
         }
      }
      poCmdDisc->m_oLockUSBResetInfo.vUnlock();


      poCmdDisc->m_oLockDeviceInfo.s16Lock();
      //! Clear device information from map if the device has disappeared before or after aoap switch
      if ((poCmdDisc->m_mapDeviceInfo.end() != poCmdDisc->m_mapDeviceInfo.find(u32DeviceHandle))
               && ((false == poCmdDisc->m_rAOAPSwitchInfo.bSwitchinProgress)||
                     (u32DeviceHandle !=  poCmdDisc->m_rAOAPSwitchInfo.u32DeviceHandle)))
      {
         poCmdDisc->m_mapDeviceInfo.erase(u32DeviceHandle);

         //! Send the device disconnection message to response interface
         AAPDeviceDisconnectionMsg oDevDisconnMsg;
         oDevDisconnMsg.vSetDeviceHandle(u32DeviceHandle);
         spi_tclAAPMsgQInterface *poMsgQinterface =
                  spi_tclAAPMsgQInterface::getInstance();
         if (NULL != poMsgQinterface)
         {
            poMsgQinterface->bWriteMsgToQ(&oDevDisconnMsg, sizeof(oDevDisconnMsg));
         }//if (NULL != poMsgQinterface)
      }
      poCmdDisc->m_oLockDeviceInfo.vUnlock();
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::vAOAPEntityAppearedCb
 ***************************************************************************/
t_Void spi_tclAAPCmdDiscoverer::vAOAPEntityAppearedCb(t_Void *pContext, t_usbDeviceInformation *prUSBDevInfo,
         t_S32 s32Result)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdDiscoverer::vAOAPEntityAppearedCb: AOAP Device is detected pContext = %p, prUSBDevInfo - %d, s32Result = %d \n",
         pContext, prUSBDevInfo, s32Result));
   if ((NULL != prUSBDevInfo) && (NULL != pContext))
   {
      ETG_TRACE_USR4(("Detected AOAP device has DeviceNo- %d, Interface-%d, productId- %d, vendorId=%d and systempath = %s\n",
            prUSBDevInfo->devNum, prUSBDevInfo->interface, prUSBDevInfo->productId, prUSBDevInfo->vendorId,
            prUSBDevInfo->sysPath.c_str()));
      spi_tclAAPCmdDiscoverer* poCmdDisc = static_cast<spi_tclAAPCmdDiscoverer*> (pContext);

      poCmdDisc->m_oLockAOAPSwitch.s16Lock();
      //! Clear aoap switch information
      poCmdDisc->m_rAOAPSwitchInfo.bSwitchinProgress = false;
      poCmdDisc->m_rAOAPSwitchInfo.u32DeviceHandle = 0;

      //! Inform aoap entity to connection manager
      DeviceInfoMsg oDeviceInfoMsg;
      if (NULL != oDeviceInfoMsg.m_prDeviceInfo)
      {
         poCmdDisc->vCopyAAPDeviceInfo(*(oDeviceInfoMsg.m_prDeviceInfo), prUSBDevInfo);
         oDeviceInfoMsg.vSetDeviceHandle((oDeviceInfoMsg.m_prDeviceInfo)->u32DeviceHandle);

         t_Bool bIsUSBResetNeeded = false;
         poCmdDisc->m_oLockDeviceInfo.s16Lock();
         if ((poCmdDisc->m_mapDeviceInfo.end()
                  == poCmdDisc->m_mapDeviceInfo.find((oDeviceInfoMsg.m_prDeviceInfo)->u32DeviceHandle)))
         {
            //! If the device is already in AOAP mode then Reset the device
            bIsUSBResetNeeded = true;
         }

         //! Store the device information if it doesn't exist
         trUSBDeviceInfo rAOAPDeviceInfo;
         //! Copy AOAP information
         poCmdDisc->vCopyUSBDeviceInfo(rAOAPDeviceInfo, prUSBDevInfo);
         poCmdDisc->m_mapDeviceInfo[(oDeviceInfoMsg.m_prDeviceInfo)->u32DeviceHandle].rA0APDeviceInfo = rAOAPDeviceInfo;
         poCmdDisc->m_oLockDeviceInfo.vUnlock();

         //! If the device has directly appeared in aoap mode, don't report the device
         //! to connection manager as AAP session cannot be established. instead try doing a USB reset for the device
         if(true == bIsUSBResetNeeded)
         {
            poCmdDisc->bSwitchToUSBMode((oDeviceInfoMsg.m_prDeviceInfo)->u32DeviceHandle);
            poCmdDisc->vRetryUSBReset((oDeviceInfoMsg.m_prDeviceInfo)->u32DeviceHandle);
         }
         else
         {
            spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
            if (NULL != poMsgQinterface)
            {
               poMsgQinterface->bWriteMsgToQ(&oDeviceInfoMsg, sizeof(oDeviceInfoMsg));
            }//if (NULL != poMsgQinterface)
         }
      }
      poCmdDisc->m_oLockAOAPSwitch.vUnlock();
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdDiscoverer::bUSBResetTimerCb
 ***************************************************************************/
t_Bool spi_tclAAPCmdDiscoverer::bUSBResetTimerCb(timer_t rTimerID, t_Void *pvObject,
         const t_Void *pvUserData)
{
   SPI_INTENTIONALLY_UNUSED(pvUserData);
   ETG_TRACE_USR1(("spi_tclAAPCmdDiscoverer::bUSBResetTimerCb : USB reset failed: Trying again .\n"));
   spi_tclAAPCmdDiscoverer* poCmdDiscoverer = static_cast<spi_tclAAPCmdDiscoverer*> (pvObject);
   Timer* poTimer = Timer::getInstance();
   //! Get the Device info for the corresponding timer ID
   if (NULL != poCmdDiscoverer)
   {
      //! Fetch the USB reset info for the timer indicated by rTimerID
      trUSBResetInfo rUSBResetInfo;
      t_U32 u32DeviceHandle = 0;
      poCmdDiscoverer->m_oLockUSBResetInfo.s16Lock();
      std::map<t_U32, trUSBResetInfo>::iterator itMapUSBResetInfo;
      ETG_TRACE_USR4(("No of devices under USB Reset = %d, timer ID = %d \n",
            poCmdDiscoverer->m_mapUSBDeviceResetInfo.size(), rTimerID));
      //! Iterate throught the map to find the USB reset details corresponding to this timer
      for (itMapUSBResetInfo = poCmdDiscoverer->m_mapUSBDeviceResetInfo.begin(); itMapUSBResetInfo
            != poCmdDiscoverer->m_mapUSBDeviceResetInfo.end(); itMapUSBResetInfo++)
      {
         if (rTimerID == (itMapUSBResetInfo->second).oTimerID)
         {
            rUSBResetInfo = itMapUSBResetInfo->second;
            u32DeviceHandle = (itMapUSBResetInfo->first);
            break;
         }
      }
      //!  Retry USB reset if the USB Reset Details are found in the list
      if ((NULL != poTimer) && (0 != u32DeviceHandle) && (poCmdDiscoverer->m_mapUSBDeviceResetInfo.end() != itMapUSBResetInfo))
      {
         ((itMapUSBResetInfo->second).u32RetryCount)++;
         ETG_TRACE_USR4((" Retry count =%d for Device = 0x%x\n", (itMapUSBResetInfo->second).u32RetryCount, u32DeviceHandle));
         poCmdDiscoverer->bSwitchToUSBMode(u32DeviceHandle);
         //! Cancel the reset timer after maximum number of retries
         if ((0 != rTimerID)
               && (((itMapUSBResetInfo->second).u32RetryCount)
                     == scou32MaxNoofUSBResetRetries))
         {
            ETG_TRACE_ERR(("Maximum no of retrials for USB reset reached. Cancelling timer.\n"));
            poTimer->CancelTimer(rTimerID);
            //! Clear USB Reset device info
            if(itMapUSBResetInfo!= poCmdDiscoverer->m_mapUSBDeviceResetInfo.end() )
            {
               poCmdDiscoverer->m_mapUSBDeviceResetInfo.erase(itMapUSBResetInfo);
            }
         }
      }
      poCmdDiscoverer->m_oLockUSBResetInfo.vUnlock();
   }
   return true;
}
