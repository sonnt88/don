/***********************************************************************/
/*!
* \file  spi_tclAAPRespSession.h
* \brief AAP Session Output Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Session Output Interface
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                   | Modification
03.03.2015  | Pruthvi Thej Nagaraju    | Initial Version
\endverbatim
*************************************************************************/

#ifndef SPI_TCLAAPRESPSESSION_H_
#define SPI_TCLAAPRESPSESSION_H_

#include "RespBase.h"
#include "AAPTypes.h"
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclAAPRespSession
* \brief AAP Session Output Interface
*
* updates all the registered clients, whenever there is
* an update on AAP session
*
****************************************************************************/
class spi_tclAAPRespSession:public RespBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespSession::spi_tclAAPRespSession()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespSession()
   * \brief   Constructor
   * \sa      ~spi_tclAAPRespSession()
   **************************************************************************/
   spi_tclAAPRespSession():RespBase(e16AAP_SESSION_REGID)
   {

   }

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespSession::~spi_tclAAPRespSession()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclAAPRespSession()
   * \brief   Destructor
   * \param   t_Void
   * \sa      spi_tclAAPRespSession(RegID enRegId)
   **************************************************************************/
   virtual ~spi_tclAAPRespSession(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespSession::vServiceDiscoveryRequestCb
   ***************************************************************************/
   /*!
   * \fn     vServiceDiscoveryRequestCb
   * \brief   Called when service discovery request is received.
   *          This call sends across an icon set and a label that can be
   *          used by the native UI to display a button that allows
   *          users to switch back to projected mode.
   * \param     szSmallIcon   32x32 png image.
   * \param     szMediumIcon  64x64 png image.
   * \param     szLargeICon   128x128 png image.
   * \param     szLabel  A label that may be displayed alongside the icon.
   * \param     szDeviceName Name of Device
   ***************************************************************************/
   virtual t_Void vServiceDiscoveryRequestCb(t_String szSmallIcon, t_String szMediumIcon, t_String szLargeICon , t_String szLabel , t_String szDeviceName){}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPSession::vUnrecoverableErrorCallback()
    ***************************************************************************/
   /*!
    * \fn      vUnrecoverableErrorCallback
    * \brief   called when phone detects a unrecoverable error:
    * advice is to reset USB on this call
    **************************************************************************/
   virtual t_Void vUnrecoverableErrorCallback(){}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPSession::vPingRequestCb()
    ***************************************************************************/
   /*!
    * \fn      vPingRequestCb
    * \brief   Called when the other end pings us.
    * \param   s64Timestamp   The (remote) timestamp of the request.
    * \param   bBugReport   Should a bug report be saved away? The implementer can choose
    *         to ignore this if it is sent with high frequency. It is in the end, the
    *         implementers responsibility to ensure that the system cannot be DoS'd
    *         if too many requests for bug reports are received.
    **************************************************************************/
   virtual t_Void vPingRequestCb(t_S64 s64TimeStamp, t_Bool bBugReport){}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPSession::vPingResponseCb()
    ***************************************************************************/
   /*!
    * \fn      vPingResponseCb
    * \brief   Called when the other end responds to our ping.
    * \param   s64Timestamp   The (remote) timestamp of the request.
    **************************************************************************/
   virtual t_Void vPingResponseCb(int64_t s64Timestamp){}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPSession::vNavigationFocusCb()
    ***************************************************************************/
   /*!
    * \fn      vNavigationFocusCb
    * \brief   Called when a navigation focus request is received from the phone.
    *  You must respond to this by calling Controller::setNavigationFocus()
    *  (even if there is no change in navigation focus). If navigation focus
    *  is given to the mobile device, all native turn by turn guidance
    *  systems must be stopped.
    * \param   enNavFocusType  he type requested (can be NAV_FOCUS_NATIVE or NAV_FOCUS_PROJECTED).
    **************************************************************************/
   virtual t_Void vNavigationFocusCb(tenAAPNavFocusType enNavFocusType){}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPSession::vByeByeRequestCb()
    ***************************************************************************/
   /*!
    * \fn      vByeByeRequestCb
    * \brief   Called when ByeByeRequest is received from phone. After taking necessary steps,
    * car side should send ByeByeResponse.
    * \param   enReason  The reason for the disconnection request.
    **************************************************************************/
   virtual t_Void vByeByeRequestCb(tenAAPByeByeReason enReason){}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPSession::vByeByeResponseCb()
    ***************************************************************************/
   /*!
    * \fn      vByeByeResponseCb
    * \brief  Called when ByeByeResponse is received from phone. Normally this is a reply for
    * ByeByeRequest message sent from car.
    **************************************************************************/
   virtual t_Void vByeByeResponseCb() {}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPSession::vVoiceSessionNotificationCb()
    ***************************************************************************/
   /*!
    * \fn      vVoiceSessionNotificationCb
    * \brief  CCalled when a voice session notification is received. Note that this callback only applies
    * to you if you do not always send a PTT short press to us always. If you always send PTT
    * short press to us, you should be able to ignore this call altogether.
    * \param enVoiceSessionStatus The status of the voice recongition session.
    **************************************************************************/
   virtual t_Void vVoiceSessionNotificationCb(tenAAPVoiceSessionStatus enVoiceSessionStatus){}

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::vAudioFocusRequestCb()
    ***************************************************************************/
   /*!
    * \fn      vAudioFocusRequestCb
    * \brief  Called when the source wishes to acquire audio focus.
    * \param enDevAudFocusRequest Can be one of AUDIO_FOCUS_GAIN, AUDIO_FOCUS_GAIN_TRANSIENT,
    *        AUDIO_FOCUS_GAIN_TRANSIENT_MAY_DUCK, AUDIO_FOCUS_RELEASE.
    **************************************************************************/
   virtual t_Void vAudioFocusRequestCb(tenAAPDeviceAudioFocusRequest enDevAudFocusRequest) {}

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::vSessionStatusInfo()
    ***************************************************************************/
   /*!
    * \fn      vSessionStatusInfo
    * \brief  informs the current session status of android auto session
    * \param  enSessionStatus : indicates current status of android auto session
    **************************************************************************/
   virtual t_Void vSessionStatusInfo(tenSessionStatus enSessionStatus, t_Bool bSessionTimedOut){}


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespSession::spi_tclAAPRespSession()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespSession(
   *                          const spi_tclAAPRespSession& corfoSrc))
   * \brief   Parameterized Constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \sa      spi_tclAAPRespSession(RegID enRegId)
   **************************************************************************/
   spi_tclAAPRespSession(const spi_tclAAPRespSession& corfoSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespSession& operator=( const spi_tclMLV...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespSession& operator=(
   *                          const spi_tclAAPRespSession& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespSession(const spi_tclAAPRespSession& otrSrc)
   ***************************************************************************/
   spi_tclAAPRespSession& operator=(const spi_tclAAPRespSession& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

};

#endif /* SPI_TCLAAPRESPSESSION_H_ */
