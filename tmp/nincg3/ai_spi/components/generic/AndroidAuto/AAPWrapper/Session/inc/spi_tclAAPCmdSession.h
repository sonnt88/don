/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdSession.h
 * \brief             Device session wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device session wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.02.2015 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAAPCMDSESSION_H_
#define SPI_TCLAAPCMDSESSION_H_

/******************************************************************************
 | includes:
 | 1)AAP - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include "AAPTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

class Transport;

/*!
 * \class spi_tclAAPCmdSession
 * \brief
 */

class spi_tclAAPCmdSession
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPCmdSession::spi_tclAAPCmdSession();
       ***************************************************************************/
      /*!
       * \fn     spi_tclAAPCmdSession()
       * \brief  Default Constructor
       * \sa      ~spi_tclAAPCmdSession()
       **************************************************************************/
      spi_tclAAPCmdSession();

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclAAPCmdSession::~spi_tclAAPCmdSession()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclAAPCmdSession()
       * \brief   Destructor
       * \sa      spi_tclAAPCmdSession()
       **************************************************************************/
      virtual ~spi_tclAAPCmdSession();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdSession::bInitializeSession()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bInitializeSession()
       * \brief   Initializes session by creating galreceiver
       * \param   cou32DeviceHandle : unique ID of ML Server
       * \retval  true : initialized successfull.
       * \retval  false : initialization failed
       * \sa      vUnInitializeSession()
       **************************************************************************/
      t_Bool bInitializeSession();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vUnInitializeSession( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vUnInitializeSession()
       * \brief   UnInitializes session by destroying galreceiver
       * \sa      bIntializeSession
       **************************************************************************/
      t_Void vUnInitializeSession();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdSession:: bStartTransport( )
       ***************************************************************************/
      /*!
       * \fn      t_Bool bStartTransport()
       * \brief   Start transport (communication between MD and HU
       * \sa      vStopTransport
       **************************************************************************/
      t_Bool bStartTransport(trAAPSessionInfo &rfrSessionInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vStopTransport( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vStopTransport()
       * \brief   Stop transport
       * \sa      bStartTransport
       **************************************************************************/
      t_Void vStopTransport();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdSession:: bSetCertificates( )
       ***************************************************************************/
      /*!
       * \fn      t_Bool spi_tclAAPCmdSession:: bSetCertificates( )
       * \brief   Set certificate path
       * \param   szCertiPath : certificate path
       * \param  enCertificateType : CertificateType to be used for authentication
       * \retval : returns true if certificates are set successfully
       **************************************************************************/
      t_Bool bSetCertificates(t_String szCertiPath, tenCertificateType enCertificatetype);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vSetHeadUnitInfo( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetHeadUnitInfo()
       * \brief   Sets info about the head unit for authentication
       * \param   rfrHeadUnitInfo : Head unit information
       **************************************************************************/
      t_Void vSetHeadUnitInfo(trAAPHeadUnitInfo &rfrHeadUnitInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vSendByeByeMessage()
       ***************************************************************************/
      /*!
       * \fn      t_Void vSendByeByeMessage()
       * \brief   Send Bye bye message to phone when user intentionally deselects aap
       * \param   rfrHeadUnitInfo : Head unit information
       **************************************************************************/
      t_Void vSendByeByeMessage();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPSession:: vSendByeByeResponse( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vSendByeByeResponse()
       * \brief   Send Bye bye response to phone in response
       * \param   rfrHeadUnitInfo : Head unit information
       **************************************************************************/
      t_Void vSendByeByeResponse();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdSession::vSetNavigationFocus()
       ***************************************************************************/
      /*!
       * \fn      vSetNavigationFocus(tenAAPNavFocusType enNavFocusType)
       * \brief   Set Navigation Focus type
       * \param   enNavFocusType : Navigation focus type
       **************************************************************************/
      t_Void vSetNavigationFocus(tenAAPNavFocusType enNavFocusType);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdSession::vSetAudioFocus()
       ***************************************************************************/
      /*!
       * \fn      vSetAudioFocus(AudioFocusStateType focusState, bool unsolicited)
       * \brief   Set Audio Focus type
       * \param
       **************************************************************************/
      t_Void vSetAudioFocus(tenAAPDeviceAudioFocusState enDevAudFocusState, bool bUnsolicited);

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION: spi_tclAAPCmdSession(const spi_tclAAPCmdSession &rfcoobjCRCBResp)
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPCmdSession(const spi_tclAAPCmdSession &rfcoobjCRCBResp)
       * \brief   Copy constructor not implemented hence made protected
       **************************************************************************/
      spi_tclAAPCmdSession(const spi_tclAAPCmdSession &rfcoobjCRCBResp);

      /***************************************************************************
       ** FUNCTION: const spi_tclAAPCmdSession & operator=(
       **                                 const spi_tclAAPCmdSession &rfcoobjCRCBResp);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclAAPCmdSession & operator=(const spi_tclAAPCmdSession &objCRCBResp);
       * \brief   assignment operator not implemented hence made protected
       **************************************************************************/
      const spi_tclAAPCmdSession & operator=(
               const spi_tclAAPCmdSession &rfcoobjCRCBResp);

};



#endif /* SPI_TCLAAPCMDSESSION_H_ */
