/***********************************************************************/
/*!
 * \file  spi_tclAAPSessionDispatcher.h
 * \brief Message Dispatcher for Session Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Session Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.03.2015  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/
#ifndef SPI_TCLAAPSessionDISPATCHER_H_
#define SPI_TCLAAPSessionDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespRegister.h"

/**************Forward Declarations******************************************/
class spi_tclAAPSessionDispatcher;

/****************************************************************************/
/*!
 * \class AAPSessionMsgBase
 * \brief Base Message type for all Session messages
 ****************************************************************************/
class AAPSessionMsgBase: public trMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  AAPSessionMsgBase::AAPSessionMsgBase
       ***************************************************************************/
      /*!
       * \fn      AAPSessionMsgBase()
       * \brief   Default constructor
       **************************************************************************/
      AAPSessionMsgBase();

      /***************************************************************************
       ** FUNCTION:  AAPSessionMsgBase::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   Pure virtual function to be overridden by inherited classes for
       *          dispatching the message
       * \param poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      virtual t_Void vDispatchMsg(
               spi_tclAAPSessionDispatcher* poSessionDispatcher) = 0;

      /***************************************************************************
       ** FUNCTION:  AAPSessionMsgBase::~AAPSessionMsgBase
       ***************************************************************************/
      /*!
       * \fn      ~AAPSessionMsgBase()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPSessionMsgBase()
      {

      }

      /***************************************************************************
       ** FUNCTION:  MLSessionMsgBase::u32GetDeviceHandle
       ***************************************************************************/
      /*!
       * \fn      u32GetDeviceHandle()
       * \brief   returns the device handle
       **************************************************************************/
      virtual t_U32 u32GetDeviceHandle(){return m_u32DeviceHandle;}

      /***************************************************************************
       ** FUNCTION:  MLSessionMsgBase::vSetDeviceHandle
       ***************************************************************************/
      /*!
       * \fn      vSetDeviceHandle()
       * \brief   sets the device handle
       **************************************************************************/
      virtual t_Void vSetDeviceHandle(const t_U32 cou32DevHndle) {m_u32DeviceHandle = cou32DevHndle;};

   private:
      t_U32 m_u32DeviceHandle;
};


/****************************************************************************/
/*!
 * \class AAPServiceDiscoveryMsg
 * \brief Session attestation response message
 ****************************************************************************/
class AAPServiceDiscoveryMsg: public AAPSessionMsgBase
{
   public:
      t_String *m_pszSmallIcon;
      t_String *m_pszMediumIcon;
      t_String *m_pszLargeIcon;
      t_String *m_pszLabel;
      t_String *m_pszDeviceName;

      /***************************************************************************
       ** FUNCTION:  AAPServiceDiscoveryMsg::AAPServiceDiscoveryMsg
       ***************************************************************************/
      /*!
       * \fn      AAPServiceDiscoveryMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPServiceDiscoveryMsg();

      /***************************************************************************
       ** FUNCTION:  AAPServiceDiscoveryMsg::~AAPServiceDiscoveryMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPServiceDiscoveryMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPServiceDiscoveryMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPServiceDiscoveryMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
      * \param  poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPServiceDiscoveryMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  AAPServiceDiscoveryMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class AAPUnrecoverableErrorMsg
 * \brief Session attestation response message
 ****************************************************************************/
class AAPUnrecoverableErrorMsg: public AAPSessionMsgBase
{
   public:

      /***************************************************************************
       ** FUNCTION:  AAPUnrecoverableErrorMsg::AAPUnrecoverableErrorMsg
       ***************************************************************************/
      /*!
       * \fn      AAPUnrecoverableErrorMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPUnrecoverableErrorMsg();

      /***************************************************************************
       ** FUNCTION:  AAPUnrecoverableErrorMsg::~AAPUnrecoverableErrorMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPUnrecoverableErrorMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPUnrecoverableErrorMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPUnrecoverableErrorMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
	   * \param  poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPUnrecoverableErrorMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPUnrecoverableErrorMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};


/****************************************************************************/
/*!
 * \class AAPNavigationFocusMsg
 * \brief Session attestation response message
 ****************************************************************************/
class AAPNavigationFocusMsg: public AAPSessionMsgBase
{
   public:

      tenAAPNavFocusType m_enNaviFocusType;

      /***************************************************************************
       ** FUNCTION:  AAPNavigationFocusMsg::AAPNavigationFocusMsg
       ***************************************************************************/
      /*!
       * \fn      AAPNavigationFocusMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPNavigationFocusMsg();

      /***************************************************************************
       ** FUNCTION:  AAPNavigationFocusMsg::~AAPNavigationFocusMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPNavigationFocusMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPNavigationFocusMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPNavigationFocusMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
      * \param  poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPNavigationFocusMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPNavigationFocusMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class AAPByeByeRequestMsg
 * \brief Session attestation response message
 ****************************************************************************/
class AAPByeByeRequestMsg: public AAPSessionMsgBase
{
   public:

      tenAAPByeByeReason m_enByeByeReason;

      /***************************************************************************
       ** FUNCTION:  AAPByeByeRequestMsg::AAPByeByeRequestMsg
       ***************************************************************************/
      /*!
       * \fn      AAPByeByeRequestMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPByeByeRequestMsg();

      /***************************************************************************
       ** FUNCTION:  AAPByeByeRequestMsg::~AAPByeByeRequestMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPByeByeRequestMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPByeByeRequestMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPByeByeRequestMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
      * \param  poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPByeByeRequestMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPByeByeRequestMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class AAPByeByeResponseMsg
 * \brief Session attestation response message
 ****************************************************************************/
class AAPByeByeResponseMsg: public AAPSessionMsgBase
{
   public:

      /***************************************************************************
       ** FUNCTION:  AAPByeByeResponseMsg::AAPByeByeResponseMsg
       ***************************************************************************/
      /*!
       * \fn      AAPByeByeResponseMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPByeByeResponseMsg();

      /***************************************************************************
       ** FUNCTION:  AAPByeByeResponseMsg::~AAPByeByeResponseMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPByeByeResponseMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPByeByeResponseMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPByeByeResponseMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
      * \param  poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPByeByeResponseMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPByeByeResponseMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class AAPVoiceSessionNotifMsg
 * \brief Session attestation response message
 ****************************************************************************/
class AAPVoiceSessionNotifMsg: public AAPSessionMsgBase
{
   public:

      tenAAPVoiceSessionStatus m_enVoiceSessionStatus;

      /***************************************************************************
       ** FUNCTION:  AAPVoiceSessionNotifMsg::AAPVoiceSessionNotifMsg
       ***************************************************************************/
      /*!
       * \fn      AAPVoiceSessionNotifMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPVoiceSessionNotifMsg();

      /***************************************************************************
       ** FUNCTION:  AAPVoiceSessionNotifMsg::~AAPVoiceSessionNotifMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPVoiceSessionNotifMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPVoiceSessionNotifMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPVoiceSessionNotifMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
      * \param  poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPVoiceSessionNotifMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPVoiceSessionNotifMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class AAPAudioFocusRequestMsg
 * \brief Session attestation response message
 ****************************************************************************/
class AAPAudioFocusRequestMsg: public AAPSessionMsgBase
{
   public:

      tenAAPDeviceAudioFocusRequest   m_enDevAudFocusRequest;

      /***************************************************************************
       ** FUNCTION:  AAPAudioFocusRequestMsg::AAPAudioFocusRequestMsg
       ***************************************************************************/
      /*!
       * \fn      AAPAudioFocusRequestMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPAudioFocusRequestMsg();

      /***************************************************************************
       ** FUNCTION:  AAPAudioFocusRequestMsg::~AAPAudioFocusRequestMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPAudioFocusRequestMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPAudioFocusRequestMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPAudioFocusRequestMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
      * \param  poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPAudioFocusRequestMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPAudioFocusRequestMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class AAPSessionStatusMsg
 * \brief Session status response message
 ****************************************************************************/
class AAPSessionStatusMsg: public AAPSessionMsgBase
{
   public:

      tenSessionStatus   m_enSessionstatus;
      t_Bool   m_bSessionTimedOut;

      /***************************************************************************
       ** FUNCTION:  AAPSessionStatusMsg::AAPSessionStatusMsg
       ***************************************************************************/
      /*!
       * \fn      AAPSessionStatusMsg()
       * \brief   Default constructor
       **************************************************************************/
      AAPSessionStatusMsg();

      /***************************************************************************
       ** FUNCTION:  AAPSessionStatusMsg::~AAPSessionStatusMsg
       ***************************************************************************/
      /*!
       * \fn      ~AAPSessionStatusMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPSessionStatusMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPSessionStatusMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
      * \param  poSessionDispatcher : pointer to Message dispatcher for Session
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPSessionDispatcher* poSessionDispatcher);

      /***************************************************************************
       ** FUNCTION:  AAPSessionStatusMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg(){}

      /***************************************************************************
       ** FUNCTION:  AAPSessionStatusMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg(){}
};

/****************************************************************************/
/*!
 * \class spi_tclAAPSessionDispatcher
 * \brief Message Dispatcher for Session Messages
 ****************************************************************************/
class spi_tclAAPSessionDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::spi_tclAAPSessionDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPSessionDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclAAPSessionDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::~spi_tclAAPSessionDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAAPSessionDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclAAPSessionDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPServiceDiscoveryMsg* poAAPServiceDiscoveryMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleSessionMsg(AAPServiceDiscoveryMsg* poAAPServiceDiscoveryMsg)
       * \brief   Handles Messages of AAPServiceDiscoveryMsg type
	   * \param   poLaunchSessionMsg : pointer to AAPServiceDiscoveryMsg.
       **************************************************************************/
      t_Void vHandleSessionMsg(AAPServiceDiscoveryMsg* poAAPServiceDiscoveryMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPUnrecoverableErrorMsg* poUnrecoverableErrorMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleSessionMsg(AAPUnrecoverableErrorMsg* poUnrecoverableErrorMsg)
       * \brief   Handles Messages of AAPUnrecoverableErrorMsg type
      * \param   poLaunchSessionMsg : pointer to AAPUnrecoverableErrorMsg.
       **************************************************************************/
      t_Void vHandleSessionMsg(AAPUnrecoverableErrorMsg* poUnrecoverableErrorMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPNavigationFocusMsg* poNavigationFocusMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleSessionMsg(AAPNavigationFocusMsg* poNavigationFocusMsg)
       * \brief   Handles Messages of AAPNavigationFocusMsg type
      * \param   poLaunchSessionMsg : pointer to AAPNavigationFocusMsg.
       **************************************************************************/
      t_Void vHandleSessionMsg(AAPNavigationFocusMsg* poNavigationFocusMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPByeByeRequestMsg* poByeByeRequestMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleSessionMsg(AAPByeByeRequestMsg* poByeByeRequestMsg)
       * \brief   Handles Messages of AAPByeByeRequestMsg type
      * \param   poLaunchSessionMsg : pointer to AAPByeByeRequestMsg.
       **************************************************************************/
      t_Void vHandleSessionMsg(AAPByeByeRequestMsg* poByeByeRequestMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPByeByeResponseMsg* poByeByeResponseMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleSessionMsg(AAPByeByeResponseMsg* poByeByeResponseMsg)
       * \brief   Handles Messages of AAPByeByeResponseMsg type
      * \param   poLaunchSessionMsg : pointer to AAPByeByeResponseMsg.
       **************************************************************************/
      t_Void vHandleSessionMsg(AAPByeByeResponseMsg* poByeByeResponseMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPVoiceSessionNotifMsg* poVoiceSessionNotifMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleSessionMsg(AAPVoiceSessionNotifMsg* poVoiceSessionNotifMsg)
       * \brief   Handles Messages of AAPVoiceSessionNotifMsg type
      * \param   poLaunchSessionMsg : pointer to AAPVoiceSessionNotifMsg.
       **************************************************************************/
      t_Void vHandleSessionMsg(AAPVoiceSessionNotifMsg* poVoiceSessionNotifMsg)const;


      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPAudioFocusRequestMsg* poAudioFocusRequestMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleSessionMsg(AAPAudioFocusRequestMsg* poAudioFocusRequestMsg)
       * \brief   Handles Messages of AAPAudioFocusRequestMsg type
      * \param   poLaunchSessionMsg : pointer to AAPAudioFocusRequestMsg.
       **************************************************************************/
      t_Void vHandleSessionMsg(AAPAudioFocusRequestMsg* poAudioFocusRequestMsg)const;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPSessionStatusMsg* poSessionStatusMsg)
       ***************************************************************************/
      /*!
       * \fn      vHandleSessionMsg(AAPSessionStatusMsg* poSessionStatusMsg)
      * \param   poLaunchSessionMsg : pointer to AAPSessionStatusMsg.
       **************************************************************************/
      t_Void vHandleSessionMsg(AAPSessionStatusMsg* poSessionStatusMsg)const;

};

#endif /* SPI_TCLAAPSessionDISPATCHER_H_ */
