/*!
 *******************************************************************************
 * \file              spi_tclAAPSession.cpp
 * \brief             Device session core class for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device session core class for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.02.2015 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "crc.h"
#include "StringHandler.h"
#include "Timer.h"
#include "SPITypes.h"
#include <aauto/DevelopersAuthenticator.h>
#include <aauto/SdcAuthenticator.h>
#include "AoapTransport.h"
#include "spi_tclAAPSession.h"
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPSessionDispatcher.h"
#include "spi_tclAAPSessionCbs.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclAAPSession.cpp.trc.h"
#endif
#endif

using namespace adit::aauto;
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
static const t_U32 scou32TimeforSessionMsginms = 10000;
static const t_Char sczKeyId            [] = "10200";
static const t_Char sczRootCertificate  [] = "/opt/bosch/spi/AAuto/certificate/rootcert.wrap";
static const t_Char sczClientCertificate[] = "/opt/bosch/spi/AAuto/certificate/clientcert.wrap";
static const t_Char sczPrivateKey       [] = "/opt/bosch/spi/AAuto/certificate/privatekey.wrap";

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSession::spi_tclAAPSession();
 ***************************************************************************/
spi_tclAAPSession::spi_tclAAPSession() : m_poTransport(NULL), m_oSessionTimerId(0), m_bShutdownInProgress(false)
{
   //! TODO remove this trace in later stage of devp
   ETG_TRACE_USR1((" spi_tclAAPSession::spi_tclAAPSession() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclAAPSession::~spi_tclAAPSession()
 ***************************************************************************/
spi_tclAAPSession::~spi_tclAAPSession()
{
   //! TODO remove this trace in later stage of devp
   ETG_TRACE_USR1((" spi_tclAAPSession::~spi_tclAAPSession() entered \n"));
   //! Destroy Transport
   m_poTransport = NULL;
   m_oSessionTimerId = 0;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::bInitializeSession()
 ***************************************************************************/
t_Bool spi_tclAAPSession::bInitializeSession()
{
	/*lint -esym(40,nullptr) nullptr is not declared */
	/*lint -esym(55,nullptr) Badtype */
   t_Bool bRetval =  false;

   m_bShutdownInProgress = false;
   //! Create GalReceiver object
   m_spoGalReceiver = new GalReceiver();
   SPI_NORMAL_ASSERT(m_spoGalReceiver == nullptr);

   //! Create object to handle galreceiver calbbacks
   m_spoSessionCbs = new spi_tclAAPSessionCbs();
   SPI_NORMAL_ASSERT(m_spoSessionCbs == nullptr);
   /*lint -esym(40,nullptr) nullptr is not declared */
   
   //! Register callbacks and initialize receiver library
   if((m_spoGalReceiver != nullptr) && (m_spoSessionCbs != nullptr))
   {
      bRetval = m_spoGalReceiver->init(m_spoSessionCbs);
   }

   //! Start the timer on initializing session to wait for the
   //!first message from Mobile Device. if the timer expires before receiving the
   //! first session message from phone, then the phone doesn't support AAP
   Timer* poAAPSessionTimer = Timer::getInstance();
   if(NULL != poAAPSessionTimer)
   {
      timer_t rTimerID;
      //! Start timer and wait for the session message
      poAAPSessionTimer->StartTimer(rTimerID, scou32TimeforSessionMsginms, 0, this,
               &spi_tclAAPSession::bSessionTimerCb, NULL);
      m_oSessionTimerId = rTimerID;
   };

   ETG_TRACE_USR1(("spi_tclAAPSession::bInitializeSession(): Creating and Initializing GALReceiver result = %d \n",
         ETG_ENUM(BOOL, bRetval)));
   return bRetval;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::vUnInitializeSession()
 ***************************************************************************/
t_Void spi_tclAAPSession::vUnInitializeSession()
{ 
   /*lint -esym(40,nullptr) nullptr is not declared */
   /*lint -esym(55,nullptr) Badtype */
   ETG_TRACE_USR1((" spi_tclAAPSession::vUnInitializeSession(): Shutting down and destroying GALReceiver \n"));
   if (m_spoGalReceiver != nullptr)
   {
      m_spoGalReceiver->shutdown();
   }
   /*lint -esym(40,nullptr) nullptr is not declared */
   
   //! Assign the pointer to null to release the memory as this is a shared pointer
   m_spoGalReceiver = nullptr;

   m_bShutdownInProgress = false;
   /*lint -esym(40,nullptr) nullptr is not declared */
   
   m_spoSessionCbs = nullptr;

   //! Send session status as inactive
   AAPSessionStatusMsg oSessionStatus;
   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   oSessionStatus.m_enSessionstatus = e8_SESSION_INACTIVE;
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oSessionStatus, sizeof(oSessionStatus));
   }//if (NULL != poMsgQinterface)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::bStartTransport
 ***************************************************************************/
t_Bool spi_tclAAPSession::bStartTransport(trAAPSessionInfo &rfrSessionInfo)
{
   t_Bool bRetval =  false;
   /*lint -esym(40,nullptr) nullptr is not declared */
   
   //! Start transport once all services are registered
   if (m_spoGalReceiver != nullptr)
   {
      m_spoGalReceiver->start();

      //! create Transport with AOAP specific implementation
      //! to establish data transfer between MD and HU.
      aoapTransportInfo_t rAoapTransportInfo;
      rAoapTransportInfo.aoapAccessoryId = rfrSessionInfo.u32HeadUnitID;
      rAoapTransportInfo.aoapDeviceId = rfrSessionInfo.u32DeviceID;
      m_poTransport = new AoapTransport(m_spoGalReceiver, &rAoapTransportInfo);

      if (NULL != m_poTransport)
      {
         //! Start the GalReceiver transport
         bRetval = m_poTransport->start();
      }
   }
   ETG_TRACE_USR1(("spi_tclAAPSession::bStartTransport(): Starting AOAP transport result = %d \n",
         ETG_ENUM(BOOL, bRetval)));
   return bRetval;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::vStopTransport()
 ***************************************************************************/
t_Void spi_tclAAPSession::vStopTransport()
{
   ETG_TRACE_USR1(("  spi_tclAAPSession::vStopTransport(): Stopping AOAP transport \n"));
   if(NULL != m_poTransport)
   {
      m_bShutdownInProgress = true;
      //! First Stop galreceiver transport and wait till its completely stopped
      //! request for stop and then Join the reader and writer threads  
      //! request for stop will internally invoke prepareshutdwon of galreceiver
      ETG_TRACE_USR1(("  spi_tclAAPSession::requestStop()  \n"));
      m_poTransport->requestStop();

      ETG_TRACE_USR1(("  spi_tclAAPSession::waitForExit()  \n"));
      m_poTransport->waitForExit();
   }

   ETG_TRACE_USR1(("  Releasing memory allocated for aoap transport\n"));
   //! Destroy Transport
   RELEASE_MEM(m_poTransport);
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::bSetCertificates()
 ***************************************************************************/
t_Bool spi_tclAAPSession::bSetCertificates(t_String szCertiPath, tenCertificateType enCertificatetype)
{ 
	/*lint -esym(40,nullptr) nullptr is not declared */
   SPI_INTENTIONALLY_UNUSED(szCertiPath);

   t_Bool bRetVal = false;

   //! Use the authenticator based on the project configuration
   if (m_spoGalReceiver != nullptr)
   {
      switch(enCertificatetype)
      {
         case e8_CERTIFICATETYPE_DEVELOPER:
         {
            ETG_TRACE_ERR((" spi_tclAAPSession::bSetCertificates Using developer certificates \n"));
            //! Use the developer authenticator for using the
            //! developer certificates embedded in code
            DevelopersAuthenticator oDevpAuthenticator;
            bRetVal = oDevpAuthenticator.setCertificates(m_spoGalReceiver);
            break;
         }

         case e8_CERTIFICATETYPE_SDC:
         {
            ETG_TRACE_ERR((" spi_tclAAPSession::bSetCertificates using certificates in SDC \n"));
            //! Use the SDC authenticator for more secore certificate storage
            SdcAuthenticator oSdcAuthenticator;
            oSdcAuthenticator.setConfigItem("keyId"            , sczKeyId            );
            oSdcAuthenticator.setConfigItem("rootCertificate"  , sczRootCertificate  );
            oSdcAuthenticator.setConfigItem("clientCertificate", sczClientCertificate);
            oSdcAuthenticator.setConfigItem("privateKey"       , sczPrivateKey       );
            bRetVal = oSdcAuthenticator.setCertificates(m_spoGalReceiver);
            break;
         }
         default:
         {
            ETG_TRACE_ERR((" spi_tclAAPSession::bSetCertificates Certificate type not supported \n"));
         }

      }
   }
   ETG_TRACE_USR1(("spi_tclAAPSession::bSetCertificates Certificatetype = %d Return Value = %d\n",
         ETG_ENUM(CERTIFICATE_TYPE,enCertificatetype), ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::vSetHeadUnitInfo()
 ***************************************************************************/
t_Void spi_tclAAPSession::vSetHeadUnitInfo(trAAPHeadUnitInfo & rfrHeadUnitInfo)
{
   ETG_TRACE_USR1((" spi_tclAAPSession::vSetHeadUnitInfo entered \n"));
  // if(NULL != m_spoGalReceiver)
   if (m_spoGalReceiver != nullptr)
   {

      ETG_TRACE_USR4(("Setting vehicle identity info : Vehicle Manufacturer = %s \n", rfrHeadUnitInfo.szVehicleManufacturer.c_str()));
      ETG_TRACE_USR4((" Vehicle Model = %s \n", rfrHeadUnitInfo.szVehicleModel.c_str()));
      ETG_TRACE_USR4((" Vehicle Model Year = %s \n", rfrHeadUnitInfo.szYear.c_str()));
      ETG_TRACE_USR4((" Vehicle Serial Number = %s \n", rfrHeadUnitInfo.szSerial.c_str()));
      //! Set the vehicle and Headunit information
      m_spoGalReceiver->setIdentityInfo(rfrHeadUnitInfo.szVehicleManufacturer.c_str(), rfrHeadUnitInfo.szVehicleModel.c_str(),
               rfrHeadUnitInfo.szYear.c_str(), rfrHeadUnitInfo.szSerial.c_str());
      //! Sets the driver position - left/right/center.
      //! This influences the layout of the screen (currently no change seen)
      m_spoGalReceiver->setDriverPosition(static_cast<DriverPosition>(rfrHeadUnitInfo.enDriverPos));

      ETG_TRACE_USR4(("Setting Headunit info : Manufacturer = %s \n", rfrHeadUnitInfo.szManufacturer.c_str()));
      ETG_TRACE_USR4((" Head Unit Model = %s \n", rfrHeadUnitInfo.szModelName.c_str()));
      ETG_TRACE_USR4((" Head unit software version = %s \n", rfrHeadUnitInfo.szSoftwareVersion.c_str()));

      //! software build is not known hence left as empty string
      m_spoGalReceiver->setHeadUnitInfo(rfrHeadUnitInfo.szManufacturer.c_str()/*Make*/,rfrHeadUnitInfo.szModelName.c_str(),
            ""/*sw build*/, rfrHeadUnitInfo.szSoftwareVersion.c_str());

      //! TODO: REMOVE: Call not needed since our system maintains time
      //m_spoGalReceiver->setCertificateVerificationTime(1417449318);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPSession::vSendByeByeMessage
 ***************************************************************************/
t_Void spi_tclAAPSession::vSendByeByeMessage()
{
   ETG_TRACE_USR1(("  spi_tclAAPCmdDiscoverer::vSendByeByeMessage(): Sending ByeBye message to phone \n"));
   //! TODO extend interface for future ByeBye reasons
   //! Send Byebye message to phone
   if (m_spoGalReceiver != nullptr)
   {
      m_spoGalReceiver->sendByeByeRequest(USER_SELECTION);
   }
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPSession::vSendByeByeResponse
 ***************************************************************************/
t_Void spi_tclAAPSession::vSendByeByeResponse()
{
   /*lint -esym(40,nullptr) nullptr is not declared */
   //!
   ETG_TRACE_USR1(("  spi_tclAAPCmdDiscoverer::vSendByeByeResponse(): Sending response to the byebye message received from phone \n"));
   if (m_spoGalReceiver != nullptr)
   {
      m_spoGalReceiver->sendByeByeResponse();
   }
}

/***************************************************************************
 ** FUNCTION:  shared_ptr<GalReceiver> spi_tclAAPSession::poGetGalReceiver();
 ***************************************************************************/
shared_ptr<GalReceiver> spi_tclAAPSession::poGetGalReceiver()
{
   /*lint -esym(40,nullptr) nullptr is not declared */

   t_Bool bValid = (m_spoGalReceiver != nullptr);
   ETG_TRACE_USR1((" spi_tclAAPSession::poGetGalReceiver m_spoGalReceiver valid = %d \n", ETG_ENUM(BOOL, bValid)));
   return m_spoGalReceiver;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::vSetNavigationFocus()
 ***************************************************************************/
t_Void spi_tclAAPSession::vSetNavigationFocus(tenAAPNavFocusType enNavFocusType)
{
   /*lint -esym(40,nullptr) nullptr is not declared */
	
   ETG_TRACE_USR1((" spi_tclAAPSession::vSetNavigationFocus() entered: enNavFocusType %d ",
         ETG_ENUM(NAV_FOCUS_TYPE, enNavFocusType)));

   if ((m_spoGalReceiver != nullptr)&& (false == m_bShutdownInProgress))
   {
      m_spoGalReceiver->setNavigationFocus(enNavFocusType);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::vSetAudioFocus()
 ***************************************************************************/
t_Void spi_tclAAPSession::vSetAudioFocus(tenAAPDeviceAudioFocusState enDevAudFocusState, bool bUnsolicited)
{
   ETG_TRACE_USR1((" spi_tclAAPSession::vSetAudioFocus() entered: enDevAudFocusState %d, bUnsolicited %d \n",
         ETG_ENUM(DEVICE_AUDIOFOCUS, enDevAudFocusState), ETG_ENUM(BOOL, bUnsolicited)));
   /*lint -esym(40,nullptr) nullptr is not declared */
   
   if ((m_spoGalReceiver != nullptr) && (false == m_bShutdownInProgress))
   {
      m_spoGalReceiver->setAudioFocus(
            static_cast<AudioFocusStateType>(enDevAudFocusState),
            bUnsolicited);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSession::vStopSessionTimer()
 ***************************************************************************/
t_Void spi_tclAAPSession::vStopSessionTimer()
{
   ETG_TRACE_USR1(("spi_tclAAPSession::vStopSessionTimer  \n"));
   Timer* poAAPSessionTimer = Timer::getInstance();
   if((NULL != poAAPSessionTimer) && (0 != m_oSessionTimerId))
   {
      //! Stop the timer if session message is received
      poAAPSessionTimer->CancelTimer(m_oSessionTimerId);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSession::bSessionTimerCb
 ***************************************************************************/
t_Bool spi_tclAAPSession::bSessionTimerCb(timer_t rTimerID, t_Void *pvObject,
         const t_Void *pvUserData)
{
   ETG_TRACE_USR1(("spi_tclAAPSession::bSessionTimerCb : Android Auto Session timer expired:"
         " No Session message received from phone\n"));
   SPI_INTENTIONALLY_UNUSED(rTimerID);
   SPI_INTENTIONALLY_UNUSED(pvObject);
   SPI_INTENTIONALLY_UNUSED(pvUserData);

   //! Send session status as inactive if no message is received from phone(timer expiry)
   AAPSessionStatusMsg oSessionStatus;
   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   oSessionStatus.m_enSessionstatus = e8_SESSION_ERROR;
   oSessionStatus.m_bSessionTimedOut = true;
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oSessionStatus, sizeof(oSessionStatus));
   }//if (NULL != poMsgQinterface)

   return true;
}
//lint -restore
