/*******************************************************************************
 * \file              spi_tclAAPSessionCbsCbs.h
 * \brief             Class implementing callbacks from GALreceiver
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Class implementing callbacks from GALreceiver
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 17.03.2015 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPSessionDispatcher.h"
#include "spi_tclAAPSessionCbs.h"
#include "spi_tclAAPSession.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclAAPSessionCbs.cpp.trc.h"
#endif
#endif


//! Galreceiver callbacks
/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::serviceDiscoveryRequestCallback()
 ***************************************************************************/
void spi_tclAAPSessionCbs::serviceDiscoveryRequestCallback(const string & smallIcon, const string & mediumIcon,
						const string & largeIcon, const string & label, const string& deviceName)
{
   ETG_TRACE_USR1((" serviceDiscoveryRequestCallback smallIcon = %d mediumIcon = %d largeIcon = %d\n",
            smallIcon.size(), mediumIcon.size(), largeIcon.size()));
   ETG_TRACE_USR1((" serviceDiscoveryRequestCallback label = %s \n", label.c_str()));
   ETG_TRACE_USR1((" serviceDiscoveryRequestCallback Device Name = %s \n", deviceName.c_str()));

   //! TODO check with Google if string is appropriate type for ICon since
   //! Icon with null entry inside the JPEG image will cause issues
   //! Callbacks for the registered objects
   AAPServiceDiscoveryMsg oServiceDiscMsg;
   if((NULL != oServiceDiscMsg.m_pszSmallIcon) && (NULL != oServiceDiscMsg.m_pszMediumIcon) &&
            (NULL != oServiceDiscMsg.m_pszLargeIcon) && (NULL != oServiceDiscMsg.m_pszLabel) && (NULL != oServiceDiscMsg.m_pszDeviceName))
   {
      *(oServiceDiscMsg.m_pszSmallIcon) = smallIcon.c_str();
      *(oServiceDiscMsg.m_pszMediumIcon) = mediumIcon.c_str();
      *(oServiceDiscMsg.m_pszLargeIcon) = largeIcon.c_str();
      *(oServiceDiscMsg.m_pszLabel) = label.c_str();
      *(oServiceDiscMsg.m_pszDeviceName) = deviceName.c_str();
   }

   spi_tclAAPMsgQInterface *poMsgQinterface =
            spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oServiceDiscMsg, sizeof(oServiceDiscMsg));
   }//if (NULL != poMsgQinterface)

   //! Send session status as active if this message is received from phone
   //! This message is considered to be initialization message (suggested from google)
   AAPSessionStatusMsg oSessionStatus;
   oSessionStatus.m_enSessionstatus = e8_SESSION_ACTIVE;
   if (NULL != poMsgQinterface)
     {
        poMsgQinterface->bWriteMsgToQ(&oSessionStatus, sizeof(oSessionStatus));
     }//if (NULL != poMsgQinterface)

   //! Stop the session timer after receiving the session init message
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vStopSessionTimer();
   }  // if(NULL != poAAPSession)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::unrecoverableErrorCallback(MessageStatus err)
 ***************************************************************************/
void spi_tclAAPSessionCbs::unrecoverableErrorCallback(MessageStatus err)
{
   ETG_TRACE_ERR((" spi_tclAAPSessionCbs::unrecoverableErrorCallback received from phone with error - %d\n", ETG_ENUM(AAP_STATUS_RETURNED, err)));
   //! TODO Create messages and send to response class

   //! Send session status as error if this message is received from phone
   AAPSessionStatusMsg oSessionStatus;
   spi_tclAAPMsgQInterface *poMsgQinterface =
            spi_tclAAPMsgQInterface::getInstance();
   oSessionStatus.m_enSessionstatus = e8_SESSION_ERROR;
   oSessionStatus.m_bSessionTimedOut = false;
   if ((err != STATUS_FRAMING_ERROR) && (NULL != poMsgQinterface))
   {
      poMsgQinterface->bWriteMsgToQ(&oSessionStatus, sizeof(oSessionStatus));
   }//if (NULL != poMsgQinterface)

   //! Stop the session timer after receiving the session error message
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vStopSessionTimer();
   }  // if(NULL != poAAPSession)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::pingRequestCallback()
 ***************************************************************************/
void spi_tclAAPSessionCbs::pingRequestCallback(int64_t timestamp, bool bugReport)
{
   ETG_TRACE_USR1(("spi_tclAAPSessionCbs::pingRequestCallback timestamp = %d,  bugReport = %d\n", timestamp, ETG_ENUM(BOOL, bugReport) ));
   //! TODO Create messages and send to response class
   //! Currently not used
}


/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::pingResponseCallback()
 ***************************************************************************/
void spi_tclAAPSessionCbs::pingResponseCallback(int64_t timestamp)
{
   ETG_TRACE_USR1(("spi_tclAAPSessionCbs::pingResponseCallback timestamp = %d \n", timestamp));
   //! TODO Create messages and send to response class
   //! Currently not used
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::navigationFocusCallback()
 ***************************************************************************/
void spi_tclAAPSessionCbs::navigationFocusCallback(NavFocusType focusType)
{
   ETG_TRACE_USR1(("spi_tclAAPSessionCbs::navigationFocusCallback focusType = %d\n", ETG_ENUM(NAV_FOCUS_TYPE, focusType) ));

   //! Callbacks for the registered objects
   AAPNavigationFocusMsg oNavFocusMsg;
   oNavFocusMsg.m_enNaviFocusType = static_cast<tenAAPNavFocusType>(focusType);

   spi_tclAAPMsgQInterface *poMsgQinterface =
            spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oNavFocusMsg, sizeof(oNavFocusMsg));
   }//if (NULL != poMsgQinterface)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::byeByeRequestCallback()
 ***************************************************************************/
void spi_tclAAPSessionCbs::byeByeRequestCallback(ByeByeReason reason)
{
   ETG_TRACE_USR1(("spi_tclAAPSessionCbs::byeByeRequestCallback: Received byebye request from phone. ByeByeReason = %d\n", ETG_ENUM(AAP_BYEBYEREASON, reason) ));
   AAPByeByeRequestMsg oByeByeRequest;
   oByeByeRequest.m_enByeByeReason =  static_cast<tenAAPByeByeReason>(reason);

   spi_tclAAPMsgQInterface *poMsgQinterface =
            spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oByeByeRequest, sizeof(oByeByeRequest));
   }//if (NULL != poMsgQinterface)
}



/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::byeByeResponseCallback()
 ***************************************************************************/
void spi_tclAAPSessionCbs::byeByeResponseCallback()
{
   ETG_TRACE_USR1(("spi_tclAAPSessionCbs::byeByeResponseCallback: Received response for byebye request sent by HeadUnit \n"));
   AAPByeByeResponseMsg oByeByeResp;
   spi_tclAAPMsgQInterface *poMsgQinterface =
            spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oByeByeResp, sizeof(oByeByeResp));
   }//if (NULL != poMsgQinterface)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::voiceSessionNotificationCallback()
 ***************************************************************************/
void spi_tclAAPSessionCbs::voiceSessionNotificationCallback(VoiceSessionStatus status)
{
   ETG_TRACE_USR1(("voiceSessionNotificationCallback VoiceSessionStatus = %d\n", ETG_ENUM(AAP_VOICESESSION_STATUS, status) ));
   AAPVoiceSessionNotifMsg oVoiceNoti;
   oVoiceNoti.m_enVoiceSessionStatus = static_cast<tenAAPVoiceSessionStatus>(status);

   spi_tclAAPMsgQInterface *poMsgQinterface =
            spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oVoiceNoti, sizeof(oVoiceNoti));
   }//if (NULL != poMsgQinterface)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPSessionCbs::audioFocusRequestCallback()
 ***************************************************************************/
void spi_tclAAPSessionCbs::audioFocusRequestCallback(AudioFocusRequestType request)
{
   ETG_TRACE_USR1(("audioFocusRequestCallback AudioFocusRequestType = %d\n", ETG_ENUM(DEVICE_AUDIOFOCUS_REQ, request) ));
   AAPAudioFocusRequestMsg oAudioFocusMsg;
   oAudioFocusMsg.m_enDevAudFocusRequest = static_cast<tenAAPDeviceAudioFocusRequest>(request);

   spi_tclAAPMsgQInterface *poMsgQinterface =
            spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oAudioFocusMsg, sizeof(oAudioFocusMsg));
   }//if (NULL != poMsgQinterface)
}


