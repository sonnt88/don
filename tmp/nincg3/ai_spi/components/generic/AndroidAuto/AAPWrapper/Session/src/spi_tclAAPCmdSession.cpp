/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdSession.cpp
 * \brief             Device session wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device session wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.02.2015 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclAAPCmdSession.h"
#include "spi_tclAAPSession.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclAAPCmdSession.cpp.trc.h"
#endif
#endif


/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAAPCmdSession::spi_tclAAPCmdSession();
 ***************************************************************************/
spi_tclAAPCmdSession::spi_tclAAPCmdSession()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdSession::spi_tclAAPCmdSession() \n"));
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclAAPCmdSession::~spi_tclAAPCmdSession()
 ***************************************************************************/
spi_tclAAPCmdSession::~spi_tclAAPCmdSession()
{
   ETG_TRACE_USR1((" spi_tclAAPCmdSession::~spi_tclAAPCmdSession() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdSession::bInitializeSession()
 ***************************************************************************/
t_Bool spi_tclAAPCmdSession::bInitializeSession()
{
   t_Bool bRetval =  false;
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      bRetval = poAAPSession->bInitializeSession();
   }  // if(NULL != poAAPSession)
   return bRetval;
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vUnInitializeSession( )
 ***************************************************************************/
t_Void spi_tclAAPCmdSession::vUnInitializeSession()
{
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vUnInitializeSession();
   }
}


/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdSession:: bStartTransport( )
 ***************************************************************************/
t_Bool spi_tclAAPCmdSession::bStartTransport(trAAPSessionInfo &rfrSessionInfo)
{
   t_Bool bRetval =  false;
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;

   if(NULL != poAAPSession)
   {
      bRetval = poAAPSession->bStartTransport(rfrSessionInfo);
   }  // if(NULL != poAAPSession)
   return bRetval;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vStopTransport( )
 ***************************************************************************/
t_Void spi_tclAAPCmdSession::vStopTransport()
{
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vStopTransport();
   }
}


/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdSession:: bSetCertificates( )
 ***************************************************************************/
t_Bool spi_tclAAPCmdSession::bSetCertificates(t_String szCertiPath, tenCertificateType enCertificatetype)
{
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   t_Bool bRetVal = false;
   if(NULL != poAAPSession)
   {
      bRetVal = poAAPSession->bSetCertificates(szCertiPath, enCertificatetype);
   }
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vSetHeadUnitInfo( )
 ***************************************************************************/
t_Void spi_tclAAPCmdSession::vSetHeadUnitInfo(trAAPHeadUnitInfo & rfrHeadUnitInfo)
{
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vSetHeadUnitInfo(rfrHeadUnitInfo);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdSession::vSetNavigationFocus()
 ***************************************************************************/
t_Void spi_tclAAPCmdSession::vSetNavigationFocus(tenAAPNavFocusType enNavFocusType)
{
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vSetNavigationFocus(enNavFocusType);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdSession::vSetAudioFocus()
 ***************************************************************************/
t_Void spi_tclAAPCmdSession::vSetAudioFocus(tenAAPDeviceAudioFocusState enDevAudFocusState, bool bUnsolicited)
{
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vSetAudioFocus(enDevAudFocusState, bUnsolicited);
   }
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vSendByeByeMessage( )
 ***************************************************************************/
t_Void spi_tclAAPCmdSession::vSendByeByeMessage()
{
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vSendByeByeMessage();
   }
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdSession:: vSendByeByeResponse( )
 ***************************************************************************/
t_Void spi_tclAAPCmdSession::vSendByeByeResponse()
{
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      poAAPSession->vSendByeByeResponse();
   }
}


