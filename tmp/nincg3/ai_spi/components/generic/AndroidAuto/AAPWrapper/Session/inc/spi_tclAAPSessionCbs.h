/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdSession.cpp
 * \brief             Device session wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device session wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.02.2015 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/


#ifndef AAUTOT_AAUTO_DEMO_ICONTROLLER_CALLBACKS_H
#define AAUTOT_AAUTO_DEMO_ICONTROLLER_CALLBACKS_H


/* *************  includes  ************* */


#include "BaseTypes.h"
/* GalReceiver headers */
#include <aauto/GalReceiver.h>
#include <aauto/IControllerCallbacks.h>

/* This class includes a general set of IControllerCallbacks that must be set up for the GAL receiver */
class spi_tclAAPSessionCbs : public IControllerCallbacks
{
public:
    spi_tclAAPSessionCbs() { }

    //! TODO comments
   //! Callbacks from galreceiver
    /***************************************************************************
     ** FUNCTION:  t_Bool spi_tclAAPSession::serviceDiscoveryRequestCallback()
     ***************************************************************************/
    /*!
     * \fn      serviceDiscoveryRequestCallback
   * \brief   Called when service discovery request is received.
   *          This call sends across an icon set and a label that can be
   *          used by the native UI to display a button that allows
   *          users to switch back to projected mode.
   * \param     smallIcon   32x32 png image.
   * \param     mediumIcon  64x64 png image.
   * \param     largeIcon   128x128 png image.
   * \param     label  A label that may be displayed alongside the icon.
   * \param		deviceName A friendly device name
   ***************************************************************************/
   void serviceDiscoveryRequestCallback(const string& smallIcon, const string& mediumIcon,
           const string& largeIcon, const string& label, const string& deviceName);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::unrecoverableErrorCallback()
    ***************************************************************************/
   /*!
    * \fn      unrecoverableErrorCallback
    * \brief   called when phone detects a unrecoverable error:
    * 		   advice is to reset USB on this call.A re-establishment of the
    * 		   GAL connection may be attempted after that.
    * \param   err The current set of error codes is:
    *  				- STATUS_AUTHENTICATION_FAILURE if the SSL handshake fails.
    *  				- STATUS_FRAMING_ERROR if an error occurs during communication with the
    *    			MD. Examples of errors include (but are not limited to) IO errors and
    *    			SSL decryption errors.
    **************************************************************************/
   void unrecoverableErrorCallback(MessageStatus err);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::pingRequestCallback()
    ***************************************************************************/
   /*!
    * \fn      pingRequestCallback
    * \brief   Called when the other end pings us.
    * \param   timestamp   The (remote) timestamp of the request.
    * \param   bugReport   Should a bug report be saved away? The implementer can choose
    *         to ignore this if it is sent with high frequency. It is in the end, the
    *         implementers responsibility to ensure that the system cannot be DoS'd
    *         if too many requests for bug reports are received.
    **************************************************************************/
   void pingRequestCallback(int64_t timestamp, bool bugReport);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::pingResponseCallback()
    ***************************************************************************/
   /*!
    * \fn      pingResponseCallback
    * \brief   Called when the other end responds to our ping.
    * \param   timestamp   The (remote) timestamp of the request.
    **************************************************************************/
   void pingResponseCallback(int64_t timestamp);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::navigationFocusCallback()
    ***************************************************************************/
   /*!
    * \fn      navigationFocusCallback
    * \brief   Called when a navigation focus request is received from the phone.
    *  You must respond to this by calling Controller::setNavigationFocus()
    *  (even if there is no change in navigation focus). If navigation focus
    *  is given to the mobile device, all native turn by turn guidance
    *  systems must be stopped.
    * \param   type  he type requested (can be NAV_FOCUS_NATIVE or NAV_FOCUS_PROJECTED).
    **************************************************************************/
   void navigationFocusCallback(NavFocusType focusType);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::byeByeRequestCallback()
    ***************************************************************************/
   /*!
    * \fn      byeByeRequestCallback
    * \brief   Called when ByeByeRequest is received from phone. After taking
    *          necessary steps, car side should send ByeByeResponse.
    * \param   reason  The reason for the disconnection request.
    **************************************************************************/
   void byeByeRequestCallback(ByeByeReason reason);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::byeByeResponseCallback()
    ***************************************************************************/
   /*!
    * \fn      byeByeResponseCallback
    * \brief  Called when ByeByeResponse is received from phone. Normally
    *  this is a reply for ByeByeRequest message sent from car.
    **************************************************************************/
   void byeByeResponseCallback();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::voiceSessionNotificationCallback()
    ***************************************************************************/
   /*!
    * \fn      voiceSessionNotificationCallback
    * \brief  Called when a voice session notification is received. Note that
    *   this callback only applies to you if you do not always send a PTT short
    *   press to us always. If you always send PTT short press to us,
    *    you should be able to ignore this call altogether.
    * \param status The status of the voice recongition session.
    **************************************************************************/
   void voiceSessionNotificationCallback(VoiceSessionStatus status);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPSession::audioFocusRequestCallback()
    ***************************************************************************/
   /*!
    * \fn    audioFocusRequestCallback
    * \brief Called when the source wishes to acquire audio focus.
    * \param request Can be one of AUDIO_FOCUS_GAIN, AUDIO_FOCUS_GAIN_TRANSIENT,
    *        AUDIO_FOCUS_GAIN_TRANSIENT_MAY_DUCK, AUDIO_FOCUS_RELEASE.
    **************************************************************************/
   void audioFocusRequestCallback(AudioFocusRequestType request);
private:
};


#endif /* AAUTOT_AAUTO_DEMO_ICONTROLLER_CALLBACKS_H */
