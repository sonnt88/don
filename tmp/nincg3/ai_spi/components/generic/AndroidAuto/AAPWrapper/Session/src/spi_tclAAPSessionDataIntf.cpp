/***********************************************************************/
/*!
* \file  spi_tclAAPSessionDataIntf.cpp
* \brief AAP Session Data Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Session Data Interface
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
12.03.2015  | Pruthvi Thej Nagaraju | Initial Version
28.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
\endverbatim
**************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclAAPSession.h"
#include "spi_tclAAPSessionDataIntf.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPSessionDataIntf.cpp.trc.h"
   #endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclAAPSessionDataIntf::spi_tclAAPSessionDataIntf()
***************************************************************************/
spi_tclAAPSessionDataIntf::spi_tclAAPSessionDataIntf()
{
   ETG_TRACE_USR1(("spi_tclAAPSessionDataIntf() entered \n"));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPSessionDataIntf::~spi_tclAAPSessionDataIntf()
***************************************************************************/
spi_tclAAPSessionDataIntf::~spi_tclAAPSessionDataIntf()
{
   ETG_TRACE_USR1(("~spi_tclAAPSessionDataIntf() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  shared_ptr<GalReceiver>  spi_tclAAPSessionDataIntf::poGetGalReceiver();
 ***************************************************************************/
shared_ptr<GalReceiver> spi_tclAAPSessionDataIntf::poGetGalReceiver()
{ 
	/*lint -esym(40,nullptr) nullptr is not declared */
   shared_ptr<GalReceiver> spoGalReceiver;
   spi_tclAAPSession* poAAPSession = spi_tclAAPSession::getInstance() ;
   if(NULL != poAAPSession)
   {
      spoGalReceiver = poAAPSession->poGetGalReceiver();
   }
   t_Bool bIsNull = (spoGalReceiver == nullptr);
   ETG_TRACE_USR1(("spi_tclAAPSessionDataIntf()  poGetGalReceiver: bIsNull = %d \n", ETG_ENUM(BOOL, bIsNull)));
   return spoGalReceiver;
}
//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
