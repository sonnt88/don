/*!
 *******************************************************************************
 * \file              spi_tclAAPSession.h
 * \brief             Device session core class for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Device session core class for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 27.02.2015 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAAPSESSION_H_
#define SPI_TCLAAPSESSION_H_

/******************************************************************************
 | includes:
 | 1)AAP - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include <aauto/util/shared_ptr.h>
#include "BaseTypes.h"
#include "AAPTypes.h"
#include "GenericSingleton.h"
#include <aauto/GalReceiver.h>
#include <aauto/IControllerCallbacks.h>

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

class Transport;

/*!
 * \class spi_tclAAPSession
 * \brief
 */

class spi_tclAAPSession: public GenericSingleton<spi_tclAAPSession>
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclAAPSession::~spi_tclAAPSession()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclAAPSession()
       * \brief   Destructor
       * \sa      spi_tclAAPSession()
       **************************************************************************/
      ~spi_tclAAPSession();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::bInitializeSession()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bInitializeSession()
       * \brief   creates and initializes GAL receiver
       * \retval  true : GAL receiver was initialized successfully.
       * \retval  false : GAL receiver initialization failed
       * \sa      vUnInitializeSession()
       **************************************************************************/
      t_Bool bInitializeSession();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::vUnInitializeSession()
       ***************************************************************************/
      /*!
       * \fn      vUnInitializeSession
       * \brief   uninitializes and releases GalReceiver
       * \sa      bInitializeSession()
       **************************************************************************/
      t_Void vUnInitializeSession();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::bStartTransport
       ***************************************************************************/
      /*!
       * \fn      t_Bool bStartTransport
       * \brief   Starts the transport layer communication between HU and MD
       * \retval  true if starting the transport was successful false otherwise
       * \sa      vStopTransport()
       **************************************************************************/
      t_Bool bStartTransport(trAAPSessionInfo &rfrSessionInfo);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::vStopTransport()
       ***************************************************************************/
      /*!
       * \fn      vStopTransport
       * \brief   Requests stopping of transport channel and waits for it to stop
       * \sa      bStartTransport()
       **************************************************************************/
      t_Void vStopTransport();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::bSetCertificates()
       ***************************************************************************/
      /*!
       * \fn      bSetCertificates
       * \brief   Set secure path where the certificates and keys are stored
       * \param   szCertiPath : Path where certificates are stored
       * \param  enCertificateType : CertificateType to be used for authentication
       **************************************************************************/
      t_Bool bSetCertificates(t_String szCertiPath, tenCertificateType enCertificatetype);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::vSetHeadUnitInfo()
       ***************************************************************************/
      /*!
       * \fn      vSetHeadUnitInfo
       * \brief   Set head unit information needed for attestation
       * \param   rfrHeadUnitInfo : Information related to the head unit
       **************************************************************************/
      t_Void vSetHeadUnitInfo(trAAPHeadUnitInfo &rfrHeadUnitInfo);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPSession:: vSendByeByeMessage( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vSendByeByeMessage()
       * \brief   Send Bye bye message to phone when user intentionally deselects aap
       **************************************************************************/
      t_Void vSendByeByeMessage();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPSession:: vSendByeByeResponse( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vSendByeByeResponse()
       * \brief   Send Bye bye response to phone in response to the byebye request received
       **************************************************************************/
      t_Void vSendByeByeResponse();

      /***************************************************************************
      ** FUNCTION:  shared_ptr<GalReceiver>  spi_tclAAPSession::poGetGalReceiver();
      ***************************************************************************/
      /*!
      * \fn      shared_ptr<GalReceiver>  poGetGalReceiver();
      * \brief   Provides GALreceiver pointer to other components
      * \retval  shared_ptr<GalReceiver>  : Shared Pointer to GalReceiver. To be used by other AAP
      *          classes for creating and registering endpoints.
      **************************************************************************/
      shared_ptr<GalReceiver> poGetGalReceiver();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::vSetNavigationFocus()
       ***************************************************************************/
      /*!
       * \fn      vSetNavigationFocus(tenAAPNavFocusType enNavFocusType)
       * \brief   Set Navigation Focus type
       * \param   enNavFocusType : Navigation focus type
       **************************************************************************/
      t_Void vSetNavigationFocus(tenAAPNavFocusType enNavFocusType);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::vSetAudioFocus()
       ***************************************************************************/
      /*!
       * \fn      vSetAudioFocus(AudioFocusStateType focusState, bool unsolicited)
       * \brief   Set Audio Focus type
       * \param
       **************************************************************************/
      t_Void vSetAudioFocus(tenAAPDeviceAudioFocusState enDevAudFocusState, bool bUnsolicited);
 
      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::vStopSessionTimer()
       ***************************************************************************/
      /*!
       * \fn      vStopSessionTimer()
       * \brief   Stop the session timer once initialization message is received
       **************************************************************************/
      t_Void vStopSessionTimer();

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      //!brief   Generic Singleton class

      friend class GenericSingleton<spi_tclAAPSession>;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSession::spi_tclAAPSession();
       ***************************************************************************/
      /*!
       * \fn     spi_tclAAPSession()
       * \brief  Default Constructor
       * \sa      ~spi_tclAAPSession()
       **************************************************************************/
      spi_tclAAPSession();

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPSession::bSessionTimerCb
       ***************************************************************************/
      /*!
       * \fn     bSessionTimerCb
       * \brief  called on expiry of session timer
       * \param  rTimerID: ID of the timer which has expired
       * \param  pvObject: pointer to object passed while starting the timer
       * \param  pvUserData: data passed during start of the timer
       **************************************************************************/
      static t_Bool bSessionTimerCb(timer_t rTimerID, t_Void *pvObject,
               const t_Void *pvUserData);

      //! Pointer to Galreceiver
      shared_ptr<GalReceiver> m_spoGalReceiver;

      //! Pointer to class containing callbacks for GAL receiver
      shared_ptr<IControllerCallbacks> m_spoSessionCbs;

      //! Pointer to transport class
      Transport *m_poTransport;

      //! timer for waiting for session message
      timer_t m_oSessionTimerId;

      //! indicates if shutdown sequence is in progress
      t_Bool m_bShutdownInProgress;
};

#endif /* SPI_TCLAAPSESSION_H_ */
