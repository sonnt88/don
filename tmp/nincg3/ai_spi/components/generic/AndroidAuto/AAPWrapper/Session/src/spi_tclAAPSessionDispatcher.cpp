/***********************************************************************/
/*!
 * \file  spi_tclAAPSessionDispatcher.h
 * \brief Message Dispatcher for Session Messages. implemented using
 *       double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Session Messages
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.03.2015  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/

#include "RespRegister.h"
#include "spi_tclAAPSessionDispatcher.h"
#include "spi_tclAAPRespSession.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
#include "trcGenProj/Header/spi_tclAAPSessionDispatcher.cpp.trc.h"
#endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleSessionMsg(this);             \
   }                                                        \
   vDeAllocateMsg();                                        \
}


/***************************************************************************
 ** FUNCTION:  AAPSessionMsgBase::AAPSessionMsgBase
 ***************************************************************************/
AAPSessionMsgBase::AAPSessionMsgBase(): m_u32DeviceHandle(0)
{
   vSetServiceID (e32MODULEID_AAPSESSION);
}

/***************************************************************************
 ** FUNCTION:  AAPServiceDiscoveryMsg::AAPServiceDiscoveryMsg
 ***************************************************************************/
AAPServiceDiscoveryMsg::AAPServiceDiscoveryMsg() : m_pszSmallIcon(NULL), m_pszMediumIcon(NULL),
         m_pszLargeIcon(NULL), m_pszLabel(NULL),m_pszDeviceName(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPServiceDiscoveryMsg::AAPServiceDiscoveryMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPServiceDiscoveryMsg, spi_tclAAPSessionDispatcher);

/***************************************************************************
 ** FUNCTION:  AAPServiceDiscoveryMsg::vAllocateMsg
 ***************************************************************************/
t_Void AAPServiceDiscoveryMsg::vAllocateMsg()
{
   m_pszSmallIcon = new t_String;
   m_pszMediumIcon = new t_String;
   m_pszLargeIcon = new t_String;
   m_pszLabel = new t_String;
   m_pszDeviceName = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_pszSmallIcon);
   SPI_NORMAL_ASSERT(NULL == m_pszMediumIcon);
   SPI_NORMAL_ASSERT(NULL == m_pszLargeIcon);
   SPI_NORMAL_ASSERT(NULL == m_pszLabel);
   SPI_NORMAL_ASSERT(NULL == m_pszDeviceName);
}

/***************************************************************************
 ** FUNCTION:  AAPServiceDiscoveryMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AAPServiceDiscoveryMsg::vDeAllocateMsg()
{
   RELEASE_MEM(m_pszSmallIcon);
   RELEASE_MEM(m_pszMediumIcon);
   RELEASE_MEM(m_pszLargeIcon);
   RELEASE_MEM(m_pszLabel);
   RELEASE_MEM(m_pszDeviceName);
}


/***************************************************************************
 ** FUNCTION:  AAPUnrecoverableErrorMsg::AAPUnrecoverableErrorMsg
 ***************************************************************************/
AAPUnrecoverableErrorMsg::AAPUnrecoverableErrorMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPUnrecoverableErrorMsg::AAPUnrecoverableErrorMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPUnrecoverableErrorMsg, spi_tclAAPSessionDispatcher);




/***************************************************************************
 ** FUNCTION:  AAPNavigationFocusMsg::AAPNavigationFocusMsg
 ***************************************************************************/
AAPNavigationFocusMsg::AAPNavigationFocusMsg():m_enNaviFocusType(e8_NAV_FOCUS_NATIVE)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationFocusMsg::AAPNavigationFocusMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPNavigationFocusMsg, spi_tclAAPSessionDispatcher);




/***************************************************************************
 ** FUNCTION:  AAPByeByeRequestMsg::AAPByeByeRequestMsg
 ***************************************************************************/
AAPByeByeRequestMsg::AAPByeByeRequestMsg(): m_enByeByeReason(e8_USER_SELECTION)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPByeByeRequestMsg::AAPByeByeRequestMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPByeByeRequestMsg, spi_tclAAPSessionDispatcher);




/***************************************************************************
 ** FUNCTION:  AAPByeByeResponseMsg::AAPByeByeResponseMsg
 ***************************************************************************/
AAPByeByeResponseMsg::AAPByeByeResponseMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPByeByeResponseMsg::AAPByeByeResponseMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPByeByeResponseMsg, spi_tclAAPSessionDispatcher);




/***************************************************************************
 ** FUNCTION:  AAPVoiceSessionNotifMsg::AAPVoiceSessionNotifMsg
 ***************************************************************************/
AAPVoiceSessionNotifMsg::AAPVoiceSessionNotifMsg(): m_enVoiceSessionStatus(e8_VOICE_SESSION_START)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPVoiceSessionNotifMsg::AAPVoiceSessionNotifMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPVoiceSessionNotifMsg, spi_tclAAPSessionDispatcher);




/***************************************************************************
 ** FUNCTION:  AAPAudioFocusRequestMsg::AAPAudioFocusRequestMsg
 ***************************************************************************/
AAPAudioFocusRequestMsg::AAPAudioFocusRequestMsg(): m_enDevAudFocusRequest(e8_AUDIO_FOCUS_REQ_GAIN)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPSessionStatusMsg::AAPSessionStatusMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPSessionStatusMsg, spi_tclAAPSessionDispatcher);


/***************************************************************************
 ** FUNCTION:  AAPSessionStatusMsg::AAPSessionStatusMsg
 ***************************************************************************/
AAPSessionStatusMsg::AAPSessionStatusMsg(): m_enSessionstatus(e8_SESSION_INACTIVE),m_bSessionTimedOut(false)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPAudioFocusRequestMsg::AAPAudioFocusRequestMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPAudioFocusRequestMsg, spi_tclAAPSessionDispatcher);


/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::spi_tclAAPSessionDispatcher
 ***************************************************************************/
spi_tclAAPSessionDispatcher::spi_tclAAPSessionDispatcher()
{
   ETG_TRACE_USR1((" spi_tclAAPSessionDispatcher::spi_tclAAPSessionDispatcher() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::~spi_tclAAPSessionDispatcher
 ***************************************************************************/
spi_tclAAPSessionDispatcher::~spi_tclAAPSessionDispatcher()
{
   ETG_TRACE_USR1((" spi_tclAAPSessionDispatcher::~spi_tclAAPSessionDispatcher() \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPServiceDiscoveryMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPServiceDiscoveryMsg* poAAPServiceDiscoveryMsg) const
{
   ETG_TRACE_USR1((" spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPServiceDiscoveryMsg* poAAPServiceDiscoveryMsg) \n"));
   if ((NULL != poAAPServiceDiscoveryMsg) && (NULL != poAAPServiceDiscoveryMsg->m_pszSmallIcon)&&
            (NULL != poAAPServiceDiscoveryMsg->m_pszMediumIcon)&& (NULL != poAAPServiceDiscoveryMsg->m_pszLargeIcon)&&
            	(NULL != poAAPServiceDiscoveryMsg->m_pszLabel)&& (NULL != poAAPServiceDiscoveryMsg->m_pszDeviceName))
   {
      CALL_REG_OBJECTS(spi_tclAAPRespSession,
               e16AAP_SESSION_REGID,
               vServiceDiscoveryRequestCb(*(poAAPServiceDiscoveryMsg->m_pszSmallIcon),*(poAAPServiceDiscoveryMsg->m_pszMediumIcon),
                        *(poAAPServiceDiscoveryMsg->m_pszLargeIcon), *(poAAPServiceDiscoveryMsg->m_pszLabel), *(poAAPServiceDiscoveryMsg->m_pszDeviceName)));
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPUnrecoverableErrorMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPUnrecoverableErrorMsg* poAAPUnrecoverableErrorMsg) const
{
   ETG_TRACE_USR1(("spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPUnrecoverableErrorMsg* poAAPUnrecoverableErrorMsg) \n"));
   if (NULL != poAAPUnrecoverableErrorMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespSession,
               e16AAP_SESSION_REGID,
               vUnrecoverableErrorCallback());
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPNavigationFocusMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPNavigationFocusMsg* poAAPNavigationFocusMsg) const
{
   ETG_TRACE_USR1((" spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPNavigationFocusMsg* poAAPNavigationFocusMsg) \n"));
   if (NULL != poAAPNavigationFocusMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespSession,
               e16AAP_SESSION_REGID,
               vNavigationFocusCb(poAAPNavigationFocusMsg->m_enNaviFocusType));
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPByeByeRequestMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPByeByeRequestMsg* poAAPByeByeRequestMsg) const
{
   ETG_TRACE_USR1((" spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPByeByeRequestMsg* poAAPByeByeRequestMsg) \n"));
   if (NULL != poAAPByeByeRequestMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespSession,
               e16AAP_SESSION_REGID,
               vByeByeRequestCb(poAAPByeByeRequestMsg->m_enByeByeReason));
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPByeByeResponseMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPByeByeResponseMsg* poAAPByeByeResponseMsg) const
{
   ETG_TRACE_USR1(("spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPByeByeResponseMsg* poAAPByeByeResponseMsg) \n"));
   if (NULL != poAAPByeByeResponseMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespSession,
               e16AAP_SESSION_REGID,
               vByeByeResponseCb());
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPVoiceSessionNotifMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPVoiceSessionNotifMsg* poAAPVoiceSessionNotifMsg) const
{
   ETG_TRACE_USR1(("spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPVoiceSessionNotifMsg* poAAPVoiceSessionNotifMsg entered \n"));
   if (NULL != poAAPVoiceSessionNotifMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespSession,
               e16AAP_SESSION_REGID,
               vVoiceSessionNotificationCb(poAAPVoiceSessionNotifMsg->m_enVoiceSessionStatus));
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPAudioFocusRequestMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPAudioFocusRequestMsg* poAAPAudioFocusRequestMsg) const
{
   ETG_TRACE_USR1((" spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPAudioFocusRequestMsg* poAAPAudioFocusRequestMsg \n"));
   if (NULL != poAAPAudioFocusRequestMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespSession,
               e16AAP_SESSION_REGID,
               vAudioFocusRequestCb(poAAPAudioFocusRequestMsg->m_enDevAudFocusRequest));
   } // if (NULL != poDeviceInfoMsg)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPSessionStatusMsg* poDeviceInfoMsg)
 ***************************************************************************/
t_Void spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPSessionStatusMsg* poAAPSessionStatusMsg) const
{
   ETG_TRACE_USR1((" spi_tclAAPSessionDispatcher::vHandleSessionMsg(AAPSessionStatusMsg* poAAPSessionStatusMsg \n"));
   if (NULL != poAAPSessionStatusMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespSession,
               e16AAP_SESSION_REGID,
               vSessionStatusInfo(poAAPSessionStatusMsg->m_enSessionstatus, poAAPSessionStatusMsg->m_bSessionTimedOut));
   } // if (NULL != poDeviceInfoMsg)
}


