
/***********************************************************************/
/*!
* \file  spi_tclAAPSessionDataIntf.h
* \brief AAP Session Data Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Session Data Interface
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
12.03.2015  | Pruthvi Thej Nagaraju | Initial Version

\endverbatim
**************************************************************************/

#ifndef _SPI_TCLAAPSESSIONDATAINTF_H_
#define _SPI_TCLAAPSESSIONDATAINTF_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <aauto/GalReceiver.h>
#include <aauto/util/shared_ptr.h>

#include "SPITypes.h"
#include "AAPTypes.h"


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class spi_tclAAPSessionDataIntf
* \brief AAP Session Data Interface
*
* It provides an interface to get the AAP Galreciver information.
****************************************************************************/

class spi_tclAAPSessionDataIntf
{

public:

    /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPSessionDataIntf::spi_tclAAPSessionDataIntf()
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPSessionDataIntf()
    * \brief   Constructor
    * \param
    * \sa      ~spi_tclAAPSessionDataIntf()
    **************************************************************************/
    spi_tclAAPSessionDataIntf();

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPSessionDataIntf::~spi_tclAAPSessionDataIntf()
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclAAPSessionDataIntf()
    * \brief   Destructor
    * \param
    * \sa      spi_tclAAPSessionDataIntf()
    **************************************************************************/
    ~spi_tclAAPSessionDataIntf();

    /***************************************************************************
    ** FUNCTION:  shared_ptr<GalReceiver>  spi_tclAAPSessionDataIntf::poGetGalReceiver();
    ***************************************************************************/
    /*!
    * \fn      shared_ptr<GalReceiver>  poGetGalReceiver();
    * \brief   Provides GALreceiver pointer to other components
    * \retval  shared_ptr<GalReceiver>  : Shared Pointer to GalReceiver. To be used by other AAP
    *          classes for creating and registering endpoints.
    **************************************************************************/
    shared_ptr<GalReceiver> poGetGalReceiver();


    /***************************************************************************
    ****************************END OF PUBLIC***********************************
    ***************************************************************************/

protected:

    /***************************************************************************
    *********************************PROTECTED**********************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPSessionDataIntf(const spi_tclAAPSessionDataIntf...
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPSessionDataIntf(
    *                             const spi_tclAAPSessionDataIntf& corfoSrc)
    * \brief   Copy constructor - Do not allow the creation of copy constructor
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPSessionDataIntf()
    ***************************************************************************/
    spi_tclAAPSessionDataIntf(const spi_tclAAPSessionDataIntf& corfoSrc);


    /***************************************************************************
    ** FUNCTION:  spi_tclAAPSessionDataIntf& operator=( const spi_tclMLV...
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPSessionDataIntf& operator=(
    *                          const spi_tclAAPSessionDataIntf& corfoSrc))
    * \brief   Assignment operator
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPSessionDataIntf(
    *                         const spi_tclAAPSessionDataIntf& otrSrc)
    ***************************************************************************/
    spi_tclAAPSessionDataIntf& operator=(
        const spi_tclAAPSessionDataIntf& otrSrc);


    /***************************************************************************
    ****************************END OF PROTECTED********************************
    ***************************************************************************/

private:

    /***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/

    /***************************************************************************
    ****************************END OF PRIVATE *********************************
    ***************************************************************************/

};// class spi_tclAAPSessionDataIntf


#endif   //_SPI_TCLAAPSESSIONDATAINTF_H_

////////////////////////////////////////////////////////////////////////////////
// <EOF>
