/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdSensor.h
 * \brief             Sensor wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT		:	Gen3
 SW-COMPONENT	:	Smart Phone Integration
 DESCRIPTION	:	Sensor wrapper for Android Auto
 COPYRIGHT		:	&copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 24.03.2015  | SHITANSHU SHEKHAR	 | Initial Version
 
 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLAAPCMDSENSOR_H_
#define _SPI_TCLAAPCMDSENSOR_H_

#include "Lock.h"
#include "SPITypes.h"
#include "AAPTypes.h"
#include "SensorSource.h"


/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAAPCmdSensor
 * \brief
 */

class spi_tclAAPCmdSensor
{
   public:

  /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::spi_tclAAPCmdSensor();
   ***************************************************************************/
  /*!
   * \fn     spi_tclAAPCmdSensor()
   * \brief  Default Constructor
   * \sa      spi_tclAAPCmdSensor()
   **************************************************************************/
  spi_tclAAPCmdSensor();

  /***************************************************************************
   ** FUNCTION:  virtual spi_tclAAPCmdSensor::~spi_tclAAPCmdSensor()
   ***************************************************************************/
  /*!
   * \fn      virtual ~spi_tclAAPCmdSensor()
   * \brief   Virtual Destructor
   * \sa      spi_tclAAPCmdSensor()
   **************************************************************************/
  virtual  ~spi_tclAAPCmdSensor();

  /***************************************************************************
   ** FUNCTION:  virtual spi_tclAAPCmdSensor::bInitialize(const trDataServiceConfigData& rfrDataServiceConfigData)
   ***************************************************************************/
  /*!
   * \fn      bInitialize(const trDataServiceConfigData& rfrDataServiceConfigData)
   * \brief   Initializes the SensorSource Endpoint, registers keycodes and touch
   *            screen.
   * \param   rDataServiceConfigData : the structure consists of values TRUE for location data, dead reckoning data
   *          environment data, gear status, accelerometer data and gyroscope data if available FALSE if not.
   * \sa      bUnInitialize()
   **************************************************************************/
  t_Bool  bInitializeSensor(const trDataServiceConfigData& rfrDataServiceConfigData);

  /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::bUnInitialize()
   ***************************************************************************/
  /*!
   * \fn      bUnInitialize()
   * \brief   Uninitializes the SensorSource Endpoint.
   * \sa      bInitialize()
   **************************************************************************/
  t_Void   bUnInitializeSensor();

  /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vRegisterSensors(t_Bool bLocDataAvailable,...)
   ***************************************************************************/
  /*!
   * \fn    vRegisterSensors(t_Bool bLocDataAvailable, t_Bool bIsDeadReckonedData,
   *                         t_Bool bSubForEnvData)
   * \brief	Used to register a sensor to be advertised during service discovery. 
   *		Must be called during initialization. 
   * \param   rDataServiceConfigData : the structure consists of values TRUE for location data, dead reckoning data
   *          environment data, gear status, accelerometer data and gyroscope data if available FALSE if not.
   * \sa    None
   **************************************************************************/
  t_Void   vRegisterSensors(const trDataServiceConfigData& rfrDataServiceConfigData);
  
  /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportGpsData(...)
   ***************************************************************************/
  /*!
   * \fn    vReportGpsData()
   * \brief	Use this method to report a GPS location. 
   *        The eX multiplication is to allow for fixed point representation of decimal values using an int32. 
   *        For example, the value 3.1415 becomes 31415000 in E7 notation and can be represented as an integer. 
   *\param  rfrcGpsData  Contains all GPS related data
   * \sa    vReportGpsData()
   **************************************************************************/  
  t_Void vReportGpsData(const trSensorData& rfrcGpsData);
  
   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportSpeedData(s32speedE3)
   ***************************************************************************/
  /*!
   * \fn    vReportSpeedData(s32speedE3)
   * \brief	Use this function to report the current speed of the vehicle. 
            The value reported here might be used in dead reckoning the position of the vehicle in the event of a GPS signal loss. 
   * \param s32speedE3	The speed in m/s absolute velocity multiplied by 1e3.
   * \sa    vReportSpeedData()
   **************************************************************************/
  t_Void vReportSpeedData(t_S32 s32speedE3);
  
   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportRpmData(t_S32 rpmE3)
   ***************************************************************************/
  /*!
   * \fn    vReportRpmData()
   * \brief	Use this function to report the current engine RPM value. 
   * \param rpmE3	The engine RPM value multiplied by 1e3.
   * \sa    vReportRpmData()
   **************************************************************************/
  t_Void vReportRpmData(t_S32 rpmE3);
  
   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportOdometerData(t_S32 kmsE1)
   ***************************************************************************/
  /*!
   * \fn    vReportOdometerData()
   * \brief	Use this function to report the current value of the odometer.
   * \param kmsE1	The odometer data in kilometers multiplied by 1e3.
   * \sa    vReportOdometerData()
   **************************************************************************/
  t_Void vReportOdometerData(t_S32 kmsE1);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportParkingBrakeData()
   ***************************************************************************/
  /*!
   * \fn    vReportParkingBrakeData()
   * \brief	Use this to report whether the parking brake is engaged or not. 
            The value of this sensor may be used to determine which UI elements can be interacted with. 
   * \param engaged : contains info whether parking brake is engaged or not
   * \sa    vReportParkingBrakeData()
   **************************************************************************/
  t_Void vReportParkingBrakeData(t_Bool engaged);
  
   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportGearData()
   ***************************************************************************/
  /*!
   * \fn    vReportGearData()
   * \brief	Use this method to report which gear the vehicle is in. 
   *        The value of this sensor may be used to determine which UI elements can be interacted with and which ones get locked out. 
   *        Additionally, these values might be used informationally.
   * \param s32Gear	Can be one of: GEAR_NEUTRAL, GEAR_1 .. GEAR_6, GEAR_DRIVE, GEAR_PARK, GEAR_REVERSE
   * \sa    vReportGearData()
   **************************************************************************/
  t_Void vReportGearData(t_S32 s32Gear);
  
   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportDayNightMode(...)
   ***************************************************************************/
  /*!
   * \fn    vReportDayNightMode()
   * \brief	Use this to report the value of the day-night sensor. 
   *        The value of this sensor will affect the UI of projected applications.
   * \param night_mode	true if night mode is enabled, false otherwise.
   * \sa    vReportDayNightMode()
   **************************************************************************/
  t_Void vReportDayNightMode(tenDayNightMode enMode);
 
   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportDrivingStatusData(...)
   ***************************************************************************/
  /*!
   * \fn    vReportDrivingStatusData(t_S32 s32RestrictionInfo)
   * \brief	Use this call to report driving status change. 
   * \param s32RestrictionInfo	Park activity will be used to for restrictions currently in effect.
   *        The available options are: DRIVE_STATUS_UNRESTRICTED, DRIVE_STATUS_NO_VIDEO,
   *        DRIVE_STATUS_NO_KEYBOARD_INPUT, DRIVE_STATUS_NO_VOICE_INPUT,
   *        DRIVE_STATUS_NO_CONFIG, DRIVE_STATUS_LIMIT_MESSAGE_LEN
   * \sa    vReportDrivingStatusData()
   **************************************************************************/
  t_Void vReportDrivingStatusData(t_S32 s32RestrictionInfo);

  /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportAccelerometerData(...)
   ***************************************************************************/
  /*!
   * \fn    vReportAccelerometerData()
   * \brief	Use this call to report data from accelerometer (include gravity). 
            Units are m/s^2 multiplied by 1e3.
   * \param hasAccelerationX	Acceleration along X-axis is valid.
   *        xAccelerationE3	Acceleration from left door to right.
   *        hasAccelerationY	Acceleration along Y-axis is valid.
   *        yAccelerationE3	Acceleration from back to nose.
   *        hasAccelerationZ	Acceleration along Z-axis is valid.
   *        zAccelerationE3	Acceleration from floor to ceiling.
   * \sa    vReportAccelerometerData()
   **************************************************************************/
  t_Void vReportAccelerometerData(t_Bool hasAccelerationX,
          t_S32 xAccelerationE3, t_Bool hasAccelerationY,
          t_S32 yAccelerationE3, t_Bool hasAccelerationZ,
          t_S32 zAccelerationE3);
  
  /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportGyroscopeData(...)
   ***************************************************************************/
  /*!
   * \fn    vReportGyroscopeData()
   * \brief	Use this call to report data from gyroscope. 
   *        Units are rad/s multiplied by 1e3.
   * \param hasRotationSpeedX	Rotation speed around X-axis is valid.
   *        xRotationSpeedE3	Rotation speed around axis from left door to right.
   *        hasRotationSpeedX	Rotation speed around X-axis is valid.
   *        yRotationSpeedE3	Rotation speed around axis from back to nose.
   *        hasRotationSpeedX	Rotation speed around X-axis is valid.
   *        zRotationSpeedE3	Rotation speed around axis from floor to ceiling.
   * \sa    vReportGyroscopeData()
   **************************************************************************/
  t_Void vReportGyroscopeData(t_Bool hasRotationSpeedX,
          t_S32 xRotationSpeedE3, t_Bool hasRotationSpeedY,
          t_S32 yRotationSpeedE3, t_Bool hasRotationSpeedZ,
          t_S32 zRotationSpeedE3);
  
  /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportGpsSatelliteData()
   ***************************************************************************/
  /*!
   * \fn    vReportGpsSatelliteData()
   * \brief	Use this call to report satellite data from GPS. 
   *        At least number in-use must be available.
   *        If per-satellite info is available then arrays must contain
   * \param numberInUse	Number of satellites used in GPS fix.
   *        hasNumberInView	Whether numberInView is valid.
   *        numberInView	Number of satellites visible to the GPS receiver.
   *        prns	Array of PRNs of satellites in view or NULL if per-satellite info unavailable.
   *        snrsE3	Array of SNRs of satellites in view in dB multiplied by 1e3 or NULL if per-satellite info unavailable.
   *        usedInFix	Array of flags whether this satellite was used in GPS fix or NULL if per-satellite info unavailable.
   *        azimuthsE3	Array of azimuths of satellites in degrees clockwise from north multiplied by 1e3
   *                    or NULL if per-satellite info unavailable or position data for satellites is absent.
   *        elevationsE3	Array of elevations of satellites in degrees from horizon to zenith multiplied by 1e3
   *                        or NULL if per-satellite info unavailable or position data for satellites is absent.
   * \sa    vReportGpsSatelliteData()
   **************************************************************************/
  t_Void vReportGpsSatelliteData(t_S32 numberInUse, t_Bool hasNumberInView, t_S32 numberInView,
          const t_S32* prns, const t_S32* snrsE3, const t_Bool* usedInFix,
          const t_S32* azimuthsE3, const t_S32* elevationsE3);

  /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdSensor::vReportEnvironmentData()
   ***************************************************************************/
  /*!
   * \fn    t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
   *                                   t_Bool bValidPressureUpdate, t_Double dPressure)
   * \brief Use this call to report Environment data to Phone
   * \param bValidTempUpdate : [IN] Temp update is valid
   * \param dTemp : [IN] Temp in Celsius
   * \param bValidPressureUpdate: [IN] Pressure update is valid
   * \param dPressure : [IN] Pressure in KPA
   * \retval t_Void
   **************************************************************************/
  t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
     t_Bool bValidPressureUpdate, t_Double dPressure);

  
private:
  //! SensorSource Endpoint
  SensorSource *m_poSensorSource;

  //! Lock to prevent accessing sesnsor enpoint from different thread after deleting the endpoint
  Lock m_oSensorEndPointLock;


};
#endif /* spi_tclAAPCmdSensor_H_ */
