/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdSensor.cpp
 * \brief             Sensor wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT        :   Gen3
 SW-COMPONENT   :   Smart Phone Integration
 DESCRIPTION    :   Sensor wrapper for Android Auto
 COPYRIGHT      :   &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 24.03.2015  | SHITANSHU SHEKHAR     | Initial Version
 11.07.2015  | Ramya Murthy          | Fix for same session ID being used for multiple Endpoints
 
 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPCmdSensor.h"
#include "spi_tclAAPSessionDataIntf.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPCmdSensor.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines and macros 
|----------------------------------------------------------------------------*/
#define dPItoDegree (180/3.14159265358979323846)         //used for converting pi to decimal degrees
static const t_S32 scos32Failed = -1;


//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::spi_tclAAPCmdSensor()
***************************************************************************/
spi_tclAAPCmdSensor::spi_tclAAPCmdSensor():m_poSensorSource(NULL)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdSensor::spi_tclAAPCmdSensor entered "));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::~spi_tclAAPCmdSensor()
***************************************************************************/
spi_tclAAPCmdSensor::~spi_tclAAPCmdSensor()
{
   m_oSensorEndPointLock.s16Lock();
   m_poSensorSource=NULL;
   ETG_TRACE_USR1((" spi_tclAAPCmdSensor::~spi_tclAAPCmdSensor entered "));
   m_oSensorEndPointLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::bInitializeSensor()
***************************************************************************/
t_Bool spi_tclAAPCmdSensor::bInitializeSensor(const trDataServiceConfigData& rfrDataServiceConfigData)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdSensor::bInitializeSensor() entered "));

   // Galreceiver is created for sensor endpoint

   spi_tclAAPSessionDataIntf oSessionDataIntf;
   shared_ptr<GalReceiver> poGalReceiver = oSessionDataIntf.poGetGalReceiver();
   t_Bool bRetVal = false;
   t_U32 u32LocationCharacterization = 0;

   /*lint -esym(40,nullptr) nullptr is not referenced */
   //! TODO  sensor endpoint created with ID 199.
   if (poGalReceiver != nullptr)
   {
      m_poSensorSource = new (std::nothrow) SensorSource(e32SESSIONID_AAPSENSOR, poGalReceiver->messageRouter(),
    		  u32LocationCharacterization);

      if (NULL != m_poSensorSource)
      {
         // galreceiver registration
         bRetVal = poGalReceiver->registerService(m_poSensorSource);

         ETG_TRACE_USR2(("[DESC]:spi_tclAAPCmdSensor::bInitializeSensor(): Registration success with GalReceiver : %d ",
               ETG_ENUM(BOOL,bRetVal)));

         vRegisterSensors(rfrDataServiceConfigData);
      }
      else
      {
         ETG_TRACE_ERR(("[ERR]:spi_tclAAPCmdSensor::bInitializeSensor(): Invalid SensorSource "));
      }
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::bUnInitializeSensor()
***************************************************************************/
t_Void spi_tclAAPCmdSensor::bUnInitializeSensor()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdSensor::bUnInitialize() entered "));
   if (NULL != m_poSensorSource)
   {
      m_oSensorEndPointLock.s16Lock();
      //! Deletion of end-point for every connection/disconnection.
      delete m_poSensorSource;
      m_poSensorSource = NULL;
      m_oSensorEndPointLock.vUnlock();
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclAAPCmdSensor::bUnInitializeSensor(): "
            "Invalid SensorSource passed for uninitialization "));
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vRegisterSensors()
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vRegisterSensors(const trDataServiceConfigData& rfrDataServiceConfigData)
{
	ETG_TRACE_USR1(("spi_tclAAPCmdSensor::vRegisterSensors() entered"));
   if (NULL != m_poSensorSource)
   {
      ETG_TRACE_USR1(("[DESC]:spi_tclAAPCmdSensor::vRegisterSensors() Registering sensors "
         "with Location Availability = %d, DeadReckoned data = %d, gear status = %d, accl data = %d, gyro data = %d, env data = %d",
         rfrDataServiceConfigData.bLocDataAvailable,rfrDataServiceConfigData.bDeadReckonedData,
         rfrDataServiceConfigData.bGearStatus,rfrDataServiceConfigData.bAcclData,rfrDataServiceConfigData.bGyroData,rfrDataServiceConfigData.bEnvData));

      //! Register for GPS, Gyro & Accelerometer sensors only if non-deadreckoned location data is available
   if ((true == rfrDataServiceConfigData.bLocDataAvailable) && (false == rfrDataServiceConfigData.bDeadReckonedData))
   {
      ETG_TRACE_USR1(("[DESC]:vRegisterSensors: Subscribed For location and dead reckoned Data with AAP Sensor Source"));
      m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_GPS));
      m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_GPS_SATELLITE_DATA));
   }
   if(true == rfrDataServiceConfigData.bGearStatus)
   {
      ETG_TRACE_USR1(("[DESC]:vRegisterSensors: Subscribed For Gear Data with AAP Sensor Source"));
      m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_GEAR));
   }
   if(true == rfrDataServiceConfigData.bAcclData)
   {
      ETG_TRACE_USR1(("[DESC]:vRegisterSensors: Subscribed For Acceleration Data with AAP Sensor Source"));
      m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_ACCELEROMETER_DATA));
   }
   if(true == rfrDataServiceConfigData.bGyroData)
   {
      ETG_TRACE_USR1(("[DESC]:vRegisterSensors: Subscribed For Gyro Data with AAP Sensor Source"));
      m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_GYROSCOPE_DATA));
   }
   if(true == rfrDataServiceConfigData.bEnvData)
   {
      m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_ENVIRONMENT_DATA));
	  ETG_TRACE_USR2(("[DESC]:vRegisterSensors: Subscribed For Environment Data with AAP Sensor Source"));
   }
   
   m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_PARKING_BRAKE));
   m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_NIGHT_MODE));
   m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_DRIVING_STATUS_DATA));
   m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_SPEED));
   //Below sensors to be registered in future
   //m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_RPM));
   //m_poSensorSource->registerSensor(static_cast<int>(e8_AAP_SENSOR_ODOMETER));;
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportGpsData(...)
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportGpsData(const trSensorData& rfrcGpsData)
{
   t_Bool bIsGPSFixAvailable = (e8GNSSQUALITY_NOFIX != rfrcGpsData.enGnssQuality);
   if (true == bIsGPSFixAvailable)
   {
      t_Double dLatitude = static_cast<t_Double>(fabs(((rfrcGpsData.dLatitude)*(dPItoDegree))));            // unit: degree
      t_Double dLongitude = static_cast<t_Double>(fabs(((rfrcGpsData.dLongitude)*(dPItoDegree))));          // unit: degree
      t_Float fHeading = static_cast<t_Float>(fmod((((rfrcGpsData.fHeading)*(dPItoDegree)) + 360.0), 360.0));   // unit: degree, range: [0,360)

      //!fabs converts negative values to positive. Retain the negative values of location.
      dLatitude  = (0 > rfrcGpsData.dLatitude)?((-1)*(dLatitude)):(dLatitude);
      dLongitude = (0 > rfrcGpsData.dLongitude)?((-1)*(dLongitude)):(dLongitude);

      ETG_TRACE_USR4(("[PARAM]::spi_tclAAPCmdSensor::vReportGpsData(): Raw data "
            "Latitude = %f, Longitude = %f, Heading = %f, Speed = %f, "
            "Altitude = %f, Accuracy = %f, PosixTime = 0x%x,  ",
            dLatitude, dLongitude, fHeading, rfrcGpsData.fSpeed,
            rfrcGpsData.fAltitude, rfrcGpsData.fAccuracy, rfrcGpsData.PosixTime));

      t_U64 timestamp = (rfrcGpsData.PosixTime) * 1000000000;

      t_S32 latitudeE7 = static_cast<t_S32>(dLatitude * 10000000);
      t_S32 longitudeE7 = static_cast<t_S32>(dLongitude * 10000000);

      t_Bool hasBearing = true;
      t_S32 bearingE6 = static_cast<t_S32>((fHeading) * 1000000);

      t_Bool hasAccuracy = true;
      t_S32 accuracyE3 =  static_cast<t_S32>(rfrcGpsData.fAccuracy * 1000);

      t_Bool hasAltitude = true;
      t_S32 altitudeE2 = static_cast<t_S32>((rfrcGpsData.fAltitude) * 100);

      t_Bool hasSpeed = true;
      t_S32 speedE3 = static_cast<t_S32>((rfrcGpsData.fSpeed) * 1000);

      ETG_TRACE_USR4(("[PARAM]::spi_tclAAPCmdSensor::vReportGpsData(): Calculated data "
            "latitudeE7 = %d, longitudeE7 = %d, bearingE6 = %d, speedE3 = %d, "
            "altitudeE2 = %d, accuracyE3 = %d, timestamp = %d ",
            latitudeE7, longitudeE7, bearingE6, speedE3,
            altitudeE2, accuracyE3, timestamp));

      t_S32 s32RetStatus = scos32Failed;

      m_oSensorEndPointLock.s16Lock();
      if(NULL != m_poSensorSource)
      {
            s32RetStatus = m_poSensorSource->reportLocationData(
            timestamp, latitudeE7, longitudeE7,
            hasAccuracy, accuracyE3, hasAltitude, altitudeE2,
            hasSpeed, speedE3, hasBearing, bearingE6);
      }
      m_oSensorEndPointLock.vUnlock();

       ETG_TRACE_USR2(("[DESC]:spi_tclAAPCmdSensor::vReportGpsData(): reportLocationData Status = %d",
             ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus)));
   }//if (NULL != m_poSensorSource)
   else if (false == bIsGPSFixAvailable)
   {
      ETG_TRACE_USR4(("[ERR]:spi_tclAAPCmdSensor::vReportGpsData(): No GPS Fix available "));
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportSpeedData()
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportSpeedData(t_S32 s32speedE3)
{
   t_S32 s32RetStatus = scos32Failed;
   t_Bool bHasCruiseEngaged = false, bHasCuiseSetSpeed = false;
   t_Bool bIsCruiseEngaged = false;
   t_S32 s32CruiseSpeed = 0;

   m_oSensorEndPointLock.s16Lock();
   if (NULL != m_poSensorSource)
   {
     s32RetStatus = m_poSensorSource->reportSpeedData(s32speedE3, bHasCruiseEngaged, bIsCruiseEngaged, bHasCuiseSetSpeed, s32CruiseSpeed);
   }
   m_oSensorEndPointLock.vUnlock();
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:spi_tclAAPCmdSensor::vReportSpeedData() Status = %d, Speed = %d ",
           ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus), s32speedE3));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportRpmData(t_S32 rpmE3)
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportRpmData(t_S32 rpmE3)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdSensor::vReportRpmData() entered "));

   SPI_INTENTIONALLY_UNUSED(rpmE3);     //To remove Lint Warning
   //TODO Implementation to be done in future
//   t_S32 s32RetStatus = m_poSensorSource->reportRpmData(rpmE3);
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportOdometerData(t_S32 kmsE1)
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportOdometerData(t_S32 kmsE1)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdSensor::vReportOdometerData() entered "));

   SPI_INTENTIONALLY_UNUSED(kmsE1);     //To remove Lint Warning
	//TODO Implementation to be done in future
//   t_S32 s32RetStatus = m_poSensorSource->reportOdometerData(kmsE1);
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportParkingBrakeData()
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportParkingBrakeData(t_Bool bEngaged)
{
   t_S32 s32RetStatus = scos32Failed;
   m_oSensorEndPointLock.s16Lock();
   if (NULL != m_poSensorSource)
   {
     s32RetStatus = m_poSensorSource->reportParkingBrakeData(bEngaged);
   }
   m_oSensorEndPointLock.vUnlock();     
   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPCmdSensor::vReportParkingBrakeData() Status = %d (ParkEngaged = %d) ",
           ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus), ETG_ENUM(BOOL, bEngaged)));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportGearData(...)
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportGearData(t_S32 s32Gear)
{
   t_S32 s32RetStatus = scos32Failed;
   m_oSensorEndPointLock.s16Lock();
   if (NULL != m_poSensorSource)
   {
      s32RetStatus = m_poSensorSource->reportGearData(s32Gear);
   }
   m_oSensorEndPointLock.vUnlock();
   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPCmdSensor::vReportGearData() Status = %d (Gear = %d) ",
         ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus), ETG_ENUM(AAP_GEARDATA, s32Gear)));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportDayNightMode(...)
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportDayNightMode(tenDayNightMode enMode)
{
   t_S32 s32RetStatus = scos32Failed;
   m_oSensorEndPointLock.s16Lock();
   if (NULL != m_poSensorSource)
   {
      t_Bool bNight = (e8_AAP_DAY_MODE != enMode);
      s32RetStatus = m_poSensorSource->reportNightModeData(bNight);
   }
   m_oSensorEndPointLock.vUnlock();
   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPCmdSensor::vReportDayNightMode() Status = %d",
         ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus)));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportDrivingStatusData(...)
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportDrivingStatusData(t_S32 s32RestrictionInfo)
{
   t_S32 s32RetStatus = scos32Failed;
   m_oSensorEndPointLock.s16Lock();
   //! Send Drive status data
   if (NULL != m_poSensorSource)
   {
      s32RetStatus = m_poSensorSource->reportDrivingStatusData(s32RestrictionInfo);
   }
   m_oSensorEndPointLock.vUnlock();
   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPCmdSensor::vReportDrivingStatusData() Status = %d (RestrictionInfo = %d)",
            ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus), s32RestrictionInfo));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportAccelerometerData(...)
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportAccelerometerData(t_Bool hasAccelerationX,
                              t_S32 xAccelerationE3, t_Bool hasAccelerationY,
                              t_S32 yAccelerationE3, t_Bool hasAccelerationZ,
                              t_S32 zAccelerationE3)
{
   t_S32 s32RetStatus = scos32Failed;
   m_oSensorEndPointLock.s16Lock();
   if (NULL != m_poSensorSource)
   {
      s32RetStatus = m_poSensorSource->reportAccelerometerData(hasAccelerationX, xAccelerationE3,
                                   hasAccelerationY, yAccelerationE3,
                                   hasAccelerationZ, zAccelerationE3);
   }
   m_oSensorEndPointLock.vUnlock();
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:spi_tclAAPCmdSensor::vReportAccelerometerData() Status = %d (yAccelerationE3 = %d)",
         ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus), yAccelerationE3));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportGyroscopeData(...)
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportGyroscopeData(t_Bool hasRotationSpeedX,
                         t_S32 xRotationSpeedE3, t_Bool hasRotationSpeedY,
                         t_S32 yRotationSpeedE3, t_Bool hasRotationSpeedZ,
                         t_S32 zRotationSpeedE3)
{
   t_S32 s32RetStatus = scos32Failed;
   m_oSensorEndPointLock.s16Lock();
   if (NULL != m_poSensorSource)
   {
      s32RetStatus = m_poSensorSource->reportGyroscopeData(hasRotationSpeedX, xRotationSpeedE3,
                                   hasRotationSpeedY, yRotationSpeedE3,
                                   hasRotationSpeedZ, zRotationSpeedE3);
   }
   m_oSensorEndPointLock.vUnlock();
   ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]:spi_tclAAPCmdSensor::vReportGyroscopeData() Status = %d (zRotationSpeedE3 = %d)",
         ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus), zRotationSpeedE3));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportGpsSatelliteData()
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportGpsSatelliteData(t_S32 numberInUse, t_Bool hasNumberInView,
                                       t_S32 numberInView,const t_S32* prns,
                                       const t_S32* snrsE3, const t_Bool* usedInFix,
                                       const t_S32* azimuthsE3, const t_S32* elevationsE3)
{
   t_S32 s32RetStatus = scos32Failed;
   m_oSensorEndPointLock.s16Lock();
   if (NULL != m_poSensorSource)
   {
      s32RetStatus = m_poSensorSource->reportGpsSatelliteData(numberInUse, hasNumberInView,
                                 numberInView,  prns, snrsE3,
                                 usedInFix,  azimuthsE3, elevationsE3);
   }
   m_oSensorEndPointLock.vUnlock();
   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPCmdSensor::vReportGpsSatelliteData() Status = %d",
        ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus)));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdSensor::vReportEnvironmentData()
***************************************************************************/
t_Void spi_tclAAPCmdSensor::vReportEnvironmentData(t_Bool bValidTempUpdate,
                                                   t_Double dTemp,
                                                   t_Bool bValidPressureUpdate, 
                                                   t_Double dPressure)
{
   t_S32 s32RetStatus = scos32Failed;

   t_S32 s32TempE3 = static_cast<t_S32>(dTemp * 1000);
   t_S32 s32PressureE3 = static_cast<t_S32>(dPressure * 1000);

   t_Bool bHasRain = false;
   t_S32 s32RainLevel = 0;

   m_oSensorEndPointLock.s16Lock();
   if (NULL != m_poSensorSource)
   {
      s32RetStatus = m_poSensorSource->reportEnvironmentData(bValidTempUpdate,s32TempE3,
         bValidPressureUpdate,s32PressureE3, bHasRain, s32RainLevel);
   }//if (NULL != m_poSensorSource)
   m_oSensorEndPointLock.vUnlock();

   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPCmdSensor::vReportEnvironmentData() Status = %d",
      ETG_ENUM(AAP_STATUS_RETURNED,s32RetStatus)));
}

//lint –restore
