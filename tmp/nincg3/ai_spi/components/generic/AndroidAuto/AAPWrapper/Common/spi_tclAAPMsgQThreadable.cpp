/***********************************************************************/
/*!
 * \file  spi_tclAAPMsgQThreadable.h
 * \brief implements threading based on MsgQthreader for AAP Wrappers
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    implements threading based on MsgQthreader for AAP Wrappers
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.03.2015  | Pruthvi Thej Nagaraju | Initial Version
 25.05.2015  | Vinoop U 			 | Extented for handling media playback metadata
 26.02.2016  | Rachana L Achar       | Extended for handling AAP Navigation 
 10.03.2016  | Rachana L Achar       | Extended for handling AAP Notification

 \endverbatim
 *************************************************************************/

#include "spi_tclAAPMsgQThreadable.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclAAPMsgQThreadable.cpp.trc.h"
   #endif
#endif

/***************************************************************************
 ** FUNCTION:  spi_tclMLAAPDiscoverer::spi_tclMLAAPDiscoverer()
 ***************************************************************************/
spi_tclAAPMsgQThreadable::spi_tclAAPMsgQThreadable():
   m_poDiscovererDispatcher(NULL),
   m_poSessionDispatcher(NULL),
   m_poBTDispatcher(NULL),
   m_poVideoDispatcher(NULL),
   m_poAudioDispatcher(NULL),
   m_poMediaPlaybackDispatcher(NULL)
{
   ETG_TRACE_USR1((" spi_tclAAPMsgQThreadable::spi_tclAAPMsgQThreadable() entered \n"));
   m_poDiscovererDispatcher = new spi_tclAAPDiscovererDispatcher();
   SPI_NORMAL_ASSERT(NULL == m_poDiscovererDispatcher);
   m_poSessionDispatcher = new spi_tclAAPSessionDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poSessionDispatcher);
   m_poBTDispatcher = new spi_tclAAPBTDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poBTDispatcher);
   m_poVideoDispatcher = new spi_tclAAPVideoDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poVideoDispatcher);
   m_poAudioDispatcher = new spi_tclAAPAudioDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poAudioDispatcher);
   m_poMediaPlaybackDispatcher = new spi_tclAAPMediaPlaybackDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poMediaPlaybackDispatcher);
   m_poNavigationDispatcher = new spi_tclAAPNavigationDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poNavigationDispatcher);
   m_poNotificationDispatcher = new spi_tclAAPNotificationDispatcher;
   SPI_NORMAL_ASSERT(NULL == m_poNotificationDispatcher);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLAAPDiscoverer::~spi_tclMLAAPDiscoverer()
 ***************************************************************************/

spi_tclAAPMsgQThreadable::~spi_tclAAPMsgQThreadable()
{
   ETG_TRACE_USR1((" spi_tclAAPMsgQThreadable::~spi_tclAAPMsgQThreadable() entered \n"));
   RELEASE_MEM(m_poAudioDispatcher);
   RELEASE_MEM(m_poVideoDispatcher);
   RELEASE_MEM(m_poBTDispatcher);
   RELEASE_MEM(m_poSessionDispatcher);
   RELEASE_MEM(m_poDiscovererDispatcher);
   RELEASE_MEM(m_poMediaPlaybackDispatcher);
   RELEASE_MEM(m_poNavigationDispatcher);
   RELEASE_MEM(m_poNotificationDispatcher);
}

/***************************************************************************
 ** FUNCTION:  spi_tclMLAAPDiscoverer::vExecute
 ***************************************************************************/

t_Void spi_tclAAPMsgQThreadable::vExecute(tShlMessage *poMessage)
{
   ETG_TRACE_USR1((" spi_tclAAPMsgQThreadable::vExecute entered \n"));
   if ((NULL != poMessage) && (NULL != poMessage->pvBuffer))
   {
      trMsgBase *prMsgBase = static_cast<trMsgBase*>(poMessage->pvBuffer);
      if (NULL != prMsgBase)
      {
         t_U32 u32ServiceID = prMsgBase->u32GetServiceID();
         ETG_TRACE_USR1(("ServiceID %d \n", u32ServiceID));

         //! Handle message to corresponding dispatcher based on Service ID
         switch (u32ServiceID)
         {
            case e32MODULEID_AAPDISCOVERER:
            {
               AAPDiscMsgBase *poDiscMsgBase =
                        static_cast<AAPDiscMsgBase*>(poMessage->pvBuffer);
               if (NULL != poDiscMsgBase)
               {
                  poDiscMsgBase->vDispatchMsg(m_poDiscovererDispatcher);
               } // if (NULL != prDiscMsgBase)
            }
               break;
            case e32MODULEID_AAPSESSION:
            {
               AAPSessionMsgBase *poSessionMsgBase =
                        static_cast<AAPSessionMsgBase*>(poMessage->pvBuffer);
               if (NULL != poSessionMsgBase)
               {
                  poSessionMsgBase->vDispatchMsg(m_poSessionDispatcher);
               } // if (NULL != prDiscMsgBase)
            }
               break;
            case e32MODULEID_AAPBLUETOOTH:
            {
               AAPBTMsgBase *poBTMsgBase =
                        static_cast<AAPBTMsgBase*>(poMessage->pvBuffer);
               if (NULL != poBTMsgBase)
               {
                  poBTMsgBase->vDispatchMsg(m_poBTDispatcher);
               } // if (NULL != prDiscMsgBase)
            }
               break;
            case e32MODULEID_AAPVIDEO:
            {
               AAPVideoMsgBase *poVideoMsgBase = static_cast<AAPVideoMsgBase*>(poMessage->pvBuffer);
               if (NULL != poVideoMsgBase)
               {
                  poVideoMsgBase->vDispatchMsg(m_poVideoDispatcher);
               } // if (NULL != prDiscMsgBase)
            }
            break;
            case e32MODULEID_AAPAUDIO:
            {
               AAPAudioMsgBase *poAudioMsgBase = static_cast<AAPAudioMsgBase*>(poMessage->pvBuffer);
               if (NULL != poAudioMsgBase)
               {
                  poAudioMsgBase->vDispatchMsg(m_poAudioDispatcher);
               } // if (NULL != prDiscMsgBase)
            }
            break;
            case e32MODULEID_AAPMEDIAPLAYBACK:
            {
               AAPMediaPlaybackMsgBase *poMediaPlaybackMsgBase = static_cast<AAPMediaPlaybackMsgBase*>(poMessage->pvBuffer);
               if (NULL != poMediaPlaybackMsgBase)
               {
            	   poMediaPlaybackMsgBase->vDispatchMsg(m_poMediaPlaybackDispatcher);
               }  // if (NULL != poMediaPlaybackMsgBase)
            }
            break;
            case e32MODULEID_AAPNAVIGATIONTBT:
            {
                AAPNavigationMsgBase *poNavigationMsgBase = static_cast<AAPNavigationMsgBase*>(poMessage->pvBuffer);
                if (NULL != poNavigationMsgBase)
                {
                  poNavigationMsgBase->vDispatchMsg(m_poNavigationDispatcher);
                }  // if (NULL != poNavigationMsgBase)
            }
            break;
            case e32MODULEID_AAPNOTIFICATION:
            {
                AAPNotificationMsgBase *poNotificationMsgBase = static_cast<AAPNotificationMsgBase*>(poMessage->pvBuffer);
                if (NULL != poNotificationMsgBase)
                {
                	poNotificationMsgBase->vDispatchMsg(m_poNotificationDispatcher);
                }  // if (NULL != poNotificationMsgBase)
            }
            break;
			
            default:
            {
               ETG_TRACE_ERR(("Unknown ServiceID %d", u32ServiceID));
            }
               break;
         } // switch (u32ServiceID)
      } // if (NULL != prMsgBase)

      t_U8 *pu8Buffer = static_cast<t_PU8> (poMessage->pvBuffer);
      RELEASE_ARRAY_MEM(pu8Buffer);
   }
   RELEASE_MEM(poMessage);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPMsgQThreadable::~spi_tclMLAAPDiscoverer()
 ***************************************************************************/
tShlMessage* spi_tclAAPMsgQThreadable::poGetMsgBuffer(size_t siBuffer)
{
   ETG_TRACE_USR1((" spi_tclAAPMsgQThreadable::poGetMsgBuffer entered \n"));
   tShlMessage* poMessage = new tShlMessage;

   if (NULL != poMessage)
   {
      if(0 < siBuffer)
      {
         //! Allocate the requested memory
         poMessage->pvBuffer = new t_U8[siBuffer];
      }
      else
      {
         poMessage->pvBuffer = NULL;
      } // if(0 < siBuffer)

      if (NULL != poMessage->pvBuffer)
      {
         poMessage->size = siBuffer;
      }
      else
      {
         //! Free the message as internal allocation failed.
         delete poMessage;
         poMessage = NULL;
      } //   if (NULL != poMessage->pvBuffer)
   } // if (NULL != poMessage)

   return poMessage;
}
