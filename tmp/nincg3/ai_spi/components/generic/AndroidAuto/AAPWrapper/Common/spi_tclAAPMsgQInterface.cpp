/***********************************************************************/
/*!
 * \file  spi_tclAAPMsgQInterface.cpp
 * \brief interface for writing data to Q to use the MsgQ based
 *        threading model for AAP Wrapper
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    interface for writing data to Q to use the MsgQ based
                 threading model for AAP Wrapper
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.03.2015  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/


/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPMsgQInterface.h"
#include "MessageQueue.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclAAPMsgQInterface.cpp.trc.h"
   #endif
#endif

using namespace shl::thread;

/***************************************************************************
 ** FUNCTION:  spi_tclAAPMsgQInterface::spi_tclAAPMsgQInterface()
 ***************************************************************************/
spi_tclAAPMsgQInterface::spi_tclAAPMsgQInterface():m_poAAPMsgQThreadable(NULL), m_poAAPMsgQThreader(NULL)
{
   ETG_TRACE_USR1(("spi_tclAAPMsgQInterface::spi_tclAAPMsgQInterface entered \n"));
   m_poAAPMsgQThreadable = new spi_tclAAPMsgQThreadable();
   SPI_NORMAL_ASSERT(NULL == m_poAAPMsgQThreadable);
   m_poAAPMsgQThreader = new MsgQThreader(m_poAAPMsgQThreadable, true);
   SPI_NORMAL_ASSERT(NULL == m_poAAPMsgQThreader);
   if(NULL != m_poAAPMsgQThreader)
   {
      m_poAAPMsgQThreader->vSetThreadName("AAPRespQ");
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPMsgQInterface::~spi_tclAAPMsgQInterface()
 ***************************************************************************/
spi_tclAAPMsgQInterface::~spi_tclAAPMsgQInterface()
{
   ETG_TRACE_USR1((" spi_tclAAPMsgQInterface::~spi_tclAAPMsgQInterface() entered \n"));

   if (NULL != m_poAAPMsgQThreader)
   {
      //Terminate the Message Queue threader before deleting it.
      MessageQueue *poMsgQ = m_poAAPMsgQThreader->poGetMessageQueu();
      trMsgBase oMsgQTermMsg;
      poMsgQ->s16Push(static_cast<t_Void*> (&oMsgQTermMsg), 0, 1, e8_TCL_THREAD_TERMINATE_MESSAGE);

      //Wait for the message queue thread to join.
      Threader::vWaitForTermination(m_poAAPMsgQThreader->pGetThreadID());
   }

   RELEASE_MEM(m_poAAPMsgQThreader);
   RELEASE_MEM(m_poAAPMsgQThreadable);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPMsgQInterface::bWriteMsgToQ
 ***************************************************************************/
t_Bool spi_tclAAPMsgQInterface::bWriteMsgToQ(trMsgBase *prMsgBase,
         t_U32 u32MsgSize)
{
   ETG_TRACE_USR1((" spi_tclAAPMsgQInterface::bWriteMsgToQ entered \n"));
   t_Bool bRetQ = false;

   if (NULL != m_poAAPMsgQThreader)
   {
      //! Get the MsgQ form threader and push the message to Q
      MessageQueue *poMsgQ = m_poAAPMsgQThreader->poGetMessageQueu();
      if ((NULL != poMsgQ) && (NULL != prMsgBase))
      {
         t_S32 s32RetMsgQ = poMsgQ->s16Push(static_cast<t_Void*>(prMsgBase), u32MsgSize);
         bRetQ = (0 == s32RetMsgQ);
      } // if ((NULL != poMsgQ) && (NULL != prMsgBase))
   } //if (NULL != m_poAAPMsgQThreader)

   if(false == bRetQ)
   {
      ETG_TRACE_ERR(("Write to MsgQ failed  \n"));
   }

   return bRetQ;
}
