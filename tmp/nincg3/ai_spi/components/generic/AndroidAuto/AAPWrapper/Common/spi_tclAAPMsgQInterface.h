/***********************************************************************/
/*!
 * \file  spi_tclAAPMsgQInterface.h
 * \brief interface for writing data to Q to use the MsgQ based
 *        threading model for AAP Wrapper
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    interface for writing data to Q to use the MsgQ based
                 threading model for AAP Wrapper
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 05.03.2015  | Pruthvi Thej Nagaraju | Initial Version

 \endverbatim
 *************************************************************************/

#ifndef SPI_TCLAAPMSGQINTERFACE_H_
#define SPI_TCLAAPMSGQINTERFACE_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "MsgQThreader.h"
#include "spi_tclAAPMsgQThreadable.h"
#include "GenericSingleton.h"

using namespace shl::thread;

/****************************************************************************/
/*!
* \class spi_tclAAPMsgQInterface
* \brief interface for writing data to Q to use the MsgQ based
*        threading model for AAP Wrapper
****************************************************************************/
class spi_tclAAPMsgQInterface : public GenericSingleton<spi_tclAAPMsgQInterface>
{
   public:

      /***************************************************************************
      ** FUNCTION:  spi_tclAAPMsgQInterface::spi_tclAAPMsgQInterface()
      ***************************************************************************/
      /*!
      * \fn      spi_tclAAPMsgQInterface()
      * \brief   Default Constructor
      * \sa      ~spi_tclAAPMsgQInterface()
      **************************************************************************/
      spi_tclAAPMsgQInterface();

      /***************************************************************************
      ** FUNCTION:  spi_tclAAPMsgQInterface::~spi_tclAAPMsgQInterface()
      ***************************************************************************/
      /*!
      * \fn      ~spi_tclAAPMsgQInterface()
      * \brief   Destructor
      * \sa      spi_tclAAPMsgQInterface()
      **************************************************************************/
      ~spi_tclAAPMsgQInterface();

      /***************************************************************************
      ** FUNCTION:  spi_tclAAPMsgQInterface::bWriteMsgToQ
      ***************************************************************************/
      /*!
      * \fn      bWriteMsgToQ(trMsgBase *prMsgBase, t_U32 u32MsgSize)
      * \brief   Write Msg to Q for dispatching the message by a seperate thread
      * \param prMsgBase : Pointer to Base Message type
      * \param u32MsgSize :  size of the message to be written to the MsgQ
      **************************************************************************/
      t_Bool bWriteMsgToQ(trMsgBase *prMsgBase, t_U32 u32MsgSize);

      //! Base Singleton class
      friend class GenericSingleton<spi_tclAAPMsgQInterface> ;

   private:

      //! Pointer to the overridden MsgQThreadable class
      spi_tclAAPMsgQThreadable *m_poAAPMsgQThreadable;

      //! pointer to MsgQThreader
      MsgQThreader *m_poAAPMsgQThreader;

};


#endif /* SPI_TCLAAPMSGQINTERFACE_H_ */
