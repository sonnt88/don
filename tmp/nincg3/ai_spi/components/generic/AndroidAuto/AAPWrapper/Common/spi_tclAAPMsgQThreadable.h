/***********************************************************************/
/*!
* \file  spi_tclAAPMsgQThreadable.h
* \brief implements threading based on MsgQthreader for AAP Wrappers
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    implements threading based on MsgQthreader for AAP Wrappers
AUTHOR:         Pruthvi Thej Nagaraju
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
05.03.2015  | Pruthvi Thej Nagaraju | Initial Version
25.05.2015  | Vinoop U 			    | Extented for handling media playback metadata 
26.02.2016  | Rachana L Achar       | Extended for handling AAP Navigation
10.03.2016  | Rachana L Achar       | Extended for handling AAP Notification

\endverbatim
*************************************************************************/

#ifndef SPI_TCLAAPMSGQTHREADER_H_
#define SPI_TCLAAPMSGQTHREADER_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "MsgQThreadable.h"
#include "spi_tclAAPDiscovererDispatcher.h"
#include "spi_tclAAPSessionDispatcher.h"
#include "spi_tclAAPBTDispatcher.h"
#include "spi_tclAAPVideoDispatcher.h"
#include "spi_tclAAPAudioDispatcher.h"
#include "spi_tclAAPMediaPlaybackDispatcher.h"
#include "spi_tclAAPNavigationDispatcher.h"
#include "spi_tclAAPNotificationDispatcher.h"

using namespace shl::thread;

/****************************************************************************/
/*!
* \class spi_tclAAPMsgQThreadable
* \brief implements threading based on MsgQthreader for AAP Wrappers
*
* Responsible for calling the respective dispatchers and to allocate memory
* when a message arrives on Q
*
****************************************************************************/

class spi_tclAAPMsgQThreadable : public MsgQThreadable
{
   public:

      /***************************************************************************
      ** FUNCTION:  spi_tclAAPMsgQThreadable::spi_tclAAPMsgQThreadable()
      ***************************************************************************/
      /*!
      * \fn      spi_tclAAPMsgQThreadable()
      * \brief   Default Constructor
      * \sa      ~spi_tclAAPMsgQThreadable()
      **************************************************************************/
      spi_tclAAPMsgQThreadable();

      /***************************************************************************
      ** FUNCTION:  spi_tclAAPMsgQThreadable::~spi_tclAAPMsgQThreadable()
      ***************************************************************************/
      /*!
      * \fn      ~spi_tclAAPMsgQThreadable()
      * \brief   Destructor
      * \sa      spi_tclAAPMsgQThreadable()
      **************************************************************************/
      ~spi_tclAAPMsgQThreadable();

   protected:
      /***************************************************************************
      ** FUNCTION:  spi_tclAAPMsgQThreadable::vExecute
      ***************************************************************************/
      /*!
      * \fn      t_Void vExecute(tShlMessage *poMessage)
      * \brief   Responsible for posting the message to respective dispatchers
      * \param   poMessage : message received from MsgQ for dispatching
      **************************************************************************/
      virtual t_Void vExecute(tShlMessage *poMessage);

      /***************************************************************************
      ** FUNCTION:  spi_tclAAPMsgQThreadable::tShlMessage* poGetMsgBuffer(size_t )
      ***************************************************************************/
      /*!
      * \fn      tShlMessage* poGetMsgBuffer(size_t )
      * \brief  This function will be called for requesting the storage allocation for received
      *           message
      * \param siBuffer: size of the buffer to be allocated for the received message
      **************************************************************************/
      virtual tShlMessage* poGetMsgBuffer(size_t siBuffer);

   private:

      //! Pointers to Message Dispatchers
      spi_tclAAPDiscovererDispatcher*  m_poDiscovererDispatcher;

      spi_tclAAPSessionDispatcher*     m_poSessionDispatcher;

      spi_tclAAPBTDispatcher*          m_poBTDispatcher;

      spi_tclAAPVideoDispatcher*       m_poVideoDispatcher;
      
      spi_tclAAPAudioDispatcher*       m_poAudioDispatcher;

      spi_tclAAPMediaPlaybackDispatcher* m_poMediaPlaybackDispatcher;

      spi_tclAAPNavigationDispatcher*  m_poNavigationDispatcher;

      spi_tclAAPNotificationDispatcher*  m_poNotificationDispatcher;
};


#endif /* SPI_TCLAAPMSGQTHREADER_H_ */
