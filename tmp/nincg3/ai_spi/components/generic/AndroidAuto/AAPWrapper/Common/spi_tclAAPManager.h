/*!
 *******************************************************************************
 * \file              spi_tclAAPManager.h
 * \brief            AAP Wrapper Manager
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    AAP Wrapper Manager to provide interface to SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 22.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 24.03.2015 |  SHITANSHU SHEKHAR		   | Added Sensor pointer
 26.02.2016 |  Rachana L Achar             | AAP Navigation implementation
 10.03.2016 |  Rachana L Achar             | AAP Notification implementation

 \endverbatim
 ******************************************************************************/
#ifndef SPI_TCLAAPMANAGER_H_
#define SPI_TCLAAPMANAGER_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "GenericSingleton.h"
#include "RespRegister.h"
#include "spi_tclAAPCmdDiscoverer.h"
#include "spi_tclAAPCmdSession.h"
#include "spi_tclAAPCmdBluetooth.h"
#include "spi_tclAAPCmdInput.h"
#include "spi_tclAAPCmdVideo.h"
#include "spi_tclAAPCmdSensor.h"
#include "spi_tclAAPCmdAudio.h"
#include "spi_tclAAPcmdMediaPlayback.h"
#include "spi_tclAAPCmdNavigation.h"
#include "spi_tclAAPCmdNotification.h"

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAAPManager
 * \brief Manager class  to provide interface for SPI to interact with
 *        AAPWrapper classes
 *
 */
class spi_tclAAPManager: public GenericSingleton<spi_tclAAPManager>
{
   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPManager::~spi_tclAAPManager();
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAAPManager()
       * \brief   Destructor
       * \sa      spi_tclAAPManager()
       **************************************************************************/
      ~spi_tclAAPManager();


      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdDiscoverer* poGetDiscovererInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclAAPCmdDiscoverer* poGetDiscovererInstance()
       * \brief   Get Discoverer command class's instance
       **************************************************************************/
      spi_tclAAPCmdDiscoverer* poGetDiscovererInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdSession* poGetSessionInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclAAPCmdSession* poGetSessionInstance()
       * \brief   Get Session command class's instance
       **************************************************************************/
      spi_tclAAPCmdSession* poGetSessionInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdBluetooth* poGetBluetoothInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclAAPCmdBluetooth* poGetBluetoothInstance()
       * \brief   Get Bluetooth command class's instance
       **************************************************************************/
      spi_tclAAPCmdBluetooth* poGetBluetoothInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdInput* poGetInputInstance()
       ***************************************************************************/
      /*!
       * \fn       poGetInputInstance* poGetInputInstance()
       * \brief   Get Input command class's instance
       **************************************************************************/
      spi_tclAAPCmdInput* poGetInputInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdSensor* poGetSensorInstance()
       ***************************************************************************/
      /*!
       * \fn       poGetSensorInstance* poGetSensorInstance()
       * \brief   Get Sensor command class's instance
       **************************************************************************/
      spi_tclAAPCmdSensor* poGetSensorInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdVideo* poGetVideoInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclAAPCmdVideo* poGetVideoInstance()
       * \brief   Get command video class's instance
       **************************************************************************/
      spi_tclAAPCmdVideo* poGetVideoInstance();	

      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdAudio* poGetAudioInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclAAPCmdAudio* poGetAudioInstance()
       * \brief    Get Audio command class's instance
       **************************************************************************/
      spi_tclAAPCmdAudio* poGetAudioInstance();
	  
      /***************************************************************************
       ** FUNCTION:    spi_tclAAPcmdMediaPlayback* poGetMediaPlaybackInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclAAPcmdMediaPlayback* poGetMediaPlaybackInstance()
       * \brief    Get media playback class's instance
       **************************************************************************/
      spi_tclAAPcmdMediaPlayback* poGetMediaPlaybackInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdNavigation* poGetNavigationInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclAAPCmdNavigation* poGetNavigationInstance()
       * \brief    Get navigation command class's instance
       **************************************************************************/
      spi_tclAAPCmdNavigation* poGetNavigationInstance();

      /***************************************************************************
       ** FUNCTION:    spi_tclAAPCmdNotification* poGetNotificationInstance()
       ***************************************************************************/
      /*!
       * \fn       spi_tclAAPCmdNotification* poGetNotificationInstance()
       * \brief    Get notification command class's instance
       **************************************************************************/
      spi_tclAAPCmdNotification* poGetNotificationInstance();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPManager::bRegisterObject(RespBase *poRespBase)
       ***************************************************************************/
      /*!
       * \fn       t_Bool spi_tclAAPManager::bRegisterObject(RespBase *poRespBase)
       * \brief   Registers the response class object
       * \ret      returns true on success
       **************************************************************************/
      t_Bool bRegisterObject(RespBase *poRespBase);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPManager::bUnRegisterObject(RespBase *poRespBase)
       ***************************************************************************/
      /*!
       * \fn       t_Bool spi_tclAAPManager::bUnRegisterObject(RespBase *poRespBase)
       * \brief    Unregisters the response class object
       * \ret      returns true on success
       **************************************************************************/
      t_Bool bUnRegisterObject(RespBase *poRespBase);

      //! Base Singleton class
      friend class GenericSingleton<spi_tclAAPManager> ;

      /***************************************************************************
       *******************************PRIVATE************************************
       **************************************************************************/
   private:

      //! Discoverer command class pointer
      spi_tclAAPCmdDiscoverer*   m_poCmdDiscoverer;

      //! Session command class pointer
      spi_tclAAPCmdSession*      m_poCmdSession;

      //! Bluetooth command class pointer
      spi_tclAAPCmdBluetooth* m_poCmdBluetooth;

      //! Input command class object
      spi_tclAAPCmdInput *m_poCmdInput;

      //! Sensor command class object
      spi_tclAAPCmdSensor *m_poCmdSensor;

      //! Video Command Class object
      spi_tclAAPCmdVideo* m_poCmdVideo;

      //! Audio command class object
      spi_tclAAPCmdAudio *m_poCmdAudio;

      //! Metadata command class object
      spi_tclAAPcmdMediaPlayback *m_poCmdMediaPlayback;

      //! Navigation command class object
      spi_tclAAPCmdNavigation *m_poCmdNavigation;

      //! Notification command class object
      spi_tclAAPCmdNotification *m_poCmdNotification;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPManager::spi_tclAAPManager()
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPManager()
       * \brief   Default Constructor
       * \sa      ~spi_tclAAPManager()
       **************************************************************************/
      spi_tclAAPManager();
};

#endif // SPI_TCLAAPMANAGER_H_

