/*!
 *******************************************************************************
 * \file              spi_tclAAPManager.cpp
 * \brief            RealAAP Wrapper Manager
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    AAP Wrapper Manager to provide interface to SPI
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 22.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 24.03.2015 |  SHITANSHU SHEKHAR		   | Added Sensor Handler
 25.05.2015 |  Vinoop U 				   | MediaMetadata handler
 26.02.2016 |  Rachana L Achar             | AAP Navigation implementation
 10.03.2016 |  Rachana L Achar             | AAP Notification implementation

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPManager.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPManager.cpp.trc.h"
   #endif
#endif


/***************************************************************************
 ** FUNCTION:  spi_tclAAPManager::spi_tclAAPManager()
 ***************************************************************************/
spi_tclAAPManager::spi_tclAAPManager() : 
   m_poCmdDiscoverer(NULL), 
   m_poCmdSession(NULL), 
   m_poCmdBluetooth(NULL),
   m_poCmdInput(NULL),
   m_poCmdSensor(NULL),
   m_poCmdVideo(NULL),
   m_poCmdAudio(NULL),
   m_poCmdMediaPlayback(NULL),
   m_poCmdNavigation(NULL),
   m_poCmdNotification(NULL)
{
   ETG_TRACE_USR1(("spi_tclAAPManager::spi_tclAAPManager  Entered \n"));
   m_poCmdDiscoverer = new spi_tclAAPCmdDiscoverer();
   SPI_NORMAL_ASSERT(NULL == m_poCmdDiscoverer);
   
   m_poCmdSession = new spi_tclAAPCmdSession();
   SPI_NORMAL_ASSERT(NULL == m_poCmdSession);
   
   m_poCmdBluetooth = new spi_tclAAPCmdBluetooth();
   SPI_NORMAL_ASSERT(NULL == m_poCmdBluetooth);
   
   m_poCmdInput = new spi_tclAAPCmdInput();
   SPI_NORMAL_ASSERT(NULL == m_poCmdInput);
   
   m_poCmdVideo = new spi_tclAAPCmdVideo();
   SPI_NORMAL_ASSERT(NULL == m_poCmdVideo);
   
   m_poCmdSensor = new spi_tclAAPCmdSensor();
   SPI_NORMAL_ASSERT(NULL == m_poCmdSensor);
   
   m_poCmdAudio = new spi_tclAAPCmdAudio();
   SPI_NORMAL_ASSERT(NULL == m_poCmdAudio);

   m_poCmdMediaPlayback = new spi_tclAAPcmdMediaPlayback();
   SPI_NORMAL_ASSERT(NULL == m_poCmdMediaPlayback);

   m_poCmdNavigation = new spi_tclAAPCmdNavigation();
   SPI_NORMAL_ASSERT(NULL == m_poCmdNavigation);

   m_poCmdNotification = new spi_tclAAPCmdNotification();
   SPI_NORMAL_ASSERT(NULL == m_poCmdNotification);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPManager::~spi_tclAAPManager()
 ***************************************************************************/
spi_tclAAPManager::~spi_tclAAPManager()
{
   ETG_TRACE_USR1(("spi_tclAAPManager::~spi_tclAAPManager  Entered \n"));
   RELEASE_MEM(m_poCmdAudio);
   RELEASE_MEM(m_poCmdVideo);
   RELEASE_MEM(m_poCmdInput);
   RELEASE_MEM(m_poCmdSensor);
   RELEASE_MEM(m_poCmdBluetooth);
   RELEASE_MEM(m_poCmdSession);
   RELEASE_MEM(m_poCmdDiscoverer);
   RELEASE_MEM(m_poCmdMediaPlayback);
   RELEASE_MEM(m_poCmdNavigation);
   RELEASE_MEM(m_poCmdNotification);
}


/***************************************************************************
 ** FUNCTION:   spi_tclAAPCmdDiscoverer* poGetDiscovererInstance()
 ***************************************************************************/
spi_tclAAPCmdDiscoverer* spi_tclAAPManager::poGetDiscovererInstance()
{
   return m_poCmdDiscoverer;
}

/***************************************************************************
 ** FUNCTION:   spi_tclAAPCmdSession* poGetSessionInstance()
 ***************************************************************************/
spi_tclAAPCmdSession* spi_tclAAPManager::poGetSessionInstance()
{
   return m_poCmdSession;
}

/***************************************************************************
 ** FUNCTION:    spi_tclAAPCmdBluetooth* poGetBluetoothInstance()
 ***************************************************************************/
spi_tclAAPCmdBluetooth* spi_tclAAPManager::poGetBluetoothInstance()
{
   return m_poCmdBluetooth;
}

/***************************************************************************
 ** FUNCTION:   spi_tclAAPCmdInput* poGetInputInstance()
 ***************************************************************************/
spi_tclAAPCmdInput* spi_tclAAPManager::poGetInputInstance()
{
   return m_poCmdInput;
}
/***************************************************************************
 ** FUNCTION:    spi_tclAAPCmdVideo* poGetVideoInstance()
 ***************************************************************************/
spi_tclAAPCmdVideo* spi_tclAAPManager::poGetVideoInstance()
{
   return m_poCmdVideo;
}

/***************************************************************************
 ** FUNCTION:   spi_tclAAPCmdInput* poGetSensorInstance()
 ***************************************************************************/
spi_tclAAPCmdSensor* spi_tclAAPManager::poGetSensorInstance()
{
   return m_poCmdSensor;
}

/***************************************************************************
 ** FUNCTION:   spi_tclAAPCmdAudio* poGetAudioInstance()
 ***************************************************************************/
spi_tclAAPCmdAudio* spi_tclAAPManager::poGetAudioInstance()
{
   return m_poCmdAudio;
}

/***************************************************************************
 ** FUNCTION:   spi_tclAAPcmdMediaPlayback* poGetMediaPlaybackInstance()
 ***************************************************************************/
spi_tclAAPcmdMediaPlayback* spi_tclAAPManager::poGetMediaPlaybackInstance()
{
   return m_poCmdMediaPlayback;
}

/***************************************************************************
 ** FUNCTION:   spi_tclAAPCmdNavigation* poGetNavigationInstance()
 ***************************************************************************/
spi_tclAAPCmdNavigation* spi_tclAAPManager::poGetNavigationInstance()
{
   return m_poCmdNavigation;
}

/***************************************************************************
 ** FUNCTION:   spi_tclAAPCmdNotification* poGetNotificationInstance()
 ***************************************************************************/
spi_tclAAPCmdNotification* spi_tclAAPManager::poGetNotificationInstance()
{
   return m_poCmdNotification;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPManager::bRegisterObject(RespBase *poRespReg)
 ***************************************************************************/
t_Bool spi_tclAAPManager::bRegisterObject(RespBase *poRespBase)
{
   ETG_TRACE_USR1(("spi_tclAAPManager::bRegisterObject  Entered \n"));
   RespRegister *pRespRegister = RespRegister::getInstance();
   t_Bool bRetReg = false;
   if(NULL!= pRespRegister)
   {
      bRetReg = pRespRegister->bRegisterObject(poRespBase);
   }
   return bRetReg;
}


/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPManager::bUnRegisterObject(RespBase *poRespReg)
 ***************************************************************************/
t_Bool spi_tclAAPManager::bUnRegisterObject(RespBase *poRespBase)
{
   ETG_TRACE_USR1(("spi_tclAAPManager::bUnRegisterObject  Entered \n"));
   RespRegister *pRespRegister = RespRegister::getInstance();
   t_Bool bRetReg = false;
   if(NULL!= pRespRegister)
   {
      bRetReg = pRespRegister->bUnregisterObject(poRespBase);
   }
   return bRetReg;
}
