
/***********************************************************************/
/*!
* \file   spi_tclAAPVideoDispatcher.h
* \brief  Message Dispatcher for Video Messages. implemented using
 *        double dispatch mechanism
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message Dispatcher for Video Messages
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAAPVIDEODISPATCHER_H_
#define _SPI_TCLAAPVIDEODISPATCHER_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespRegister.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
class spi_tclAAPVideoDispatcher;


/****************************************************************************/
/*!
* \class    AAPVideoMsgBase
* \brief    Base Message type for all
****************************************************************************/
class AAPVideoMsgBase: public trMsgBase
{
public:
   /***************************************************************************
   ** FUNCTION:  AAPVideoMsgBase::AAPVideoMsgBase
   ***************************************************************************/
   /*!
   * \fn      AAPVideoMsgBase()
   * \brief   Default constructor
   **************************************************************************/
   AAPVideoMsgBase();

   /***************************************************************************
   ** FUNCTION:  AAPVideoMsgBase::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher)
   * \brief   Pure virtual function to be overridden by inherited classes for
   *          dispatching the message
   * \param   poDispatcher: pointer to Video Message Dispatcher
   **************************************************************************/
   virtual t_Void vDispatchMsg( spi_tclAAPVideoDispatcher* poDispatcher) = 0;

   /***************************************************************************
   ** FUNCTION:  AAPVideoMsgBase::~AAPVideoMsgBase
   ***************************************************************************/
   /*!
   * \fn      ~AAPVideoMsgBase()
   * \brief   Destructor
   **************************************************************************/
   virtual ~AAPVideoMsgBase(){}

};  //class AAPVideoMsgBase


/****************************************************************************/
/*!
* \class    PlaybackStartMsg
****************************************************************************/
class PlaybackStartMsg: public AAPVideoMsgBase
{
public:
   /***************************************************************************
   ** FUNCTION:  PlaybackStartMsg::PlaybackStartMsg
   ***************************************************************************/
   /*!
   * \fn      PlaybackStartMsg()
   * \brief   Default constructor
   **************************************************************************/
   PlaybackStartMsg(){}

   /***************************************************************************
   ** FUNCTION:  PlaybackStartMsg::~PlaybackStartMsg
   ***************************************************************************/
   /*!
   * \fn      ~PlaybackStartMsg()
   * \brief   PlaybackStartMsg
   **************************************************************************/
   virtual ~PlaybackStartMsg() {}

   /***************************************************************************
   ** FUNCTION:  PlaybackStartMsg::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher)
   * \brief   virtual function for dispatching the message of 'this' type
   * \param   poDispatcher: pointer to Video Message Dispatcher
   **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher);
}; //class PlaybackStartMsg

/****************************************************************************/
/*!
* \class    PlaybackStopMsg
****************************************************************************/
class PlaybackStopMsg: public AAPVideoMsgBase
{
public:
   /***************************************************************************
   ** FUNCTION:  PlaybackStopMsg::PlaybackStopMsg
   ***************************************************************************/
   /*!
   * \fn      PlaybackStopMsg()
   * \brief   Default constructor
   **************************************************************************/
   PlaybackStopMsg(){}

   /***************************************************************************
   ** FUNCTION:  PlaybackStopMsg::~PlaybackStopMsg
   ***************************************************************************/
   /*!
   * \fn      ~PlaybackStopMsg()
   * \brief   PlaybackStopMsg
   **************************************************************************/
   virtual ~PlaybackStopMsg() {}

   /***************************************************************************
   ** FUNCTION:  PlaybackStopMsg::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher)
   * \brief   virtual function for dispatching the message of 'this' type
   * \param   poDispatcher: pointer to Video Message Dispatcher
   **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher);
};//class PlaybackStopMsg

/****************************************************************************/
/*!
* \class    VideoSetupMsg
****************************************************************************/
class VideoSetupMsg: public AAPVideoMsgBase
{
public:
   tenMediaCodecTypes m_enMediaCodecType;

   /***************************************************************************
   ** FUNCTION:  VideoSetupMsg::VideoSetupMsg
   ***************************************************************************/
   /*!
   * \fn      VideoSetupMsg()
   * \brief   Default constructor
   **************************************************************************/
   VideoSetupMsg(): m_enMediaCodecType(e8MEDIA_CODEC_UNKNOWN){}

   /***************************************************************************
   ** FUNCTION:  VideoSetupMsg::~VideoSetupMsg
   ***************************************************************************/
   /*!
   * \fn      ~VideoSetupMsg()
   * \brief   VideoSetupMsg
   **************************************************************************/
   virtual ~VideoSetupMsg() {}

   /***************************************************************************
   ** FUNCTION:  VideoSetupMsg::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher)
   * \brief   virtual function for dispatching the message of 'this' type
   * \param   poDispatcher: pointer to Video Message Dispatcher
   **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher);
};//class VideoSetupMsg

/****************************************************************************/
/*!
* \class    VideoFocusMsg
****************************************************************************/
class VideoFocusMsg: public AAPVideoMsgBase
{
public:

   tenVideoFocus m_enVideoFocus;
   tenVideoFocusReason m_enVideoFocusReason;

   /***************************************************************************
   ** FUNCTION:  AAPVideoFocusMsg::VideoFocusMsg
   ***************************************************************************/
   /*!
   * \fn      VideoFocusMsg()
   * \brief   Default constructor
   **************************************************************************/
   VideoFocusMsg():m_enVideoFocus(e8VIDEOFOCUS_UNKNOWN),
      m_enVideoFocusReason(e8VIDEOFOCUS_REASON_UNKNOWN){}

   /***************************************************************************
   ** FUNCTION:  VideoFocusMsg::~VideoFocusMsg
   ***************************************************************************/
   /*!
   * \fn      ~VideoFocusMsg()
   * \brief   VideoFocusMsg
   **************************************************************************/
   virtual ~VideoFocusMsg() {}

   /***************************************************************************
   ** FUNCTION:  VideoFocusMsg::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher)
   * \brief   virtual function for dispatching the message of 'this' type
   * \param   poDispatcher: pointer to Video Message Dispatcher
   **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher);
}; //class VideoFocusMsg


/****************************************************************************/
/*!
* \class    VideoConfigMsg
****************************************************************************/
class VideoConfigMsg: public AAPVideoMsgBase
{
public:

   t_S32 m_s32LogicalUIWidth;
   t_S32 m_s32LogicalUIHeight;

   /***************************************************************************
   ** FUNCTION:  VideoConfigMsg::VideoConfigMsg
   ***************************************************************************/
   /*!
   * \fn      VideoConfigMsg()
   * \brief   Default constructor
   **************************************************************************/
   VideoConfigMsg():m_s32LogicalUIWidth(0),m_s32LogicalUIHeight(0){}

   /***************************************************************************
   ** FUNCTION:  VideoConfigMsg::~VideoConfigMsg
   ***************************************************************************/
   /*!
   * \fn      ~VideoConfigMsg()
   * \brief   VideoConfigMsg
   **************************************************************************/
   virtual ~VideoConfigMsg() {}

   /***************************************************************************
   ** FUNCTION:  VideoConfigMsg::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(VideoConfigMsg* poDiscDispatcher)
   * \brief   virtual function for dispatching the message of 'this' type
   * \param poDiscDispatcher: pointer to Discoverer Message Dispatcher
   **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPVideoDispatcher* poDispatcher);
}; //class VideoConfigMsg
/****************************************************************************/
/*!
* \class    spi_tclAAPVideoDispatcher
* \brief    Message Dispatcher for Video Messages
****************************************************************************/
class spi_tclAAPVideoDispatcher
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher::spi_tclAAPVideoDispatcher()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideoDispatcher()
   * \brief   Default Constructor
   * \param   t_Void
   * \sa      ~spi_tclAAPVideoDispatcher()
   **************************************************************************/
   spi_tclAAPVideoDispatcher(){}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher::~spi_tclAAPVideoDispatcher()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPVideoDispatcher()
   * \brief   Destructor
   * \param   t_Void
   * \sa      spi_tclAAPVideoDispatcher()
   **************************************************************************/
   ~spi_tclAAPVideoDispatcher(){}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleVideoMsg(PlaybackStartMsg..)
   ***************************************************************************/
   /*!
   * \fn      vHandleVideoMsg(PlaybackStartMsg* poPlaybackStart)
   * \brief   Handles Messages of PlaybackStartMsg type
   * \param   poPlaybackStart :Pointer object of type PlaybackStartMsg
   **************************************************************************/
   t_Void vHandleVideoMsg(PlaybackStartMsg* poPlaybackStart)const;

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleVideoMsg(PlaybackStopMsg..)
   ***************************************************************************/
   /*!
   * \fn      vHandleVideoMsg(PlaybackStopMsg* poPlaybackStop)
   * \brief   Handles Messages of PlaybackStopMsg type
   * \param   poPlaybackStop :Pointer object of type PlaybackStopMsg
   **************************************************************************/
   t_Void vHandleVideoMsg(PlaybackStopMsg* poPlaybackStop)const;

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleVideoMsg(VideoFocusMsg..)
   ***************************************************************************/
   /*!
   * \fn      vHandleVideoMsg(VideoFocusMsg* poVideoFocusMsg)
   * \brief   Handles Messages of VideoFocusMsg type
   * \param   poVideoFocusMsg :Pointer object of type VideoFocusMsg
   **************************************************************************/
   t_Void vHandleVideoMsg(VideoFocusMsg* poVideoFocusMsg)const;

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleDiscoveryMsg(VideoFocusReq..)
   ***************************************************************************/
   /*!
   * \fn      vHandleVideoMsg(VideoSetupMsg* poVideoSetupMsg)
   * \brief   Handles Messages of VideoSetupMsg type
   * \param   poVideoSetupMsg : Pointer object of type VideoSetupMsg
   **************************************************************************/
   t_Void vHandleVideoMsg(VideoSetupMsg* poVideoSetupMsg)const;

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleDiscoveryMsg(VideoConfigMsg..)
   ***************************************************************************/
   /*!
   * \fn      vHandleVideoMsg(VideoConfigMsg* poVideoCfgMsg)
   * \brief   Handles Messages of VideoSetupMsg type
   * \param   poVideoSetupMsg : Pointer object of type VideoSetupMsg
   **************************************************************************/
   t_Void vHandleVideoMsg(VideoConfigMsg* poVideoCfgMsg)const;


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher(const spi_tclAAPVideoDispatcher...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideoDispatcher(
   *                             const spi_tclAAPVideoDispatcher& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPVideoDispatcher()
   ***************************************************************************/
   spi_tclAAPVideoDispatcher(const spi_tclAAPVideoDispatcher& corfoSrc);


   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideoDispatcher& operator=(
   *                          const spi_tclAAPVideoDispatcher& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPVideoDispatcher(const spi_tclAAPVideoDispatcher& otrSrc)
   ***************************************************************************/
   spi_tclAAPVideoDispatcher& operator=(const spi_tclAAPVideoDispatcher& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/


}; //class spi_tclAAPVideoDispatcher



#endif //_SPI_TCLAAPVIDEODISPATCHER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
