
/***********************************************************************/
/*!
* \file  spi_tclAAPCmdVideo.h
* \brief Wrapper class for GalReceiver Video Sink
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Wrapper class for GalReceiver Video Sink
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPCMDVIDEO_H_
#define _SPI_TCLAAPCMDVIDEO_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <aauto/GalReceiver.h>
#include "aauto_gstreamer.h"
#include "aauto/GstreamerVideoSink.h"
#include "SPITypes.h"
#include "AAPTypes.h"

using namespace adit::aauto;



/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class    spi_tclAAPCmdVideo
* \brief    Wrapper class to interact with Google & ADIT API's.
*           It is responsible to create Video Sink and handle ADITVideoSink Callbacks
****************************************************************************/
class spi_tclAAPCmdVideo: public IAditVideoSinkCallbacks
{

public:

    /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPCmdVideo::spi_tclAAPCmdVideo()
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPCmdVideo()
    * \brief   Default Constructor
    * \sa      ~spi_tclAAPCmdVideo()
    **************************************************************************/
    spi_tclAAPCmdVideo();

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPCmdVideo::~spi_tclAAPCmdVideo()
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclAAPCmdVideo()
    * \brief   Destructor
    * \sa      spi_tclAAPCmdVideo()
    **************************************************************************/
    ~spi_tclAAPCmdVideo();

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPCmdVideo::bInitialize()
    ***************************************************************************/
    /*!
    * \fn      t_Bool bInitialize(const trAAPVideoConfig& corfrAAPVideoConfig)
    * \brief   method to create and initialize video sink
    * \param   corfrAAPVideoConfig : [IN] Video configuration
    * \sa      vUninitialize()
    * \retval  t_Bool
    **************************************************************************/
    t_Bool bInitialize(const trAAPVideoConfig& corfrAAPVideoConfig);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdVideo::vUninitialize()
    ***************************************************************************/
    /*!
    * \fn      t_Void vUninitialize()
    * \brief   method to un initialize video sink
    * \sa      bInitialize()
    * \retval  t_Void
    **************************************************************************/
    t_Void vUninitialize();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdVideo::vSetVideoFocus()
    ***************************************************************************/
    /*!
    * \fn      t_Void vSetVideoFocus(tenVideoFocus enVideoFocus,
    *          t_Bool bUnsolicited)
    * \brief   method to request or Grant/Deny Video Focus to/from MD
    * \param   enVideoFocus : [IN] Video Focus mode
    * \param   bUnsolicited : TRUE - if it is response for Phone's request
    *                         FALSE - If SPI triggers it
    * \sa      playbackStartCallback(),playbackStopCallback(),videoFocusCallback()
    * \retval  t_Void
    **************************************************************************/
    t_Void vSetVideoFocus(tenVideoFocus enVideoFocus,
       t_Bool bUnsolicited);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdVideo::playbackStartCallback()
    ***************************************************************************/
    /*!
    * \fn      t_Void playbackStartCallback()
    * \brief   method to inform the application that the Video Play back is started
    * \retval  t_Void
    **************************************************************************/
    t_Void playbackStartCallback();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdVideo::playbackStopCallback()
    ***************************************************************************/
    /*!
    * \fn      t_Void playbackStopCallback()
    * \brief   method to inform the application that the Video Play back is stopped
    * \retval  t_Void
    **************************************************************************/
    t_Void playbackStopCallback();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdVideo::videoFocusCallback()
    ***************************************************************************/
    /*!
    * \fn      t_Void videoFocusCallback(t_S32 s32Focus, t_S32 s32Reason)
    * \brief   Method to inform the application that the Video Focus is requested
    *          or rejected from the MD
    * \param   s32Focus  : [IN] Video Focus Mode
    * \param   s32Reason : [IN] Video Focus Reason
    * \retval  t_Void
    **************************************************************************/
    t_Void videoFocusCallback(t_S32 s32Focus, t_S32 s32Reason);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdVideo::setupCallback(t_S32 s32MediaCodecType)
    ***************************************************************************/
    /*!
    * \fn      t_Void setupCallback(t_S32 s32MediaCodecType)
    * \brief   Signals that the MD wants to set up a video stream
    *          and the information in this callback should be used
    *          to set up any hardware necessary to handle the incoming stream type.
    * \param   s32MediaCodecType  : [IN] Media Codec Type
    *          (Currently, the only valid value is MEDIA_CODEC_VIDEO_H264_BP)
    * \retval  t_Void
    **************************************************************************/
    t_Void setupCallback(t_S32 s32MediaCodecType);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdVideo::setupCallback(t_S32 s32MediaCodecType)
    ***************************************************************************/
    /*!
    * \fn      t_Void setupCallback(t_S32 s32MediaCodecType)
    * \brief   Method to inform when MD sends video configuration. This configuration can be
    *          different from what is originally set by setConfigItem
    * \param   s32VideoWidth   : [IN] The width of codec resolution selected.
    * \param   s32VideoHeight  : [IN] The height of codec resolution selected.
    * \param   s32UIResWidth   : [IN] The width of the logical UI resolution.
    * \param   s32UIResHeight  : [IN] The height of the logical UI resolution.
    * \retval  t_Void
    **************************************************************************/
    t_Void sourceVideoConfigCallback(t_S32 s32VideoWidth, t_S32 s32VideoHeight,
             t_S32 s32UIResWidth, t_S32 s32UIResHeight);

    /***************************************************************************
    ****************************END OF PUBLIC***********************************
    ***************************************************************************/

protected:

    /***************************************************************************
    *********************************PROTECTED**********************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPCmdVideo(const spi_tclAAPCmdVideo...
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPCmdVideo(const spi_tclAAPCmdVideo& corfoSrc)
    * \brief   Copy constructor - Do not allow the creation of copy constructor
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPCmdVideo()
    ***************************************************************************/
    spi_tclAAPCmdVideo(const spi_tclAAPCmdVideo& corfoSrc);


    /***************************************************************************
    ** FUNCTION:  spi_tclAAPCmdVideo& operator=( const spi_tclAAP...
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPCmdVideo& operator=(const spi_tclAAPCmdVideo& corfoSrc))
    * \brief   Assignment operator
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPCmdVideo(const spi_tclAAPCmdVideo& otrSrc)
    ***************************************************************************/
    spi_tclAAPCmdVideo& operator=(const spi_tclAAPCmdVideo& corfoSrc);


    /***************************************************************************
    ****************************END OF PROTECTED********************************
    ***************************************************************************/

private:

    /***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdVideo::vSetVideoConfig()
    ***************************************************************************/
    /*!
    * \fn      t_Void vSetVideoConfig(const trAAPVideoConfig& corfrAAPVideoConfig)
    * \brief   method to set the video configuration for Video Sink
    * \param   corfrAAPVideoConfig : [IN] Video configuration
    * \retval  t_Void
    **************************************************************************/
    t_Void vSetVideoConfig(const trAAPVideoConfig& corfrAAPVideoConfig);

    //! Member variable for ADIT Video Sink
    AditVideoSink* m_poVideoSink;

    /***************************************************************************
    ****************************END OF PRIVATE *********************************
    ***************************************************************************/


}; //class spi_tclAAPCmdVideo

#endif //_SPI_TCLAAPCMDVIDEO_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
