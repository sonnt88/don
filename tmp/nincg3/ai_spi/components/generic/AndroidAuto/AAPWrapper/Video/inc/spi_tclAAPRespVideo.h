
/***********************************************************************/
/*!
* \file   spi_tclAAPRespVideo.h
* \brief  VideoSink Call backs output interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    VideoSink Call backs output interface
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAAPRESPVIDEO_H_
#define _SPI_TCLAAPRESPVIDEO_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "RespBase.h"
#include "AAPTypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class  spi_tclAAPRespVideo.h
* \brief  VideoSink Call backs output interface
*         IAditVideoSinkCallbacks are updated to Application using this
*****************************************************************************/
class spi_tclAAPRespVideo:public RespBase
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespVideo::spi_tclAAPRespVideo()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespVideo()
   * \brief   Default Constructor
   * \sa      ~spi_tclAAPRespVideo()
   **************************************************************************/
   spi_tclAAPRespVideo():RespBase(e16AAP_VIDEO_REGID){}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespVideo::~spi_tclAAPRespVideo()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPRespVideo()
   * \brief   Destructor
   * \sa      spi_tclAAPRespVideo()
   **************************************************************************/
   virtual ~spi_tclAAPRespVideo(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespVideo::vPlaybackStartCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPlaybackStartCallback()
   * \brief   method to update that the Video Play back is started
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vPlaybackStartCallback(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespVideo::vPlaybackStopCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vPlaybackStopCallback()
   * \brief   method to update that the Video Play back is stopped
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vPlaybackStopCallback(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespVideo::vVideoFocusCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vVideoFocusCallback(tenVideoFocus enVideoFocus,
   *                 tenVideoFocusReason enVideoFocusReason)
   * \brief   Method to update that the Video Focus is requested
   *          or rejected from the MD
   * \param   enVideoFocus         : [IN] Video Focus Mode
   * \param   enVideoFocusReason   : [IN] Video Focus Reason
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vVideoFocusCallback(tenVideoFocus enVideoFocus,
      tenVideoFocusReason enVideoFocusReason){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespVideo::vVideoSetupCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vVideoSetupCallback(
   *                 tenVideoFocus enReqVideoFocus)
   * \brief   Method to update video setup request received from MD
   * \param   enReqVideoFocus : [IN] Video Focus Mode
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vVideoSetupCallback(tenMediaCodecTypes enMediaCodecType){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespVideo::vVideoConfigCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vVideoConfigCallback()
   * \brief   Method to update video configuration received from MD
   * \param   s32LogicalUIWidth : [IN] Logical UI width
   * \param   s32LogicalUIHeight: [IN] Logical UI Height
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vVideoConfigCallback(t_S32 s32LogicalUIWidth, t_S32 s32LogicalUIHeight){}

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespVideo(const spi_tclAAPRespVideo...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespVideo(const spi_tclAAPRespVideo& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespVideo()
   ***************************************************************************/
   spi_tclAAPRespVideo(const spi_tclAAPRespVideo& corfoSrc);


   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespVideo& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespVideo& operator=(const spi_tclAAPRespVideo& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespVideo(const spi_tclAAPRespVideo& otrSrc)
   ***************************************************************************/
   spi_tclAAPRespVideo& operator=(const spi_tclAAPRespVideo& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/


}; //class spi_tclAAPRespVideo

#endif //_SPI_TCLAAPRESPVIDEO_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
