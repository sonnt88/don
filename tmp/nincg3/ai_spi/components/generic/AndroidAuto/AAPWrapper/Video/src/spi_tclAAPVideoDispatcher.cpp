
/***********************************************************************/
/*!
* \file    spi_tclAAPVideoDispatcher.cpp
* \brief   Message Dispatcher for Video Messages
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message Dispatcher for Video Messages
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shiva Kumar Gurija    | Initial Version
21.08.2015  | Sameer Chandra        | Added new video Config message
\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclAAPRespVideo.h"
#include "spi_tclAAPVideoDispatcher.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclAAPVideoDispatcher.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(DISPATCHER* poDispatcher)      \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleVideoMsg(this);                  \
   }                                                        \
}

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  AAPVideoMsgBase::AAPVideoMsgBase
***************************************************************************/
AAPVideoMsgBase::AAPVideoMsgBase()
{
   ETG_TRACE_USR1(("AAPVideoMsgBase() entered "));
   vSetServiceID(e32MODULEID_AAPVIDEO);
}

/***************************************************************************
** FUNCTION:  PlaybackStartMsg::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(PlaybackStartMsg, spi_tclAAPVideoDispatcher);


/***************************************************************************
** FUNCTION:  PlaybackStopMsg::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(PlaybackStopMsg, spi_tclAAPVideoDispatcher);

/***************************************************************************
** FUNCTION:  VideoFocusMsg::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(VideoFocusMsg, spi_tclAAPVideoDispatcher);

/***************************************************************************
** FUNCTION:  VideoSetupMsg::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(VideoSetupMsg, spi_tclAAPVideoDispatcher);

/***************************************************************************
** FUNCTION:  VideoSetupMsg::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(VideoConfigMsg, spi_tclAAPVideoDispatcher);


/***************************************************************************
** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleVideoMsg(PlaybackStartMsg..)
***************************************************************************/
t_Void spi_tclAAPVideoDispatcher::vHandleVideoMsg(PlaybackStartMsg* poPlaybackStart)const
{
   ETG_TRACE_USR1(("PlaybackStartMsgDispatcher"));
   if (NULL != poPlaybackStart)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespVideo,
         e16AAP_VIDEO_REGID,
         vPlaybackStartCallback());
   } // if (NULL != poPlaybackStart)
}

/***************************************************************************
** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleVideoMsg(PlaybackStopMsg..)
***************************************************************************/
t_Void spi_tclAAPVideoDispatcher::vHandleVideoMsg(PlaybackStopMsg* poPlaybackStop)const
{
   ETG_TRACE_USR1(("PlaybackStopMsgoDispatcher"));
   if (NULL != poPlaybackStop)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespVideo,
         e16AAP_VIDEO_REGID,
         vPlaybackStopCallback());
   } // if (NULL != poPlaybackStop)
}

/***************************************************************************
** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleVideoMsg(VideoFocusMsg..)
***************************************************************************/
t_Void spi_tclAAPVideoDispatcher::vHandleVideoMsg(VideoFocusMsg* poVideoFocusMsg)const
{
   ETG_TRACE_USR1(("VideoFocusMsgDispatcher"));
   if (NULL != poVideoFocusMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespVideo,
         e16AAP_VIDEO_REGID,
         vVideoFocusCallback(poVideoFocusMsg->m_enVideoFocus,
         poVideoFocusMsg->m_enVideoFocusReason));
   } // if (NULL != poVideoFocusMsg)
}

/***************************************************************************
** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleVideoMsg(VideoSetupMsg..)
***************************************************************************/
t_Void spi_tclAAPVideoDispatcher::vHandleVideoMsg(VideoSetupMsg* poVideoSetupMsg)const
{
   ETG_TRACE_USR1(("VideoSetupMsg Dispatcher"));
   if (NULL != poVideoSetupMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespVideo,
         e16AAP_VIDEO_REGID,
         vVideoSetupCallback(poVideoSetupMsg->m_enMediaCodecType));
   } // if (NULL != poVideoSetupMsg)
}

/***************************************************************************
** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleVideoMsg(VideoConfigMsg..)
***************************************************************************/
t_Void spi_tclAAPVideoDispatcher::vHandleVideoMsg(VideoConfigMsg* poVideoCfgMsg)const
{
   ETG_TRACE_USR1(("VideoSetupMsg Dispatcher"));
   if (NULL != poVideoCfgMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespVideo,
         e16AAP_VIDEO_REGID,
         vVideoConfigCallback(poVideoCfgMsg->m_s32LogicalUIWidth,poVideoCfgMsg->m_s32LogicalUIHeight));
   } // if (NULL != poVideoCfgMsg)
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
