/***********************************************************************/
/*!
* \file   spi_tclAAPCmdVideo.cpp
* \brief  Wrapper class for GalReceiver Video Sink
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Wrapper class for GalReceiver Video Sink
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date         | Author                        | Modification
20.03.2015   | Shiva Kumar Gurija            | Initial Version
28.05.2015   | Tejaswini H B(RBEI/ECP2)      | Added Lint comments to suppress C++11 Errors
11.07.2015   | Ramya Murthy                  | Fix for same session ID being used for multiple Endpoints

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
//#include "AautoLogging.h"
#include "StringHandler.h"
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPSessionDataIntf.h"
#include "spi_tclAAPVideoDispatcher.h"
#include "spi_tclAAPCmdVideo.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclAAPCmdVideo.cpp.trc.h"
#endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define MAX_KEYSIZE 256

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_String cszVideoCodecKey = "video-codec";
static const t_String cszMaxUnAckedFramesKey = "video-maxUnackedFrames";
static const t_String cszScreenWidthKey = "video-width";
static const t_String cszScreenHeightKey = "video-height";
static const t_String cszDpiDensityKey = "video-density";
static const t_String cszFpsKey = "video-fps";
static const t_String cszVideoPipelineKey = "gstreamer-video-pipeline";
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
** FUNCTION:  spi_tclAAPCmdVideo::spi_tclAAPCmdVideo()
***************************************************************************/
spi_tclAAPCmdVideo::spi_tclAAPCmdVideo() : m_poVideoSink(NULL)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::spi_tclAAPCmdVideo() entered "));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdVideo::~spi_tclAAPCmdVideo()
***************************************************************************/
spi_tclAAPCmdVideo::~spi_tclAAPCmdVideo()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::~spi_tclAAPCmdVideo() entered "));
   m_poVideoSink = NULL;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPCmdVideo::bInitialize()
***************************************************************************/
t_Bool spi_tclAAPCmdVideo::bInitialize(const trAAPVideoConfig& corfrAAPVideoConfig)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::bInitialize: UnAckedFrames-%d, ScreenWidth-%d screenHeight-%d " \
      "DpiDensity-%d Fps-%d Autostartprojection-%d codecType-%s ",corfrAAPVideoConfig.u8MaxUnAckedFrames,
      corfrAAPVideoConfig.u32ScreenWidth,corfrAAPVideoConfig.u32ScreenHeight,
      corfrAAPVideoConfig.u16DpiDensity,corfrAAPVideoConfig.u8Fps,
      corfrAAPVideoConfig.bAutoStartProjection,
      corfrAAPVideoConfig.szVideoCodec.c_str()));

   //! load the library: enables ADIT logging for video
   adit::aauto::GstreamerEntryPoint(NULL);

   t_Bool bRet = false;

   //Get the GalReceiver using session data intf
   spi_tclAAPSessionDataIntf oSessionDataIntf;
   shared_ptr<GalReceiver> spGalRcvr = oSessionDataIntf.poGetGalReceiver();
   /*lint -esym(40,nullptr) nullptr is not declared */
   if(spGalRcvr != nullptr)
   {
      //Create GstreamerVideoSink object for Video Session
      m_poVideoSink = new (std::nothrow)GstreamerVideoSink(e32SESSIONID_AAPVIDEO,
         spGalRcvr->messageRouter(),corfrAAPVideoConfig.bAutoStartProjection);

      if(NULL !=m_poVideoSink)
      {
         vSetVideoConfig(corfrAAPVideoConfig);  

         bRet = m_poVideoSink->init();

         // register for Video Sink Service with GalReciever
         if (true == bRet)
         {
            if(true == spGalRcvr->registerService(m_poVideoSink))
            {
               bRet = true;
            }//if(true == spGalRcvr->registerServic
            else
            {
               ETG_TRACE_ERR(("spi_tclAAPCmdVideo::bInitialize - Error in registering for Service"));
            }
         }//if(true == bRet)
         else
         {
            ETG_TRACE_ERR(("spi_tclAAPCmdVideo::bInitialize - error in initializing Video Sink "));
         }
      }//if(NULL != m_poVideoSink)
      else
      {
         ETG_TRACE_ERR(("spi_tclAAPCmdVideo::bInitialize - m_poVideoSink is NULL "));
      }
   } //if(spGalRcvr != nullptr)

   ETG_TRACE_USR4(("spi_tclAAPCmdVideo::bInitialize left with %d ", ETG_ENUM(BOOL, bRet)));

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdVideo::vUninitialize()
***************************************************************************/
t_Void spi_tclAAPCmdVideo::vUninitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::vUninitialize() entered "));
   //add code
   if(NULL != m_poVideoSink)
   {
      m_poVideoSink->shutdown();
   }
   RELEASE_MEM(m_poVideoSink); 

   //! Unload the library : disables ADIT logging for video
   adit::aauto::GstreamerExitPoint();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdVideo::vSetVideoConfig()
***************************************************************************/
t_Void spi_tclAAPCmdVideo::vSetVideoConfig(const trAAPVideoConfig& corfrAAPVideoConfig)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::vSetVideoConfig() "));

   if (NULL != m_poVideoSink)
   {
      StringHandler oStrUtil;

      //Sets media codec type which needs to be set.In Galreceiver1.0 only �MEDIA_CODEC_VIDEO_H264_BP� is available
      ETG_TRACE_USR4((" VideoCodec:%s",corfrAAPVideoConfig.szVideoCodec.c_str()));
      m_poVideoSink->setConfigItem(cszVideoCodecKey.c_str(),corfrAAPVideoConfig.szVideoCodec.c_str());

      //Sets how many frames should be kept outstanding.
      //Once this limit is hit the other side will stop sending new video frames
      t_String szMaxUnAckedFrames;
      oStrUtil.vConvertIntToStr(corfrAAPVideoConfig.u8MaxUnAckedFrames,szMaxUnAckedFrames,DECIMAL_STRING);
      ETG_TRACE_USR4(("MaxUnAckedFrames:%s",szMaxUnAckedFrames.c_str()));
      m_poVideoSink->setConfigItem(cszMaxUnAckedFramesKey.c_str(),szMaxUnAckedFrames.c_str());


      //Sets the Screen width in pixels (X-Axis)
      t_String szScreenWidth;
      oStrUtil.vConvertIntToStr(corfrAAPVideoConfig.u32ScreenWidth,szScreenWidth,DECIMAL_STRING);
      ETG_TRACE_USR4(("ScreenWidth:%s",szScreenWidth.c_str()));
      m_poVideoSink->setConfigItem(cszScreenWidthKey.c_str(),szScreenWidth.c_str());


      //Sets the Screen Height in pixels (Y-Axis)
      t_String szScreenHeight;
      oStrUtil.vConvertIntToStr(corfrAAPVideoConfig.u32ScreenHeight,szScreenHeight,DECIMAL_STRING);
      ETG_TRACE_USR4(("ScreenHeight:%s",szScreenHeight.c_str()));
      m_poVideoSink->setConfigItem(cszScreenHeightKey.c_str(),szScreenHeight.c_str());


      //Sets the Pixel Density in dpi
      t_String szDpiDensity;
      oStrUtil.vConvertIntToStr(corfrAAPVideoConfig.u16DpiDensity,szDpiDensity,DECIMAL_STRING);
      ETG_TRACE_USR4(("DpiDensity:%s",szDpiDensity.c_str()));
      m_poVideoSink->setConfigItem(cszDpiDensityKey.c_str(),szDpiDensity.c_str());


      //Sets the Frame rate of Video - imx6 supports only 30fps
      t_String szFps;
      oStrUtil.vConvertIntToStr(corfrAAPVideoConfig.u8Fps,szFps,DECIMAL_STRING);
      ETG_TRACE_USR4(("Fps:%s",szFps.c_str()));
      m_poVideoSink->setConfigItem(cszFpsKey.c_str(),szFps.c_str());


      //prepare GstreamerPipeline appsrc element value
      t_Char szVideoPipeline[MAX_KEYSIZE]={0};
      snprintf(szVideoPipeline,MAX_KEYSIZE,
         "vpudec low-latency=true frame-plus=2 framedrop=false framerate-nu=%d dis-reorder=true ! gst_apx_sink display-width=%d display-height=%d layer-id=%d surface-id=%d sync=false qos=false max-lateness=3000000000 force-aspect-ratio=true",
         corfrAAPVideoConfig.u8Fps,corfrAAPVideoConfig.u32ScreenWidth,
         corfrAAPVideoConfig.u32ScreenHeight,corfrAAPVideoConfig.u16LayerID,
         corfrAAPVideoConfig.u16SurfaceID);

      ETG_TRACE_USR4(("VideoPipeline:%s",szVideoPipeline));
      //Sets the GstreamerPipeline appsrc element
      m_poVideoSink->setConfigItem(cszVideoPipelineKey.c_str(),
         szVideoPipeline);
  

      //register for IAditVideoSinkCallbacks callbacks
      m_poVideoSink->registerCallbacks(this);

   }//if (NULL != m_poVideoSink)
   else
   {
      ETG_TRACE_ERR(("spi_tclAAPCmdVideo::vSetVideoConfig- Video Sink is NULL"));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdVideo::vSetVideoFocus()
***************************************************************************/
t_Void spi_tclAAPCmdVideo::vSetVideoFocus(tenVideoFocus enVideoFocus,
                                          t_Bool bUnsolicited)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::vSetVideoFocus:VideoFocusMode-%d Unsolicited-%d ",
		   ETG_ENUM(VIDEOFOCUS_MODE,enVideoFocus),ETG_ENUM(BOOL,bUnsolicited)));

   if (NULL != m_poVideoSink)
   {
      t_S32 s32VideoFocus = static_cast<t_S32> (enVideoFocus);
      m_poVideoSink->setVideoFocus(s32VideoFocus,bUnsolicited);
   }//if (NULL != m_poVideoSink)
   else
   {
      ETG_TRACE_ERR(("spi_tclAAPCmdVideo::vSetVideoFocus:VideoSink is NULL"));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdVideo::playbackStartCallback()
***************************************************************************/
t_Void spi_tclAAPCmdVideo::playbackStartCallback()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::playbackStartCallback() entered "));

   PlaybackStartMsg oPlaybackStartMsg;
   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oPlaybackStartMsg, sizeof(oPlaybackStartMsg));
   }//if (NULL != poMsgQinterface) 
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdVideo::playbackStopCallback()
***************************************************************************/
t_Void spi_tclAAPCmdVideo::playbackStopCallback()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::playbackStopCallback() entered "));

   PlaybackStopMsg oPlaybackStopMsg;
   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oPlaybackStopMsg, sizeof(oPlaybackStopMsg));
   }//if (NULL != poMsgQinterface) 
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdVideo::setupCallback(t_S32 s32MediaCodecType)
***************************************************************************/
t_Void spi_tclAAPCmdVideo::setupCallback(t_S32 s32MediaCodecType)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::setupCallback() entered MediaCodecType - %d",
            ETG_ENUM(MEDIACODEC_TYPE, s32MediaCodecType)));

   VideoSetupMsg oVideoSetupMsg;
   oVideoSetupMsg.m_enMediaCodecType = static_cast<tenMediaCodecTypes>(s32MediaCodecType);

   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oVideoSetupMsg, sizeof(oVideoSetupMsg));
   }//if (NULL != poMsgQinterface)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdVideo::videoFocusCallback()
***************************************************************************/
t_Void spi_tclAAPCmdVideo::videoFocusCallback(t_S32 s32Focus, t_S32 s32Reason)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::videoFocusCallback: FocusMode-%d, FocusReason-%d ",
      ETG_ENUM(VIDEOFOCUS_MODE,s32Focus),ETG_ENUM(VIDEOFOCUS_REASON,s32Reason)));

   VideoFocusMsg oVideoFocusMsg;
   oVideoFocusMsg.m_enVideoFocus = static_cast<tenVideoFocus>(s32Focus);
   oVideoFocusMsg.m_enVideoFocusReason = static_cast<tenVideoFocusReason>(s32Reason);

   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oVideoFocusMsg, sizeof(oVideoFocusMsg));
   }//if (NULL != poMsgQinterface)  
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdVideo::sourceVideoConfigCallback()
***************************************************************************/
t_Void spi_tclAAPCmdVideo::sourceVideoConfigCallback(t_S32 s32VideoWidth, t_S32 s32VideoHeight,
         t_S32 s32UIResWidth, t_S32 s32UIResHeight)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdVideo::sourceVideoConfigCallback: Video Width-%d, Video Height - %d , "
            "UI resolution width - %d, UI resolution height - %d",
            s32VideoWidth, s32VideoHeight, s32UIResWidth, s32UIResHeight));

   VideoConfigMsg oVideoConfigMsg;
   oVideoConfigMsg.m_s32LogicalUIHeight = s32UIResHeight;
   oVideoConfigMsg.m_s32LogicalUIWidth  = s32UIResWidth;

   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      poMsgQinterface->bWriteMsgToQ(&oVideoConfigMsg, sizeof(oVideoConfigMsg));
   }//if (NULL != poMsgQinterface)
}
//lint �restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
