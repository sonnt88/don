/*!
 *******************************************************************************
 * \file              spi_tclAAPBluetoothCbs.cpp
 * \brief             BT Endpoint callbacks handler for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    BT Endpoint callbacks handler for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 18.03.2015 |  Ramya Murthy                | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPBTDispatcher.h"
#include "spi_tclAAPBluetoothCbs.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclAAPBluetoothCbs.cpp.trc.h"
#endif
#endif


/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetoothCbs::onPairingRequest(...)
***************************************************************************/
void spi_tclAAPBluetoothCbs::onPairingRequest(const string& rfcoszAAPMacAddress,
                                  int s32PairingMethod)
{
   ETG_TRACE_USR1(("spi_tclAAPBluetoothCbs::onPairingRequest() entered: "
         "PairingMethod = %d, AAPMacAddress = %s ",
         s32PairingMethod, rfcoszAAPMacAddress.c_str()));

   spi_tclAAPMsgQInterface* poMsgQInterface = spi_tclAAPMsgQInterface::getInstance();
   if ((NULL != poMsgQInterface) && (0 < s32PairingMethod) && (false == rfcoszAAPMacAddress.empty()))
   {
      AAPBTPairingRequestMsg oPairingReqMsg;
      if (NULL != oPairingReqMsg.poszAAPBTAddress)
      {
         t_String szAAPBTAddr = szConvertToBTAddress(rfcoszAAPMacAddress).c_str();
         ETG_TRACE_USR1(("onPairingRequest(): AAPBTAddress = %s ", szAAPBTAddr.c_str()));
         *(oPairingReqMsg.poszAAPBTAddress) = szAAPBTAddr;
      }
      oPairingReqMsg.enAAPPairingMethod = static_cast<tenBTPairingMethod>(s32PairingMethod);
      poMsgQInterface->bWriteMsgToQ(&oPairingReqMsg, sizeof(oPairingReqMsg));
   } //if ((NULL != poMsgQInterface) && (0 < s32PairingMethod))
   else if (0 >= s32PairingMethod)
   {
      ETG_TRACE_ERR(("onPairingRequest: Invalid Pairing method! "));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPBluetoothCbs::szConvertToBTAddress(...)
***************************************************************************/
t_String spi_tclAAPBluetoothCbs::szConvertToBTAddress(const t_String& rfcoszBTMACAddress)
{
   //! Initialize string with the BT MAC address
   t_String szBTAddress = rfcoszBTMACAddress.c_str();
   //! Remove all instances of ':' character from string and convert to uppercase.
   //! (Example: If szBTMACAddress is "28:e1:4c:df:30:72", format string as "28E14CDF3072"
   if (false == szBTAddress.empty())
   {
      szBTAddress.erase(std::remove(szBTAddress.begin(), szBTAddress.end(), ':'), szBTAddress.end());
      std::transform(szBTAddress.begin(), szBTAddress.end(), szBTAddress.begin(), ::toupper);
   }
   return szBTAddress;
}

