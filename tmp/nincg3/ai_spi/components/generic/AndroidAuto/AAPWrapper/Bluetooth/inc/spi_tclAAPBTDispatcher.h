/***********************************************************************/
/*!
 * \file  spi_tclAAPBTDispatcher.h
 * \brief Message Dispatcher for Bluetooth Messages. implemented using
 *        double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Bluetooth Messages
 AUTHOR:         Ramya Murthy
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 10.03.2015  | Ramya Murthy          | Initial Version

 \endverbatim
 *************************************************************************/
#ifndef _SPI_TCLAAPBTDISPATCHER_H_
#define _SPI_TCLAAPBTDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespRegister.h"

/**************Forward Declarations******************************************/
class spi_tclAAPBTDispatcher;

/****************************************************************************/
/*!
 * \class AAPBTMsgBase
 * \brief Base Message type for all BT messages
 ****************************************************************************/
class AAPBTMsgBase: public trMsgBase
{
   public:

   /***************************************************************************
    ** FUNCTION:  AAPBTMsgBase::AAPBTMsgBase
    ***************************************************************************/
   /*!
    * \fn      AAPBTMsgBase()
    * \brief   Default constructor
    **************************************************************************/
   AAPBTMsgBase();

   /***************************************************************************
    ** FUNCTION:  AAPBTMsgBase::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPBTDispatcher* poBTDispatcher)
    * \brief   Pure virtual function to be overridden by inherited classes for
    *          dispatching the message
    * \param   poBTDispatcher : pointer to Message dispatcher for BT
    **************************************************************************/
   virtual t_Void vDispatchMsg(spi_tclAAPBTDispatcher* poBTDispatcher) = 0;

   /***************************************************************************
    ** FUNCTION:  AAPBTMsgBase::~AAPBTMsgBase
    ***************************************************************************/
   /*!
    * \fn      ~AAPBTMsgBase()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPBTMsgBase()
   {

   }
};

/****************************************************************************/
/*!
 * \class AAPBTPairingRequestMsg
 * \brief BT Pairing Request msg
 ****************************************************************************/
class AAPBTPairingRequestMsg: public AAPBTMsgBase
{
   public:

   t_String* poszAAPBTAddress;
   tenBTPairingMethod enAAPPairingMethod;

   /***************************************************************************
    ** FUNCTION:  AAPBTPairingRequestMsg::AAPBTPairingRequestMsg
    ***************************************************************************/
   /*!
    * \fn      AAPBTPairingRequestMsg()
    * \brief   Default constructor
    **************************************************************************/
   AAPBTPairingRequestMsg();

   /***************************************************************************
    ** FUNCTION:  AAPBTPairingRequestMsg::~AAPBTPairingRequestMsg
    ***************************************************************************/
   /*!
    * \fn      ~AAPBTPairingRequestMsg()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPBTPairingRequestMsg(){}

   /***************************************************************************
    ** FUNCTION:  AAPBTPairingRequestMsg::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPBTDispatcher* poBTDispatcher)
    * \brief   virtual function for dispatching the message of 'this' type
    * \param   poBTDispatcher : pointer to Message dispatcher for BT
    **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPBTDispatcher* poBTDispatcher);

   /***************************************************************************
    ** FUNCTION:  AAPBTPairingRequestMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg();

   /***************************************************************************
    ** FUNCTION:  AAPBTPairingRequestMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg();
};


/****************************************************************************/
/*!
 * \class spi_tclAAPBTDispatcher
 * \brief Message Dispatcher for BT Messages
 ****************************************************************************/
class spi_tclAAPBTDispatcher
{
   public:
   /***************************************************************************
    ** FUNCTION:  spi_tclAAPBTDispatcher::spi_tclAAPBTDispatcher
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPBTDispatcher()
    * \brief   Default constructor
    **************************************************************************/
   spi_tclAAPBTDispatcher();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPBTDispatcher::~spi_tclAAPBTDispatcher
    ***************************************************************************/
   /*!
    * \fn      ~spi_tclAAPBTDispatcher()
    * \brief   Destructor
    **************************************************************************/
   ~spi_tclAAPBTDispatcher();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPBTDispatcher::vHandleBTMsg(AAPBTPairingRequestMsg*...)
    ***************************************************************************/
   /*!
    * \fn      vHandleBTMsg(AAPBTPairingRequestMsg* poBTPairingRequest)
    * \brief   Handles Messages of AAPBTPairingRequestMsg type
    * \param   poBTPairingRequest : pointer to AAPBTPairingRequestMsg.
    **************************************************************************/
   t_Void vHandleBTMsg(AAPBTPairingRequestMsg* poBTPairingRequest) const;

};

#endif /* _SPI_TCLAAPBTDISPATCHER_H_ */
