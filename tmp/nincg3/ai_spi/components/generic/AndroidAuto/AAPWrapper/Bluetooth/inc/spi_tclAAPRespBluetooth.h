/*!
 *******************************************************************************
 * \file              spi_tclAAPRespBluetooth.h
 * \brief             AAP Wrapper Response class for Bluetooth
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:   AAP Wrapper Response class for Bluetooth
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author               | Modifications
10.03.2015  | Ramya Murthy          | Initial Version

 \endverbatim
 ******************************************************************************/
#ifndef _SPI_TCLAAPRESPBLUETOOTH_H_
#define _SPI_TCLAAPRESPBLUETOOTH_H_

#include "AAPTypes.h"
#include "RespBase.h"

class spi_tclAAPRespBluetooth: public RespBase
{

public:

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclAAPRespBluetooth::spi_tclAAPRespBluetooth()
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPRespBluetooth()
    * \brief   Constructor
    * \sa      spi_tclAAPRespBluetooth()
    **************************************************************************/
   spi_tclAAPRespBluetooth(): RespBase(e16AAP_BT_REGID) {}

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclAAPRespBluetooth::~spi_tclAAPRespBluetooth()
    ***************************************************************************/
   /*!
    * \fn      virtual ~spi_tclAAPRespBluetooth()
    * \brief   Destructor
    * \sa      spi_tclAAPRespBluetooth()
    **************************************************************************/
   virtual ~spi_tclAAPRespBluetooth() {}

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPRespBluetooth::vPostBTPairingRequest()
    ***************************************************************************/
   /*!
    * \fn      t_Void vPostBTPairingRequest()
    * \brief   Called when Pairing request is sent by AAP device.
    *          Mandatory interface to be implemented.
    * \param   [IN] szAAPBTAddress: BT MAC address of AAP device
    * \param   [IN] enPairingMethod: Pairing method selected by AAP device
    * \sa      None
    **************************************************************************/
   virtual t_Void vPostBTPairingRequest(t_String szAAPBTAddress,
         tenBTPairingMethod enPairingMethod) = 0;

};

#endif /* _SPI_TCLAAPRESPBLUETOOTH_H_ */
