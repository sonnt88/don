/***************************************************************************/
/*!
* \file  spi_tclAAPCmdBluetooth.cpp
* \brief Interface to interact with AAP BT Endpoint
****************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface to interact with AAP BT Endpoint
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
10.03.2015  | Ramya Murthy          | Initial Version
 11.07.2015 | Ramya Murthy          | Fix for same session ID being used for multiple Endpoints

\endverbatim
*****************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <algorithm>

#include "spi_tclAAPSessionDataIntf.h"
#include "spi_tclAAPCmdBluetooth.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPCmdBluetooth.cpp.trc.h"
   #endif
#endif
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdBluetooth::spi_tclAAPCmdBluetooth()
***************************************************************************/
/*lint -esym(40,nullptr) nullptr is not referenced */
spi_tclAAPCmdBluetooth::spi_tclAAPCmdBluetooth() :
   m_poBTEndpoint(NULL)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdBluetooth() entered "));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdBluetooth::~spi_tclAAPCmdBluetooth()
***************************************************************************/
spi_tclAAPCmdBluetooth::~spi_tclAAPCmdBluetooth()
{
   /*lint -esym(40,nullptr) nullptr is not referenced */
   ETG_TRACE_USR1(("~spi_tclAAPCmdBluetooth() entered "));
   m_spoBluetoothCbs = nullptr;
   RELEASE_MEM(m_poBTEndpoint);
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPCmdBluetooth::bInitialiseBTEndpoint()
***************************************************************************/
t_Bool spi_tclAAPCmdBluetooth::bInitialiseBTEndpoint(
      const t_String& rfcoszVehicleBTAddress,
      tenBTPairingMethod enBTPairingMethod)
{
   /*lint -esym(40,nullptr) nullptr is not referenced */
   spi_tclAAPSessionDataIntf oSessionDataIntf;
   shared_ptr<GalReceiver> spoGalReceiver = oSessionDataIntf.poGetGalReceiver();
   MessageRouter* poMessageRouter = (spoGalReceiver != nullptr) ?
         (spoGalReceiver->messageRouter()) : (NULL);

   //! Convert Vehicle BT Address to MAC Address
   t_String szVehMacAddr = szConvertToMacAddress(rfcoszVehicleBTAddress);
   t_Bool bRegSuccess = false;

   if ((NULL != poMessageRouter) && (NULL == m_poBTEndpoint) && (false == szVehMacAddr.empty()))
   {
      ETG_TRACE_USR1(("spi_tclAAPCmdBluetooth::bInitialiseBTEndpoint: Creating BT Endpoint "));
      m_poBTEndpoint = new BluetoothEndpoint((t_U8)e32SESSIONID_AAPBLUETOOTH, poMessageRouter);
      SPI_NORMAL_ASSERT(NULL == m_poBTEndpoint);

      if (NULL != m_poBTEndpoint)
      {
         m_spoBluetoothCbs = new spi_tclAAPBluetoothCbs();

         //! Register callbacks, Vehicle BT Address & Supported Pairing method to endpoint
         m_poBTEndpoint->registerCallbacks(m_spoBluetoothCbs);
         m_poBTEndpoint->registerCarBluetoothAddress(szVehMacAddr);
         m_poBTEndpoint->registerPairingMethod(static_cast<BluetoothPairingMethod>(enBTPairingMethod));
         bRegSuccess = spoGalReceiver->registerService(m_poBTEndpoint);

         ETG_TRACE_USR1(("spi_tclAAPCmdBluetooth::bInitialiseBTEndpoint: "
               "Registration success %d (for PairMethod %d, VehMacAddr %s)",
               ETG_ENUM(BOOL, bRegSuccess),
               ETG_ENUM(AAP_BTPAIRING_METHOD, enBTPairingMethod),
               szVehMacAddr.c_str()));
      }
   }// if (NULL == m_poBTEndpoint)
   else if (true == szVehMacAddr.empty())
   {
      ETG_TRACE_ERR(("spi_tclAAPCmdBluetooth::bInitialiseBTEndpoint: BT Endpoint not created due to empty Vehicle MAC Address!  "));
   }
   else
   {
      ETG_TRACE_ERR(("spi_tclAAPCmdBluetooth::bInitialiseBTEndpoint: Invalid pointer encountered! "));
   }
   return bRegSuccess;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdBluetooth::vUninitialiseBTEndpoint()
***************************************************************************/
t_Void spi_tclAAPCmdBluetooth::vUninitialiseBTEndpoint()
{
	/*lint -esym(40,nullptr) nullptr is not referenced */
   ETG_TRACE_USR1(("spi_tclAAPCmdBluetooth::vUninitialiseBTEndpoint() entered "));
   RELEASE_MEM(m_poBTEndpoint);
   m_spoBluetoothCbs = nullptr;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdBluetooth::vSendBTPairingResponse()
***************************************************************************/
t_Void spi_tclAAPCmdBluetooth::vSendBTPairingResponse(
      t_Bool bReadyToPair, t_Bool bAAPDevicePaired)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdBluetooth::vSendBTPairingResponse() entered: "
         "IsHUReadyToPair = %d, IsAAPDevicePairedToHU = %d ",
         ETG_ENUM(BOOL, bReadyToPair), ETG_ENUM(BOOL, bAAPDevicePaired)));

   if (NULL != m_poBTEndpoint)
   {
      (false == bReadyToPair)?
         (m_poBTEndpoint->requestDelayedPairing()) :
         (m_poBTEndpoint->onReadyForPairing(bAAPDevicePaired));
   }//if (NULL != m_poBTEndpoint)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdBluetooth::vSendAuthenticationData()
***************************************************************************/
t_Void spi_tclAAPCmdBluetooth::vSendAuthenticationData(
      const t_String& rfcoszAuthData)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdBluetooth::vSendAuthenticationData() entered: AuthData = %s ",
         rfcoszAuthData.c_str()));
   if (NULL != m_poBTEndpoint)
   {
      m_poBTEndpoint->sendAuthData(rfcoszAuthData);
   }//if (NULL != m_poBTEndpoint)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdBluetooth::szConvertToMacAddress(...)
***************************************************************************/
t_String spi_tclAAPCmdBluetooth::szConvertToMacAddress(const t_String& rfcoszBTAddress)
{
   //! Initialize string with the BT address
   t_String szMacAddress;

   if (false == rfcoszBTAddress.empty())
   {
      szMacAddress = rfcoszBTAddress.c_str();
      // Insert the ":" charecter in the address string begin from second position and after the two characters.
      //! (Example: If szBTMACAddress is "28E14CDF3072", format string as "28:E1:4C:DF:30:72"
      const t_U8 cou8Init = 2;
      const t_U8 cou8Iterator = 3;
      for(t_U8 u8Index = cou8Init; u8Index < szMacAddress.length(); u8Index+=cou8Iterator)
      {
         szMacAddress.insert(u8Index,":");
      }
      std::transform(szMacAddress.begin(), szMacAddress.end(), szMacAddress.begin(), ::toupper);
   }
   return szMacAddress;
}

//lint –restore

///////////////////////////////////////////////////////////////////////////////
// <EOF>
