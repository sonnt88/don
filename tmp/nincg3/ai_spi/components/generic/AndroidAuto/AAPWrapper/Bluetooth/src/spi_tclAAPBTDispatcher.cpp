/***********************************************************************/
/*!
 * \file  spi_tclAAPBTDispatcher.cpp
 * \brief Message Dispatcher for Bluetooth Messages. implemented using
 *        double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Bluetooth Messages
 AUTHOR:         Ramya Murthy
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 10.03.2015  | Ramya Murthy          | Initial Version

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "spi_tclAAPBTDispatcher.h"
#include "spi_tclAAPRespBluetooth.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclAAPBTDispatcher.cpp.trc.h"
   #endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleBTMsg(this);             \
   }                                                        \
   vDeAllocateMsg();                                        \
}

/***************************************************************************
 ** FUNCTION:  AAPBTMsgBase::AAPBTMsgBase
 ***************************************************************************/
AAPBTMsgBase::AAPBTMsgBase()
{
   ETG_TRACE_USR1(("AAPBTMsgBase() entered "));
   vSetServiceID(e32MODULEID_AAPBLUETOOTH);
}

/***************************************************************************
 ** FUNCTION:  AAPBTPairingRequestMsg::AAPBTPairingRequestMsg
 ***************************************************************************/
AAPBTPairingRequestMsg::AAPBTPairingRequestMsg():
      poszAAPBTAddress(NULL),
      enAAPPairingMethod(scenPreferredAAPBTPairingMethod)
{
   ETG_TRACE_USR1(("AAPBTPairingRequestMsg() entered "));
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPBTPairingRequestMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPBTPairingRequestMsg, spi_tclAAPBTDispatcher);

/***************************************************************************
 ** FUNCTION:  AAPBTPairingRequestMsg::vAllocateMsg
 ***************************************************************************/
t_Void AAPBTPairingRequestMsg::vAllocateMsg()
{
   ETG_TRACE_USR1(("AAPBTPairingRequestMsg::vAllocateMsg() entered "));
   poszAAPBTAddress = new t_String;
   SPI_NORMAL_ASSERT(NULL == poszAAPBTAddress);
}

/***************************************************************************
 ** FUNCTION:  AAPBTPairingRequestMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AAPBTPairingRequestMsg::vDeAllocateMsg()
{
   ETG_TRACE_USR1(("AAPBTPairingRequestMsg::vDeAllocateMsg() entered "));
   RELEASE_MEM(poszAAPBTAddress);
}


/***************************************************************************
 ** FUNCTION:  spi_tclAAPBTDispatcher::spi_tclAAPBTDispatcher
 ***************************************************************************/
spi_tclAAPBTDispatcher::spi_tclAAPBTDispatcher()
{
   ETG_TRACE_USR1(("spi_tclAAPBTDispatcher() entered "));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPBTDispatcher::~spi_tclAAPBTDispatcher
 ***************************************************************************/
spi_tclAAPBTDispatcher::~spi_tclAAPBTDispatcher()
{
   ETG_TRACE_USR1(("~spi_tclAAPBTDispatcher() entered "));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPBTDispatcher::vHandleBTMsg(AAPBTPairingRequestMsg...)
 ***************************************************************************/
t_Void spi_tclAAPBTDispatcher::
      vHandleBTMsg(AAPBTPairingRequestMsg* poBTPairingRequest)const
{
   ETG_TRACE_USR1(("spi_tclAAPBTDispatcher::vHandleBTMsg entered "));
   if ((NULL != poBTPairingRequest) && (NULL != poBTPairingRequest->poszAAPBTAddress))
   {
      CALL_REG_OBJECTS(spi_tclAAPRespBluetooth,
         e16AAP_BT_REGID,
         vPostBTPairingRequest(*(poBTPairingRequest->poszAAPBTAddress),
               poBTPairingRequest->enAAPPairingMethod));
   }
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
