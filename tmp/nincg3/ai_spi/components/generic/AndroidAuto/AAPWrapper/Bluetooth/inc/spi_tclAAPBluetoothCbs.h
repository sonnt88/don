/*!
 *******************************************************************************
 * \file              spi_tclAAPBluetoothCbs.h
 * \brief             BT Endpoint callbacks handler for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    BT Endpoint callbacks handler for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 18.03.2015 |  Ramya Murthy                | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLAAPBLUETOOTHCBS_H_
#define _SPI_TCLAAPBLUETOOTHCBS_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include <IBluetoothCallbacks.h>

/* This class includes a general set of IBluetoothCallbacks that must be set up for the BT Endpoint */
class spi_tclAAPBluetoothCbs : public IBluetoothCallbacks
{
public:

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclAAPBluetoothCbs::spi_tclAAPBluetoothCbs()
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPBluetoothCbs()
    * \brief   Constructor
    * \sa      ~spi_tclAAPBluetoothCbs()
    **************************************************************************/
    spi_tclAAPBluetoothCbs() { }

    /***************************************************************************
     ** FUNCTION:  virtual spi_tclAAPBluetoothCbs::~spi_tclAAPBluetoothCbs()
     ***************************************************************************/
    /*!
     * \fn      virtual ~spi_tclAAPBluetoothCbs()
     * \brief   Destructor
     * \sa      spi_tclAAPBluetoothCbs()
     **************************************************************************/
    virtual ~spi_tclAAPBluetoothCbs() {}

    /**********Start of functions overridden from IBluetoothCallbacks**********/

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPBluetoothCbs::onPairingRequest(...)
    ***************************************************************************/
    /*!
    * \fn      onPairingRequest(const t_String& rfcoszAAPMacAddress, t_S32 s32PairingMethod)
    * \brief   Interface to send BT Pairing response to BT Endpoint
    * \param   [IN] rfcoszAAPMacAddress: BT MAC address of AAP device
    * \param   [IN] s32PairingMethod: Pairing method selected by AAP device
    * \sa      None
    ***************************************************************************/
    void onPairingRequest(const string& rfcoszAAPMacAddress, int s32PairingMethod);

    /***********End of functions overridden from IBluetoothCallbacks**********/


private:

    /***************************************************************************
     ** FUNCTION: spi_tclAAPBluetoothCbs(const spi_tclAAPBluetoothCbs &rfcoObject)
     ***************************************************************************/
    /*!
     * \fn      spi_tclAAPBluetooth(const spi_tclAAPBluetooth &rfcoObject)
     * \brief   Copy constructor not implemented hence made private
     **************************************************************************/
    spi_tclAAPBluetoothCbs(const spi_tclAAPBluetoothCbs& rfcoObject);

    /***************************************************************************
     ** FUNCTION: const spi_tclAAPBluetoothCbs & operator=(
     **                                 const spi_tclAAPBluetoothCbs &rfcoObject);
     ***************************************************************************/
    /*!
     * \fn      const spi_tclAAPBluetoothCbs & operator=(const spi_tclAAPBluetoothCbs &rfcoObject);
     * \brief   assignment operator not implemented hence made private
     **************************************************************************/
    const spi_tclAAPBluetoothCbs& operator=(const spi_tclAAPBluetoothCbs& rfcoObject);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPBluetoothCbs::szConvertToBTAddress(...)
    ***************************************************************************/
    /*!
    * \fn      szConvertToBTAddress(const t_String& rfcoszBTMACAddress)
    * \brief   Converts a MAC address to BT address
    * \param   [IN] rfcoszBTMACAddress: MAC address of a device
    * \retval  t_String: BT Address
    * \sa      None
    ***************************************************************************/
    t_String szConvertToBTAddress(const t_String& rfcoszBTMACAddress);
};


#endif /* _SPI_TCLAAPBLUETOOTHCBS_H_ */
