/***********************************************************************/
/*!
* \file  spi_tclAAPCmdBluetooth.h
* \brief Interface to interact with AAP BT Endpoint
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface to interact with AAP BT Endpoint
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
10.03.2015  | Ramya Murthy          | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPCMDBLUETOOTH_H_
#define _SPI_TCLAAPCMDBLUETOOTH_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <BluetoothEndpoint.h>

#include "AAPTypes.h"
#include "spi_tclAAPBluetoothCbs.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/* Forward declaration */

/******************************************************************************/
/*!
* \class spi_tclAAPCmdBluetooth
* \brief Interface to interact with Bluetooth Endpoint
*
* It provides an interface to interact with VNC CDB SDK.
* It is responsible for creation & initialization of Bluetooth Endpoint.
*******************************************************************************/
class spi_tclAAPCmdBluetooth
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdBluetooth::spi_tclAAPCmdBluetooth()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPCmdBluetooth()
   * \brief   Default Constructor
   * \sa      ~spi_tclAAPCmdBluetooth()
   ***************************************************************************/
   spi_tclAAPCmdBluetooth();

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdBluetooth::~spi_tclAAPCmdBluetooth()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPCmdBluetooth()
   * \brief   Destructor
   * \sa      spi_tclAAPCmdBluetooth()
   ***************************************************************************/
   ~spi_tclAAPCmdBluetooth();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPCmdBluetooth::bInitialiseBTEndpoint()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialiseBTEndpoint()
   * \brief   Creates and initialises an instance of BT Endpoint
   * \retval  t_Bool  :  True if the BT Endpoint is initialised, else False
   * \sa      vUninitialiseBTEndpoint()
   ***************************************************************************/
   t_Bool bInitialiseBTEndpoint(const t_String& rfcoszVehicleBTAddress,
         tenBTPairingMethod enBTPairingMethod);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPCmdBluetooth::vUninitialiseBTEndpoint()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialiseBTEndpoint()
   * \brief   Uninitialises and destroys an instance of BT Endpoint
   * \retval  t_Void
   * \sa      bInitialiseBTEndpoint()
   ***************************************************************************/
   t_Void vUninitialiseBTEndpoint();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPCmdBluetooth::vSendBTPairingResponse()
   ***************************************************************************/
   /*!
   * \fn      vSendBTPairingResponse(t_Bool bReadyToPair, t_Bool bAAPDevicePaired)
   * \brief   Interface to send Pairing response to BT Endpoint
   * \param   bReadyToPair [IN] : True - if HU is ready to pair AAP device, else False.
   * \param   bAAPDevicePaired [IN] : True - if HU is paired with AAP device, else False.
   * \sa      None
   ***************************************************************************/
   t_Void vSendBTPairingResponse(t_Bool bReadyToPair, t_Bool bAAPDevicePaired);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPCmdBluetooth::vSendAuthenticationData()
   ***************************************************************************/
   /*!
   * \fn      vSendAuthenticationData(const t_String& rfcoszAuthData)
   * \brief   Interface to send pairing authentication data to BT Endpoint
   * \param   rfcoszAuthData [IN] : Pairing authentication data
   * \sa      None
   ***************************************************************************/
   t_Void vSendAuthenticationData(const t_String& rfcoszAuthData);

private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
    ** FUNCTION: spi_tclAAPCmdBluetooth(const spi_tclAAPCmdBluetooth &rfcoCmdBluetooth)
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPCmdBluetooth(const spi_tclAAPCmdBluetooth &rfcoCmdBluetooth)
    * \brief   Copy constructor not implemented hence made private
    **************************************************************************/
   spi_tclAAPCmdBluetooth(const spi_tclAAPCmdBluetooth &rfcoCmdBluetooth);

   /***************************************************************************
    ** FUNCTION: const spi_tclAAPCmdBluetooth & operator=(
    **                                 const spi_tclAAPCmdBluetooth &rfcoCmdBluetooth);
    ***************************************************************************/
   /*!
    * \fn      const spi_tclAAPCmdBluetooth & operator=(
    *             const spi_tclAAPCmdBluetooth &rfcoCmdBluetooth);
    * \brief   assignment operator not implemented hence made private
    **************************************************************************/
   const spi_tclAAPCmdBluetooth & operator=(
            const spi_tclAAPCmdBluetooth &rfcoCmdBluetooth);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPCmdBluetooth::szConvertToMacAddress(...)
   ***************************************************************************/
   /*!
   * \fn      szConvertToMacAddress(const t_String& rfcoszBTAddress)
   * \brief   Converts a BT address to MAC address
   * \param   [IN] rfcoszBTAddress: BT address of a device
   * \retval  t_String: MAC Address
   * \sa      None
   ***************************************************************************/
   t_String szConvertToMacAddress(const t_String& rfcoszBTAddress);

   //! Bluetooth Endpoint pointer
   BluetoothEndpoint*   m_poBTEndpoint;

   shared_ptr<IBluetoothCallbacks>  m_spoBluetoothCbs;

};

#endif // _SPI_TCLAAPCMDBLUETOOTH_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
