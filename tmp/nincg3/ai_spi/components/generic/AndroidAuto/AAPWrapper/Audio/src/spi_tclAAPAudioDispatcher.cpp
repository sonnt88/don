/***********************************************************************/
/*!
 * \file              spi_tclAAPAudioDispatcher.cpp
 * \brief             Message Dispatcher for Audio Messages. implemented using
 *                    double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Audio Messages
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/

#include "RespRegister.h"
#include "spi_tclAAPAudioDispatcher.h"
#include "spi_tclAAPRespAudio.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
#include "trcGenProj/Header/spi_tclAAPAudioDispatcher.cpp.trc.h"
#endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleAudioMsg(this);             \
   }                                                        \
   vDeAllocateMsg();                                        \
}


/***************************************************************************
 ** FUNCTION:  AAPAudioMsgBase::AAPAudioMsgBase
 ***************************************************************************/
AAPAudioMsgBase::AAPAudioMsgBase()
{
   vSetServiceID (e32MODULEID_AAPAUDIO);
}

/***************************************************************************
 ** FUNCTION:  AudioPlaybackStartMsg::AudioPlaybackStartMsg
 ***************************************************************************/
AudioPlaybackStartMsg::AudioPlaybackStartMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AudioPlaybackStartMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AudioPlaybackStartMsg, spi_tclAAPAudioDispatcher);

/***************************************************************************
 ** FUNCTION:  AudioPlaybackStartMsg::vAllocateMsg
 ***************************************************************************/
t_Void AudioPlaybackStartMsg::vAllocateMsg()
{

}

/***************************************************************************
 ** FUNCTION:  AudioPlaybackStartMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AudioPlaybackStartMsg::vDeAllocateMsg()
{

}

/***************************************************************************
 ** FUNCTION:  AudioPlaybackStopMsg::AudioPlaybackStopMsg
 ***************************************************************************/
AudioPlaybackStopMsg::AudioPlaybackStopMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AudioPlaybackStopMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AudioPlaybackStopMsg, spi_tclAAPAudioDispatcher);

/***************************************************************************
 ** FUNCTION:  AudioPlaybackStopMsg::vAllocateMsg
 ***************************************************************************/
t_Void AudioPlaybackStopMsg::vAllocateMsg()
{

}

/***************************************************************************
 ** FUNCTION:  AudioPlaybackStopMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AudioPlaybackStopMsg::vDeAllocateMsg()
{

}

/***************************************************************************
 ** FUNCTION:  AudioStreamErrorMsg::AudioStreamErrorMsg
 ***************************************************************************/
AudioStreamErrorMsg::AudioStreamErrorMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  ::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AudioStreamErrorMsg, spi_tclAAPAudioDispatcher);

/***************************************************************************
 ** FUNCTION:  AudioStreamErrorMsg::vAllocateMsg
 ***************************************************************************/
t_Void AudioStreamErrorMsg::vAllocateMsg()
{

}

/***************************************************************************
 ** FUNCTION:  AudioStreamErrorMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AudioStreamErrorMsg::vDeAllocateMsg()
{

}

/***************************************************************************
 ** FUNCTION:  AudioSrcMicRequestMsg::AudioSrcMicRequestMsg
 ***************************************************************************/
AudioSrcMicRequestMsg::AudioSrcMicRequestMsg()
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AudioSrcMicRequestMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AudioSrcMicRequestMsg, spi_tclAAPAudioDispatcher);

/***************************************************************************
 ** FUNCTION:  AudioSrcMicRequestMsg::vAllocateMsg
 ***************************************************************************/
t_Void AudioSrcMicRequestMsg::vAllocateMsg()
{

}

/***************************************************************************
 ** FUNCTION:  AudioSrcMicRequestMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AudioSrcMicRequestMsg::vDeAllocateMsg()
{

}
/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudioDispatcher::spi_tclAAPAudioDispatcher
 ***************************************************************************/
spi_tclAAPAudioDispatcher::spi_tclAAPAudioDispatcher()
{
   ETG_TRACE_USR1(("spi_tclAAPAudioDispatcher Entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudioDispatcher::~spi_tclAAPAudioDispatcher
 ***************************************************************************/
spi_tclAAPAudioDispatcher::~spi_tclAAPAudioDispatcher()
{
   ETG_TRACE_USR1(("~spi_tclAAPAudioDispatcher Entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioPlaybackStartMsg* poPlaybackStart)
 ***************************************************************************/
t_Void spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioPlaybackStartMsg* poPlaybackStart) const
{
   ETG_TRACE_USR1(("spi_tclAAPAudioDispatcher::vHandleAudioMsg:PlaybackStart Entered \n"));
   if (NULL != poPlaybackStart)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespAudio,
            e16AAP_AUDIO_REGID,
            vPlaybackStartCb(poPlaybackStart->m_enAudStreamType, poPlaybackStart->m_s32SessionID));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioPlaybackStopMsg* poPlaybackStart)
 ***************************************************************************/
t_Void spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioPlaybackStopMsg* poPlaybackStop) const
{
   ETG_TRACE_USR1(("spi_tclAAPAudioDispatcher::vHandleAudioMsg:PlaybackStop Entered \n"));
   if (NULL != poPlaybackStop)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespAudio,
            e16AAP_AUDIO_REGID,
            vPlaybackStopCb(poPlaybackStop->m_enAudStreamType, poPlaybackStop->m_s32SessionID));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioStreamErrorMsg* poPlaybackError)
 ***************************************************************************/
t_Void spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioStreamErrorMsg* poPlaybackError) const
{
   ETG_TRACE_USR1(("spi_tclAAPAudioDispatcher::vHandleAudioMsg:AudioPlaybackError Entered \n"));
   if (NULL != poPlaybackError)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespAudio,
            e16AAP_AUDIO_REGID,
            vAudStreamPlaybackErrorCb(poPlaybackError->m_rAAPAudStreamInfo));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioSrcMicRequestMsg* poMicRequestMsg)
 ***************************************************************************/
t_Void spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioSrcMicRequestMsg* poMicRequestMsg) const
{
   ETG_TRACE_USR1(("spi_tclAAPAudioDispatcher::vHandleAudioMsg:poMicRequestMsg Entered \n"));
   if (NULL != poMicRequestMsg)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespAudio,
            e16AAP_AUDIO_REGID,
            vMicRequestCb(poMicRequestMsg->m_bMicOpen, poMicRequestMsg->m_bNoiseReductionEnabled,
                     poMicRequestMsg->m_bEchoCancellationEnabled, poMicRequestMsg->m_s32MaxUnacked));
   }
}

