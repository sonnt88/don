/*!
 *******************************************************************************
 * \file              spi_tclAAPAudioSinkEndpoint.h
 * \brief             Audio Sink Endpoint for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Sink Endpoint for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAAPAUDIOSINKENDPOINT_H_
#define SPI_TCLAAPAUDIOSINKENDPOINT_H_

/******************************************************************************
 | includes:
 | 1)AAP - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include <semaphore.h>
#include <aauto/IAudioSinkCallbacks.h>
#include <aauto/AditAudioSink.h>
using namespace adit::aauto;

typedef adit::aauto::IAditAudioSinkCallbacks IAudioCb;

#include "BaseTypes.h"
#include "AAPTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAAPAudioSinkEndpoint
 * \brief
 */

class spi_tclAAPAudioSinkEndpoint: public IAudioCb
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioSinkEndpoint::spi_tclAAPAudioSinkEndpoint();
       ***************************************************************************/
      /*!
       * \fn     spi_tclAAPAudioSinkEndpoint()
       * \brief  Default Constructor
       * \sa      ~spi_tclAAPAudioSinkEndpoint()
       **************************************************************************
      spi_tclAAPAudioSinkEndpoint();/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioSinkEndpoint::spi_tclAAPAudioSinkEndpoint
       **            (tenAAPAudStreamType enAudStreamType);
       ***************************************************************************/
      /*!
       * \fn     spi_tclAAPAudioSinkEndpoint(tenAAPAudStreamType enAudStreamType)
       * \brief  Parameterized Constructor
       * \sa     ~spi_tclAAPAudioSinkEndpoint()
       **************************************************************************/
      spi_tclAAPAudioSinkEndpoint(tenAAPAudStreamType enAudStreamType);

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclAAPAudioSinkEndpoint::~spi_tclAAPAudioSinkEndpoint()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclAAPAudioSinkEndpoint()
       * \brief   Destructor
       * \sa      spi_tclAAPAudioSinkEndpoint()
       **************************************************************************/
      virtual ~spi_tclAAPAudioSinkEndpoint();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudioSinkEndpoint::bInitialize()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bInitialize()
       * \brief   Initializes session by creating galreceiver
       * \param   cou8SessionID : unique ID of session
       * \param   rfszAudioPipeConfig: audio pipeline configuration
       * \retval  true : initialized successfull.
       * \retval  false : Initialization failed
       * \sa      vUnintialize()
       **************************************************************************/
      t_Bool bInitialize(const t_U8 cou8SessionID, t_String &rfszAudioPipeConfig);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: vUninitialize( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vUninitialize()
       * \brief   Uninitializes session by destroying galreceiver
       * \sa      bInitialize()
       **************************************************************************/
      t_Void vUninitialize();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: playbackStartCallback(t_S32 s32SessionID )
       ***************************************************************************/
      /*!
       * \fn      t_Void playbackStartCallback(t_S32 s32SessionID)
       * \brief   Callback to start audio streaming from IAditAudioSinkCallbacks.
       * \sa      playbackStopCallback()
       **************************************************************************/
      t_Void playbackStartCallback(t_S32 s32SessionID);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: playbackStopCallback(t_S32 s32SessionID )
       ***************************************************************************/
      /*!
       * \fn      t_Void playbackStopCallback(t_S32 s32SessionID)
       * \brief   Callback to stop audio streaming from IAditAudioSinkCallbacks.
       * \sa      playbackStartCallback()
       **************************************************************************/
      t_Void playbackStopCallback(t_S32 s32SessionID);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: vSetAudioStreamConfig( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetAudioStreamConfig()
       * \brief   Function to set Audio config for a specific stream and key.
       * \sa      None
       **************************************************************************/
      t_Void vSetAudioStreamConfig(tenAAPAudStreamType m_enStreamType, t_String szConfigKey,
                        t_String szConfigValue);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudioSinkEndpoint:: bPlaybackStarted( )
       ***************************************************************************/
      /*!
       * \fn      t_Bool bPlaybackStarted()
       * \brief   Function to trigger playback has started once audio channel is allocated.
       * \retval  true  : bPlaybackStarted returns SUCCESS
       * \retval  false : bPlaybackStarted returns FAILURE
       * \sa      None
       **************************************************************************/
      t_Bool bPlaybackStarted();

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION: spi_tclAAPAudioSinkEndpoint(const spi_tclAAPAudioSinkEndpoint
       **            &rfcoobjCRCBResp)
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPAudioSinkEndpoint(const spi_tclAAPAudioSinkEndpoint
       *          &rfcoobjCRCBResp)
       * \brief   Copy constructor not implemented hence made protected
       **************************************************************************/
      spi_tclAAPAudioSinkEndpoint(const spi_tclAAPAudioSinkEndpoint &rfcoobjCRCBResp);

      /***************************************************************************
       ** FUNCTION: const spi_tclAAPAudioSinkEndpoint & operator=(
       **                       const spi_tclAAPAudioSinkEndpoint &rfcoobjCRCBResp);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclAAPAudioSinkEndpoint & operator=
       *          (const spi_tclAAPAudioSinkEndpoint &objCRCBResp);
       * \brief   assignment operator not implemented hence made protected
       **************************************************************************/
      const spi_tclAAPAudioSinkEndpoint & operator=
               (const spi_tclAAPAudioSinkEndpoint &rfcoobjCRCBResp);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: vSetAudioStreamConfig( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetAudioStreamConfig()
       * \brief   Function to set the Audio configuration for specific streams.
       * \sa      None
       **************************************************************************/
      t_Void vSetAudioStreamConfig();

      //! Pointer to AditAudioSink class
      AditAudioSink* m_pAudSinkEndpoint;

      //! Variable for Audio Stream Type
      const tenAAPAudStreamType m_cenStreamType;

      //!Semaphore to synchronize for PlaybackstartCallback from AAp device
      sem_t m_PlaybackStartSem;

      //!Indicates whether Endpoint shutdown() is called
      t_Bool m_bEndpointShutdownStarted;
};

#endif /* SPI_TCLAAPAUDIOSINKENDPOINT_H_ */
