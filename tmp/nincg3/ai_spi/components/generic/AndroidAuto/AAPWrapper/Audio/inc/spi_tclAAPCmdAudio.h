/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdAudio.h
 * \brief             Audio Endpoint Wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Endpoint Wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAAPCMDAUDIO_H_
#define SPI_TCLAAPCMDAUDIO_H_

/******************************************************************************
 | includes:
 | 1)AAP - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include "AAPTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

#define NUM_OF_AUD_SINKS  2

class spi_tclAAPAudioSinkEndpoint;
class spi_tclAAPAudioSourceEndpoint;

/*!
 * \class spi_tclAAPCmdAudio
 * \brief
 */

class spi_tclAAPCmdAudio
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPCmdAudio::spi_tclAAPCmdAudio();
       ***************************************************************************/
      /*!
       * \fn     spi_tclAAPCmdAudio()
       * \brief  Default Constructor
       * \sa      ~spi_tclAAPCmdAudio()
       **************************************************************************/
      spi_tclAAPCmdAudio();

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclAAPCmdAudio::~spi_tclAAPCmdAudio()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclAAPCmdAudio()
       * \brief   Destructor
       * \sa      spi_tclAAPCmdAudio()
       **************************************************************************/
      virtual ~spi_tclAAPCmdAudio();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdAudio::vSetAudioPipeConfig()
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetAudioPipeConfig()
       * \brief   Set the Audio pipeline congigration for alsa devices
       * \param   rfmapAudioPipeConfig: Contains audio pipeline configuration
       **************************************************************************/
      t_Void vSetAudioPipeConfig(tmapAudioPipeConfig &rfmapAudioPipeConfig);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdAudio::bCreateEndpoints()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bCreateEndpoints()
       * \brief   Creates the Audio Sink and Source endpoints for various Audio streams
       * \param   enAudStreamType : Stream Type for which endpoint has to be created
       * \param   u8SessionID : Session ID.
       * \retval  true  : Creation of endpoints successfull.
       * \retval  false : Creation of endpoints failed
       * \sa      vDestroyEndpoints()
       **************************************************************************/
      t_Bool bCreateEndpoints(/*tenAAPAudStreamType enAudStreamType, t_U8 u8SessionID*/);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdAudio:: vDestroyEndpoints( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vDestroyEndpoints()
       * \brief   Destroys the Audio Sink and Source endpoints for various Audio streams
       * \sa      bCreateEndpoints()
       **************************************************************************/
      t_Void vDestroyEndpoints();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdAudio:: bPlaybackStarted( )
       ***************************************************************************/
      /*!
       * \fn      t_Bool bPlaybackStarted()
       * \brief   Trigger received when Audio channel has been allocated
       *          (On source activity ON)
       * \sa      None
       **************************************************************************/
      t_Bool bPlaybackStarted(tenAAPAudStreamType enAudStreamType);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPCmdAudio:: bMicrequestCompleted( )
       ***************************************************************************/
      /*!
       * \fn      t_Bool bMicrequestCompleted()
       * \brief   Trigger received when Speech Audio channel has been allocated
       *          (On source activity ON)
       * \sa      None
       **************************************************************************/
      t_Bool bMicRequestCompleted();

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPCmdAudio:: vSetAudioStreamConfig( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetAudioStreamConfig()
       * \brief   Function to set Audio config for a specific stream and key.
       * \param   enStreamType: Audio stream type for which config has to be set
       * \param   szConfigKey: Type of the configuration
       * \param   enAudioStreamConfig: Audio stream configuration  to be set
       * \sa      None
       **************************************************************************/
      t_Void vSetAudioStreamConfig(tenAAPAudStreamType enStreamType, t_String szConfigKey,
               tenAudioStreamConfig enAudioStreamConfig);

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION: spi_tclAAPCmdAudio(const spi_tclAAPCmdAudio &rfcoobjCRCBResp)
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPCmdAudio(const spi_tclAAPCmdAudio &rfcoobjCRCBResp)
       * \brief   Copy constructor not implemented hence made protected
       **************************************************************************/
      spi_tclAAPCmdAudio(const spi_tclAAPCmdAudio &rfcoobjCRCBResp);

      /***************************************************************************
       ** FUNCTION: const spi_tclAAPCmdAudio & operator=(
       **                                 const spi_tclAAPCmdAudio &rfcoobjCRCBResp);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclAAPCmdAudio & operator=(const spi_tclAAPCmdAudio &objCRCBResp);
       * \brief   assignment operator not implemented hence made protected
       **************************************************************************/
      const spi_tclAAPCmdAudio & operator=(
               const spi_tclAAPCmdAudio &rfcoobjCRCBResp);

      //! Array of Pointers for various Audio Sink streams
      spi_tclAAPAudioSinkEndpoint* m_mapAudSinkEndpoints[NUM_OF_AUD_SINKS];

      //! Pointer to spi_tclAAPAudioSourceEndpoint class for Microphone stream
      spi_tclAAPAudioSourceEndpoint* m_pAudSourceEndpointForMic;

      //! Map to store audio pipeline configuration for different audio streams
      tmapAudioPipeConfig m_mapAudioPipeConfig;
};

#endif /* SPI_TCLAAPCMDAUDIO_H_ */
