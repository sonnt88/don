/*!
 *******************************************************************************
 * \file              spi_tclAAPAudioSourceEndpoint.h
 * \brief             Audio Source Endpoint for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Source Endpoint for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 23.04.2015 |  Deepti Samant       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAAPAUDIOSOURCEENDPOINT_H_
#define SPI_TCLAAPAUDIOSOURCEENDPOINT_H_

/******************************************************************************
 | includes:
 | 1)AAP - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include <semaphore.h>
#include <aauto/IAudioSourceCallbacks.h>
#include <aauto/AditAudioSource.h>
using namespace adit::aauto;

typedef adit::aauto::IAditAudioSourceCallbacks IAudioSrcCb;

#include "BaseTypes.h"
#include "AAPTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAAPAudioSourceEndpoint
 * \brief
 */

class spi_tclAAPAudioSourceEndpoint: public IAudioSrcCb
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioSourceEndpoint::spi_tclAAPAudioSourceEndpoint();
       ***************************************************************************/
      /*!
       * \fn     spi_tclAAPAudioSourceEndpoint()
       * \brief  Default Constructor
       * \sa      ~spi_tclAAPAudioSourceEndpoint()
       **************************************************************************/
      spi_tclAAPAudioSourceEndpoint();

      /***************************************************************************
       ** FUNCTION:  virtual spi_tclAAPAudioSourceEndpoint::~spi_tclAAPAudioSourceEndpoint()
       ***************************************************************************/
      /*!
       * \fn      virtual ~spi_tclAAPAudioSourceEndpoint()
       * \brief   Destructor
       * \sa      spi_tclAAPAudioSourceEndpoint()
       **************************************************************************/
      virtual ~spi_tclAAPAudioSourceEndpoint();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudioSourceEndpoint::bInitialize()
       ***************************************************************************/
      /*!
       * \fn      t_Bool bInitialize()
       * \brief   Initializes session by creating galreceiver
       * \param   cou8SessionID : unique ID for Session
       * \param   rfszAudioPipeConfig: audio pipeline configuration
       * \retval  true : initialized successfull.
       * \retval  false : nitialization failed
       * \sa      vUnintialize()
       **************************************************************************/
      t_Bool bInitialize(const t_U8 cou8SessionID, t_String &rfszAudioPipeConfig);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudioSourceEndpoint:: vUninitialize( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vUninitialize()
       * \brief   Uninitializes session by destroying galreceiver
       * \sa      bInitialize()
       **************************************************************************/
      t_Void vUninitialize();

      /***************************************************************************
       ** FUNCTION:  t_S32 spi_tclAAPAudioSourceEndpoint::microphoneRequestCallback(t_Bool bMicOpen,... )
       ***************************************************************************/
      /*!
       * \fn      t_S32 microphoneRequestCallback(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled,
       *          t_Bool bEchoCancellationEnabled)
       * \brief   Callback for MicroPhone request from IAditAudioSourceCallbacks.
       * \sa      None
       **************************************************************************/
      t_S32 microphoneRequestCallback(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled,
               t_Bool bEchoCancellationEnabled);

      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPAudioSourceEndpoint:: vSetAudioStreamConfig( )
       ***************************************************************************/
      /*!
       * \fn      t_Void vSetAudioStreamConfig()
       * \brief   Function to set Audio config for a specific stream and key.
       * \sa      None
       **************************************************************************/
      t_Void vSetAudioStreamConfig(tenAAPAudStreamType m_enStreamType, t_String szConfigKey,
                        t_String szConfigValue);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPAudioSourceEndpoint:: bMicrophoneRequestCompleted( )
       ***************************************************************************/
      /*!
       * \fn      t_Bool bMicrophoneRequestCompleted()
       * \brief   Function to trigger once audio channel for Speech is allocated.
       * \retval  true  : bMicrophoneRequestCompleted returns SUCCESS
       * \retval  false : bMicrophoneRequestCompleted returns FAILURE
       * \sa      None
       **************************************************************************/
      t_Bool bMicrophoneRequestCompleted();

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION: spi_tclAAPAudioSourceEndpoint(const spi_tclAAPAudioSourceEndpoint
       **            &rfcoobjCRCBResp)
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPAudioSourceEndpoint(const spi_tclAAPAudioSourceEndpoint
       *          &rfcoobjCRCBResp)
       * \brief   Copy constructor not implemented hence made protected
       **************************************************************************/
      spi_tclAAPAudioSourceEndpoint(const spi_tclAAPAudioSourceEndpoint &rfcoobjCRCBResp);

      /***************************************************************************
       ** FUNCTION: const spi_tclAAPAudioSourceEndpoint & operator=(
       **                       const spi_tclAAPAudioSourceEndpoint &rfcoobjCRCBResp);
       ***************************************************************************/
      /*!
       * \fn      const spi_tclAAPAudioSourceEndpoint & operator=
       *          (const spi_tclAAPAudioSourceEndpoint &objCRCBResp);
       * \brief   assignment operator not implemented hence made protected
       **************************************************************************/
      const spi_tclAAPAudioSourceEndpoint & operator=
               (const spi_tclAAPAudioSourceEndpoint &rfcoobjCRCBResp);

      //! Pointer to AditAudioSink class
      AditAudioSource* m_pAudMicSourceEndpoint;

      //!Semaphore to synchronize for microphoneRequestCallback from AAp device
      sem_t m_MicrophoneReqSem;

      //!Indicates whether Endpoint shutdown() is called
      t_Bool m_bEndpointShutdownStarted;
};

#endif /* SPI_TCLAAPAUDIOSOURCEENDPOINT_H_ */
