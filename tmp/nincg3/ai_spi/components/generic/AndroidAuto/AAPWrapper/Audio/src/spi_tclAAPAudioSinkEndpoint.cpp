/*!
 *******************************************************************************
 * \file              spi_tclAAPAudioSinkEndpoint.cpp
 * \brief             Audio Endpoint for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Endpoint for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version
 28.04.2015 |  Ramya Murthy        | Removed StopPlayack semaphore
 06.05.2015 |  Ramya Murthy        | Changed StartPlayback semaphore to timed_wait                                     

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
//#include "SPITypes.h"
#include <time.h>
#include <errno.h>
#include <aauto_alsa.h>
#include <aauto/AlsaAudioSink.h>
#include <aauto/util/shared_ptr.h>
#include <aauto/GalReceiver.h>

#include "spi_tclAudioSettings.h"
#include "spi_tclAAPSessionDataIntf.h"
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPAudioDispatcher.h"
#include "spi_tclAAPAudioSinkEndpoint.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAAPAudioSinkEndpoint.cpp.trc.h"
#endif
#endif
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define SEM_TIMEOUT_IN_SEC  2

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudioSinkEndpoint::spi_tclAAPAudioSinkEndpoint();
 ***************************************************************************/
spi_tclAAPAudioSinkEndpoint::spi_tclAAPAudioSinkEndpoint(tenAAPAudStreamType enAudStreamType):
               m_pAudSinkEndpoint(NULL), m_cenStreamType(enAudStreamType),
               m_bEndpointShutdownStarted(false)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint Entered StreamType : %d", ETG_ENUM(AUDSTREAM_TYPE, m_cenStreamType)));

   if(-1 == sem_init(&m_PlaybackStartSem, 0, 0))
   {
      ETG_TRACE_ERR(("Creation of semaphore failed : Error Number %d ", errno));
   }
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclAAPAudioSinkEndpoint::~spi_tclAAPAudioSinkEndpoint()
 ***************************************************************************/
spi_tclAAPAudioSinkEndpoint::~spi_tclAAPAudioSinkEndpoint()
{
   ETG_TRACE_USR1(("~spi_tclAAPAudioSinkEndpoint Entered  "));

   if (-1 == sem_destroy(&m_PlaybackStartSem))
   {
      ETG_TRACE_ERR(("Destruction of semaphore failed : Error Number %d ", errno));
   }

   m_pAudSinkEndpoint = NULL;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudioSinkEndpoint::bInitialize
 ***************************************************************************/
t_Bool spi_tclAAPAudioSinkEndpoint::bInitialize(const t_U8 cou8SessionID, t_String &rfszAudioPipeConfig)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint:bInitialize Entered Session ID %d StreamType %d Audio pipeline config = %s",
            cou8SessionID, ETG_ENUM(AUDSTREAM_TYPE, m_cenStreamType), rfszAudioPipeConfig.c_str()));

   t_Bool bRetVal = true;
   spi_tclAAPSessionDataIntf oSessionDataIntf;

   //Check if GalReciever pointer should be a class member variable
   shared_ptr<GalReceiver> spoGalReceiver = oSessionDataIntf.poGetGalReceiver();
   /*lint -esym(40,nullptr) nullptr is not referenced */
   if((NULL == m_pAudSinkEndpoint) && (spoGalReceiver != nullptr))
   {
      m_pAudSinkEndpoint = new AlsaAudioSink(cou8SessionID, spoGalReceiver->messageRouter());
   }
   /*lint -esym(40,nullptr) nullptr is not referenced */
   if ((NULL != m_pAudSinkEndpoint) && (spoGalReceiver != nullptr))
   {
      /* ALSA Configuration */
      m_pAudSinkEndpoint->setConfigItem("enable-verbose-logging", "1"); // 1 - enabled
      m_pAudSinkEndpoint->setConfigItem("disable-real-time-priority-audio", "0");
      m_pAudSinkEndpoint->setConfigItem("audio-threads-real-time-priority", "42");

      /* GalReceiver Configuration */
      m_pAudSinkEndpoint->setConfigItem("audio-sink-max-unacked-frames", "4");
      m_pAudSinkEndpoint->setConfigItem("alsa-main-audio", rfszAudioPipeConfig.c_str());
      vSetAudioStreamConfig();

      m_pAudSinkEndpoint->registerCallbacks(this);

      bRetVal = m_pAudSinkEndpoint->init();
      ETG_TRACE_USR4(("Endpoint init ret value %d  ", ETG_ENUM(BOOL, bRetVal)));

      bRetVal = bRetVal && (spoGalReceiver->registerService(m_pAudSinkEndpoint));
      ETG_TRACE_USR4(("Endpoint registration ret value %d  ", ETG_ENUM(BOOL, bRetVal)));
   }
   else
   {
      bRetVal = false;
   }

   ETG_TRACE_USR4(("spi_tclAAPAudioSinkEndpoint:bInitialize Return Value %d  ", ETG_ENUM(BOOL, bRetVal)));

   return bRetVal;
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: vUninitialize( )
 ***************************************************************************/
t_Void spi_tclAAPAudioSinkEndpoint::vUninitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint:vUninitialize Entered StreamType %d ", ETG_ENUM(AUDSTREAM_TYPE, m_cenStreamType)));

   if(NULL != m_pAudSinkEndpoint)
   {
      m_bEndpointShutdownStarted = true;
      m_pAudSinkEndpoint->shutdown();
      m_bEndpointShutdownStarted = false;
   }
   RELEASE_MEM(m_pAudSinkEndpoint);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: vSetAudioStreamConfig( )
 ***************************************************************************/
t_Void spi_tclAAPAudioSinkEndpoint::vSetAudioStreamConfig()
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint::vSetAudioStreamConfig Entered  "));
   spi_tclAudioSettings* poAudSettings = spi_tclAudioSettings::getInstance();

   if ((NULL != m_pAudSinkEndpoint) && (NULL != poAudSettings))
   {
      switch (m_cenStreamType)
      {
         case e8_AUDIOTYPE_MEDIA:
         {
            m_pAudSinkEndpoint->setConfigItem("audio-sink-stream-type", "AUDIO_STREAM_MEDIA");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-stream-codec", "MEDIA_CODEC_AUDIO_PCM");
            //m_pAudSinkEndpoint->setConfigItem("alsa-main-audio", "device=AdevEnt1Out periodMs=16 bufferPeriods=4 silenceMs=48");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-sampling-rate", "48000");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-bits-per-sample", "16");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-channels", "2");
            if (poAudSettings->bGetAAPMediaStreamRecEnabled())
            {
               m_pAudSinkEndpoint->setConfigItem("audio-sink-record", "enable=1 filename=/var/opt/bosch/dynamic/spi/MyTestMedia");
            }
         }//End of if( e8_AUDIOTYPE_MEDIA == m_cenStreamType)
            break;
         case e8_AUDIOTYPE_GUIDANCE:
         {
            m_pAudSinkEndpoint->setConfigItem("audio-sink-stream-type", "AUDIO_STREAM_GUIDANCE");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-stream-codec", "MEDIA_CODEC_AUDIO_PCM");

           // m_pAudSinkEndpoint->setConfigItem("alsa-main-audio", "device=AdevAudioInfoMonoOut periodMs=16");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-sampling-rate", "16000");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-bits-per-sample", "16");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-channels", "1");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-threshold-ms", "50");
            if (poAudSettings->bGetAAPGuidanceStreamRecEnabled())
            {
               m_pAudSinkEndpoint->setConfigItem("audio-sink-record", "enable=1 filename=/var/opt/bosch/dynamic/spi/MyTestGuidance");
            }
         }//End of if( e8_AUDIOTYPE_GUIDANCE == m_cenStreamType)
            break;
         case e8_AUDIOTYPE_SYSTEM_AUDIO:
         {
            m_pAudSinkEndpoint->setConfigItem("audio-sink-stream-type", "AUDIO_STREAM_SYSTEM_AUDIO");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-stream-codec", "MEDIA_CODEC_AUDIO_PCM");
            //m_pAudSinkEndpoint->setConfigItem("alsa-main-audio", "device=AdevAudioInfoMonoOut periodMs=16");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-sampling-rate", "16000");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-bits-per-sample", "16");
            m_pAudSinkEndpoint->setConfigItem("audio-sink-channels", "1");
         }
            break;
         default:
            ETG_TRACE_ERR(("vSetAudioStreamConfig: Invalid Stream Type! "));
            break;
      }//switch (m_cenStreamType)
   }//End of if(NULL != m_pAudSinkEndpoint)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: vSetAudioStreamConfig()
 ***************************************************************************/
t_Void spi_tclAAPAudioSinkEndpoint::vSetAudioStreamConfig(tenAAPAudStreamType enStreamType,
                  t_String szConfigKey, t_String szConfigValue)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint::vSetAudioStreamConfig Entered: Config Key %s ", szConfigKey.c_str()));
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint::vSetAudioStreamConfig Entered: Config Value %s ", szConfigValue.c_str()));
   //TO DO:Check if enStreamType parameter is required
   SPI_INTENTIONALLY_UNUSED(enStreamType);

   if (NULL != m_pAudSinkEndpoint)
   {
      m_pAudSinkEndpoint->setConfigItem(szConfigKey.c_str(), szConfigValue.c_str());
   }//End of if(NULL != m_pAudSinkEndpoint)
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: playbackStartCallback
 **            (t_S32 s32SessionID)
 ***************************************************************************/
t_Void spi_tclAAPAudioSinkEndpoint::playbackStartCallback(t_S32 s32SessionID)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint:playbackStartCallback Entered for SessionID %d , StreamType %d",
            s32SessionID, ETG_ENUM(AUDSTREAM_TYPE, m_cenStreamType)));

   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      AudioPlaybackStartMsg oPlaybackStartMsg;
      oPlaybackStartMsg.m_enAudStreamType = m_cenStreamType;
      oPlaybackStartMsg.m_s32SessionID    = s32SessionID;
      poMsgQinterface->bWriteMsgToQ(&oPlaybackStartMsg, sizeof(oPlaybackStartMsg));
   }//if (NULL != poMsgQinterface)

   //! Avoid blocking the call if Endpoint shutdown is not in progress (Fix for GMMY17-8658)
   if (false == m_bEndpointShutdownStarted)
   {
      struct timespec rTimeSpec;

      if (-1 == clock_gettime(CLOCK_REALTIME, &rTimeSpec))
      {
         ETG_TRACE_ERR(("Error fetching clock time "));
      }
      else
      {
         rTimeSpec.tv_sec += SEM_TIMEOUT_IN_SEC;

         if (-1 == sem_timedwait(&m_PlaybackStartSem, &rTimeSpec))
         {
            if ((errno == ETIMEDOUT)&& (NULL != poMsgQinterface))
            {
               ETG_TRACE_ERR(("sem_timedwait() timed out "));
               trAAPAudioStreamInfo rAudioStreamInfo(m_cenStreamType,e8_AUD_STREAM_OPEN,e8_AUDSTREAM_ERR_TIMEOUT,s32SessionID);
               AudioStreamErrorMsg oPlaybackErrMsg(rAudioStreamInfo);
               poMsgQinterface->bWriteMsgToQ(&oPlaybackErrMsg, sizeof(oPlaybackErrMsg));
            }
            else
            {
               ETG_TRACE_ERR(("sem_timedwait() failed : Error Number %d ", errno));
            }
         }
      }
   }//if (false == m_bEndpointShutdownStarted)

   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint:playbackStartCallback left for Stream Type %d",ETG_ENUM(AUDSTREAM_TYPE, m_cenStreamType)));

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioSinkEndpoint:: playbackStopCallback
 **            (t_S32 s32SessionID )
 ***************************************************************************/
t_Void spi_tclAAPAudioSinkEndpoint::playbackStopCallback(t_S32 s32SessionID)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint:playbackStopCallback Entered: SessionID %d StreamType %d",
            s32SessionID, ETG_ENUM(AUDSTREAM_TYPE, m_cenStreamType)));

   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      AudioPlaybackStopMsg oPlaybackStopMsg;
      oPlaybackStopMsg.m_enAudStreamType = m_cenStreamType;
      oPlaybackStopMsg.m_s32SessionID    = s32SessionID;
      poMsgQinterface->bWriteMsgToQ(&oPlaybackStopMsg, sizeof(oPlaybackStopMsg));
   }//if (NULL != poMsgQinterface)

   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint:playbackStopCallback left "));

}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudioSinkEndpoint:: bPlaybackStarted()
 ***************************************************************************/
t_Bool spi_tclAAPAudioSinkEndpoint::bPlaybackStarted()
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSinkEndpoint:bPlaybackStarted Entered StreamType %d", ETG_ENUM(AUDSTREAM_TYPE, m_cenStreamType)));

   t_Bool bRetVal = true;

   if (-1 == sem_post(&m_PlaybackStartSem))
   {
      ETG_TRACE_ERR(("sem_post failed : Error Number %d ", errno));
      bRetVal = false;
   }

   return bRetVal;
}

//lint –restore
