/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdAudio.cpp
 * \brief             Audio Endpoint Wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Endpoint Wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version
 11.07.2015 | Ramya Murthy         | Fix for same session ID being used for multiple Endpoints

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "aauto_alsa.h"
#include "spi_tclAAPCmdAudio.h"
#include "spi_tclAAPAudioSinkEndpoint.h"
#include "spi_tclAAPAudioSourceEndpoint.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAAPCmdAudio.cpp.trc.h"
#endif
#endif

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAAPCmdAudio::spi_tclAAPCmdAudio();
 ***************************************************************************/
spi_tclAAPCmdAudio::spi_tclAAPCmdAudio():m_pAudSourceEndpointForMic(NULL)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdAudio Entered \n"));

   for(t_U8 u8NumAudSinks = 0; u8NumAudSinks < NUM_OF_AUD_SINKS; u8NumAudSinks++)
   {
      m_mapAudSinkEndpoints[u8NumAudSinks] = NULL;
   }
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclAAPCmdAudio::~spi_tclAAPCmdAudio()
 ***************************************************************************/
spi_tclAAPCmdAudio::~spi_tclAAPCmdAudio()
{
   ETG_TRACE_USR1(("~spi_tclAAPCmdAudio Entered \n"));
   RELEASE_MEM(m_pAudSourceEndpointForMic);
}
/***************************************************************************
 ** FUNCTION:  spi_tclAAPAudio::vSetAudioPipeConfig
 ***************************************************************************/
t_Void spi_tclAAPCmdAudio::vSetAudioPipeConfig(tmapAudioPipeConfig &rfmapAudioPipeConfig)
{
   ETG_TRACE_USR1((" spi_tclAAPAudio::vSetAudioPipeConfig audio pipeline configuration size = %d", rfmapAudioPipeConfig.size()));
   m_mapAudioPipeConfig = rfmapAudioPipeConfig;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdAudio::bCreateEndpoints()
 ***************************************************************************/
t_Bool spi_tclAAPCmdAudio::bCreateEndpoints()
{
   ETG_TRACE_USR1(("bCreateEndpoints Entered \n"));
   //! load the library: enables ADIT logging for audio
   adit::aauto::AlsaEntryPoint(NULL);

   t_Bool bRetVal = true;
   t_U8 u8AudioSessionID = e32SESSIONID_AAPAUDIO; //first endpoint ID, increment for subsequent audio endpoints

   for(t_U8 u8NumAudSinks = 0; u8NumAudSinks < NUM_OF_AUD_SINKS; u8NumAudSinks++)
   {
      if(NULL == m_mapAudSinkEndpoints[u8NumAudSinks])
      {
         tenAAPAudStreamType enAudioStream = static_cast<tenAAPAudStreamType>(u8NumAudSinks);
         m_mapAudSinkEndpoints[u8NumAudSinks] = new spi_tclAAPAudioSinkEndpoint(enAudioStream);
         t_String szAudioPipeConfig;
         if(e8_AUDIOTYPE_MEDIA == enAudioStream)
         {
            szAudioPipeConfig = m_mapAudioPipeConfig[e8AUDIOCONFIG_MAINAUDIO_MEDIA_STANDALONE];
         }
         else if(e8_AUDIOTYPE_GUIDANCE == enAudioStream)
         {
            szAudioPipeConfig = m_mapAudioPipeConfig[e8AUDIOCONFIG_ALTERNATEAUDIO];
         }

         if (NULL != m_mapAudSinkEndpoints[u8NumAudSinks])
         {
            bRetVal = bRetVal && ((m_mapAudSinkEndpoints[u8NumAudSinks])->bInitialize(u8AudioSessionID++, szAudioPipeConfig));
         }//End of if(NULL != m_mapAudSinkEndpoints[u8NumAudSinks])
      }//End of if(NULL == m_mapAudSinkEndpoints[u8NumAudSinks])
   }//End of for(t_U8 u8NumAudSinks = 0...)

//#if 0
   //! Endpoint for Microphone
   if (NULL == m_pAudSourceEndpointForMic)
   {
      m_pAudSourceEndpointForMic = new spi_tclAAPAudioSourceEndpoint();

      if (NULL != m_pAudSourceEndpointForMic)
      {
         bRetVal = bRetVal && (m_pAudSourceEndpointForMic->bInitialize(u8AudioSessionID, m_mapAudioPipeConfig[e8AUDIOCONFIG_AUDIOIN_16kHz]));
      }//End of if(NULL != m_pAudSourceEndpointForMic)
   }//End of if(NULL == m_pAudSourceEndpointForMic)
//#endif

   ETG_TRACE_USR4(("bCreateEndpoints Return Value %d \n", ETG_ENUM(BOOL, bRetVal)));

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdAudio:: vDestroyEndpoints( )
 ***************************************************************************/
t_Void spi_tclAAPCmdAudio::vDestroyEndpoints()
{
   ETG_TRACE_USR1(("vDestroyEndpoints Entered \n"));

   for(t_U8 u8NumAudSinks = 0; u8NumAudSinks < NUM_OF_AUD_SINKS; u8NumAudSinks++)
   {
         if (NULL != m_mapAudSinkEndpoints[u8NumAudSinks])
         {
            (m_mapAudSinkEndpoints[u8NumAudSinks])->vUninitialize();
         }//End of if(NULL != m_mapAudSinkEndpoints[u8NumAudSinks])

         RELEASE_MEM(m_mapAudSinkEndpoints[u8NumAudSinks]);
   }//End of for(t_U8 u8NumAudSinks = 0...)

//#if 0
   //! Endpoint for Microphone Source
   if (NULL != m_pAudSourceEndpointForMic)
   {
      m_pAudSourceEndpointForMic->vUninitialize();
   }

   RELEASE_MEM(m_pAudSourceEndpointForMic);
//#endif
   //! Unload the library : disables ADIT logging for audio
   adit::aauto::AlsaExitPoint();
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdAudio::bPlaybackStarted()
 ***************************************************************************/
t_Bool spi_tclAAPCmdAudio::bPlaybackStarted(tenAAPAudStreamType enAudStreamType)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdAudio::bPlaybackStarted Entered: AudStreamType = %d \n",
         ETG_ENUM(AUDSTREAM_TYPE, enAudStreamType)));

   t_Bool bRetVal = true;
   if((enAudStreamType < NUM_OF_AUD_SINKS) && (NULL != m_mapAudSinkEndpoints[enAudStreamType]))
   {
      bRetVal = m_mapAudSinkEndpoints[enAudStreamType]->bPlaybackStarted();
   }

   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPCmdAudio::bMicRequestCompleted()
 ***************************************************************************/
t_Bool spi_tclAAPCmdAudio::bMicRequestCompleted()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdAudio::bMicRequestCompleted Entered"));

   t_Bool bRetVal = true;

   if(NULL != m_pAudSourceEndpointForMic)
   {
      bRetVal = m_pAudSourceEndpointForMic->bMicrophoneRequestCompleted();
   }

   return bRetVal;
}
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPCmdAudio:: vSetAudioStreamConfig( )
 ***************************************************************************/
t_Void spi_tclAAPCmdAudio::vSetAudioStreamConfig(tenAAPAudStreamType enStreamType, t_String szConfigKey,
		tenAudioStreamConfig enAudioStreamConfig)
{
   ETG_TRACE_USR1(("spi_tclAAPCmdAudio::vSetAudioStreamConfig Entered"));
   if ((enStreamType < NUM_OF_AUD_SINKS) && (NULL != m_mapAudSinkEndpoints[enStreamType]))
   {
      m_mapAudSinkEndpoints[enStreamType]-> vSetAudioStreamConfig(enStreamType, szConfigKey, m_mapAudioPipeConfig[enAudioStreamConfig]);
   }
}

