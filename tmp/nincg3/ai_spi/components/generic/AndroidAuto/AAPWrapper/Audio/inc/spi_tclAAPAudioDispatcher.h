/***********************************************************************/
/*!
 * \file              spi_tclAAPAudioDispatcher.h
 * \brief             Message Dispatcher for Audio Messages. implemented using
 *                    double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Audio Messages
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version

 \endverbatim
 *************************************************************************/

#ifndef SPI_TCLAAPAUDIODISPATCHER_H_
#define SPI_TCLAAPAUDIODISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespRegister.h"

/**************Forward Declarations******************************************/
class spi_tclAAPAudioDispatcher;

/****************************************************************************/
/*!
 * \class AAPAudioMsgBase
 * \brief Base Message type for all Audio messages
 ****************************************************************************/
class AAPAudioMsgBase: public trMsgBase
{
   public:
      /***************************************************************************
       ** FUNCTION:  AAPAudioMsgBase::AAPAudioMsgBase
       ***************************************************************************/
      /*!
       * \fn      AAPAudioMsgBase()
       * \brief   Default constructor
       **************************************************************************/
      AAPAudioMsgBase();

      /***************************************************************************
       ** FUNCTION:  AAPAudioMsgBase::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher)
       * \brief   Pure virtual function to be overridden by inherited classes for
       *          dispatching the message
       * \param poAAPAudioDispatcher : pointer to Message dispatcher for Audio
       **************************************************************************/
      virtual t_Void vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher) = 0;

      /***************************************************************************
       ** FUNCTION:  AAPAudioMsgBase::~AAPAudioMsgBase
       ***************************************************************************/
      /*!
       * \fn      ~AAPAudioMsgBase()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AAPAudioMsgBase()
      {

      }

   private:
};


/****************************************************************************/
/*!
 * \class AudioPlaybackStartMsg
 * \brief Playback start response message for various streams
 ****************************************************************************/
class AudioPlaybackStartMsg: public AAPAudioMsgBase
{
   public:
      tenAAPAudStreamType m_enAudStreamType;
      t_S32 m_s32SessionID;

      /***************************************************************************
       ** FUNCTION:  AudioPlaybackStartMsg::AudioPlaybackStartMsg
       ***************************************************************************/
      /*!
       * \fn      AudioPlaybackStartMsg()
       * \brief   Default constructor
       **************************************************************************/
      AudioPlaybackStartMsg();

      /***************************************************************************
       ** FUNCTION:  AudioPlaybackStartMsg::~AudioPlaybackStartMsg
       ***************************************************************************/
      /*!
       * \fn      ~AudioPlaybackStartMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AudioPlaybackStartMsg(){}

      /***************************************************************************
       ** FUNCTION:  AudioPlaybackStartMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param   poAAPAudioDispatcher : pointer to Message dispatcher for Audio
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  ServiceDiscoveryMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  ServiceDiscoveryMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class AudioPlaybackStopMsg
 * \brief Playback stastoprt response message for various streams
 ****************************************************************************/
class AudioPlaybackStopMsg: public AAPAudioMsgBase
{
   public:
      tenAAPAudStreamType m_enAudStreamType;
      t_S32 m_s32SessionID;

      /***************************************************************************
       ** FUNCTION:  AudioPlaybackStopMsg::AudioPlaybackStopMsg
       ***************************************************************************/
      /*!
       * \fn      AudioPlaybackStopMsg()
       * \brief   Default constructor
       **************************************************************************/
      AudioPlaybackStopMsg();

      /***************************************************************************
       ** FUNCTION:  AudioPlaybackStopMsg::~AudioPlaybackStopMsg
       ***************************************************************************/
      /*!
       * \fn      ~AudioPlaybackStopMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AudioPlaybackStopMsg(){}

      /***************************************************************************
       ** FUNCTION:  AudioPlaybackStopMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param   poAAPAudioDispatcher : pointer to Message dispatcher for Audio
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  AudioPlaybackStopMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  AudioPlaybackStopMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class AudioStreamErrorMsg
 * \brief Semaphore Playback error for Audio Sink
 ****************************************************************************/
class AudioStreamErrorMsg: public AAPAudioMsgBase
{
   public:
      trAAPAudioStreamInfo m_rAAPAudStreamInfo;
	  
	   /***************************************************************************
       ** FUNCTION:  AudioStreamErrorMsg::AudioStreamErrorMsg
       ***************************************************************************/
      /*!
       * \fn      AudioStreamErrorMsg()
       * \brief   Default constructor
       **************************************************************************/
	   AudioStreamErrorMsg();

      /***************************************************************************
       ** FUNCTION:  AudioStreamErrorMsg::AudioStreamErrorMsg
       ***************************************************************************/
      /*!
       * \fn      AudioStreamErrorMsg()
       * \brief   Parametrized constructor
       **************************************************************************/
      AudioStreamErrorMsg(trAAPAudioStreamInfo rAudioStreamInfo):m_rAAPAudStreamInfo(rAudioStreamInfo){}

      /***************************************************************************
       ** FUNCTION:  AudioStreamErrorMsg::~AudioStreamErrorMsg
       ***************************************************************************/
      /*!
       * \fn      ~AudioStreamErrorMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AudioStreamErrorMsg(){}

      /***************************************************************************
       ** FUNCTION:  AudioStreamErrorMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param   poAAPAudioDispatcher : pointer to Message dispatcher for Audio
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  AudioStreamErrorMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  AudioStreamErrorMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class AudioSrcMicRequestMsg
 * \brief Mic Request message for input Audio source
 ****************************************************************************/
class AudioSrcMicRequestMsg: public AAPAudioMsgBase
{
   public:
      t_Bool m_bMicOpen;
      t_Bool m_bNoiseReductionEnabled;
      t_Bool m_bEchoCancellationEnabled;
      t_S32  m_s32MaxUnacked;

      /***************************************************************************
       ** FUNCTION:  AudioSrcMicRequestMsg::AudioSrcMicRequestMsg
       ***************************************************************************/
      /*!
       * \fn      AudioSrcMicRequestMsg()
       * \brief   Default constructor
       **************************************************************************/
      AudioSrcMicRequestMsg();

      /***************************************************************************
       ** FUNCTION:  AudioSrcMicRequestMsg::~AudioSrcMicRequestMsg
       ***************************************************************************/
      /*!
       * \fn      ~AudioSrcMicRequestMsg()
       * \brief   Destructor
       **************************************************************************/
      virtual ~AudioSrcMicRequestMsg(){}

      /***************************************************************************
       ** FUNCTION:  AudioSrcMicRequestMsg::vDispatchMsg
       ***************************************************************************/
      /*!
       * \fn      vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher)
       * \brief   virtual function for dispatching the message of 'this' type
       * \param   poAAPAudioDispatcher : pointer to Message dispatcher for Audio
       **************************************************************************/
      t_Void vDispatchMsg(spi_tclAAPAudioDispatcher* poAAPAudioDispatcher);

      /***************************************************************************
       ** FUNCTION:  AudioSrcMicRequestMsg::vAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vAllocateMsg()
       * \brief   Allocates memory for non trivial datatypes (ex STL containers)
       * \sa      vDeAllocateMsg
       **************************************************************************/
      t_Void vAllocateMsg();

      /***************************************************************************
       ** FUNCTION:  AudioSrcMicRequestMsg::vDeAllocateMsg
       ***************************************************************************/
      /*!
       * \fn      vDeAllocateMsg()
       * \brief   Destroys memory allocated by vAllocateMsg()
       * \sa      vAllocateMsg
       **************************************************************************/
      t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class spi_tclAAPAudioDispatcher
 * \brief Message Dispatcher for Audio Messages
 ****************************************************************************/
class spi_tclAAPAudioDispatcher
{
   public:
      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioDispatcher::spi_tclAAPAudioDispatcher
       ***************************************************************************/
      /*!
       * \fn      spi_tclAAPAudioDispatcher()
       * \brief   Default constructor
       **************************************************************************/
      spi_tclAAPAudioDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioDispatcher::~spi_tclAAPAudioDispatcher
       ***************************************************************************/
      /*!
       * \fn      ~spi_tclAAPSessionDispatcher()
       * \brief   Destructor
       **************************************************************************/
      ~spi_tclAAPAudioDispatcher();

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioPlaybackStartMsg*
       **            poPlaybackStart)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(AudioPlaybackStartMsg* poPlaybackStart)
       * \brief   Handles Messages of AudioPlaybackStartMsg type
	    * \param   poPlaybackStart : pointer to AudioPlaybackStartMsg.
       **************************************************************************/
      t_Void vHandleAudioMsg(AudioPlaybackStartMsg* poPlaybackStart) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioPlaybackStopMsg*
       **            poPlaybackStop)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(AudioPlaybackStopMsg* poPlaybackStop)
       * \brief   Handles Messages of AudioPlaybackStopMsg type
       * \param   poPlaybackStop : pointer to AudioPlaybackStopMsg.
       **************************************************************************/
      t_Void vHandleAudioMsg(AudioPlaybackStopMsg* poPlaybackStop) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioStreamErrorMsg*
       **            poPlaybackError)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(AudioStreamErrorMsg* poPlaybackError)
       * \brief   Handles Messages of AudioStreamErrorMsg type
       * \param   poPlaybackError : pointer to AudioStreamErrorMsg.
       **************************************************************************/
      t_Void vHandleAudioMsg(AudioStreamErrorMsg* poPlaybackError) const;

      /***************************************************************************
       ** FUNCTION:  spi_tclAAPAudioDispatcher::vHandleAudioMsg(AudioMicRequestMsg*
       **            poMicRequest)
       ***************************************************************************/
      /*!
       * \fn      vHandleAudioMsg(AudioMicRequestMsg* poMicRequest)
       * \brief   Handles Messages of AudioMicRequestMsg type
       * \param   poPlaybackStop : pointer to AudioMicRequestMsg.
       **************************************************************************/
      t_Void vHandleAudioMsg(AudioSrcMicRequestMsg* poMicRequest) const;

};

#endif /* SPI_TCLAAPAUDIODISPATCHER_H_ */
