/*!
 *******************************************************************************
 * \file              spi_tclAAPAudioSourceEndpoint.cpp
 * \brief             Audio Endpoint for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Audio Endpoint for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 16.03.2015 |  Deepti Samant       | Initial Version

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
//#include "SPITypes.h"
#include <time.h>
#include <errno.h>
#include <aauto_alsa.h>
#include <aauto/AlsaAudioSource.h>
#include <aauto/util/shared_ptr.h>
#include <aauto/GalReceiver.h>

#include "spi_tclAAPSessionDataIntf.h"
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPAudioDispatcher.h"
#include "spi_tclAAPAudioSourceEndpoint.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AUDIO
#include "trcGenProj/Header/spi_tclAAPAudioSourceEndpoint.cpp.trc.h"
#endif
#endif
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
#define SEM_TIMEOUT_IN_SEC  3

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNspi_tclAAPAudioSourceEndpointEndpoint::spi_tclAAPAudioSourceEndpoint();
 ***************************************************************************/
spi_tclAAPAudioSourceEndpoint::spi_tclAAPAudioSourceEndpoint():m_pAudMicSourceEndpoint(NULL),
m_bEndpointShutdownStarted(false)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSourceEndpoint Entered  "));

   //t_S32 s32InitStartSem = sem_init(&m_MicrophoneReqSem, 0, 0);
   if(-1 == sem_init(&m_MicrophoneReqSem, 0, 0))
   {
      ETG_TRACE_ERR(("Creation of semaphore failed : Error Number %d ", errno));
   }
}

/***************************************************************************
 ** FUNCTION:  virtual spi_tclAAPAudioSourceEndpoint::~spi_tclAAPAudioSourceEndpoint()
 ***************************************************************************/
spi_tclAAPAudioSourceEndpoint::~spi_tclAAPAudioSourceEndpoint()
{
   ETG_TRACE_USR1(("~spi_tclAAPAudioSourceEndpoint Entered  "));
   //TODO
   //t_S32 s32InitStopSem = sem_destroy(&m_MicrophoneReqSem);
   if (-1 == sem_destroy(&m_MicrophoneReqSem))
   {
      ETG_TRACE_ERR(("Destruction of semaphore failed : Error Number %d ", errno));
   }
   RELEASE_MEM(m_pAudMicSourceEndpoint);
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudioSourceEndpoint::bInitialize()
 ***************************************************************************/
t_Bool spi_tclAAPAudioSourceEndpoint::bInitialize(const t_U8 cou8SessionID, t_String &rfszAudioPipeConfig)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSourceEndpoint:bInitialize Entered with session id = %d, Audiopipeline config = %s",cou8SessionID,  rfszAudioPipeConfig.c_str()));

   t_Bool bRetVal = true;
   spi_tclAAPSessionDataIntf oSessionDataIntf;

   //Check if GalReciever pointer should be a class member variable
   shared_ptr<GalReceiver> spoGalReceiver = oSessionDataIntf.poGetGalReceiver();
   /*lint -esym(40,nullptr) nullptr is not referenced */
   if((NULL == m_pAudMicSourceEndpoint) && (spoGalReceiver != nullptr))
   {
      m_pAudMicSourceEndpoint = new AlsaAudioSource(cou8SessionID, spoGalReceiver->messageRouter(), MEDIA_CODEC_AUDIO_PCM);
   }
   /*lint -esym(40,nullptr) nullptr is not referenced */
   if ((NULL != m_pAudMicSourceEndpoint) && (spoGalReceiver != nullptr))
   {
      /* ALSA Configuration */
      m_pAudMicSourceEndpoint->setConfigItem("enable-verbose-logging", "1"); // 1 - enabled
      //m_pAudMicSourceEndpoint->setConfigItem("disable-real-time-priority-audio", "0");
      //m_pAudMicSourceEndpoint->setConfigItem("audio-threads-real-time-priority", "42");

      /* GalReceiver Configuration */
      //m_pAudMicSourceEndpoint->setConfigItem("audio-sink-max-unacked-frames", "4096");
      m_pAudMicSourceEndpoint->setConfigItem("alsa-main-audio-in", rfszAudioPipeConfig.c_str());
      m_pAudMicSourceEndpoint->setConfigItem("audio-sink-sampling-rate", "16000");
      m_pAudMicSourceEndpoint->setConfigItem("audio-sink-bits-per-sample", "16");
      m_pAudMicSourceEndpoint->setConfigItem("audio-sink-channels", "1");

      m_pAudMicSourceEndpoint->registerCallbacks(this);

      bRetVal = bRetVal && m_pAudMicSourceEndpoint->init();
      ETG_TRACE_USR4(("Endpoint init ret value %d  ", ETG_ENUM(BOOL, bRetVal)));

      bRetVal = bRetVal && (spoGalReceiver->registerService(m_pAudMicSourceEndpoint));
      ETG_TRACE_USR4(("Endpoint registration ret value %d  ", ETG_ENUM(BOOL, bRetVal)));
   }
   else
   {
      bRetVal = false;
   }

   ETG_TRACE_USR4(("spi_tclAAPAudioSourceEndpoint:bInitialize Return Value %d  ", ETG_ENUM(BOOL, bRetVal)));

   return bRetVal;
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioSourceEndpoint:: vUninitialize( )
 ***************************************************************************/
t_Void spi_tclAAPAudioSourceEndpoint::vUninitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSourceEndpoint:vUninitialize Entered  "));

   if(NULL != m_pAudMicSourceEndpoint)
   {
      m_bEndpointShutdownStarted = true;
      m_pAudMicSourceEndpoint->shutdown();
   }

   RELEASE_MEM(m_pAudMicSourceEndpoint);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioSourceEndpoint:: vSetAudioStreamConfig()
 ***************************************************************************/
t_Void spi_tclAAPAudioSourceEndpoint::vSetAudioStreamConfig(tenAAPAudStreamType enStreamType,
                  t_String szConfigKey, t_String szConfigValue)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSourceEndpoint::vSetAudioStreamConfig Entered: Config Key %s ", szConfigKey.c_str()));
   ETG_TRACE_USR1(("spi_tclAAPAudioSourceEndpoint::vSetAudioStreamConfig Entered: Config Value %s ", szConfigValue.c_str()));
   SPI_INTENTIONALLY_UNUSED(enStreamType);
   //TO DO:Check if enStreamType parameter is required

   if (NULL != m_pAudMicSourceEndpoint)
   {
      m_pAudMicSourceEndpoint->setConfigItem(szConfigKey.c_str(), szConfigValue.c_str());
   }//End of if(NULL != m_pAudSinkEndpoint)
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioSourceEndpoint:: microphoneRequestCallback
 **            (t_Bool bMicOpen, t_Bool bNoiseReductionEnabled,
 **            t_Bool bEchoCancellationEnabled)
 ***************************************************************************/
t_S32 spi_tclAAPAudioSourceEndpoint::microphoneRequestCallback(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled,
         t_Bool bEchoCancellationEnabled)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSourceEndpoint:microphoneRequestCallback Entered: Mic Open %d , NREnabled %d, "
            "ECEnabled %d", ETG_ENUM(BOOL, bMicOpen), ETG_ENUM(BOOL, bNoiseReductionEnabled),
            ETG_ENUM(BOOL, bEchoCancellationEnabled)));

   spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQinterface)
   {
      AudioSrcMicRequestMsg oMicRequestMsg;
      oMicRequestMsg.m_bMicOpen                   = bMicOpen;
      oMicRequestMsg.m_bNoiseReductionEnabled     = bNoiseReductionEnabled;
      oMicRequestMsg.m_bEchoCancellationEnabled   = bEchoCancellationEnabled;
      oMicRequestMsg.m_s32MaxUnacked              = 0;
      poMsgQinterface->bWriteMsgToQ(&oMicRequestMsg, sizeof(oMicRequestMsg));
   }//if (NULL != poMsgQinterface)

   //! Avoid blocking the call if Endpoint shutdown is not in progress (Fix for GMMY17-8658)
   if (false == m_bEndpointShutdownStarted)
   {
      struct timespec rTimeSpec;

      if (-1 == clock_gettime(CLOCK_REALTIME, &rTimeSpec))
      {
         ETG_TRACE_ERR(("Error fetching clock time "));
      }
      else
      {
         rTimeSpec.tv_sec += SEM_TIMEOUT_IN_SEC;

         if (-1 == sem_timedwait(&m_MicrophoneReqSem, &rTimeSpec))
         {
            if ((errno == ETIMEDOUT)&& (NULL != poMsgQinterface))
            {
               ETG_TRACE_ERR(("sem_timedwait() timed out "));
               tenAAPStreamState enStreamState = bMicOpen ? (e8_AUD_STREAM_OPEN) : (e8_AUD_STREAM_CLOSED);
               trAAPAudioStreamInfo rAudioStreamInfo(e8_AUDIOTYPE_MICROPHONE, enStreamState, e8_AUDSTREAM_ERR_TIMEOUT, 0,
                     bNoiseReductionEnabled, bEchoCancellationEnabled);
               AudioStreamErrorMsg oPlaybackErrMsg(rAudioStreamInfo);
               poMsgQinterface->bWriteMsgToQ(&oPlaybackErrMsg, sizeof(oPlaybackErrMsg));
            }
            else
            {
               ETG_TRACE_ERR(("sem_timedwait() failed : Error Number %d ", errno));
            }
         }
      }
   }//if (false == m_bEndpointShutdownStarted)

   ETG_TRACE_USR1(("spi_tclAAPAudioSourceEndpoint:microphoneRequestCallback left "));

   return STATUS_SUCCESS;

}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudioSourceEndpoint:: bMicrophoneRequestCompleted()
 ***************************************************************************/
t_Bool spi_tclAAPAudioSourceEndpoint::bMicrophoneRequestCompleted()
{
   ETG_TRACE_USR1(("spi_tclAAPAudioSourceEndpoint:bMicrophoneRequestCompleted Entered "));

   t_Bool bRetVal = true;

   if (-1 == sem_post(&m_MicrophoneReqSem))
   {
      ETG_TRACE_ERR(("sem_post failed : Error Number %d ", errno));
      bRetVal = false;
   }

   return bRetVal;
}

//lint –restore
