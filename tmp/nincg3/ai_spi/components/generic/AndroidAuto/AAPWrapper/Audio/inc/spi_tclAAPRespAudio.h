/***********************************************************************/
/*!
* \file  spi_tclAAPRespAudio.h
* \brief AAP Audio Output Interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Audio Output Interface
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                   | Modification
23.03.2015  | Ramya Murthy             | Initial Version
\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPRESPAUDIO_H_
#define _SPI_TCLAAPRESPAUDIO_H_

#include "RespBase.h"
#include "AAPTypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class spi_tclAAPRespAudio
* \brief AAP Audio Output Interface
*
****************************************************************************/
class spi_tclAAPRespAudio:public RespBase
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespAudio::spi_tclAAPRespAudio()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespAudio()
   * \brief   Constructor
   * \sa      ~spi_tclAAPRespAudio()
   **************************************************************************/
   spi_tclAAPRespAudio():RespBase(e16AAP_AUDIO_REGID)
   {

   }

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespAudio::~spi_tclAAPRespAudio()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclAAPRespAudio()
   * \brief   Destructor
   * \param   t_Void
   * \sa      spi_tclAAPRespAudio(RegID enRegId)
   **************************************************************************/
   virtual ~spi_tclAAPRespAudio()
   {

   }

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPRespAudio::vPlaybackStartCb(...)
    ***************************************************************************/
   /*!
    * \fn      t_Void vPlaybackStartCb(tenAAPAudStreamType enAudStreamType, t_S32 s32SessionID)
    * \brief   Callback to start audio streaming from IAditAudioSinkCallbacks.
    * \sa      vPlaybackStopCb()
    **************************************************************************/
   virtual t_Void vPlaybackStartCb(tenAAPAudStreamType enAudStreamType, t_S32 s32SessionID) = 0;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPRespAudio::vPlaybackStopCb(...)
    ***************************************************************************/
   /*!
    * \fn      t_Void vPlaybackStopCb(tenAAPAudStreamType enAudStreamType, t_S32 s32SessionID)
    * \brief   Callback to stop audio streaming from IAditAudioSinkCallbacks.
    * \sa      vPlaybackStartCb()
    **************************************************************************/
   virtual t_Void vPlaybackStopCb(tenAAPAudStreamType enAudStreamType, t_S32 s32SessionID) = 0;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPRespAudio::vAudStreamPlaybackErrorCb(...)
    ***************************************************************************/
   /*!
    * \fn      t_Void vAudStreamPlaybackErrorCb(trAAPAudioStreamInfo rAAPAudStreamInfo)
    * \brief   Callback to handle error from Audio Sink/Source Endpoint.
    * \sa      None
    **************************************************************************/
   virtual t_Void vAudStreamPlaybackErrorCb(trAAPAudioStreamInfo rAAPAudStreamInfo) = 0;

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPRespAudio::vMicRequestCb(...)
    ***************************************************************************/
   /*!
    * \fn      t_Void vMicRequestCb(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled,
    *          t_Bool bEchoCancellationEnabled, t_S32 s32MaxUnacked)
    * \brief   Callback to request for microphone from IAditAudioSourceCallbacks.
    * \sa      None
    **************************************************************************/
   virtual t_Void vMicRequestCb(t_Bool bMicOpen, t_Bool bNoiseReductionEnabled,
            t_Bool bEchoCancellationEnabled, t_S32 s32MaxUnacked) = 0;

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespAudio::spi_tclAAPRespAudio()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespAudio(const spi_tclAAPRespAudio& corfoSrc))
   * \brief   Parameterized Constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \sa      spi_tclAAPRespAudio(RegID enRegId)
   **************************************************************************/
   spi_tclAAPRespAudio(const spi_tclAAPRespAudio& corfoSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespAudio& operator=( const spi_tclMLV...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespAudio& operator=(
   *                          const spi_tclAAPRespAudio& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespAudio(const spi_tclAAPRespAudio& otrSrc)
   ***************************************************************************/
   spi_tclAAPRespAudio& operator=(const spi_tclAAPRespAudio& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

};

#endif /* _SPI_TCLAAPRESPAUDIO_H_ */
