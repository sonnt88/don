
/***********************************************************************/
/*!
* \file   spi_tclAAPRespNavigation
* \brief  NavigationStatus Call backs output interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    NavigationStatus Call backs output interface
AUTHOR:         Dhiraj Asopa
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
08.02.2016  | Dhiraj Asopa          | Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAAPRESPNAVIGATION_H_
#define _SPI_TCLAAPRESPNAVIGATION_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "RespBase.h"
#include "AAPTypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class  spi_tclAAPRespNavigation
* \brief  Navigation Status is  updated to Application using this
*****************************************************************************/
class spi_tclAAPRespNavigation:public RespBase
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespNavigation::spi_tclAAPRespNavigation()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespNavigation()
   * \brief   Default Constructor
   * \sa      ~spi_tclAAPRespNavigation()
   **************************************************************************/
   spi_tclAAPRespNavigation():RespBase(e16AAP_NAVIGATION){}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespNavigation::~spi_tclAAPRespNavigation()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPRespNavigation()
   * \brief   Destructor
   * \sa      spi_tclAAPRespNavigation()
   **************************************************************************/
   virtual ~spi_tclAAPRespNavigation(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespNavigation::vNavigationStatusCallback
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vNavigationStatusCallback()
   * \brief   Callback to update the navigation status from INavigationStatusCallbacks
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vNavigationStatusCallback(tenNavAppState enNavAppState){};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespNavigation::vNavigationNextTurnCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vNavigationNextTurnCallback()
   * \brief   Callback to update the navigation next turn from INavigationStatusCallbacks
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vNavigationNextTurnCallback(t_String szAAPNavRoadName,tenAAPNavNextTurnSide enAAPNavNextTurnSide,
                                              tenAAPNavNextTurnType enAAPNavNextTurnType,t_String szAAPNavImage,
                                              t_S32 S32TurnAngle,t_S32 S32TurnNumber){};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespNavigation::vNavigationNextTurnDistanceCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vNavigationNextTurnDistanceCallback()
   * \brief   Callback to update the next turn distance from INavigationStatusCallbacks
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vNavigationNextTurnDistanceCallback(t_S32 S32DistanceMeters,t_S32 S32TimeSeconds){};

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespNavigation(const spi_tclAAPRespNavigation...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespNavigation(const spi_tclAAPRespNavigation& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespNavigation()
   ***************************************************************************/
   spi_tclAAPRespNavigation(const spi_tclAAPRespNavigation& corfoSrc);


   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespNavigation& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespNavigation& operator=(const spi_tclAAPRespNavigation& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespNavigation(const spi_tclAAPRespNavigation& otrSrc)
   ***************************************************************************/
   spi_tclAAPRespNavigation& operator=(const spi_tclAAPRespNavigation& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/


}; //class spi_tclAAPRespNavigation

#endif //_SPI_TCLAAPRESPNAVIGATION_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
