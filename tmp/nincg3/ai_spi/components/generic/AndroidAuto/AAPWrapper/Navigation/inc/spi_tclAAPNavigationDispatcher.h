/***********************************************************************/
/*!
 * \file  spi_tclAAPNavigationDispatcher.h
 * \brief Message Dispatcher for Navigation Messages. implemented using
 *        double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Navigation Messages
 AUTHOR:         Dhiraj Asopa
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 08.02.2016  | Dhiraj Asopa          | Initial Version

 \endverbatim
 *************************************************************************/
#ifndef _SPI_TCLAAPNAVIGATIONDISPATCHER_H_
#define _SPI_TCLAAPNAVIGATIONDISPATCHER_H_

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "RespRegister.h"
#include "AAPTypes.h"

/**************Forward Declarations******************************************/
class spi_tclAAPNavigationDispatcher;

/****************************************************************************/
/*!
 * \class AAPNavigationMsgBase
 * \brief Base Message type for all Navigation messages
 ****************************************************************************/
class AAPNavigationMsgBase: public trMsgBase
{
   public:

   /***************************************************************************
    ** FUNCTION:  AAPNavigationMsgBase::AAPNavigationMsgBase
    ***************************************************************************/
   /*!
    * \fn      AAPNavigationMsgBase()
    * \brief   Default constructor
    **************************************************************************/
   AAPNavigationMsgBase();

   /***************************************************************************
    ** FUNCTION:  AAPNavigationMsgBase::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPNavigationDispatcher* poNavigationDispatcher)
    * \brief   Pure virtual function to be overridden by inherited classes for
    *          dispatching the message
    * \param   poNavigationDispatcher : pointer to Message dispatcher for Navigation
    **************************************************************************/
   virtual t_Void vDispatchMsg(spi_tclAAPNavigationDispatcher* poNavigationDispatcher) = 0;

   /***************************************************************************
    ** FUNCTION:  AAPNavigationMsgBase::~AAPNavigationMsgBase
    ***************************************************************************/
   /*!
    * \fn      ~AAPNavigationMsgBase()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPNavigationMsgBase()
   {

   }
};

/****************************************************************************/
/*!
 * \class AAPNavigationStatusMsg
 * \brief Navigation Status msg
 ****************************************************************************/
class AAPNavigationStatusMsg: public AAPNavigationMsgBase
{
   public:

   tenNavAppState m_enNavAppState;

   /***************************************************************************
    ** FUNCTION:  AAPNavigationStatusMsg::AAPNavigationStatusMsg
    ***************************************************************************/
   /*!
    * \fn      AAPNavigationStatusMsg()
    * \brief   Default constructor
    **************************************************************************/
   AAPNavigationStatusMsg();

   /***************************************************************************
    ** FUNCTION:  AAPNavigationStatusMsg::~AAPNavigationStatusMsg
    ***************************************************************************/
   /*!
    * \fn      ~AAPNavigationStatusMsg()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPNavigationStatusMsg(){}

   /***************************************************************************
    ** FUNCTION:  AAPNavigationStatusMsg::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPNavigationDispatcher* poNavigationDispatcher)
    * \brief   virtual function for dispatching the message of 'this' type
    * \param   poNavigationDispatcher : pointer to Message dispatcher for Navigation
    **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPNavigationDispatcher* poNavigationDispatcher);

   /***************************************************************************
    ** FUNCTION:  AAPNavigationStatusMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg();

   /***************************************************************************
    ** FUNCTION:  AAPNavigationStatusMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg();
};


/****************************************************************************/
/*!
 * \class AAPNavigationNextTurnMsg
 * \brief Navigation Next Turn msg
 ****************************************************************************/
class AAPNavigationNextTurnMsg: public AAPNavigationMsgBase
{
   public:
   t_String* m_poszAAPNavRoadName;
   tenAAPNavNextTurnSide m_enAAPNavNextTurnSide;
   tenAAPNavNextTurnType m_enAAPNavNextTurnType;
   t_String* m_poszAAPNavImage;
   t_S32 m_s32AAPNavTurnAngle;
   t_S32 m_s32AAPNavTurnNumber;


   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnMsg::AAPNavigationNextTurnMsg
    ***************************************************************************/
   /*!
    * \fn      AAPNavigationNextTurnMsg()
    * \brief   Default constructor
    **************************************************************************/
   AAPNavigationNextTurnMsg();

   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnMsg::~AAPNavigationNextTurnMsg
    ***************************************************************************/
   /*!
    * \fn      ~AAPNavigationNextTurnMsg()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPNavigationNextTurnMsg(){}

   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnMsg::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPNavigationDispatcher* poNavigationDispatcher)
    * \brief   virtual function for dispatching the message of 'this' type
    * \param   poNavigationDispatcher : pointer to Message dispatcher for Navigation
    **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPNavigationDispatcher* poNavigationDispatcher);

   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg();

   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class AAPNavigationNextTurnDistanceMsg
 * \brief Navigation Next Turn Distance msg
 ****************************************************************************/
class AAPNavigationNextTurnDistanceMsg: public AAPNavigationMsgBase
{
   public:
   t_S32 m_s32AAPNavDistance;
   t_S32 m_s32AAPNavTime;


   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnDistanceMsg::AAPNavigationNextTurnDistanceMsg
    ***************************************************************************/
   /*!
    * \fn      AAPNavigationNextTurnDistanceMsg()
    * \brief   Default constructor
    **************************************************************************/
   AAPNavigationNextTurnDistanceMsg();

   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnDistanceMsg::~AAPNavigationNextTurnDistanceMsg
    ***************************************************************************/
   /*!
    * \fn      ~AAPNavigationNextTurnDistanceMsg()
    * \brief   Destructor
    **************************************************************************/
   virtual ~AAPNavigationNextTurnDistanceMsg(){}

   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnDistanceMsg::vDispatchMsg
    ***************************************************************************/
   /*!
    * \fn      vDispatchMsg(spi_tclAAPNavigationDispatcher* poNavigationDispatcher)
    * \brief   virtual function for dispatching the message of 'this' type
    * \param   poNavigationDispatcher : pointer to Message dispatcher for Navigation
    **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPNavigationDispatcher* poNavigationDispatcher);

   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnDistanceMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg();

   /***************************************************************************
    ** FUNCTION:  AAPNavigationNextTurnDistanceMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg();
};

/****************************************************************************/
/*!
 * \class spi_tclAAPNavigationDispatcher
 * \brief Message Dispatcher for Navigation Messages
 ****************************************************************************/
class spi_tclAAPNavigationDispatcher
{
   public:
   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNavigationDispatcher::spi_tclAAPNavigationDispatcher
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPNavigationDispatcher()
    * \brief   Default constructor
    **************************************************************************/
   spi_tclAAPNavigationDispatcher();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNavigationDispatcher::~spi_tclAAPNavigationDispatcher
    ***************************************************************************/
   /*!
    * \fn      ~spi_tclAAPNavigationDispatcher()
    * \brief   Destructor
    **************************************************************************/
   ~spi_tclAAPNavigationDispatcher();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNavigationDispatcher::vHandleNavigationMsg(AAPNavigationStatusMsg*...)
    ***************************************************************************/
   /*!
    * \fn      vHandleNavigationMsg(AAPNavigationStatusMsg* poNavStatus)
    * \brief   Handles Messages of AAPNavigationStatusMsg type
    * \param   poNavStatus : pointer to AAPNavigationStatusMsg.
    **************************************************************************/
   t_Void vHandleNavigationMsg(AAPNavigationStatusMsg* poNavStatus) const;


   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNavigationDispatcher::vHandleNavigationMsg(AAPNavigationNextTurnMsg*...)
    ***************************************************************************/
   /*!
    * \fn      vHandleNavigationMsg(AAPNavigationNextTurnMsg* poNavNxtTurn)
    * \brief   Handles Messages of AAPNavigationNextTurnMsg type
    * \param   poNavNxtTurn : pointer to AAPNavigationStatusMsg.
    **************************************************************************/
   t_Void vHandleNavigationMsg(AAPNavigationNextTurnMsg* poNavNxtTurn) const;

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPNavigationDispatcher::vHandleNavigationMsg(AAPNavigationNextTurnDistanceMsg*...)
    ***************************************************************************/
   /*!
    * \fn      vHandleNavigationMsg(AAPNavigationNextTurnDistanceMsg* poNavNxtTurnDist)
    * \brief   Handles Messages of AAPNavigationNextTurnDistanceMsg type
    * \param   poNavNxtTurnDist : pointer to AAPNavigationStatusMsg.
    **************************************************************************/
   t_Void vHandleNavigationMsg(AAPNavigationNextTurnDistanceMsg* poNavNxtTurnDist) const;


   protected:

      /***************************************************************************
      *********************************PROTECTED**********************************
      ***************************************************************************/

      /***************************************************************************
      ** FUNCTION:  spi_tclAAPNavigationDispatcher(const spi_tclAAPNavigationDispatcher...
      ***************************************************************************/
      /*!
      * \fn      spi_tclAAPNavigationDispatcher(
      *                             const spi_tclAAPNavigationDispatcher& corfoSrc)
      * \brief   Copy constructor - Do not allow the creation of copy constructor
      * \param   corfoSrc : [IN] reference to source data interface object
      * \retval
      * \sa      spi_tclAAPNavigationDispatcher()
      ***************************************************************************/
   spi_tclAAPNavigationDispatcher(const spi_tclAAPNavigationDispatcher& corfoSrc);


      /***************************************************************************
      ** FUNCTION:  spi_tclAAPVideoDispatcher& operator=( const spi_tclAAP...
      ***************************************************************************/
      /*!
      * \fn      spi_tclAAPNavigationDispatcher& operator=(
      *                          const spi_tclAAPNavigationDispatcher& corfoSrc))
      * \brief   Assignment operator
      * \param   corfoSrc : [IN] reference to source data interface object
      * \retval
      * \sa      spi_tclAAPNavigationDispatcher(const spi_tclAAPNavigationDispatcher& otrSrc)
      ***************************************************************************/
   spi_tclAAPNavigationDispatcher& operator=(const spi_tclAAPNavigationDispatcher& corfoSrc);


      /***************************************************************************
      ****************************END OF PROTECTED********************************
      ***************************************************************************/

   private:

      /***************************************************************************
      *********************************PRIVATE************************************
      ***************************************************************************/


      /***************************************************************************
      ****************************END OF PRIVATE *********************************
      ***************************************************************************/

};

#endif /* _SPI_TCLAAPNAVIGATIONDISPATCHER_H_ */
