/*!
 *******************************************************************************
 * \file              spi_tclAAPNavigationCbs.cpp
 * \brief             Navigation Status Endpoint callbacks handler for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Navigation Status Endpoint callbacks handler for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 08.02.2016 |  Dhiraj Asopa                | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPNavigationCbs.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclAAPNavigationCbs.cpp.trc.h"
#endif
#endif


/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPNavigationCbs:: navigationStatusCallback (int s32Status)
***************************************************************************/
void spi_tclAAPNavigationCbs:: navigationStatusCallback (int s32Status)
{
   ETG_TRACE_USR1(("spi_tclAAPNavigationCbs::navigationStatusCallback() entered"));
   ETG_TRACE_USR2(("[DESC]: navigationStatusCallback: Navigation Status = %d",
                   ETG_ENUM(NAVIGATION_STATUS,s32Status)));

   if (0 >= s32Status)
   {
      ETG_TRACE_ERR(("[ERR]: navigationStatusCallback: Invalid Navigation Status! "));
   }
   
   spi_tclAAPMsgQInterface* poMsgQInterface = spi_tclAAPMsgQInterface::getInstance();
   if ((NULL != poMsgQInterface) && (0 < s32Status))
   {
      AAPNavigationStatusMsg oNavStatusMsg;
      oNavStatusMsg.m_enNavAppState = static_cast<tenNavAppState>(s32Status);
      poMsgQInterface->bWriteMsgToQ(&oNavStatusMsg, sizeof(oNavStatusMsg));
   } //if ((NULL != poMsgQInterface) && (0 < oNavStatusMsg))
}
/*****************************************************************************************************
** FUNCTION:  t_Void spi_tclAAPNavigationCbs:: navigationNextTurnCallback (const string& rfcoszRoad,
**        int s32TurnSide,int s32Event,const string& rfcoszImage,int s32TurnAngle,int s32TurnNumber)
*******************************************************************************************************/
void spi_tclAAPNavigationCbs::navigationNextTurnCallback (const string& corfszRoad,int s32TurnSide,int s32Event,const string& corfszImage,int s32TurnAngle,int s32TurnNumber)
{
   ETG_TRACE_USR1(("spi_tclAAPNavigationCbs::navigationNextTurnCallback() entered"));
   ETG_TRACE_USR2(("[DESC]:navigationNextTurnCallback: Road Name=%s", corfszRoad.c_str()));
   ETG_TRACE_USR2(("[DESC]:navigationNextTurnCallback: Next Turn side = %d,Next Turn Event = %d,Turn Angle = %d ,"
                   "Turn Number = %d,Image = %s",s32TurnSide,s32Event,s32TurnAngle,s32TurnNumber,corfszImage.c_str()));

   if ( (false == corfszRoad.empty()) || (0 > s32TurnSide) || (0 > s32Event) || (0 > s32TurnAngle) || (0 > s32TurnNumber))
   {
      ETG_TRACE_ERR(("[ERR]: navigationNextTurnCallback: Invalid Next Turn Data! "));
   }
   
   spi_tclAAPMsgQInterface* poMsgQInterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      AAPNavigationNextTurnMsg oNavNextTurnMsg;

      if ( NULL != oNavNextTurnMsg.m_poszAAPNavRoadName )
      {
         *(oNavNextTurnMsg.m_poszAAPNavRoadName) = corfszRoad.c_str();
      }

      //In kitchenSink application, if turn side input is not provided, 0 is sent as turn side
      //Set the turn side as Unspecified if received as 0
      if (0 == s32TurnSide)
      {
         oNavNextTurnMsg.m_enAAPNavNextTurnSide = e8_NAV_NEXT_TURN_UNSPECIFIED;
      }
      else
      {
         oNavNextTurnMsg.m_enAAPNavNextTurnSide = static_cast<tenAAPNavNextTurnSide>(s32TurnSide);
      }

      oNavNextTurnMsg.m_enAAPNavNextTurnType = static_cast<tenAAPNavNextTurnType>(s32Event);

      if ( NULL != oNavNextTurnMsg.m_poszAAPNavImage )
      {
         *(oNavNextTurnMsg.m_poszAAPNavImage) = corfszImage.c_str();
      }

      oNavNextTurnMsg.m_s32AAPNavTurnAngle=s32TurnAngle;
      oNavNextTurnMsg.m_s32AAPNavTurnNumber=s32TurnNumber;
      poMsgQInterface->bWriteMsgToQ(&oNavNextTurnMsg, sizeof(oNavNextTurnMsg));
   }
}

/*********************************************************************************************************************************
** FUNCTION:  t_Void spi_tclAAPNavigationCbs::navigationNextTurnDistanceCallback (int s32DistanceMeters, int s32TimeSeconds);
***********************************************************************************************************************************/
void spi_tclAAPNavigationCbs::navigationNextTurnDistanceCallback (int s32DistanceMeters, int s32TimeSeconds)
{
   ETG_TRACE_USR1(("spi_tclAAPNavigationCbs::navigationNextTurnDistanceCallback() entered: Distance=%d,Time=%d",s32DistanceMeters,s32TimeSeconds));

   if ( (0 > s32DistanceMeters) && (0 > s32TimeSeconds) )
   {
      ETG_TRACE_ERR(("[ERR]: navigationNextTurnDistanceCallback: Invalid Next Turn Distance Time Data! "));
   }
   
   spi_tclAAPMsgQInterface* poMsgQInterface = spi_tclAAPMsgQInterface::getInstance();
   if (NULL != poMsgQInterface)
   {
      AAPNavigationNextTurnDistanceMsg oNavNextTurnDistanceMsg;
      oNavNextTurnDistanceMsg.m_s32AAPNavDistance=s32DistanceMeters;
      oNavNextTurnDistanceMsg.m_s32AAPNavTime=s32TimeSeconds;
      poMsgQInterface->bWriteMsgToQ(&oNavNextTurnDistanceMsg, sizeof(oNavNextTurnDistanceMsg));
   }
}




