/***********************************************************************/
/*!
 * \file  spi_tclAAPNavigationDispatcher.cpp
 * \brief Message Dispatcher for Navigation TBT Messages. implemented using
 *        double dispatch mechanism
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Message Dispatcher for Navigation TBT Messages
 AUTHOR:         Dhiraj Asopa
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 08.02.2016  | Dhiraj Asopa          | Initial Version

 \endverbatim
 *************************************************************************/

/***************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |--------------------------------------------------------------------------*/
#include "spi_tclAAPNavigationDispatcher.h"
#include "spi_tclAAPRespNavigation.h"

//! Includes for Trace files
#include "Trace.h"
   #ifdef TARGET_BUILD
      #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclAAPNavigationDispatcher.cpp.trc.h"
   #endif
#endif

//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(                               \
         DISPATCHER* poDispatcher)                          \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleNavigationMsg(this);             \
   }                                                        \
   vDeAllocateMsg();                                        \
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationMsgBase::AAPNavigationMsgBase
 ***************************************************************************/
AAPNavigationMsgBase::AAPNavigationMsgBase()
{
   ETG_TRACE_USR1(("AAPNavigationMsgBase() entered "));
   vSetServiceID(e32MODULEID_AAPNAVIGATIONTBT);
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationStatusMsg::AAPNavigationStatusMsg
 ***************************************************************************/
AAPNavigationStatusMsg::AAPNavigationStatusMsg():
      m_enNavAppState(e8SPI_NAV_UNKNOWN)
{
   ETG_TRACE_USR1(("AAPNavigationStatusMsg() entered "));
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationStatusMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPNavigationStatusMsg, spi_tclAAPNavigationDispatcher);

/***************************************************************************
 ** FUNCTION:  AAPNavigationStatusMsg::vAllocateMsg
 ***************************************************************************/
t_Void AAPNavigationStatusMsg::vAllocateMsg()
{
   ETG_TRACE_USR1(("AAPNavigationStatusMsg::vAllocateMsg() entered "));
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationStatusMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AAPNavigationStatusMsg::vDeAllocateMsg()
{
   ETG_TRACE_USR1(("AAPNavigationStatusMsg::vDeAllocateMsg() entered "));

}
/***************************************************************************
 ** FUNCTION:  AAPNavigationNextTurnMsg::AAPNavigationNextTurnMsg
 ***************************************************************************/
AAPNavigationNextTurnMsg::AAPNavigationNextTurnMsg():
      m_poszAAPNavRoadName(NULL),
      m_enAAPNavNextTurnSide(e8_NAV_NEXT_TURN_UNSPECIFIED),
      m_enAAPNavNextTurnType(e8_NAV_NEXT_TURN_UNKNOWN),
      m_poszAAPNavImage(NULL),
      m_s32AAPNavTurnAngle(-1),
      m_s32AAPNavTurnNumber(-1)
{
   ETG_TRACE_USR1(("AAPNavigationNextTurnMsg() entered "));
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationNextTurnMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPNavigationNextTurnMsg, spi_tclAAPNavigationDispatcher);

/***************************************************************************
 ** FUNCTION:  AAPNavigationNextTurnMsg::vAllocateMsg
 ***************************************************************************/
t_Void AAPNavigationNextTurnMsg::vAllocateMsg()
{
   ETG_TRACE_USR1(("AAPNavigationNextTurnMsg::vAllocateMsg() entered "));
   m_poszAAPNavRoadName = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_poszAAPNavRoadName);
   m_poszAAPNavImage = new t_String;
   SPI_NORMAL_ASSERT(NULL == m_poszAAPNavImage);
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationNextTurnMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AAPNavigationNextTurnMsg::vDeAllocateMsg()
{
   ETG_TRACE_USR1(("AAPNavigationNextTurnMsg::vDeAllocateMsg() entered "));
   RELEASE_MEM(m_poszAAPNavRoadName);
   RELEASE_MEM(m_poszAAPNavImage);
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationNextTurnDistanceMsg::AAPNavigationNextTurnDistanceMsg
 ***************************************************************************/
AAPNavigationNextTurnDistanceMsg::AAPNavigationNextTurnDistanceMsg():
      m_s32AAPNavDistance(-1),
      m_s32AAPNavTime(-1)
{
   ETG_TRACE_USR1(("AAPNavigationNextTurnDistanceMsg() entered "));
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationNextTurnDistanceMsg::vDispatchMsg
 ***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(AAPNavigationNextTurnDistanceMsg, spi_tclAAPNavigationDispatcher);

/***************************************************************************
 ** FUNCTION:  AAPNavigationNextTurnDistanceMsg::vAllocateMsg
 ***************************************************************************/
t_Void AAPNavigationNextTurnDistanceMsg::vAllocateMsg()
{
   ETG_TRACE_USR1(("AAPNavigationNextTurnDistanceMsg::vAllocateMsg() entered "));
}

/***************************************************************************
 ** FUNCTION:  AAPNavigationNextTurnMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void AAPNavigationNextTurnDistanceMsg::vDeAllocateMsg()
{
   ETG_TRACE_USR1(("AAPNavigationNextTurnDistanceMsg::vDeAllocateMsg() entered "));
}
/***************************************************************************
 ** FUNCTION:  spi_tclAAPNavigationDispatcher::spi_tclAAPNavigationDispatcher
 ***************************************************************************/
spi_tclAAPNavigationDispatcher::spi_tclAAPNavigationDispatcher()
{
   ETG_TRACE_USR1(("spi_tclAAPNavigationDispatcher() entered "));
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPNavigationDispatcher::~spi_tclAAPNavigationDispatcher
 ***************************************************************************/
spi_tclAAPNavigationDispatcher::~spi_tclAAPNavigationDispatcher()
{
   ETG_TRACE_USR1(("~spi_tclAAPNavigationDispatcher() entered "));
}

/*********************************************************************************************
 ** FUNCTION:  spi_tclAAPNavigationDispatcher::vHandleNavigationMsg(AAPNavigationStatusMsg...)
 *********************************************************************************************/
t_Void spi_tclAAPNavigationDispatcher::
vHandleNavigationMsg(AAPNavigationStatusMsg* poNavStatus)const
{
   ETG_TRACE_USR1(("spi_tclAAPNavigationDispatcher::vHandleNavigationMsg entered "));
   if (NULL != poNavStatus)
      {
         CALL_REG_OBJECTS(spi_tclAAPRespNavigation,
               e16AAP_NAVIGATION,
               vNavigationStatusCallback(poNavStatus->m_enNavAppState));
      } // if (NULL != poNavStatus)

}

/*************************************************************************************************
 ** FUNCTION:  spi_tclAAPNavigationDispatcher::vHandleNavigationMsg(AAPNavigationNextTurnMsg...)
 *************************************************************************************************/
t_Void
spi_tclAAPNavigationDispatcher::vHandleNavigationMsg(
      AAPNavigationNextTurnMsg* poNavNxtTurn) const
{
   ETG_TRACE_USR1(("spi_tclAAPNavigationDispatcher::vHandleNavigationMsg entered "));
   if ( (NULL != poNavNxtTurn) && (NULL != poNavNxtTurn->m_poszAAPNavRoadName) && (NULL != poNavNxtTurn->m_poszAAPNavImage) )
   {
      CALL_REG_OBJECTS(spi_tclAAPRespNavigation,
            e16AAP_NAVIGATION,
            vNavigationNextTurnCallback(*(poNavNxtTurn->m_poszAAPNavRoadName),poNavNxtTurn->m_enAAPNavNextTurnSide,
                  poNavNxtTurn->m_enAAPNavNextTurnType,*(poNavNxtTurn->m_poszAAPNavImage),
                  poNavNxtTurn->m_s32AAPNavTurnAngle,poNavNxtTurn->m_s32AAPNavTurnNumber));
   } // if (NULL != poNavNxtTurn)

}

/********************************************************************************************************
 ** FUNCTION:  spi_tclAAPNavigationDispatcher::vHandleNavigationMsg(AAPNavigationNextTurnDistanceMsg...)
 ********************************************************************************************************/
t_Void
spi_tclAAPNavigationDispatcher::vHandleNavigationMsg(
      AAPNavigationNextTurnDistanceMsg* poNavNxtTurnDist) const
{
   ETG_TRACE_USR1(("spi_tclAAPNavigationDispatcher::vHandleNavigationMsg entered "));
   if ( NULL != poNavNxtTurnDist )
   {
      CALL_REG_OBJECTS(spi_tclAAPRespNavigation,
            e16AAP_NAVIGATION,
            vNavigationNextTurnDistanceCallback(poNavNxtTurnDist->m_s32AAPNavDistance,poNavNxtTurnDist->m_s32AAPNavTime));
   } // if (NULL != poNavNxtTurnDist)

}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
