/*!
 *******************************************************************************
 * \file              spi_tclAAPNavigationCbs.h
 * \brief             Navigation Endpoint callbacks handler for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Navigation Endpoint callbacks handler for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 08.02.2016 |  Dhiraj Asopa                | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef _SPI_TCLAAPNAVIGATIONCBS_H_
#define _SPI_TCLAAPNAVIGATIONCBS_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include <INavigationStatusCallbacks.h>

/* This class includes a general set of INavigationStatusCallbacks that must be set up for the Navigation Status Endpoint */
class spi_tclAAPNavigationCbs : public INavigationStatusCallbacks
{
public:

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclAAPNavigationCbs::spi_tclAAPNavigationCbs()
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPNavigationCbs()
    * \brief   Constructor
    * \sa      ~spi_tclAAPNavigationCbs()
    **************************************************************************/
    spi_tclAAPNavigationCbs() { }

    /***************************************************************************
     ** FUNCTION:  virtual spi_tclAAPNavigationCbs::~spi_tclAAPNavigationCbs()
     ***************************************************************************/
    /*!
     * \fn      virtual ~spi_tclAAPNavigationCbs()
     * \brief   Destructor
     * \sa      spi_tclAAPNavigationCbs()
     **************************************************************************/
    virtual ~spi_tclAAPNavigationCbs() {}

    /**********Start of functions overridden from INavigationStatusCallbacks**********/

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPNavigationCbs::navigationStatusCallback(...)
    ***************************************************************************/
    /*!
     * \fn      navigationStatusCallback (t_S32 s32Status)
     * \brief   Interface to send  navigation status to Navigation Status Endpoint
     * \param   [IN] s32Status: Navigation status (active, inactive, unavailable)
     * \sa      None
     ***************************************************************************/
    void navigationStatusCallback(int s32Status);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPNavigationCbs::navigationNextTurnCallback(...)
    ***************************************************************************/
   /*!
    * \fn      navigationNextTurnCallback (const string& rfcoszRoad,int s32TurnSide,int s32Event
    *                                     ,const string& rfcoszImage,int s32TurnAngle,int s32TurnNumber)
    * \brief   Interface to send Navigation Next Turn message to Navigation Status Endpoint
    * \param   [IN] rfcoszRoad: name of the road
    * \param   [IN] s32TurnSide: turn side (may be TurnSide values, defined in protos.pb.h: LEFT, RIGHT or UNSPECIFIED)
    * \param   [IN] s32Event: event type (may be NextTurnEnum values, defined in protos.pb.h: TURN, U_TURN, ROUNDABOUT_ENTER_AND_EXIT, etc).
    * \param   [IN] rfcoszImage:image to be shown in the instrument cluster (PNG format). Will be null if instrument cluster type is ENUM
    * \param   [IN] s32TurnAngle:turn angle in degrees between the roundabout entry and exit (0..359). Only used for event type ROUNDABOUT_ENTER_AND_EXIT. -1 if unused.
    * \param   [IN] s32TurnNumber:  turn number, counting around from the roundabout entry to the exit. Only used for event type ROUNDABOUT_ENTER_AND_EXIT. -1 if unused.
    * \sa      None
    ***************************************************************************/
    void navigationNextTurnCallback(const string& rfcoszRoad,int s32TurnSide,int s32Event,const string& rfcoszImage,int s32TurnAngle,int s32TurnNumber);

    /***************************************************************************
     ** FUNCTION:  t_Void spi_tclAAPNavigationCbs::navigationNextTurnDistanceCallback(...)
     ***************************************************************************/
    /*!
     * \fn      navigationNextTurnDistanceCallback(int s32DistanceMeters, int s32TimeSeconds)
     * \brief   Interface to send Navigation Next Turn Distance message to Navigation Status Endpoint
     * \param   [IN] s32DistanceMeters: Distance to next turn event in meters.
     * \param   [IN] s32TimeSeconds:   Time to next turn event in seconds.
     * \sa      None
     ***************************************************************************/
    void navigationNextTurnDistanceCallback(int s32DistanceMeters, int s32TimeSeconds);

    /***********End of functions overridden from INavigationStatusCallbacks**********/


private:

    /***************************************************************************
     ** FUNCTION: spi_tclAAPNavigationCbs(const spi_tclAAPNavigationCbs &rfcoObject)
     ***************************************************************************/
    /*!
     * \fn      spi_tclAAPNavigation(const spi_tclAAPNavigation &rfcoObject)
     * \brief   Copy constructor not implemented hence made private
     **************************************************************************/
    spi_tclAAPNavigationCbs(const spi_tclAAPNavigationCbs& rfcoObject);

    /***************************************************************************
     ** FUNCTION: const spi_tclAAPNavigationCbs & operator=(
     **                                 const spi_tclAAPNavigationCbs &rfcoObject);
     ***************************************************************************/
    /*!
     * \fn      const spi_tclAAPNavigationCbs & operator=(const spi_tclAAPNavigationCbs &rfcoObject);
     * \brief   assignment operator not implemented hence made private
     **************************************************************************/
    const spi_tclAAPNavigationCbs& operator=(const spi_tclAAPNavigationCbs& rfcoObject);

};


#endif /* _SPI_TCLAAPNAVIGATIONCBS_H_ */
