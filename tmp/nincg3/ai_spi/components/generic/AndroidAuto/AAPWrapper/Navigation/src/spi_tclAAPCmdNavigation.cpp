/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdNavigation.cpp
 * \brief             Navigation TBT  wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Navigation TBT wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 08.02.2016 | Dhiraj Asopa                 | Initial Version


 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPSessionDataIntf.h"
#include "spi_tclAAPCmdNavigation.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPCmdNavigation.cpp.trc.h"
   #endif
#endif
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_U32 scou32MinInterval = 1000;
static const t_U32 scou32Height = 0;
static const t_U32 scou32Width = 0;
static const t_U32 scou32ColorDepthBits = 0;

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdNavigation::spi_tclAAPCmdNavigation()
***************************************************************************/
spi_tclAAPCmdNavigation::spi_tclAAPCmdNavigation():
m_poNavEndpoint(NULL)
{
   ETG_TRACE_USR1((" spi_tclAAPCmdNavigation::spi_tclAAPCmdNavigation entered "));
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdNavigation::~spi_tclAAPCmdNavigation()
***************************************************************************/
spi_tclAAPCmdNavigation::~spi_tclAAPCmdNavigation()
{
   ETG_TRACE_USR1((" spi_tclAAPCmdNavigation::~spi_tclAAPCmdNavigation entered "));
   RELEASE_MEM(m_poNavEndpoint);
   m_spoNavigationCbs = nullptr;
}

/***************************************************************************
** FUNCTION:t_Bool spi_tclAAPCmdNavigation::bInitializeNavigationStatus
***************************************************************************/
t_Bool spi_tclAAPCmdNavigation::bInitializeNavigationStatus()
{
   /*lint -esym(40,nullptr)nullptr Undeclared identifier */
   ETG_TRACE_USR1((" spi_tclAAPCmdNavigation::bInitializeNavigationStatus entered "));
   //! Get Galreciever using Session Data Interface
   spi_tclAAPSessionDataIntf oSessionDataIntf;
   shared_ptr<GalReceiver> spoGalReceiver = oSessionDataIntf.poGetGalReceiver();
   MessageRouter* poMessageRouter = (spoGalReceiver != nullptr) ? (spoGalReceiver->messageRouter()) : (NULL);
   t_Bool bRetVal = false;
   if ((NULL != poMessageRouter) && (NULL == m_poNavEndpoint))
   {
      NavigationStatusService_InstrumentClusterType enICType = NavigationStatusService_InstrumentClusterType_ENUM;
      m_poNavEndpoint = new NavigationStatusEndpoint((t_U8)e32SESSIONID_AAPNAVIGATION, poMessageRouter,
    		             scou32MinInterval,scou32Height,scou32Width,scou32ColorDepthBits,enICType);
      SPI_NORMAL_ASSERT(NULL == m_poNavEndpoint);
      if (NULL != m_poNavEndpoint)
      {
         m_spoNavigationCbs = new spi_tclAAPNavigationCbs();
         m_poNavEndpoint->registerCallbacks(m_spoNavigationCbs);
         bRetVal = spoGalReceiver->registerService(m_poNavEndpoint);
         ETG_TRACE_USR4(("[DESC]: bInitializeNavigationStatus: Registration with GalReceiver : %d",ETG_ENUM(BOOL,bRetVal)));
      }
   }
   return bRetVal;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPCmdNavigation::vUninitialiseNavigationStatus()
***************************************************************************/
t_Void spi_tclAAPCmdNavigation::vUninitializeNavigationStatus()
{
   /*lint -esym(40,nullptr)nullptr Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAAPCmdNavigation::vUninitialiseNavigationStatus() entered "));
   RELEASE_MEM(m_poNavEndpoint);
   m_spoNavigationCbs = nullptr;
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>

