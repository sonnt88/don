/***********************************************************************/
/*!
* \file  spi_tclAAPCmdNavigation.h
* \brief Interface to interact with AAP Navigation Endpoint
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Interface to interact with AAP Navigation Endpoint
AUTHOR:         Dhiraj Asopa
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
08.02.2016  | Dhiraj Asopa          | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPCMDNAVIGATION_H_
#define _SPI_TCLAAPCMDNAVIGATION_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <NavigationStatusEndpoint.h>

#include "AAPTypes.h"
#include "spi_tclAAPNavigationCbs.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/* Forward declaration */

/******************************************************************************/
/*!
* \class spi_tclAAPCmdNavigation
* \brief Interface to interact with Navigation Endpoint
*
* It is responsible for creation & initialization of Navigation Endpoint.
*******************************************************************************/
class spi_tclAAPCmdNavigation
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdNavigation::spi_tclAAPCmdNavigation()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPCmdNavigation()
   * \brief   Default Constructor
   * \sa      ~spi_tclAAPCmdNavigation()
   ***************************************************************************/
   spi_tclAAPCmdNavigation();

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPCmdNavigation::~spi_tclAAPCmdNavigation()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPCmdNavigation()
   * \brief   Destructor
   * \sa      spi_tclAAPCmdNavigation()
   ***************************************************************************/
   ~spi_tclAAPCmdNavigation();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPCmdNavigation::bInitializeNavigationStatus()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitializeNavigationStatus()
   * \brief   Creates and initialises an instance of Navigation Endpoint
   * \retval  t_Bool  :  True if the Navigation Endpoint is initialised, else False
   * \sa      bUninitializeNavigationStatus()
   ***************************************************************************/
   t_Bool bInitializeNavigationStatus();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPCmdNavigation::bUninitializeNavigationStatus()
   ***************************************************************************/
   /*!
   * \fn      t_Void bUninitializeNavigationStatus()
   * \brief   Uninitialises and destroys an instance of Navigation Endpoint
   * \retval  t_Void
   * \sa      bInitializeNavigationStatus()
   ***************************************************************************/
   t_Void vUninitializeNavigationStatus();


private:
   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
    ** FUNCTION: spi_tclAAPCmdNavigation(const spi_tclAAPCmdNavigation &rfcoCmdNavigation)
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPCmdNavigation(const spi_tclAAPCmdNavigation &rfcoCmdNavigation)
    * \brief   Copy constructor not implemented hence made private
    **************************************************************************/
   spi_tclAAPCmdNavigation(const spi_tclAAPCmdNavigation &rfcoCmdNavigation);

   /***************************************************************************
    ** FUNCTION: const spi_tclAAPCmdNavigation & operator=(
    **                                 const spi_tclAAPCmdNavigation &rfcoCmdNavigation);
    ***************************************************************************/
   /*!
    * \fn      const spi_tclAAPCmdNavigation & operator=(
    *             const spi_tclAAPCmdNavigation &rfcoCmdNavigation);
    * \brief   assignment operator not implemented hence made private
    **************************************************************************/
   const spi_tclAAPCmdNavigation & operator=(
            const spi_tclAAPCmdNavigation &rfcoCmdNavigation);

   //! NavigationStatus Endpoint pointer
   NavigationStatusEndpoint*   m_poNavEndpoint;

   shared_ptr<INavigationStatusCallbacks>  m_spoNavigationCbs;

};

#endif // _SPI_TCLAAPCMDNAVIGATION_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
