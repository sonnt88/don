/*!
 *******************************************************************************
 * \file              spi_tclAAPcmdMediaPlayback.h
 * \brief             MediaMetadata wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    MediMetadata wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 24.05.2015 |  Vinoop					   | Initial Version

 \endverbatim
 ******************************************************************************/
#ifndef SPI_TCLAAPCMDMEDIAPLAYBACK_H_
#define SPI_TCLAAPCMDMEDIAPLAYBACK_H_

#include "SPITypes.h"
#include "AAPTypes.h"
#include "MediaPlaybackStatusEndpoint.h"
//#include "IMediaPlaybackStatusCallbacks.h"
#include "spi_tclMediaPlaybackCbs.h"
/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAAPcmdMediaPlayback
 * \brief
 */

class spi_tclAAPcmdMediaPlayback
{
public:

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  spi_tclAAPcmdMediaPlayback::spi_tclAAPcmdMediaPlayback();
***************************************************************************/
/*!
* \fn     spi_tclAAPcmdMediaPlayback()
* \brief  Default Constructor
* \sa     spi_tclAAPcmdMediaPlayback()
**************************************************************************/
spi_tclAAPcmdMediaPlayback();

/***************************************************************************
** FUNCTION:  virtual spi_tclAAPcmdMediaPlayback::~spi_tclAAPcmdMediaPlayback()
***************************************************************************/
/*!
* \fn      virtual ~spi_tclAAPcmdMediaPlayback()
* \brief   Virtual Destructor
* \sa      spi_tclAAPcmdMediaPlayback()
**************************************************************************/
virtual  ~spi_tclAAPcmdMediaPlayback();

/***************************************************************************
 ** FUNCTION:  spi_tclAAPcmdMediaPlayback::bInitialize()
 ***************************************************************************/
/*!
 * \fn      bInitialize()
 * \brief   Creates MediaPlayback endpoints and registers callbacks
 * \sa      bInitialize()
 **************************************************************************/
t_Bool  bInitializeMediaPlayback();

/***************************************************************************
 ** FUNCTION:  spi_tclAAPcmdMediaPlayback::vReportAction()
 ***************************************************************************/
/*!
 * \fn      vReportAction()
 * \brief 	Reports an action associated with media playback
 * \sa      vReportAction()
 **************************************************************************/
t_Void vReportAction();

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPcmdMediaPlayback::vUninitialiseMediaPlayback()
***************************************************************************/
/*!
* \fn      t_Void spi_tclAAPcmdMediaPlayback
* \brief   Uninitialises and destroys an instance of MediaPlayback Endpoint
* \retval  t_Void
* \sa      bInitializeMediaPlayback()
***************************************************************************/
t_Void vUninitialiseMediaPlayback();

private:
  //! MediaPlaybackStatusEndpoint Endpoint
MediaPlaybackStatusEndpoint *m_poMediaPlaybackStatusEndpoint;

//! Pointer to class containing callbacks for GAL receiver
shared_ptr<IMediaPlaybackStatusCallbacks> m_spoPlaybackStatusCbs;

};
#endif
///////////////////////////////////////////////////////////////////////////////
// <EOF>
