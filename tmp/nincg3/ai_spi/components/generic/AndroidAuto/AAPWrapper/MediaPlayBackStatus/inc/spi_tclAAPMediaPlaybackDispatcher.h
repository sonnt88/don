
/***********************************************************************/
/*!
* \file   spi_tclAAPMediaPlaybackDispatcher.h
* \brief  Message Dispatcher for Media metadata Messages. implemented using
 *        double dispatch mechanism
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message Dispatcher for MediaMetadata
AUTHOR:         Vinoop
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
22.05.2015  | Vinoop   				| Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAAPMEDIAPLAYBACKDISPATCHER_H_
#define _SPI_TCLAAPMEDIAPLAYBACKDISPATCHER_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespRegister.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
class spi_tclAAPMediaPlaybackDispatcher;


/****************************************************************************/
/*!
* \class    AAPMediaPlaybackMsgBase
* \brief    Base Message type for all
****************************************************************************/
class AAPMediaPlaybackMsgBase: public trMsgBase
{
public:
   /***************************************************************************
   ** FUNCTION:  AAPMediaPlaybackMsgBase::AAPMediaPlaybackMsgBase
   ***************************************************************************/
   /*!
   * \fn      AAPMediaPlaybackMsgBase()
   * \brief   Default constructor
   **************************************************************************/
	AAPMediaPlaybackMsgBase();

   /***************************************************************************
   ** FUNCTION:  AAPMediaPlaybackMsgBase::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(spi_tclAAPMediaPlaybackDispatcher* poDiscDispatcher)
   * \brief   Pure virtual function to be overridden by inherited classes for
   *          dispatching the message
   * \param poMediaPlaybackDispatcher: pointer to MediaPlayback Message Dispatcher
   **************************************************************************/
   virtual t_Void vDispatchMsg( spi_tclAAPMediaPlaybackDispatcher* poMediaPlaybackDispatcher)=0;

   /***************************************************************************
   ** FUNCTION:  AAPVideoMsgBase::~AAPVideoMsgBase
   ***************************************************************************/
   /*!
   * \fn      ~AAPVideoMsgBase()
   * \brief   Destructor
   **************************************************************************/
   virtual ~AAPMediaPlaybackMsgBase(){}

};  //class AAPMediaPlaybackMsgBase


/****************************************************************************/
/*!
* \class    MediaPlaybackStatusMsg
****************************************************************************/
class MediaPlaybackStatusMsg: public AAPMediaPlaybackMsgBase
{
public:

	trAAPMediaPlaybackStatus *m_prAAPMediaPlaybackStatus;
   /***************************************************************************
   ** FUNCTION:  MediaPlaybackStatusMsg::MediaPlaybackStatusMsg
   ***************************************************************************/
   /*!
   * \fn      MediaPlaybackStatusMsg()
   * \brief   Default constructor
   **************************************************************************/
	MediaPlaybackStatusMsg();

   /***************************************************************************
   ** FUNCTION:  MediaPlaybackStartMsg::~MediaPlaybackStartMsg
   ***************************************************************************/
   /*!
   * \fn      ~MediaPlaybackStartMsg()
   * \brief   MediaPlaybackStartMsg
   **************************************************************************/
   virtual ~MediaPlaybackStatusMsg();

   /***************************************************************************
   ** FUNCTION:  MediaPlaybackStatusMsg::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(spi_tclAAPMediaPlaybackDispatcher* poDiscDispatcher)
   * \brief   virtual function for dispatching the message of 'this' type
   * \param poDiscDispatcher: pointer to MediaPlayback Message Dispatcher
   **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPMediaPlaybackDispatcher* poDispatcher);


   /***************************************************************************
    ** FUNCTION:  MediaMetadataStatusMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg();

   /***************************************************************************
    ** FUNCTION:  MediaMetadataStatusMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg();

}; //class MediaPlaybackStartMsg

/****************************************************************************/
/*!
* \class    MediaMetadataStatusMsg
****************************************************************************/
class MediaMetadataStatusMsg: public AAPMediaPlaybackMsgBase
{
public:

	trAAPMediaPlaybackMetadata *m_prAAPMediaPlaybackMetadata;
   /***************************************************************************
   ** FUNCTION:  MediaMetadataStatusMsg::MediaMetadataStatusMsg
   ***************************************************************************/
   /*!
   * \fn      MediaMetadataStatusMsg()
   * \brief   Default constructor
   **************************************************************************/
	MediaMetadataStatusMsg();

   /***************************************************************************
   ** FUNCTION:  MediaMetadataStatusMsg::~MediaMetadataStatusMsg
   ***************************************************************************/
   /*!
   * \fn      ~MediaMetadataStatusMsg()
   * \brief   MediaMetadataStatusMsg
   **************************************************************************/
   virtual ~MediaMetadataStatusMsg();

   /***************************************************************************
   ** FUNCTION:  MediaMetadataStatusMsg::vDispatchMsg
   ***************************************************************************/
   /*!
   * \fn      vDispatchMsg(MediaMetadataStatusMsg* poDiscDispatcher)
   * \brief   virtual function for dispatching the message of 'this' type
   * \param poDispatcher: pointer to  Message Dispatcher
   **************************************************************************/
   t_Void vDispatchMsg(spi_tclAAPMediaPlaybackDispatcher* poDispatcher);

   /***************************************************************************
    ** FUNCTION:  MediaMetadataStatusMsg::vAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vAllocateMsg()
    * \brief   Allocates memory for non trivial datatypes (ex STL containers)
    * \sa      vDeAllocateMsg
    **************************************************************************/
   t_Void vAllocateMsg();

   /***************************************************************************
    ** FUNCTION:  MediaMetadataStatusMsg::vDeAllocateMsg
    ***************************************************************************/
   /*!
    * \fn      vDeAllocateMsg()
    * \brief   Destroys memory allocated by vAllocateMsg()
    * \sa      vAllocateMsg
    **************************************************************************/
   t_Void vDeAllocateMsg();

};//class PlaybackStopMsg

/****************************************************************************/
/*!
* \class    spi_tclAAPMediaPlaybackDispatcher
* \brief    Message Dispatcher for Media playback maessages
****************************************************************************/
class spi_tclAAPMediaPlaybackDispatcher
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPMediaPlaybackDispatcher::spi_tclAAPMediaPlaybackDispatcher()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideoDispatcher()
   * \brief   Default Constructor
   * \param   t_Void
   * \sa      ~spi_tclAAPVideoDispatcher()
   **************************************************************************/
	spi_tclAAPMediaPlaybackDispatcher(){}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPMediaPlaybackDispatcher::~spi_tclAAPMediaPlaybackDispatcher()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPMediaPlaybackDispatcher()
   * \brief   Destructor
   * \param   t_Void
   * \sa      spi_tclAAPMediaPlaybackDispatcher()
   **************************************************************************/
   ~spi_tclAAPMediaPlaybackDispatcher(){}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPMediaPlaybackDispatcher::spi_tclAAPMediaPlaybackDispatcher(PlaybackStartMsg..)
   ***************************************************************************/
   /*!
   * \fn      vHandlePlaybackStatusMsg(MediaPlaybackStatusMsg* poPlaybackStatus)
   * \brief   Handles Messages of PlaybackStartMsg type
   * \param   poPlaybackStatus : [IN] poPlaybackStatus message
   **************************************************************************/
   t_Void vHandleMediaStatusMsg(MediaPlaybackStatusMsg* poPlaybackStatus)const;

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleDiscoveryMsg(PlaybackStopMsg..)
   ***************************************************************************/
   /*!
   * \fn      vHandleMetadataStatusMsg(PlaybackStopMsg* poPlaybackStop)
   * \brief   Handles Messages of PlaybackStopMsg type
   * \param   poMLAppName :
   **************************************************************************/
   t_Void vHandleMediaStatusMsg(MediaMetadataStatusMsg* poMetadataStatus)const;

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPMediaPlaybackDispatcher(const spi_tclAAPVideoDispatcher...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPMediaPlaybackDispatcher(
   *                             const spi_tclAAPVideoDispatcher& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPMediaPlaybackDispatcher()
   ***************************************************************************/
   spi_tclAAPMediaPlaybackDispatcher(const spi_tclAAPMediaPlaybackDispatcher& corfoSrc);


   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoDispatcher& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPMediaPlaybackDispatcher& operator=(
   *                          const spi_tclAAPMediaPlaybackDispatcher& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPVideoDispatcher(const spi_tclAAPVideoDispatcher& otrSrc)
   ***************************************************************************/
   spi_tclAAPMediaPlaybackDispatcher& operator=(const spi_tclAAPMediaPlaybackDispatcher& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/


}; //class spi_tclAAPMediaPlaybackDispatcher



#endif //_SPI_TCLAAPMEDIAPLAYBACKDISPATCHER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
