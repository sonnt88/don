/*!
 *******************************************************************************
 * \file              spi_tclMediaPlaybackCbs.cpp
 * \brief             MediaPlaybackStatus Endpoint for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    MediaPlaybackStatusEndpoint for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 25.05.2015 |  Vinoop	     	   | Initial Version

 \endverbatim
 ******************************************************************************/

#include "StringHandler.h"
#include "spi_tclAAPMsgQInterface.h"
#include "spi_tclAAPMediaPlaybackDispatcher.h"
#include "spi_tclMediaPlaybackCbs.h"
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
#include "trcGenProj/Header/spi_tclMediaPlaybackCbs.cpp.trc.h"
#endif
#endif

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclMediaPlaybackCbs::spi_tclMediaPlaybackStatusEndpoint();
 ***************************************************************************/
spi_tclMediaPlaybackCbs::spi_tclMediaPlaybackCbs()
{
	ETG_TRACE_USR1(("spi_tclMediaPlaybackStatusEndpoint Entered  "));

}

/***************************************************************************
 ** FUNCTION:  spi_tclMediaPlaybackCbs::~spi_tclMediaPlaybackStatusEndpoint();
 ***************************************************************************/
spi_tclMediaPlaybackCbs::~spi_tclMediaPlaybackCbs()
{
	ETG_TRACE_USR1(("~spi_tclMediaPlaybackStatusEndpoint Entered  "));
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediaPlaybackCbs::mediaPlaybackStatusCallback();
 ***************************************************************************/
int spi_tclMediaPlaybackCbs::mediaPlaybackStatusCallback(struct MediaPlaybackStatusStruct status)
{
	ETG_TRACE_USR1(("mediaPlaybackStatusCallback Entered  "));

	MediaPlaybackStatusMsg oMediaPlaybackStatusMsg;
	trAAPMediaPlaybackStatus rAAPMediaPlaybackPlaybackStatus;
	rAAPMediaPlaybackPlaybackStatus.u32State = status.state;
	/*rAAPMediaPlaybackPlaybackStatus.szMediaSource = status.media_source.c_str();*/
	strcpy(rAAPMediaPlaybackPlaybackStatus.cMediaSource, status.media_source.c_str());
	rAAPMediaPlaybackPlaybackStatus.u32PlaybackSeconds = status.playback_seconds;
	rAAPMediaPlaybackPlaybackStatus.bShuffle =  status.shuffle;
	rAAPMediaPlaybackPlaybackStatus.bRepeat = status.repeat;
	rAAPMediaPlaybackPlaybackStatus.bRepeatOne = status.repeat_one;

	*(oMediaPlaybackStatusMsg.m_prAAPMediaPlaybackStatus) = rAAPMediaPlaybackPlaybackStatus;

	spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
	if (NULL != poMsgQinterface)
	   {
		 poMsgQinterface->bWriteMsgToQ(&oMediaPlaybackStatusMsg, sizeof(oMediaPlaybackStatusMsg));
	   }//if (NULL != poMsgQinterface)

	return STATUS_SUCCESS;
}

/***************************************************************************
 ** FUNCTION:  spi_tclMediaPlaybackCbs::mediaPlaybackStatusCallback();
 ***************************************************************************/
int spi_tclMediaPlaybackCbs::mediaPlaybackMetadataCallback(struct MediaPlaybackMetadataStruct metadata)
{
	ETG_TRACE_USR1(("mediaPlaybackMetadataCallback Entered  "));

	MediaMetadataStatusMsg oMediaMetadataStatusMsg;
	if(NULL != oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata)
	{
		strncpy(oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cSong, metadata.song.c_str(),STR_METADATA_LENGTH);
		strncpy(oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cAlbum, metadata.album.c_str(),STR_METADATA_LENGTH);
		strncpy(oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cArtist, metadata.artist.c_str(),STR_METADATA_LENGTH);
		strncpy(oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cAlbum_art, metadata.album_art.c_str(),STR_METADATA_LENGTH);
		strncpy(oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cPlaylist, metadata.playlist.c_str(),STR_METADATA_LENGTH);
		oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->u32DurationSeconds = metadata.duration_seconds;
		oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->u32Rating = metadata.rating;
		
		ETG_TRACE_USR4(("Call backs Song = %s",oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cSong));
		ETG_TRACE_USR4(("Call backs Album = %s",oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cAlbum));
		ETG_TRACE_USR4(("Call backs Artist = %s",oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cArtist));
		ETG_TRACE_USR4(("Call backs Album_art = %s",oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cAlbum_art));
		ETG_TRACE_USR4(("Call backs Playlist = %s",oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->cPlaylist));
	    ETG_TRACE_USR4(("Duration = %d, Rating = %d", oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->u32DurationSeconds ,
	    		oMediaMetadataStatusMsg.m_prAAPMediaPlaybackMetadata->u32Rating));
	}

	spi_tclAAPMsgQInterface *poMsgQinterface = spi_tclAAPMsgQInterface::getInstance();
	if (NULL != poMsgQinterface)
	{
	   poMsgQinterface->bWriteMsgToQ(&oMediaMetadataStatusMsg, sizeof(oMediaMetadataStatusMsg));
	}//if (NULL != poMsgQinterface)
    //! Below traces to be removed after tests

    return STATUS_SUCCESS;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>

