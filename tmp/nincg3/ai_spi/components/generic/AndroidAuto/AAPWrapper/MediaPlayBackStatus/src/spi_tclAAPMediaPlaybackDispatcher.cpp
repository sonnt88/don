
/***********************************************************************/
/*!
* \file    spi_tclAAPMediaPlaybackDispatcher.h
* \brief   Message Dispatcher for MediaMetadata  Messages
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message Dispatcher for MediaMetadata Messages
AUTHOR:         Vinoop U
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
25.05.205  | Vinoop U    			| Initial Version

\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclAAPRespMediaPlayback.h"
#include "spi_tclAAPMediaPlaybackDispatcher.h"


//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_MSGQTHREADER
      #include "trcGenProj/Header/spi_tclAAPMediaPlaybackDispatcher.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
//! Macro to define message dispatch function
#define DEFINE_DISPATCH_MESSAGE_FUNCTION(COMMAND,DISPATCHER)\
t_Void COMMAND::vDispatchMsg(DISPATCHER* poDispatcher)      \
{                                                           \
   if (NULL != poDispatcher)                                \
   {                                                        \
      poDispatcher->vHandleMediaStatusMsg(this);                  \
   }														\
	vDeAllocateMsg();										\
}

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  AAPMediaPlaybackMsgBase::AAPMediaPlaybackMsgBase
***************************************************************************/
AAPMediaPlaybackMsgBase::AAPMediaPlaybackMsgBase()
{
   ETG_TRACE_USR1(("AAPMediaPlaybackMsgBase() entered "));
   vSetServiceID(e32MODULEID_AAPMEDIAPLAYBACK);
}

/***************************************************************************
 ** FUNCTION:  MediaPlaybackStatusMsg::MediaPlaybackStatusMsg
 ***************************************************************************/
MediaPlaybackStatusMsg::MediaPlaybackStatusMsg() :  m_prAAPMediaPlaybackStatus(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MediaPlaybackStatusMsg::MediaPlaybackStatusMsg
 ***************************************************************************/
MediaPlaybackStatusMsg::~MediaPlaybackStatusMsg()
{
   m_prAAPMediaPlaybackStatus = NULL;
}
/***************************************************************************
** FUNCTION:  MediaPlaybackStatusMsg::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MediaPlaybackStatusMsg, spi_tclAAPMediaPlaybackDispatcher);

/***************************************************************************
 ** FUNCTION:  MediaPlaybackStatusMsg::vAllocateMsg
 ***************************************************************************/
t_Void MediaPlaybackStatusMsg::vAllocateMsg()
{
	m_prAAPMediaPlaybackStatus = new trAAPMediaPlaybackStatus;
   SPI_NORMAL_ASSERT(NULL == m_prAAPMediaPlaybackStatus);
}

/***************************************************************************
 ** FUNCTION:  MediaPlaybackStatusMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MediaPlaybackStatusMsg::vDeAllocateMsg()
{
   RELEASE_MEM(m_prAAPMediaPlaybackStatus);
}


/***************************************************************************
 ** FUNCTION:  MediaMetadataStatusMsg::DeviceInfoMsg
 ***************************************************************************/
MediaMetadataStatusMsg::MediaMetadataStatusMsg() :  m_prAAPMediaPlaybackMetadata(NULL)
{
   vAllocateMsg();
}

/***************************************************************************
 ** FUNCTION:  MediaMetadataStatusMsg::DeviceInfoMsg
 ***************************************************************************/
MediaMetadataStatusMsg::~MediaMetadataStatusMsg()
{
   m_prAAPMediaPlaybackMetadata = NULL;
}

/***************************************************************************
** FUNCTION:  MediaMetadataStatusMsg::vDispatchMsg
***************************************************************************/
DEFINE_DISPATCH_MESSAGE_FUNCTION(MediaMetadataStatusMsg, spi_tclAAPMediaPlaybackDispatcher);

/***************************************************************************
 ** FUNCTION:  DeviceInfoMsg::vAllocateMsg
 ***************************************************************************/
t_Void MediaMetadataStatusMsg::vAllocateMsg()
{
   m_prAAPMediaPlaybackMetadata = new trAAPMediaPlaybackMetadata;
   SPI_NORMAL_ASSERT(NULL == m_prAAPMediaPlaybackMetadata);
}

/***************************************************************************
 ** FUNCTION:  MediaMetadataStatusMsg::vDeAllocateMsg
 ***************************************************************************/
t_Void MediaMetadataStatusMsg::vDeAllocateMsg()
{
   RELEASE_MEM(m_prAAPMediaPlaybackMetadata);
}

/***************************************************************************
** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleMediaStatusMsg(PlaybackStartMsg..)
***************************************************************************/
t_Void spi_tclAAPMediaPlaybackDispatcher::vHandleMediaStatusMsg(MediaPlaybackStatusMsg* poPlaybackStatus)const
{
   ETG_TRACE_USR1(("vHandlePlaybackStatusMsg"));
   if (NULL != poPlaybackStatus)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespMediaPlayback,
      e16AAP_MEDIAPLAYBACK_REGID,
      vMediaPlaybackStatusCallback(poPlaybackStatus->m_prAAPMediaPlaybackStatus));
   } // if (NULL != poPlaybackStart)
}


/***************************************************************************
** FUNCTION:  spi_tclAAPVideoDispatcher::vHandleMediaStatusMsg(PlaybackStopMsg..)
***************************************************************************/
t_Void spi_tclAAPMediaPlaybackDispatcher::vHandleMediaStatusMsg(MediaMetadataStatusMsg* poMetadataStatus)const
{
   ETG_TRACE_USR1(("vHandleMetadataStatusMsg"));
   if (NULL != poMetadataStatus)
   {
      CALL_REG_OBJECTS(spi_tclAAPRespMediaPlayback,
      e16AAP_MEDIAPLAYBACK_REGID,
      vMediaPlaybackMetadataCallback(poMetadataStatus->m_prAAPMediaPlaybackMetadata));
   } // if (NULL != poPlaybackStop)
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
