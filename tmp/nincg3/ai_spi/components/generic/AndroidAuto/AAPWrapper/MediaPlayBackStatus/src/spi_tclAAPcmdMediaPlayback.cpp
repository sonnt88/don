/*!
 *******************************************************************************
 * \file              spi_tclAAPcmdMediaPlayback.cpp
 * \brief             MediaMetadata wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    MediaMetadata wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 25.05.2015 |  Vinoop           		   | Initial Version
 11.07.2015 | Ramya Murthy                 | Fix for same session ID being used for multiple Endpoints

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclAAPcmdMediaPlayback.h"
#include "spi_tclAAPSessionDataIntf.h"
#include "SPITypes.h"
#include "spi_tclMediaPlaybackCbs.h"
#include "RespBase.h"
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPcmdMediaPlayback.cpp.trc.h"
   #endif
#endif
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
** FUNCTION:  spi_tclAAPcmdMediaPlayback::spi_tclAAPcmdMediaPlayback()
***************************************************************************/
spi_tclAAPcmdMediaPlayback::spi_tclAAPcmdMediaPlayback():
m_poMediaPlaybackStatusEndpoint(NULL)
{
   ETG_TRACE_USR1((" spi_tclAAPcmdMediaPlayback::spi_tclAAPcmdMediaPlayback entered "));

}

/***************************************************************************
** FUNCTION:  spi_tclAAPcmdMediaPlayback::~spi_tclAAPcmdMediaPlayback()
***************************************************************************/
spi_tclAAPcmdMediaPlayback::~spi_tclAAPcmdMediaPlayback()
{
   ETG_TRACE_USR1((" spi_tclAAPcmdMediaPlayback::~spi_tclAAPcmdMediaPlayback entered "));
   m_poMediaPlaybackStatusEndpoint = NULL;
}


/***************************************************************************
 ** FUNCTION: t_Void spi_tclAAPcmdMediaPlayback::vReportAction()
 ***************************************************************************/
t_Void spi_tclAAPcmdMediaPlayback::vReportAction()
{
	ETG_TRACE_USR1((" spi_tclAAPcmdMediaPlayback::vReportAction entered "));
//	if(NULL != m_poMediaPlaybackStatusEndpoint)
//	{
//		//@TODO
//		//m_poMediaPlaybackStatusEndpoint->reportAction(action);
//	}
}
/***************************************************************************
** FUNCTION:t_Bool spi_tclAAPcmdMediaPlayback::~spi_tclAAPcmdMediaPlayback()
***************************************************************************/
t_Bool spi_tclAAPcmdMediaPlayback::bInitializeMediaPlayback()
{
	/*lint -esym(40,nullptr)nullptr Undeclared identifier */
	ETG_TRACE_USR1((" spi_tclAAPcmdMediaPlayback::bInitializeMediaPlayback entered "));
   //! Get Galreciever using Session Data Interface
   spi_tclAAPSessionDataIntf oSessionDataIntf;
   shared_ptr<GalReceiver> poGalReceiver = oSessionDataIntf.poGetGalReceiver();
   t_Bool bRetVal = false;

   if(poGalReceiver != nullptr)
   {
	   m_poMediaPlaybackStatusEndpoint =
	         new (std::nothrow) MediaPlaybackStatusEndpoint(e32SESSIONID_AAPMEDIAPLAYBACK, poGalReceiver->messageRouter());
      if ((NULL != m_poMediaPlaybackStatusEndpoint))
      {
         m_spoPlaybackStatusCbs = new spi_tclMediaPlaybackCbs();
         m_poMediaPlaybackStatusEndpoint->registerCallbacks(m_spoPlaybackStatusCbs);
         bRetVal = poGalReceiver->registerService(m_poMediaPlaybackStatusEndpoint);
         ETG_TRACE_USR4(("spi_tclAAPcmdMediaPlayback::Registration with GalReceiver : %d\n",ETG_ENUM(BOOL,bRetVal)));
      }
   }
   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPcmdMediaPlayback::vUninitialiseBTEndpoint()
***************************************************************************/
t_Void spi_tclAAPcmdMediaPlayback::vUninitialiseMediaPlayback()
{
   /*lint -esym(40,nullptr)nullptr Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAAPcmdMediaPlayback::vUninitialiseMediaPlayback() entered "));
   m_spoPlaybackStatusCbs = nullptr;
   RELEASE_MEM(m_poMediaPlaybackStatusEndpoint);
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>

