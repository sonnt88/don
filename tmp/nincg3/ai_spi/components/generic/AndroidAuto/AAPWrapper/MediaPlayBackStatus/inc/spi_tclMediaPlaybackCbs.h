/*!
 *******************************************************************************
 * \file              spi_tclMediaPlaybackCbs.h
 * \brief             MediaPlaybackStatus Endpoint for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    MediaPlaybackStatus Endpoint for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author              | Modifications
 20.05.2015 |  Vinoop       	   | Initial Version

 \endverbatim
 ******************************************************************************/
#ifndef SPI_TCLMEDIAPLAYBACKCBS_H_
#define SPI_TCLMEDIAPLAYBACKCBS_H_

/******************************************************************************
 | includes:
 | 1)AAP - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include <semaphore.h>
#include <IMediaPlaybackStatusCallbacks.h>
//#include <MediaPlaybackStatusEndpoint.h>

#include "BaseTypes.h"
#include "AAPTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclMediaPlaybackCbs
 * \brief
 */
class spi_tclMediaPlaybackCbs: public IMediaPlaybackStatusCallbacks
{
public:
   /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclMediaPlaybackCbs::spi_tclMediaPlaybackCbs();
    ***************************************************************************/
   /*!
    * \fn     spi_tclMediaPlaybackCbs()
    * \brief  Default Constructor
    * \sa      ~spi_tclMediaPlaybackCbs()
    **************************************************************************/
	spi_tclMediaPlaybackCbs();

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclMediaPlaybackCbs::~spi_tclMediaPlaybackCbs()
    ***************************************************************************/
   /*!
    * \fn      virtual ~spi_tclMediaPlaybackCbs()
    * \brief   Destructor
    * \sa      spi_tclMediaPlaybackCbs()
    **************************************************************************/
   virtual ~spi_tclMediaPlaybackCbs();

   /***************************************************************************
    ** FUNCTION:   spi_tclMediaPlaybackCbs::~mediaPlaybackStatusCallback()
    ***************************************************************************/
   /*!
    * \fn      t_Void mediaPlaybackStatusCallback (struct MediaPlaybackStatusStruct status)
    * \brief   method to inform the application that the PlaybackStatus.
			   Called when media playback status message comes through.
	* \param   status  : [IN] ccommunicate media playback status to the receiver library. 
    * \sa
    **************************************************************************/
   int mediaPlaybackStatusCallback (struct MediaPlaybackStatusStruct status);

   /***************************************************************************
    ** FUNCTION:   spi_tclMediaPlaybackCbs::~mediaPlaybackMetadataCallback()
    ***************************************************************************/
   /*!
    * \fn       t_Void mediaPlaybackMetadataCallback (struct MediaPlaybackMetadataStruct metadata)
    * \brief	method to inform the application that the PlaybackMetadata
		        Called when media playback metadata message comes through
	* \param   metadata  : [IN] communicate media playback status to the receiver library. 
    * \sa
    **************************************************************************/
   int mediaPlaybackMetadataCallback (struct MediaPlaybackMetadataStruct metadata);

private:

   /***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/
};
#endif
///////////////////////////////////////////////////////////////////////////////
// <EOF>
