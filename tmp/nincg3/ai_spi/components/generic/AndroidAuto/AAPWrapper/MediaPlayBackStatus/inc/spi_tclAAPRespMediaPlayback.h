
/***********************************************************************/
/*!
* \file   spi_tclAAPRespMediaPlayback.h
* \brief  MediaMetadata Call backs output interface
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    MediaMetadata Call backs output interface
AUTHOR:         Vinoop
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
25.05.2015  | Vinoop   			    | Initial Version

\endverbatim
*************************************************************************/
#ifndef _SPI_TCLAAPRESPMEDIAPLAYBACK_H_
#define _SPI_TCLAAPRESPMEDIAPLAYBACK_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "RespBase.h"
#include "AAPTypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class  spi_tclAAPRespMediaPlayback.h
* \brief  Media Metadata info  are updated to Application using this
*****************************************************************************/
class spi_tclAAPRespMediaPlayback:public RespBase
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespMediaPlayback::spi_tclAAPRespMediaPlayback()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespMediaPlayback()
   * \brief   Default Constructor
   * \sa      ~spi_tclAAPRespMediaPlayback()
   **************************************************************************/
	spi_tclAAPRespMediaPlayback():RespBase(e16AAP_MEDIAPLAYBACK_REGID){}

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespMediaPlayback::~spi_tclAAPRespMediaPlayback()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPRespMediaPlayback()
   * \brief   Destructor
   * \sa      spi_tclAAPRespMediaPlayback()
   **************************************************************************/
   virtual ~spi_tclAAPRespMediaPlayback(){}

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespMediaPlayback::vMediaPlaybackStatusCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vMediaPlaybackStatusCallback()
   * \brief   method to update  the MediaPlaybackStatusCallback
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vMediaPlaybackStatusCallback(const trAAPMediaPlaybackStatus* rAAPMediaPlaybackStatus){};

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPRespMediaPlayback::vMediaPlaybackMetadataCallback()
   ***************************************************************************/
   /*!
   * \fn      virtual t_Void vMediaPlaybackMetadataCallback()
   * \brief   method to update the MediaPlaybackMetadataCallback
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vMediaPlaybackMetadataCallback(trAAPMediaPlaybackMetadata *rAAPMediaPlaybackMetadata){};

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespMediaPlayback(const spi_tclAAPRespMediaPlayback...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespMediaPlayback(const spi_tclAAPRespMediaPlayback& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespMediaPlayback()
   ***************************************************************************/
   spi_tclAAPRespMediaPlayback(const spi_tclAAPRespMediaPlayback& corfoSrc);


   /***************************************************************************
   ** FUNCTION:  spi_tclAAPRespMediaPlayback& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPRespMediaPlayback& operator=(const spi_tclAAPRespMediaPlayback& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPRespMediaPlayback(const spi_tclAAPRespMediaPlayback& otrSrc)
   ***************************************************************************/
   spi_tclAAPRespMediaPlayback& operator=(const spi_tclAAPRespMediaPlayback& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/


}; //class spi_tclAAPRespVideo

#endif //_SPI_TCLAAPRESPVIDEO_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
