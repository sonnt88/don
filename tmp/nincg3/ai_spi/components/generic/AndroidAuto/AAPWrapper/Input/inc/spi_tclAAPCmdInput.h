/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdInput.h
 * \brief             Input wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Input wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 13.03.2015 |  Sameer Chandra		       | Initial Version
 25.04.2015 |  Sameer Chandra		       | Wayland Adaptations
 17.07.2015 |  Sameer Chandra              | Added Knob Encoder Implementation.
 05.02.2016 |  Rachana L Achar             | Added trAAPKeyCode

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAAPCMDINPUT_H_
#define SPI_TCLAAPCMDINPUT_H_

#include "SPITypes.h"
#include "AAPTypes.h"
#include "RespBase.h"
#include "spi_tclAAPInputSource.h"

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class spi_tclAAPCmdInput
 * \brief
 */

struct trAAPKeyCode
{
   //! Corresponding Switch Code
   tenKeyCode enKeyCode;
   //!Corresponding AAP Switch Code
   tenAAPkeyCodes enAAPKeyCode;

};

class spi_tclAAPCmdInput
{
public:

   /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPCmdInput::spi_tclAAPCmdInput();
    ***************************************************************************/
   /*!
    * \fn     spi_tclAAPCmdInput()
    * \brief  Default Constructor
    * \sa     spi_tclAAPCmdInput()
    **************************************************************************/
   spi_tclAAPCmdInput();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPCmdInput::~spi_tclAAPCmdInput()
    ***************************************************************************/
   /*!
    * \fn      virtual ~spi_tclAAPCmdInput()
    * \brief   Virtual Destructor
    * \sa      spi_tclAAPCmdInput()
    **************************************************************************/
   ~spi_tclAAPCmdInput();

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclAAPCmdInput::bInitializeInput()
    ***************************************************************************/
   /*!
    * \fn      bInitializeInout()
    * \brief   Initializes the InputSource Endpoint, registers keycodes and touch
    * 	      screen.
    * \param   rAAPInputConfig  : [IN] Display Configuration 
    * \sa      bInitialize()
    **************************************************************************/
   t_Bool bInitializeInput(const trAAPInputConfig& rAAPInputConfig);

   /***************************************************************************
    ** FUNCTION:  virtual spi_tclAAPCmdInput::bUnInitialize()
    ***************************************************************************/
   /*!
    * \fn      bUnInitialize()
    * \brief   Uninitializes the InputSource Endpoint.
    * \sa      bUnInitialize()
    **************************************************************************/
   t_Void bUnInitializeInput();

   /***************************************************************************
    ** FUNCTION: t_Void spi_tclAAPCmdInput::vReportTouch
    ***************************************************************************/
   /*!
    * \fn      vReportTouch(t_U32 u32DeviceHandle,trTouchData &rfrTouchData)
    * \brief   Receives the Touch events and forwards it to Project Specific Endpoint
    * 		  wrapper.
    * \param   u32DeviceHandle  : [IN] unique identifier to AAP device
    * \param   rfrTouchData     : [IN] reference to touch data structure which contains
    *          touch details received /ref trTouchData
    * \param   rScalingAttributes : [IN] Scaling attributes to scale the touch coordinates
    * \retval  NONE
    **************************************************************************/
   t_Void vReportTouch(t_U32 u32DeviceHandle, trTouchData &rfrTouchData,
                       trScalingAttributes rScalingAttributes);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdInput::vReportkey
    ***************************************************************************/
   /*!
    * \fn      vReportkey(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
    tenKeyCode enKeyCode)
    * \brief   Receives hard key events and forwards it to Project Specific Endpoint.
    * \param   u32DeviceHandle : [IN] unique identifier to AAP device
    * \param   enKeyMode       : [IN] indicates keypress or keyrelease
    * \param   enKeyCode       : [IN] unique key code identifier
    * \retval  NONE
    **************************************************************************/
   t_Void vReportkey(t_U32 u32DeviceHandle, tenKeyMode enKeyMode,
               tenKeyCode enKeyCode);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdInput::vReportKnobkey
    ***************************************************************************/
   /*!
    * \fn      vReportKnobkey(t_U32 u32DeviceHandle, t_S32 s32DeltaCnts)
    * \brief   Receives hard key events and forwards it to Project Specific Endpoint.
    * \param   u32DeviceHandle : [IN] unique identifier to AAP device
    * \param   s32DeltaCnts    : [IN] Knob Encoder delta count change
    * \retval  NONE
    **************************************************************************/
   t_Void vReportKnobkey(t_U32 u32DeviceHandle, t_S32 s32DeltaCnts);

protected:

   /***************************************************************************
    *********************************PROTECTED**********************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPCmdInput(const spi_tclAAPCmdInput...
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPCmdInput(const spi_tclAAPCmdInput& corfoSrc)
    * \brief   Copy constructor - Do not allow the creation of copy constructor
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPCmdInput()
    ***************************************************************************/
   spi_tclAAPCmdInput(const spi_tclAAPCmdInput& corfoSrc);

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPCmdInput& operator=( const spi_tclAAPCmdInput...
    ***************************************************************************/
   /*!
    * \fn      spi_tclAAPCmdInput& operator=(const spi_tclAAPCmdInput& corfoSrc))
    * \brief   Assignment operator
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPCmdInput(const spi_tclAAPCmdInput& otrSrc)
    ***************************************************************************/
   spi_tclAAPCmdInput& operator=(const spi_tclAAPCmdInput& corfoSrc);

   /***************************************************************************
    ****************************END OF PROTECTED********************************
    ***************************************************************************/
private:

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdInput::vSetAAPCodes
    ***************************************************************************/
   /*!
    * \fn      vSetAAPCodes()
    * \brief   Maps SPI and AAP gesture/key codes.
    * \param   bIsRotarySupported : [IN] Flag indicating wether the HU supports
    *          Rotary Controllers or not.
    * \retval  NONE
    **************************************************************************/
   t_Void vSetAAPCodes(t_Bool bIsRotarySupported);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdInput::u64GenerateTimeStamp
    ***************************************************************************/
   /*!
    * \fn      u64GenerateTimeStamp()
    * \brief   Generates time stamp to report touch and key events.
    * \param   NONE
    * \retval  NONE
    ***************************************************************************/
   t_U64 u64GenerateTimeStamp();

   /****************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPCmdInput::enGetKeyCode
    ***************************************************************************/
   /*!
    * \fn      enGetKeyCode()
    * \brief   Retrieves AAP key code for corresponding SPI key code.
    * \param   NONE
    * \retval  NONE
    **************************************************************************/
   tenAAPkeyCodes enGetKeyCode(tenKeyCode enSpiKeyCode);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPInputHandler::enGetGestureMode
    ***************************************************************************/
   /*!
    * \fn      enGetGestureMode()
    * \brief   Retrieves AAP gesture code for corresponding SPI gesture code.
    * \param   NONE
    * \retval  NONE
    **************************************************************************/
   tenAAPPointerAction enGetGestureMode(tenTouchMode enMode);

   //! AAP Input Source
   spi_tclAAPInputSource* m_poInputSource;

   //! Keycodes for AndroidAuto
   set<t_S32> m_keyCodes;

   //! Mapping between SPI and AAP keycodes
   std::map<tenKeyCode, tenAAPkeyCodes> m_keyCodeMap;

   //! Mapping between SPI and AAP gesture codes.
   std::map<tenTouchMode, tenAAPPointerAction> m_GestureModeMap;

};
#endif /* SPI_TCLAAPCMDINPUT_H_ */
