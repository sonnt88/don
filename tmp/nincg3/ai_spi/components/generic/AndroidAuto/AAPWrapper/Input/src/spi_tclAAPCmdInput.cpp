/*!
 *******************************************************************************
 * \file              spi_tclAAPCmdInput.cpp
 * \brief             Input wrapper for Android Auto
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Input wrapper for Android Auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 13.03.2015 |  Sameer Chandra              | Initial Version
 25.04.2015 |  Sameer Chandra			   | Wayland Adaptations.
 17.07.2015 |  Sameer Chandra              | Added Knob Encoder Implementation.
 05.02.2016 |  Rachana L Achar             | Added Play and Pause keycodes

 \endverbatim
 ******************************************************************************/
/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "StringHandler.h"
#include "spi_tclAAPCmdInput.h"
#include "spi_tclAAPSessionDataIntf.h"
#include <set>
#include <errno.h>
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_AAPWRAPPER
      #include "trcGenProj/Header/spi_tclAAPCmdInput.cpp.trc.h"
   #endif
#endif

const t_S64 BILLION = 1000000000;
const t_U32 coU32MetaState = 0;


static trAAPKeyCode aKeyCode[] =
{
    { e32DEV_BACKWARD, e32_KEYCODE_BACK },
    { e32DEV_MENU, e32_KEYCODE_ENTER },
    { e32MULTIMEDIA_NEXT, e32_KEYCODE_MEDIA_NEXT },
    { e32MULTIMEDIA_PREVIOUS, e32_KEYCODE_MEDIA_PREVIOUS },
    { e32DEV_SEARCH, e32_KEYCODE_SERACH },
    { e32APP_KEYCODE_MEDIA, e32_KEYCODE_MEDIA},
    { e32APP_KEYCODE_TELEPHONY, e32_KEYCODE_TEL},
    { e32APP_KEYCODE_NAVIGATION, e32_KEYCODE_NAVIGATION,},
    { e32MULTIMEDIA_PLAY, e32_KEYCODE_PLAY},
    { e32MULTIMEDIA_PAUSE, e32_KEYCODE_PAUSE},
};

static trAAPGestureMode aGestureMode[] =
{
    { e8TOUCH_PRESS, e8_ACTION_DOWN },
    { e8TOUCH_RELEASE, e8_ACTION_UP },
    { e8TOUCH_MOVED, e8_ACTION_MOVED },
    { e8TOUCH_MULTI_PRESS, e8_ACTION_POINTER_DOWN },
    { e8TOUCH_MULTI_RELEASE, e8_ACTION_POINTER_UP },
};

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::spi_tclAAPCmdInput()
***************************************************************************/
spi_tclAAPCmdInput::spi_tclAAPCmdInput():m_poInputSource(NULL)
{

  ETG_TRACE_USR1((" spi_tclAAPCmdInput::spi_tclAAPCmdInput entered "));

}
/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::~spi_tclAAPCmdInput()
***************************************************************************/
spi_tclAAPCmdInput::~spi_tclAAPCmdInput()
{
  ETG_TRACE_USR1((" spi_tclAAPCmdInput::~spi_tclAAPCmdInput entered "));
  m_poInputSource = NULL;

}
/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::vReportkey()
***************************************************************************/
t_Void spi_tclAAPCmdInput::vReportkey(t_U32 u32DeviceHandle,tenKeyMode enKeyMode,
		tenKeyCode enKeyCode)
{
  SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);

  ETG_TRACE_USR1(("spi_tclAAPCmdInput::vReportkey entered "));

  if (NULL != m_poInputSource)
    {
      //! Generate Time Stamp for the reported touch event.

      t_U64 u64TimeStamp = u64GenerateTimeStamp();

      //Send the Key report to using InputSource
      t_U32 u32keycode = static_cast<t_U32> (enGetKeyCode(enKeyCode));

      if (e32_KEYCODE_INVALID != u32keycode)
        {
          ETG_TRACE_USR4(("Key Code sent    : Key type = %d , key event = %d", ETG_ENUM(AAP_KEY_TYPE,u32keycode), ETG_ENUM(KEY_MODE,enKeyMode)));
          m_poInputSource->vReportkey(u64TimeStamp, u32keycode, static_cast<t_Bool> (enKeyMode), coU32MetaState);
        }

    }

}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::vProcessKeyEvents()
***************************************************************************/
t_Void spi_tclAAPCmdInput::vReportTouch(t_U32 u32DeviceHandle,trTouchData &rfrTouchData,
                                        trScalingAttributes rScalingAttributes)
{
   SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);
   ETG_TRACE_USR1(("spi_tclAAPCmdInput::vReportTouch entered \n"));

   std::vector<trTouchInfo>::iterator itrTouchInfo;
   std::vector<trTouchCoordinates> vTouchCoord;
   std::vector<trTouchCoordinates>::iterator itrTouchCoord;

   t_U32 u32NumofPointers = rfrTouchData.u32TouchDescriptors;
   ETG_TRACE_USR4(("Number of Pointers to Process    : %d ", u32NumofPointers));

   t_U8 u32TouchCount = 0;

   t_S32 as32TouchAction[u32NumofPointers];
   memset(as32TouchAction,0,sizeof(as32TouchAction));

   t_U32 au32TouchDescriptors[u32NumofPointers];
   memset(au32TouchDescriptors,0,sizeof(au32TouchDescriptors));

   t_U32 au32XCoordinates[u32NumofPointers];
   memset(au32XCoordinates,0,sizeof(au32XCoordinates));

   t_U32 au32YCoordinates[u32NumofPointers];
   memset(au32YCoordinates,0,sizeof(au32YCoordinates));

   for (itrTouchInfo = rfrTouchData.tvecTouchInfoList.begin(); itrTouchInfo
      != rfrTouchData.tvecTouchInfoList.end(); ++itrTouchInfo)
   {
      vTouchCoord = itrTouchInfo->tvecTouchCoordinatesList;

      for (itrTouchCoord = vTouchCoord.begin(); itrTouchCoord
          != vTouchCoord.end(); ++itrTouchCoord)
      {
         tenTouchMode enMode = static_cast<tenTouchMode> (itrTouchCoord->enTouchMode);

         au32TouchDescriptors[u32TouchCount] = static_cast<t_U32> (itrTouchCoord->u8Identifier);

         ETG_TRACE_USR4(("Touch Mode : %d, Identifier : %d ", ETG_ENUM(TOUCH_MODE,enMode),
        		  	  	  	  	  	  	  	  	  	  au32TouchDescriptors[u32TouchCount]));

         t_S32 s32xCoordinate = (itrTouchCoord->s32XCoordinate - rScalingAttributes.s32XStartCoordinate);
         t_S32 s32yCoordinate = (itrTouchCoord->s32YCoordinate - rScalingAttributes.s32YStartCoordinate);
         ETG_TRACE_USR4(("Shifted X-Coordinate : [%d]",s32xCoordinate));

         if ( (( 0 > s32xCoordinate) || (s32xCoordinate > rScalingAttributes.u32ScreenWidth)) ||
              (( 0 > s32yCoordinate) || (s32yCoordinate > rScalingAttributes.u32ScreenHeight)))
         {
            // Only populate the array if the touch coordinates fall in the range after
            // Discard this particular coordinate
            --u32NumofPointers;
            continue;
         }

         au32XCoordinates[u32TouchCount]  =  static_cast<t_U32> ((rScalingAttributes.fWidthScaleValue)*s32xCoordinate);

         au32YCoordinates[u32TouchCount]  =  static_cast<t_U32> ((rScalingAttributes.fHeightScalingValue)*s32yCoordinate);

         as32TouchAction[u32TouchCount]   =  static_cast<t_U32>(enGetGestureMode(enMode));

         ETG_TRACE_USR4(("Touch Action  : %d, at  X Coordinate : %d, Y Coordinate : %d",
                          ETG_ENUM(AAP_POINTER_EVENT,as32TouchAction[u32TouchCount]),
                          au32XCoordinates[u32TouchCount],au32YCoordinates[u32TouchCount]));

         ++u32TouchCount;
      }
   }

   // Now populate the touch coordintes only for the ones that fall inside the region.


   if (NULL != m_poInputSource)
   {
      for (t_U32 u32ReportIndex =0;u32ReportIndex < u32TouchCount;++u32ReportIndex)
      {
         //! Report each touch event individually with all touch descriptors
         //! populated.Generate Time Stamp for the reported touch event.

         t_U64 u64TimeStamp = u64GenerateTimeStamp();
         ETG_TRACE_USR4(("Reporting Touch event at timestamp : %llu \n",u64TimeStamp ));

         m_poInputSource->vReportTouch(u64TimeStamp,u32NumofPointers,au32TouchDescriptors,au32XCoordinates,
                                       au32YCoordinates,as32TouchAction[u32ReportIndex],u32ReportIndex);
      }
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::vProcessKeyEvents()
***************************************************************************/
t_Bool spi_tclAAPCmdInput::bInitializeInput(const trAAPInputConfig& rAAPInputConfig)
{
   t_Bool bInitStatus = false;

   vSetAAPCodes(rAAPInputConfig.bIsRotaryCtrl);
   m_poInputSource = new (std::nothrow) spi_tclAAPInputSource();

   if (NULL != m_poInputSource)
   {
      bInitStatus = m_poInputSource->bInitialize(rAAPInputConfig,m_keyCodes);
   }

   return bInitStatus;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::vProcessKeyEvents()
***************************************************************************/
t_Void spi_tclAAPCmdInput::bUnInitializeInput()
{
   ETG_TRACE_USR1(("spi_tclAAPCmdInput::bUnInitialize() entered "));

   if (NULL != m_poInputSource)
   {
       //! Delete the end-point since it is created for every connection/disconnection.
       m_poInputSource->bUnInitialize();

       //! Clear the key-code set
       m_keyCodes.clear();
   }
   RELEASE_MEM (m_poInputSource); // moved outside NULL check(NULL check is already part of RELEASE_MEM) to make Lint happy.

}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::getAAPcodes()
***************************************************************************/
t_Void spi_tclAAPCmdInput::vSetAAPCodes(t_Bool bIsRotarySupported)
{
   //!Insert keys that are supported by Head Unit.
   m_keyCodes.insert(e32_KEYCODE_BACK);
   m_keyCodes.insert(e32_KEYCODE_MEDIA_NEXT);
   m_keyCodes.insert(e32_KEYCODE_MEDIA_PREVIOUS);
   m_keyCodes.insert(e32_KEYCODE_SERACH);
   m_keyCodes.insert(e32_KEYCODE_MEDIA);
   m_keyCodes.insert(e32_KEYCODE_NAVIGATION);
   m_keyCodes.insert(e32_KEYCODE_TEL);
   m_keyCodes.insert(e32_KEYCODE_PLAY);
   m_keyCodes.insert(e32_KEYCODE_PAUSE);

   if (true == bIsRotarySupported)
   {
      //Based on the configuration bind the Rotary Keys
      m_keyCodes.insert(e32_KEYCODE_ROTARY_CONTROLLER);
      m_keyCodes.insert(e32_KEYCODE_ENTER);
   }


   //! populate mapping between SPI and AAP key-codes
   for (tU32 u32KeyIndex = 0; u32KeyIndex < ((sizeof(aKeyCode)) / sizeof(trAAPKeyCode)); ++u32KeyIndex)
   {
      m_keyCodeMap.insert(std::pair<tenKeyCode, tenAAPkeyCodes>(
        aKeyCode[u32KeyIndex].enKeyCode, aKeyCode[u32KeyIndex].enAAPKeyCode));
   }

   //! populate mapping between SPI and AAP Gesture codes
   for (tU32 u32GestureIndex = 0; u32GestureIndex < ((sizeof(aGestureMode)) / sizeof(trAAPGestureMode)); ++u32GestureIndex)
   {
      m_GestureModeMap.insert(std::pair<tenTouchMode, tenAAPPointerAction>(
        aGestureMode[u32GestureIndex].enTouchMode, aGestureMode[u32GestureIndex].enAAPPointerAction));
   }
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::u64GenerateTimeStamp()
***************************************************************************/
t_U64 spi_tclAAPCmdInput::u64GenerateTimeStamp()
{
   struct timespec start;
   if (clock_gettime(CLOCK_REALTIME, &start) == -1)
   {
      ETG_TRACE_ERR(("spi_tclAAPCmdInput::generateTimeStamp()Failed with error:%d ",errno));
   }
   t_U64 u64TimeinSec = start.tv_sec;
   t_U64 u64TimeinNanoSec = start.tv_nsec;
   t_U64 u64TimeStampNano = (((u64TimeinSec * BILLION) + u64TimeinNanoSec));

   return u64TimeStampNano;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::enGetKeycodes()
***************************************************************************/
tenAAPkeyCodes spi_tclAAPCmdInput::enGetKeyCode(tenKeyCode enSpiKeyCode)
{
  tenAAPkeyCodes enAAPKeyCode = e32_KEYCODE_INVALID;
  std::map<tenKeyCode, tenAAPkeyCodes>::const_iterator itrKeyCode;

  itrKeyCode = m_keyCodeMap.find(enSpiKeyCode);
  if (m_keyCodeMap.end() != itrKeyCode)
    {
      enAAPKeyCode = itrKeyCode->second;
    }
  return enAAPKeyCode;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::enGetGestureMode()
***************************************************************************/
tenAAPPointerAction spi_tclAAPCmdInput::enGetGestureMode (tenTouchMode enMode)
{
  tenAAPPointerAction enAAPPointerAction = e8_ACTION_INVALID;
  std::map<tenTouchMode, tenAAPPointerAction>::const_iterator itrPoniterMode;

  itrPoniterMode = m_GestureModeMap.find(enMode);
  if (m_GestureModeMap.end() != itrPoniterMode)
  {
     enAAPPointerAction = itrPoniterMode->second;
  }
 
  return enAAPPointerAction;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPCmdInput::vReportKnobkey()
***************************************************************************/
t_Void spi_tclAAPCmdInput::vReportKnobkey(t_U32 u32DeviceHandle, t_S32 s32DeltaCnts)
{
  SPI_INTENTIONALLY_UNUSED(u32DeviceHandle);

  ETG_TRACE_USR1(("spi_tclAAPCmdInput::vReportKnobkey entered "));

  if ( NULL != m_poInputSource )
  {
     //! Generate Time Stamp for the reported touch event.

     t_U64 u64TimeStamp = u64GenerateTimeStamp();

     ETG_TRACE_USR4(("For Device %d: Knob Key Code sent with delta Count [%d] ",u32DeviceHandle,s32DeltaCnts));
     m_poInputSource->vReportKnobkey(u64TimeStamp, e32_KEYCODE_ROTARY_CONTROLLER, s32DeltaCnts);
  }
}
