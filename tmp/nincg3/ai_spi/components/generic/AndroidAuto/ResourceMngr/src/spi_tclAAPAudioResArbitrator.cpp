/***********************************************************************/
/*!
* \file         spi_tclAAPAudioResArbitrator.h
* \brief
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:
AUTHOR:         Shihabudheen P M
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shihabudheen P M      | Initial Version 
06.05.2015  |  Ramya Murthy         | Impl. to send GUIDANCE_ONLY for transient focus requests
12.05.2015  | Ramya Murthy          | Moved resource arbitration logic to config file
09.07.2015  | Ramya Murthy          | Fix for No iPod audio while Android Auto active (GMMY17-3482)
                                      - take/borrow audio focus if audio is no longer available for MD

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include "spi_tclAAPAudioResArbitrator.h"

//! Initialise table for Device audio focus requests
#define AAP_AUDIO_FOCUS_RESPONSE
static trAAPAudioResponseContext rAAPAudioRespContext[] =
#include "spi_tclAAPContext.cfg"
#undef AAP_AUDIO_FOCUS_RESPONSE

//! Initialise table for audio focus notifications to Device
#define AAP_AUDIO_FOCUS_NOTIFICATION
static trAAPAudioNotifContext rAAPAudioNotifContext[] =
#include "spi_tclAAPContext.cfg"
#undef AAP_AUDIO_FOCUS_NOTIFICATION

static t_Bool sbMDMediaPlayPending = false;

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
      #include "trcGenProj/Header/spi_tclAAPAudioResArbitrator.cpp.trc.h"
   #endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudioResArbitrator::spi_tclAAPAudioResArbitrator()
 ***************************************************************************/
spi_tclAAPAudioResArbitrator::spi_tclAAPAudioResArbitrator(
      trAAPAudioRsrcMngrCallbacks rAudioRsrcMngrCbs):
      m_rAudioRsrcMngrCbs(rAudioRsrcMngrCbs),
      m_enCurAccFocusState(e8_CAR_FOCUS_STATE_GAINED),
      m_enCurAccAudContextType(e8_AUDIO_CTXT_MAIN),
      m_enCurAudioCntxt(e8SPI_AUDIO_MAIN)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioResArbitrator() entered"));
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudioResArbitrator::~spi_tclAAPAudioResArbitrator()
 ***************************************************************************/
spi_tclAAPAudioResArbitrator::~spi_tclAAPAudioResArbitrator()
{
   ETG_TRACE_USR1(("~spi_tclAAPAudioResArbitrator() entered"));
}


/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPAudioResArbitrator::vOnAudioFocusRequest()
 ***************************************************************************/
t_Void spi_tclAAPAudioResArbitrator::vOnAudioFocusRequest(
      tenAAPDeviceAudioFocusRequest enDevAudFocusRequest,
      t_Bool Unsolicited)
{
   /*lint -esym(40,fvSetAudioFocus)fvSetAudioFocus Undeclared identifier */
   /*lint -esym(40,_1) _1 Undeclared identifier */
   /*lint -esym(40,_2) _2 Undeclared identifier */
   /*lint -esym(746,fvSetAudioFocus) function fvSetAudioFocus() not made in the presence of a prototype */
   ETG_TRACE_USR1(("spi_tclAAPAudioResArbitrator::vOnAudioFocusRequest :: ReqType :%d ",
      ETG_ENUM(DEVICE_AUDIOFOCUS_REQ, enDevAudFocusRequest)));

   //! Validate audio focus request & determine response
   tenAccessoryAudioFocusState enNewAccAudFocusState = e8_CAR_FOCUS_STATE_GAINED; // to store the latest state change.
   tenAAPDeviceAudioFocusState enNewDevAudFocusState = e8_FOCUS_STATE_LOSS;

   m_oAudioRsrcMngrCbLock.s16Lock();

   if (false == bGetAudioFocusResponse(enDevAudFocusRequest, enGetCurrentAccFocusState(), enGetCurrentAccAudioCtxtType(),
         enNewDevAudFocusState, enNewAccAudFocusState))
   {
      ETG_TRACE_USR3(("spi_tclAAPAudioResArbitrator::vOnAudioFocusRequest: No response found in map. Sending default. "));
      enNewDevAudFocusState = e8_FOCUS_STATE_LOSS;
      enNewAccAudFocusState = e8_CAR_FOCUS_STATE_GAINED;
   }

   //!Set flag that accessory has responded for GAIN request and device is in GAIN state
   if((e8_AUDIO_FOCUS_REQ_GAIN == enDevAudFocusRequest)
            && (e8_FOCUS_STATE_GAIN == enNewDevAudFocusState))
   {
      //! Set the flag if main audio is not allocated to SPI
      sbMDMediaPlayPending = !((e8SPI_AUDIO_MAIN == m_enCurAudioCntxt) && (false == m_bCurCtxtFlag));
   }

   ETG_TRACE_USR4(("spi_tclAAPAudioResArbitrator::bAudioFocusRequest :: sbMDMediaPlayPending :%d ",
            ETG_ENUM(BOOL, sbMDMediaPlayPending)));

   //! Store latest accessory focus state
   vSetCurrentAccFocusState(enNewAccAudFocusState);

   //! Send response to device
   if (NULL != m_rAudioRsrcMngrCbs.fvSetAudioFocus)
   {
      m_rAudioRsrcMngrCbs.fvSetAudioFocus(enNewDevAudFocusState, Unsolicited);
   }
   m_oAudioRsrcMngrCbLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPAudioResArbitrator::vSetAccessoryAudioContext()
***************************************************************************/
t_Void spi_tclAAPAudioResArbitrator::vSetAccessoryAudioContext(
   const tenAudioContext coenAudioCntxt, t_Bool bReqFlag)
{
   m_oAudioRsrcMngrCbLock.s16Lock();

   tenAccessoryAudioFocusState enCurAccFocusState = enGetCurrentAccFocusState();

   ETG_TRACE_USR1(("spi_tclAAPAudioResArbitrator::vSetAccessoryAudioContext :: "
         "AudioCntxt :%d, bReqFlag : %d, nCurAccFocusState : %d sbMDMediaPlayPending %d",
         ETG_ENUM(AUDIO_CONTEXT, coenAudioCntxt), ETG_ENUM(BOOL, bReqFlag),
         ETG_ENUM(NATIVE_AUDIOFOCUS, enCurAccFocusState), ETG_ENUM(BOOL, sbMDMediaPlayPending)));

   //! Process audio context update only if there is a change in audio context or flag.
   if ((coenAudioCntxt != m_enCurAudioCntxt) || (bReqFlag != m_bCurCtxtFlag))
   {
      m_enCurAudioCntxt = coenAudioCntxt;
      m_bCurCtxtFlag = bReqFlag;

      tenAAPDeviceAudioFocusState enNewDevAudFocusState = e8_FOCUS_STATE_LOSS;
      tenAccessoryAudioFocusState enNewAccAudFocusState = enCurAccFocusState;
      tenAudioResourceOwner enAudResourceOwner = (true == bReqFlag)?
           e8AUDIO_OWNER_CAR : e8AUDIO_OWNER_DEVICE;
		   
      //! Evaluate audio context (Main/Transient/Mix)
      tenAudioContextType enAudContextType = e8_AUDIO_CTXT_MAIN; //Initialised to avoid Lint warning
      t_Bool bValidAudioContext = true;
      switch (coenAudioCntxt)
      {
         case e8SPI_AUDIO_MAIN:
         case e8SPI_AUDIO_INTERNET_APP:
         {
            enAudContextType = e8_AUDIO_CTXT_MAIN;
            vSetCurrentAccAudioCtxtType(e8_AUDIO_CTXT_MAIN); 
         }
            break;
         case e8SPI_AUDIO_SPEECH_REC:
         case e8SPI_AUDIO_ADVISOR_PHONE:
         case e8SPI_AUDIO_EMER_PHONE:
         case e8SPI_AUDIO_PHONE:
         case e8SPI_AUDIO_INCOM_TONE:
         case e8SPI_AUDIO_SYNC_MSG:
         case e8SPI_AUDIO_ASYNC_MSG:
         case s8SPI_AUDIO_TRAFFIC:
         case e8SPI_AUDIO_LVM:
         case e8SPI_AUDIO_CUE:
         {
            enAudContextType = e8_AUDIO_CTXT_TRANSIENT;
            (bReqFlag) ? vSetCurrentAccAudioCtxtType(e8_AUDIO_CTXT_TRANSIENT):
                  vSetCurrentAccAudioCtxtType(e8_AUDIO_CTXT_MAIN);
         }
            break;
         case e8SPI_AUDIO_MIX_ALERT_MSG:
         {
            enAudContextType = e8_AUDIO_CTXT_TRANSIENT_MIX;
            (bReqFlag) ? vSetCurrentAccAudioCtxtType(e8_AUDIO_CTXT_TRANSIENT_MIX):
                  vSetCurrentAccAudioCtxtType(e8_AUDIO_CTXT_MAIN);
         }
            break;
         case e8SPI_AUDIO_UNKNOWN:
         {
            //Temporarily used to mock audio loss to phone when it loses main audio channel
            //@Note: This audio context is not stored, since it is an invalid context.
            //The last valid audio context remains stored, to validate next audio focus request or notification to phone.
            if (e8_AUDIO_CTXT_TRANSIENT_MIX == enGetCurrentAccAudioCtxtType())
            {
               //@Note: If phone does not have main audio & native is playing on mix, accessory gains audio focus.
               //Hence audio focus LOSS needs to be sent to phone.
               enAudContextType = e8_AUDIO_CTXT_MAIN;
            }
            else
            {
               //@Note: If phone does not have main audio, phone should lose focus transiently, until
               //next audio context is known.
               enAudContextType = e8_AUDIO_CTXT_TRANSIENT;
            }
         }
            break;
         case e8SPI_AUDIO_NONE:
         {
            enAudContextType = e8_AUDIO_CTXT_MAIN;
            vSetCurrentAccAudioCtxtType(e8_AUDIO_CTXT_MAIN);
            //Note: This is used to reset native audio context to main when there is no channel active.
            //Audio focus should not be updated to phone in this case.
            bValidAudioContext = false;
         }
            break;
         case e8SPI_AUDIO_EMER_MSG:
         case e8SPI_AUDIO_SHORT_MIX_ALERT_MSG:
         case e8SPI_AUDIO_ALERT_TONE:
         default:
            bValidAudioContext = false;
            break;
      }//switch (coenAudioCntxt)

      //! Main audio source changed => Do not Notify LOSS for MAIN channel if playback start from MD is not yet received
      //! when MD has FOCUS GAIN. If MD Media is already playing, Notify Audio focus LOSS when Main channel is taken over by HU.
      //! But if audio is allocated to MD, audio focus update maybe required, and should be checked in notification table.
      t_Bool bAudFocusUpdReqd =
               (e8SPI_AUDIO_MAIN == coenAudioCntxt) ?
               (((true == bReqFlag) && (false == sbMDMediaPlayPending)) || (false == bReqFlag)) :
               (true);

      ETG_TRACE_USR4(("vSetAccessoryAudioContext :: bAudFocusUpdReqd :%d ",
               ETG_ENUM(BOOL, bAudFocusUpdReqd)));

      //!Clear the flag once channel status update has been received and any channel is allocated for SPI.
      sbMDMediaPlayPending = (e8SPI_AUDIO_MAIN == coenAudioCntxt)?(false):(sbMDMediaPlayPending);

      //! Notify audio focus state to device if state has changed
      if (
         (true == bValidAudioContext)
         &&
         (true == bGetAudioFocusNotification(enAudResourceOwner, enAudContextType,
                  enCurAccFocusState, enNewDevAudFocusState, enNewAccAudFocusState))
         )
      {
         vSetCurrentAccFocusState(enNewAccAudFocusState);

         //!Do not update Focus to Device.
         //!Use case : Device has requested for GAIN focus and Accessory responses with STATE_GAIN
         //!But not yet received MediaPlaybackStartCallback to play Media.
         //!Meantime last Accessory audio source has been restored(FM,USB Media),then avoid sending STATE_LOSS
         //!so that accessory receives MediaPlaybackStartCallback
         //!Native Media playing->start Device VR->Play Device Media
         if ((true == bAudFocusUpdReqd) && (NULL != m_rAudioRsrcMngrCbs.fvSetAudioFocus))
         {
            m_rAudioRsrcMngrCbs.fvSetAudioFocus(enNewDevAudFocusState, true);
         }
      }
      else
      {
         ETG_TRACE_USR3(("spi_tclAAPAudioResArbitrator::vSetAccessoryAudioContext: No change in audio state "));
      }
   }//if ((coenAudioCntxt != m_enCurAudioCntxt) && (bReqFlag != m_bCurCtxtFlag))
   m_oAudioRsrcMngrCbLock.vUnlock();
}

/***************************************************************************
** FUNCTION: spi_tclAAPAudioResArbitrator::bGetAudioFocusResponse()
***************************************************************************/
t_Bool spi_tclAAPAudioResArbitrator::bGetAudioFocusResponse(
         tenAAPDeviceAudioFocusRequest enDevAudFocusRequest,
         tenAccessoryAudioFocusState enCurAccAudFocusState,
         tenAudioContextType enCurAccAudContext,
         tenAAPDeviceAudioFocusState& rfeNewDevAudFocusState,
         tenAccessoryAudioFocusState& rfenNewAccAudFocusState)
{
   t_Bool bRetVal = false;

   t_U32 u32ContainerSize = (sizeof(rAAPAudioRespContext))/(sizeof(trAAPAudioResponseContext));

   //@Note: Currently GAIN_TRANSIENT and GAIN_TRANSIENT_MAY_DUCK are treated as same.
   //Hence config tabel contains only one entry for both types to avoid duplicate entries.
   if (e8_AUDIO_FOCUS_REQ_GAIN_TRANSIENT_MAY_DUCK == enDevAudFocusRequest)
   {
      enDevAudFocusRequest = e8_AUDIO_FOCUS_REQ_GAIN_TRANSIENT;
   }

   //! Iterate through table to find match based on audio focus request, 
   //! current accessory audio focus and accessory transient audio state
   for(t_U8 u8index = 0; u8index < u32ContainerSize; u8index++)
   {
      if (
         (enDevAudFocusRequest == rAAPAudioRespContext[u8index].enDevAudFocusRequest)
         &&
         (enCurAccAudFocusState == rAAPAudioRespContext[u8index].enCurAccAudFocusState)
         &&
         (enCurAccAudContext == rAAPAudioRespContext[u8index].enCurAccAudContext)
         )
      {
         bRetVal = true;
         rfeNewDevAudFocusState = rAAPAudioRespContext[u8index].eNewDevAudFocusState;
         rfenNewAccAudFocusState = rAAPAudioRespContext[u8index].enNewAccAudFocusState;
         // stop the process one search hits at the desired value
         break;
      }
   }

   ETG_TRACE_USR2(("spi_tclAAPAudioResArbitrator::bGetAudioFocusResponse responded with :: "
              "Success: %d, NewDevAudFocusState :%d, NewAccAudFocusState : %d "
              "(for DevAudFocusRequest : %d, CurAccAudFocusState : %d, CurAccAudContext : %d)",
              ETG_ENUM(BOOL, bRetVal),
              ETG_ENUM(DEVICE_AUDIOFOCUS, rfeNewDevAudFocusState),
              ETG_ENUM(NATIVE_AUDIOFOCUS, rfenNewAccAudFocusState),
              ETG_ENUM(DEVICE_AUDIOFOCUS_REQ, enDevAudFocusRequest),
              ETG_ENUM(NATIVE_AUDIOFOCUS, enCurAccAudFocusState),
              ETG_ENUM(AUDIO_CONTEXT_TYPE, enCurAccAudContext)));

   return bRetVal;
}

/***************************************************************************
** FUNCTION: spi_tclAAPAudioResArbitrator::bGetAudioFocusNotification()
***************************************************************************/
t_Bool spi_tclAAPAudioResArbitrator::bGetAudioFocusNotification(
         tenAudioResourceOwner enAudResourceOwner,
         tenAudioContextType enAudContextType,
         tenAccessoryAudioFocusState enCurAccAudFocusState,
         tenAAPDeviceAudioFocusState& rfeNewDevAudFocusState,
         tenAccessoryAudioFocusState& rfenNewAccAudFocusState)
{
   t_Bool bRetVal = false;

   t_U32 u32ContainerSize = (sizeof(rAAPAudioNotifContext))/(sizeof(trAAPAudioNotifContext));

   //! Iterate through table to find match based on current audio resource owner, 
   //! audio context type and accessory audio focus.
   for(t_U8 u8index = 0; u8index < u32ContainerSize; u8index++)
   {
      if (
         (enAudResourceOwner == rAAPAudioNotifContext[u8index].enAudResourceOwner)
         &&
         (enAudContextType == rAAPAudioNotifContext[u8index].enAudContextType)
         &&
         (enCurAccAudFocusState == rAAPAudioNotifContext[u8index].enCurAccAudFocusState)
         )
      {
         bRetVal = true;
         rfeNewDevAudFocusState = rAAPAudioNotifContext[u8index].eNewDevAudFocusState;
         rfenNewAccAudFocusState = rAAPAudioNotifContext[u8index].enNewAccAudFocusState;
         // stop the process one search hits at the desired value
         break;
      }
   }

   ETG_TRACE_USR2(("spi_tclAAPAudioResArbitrator::bGetAudioFocusNotification responded with :: "
              "Success: %d, NewDevAudFocusState :%d, NewAccAudFocusState : %d "
              "(for CurAudResourceOwner : %d, CurAudContextType : %d, CurAccAudFocusState : %d)",
              ETG_ENUM(BOOL, bRetVal),
              ETG_ENUM(DEVICE_AUDIOFOCUS, rfeNewDevAudFocusState),
              ETG_ENUM(NATIVE_AUDIOFOCUS, rfenNewAccAudFocusState),
              ETG_ENUM(AUDIO_RSRC_OWNER, enAudResourceOwner),
              ETG_ENUM(AUDIO_CONTEXT_TYPE, enAudContextType),
              ETG_ENUM(NATIVE_AUDIOFOCUS, enCurAccAudFocusState)));

   return bRetVal;
}

/***************************************************************************
** FUNCTION:  tenAccessoryAudioFocusState spi_tclAAPAudioResArbitrator::enGetCurrentAccFocusState()
***************************************************************************/
tenAccessoryAudioFocusState spi_tclAAPAudioResArbitrator::enGetCurrentAccFocusState()
{
   //! Read current accessory audio focus state
   ETG_TRACE_USR2(("spi_tclAAPAudioResArbitrator::enGetCurrentAccFocusState left with: %d ",
         ETG_ENUM(NATIVE_AUDIOFOCUS, m_enCurAccFocusState)));
   return m_enCurAccFocusState;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPAudioResArbitrator::vSetCurrentAccFocusState()
***************************************************************************/
t_Void spi_tclAAPAudioResArbitrator::vSetCurrentAccFocusState(
      tenAccessoryAudioFocusState enNewAccAudFocusState)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioResArbitrator::vSetCurrentAccFocusState: %d ",
         ETG_ENUM(NATIVE_AUDIOFOCUS, enNewAccAudFocusState)));

   //! Store current accessory audio focus state
   m_enCurAccFocusState = enNewAccAudFocusState;
}

/***************************************************************************
** FUNCTION:  tenAudioContextType spi_tclAAPAudioResArbitrator::enGetCurrentAccAudioCtxtType()
***************************************************************************/
tenAudioContextType spi_tclAAPAudioResArbitrator::enGetCurrentAccAudioCtxtType()
{
   //! Read current accessory audio context type
   ETG_TRACE_USR2(("spi_tclAAPAudioResArbitrator::enGetCurrentAccAudioCtxtType left with: %d ",
         ETG_ENUM(AUDIO_CONTEXT_TYPE, m_enCurAccAudContextType)));

   return m_enCurAccAudContextType;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPAudioResArbitrator::vSetCurrentAccAudioCtxtType()
***************************************************************************/
t_Void spi_tclAAPAudioResArbitrator::vSetCurrentAccAudioCtxtType(
      tenAudioContextType enNewAccAudioCtxtType)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioResArbitrator::vSetCurrentAccAudioCtxtType: %d ",
         ETG_ENUM(AUDIO_CONTEXT_TYPE, enNewAccAudioCtxtType)));

   //! Set current accessory audio context type
   m_enCurAccAudContextType = enNewAccAudioCtxtType;
}

//lint –restore
