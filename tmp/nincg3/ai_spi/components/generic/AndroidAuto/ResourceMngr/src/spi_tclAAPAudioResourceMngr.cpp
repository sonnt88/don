/***********************************************************************/
/*!
* \file         spi_tclAAPAudioResourceMngr.cpp
* \brief
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Ramya Murthy          | Initial Version
28.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors
\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclAAPAudioResourceMngr.h"
#include "spi_tclAAPAudioResArbitrator.h"
#include "spi_tclAAPManager.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
      #include "trcGenProj/Header/spi_tclAAPAudioResourceMngr.cpp.trc.h"
   #endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclAAPAudioResourceMngr::spi_tclAAPAudioResourceMngr()
***************************************************************************/
spi_tclAAPAudioResourceMngr::spi_tclAAPAudioResourceMngr():
   m_poAudioResArb(NULL),
   m_u32CurSelectedDevId(0)
{
	/*lint -esym(40,_1)_1 Undeclared identifier */
	/*lint -esym(40,_2)_2 Undeclared identifier */
    ETG_TRACE_USR1(("spi_tclAAPAudioResourceMngr() entered "));

   spi_tclAAPManager* poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      //! Register with AAP manager for Session callbacks
      poAAPManager->bRegisterObject((spi_tclAAPRespSession*)this);
   }//if(NULL != poAAPManager)
   
   trAAPAudioRsrcMngrCallbacks rAudioRsrcMngrCbs;
   rAudioRsrcMngrCbs.fvSetAudioFocus = std::bind(
         &spi_tclAAPAudioResourceMngr::vSetAudioFocus,
         this,
         SPI_FUNC_PLACEHOLDERS_2);

   m_poAudioResArb = new spi_tclAAPAudioResArbitrator(rAudioRsrcMngrCbs);
   SPI_NORMAL_ASSERT(NULL == m_poAudioResArb);
}

/***************************************************************************
** FUNCTION:  spi_tclAAPAudioResourceMngr::~spi_tclAAPAudioResourceMngr()
***************************************************************************/
spi_tclAAPAudioResourceMngr::~spi_tclAAPAudioResourceMngr()
{
   ETG_TRACE_USR1(("~spi_tclAAPAudioResourceMngr() entered"));
   RELEASE_MEM(m_poAudioResArb);
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudioResourceMngr::vAudioFocusRequestCb()
 ***************************************************************************/
t_Void spi_tclAAPAudioResourceMngr::vAudioFocusRequestCb(
      tenAAPDeviceAudioFocusRequest enDevAudFocusRequest)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioResourceMngr::vAudioFocusRequestCb() entered: "
         "Audio focus request %d ",
         ETG_ENUM(DEVICE_AUDIOFOCUS_REQ, enDevAudFocusRequest)));
         
   if(NULL != m_poAudioResArb)
   {
      //! Forward Audio focus request for further processing
      m_poAudioResArb->vOnAudioFocusRequest(enDevAudFocusRequest, false);
   }//if(NULL != m_poAudioResArb)
}

/***************************************************************************
 ** FUNCTION:  t_Void  spi_tclAAPAudioResourceMngr::vOnSPISelectDeviceResult()
 ***************************************************************************/
t_Void spi_tclAAPAudioResourceMngr::vOnSPISelectDeviceResult(t_U32 u32DevID,
                                      tenDeviceConnectionReq enDeviceConnReq,
                                      tenResponseCode enRespCode,
                                      tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1(("spi_tclAAPAudioResourceMngr::vOnSPISelectDeviceResult"));
   SPI_INTENTIONALLY_UNUSED(enRespCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);

   m_u32CurSelectedDevId = (e8DEVCONNREQ_SELECT == enDeviceConnReq) ? (u32DevID) : (0);

   //! Mock release of audio resource by device (Fix for GMMY17-6634)
   if ((e8DEVCONNREQ_DESELECT == enDeviceConnReq) && (NULL != m_poAudioResArb))
   {
      m_poAudioResArb->vOnAudioFocusRequest(e8_AUDIO_FOCUS_REQ_RELEASE, true);
   }//if ((e8DEVCONNREQ_DESELECT == enDeviceConnReq) && (NULL != m_poAudioResArb))
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPAudioResourceMngr::vSetAccessoryAudioContext()
***************************************************************************/
t_Void spi_tclAAPAudioResourceMngr::vSetAccessoryAudioContext(const t_U32 cou32DevId,
   const tenAudioContext coenAudioCntxt,
   t_Bool bReqFlag)
{
   SPI_INTENTIONALLY_UNUSED(cou32DevId);

   //! Forward audio context
   if (NULL != m_poAudioResArb)
   {
      m_poAudioResArb->vSetAccessoryAudioContext(coenAudioCntxt, bReqFlag);
   }//if (NULL != m_poAudioResArb)
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPAudioResourceMngr::vSetAudioFocus()
 ***************************************************************************/
t_Void spi_tclAAPAudioResourceMngr::vSetAudioFocus(
      tenAAPDeviceAudioFocusState enDevAudFocusState, t_Bool bUnsolicited)
{
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if ((NULL != poAAPManager) && (0 != m_u32CurSelectedDevId))
   {
      spi_tclAAPCmdSession* poCmdSession = poAAPManager->poGetSessionInstance();
      if(NULL != poCmdSession)
      {
         ETG_TRACE_USR1(("spi_tclAAPAudioResourceMngr::vSetAudioFocus: "
               "New device audio focus %d, Unsolicited %d ",
               ETG_ENUM(DEVICE_AUDIOFOCUS, enDevAudFocusState),
               ETG_ENUM(BOOL, bUnsolicited)));
         poCmdSession->vSetAudioFocus(enDevAudFocusState, bUnsolicited); //TODO
      } // if(NULL != poCmdSession)
   }//if ((NULL != poAAPManager) && (0 != m_u32CurSelectedDevId))
}
//lint –restore
///////////////////////////////////////////////////////////////////////////////
// <EOF>
