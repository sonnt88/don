
/***********************************************************************/
/*!
* \file    spi_tclAAPVideoResourceMngr.cpp
* \brief   AAP Video Resource Manager
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Video resource manager
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date         | Author                | Modification
20.03.2015   | Shiva Kumar Gurija    | Initial Version
11.04.2015   | Shiva Kumar Gurija    | Handling Video Focus Notifications from HU
04.02.2016  | Shiva Kumar Gurija    | Moved LaunchApp handling from Video to RsrcMngr

\endverbatim
*************************************************************************/


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclResorceMngrDefines.h"
#include "spi_tclAAPManager.h"
#include "spi_tclAAPCmdVideo.h"
#include "spi_tclAAPResourceMngr.h"
#include "spi_tclAAPVideoResourceMngr.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
      #include "trcGenProj/Header/spi_tclAAPVideoResourceMngr.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static spi_tclAAPManager* spoAAPMngr = NULL;
static spi_tclAAPCmdVideo* spoCmdVideo = NULL;

static t_Bool sbHandsetInteractionReqd = false;

static timer_t srDevAuthAndAccessTimerID;
static t_Bool sbDevAuthAndAccessTimerRunning = false;
static const t_U32 cou32DevAuthAndAccessTimerVal = 3000;
static t_Bool sbVideoSetupCbRcvd = false;


#define AAP_VIDEO_HU_REQ_CNTXT
static const trAAPAccVideoFocusNoti sacoAccVideoFocusNoti[]=
#include "spi_tclAAPVideoContext.cfg"
#undef AAP_VIDEO_HU_REQ_CNTXT

#define AAP_VIDEO_HNDL_HU_REQSTATE
static const trAAPVideoFocusState  sacoVideoFocusState[]=
#include "spi_tclAAPVideoContext.cfg"
#undef AAP_VIDEO_HNDL_HU_REQSTATE

#define AAP_VIDEO_MD_FOCUS_REQ
static const trAAPMDVideoFocusReq  sacoMDVideoFocusReq[]=
#include "spi_tclAAPVideoContext.cfg"
#undef AAP_VIDEO_MD_FOCUS_REQ

/***************************************************************************
** FUNCTION:  spi_tclAAPVideoResourceMngr::spi_tclAAPVideoResourceMngr()
***************************************************************************/
spi_tclAAPVideoResourceMngr::spi_tclAAPVideoResourceMngr(
   spi_tclAAPResourceMngr* poAAPRsrcMngr):m_poAAPRsrcMngr(poAAPRsrcMngr),
   m_enCurMDFocusState(e8AAP_VIDEOFOCUSSTATE_LOSS),
   m_enCurAccDispCntxt(e8DISPLAY_CONTEXT_UNKNOWN),
   m_enPlaybackState(e8VID_PB_STATE_STOPPED),
   m_u32SelectedDeviceID(0),
   m_bInitialMDVideoFocus(false)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr() entered "));
   //nothing to do
}

/***************************************************************************
** FUNCTION:  spi_tclAAPVideoResourceMngr::~spi_tclAAPVideoResourceMngr()
***************************************************************************/
spi_tclAAPVideoResourceMngr::~spi_tclAAPVideoResourceMngr()
{
   ETG_TRACE_USR1(("~spi_tclAAPVideoResourceMngr() entered"));

   m_poAAPRsrcMngr   = NULL;
   spoAAPMngr        = NULL;
   spoCmdVideo       = NULL;
   //nothing to do
}
/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bInitialize()
***************************************************************************/
t_Bool spi_tclAAPVideoResourceMngr::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::bInitialize() entered"));

   t_Bool bRet = false;

   spoAAPMngr = spi_tclAAPManager::getInstance();
   SPI_NORMAL_ASSERT(NULL == spoAAPMngr);

   if (NULL != spoAAPMngr)
   {
      spoCmdVideo = spoAAPMngr->poGetVideoInstance();

      bRet=spoAAPMngr->bRegisterObject((spi_tclAAPVideoResourceMngr*) this);
   }//if (NULL != spoAAPMngr)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vUninitialize()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vUnInitialize()
{
   //Nothing to do
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayContext()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayContext(const t_U32 cou32DevId,
                                                                t_Bool bDisplayFlag, 
                                                                tenDisplayContext enDisplayContext)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayContext:Device ID-0x%x Accessory Takes Focus-%d AccDisplayContext-%d"
      , cou32DevId,ETG_ENUM(BOOL,bDisplayFlag),ETG_ENUM(DISPLAY_CONTEXT,enDisplayContext)));

   // do not process the update, if the DisplayFlag is false for a context, which is not active currently.
   if(  ( true == bDisplayFlag ) || 
      ( (false == bDisplayFlag) && (m_enCurAccDispCntxt == enDisplayContext) )  )
   {
      if( true ==  bDisplayFlag )
      {
         m_enCurAccDispCntxt =  enDisplayContext;
      } //if( true ==  bDisplayFlag )

      tenVideoFocusState enCurAccFocusState = e8AAP_VIDEOFOCUSSTATE_UNKNOWN;
      tenVideoFocusState enNewMDFocusState = e8AAP_VIDEOFOCUSSTATE_UNKNOWN;

      m_oCurMDFocusStateLock.s16Lock();
      if(  ( true == bGetAccVideoFocusState(enDisplayContext,bDisplayFlag,enCurAccFocusState) )  &&  
         ( true == bGetUpdatedMDFocusState(m_enCurMDFocusState,enCurAccFocusState,enNewMDFocusState) )  )
      {
         switch(m_enCurMDFocusState)
         {
         case e8AAP_VIDEOFOCUSSTATE_GAIN:
            {
               //HMI does a TAKE or BORROW, when the MD has the screen, SetVideoFocus to Native
               if( ( e8AAP_VIDEOFOCUSSTATE_GAIN == enCurAccFocusState ) || 
                  (e8AAP_VIDEOFOCUSSTATE_GAIN_TRANSIENT == enCurAccFocusState) )
               {
                  vSetVideoFocus(e8VIDEOFOCUS_NATIVE,false);
               }//if( (e8AAP_VIDEOFOCUSSTATE_GAIN == enCurAccFocusState )
            }//case e8AAP_VIDEOFOCUSSTATE_GAIN:
            break;
         case e8AAP_VIDEOFOCUSSTATE_LOSS_TRANSIENT:
            {
               //HMI does a UNBORROW, SetVideoFocus to PROJECTED on the Phone.
              if(e8AAP_VIDEOFOCUSSTATE_LOSS_TRANSIENT == enCurAccFocusState)
               {
                   vSetVideoFocus(e8VIDEOFOCUS_PROJECTED,false);
               }//if(e8AAP_VIDEOFOCUSSTATE_LOSS_TRANSIENT == enCurAccFocusState)
            }//case e8AAP_VIDEOFOCUSSTATE_LOSS_TRANSIENT:
            break;
         default:
            {
               ETG_TRACE_ERR(("[ERR]:vSetAccessoryDisplayContext:default state"));
            }
         }//switch(m_enCurMDFocusState)

         //upadte the MD focus state
         m_enCurMDFocusState = enNewMDFocusState;

      }//if( (true == bGetAccVideoFocusState(enDisplayContext,bDisplayFlag,enCurAccFocusState
      else
      {
         ETG_TRACE_ERR(("[ERR]:vSetAccessoryDisplayContext: Element not found"));
      }
      m_oCurMDFocusStateLock.vUnlock();
   } //if(  ( true == bDisplayFlag ) || 
   else
   {
      ETG_TRACE_USR4(("spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayContext: Current active Context is %d ",
         ETG_ENUM(DISPLAY_CONTEXT,m_enCurAccDispCntxt)));
   }
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bGetAccVideoFocusState()
***************************************************************************/
t_Bool spi_tclAAPVideoResourceMngr::bGetAccVideoFocusState(
   tenDisplayContext enAccDispCntxt,
   t_Bool bDisplayFlag,
   tenVideoFocusState& rfenVideoFocusState)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::bGetAccVideoFocusState: AccDisplayContext-%d Accessory Takes Focus-%d",
      ETG_ENUM(DISPLAY_CONTEXT,enAccDispCntxt),ETG_ENUM(BOOL,bDisplayFlag)));

   t_Bool bRet=false;

   t_U32 u32ContSize = (sizeof(sacoAccVideoFocusNoti))/(sizeof(trAAPAccVideoFocusNoti));
   for(t_U8 u8Index =0; u8Index < u32ContSize; u8Index++)
   {
      // Check for the matching Accessory context entry
      if(enAccDispCntxt == sacoAccVideoFocusNoti[u8Index].enAccDispCntxt)
      {
         bRet = true;

         rfenVideoFocusState = (true == bDisplayFlag)? (sacoAccVideoFocusNoti[u8Index].enAccFocusReqType):
            (sacoAccVideoFocusNoti[u8Index].enAccFocusRelType);

         ETG_TRACE_USR4(("[DESC]:Derived Accessory Focus State based on Accessory display context-%d",
            ETG_ENUM(VIDEOFOCUS_STATE,rfenVideoFocusState)));

         // stop the process once search hits at the desired value
         break;
      } //if(enAccDispCntxt == sacoAccVideoFocusNoti[u8Index].enAccDispCntxt)
   } //for(t_U8 u8Index =0; u8Index < u32ContSize; u8Index++) 

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bGetUpdatedMDFocusState()
***************************************************************************/
t_Bool spi_tclAAPVideoResourceMngr::bGetUpdatedMDFocusState(tenVideoFocusState enCurMDFocusState,
                                                            tenVideoFocusState enCurAccFocusState,
                                                            tenVideoFocusState& rfenUpdatedMDFocusState)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::bGetUpdatedMDFocusState:CurMDFocusState-%d" \
      "Derived Accessory Focus State-%d ",ETG_ENUM(VIDEOFOCUS_STATE,enCurMDFocusState),
      ETG_ENUM(VIDEOFOCUS_STATE,enCurAccFocusState)));

   t_Bool bRet=false;

   t_U32 u32ContSize = (sizeof(sacoVideoFocusState))/(sizeof(trAAPVideoFocusState));
   for(t_U8 u8Index =0; u8Index < u32ContSize; u8Index++)
   {
      if( (enCurMDFocusState == sacoVideoFocusState[u8Index].enCurMDFocusState) && 
         (enCurAccFocusState == sacoVideoFocusState[u8Index].enReqFocusState) )
      {
         bRet = true;

         rfenUpdatedMDFocusState = sacoVideoFocusState[u8Index].enUpdatedMDFocusState;

         ETG_TRACE_USR4(("[PARAM]:bGetUpdatedMDFocusState:ResultedMDFocusState-%d"
            ,ETG_ENUM(VIDEOFOCUS_STATE,rfenUpdatedMDFocusState)));

         // stop the process once search hits at the desired value
         break; 
      } //if( (enCurMDFocusState == sacoVideoFocusState[u8Index]
   } //for(t_U8 u8Index =0; u8Index < u32ContSize; u8Index++)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bGetAccRespType()
***************************************************************************/
t_Bool spi_tclAAPVideoResourceMngr::bGetAccRespType(tenAAPMDVideoFocusReqResp& rfenRespType)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::bGetAccRespType:Current AccDispCntxt-%d Current MDFocusState-%d" ,
      ETG_ENUM(DISPLAY_CONTEXT,m_enCurAccDispCntxt),ETG_ENUM(VIDEOFOCUS_STATE,m_enCurMDFocusState)));

   t_Bool bRet=false;

   t_U32 u32ContSize = (sizeof(sacoMDVideoFocusReq))/(sizeof(trAAPMDVideoFocusReq));
   for(t_U8 u8Index =0; u8Index < u32ContSize; u8Index++)
   {
      if(m_enCurAccDispCntxt == sacoMDVideoFocusReq[u8Index].enAccDispCntxt)
      {
         bRet = true;

         m_oCurMDFocusStateLock.s16Lock();
         rfenRespType = (e8AAP_VIDEOFOCUSSTATE_LOSS == m_enCurMDFocusState) ? 
            sacoMDVideoFocusReq[u8Index].enMDFocusReqResp_StateLoss:
            sacoMDVideoFocusReq[u8Index].enMDFocusReqResp_StateLossTransient;
         m_oCurMDFocusStateLock.vUnlock();

         ETG_TRACE_USR4(("[PARAM]:bGetAccRespType:Accesspry response for MD Video focus request-%d"
            ,ETG_ENUM(VIDEOFOCUS_RESP_TYPE,rfenRespType)));

         // stop the process once search hits at the desired value
         break; 
      } //if( (enCurMDFocusState == sacoMDVideoFocusReq[u8Index]
   } //for(t_U8 u8Index =0; u8Index < u32ContSize; u8Index++)

   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vSetVideoFocus()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vSetVideoFocus(tenVideoFocus enVideoFocus,
                                                   t_Bool bUnsolicited)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vSetVideoFocus:VideoFocusMode-%d Unsolicited-%d",
      ETG_ENUM(VIDEOFOCUS_MODE,enVideoFocus),ETG_ENUM(BOOL,bUnsolicited)));

   //PROJECTED && STOPPED => SetVideoFocus to PROJECTED & State to PB_START_RQSTD
   //NATIVE && STARTED => SetVideoFocus to NATIVE & State to PB_STOP_RQSTD

   t_Bool bSendVideoFocusReq = false;

   m_oVideoPlaybackStateLock.s16Lock();

   ETG_TRACE_USR4(("[PARAM]:vSetVideoFocus:Current Video Playback state - %d",ETG_ENUM(AAP_VID_PB_STATE,m_enPlaybackState)));

   /*
   Request is to set the Video Focus with PROJECTED and the play back state is STOPPED, Then send the request to phone 
   and set the state to request is in progress. 
   Request is received when the Playback state 
   is STOP in progress => delay processing of the  request. That will be processed, when the PlaybackStopCb comes
                          There processing happens, based on the current MD Focus state and it is set
                          upon receiving the SetAccessoryDisplay context update & Launch App requests.
   is STARTED or START in progress => no need to process the update. ignore it. 
   */
   if((e8VIDEOFOCUS_PROJECTED == enVideoFocus) && (e8VID_PB_STATE_STOPPED == m_enPlaybackState))
   {
      bSendVideoFocusReq = true;
      m_enPlaybackState = e8VID_PB_STATE_START_RQSTD;
   }
   /*
   Request is to set the Video Focus with NATIVE and the play back state is STARTED. Then send the request to phone 
   and set the state to request is in progress.
   Request is received when the Playback state 
   is START is in progress => delay processing of the request. That will be processed, when the PlaybackStartCb comes.
                              There processing happens, based on the current MD Focus state and it is set
                              upon receiving the SetAccessoryDisplay context update & Launch App requests.
   is STOPPED or STOP in progress => no need to process the update. ignore it. 
   */   
   else if((e8VIDEOFOCUS_NATIVE == enVideoFocus)&&(e8VID_PB_STATE_STARTED == m_enPlaybackState))
   {
      bSendVideoFocusReq = true;
      m_enPlaybackState = e8VID_PB_STATE_STOP_RQSTD;
   }//else if((e8VIDEOFOCUS_NATIVE == enVideoFocus)&&(
   else
   {
      ETG_TRACE_USR4(("[DESC]:Ignore the request. Some other request is still being processed (or) no need to send request"));
   }
   m_oVideoPlaybackStateLock.vUnlock();

   if((true == bSendVideoFocusReq)&&(NULL != spoCmdVideo))
   {
      spoCmdVideo->vSetVideoFocus(enVideoFocus, bUnsolicited);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vPlaybackStartCallback()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vPlaybackStartCallback()
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vPlaybackStartCallback"));
   m_oVideoPlaybackStateLock.s16Lock();
   m_enPlaybackState = e8VID_PB_STATE_STARTED;
   m_oVideoPlaybackStateLock.vUnlock();

   m_oCurMDFocusStateLock.s16Lock();
   if( (e8AAP_VIDEOFOCUSSTATE_LOSS == m_enCurMDFocusState) || (e8AAP_VIDEOFOCUSSTATE_LOSS_TRANSIENT == m_enCurMDFocusState) )
   {
      //MD was in GAIN state, when the SetVideoFocus with PROJECTED is requested.
      //Due to some user interaction,MD's state changed to LOSS and this has happened, when the last request is being 
      //processed. So request the Phone to release the Video focus.
      ETG_TRACE_USR2(("[DESC]:Accessory has taken Video Focus due to user interactions. Release the Video Focus"));
      vSetVideoFocus(e8VIDEOFOCUS_NATIVE,false);
   }
   //Enable Projection screen
   else if(NULL != m_poAAPRsrcMngr)
   {
      m_poAAPRsrcMngr->vUpdateDeviceDisplayCntxt(true,e8DISPLAY_CONTEXT_UNKNOWN);
   }//else if(NULL != m_poAAPRsrcMngr)

   m_oCurMDFocusStateLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vPlaybackStopCallback()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vPlaybackStopCallback()
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vPlaybackStopCallback"));

   m_oVideoPlaybackStateLock.s16Lock();
   m_enPlaybackState = e8VID_PB_STATE_STOPPED;
   m_oVideoPlaybackStateLock.vUnlock();

   m_oCurMDFocusStateLock.s16Lock();
   if( (e8AAP_VIDEOFOCUSSTATE_GAIN == m_enCurMDFocusState) || (e8AAP_VIDEOFOCUSSTATE_GAIN_TRANSIENT == m_enCurMDFocusState) )
   {
      //MD was in LOSS/LOSS_TRANSIENT state, when the SetVideoFocus with NATIVE request is sent.
      //Due to user interactions, HU released the Video Focus, before the response is received for the last request.
      //In this case, request Phone to take the Video Focus.
      ETG_TRACE_USR2(("[DESC]:Accessory has released the Video Focus due to user interactions. Request Phone for projection"));
      vSetVideoFocus(e8VIDEOFOCUS_PROJECTED,false);
   }
   //Disable Projection screen
   else if(NULL != m_poAAPRsrcMngr)
   {
      m_poAAPRsrcMngr->vUpdateDeviceDisplayCntxt(false,e8DISPLAY_CONTEXT_UNKNOWN);
   }//else if(NULL != m_poAAPRsrcMngr)

   m_oCurMDFocusStateLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vVideoFocusCallback()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vVideoFocusCallback(tenVideoFocus enVideoFocus,
                                                        tenVideoFocusReason enVideoFocusReason)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vVideoFocusCallback: VideoFocusMode-%d VideoFocusReason-%d",
      ETG_ENUM(VIDEOFOCUS_MODE,enVideoFocus),ETG_ENUM(VIDEOFOCUS_REASON,enVideoFocusReason)));

   if(enVideoFocus == e8VIDEOFOCUS_NATIVE)
   {
       m_oCurMDFocusStateLock.s16Lock();
       m_enCurMDFocusState = e8AAP_VIDEOFOCUSSTATE_LOSS;
       m_oCurMDFocusStateLock.vUnlock();

      //Switch to Native UI, whenever the MD requests
      vSetVideoFocus(e8VIDEOFOCUS_NATIVE, true);
      //Switch to Native UI
      if(NULL != m_poAAPRsrcMngr)
      {
         m_poAAPRsrcMngr->vUpdateDeviceDisplayCntxt(false,e8DISPLAY_CONTEXT_UNKNOWN);
      }//if(NULL != m_poAAPRsrcMngr)
   }//if(enVideoFocusMode == e8VIDEOFOCUS_NATIVE)
   else
   {
      //by default set the response to DENY for the Video Focus callback received.
      tenAAPMDVideoFocusReqResp enRespType = e8AAP_VIDEOFOCUS_DENY;

      if( true == bGetAccRespType(enRespType) )
      {
         switch(enRespType)
         {
         case e8AAP_VIDEOFOCUS_GRANT:
            {
               //Video Focus can be granted to Phone in the current context
               m_oCurMDFocusStateLock.s16Lock();
               m_enCurMDFocusState = e8AAP_VIDEOFOCUSSTATE_GAIN ;
               m_oCurMDFocusStateLock.vUnlock();

               vSetVideoFocus(e8VIDEOFOCUS_PROJECTED,true);
            } //case e8AAP_VIDEOFOCUS_GRANT:
            break;
         case e8AAP_VIDEOFOCUS_DENY:
            {
               vSetVideoFocus(e8VIDEOFOCUS_NATIVE,true);
               //Already Native UI is enabled and Phone is requesting for Focus
               //no need to send DeviceDisplayContext with true to HMI
            } //case e8AAP_VIDEOFOCUS_DENY:
            break;
         case e8AAP_VIDEOFOCUS_DELAY:
         default:
            {
               //Delay is not implemented and it's not required at the moment
               ETG_TRACE_ERR(("spi_tclAAPVideoResourceMngr::vVideoFocusCallback: invalid response"));
            }//default:
         }//switch(enGetAccRespType())
      }//if( true == enGetAccRespType(enRespType)
   } //else
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vOnSPISelectDeviceResult()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vOnSPISelectDeviceResult(t_U32 u32DevID,
                                                             tenDeviceConnectionReq enDevConnReq,
                                                             tenResponseCode enRespCode,
                                                             tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vOnSPISelectDeviceResult: Result of -%d ",
      ETG_ENUM(CONNECTION_REQ,enDevConnReq)));

   SPI_INTENTIONALLY_UNUSED(u32DevID);
   SPI_INTENTIONALLY_UNUSED(enRespCode);
   SPI_INTENTIONALLY_UNUSED(enErrorCode);

   vStopTimer();

   //!Start the timer on device selection is successful
   if((e8DEVCONNREQ_SELECT == enDevConnReq) && (e8SUCCESS == enRespCode))
   {
      m_u32SelectedDeviceID = u32DevID;

      m_oVideoSetupLock.s16Lock();
      m_bInitialMDVideoFocus = false;
      sbVideoSetupCbRcvd = false;
      m_oVideoSetupLock.vUnlock();

      vStartTimer();
   }

   if(e8DEVCONNREQ_DESELECT == enDevConnReq)
   {
      m_u32SelectedDeviceID = 0;

      m_oVideoSetupLock.s16Lock();
      m_bInitialMDVideoFocus = false;
      sbVideoSetupCbRcvd = false;
      m_oVideoSetupLock.vUnlock();

      m_oCurMDFocusStateLock.s16Lock();
      m_enCurMDFocusState = e8AAP_VIDEOFOCUSSTATE_LOSS;
      m_oCurMDFocusStateLock.vUnlock();

      m_oVideoPlaybackStateLock.s16Lock();
      //Set the Video Focus request state to default value, on device de selection
      m_enPlaybackState = e8VID_PB_STATE_STOPPED;
      m_oVideoPlaybackStateLock.vUnlock();
   }
   //!Whenever device is activated/de-activated default the user interaction flag to false.
   if(NULL != m_poAAPRsrcMngr)
   {
      sbHandsetInteractionReqd = false;
      m_poAAPRsrcMngr->vDevAuthAndAccessInfoCb(m_u32SelectedDeviceID, sbHandsetInteractionReqd);
   }

}

/***************************************************************************
** FUNCTION: t_Void spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayMode(t_U32...
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayMode(const t_U32 cou32DeviceHandle,
                                                             const trDisplayContext corDisplayContext,
                                                             const trDisplayConstraint corDisplayConstraint,
                                                             const tenDisplayInfo coenDisplayInfo)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayMode: Resource Mngt handled by %d",
      ETG_ENUM(DISPLAY_INFO,coenDisplayInfo)));

   SPI_INTENTIONALLY_UNUSED(corDisplayConstraint);

   if(e8_DISPLAY_CONTEXT == coenDisplayInfo)
   {
      vSetAccessoryDisplayContext(cou32DeviceHandle, 
         corDisplayContext.bDisplayFlag, 
         corDisplayContext.enDisplayContext);
   }//if(e8_DISPLAY_CONTEXT == coenDisplayInfo)

   //If HMI is handling display context, SPI has to use the interfaces DeviceVideoFocusRequest & SetDeviceVideoFocus.

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vLaunchApp()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vLaunchApp(t_U32 u32DevId,t_U32 u32AppId)
{
   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vLaunchApp:Device ID-0x%x AppID-0x%x", 
      u32DevId,u32AppId));

   m_oVideoSetupLock.s16Lock();
   // Device selection is successful but MD has not yet completed video setup.
   // Maintain the Projection view request pending until Video setup is received for processing.
   // If a unknown device is connected, clear the pending request on expiry of timer to start first run experience.
   if((false == sbVideoSetupCbRcvd) && (true == sbDevAuthAndAccessTimerRunning))
   {
      ETG_TRACE_USR2(("[DESC]:Projection UI requested before Video Setup is completed - Delaying Video Focus to MD"));
      m_bInitialMDVideoFocus = true;
   }//if( true == sbDevAuthAndAccessTimerRunning)
   // Request for Projected view by providing focus to MD only if video setup is completed
   else if ((NULL != spoCmdVideo) && (true == sbVideoSetupCbRcvd))
   {
      //Get the accessory response based on the current display context, for the launch app request.
      //If the RVC is ACTIVE, Ignore the Launch App request. [Long press of MENU button by User, when RVC is ACTIVE]
      if(true == bProcessLaunchApp())
      {
         //Set the Playback state to GAIN
         m_oCurMDFocusStateLock.s16Lock();
         m_enCurMDFocusState = e8AAP_VIDEOFOCUSSTATE_GAIN;
         m_oCurMDFocusStateLock.vUnlock();

         //Send Projection request to Phone
         m_oVideoPlaybackStateLock.s16Lock();
         ETG_TRACE_USR4(("[PARAM]:vLaunchApp:Current Video Playback state - %d",ETG_ENUM(AAP_VID_PB_STATE,m_enPlaybackState)));

         /*
         if the Video Focus is already with MD & user has launched app, then directly request HMI to enable the Projection layer.
         if not, send VideoFocus request to PHONE and set the Video Focus state to START request in progress.
         */
         if (e8VID_PB_STATE_STARTED == m_enPlaybackState)
         {
            ETG_TRACE_USR4(("[DESC]:MD already has the Video Focus. Directly enable projection Layer"));
            if(NULL != m_poAAPRsrcMngr)
            {
               m_poAAPRsrcMngr->vUpdateDeviceDisplayCntxt(true,e8DISPLAY_CONTEXT_UNKNOWN);
            }
         }//if (e8VID_PB_STATE_STARTED == m_enPlaybackState)
         else
         {
        	//@todo - Check whether we really need to check the display context, before triggering
        	//SetVideoFocus request. This should be ok to send this, since HMI is triggering this.
            vSetVideoFocus(e8VIDEOFOCUS_PROJECTED, true);
         }

         m_oVideoPlaybackStateLock.vUnlock();
      }
      else
      {
         ETG_TRACE_ERR(("[ERR]: Do not process the Launch app request in the current display context"));
      }

   }//if (NULL != spoCmdVideo)
   else
   {
      ETG_TRACE_USR2(("[DESC]: Ignoring Projection UI requested before Video Setup"));
   }
   m_oVideoSetupLock.vUnlock();

   //return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vVideoFocusCallback()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vVideoSetupCallback(tenMediaCodecTypes enMediaCodecType)
{
   /*lint -esym(40,fpvDeviceAuthAndAccessCb) fpvDeviceAuthAndAccessCb Undeclared identifier */


   ETG_TRACE_USR1(("spi_tclAAPVideoResourceMngr::vVideoSetupCallback: MediaCodecType - %d",
      ETG_ENUM(MEDIACODEC_TYPE, enMediaCodecType)));

   //Set this to  true, so that we send SetVideoFocus on next Launch App request onwards.
   vStopTimer();

   m_oVideoSetupLock.s16Lock();
   sbVideoSetupCbRcvd = true;
   if ((NULL != m_poAAPRsrcMngr) && (true == sbHandsetInteractionReqd))
   {
      sbHandsetInteractionReqd = false;
      m_poAAPRsrcMngr->vDevAuthAndAccessInfoCb(m_u32SelectedDeviceID,sbHandsetInteractionReqd);
   } //End of if(NULL != 

   if(true == m_bInitialMDVideoFocus)
   {
      m_bInitialMDVideoFocus = false;
      if (NULL != spoCmdVideo)
      {
         ETG_TRACE_USR2(("[DESC]:Provide Initial Video Focus to MD"));
		 
         m_oCurMDFocusStateLock.s16Lock();
         m_enCurMDFocusState = e8AAP_VIDEOFOCUSSTATE_GAIN ;
         m_oCurMDFocusStateLock.vUnlock();

         spoCmdVideo->vSetVideoFocus(e8VIDEOFOCUS_PROJECTED, false);
      }//if (NULL != spoCmdVideo)
   }//if(true == m_bInitialMDVideoFocus)
   m_oVideoSetupLock.vUnlock();
}

//!Static
/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bDevAuthAndAccessTimerCb
***************************************************************************/
t_Bool spi_tclAAPVideoResourceMngr::bDevAuthAndAccessTimerCb(timer_t rTimerID, t_Void *pvObject,
                                                             const t_Void *pvUserData)
{
   /*lint -esym(40,fpvDeviceAuthAndAccessCb) fpvDeviceAuthAndAccessCb Undeclared identifier */


   ETG_TRACE_USR4(("spi_tclAAPVideoResourceMngr:bDevAuthAndAccessTimerCb()"));

   SPI_INTENTIONALLY_UNUSED(pvUserData);
   SPI_INTENTIONALLY_UNUSED(rTimerID);

   //! Clear the Initial MD Video Focus as the connected device is unknown device and requires first
   //! run experience to be completed.

   spi_tclAAPVideoResourceMngr* poAAPVideoRsrcMngr = static_cast<spi_tclAAPVideoResourceMngr*> (pvObject);

   if (NULL != poAAPVideoRsrcMngr)
   {
      poAAPVideoRsrcMngr->vStopTimer();

      poAAPVideoRsrcMngr->m_oVideoSetupLock.s16Lock();
      poAAPVideoRsrcMngr->m_bInitialMDVideoFocus = false;

      if ((NULL != poAAPVideoRsrcMngr->m_poAAPRsrcMngr)
         && (false == sbHandsetInteractionReqd))
      {
         sbHandsetInteractionReqd = true;
         poAAPVideoRsrcMngr->m_poAAPRsrcMngr->vDevAuthAndAccessInfoCb(poAAPVideoRsrcMngr->m_u32SelectedDeviceID,
            sbHandsetInteractionReqd);
      } //End of if(NULL != poAAPVideo->m_rVideoCallbacks.fpvDeviceAuthAndAccessCb)...
      poAAPVideoRsrcMngr->m_oVideoSetupLock.vUnlock();
   }//End of if (NULL != poAAPVideo)

   return true;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vStartTimer()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vStartTimer()
{
   m_oVideoSetupLock.s16Lock();
   Timer* poTimer = Timer::getInstance();

   if(NULL != poTimer)
   {
      poTimer->StartTimer(srDevAuthAndAccessTimerID,
         cou32DevAuthAndAccessTimerVal, 0, this,
         &spi_tclAAPVideoResourceMngr::bDevAuthAndAccessTimerCb, NULL);

      ETG_TRACE_USR4(("[DESC]:Device Auth and Access Timer started"));

      sbDevAuthAndAccessTimerRunning = true;
   }//End of if(NULL != poTimer)
   m_oVideoSetupLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vStopTimer()
***************************************************************************/
t_Void spi_tclAAPVideoResourceMngr::vStopTimer()
{
   m_oVideoSetupLock.s16Lock();
   Timer* poTimer = Timer::getInstance();

   if ((NULL != poTimer) && (true == sbDevAuthAndAccessTimerRunning))
   {
      poTimer->CancelTimer(srDevAuthAndAccessTimerID);
      ETG_TRACE_USR4(("[DESC]:Device Auth and Access Timer Stopped"));

      sbDevAuthAndAccessTimerRunning = false;
   }//End of if (NULL != poTimer)...
   m_oVideoSetupLock.vUnlock();
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bProcessLaunchApp()
***************************************************************************/
t_Bool spi_tclAAPVideoResourceMngr::bProcessLaunchApp()
{
   t_Bool bRet=false;

   t_U32 u32ContSize = (sizeof(sacoMDVideoFocusReq))/(sizeof(trAAPMDVideoFocusReq));
   for(t_U8 u8Index =0; u8Index < u32ContSize; u8Index++)
   {
      if(m_enCurAccDispCntxt == sacoMDVideoFocusReq[u8Index].enAccDispCntxt)
      {
         bRet = (e8AAP_VIDEOFOCUS_GRANT==sacoMDVideoFocusReq[u8Index].enMDFocusReqResp_StateLoss);

         ETG_TRACE_USR4(("[DESC]:Accessory response for Start Video focus request by User:Display Context-%d Result:%d",
            ETG_ENUM(DISPLAY_CONTEXT,m_enCurAccDispCntxt),ETG_ENUM(BOOL,bRet)));

         // stop the process once search hits at the desired value
         break; 
      } //if( (enCurMDFocusState == sacoMDVideoFocusReq[u8Index]
   } //for(t_U8 u8Index =0; u8Index < u32ContSize; u8Index++)

   return bRet;
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>

