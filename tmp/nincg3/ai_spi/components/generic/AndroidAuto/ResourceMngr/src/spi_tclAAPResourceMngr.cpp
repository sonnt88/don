/***********************************************************************/
/*!
* \file    spi_tclAAPResourceMngr.cpp
* \brief   AAP Resource Manager
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Resource Manager
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shiva Kumar Gurija    | Initial Version
28.05.2015 |  Tejaswini H B(RBEI/ECP2)     | Added Lint comments to suppress C++11 Errors

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "spi_tclResorceMngrDefines.h"
#include "spi_tclAAPVideoResourceMngr.h"
#include "spi_tclAAPManager.h"
#include "spi_tclAAPCmdSession.h"
#include "spi_tclAAPResourceMngr.h"
#include "spi_tclAAPAudioResourceMngr.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_RSRCMNGR
      #include "trcGenProj/Header/spi_tclAAPResourceMngr.cpp.trc.h"
   #endif
#endif
//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e515 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported	
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/
t_U32 su32CurSelectedDevId = 0;
static tenNavAppState senCurAccNavAppState = e8SPI_NAV_NOT_ACTIVE;

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclAAPResourceMngr::spi_tclAAPResourceMngr()
***************************************************************************/
spi_tclAAPResourceMngr::spi_tclAAPResourceMngr() :
         m_poVideoRsrcMngr(NULL), m_poAudioRsrcMngr(NULL), m_bDevRequestsNavFocus(false)
{
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr() entered "));
   m_poVideoRsrcMngr = new (std::nothrow)spi_tclAAPVideoResourceMngr(this);
   m_poAudioRsrcMngr = new (std::nothrow)spi_tclAAPAudioResourceMngr();

   //! Register with AAP manager for Session callbacks
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      poAAPManager->bRegisterObject((spi_tclAAPRespSession*)this);
   }//if (NULL != poAAPManager)

   m_rDeviceAppStates.enSpeechAppState = e8SPI_SPEECH_UNKNOWN;
   m_rDeviceAppStates.enPhoneAppState  = e8SPI_PHONE_UNKNOWN;
   m_rDeviceAppStates.enNavAppState    = e8SPI_NAV_UNKNOWN;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPResourceMngr::~spi_tclAAPResourceMngr()
***************************************************************************/
spi_tclAAPResourceMngr::~spi_tclAAPResourceMngr()
{
   ETG_TRACE_USR1(("~spi_tclAAPResourceMngr() entered"));

   RELEASE_MEM(m_poVideoRsrcMngr);
   RELEASE_MEM(m_poAudioRsrcMngr);

   m_bDevRequestsNavFocus = false;

   m_rDeviceAppStates.enSpeechAppState = e8SPI_SPEECH_UNKNOWN;
   m_rDeviceAppStates.enPhoneAppState  = e8SPI_PHONE_UNKNOWN;
   m_rDeviceAppStates.enNavAppState    = e8SPI_NAV_UNKNOWN;
}

/***************************************************************************
** FUNCTION:  t_Void  spi_tclAAPResourceMngr::vRegRsrcMngrCallBack()
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vRegRsrcMngrCallBack(
         trRsrcMngrCallback rRsrcMngrCallback)
{
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vRegRsrcMngrCallBack entered"));
   m_rRsrcMngrCb = rRsrcMngrCallback;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclResourceMngrBase::bInitialize()
***************************************************************************/
t_Bool spi_tclAAPResourceMngr::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::bInitialize entered"));

   t_Bool bRet = false;
   if (NULL != m_poVideoRsrcMngr)
   {
      bRet = m_poVideoRsrcMngr->bInitialize();
   }//if(NULL != m_poVideoRsrcMngr)
   return bRet;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPResourceMngr::vUninitialize()
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vUnInitialize()
{
   if (NULL != m_poVideoRsrcMngr)
   {
      m_poVideoRsrcMngr->vUnInitialize();
   }//if(NULL != m_poVideoRsrcMngr)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPResourceMngr::vSetAccessoryDisplayContext()
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vSetAccessoryDisplayContext(
         const t_U32 cou32DevId, t_Bool bDisplayFlag,
         tenDisplayContext enDisplayContext, const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vSetAccessoryDisplayContext"));
   SPI_INTENTIONALLY_UNUSED(rfrcUsrCntxt);

   if (NULL != m_poVideoRsrcMngr)
   {
      m_poVideoRsrcMngr->vSetAccessoryDisplayContext(cou32DevId,
               bDisplayFlag,
               enDisplayContext);
   }//if(NULL != poVideoRsrcMngr)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPResourceMngr::vSetAccessoryAudioContext()
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vSetAccessoryAudioContext(
         const t_U32 cou32DevId, const tenAudioContext coenAudioCntxt,
         t_Bool bReqFlag, const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vSetAccessoryAudioContext"));
   //Add code
   SPI_INTENTIONALLY_UNUSED(rfrcUsrCntxt);

   if (NULL != m_poAudioRsrcMngr)
   {
      m_poAudioRsrcMngr->vSetAccessoryAudioContext(cou32DevId,
               coenAudioCntxt,
               bReqFlag);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void  spi_tclAAPResourceMngr::vOnSPISelectDeviceResult()
 ***************************************************************************/
t_Void spi_tclAAPResourceMngr::vOnSPISelectDeviceResult(t_U32 u32DevID,
         tenDeviceConnectionReq enDeviceConnReq, tenResponseCode enRespCode,
         tenErrorCode enErrorCode)
{
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vOnSPISelectDeviceResult"));

   su32CurSelectedDevId = ((e8DEVCONNREQ_SELECT == enDeviceConnReq)
            && (e8SUCCESS == enRespCode)) ? u32DevID : 0;

   if (NULL != m_poVideoRsrcMngr)
   {
      m_poVideoRsrcMngr->vOnSPISelectDeviceResult(u32DevID,
            enDeviceConnReq, enRespCode, enErrorCode);
   }//if(NULL != poVideoRsrcMngr)

   if (NULL != m_poAudioRsrcMngr)
   {
      m_poAudioRsrcMngr->vOnSPISelectDeviceResult(u32DevID,
            enDeviceConnReq, enRespCode, enErrorCode);
   }

   //!Reset the Application states once AAP device is disconnected/deselected
   if((e8DEVCONNREQ_DESELECT == enDeviceConnReq) && (NULL != m_rRsrcMngrCb.fvPostDeviceAppState) &&
            (NULL != m_rRsrcMngrCb.fvSetDeviceAppState))
   {
      m_rDeviceAppStates.enSpeechAppState = e8SPI_SPEECH_END;
      m_rDeviceAppStates.enPhoneAppState  = e8SPI_PHONE_NOT_ACTIVE;
      m_rDeviceAppStates.enNavAppState    = e8SPI_NAV_NOT_ACTIVE;

      vAcquireDevAppStateLock();
      m_rRsrcMngrCb.fvSetDeviceAppState(m_rDeviceAppStates.enSpeechAppState,
               m_rDeviceAppStates.enPhoneAppState, m_rDeviceAppStates.enNavAppState);
      vReleaseDevAppStateLock();

      m_rRsrcMngrCb.fvPostDeviceAppState(m_rDeviceAppStates.enSpeechAppState,
               m_rDeviceAppStates.enPhoneAppState, m_rDeviceAppStates.enNavAppState, corEmptyUsrContext);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPResourceMngr::vUpdateDeviceDisplayCntxt()
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vUpdateDeviceDisplayCntxt(t_Bool bDisplayFlag,
        tenDisplayContext enDisplayContext)
{
   /*lint -esym(40,fvPostDeviceDisplayContext) fvPostDeviceDisplayContext is not declared */
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vUpdateDeviceDisplayCntxt:bDisplayFlag-%d", ETG_ENUM(BOOL,
            bDisplayFlag)));

   if (NULL != m_rRsrcMngrCb.fvPostDeviceDisplayContext)
   {
      trUserContext rUsrCntxt;
      (m_rRsrcMngrCb.fvPostDeviceDisplayContext)(bDisplayFlag,
               enDisplayContext,
               rUsrCntxt);
   }//if(NULL != m_rRsrcMngrCb.fvPostDeviceDisplayContext)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPResourceMngr::vSetAccessoryAppState()
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vSetAccessoryAppState(
         const tenSpeechAppState enAccSpeechAppState,
         const tenPhoneAppState enAccPhoneAppState,
         const tenNavAppState enAccNavAppState,
         const trUserContext& rfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vSetAccessoryAppState: NavAppState - Current %d, Updated %d ",
            ETG_ENUM(NAV_APP_STATE, senCurAccNavAppState), ETG_ENUM(NAV_APP_STATE, enAccNavAppState)));

   //!Accessory Phone and Speech App states are not used since device does not require this info.
   SPI_INTENTIONALLY_UNUSED(enAccSpeechAppState);
   SPI_INTENTIONALLY_UNUSED(enAccPhoneAppState);
   SPI_INTENTIONALLY_UNUSED(rfrcUsrCntxt);

   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if ((NULL != poAAPManager) && (0 != su32CurSelectedDevId) &&
      ((true == m_bDevRequestsNavFocus) || /*If device had requested for focus*/
       ((enAccNavAppState != senCurAccNavAppState) && (e8SPI_NAV_ACTIVE == enAccNavAppState))))
   {
      //Send nav focus update
      spi_tclAAPCmdSession *poCmdSession = poAAPManager->poGetSessionInstance();

      if ((NULL != poCmdSession) && (NULL != m_rRsrcMngrCb.fvPostDeviceAppState) &&
               (NULL != m_rRsrcMngrCb.fvSetDeviceAppState))
      {
         tenAAPNavFocusType enNavFocusType = (e8SPI_NAV_ACTIVE == enAccNavAppState)
                 ? (e8_NAV_FOCUS_NATIVE) : (e8_NAV_FOCUS_PROJECTED);

         poCmdSession->vSetNavigationFocus(enNavFocusType);

         //!Use Case:Device Navi Active. User initiates AAP Navigation.
         //!In this case after setting the Navigation Focus to NATIVE,
         //!device does not send Navigation Focus Callback which informs that
         //!the focus is with HU.Therefore trigger this message once Focus is set to NATIVE.
         m_rDeviceAppStates.enNavAppState = (e8_NAV_FOCUS_NATIVE == enNavFocusType) ? e8SPI_NAV_NOT_ACTIVE:e8SPI_NAV_ACTIVE;

         vAcquireDevAppStateLock();
         m_rRsrcMngrCb.fvSetDeviceAppState(m_rDeviceAppStates.enSpeechAppState,
                  m_rDeviceAppStates.enPhoneAppState, m_rDeviceAppStates.enNavAppState);
         vReleaseDevAppStateLock();

         m_rRsrcMngrCb.fvPostDeviceAppState(m_rDeviceAppStates.enSpeechAppState,
                  m_rDeviceAppStates.enPhoneAppState, m_rDeviceAppStates.enNavAppState, corEmptyUsrContext);
      }//End of if (NULL != poCmdSession)
   }//End of if if ((NULL != poAAPManager) && (0 != su32CurSelectedDevId)...

   //!Clear flag since response is sent
   //!TODO:Check of lock is required. This variable is updated from 2 thread contexts
   m_bDevRequestsNavFocus = false;

   //!Store current accessory Navigation app state
   senCurAccNavAppState = enAccNavAppState;
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vNavigationFocusCb()
 ***************************************************************************/
t_Void spi_tclAAPResourceMngr::vNavigationFocusCb(tenAAPNavFocusType enNavFocusType)
{
   /*lint -esym(40,fvPostDeviceAppState) fvPostDeviceAppState is not declared */
   /*lint -esym(40,fvSetDeviceAppState) fvSetDeviceAppState is not declared */

   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vNavigationFocusCb() entered: NavFocusType = %d ", ETG_ENUM(NAV_FOCUS_TYPE,
            enNavFocusType)));

   //!Set the device request focus state. This would be used later to Set the navigation focus to AAP device
   //!once Native Navigation is cancelled.
   m_bDevRequestsNavFocus = ((e8_NAV_FOCUS_PROJECTED == enNavFocusType) && (e8SPI_NAV_ACTIVE == senCurAccNavAppState));

   //!Navigation App state has to be updated always to the client.
   m_rDeviceAppStates.enNavAppState = (e8_NAV_FOCUS_PROJECTED == enNavFocusType)?(e8SPI_NAV_ACTIVE):(e8SPI_NAV_NOT_ACTIVE);

   if ((NULL != m_rRsrcMngrCb.fvPostDeviceAppState) && (NULL != m_rRsrcMngrCb.fvSetDeviceAppState))
   {
      vAcquireDevAppStateLock();
      m_rRsrcMngrCb.fvSetDeviceAppState(m_rDeviceAppStates.enSpeechAppState,
               m_rDeviceAppStates.enPhoneAppState, m_rDeviceAppStates.enNavAppState);
      vReleaseDevAppStateLock();

      m_rRsrcMngrCb.fvPostDeviceAppState(m_rDeviceAppStates.enSpeechAppState,
               m_rDeviceAppStates.enPhoneAppState, m_rDeviceAppStates.enNavAppState, corEmptyUsrContext);
      }//End of if ((NULL != m_rRsrcMngrCb.fvPostDeviceAppState) ...))

   //!If Native navigation is not active, grant the Navigation status immediately to the device
   //!irrespective of the focus type from the device(NATIVE/PROJECTION).
   if(e8SPI_NAV_ACTIVE != senCurAccNavAppState)
   {
      //! If native navigation is not running, accept request
      spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();

      if (NULL != poAAPManager)
      {
         spi_tclAAPCmdSession *poCmdSession =
                  poAAPManager->poGetSessionInstance();
         if (NULL != poCmdSession)
         {
            poCmdSession->vSetNavigationFocus(enNavFocusType);
         }//End of if (NULL != poCmdSession)
      }//End of if (NULL != poAAPManager)
   }//End of else
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPResourceMngr::vSessionStatusInfo()
 ***************************************************************************/
t_Void spi_tclAAPResourceMngr::vSessionStatusInfo(tenSessionStatus enSessionStatus, t_Bool bSessionTimedOut)
{
   /*lint -esym(40,fvUpdateSessionStatus)fvUpdateSessionStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vSessionStatusInfo enSessionStatus = %d\n",
            ETG_ENUM(SESSION_STATUS, enSessionStatus)));

   SPI_INTENTIONALLY_UNUSED(bSessionTimedOut);

   if (NULL != m_rRsrcMngrCb.fvUpdateSessionStatus)
   {
      (m_rRsrcMngrCb.fvUpdateSessionStatus)(su32CurSelectedDevId, e8DEV_TYPE_ANDROIDAUTO,
               enSessionStatus);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vVoiceSessionNotificationCb()
 ***************************************************************************/
t_Void spi_tclAAPResourceMngr::vVoiceSessionNotificationCb(tenAAPVoiceSessionStatus enVoiceSessionStatus)
{
   ETG_TRACE_USR1(("spi_tclAAPResourceMngr::vVoiceSessionNotificationCb() entered: VoiceSessionStatus = %d ", ETG_ENUM(AAP_VOICE_SESSION_STATUS,
       enVoiceSessionStatus)));

   // Post the current state to Resource manager so that the Voice session State can be checked else where

   m_rDeviceAppStates.enSpeechAppState = (e8_VOICE_SESSION_START == enVoiceSessionStatus)? e8SPI_SPEECH_SPEAKING : e8SPI_SPEECH_END;

   if ((NULL != m_rRsrcMngrCb.fvPostDeviceAppState) && (NULL != m_rRsrcMngrCb.fvSetDeviceAppState))
   {
      vAcquireDevAppStateLock();
      m_rRsrcMngrCb.fvSetDeviceAppState(m_rDeviceAppStates.enSpeechAppState,
               m_rDeviceAppStates.enPhoneAppState, m_rDeviceAppStates.enNavAppState);
      vReleaseDevAppStateLock();

      m_rRsrcMngrCb.fvPostDeviceAppState(m_rDeviceAppStates.enSpeechAppState, m_rDeviceAppStates.enPhoneAppState,
         m_rDeviceAppStates.enNavAppState, corEmptyUsrContext);
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAAPResourceMngr::vSetAccessoryDisplayMode(t_U32...
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vSetAccessoryDisplayMode(const t_U32 cou32DeviceHandle,
                                                        const trDisplayContext corDisplayContext,
                                                        const trDisplayConstraint corDisplayConstraint,
                                                        const tenDisplayInfo coenDisplayInfo)
{
   if (NULL != m_poVideoRsrcMngr)
   {
      m_poVideoRsrcMngr->vSetAccessoryDisplayMode(cou32DeviceHandle,
         corDisplayContext, corDisplayConstraint, coenDisplayInfo);
   }//if(NULL != poVideoRsrcMngr)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPResourceMngr::vLaunchApp()
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vLaunchApp(t_U32 u32DevId,t_U32 u32AppId)
{
   if (NULL != m_poVideoRsrcMngr)
   {
      m_poVideoRsrcMngr->vLaunchApp(u32DevId,u32AppId);
   }//if(NULL != poVideoRsrcMngr)
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAAPResourceMngr::vDevAuthAndAccessInfoCb()
***************************************************************************/
t_Void spi_tclAAPResourceMngr::vDevAuthAndAccessInfoCb(const t_U32 cou32DevId,
                               const t_Bool cobHandsetInteractionReqd)
{
   /*lint -esym(40,fpvDeviceAuthAndAccessCb) fpvDeviceAuthAndAccessCb Undeclared identifier */
   /*lint -esym(746,fpvDeviceAuthAndAccessCb)call to function fpvDeviceAuthAndAccessCb() not made in the presence of a prototype */

   if (NULL != m_rRsrcMngrCb.fpvDeviceAuthAndAccessCb)
   {
      (m_rRsrcMngrCb.fpvDeviceAuthAndAccessCb)(cou32DevId,cobHandsetInteractionReqd);
   }//if (NULL != m_rRsrcMngrCb.fpvD
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
