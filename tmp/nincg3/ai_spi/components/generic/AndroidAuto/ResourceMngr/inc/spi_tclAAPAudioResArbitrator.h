/***********************************************************************/
/*!
* \file         spi_tclAAPAudioResArbitrator.h
* \brief
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:
AUTHOR:         Shihabudheen P M
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shihabudheen P M      | Initial Version
12.05.2015  | Ramya Murthy          | Moved resource arbitration logic to config file

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPAUDIORESARBITRATOR_H_
#define _SPI_TCLAAPAUDIORESARBITRATOR_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "AAPTypes.h"
#include "Lock.h"


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class
* \brief
*
*
*
****************************************************************************/

class spi_tclAAPAudioResArbitrator
{
public:
   
   /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAudioResArbitrator::spi_tclAAPAudioResArbitrator()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAudioResArbitrator()
   * \brief   Default Constructor
   * \param   t_Void
   * \sa      ~spi_tclAAPAudioResArbitrator()
   **************************************************************************/
   spi_tclAAPAudioResArbitrator(trAAPAudioRsrcMngrCallbacks rAudioRsrcMngrCbs);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAudioResArbitrator::~spi_tclAAPAudioResArbitrator()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPAudioResArbitrator()
   * \brief   Default Destructor
   * \param   t_Void
   * \sa      spi_tclAAPAudioResArbitrator()
   **************************************************************************/
   ~spi_tclAAPAudioResArbitrator();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAudioResArbitrator::vOnAudioFocusRequest()
   ***************************************************************************/
   /*!
   * \fn      vOnAudioFocusRequest()
   * \brief   Function to evaluate the audio focus request from the AAP phone
   * \param   enDevAudFocusRequest : [IN] Audio focus request
   * \param   bUnsolicited : [IN]
   *           True - if audio focus is changed due to audio context change
   *           False - if audio focus is changed on device request
   **************************************************************************/
   t_Void vOnAudioFocusRequest(tenAAPDeviceAudioFocusRequest enDevAudFocusRequest,
         t_Bool Unsolicited);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAudioResArbitrator::vSetAccessoryAudioContext()
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryAudioContext(const tenAudioContext coenAudioCntxt, t_Bool bDisplayFlag)
   * \brief   To send accessory audio context related info .
   * \param   coenAudioCntxt : [IN] Audio Context
   * \pram    bReqFlag : [IN] Request/ Release flag
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryAudioContext(const tenAudioContext coenAudioCntxt, t_Bool bReqFlag);

    /***************************************************************************
    ****************************END OF PUBLIC***********************************
    ***************************************************************************/
protected:

   /***************************************************************************
    *********************************PROTECTED*********************************
    ***************************************************************************/

   /**************************************************************************
   ****************************END OF PROTECTED*******************************
   ***************************************************************************/
private:
    
	/***************************************************************************
    *********************************PRIVATE***********************************
    ***************************************************************************/

   /***************************************************************************
   ** FUNCTION: spi_tclAAPAudioResArbitrator::bGetAudioFocusResponse()
   ***************************************************************************/
   /*!
   * \fn      bGetAudioFocusResponse()
   * \brief   Evaluates response for Audio FOcus request from device based on current audio context.
   * \param   enDevAudFocusRequest : [IN] Audio Focus request from device
   * \param   enCurAccAudFocusState : [IN] Current Accessory audio focus state
   * \param   enCurAccAudContext : [IN] Current Accessory audio context
   * \param   rfenNewDevAudFocusState : [OUT] New Audio Focus state of device to be sent as response
   * \param   rfenNewAccAudFocusState : [OUT] New Accessory audio focus state
   * \retval  t_Bool: True - if audio focus request is valid in current audio context, else False.
   **************************************************************************/
   t_Bool bGetAudioFocusResponse(tenAAPDeviceAudioFocusRequest enDevAudFocusRequest,
            tenAccessoryAudioFocusState enCurAccAudFocusState,
            tenAudioContextType enCurAccAudContext,
            tenAAPDeviceAudioFocusState& rfenNewDevAudFocusState,
            tenAccessoryAudioFocusState& rfenNewAccAudFocusState);

   /***************************************************************************
   ** FUNCTION: spi_tclAAPAudioResArbitrator::bGetAudioFocusNotification()
   ***************************************************************************/
   /*!
   * \fn      vUpdateFocusGainedTransientState()
   * \brief   Function to update the device audio focus request when the Car has gained focus transiently.
   * \param   enAudResourceOwner : [IN] Current Audio resource owner
   * \param   enAudContextType : [IN] Current Audio context
   * \param   enCurAccAudFocusState : [IN] Current Accessory audio focus state
   * \param   rfenNewDevAudFocusState : [OUT] New Audio Focus state of device to be sent as notification
   * \param   rfenNewAccAudFocusState : [OUT] New Accessory audio focus state
   * \retval  t_Bool: True - if there is a change in device audio focus which needs to be notified.
   **************************************************************************/
   t_Bool bGetAudioFocusNotification(tenAudioResourceOwner enAudResourceOwner,
            tenAudioContextType enAudContextType,
            tenAccessoryAudioFocusState enCurAccAudFocusState,
            tenAAPDeviceAudioFocusState& rfenNewDevAudFocusState,
            tenAccessoryAudioFocusState& rfenNewAccAudFocusState);

   /***************************************************************************
   ** FUNCTION:  tenAccessoryAudioFocusState spi_tclAAPAudioResArbitrator::enGetCurrentAccFocusState()
   ***************************************************************************/
   /*!
   * \fn      enGetCurrentAccFocusState()
   * \brief   Function to read current Accessory Audio Focus state
   * \param   None
   * \retval  tenAccessoryAudioFocusState: Current Accessory Audio Focus state
   **************************************************************************/
   tenAccessoryAudioFocusState enGetCurrentAccFocusState();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAudioResArbitrator::vSetCurrentAccFocusState()
   ***************************************************************************/
   /*!
   * \fn      vSetCurrentAccFocusState()
   * \brief   Function to set current Accessory Audio Focus state
   * \param   enNewAccAudFocusState : [IN] Accessory Audio Focus state
   * \retval  None
   **************************************************************************/
   t_Void vSetCurrentAccFocusState(tenAccessoryAudioFocusState enNewAccAudFocusState);

   /***************************************************************************
   ** FUNCTION:  tenAudioContextType spi_tclAAPAudioResArbitrator::enGetCurrentAccAudioCtxtType()
   ***************************************************************************/
   /*!
   * \fn      enGetCurrentAccAudioCtxtType()
   * \brief   Function to read current Accessory Audio Context
   * \param   None
   * \retval  tenAudioContextType: Current Accessory Audio Focus state
   **************************************************************************/
   tenAudioContextType enGetCurrentAccAudioCtxtType();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAudioResArbitrator::vSetCurrentAccAudioCtxtType()
   ***************************************************************************/
   /*!
   * \fn      vSetCurrentAccAudioCtxtType()
   * \brief   Function to read current Accessory Audio Context
   * \param   enNewAccAudioCtxtType : [IN] Accessory Audio Context
   * \retval  None
   **************************************************************************/
   t_Void vSetCurrentAccAudioCtxtType(tenAudioContextType enNewAccAudioCtxtType);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAudioResArbitrator(const spi_tclAAPAudioResArbitrator...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAudioResArbitrator(
   *                             const spi_tclAAPAudioResArbitrator& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPAudioResArbitrator()
   ***************************************************************************/
   spi_tclAAPAudioResArbitrator(const spi_tclAAPAudioResArbitrator& corfoSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAudioResArbitrator& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAudioResArbitrator& operator=(
   *                          const spi_tclAAPAudioResArbitrator& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPAudioResArbitrator(const spi_tclAAPAudioResArbitrator& otrSrc)
   ***************************************************************************/
   spi_tclAAPAudioResArbitrator& operator=(const spi_tclAAPAudioResArbitrator& corfoSrc);


   //! Callbacks to Audio Resource Manager
   trAAPAudioRsrcMngrCallbacks   m_rAudioRsrcMngrCbs;

   //! Lock for m_rAudioRsrcMngrCbs data
   Lock                          m_oAudioRsrcMngrCbLock;

   //! Stores current accessory(native) audio focus state
   tenAccessoryAudioFocusState   m_enCurAccFocusState;

   //! Current accessory audio context (high level context such as main/mix/transient)
   tenAudioContextType           m_enCurAccAudContextType;

   //! Stores latest audio context received in vSetAccessoryAudioContext()
   tenAudioContext               m_enCurAudioCntxt;

   //! Stores latest audio flag received in vSetAccessoryAudioContext()
   t_Bool                        m_bCurCtxtFlag;


   /***************************************************************************
    *********************************END OF PRIVATE****************************
    ***************************************************************************/
};

#endif
