/***********************************************************************/
/*!
* \file    spi_tclAAPResourceMngr.h
* \brief   AAP Resource Manager
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Resource Manager
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPRESOURCEMNGR_H_
#define _SPI_TCLAAPRESOURCEMNGR_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "AAPTypes.h"
#include "DiPOTypes.h"
#include "spi_tclResorceMngrDefines.h"
#include "spi_tclResourceMngrBase.h"
#include "spi_tclAAPRespSession.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
class spi_tclAAPVideoResourceMngr;
class spi_tclAAPAudioResourceMngr;

/****************************************************************************/
/*!
* \class  spi_tclAAPResourceMngr
* \brief  AAP Resource Manager
****************************************************************************/
class spi_tclAAPResourceMngr: public spi_tclResourceMngrBase,
         public spi_tclAAPRespSession
{

public:

    /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPResourceMngr::spi_tclAAPResourceMngr()
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPResourceMngr()
    * \brief   Default Constructor
    * \param   t_Void
    * \sa      ~spi_tclAAPResourceMngr()
    **************************************************************************/
    spi_tclAAPResourceMngr();

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPResourceMngr::~spi_tclAAPResourceMngr()
    ***************************************************************************/
    /*!
    * \fn      ~spi_tclAAPResourceMngr()
    * \brief   Destructor
    * \param   t_Void
    * \sa      spi_tclAAPResourceMngr()
    **************************************************************************/
    ~spi_tclAAPResourceMngr();

    /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPResourceMngr::bInitialize()
    ***************************************************************************/
    /*!
    * \fn      t_Bool bInitialize()
    * \brief   To Initialize all the Resource Manager related classes
    * \retval  t_Bool
    * \sa      vUninitialize()
    **************************************************************************/
    t_Bool bInitialize();

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vUninitialize()
    ***************************************************************************/
    /*!
    * \fn      t_Void vUninitialize()
    * \brief   To Uninitialize all the Resource Manager related classes
    * \retval  t_Void
    * \sa      bInitialize()
    **************************************************************************/
    t_Void vUnInitialize();

    /***************************************************************************
    ** FUNCTION:  t_Void  spi_tclAAPResourceMngr::vRegRsrcMngrCallBack()
    ***************************************************************************/
    /*!
    * \fn      t_Void vRegRsrcMngrCallBack()
    * \brief   To Register for the asynchronous responses that are required from
    *          ML/DiPo Resource Manager
    * \param   rRsrcMngrCallback : [IN] Resource Manager callbacks structure
    * \retval  t_Void
    **************************************************************************/
    t_Void vRegRsrcMngrCallBack(trRsrcMngrCallback rRsrcMngrCallback);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vSetAccessoryDisplayContext()
    ***************************************************************************/
    /*!
    * \fn      virtual t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
    *        t_Bool bDisplayFlag, tenDisplayContext enDisplayContext)
    * \brief   To send accessory display context related info .
    * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
    * \param   enDevConnType : [IN] Device connection Type USB/WIFI
    * \pram    rfrcUsrCntxt: [IN] User Context Details.
    * \retval  t_Void
    **************************************************************************/
    t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
            t_Bool bDisplayFlag,
            tenDisplayContext enDisplayContext,
            const trUserContext& rfrcUsrCntxt);

    /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vSetAccessoryAudioContext()
    ***************************************************************************/
    /*!
    * \fn     vSetAccessoryAudioContext(const t_U32 cou32DevId, const t_U8 cu8AudioCntxt
    *       t_Bool bDisplayFlag, const trUserContext& rfrcUsrCntxt)
    * \brief   To send accessory audio context related info .
    * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
    * \param   cu8AudioCntxt : [IN] Audio Context
    * \pram    bReqFlag : [IN] Request/ Release flag
    * \pram    rfrcUsrCntxt: [IN] User Context Details.
    * \retval  t_Void
    **************************************************************************/
    t_Void vSetAccessoryAudioContext(const t_U32 cou32DevId,
            const tenAudioContext coenAudioCntxt,
            t_Bool bReqFlag,
            const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vSetAccessoryAppState()
    ***************************************************************************/
    /*!
    * \fn     t_Void vSetAccessoryAppState(tenSpeechAppState enAccSpeechAppState,
    *         tenPhoneAppState enAccPhoneAppState,
    *         tenNavAppState enAccNavAppState
    *
    * \brief   To set accessory app state realated info.
    * \pram    enAccSpeechAppState: [IN] Accessory speech state.
    * \param   enAccPhoneAppState : [IN] Accessory phone state
    * \pram    enAccNavAppState   : [IN] Accessory navigation app state
    * \pram    rfrcUsrCntxt    : [IN] User Context Details.
    * \retval  t_Void
    **************************************************************************/
   t_Void vSetAccessoryAppState(const tenSpeechAppState enAccSpeechAppState,
            const tenPhoneAppState enAccPhoneAppState,
            const tenNavAppState enAccNavAppState,
            const trUserContext& rfrcUsrCntxt);

   /***************************************************************************
    ** FUNCTION:  t_Void  spi_tclAAPResourceMngr::vOnSPISelectDeviceResult()
    ***************************************************************************/
    /*!
    * \fn      t_Void vOnSPISelectDeviceResult()
    * \brief   Interface to receive result of SPI device selection/deselection
    * \param   u32DevID : [IN] Resource Manager callbacks structure.
    * \param   enDeviceConnReq : [IN] Select/ deselect.
    * \param   enRespCode : [IN] Response code (success/failure)
    * \param   enErrorCode : [IN] Error
    * \retval  t_Void
    **************************************************************************/
   t_Void vOnSPISelectDeviceResult(t_U32 u32DevID, tenDeviceConnectionReq enDeviceConnReq,
         tenResponseCode enRespCode, tenErrorCode enErrorCode);
			   
   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vUpdateDeviceDisplayCntxt()
    ***************************************************************************/
    /*!
    * \fn      virtual t_Void vUpdateDeviceDisplayCntxt(t_Bool bDisplayFlag,
    *           tenDisplayContext enDisplayContext)
    * \brief   Request HMI to enable/diable Layer
    * \pram    bDisplayFlag      : [IN] TRUE - enable layer
    *                                   FALSE - disable layer
    * \param   enDisplayContext  : [IN] Display context
    * \retval  t_Void
    **************************************************************************/
    t_Void vUpdateDeviceDisplayCntxt(t_Bool bDisplayFlag,
            tenDisplayContext enDisplayContext);
            
    /***************************************************************************
     ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vNavigationFocusCb()
     ***************************************************************************/
    /*!
     * \fn      vNavigationFocusCb
     * \brief   Called when a navigation focus request is received from the phone.
     *  You must respond to this by calling Controller::setNavigationFocus()
     *  (even if there is no change in navigation focus). If navigation focus
     *  is given to the mobile device, all native turn by turn guidance
     *  systems must be stopped.
     * \param   enNavFocusType  he type requested (can be NAV_FOCUS_NATIVE or NAV_FOCUS_PROJECTED).
     **************************************************************************/
    virtual t_Void vNavigationFocusCb(tenAAPNavFocusType enNavFocusType);

    /***************************************************************************
     ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vVoiceSessionNotificationCb()
     ***************************************************************************/
    /*!
     * \fn      vVoiceSessionNotificationCb
     * \brief  Called when a voice session notification is received. Note that this callback only applies
     * to you if you do not always send a PTT short press to us always. If you always send PTT
     * short press to us, you should be able to ignore this call altogether.
     * \param enVoiceSessionStatus The status of the voice recongition session.
     **************************************************************************/
    virtual t_Void vVoiceSessionNotificationCb(tenAAPVoiceSessionStatus enVoiceSessionStatus);

    /***************************************************************************
     ** FUNCTION:  t_Bool spi_tclAAPResourceMngr::vSessionStatusInfo()
     ***************************************************************************/
    /*!
     * \fn      vSessionStatusInfo
     * \brief  informs the current session status of android auto session. function
     *         overridden from spi_tclAAPRespSession.h
     * \param  enSessionStatus : indicates current status of android auto session
     **************************************************************************/
    virtual t_Void vSessionStatusInfo(tenSessionStatus enSessionStatus, t_Bool bSessionTimedOut);


    /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPResourceMngr::vSetAccessoryDisplayMode(t_U32...
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryDisplayMode()
   * \brief  Accessory display mode update request.
   * \param  [IN] cou32DeviceHandle      : Uniquely identifies the target Device.
   * \param  [IN] corDisplayContext     : Display context info
   * \param  [IN] corDisplayConstraint  : DiDisplay constraint info
   * \param  [IN] coenDisplayInfo       : Display info flag
   * \sa
   **************************************************************************/
   t_Void vSetAccessoryDisplayMode(const t_U32 cou32DeviceHandle,
      const trDisplayContext corDisplayContext,
      const trDisplayConstraint corDisplayConstraint,
      const tenDisplayInfo coenDisplayInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPResourceMngr::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchApp(t_U32 u32DevId,
   *                 ct_U32 u32AppId)
   * \brief   To Launch the Video for the requested app 
   * \pram    u32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    u32AppId  : [IN] Application Id
   * \retval  t_Void
   **************************************************************************/
   t_Void vLaunchApp(t_U32 u32DevId,t_U32 u32AppId);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPResourceMngr::vDevAuthAndAccessInfoCb()
   ***************************************************************************/
   /*!
   * \fn     t_Void vDevAuthAndAccessInfoCb(const t_U32 cou32DevId,
   *            const t_Bool cobHandsetInteractionReqd)
   * \brief  method to update the authorization and access to AAP projection device
   * \param  cou32DevId                 : [IN] Uniquely identifies the target Device.
   * \param  cobHandsetInteractionReqd  : [IN] Set if interaction required on Handset
   * \retval t_Void
   **************************************************************************/
   t_Void vDevAuthAndAccessInfoCb(const t_U32 cou32DevId,
            const t_Bool cobHandsetInteractionReqd);

    /***************************************************************************
    ****************************END OF PUBLIC***********************************
    ***************************************************************************/

protected:

    /***************************************************************************
    *********************************PROTECTED**********************************
    ***************************************************************************/

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPResourceMngr(const spi_tclAAPResourceMngr...
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPResourceMngr(
    *                             const spi_tclAAPResourceMngr& corfoSrc)
    * \brief   Copy constructor - Do not allow the creation of copy constructor
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPResourceMngr()
    ***************************************************************************/
    spi_tclAAPResourceMngr(const spi_tclAAPResourceMngr& corfoSrc);

    /***************************************************************************
    ** FUNCTION:  spi_tclAAPResourceMngr& operator=( const spi_tclAAP...
    ***************************************************************************/
    /*!
    * \fn      spi_tclAAPResourceMngr& operator=(
    *                          const spi_tclAAPResourceMngr& corfoSrc))
    * \brief   Assignment operator
    * \param   corfoSrc : [IN] reference to source data interface object
    * \retval
    * \sa      spi_tclAAPResourceMngr(const spi_tclAAPResourceMngr& otrSrc)
    ***************************************************************************/
    spi_tclAAPResourceMngr& operator=(const spi_tclAAPResourceMngr& corfoSrc);

    /***************************************************************************
    ****************************END OF PROTECTED********************************
    ***************************************************************************/

private:

    /***************************************************************************
    *********************************PRIVATE************************************
    ***************************************************************************/

    //! Callback functions to Resource manager
    trRsrcMngrCallback  m_rRsrcMngrCb;
    
    //! Pointer to Video resource manager    
    spi_tclAAPVideoResourceMngr* m_poVideoRsrcMngr;

      //! Pointer to Audio resource manager
    spi_tclAAPAudioResourceMngr *m_poAudioRsrcMngr;

    //! Variable to keep track if MD has requested for Navi Focus
    t_Bool m_bDevRequestsNavFocus;

    //!Structure varaible to store device app states
    trDiPOAppState m_rDeviceAppStates;
    /***************************************************************************
    ****************************END OF PRIVATE *********************************
    ***************************************************************************/
}; //class spi_tclAAPResourceMngr

#endif //_SPI_TCLAAPRESOURCEMNGR_H_
///////////////////////////////////////////////////////////////////////////////
// <EOF>
