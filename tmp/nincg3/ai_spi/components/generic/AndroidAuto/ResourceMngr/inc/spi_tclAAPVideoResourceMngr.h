
/***********************************************************************/
/*!
* \file   spi_tclAAPVideoResourceMngr.h
* \brief  AAP Video resource manager
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    AAP Video resource manager
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Shiva Kumar Gurija    | Initial Version
11.04.2015  | Shiva Kumar Gurija    | Handling Video Focus Notifications from HU
04.02.2016  | Shiva Kumar Gurija    | Moved LaunchApp handling from Video to RsrcMngr

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPVIDEORESOURCEMNGR_H_
#define _SPI_TCLAAPVIDEORESOURCEMNGR_H_


/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "AAPTypes.h"
#include "Lock.h"
#include "spi_tclAAPRespVideo.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
class spi_tclAAPResourceMngr;

enum tenVideoPlaybackState
{
   e8VID_PB_STATE_START_RQSTD =0,
   e8VID_PB_STATE_STARTED =1,
   e8VID_PB_STATE_STOP_RQSTD =2,
   e8VID_PB_STATE_STOPPED =3
};

/****************************************************************************/
/*!
* \class   spi_tclAAPVideoResourceMngr
* \brief   AAP Video resource manager
*
*           This class implements the logic to when to request for Video Focus
*           and how to respond to the VideoFocus request from the Phone.
*
****************************************************************************/
class spi_tclAAPVideoResourceMngr:public spi_tclAAPRespVideo
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoResourceMngr::spi_tclAAPVideoResourceMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideoResourceMngr(spi_tclAAPResourceMngr* poAAPRsrcMngr)
   * \brief   parameterized  Constructor
   * \param   poAAPRsrcMngr : [IN] AAP resource manager
   * \sa      ~spi_tclAAPVideoResourceMngr()
   **************************************************************************/
   spi_tclAAPVideoResourceMngr(spi_tclAAPResourceMngr* poAAPRsrcMngr);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoResourceMngr::~spi_tclAAPVideoResourceMngr()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPVideoResourceMngr()
   * \brief   Destructor
   * \sa      spi_tclAAPVideoResourceMngr()
   **************************************************************************/
   ~spi_tclAAPVideoResourceMngr();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bInitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bInitialize()
   * \brief   To Initialize all the Video resources
   * \retval  t_Bool
   * \sa      vUninitialize()
   **************************************************************************/
   t_Bool bInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vUninitialize()
   ***************************************************************************/
   /*!
   * \fn      t_Void vUninitialize()
   * \brief    To Uninitialize  the Video resources
   * \retval  t_Void
   * \sa      bInitialize()
   **************************************************************************/
   t_Void vUnInitialize();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayContext()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
   *             t_Bool bDisplayFlag, tenDisplayContext enDisplayContext)
   * \brief   To send accessory display context related info .
   * \pram    cou32DevId    : [IN] Uniquely identifies the target Device.
   * \param   bDisplayFlag  : [IN] Display flag
   * \pram    enDisplayContext : [IN] display context
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryDisplayContext(const t_U32 cou32DevId,
      t_Bool bDisplayFlag, tenDisplayContext enDisplayContext);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vOnSPISelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSPISelectDeviceResult(t_U32 u32DevID,
   *             tenDeviceConnectionReq enDevConnReq,
   *             tenResponseCode enRespCode, 
   *             tenErrorCode enErrorCode)
   * \brief   Called when SelectDevice operation is complete & with the result
   *           of the operation.
   * \param   u32DevID : [IN] Unique handle of selected device
   * \param   enDevConnReq: [IN]  Connection request type for the device
   * \param   enRespCode: [IN]  Response code enumeration
   * \param   enErrorCode: [IN]  Error code enumeration
   * \retval  None
   **************************************************************************/
   t_Void vOnSPISelectDeviceResult(t_U32 u32DevID,
      tenDeviceConnectionReq enDevConnReq,
      tenResponseCode enRespCode,
      tenErrorCode enErrorCode);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPVideoResourceMngr::vSetAccessoryDisplayMode(t_U32...
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryDisplayMode()
   * \brief  Accessory display mode update request.
   * \param  [IN] cou32DeviceHandle      : Uniquely identifies the target Device.
   * \param  [IN] corDisplayContext     : Display context info
   * \param  [IN] corDisplayConstraint  : DiDisplay constraint info
   * \param  [IN] coenDisplayInfo       : Display info flag
   * \sa
   **************************************************************************/
   t_Void vSetAccessoryDisplayMode(const t_U32 cou32DeviceHandle,
      const trDisplayContext corDisplayContext,
      const trDisplayConstraint corDisplayConstraint,
      const tenDisplayInfo coenDisplayInfo);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vLaunchApp()
   ***************************************************************************/
   /*!
   * \fn      t_Void vLaunchApp(t_U32 u32DevId,
   *                 ct_U32 u32AppId)
   * \brief   To Launch the Video for the requested app 
   * \pram    u32DevId  : [IN] Uniquely identifies the target Device.
   * \pram    u32AppId  : [IN] Application Id
   * \retval  t_Void
   **************************************************************************/
   t_Void vLaunchApp(t_U32 u32DevId,t_U32 u32AppId);


   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoResourceMngr(const spi_tclAAPVideoResourceMngr...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideoResourceMngr(
   *                             const spi_tclAAPVideoResourceMngr& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPVideoResourceMngr()
   ***************************************************************************/
   spi_tclAAPVideoResourceMngr(const spi_tclAAPVideoResourceMngr& corfoSrc);


   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoResourceMngr& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideoResourceMngr& operator=(
   *                          const spi_tclAAPVideoResourceMngr& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPVideoResourceMngr(const spi_tclAAPVideoResourceMngr& otrSrc)
   ***************************************************************************/
   spi_tclAAPVideoResourceMngr& operator=(const spi_tclAAPVideoResourceMngr& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPVideoResourceMngr::spi_tclAAPVideoResourceMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPVideoResourceMngr()
   * \brief   Constructor
   * \sa      ~spi_tclAAPVideoResourceMngr()
   **************************************************************************/
   spi_tclAAPVideoResourceMngr();


   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vPlaybackStartCallback()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPlaybackStartCallback()
   * \brief   method to update that the Video Play back is started
   * \retval  t_Void
   **************************************************************************/
   t_Void vPlaybackStartCallback();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vPlaybackStopCallback()
   ***************************************************************************/
   /*!
   * \fn      t_Void vPlaybackStopCallback()
   * \brief   method to update that the Video Play back is stopped
   * \retval  t_Void
   **************************************************************************/
   t_Void vPlaybackStopCallback();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vVideoFocusCallback()
   ***************************************************************************/
   /*!
   * \fn      t_Void vVideoFocusCallback(tenVideoFocus enVideoFocus,
   *                 tenVideoFocusReason enVideoFocusReason)
   * \brief   Method to update that the Video Focus is requested
   *          or rejected from the MD
   * \param   enVideoFocus         : [IN] Video Focus Mode
   * \param   enVideoFocusReason   : [IN] Video Focus Reason
   * \retval  t_Void
   **************************************************************************/
   t_Void vVideoFocusCallback(tenVideoFocus enVideoFocus,
      tenVideoFocusReason enVideoFocusReason);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bGetAccVideoFocusState()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bGetAccVideoFocusState(tenDisplayContext enAccDispCntxt,
   *                 t_Bool bDisplayFlag,tenVideoFocusState& rfenVideoFocusState)
   * \brief   Method to get the current HMI requesting Video Focus state from the
   *          configuration file. Video Focus states for various contexts is defined
   *          based on the Carplay experience
   * \param   enAccDispCntxt : [IN] Accessory Display Context
   * \param   bDisplayFlag   : [IN] TRUE - HMI is requesting for the Video Focus
   *                          FALSE - HMI screen with the received context has lost focus
   * \param   rfenVideoFocusState : [OUT] HMI requested Video focus state
   * \retval  t_Bool - TRUE - if the received display context is available in the config
   *                   FALSE - if the received display context is not available in the config
   **************************************************************************/
   t_Bool bGetAccVideoFocusState(tenDisplayContext enAccDispCntxt,
      t_Bool bDisplayFlag, tenVideoFocusState& rfenVideoFocusState);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bGetUpdatedMDFocusState()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bGetUpdatedMDFocusState(tenVideoFocusState enCurMDFocusState,
   *                 tenVideoFocusState enCurAccFocusState,
   *                 tenVideoFocusState& rfenUpdatedMDFocusState)
   * \brief   Method to get the New MD Focus state based on the Current Hu & MD focus states
   * \param   enCurMDFocusState       : [IN] MD Current Focus State
   * \param   enCurAccFocusState      : [IN] Accessory Current Focus State
   * \param   rfenUpdatedMDFocusState : [OUT] MD New Focus State
   * \retval  t_Bool - TRUE - if the received focus states are available in the config
   *                   FALSE - if the received ocus states are not available in the config
   **************************************************************************/
   t_Bool bGetUpdatedMDFocusState(tenVideoFocusState enCurMDFocusState,
      tenVideoFocusState enCurAccFocusState,
      tenVideoFocusState& rfenUpdatedMDFocusState);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bGetAccRespType()
   ***************************************************************************/
   /*!
   * \fn      t_Bool bGetAccRespType(tenAAPMDVideoFocusReqResp& rfenRespType)
   * \brief   Method to get the Accessory Response for the MD Focus request 
   *          for PROJECTED_MODE
   * \param   rfenRespType    : [IN] Accessory Response for the Focus request
   * \retval  t_Bool - TRUE - if the current display context is available in the config
   **************************************************************************/
   t_Bool bGetAccRespType(tenAAPMDVideoFocusReqResp& rfenRespType);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vSetVideoFocus()
   ***************************************************************************/
   /*!
   * \fn      t_Void vSetVideoFocus(tenVideoFocus enVideoFocus,
   *                 t_Bool bUnsolicited)
   * \brief   Method to send the Video Focus request to the wrapper
   * \param   enVideoFocus : [IN] Video Focus Mode
   * \param   bUnsolicited : [IN] TRUE - Solicited
   *                         FALSE - unsolicited
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetVideoFocus(tenVideoFocus enVideoFocus,t_Bool bUnsolicited);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vVideoFocusCallback()
   ***************************************************************************/
   /*!
   * \fn      t_Void vVideoSetupCallback(tenMediaCodecTypes enMediaCodecType)
    * \brief   Method to inform the application that Video setup request is received
    *          from the MD
    * \param   enMediaCodecType  : [IN] Media Codec Type
    * \retval  t_Void
   **************************************************************************/
   t_Void vVideoSetupCallback(tenMediaCodecTypes enMediaCodecType);

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bDevAuthAndAccessTimerCb
    ***************************************************************************/
   /*!
    * \fn     t_Bool bDevAuthAndAccessTimerCb(timer_t rTimerID, t_Void *pvObject...)
    * \brief  called on expiry of Device Authorization and Access timer
    * \param  rTimerID: ID of the timer which has expired
    * \param  pvObject: pointer to object passed while starting the timer
    * \param  pvUserData: data passed during start of the timer
    **************************************************************************/
   static t_Bool bDevAuthAndAccessTimerCb(timer_t rTimerID, t_Void *pvObject,
            const t_Void *pvUserData);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vStartTimer()
    ***************************************************************************/
   /*!
    * \fn     t_Void vStartTimer();
    * \brief  Method to start the timer
    * \param  None
    * \retval  t_Void
    **************************************************************************/
   t_Void vStartTimer();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPVideoResourceMngr::vStopTimer()
    ***************************************************************************/
   /*!
    * \fn     t_Void vStopTimer();
    * \brief  Method to stop the timer
    * \param  None
    * \retval  t_Void
    **************************************************************************/
   t_Void vStopTimer();

   /***************************************************************************
    ** FUNCTION:  t_Bool spi_tclAAPVideoResourceMngr::bProcessLaunchApp()
    ***************************************************************************/
   /*!
    * \fn     t_Bool bProcessLaunchApp();
    * \brief  Method to check whether the launch app can be processed,
    *         based on the current display context.
    * \param  None
    * \retval  t_Bool
    **************************************************************************/
   t_Bool bProcessLaunchApp();

   //AAP resource manager pointer
   spi_tclAAPResourceMngr* m_poAAPRsrcMngr;

   //Current Video Focus State of MD
   tenVideoFocusState m_enCurMDFocusState;

   //Current Display context of HMI
   tenDisplayContext m_enCurAccDispCntxt;

   //Current Video Playback State
   tenVideoPlaybackState m_enPlaybackState;

   Lock m_oVideoPlaybackStateLock;

   Lock m_oCurMDFocusStateLock;

   //! Currently selected device
   t_U32 m_u32SelectedDeviceID;

   //! Initial Video Focus to MD.
   t_Bool m_bInitialMDVideoFocus;

   //! Lock for Video Setup
   Lock  m_oVideoSetupLock;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //class spi_tclAAPVideoResourceMngr

#endif //_SPI_TCLAAPVIDEORESOURCEMNGR_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
