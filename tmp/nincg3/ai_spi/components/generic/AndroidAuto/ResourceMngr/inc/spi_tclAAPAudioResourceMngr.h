/***********************************************************************/
/*!
* \file         spi_tclAAPAudioResourceMngr.h
* \brief
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
20.03.2015  | Ramya Murthy          | Initial Version

\endverbatim
*************************************************************************/

#ifndef _SPI_TCLAAPAUDIORESOURCEMNGR_H_
#define _SPI_TCLAAPAUDIORESOURCEMNGR_H_

#include "SPITypes.h"
#include "spi_tclAAPRespSession.h"

class spi_tclAAPAudioResArbitrator;

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/


/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class
* \brief
*
*
*
****************************************************************************/
class spi_tclAAPAudioResourceMngr : public spi_tclAAPRespSession
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAudioResourceMngr::spi_tclAAPAudioResourceMngr()
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAudioResourceMngr()
   * \brief   Default Constructor
   * \param   t_Void
   * \sa      ~spi_tclAAPAudioResourceMngr()
   **************************************************************************/
   spi_tclAAPAudioResourceMngr();

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAudioResourceMngr::~spi_tclAAPAudioResourceMngr()
   ***************************************************************************/
   /*!
   * \fn      ~spi_tclAAPAudioResourceMngr()
   * \brief   Destructor
   * \param   t_Void
   * \sa      spi_tclAAPAudioResourceMngr()
   **************************************************************************/
   ~spi_tclAAPAudioResourceMngr();

   /***************************************************************************
   ** FUNCTION:  t_Void  spi_tclAAPAudioResourceMngr::vOnSPISelectDeviceResult()
   ***************************************************************************/
   /*!
   * \fn      t_Void vOnSPISelectDeviceResult()
   * \brief   Interface to receive result of SPI device selection/deselection
   * \param   u32DevID : [IN] Resource Manager callbacks structure.
   * \param   enDeviceConnReq : [IN] Select/ deselect.
   * \param   enRespCode : [IN] Response code (success/failure)
   * \param   enErrorCode : [IN] Error
   * \retval  t_Void
   **************************************************************************/
   t_Void vOnSPISelectDeviceResult(t_U32 u32DevID, tenDeviceConnectionReq enDeviceConnReq,
         tenResponseCode enRespCode, tenErrorCode enErrorCode);

   /**********Start of functions overridden from spi_tclAAPRespSession*********/

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPAudioResourceMngr::vAudioFocusRequestCb()
   ***************************************************************************/
   /*!
   * \fn      vAudioFocusRequestCb
   * \brief   Called when the source wishes to acquire audio focus.
   * \param   enDevAudFocusRequest : [IN] Indicates audio focus requested by device
   **************************************************************************/
   t_Void vAudioFocusRequestCb(tenAAPDeviceAudioFocusRequest enDevAudFocusRequest);

   /**********End of functions overridden from spi_tclAAPRespSession**********/

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPAudioResourceMngr::vSetAccessoryAudioContext(...)
   ***************************************************************************/
   /*!
   * \fn     vSetAccessoryAudioContext(const t_U32 cou32DevId, const t_U8 cu8AudioCntxt
   *           t_Bool bDisplayFlag)
   * \brief   To send accessory audio context related info .
   * \pram    cou32DevId  : [IN] Uniquely identifies the target Device.
   * \param   cu8AudioCntxt : [IN] Audio Context
   * \pram    bReqFlag : [IN] Request/ Release flag
   * \retval  t_Void
   **************************************************************************/
   t_Void vSetAccessoryAudioContext(const t_U32 cou32DevId,
         const tenAudioContext coenAudioCntxt,
         t_Bool bReqFlag);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPAudioResourceMngr::vSetAudioFocus(...)
   ***************************************************************************/
   /*!
   * \fn      vSetAudioFocus(tenAAPDeviceAudioFocusState enDevAudFocusState,
   *              t_Bool bUnsolicited)
   * \brief   Sets the Audio Focus type to device
   * \param  enDevAudFocusRequest : [IN] Indicates audio focus requested by device
   * \param  bUnsolicited : [IN]
   *           True - if audio focus is changed due to audio context change
   *           False - if audio focus is changed on device request
   **************************************************************************/
   t_Void vSetAudioFocus(tenAAPDeviceAudioFocusState enDevAudFocusState,
         t_Bool bUnsolicited);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAudioResourceMngr(const spi_tclAAPAudioResourceMngr...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAudioResourceMngr(const spi_tclAAPAudioResourceMngr& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPAudioResourceMngr()
   ***************************************************************************/
   spi_tclAAPAudioResourceMngr(const spi_tclAAPAudioResourceMngr& corfoSrc);

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPAudioResourceMngr& operator=( const spi_tclAAP...
   ***************************************************************************/
   /*!
   * \fn      spi_tclAAPAudioResourceMngr& operator=(
   *                          const spi_tclAAPAudioResourceMngr& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source data interface object
   * \retval
   * \sa      spi_tclAAPAudioResourceMngr(const spi_tclAAPAudioResourceMngr& otrSrc)
   ***************************************************************************/
   spi_tclAAPAudioResourceMngr& operator=(const spi_tclAAPAudioResourceMngr& corfoSrc);


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   //! Pointer to Audio resource arbitrator
   spi_tclAAPAudioResArbitrator* m_poAudioResArb;

   //! Stores device handle of selected device
   t_U32 m_u32CurSelectedDevId;
   //TODO - accessed from different thread contexts. use mutex?

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //class spi_tclAAPAudioResourceMngr

#endif //_SPI_TCLAAPAUDIORESOURCEMNGR_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
