/***********************************************************************/
/*!
* \file  spi_tclAAPDataService.h
* \brief AAP Data Service Class
*************************************************************************
\verbatim

PROJECT        :   Gen3
SW-COMPONENT   :   Smart Phone Integration
DESCRIPTION    :   Android Auto Data Service class implements Data Service Info Management for
                    Android Auto capable devices. This class must be derived from base Data Service class.
AUTHOR         :   SHITANSHU SHEKHAR (RBEI/ECP2)
COPYRIGHT      :   &copy; RBEI

HISTORY:
Date        | Author                         | Modification
24.03.2015  | SHITANSHU SHEKHAR (RBEI/ECP2)  | Initial Version
30.10.2015  | Shiva Kumar G                  | Implemented ReportEnvironment Data feature

\endverbatim
*************************************************************************/
#include "spi_tclDataServiceSettings.h"
#include <cmath>

#include "spi_tclAAPDataService.h"
#include "spi_tclMediator.h"
#include "spi_tclConfigReader.h"
#include "Timer.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_DATASERVICE
      #include "trcGenProj/Header/spi_tclAAPDataService.cpp.trc.h"
   #endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const trAAPSelectedDeviceInfo corEmptyDeviceInfo;
static timer_t srOutsideTempUpdateTimerID;
static const t_U32 scou32OutsideTempUpdateTimerVal = 10000;
static t_Bool sbOutsideTempUpdateTimerRunning = false;

static timer_t srVehSpeedTimerID;
static t_Bool sbVehSpeedTimerRunning = false;


/****************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDataService::spi_tclAAPDataService()
 ***************************************************************************/
spi_tclAAPDataService::spi_tclAAPDataService(const trDataServiceCb& rfcorDataServiceCb)
: m_rDataServiceCb(rfcorDataServiceCb),
  m_enDayNightMode(e8_AAP_DAY_MODE),
  m_bParkBrakeActive(false),
  m_enVehMovState(e8VEHICLE_MOVEMENT_STATE_INVALID),
  m_s32AAPParkModeRestrictionInfo(static_cast<int>(e8_DRIVE_STATUS_UNRESTRICTED)),
  m_s16VehicleSpeed(0),
  m_dOutsideTemp(0),
  m_bOutsideTempValidityFlag(false),
  m_dBarometricPressure(0),
  m_bBaroPressureValidityFlag(false)
{
   ETG_TRACE_USR1(("spi_tclAAPDataService() entered "));

}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDataService::~spi_tclAAPDataService()
 ***************************************************************************/
spi_tclAAPDataService::~spi_tclAAPDataService()
{
   ETG_TRACE_USR1(("~spi_tclAAPDataService() entered "));
   vStopVehicleSpeedUpdate();
   m_poAAPManager = NULL;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPDataService::bInitialise()
 ***************************************************************************/
t_Bool spi_tclAAPDataService::bInitialise()
{
   ETG_TRACE_USR1(("spi_tclAAPDataService::bInitialise() entered "));

   //! If data service is not initialised, perform initializations which are
   // not device dependent.
   t_Bool bInitSuccess = false;
   m_poAAPManager = spi_tclAAPManager::getInstance();

   if (NULL != m_poAAPManager)
   {
      bInitSuccess = true;
      vRegisterCallbacks();
   }// if (NULL != m_poAAPManager)

   ETG_TRACE_USR2(("[DESC]:spi_tclAAPDataService::bInitialise() left with result = %u ", bInitSuccess));
   return bInitSuccess;
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPDataService::bUninitialise()
 ***************************************************************************/
t_Bool spi_tclAAPDataService::bUninitialise()
{
   ETG_TRACE_USR1(("spi_tclAAPDataService::bUninitialise() entered "));

   t_Bool bUnInitSuccess = false;

   RELEASE_MEM(m_poAAPManager);

   return bUnInitSuccess;
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPDataService::vRegisterCallbacks()
***************************************************************************/
t_Void spi_tclAAPDataService::vRegisterCallbacks()
{
   /*lint -esym(40,fvSetDayNightMode) fvSetDayNightMode is not declared */
   /*lint -esym(40,_1) _1is not declared */
   ETG_TRACE_USR1((" spi_tclAAPDataService::vRegisterCallbacks() Entered "));

   trAAPSensorCallback rDayNightCbs;
   rDayNightCbs.fvSetDayNightMode = std::bind(
      &spi_tclAAPDataService::vOnSetDayNightMode, this,
      std::placeholders::_1);
   spi_tclMediator* poMediator = spi_tclMediator::getInstance();
   if (NULL != poMediator)
   {
      poMediator->vRegisterCallbacks(rDayNightCbs);
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPDataService::vOnSetDayNightMode()
 ***************************************************************************/
t_Void spi_tclAAPDataService::vOnSetDayNightMode(tenVehicleConfiguration enVehicleConfig)
{
   ETG_TRACE_USR1(("[DESC]:spi_tclAAPDataService::vOnSetDayNightMode() entered "
         "with Day/Night mode =%d ", enVehicleConfig));
   m_enDayNightMode = (e8_DAY_MODE == enVehicleConfig)?(e8_AAP_DAY_MODE):(e8_AAP_NIGHT_MODE);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPDataService::vSelectDevice(t_U32...)
 ***************************************************************************/
t_Void spi_tclAAPDataService::vSelectDevice(const t_U32 cou32DevId,
      tenDeviceConnectionReq enConnReq)
{
   /*lint -esym(40,fvSubscribeForEnvData) fvSubscribeForEnvData Undeclared identifier */

   ETG_TRACE_USR1(("spi_tclAAPDataService::vSelectDevice() entered "));
   if(e8DEVCONNREQ_DESELECT == enConnReq)
   {
      vStopVehicleSpeedUpdate();

      spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
               (m_poAAPManager->poGetSensorInstance()) : (NULL);

      if ((NULL != poCmdSensor) && (cou32DevId == m_rSelDevInfo.u32DeviceHandle))
      {
         //! Clear Selected device's info
         m_rSelDevInfo = corEmptyDeviceInfo;

         vPostSetLocDataSubscription(false);

         //Un subscribe for the Outside temperature updates and stop the timer 

         if( true == m_rAAPDataServiceConfigData.bEnvData)
         {
            vStopOutsideTempUpdateTimer();

            if(NULL != m_rDataServiceCb.fvSubscribeForEnvData)
            {
               m_rDataServiceCb.fvSubscribeForEnvData(false);
            }//if(NULL != m_rDataServiceCb.fvSubscribeForEnvData)

         }

         m_dOutsideTemp = 0;
         m_bOutsideTempValidityFlag = false;
         m_dBarometricPressure = 0;
         m_bBaroPressureValidityFlag = false;


         poCmdSensor->bUnInitializeSensor();
         ETG_TRACE_USR2(("[DESC]:spi_tclAAPDataService::vOnDeselectDevice: Sensor end point destroyed  SUCCESSFULLY "));

      }//if ((u32DeviceHandle == m_rSelDevInfo.u32DeviceHandle))
   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAAPDataService::vOnSelectDeviceResult()
 ***************************************************************************/
t_Void spi_tclAAPDataService::vOnSelectDeviceResult(t_U32 u32DeviceHandle)
{
   /*lint -esym(40,fvSubscribeForEnvData) fvSubscribeForEnvData Undeclared identifier */

   ETG_TRACE_USR1(("spi_tclAAPDataService::vOnSelectDeviceResult() entered: u32DeviceHandle = %u ",
         u32DeviceHandle));

   //! Clear Selected device's info & update the newly selected device's handle.
   m_rSelDevInfo = corEmptyDeviceInfo;
   m_rSelDevInfo.u32DeviceHandle = u32DeviceHandle;

   // Initialize AAPCMDSENSOR HERE using AAP Manager
   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
      (m_poAAPManager->poGetSensorInstance()) : (NULL);

   if ((NULL != poCmdSensor) && 
      (true == poCmdSensor->bInitializeSensor(m_rAAPDataServiceConfigData)))
   {
      ETG_TRACE_USR2(("[DESC]:spi_tclAAPDataService::vOnSelectDeviceResult: Sensor end point creation  SUCCESSFUL "));
      vPostSetLocDataSubscription(true);

      if(true == m_rAAPDataServiceConfigData.bEnvData)
      {
         if(NULL != m_rDataServiceCb.fvSubscribeForEnvData)
         {
            m_rDataServiceCb.fvSubscribeForEnvData(true);
         }//if(NULL != m_rDataServiceCb.fvSubscribeForEnvData)

         vReportEnvironmentData(m_bOutsideTempValidityFlag,m_dOutsideTemp,
            m_bBaroPressureValidityFlag,m_dBarometricPressure);

         //Outside Temperature needs to be updated with a frequency of 0.1 Hz (Once in every 10sec).
         vStartOutsideTempUpdateTimer();
      }

      poCmdSensor->vReportDayNightMode(m_enDayNightMode);

      vReportParkingBrake();
      vReportGear();
      vParkRestrictionInfo();
      vReportDrivingStatus();

      vStartVehicleSpeedUpdate();
   }//if ((NULL != poCmdSensor) && (true == poCmdSensor->bInitializeSensor()))
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPDataService::vOnDeselectDeviceResult(t_U32...)
 ***************************************************************************/
t_Void spi_tclAAPDataService::vOnDeselectDeviceResult(t_U32 u32DeviceHandle)
{
   ETG_TRACE_USR1(("[DESC]:spi_tclAAPDataService::vOnDeselectDeviceResult() entered: u32DeviceHandle = %u ", u32DeviceHandle));
   //! Nothing to be done
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vPostSetLocDataSubscription(...)
***************************************************************************/
t_Void spi_tclAAPDataService::vPostSetLocDataSubscription(t_Bool bSubscriptionOn)
{
   /*lint -esym(40,fvSubscribeForLocationData) fvSubscribeForLocationData is not declared */
   ETG_TRACE_USR1(("[DESC]:spi_tclAAPDataService::vPostSetLocDataSubscription() entered: bSubscriptionOn = %u ",
      ETG_ENUM(BOOL, bSubscriptionOn)));

   if (NULL != m_rDataServiceCb.fvSubscribeForLocationData)
   {
      m_rDataServiceCb.fvSubscribeForLocationData(bSubscriptionOn, e8GPS_DATA);
   }
}


/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vOnData(const trGPSData& rfcorGpsData)
***************************************************************************/
t_Void spi_tclAAPDataService::vOnData(const trGPSData& rfrcGpsData)
{
   SPI_INTENTIONALLY_UNUSED(rfrcGpsData);
//	t_S32 s32GyroZE3 = static_cast<t_S32>((((t_Double)rfrcGpsData.s16TurnRate/100)* 0.0174532925)* 1000);    // gyro sent in rad/s
//	t_S32 s32AccelerationYE3 = (rfrcGpsData.s16Acceleration/100) *1000 ;										// acceleration is in m/s2
//                                     //TODO Commented for legality permission
//	if(NULL != m_poAAPManager)
//	{
//	   if(NULL != m_poAAPManager->poGetSensorInstance())
//	   {
//	       m_poAAPManager->poGetSensorInstance()->vReportAccelerometerData(false,0,true,s32AccelerationYE3,false,0);
//	       m_poAAPManager->poGetSensorInstance()->vReportGyroscopeData(false,0,false,0,true,s32GyroZE3);
//	   }
//   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vSetLocDataAvailablility(t_Bool bLocDataAvailable);
***************************************************************************/
t_Void spi_tclAAPDataService::vSetSensorDataAvailablility(const trDataServiceConfigData& rfrDataServiceConfigInfo)
{
   ETG_TRACE_USR1(("[DESC]:spi_tclAAPDataService::vSetLocDataAvailablility() entered: "
         "bLocDataAvailable = %d , bIsDeadReckonedData = %d ",
         rfrDataServiceConfigInfo.bLocDataAvailable, rfrDataServiceConfigInfo.bDeadReckonedData));
         m_rAAPDataServiceConfigData = rfrDataServiceConfigInfo;
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vOnData(const trSensorData& rfcorSensorData)
***************************************************************************/
t_Void spi_tclAAPDataService::vOnData(const trSensorData& rfcorSensorData)
{
   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
         (m_poAAPManager->poGetSensorInstance()) : (NULL);

   if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))
   {
       poCmdSensor->vReportGpsData(rfcorSensorData);

	   t_S32 numberInUse = static_cast<int>(rfcorSensorData.u16SatellitesUsed);
	   t_Bool hasNumberInView = false;		//TODO Need to decide what to pass as an arguement
	   t_S32 numberInView = 0;				//TODO Need to decide what to pass as an arguement
	   const t_S32* prns = NULL;
	   const t_S32* snrsE3 = NULL;
	   const t_Bool* usedInFix = NULL;
	   const t_S32* azimuthsE3 = NULL;
	   const t_S32* elevationsE3 = NULL;
	   poCmdSensor->vReportGpsSatelliteData(numberInUse, hasNumberInView,
                                          numberInView,  prns, snrsE3,
                                          usedInFix,  azimuthsE3, elevationsE3);

   }//if (NULL != poCmdSensor)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vOnAccSensorData
** (const std::vector<trAccSensorData>& corfvecrAccSensorData)
***************************************************************************/
t_Void spi_tclAAPDataService::vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
{
   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
		   (m_poAAPManager->poGetSensorInstance()) : (NULL);

   if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))
   {
	  ETG_TRACE_USR2(("[DESC]:spi_tclAAPDataService ::vOnAccSensorData Acc list size = %d", corfvecrAccSensorData.size()));

      for(tU16 u16Index = 0 ; (u16Index < corfvecrAccSensorData.size()) && (u16Index < 20) ; ++u16Index)
      {
    	  // Convert to axes requested by Google
         t_S32 s32AccZE3 = static_cast<t_S32>((corfvecrAccSensorData[u16Index].fAccZ) * (1000));
         t_S32 s32AccYE3 = static_cast<t_S32>((corfvecrAccSensorData[u16Index].fAccX) * (1000));
         t_S32 s32AccXE3 = (-1) * (static_cast<t_S32>((corfvecrAccSensorData[u16Index].fAccY) * (1000)));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]: spi_tclAAPDataService ::vOnAccSensorData Acc Values"
  	   		" s32AccZE3 = %d, s32AccYE3 = %d, s32AccXE3 = %d", s32AccZE3,
  	   		s32AccYE3, s32AccXE3));
	     poCmdSensor->vReportAccelerometerData(true,s32AccXE3,true,s32AccYE3,true,s32AccZE3);
      }
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vOnGyroSensorData
** (const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
***************************************************************************/
t_Void spi_tclAAPDataService::vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
{
   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
		   (m_poAAPManager->poGetSensorInstance()) : (NULL);

   if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))
   {
	  ETG_TRACE_USR2(("[DESC]:spi_tclAAPDataService ::vOnGyroSensorData Gyro list size = %d", corfvecrGyroSensorData.size()));

	  for(tU16 u16Index = 0 ; (u16Index < corfvecrGyroSensorData.size()) && (u16Index < 20) ; ++u16Index)
	  {
	     // Convert to axes requested by Google and rad/s
         t_S32 s32GyroZE3 = static_cast<t_S32>((corfvecrGyroSensorData[u16Index].fGyroZ) * (3.1415 / 180.0) * (1000));
         t_S32 s32GyroYE3 = static_cast<t_S32>((corfvecrGyroSensorData[u16Index].fGyroX) * (3.1415 / 180.0) * (1000));
         t_S32 s32GyroXE3 = (-1) * (static_cast<t_S32>((corfvecrGyroSensorData[u16Index].fGyroY) * (3.1415 / 180.0) * (1000)));

         ETG_TRACE_USR4_CLS((TR_CLASS_SMARTPHONEINT_SENSORDATA, "[PARAM]: spi_tclAAPDataService ::vOnGyroSensorData Gyro Values"
  				" s32GyroZE3 = %d, s32GyroYE3 = %d, s32GyroXE3 = %d", s32GyroZE3,
  				s32GyroYE3, s32GyroXE3));
	     poCmdSensor->vReportGyroscopeData(true,s32GyroXE3,true,s32GyroYE3,true,s32GyroZE3);
	  }
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vOnData(const trVehicleData rfrcVehicleData)
***************************************************************************/
t_Void spi_tclAAPDataService::vOnData(const trVehicleData& rfrcVehicleData, t_Bool bSolicited)
{
   //! Store Vehicle speed info
   m_s16VehicleSpeed = rfrcVehicleData.s16Speed;

   //! Send data to selected phone on vehicle data change
   if (false == bSolicited)
   {
      if(0 != m_rSelDevInfo.u32DeviceHandle)
      {
         vSetVehicleData(rfrcVehicleData.bParkBrakeActive, rfrcVehicleData.enVehMovState);
      }
      else
      {
         //! Store vehicle data since no device is selected
         ETG_TRACE_USR4(("[PARAM]:spi_tclAAPDataService::vOnData() entered "
               "GearPosition = %d, ParkBrakeActive = %d, VehMovState = %d  ",
               ETG_ENUM(GEARPOSITION, rfrcVehicleData.enGearPosition),
               rfrcVehicleData.bParkBrakeActive, ETG_ENUM(VEH_MOV_STATE, rfrcVehicleData.enVehMovState) ));

         //! Set park brake and vehicle movement state
         m_bParkBrakeActive = (rfrcVehicleData.bParkBrakeActive);
         m_enVehMovState = (rfrcVehicleData.enVehMovState);
      }
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclAAPDataService::vSetVehicleData()
***************************************************************************/
t_Void spi_tclAAPDataService::vSetVehicleData(t_Bool bParkBrakeActive,
      tenVehicleMovementState enVehMovState)
{
   //! Send changed ParkBrake and Gear data
   if ((bParkBrakeActive != m_bParkBrakeActive) || (enVehMovState != m_enVehMovState)) /*if states have changed*/
   {
      if (bParkBrakeActive != m_bParkBrakeActive)
      {
         //! Store latest state
         m_bParkBrakeActive = bParkBrakeActive;

         vReportParkingBrake();
      }
      if (enVehMovState != m_enVehMovState)
      {
         //! Store latest state
         m_enVehMovState = enVehMovState;
         vReportGear();
      }

      vReportDrivingStatus();
   }//if ((bParkBrakeActive != m_bParkBrakeActive) || (enVehMovState != m_enVehMovState))
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vReportParkingBrake()
***************************************************************************/
t_Void spi_tclAAPDataService::vReportParkingBrake()
{
	ETG_TRACE_USR1(("[DESC]:spi_tclAAPDataService::vReportParkingBrake() entered: ParkBrakeActive = %d ",
	      ETG_ENUM(BOOL, m_bParkBrakeActive)));

   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
         (m_poAAPManager->poGetSensorInstance()) : (NULL);

   //! Send park mode data
   if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))
   {
      poCmdSensor->vReportParkingBrakeData(m_bParkBrakeActive);
	}
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPDataService::vReportGear()
***************************************************************************/
t_Void spi_tclAAPDataService::vReportGear()
{
	ETG_TRACE_USR1(("[DESC]:spi_tclAAPDataService::vReportGear() entered: VehMovState = %d ",
	      ETG_ENUM(VEH_MOV_STATE, m_enVehMovState)));

   //! Send Gear data
   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
            (m_poAAPManager->poGetSensorInstance()) : (NULL);

	if ((e8VEHICLE_MOVEMENT_STATE_INVALID != m_enVehMovState) && (NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))
   {
      switch(m_enVehMovState)
      {
         case e8VEHICLE_MOVEMENT_STATE_PARKED       :
            poCmdSensor->vReportGearData(static_cast<int>(e8_AAP_GEAR_PARK));
            break;
         case e8VEHICLE_MOVEMENT_STATE_NEUTRAL :
            poCmdSensor->vReportGearData(static_cast<int>(e8_AAP_GEAR_NEUTRAL));
          break;
         case e8VEHICLE_MOVEMENT_STATE_FORWARD     :
            poCmdSensor->vReportGearData(static_cast<int>(e8_AAP_GEAR_DRIVE));
            break;
         case e8VEHICLE_MOVEMENT_STATE_REVERSE     :
            poCmdSensor->vReportGearData(static_cast<int>(e8_AAP_GEAR_REVERSE));
            break;
         case e8VEHICLE_MOVEMENT_STATE_INVALID        :
         default :
            //Nothing to do.
            break;
       }//switch(m_enVehMovState)
   }//if (NULL != poCmdSensor)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPDataService::vParkRestrictionInfo()
 ***************************************************************************/
t_Void spi_tclAAPDataService::vParkRestrictionInfo()
{
	ETG_TRACE_USR1(("spi_tclAAPDataService::vParkRestrictionInfo "));
	spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();
	m_s32AAPParkModeRestrictionInfo= poConfigReader->u8GetAAPParkRestrictionInfo();
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPDataService::vReportDrivingStatus()
 ***************************************************************************/
t_Void spi_tclAAPDataService::vReportDrivingStatus()
{
   //! Set driving status
   tenDrivingMode enDrivingMode =
         ((m_bParkBrakeActive) || (e8VEHICLE_MOVEMENT_STATE_PARKED == m_enVehMovState)) ?
         (e8_PARK_MODE) : (e8_DRIVE_MODE);

	t_S32 s32AAPRestrictionInfo = m_s32AAPParkModeRestrictionInfo;

   //! Read drive level restriction info
   spi_tclConfigReader *poConfigReader = spi_tclConfigReader::getInstance();
   if ((e8_DRIVE_MODE == enDrivingMode) && (NULL != poConfigReader))
   {
      s32AAPRestrictionInfo = poConfigReader->u8GetAAPDriveRestrictionInfo();
   }
   
   ETG_TRACE_USR4(("[PARAM]:spi_tclAAPDataService::vReportDrivingStatus(): DriveMode = %d, AAPRestrictionInfo = %d ",
         ETG_ENUM(AAP_DRIVING_MODE, enDrivingMode), s32AAPRestrictionInfo));

   //! Send park mode data
   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
         (m_poAAPManager->poGetSensorInstance()) : (NULL);
   if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))
   {
      poCmdSensor->vReportDrivingStatusData(s32AAPRestrictionInfo);
   }
}
/***************************************************************************
 ** FUNCTION:  spi_tclAAPDataService::vReportVehicleSpeedInfo
 ***************************************************************************/
t_Void spi_tclAAPDataService::vReportVehicleSpeedInfo()
{
   //ETG_TRACE_USR1(("spi_tclAAPDataService::vReportVehicleSpeedInfo entered "));

   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
                     (m_poAAPManager->poGetSensorInstance()) : (NULL);

   //! Send Vehicle speed data
   if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))
   {
      t_S32 s32SpeedE3 = ((m_s16VehicleSpeed) * 10);  // convert to m/s
      s32SpeedE3 = (e8VEHICLE_MOVEMENT_STATE_REVERSE == m_enVehMovState) ? ((-1)*(s32SpeedE3)):(s32SpeedE3);
      poCmdSensor->vReportSpeedData(s32SpeedE3);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDataService::vStartVehicleSpeedUpdate
 ***************************************************************************/
t_Void spi_tclAAPDataService::vStartVehicleSpeedUpdate()
{
   ETG_TRACE_USR1(("spi_tclAAPDataService::vStartVehicleSpeedUpdate entered "));

   //! Start timer to send VehicleSpeed data at 10Hz rate
   Timer* poTimer = Timer::getInstance();
   if ((false == sbVehSpeedTimerRunning) && (NULL != poTimer))
   {
      poTimer->StartTimer(srVehSpeedTimerID, 100, 100, this,
                        &spi_tclAAPDataService::bVehicleSpeedTimerCb, NULL);
      ETG_TRACE_USR2(("[DESC]:Vehicle speed timer started"));
      sbVehSpeedTimerRunning = true;
   }//End of if (NULL != poTimer)
   else if (true == sbVehSpeedTimerRunning)
   {
      ETG_TRACE_ERR(("[ERR]:Vehicle speed timer already running"));
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDataService::vStopVehicleSpeedUpdate
 ***************************************************************************/
t_Void spi_tclAAPDataService::vStopVehicleSpeedUpdate()
{
   ETG_TRACE_USR1(("spi_tclAAPDataService::vStopVehicleSpeedUpdate entered "));

   //! Stop timer
   Timer* poTimer = Timer::getInstance();
   if ((true == sbVehSpeedTimerRunning) && (NULL != poTimer))
   {
      poTimer->CancelTimer(srVehSpeedTimerID);
      ETG_TRACE_USR2(("[DESC]:Vehicle speed timer stopped "));

      sbVehSpeedTimerRunning = false;
   }//End of if (NULL != poTimer)
   else if (false == sbVehSpeedTimerRunning)
   {
      ETG_TRACE_ERR(("[ERR]:Vehicle speed timer not running"));
   }
}

//!Static
/***************************************************************************
 ** FUNCTION:  spi_tclAAPDataService::bVehicleSpeedTimerCb
 ***************************************************************************/
t_Bool spi_tclAAPDataService::bVehicleSpeedTimerCb(
         timer_t rTimerID, t_Void *pvObject, const t_Void *pvUserData)
{
   SPI_INTENTIONALLY_UNUSED(pvUserData);
   SPI_INTENTIONALLY_UNUSED(rTimerID);
   //ETG_TRACE_USR1(("spi_tclAAPDataService::bVehicleSpeedTimerCb entered "));

   spi_tclAAPDataService* poAAPDataService = static_cast<spi_tclAAPDataService*> (pvObject);
   if (NULL != poAAPDataService)
   {
      poAAPDataService->vReportVehicleSpeedInfo();
   }

   return true;
}

/***************************************************************************
** FUNCTION:  spi_tclAAPDataService::vReportEnvironmentData()
***************************************************************************/
t_Void spi_tclAAPDataService::vReportEnvironmentData(t_Bool bValidTempUpdate,
                                                     t_Double dTemp,
                                                     t_Bool bValidPressureUpdate, 
                                                     t_Double dPressure)
{
   ETG_TRACE_USR1(("spi_tclAAPDataService::vReportEnvironmentData entered"));

   t_Bool bResetTempUpdateTimer = false;
   //Pressure & Temperature updates needs to be stored, because in case of G3G projects,
   //HMI would set this before the AAP device becomes active and then doesn't update later, if there is no change.
   if(true == bValidTempUpdate)
   {
      m_dOutsideTemp = dTemp;
      m_bOutsideTempValidityFlag = bValidTempUpdate;

      bResetTempUpdateTimer = true;
   }//if(true == bValidTempUpdate)

   if(true == bValidPressureUpdate)
   {
      m_dBarometricPressure = dPressure;
      m_bBaroPressureValidityFlag = bValidPressureUpdate;
   }//if(true == bValidPressureUpdate)

   spi_tclAAPCmdSensor* poCmdSensor = (NULL != m_poAAPManager)?
      (m_poAAPManager->poGetSensorInstance()) : (NULL);

   if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle) && (true == m_rAAPDataServiceConfigData.bEnvData))
   {
      if(true == bResetTempUpdateTimer)
      {
         vStopOutsideTempUpdateTimer();
         vStartOutsideTempUpdateTimer();
      }//if(true == bResetTempUpdateTimer)
      poCmdSensor->vReportEnvironmentData(bValidTempUpdate,dTemp,bValidPressureUpdate,dPressure);
   }//if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))
}

/***************************************************************************
** FUNCTION:  t_Bool spi_tclAAPDataService::bTriggerOutsideTempUpdateCb
***************************************************************************/
t_Bool spi_tclAAPDataService::bTriggerOutsideTempUpdateCb(timer_t rTimerID, 
                                                          t_Void *pvObject,
                                                          const t_Void *pvUserData)
{
   /*lint -esym(40,fpvDeviceAuthAndAccessCb) fpvDeviceAuthAndAccessCb Undeclared identifier */

   ETG_TRACE_USR1(("spi_tclAAPDataService::bTriggerOutsideTempUpdateCb entered"));

   SPI_INTENTIONALLY_UNUSED(pvUserData);
   SPI_INTENTIONALLY_UNUSED(rTimerID);

   if(NULL != pvObject)
   {
      spi_tclAAPDataService* poAAPVDataSrvc = static_cast<spi_tclAAPDataService*> (pvObject);

      spi_tclAAPCmdSensor* poCmdSensor = (NULL != poAAPVDataSrvc->m_poAAPManager)?
         (poAAPVDataSrvc->m_poAAPManager->poGetSensorInstance()) : (NULL);

      if ((NULL != poCmdSensor) && (0 != poAAPVDataSrvc->m_rSelDevInfo.u32DeviceHandle))
      {
         t_Double dPressure = 0;
         poCmdSensor->vReportEnvironmentData(poAAPVDataSrvc->m_bOutsideTempValidityFlag, poAAPVDataSrvc->m_dOutsideTemp,
            false, dPressure);
      }//if ((NULL != poCmdSensor) && (0 != m_rSelDevInfo.u32DeviceHandle))

   }//End of if (NULL != poAAPVideo)

   return true;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDataService::vStartOutsideTempUpdateTimer
 ***************************************************************************/
t_Void spi_tclAAPDataService::vStartOutsideTempUpdateTimer()
{
   Timer* poTimer = Timer::getInstance();

   if((false == sbOutsideTempUpdateTimerRunning)&&(NULL != poTimer))
   {
      poTimer->StartTimer(srOutsideTempUpdateTimerID,
               scou32OutsideTempUpdateTimerVal, scou32OutsideTempUpdateTimerVal, this,
               &spi_tclAAPDataService::bTriggerOutsideTempUpdateCb, NULL);

      ETG_TRACE_USR2(("[DESC]:OutsideTempUpdateTimer started"));

      sbOutsideTempUpdateTimerRunning = true ;

   }//End of if(NULL != poTimer)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAAPDataService::vStopOutsideTempUpdateTimer
 ***************************************************************************/
t_Void spi_tclAAPDataService::vStopOutsideTempUpdateTimer()
{
   Timer* poTimer = Timer::getInstance();

   if ((true == sbOutsideTempUpdateTimerRunning)&&(NULL != poTimer) )
   {
      sbOutsideTempUpdateTimerRunning = false;
      poTimer->CancelTimer(srOutsideTempUpdateTimerID);
      ETG_TRACE_USR2(("[DESC]:OutsideTempUpdateTimer Stopped"));
   }//End of if (NULL != poTimer)...
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAAPDataService::vSetFeatureRestrictions(...)
 ***************************************************************************/
t_Void spi_tclAAPDataService::vSetFeatureRestrictions(const t_U8 cou8ParkModeRestrictionInfo,
      const t_U8 cou8DriveModeRestrictionInfo)
{
   ETG_TRACE_USR1(("[DESC]:spi_tclAAPDataService::vSetFeatureRestrictions() Entered with"
         "Park mode Restriction = %d, Drive mode Restriction = %d ",
         cou8ParkModeRestrictionInfo, cou8DriveModeRestrictionInfo));

   SPI_INTENTIONALLY_UNUSED(cou8DriveModeRestrictionInfo);

   m_s32AAPParkModeRestrictionInfo = cou8ParkModeRestrictionInfo;
   //Send Driving Status
   vReportDrivingStatus();
}

///////////////////////////////////////////////////////////////////////////////
//lint restore
// <EOF>
