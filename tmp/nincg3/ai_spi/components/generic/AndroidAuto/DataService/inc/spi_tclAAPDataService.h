/***********************************************************************/
/*!
* \file  spi_tclAAPDataService.h
* \brief AAP Data Service Class
*************************************************************************
\verbatim

PROJECT			:   Gen3
SW-COMPONENT	:   Smart Phone Integration
DESCRIPTION		:   Android Auto Data Service class implements Data Service Info Management for
 	 	 	 	 	Android Auto capable devices. This class must be derived from base Data Service class.
AUTHOR			:   SHITANSHU SHEKHAR (RBEI/ECP2)
COPYRIGHT		:   &copy; RBEI

HISTORY:
Date        | Author                         | Modification
24.03.2015  | SHITANSHU SHEKHAR (RBEI/ECP2)  | Initial Version
30.10.2015  | Shiva Kumar G                  | Implemented ReportEnvironment Data feature
\endverbatim
*************************************************************************/

#ifndef SPI_TCLAAPDATASERVICE_H_
#define SPI_TCLAAPDATASERVICE_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "SPITypes.h"
#include "spi_tclDataServiceDevBase.h"
#include "spi_tclDataServiceTypes.h"
#include "spi_tclAAPManager.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
struct trAAPSelectedDeviceInfo
{
   t_U32 u32DeviceHandle;

   trAAPSelectedDeviceInfo():
      u32DeviceHandle(0)
   {
   }
};

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/* Forward declarations */


/*!
 * \class spi_tclAAPDataService
 * \brief AAP Connection class implements Data Service Info Management for
 *        AAP capable devices. This class must be derived from base
 *        Data Service class.
 */

class spi_tclAAPDataService :
   public spi_tclDataServiceDevBase  //! Base Connection Class
{
public:
   /***************************************************************************
    *********************************PUBLIC*************************************
    ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPDataService::spi_tclAAPDataService(const trDataServiceCb...))
    ***************************************************************************/
   /*!
    * \fn     spi_tclAAPDataService(const trDataServiceCb& rfcorDataServiceCb)
    * \brief  Parameterised Constructor
    * \param  rfcorDataServiceCb: [IN] Structure containing callbacks to
    *            DataService Manager.
    * \sa     ~spi_tclAAPDataService()
    **************************************************************************/
   spi_tclAAPDataService(const trDataServiceCb& rfcorDataServiceCb);

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPDataService::~spi_tclAAPDataService
    ***************************************************************************/
   /*!
    * \fn     ~spi_tclAAPDataService()
    * \brief  Destructor
    * \sa     spi_tclAAPDataService()
    **************************************************************************/
   virtual ~spi_tclAAPDataService();


   /***** Start of Methods overridden from spi_tclDataServiceDevBase *********/

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPDataService::bInitialise();
   ***************************************************************************/
   /*!
   * \fn      bInitialise()
   * \brief   Method to initialise the service handler. (Performs initialisations which
   *          are not device specific.)
   *          Mandatory interface to be implemented.
   * \retval  t_Bool: TRUE - If ServiceHandler is initialised successfully, else FALSE.
   * \sa      bUninitialise()
   ***************************************************************************/
   virtual t_Bool bInitialise();

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPDataService::bUninitialise();
   ***************************************************************************/
   /*!
   * \fn      bUninitialise()
   * \brief   Method to uninitialise the service handler.
   *          Mandatory interface to be implemented.
   * \retval  t_Bool: TRUE - If ServiceHandler is uninitialised successfully, else FALSE.
   * \sa      bInitialise()
   ***************************************************************************/
   virtual t_Bool bUninitialise();

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPDataService::vSelectDevice(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vSelectDevice
    * \brief   Called when a device is selection request is received
    * \param   [IN] cou32DevId: Unique handle of selected device
    * \param   [IN] enConnReq: Indicated the category of the device
    * \retval  None
    **************************************************************************/
   virtual t_Void vSelectDevice(const t_U32 cou32DevId,
         tenDeviceConnectionReq enConnReq);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPDataService::vOnSelectDeviceResult(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vOnSelectDeviceResult(t_U32 u32DeviceHandle)
    * \brief   Called when a device is selected.
    *          Mandatory interface to be implemented.
    * \param   [IN] u32DeviceHandle: Unique handle of selected device
    * \retval  None
    * \sa      vOnDeselectDevice()
    **************************************************************************/
   virtual t_Void vOnSelectDeviceResult(t_U32 u32DeviceHandle);

   /***************************************************************************
    ** FUNCTION:  t_Void spi_tclAAPDataService::vOnDeselectDeviceResult(t_U32...)
    ***************************************************************************/
   /*!
    * \fn      vOnDeselectDeviceResult(t_U32 u32DeviceHandle)
    * \brief   Called when currently selected device is de-selected.
    *          Mandatory interface to be implemented.
    * \param   [IN] u32DeviceHandle: Unique handle of selected device
    * \retval  None
    * \sa      vOnSelectDevice()
    **************************************************************************/
   virtual t_Void vOnDeselectDeviceResult(t_U32 u32DeviceHandle);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vOnData(const trGPSData& rfcorGpsData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trGPSData& rfcorGpsData)
   * \brief   Method to receive GPS data.
   *          Optional interface to be implemented.
   * \param   rGpsData: [IN] GPS data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trGPSData& rfcorGpsData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vOnData(const trVehicleData& rfrcVehicleData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trVehicleData rfrcVehicleData)
   * \brief   Method to receive Vehicle (gear) data.
   *          Optional interface to be implemented.
   * \param   rfrcVehicleData: [IN] Vehicle data
   * \param   bSolicited: [IN] True if the data update is for changed vehicle data, else False
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trVehicleData& rfrcVehicleData, t_Bool bSolicited);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vOnData(const trSensorData& rfcorSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnData(const trSensorData& rfcorSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnData(const trSensorData& rfcorSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vOnAccSensorData
   ** (const std::vector<trAccSensorData>& rfcoSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnAccSensorData(const std::vector<trAccSensorData>& rfcoSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnAccSensorData(const std::vector<trAccSensorData>& rfcoSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vOnGyroSensorData
   ** (const std::vector<trGyroSensorData>& rfcoSensorData)
   ***************************************************************************/
   /*!
   * \fn      vOnGyroSensorData(const std::vector<trGyroSensorData>& rfcoSensorData)
   * \brief   Method to receive Sensor data.
   *          Optional interface to be implemented.
   * \param   rfcorSensorData: [IN] Sensor data
   * \retval  None
   ***************************************************************************/
   virtual t_Void vOnGyroSensorData(const std::vector<trGyroSensorData>& rfcoSensorData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vSetLocDataAvailablility(t_Bool...)
   ***************************************************************************/
   /*!
   * \fn      vSetLocDataAvailablility(t_Bool bLocDataAvailable)
   * \brief   Interface to set the availability of LocationData
   * \param   rfrDataServiceConfigInfo: the structure consists of values TRUE for location data, dead reckoning data
   *          environment data, gear status, accelerometer data and gyroscope data if available FALSE if not.
   * \retval  None
   **************************************************************************/
   virtual t_Void vSetSensorDataAvailablility(const trDataServiceConfigData& rfrDataServiceConfigInfo);

  /***************************************************************************
   ** FUNCTION:  spi_tclAAPDataService::vReportEnvironmentData()
   ***************************************************************************/
  /*!
   * \fn    t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
   *                                   t_Bool bValidPressureUpdate, t_Double dPressure)
   * \brief Use this call to report Environment data to Phone
   * \param bValidTempUpdate : [IN] Temp update is valid
   * \param dTemp : [IN] Temp in Celsius
   * \param bValidPressureUpdate: [IN] Pressure update is valid
   * \param dPressure : [IN] Pressure in KPA
   * \retval t_Void
   **************************************************************************/
  t_Void vReportEnvironmentData(t_Bool bValidTempUpdate,t_Double dTemp,
     t_Bool bValidPressureUpdate, t_Double dPressure);

   /******* End of Methods overridden from spi_tclDataServiceDevBase *********/

   /***************************************************************************
    ** FUNCTION:  t_Void vSetFeatureRestrictions()
    ***************************************************************************/
   /*!
    * \fn      t_Void vSetFeatureRestrictions(tenDeviceCategory enDevCategory,
    *          const t_U8 cou8ParkModeRestrictionInfo,
    *          const t_U8 cou8DriveModeRestrictionInfo)
    * \brief   Method to set Vehicle Park/Drive Mode Restriction
    * \param   cou8ParkModeRestrictionInfo : Park mode restriction
    *          cou8DriveModeRestrictionInfo : Drive mode restriction
    * \retval  t_Void
    **************************************************************************/

   t_Void vSetFeatureRestrictions(const t_U8 cou8ParkModeRestrictionInfo,
         const t_U8 cou8DriveModeRestrictionInfo);

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
    ** FUNCTION:t_Void spi_tclAAPDataService::vOnSetDayNightMode(...)
    ***************************************************************************/
   /*!
    * \fn      vOnSetDayNightMode(tenVehicleConfiguration enVehicleConfig)
    * \brief   To day/night data
    * \param   [IN] enVehicleConfig: contains day/night mode information
    * \retval  None
    **************************************************************************/
   t_Void vOnSetDayNightMode(tenVehicleConfiguration enVehicleConfig);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPDataService::vSetVehicleData()
   ***************************************************************************/
   /*!
   * \fn     t_Void vSetVehicleData()
   * \brief  Interface to set the Vehicle data
   * \param  [IN] bParkBrakeActive : Identifies Park brake status
   * \param  [IN] enVehMovState    : Identifies Vehicle gear status
   **************************************************************************/
   t_Void vSetVehicleData(t_Bool bParkBrakeActive, tenVehicleMovementState enVehMovState);

   /***************************************************************************
   ** FUNCTION: t_Void spi_tclAAPDataService::vRegisterCallbacks()
   ***************************************************************************/
   /*!
   * \fn      vRegisterCallbacks()
   * \brief   Method to Register callback
   * \param   None
   * \retval  None
   ***************************************************************************/
   t_Void vRegisterCallbacks();

   /***************************************************************************
    ** FUNCTION:t_Void spi_tclAAPDataService::vPostSetLocDataSubscription(...)
    ***************************************************************************/
   /*!
    * \fn      vPostSetLocDataSubscription(t_Bool bSubscriptionOn)
    * \brief   Used to toggle subscription of location data
    * \param   [IN] bSubscriptionOn: toggles subscription
    * \retval  None
    **************************************************************************/
   t_Void vPostSetLocDataSubscription(t_Bool bSubscriptionOn);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vReportParkingBrake()
   ***************************************************************************/
   /*!
   * \fn      vReportParkingBrake()
   * \brief   Method to receive Vehicle parking brake data.
   * \param   None
   * \retval  None
   ***************************************************************************/
   t_Void vReportParkingBrake();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vReportGear()
   ***************************************************************************/
   /*!
   * \fn      vReportGear()
   * \brief   Method to receive Vehicle (gear) data.
   * \param   None
   * \retval  None
   ***************************************************************************/
   t_Void vReportGear();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vReportDrivingStatus()
   ***************************************************************************/
   /*!
   * \fn      vReportDrivingStatus()
   * \brief   Method to receive driving status data.
   * \retval  None
   ***************************************************************************/
   t_Void vReportDrivingStatus();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vParkRestrictionInfo()
   ***************************************************************************/
   /*!
   * \fn      vParkRestrictionInfo()
   * \brief   Method to receive ParkRestriction Info.
   * \retval  t_Void
   ***************************************************************************/
   t_Void vParkRestrictionInfo();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPDataService::vReportVehicleSpeedInfo
    ***************************************************************************/
   /*!
   * \fn      vReportVehicleSpeedInfo()
   * \brief   Method to send Vehicle Speed Info.
   * \retval  t_Void
   ***************************************************************************/
   t_Void vReportVehicleSpeedInfo();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPDataService::vStartVehicleSpeedUpdate
    ***************************************************************************/
   /*!
   * \fn      vStartVehicleSpeedUpdate()
   * \brief   Method to start timer to send Vehicle Speed info
   * \retval  t_Void
   ***************************************************************************/
   t_Void vStartVehicleSpeedUpdate();

   /***************************************************************************
    ** FUNCTION:  spi_tclAAPDataService::vStopVehicleSpeedUpdate
    ***************************************************************************/
   /*!
   * \fn      vStopVehicleSpeedUpdate()
   * \brief   Method to stop timer to send Vehicle Speed info
   * \retval  t_Void
   ***************************************************************************/
   t_Void vStopVehicleSpeedUpdate();

   /***************************************************************************
   ** FUNCTION:  spi_tclAAPDataService::bVehicleSpeedTimerCb
   ***************************************************************************/
   /*!
   * \fn     bVehicleSpeedTimerCb
   * \brief  called on expiry of VehicleSpeed timer
   * \param  rTimerID: ID of the timer which has expired
   * \param  pvObject: pointer to object passed while starting the timer
   * \param  pvUserData: data passed during start of the timer
   **************************************************************************/
   static t_Bool bVehicleSpeedTimerCb(timer_t rTimerID, t_Void *pvObject,
           const t_Void *pvUserData);

   /***************************************************************************
   ** FUNCTION:  t_Bool spi_tclAAPDataService::bTriggerOutsideTempUpdateCb
   ***************************************************************************/
   /*!
   * \fn     t_Bool bTriggerOutsideTempUpdateCb(timer_t rTimerID, t_Void *pvObject...)
   * \brief  called on expiry of Outside temperature update to Phone
   * \param  rTimerID: ID of the timer which has expired
   * \param  pvObject: pointer to object passed while starting the timer
   * \param  pvUserData: data passed during start of the timer
   **************************************************************************/
   static t_Bool bTriggerOutsideTempUpdateCb(timer_t rTimerID, t_Void *pvObject,
      const t_Void *pvUserData);

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vStartOutsideTempUpdateTimer()
   ***************************************************************************/
   /*!
   * \fn     t_Void vStartOutsideTempUpdateTimer();
   * \brief  Method to start the timer, which sends Outside temperature updates
   *         once in every 10sec.
   * \param  None
   * \retval  t_Void
   **************************************************************************/
   t_Void vStartOutsideTempUpdateTimer();

   /***************************************************************************
   ** FUNCTION:  t_Void spi_tclAAPDataService::vStopOutsideTempUpdateTimer()
   ***************************************************************************/
   /*!
   * \fn     t_Void vStopOutsideTempUpdateTimer();
   * \brief  Method to stop the timer, which sends Outside temperature updates
   *         once in every 10sec.
   * \param  None
   * \retval  t_Void
   **************************************************************************/
   t_Void vStopOutsideTempUpdateTimer();

   /***************************************************************************
   ** Data Members
   ***************************************************************************/

   /***************************************************************************
   ** Pointer to AAP manager
   ***************************************************************************/
   spi_tclAAPManager*         m_poAAPManager;

   /***************************************************************************
    ** Selected device's information
    ***************************************************************************/
   trAAPSelectedDeviceInfo     m_rSelDevInfo;

   /***************************************************************************
    ** DataService callbacks structure
    ***************************************************************************/
   trDataServiceCb            m_rDataServiceCb;

   /***************************************************************************
    ** Day Night mode info
    ***************************************************************************/
   tenDayNightMode            m_enDayNightMode;

   /***************************************************************************
    ** Vehicle park brake status
    ***************************************************************************/
   t_Bool                     m_bParkBrakeActive;

   /***************************************************************************
    ** Vehicle gear status
    ***************************************************************************/
   tenVehicleMovementState    m_enVehMovState;

   /***************************************************************************
    ** Vehicle Variant status
    ***************************************************************************/
   t_Bool    m_bLocDataAvailable;

   /***************************************************************************
    ** Location data type
    ***************************************************************************/
   t_Bool    m_bDeadReckonedLocData;

   /***************************************************************************
   ** Park Mode Info
   ***************************************************************************/
   t_S32 m_s32AAPParkModeRestrictionInfo;

   /***************************************************************************
   ** Vehicle Speed info
   ***************************************************************************/
   t_S16 m_s16VehicleSpeed;

/****************************************************************************
** Environment Info
*****************************************************************************/
   t_Double m_dOutsideTemp;
   t_Bool m_bOutsideTempValidityFlag;

   t_Double m_dBarometricPressure;
   t_Bool m_bBaroPressureValidityFlag;
   trDataServiceConfigData m_rAAPDataServiceConfigData;

};
#endif // SPI_TCLAAPDATASERVICE_H_
