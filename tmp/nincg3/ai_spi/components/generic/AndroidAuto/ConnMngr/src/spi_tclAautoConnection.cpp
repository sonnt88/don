/*!
 *******************************************************************************
 * \file             spi_tclAautoConnection.h
 * \brief            Android auto Connection class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Android auto Connection class to handle android devices capable of android auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 18.12.2014 |  Pruthvi Thej Nagaraju       | Initial Version
 25.5.2015  |  Vinoop U      			   | Extended for Media metadata handling
 25.01.2016 |  Rachana L Achar             | Logiscope improvements
 \endverbatim
 ******************************************************************************/

#include "AoapTransport.h"
#include "spi_tclAautoConnection.h"
#include "spi_tclAAPCmdDiscoverer.h"
#include "spi_tclAAPManager.h"
#include "spi_tclAAPCmdSession.h"
#include "spi_tclMediator.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/spi_tclAautoConnection.cpp.trc.h"
#endif
#endif

using namespace adit::aauto;

static const t_Char* sczHeadUnitDescription = "Android Auto";
static const t_Char* sczMobileManufacturer= "Android";
static const t_Char* sczMobileModelName = "Android Auto";
static const t_Char* sczApplicationURI = "https://www.android.com/auto/";
static const t_Char* sczApplicationVersion = "1.0";
static const t_Char* sczCertificatePath = "/var/opt/bosch/dynamic/spi/tbd";
static const t_U8    scu8MaxSessionErrCnt = 4;

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1401 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e601 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e19 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e10 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e55 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e58 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e48 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e808 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e64 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::spi_tclAautoConnection
 ***************************************************************************/
spi_tclAautoConnection::spi_tclAautoConnection() :
   m_enDeviceSelReq(e8DEVCONNREQ_SELECT), m_u32CurrSelectedDevice(0), m_bSwitchInProgress(false),
   m_enCertificateType(e8_CERTIFICATETYPE_DEVELOPER)
{
   ETG_TRACE_USR1(("spi_tclAautoConnection:: spi_tclAautoConnection0x%X | %d\n", GOOGLE_PROTOBUF_VERSION, GOOGLE_PROTOBUF_VERSION));
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      //! Register with AAP manager.
      poAAPManager->bRegisterObject((spi_tclAAPRespDiscoverer*)this);
      poAAPManager->bRegisterObject((spi_tclAAPRespSession*)this);
      poAAPManager->bRegisterObject((spi_tclAAPRespMediaPlayback*)this);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::~spi_tclAautoConnection
 ***************************************************************************/
spi_tclAautoConnection::~spi_tclAautoConnection()
{
   ETG_TRACE_USR1((" spi_tclAautoConnection::~spi_tclAautoConnection() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  spi_tclConnection::bInitializeConnection
 ***************************************************************************/
t_Bool spi_tclAautoConnection::bInitializeConnection()
{
   t_Bool bRetVal = false;

   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      spi_tclAAPCmdDiscoverer *poAAPDiscoverer = poAAPManager->poGetDiscovererInstance();
      if (NULL != poAAPDiscoverer)
      {
         //! Create discoverer and register callbacks
         bRetVal = poAAPDiscoverer->bInitializeDiscoverer();
      }
   }

   ETG_TRACE_USR1((" spi_tclAautoConnection::bInitializeConnection bRetVal = %d \n",
            ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::bStartDeviceDetection
 ***************************************************************************/
t_Bool spi_tclAautoConnection::bStartDeviceDetection()
{
   t_Bool bRetVal = false;

   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      spi_tclAAPCmdDiscoverer *poAAPDiscoverer = poAAPManager->poGetDiscovererInstance();
      //! Start Android Auto device discovery
      if (NULL != poAAPDiscoverer)
      {
         bRetVal = poAAPDiscoverer->bStartDeviceDiscovery();
      }
   }
   ETG_TRACE_USR1(("spi_tclAautoConnection::bStartDeviceDetection: Start USB device detection bRetVal = %d \n",
            ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vUnInitializeConnection
 ***************************************************************************/
t_Void spi_tclAautoConnection::vUnInitializeConnection()
{
   ETG_TRACE_USR1((" spi_tclAautoConnection::vUnInitializeConnection entered \n"));

   //! Destroy  Android Auto discoverer
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      spi_tclAAPCmdDiscoverer *poAAPDiscoverer = poAAPManager->poGetDiscovererInstance();
      if (NULL != poAAPDiscoverer)
      {
         poAAPDiscoverer->vUnInitializeDiscoverer();
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vOnLoadSettings
 ***************************************************************************/
t_Void spi_tclAautoConnection::vOnLoadSettings(trHeadUnitInfo &rfrheadUnitInfo, tenCertificateType enCertificateType)
{
   ETG_TRACE_USR1((" spi_tclAautoConnection::vOnLoadSettings entered \n"));

   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      spi_tclAAPCmdDiscoverer *poAAPDiscoverer = poAAPManager->poGetDiscovererInstance();
      if (NULL != poAAPDiscoverer)
      {
         trAAPHeadUnitInfo rHeadUnitInfo;
         rHeadUnitInfo.szDescription = sczHeadUnitDescription;
         rHeadUnitInfo.szManufacturer = sczMobileManufacturer;
         rHeadUnitInfo.szModelName = sczMobileModelName;
         rHeadUnitInfo.szYear = rfrheadUnitInfo.szModelYear;
         rHeadUnitInfo.szSerial = rfrheadUnitInfo.szVehicleID;
         rHeadUnitInfo.szURI = sczApplicationURI;
         rHeadUnitInfo.szVersion = sczApplicationVersion;
         rHeadUnitInfo.szSoftwareVersion = m_rHeadUnitInfo.szSoftwareVersion;
         rHeadUnitInfo.enDriverPos = (e8RIGHT_HAND_DRIVE == rfrheadUnitInfo.enDrivePosition) ?
                           e8DRIVERPOSITION_RIGHT : e8DRIVERPOSITION_LEFT;
         //! Set the headunit information
         poAAPDiscoverer->vSetHeadUnitInfo(rHeadUnitInfo);
         m_rHeadUnitInfo = rfrheadUnitInfo;
      }
   }
   m_enCertificateType = enCertificateType;
   m_rSessionHistory.u32DeviceID = 0;
   m_rSessionHistory.u8ErrorCnt = 0;
   m_rSessionHistory.enSessionStatus = e8_SESSION_UNKNOWN;

}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::bPrepareSession
 ***************************************************************************/
t_Bool spi_tclAautoConnection::bPrepareSession()
{
   t_Bool bRetVal = false;
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      spi_tclAAPCmdSession *poCmdSession = poAAPManager->poGetSessionInstance();
      if (NULL != poCmdSession)
      {
         bRetVal = poCmdSession->bInitializeSession();
         //! create and initialize gal receiver session
         if (true == bRetVal)
         {
            //! TODO Add certificate path in connection settings
            bRetVal = poCmdSession->bSetCertificates(sczCertificatePath, m_enCertificateType);
            trAAPHeadUnitInfo rHeadUnitInfo;
            rHeadUnitInfo.szDescription = sczHeadUnitDescription;
            rHeadUnitInfo.szManufacturer = m_rHeadUnitInfo.szHUManufacturer;
            rHeadUnitInfo.szModelName = m_rHeadUnitInfo.szHUModelName;
            rHeadUnitInfo.szYear = m_rHeadUnitInfo.szModelYear;
            rHeadUnitInfo.szSerial = m_rHeadUnitInfo.szVehicleID;
            rHeadUnitInfo.szURI = sczApplicationURI;
            rHeadUnitInfo.szVersion = sczApplicationVersion;
            rHeadUnitInfo.szSoftwareVersion = m_rHeadUnitInfo.szSoftwareVersion;
            rHeadUnitInfo.szVehicleManufacturer = m_rHeadUnitInfo.szVehicleManufacturer;
            rHeadUnitInfo.szVehicleModel = m_rHeadUnitInfo.szVehicleModelName;
            rHeadUnitInfo.enDriverPos = (e8RIGHT_HAND_DRIVE == m_rHeadUnitInfo.enDrivePosition) ?
                              e8DRIVERPOSITION_RIGHT : e8DRIVERPOSITION_LEFT;
            poCmdSession->vSetHeadUnitInfo(rHeadUnitInfo);
         }
      }
   }
   ETG_TRACE_USR1((" spi_tclAautoConnection::bPrepareSession: Prepare information needed for AOAP Session bRetVal = %d \n",
         ETG_ENUM(BOOL, bRetVal)));
   return bRetVal;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vOnAddDevicetoList
 ***************************************************************************/
t_Void spi_tclAautoConnection::vOnAddDevicetoList(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclAautoConnection::vOnAddDevicetoList not used \n"));
   SPI_INTENTIONALLY_UNUSED(cou32DeviceHandle);
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::bSetDevProjUsage
 ***************************************************************************/
t_Bool spi_tclAautoConnection::bSetDevProjUsage(tenEnabledInfo enServiceStatus)
{
   ETG_TRACE_USR1((" spi_tclAautoConnection::bSetDevProjUsage : Set Android Auto setting to Android Auto setting = %d \n",
            ETG_ENUM(ENABLED_INFO, enServiceStatus)));
   //! Handle Android Auto ON/OFF
   t_Bool bRetval = true;
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if ((NULL != poAAPManager))
   {
      spi_tclAAPCmdDiscoverer *poAAPDiscoverer = poAAPManager->poGetDiscovererInstance();
      if (NULL != poAAPDiscoverer)
      {
         if (e8USAGE_ENABLED == enServiceStatus)
         {
           //! TODO uncomment below lines after ADIT solves the reset issue on starting and stopping discovery
           // bRetval = poAAPDiscoverer->bStartDeviceDiscovery();
         }
         else
         {
           // poAAPDiscoverer->vStopDeviceDiscovery();
         }
      }
   }
   return bRetval;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vRegisterCallbacks
 ***************************************************************************/
t_Void spi_tclAautoConnection::vRegisterCallbacks(trConnCallbacks &rfrConnCallbacks)
{
   ETG_TRACE_USR1((" spi_tclAautoConnection::vRegisterCallbacks entered \n"));
   m_rAautoConnCallbacks = rfrConnCallbacks;
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::bRequestDeviceSwitch
 ***************************************************************************/
t_Bool spi_tclAautoConnection::bRequestDeviceSwitch(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat)
{
   t_Bool bResult = false;
   //! Request device switch to AOAP mode

   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      spi_tclAAPCmdDiscoverer *poAAPDiscoverer = poAAPManager->poGetDiscovererInstance();
      if (NULL != poAAPDiscoverer)
      {
         if(e8DEV_TYPE_ANDROIDAUTO == enDevCat)
         {
            bResult = poAAPDiscoverer->bSwitchToAOAPMode(cou32DeviceHandle);
         }
         else if (e8DEV_TYPE_UNKNOWN == enDevCat)
         {
            bResult =  poAAPDiscoverer->bSwitchToUSBMode(cou32DeviceHandle);
            poAAPDiscoverer->vRetryUSBReset(cou32DeviceHandle);
         }
      }
   }
   ETG_TRACE_USR1((" spi_tclAautoConnection::bRequestDeviceSwitch: Device USB profile switch request Result = %d \n", ETG_ENUM(BOOL, bResult)));
   return bResult;
}


/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vOnSelectDevice()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vOnSelectDevice(const t_U32 cou32DeviceHandle, tenDeviceConnectionType enDevConnType,
         tenDeviceConnectionReq enDevConnReq, tenEnabledInfo enDAPUsage, tenEnabledInfo enCDBUsage,
         t_Bool bIsHMITrigger, const trUserContext corUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(enDAPUsage);
   SPI_INTENTIONALLY_UNUSED(enCDBUsage);
   SPI_INTENTIONALLY_UNUSED(corUsrCntxt);
   SPI_INTENTIONALLY_UNUSED(enDevConnType); //For fixing lint warnings
   
   ETG_TRACE_USR1((" spi_tclAautoConnection::vOnSelectDevice entered"));
 
   switch(enDevConnReq)
   {
      case e8DEVCONNREQ_SELECT:
      {
         // Trigger AAP switch and post SelectDevice result
         vProcessDeviceSelection(cou32DeviceHandle);
         break;
      }
      case e8DEVCONNREQ_DESELECT:
      {
         //! Wait for bye bye response
         //! Send bye bye message only if the device is deselected from HMI
         //!  wait till ByeByeResponse from phone before stopping transport
         vProcessDeviceDeselection(cou32DeviceHandle, bIsHMITrigger);
         break;
      }
      default: break;
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vProcessDeviceSelection()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vProcessDeviceSelection(const t_U32& corfu32DeviceHandle)
{
    ETG_TRACE_USR1(("spi_tclAautoConnection::vProcessDeviceSelection entered"));
    spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   
    if (NULL != poAAPManager)
    {
       spi_tclAAPCmdDiscoverer *poAAPDiscoverer = poAAPManager->poGetDiscovererInstance();
       if (NULL != poAAPDiscoverer)
       {
          trAAPSessionInfo rSessionInfo;
          poAAPDiscoverer->vGetSessionInfo(corfu32DeviceHandle, rSessionInfo);
          if ((scou32InvalidID == rSessionInfo.u32DeviceID) && (scou32InvalidID == rSessionInfo.u32HeadUnitID))
          {
             m_bSwitchInProgress = bRequestDeviceSwitch(corfu32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO);
             //! if switch is in progress wait till entity is reported else report failure
             if(false == m_bSwitchInProgress)
             {
                vPostSelectDeviceResult(e8DEVCONNREQ_SELECT, e8FAILURE, e8SELECTION_FAILED);
             } //if(false == m_bSwitchInProgress)
          }//if ((scou32InvalidID == rSessionInfo.u32DeviceID) && (scou32InvalidID == rSessionInfo.u32HeadUnitID))
          else
          {
             t_Bool bResult = false;
             bResult = bPrepareSession();
             //! Post select device result for device selection request
             tenErrorCode enErrorType = (true == bResult) ? e8NO_ERRORS : e8SELECTION_FAILED;
             tenResponseCode enRespCode = (true == bResult) ? e8SUCCESS : e8FAILURE;
             vPostSelectDeviceResult(e8DEVCONNREQ_SELECT, enRespCode, enErrorType);
          }
       } //if (NULL != poAAPDiscoverer)
    } //if (NULL != poAAPManager)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vProcessDeviceDeselection()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vProcessDeviceDeselection(const t_U32& corfu32DeviceHandle,
       t_Bool bIsHMITrigger)
{
    ETG_TRACE_USR1(("spi_tclAautoConnection::vProcessDeviceDeselection entered"));
    spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   
    if (NULL != poAAPManager)
    {
       spi_tclAAPCmdDiscoverer *poAAPDiscoverer = poAAPManager->poGetDiscovererInstance();
       spi_tclAAPCmdSession *poCmdSession = poAAPManager->poGetSessionInstance();
       if (NULL != poCmdSession)
       {
          // Check if device is deselected from HMI
          if (true == bIsHMITrigger)
          {
             poCmdSession->vSendByeByeMessage();
          }
          //Stop transport and post selectDevice result for device deselection request
          else
          {
             poCmdSession->vStopTransport();
             vPostSelectDeviceResult(e8DEVCONNREQ_DESELECT, e8SUCCESS, e8NO_ERRORS);
          }
       } //if (NULL != poCmdSession)

       if (NULL != poAAPDiscoverer)
       {
          poAAPDiscoverer->vClearSessionInfo(corfu32DeviceHandle);
       }
    } //if (NULL != poAAPManager)
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vPostSelctDeviceResult()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vPostSelectDeviceResult(tenDeviceConnectionReq coenConnReq,
         tenResponseCode enResponse, tenErrorCode enErrorType)
{
	/*lint -esym(40,fvSelectDeviceResult)fvSelectDeviceResult Undeclared identifier */
   SPI_INTENTIONALLY_UNUSED(enResponse);
   ETG_TRACE_USR1(("  spi_tclAautoConnection::vPostSelectDeviceResult: Sending device selection result \n"));
   if(NULL != m_rAautoConnCallbacks.fvSelectDeviceResult)
   {
      (m_rAautoConnCallbacks.fvSelectDeviceResult)(enErrorType);
   }

   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if((e8DEVCONNREQ_SELECT == coenConnReq) && (e8NO_ERRORS == enErrorType))
   {
      if (NULL != poAAPManager)
      {
         //! Initialise Mediametadata endpoint
         spi_tclAAPcmdMediaPlayback* poAAPCmdMediaPlayback = poAAPManager->poGetMediaPlaybackInstance();
         if (NULL != poAAPCmdMediaPlayback)
         {
            poAAPCmdMediaPlayback->bInitializeMediaPlayback();
         }
      }
   }
   else if(e8DEVCONNREQ_DESELECT == coenConnReq)
   {
      if (NULL != poAAPManager)
      {
         //! De-Initialise Mediametadata endpoint on disconnection
         spi_tclAAPcmdMediaPlayback* poAAPCmdMediaPlayback = poAAPManager->poGetMediaPlaybackInstance();
         if (NULL != poAAPCmdMediaPlayback)
         {
            poAAPCmdMediaPlayback->vUninitialiseMediaPlayback();
         }
      }
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vOnSelectDeviceResult()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vOnSelectDeviceResult(const t_U32 cou32DeviceHandle,
         const tenDeviceConnectionReq coenConnReq, const tenResponseCode coenRespCode,
         tenDeviceCategory enDevCat, t_Bool bIsHMITrigger)
{
   SPI_INTENTIONALLY_UNUSED(bIsHMITrigger);
   SPI_INTENTIONALLY_UNUSED(enDevCat);
   ETG_TRACE_USR1((" spi_tclAautoConnection::vOnSelectDeviceResult entered \n"));
   m_enDeviceSelReq = coenConnReq;

   //! Start transport to establish communication between AAP Device and HU
   //! once selection is complete
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if ((e8SUCCESS == coenRespCode) && (NULL != poAAPManager))
   {
      spi_tclAAPCmdSession *poCmdSession = poAAPManager->poGetSessionInstance();
      spi_tclAAPCmdDiscoverer *poCmdDisc = poAAPManager->poGetDiscovererInstance();
      if ((NULL != poCmdSession) && (NULL != poCmdDisc))
      {
         //! Get the session info stored during AOAP switch
         trAAPSessionInfo rSessionInfo;
         poCmdDisc->vGetSessionInfo(cou32DeviceHandle, rSessionInfo);
         if (e8DEVCONNREQ_SELECT == coenConnReq)
         {
            m_u32CurrSelectedDevice = cou32DeviceHandle;
            //! Start transport on successful selection
            poCmdSession->bStartTransport(rSessionInfo);
            //! Check if the device selected is same as that was selected in last selection cycle.
            //! In case the device is different, clear the last session history.
            if (cou32DeviceHandle != m_rSessionHistory.u32DeviceID)
            {
               ETG_TRACE_USR4((" Different device selected. Clearing Session History\n"));
               m_rSessionHistory.u32DeviceID = cou32DeviceHandle;
               m_rSessionHistory.u8ErrorCnt = 0;
               //! If Session has become active even before selection result is received, keep the status unchanged.
               m_rSessionHistory.enSessionStatus = (e8_SESSION_ACTIVE != m_rSessionHistory.enSessionStatus)
                     ? (e8_SESSION_INACTIVE) : (m_rSessionHistory.enSessionStatus);
            }// if( cou32DeviceHandle == m_rSessionHistory.u32DeviceID)
         }
         else if (e8DEVCONNREQ_DESELECT == coenConnReq)
         {
            m_u32CurrSelectedDevice = 0;
            poCmdSession->vUnInitializeSession();
         }
      }
   }

   // If the response is a failure, assume that any of the internal
   // component is not ready to accept the latest state chanege and handle it accordingly.
   //! TODO Reset USB Connection on failure???

}

//! Methods overwriteen from Andorid auto discoverer response class
/***************************************************************************
** FUNCTION:  t_Void spi_tclAautoConnection::vPostDeviceInfo
***************************************************************************/
t_Void spi_tclAautoConnection::vPostDeviceInfo(
    const t_U32 cou32DeviceHandle,
    const trDeviceInfo &corfrDeviceInfo)
{
   ETG_TRACE_USR1((" spi_tclAautoConnection::vPostDeviceInfo entered \n"));
   //! If the aoap switch was requested during selection, then send the 
   //! device selection result after aoap entity is reported and session is prepared

   if((true == m_bSwitchInProgress) && (e8DEV_TYPE_ANDROIDAUTO == corfrDeviceInfo.enDeviceCategory))
   {
      t_Bool bResult = bPrepareSession();
      //! Post select device result for device selection request
      tenErrorCode enErrorType = (true == bResult) ? e8NO_ERRORS : e8SELECTION_FAILED;
      tenResponseCode enRespCode = (true == bResult) ? e8SUCCESS : e8FAILURE;
      vPostSelectDeviceResult(e8DEVCONNREQ_SELECT, enRespCode, enErrorType);
      m_bSwitchInProgress = false;
   }
   vOnDeviceConnection(cou32DeviceHandle, corfrDeviceInfo);
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAAPRespDiscoverer::vPostDeviceDisconnected
***************************************************************************/
t_Void spi_tclAautoConnection::vPostDeviceDisconnected(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("  spi_tclAautoConnection::vPostDeviceDisconnected entered \n"));
   vOnDeviceDisconnection(cou32DeviceHandle);
}


//! Methods overwriteen from Andorid auto session response class
/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAautoConnection::vUnrecoverableErrorCallback()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vUnrecoverableErrorCallback()
{
   ETG_TRACE_USR1(("  spi_tclAautoConnection::vUnrecoverableErrorCallback entered \n"));
   //! Reset USB Connection on receiving this error from phone.
   //! This reset is not required as this is done during deselection
//   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
//   if (NULL != poAAPManager)
//   {
//      spi_tclAAPCmdDiscoverer *poCmdDisc = poAAPManager->poGetDiscovererInstance();
//      if(NULL!=poCmdDisc)
//      {
//         poCmdDisc->bSwitchToUSBMode(m_u32CurrSelectedDevice);
//      }
//   }
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAautoConnection::vByeByeRequestCb()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vByeByeRequestCb(tenAAPByeByeReason enReason)
{
   ETG_TRACE_USR1(("  spi_tclAautoConnection::vByeByeRequestCb enReason = %d \n", enReason));
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      spi_tclAAPCmdSession *poCmdSession = poAAPManager->poGetSessionInstance();
      if(NULL != poCmdSession)
      {
         //! Set the select error flag as device has sent a ByeBye message
         if (NULL != (m_rAautoConnCallbacks.fvDeviceConnection))
         {
            (m_rAautoConnCallbacks.fvSelectDeviceError)(m_u32CurrSelectedDevice, true);
         }
         //! TODO : do cleanup on byebye message check if deselection is sufficient or USB reset needed
         //! Ideally post internal deselection
         //! Send Device deselection to SPI components
         spi_tclMediator *poMediator = spi_tclMediator::getInstance();
         if (NULL != poMediator)
         {
            poMediator->vPostAutoDeviceSelection(m_u32CurrSelectedDevice, e8DEVCONNREQ_DESELECT);
         } // if (NULL != poMediator)
         poCmdSession->vSendByeByeResponse();
      }
   }

}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAautoConnection::vByeByeResponseCb()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vByeByeResponseCb()
{
   ETG_TRACE_USR1(("  spi_tclAautoConnection::vByeByeResponseCb: Stopping transport and deselecting the device \n"));
   spi_tclAAPManager *poAAPManager = spi_tclAAPManager::getInstance();
   if (NULL != poAAPManager)
   {
      spi_tclAAPCmdSession *poCmdSession = poAAPManager->poGetSessionInstance();
      if (NULL != poCmdSession)
      {
         poCmdSession->vStopTransport();
      }
   }
   //! Post select device result after receiving bye bye response from phone
   vPostSelectDeviceResult(e8DEVCONNREQ_DESELECT,e8SUCCESS, e8NO_ERRORS);
}



/***************************************************************************
 *********************************PRIVATE***********************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vOnDeviceConnection
 ***************************************************************************/
t_Void spi_tclAautoConnection::vOnDeviceConnection(const t_U32 cou32DeviceHandle, const trDeviceInfo &corfrDeviceInfo)
{
   ETG_TRACE_USR1((" spi_tclAautoConnection::vOnDeviceConnection entered \n"));
   if (NULL != (m_rAautoConnCallbacks.fvDeviceConnection))
   {
      (m_rAautoConnCallbacks.fvDeviceConnection)(cou32DeviceHandle, corfrDeviceInfo, e8DEV_TYPE_ANDROIDAUTO);
   }
}

/***************************************************************************
 ** FUNCTION:  spi_tclAautoConnection::vOnDeviceDisconnection
 ***************************************************************************/
t_Void spi_tclAautoConnection::vOnDeviceDisconnection(const t_U32 cou32DeviceHandle)
{
   ETG_TRACE_USR1(("spi_tclAautoConnection::vOnDeviceDisconnection entered \n"));

   if (NULL != (m_rAautoConnCallbacks.fvDeviceDisconnection))
   {
      (m_rAautoConnCallbacks.fvDeviceDisconnection)(cou32DeviceHandle, e8DEV_TYPE_ANDROIDAUTO);
   }//if(NULL != (m_rDiPoConnCallbacks.fvDeviceDisconnection))
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclAAPAppMngr::vMediaPlaybackStatusCallback()
***************************************************************************/
t_Void spi_tclAautoConnection::vMediaPlaybackStatusCallback(const trAAPMediaPlaybackStatus* prAAPMediaPlaybackStatus)
{
   ETG_TRACE_USR1(("spi_tclAautoConnection::vMediaPlaybackStatusCallback entered \n"));

   trUserContext rfcorUsrCntxt;
   t_Bool bSolicited = false;

   if (NULL != prAAPMediaPlaybackStatus)
   {
      ETG_TRACE_USR1(("vMediaPlaybackStatusCallback :: Data Received from AAP : PlayBackState = %d, Shuffle = %d, Repeat_ALL = %d, "
            "Repeat_ONE = %d ",prAAPMediaPlaybackStatus->u32State, ETG_ENUM(BOOL, prAAPMediaPlaybackStatus->bShuffle),
            ETG_ENUM(BOOL, prAAPMediaPlaybackStatus->bRepeat), ETG_ENUM(BOOL, prAAPMediaPlaybackStatus->bRepeatOne)));

      ETG_TRACE_USR1(("vMediaPlaybackStatusCallback :: Data Received from AAP : Media Source = %s ",
            prAAPMediaPlaybackStatus->cMediaSource));

      m_rAppMediaPlaytime.u32ElapsedPlayTime = prAAPMediaPlaybackStatus->u32PlaybackSeconds;
      ETG_TRACE_USR2(("vMediaPlaybackStatusCallback :: Total play time = %d",m_rAppMediaPlaytime.u32TotalPlayTime ));
      ETG_TRACE_USR2(("vMediaPlaybackStatusCallback :: Current play time = %d",m_rAppMediaPlaytime.u32ElapsedPlayTime ));

      // Get Info about Play back state (i.e. stopped, playing, paused)
      tenMediaPlayBackState enMediaPlayBackState = static_cast<tenMediaPlayBackState>((prAAPMediaPlaybackStatus->u32State) - 1);
      if (enMediaPlayBackState != m_rAppMediaMetaData.enMediaPlayBackState)
      {
         m_rAppMediaMetaData.enMediaPlayBackState = enMediaPlayBackState;
         bSolicited = true;
         ETG_TRACE_USR2(("vMediaPlaybackStatusCallback:: Play Back state = %d",
               ETG_ENUM(PLAYBACK_STATE,m_rAppMediaMetaData.enMediaPlayBackState)));
      }

      // Get Info about shuffle
      tenMediaPlayBackShuffleState enMediaPlayBackShuffleState = (prAAPMediaPlaybackStatus->bShuffle) ? (e8SHUFFLE_ON) : (e8SHUFFLE_OFF);
      if (enMediaPlayBackShuffleState != m_rAppMediaMetaData.enMediaPlayBackShuffleState)
      {
         m_rAppMediaMetaData.enMediaPlayBackShuffleState = enMediaPlayBackShuffleState;
         bSolicited = true;
         ETG_TRACE_USR2(("vMediaPlaybackStatusCallback:: Shuffle state = %d",
               ETG_ENUM(SHUFFLE_MODE, m_rAppMediaMetaData.enMediaPlayBackShuffleState)));
      }

      // Set the playback repeat all and repeat one states
      vSetPlaybackRepeatState(prAAPMediaPlaybackStatus, bSolicited);

      //Set the playback media source
      vSetPlaybackMediaSource(prAAPMediaPlaybackStatus, bSolicited);
   }
   //! Forward Play time data to Connection Manager
   if (NULL != (m_rAautoConnCallbacks.fvAppMediaPlaytime))
   {
      (m_rAautoConnCallbacks.fvAppMediaPlaytime)(m_rAppMediaPlaytime, rfcorUsrCntxt);
   }//if (NULL != (m_rDiPoConnCallbacks.fvAppMediaPlaytime))

   //! Forward meta data to Connection Manager
   if (NULL != (m_rAautoConnCallbacks.fvAppMediaMetaData) && (true == bSolicited))
   {
      (m_rAautoConnCallbacks.fvAppMediaMetaData)(m_rAppMediaMetaData, rfcorUsrCntxt);
   }//if (NULL != (m_rDiPoConnCallbacks.fvAppMediaMetaData))
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAautoConnection::vSetPlaybackRepeatState(const...
***************************************************************************/
t_Void spi_tclAautoConnection::vSetPlaybackRepeatState(
	   const trAAPMediaPlaybackStatus* coprAAPMediaPlaybackStatus,
	   t_Bool& bSolicited)
{
    // Get Info about repeat all and repeat one states
    tenMediaPlayBackRepeatState enMediaPlayBackRepeatAllState = (true == coprAAPMediaPlaybackStatus->bRepeat)
        ? (e8REPEAT_ALL) : (m_rAppMediaMetaData.enMediaPlayBackRepeatState);

    tenMediaPlayBackRepeatState enMediaPlayBackRepeatOneState = (true == coprAAPMediaPlaybackStatus->bRepeatOne)
        ? (e8REPEAT_ONE) : (m_rAppMediaMetaData.enMediaPlayBackRepeatState);

    // Set the repeat all state of AppMedia metadata
    if((enMediaPlayBackRepeatAllState != m_rAppMediaMetaData.enMediaPlayBackRepeatState))
    {
        m_rAppMediaMetaData.enMediaPlayBackRepeatState = enMediaPlayBackRepeatAllState;
        bSolicited = true;
        ETG_TRACE_USR2(("vMediaPlaybackStatusCallback:: Repeat All state = %d",m_rAppMediaMetaData.enMediaPlayBackRepeatState));
    }// if((enMediaPlayBackRepeatAllState != m_rAppMediaMetaData.enMediaPlayBackRepeatState))

    // Set the repeat one state of AppMedia metadata
    else if (enMediaPlayBackRepeatOneState != m_rAppMediaMetaData.enMediaPlayBackRepeatState)
    {
        m_rAppMediaMetaData.enMediaPlayBackRepeatState = enMediaPlayBackRepeatOneState;
        bSolicited = true;
        ETG_TRACE_USR2(("vMediaPlaybackStatusCallback:: Repeat One state = %d",m_rAppMediaMetaData.enMediaPlayBackRepeatState));
    }// else if (enMediaPlayBackRepeatOneState != m_rAppMediaMetaData.enMediaPlayBackRepeatState)
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclAautoConnection::vSetPlaybackMediaSource(const...
***************************************************************************/
t_Void spi_tclAautoConnection::vSetPlaybackMediaSource(
	   const trAAPMediaPlaybackStatus* coprAAPMediaPlaybackStatus, 
	   t_Bool& bSolicited)
{
    //Set the media source of the app media metadata
    if (0 != strcmp(m_rAppMediaMetaData.szAppName.c_str(),coprAAPMediaPlaybackStatus->cMediaSource))
    {
        m_rAppMediaMetaData.szAppName.assign(coprAAPMediaPlaybackStatus->cMediaSource, STR_METADATA_LENGTH);
        bSolicited = true;
        ETG_TRACE_USR2(("vSetPlaybackMediaSource:: Media Source  = %s",m_rAppMediaMetaData.szAppName.c_str()));
    }//if (0 != strcmp(m_rAppMediaMetaData.szAppName.c_str(),coprAAPMediaPlaybackStatus->cMediaSource))
}

/***************************************************************************
** FUNCTION:  t_Bool  spi_tclAAPAppMngr::vMediaPlaybackMetadataCallback()
***************************************************************************/
t_Void spi_tclAautoConnection::vMediaPlaybackMetadataCallback(trAAPMediaPlaybackMetadata *prAAPMediaPlaybackMetadata)
{
	ETG_TRACE_USR1(("spi_tclAautoConnection::vMediaPlaybackMetadataCallback entered \n"));

   trUserContext rfcorUsrCntxt;
   m_rAppMediaMetaData.bMediaMetadataValid = true;

   if(NULL !=prAAPMediaPlaybackMetadata)
   {
      m_rAppMediaMetaData.szArtist.assign(prAAPMediaPlaybackMetadata->cArtist, STR_METADATA_LENGTH);
      m_rAppMediaMetaData.szAlbum.assign(prAAPMediaPlaybackMetadata->cAlbum, STR_METADATA_LENGTH);
      m_rAppMediaMetaData.szTitle.assign(prAAPMediaPlaybackMetadata->cSong, STR_METADATA_LENGTH);
      m_rAppMediaMetaData.szAlbumArt.assign(prAAPMediaPlaybackMetadata->cAlbum_art, STR_METADATA_LENGTH);
      m_rAppMediaMetaData.u8MediaRating = prAAPMediaPlaybackMetadata->u32Rating;
      m_rAppMediaPlaytime.u32TotalPlayTime = prAAPMediaPlaybackMetadata->u32DurationSeconds;

      m_rAppMediaMetaData.szImageMIMEType = "";
      if((!(m_rAppMediaMetaData.szAlbumArt).empty()))
      {
         m_rAppMediaMetaData.szImageMIMEType = "PNG";
      }

	   //@ToDo should send TotalPlaytime metadata.

      ETG_TRACE_USR4(("vMediaPlaybackMetadataCallback :: MetadataValid  = %d, \n", ETG_ENUM(BOOL, m_rAppMediaMetaData.bMediaMetadataValid)));
      ETG_TRACE_USR1(("vMediaPlaybackMetadataCallback :: Song  = %s", m_rAppMediaMetaData.szTitle.c_str()));
      ETG_TRACE_USR1(("vMediaPlaybackMetadataCallback :: Album  = %s",m_rAppMediaMetaData.szAlbum.c_str()));
      ETG_TRACE_USR1(("vMediaPlaybackMetadataCallback :: Artist  = %s",m_rAppMediaMetaData.szArtist.c_str()));
      ETG_TRACE_USR4(("vMediaPlaybackMetadataCallback :: MediaRating  = %d ", m_rAppMediaMetaData.u8MediaRating));
      ETG_TRACE_USR4(("vMediaPlaybackMetadataCallback :: Image MIME Type  = %s ", m_rAppMediaMetaData.szImageMIMEType.c_str()));

   }
   //! Forward metadata to Connection Manager
   if (NULL != (m_rAautoConnCallbacks.fvAppMediaMetaData))
   {
      (m_rAautoConnCallbacks.fvAppMediaMetaData)(m_rAppMediaMetaData, rfcorUsrCntxt);
   }//if (NULL != (m_rDiPoConnCallbacks.fvAppMetadata))

}
/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclAautoConnection::vSessionStatusInfo()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vSessionStatusInfo(tenSessionStatus enSessionStatus, t_Bool bSessionTimedOut)
{
   /*lint -esym(40,fvUpdateSessionStatus)fvUpdateSessionStatus Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclAautoConnection::vSessionStatusInfo enSessionStatus = %d\n", ETG_ENUM(SESSION_STATUS,
         enSessionStatus)));

   if ((e8_SESSION_ACTIVE == enSessionStatus) || (e8_SESSION_INACTIVE == enSessionStatus))
   {
      //! Check if the session became INACTIVE after an Error Notification.
      if ((e8_SESSION_ERROR == m_rSessionHistory.enSessionStatus) && (e8_SESSION_INACTIVE == enSessionStatus))
      {
         ETG_TRACE_USR4(("Session Inactive is received after Session Error was notified. Session Error Count is kept unchanged"));
      }//if((e8_SESSION_ERROR == m_rSessionHistory.enSessionStatus) && (e8_SESSION_INACTIVE == enSessionStatus))
      else
      {
         ETG_TRACE_USR4(("Session is activated successfully or gracefully ended. Clearing Session Error Count"));
         //! No update required for device handle. If the device handle has changed, this will be updated in SelectDeviceResult.
         //! In case the device handle is same, it is just sufficient to reset the error count and update the session status.
         //! Update the session status independent of Device handle when Session Setup/end is successful/graceful.
         m_rSessionHistory.enSessionStatus = enSessionStatus;
         //! Clear the error count as the session could be activated/ended successfully now.
         m_rSessionHistory.u8ErrorCnt = 0;
      }
   }//if((e8_SESSION_ACTIVE == enSessionStatus) || (e8_SESSION_INACTIVE == enSessionStatus))

   if (e8_SESSION_ERROR == enSessionStatus)
   {
      ETG_TRACE_USR4(("Session Error Count is %d", m_rSessionHistory.u8ErrorCnt));
      //! Check if the last state of the session was error. If the session was previously ended gracefully or activated successfully,
      //! it is not required to check for maximum session failures.
      if (e8_SESSION_ERROR == m_rSessionHistory.enSessionStatus)
      {
         ETG_TRACE_USR4(
               ("Session error notified by MD. Previous session activation was failed due to error.Updating Session Error Count"));
         m_rSessionHistory.u8ErrorCnt++;
         //! Check if the maximum number of consecutive session errors has been reached or if the error is triggerred due to timeout
         if (((scu8MaxSessionErrCnt == m_rSessionHistory.u8ErrorCnt) || (true == bSessionTimedOut)) && (NULL
               != (m_rAautoConnCallbacks.fvDeviceConnection)))
         {
            ETG_TRACE_USR4(("Maximum number of attempts to activate session without error reached. Set Error flag for Device"));
            //! Set the Error Flag for the device to prevent subsequent automatic selection.
            (m_rAautoConnCallbacks.fvSelectDeviceError)(m_rSessionHistory.u32DeviceID, true);
            //! Clear the Session History as error has been handled for the device
            m_rSessionHistory.u8ErrorCnt = 0;
            m_rSessionHistory.enSessionStatus = e8_SESSION_UNKNOWN;
            m_rSessionHistory.u32DeviceID = 0;
         }//! if( == m_rSessionHistory.u8ErrorCnt)

      }//if(e8_SESSION_ERROR == m_rSessionHistory.enSessionStatus)
      else
      {
         //! Update the Session state to error. Start counting session errors.
         ETG_TRACE_USR4(("Session error notified by MD. No error from previous session.Updating Session State to Error and Initialize Count"));
         m_rSessionHistory.enSessionStatus = e8_SESSION_ERROR;
         m_rSessionHistory.u8ErrorCnt = 1;
      }// end of if-else; if(e8_SESSION_ERROR == m_rSessionHistory.enSessionStatus)

      //! Send Device deselection to SPI components
      spi_tclMediator *poMediator = spi_tclMediator::getInstance();
      //! TO BE CONSIDERED: Handling of Session error notified from even before Device selection process is completed from SPI.
      //! Currently, not handled as m_u32CurrSelectedDevice will not be updated and attempt to deselect will results in BUSY status.
      if (NULL != poMediator)
      {
         poMediator->vPostAutoDeviceSelection(m_u32CurrSelectedDevice, e8DEVCONNREQ_DESELECT);
      } // if (NULL != poMediator)
   }//! if(e8_SESSION_ERROR == m_rSessionHistory.enSessionStatus)

}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclAautoConnection::vServiceDiscoveryRequestCb()
 ***************************************************************************/
t_Void spi_tclAautoConnection::vServiceDiscoveryRequestCb(t_String szSmallIcon, t_String szMediumIcon, t_String szLargeICon , t_String szLabel , t_String szDeviceName)
{
	SPI_INTENTIONALLY_UNUSED(szSmallIcon);
	SPI_INTENTIONALLY_UNUSED(szMediumIcon);
	SPI_INTENTIONALLY_UNUSED(szLargeICon);
	SPI_INTENTIONALLY_UNUSED(szLabel);

	ETG_TRACE_USR1(("spi_tclAautoConnection::vServiceDiscoveryRequestCb Device Name = %s \n", szDeviceName.c_str()));

	if(NULL != (m_rAautoConnCallbacks.fvSetDeviceName))
	{
	   (m_rAautoConnCallbacks.fvSetDeviceName)(m_rSessionHistory.u32DeviceID,szDeviceName);
	}
}

//lint -restore
