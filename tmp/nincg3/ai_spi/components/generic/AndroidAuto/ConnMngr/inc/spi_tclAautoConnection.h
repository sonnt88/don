/*!
 *******************************************************************************
 * \file             spi_tclAautoConnection.h
 * \brief            Android auto Connection class
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Android auto Connection class to handle android devices capable of android auto
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 14.03.2015 |  Pruthvi Thej Nagaraju       | Initial Version
 27.05.2015 |  Vinoop U                    | Extend for handling Media metadata
 25.01.2016 |  Rachana L Achar             | Logiscope improvements

 \endverbatim
 ******************************************************************************/

#ifndef SPI_TCLAAUTOCONNECTION_H_
#define SPI_TCLAAUTOCONNECTION_H_

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "spi_tclConnection.h"
#include "spi_tclAAPCmdDiscoverer.h"
#include "spi_tclAAPCmdSession.h"
#include "spi_tclAAPRespDiscoverer.h"
#include "spi_tclAAPRespSession.h"
#include "spi_tclAAPRespMediaPlayback.h"
#include "spi_tclAAPcmdMediaPlayback.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
 //! Stores infomation about the last session with the AAP Device
struct trSessionHistory
{
   t_U32 u32DeviceID; 
   t_U8  u8ErrorCnt;
   tenSessionStatus enSessionStatus;
   trSessionHistory():u32DeviceID(0),u8ErrorCnt(0),enSessionStatus(e8_SESSION_UNKNOWN)
   {
   }
};
/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/


class spi_tclAautoConnection: public spi_tclConnection, //! Base Connection Class
   public spi_tclAAPRespDiscoverer, public spi_tclAAPRespSession,public spi_tclAAPRespMediaPlayback
{
   public:
      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::spi_tclAautoConnection
       ***************************************************************************/
      /*!
       * \fn     spi_tclAautoConnection()
       * \brief  Default Constructor
       * \sa      ~spi_tclAautoConnection()
       **************************************************************************/
      spi_tclAautoConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::~spi_tclAautoConnection
       ***************************************************************************/
      /*!
       * \fn     ~spi_tclAautoConnection()
       * \brief  Destructor
       * \sa     spi_tclAautoConnection()
       **************************************************************************/
      ~spi_tclAautoConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::bInitializeConnection
       ***************************************************************************/
      /*!
       * \fn     bInitializeConnection()
       * \brief  Initialization of device detection and
       *         any other required initializations
       * \retval returns true on successful initialization and false on failure
       **************************************************************************/
      t_Bool bInitializeConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::bStartDeviceDetection
       ***************************************************************************/
      /*!
       * \fn     bStartDeviceDetection()
       * \brief  Starts device detection
       * \retval returns true on successful start of device detection and false on failure
       **************************************************************************/
       t_Bool bStartDeviceDetection();

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vUnInitializeConnection
       ***************************************************************************/
      /*!
       * \fn     vUnInitializeConnection()
       * \brief  Uninitialization of sdk's etc
       **************************************************************************/
      t_Void vUnInitializeConnection();

      /***************************************************************************
       ** FUNCTION:  spi_tclConnection::vOnLoadSettings
       ***************************************************************************/
      /*!
       * \fn     vOnLoadSettings()
       * \brief  Called on loadsettings
       *         Optional interface
       * \param rfrheadUnitInfo : Head unit information
       * \param enCertificateType : CertificateType to be used for authentication
       **************************************************************************/
      t_Void vOnLoadSettings(trHeadUnitInfo &rfrheadUnitInfo, tenCertificateType enCertificateType);

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vOnAddDevicetoList
       ***************************************************************************/
      /*!
       * \fn     vOnAddDevicetoList()
       * \brief  To be called when a new device has to be added to device list
       * \param  cou32DeviceHandle: Device handle of the device to be added to
       *         the device list
       **************************************************************************/
      t_Void vOnAddDevicetoList(const t_U32 cou32DeviceHandle);

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::bSetDevProjUsage
       ***************************************************************************/
      /*!
       * \fn     bSetDevProjUsage()
       * \brief  Called when the SPI featured is turned ON or OFF by the user
       * \param  enServiceStatus: Indicates if the SPI feature is enabled or not
       **************************************************************************/
      t_Bool bSetDevProjUsage(tenEnabledInfo enServiceStatus);

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vRegisterCallbacks
       ***************************************************************************/
      /*!
       * \fn     vRegisterCallbacks()
       * \brief  interface for the creator class to register for the required
       *        callbacks.
       * \param rfrConnCallbacks : reference to the callback structure
       *        populated by the caller
       **************************************************************************/
      t_Void vRegisterCallbacks(trConnCallbacks &ConnCallbacks);

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::bRequestDeviceSwitch
       ***************************************************************************/
      /*!
       * \fn     bRequestDeviceSwitch()
       * \brief  Request device to switch to AOAP mode
       * \param  cou32DeviceHandle: Device handle of the device to be switched
       *\retval : Returns true on successful switch false otherwise
       **************************************************************************/
      t_Bool bRequestDeviceSwitch(const t_U32 cou32DeviceHandle, tenDeviceCategory enDevCat);


      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclAautoConnection::vOnSelectDevice()
      ***************************************************************************/
      /*!
      * \fn     t_Void vOnSelectDevice()
      * \brief  Function to get the deviec selection.
      * \param  cou32DeviceHandle : [IN] Device handle
      * \param  enDevConnReq : [IN] Selection status
      **************************************************************************/
      virtual t_Void vOnSelectDevice(const t_U32 cou32DeviceHandle,
               tenDeviceConnectionType enDevConnType,
               tenDeviceConnectionReq enDevSelReq, tenEnabledInfo enDAPUsage,
               tenEnabledInfo enCDBUsage, t_Bool bIsHMITrigger,
               const trUserContext corUsrCntxt);

   private:
      /***************************************************************************
       *********************************PRIVATE***********************************
       ***************************************************************************/
     /***************************************************************************
      ** FUNCTION:  t_Void spi_tclAautoConnection::vPostSelctDeviceResult()
      ***************************************************************************/
      /*!
      * \fn     t_Void vPostSelctDeviceResult()
      * \brief  Function to post the device selection statu.
      * \pram    enDevSelReq : [IN] Identifies the Connection Request.
      * \param  enResponse : [IN] Operation status.
      * \param  enErrorType : [IN] error value.
      **************************************************************************/
      virtual t_Void vPostSelectDeviceResult(tenDeviceConnectionReq coenConnReq,
               tenResponseCode enResponse,  tenErrorCode enErrorType);

     /***************************************************************************
      ** FUNCTION:  t_Void spi_tclAautoConnection::vOnSelectDeviceResult()
      ***************************************************************************/
      /*!
      * \fn      t_Void vOnSelectDeviceResult
      * \brief   To perform the actions that are required, after the select device is
      *           successful/failed
      * \pram    cou32DeviceHandle  : [IN] Uniquely identifies the target Device.
      * \pram    enDevSelReq : [IN] Identifies the Connection Request.
      * \pram    coenRespCode: [IN] Response code. Success/Failure
      * \pram    enDevCat    : [IN] Device Category. ML/DiPo
      * \retval  t_Void
      **************************************************************************/
      t_Void vOnSelectDeviceResult(const t_U32 cou32DeviceHandle,
         const tenDeviceConnectionReq coenConnReq,
         const tenResponseCode coenRespCode,
         tenDeviceCategory enDevCat, t_Bool bIsHMITrigger);

      //! Methods overwriteen from Andorid auto discoverer response class
      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclAautoConnection::vPostDeviceInfo
      ***************************************************************************/
      /*!
      * \fn      t_Void vPostDeviceInfo
      * \brief   To Post the device info to SPI, when a new device is detected
      * \param   cou32DeviceHandle     : [IN] Device Id
      * \param   corfrDeviceInfo : [IN] const reference to the DeviceInfo structure.
      * \retval  t_Void
      * \sa      vPostDeviceDisconncted(const t_U32 cou32DeviceHandle)
      ***************************************************************************/
      virtual t_Void vPostDeviceInfo(
          const t_U32 cou32DeviceHandle,
          const trDeviceInfo &corfrDeviceInfo);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclAAPRespDiscoverer::vPostDeviceDisconnected
      ***************************************************************************/
      /*!
      * \fn      virtual t_Void vPostDeviceDisconnected
      * \brief   To Post the Device Id to SPI, when a device is disconnected
      * \param   rUserContext : [IN] Context information passed from the caller
      * \param   cou32DeviceHandle    : [IN] Device Id
      * \retval  t_Void
      **************************************************************************/
      virtual t_Void vPostDeviceDisconnected(const t_U32 cou32DeviceHandle);

      //! Methods from android auto session response class
      /***************************************************************************
       ** FUNCTION:  t_Void spi_tclAAPSession::vUnrecoverableErrorCallback()
       ***************************************************************************/
      /*!
       * \fn      vUnrecoverableErrorCallback
       * \brief   called when phone detects a unrecoverable error:
       * advice is to reset USB on this call
       **************************************************************************/
      virtual t_Void vUnrecoverableErrorCallback();

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::vByeByeRequestCb()
       ***************************************************************************/
      /*!
       * \fn      vByeByeRequestCb
       * \brief   Called when ByeByeRequest is received from phone. After taking necessary steps,
       * car side should send ByeByeResponse.
       * \param   enReason  The reason for the disconnection request.
       **************************************************************************/
      virtual t_Void vByeByeRequestCb(tenAAPByeByeReason enReason);

      /***************************************************************************
       ** FUNCTION:  t_Bool spi_tclAAPSession::vByeByeResponseCb()
       ***************************************************************************/
      /*!
       * \fn      vByeByeResponseCb
       * \brief  Called when ByeByeResponse is received from phone. Normally this is a reply for
       * ByeByeRequest message sent from car.
       **************************************************************************/
      virtual t_Void vByeByeResponseCb();

      /***************************************************************************
      ** FUNCTION:  t_Void  spi_tclAAPSession::vMediaPlaybackStatusCallback()
      ***************************************************************************/
      /*!
      * \fn      t_Void vMediaPlaybackStatusCallback()
	  * \brief  Called when Playbackstatus callback is recieved from phone 
   \ * \param   rAAPMediaPlaybackStatus  for mediplaybackstatus update
      **************************************************************************/
      t_Void vMediaPlaybackStatusCallback(const trAAPMediaPlaybackStatus* prAAPMediaPlaybackStatus);

      /***************************************************************************
      ** FUNCTION:  t_Void  spi_tclAAPSession::vMediaPlaybackMetadataCallback()
      ***************************************************************************/
      /*!
      * \fn      t_Void vMediaPlaybackMetadataCallback()
	  * \brief  Called when metadata callback is recieved from phone 
   \  *	\param   rAAPMediaPlaybackMetadata  for MediaPlaybackMetadata update
      **************************************************************************/
     t_Void vMediaPlaybackMetadataCallback(trAAPMediaPlaybackMetadata *prAAPMediaPlaybackMetadata);

     /***************************************************************************
      ** FUNCTION:  t_Bool spi_tclAAPResourceMngr::vSessionStatusInfo()
      ***************************************************************************/
     /*!
      * \fn      vSessionStatusInfo
      * \brief  informs the current session status of android auto session. function
      *         overridden from spi_tclAAPRespSession.h
      * \param  enSessionStatus : indicates current status of android auto session
      **************************************************************************/
     virtual t_Void vSessionStatusInfo(tenSessionStatus enSessionStatus, t_Bool bSessionTimedOut);

      //! Callbacks for ConnMngr to register. These callbacks will be used to
      //! inform device detection and device disconnection to connection manager
      trConnCallbacks m_rAautoConnCallbacks;

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vOnDeviceConnection
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceConnection()
       * \brief  to be called on Android auto device connection
       **************************************************************************/
      t_Void vOnDeviceConnection(const t_U32 cou32DeviceHandle,
               const trDeviceInfo &corfrDeviceInfo);

      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vOnDeviceDisconnection
       ***************************************************************************/
      /*!
       * \fn     vOnDeviceDisconnection()
       * \brief to be called on android auto device disconnection
       **************************************************************************/
      t_Void vOnDeviceDisconnection(const t_U32 cou32DeviceHandle);


      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::bPrepareSession
       ***************************************************************************/
      /*!
       * \fn     bPrepareSession()
       * \brief  Prepares for android auto session
       * \brief  true if session preparation successful
       **************************************************************************/
      t_Bool bPrepareSession();
  
      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vSetPlaybackRepeatState(const trAAP...
       ***************************************************************************/
      /*!
       * \fn     vSetPlaybackRepeatState(
                 const trAAPMediaPlaybackStatus* coprAAPMediaPlaybackStatus)
       * \brief  Gets the repeat all and repeat one states from the given 
       *         MediaPlaybackStatus object
       * \brief  Sets repeat all and repeat one states of AppMedia metadata
       * \param  prAAPMediaPlaybackStatus  for MediaPlaybackStatus update
       **************************************************************************/
      t_Void vSetPlaybackRepeatState(const trAAPMediaPlaybackStatus* coprAAPMediaPlaybackStatus, t_Bool& bSolicited);
  
      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vSetPlaybackMediaSource(const trAAP...
       ***************************************************************************/
      /*!
       * \fn     vSetPlaybackMediaSource(
                 const trAAPMediaPlaybackStatus* coprAAPMediaPlaybackStatus)
       * \brief  Sets the media source of AppMedia metadata using the given 
       *         MediaPlaybackStatus object
       * \param  prAAPMediaPlaybackStatus  for MediaPlaybackStatus update
       **************************************************************************/
      t_Void vSetPlaybackMediaSource(const trAAPMediaPlaybackStatus* coprAAPMediaPlaybackStatus, t_Bool& bSolicited);
	  
      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vProcessDeviceSelection(const t_U32...
       ***************************************************************************/
      /*!
       * \fn     vProcessDeviceSelection(const t_U32& corfu32DeviceHandle)
       * \brief  Triggers AAP switch if the AAP connection is not up
       *         Posts SelectDevice result
       * \param  corfu32DeviceHandle  : [IN] Device handle
       **************************************************************************/
      t_Void vProcessDeviceSelection(const t_U32& corfu32DeviceHandle);
  
      /***************************************************************************
       ** FUNCTION:  spi_tclAautoConnection::vProcessDeviceDeselection()
       ***************************************************************************/
      /*!
       * \fn     vProcessDeviceDeselection(const t_U32& corfu32DeviceHandle,
       *         t_Bool bIsHMITrigger)
       * \brief  Checks if device is deselected from HMI.
       *	     If yes, sends Bye Bye message.
       *         Else stops transport and posts SelectDevice result
       * \param  corfu32DeviceHandle : [IN] Device handle
       * \param  bIsHMITrigger       : [IN] Device deselection from HMI check
       **************************************************************************/
      t_Void vProcessDeviceDeselection(const t_U32& corfu32DeviceHandle,
             t_Bool bIsHMITrigger);

      /***************************************************************************
      ** FUNCTION:  t_Void spi_tclAAPRespSession::vServiceDiscoveryRequestCb
      ***************************************************************************/
      /*!
      * \fn     vServiceDiscoveryRequestCb
      * \brief   Called when service discovery request is received.
      *          This call sends across an icon set and a label that can be
      *          used by the native UI to display a button that allows
      *          users to switch back to projected mode.
      * \param     szSmallIcon   32x32 png image.
      * \param     szMediumIcon  64x64 png image.
      * \param     szLargeICon   128x128 png image.
      * \param     szLabel  A label that may be displayed alongside the icon.
      * \param     szDeviceName Name of Device
      ***************************************************************************/
      virtual t_Void vServiceDiscoveryRequestCb(t_String szSmallIcon, t_String szMediumIcon, t_String szLargeICon , t_String szLabel , t_String szDeviceName);

      //! Device selection status
      tenDeviceConnectionReq m_enDeviceSelReq; 

      //! Current selected device
      t_U32 m_u32CurrSelectedDevice; 

      //! Populate metadata info
      //trAppMetaData m_rAppMetaData;

      //! DiPo media metadata
      trAppMediaMetaData m_rAppMediaMetaData;

      //! DiPo  media playtime
      trAppMediaPlaytime m_rAppMediaPlaytime;

      //! Indicates if device switch request is in progress
      t_Bool m_bSwitchInProgress;

      //! Store head unit information
      trHeadUnitInfo m_rHeadUnitInfo;

      //! Maintain the status of last session
      trSessionHistory m_rSessionHistory;

      //! Certificate type
      tenCertificateType m_enCertificateType;
};
#endif // SPI_TCLAAUTOCONNECTION_H_
