/***********************************************************************/
/*!
* \file  NmeaEncoder.cpp
* \brief Nmea String Encoder class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Nmea String Encoder class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
04.02.2014  | Ramya Murthy          | Initial Version
23.04.2014  | Ramya Murthy          | Updated posix time conversion.
13.06.2014  | Ramya Murthy          | Implementation for VDSensor data
                                      integration in GGA & RMC formats,
                                      and included PASCD format.
14.10.2014  | Hari Priya E R        |Added Changes for handling PASCD data
23.04.2015  | Ramya Murthy          | Changes to support PASCD & PAGCD sentences using GPS data

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <ctime>
#include <sstream>
#include <ios>
#include <cmath>

#include "NmeaEncoder.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/NmeaEncoder.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
//! Defines for number of digits before decimal point (i.e. integral part)
#define LATLONG_MINUTES_NUM_DIGITS_INTEGRAL  2
#define HEADING_NUM_DIGITS_INTEGRAL          3


//! Precision for Latitude/Longitude values (i.e. Number of digits after decimal point)
#define LATLONG_PRECISION              6

//! Precision for Altitude values (i.e. Number of digits after decimal point)
#define ALTITUDE_PRECISION             1

//! Precision for Speed values (i.e. Number of digits after decimal point)
#define SPEED_PRECISION                2

//! Precision for Heading values (i.e. Number of digits after decimal point)
#define HEADING_PRECISION              1

//! Precision for Geoidal separation value (i.e. Number of digits after decimal point)
#define GEOIDAL_SEPARATION_PRECISION   2

//! Precision for Time value (i.e. Number of digits after decimal point)
#define TIME_PRECISION                 2

//! Precision for HDOP value (i.e. Number of digits after decimal point)
#define HDOP_PRECISION                 1


//! Number of characters in time formatted as "hhmmss"
#define TIME_HHMMSS_WIDTH           6

//! Number of characters in date formatted as "ddmmyyyy"
#define DATE_DDMMYY_WIDTH           6

//! Number of characters in checksum value
#define CHECKSUM_WIDTH              2

//! Number of digits in Latitude degrees value
#define LATITUDE_DEGREES_WIDTH      2

//! Number of digits in Longitude degrees value
#define LONGITUDE_DEGREES_WIDTH     3

//! Number of digits in Latitude/Longitude minutes value
//! @Note: This includes digits before & after decimal point, and the decimal point (1).
#define LATLONG_MINUTES_WIDTH       ((LATLONG_MINUTES_NUM_DIGITS_INTEGRAL) + (LATLONG_PRECISION) + 1)

//! Number of digits in Speed & Heading values
//! @Note: This includes digits before & after decimal point, and the decimal point (1).
#define HEADING_WIDTH               ((HEADING_NUM_DIGITS_INTEGRAL) + (HEADING_PRECISION) + 1)

//! Number of digits in Satellites used value
#define SATELLITES_USED_WIDTH       2

//! Precision for Speed & Heading values (i.e. Number of digits after decimal point)
#define NUM_MINUTES_PER_DEGREE      60

//! Macro to convert milliseconds to seconds
#define CONVERT_MILLISECONDS_TO_SECONDS(TIME_IN_MS)   ((TIME_IN_MS)/(1000.0))

#define CHAR_POSITION_2             1



/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static const t_String coszNmea_String_Start_Symbol = "$";
static const t_String coszNmea_String_End_Symbol   = "*";
static const t_String coszNmea_String_CRLF         = "\r\n";

static const t_String coszDirection_North = "N";
static const t_String coszDirection_South = "S";
static const t_String coszDirection_East  = "E";
static const t_String coszDirection_West  = "W";

static const tTalkerIdentifier coszNmea_TalkerIdentifier_GPS      = "GP";
static const tTalkerIdentifier coszNmea_TalkerIdentifier_Unknown  = "--";

static const tSentenceIdentifier coszNmea_SentenceIdentifier_GGA = "GGA";
static const tSentenceIdentifier coszNmea_SentenceIdentifier_RMC = "RMC";

static const t_String coszUnits_Meter = "M";
static const t_Char cochZero = '0';

static const t_String coszGPSQlty_FixNotAvailable = "0";
static const t_String coszGPSQlty_GPSFixAvailable = "1";
static const t_String coszGPSQlty_DGPSFixAvailable  = "2";

static const t_String coszDataStatus_Valid = "A";
static const t_String coszDataStatus_Invalid = "V";

static const t_String coszFAAMode_Autonomous = "A";
static const t_String coszFAAMode_Differential = "D";
static const t_String coszFAAMode_DataInvalid = "N";

static const t_String coszTransmissionState_Unknown = "U";
static const t_String coszTransmissionState_Park = "P";
static const t_String coszTransmissionState_Forward = "D";
static const t_String coszTransmissionState_Reverse = "R";
static const t_String coszTransmissionState_Neutral  = "N";

static const t_String coszSensorType_CombinedLeftRightWheelSensors = "C";

//! Conversion formula: for converting cm per second to knots.
//! @Note: 1 centimeter / second = 0.0194384449 knots
static const t_Double codConversionRate_CmpsToKnots = 0.0194384449;
//! Conversion formula: for converting cm per second to meter per second.
static const t_Double codConversionRate_CmpsToMps = 0.01;
//! Conversion formula: for converting centi degree per second to degree per second.
static const t_Double codConversionRate_CentiDegToDeg_ps = 0.01;

//! Flag to check for initialisation of string streams
static t_Bool bIsDataStreamsInitialised = false;

//! String stream object for converting Date info to string
static std::ostringstream oChecksumStream;
//! String stream object for converting Latitude degrees info to string
static std::ostringstream oLatitudeDegStream;
//! String stream object for converting Longitude degrees info to string
static std::ostringstream oLongitudeDegStream;
//! String stream object for converting Latitude/Longitude minutes info to string
static std::ostringstream oLatLongMinStream;
//! String stream object for converting Altitude info to string
static std::ostringstream oAltitudeStream;
//! String stream object for converting Speed info to string
static std::ostringstream oSpeedStream;
//! String stream object for converting  info to string
static std::ostringstream oHeadingStream;
//! String stream object for converting Time info to string
static std::ostringstream oTimeStream;
//! String stream object for converting TurnRate info to string
static std::ostringstream oTurnRateStream;

//! String stream object for converting Satellites used info to string
static std::ostringstream oSatellitesUsedStream;
//! String stream object for converting HDOP info to string
static std::ostringstream oHDOPStream;
//! String stream object for converting Geoidal Separation info to string
static std::ostringstream oGeoidalSeparationStream;

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  NmeaEncoder::NmeaEncoder(const trGPSData& rfcorGpsData,...)
***************************************************************************/
NmeaEncoder::NmeaEncoder(const trGPSData& rfcorGpsData, const trSensorData& rfcoSensorData,
                         const trVehicleData& rfcorVehicleData)
   : m_rGpsData(rfcorGpsData), m_rSensorData(rfcoSensorData),m_rVehicleData(rfcorVehicleData)
{
   ETG_TRACE_USR4(("NmeaEncoder() entered "));

   if (false == bIsDataStreamsInitialised)
   {
      vInitialise();
   }
}

/***************************************************************************
** FUNCTION: NmeaEncoder::~NmeaEncoder()
***************************************************************************/
NmeaEncoder::~NmeaEncoder()
{
   ETG_TRACE_USR4(("~NmeaEncoder() entered "));
}

/***************************************************************************
** FUNCTION: NmeaEncoder::vInitialise()
***************************************************************************/
t_Void NmeaEncoder::vInitialise()
{
   ETG_TRACE_USR4(("NmeaEncoder::vInitialise() entered "));

   bIsDataStreamsInitialised = true;
   //Flag prevents re-initialisation of data stream fields

   //! Initialise all static data stream object fields (i.e, width, precision, etc.)

   //For Checksum data: Formatted as:
   //<CHECKSUM_WIDTH>-digit hexadecimal number, zero-padded. (e.g,
   oChecksumStream.fill(cochZero);
   oChecksumStream.width(CHECKSUM_WIDTH);
   oChecksumStream.setf(std::ios::hex, std::ios::basefield);
   oChecksumStream.setf(std::ios::uppercase);

   //For Latitude degrees data: Formatted as:
   //<LATITUDE_DEGREES_WIDTH>-digit decimal number (Range 0-90, zero padded)
   oLatitudeDegStream.fill(cochZero);
   oLatitudeDegStream.width(LATITUDE_DEGREES_WIDTH);
   oLatitudeDegStream.setf(std::ios::dec, std::ios::basefield);

   //For Longitude degrees data: Formatted as:
   //<LONGITUDE_DEGREES_WIDTH>-digit decimal number (Range 0-180, zero padded)
   oLongitudeDegStream.fill(cochZero);
   oLongitudeDegStream.width(LONGITUDE_DEGREES_WIDTH);
   oLongitudeDegStream.setf(std::ios::dec, std::ios::basefield);

   //For Latitude & Longitude minutes data: Formatted as decimal number with:
   //a) <LATLONG_MINUTES_NUM_DIGITS_INTEGRAL> digits before decimal point (Range 0-59, zero padded), and
   //b) <LATLONG_PRECISION> digits after decimal point
   oLatLongMinStream.fill(cochZero);
   oLatLongMinStream.width(LATLONG_MINUTES_WIDTH);
   oLatLongMinStream.precision(LATLONG_PRECISION);
   oLatLongMinStream.setf(std::ios::dec, std::ios::basefield);
   oLatLongMinStream.setf(std::ios::fixed, std::ios::floatfield);

   //For Altitude data: Formatted as data with maximum of <ALTITUDE_PRECISION> digits after decimal point.
   oAltitudeStream.precision(ALTITUDE_PRECISION);

   //For Speed data: Formatted as decimal number with <SPEED_PRECISION> digits after decimal point.
   oSpeedStream.precision(SPEED_PRECISION);
   oSpeedStream.setf(std::ios::dec, std::ios::basefield);
   oSpeedStream.setf(std::ios::fixed, std::ios::floatfield);

   //For Heading data: Formatted as decimal number with:
   //a) Maximum of <HEADING_NUM_DIGITS_INTEGRAL> digits before decimal point (zero-padded), and
   //b) <HEADING_PRECISION> digit after decimal point.
   oHeadingStream.fill(cochZero);
   oHeadingStream.precision(HEADING_PRECISION);
   oHeadingStream.width(HEADING_WIDTH);
   oHeadingStream.setf(std::ios::dec, std::ios::basefield);
   oHeadingStream.setf(std::ios::fixed, std::ios::floatfield);

   //For Time data: Formatted as decimal number with <TIME_PRECISION> digits after decimal point.
   oTimeStream.precision(TIME_PRECISION);
   oTimeStream.setf(std::ios::dec, std::ios::basefield);
   oTimeStream.setf(std::ios::fixed, std::ios::floatfield);

   //For Satellites used data: Formatted as:
   //<SATELLITES_USED_WIDTH>-digit decimal number (zero-padded, range: 00-12)
   oSatellitesUsedStream.fill(cochZero);
   oSatellitesUsedStream.width(SATELLITES_USED_WIDTH);
   oSatellitesUsedStream.setf(std::ios::dec, std::ios::basefield);
   oSatellitesUsedStream.setf(std::ios::uppercase);

   //For HDOP data: Formatted as 3-digit decimal number (Max: 99.0)
   oHDOPStream.precision(HDOP_PRECISION);
   oHDOPStream.setf(std::ios::dec, std::ios::basefield);
   oHDOPStream.setf(std::ios::fixed, std::ios::floatfield);

   //For GeoidalSeparation data: Formatted as:
   //3-digit decimal number, with <GEOIDAL_SEPARATION_PRECISION> digits after decimal point.
   oGeoidalSeparationStream.precision(GEOIDAL_SEPARATION_PRECISION);
   oGeoidalSeparationStream.setf(std::ios::dec, std::ios::basefield);
   oGeoidalSeparationStream.setf(std::ios::fixed, std::ios::floatfield);

   //For TurnRate data: Formatted as decimal number with <SPEED_PRECISION> digits after decimal point.
   oTurnRateStream.precision(SPEED_PRECISION);
   oTurnRateStream.setf(std::ios::dec, std::ios::basefield);
   oTurnRateStream.setf(std::ios::fixed, std::ios::floatfield);

}

/***************************************************************************
** FUNCTION:  t_String NmeaEncoder::szGetNmeaGGASentence(tenNmeaDataSource enDataSource)
***************************************************************************/
t_String NmeaEncoder::szGetNmeaGGASentence(tenNmeaDataSource enDataSource) const
{
   ETG_TRACE_USR4(("NmeaEncoder::szGetNmeaGGASentence() entered "));

   //! ------------------------------------------------------------------------------
   //! @Note: GGA String format:
   //! $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
   //! If any of the fields are not available, empty string with separator should be added!
   //! ------------------------------------------------------------------------------
   //! ------------------------ Data -------------------------||-- Mandatory (Y/N) --
   //! 0) SentenceID: Talker identifier(default GP)+"GGA"     ||        Yes
   //! 1) Time (UTC)                                          ||        Yes
   //! 2) Latitude                                            ||        Yes
   //! 3) N or S (North or South)                             ||        Yes
   //! 4) Longitude                                           ||        Yes
   //! 5) E or W (East or West)                               ||        Yes
   //! 6) GPS Quality Indicator                               ||        Yes
   //! 7) Number of satellites in use, 00 - 12                ||        No
   //! 8) Horizontal Dilution of precision                    ||        No
   //! 9) Antenna Altitude above/below mean-sea-level (geoid) ||        Yes
   //! 10) Units of antenna altitude, meters                  ||        Yes
   //! 11) Geoidal separation                                 ||        No
   //! 12) Units of geoidal separation, meters                ||        Yes
   //! 13) Age of differential GPS data                       ||        No
   //! 14) Differential reference station ID, 0000-1023       ||        No
   //! 15) Checksum                                           ||        Yes
   //! ------------------------------------------------------------------------------

   t_String szGGASentence;

   if (e8NMEA_SOURCE_UNKNOWN != enDataSource)
   {
      //! Initialise the NMEA formatted string
      vInitialiseNmeaString(szGGASentence, coszNmea_SentenceIdentifier_GGA, enDataSource);

      //Add info: Time (UTC)
      vAddDataToNmeaString(szGGASentence, szGetFormattedTime(true), false); //GPGGA always uses GPS data
      //Add info: Latitude + Direction
      vAddDataToNmeaString(szGGASentence, szGetFormattedLatitudeInfo(), false);
      //Add info: Longitude + Direction
      vAddDataToNmeaString(szGGASentence, szGetFormattedLongitudeInfo(), false);
      //Add info: GPS Quality Indicator
      vAddDataToNmeaString(szGGASentence, szGetGPSQualityIndicatorInfo(), false);
      //Add info: Number of satellites in use
      vAddDataToNmeaString(szGGASentence, szGetSatellitesUsedInfo(), false);
      //Add info: Horizontal Dilution of precision
      vAddDataToNmeaString(szGGASentence, szGetHorizDilutionOfPrecisionInfo(), false);
      //Add info: Antenna Altitude + Units of altitude
      vAddDataToNmeaString(szGGASentence, szGetFormattedAltitudeInfo(), false);
      //Add info: Geoidal separation + Units of separation
      vAddDataToNmeaString(szGGASentence, szGetGeoidalSeparationInfo(), false);
      //Add info: Age of differential GPS data
      vAddDataToNmeaString(szGGASentence, "", false); //currently info unavailable
      //Add info: Differential reference station ID
      vAddDataToNmeaString(szGGASentence, "", true); //currently info unavailable

      //! Finalise the NMEA formatted string
      vFinaliseNmeaString(szGGASentence);

   } //if (e8NMEA_SOURCE_UNKNOWN != enDataSource)

   return szGGASentence;
}



/***************************************************************************
** FUNCTION:  t_String NmeaEncoder::szGetNmeaRMCSentence(tenNmeaDataSource enDataSource)
***************************************************************************/
t_String NmeaEncoder::szGetNmeaRMCSentence(tenNmeaDataSource enDataSource) const
{
   ETG_TRACE_USR4(("NmeaEncoder::szGetNmeaRMCSentence() entered "));

   //! ------------------------------------------------------------------------------
   //! @Note: RMC String format:
   //! "$--RMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,xxxx,x.x,a*hh"
   //! If any of the fields are not available, empty string with separator should be added!
   //! ------------------------------------------------------------------------------
   //! ------------------------ Data -------------------------||-- Mandatory (Y/N) --
   //! 0) SentenceID: Talker identifier(default GP)+"RMC"     ||        Yes
   //! 1) Time (UTC)                                          ||        Yes
   //! 2) Data Status (A=OK, V=Navigation receiver warning)   ||        Yes
   //! 3) Latitude                                            ||        Yes
   //! 4) N or S (North or South)                             ||        Yes
   //! 5) Longitude                                           ||        Yes
   //! 6) E or W (East or West)                               ||        Yes
   //! 7) Speed over ground, knots                            ||        Yes
   //! 8) Track made good, degrees true                       ||        Yes
   //! 9) Date, ddmmyy                                        ||        Yes
   //! 10) Magnetic Variation, degrees                        ||        No
   //! 11) magnetic Variation (E or W)                        ||        No
   //! 12) FAA Mode Indicator (NMEA v2.3 and later)           ||        Yes
   //! 13) Checksum                                           ||        Yes
   //! ------------------------------------------------------------------------------

   t_String szRMCSentence;

   if (e8NMEA_SOURCE_UNKNOWN != enDataSource)
   {
      //! Initialise the NMEA formatted string
      vInitialiseNmeaString(szRMCSentence, coszNmea_SentenceIdentifier_RMC, enDataSource);

      //Add info: Time (UTC)
      vAddDataToNmeaString(szRMCSentence, szGetFormattedTime(true), false); //GPRMC always uses GPS data
      //Add info: Data Status
      vAddDataToNmeaString(szRMCSentence, szGetDataStatusInfo(), false);
      //Add info: Latitude + Direction
      vAddDataToNmeaString(szRMCSentence, szGetFormattedLatitudeInfo(), false);
      //Add info: Longitude + Direction
      vAddDataToNmeaString(szRMCSentence, szGetFormattedLongitudeInfo(), false);
      //Add info: Speed over ground, knots
      vAddDataToNmeaString(szRMCSentence, szGetFormattedSpeedInfo(true), false); //GPRMC always uses GPS data
      //Add info: Track made good, degrees true
      vAddDataToNmeaString(szRMCSentence, szGetFormattedHeadingInfo(), false);
      //Add info: Date, ddmmyy
      vAddDataToNmeaString(szRMCSentence, szGetFormattedDate(), false);
      //Add info: Magnetic Variation, degrees
      vAddDataToNmeaString(szRMCSentence, "", false); //currently info unavailable
      //Add info: Magnetic Variation (E or W)
      vAddDataToNmeaString(szRMCSentence, "", false); //currently info unavailable
      //Add info: FAA Mode Indicator field (added in NMEA 2.3 and later)
      vAddDataToNmeaString(szRMCSentence, szGetFAAModeIndicatorInfo(), true);

      //! Finalise the NMEA formatted string
      vFinaliseNmeaString(szRMCSentence);

   } //if (e8NMEA_SOURCE_UNKNOWN != enDataSource)

   return szRMCSentence;
}

/***************************************************************************
** FUNCTION:  t_String NmeaEncoder::szGetNmeaPASCDSentence()
***************************************************************************/
t_String NmeaEncoder::szGetNmeaPASCDSentence(t_Bool bUseGpsData) const
{
   ETG_TRACE_USR4(("NmeaEncoder::szGetNmeaPASCDSentence() entered "));

   //! ------------------------------------------------------------------------------
   //! @Note: PASCD String fields:
   //! If any of the fields are not available, empty string with separator should be added!
   //! ------------------------------------------------------------------------------
   //! ------------------------ Data -------------------------||-- Mandatory (Y/N) --
   //! 0) SentenceID: Proprietary Sentence identifier         ||        Yes
   //! 1) Timestamp                                           ||        Yes
   //! 2) SensorType                                          ||        Yes
   //! 3) Transmission state                                  ||        Yes
   //! 4) SlipDetect                                          ||        Yes
   //! 5) SampleCount                                         ||        Yes
   //! 6) timeOffset_i                                        ||        Yes
   //! 7) speed_i                                             ||        Yes
   //! 8) Checksum                                            ||        Yes
   //! ------------------------------------------------------------------------------

   t_String szPASCDSentence;
   

   //! Initialise the NMEA formatted string
   vInitialiseProprietaryNmeaString(szPASCDSentence, e8NMEA_PASCD);

   //Add info: Timestamp
   vAddDataToNmeaString(szPASCDSentence, szGetFormattedTime(bUseGpsData), false);
   //Add info: SensorType
   vAddDataToNmeaString(szPASCDSentence, szGetFormattedSensorTypeInfo(bUseGpsData), false);
   //Add info: Transmission state
   vAddDataToNmeaString(szPASCDSentence, szGetFormattedTransStateInfo(), false);
   //Add info: SlipDetect
   vAddDataToNmeaString(szPASCDSentence, "0", false);
   //Add info: SampleCount
   vAddDataToNmeaString(szPASCDSentence, "1", false);
   //Add info: timeOffset_i
   vAddDataToNmeaString(szPASCDSentence, "0", false);
   //Add info: speed_i
   vAddDataToNmeaString(szPASCDSentence, szGetFormattedSpeedInfo(bUseGpsData), true);

   //! Finalise the NMEA formatted string
   vFinaliseNmeaString(szPASCDSentence);

   return szPASCDSentence;
}

/***************************************************************************
** FUNCTION:  t_String NmeaEncoder::szGetNmeaPAGCDSentence()
***************************************************************************/
t_String NmeaEncoder::szGetNmeaPAGCDSentence(t_Bool bUseGpsData) const
{
   ETG_TRACE_USR4(("NmeaEncoder::szGetNmeaPAGCDSentence() entered "));

   //! ------------------------------------------------------------------------------
   //! @Note: PAGCD String fields:
   //! If any of the fields are not available, empty string with separator should be added!
   //! ------------------------------------------------------------------------------
   //! ------------------------ Data -------------------------||-- Mandatory (Y/N) --
   //! 0) SentenceID: Proprietary Sentence identifier         ||        Yes
   //! 1) Timestamp                                           ||        Yes
   //! 2) SampleCount                                         ||        Yes
   //! 3) timeOffset_i                                        ||        Yes
   //! 4) xAxisSample_i                                       ||        Yes
   //! 5) yAxisSample_i                                       ||        Yes
   //! 6) zAxisSample_i                                       ||        Yes
   //! 7) Checksum                                            ||        Yes
   //! ------------------------------------------------------------------------------

   t_String szPAGCDSentence;


   //! Initialise the NMEA formatted string
   vInitialiseProprietaryNmeaString(szPAGCDSentence, e8NMEA_PAGCD);

   //Add info: Timestamp
   vAddDataToNmeaString(szPAGCDSentence, szGetFormattedTime(bUseGpsData), false);
   //Add info: SampleCount
   vAddDataToNmeaString(szPAGCDSentence, "1", false);
   //Add info: timeOffset_i
   vAddDataToNmeaString(szPAGCDSentence, "0", false);
   //Add info: xAxisSample_i
   vAddDataToNmeaString(szPAGCDSentence, "", false); //currently info unavailable
   //Add info: yAxisSample_i
   vAddDataToNmeaString(szPAGCDSentence, "", false); //currently info unavailable
   //Add info: zAxisSample_i
   vAddDataToNmeaString(szPAGCDSentence, szGetFormattedTurnRateInfo(), true);

   //! Finalise the NMEA formatted string
   vFinaliseNmeaString(szPAGCDSentence);

   return szPAGCDSentence;
}

/***************************************************************************
*********************************PRIVATE************************************
***************************************************************************/

/***************************************************************************
** FUNCTION: t_Void NmeaEncoder::vAddDataToNmeaString()
***************************************************************************/
t_Void NmeaEncoder::vAddDataToNmeaString(t_String& rfszNmeaString ,
      const t_String& rfcoszData,
      t_Bool bIsLastDataInString,
      t_String szEndDelimiter) const
{
   rfszNmeaString  += rfcoszData;
   if (false == bIsLastDataInString)
   {
      rfszNmeaString  += szEndDelimiter;
   }
}

/***************************************************************************
** FUNCTION: t_Void NmeaEncoder::vInitialiseNmeaString()
***************************************************************************/
t_Void NmeaEncoder::vInitialiseNmeaString(t_String& rfszNmeaString,
      const tSentenceIdentifier& rfcoSentenceIdentifier,
      tenNmeaDataSource enDataSource) const
{
   ETG_TRACE_USR4(("NmeaEncoder::vInitialiseNmeaString() entered "));
   ETG_TRACE_USR4(("vInitialiseNmeaString: Received Sentence Identifier = %s ", rfcoSentenceIdentifier.c_str()));
   ETG_TRACE_USR4(("vInitialiseNmeaString: Received Data Source = %u ", ETG_ENUM(NMEA_DATA_SOURCE_TYPE, enDataSource)));

   //!-------------------------------------------------------------
   //! Sentence ID format: <Talker_Identifier><Sentence_Identifier>
   //! For example, "GPGGA"
   //!-------------------------------------------------------------
   //! Note:
   //! <Talker_Identifier>   : Max. 2 characters
   //! <Sentence_Identifier> : Max. 3 characters
   //!-------------------------------------------------------------
   //Add starting symbol & Talker identifier to NMEA string
   tTalkerIdentifier TalkerIdentfier = (e8NMEA_SOURCE_GPS == enDataSource) ?
	   coszNmea_TalkerIdentifier_GPS : coszNmea_TalkerIdentifier_Unknown;
   rfszNmeaString  = coszNmea_String_Start_Symbol + static_cast<t_String>(TalkerIdentfier);
   
   //Append Sentence identifier
   vAddDataToNmeaString(rfszNmeaString , static_cast<t_String>(rfcoSentenceIdentifier), false);

   ETG_TRACE_USR4(("vInitialiseNmeaString() left with NMEA string = %s ", rfszNmeaString .c_str()));
}

/***************************************************************************
** FUNCTION: t_Void NmeaEncoder::vInitialiseProprietaryNmeaString()
***************************************************************************/
t_Void NmeaEncoder::vInitialiseProprietaryNmeaString(t_String& rfszNmeaString,
      tenNmeaSentenceType enNmeaSentenceType) const
{
   ETG_TRACE_USR4(("NmeaEncoder::vInitialiseProprietaryNmeaString() entered: NmeaSentenceType = %d ",
         ETG_ENUM(NMEA_SENTENCE_TYPE, enNmeaSentenceType)));

   //!-------------------------------------------------------------
   //! Sentence ID : "PASCD", "PAGCD", etc.
   //!-------------------------------------------------------------

   //! Determine sentence ID
   t_String szSentenceID;
   switch (enNmeaSentenceType)
   {
      case e8NMEA_PASCD:
         szSentenceID = "PASCD";
         break;
      case e8NMEA_PAGCD:
         szSentenceID = "PAGCD";
         break;
      case e8NMEA_PAACD:
         szSentenceID = "PAACD";
         break;
      default:
         ETG_TRACE_ERR(("vInitialiseProprietaryNmeaString: Invalid enum! "));
         break;
   }

   //Append Start symbol ($) and Sentence ID to start of sentence
   vAddDataToNmeaString(rfszNmeaString , coszNmea_String_Start_Symbol + szSentenceID, false);

   ETG_TRACE_USR4(("vInitialiseProprietaryNmeaString() left with NMEA string = %s ", rfszNmeaString .c_str()));
}

/***************************************************************************
** FUNCTION: t_Void NmeaEncoder::vFinaliseNmeaString()
***************************************************************************/
t_Void NmeaEncoder::vFinaliseNmeaString(t_String& rfszNmeaString ) const
{
   //! Append NMEA terminating string symbol to the NMEA string.
   rfszNmeaString  += coszNmea_String_End_Symbol;

   //!-------------------------------------------------------------
   //! @Note: Checksum is a 2-digit hexadecimal number, which is
   //! calculated as the exclusive OR of all characters between,
   //! but not including, the "$" and "*" of NMEA sentence.
   //!-------------------------------------------------------------

   //! Calculate checksum value
   t_U16 u16CheckSum = 0;
   for (t_U16 u16Index = 1; u16Index < (rfszNmeaString .length() - 1); ++u16Index)
   {
      u16CheckSum = u16CheckSum ^ rfszNmeaString[u16Index];
   }
   ETG_TRACE_USR4(("vFinaliseNmeaString: Calculated Checksum = 0x%x ", u16CheckSum));

   //! Convert checksum value to string
   oChecksumStream.str("");
   oChecksumStream.width(CHECKSUM_WIDTH); 
   oChecksumStream << u16CheckSum;
   //! Append Checksum and Carriage return & Line Feed to the NMEA string
   rfszNmeaString  += oChecksumStream.str() + coszNmea_String_CRLF;

   ETG_TRACE_USR4(("NmeaEncoder::vFinaliseNmeaString() left with NMEA string = %s ", rfszNmeaString.c_str()));
}


/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedTime()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedTime(t_Bool bUseGpsData) const
{
   //If the sentence is PASCD,use the system time,else use the time value received from the GMLAN Gateway
   t_PosixTime tPosixTime = (true == bUseGpsData)?
         (m_rGpsData.PosixTime) : (m_rVehicleData.PosixTime);
   time_t T = static_cast<time_t>(tPosixTime);
   trDateTime* prDateTimeInfo = gmtime(&T);

   t_String szFormattedTime;
   SPI_NORMAL_ASSERT(NULL == prDateTimeInfo);
   if (NULL != prDateTimeInfo)
   {
      //!-------------------------------------------------------------
      //! Format time as a string : "HHMMSS"
      //!-------------------------------------------------------------
      t_Char TimeBuffer[TIME_HHMMSS_WIDTH + 1];
      memset(&TimeBuffer, 0 , (TIME_HHMMSS_WIDTH + 1));
      //@Note: Size of buffer = Num of characters in Time + 1 (for terminating null character)
      strftime(TimeBuffer, (TIME_HHMMSS_WIDTH + 1) ,"%H%M%S", prDateTimeInfo);

      //!-------------------------------------------------------------
      //! Calculate time precision in milliseconds as a decimal number
      //! with 2 digits after decimal point. (Example: 500ms = 0.50s)
      //!-------------------------------------------------------------
      oTimeStream.str("");
      oTimeStream << CONVERT_MILLISECONDS_TO_SECONDS(m_rGpsData.u16ExactTime);

      //!-------------------------------------------------------------
      //! Format final time as : "HHMMSS.SS"
      //!-------------------------------------------------------------
      szFormattedTime = (TimeBuffer + (oTimeStream.str().substr(CHAR_POSITION_2 , TIME_PRECISION + 1)));
      //@Note: A sub-string of milliseconds string is used in order to use only characters
      //after & including the decimal point.
      //i.e, If milliseconds data = 0.50, extracted substring is ".50"
   }
   else
   {
      ETG_TRACE_ERR(("ERROR: szGetFormattedTime: Error while converting Posix time!"));
   }
   return szFormattedTime;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedDate()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedDate() const
{
   time_t T = static_cast<time_t>(m_rGpsData.PosixTime);
   trDateTime* prDateTimeInfo = gmtime(&T);
   
   t_String szFormattedDate;
   SPI_NORMAL_ASSERT(NULL == prDateTimeInfo);
   if (NULL != prDateTimeInfo)
   {
      //! Format date as: "ddmmyy"
      t_Char DateBuffer[DATE_DDMMYY_WIDTH + 1];
      memset(&DateBuffer, 0, (DATE_DDMMYY_WIDTH + 1));
      //@Note: Size of buffer = Num of characters in Date + 1 (for terminating null character)
      strftime(DateBuffer, (DATE_DDMMYY_WIDTH + 1), "%d%m%y", prDateTimeInfo);

      szFormattedDate = DateBuffer;
   }
   else
   {
      ETG_TRACE_ERR(("ERROR: szGetFormattedDate: Error while converting Posix time!"));
   }
   return szFormattedDate;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedLatLongInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedLatitudeInfo() const
{
   //!-------------------------------------------------------------
   //! Format Latitude as: <value in decimal>,<direction:S/N>
   //!-------------------------------------------------------------
   //! Note:
   //! <Latitude in degrees> =  <Latitude in decimal> / <Resolution>
   //! Direction is based in Latitude value i.e. (South=-, North=+)
   //!-------------------------------------------------------------

   t_String szFormattedLatitude;
   t_String szDirection;

   if (true == m_rGpsData.bLatitudeAvailable)
   {
      // Calculate Latitude & convert to string
      szFormattedLatitude = szGetLatitudeInDegreesMinutes(
            fabs((m_rGpsData.s32Latitude) / (m_rGpsData.dLatLongResolution)));

      // Add separator & direction info
      szDirection = (0 > m_rGpsData.s32Latitude) ? (coszDirection_South) : (coszDirection_North);
   }
   szFormattedLatitude += coszNmea_String_Delimiter + szDirection;

   return szFormattedLatitude;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedLongitudeInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedLongitudeInfo() const
{
   //!-------------------------------------------------------------
   //! Format Longitude as: <value in decimal>,<direction:E/W>
   //!-------------------------------------------------------------
   //! Note:
   //! <Longitude in degrees> =  <Longitude in decimal> / <Resolution>
   //! Direction is based in Longitude value i.e. (West=-, East=+)
   //!-------------------------------------------------------------

   // Calculate Longitude & convert to string
   t_String szFormattedLongitude;
   t_String szDirection;

   if (true == m_rGpsData.bLongitudeAvailable)
   {
      szFormattedLongitude = szGetLongitudeInDegreesMinutes(
            fabs((m_rGpsData.s32Longitude) / (m_rGpsData.dLatLongResolution)));

      // Add separator & direction info
      szDirection = (0 > m_rGpsData.s32Longitude) ? (coszDirection_West) : (coszDirection_East);
   }
   szFormattedLongitude += coszNmea_String_Delimiter + szDirection;

   return szFormattedLongitude;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedAltitudeInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedAltitudeInfo() const
{
   //!-------------------------------------------------------------
   //! Format Altitude as: <Altitude>,<Units>
   //!-------------------------------------------------------------
   //! Note:
   //! Default Altitude units is meters "M"
   //!-------------------------------------------------------------

   t_String szFormattedAltitude;
   //Important - clear stream before use!
   oAltitudeStream.str("");

   if (true == m_rGpsData.bAltitudeAvailable)
   {
      // Calculate Altitude & convert to string
      oAltitudeStream << m_rGpsData.s32Altitude;
   }
   // Append Units data to string
   szFormattedAltitude = oAltitudeStream.str() + coszNmea_String_Delimiter + coszUnits_Meter;

   return szFormattedAltitude;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedSpeedInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedSpeedInfo(t_Bool bUseGpsData) const
{
  //!-------------------------------------------------------------
   //! Format Speed in knots
   //!-------------------------------------------------------------
   //! Note:
   //! <Speed in knots> = <Speed in cm/s> * 0.0194384449
   //!-------------------------------------------------------------

   //Important - clear stream before use!
   oSpeedStream.str("");

   // Calculate Speed & convert to string (negative values are ignored)
   //If the sentence is PASCD,use the vehicle data speed,else use the GPS data speed
   if (true == m_rVehicleData.bSpeedAvailable)
   {
      t_S16 s16Speed = m_rVehicleData.s16Speed;
      if (0 > s16Speed)
      {
         //! For negative speed values, remove the negative sign.
         s16Speed = s16Speed * (-1);
      }
      oSpeedStream << (s16Speed * codConversionRate_CmpsToMps);
   }//if (true == m_rVehicleData.bSpeedAvailable)
   else if ((true == bUseGpsData) && (true == m_rGpsData.bSpeedAvailable))
   {
      t_S16 s16Speed = m_rGpsData.s16Speed;
      if (0 > s16Speed)
      {
         //! For negative speed values, remove the negative sign.
         s16Speed = s16Speed * (-1);
      }
      oSpeedStream << (s16Speed * codConversionRate_CmpsToKnots);
   }//else if ((true == bUseGpsData) && (true == m_rGpsData.bSpeedAvailable))

   return oSpeedStream.str();
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedHeadingInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedHeadingInfo() const
{
   //!-------------------------------------------------------------
   //! Format Heading in degrees
   //!-------------------------------------------------------------
   //! Note:
   //! <Heading in degrees> =  <Heading in decimal> / <Resolution>
   //!-------------------------------------------------------------

   //Important - clear stream before use!
   oHeadingStream.str("");
   oHeadingStream.width(HEADING_WIDTH);
   
   // Calculate Heading & convert to string
   if (true == m_rGpsData.bHeadingAvailable)
   {
      if(e8CLOCKWISE == m_rGpsData.enHeadingDir)
      {
         oHeadingStream << (m_rGpsData.dHeading)/(m_rGpsData.dHeadingResolution);
      }
      else if (e8ANTICLOCKWISE == m_rGpsData.enHeadingDir)
      {
         (0 != ((m_rGpsData.dHeading) / (m_rGpsData.dHeadingResolution))) ?
            (oHeadingStream << (360 - ((m_rGpsData.dHeading) / (m_rGpsData.dHeadingResolution)))):
         (oHeadingStream << ((m_rGpsData.dHeading) / (m_rGpsData.dHeadingResolution)));
      }
   }//if (true == m_rGpsData.bHeadingAvailable)

   return oHeadingStream.str();
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetLatitudeInDegreesMinutes()
***************************************************************************/
t_String NmeaEncoder::szGetLatitudeInDegreesMinutes(const t_Double& rfcodLatInDecimalDegrees) const
{
   //!-------------------------------------------------------------
   //! Format Latitude as: <Degree_value_dd><Minute_value_mm>
   //!-------------------------------------------------------------
   //! Example:
   //! 1) If Latitude_In_DecimalDeg = 49.279167:
   //!      Latitude_In_DegMin = "4916.75" (i.e, 49 Deg, 16.75 Min)
   //! 2) If Latitude_In_DecimalDeg = 9.279167:
   //!      Latitude_In_DegMin = "0916.75" (i.e, 9 Deg, 16.75 Min)
   //! 3) If Latitude_In_DecimalDeg = 49.087833:
   //!      Latitude_In_DegMin = "4905.27" (i.e, 49 Deg, 5.27 Min)
   //! 4) If Latitude_In_DecimalDeg = 9.087833:
   //!      Latitude_In_DegMin = "0905.27" (i.e, 9 Deg, 5.27 Min)
   //!-------------------------------------------------------------

   //Extract degree value (whole number part) and minute value (fractional part)
   t_U32 u32DegreeValue = static_cast<t_U32>(rfcodLatInDecimalDegrees);
   t_Double dMinuteValue = (rfcodLatInDecimalDegrees - u32DegreeValue) * (NUM_MINUTES_PER_DEGREE);

   //Convert & format as string
   //Important - clear streams before use!
   oLatitudeDegStream.str("");
   oLatitudeDegStream.width(LATITUDE_DEGREES_WIDTH);
   oLatLongMinStream.str("");
   oLatLongMinStream.width(LATLONG_MINUTES_WIDTH);
   oLatitudeDegStream << u32DegreeValue;
   oLatLongMinStream << dMinuteValue;

   return (oLatitudeDegStream.str() + oLatLongMinStream.str());
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetLongitudeInDegreesMinutes()
***************************************************************************/
t_String NmeaEncoder::szGetLongitudeInDegreesMinutes(const t_Double& rfcodLongInDecimalDegrees) const
{
   //!-------------------------------------------------------------
   //! Format Longitude as: <Degree_value_ddd><Minute_value_mm>
   //!-------------------------------------------------------------
   //! Example:
   //! 1) If Longitude_In_DecimalDeg = 149.279167:
   //!      Longitude_In_DegMin = "14916.75" (i.e, 149 Deg, 16.75 Min)
   //! 2) If Longitude_In_DecimalDeg = 9.279167:
   //!      Longitude_In_DegMin = "00916.75" (i.e, 9 Deg, 16.75 Min)
   //! 3) If Longitude_In_DecimalDeg = 149.087833:
   //!      Longitude_In_DegMin = "14905.27" (i.e, 49 Deg, 5.27 Min)
   //! 4) If Longitude_In_DecimalDeg = 9.087833:
   //!      Longitude_In_DegMin = "00905.27" (i.e, 9 Deg, 5.27 Min)
   //!-------------------------------------------------------------

   //Extract degree value (whole number part) and minute value (fractional part)
   t_U32 u32DegreeValue = static_cast<t_U32>(rfcodLongInDecimalDegrees);
   t_Double dMinuteValue = (rfcodLongInDecimalDegrees - u32DegreeValue) * (NUM_MINUTES_PER_DEGREE);

   //Convert & format as string
   //Important - clear streams before use!
   oLongitudeDegStream.str("");
   oLongitudeDegStream.width(LONGITUDE_DEGREES_WIDTH);
   oLatLongMinStream.str("");
   oLatLongMinStream.width(LATLONG_MINUTES_WIDTH);
   oLongitudeDegStream << u32DegreeValue;
   oLatLongMinStream << dMinuteValue;

   return (oLongitudeDegStream.str() + oLatLongMinStream.str());
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetGPSQualityIndicatorInfo()
***************************************************************************/
t_String NmeaEncoder::szGetGPSQualityIndicatorInfo() const
{
   //!-------------------------------------------------------------
   //! Format GPS Quality indicator as below:
   //!-------------------------------------------------------------
   //! Value = Decimal, 1 digit:
   //!  0 = invalid
   //!  1 = GPS fix (SPS)
   //!  2 = DGPS fix
   //!  3 = PPS fix
   //!  4 = Real Time Kinematic
   //!  5 = Float RTK
   //!  6 = estimated (dead reckoning) (NMEA v2.3 feature)
   //!  7 = Manual input mode
   //!  8 = Simulation mode
   //!-------------------------------------------------------------

   //Determine GPS Quality indicator value
   t_String szGPSQltyIndicator;

   switch (m_rSensorData.enGnssQuality)
   {
      case e8GNSSQUALITY_AUTONOMOUS:
         szGPSQltyIndicator = coszGPSQlty_GPSFixAvailable;
      break;
      case e8GNSSQUALITY_DIFFERENTIAL:
         szGPSQltyIndicator = coszGPSQlty_DGPSFixAvailable;
      break;
      case e8GNSSQUALITY_NOFIX:
      case e8GNSSQUALITY_UNKNOWN:
         szGPSQltyIndicator = coszGPSQlty_FixNotAvailable;
      break;
   } //switch (m_rSensorData.enGnssQuality)

   //TODO - how to determine other GPS Quality indicator values

   return szGPSQltyIndicator;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetSatellitesUsedInfo()
***************************************************************************/
t_String NmeaEncoder::szGetSatellitesUsedInfo() const
{
   //!-------------------------------------------------------------
   //! Format No. of Satellites in use data as:
   //! 2-digit decimal number (zero-padded, Range: 00-12)
   //!-------------------------------------------------------------
   //! Example: 05
   //!-------------------------------------------------------------

   //Format Satellites used value
   //Important - clear stream before use!
   oSatellitesUsedStream.str("");
   oSatellitesUsedStream.width(SATELLITES_USED_WIDTH);

   if (true == m_rSensorData.bNumSatUsedAvailable)
   {
      oSatellitesUsedStream << m_rSensorData.u16SatellitesUsed;
   }

   return oSatellitesUsedStream.str();
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetHorizDilutionOfPrecisionInfo()
***************************************************************************/
t_String NmeaEncoder::szGetHorizDilutionOfPrecisionInfo() const
{
   //!-------------------------------------------------------------
   //! Format Horizontal dilution of precision data as:
   //! 3-digit decimal number (Max: 99.0)
   //!-------------------------------------------------------------
   //! Example: 1.5
   //!-------------------------------------------------------------

   //Format Horizontal dilution of precision value
   //Important - clear stream before use!
   oHDOPStream.str("");

   if (true == m_rSensorData.bHDOPAvailable)
   {
      oHDOPStream << m_rSensorData.dHDOP;
   }

   return oHDOPStream.str();
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetGeoidalSeparationInfo()
***************************************************************************/
t_String NmeaEncoder::szGetGeoidalSeparationInfo() const
{
   //!-------------------------------------------------------------
   //! Format Geoidal Separation data as: <Value>,<Unit>
   //! <Value> : 4-digit decimal number, with 2-digits after decimal point
   //! <Unit>  : "M" (meters)
   //!-------------------------------------------------------------
   //! Example:
   //! 1) 47.6,M
   //! 2) -31.81,M
   //!-------------------------------------------------------------

   //Format Geoidal Separation value
   //Important - clear stream before use!
   oGeoidalSeparationStream.str("");

   if (true == m_rSensorData.bGeoidalSepAvailable)
   {
      oGeoidalSeparationStream << m_rSensorData.dGeoidalSeparation;
   }

   // Append with Units data to string
   t_String szFormattedGeoidalSep =
         oGeoidalSeparationStream.str() + coszNmea_String_Delimiter + coszUnits_Meter;

   return szFormattedGeoidalSep;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetDataStatusInfo()
***************************************************************************/
t_String NmeaEncoder::szGetDataStatusInfo() const
{
   //!-------------------------------------------------------------
   //! Format Data Status as: "A" or "V"
   //!-------------------------------------------------------------
   //! A = OK, data is valid
   //! V = Navigation receiver warning
   //!-------------------------------------------------------------

   //Determine Data Status value
   t_String szDataStatus;

   switch (m_rSensorData.enGnssMode)
   {
      case e8GNSSMODE_2DFIX:
      case e8GNSSMODE_3DFIX:
         szDataStatus = coszDataStatus_Valid;
      break;
      case e8GNSSMODE_NOFIX:
      case e8GNSSMODE_UNKNOWN:
      default:
         szDataStatus = coszDataStatus_Invalid;
      break;
   } //switch (m_rSensorData.enGnssMode)

   return szDataStatus;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFAAModeIndicatorInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFAAModeIndicatorInfo() const
{
   //!-------------------------------------------------------------
   //! Format FAA mode indicator as below:
   //!-------------------------------------------------------------
   //! In NMEA 2.3, several sentences (APB, BWC, BWR, GLL, RMA, RMB, RMC,
   //! VTG, WCV, and XTE) got a new last field carrying the signal integrity
   //! information needed by the FAA.  (The values in the GGA mode field were
   //! extended to carry this information as well.) Here are the values:
   //! FAA Mode Indicator
   //!      A = Autonomous mode
   //!      D = Differential Mode
   //!      E = Estimated (dead-reckoning) mode
   //!      M = Manual Input Mode
   //!      S = Simulated Mode
   //!      N = Data Not Valid
   //! This field may be empty. In pre-2.3 versions it is omitted. [NTUM] says
   //! that according to the NMEA specification, it dominates the Status field
   //! -- the Status field will be set to "A" (data valid) for Mode Indicators
   //! A and D, and to "V" (data invalid) for all other values of the Mode
   //! Indicator.  This is confirmed by [IEC].
   //!-------------------------------------------------------------

   //Determine FAA mode indicator
   t_String szFAAMode;
   switch (m_rSensorData.enGnssQuality)
   {
      case e8GNSSQUALITY_AUTONOMOUS:
         szFAAMode = coszFAAMode_Autonomous;
         break;
      case e8GNSSQUALITY_DIFFERENTIAL:
         szFAAMode = coszFAAMode_Differential;
         break;
      case e8GNSSQUALITY_NOFIX:
      case e8GNSSQUALITY_UNKNOWN:
      default:
         szFAAMode = coszFAAMode_DataInvalid;
         break;
   } //switch (m_rSensorData.enGnssQuality)

   //TODO - how to determine other FAA mode values

   return szFAAMode;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedTransStateInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedTransStateInfo()const
{
   t_String szTransState;

   //!-------------------------------------------------------------
   //! Map the Gear position to one of the below Transmission states:
   //! U = Unknown
   //! P = Park
   //! R = Reverse
   //! D = Driving forward
   //! N = Neutral
   //!-------------------------------------------------------------

   switch (m_rVehicleData.enVehMovState)
   {
      case e8VEHICLE_MOVEMENT_STATE_REVERSE:
         szTransState = coszTransmissionState_Reverse;
         break;
      case e8VEHICLE_MOVEMENT_STATE_FORWARD:
         szTransState = coszTransmissionState_Forward;
         break;
      case e8VEHICLE_MOVEMENT_STATE_NEUTRAL:
         szTransState = coszTransmissionState_Neutral;
         break;
      case e8VEHICLE_MOVEMENT_STATE_PARKED:
         szTransState = coszTransmissionState_Park;
         break;
      case e8VEHICLE_MOVEMENT_STATE_INVALID:
      default:
         szTransState = coszTransmissionState_Unknown;
         ETG_TRACE_ERR(("szGetFormattedTransStateInfo: Invalid transmission state %d",
               ETG_ENUM(VEH_MOV_STATE, m_rVehicleData.enVehMovState)));
         break;
   }//switch (m_rGpsData.enVehMovementState)

   return szTransState;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedSensorTypeInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedSensorTypeInfo(t_Bool bUseGpsData)const
{
   t_String szSensorType;

   if ((false == bUseGpsData) ||
         (e8SENSOR_TYPE_COMBINED_LEFT_RIGHT_WHEEL == m_rGpsData.enSensorType))
   {
      szSensorType = coszSensorType_CombinedLeftRightWheelSensors;
   }

   return szSensorType;
}

/***************************************************************************
** FUNCTION: t_String NmeaEncoder::szGetFormattedTurnRateInfo()
***************************************************************************/
t_String NmeaEncoder::szGetFormattedTurnRateInfo()const
{
   //!-------------------------------------------------------------
   //! Calculate Turn Rate as:
   //!-------------------------------------------------------------
   //! <TurnRate_in_deg/sec> = <TurnRate_in_centideg/sec> * (0.01) * (-1)
   //!-------------------------------------------------------------

   //@Note: Positive turnrate means vehicle is turning left, negative means vehicle is turning right.
   //But for PAGCD the reverse is needed, hencevalue should always be multiplied by -1.

   oTurnRateStream.str("");
   t_S16 s16NewTurnRate = (m_rGpsData.s16TurnRate) * (-1);
   oTurnRateStream << (s16NewTurnRate * codConversionRate_CentiDegToDeg_ps);

   return oTurnRateStream.str();
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
