/***********************************************************************/
/*!
* \file  NmeaEncoder.h
* \brief Nmea String Encoder class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Nmea String Encoder class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
04.02.2014  | Ramya Murthy          | Initial Version
13.06.2014  | Ramya Murthy          | Implementation for VDSensor data
                                      integration in GGA & RMC formats,
                                      and included PASCD format.
14.10.2014  | Hari Priya E R        |Added Changes for handling PASCD data
23.04.2015  | Ramya Murthy          | Changes to support PASCD & PAGCD sentences using GPS data

\endverbatim
*************************************************************************/
#ifndef _NMEAENCODER_H_
#define _NMEAENCODER_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "SPITypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef struct tm trDateTime;

typedef t_String  tTalkerIdentifier;
typedef t_String  tSentenceIdentifier;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
const static t_String coszNmea_String_Delimiter = ",";

/****************************************************************************/
/*!
* \class NmeaEncoder
* \brief Nmea Format Encoder utility
*
* It provides methods to retrieve GPS data in NMEA-format encoded strings
****************************************************************************/
class NmeaEncoder
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  NmeaEncoder::NmeaEncoder(const trGPSData& rfcorGpsData,...)
   ***************************************************************************/
   /*!
   * \fn      NmeaEncoder(const trGPSData& rfcorGpsData,
   *             const trSensorData& rfcoSensorData)
   * \brief   Parameterized Constructor
   * \param   rfcorGpsData: GPS data
   * \param   rfcoSensorData: Sensor data
   * \param   rfcorVehicleData: vehicle Data
   * \sa      ~NmeaEncoder()
   ***************************************************************************/
   NmeaEncoder(const trGPSData& rfcorGpsData, const trSensorData& rfcoSensorData,
      const trVehicleData& rfcorVehicleData);

   /***************************************************************************
   ** FUNCTION: NmeaEncoder::~NmeaEncoder()
   ***************************************************************************/
   /*!
   * \fn       ~NmeaEncoder()
   * \brief   Destructor
   * \param   tVoid
   * \sa      NmeaEncoder()
   ***************************************************************************/
   virtual ~NmeaEncoder();

   /***************************************************************************
   ** FUNCTION: t_Void NmeaEncoder::vInitialise()
   ***************************************************************************/
   /*!
   * \fn      vInitialise()
   * \brief   Performs necessary internal static data initialisation.
   *          Can be called before object creation. Else, will be called the
   *          first time an object of this class is created.
   * \retval  t_Void
   ***************************************************************************/
   static t_Void vInitialise();

   /***************************************************************************
   ** FUNCTION:  t_String NmeaEncoder::szGetNmeaGGASentence(tenNmeaDataSource...
   ***************************************************************************/
   /*!
   * \fn      szGetNmeaGGASentence(tenNmeaDataSource enDataSource = e8NMEA_SOURCE_GPS)
   * \brief   Provides NMEA GGA encoded string
   * \param   enDataSource : [IN] NMEA data source identifier enum
   * \retval  t_String: NMEA GGA-format String
   ***************************************************************************/
   virtual t_String szGetNmeaGGASentence(tenNmeaDataSource enDataSource = e8NMEA_SOURCE_GPS) const;

   /***************************************************************************
   ** FUNCTION:  t_String NmeaEncoder::szGetNmeaRMCSentence(tenNmeaDataSource...
   ***************************************************************************/
   /*!
   * \fn      szGetNmeaRMCSentence(tenNmeaDataSource enDataSource = e8NMEA_SOURCE_GPS)
   * \brief   Provides NMEA RMC encoded string
   * \param   enDataSource : [IN] NMEA data source identifier enum
   * \retval  t_String: NMEA RMC-format String
   ***************************************************************************/
   virtual t_String szGetNmeaRMCSentence(tenNmeaDataSource enDataSource = e8NMEA_SOURCE_GPS) const;

   /***************************************************************************
   ** FUNCTION:  t_String NmeaEncoder::szGetNmeaPASCDSentence()
   ***************************************************************************/
   /*!
   * \fn      t_Bool szGetNmeaPASCDSentence())
   * \brief   Provides NMEA PASCD encoded string
   * \param   None
   * \retval  t_String: NMEA PASCD-format String
   ***************************************************************************/
   virtual t_String szGetNmeaPASCDSentence(t_Bool bUseGpsData = false) const;

   /***************************************************************************
   ** FUNCTION:  t_String NmeaEncoder::szGetNmeaPAGCDSentence()
   ***************************************************************************/
   /*!
   * \fn      t_Bool szGetNmeaPAGCDSentence())
   * \brief   Provides NMEA PAGCD encoded string
   * \param   None
   * \retval  t_String: NMEA PAGCD-format String
   ***************************************************************************/
   virtual t_String szGetNmeaPAGCDSentence(t_Bool bUseGpsData = false) const;

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION: t_Void NmeaEncoder::vAddDataToNmeaString()
   ***************************************************************************/
   /*!
   * \fn      vAddDataToNmeaString(t_String& rfszNmeaString ,
   *             const t_String& rfcoszData, t_Bool bIsLastDataInString,
   *             t_String szEndDelimiter = coszNmea_String_Delimiter)
   * \brief   Appends data in string format to the end of input NMEA string.
   *          If bIsLastDataInString = FALSE, it then also appends a Delimiter after
   *          the data. Else, does not append anything after data (since Delimiter
   *          is only required between individual data).
   * \param   rfszNmeaString  : [IO] NMEA string
   * \param   rfcoszData   : [IN] String data to be appended to NMEA string
   * \param   bIsLastDataInString: [IN] Flag to indicate if data is the last data
   *             in NEMA string
   * \param   szEndDelimiter : [IN] Delimiter to be added at the end of data
   * \retval  t_Void
   ***************************************************************************/
   t_Void vAddDataToNmeaString(t_String& rfszNmeaString ,
         const t_String& rfcoszData,
         t_Bool bIsLastDataInString,
         t_String szEndDelimiter = coszNmea_String_Delimiter) const;

   /***************************************************************************
   ** FUNCTION: t_Void NmeaEncoder::vInitialiseNmeaString()
   ***************************************************************************/
   /*!
   * \fn      vInitialiseNmeaString(t_String& rfszNmeaString ,
   *             const tSentenceIdentifier& rfcoSentenceIdentifier,
   *             tenNmeaDataSource enDataSource)
   * \brief   Initialises the NMEA string with NMEA format type data.
   *          Should be called before adding location data to NMEA string.
   * \param   rfszNmeaString  : [IO] NMEA string
   * \param   rfcoSentenceIdentifier : [IN] Unique sentence identifier
   *          (indicates the type of NMEA string being initialised. e.g, GGA, RMC, etc.)
   * \param   enDataSource: [IN] Identifies the source of NMEA data. e.g, GPS.
   * \retval  t_Void
   * \sa      vFinaliseNmeaString()
   ***************************************************************************/
   t_Void vInitialiseNmeaString(t_String& rfszNmeaString ,
         const tSentenceIdentifier& rfcoSentenceIdentifier,
         tenNmeaDataSource enDataSource) const;

   /***************************************************************************
   ** FUNCTION: t_Void NmeaEncoder::vInitialiseProprietaryNmeaString()
   ***************************************************************************/
   /*!
   * \fn      vInitialiseProprietaryNmeaString(t_String& rfszNmeaString ,
   *             tenNmeaSentenceType enNmeaSentenceType)
   * \brief   Initialises the NMEA string with NMEA format type data.
   *          Should be called before adding location data to NMEA string.
   * \param   rfszNmeaString  : [IO] NMEA string
   * \param   enNmeaSentenceType : [IN] Proprietary sentence identifier
   *          (PASCD, PAGCD, etc.)
   * \retval  t_Void
   * \sa      vFinaliseNmeaString()
   ***************************************************************************/
   t_Void vInitialiseProprietaryNmeaString(t_String& rfszNmeaString,
         tenNmeaSentenceType enNmeaSentenceType) const;

   /***************************************************************************
   ** FUNCTION: t_Void NmeaEncoder::vFinaliseNmeaString()
   ***************************************************************************/
   /*!
   * \fn      vFinaliseNmeaString(t_String& rfszNmeaString )
   * \brief   Performs final formatting of NMEA string.
   *          Should be called after all location data has been to the NEMA string.
   * \param   rfszNmeaString  : [IO] NMEA string
   * \retval  t_Void
   * \sa      vInitialiseNmeaString()
   ***************************************************************************/
   t_Void vFinaliseNmeaString(t_String& rfszNmeaString ) const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedTime(tenNmeaSentenceType 
                                              enNmeaSentenceType)
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedTime(tenNmeaSentenceType enNmeaSentenceType)
   * \brief   Provides Time info as UTC-formatted ("HHMMSS") string.
   * \param   bUseGpsData: Indicates whether Time info should be taken from GPS data
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedTime(t_Bool bUseGpsData) const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedDate()
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedDate()
   * \brief   Provides Date info as "DDMMYYYY" formatted string.
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedDate() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedLatLongInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedLatitudeInfo()
   * \brief   Provides Latitude & Direction (South/North) info as a formatted string.
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedLatitudeInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedLongitudeInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedLongitudeInfo()
   * \brief   Provides Longitude & Direction (East/West) info as a formatted string.
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedLongitudeInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedAltitudeInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedAltitudeInfo()
   * \brief   Provides Altitude & Units info as a formatted string.
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedAltitudeInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedSpeedInfo(t_Bool bUseGpsData)
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedSpeedInfo(t_Bool bUseGpsData)
   * \brief   Provides Speed info as a formatted string.
   * \param   bUseGpsData: Indicates whether Speed info should be taken from GPS data
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedSpeedInfo(t_Bool bUseGpsData) const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedHeadingInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedHeadingInfo()
   * \brief   Provides Heading info as a formatted string.
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedHeadingInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetLatitudeInDegreesMinutes()
   ***************************************************************************/
   /*!
   * \fn      szGetLatitudeInDegreesMinutes(const t_Double& rfcodLatInDecimalDegrees)
   * \brief   Converts a positive, Latitude value in decimal degrees to decimal minutes
   *          and provides it as a string.
   * \param   rfcodLatInDecimalDegrees: [IN] Latitude in decimal degrees
   * \retval  t_String
   ***************************************************************************/
   t_String szGetLatitudeInDegreesMinutes(const t_Double& rfcodLatInDecimalDegrees) const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetLongitudeInDegreesMinutes()
   ***************************************************************************/
   /*!
   * \fn      szGetLongitudeInDegreesMinutes(const t_Double& rfcodLongInDecimalDegrees)
   * \brief   Converts a positive, Longitude value in decimal degrees to decimal minutes
   *          and provides it as a string.
   * \param   rfcodLatInDecimalDegrees: [IN] Longitude in decimal degrees
   * \retval  t_String
   ***************************************************************************/
   t_String szGetLongitudeInDegreesMinutes(const t_Double& rfcodLongInDecimalDegrees) const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetGPSQualityIndicatorInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetGPSQualityIndicatorInfo()
   * \brief   Provides GPS Quality Indicator value as a string
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetGPSQualityIndicatorInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetSatellitesUsedInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetSatellitesUsedInfo()
   * \brief   Provides number of Satellites used value as a string
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetSatellitesUsedInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetHorizDilutionOfPrecisionInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetHorizDilutionOfPrecisionInfo()
   * \brief   Provides Horizontal dilution of precision value as a string
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetHorizDilutionOfPrecisionInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetGeoidalSeparationInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetGeoidalSeparationInfo()
   * \brief   Provides Geoidal Separation value as a string
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetGeoidalSeparationInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetDataStatusInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetDataStatusInfo()
   * \brief   Provides Data Status value as a string
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetDataStatusInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFAAModeIndicatorInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetFAAModeIndicatorInfo()
   * \brief   Provides FAA mode as a string
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFAAModeIndicatorInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedTransStateInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedTransStateInfo()
   * \brief   Provides Transmission State Info as a string
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedTransStateInfo() const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedSensorTypeInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedSensorTypeInfo()
   * \brief   Provides Sensor type Info as a string
   * \param   bUseGpsData: Indicates whether sensor type info should be taken from GPS data
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedSensorTypeInfo(t_Bool bUseGpsData)const;

   /***************************************************************************
   ** FUNCTION: t_String NmeaEncoder::szGetFormattedTurnRateInfo()
   ***************************************************************************/
   /*!
   * \fn      szGetFormattedTurnRateInfo()
   * \brief   Provides Turn rate Info as a string
   * \param   None
   * \retval  t_String
   ***************************************************************************/
   t_String szGetFormattedTurnRateInfo()const;

   /***************************************************************************
   ** Data members
   ***************************************************************************/

   /*
    * \brief   Structure holding superset of GPS data
    */
   trGPSData      m_rGpsData;

   /*
    * \brief   Structure holding superset of Sensor data
    */
   trSensorData   m_rSensorData;

   /*
   * \brief   Structure holding superset of Vehicle data
   */
   trVehicleData  m_rVehicleData;


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif  // _NMEAENCODER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
