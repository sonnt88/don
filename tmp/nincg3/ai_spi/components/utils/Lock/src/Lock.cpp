/***********************************************************************/
/*!
 * \file   Lock.cpp
 * \brief  mutex lock
 *************************************************************************
 \verbatim

    PROJECT:        Gen3
    SW-COMPONENT:   Smart Phone Integration
    DESCRIPTION:    TCL mutex handling
    AUTHOR:         ppa1kor
    COPYRIGHT:      &copy; RBEI

    HISTORY:
    Date        | Author                | Modification
    05.10.2013  | ppa1kor               | Initial Version
    09.10.2013  | Shihabudheen P M      | Updated
    15.07.2015  | Sameer Chandra        | Fixed Issues

 \endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/

#include "Lock.h"
#include <unistd.h>
#include <stdio.h>

/******************************************************************************
 | defines and macros (scope: module-local)
 |----------------------------------------------------------------------------*/

//To make sure that the mutex is not already owned by anyone
#define NO_OWNER (-1)

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/Lock.cpp.trc.h"
   #endif
#endif
/*************************************************************************
 ** FUNCTION:  Lock::Lock()
 *************************************************************************/
Lock::Lock()
{
   // TODO Auto-generated constructor stub
   m_ToValue = 30; // default time out value
   vInitIt();
   s16CreateIt();

}

/*************************************************************************
 ** FUNCTION:  Lock::~Lock()
 *************************************************************************/
Lock::~Lock()
{
   // Since the lock is exclusive called on a
   // muetx it is enough to just destroy the mutex here
   s16DeCreateIt();
}

/*************************************************************************
 ** FUNCTION:  void Lock::vInitIt()
 *************************************************************************/
void Lock::vInitIt()
{
   m_isInit = 0;
   m_mutex.__data.__owner = NO_OWNER;
}

/*************************************************************************
 ** FUNCTION:  int Lock::s16CreateIt()
 *************************************************************************/
int Lock::s16CreateIt()
{
   if (0 == m_isInit)
   {
      pthread_mutexattr_t mta;
      if(pthread_mutexattr_init(&mta) != 0)
      {
         ETG_TRACE_ERR(("\n Pthread_mutexattr_init error"));
      }
      else if(pthread_mutexattr_settype(&mta,
            PTHREAD_MUTEX_RECURSIVE) != 0)
      {
         ETG_TRACE_ERR(("\n Pthread_mutexattr_settype error"));
      }
      else  if(pthread_mutex_init(&m_mutex, &mta) != 0)
      {
         ETG_TRACE_ERR(("\n Pthread_mutex_init error"));
      }
      else
      {
         m_isInit = 1;
      }
   }
   return m_isInit;
}

/*************************************************************************
 ** FUNCTION:  int Lock::s16DeCreateIt()
 *************************************************************************/
int Lock::s16DeCreateIt()
{
   int res;
   res = pthread_mutex_destroy(&m_mutex);

   // set a defined value into the onwer to mark this mutex as released
   m_mutex.__data.__owner = NO_OWNER;

   return res;
}

/*************************************************************************
 ** FUNCTION:  int Lock::s16LockIt()
 *************************************************************************/
int Lock::s16LockIt()
{
   if (s16CreateIt())
   {
      struct timespec abs_time;
      int err;

      // if no valid mutex: return, dont lock
      if (m_mutex.__data.__owner == NO_OWNER)
         return -1;

      clock_gettime(CLOCK_REALTIME, &abs_time);
      abs_time.tv_sec += m_ToValue; // try it

      while (1)
      {
         if (!m_ToValue)
         {
            // lock the mutex
            err = pthread_mutex_lock(&m_mutex);
         }
         else
         {
            // Blocked until the lock is available. Expires after timeout.
            err = pthread_mutex_timedlock(&m_mutex, &abs_time);
         }
         // Continueue in loop until get a lock
         if (err == -1 && errno == EINTR)
            continue;
         break;
      }
      if (err != 0)
      {
         //fprintf(stderr, "pthread_mutex_lock: err=%d, errno=%d/%s\n", err, errno, strerror(errno));
         //MP_NORMAL_ASSERT(err == 0);
         ETG_TRACE_ERR(("\n Pthread_mutex_lock error= %d, errorno=%d\n",err,errno));
         return -1;
      }
   }

   return 0;
}

/*************************************************************************
 ** FUNCTION:  void Lock::vUnlockIt()
 *************************************************************************/
void Lock::vUnlockIt()
{
   if (s16CreateIt())
   {
      // if no valid mutex: return, dont unlock
      if (m_mutex.__data.__owner == NO_OWNER)
         return;

      pthread_mutex_unlock(&m_mutex);
   }
}

/*************************************************************************
 ** FUNCTION:  int Lock::s16Lock()
 *************************************************************************/
int Lock::s16Lock()
{
   return s16LockIt();
}

/*************************************************************************
 ** FUNCTION:  void Lock::vUnlock()
 *************************************************************************/
void Lock::vUnlock()
{
   vUnlockIt();
}

/*************************************************************************
 ** FUNCTION:  Lock::s16SetNotReantrant()
 *************************************************************************/
int Lock::s16SetNotReantrant()
{
   pthread_mutexattr_t mta;
   pthread_mutexattr_init(&mta);
   return pthread_mutex_init(&m_mutex, &mta);
}

/*************************************************************************
 ** void Lock::vSetTimeout(const unsigned int sec)
 *************************************************************************/
void Lock::vSetTimeout(const unsigned int sec)
{
   m_ToValue = sec;
}

