#ifndef _GENERICSINGLETON_H
#define _GENERICSINGLETON_H

/***********************************************************************/
/*!
* \file  GenericSingleton.h.h
* \brief String Utitlity
*************************************************************************
\verbatim

PROJECT:         Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Generic Singleton class implementation
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version
12.11.2013  | Deepti Samant         | Added function getInstance(C* val)
08.01.2014  | Deepti Samant         | Added function getInstance(C* val1, M* val2)
10.12.2015  | Shiva Kumar Gurija    | Thread safety is implemented

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <cstddef>
#include "Lock.h"

/****************************************************************************/
/*!
* \class GenericSingleton
* \brief Generic Singleton class implementation
****************************************************************************/
template <typename T>
class GenericSingleton
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  T* GenericSingleton::getInstance()
   ***************************************************************************/
   /*!
   * \fn      static T* getInstance()
   * \brief   Method to get the instance of the singleton class
   * \param   T*
   * \sa
   ***************************************************************************/
   static T* getInstance()
   {
      if (NULL == m_instance)
      {
         //Two threads can call getInstance() method at the same time.
         //In such cases, to prevent creating object twice, make it thread safe.

         //Additional NULL check was kept above, to avoid entering critical section every time,
         //whenever clients does a getInstance(). With this approach we avoid entering into critical section always
         //and avoid delay in acquiring the object. 
         m_sLock.s16Lock();
         if(NULL == m_instance)
         {
            m_instance = new T;
         }//if (NULL == m_instance)
         m_sLock.vUnlock();

      }//if (NULL == m_instance)
      return m_instance;
   }

   /***************************************************************************
   ** FUNCTION:  template <class C> static T* getInstance(C* val)
   ***************************************************************************/
   /*!
   * \fn      template <class C> static T* getInstance(C* val)
   * \brief   Method to get the instance of the singleton class which takes
   *		  one parameter as an argument
   * \param   Template class object
   * \sa
   ***************************************************************************/
   template <class C> 
   static T* getInstance(C* val)
   {
      if (NULL == m_instance)
      {
         //Two threads can call getInstance() method at the same time.
         //In such cases, to prevent creating object twice, make it thread safe.

         //Additional NULL check was kept above, to avoid entering critical section every time,
         //whenever clients does a getInstance(). With this approach we avoid entering into critical section always
         //and avoid delay in acquiring the object. 
         m_sLock.s16Lock();
         if(NULL == m_instance)
         {
            m_instance = new T(val);
         }//if (NULL == m_instance)
         m_sLock.vUnlock();

      }//if (NULL == m_instance)
      return m_instance;
   }

   /***************************************************************************
   ** FUNCTION:  template <class C, class M> static T* getInstance(C* val1, M* val2)
   ***************************************************************************/
   /*!
   * \fn      template <class C, class M> static T* getInstance(C* val1, M* val2)
   * \brief   Method to get the instance of the singleton class which takes
   *		  two parameters as arguments.
   * \param   Template class object
   * \sa
   ***************************************************************************/
   template <class C, class M> 
   static T* getInstance(C* val1, M* val2)
   {
      if (NULL == m_instance)
      {
         //Two threads can call getInstance() method at the same time.
         //In such cases, to prevent creating object twice, make it thread safe.

         //Additional NULL check was kept above, to avoid entering critical section every time,
         //whenever clients does a getInstance(). With this approach we avoid entering into critical section always
         //and avoid delay in acquiring the object. 
         m_sLock.s16Lock();
         if(NULL == m_instance)
         {
            m_instance = new T(val1, val2);
         }//if (NULL == m_instance)
         m_sLock.vUnlock();

      }//if (NULL == m_instance)
      return m_instance;
   }
   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  GenericSingleton::GenericSingleton()
   ***************************************************************************/
   /*!
   * \fn      GenericSingleton()
   * \brief   Constructor
   * \param   tVoid
   * \sa      ~GenericSingleton()
   ***************************************************************************/
   GenericSingleton()
   {
   }

   /***************************************************************************
   ** FUNCTION: virtual GenericSingleton::~GenericSingleton()
   ***************************************************************************/
   /*!
   * \fn      virtual ~GenericSingleton()
   * \brief   Destructor
   * \param   tVoid
   * \sa      GenericSingleton()
   ***************************************************************************/
   virtual ~GenericSingleton()
   {
      m_instance = NULL;
   }

   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   //! Static instance of the class
   static T* m_instance;

   static Lock m_sLock;

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

}; //class GenericSingleton

template <class T> T* GenericSingleton <T>::m_instance = NULL;
template <class T> Lock GenericSingleton<T>::m_sLock;


#endif // _GENERICSINGLETON_H

///////////////////////////////////////////////////////////////////////////////
// <EOF>
