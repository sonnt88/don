/***********************************************************************/
/*!
 * \file  XmlReader.cpp
 * \brief Generic xml paarser based on libxml
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Xml parser
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      14.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <iostream>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xinclude.h>
#include <libxml/xmlIO.h>
#include "Xmlable.h"
#include "XmlDocument.h"
#include "XmlReader.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/XmlReader.cpp.trc.h"
   #endif
#endif

namespace shl
{
   namespace xml
   {
      /******************************************************************************
      ** FUNCTION:  tclXmlReader::tclXmlReader(tclXmlDocument *const cpoXmlDocument,..
      ******************************************************************************/
      tclXmlReader::tclXmlReader(tclXmlDocument * const cpoXmlDocument,
            tclXmlable * const cpoXmlable) :
            m_cpoXmlDocument(cpoXmlDocument), m_cpoXmlable(cpoXmlable)
      {
      }

      /******************************************************************************
      ** FUNCTION:  tclXmlReader::~tclXmlReader()
      ******************************************************************************/
      tclXmlReader::~tclXmlReader()
      {
      }

      /******************************************************************************
      ** FUNCTION:  bool tclXmlReader::bRead(string sStartNode, const xmlAttr ...
      ******************************************************************************/
      bool tclXmlReader::bRead(string sStartNode,
            const xmlAttr * poXmlAttr) const
      {
         bool bRetVal = false;
         xmlNodePtr poXmlNode = NULL;
         if ((NULL != m_cpoXmlDocument) && (NULL != m_cpoXmlable))
         {
            // check for document validity
            bRetVal = m_cpoXmlDocument->bIsValid();

            bRetVal = (bRetVal)? (m_cpoXmlDocument->bGetFirstChild(sStartNode, poXmlNode,
                  poXmlAttr)) : false;

            // code to avoid the spaces in xml file
            while ((bRetVal) && (NULL != poXmlNode) && (0 == xmlStrcmp(poXmlNode->name, (xmlChar*)const_cast<char *>("text"))))
            {
               bRetVal = (bRetVal) ? m_cpoXmlDocument->bGetNextSibling(poXmlNode,
                     poXmlNode):false;
            }

            bRetVal = (bRetVal)? m_cpoXmlable->bXmlReadNode(poXmlNode) : false;

            while (true == bRetVal)
            {
               // get the next sibling
               bRetVal = m_cpoXmlDocument->bGetNextSibling(poXmlNode,
                     poXmlNode);
               if ((NULL != poXmlNode) && (true == bRetVal))
               {
                  // call the virtual function to handle the node
                  bRetVal = m_cpoXmlable->bXmlReadNode(poXmlNode);
               }
               else
               {
                  ETG_TRACE_ERR(
                        ("\nGetNextSibling return NULL pointer or false value \n"));
               }
            } //while(true == bRetVal)
         } // if (NULL != m_cpoXmlDocument && NULL != m_cpoXmlable)
         else
         {
            ETG_TRACE_ERR((" \nXml document is not valid"));
         }
         return bRetVal;
      }
   } // xml
} // shl
