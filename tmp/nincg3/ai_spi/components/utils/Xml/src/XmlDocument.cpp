/***********************************************************************/
/*!
 * \file  XmlDocument.cpp
 * \brief Generic xml paarser based on libxml
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Xml parser
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      14.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <cstdio>
#include "XmlDocument.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/XmlDocument.cpp.trc.h"
   #endif
#endif

using namespace std;

namespace shl
{
   namespace xml
   {
      /******************************************************************************
      ** FUNCTION:  tclXmlDocument::tclXmlDocument(string sXmlDoc, string sRootNode)
      ******************************************************************************/
      tclXmlDocument::tclXmlDocument(string sXmlDoc)
      {
         // create the parser
         m_poXmlDoc = xmlParseFile(sXmlDoc.c_str());
      }

      /******************************************************************************
      ** FUNCTION:  tclXmlDocument::~tclXmlDocument()
      ******************************************************************************/
      tclXmlDocument::~tclXmlDocument()
      {
         bClose();
      }

      /******************************************************************************
      ** FUNCTION:  bool tclXmlDocument::bIsValid() const
      ******************************************************************************/
      bool tclXmlDocument::bIsValid() const
      {
         return (NULL != m_poXmlDoc);
      }

      /******************************************************************************
      ** FUNCTION:   bool tclXmlDocument::bLoad(string sXmlDoc, string sRootNode)
      ******************************************************************************/
      bool tclXmlDocument::bLoad(string sXmlDoc, string sRootNode)
      {
         // load the xml document
         m_poXmlDoc = xmlParseFile((const char*) sXmlDoc.c_str());
         if(NULL != m_poXmlDoc)
         {
           xmlNodePtr node = poSearchElement(xmlDocGetRootElement(m_poXmlDoc),sRootNode);
           if (NULL == xmlDocSetRootElement(m_poXmlDoc,node))
           {
              ETG_TRACE_ERR(("Root node not located successfully.\n"));
           }
           ETG_TRACE_USR2(("The xml document loaded.\n"));
         }
         else
         {
            ETG_TRACE_ERR(("Xml document is not loaded successfully \n"));
         }
         return (NULL != m_poXmlDoc);
      }

      /******************************************************************************
      ** FUNCTION:  int tclXmlDocument::iGetNodeCount(string sTagName)
      ******************************************************************************/
      // TODO Revisit the search
      int tclXmlDocument::s16GetNodeCount(string sTagName)
      {
         xmlNodePtr poXmlNode = NULL;
         int nodeCount = 0;
         if (true == bIsValid())
         {
            poXmlNode = poSearchElement(xmlDocGetRootElement(m_poXmlDoc),sTagName);
            if (NULL != poXmlNode)
            {
               // Starts with childrens
               poXmlNode = poXmlNode->children;
               while (NULL != poXmlNode)
               {
                  ++nodeCount;
                  poXmlNode = poXmlNode->next;
               }
            }
            else
            {
               nodeCount = -1;
               ETG_TRACE_ERR(("The xml search operation return NULL \n"));
            }
         }
         else
         {
            nodeCount = -1;
            ETG_TRACE_ERR(("The xml document parser return an error \n"));
         }
         return nodeCount;
      }

      /******************************************************************************
      ** FUNCTION:  bool tclXmlDocument::bFindNode(const string sTagName, ..
      ******************************************************************************/
      bool tclXmlDocument::bFindNode(const string sTagName,
            const xmlAttr *cpoXmlAttr, xmlNodePtr &rfcoXmlNode)
      {
         bool bRetVal = false;
         if (true == bIsValid())
         {
            rfcoXmlNode = poSearchElement(xmlDocGetRootElement(m_poXmlDoc),
                  sTagName, cpoXmlAttr);
            bRetVal = (NULL != rfcoXmlNode);
         }
         else
         {
            ETG_TRACE_ERR(("The xml document parser return an error \n"));
         }
         return bRetVal;

      }

      /******************************************************************************
      ** FUNCTION:  bool tclXmlDocument::bGetFirstChild(string sRootTag,...
      ******************************************************************************/
      bool tclXmlDocument::bGetFirstChild(string sRootTag,
            xmlNodePtr &rfoXmlNode, const xmlAttr * poXmlAttr)
      {
         xmlNodePtr node = NULL;
         bool bRetVal = false;
         if (true == bIsValid())
         {
            // Search the element
            node = poSearchElement(xmlDocGetRootElement(m_poXmlDoc), sRootTag,
                  poXmlAttr);
            if (NULL != node)
            {
               rfoXmlNode = node->children;
               bRetVal = true;
            }
            else
            {
               rfoXmlNode = NULL;
               ETG_TRACE_ERR(("The node is NULL \n"));
            }
         }
         else
         {
            ETG_TRACE_ERR(("The document parser return an error. \n"));
         }
         return bRetVal;
      }

      /******************************************************************************
      ** FUNCTION:  bool tclXmlDocument::bGetFirstChild(xmlNodePtr &rfoXmlNode)
      ******************************************************************************/
      bool tclXmlDocument::bGetFirstChild(xmlNodePtr &rfoXmlNode)
      {
         if(true == bIsValid())
         {
            rfoXmlNode = xmlDocGetRootElement(m_poXmlDoc);
         }
         else
         {
            ETG_TRACE_ERR(("The Xml parser is not ready for operation \n"));
         }
         return (NULL != rfoXmlNode);
      }

      /******************************************************************************
      ** FUNCTION:  bool tclXmlDocument::bGetNextSibling(xmlNodePtr poxmlNode,...
      ******************************************************************************/
      bool tclXmlDocument::bGetNextSibling(xmlNodePtr poxmlNode,xmlNodePtr &rfoXmlNode)const
      {
         if (true == bIsValid())
         {
            rfoXmlNode = xmlNextElementSibling(poxmlNode);
         }
         else
         {
            ETG_TRACE_ERR(("The Xml parser is not ready for operation \n"));
         }
         return (NULL != rfoXmlNode);
      }

      /******************************************************************************
      ** FUNCTION:  bool tclXmlDocument::bCloseNode(int iDepth)
      ******************************************************************************/
      bool tclXmlDocument::bCloseNode(xmlNodePtr poXmlNode, int iDepth,
            xmlNodePtr &poXmlParentNode)
      {
         bool bRetVal =false;
         while (iDepth > 0)
         {
            if (NULL != poXmlNode)
            {
               poXmlParentNode = poXmlNode->parent;
               bRetVal = true;
            }
            if (poXmlNode == xmlDocGetRootElement(m_poXmlDoc))
            {
               poXmlParentNode = xmlDocGetRootElement(m_poXmlDoc);
               break;
            }
         }
         return bRetVal;
      }

      /******************************************************************************
      ** FUNCTION:  bool tclXmlDocument::bClose()
      ******************************************************************************/
      bool tclXmlDocument::bClose()
      {
         if(true == bIsValid())
         {
            xmlFreeDoc(m_poXmlDoc);
            m_poXmlDoc = NULL;
         }
         return (false == bIsValid());
      }

      /******************************************************************************
      ** FUNCTION:  xmlNodePtr tclXmlDocument::poSearchElement(xmlNodePtr poNode, ...
      ******************************************************************************/
      xmlNodePtr tclXmlDocument::poSearchElement(xmlNodePtr poNode,
            string sTagName, const xmlAttr *poXmlAttr)
      {
         xmlNodePtr node = NULL;
         if (NULL != poNode)
         {
            // Check for the match with node name
            if (0 == xmlStrcmp(poNode->name, (xmlChar *)const_cast<char *>(sTagName.c_str())))
            {
               node = poNode;
            }
            // Traverse to the child of the node to continue search
            else if (NULL != poNode->children)
            {
               node = poSearchElement(poNode->children, sTagName, poXmlAttr);
            }
            // Traverse to the sibling to continue search, if there is no child
            else if (NULL != poNode->next)
            {
               node = poSearchElement(poNode->next, sTagName, poXmlAttr);
            }
            // Traverse to the parent, if child and sibling node is not present(Backtrack)
            else if (NULL != poNode->parent)
            {
              // Code to locate a parent, which has a sibling to proceed search (Backtrack)
               while((NULL == poNode->next) && (NULL != poNode->parent))
               {
                  poNode = poNode->parent;
               }
               if(NULL != poNode->next)
               {
                  node = poSearchElement(poNode->next, sTagName, poXmlAttr);
               }
            }
         }
         return node;
      } // poSearchElement(xmlNodePtr poNode, string sTagName )
   } // end of xml
} // end of shl


