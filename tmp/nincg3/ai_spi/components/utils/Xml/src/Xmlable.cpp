/***********************************************************************/
/*!
 * \file  Xmlable.cpp
 * \brief Generic Xml parser based on libxml
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Xml parser
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      18.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim 
 *************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <iostream>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xinclude.h>
#include <libxml/xmlIO.h>
#include <libxml/xmlerror.h>
#include "Xmlable.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/Xmlable.cpp.trc.h"
   #endif
#endif
/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

namespace shl
{
   namespace xml
   {

      /*************************************************************************
      ** FUNCTION:  tclXmlable::tclXmlable()
      *************************************************************************/
      tclXmlable::tclXmlable()
      {
      }

      /*************************************************************************
       ** FUNCTION:  bool tclXmlReadable::bGetAttribute(string sAttribName,...
       *************************************************************************/
      bool tclXmlReadable::bGetAttribute(string sAttribName,
            const xmlNodePtr poNode, int &iValue)const
      {
         string sValue;
         // Get the attribute value from xml
         bool bRetVal = bGetAttribute(sAttribName, poNode, sValue);
         if (true == bRetVal)
         {
            // Convert string to integer
            iValue = atoi(sValue.c_str());
            ETG_TRACE_USR2(("bGetAttribute returned with %d value", iValue));
         }
         else
         {
            ETG_TRACE_ERR(("Get Attribute function return an error"));
         }
         return bRetVal;
      }

      /*************************************************************************
       ** FUNCTION:  bool tclXmlReadable::bGetAttribute(string sAttribName, ...
       *************************************************************************/
      bool tclXmlReadable::bGetAttribute(string sAttribName,
                  const xmlNodePtr poNode, bool &bValue)const
      {
         bool bRetVal =false;
         if(NULL != poNode)
         {
            // Get property value from xml
            xmlChar * value = (xmlGetProp(poNode,
                           (xmlChar *)const_cast<char *>(sAttribName.c_str())));
            if ((0 == xmlStrcmp(value, (xmlChar*)const_cast<char *>("true")))
                  || (0 == xmlStrcmp(value, (xmlChar*)const_cast<char *>("TRUE")))
                  || (0 == xmlStrcmp(value, (xmlChar*)const_cast<char *>("True"))))
            {
               bRetVal = true;
               bValue = true;
            }
            else if ((0 == xmlStrcmp(value, (xmlChar*)const_cast<char *>("false")))
                     || (0 == xmlStrcmp(value, (xmlChar*)const_cast<char *>("FALSE")))
                     || (0 == xmlStrcmp(value, (xmlChar*)const_cast<char *>("False"))))
            {
               bRetVal = true;
               bValue = false;
            }
            else
            {
               ETG_TRACE_USR2(("\nNo boolean parameter existed"));
            }
            xmlFree(value);
            ETG_TRACE_USR2(("\nbGetAttribute returned with %d value", bValue));
         }
         else
         {
            ETG_TRACE_ERR(("\nNode pointer is NULL"));
         }

         return bRetVal;
      }

      /*************************************************************************
       ** FUNCTION:  bool tclXmlReadable::bGetAttribute(string sAttribName, ...
       *************************************************************************/
      bool tclXmlReadable::bGetAttribute(string sAttribName, const xmlNodePtr poNode,
            string &sValue)const
      {
         bool bRetVal = false;
         if (NULL != poNode)
         {
            // Get the property value from xml
            xmlChar *value = xmlGetProp(poNode,
                  (xmlChar *)const_cast<char *>(sAttribName.c_str()));

            // Convert the value to string
            if ( NULL != value)
            {
             sValue = (string) ((const char *) value);
            }
            xmlFree(value);
            bRetVal = (!sValue.empty()) ? true : false;
            ETG_TRACE_USR2(("bGetAttribute returned with %s value", sValue.c_str()));
         }
         else
         {
            ETG_TRACE_ERR(("Node pointer is NULL"));
         }
         return bRetVal;
      }
   }// end of xml
} // end of shl

