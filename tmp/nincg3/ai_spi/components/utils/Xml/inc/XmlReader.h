#ifndef XMLREADER_H_
#define XMLREADER_H_

/***********************************************************************/
/*!
 * \file  XmlReader.h
 * \brief Generic xml reader based on libxml
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Xml Parser
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      14.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/
#include <string>
/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/
// forward declaration
struct _xmlDoc;
typedef struct _xmlDoc xmlDoc;
typedef xmlDoc *xmlDocPtr;
// forward declaration
struct _xmlNode;
typedef struct _xmlNode xmlNode;
typedef xmlNode *xmlNodePtr;
// forward declarations
struct _xmlAttr;
typedef struct _xmlAttr xmlAttr;

using namespace std;

namespace shl
{
   namespace xml
   {
      // forward declaration
      class tclXmlDocument;

      // forward declaration
      class tclXmlable;

      /****************************************************************************/
      /*!
       * \class tclXmlReader
       * \brief Generic xml reader
       *
       * This is implemented based on the libxml library. This class provides
       * some functionality to read the nodes from xml files.
       *
       ***************************************************************************/
      class tclXmlReader
      {
         public:

            /***************************************************************************
            *********************************PUBLIC*************************************
            ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  tclXmlReader::tclXmlReader(tclXmlDocument * const  ...)
             *************************************************************************/
             /*!
             * \fn    tclXmlReader(tclXmlDocument * const cpoXmlDocument, tclXmlable .)
             * \brief Constructor based on dependency inversion principle.
             * \sa    virtual ~tclXmlable()
             *************************************************************************/
            tclXmlReader(tclXmlDocument *const cpoXmlDocument, tclXmlable *const cpoXmlable);

            /*************************************************************************
             ** FUNCTION:  tclXmlReader::~tclXmlReader()
             *************************************************************************/
             /*!
             * \fn    virtual ~tclXmlReader()
             * \brief Destructor
             * \sa    tclXmlable()
             *************************************************************************/
            virtual ~tclXmlReader();

            /*************************************************************************
             ** FUNCTION:  virtual bool tclXmlReader::~bRead(string sStartNode,  ..)
             *************************************************************************/
             /*!
             * \fn     virtual bool bRead(string sStartNode, const xmlAttr ...)
             * \brief  Function to read the xml file
             * \param  sStartNode : [IN] start node to begin read process.
             * \param  poXmlAttr : [IN] List of attributes
             * \retval bool : true if success, false otherwise.
             *************************************************************************/
            virtual bool bRead(string sStartNode, const xmlAttr * poXmlAttr = NULL ) const;

            /***************************************************************************
            ****************************END OF PUBLIC***********************************
            ***************************************************************************/

         protected:

            /***************************************************************************
            *******************************PROTECTED************************************
            ***************************************************************************/

            /***************************************************************************
            ****************************END OF PROTECTED********************************
            ***************************************************************************/

         private:

            /***************************************************************************
            *********************************PRIVATE************************************
            ***************************************************************************/

             // Xml Document pointer
            tclXmlDocument* const m_cpoXmlDocument;

            // Xml readable pointer
            tclXmlable* const m_cpoXmlable;

            /***************************************************************************
            ****************************END OF PRIVATE**********************************
            ***************************************************************************/
      };

   }
}

#endif /* XMLREADER_H_ */
