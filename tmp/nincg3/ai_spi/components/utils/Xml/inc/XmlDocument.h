#ifndef XMLDOCUMENT_H_
#define XMLDOCUMENT_H_
/***********************************************************************/
/*!
 * \file  XmlDocument.h
 * \brief Generic xml paarser based on libxml
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Xml parser
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      14.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/


/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/

#include <string>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xinclude.h>
#include <libxml/xmlIO.h>
/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/
// forward declarations
struct _xmlDoc;
typedef struct _xmlDoc xmlDoc;
typedef xmlDoc *xmlDocPtr;
// forward declarations
struct _xmlNode;
typedef struct _xmlNode xmlNode;
typedef xmlNode *xmlNodePtr;
// forward declarations
struct _xmlAttr;
typedef struct _xmlAttr xmlAttr;

using namespace std;

namespace shl
{
   namespace xml
   {

      /****************************************************************************/
      /*!
       * \class tclXmlDocument
       * \brief Generic xml paarser
       *
       * This is implemented based on the libxml library. This class provides
       * some functionality to deal with xml document related operations.
       *
       ***************************************************************************/
      class tclXmlDocument
      {
         public:
            /***************************************************************************
            *********************************PUBLIC*************************************
            ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  tclXmlDocument::tclXmlDocument(string sXmlDoc, string sRootNode)
             *************************************************************************/
            /*!
             * \fn    tclXmlable()
             * \brief Constructor
             * \param sXmlDoc : [IN] Path to the xml document
             * \param sRootNode :[IN] Label of the root node
             * \sa    virtual ~tclXmlDocument()
             *************************************************************************/
            tclXmlDocument(string sXmlDoc);

            /*************************************************************************
             ** FUNCTION:  virtual tclXmlDocument::~tclXmlDocument()
             *************************************************************************/
            /*!
             * \fn    tclXmlable()
             * \brief Destructor
             * \sa    tclXmlDocument()
             *************************************************************************/
            virtual ~tclXmlDocument();

            /*************************************************************************
             ** FUNCTION:  bool tclXmlDocument::bIsValid() const
             *************************************************************************/
            /*!
             * \fn     bool bIsValid() const
             * \brief  Check the xml parser loaded
             * \retval bool : True if valid,false otherwise
             *************************************************************************/
            bool bIsValid() const;

            /*************************************************************************
             ** FUNCTION:  bool tclXmlDocument::bLoad(string sXmlDoc, string sRootNode)
             *************************************************************************/
            /*!
             * \fn     bool bLoad(string sXmlDoc, string sRootNode)
             * \brief  Load the xml document with specific node as root
             * \param  sXmlDox : [IN] Path to the xml document
             * \param  sRootNode: [IN] Name of the node to set as root node
             * \retval bool : true if success, false otherwise
             *************************************************************************/
            bool bLoad(string sXmlDoc, string sRootNode);

            /*************************************************************************
             ** FUNCTION:  bool tclXmlDocument::bClose()
             *************************************************************************/
            /*!
             * \fn     bool bClose();
             * \brief  Clean up all the resources allocated
             * \retval bool : true if success, false otherwise
             *************************************************************************/
            bool bClose();

            /*************************************************************************
             ** FUNCTION:  bool tclXmlDocument::bSave()
             *************************************************************************/
            /*!
             * \fn     bool bSave()
             * \brief  Save xml document
             * \retval bool : true if success, false otherwise
             * \note   Save is not supported at present
             *************************************************************************/
            bool bSave(){return false;} ;

            /*************************************************************************
             ** FUNCTION:  int tclXmlDocument::iGetNodeCount(string sTagName)
             *************************************************************************/
            /*!
             * \fn     int iGetNodeCount(string sTagName)
             * \brief  Node count
             * \param  sTagName : [IN] Label of the node
             * \retval int : Number of nodes
             *************************************************************************/
            int s16GetNodeCount(string sTagName);

            /*************************************************************************
             ** FUNCTION:  virtual tclXmlDocument::~tclXmlDocument()
             *************************************************************************/
            /*!
             * \fn    tclXmlable()
             * \brief Destructor
             * \sa    tclXmlDocument()
             *************************************************************************/
            bool bGetFirstChild(string sRootTag, xmlNodePtr &rfoXmlNode,
                  const xmlAttr * poXmlAttr = NULL);

            /*************************************************************************
             ** FUNCTION:  bool tclXmlDocument::bGetFirstChild(xmlNodePtr &rfoXmlNode)
             *************************************************************************/
            /*!
             * \fn     bool bGetFirstChild(xmlNodePtr &rfoXmlNode)
             * \brief  Get first child
             * \param  rfoXmlNode : [OUT] Pointer to the child
             * \retval bool : true if success, false otherwise
             *************************************************************************/
            bool bGetFirstChild(xmlNodePtr &rfoXmlNode);

            /*************************************************************************
             ** FUNCTION:  bool tclXmlDocument::bGetNextSibling(xmlNodePtr poxmlNode,..)
             *************************************************************************/
            /*!
             * \fn     bool bGetNextSibling(xmlNodePtr poxmlNode ,xmlNodePtr &rfoXmlNode)
             * \brief  Locate next sibling
             * \param  poxmlNode : [IN] Pointer to the node
             * \param  rfoXmlNode : [OUT] reference to the resultant node
             * \retval bool : true if success, false otherwise
             *************************************************************************/
            bool bGetNextSibling(xmlNodePtr poxmlNode ,xmlNodePtr &rfoXmlNode)const;

            /*************************************************************************
             ** FUNCTION:  bool tclXmlDocument::bFindNode(const string sTagName, ...)
             *************************************************************************/
            /*!
             * \fn     bool bFindNode(const string sTagName, const xmlAttr *cpoXmlAttr,..)
             * \brief  Search to find a node
             * \param  sTagName : [IN] String to search
             * \param  cpoXmlAttr : [IN] Attribute list
             * \param  rfcoXmlNode : [OUT] Node pointer
             * \retval bool : true if success, false otherwise
             *************************************************************************/
            bool bFindNode(const string sTagName, const xmlAttr *cpoXmlAttr,
                  xmlNodePtr &rfcoXmlNode);

            /*************************************************************************
             ** FUNCTION:  bool tclXmlDocument::bCloseNode(xmlNodePtr poXmlNode, ...)
             *************************************************************************/
            /*!
             * \fn     bool bCloseNode(xmlNodePtr poXmlNode, int iDepth, xmlNodePtr...
             * \brief  Close the node level and come up to above level
             * \param  poXmlNode : [IN] Node pointer
             * \param  iDepth : [IN] Number of level to close
             * \param
             *************************************************************************/
            bool bCloseNode(xmlNodePtr poXmlNode, int iDepth, xmlNodePtr &poXmlParentNode);

            /***************************************************************************
            ****************************END OF PUBLIC***********************************
            ***************************************************************************/
         protected:
            /***************************************************************************
            *******************************PROTECTED************************************
            ***************************************************************************/
            // xml document pointer
            xmlDocPtr m_poXmlDoc;

            // xml error handler
           // xmlError m_XmlError;
            /***************************************************************************
            ****************************END OF PROTECTED********************************
            ***************************************************************************/
         private:

            /***************************************************************************
            *********************************PRIVATE************************************
            ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  xmlNodePtr tclXmlDocument::poSearchElement(xmlNodePtr poNode,...)
             *************************************************************************/
            /*!
             * \fn     xmlNodePtr poSearchElement(xmlNodePtr poNode, string string)
             * \brief  To search the element node
             * \param  poNode : [IN] Node pointer
             * \param  string : [IN] return value
             * \retval xmlNodePtr : node pointer of the corresponding result
             *************************************************************************/
            xmlNodePtr poSearchElement(xmlNodePtr poNode, string string, const xmlAttr *poXmlAttr = NULL);
            /***************************************************************************
            ****************************END OF PRIVATE**********************************
            ***************************************************************************/
      };
   }
}

#endif /* XMLDOCUMENT_H_ */
