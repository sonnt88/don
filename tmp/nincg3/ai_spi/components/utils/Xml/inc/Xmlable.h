#ifndef XMLABLE_H_
#define XMLABLE_H_
/***********************************************************************/
/*!
 * \file  Xmlable.h
 * \brief Generic xml paarser based on libxml
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Xml Parser
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      14.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/
#include <string>

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/
// Forward declaration of xmlNodePtr
struct _xmlNode;
typedef struct _xmlNode xmlNode;
typedef xmlNode *xmlNodePtr;

using namespace std;

namespace shl
{
   namespace xml
   {
      /****************************************************************************/
      /*!
       * \class tclXmlable
       * \brief Generic xml paarser
       *
       * This is implemented based on the libxml library. This act as a basic class
       * to implement the generic xml parser, and also provide functions which
       * should be implemented by inheriting classes.
       *
       ***************************************************************************/

      class tclXmlable
      {
         public:
            /*************************************************************************
             *********************************PUBLIC**********************************
             *************************************************************************/

            /*************************************************************************
             ** FUNCTION:  tclXmlable::tclXmlable()
             *************************************************************************/
            /*!
             * \fn    tclXmlable()
             * \brief Constructor
             * \sa    virtual ~tclXmlable()
             *************************************************************************/
            tclXmlable();

            /*************************************************************************
             ** FUNCTION:  virtual tclXmlable::~tclXmlable()
             *************************************************************************/
            /*!
             * \fn    virtual ~tclXmlable()
             * \brief Destructor
             * \sa    tclXmlable()
             *************************************************************************/
            virtual ~tclXmlable() {};

            /*************************************************************************
             ** FUNCTION:  virtual bool tclXmlable::bXmlReadNode(xmlNode *poNode)
             *************************************************************************/
            /*!
             * \fn     virtual bool bXmlReadNode(xmlNode *poNode)
             * \brief  virtual function to read data from a xml node
             * \param  poNode : [IN] pointer to xml node
             * \retval bool : true if success, false otherwise.
             * \note   User has to implement the functio to get xml parser functionality
             * \sa     bXmlWriteNode
             *************************************************************************/
            virtual bool bXmlReadNode(xmlNodePtr poNode) =0;

            /*************************************************************************
             ** FUNCTION:  virtual bool tclXmlable::bXmlWriteNode(xmlNodePtr poNode)
             *************************************************************************/
            /*!
             * \fn     virtual bool bXmlWriteNode(xmlNodePtr poNode)
             * \brief  virtual function to write data to a xml node
             * \param  poNode : [IN] pointer to xml node
             * \retval bool : true if success, false otherwise.
             * \sa     bXmlReadNode
             *************************************************************************/

            virtual bool bXmlWriteNode(xmlNodePtr poNode) = 0;

            /***************************************************************************
             ****************************END OF PUBLIC***********************************
             ***************************************************************************/

         protected:

            /***************************************************************************
             *******************************PROTECTED************************************
             ***************************************************************************/

            /***************************************************************************
             ****************************END OF PROTECTED********************************
             ***************************************************************************/

         private:

            /***************************************************************************
             *********************************PRIVATE************************************
             ***************************************************************************/

            /***************************************************************************
             ****************************END OF PRIVATE**********************************
             ***************************************************************************/
      }; // class tclXmlable

      /****************************************************************************/
      /*!
       * \class tclXmlReadable
       * \brief Generic xml paarser
       *
       * This is implemented based on the libxml library. This act as a basic class
       * to implement the generic xml document reader
       *
       ***************************************************************************/
      class tclXmlReadable: public tclXmlable
      {
         public:
            /***************************************************************************
             *********************************PUBLIC*************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  tclXmlReadable::tclXmlReadable() {}
             *************************************************************************/
            /*!
             * \fn    tclXmlReadable()
             * \brief Constructor
             * \sa    virtual ~tclXmlReadable()
             *************************************************************************/
            tclXmlReadable() {};

            /*************************************************************************
             ** FUNCTION:  tclXmlReadable::~tclXmlReadable() {}
             *************************************************************************/
            /*!
             * \fn    ~tclXmlReadable()
             * \brief Destructor
             * \sa    tclXmlReadable()
             *************************************************************************/
            virtual ~tclXmlReadable() {};

         protected:
            /***************************************************************************
             *******************************PROTECTED************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  bool tclXmlReadable::bGetAttribute(string sAttribName,...)
             *************************************************************************/
            /*!
             * \fn     bool bGetAttribute(string sAttribName, ...)
             * \brief  Convert data to integer
             * \param  sAttribName : [IN] Attribute name
             * \param  ppcAttrib :[IN] pointer to the list of attributes
             * \param  iValue : [OUT] converted integer value
             * \retval bool : true if success, false otherwise.
             *************************************************************************/
            bool bGetAttribute(string sAttribName, const xmlNodePtr poNode,
                  int &iValue)const;

            /*************************************************************************
             ** FUNCTION:  bool tclXmlable::bGetAttribute(string sAttribName, ...)
             *************************************************************************/
            /*!
             * \fn     bGetAttribute(string sAttribName, const xmlChar ** ppcAttrib, ...)
             * \brief  convert data to string
             * \param  sAttribName : [IN] Attribute name
             * \param  ppcAttrib :[IN] pointer to the list of attributes
             * \param  sValue : [OUT] converted string value
             * \retval bool : true if success, false otherwise.
             *************************************************************************/
            bool bGetAttribute(string sAttribName,
                             const xmlNodePtr poNode, string &sValue)const;

            /*************************************************************************
             ** FUNCTION:  bool tclXmlable::bGetAttribute(string sAttribName, ...)
             *************************************************************************/
            /*!
             * \fn     bGetAttribute(string sAttribName, const xmlNodePtr poNode, ...
             * \brief  convert data to string
             * \param  sAttribName : [IN] Attribute name
             * \param  ppcAttrib :[IN] pointer to the list of attributes
             * \param  sValue : [OUT] converted boolean value
             * \retval bool : true if success, false otherwise.
             *************************************************************************/
            bool bGetAttribute(string sAttribName,
                             const xmlNodePtr poNode, bool &bValue)const;

            /***************************************************************************
             ****************************END OF PROTECTED********************************
             ***************************************************************************/
         private:

            /***************************************************************************
             *********************************PRIVATE************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  virtual tclXmlable::bXmlWriteNode(xmlNodePtr poNode)
             *************************************************************************/
            /*!
             * \fn     virtual bool bXmlWriteNode(xmlNodePtr poNode)
             * \brief  Write a xml node
             * \param  poNode : [IN] Pointer to the xml node
             * \retval bool : true if success, false otherwise.
             * \note   Write functionality is disabled for reader.
             *************************************************************************/
            virtual bool bXmlWriteNode(xmlNodePtr poNode)
            {
               return false;
            };

            /***************************************************************************
             ****************************END OF PRIVATE**********************************
             ***************************************************************************/
      }; // class tclXmlReadable

      /****************************************************************************/
      /*!
       * \class tclXmlWritable
       * \brief Generic xml paarser
       *
       * This is implemented based on the libxml library. This act as a basic class
       * to implement the generic xml document writer
       *
       ***************************************************************************/
      class tclXmlWritable: public tclXmlable
      {
         public:
            /***************************************************************************
             *********************************PUBLIC*************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  tclXmlWritable::tclXmlWritable() {}
             *************************************************************************/
            /*!
             * \fn    tclXmlWritable()
             * \brief Constructor
             * \sa    virtual ~tclXmlWritable()
             *************************************************************************/
            tclXmlWritable() {};

            /*************************************************************************
             ** FUNCTION: virtual tclXmlWritable::~tclXmlWritable()
             *************************************************************************/
            /*!
             * \fn    virtual ~tclXmlWritable()
             * \brief Destructor
             * \sa    tclXmlWritable()
             *************************************************************************/
            virtual ~tclXmlWritable() {};

            /*************************************************************************
             ** FUNCTION:  virtual bool tclXmlWritable::bXmlReadNode(xmlNodePtr poNode)
             *************************************************************************/
            /*!
             * \fn     virtual bool bXmlReadNode(xmlNodePtr poNode)
             * \brief  Write a xml node
             * \param  poNode : [IN] Pointer to the xml node
             * \retval bool : true if success, false otherwise.
             * \note   This operation not supported at present.
             * \sa     bXmlWriteNode
             *************************************************************************/
            virtual bool bXmlReadNode(xmlNodePtr poNode)
            {
               return false;
            };

            /*************************************************************************
             ** FUNCTION:  virtual bool tclXmlWritable::bXmlWriteNode(xmlNodePtr poNode)
             *************************************************************************/
            /*!
             * \fn     virtual bool bXmlWriteNode(xmlNodePtr poNode)
             * \brief  Write a xml node
             * \param  poNode : [IN] Pointer to the xml node
             * \retval bool : true if success, false otherwise.
             * \note   This operation not supported at present.
             * \sa     bXmlReadNode
             *************************************************************************/
            virtual bool bXmlWriteNode(xmlNodePtr poNode)
            {
               return false;
            };

            /***************************************************************************
             ****************************END OF PUBLIC***********************************
             ***************************************************************************/
         protected:
            /***************************************************************************
             *******************************PROTECTED************************************
             ***************************************************************************/

            /***************************************************************************
             ****************************END OF PROTECTED********************************
             ***************************************************************************/
         private:
            /***************************************************************************
             *********************************PRIVATE************************************
             ***************************************************************************/

            /***************************************************************************
             ****************************END OF PRIVATE**********************************
             ***************************************************************************/
      };
   } // end of xml
} // end of shl

#endif /* XMLABLE_H_ */
