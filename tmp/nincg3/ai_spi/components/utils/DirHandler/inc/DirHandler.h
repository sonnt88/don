/*!
*******************************************************************************
* \file              DirHandler.h
* \brief             Directory Handling
*******************************************************************************
\verbatim
   PROJECT:        GM Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Directory Handler
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date       |  Author                    | Modifications
      06.08.2013 |  Shiva Kumar Gurija        | Initial Version

\endverbatim
******************************************************************************/

#ifndef DIRHANDLER_H_
#define DIRHANDLER_H_

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"
/******************************************************************************
| typedefs (scope: module-global)
|----------------------------------------------------------------------------*/
/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define ERROR ((t_S32)-1)
#define Ok ((t_S32)0)

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

//struct dirent and DIR* are defined in dirent.h
#include <dirent.h>


namespace shl {
   namespace io {

      /*!
      * \class DirHandler
      * \brief Exception safe directory handler for performing directory operations.
      *
      * This is based on design pattern \ref RAII "RAII"
      */

      class DirHandler
      {
      public:

         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

         /***************************************************************************
         ** FUNCTION:  DirHandler::DirHandler(const t_Char* szDirectory)
         ***************************************************************************/
         /*!
         * \fn      explicit DirHandler(const t_Char* szDirectory)
         * \brief   Parameterized Constructor
         * \param   szDirectory :  [IN] Directory name (inclusive of path)
         * \sa      ~DirHandler()
         **************************************************************************/
         explicit DirHandler(const t_Char* szDirectory);

         /***************************************************************************
         ** FUNCTION:  virtual DirHandler::~DirHandler()
         ***************************************************************************/
         /*!
         * \fn      virtual ~DirHandler()
         * \brief   Destructor
         * \sa      DirHandler(const t_Char* szDirectory)
         **************************************************************************/
         virtual ~DirHandler();

         /***************************************************************************
         ** FUNCTION:  virtual t_Bool DirHandler::bIsValid() const
         ***************************************************************************/
         /*!
         * \fn      virtual t_Bool bIsValid() const
         * \brief   This function checks the validity of the directory control
         *          structure.
         * \retval  t_Bool : TRUE if directory control structure is valid,
         *                  FALSE otherwise.
         * \sa      u32ErrorCode()
         **************************************************************************/
         virtual t_Bool bIsValid() const;

         /***************************************************************************
         ** FUNCTION:  virtual t_Bool DirHandler::bMkDir(const t_Char* szDirectory)
         ***************************************************************************/
         /*!
         * \fn      virtual t_Bool bMkDir(const t_Char* szDirectory)
         * \brief   This function to create a sub directory within the current
         *          directory.
         * \param   szDirectory : [IN] Sub directory name.
         * \retval  t_Bool       : TRUE or FALSE in case of error.
         * \sa      bRmDir(const t_Char* szDirectory)
         **************************************************************************/
         virtual t_Bool bMkDir(const t_Char* szDirectory);

         /***************************************************************************
         ** FUNCTION:  virtual t_Bool DirHandler::bRmDir(const t_Char* szDirectory)
         ***************************************************************************/
         /*!
         * \fn      virtual t_Bool bRmDir(const t_Char* szDirectory)
         * \brief   This function to remove a sub directory within the current
         *          directory. This is a recursive call, hence all the contents of
         *          this sub directory specified will be removed.
         * \param   szDirectory : [IN] Sub directory name.
         * \retval  t_Bool       : TRUE or FALSE in case of error.
         * \sa      bMkDir(const t_Char* szDirectory)
         **************************************************************************/
         virtual t_Bool bRmDir(const t_Char* szDirectory);

         /***************************************************************************
         ** FUNCTION:  virtual dirent* DirHandler::prReadDir()
         ***************************************************************************/
         /*!
         * \fn      virtual dirent* prReadDir()
         * \brief   This function reads the content of a directory.
         * \param   NONE
         * \retval  dirent* : Pointer to directory entry or NULL
         *          in case of error
         **************************************************************************/
         virtual dirent* prReadDir();

         /***************************************************************************
         ** FUNCTION:  t_U32 DirHandler::u32ErrorCode() const
         ***************************************************************************/
         /*!
         * \fn      t_U32 u32ErrorCode() const
         * \brief   This function returns the last error code during the file
         *          handling
         * \retval  t_U32 : Error code
         * \sa      bIsValid()
         **************************************************************************/
         t_U32 u32ErrorCode() const;

         /***************************************************************************
         ** FUNCTION:  t_Bool DirHandler::bIsDirectory(const t_Char* csDirectory)
         ***************************************************************************/
         /*!
         * \fn      virtual t_Bool bIsDirectory(const t_Char* csDirectory)
         * \brief   This function checks whether the given parameter is directory
         *          or not
         * \retval  t_Bool : true if it is directory else false
         **************************************************************************/
         virtual t_Bool bIsDirectory(const t_Char* csDirectory);

         /***************************************************************************
         ****************************END OF PUBLIC***********************************
         ***************************************************************************/

      protected:

         /***************************************************************************
         *******************************PROTECTED************************************
         ***************************************************************************/

         /***************************************************************************
         ** FUNCTION:  DirHandler::DirHandler()
         ***************************************************************************/
         /*!
         * \fn      DirHandler()
         * \brief   Default Constructor
         * \note    Default constructor is declared protected to disable default
         *          construction.
         * \sa      DirHandler(const t_Char* szDirectory)
         **************************************************************************/
         DirHandler();

         /***************************************************************************
         ** FUNCTION:  DirHandler::DirHandler(const DirHandler..)
         ***************************************************************************/
         /*!
         * \brief   Copy Constructor
         * \param   rfcoDirHandler : [IN] Const reference to object to be copied
         * \note    Default copy constructor is declared protected to disable it. So
         *          that any attempt to copy will be caught by the compiler.
         * \sa      operator=(const DirHandler &rfcoDirHandler)
         **************************************************************************/
         DirHandler(const DirHandler &rfcoDirHandler);

         /***************************************************************************
         ** FUNCTION:  DirHandler& DirHandler::operator=(const ..)
         ***************************************************************************/
         /*!
         * \fn      DirHandler& operator=(const DirHandler &rfcoDirHandler)
         * \brief   Assignment Operator
         * \param   rfcoDirHandler     : [IN] Const reference to object to be copied
         * \retval  DirHandler& : Reference to this pointer.
         * \note    Assignment operator is declared protected to disable it. So
         *          that any attempt for assignment will be caught by the compiler.
         * \sa      DirHandler(const DirHandler &rfcoDirHandler)
         **************************************************************************/
         DirHandler& operator=(const DirHandler &rfcoDirHandler);

         /***************************************************************************
         ** FUNCTION:  t_Void DirHandler::vErrorCode(tCU32 cu32ErrorCode)
         ***************************************************************************/
         /*!
         * \fn      t_Void vErrorCode(tCU32 cu32ErrorCode) const
         * \brief   This function shall give the failure reason for opening or
         *          creating a file
         * \param   cu32ErrorCode : Error code
         * \retval  t_Void
         * \sa      u32ErrorCode()
         **************************************************************************/
         t_Void vErrorCode(const t_U32 cu32ErrorCode) const;

         /*!
         * \addtogroup tclDir
         */
         /*! @{*/

         //! Resource : IO Control Structure for Directory to be acquired
         DIR *m_pioCtrlDir;
         //! Last Error code
         t_U32 m_u32ErrorCode;

         /*! @}*/

         /***************************************************************************
         ****************************END OF PROTECTED********************************
         ***************************************************************************/

      private:

         /***************************************************************************
         *********************************PRIVATE************************************
         ***************************************************************************/

         /***************************************************************************
         ****************************END OF PRIVATE**********************************
         ***************************************************************************/
      }; // class DirHandler


   }  // namespace io
}  // namespace shl

#endif   // #ifndef DIRHANDLER_H_

//////////////////////////////////////////////////////////////////////////////////////////

// <EOF>
