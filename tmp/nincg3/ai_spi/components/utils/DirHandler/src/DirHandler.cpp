/*!
*******************************************************************************
* \file              DirHandler.cpp
* \brief             SHL-Directory Handling
*******************************************************************************
\verbatim
   PROJECT:        GM Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    SHL - Directory Handler
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date       |  Author                    | Modifications
      06.08.2013 |  Shiva Kumar Gurija        | Initial Version

\endverbatim
******************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
//dirent.h files is included in header file

#include "DirHandler.h"
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_CONNECTIVITY
#include "trcGenProj/Header/DirHandler.cpp.trc.h"
#endif
#endif

/******************************************************************************
| defines and macros (scope: module-local)
|--------------------------------------------------------l--------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

namespace shl {
   namespace io {

      /******************************************************************************
      ** FUNCTION:  DirHandler::DirHandler(const t_Char* szDirectory)
      ******************************************************************************/

      /*explicit*/
      DirHandler::DirHandler(const t_Char* szDirectory)
         :m_pioCtrlDir(NULL), m_u32ErrorCode(Ok)
      {
         if(NULL != szDirectory)
         {
            m_pioCtrlDir= opendir(szDirectory);
         }
      }  // DirHandler::DirHandler(const t_Char* szDirectory)

      /******************************************************************************
      ** FUNCTION:  virtual DirHandler::~DirHandler()
      ******************************************************************************/
      /*virtual*/
      DirHandler::~DirHandler()
      {
         if(NULL != m_pioCtrlDir)
         {
            t_S32 s32StatCode = closedir(m_pioCtrlDir);
            if(ERROR == s32StatCode)
            {
               ETG_TRACE_ERR(("~DirHandler:Error in closing directory\n"));
            }
            // Invalidate the IO Control directory structure
            m_pioCtrlDir   =  NULL;
            m_u32ErrorCode = Ok ;
         }  // if(NULL != m_pioCtrlDir)
      }  // DirHandler::~DirHandler()

      /***************************************************************************
      ** FUNCTION:  virtual t_Bool DirHandler::bIsValid() const
      ***************************************************************************/
      /*virtual*/
      t_Bool DirHandler::bIsValid() const
      {
         return(NULL != m_pioCtrlDir);
      }  // t_Bool DirHandler::bIsValid() const

      /******************************************************************************
      ** FUNCTION:  virtual t_Bool DirHandler::bMkDir(const t_Char* szDirectory)
      ******************************************************************************/
      /*virtual*/
      t_Bool DirHandler::bMkDir(const t_Char* szDirectory)
      {
         t_Bool bRetVal  =  false;

         if((NULL != m_pioCtrlDir) && (NULL != szDirectory))
         {
            // Reset the Error code.
            m_u32ErrorCode =  Ok;
            //Create a directory with Read,Write and Execute access to user and Group
            // and Read and execute access to Others.
            if(ERROR != mkdir(szDirectory,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH))
            {
               bRetVal  =  true;
            }  // if(ERROR != mkdir(szDirectory, permissions..
            else
            {
               // Query & Set the error code.
               m_u32ErrorCode =  errno;
               vErrorCode(m_u32ErrorCode);
            }  // End of if-else; if(ERROR != mkdir(..
         }  // if(NULL != m_pioCtrlDir)

         return (bRetVal);
      }  // t_Bool DirHandler::bMkDir(const t_Char* szDirectory)

      /******************************************************************************
      ** FUNCTION:  virtual t_Bool DirHandler::bRmDir(const t_Char* szDirectory)
      ******************************************************************************/
      /*virtual*/
      t_Bool DirHandler::bRmDir(const t_Char* szDirectory)
      {
         SPI_INTENTIONALLY_UNUSED(szDirectory); //Should be removed when function is implemented!
         t_Bool bRetVal  =  false;
         //! TODO to be tested and released
         /*
         if(NULL != m_pioCtrlDir)
         {
            /// \brief A directory can be removed only if the directory is empty.
            /// Reference -
            /// <A HREF="http://www.opengroup.org/onlinepubs/009695399/functions/rmdir.html">
            /// The Open Group Base Spec Issue 6, IEEE Std 1003.1, 2004 Edition</A>.
            ///
            /// If the directory has to be removed the files in the directory have to
            /// be removed first by iterating through the directory files list
            /// And remove files and directories recursively

            DIR*            dp = NULL;
            struct dirent*  ep = NULL;
            char            p_buf[512] = {0};

            if ((NULL != szDirectory) && (false == bIsDirectory(szDirectory)))
            {
               return bRetVal;
            }

            dp = opendir(szDirectory);

            if (NULL != dp)
            {
               while ((ep = readdir(dp)) != NULL)
               {
                  sprintf(p_buf, "%s/%s", szDirectory, ep->d_name);
                  if (true == bIsDirectory(p_buf))
                  {
                     if (
                     //directories with the names '.' and '..'refers to present and previous directories respectively.
                     //deletion of these directories can lead to crashes in the system
                     ((0 != strcmp(ep->d_name, ".")) && (0 != strcmp(
                              ep->d_name, ".."))) && (false == bRmDir(p_buf)))
                     {
                        return bRetVal;
                     }
                  }
                  else
                  {
                     if (ERROR == remove(p_buf))
                     {
                        ETG_TRACE_ERR(
                                 ("DirHandler::bRmDir: Error in removing file %s \n", p_buf));
                        m_u32ErrorCode = errno;
                        vErrorCode( m_u32ErrorCode);
                     }
                  }
               }
               closedir(dp);
               if (ERROR != rmdir(szDirectory))
               {
                  bRetVal = true;
                  // Reset the Error code.
                  m_u32ErrorCode = Ok;
               } //if(ERROR != rmdir(...
               else
               {
                  ETG_TRACE_ERR(
                           ("DirHandler::bRmDir: Error in removing directory %s \n", szDirectory));
                  m_u32ErrorCode = errno;
                  vErrorCode( m_u32ErrorCode);
               }
            }

         }  // if(NULL != m_pioCtrlDir)
         */
         return (bRetVal);
      }  // t_Bool DirHandler::bRmDir(const t_Char* szDirectory)

      /******************************************************************************
      ** FUNCTION:  virtual OSAL_trIOCtrlDirent* DirHandler::prReadDir()
      ******************************************************************************/
      /*virtual*/

      dirent* DirHandler::prReadDir()
      {
         if(NULL == m_pioCtrlDir)
         {
            return NULL;
         } // if(NULL == m_pioCtrlDir)

         dirent* pDirent  =  readdir(m_pioCtrlDir);
         if(NULL != pDirent)
         {
            m_u32ErrorCode =  Ok;
         }  // if(NULL != pDirent)
         else
         {
            // :NOTE: From the description of the POSIX - readdir(), in case of reaching
            // end of directory, a NULL pointer is returned without changing the error
            // code value. This means that the value could be old error value if queried
            // Hence, here we explicitly set to resources does not exists as error code.
            m_u32ErrorCode =  0;//UNKNOWN_ERROR;
         }  // End of if-else; if(NULL != pDirent)

         return pDirent;
      }  // dirent* DirHandler::prReadDir()


      /******************************************************************************
      ** FUNCTION:  t_U32 DirHandler::u32ErrorCode() const
      ******************************************************************************/
      t_U32 DirHandler::u32ErrorCode() const
      {
         return (m_u32ErrorCode);
      }  // t_U32 ihl_tclDirHandler::u32ErrorCode() const

      /***************************************************************************
      ** FUNCTION:  t_Bool DirHandler::bIsDirectory(const t_Char* csDirectory)
      ***************************************************************************/
      t_Bool DirHandler::bIsDirectory(const t_Char* csDirectory)
      {
         t_Bool bRetval = false;
         struct stat s_buf;
         m_u32ErrorCode =  Ok;
         if (NULL != csDirectory)
         {
            if (ERROR != stat(csDirectory, &s_buf))
            {
               bRetval = S_ISDIR(s_buf.st_mode);
            }
            else
            {
               m_u32ErrorCode = errno;
               //vErrorCode(m_u32ErrorCode);
            }
         }
         return bRetval;
      }

      /***************************************************************************
      ** FUNCTION:  t_Void DirHandler::vErrorCode(tCU32 cu32ErrorCode)
      ***************************************************************************/
      t_Void DirHandler::vErrorCode(const t_U32  cu32ErrorCode) const
      {
         switch(cu32ErrorCode)
         {
         case EACCES:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: Permission denied\n"));
            break;
         case EBADF:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: fd is not a valid file descriptor opened for reading\n"));
            break;
         case EMFILE:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: Too many file descriptors in use by process\n"));
            break;
         case ENFILE:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: Too many files are currently open in the system\n"));
            break;
         case ENOENT:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: Directory does not exist, or name is an empty string\n"));
            break;
         case ENOMEM:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: Insufficient memory to complete the operation\n"));
            break;
         case ENOTDIR:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: name is not a directory\n"));
            break;
         case EDQUOT:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: The user's quota of disk blocks or inodes on the file system has been exhausted\n"));
            break;
         case EEXIST:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: pathname already exists (not necessarily as a directory)\n"));
            break;
         case EFAULT:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: pathname points outside your accessible address space\n"));
            break;
         case ELOOP:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: Too many symbolic links were encountered in resolving pathname\n"));
            break;
         case ENAMETOOLONG:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: pathname was too long\n"));
            break;
         case ENOSPC:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: The device containing pathname has no room for the new directory\n"));
            break;
         case EPERM:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: The file system containing pathname does not support the creation of directories\n"));
            break;
         case EROFS:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: pathname refers to a file on a read-only file system\n"));
            break;
         case EBUSY:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: pathname is currently in use by the system or some process that prevents its removal\n"));
            break;
         default:
            ETG_TRACE_ERR(("DirHandler::vErrorCode: default case\n"));
            break;
         }
      }
   }  // namespace io
}  // namespace shl



///////////////////////////////////////////////////////////////////////////////

// <EOF>
