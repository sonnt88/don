/***********************************************************************/
/*!
 * \file   MessageQueue.cpp
 * \brief  Message queue
 *************************************************************************
 \verbatim

    PROJECT:        Gen3
    SW-COMPONENT:   Smart Phone Integration
    DESCRIPTION:    Message Queue
    AUTHOR:         Shihabudheen P M
    COPYRIGHT:      &copy; RBEI

    HISTORY:
    Date        | Author                | Modification
    10.08.2013  | Shihabudheen P M      | Updated

    Note : The base version is from Mediaplayer
    Changes: 1. Introduce message type
             2. Modify the functions s16Push,s16Pop, poWaitForMessage with
                message type
 \endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/time.h>
#include "MessageQueue.h"

/******************************************************************************
 | defines and macros (scope: module-local)
 |----------------------------------------------------------------------------*/
#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#define ET_TRACE_INFO_ON

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/MessageQueue.cpp.trc.h"
   #endif
#endif

///some random but fixed number as magic token. Be careful to just use a hex 
///   value which fits to the size of int
#define MAGIC 0x8976543 

/*************************************************************************
 ** FUNCTION:  MessageQueue(const char *name)
 *************************************************************************/
MessageQueue::MessageQueue(const char *name)
{
   mName = name;
   pthread_mutex_init(&mWaitLock, NULL);
   pthread_cond_init(&mWaitCondition, NULL);
}

/*************************************************************************
 ** FUNCTION:  ~MessageQueue(void)
 *************************************************************************/
MessageQueue::~MessageQueue(void)
{
   pthread_mutex_destroy(&mWaitLock);
   pthread_cond_destroy(&mWaitCondition);
   mName = NULL;
}

/*************************************************************************
 ** FUNCTION:  s16Push(const void * message, size_t msgSize, int priority)
 *************************************************************************/
int MessageQueue::s16Push(const void * message, size_t msgSize, int priority,
         tenMsgType cMsgType)
{
   trMessageHeader *msgHeader = NULL;
   mAccessLock.s16Lock();

   // allocate the buffer ( space for the header + data)
   unsigned char* buffer = (unsigned char *) new unsigned char[msgSize
         + sizeof(trMessageHeader)];

   if (NULL == buffer)
      return -1;

   // set the message header
   msgHeader =(trMessageHeader *) ((void *)buffer);
   msgHeader->messageType = cMsgType;
   msgHeader->size = msgSize;
   msgHeader->token = MAGIC;

   // set the message data
   memcpy(buffer + sizeof(trMessageHeader), message, msgSize);

   // insert the message into the queue according to its priority
   mMessages[priority].push_back(buffer);
   s16ReleaseMessageWait();
   mAccessLock.vUnlock();
   return 0;
}

/*************************************************************************
 ** FUNCTION:  int MessageQueue::s16Pop(void** message, size_t *msgSize...
 *************************************************************************/
int MessageQueue::s16Pop(void** message, size_t *msgSize, tenMsgType *msgType)
{
   int res = -1;
   map<int, vector<void*> >::iterator iter;
   mAccessLock.s16Lock();

   // look for message lists from high to low priority
   for (iter = mMessages.begin(); iter != mMessages.end(); ++iter)
   {
      if (iter->second.size() > 0)
      {
         // get the message at the front of the queue
         void* buffer = iter->second.front();

         // set the return message with the popped element
         trMessageHeader *msgHeader = (trMessageHeader *) buffer;
         if ((msgHeader->token == MAGIC) && (NULL != msgHeader) )
         {
            *message =(unsigned char*) new unsigned char[msgHeader->size];
            if (*message)
            {
               memcpy(*message,
                     (unsigned char*) buffer + sizeof(trMessageHeader),
                     msgHeader->size);
               *msgSize = msgHeader->size;
               *msgType = msgHeader->messageType;
            }
         } else
         {
             ETG_TRACE_ERR (("Unknown message with wrong magic found in the queue : %s",mName));
         }

         // free the element and erase it from the queue
         delete [](unsigned char *)buffer;
         iter->second.erase(iter->second.begin());
         // a message found and poped out
         res = 0;
         break;
      }
   }

   mAccessLock.vUnlock();
   return res;
}

/*************************************************************************
 ** FUNCTION:  void* MessageQueue::poWaitForMessage(size_t *msgSize, ...
 *************************************************************************/
void* MessageQueue::poWaitForMessage(size_t *msgSize, tenMsgType *msgType,
      unsigned int time_out_in_ms)
{
   int res;

   void *retMessage = NULL;

   // attempt to retrieve a message from queue
   res = s16Pop(&retMessage, msgSize, msgType);

   // no message from the queue
   if (res)
   {
      // if no message popped from the queue, start waiting till message arrives in the queue
      res = s16StartMessageWait(time_out_in_ms);
      // wait ends with timeout
      if (ETIMEDOUT == res)
      {
          ETG_TRACE_ERR (("MessageQueue::WaitForMessage -> timeout(%d msec) occured while reading queue %s",time_out_in_ms,mName));
      }
      // wait ends with message arrived in queue
      else if (!res)
      {
         res = s16Pop(&retMessage, msgSize, msgType);
         if (res)
         {
             ETG_TRACE_ERR (("MessageQueue::WaitForMessage ->error:%d no message found in queue even after wait in queue %s , Should NOT OCCUR!!",res,mName));
         }
      }
      // wait ends with an error
      else
      {
          ETG_TRACE_ERR(("MessageQueue::WaitForMessage error while waiting for message from queue : %d (%s) !",res,mName));
      }
   }

   return retMessage;
}

/*************************************************************************
 ** FUNCTION:  unsigned int MessageQueue::u16GetCurMessagesCount()
 *************************************************************************/
unsigned int MessageQueue::u16GetCurMessagesCount()
{
   unsigned int size = 0x00;
   map<int, vector<void*> >::const_iterator iter;

   mAccessLock.s16Lock();
   for (iter = mMessages.begin(); iter != mMessages.end(); ++iter)
   {
      size += iter->second.size();
   }
   mAccessLock.vUnlock();

   return size;
}

/*************************************************************************
 ** FUNCTION:  void* MessageQueue::poCreateMessageBuffer(size_t size)
 *************************************************************************/
void* MessageQueue::poCreateMessageBuffer(size_t size)const
{
   //return malloc(size);
   return (unsigned char*) new unsigned char[size];
}

/*************************************************************************
 ** FUNCTION:  int MessageQueue::s16DropMessage(void* message)
 *************************************************************************/
int MessageQueue::s16DropMessage(void* message)
{
   if (message)
      delete [](unsigned char *)message;
   return 0;
}

/*************************************************************************
 ** FUNCTION:  int MessageQueue::s16Flush()
 *************************************************************************/
int MessageQueue::s16Flush()
{
   map<int, vector<void*> >::iterator iter;
   mAccessLock.s16Lock();

   ///iterate through all the priorities list
   for (iter = mMessages.begin(); iter != mMessages.end(); ++iter)
   {
      while (iter->second.size() > 0)
      {
         /// free the last element
         delete []((unsigned char *)iter->second.back());
         iter->second.pop_back();
      }
   }

   mAccessLock.vUnlock();
   return 0;
}

/*************************************************************************
 ** FUNCTION:int MessageQueue::s16ReleaseMessageWait(void)
 *************************************************************************/
int MessageQueue::s16ReleaseMessageWait(void)
{
   return pthread_cond_signal(&mWaitCondition);
}

/*************************************************************************
 ** FUNCTION:  int MessageQueue::s16StartMessageWait(unsigned int ....)
 *************************************************************************/
int MessageQueue::s16StartMessageWait(unsigned int u16WaitTime)
{
   int res;

   if (u16WaitTime)
   {
      struct timespec timeToWait;
      struct timeval currentTime;

      // get current time
      gettimeofday(&currentTime, NULL);

      // convert timeval to timespec
      timeToWait.tv_sec = currentTime.tv_sec;
      timeToWait.tv_nsec = currentTime.tv_usec * 1000;

      // update timeToWait with wait duration
      timeToWait.tv_sec += u16WaitTime / 1000;
      timeToWait.tv_nsec += ((u16WaitTime % 1000) * 1000000);
      if (timeToWait.tv_nsec > 1000000000)
      {
         timeToWait.tv_sec += timeToWait.tv_nsec / 1000000000;
         timeToWait.tv_sec += timeToWait.tv_nsec % 1000000000;
      }

      //lock the calling thread and start wait on a condition
      //condition wait will get releases when timeout occurs or if a message arrives in queue
      pthread_mutex_lock(&mWaitLock);
      res = pthread_cond_timedwait(&mWaitCondition, &mWaitLock, &timeToWait);
      pthread_mutex_unlock(&mWaitLock);
   }
   else
   {
      pthread_mutex_lock(&mWaitLock);
      res = pthread_cond_wait(&mWaitCondition, &mWaitLock);
      pthread_mutex_unlock(&mWaitLock);
   }

   return res;
}
