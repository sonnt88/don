#ifndef _MESSAGEQUEUE_H_
#define _MESSAGEQUEUE_H_

/***********************************************************************/
/*!
 * \file  MessageQueue.h
 * \brief Message queue
 *************************************************************************
 \verbatim

    PROJECT:        Gen3
    SW-COMPONENT:   Smart Phone Integration
    DESCRIPTION:    Message queue implementation
    AUTHOR:         Shihabudheen P M
    COPYRIGHT:      &copy; RBEI

    HISTORY:
    Date        | Author                | Modification
    10.08.2013  | Shihabudheen P M      | Updated

    Note : The base version is from Mediaplayer
    Changes: 1. Introduce message type
             2. Modify the functions s16Push,s16Pop, poWaitForMessage with
                message type
 \endverbatim
 *************************************************************************/

/******************************************************************************
 | Include headers and namespaces(scope: global)
 |----------------------------------------------------------------------------*/
#include <map>
#include <string>
#include <vector>
#include <pthread.h>
//#include "BaseTypes.h"
#include "Lock.h"

using namespace std;

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/*! \enum msgType
 * Type to represent the message type.
 */
enum tenMsgType
{
   e8_TCL_THREAD_TERMINATE_MESSAGE, e8_TCL_DATA_MESSAGE
};
//enum msgType

/*! \struct trMessageHeader
 *  Header for the messages
 */
struct trMessageHeader
{
      // size of the actual user data
      unsigned int size;
      // Type to identify thye nature of message.
      tenMsgType messageType;
      // a token to compare if sent data is valid
      unsigned int token;
};

/****************************************************************************/
/*!
 * \class MessageQueue
 * \brief Message Queue Implementation
 *
 * Message queue is implemented to provide a haldler to message passing.
 * This implemented based on the Posix calls. This gives you an easy
 * way to dealt with messages and some intelligence to perform operation
 * on messages and return proper error codes on failure.
 ****************************************************************************/
class MessageQueue
{
   public:

      /*************************************************************************
       *********************************PUBLIC***********************************
       *************************************************************************/

      /*************************************************************************
       ** FUNCTION:  MessageQueue::MessageQueue(const char *name)
       *************************************************************************/
      /*!
       * \fn    MessageQueue(const char *name)
       * \brief Constructor
       * \param name : [IN] Name of the message queue
       * \sa     ~MessageQueue(void)
       *************************************************************************/
      MessageQueue(const char *name);

      /*************************************************************************
       ** FUNCTION:  MessageQueue::~MessageQueue(void)
       *************************************************************************/
      /*!
       * \fn    ~MessageQueue(void)
       * \brief Destructor
       * \sa    MessageQueue
       *************************************************************************/
      virtual ~MessageQueue(void);

      /*************************************************************************
       ** FUNCTION:  MessageQueue::s16Push(const void* message, size_t msgSize,...)
       *************************************************************************/
      /*!
       * \fn     s16Push(const void* message, size_t msgSize, int priority)
       * \brief  pushes a new message into the queue respecting its priority.
       * \param  message : [IN] message data
       * \param  msgSize : [IN] message size
       * \param  priority: [IN] message priority
       * \retval int : 0 if message post is success, -1 otherwise.
       * \sa    Pop
       *************************************************************************/
      int s16Push(const void * message, size_t msgSize, int priority = 1,
               tenMsgType cMsgType = e8_TCL_DATA_MESSAGE  );

      /*************************************************************************
       ** FUNCTION:  MessageQueue::s16Pop(void** message , size_t *msgSize
       *************************************************************************/
      /*!
       * \fn     s16Pop(void** message , size_t *msgSize
       * \brief  pops out highest priority element from the queue
       * \param  message : [OUT] message datal
       * \param  msgSize : [OUT] message size
       * \retval int : 0 if message pop is success, -1 otherwise.
       * \note   caller of this method should free the message receive
       * \sa    Push
       *************************************************************************/
      int s16Pop(void** message, size_t *msgSize, tenMsgType *msgType);

      /*************************************************************************
       ** FUNCTION:  MessageQueue::poWaitForMessage (size_t *msgSize,...)
       *************************************************************************/
      /*!
       * \fn     poWaitForMessage (size_t *msgSize,unsigned int time_out_in_ms)
       * \brief  pops out highest priority element from the queue ,
       waits if the queue is empty
       * \param  msgSize : [OUT] message size
       * \param  time_out_in_ms : [IN] time out
       * \note   caller of this method should free the message receive
       * \sa    shl_tclFileHandler(tCString csFileName, shl_enFileAccess enAccess)
       *************************************************************************/
      void* poWaitForMessage(size_t *msgSize, tenMsgType *msgType,
            unsigned int time_out_in_ms);

      /*************************************************************************
       ** FUNCTION:  MessageQueue::poCreateMessageBuffer (size_t size)
       *************************************************************************/
      /*!
       * \fn     poCreateMessageBuffer (size_t size)
       * \brief  Helper methods to create and drop message buffer.
       * \param  size : [IN] buffer size
       * \retval void*: address of the buffer created
       * \note   Clients need not use this and also can handle memory allocation
       *         themselves
       *************************************************************************/
      void *poCreateMessageBuffer(size_t size)const;

      /*************************************************************************
       ** FUNCTION:  MessageQueue::s16DropMessage (void* message )
       *************************************************************************/
      /*!
       * \fn     s16DropMessage (void* message )
       * \brief  Remove the message
       * \param  message : [IN] ponter to the message
       * \retval int : 0 if successfully removed, -1 otherwise
       *************************************************************************/
      int s16DropMessage(void* message);

      /*************************************************************************
       ** FUNCTION:  MessageQueue::u16GetCurMessagesCount()
       *************************************************************************/
      /*!
       * \fn     u16GetCurMessagesCount()
       * \brief  Returns the message count in the queue
       * \retval unsigned int : message count
       *************************************************************************/
      unsigned int u16GetCurMessagesCount();

      /*************************************************************************
       ** FUNCTION:  MessageQueue::s16Flush()
       *************************************************************************/
      /*!
       * \fn     s16Flush()
       * \brief  Removes all the message from the queue and deallocates memory
       *         used by the messages
       * \retval int : 0 if success, -1 otherwise.
       *************************************************************************/
      int s16Flush();

      /*************************************************************************
       ****************************END OF PUBLIC*********************************
       *************************************************************************/

   private:

      /*************************************************************************
       *********************************PRIVATE**********************************
       *************************************************************************/

      // name of the message queue
      const char *mName;

      // pair of key "priority" to  value "vector container as queue holding
      // messages with this priority"
      map<int, vector<void*> > mMessages;

      Lock mAccessLock;

      // wait condition used to wait for message to arrive into the queue
      pthread_cond_t mWaitCondition;
      // wait lock used along with the wait condition
      pthread_mutex_t mWaitLock;

      /*************************************************************************
       ** FUNCTION:  int MessageQueue::s16ReleaseMessageWait(void)
       *************************************************************************/
      /*!
       * \fn     ReleaseMessageWait(void)
       * \brief  Release waiting on message queue
       * \retval int : 0 if success
       *************************************************************************/
      int s16ReleaseMessageWait(void);

      /*************************************************************************
       ** FUNCTION:  int MessageQueue::s16StartMessageWait(unsigned int milliSeconds)
       *************************************************************************/
      /*!
       * \fn     StartMessageWait(unsigned int milliSeconds)
       * \brief  starts waiting on message queue
       * \retval int : 0 if success
       *************************************************************************/
      int s16StartMessageWait(unsigned int u16WaitTime);

      /*************************************************************************
       ****************************END OF PRIVATE *******************************
       *************************************************************************/

};

#endif /* MESSAGEQUEUE_H_ */
