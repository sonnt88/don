
/***********************************************************************/
/*!
* \file   MsgContext.cpp
* \brief  Utility to store and retrieve MsgContexts associated with a ReqId.
*************************************************************************
\verbatim

PROJECT      :  GM Gen3
SW-COMPONENT :  Smart Phone Integration
DESCRIPTION  :  Utility to store and retrieve MsgContexts associated
                with a ReqId.
AUTHOR       :  Shiva Kumar Gurija
COPYRIGHT    :  &copy; RBEI

HISTORY:
Date        | Author                | Modification
10.08.2013  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include <map>
#include "BaseTypes.h"
#include "SPITypes.h"
#include "MsgContext.h"
using namespace std;

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/MsgContext.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/*************************************************************************
** FUNCTION:  MsgContext::MsgContext()
*************************************************************************/
MsgContext::MsgContext()
{
   ETG_TRACE_USR1(("MsgContext() entered \n"));

   m_mapMsgCntxt.clear();

}

/*************************************************************************
** FUNCTION:  virtual MsgContext::~MsgContext()
*************************************************************************/
MsgContext::~MsgContext()
{
   ETG_TRACE_USR1(("~MsgContext() entered \n"));

   //Aquire Lock
   m_oLock.s16Lock();

   //Clear Contents
   m_mapMsgCntxt.clear();

   //Release Lock
   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  void MsgContext::vAddMsgContext(unsigned int u32Id,....
***************************************************************************/
void MsgContext::vAddMsgContext(unsigned int u32Id,trMsgContext rMsgCntxt)
{
   ETG_TRACE_USR1(("vAddMsgContext() entered - %d \n",u32Id));

   //Aquire Lock
   m_oLock.s16Lock();

   //Insert the elements to map
   m_mapMsgCntxt.insert(std::pair<unsigned int,trMsgContext>(u32Id,rMsgCntxt));

   //Release Lock
   m_oLock.vUnlock();

}

/***************************************************************************
** FUNCTION:  trMsgContext MsgContext::rGetMsgContext(unsigned int u32Id)
***************************************************************************/
trMsgContext MsgContext::rGetMsgContext(unsigned int u32Id)
{
   ETG_TRACE_USR1(("rGetMsgContext() entered - %d\n",u32Id));

   trMsgContext rMsgCntxt;

   //Aquire Lock
   m_oLock.s16Lock();

   if(
      ( 0 != m_mapMsgCntxt.size())
      &&
      ( m_mapMsgCntxt.end() != m_mapMsgCntxt.find(u32Id) )
      )
   {
      rMsgCntxt =  m_mapMsgCntxt[u32Id];
      //Erase the element after reading
      m_mapMsgCntxt.erase(u32Id);
   }

   //Release Lock
   m_oLock.vUnlock();

   return rMsgCntxt;
}

/***************************************************************************
** FUNCTION:  void MsgContext::vClearMsgContexts()
***************************************************************************/
void MsgContext::vClearMsgContexts()
{
   ETG_TRACE_USR1(("vClearMsgContexts() entered \n"));

   //Aquire Lock
   m_oLock.s16Lock();

   //Clear Contents
   m_mapMsgCntxt.clear();

   //Release Lock
   m_oLock.vUnlock();

}

////////////////////////////////////////////////////////////////////////////////
// <EOF>

