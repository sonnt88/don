
/**************************************************************************/
/*!
* \file  tclMLContextUtility.h
* \brief Utility to store and retrieve MsgContexts associated with a ReqId.
****************************************************************************
\verbatim

PROJECT      :  GM Gen3
SW-COMPONENT :  Smart Phone Integration
DESCRIPTION  :  Utility to store and retrieve MsgContexts associated
                with a ReqId.
AUTHOR       :  Shiva Kumar Gurija
COPYRIGHT    :  &copy; RBEI

HISTORY:
Date        | Author                | Modification
10.08.2013  | Shiva Kumar Gurija    | Initial Version

\endverbatim
****************************************************************************/

#ifndef _MSGCONTEXT_H_
#define _MSGCONTEXT_H_

#include "SPITypes.h"
#include "Lock.h"

/***************************************************************************
| typedefs (scope: module-local)
|-------------------------------------------------------------------------*/

/***************************************************************************
| defines and macros (scope: global)
|-------------------------------------------------------------------------*/

/*************************************************************************/
/*!
* \class  MsgContext
* \brief  This class can be used to store and retrieve Message Contexts
*         associated with the RequestId's.
**************************************************************************/
class MsgContext
{

public:

   /************************************************************************
   *********************************PUBLIC**********************************
   ************************************************************************/

   /***********************************************************************
   ** FUNCTION:  MsgContext::MsgContext()
   ************************************************************************/
   /*!
   * \fn    MsgContext()
   * \brief Constructor
   * \sa    ~MsgContext()
   ************************************************************************/
   MsgContext();

   /************************************************************************
   ** FUNCTION:  virtual MsgContext::~MsgContext()
   ************************************************************************/
   /*!
   * \fn    virtual ~MsgContext()
   * \brief Destructor
   * \sa    MsgContext()
   ************************************************************************/
   virtual ~MsgContext();

   /************************************************************************
   ** FUNCTION:  void MsgContext::vAddMsgContext(unsigned int u32Id,.
   ************************************************************************/
   /*!
   * \fn      void vAddMsgContext(unsigned int u32Id,trMsgContext rMsgCntxt)
   * \brief   Method to insert the Message Context associated with ReqId 
   * \param   u32Id      : [IN] Request ID
   * \param   rMsgCntxt  : [IN] Message Context structure
   * \retval  void
   * \sa      rGetMsgContext(unsigned int u32Id)
   ************************************************************************/
   void vAddMsgContext(unsigned int u32Id,trMsgContext rMsgCntxt);

   /************************************************************************
   ** FUNCTION:  trMsgContext MsgContext::rGetMsgContext(unsigned int ..
   ************************************************************************/
   /*!
   * \fn      trMsgContext rGetMsgContext(unsigned int u32Id)
   * \brief   Method to get the MsgContext structure associated with the ReqId
   *          and the retrieved MsgContext will be removed from the Container.
   * \param   u32Id        : [IN] Request ID
   * \retval  trMsgContext : Message context structure
   * \sa      vAddMsgContext(unsigned int u32Id,trMsgContext rMsgCntxt)
   ************************************************************************/
   trMsgContext rGetMsgContext(unsigned int u32Id);

   /************************************************************************
   ** FUNCTION:  void MsgContext::vClearMsgContexts()
   ************************************************************************/
   /*!
   * \fn      void vClearMsgContexts()
   * \brief   To clear all the Message contexts Map
   * \retval  void
   * \sa
   ************************************************************************/
   void vClearMsgContexts();

   /************************************************************************
   ****************************END OF PUBLIC********************************
   ************************************************************************/

protected:

   /************************************************************************
   *********************************PROTECTED*******************************
   *************************************************************************/

   /************************************************************************
   ** FUNCTION:  MsgContext(const MsgContext& corfoS
   *************************************************************************/
   /*!
   * \fn      MsgContext(const MsgContext& corfoSrc)
   * \brief   Copy constructor - Do not allow the creation of copy constructor
   * \param   corfoSrc : [IN] reference to source  object
   * \sa      MsgContext()
   *************************************************************************/
   MsgContext(const MsgContext& corfoSrc);

   /*************************************************************************
   ** FUNCTION:  MsgContext& operator=(const tclMLMsgContextUtil.
   *************************************************************************/
   /*!
   * \fn      MsgContext& operator=(const MsgContext& corfoSrc))
   * \brief   Assignment operator
   * \param   corfoSrc : [IN] reference to source object
   * \retval  MsgContext&
   * \sa      MsgContext(const MsgContext& otrSrc)
   *************************************************************************/
   MsgContext& operator=(const MsgContext& corfoSrc);

   /*************************************************************************
   ****************************END OF PROTECTED******************************
   *************************************************************************/

private:

   /*************************************************************************
   *********************************PRIVATE**********************************
   **************************************************************************/

   /**
   * Map to store MsgContexts associated with RequestId's
   */
   std::map<unsigned int,trMsgContext> m_mapMsgCntxt;

   //! Lock Variable 
   Lock m_oLock;

   /*************************************************************************
   ****************************END OF PRIVATE *******************************
   *************************************************************************/

}; //MsgContext


#endif //_MSGCONTEXT_H_

////////////////////////////////////////////////////////////////////////////////
// <EOF>
