/***********************************************************************/
/*!
* \file  KdsHandler.h
* \brief KDS data handler class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    KDS data handler class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.07.2014  | Ramya Murthy          | Initial Version
08.04.2015  | Ram                   | Moved the Suzuuki configuration values
                                      to project specific file spi_tclConfigReader.cpp

\endverbatim
*************************************************************************/

#ifndef _KDSHANDLER_H_
#define _KDSHANDLER_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/


/****************************************************************************/
/*!
* \class KdsHandler
* \brief KDS data handler class
*
* It provides methods to read KDS data
****************************************************************************/
class KdsHandler
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  KdsHandler::KdsHandler()
   ***************************************************************************/
   /*!
   * \fn      KdsHandler()
   * \brief   Constructor
   * \sa      ~KdsHandler()
   ***************************************************************************/
   KdsHandler();

   /***************************************************************************
   ** FUNCTION: KdsHandler::~KdsHandler()
   ***************************************************************************/
   /*!
   * \fn      ~KdsHandler()
   * \brief   Destructor
   * \sa      KdsHandler()
   ***************************************************************************/
   virtual ~KdsHandler();

   /***************************************************************************
   ** FUNCTION: tBool KdsHandler::bReadData(tU16 u16KdsKey, tU16 u16DataLen, tU8* pu8DataBuffer)
   ***************************************************************************/
   /*!
   * \fn      bReadData(tU16 u16KdsKey, tU16 u16DataLen, tU8* pu8DataBuffer)
   * \brief   Interface to read data using a Key in KDS.
   * \param   u16KdsKey [IN] : Key address at which data is stored in KDS
   * \param   u16DataLen [IN] : Total size of the data, stored at u16KdsKey
   * \param   pu8DataBuffer [OUT] : Pointer to buffer, to which read data should be copied.
   * \retval  tBool: TRUE - if read is successful, else FALSE.
   ***************************************************************************/
   tBool bReadData(tU16 u16KdsKey, tU16 u16DataLen, tVoid* pu8DataBuffer);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif  // _KDSHANDLER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
