/***********************************************************************/
/*!
* \file  KdsHandler.h
* \brief KDS data handler class
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    KDS data handler class
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.07.2014  | Ramya Murthy          | Initial Version

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include "KdsHandler.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/KdsHandler.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  KdsHandler::KdsHandler()
***************************************************************************/
KdsHandler::KdsHandler()
{
   ETG_TRACE_USR4(("KdsHandler() entered \n"));
}

/***************************************************************************
** FUNCTION: KdsHandler::~KdsHandler()
***************************************************************************/
KdsHandler::~KdsHandler()
{
   ETG_TRACE_USR4(("~KdsHandler() entered \n"));
}

/***************************************************************************
** FUNCTION:  tBool KdsHandler::bReadData(tU16 u16KdsKey, tU16 u16DataLen, tU8* pu8DataBuffer)
***************************************************************************/
tBool KdsHandler::bReadData(tU16 u16KdsKey, tU16 u16DataLen, tVoid* pu8DataBuffer)
{
   ETG_TRACE_USR4(("KdsHandler::bReadData() entered: u16KdsKey = 0x%x, u16DataLen = 0x%x \n",
         u16KdsKey, u16DataLen));

   tBool bReadSuccess = FALSE;

   NORMAL_M_ASSERT(OSAL_NULL != pu8DataBuffer);
   if (OSAL_NULL != pu8DataBuffer)
   {
      //! Open KDS file
      OSAL_tIODescriptor tIoKdsHandle = OSAL_IOOpen(OSAL_C_STRING_DEVICE_KDS, OSAL_EN_READONLY);

      if (OSAL_ERROR != tIoKdsHandle)
      {
         tsKDSEntry rKDSEntryData;
         rKDSEntryData.u16Entry = u16KdsKey;
         rKDSEntryData.u16EntryLength = u16DataLen;
         rKDSEntryData.u16EntryFlags = M_KDS_ENTRY_FLAG_NONE;
         rKDSEntryData.au8EntryData[0] = 0;

         //! Read required data from file
         tS32 s32OsalReadErr = OSAL_s32IORead(tIoKdsHandle, (tPS8) &rKDSEntryData, (sizeof(rKDSEntryData)));
         if (OSAL_ERROR != s32OsalReadErr)
         {
            (tVoid) OSAL_pvMemoryCopy((tVoid*) (pu8DataBuffer), rKDSEntryData.au8EntryData, u16DataLen);
            bReadSuccess = TRUE;
         }
         else
         {
            ETG_TRACE_ERR(("KdsHandler::bReadData: Error reading KDS file: %d \n", OSAL_u32ErrorCode()));
         }

         //! Close KDS file
         (tVoid) OSAL_s32IOClose(tIoKdsHandle);

      }//if (OSAL_ERROR != tIoKdsHandle)
      else
      {
         ETG_TRACE_ERR(("KdsHandler::bReadData: Error opening KDS file: %d \n", OSAL_u32ErrorCode()));
      }
   }//if (OSAL_NULL != pu8DataBuffer)

   return bReadSuccess;
}

