/*!
*******************************************************************************
* \file              Trace.h
* \brief             Handles message tracing
*******************************************************************************
\verbatim
   PROJECT:        GM Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Handles message tracing
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date       |  Author                    | Modifications
      26.08.2015 |  Pruthvi Thej Nagaraju     | Initial Version
\endverbatim
******************************************************************************/
//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
#include "trcGenProj/Header/Trace.cpp.trc.h"
#endif
#endif

/***************************************************************************
** FUNCTION:  void vFunctionTracer
***************************************************************************/
void vFunctionTracer(bool bResult, unsigned int u32LineNo, const char *czFileName, const char *czArg1)
{
   (void)czArg1;
   if (true == bResult)
   {
      ETG_TRACE_FATAL(("Assertion Failed at line = %d, (%s) ", u32LineNo, czFileName));
   }
}
/***************************************************************************
** FUNCTION:  void vFunctionTracer
***************************************************************************/
void vFunctionTracer(unsigned int u32LineNo, const char *czFileName)
{
   ETG_TRACE_FATAL(("Assertion Failed at line = %d, (%s) ", u32LineNo, czFileName));
}
