/*!
*******************************************************************************
* \file              Trace.h
* \brief             Handles message tracing
*******************************************************************************
\verbatim
   PROJECT:        GM Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Handles message tracing
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date       |  Author                    | Modifications
      05.09.2013 |  Pruthvi Thej Nagaraju     | Initial Version
	  17.06.2014 |  Priju K Padiyath          | Restructuring + DLT tracing 

\endverbatim
******************************************************************************/

#ifndef TRACE_H_
#define TRACE_H_

//! Uncomment the following command for enabling syslog instead of TTFis
//#define SPI_ENABLE_SYSLOG
#define SPI_THREADID  (int)pthread_self()
//! Define for syslog.disable other mechanisms.
#ifdef SPI_ENABLE_SYSLOG

#undef TARGET_BUILD
#undef PC_BUILD
#undef SPI_ENABLE_DLT

 #include <stdarg.h>
      #include <syslog.h>
      inline void vLogtoFile(const char *format, ...)
      {
         va_list args;
         va_start(args, format);
         openlog("spilog", LOG_PID|LOG_CONS, LOG_USER);
         vsyslog(LOG_INFO, format, args);
         closelog();
         //vprintf(format, args);
         va_end(args);
      }
      #define ETG_TRACE_USR1(ARGS) vLogtoFile ARGS
      #define ETG_TRACE_USR2(ARGS) vLogtoFile ARGS
      #define ETG_TRACE_USR3(ARGS) vLogtoFile ARGS
      #define ETG_TRACE_USR4(ARGS) vLogtoFile ARGS
      #define ETG_TRACE_ERR(ARGS) vLogtoFile ARGS
      #define ETG_TRACE_FATAL(ARGS) vLogtoFile ARGS
      #define ETG_TRACE_ERRMEM(ARGS) vLogtoFile ARGS
      #define ETG_ENUM(ARG1,ARG2) ARG2

//! Define for ETG tracing.disable other mechanisms.
#elif defined(TARGET_BUILD)

#undef PC_BUILD
#undef SPI_ENABLE_DLT

#define ETRACE_S_IMPORT_INTERFACE_GENERIC
   #define ET_TRACE_INFO_ON
   #include "etrace_if.h"
   #include "spi_trace_macros.h"

 //! Define for printf.disable other mechanisms.
#elif defined(PC_BUILD)

#undef SPI_ENABLE_DLT

#include <stdio.h>
      //Trace macros for printing trace on PC
      #define DISP(VAR) (std::cout<<#VAR<<" = "<<VAR<<std::endl)
      #define ETG_TRACE_USR1(ARGS) printf ARGS
      #define ETG_TRACE_USR2(ARGS) printf ARGS
      #define ETG_TRACE_USR3(ARGS) printf ARGS
      #define ETG_TRACE_USR4(ARGS) printf ARGS
      #define ETG_TRACE_ERR(ARGS) printf ARGS
      #define ETG_TRACE_FATAL(ARGS) printf ARGS
      #define ETG_TRACE_ERRMEM(ARGS) printf ARGS
      #define ETG_ENUM(ARG1,ARG2) ARG2
	  
//! Define for DLT.disable other mechanisms.
#elif defined(SPI_ENABLE_DLT)

#undef SPI_ENABLE_SYSLOG
#undef TARGET_BUILD
#undef PC_BUILD

#include <stdio.h>
#include <dlt/dlt.h>
#define SPI_LOG_FATAL(log_string, args...) do {  \
        char dltlog[DLT_USER_BUF_MAX_SIZE] =""; \
        snprintf(dltlog, DLT_USER_BUF_MAX_SIZE, log_string, ## args); \
        DLT_LOG(SPI_LOG_CLASS, DLT_LOG_FATAL, DLT_STRING(dltlog)); \
    }while(0);

#define SPI_LOG_ERROR(log_string, args...) do {  \
        char dltlog[DLT_USER_BUF_MAX_SIZE] =""; \
        snprintf(dltlog, DLT_USER_BUF_MAX_SIZE, log_string, ## args); \
        DLT_LOG(SPI_LOG_CLASS, DLT_LOG_ERROR, DLT_STRING(dltlog)); \
    }while(0);

#define SPI_LOG_WARN(log_string, args...) do {  \
        char dltlog[DLT_USER_BUF_MAX_SIZE] =""; \
        snprintf(dltlog, DLT_USER_BUF_MAX_SIZE, log_string, ## args); \
        DLT_LOG(SPI_LOG_CLASS, DLT_LOG_WARN, DLT_STRING(dltlog)); \
    }while(0);

#define SPI_LOG_INFO(log_string, args...) do {  \
        char dltlog[DLT_USER_BUF_MAX_SIZE] =""; \
        snprintf(dltlog, DLT_USER_BUF_MAX_SIZE, log_string, ## args); \
        DLT_LOG(SPI_LOG_CLASS, DLT_LOG_INFO, DLT_STRING(dltlog)); \
    }while(0);

#define SPI_LOGD_DEBUG(log_string, args...) do {  \
        char dltlog[DLT_USER_BUF_MAX_SIZE] =""; \
        snprintf(dltlog, DLT_USER_BUF_MAX_SIZE, log_string, ## args); \
        DLT_LOG(SPI_LOG_CLASS, DLT_LOG_DEBUG, DLT_STRING(dltlog)); \
    }while(0);

#define SPI_LOGD_VERBOSE(log_string, args...) do {  \
        char dltlog[DLT_USER_BUF_MAX_SIZE] =""; \
        snprintf(dltlog, DLT_USER_BUF_MAX_SIZE, log_string, ## args); \
        DLT_LOG(SPI_LOG_CLASS, DLT_LOG_VERBOSE, DLT_STRING(dltlog)); \
    }while(0);

#define LOG_DECLARE_CONTEXT(context) DltContext context;
#define LOG_IMPORT_CONTEXT(context) extern DltContext context;
#define LOG_REGISTER_APP(component, description) DLT_REGISTER_APP(component, description);
#define LOG_REGISTER_CONTEXT(context, subComponent, description)  DLT_REGISTER_CONTEXT(context, subComponent, description);
#define LOG_UNREGISTER_CONTEXT(context) DLT_UNREGISTER_CONTEXT(context);
#define LOG_UNREGISTER_APP() DLT_UNREGISTER_APP();


 #define ETG_TRACE_USR1(ARGS) SPI_LOG_INFO ARGS
 #define ETG_TRACE_USR2(ARGS) SPI_LOG_INFO ARGS
 #define ETG_TRACE_USR3(ARGS) SPI_LOGD_DEBUG ARGS
 #define ETG_TRACE_USR4(ARGS) SPI_LOGD_DEBUG ARGS
 #define ETG_TRACE_ERR(ARGS) SPI_LOG_ERROR ARGS
 #define ETG_TRACE_FATAL(ARGS) SPI_LOG_FATAL ARGS
 #define ETG_TRACE_ERRMEM(ARGS) SPI_LOG_WARN ARGS
 #define ETG_ENUM(ARG1,ARG2) ARG2

//! If nothing is defined ,by default enable target build
#else
#define TARGET_BUILD
#define ETRACE_S_IMPORT_INTERFACE_GENERIC
   #define ET_TRACE_INFO_ON
   #include "etrace_if.h"
   #include "spi_trace_macros.h"

#endif //end of if else

//! Functions for tracing Asserts
/***************************************************************************
** FUNCTION:  void vFunctionTracer
***************************************************************************/
/*!
* \fn      void vFunctionTracer
* \brief   USed for tracing asserts on failure
* \param   bResult : result of condition check
* \param   u32LineNo : Line number
* \param   czFileName : File name
* \param   czArg1 : Argument (condition check)
**************************************************************************/
void  vFunctionTracer(bool bResult, unsigned int u32LineNo, const char *czFileName, const char *czArg1);

/***************************************************************************
** FUNCTION:  void vFunctionTracer
***************************************************************************/
/*!
* \fn      void vFunctionTracer
* \brief   USed for tracing asserts on failure
* \param   u32LineNo : Line number
* \param   czFileName : File name
**************************************************************************/
void  vFunctionTracer(unsigned int u32LineNo, const char *czFileName);

//! Macros for tracing assserts
#define SPI_NORMAL_ASSERT(ARG)           vFunctionTracer((ARG),__LINE__,__FILE__, #ARG);

#define SPI_NORMAL_ASSERT_ALWAYS()       vFunctionTracer(__LINE__,__FILE__);

#endif //#ifndef TRACE_H_
