
/***********************************************************************/
/*!
 * \file  TraceStreamable.h
 * \brief Generic message Sender
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Message sender
   AUTHOR:         VIJETH
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      12.11.2013  | VIJETH      | Initial Version

\endverbatim
 *************************************************************************/

#ifndef TRACESTREAMABLE_H_
#define TRACESTREAMABLE_H_

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/

#include<map>

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include <common_fi_if.h>

#include "TraceCmdFunctor.h"

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define SPI_C_U16_TRACE_STREAM_FID                 0x010C
#define SPI_C_U16_SRV_TRACE_LOOPBACK               0xFFFE
#define SPI_C_U8_OPCODE_LOOPBACK                   CCA_C_U8_OPCODE_METHODSTART

/******************************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| forward declarations (scope: global)
|----------------------------------------------------------------------------*/

class ahl_tclBaseOneThreadApp;
class fi_tclMessageBase;
namespace spi {
   namespace spitrace {

/*!
 * \class TraceStreamable
 * \brief Trace Command Streamable class - Abstract base class.
 * This class provides a trace command dispatcher interface.
 * The dispatcher mechanism is generic as this depends on the command mapping,
 * which has to be performed by the concrete class by deriving from this base
 * class.
 * The derived class uses the vSetupCmds() interface to setup the commands which
 * need to be supported.
 */

class TraceStreamable
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  TraceStreamable::TraceStreamable(ahl_tclB..
   ***************************************************************************/
   /*!
    * \brief   Parameterized Constructor, based on Dependency Injection
    *          Principle (DIP)
    * \param   [cpoApp]:        (->I) Pointer to the main application
    * \retval  NONE
    **************************************************************************/
   explicit TraceStreamable(ahl_tclBaseOneThreadApp* const cpoApp
      , tU16 u16MajorVer FI_DEFAULT_VERSION);

   /***************************************************************************
   ** FUNCTION:  virtual TraceStreamable::~TraceStreamable()
   ***************************************************************************/
   /*!
    * \brief   Destructor
    * \param   NONE
    * \retval  NONE
    **************************************************************************/
   virtual ~TraceStreamable();

   /***************************************************************************
   ** FUNCTION:  tBool TraceStreamable::bStream(tU8 const* const cpu..
   ***************************************************************************/
   /*!
    * \brief   Streamer interface which performs the mapping based on the input
    *          trace stream provided.
    * \param   [cpu8Buffer]:   (->I) Input trace channel buffer.
    * \retval  [tBool]: TRUE if streaming was successful, FALSE otherwise.
    **************************************************************************/
   tBool bStream(tU8 const* const cpu8Buffer);

   /***************************************************************************
   ** FUNCTION:  virtual tVoid TraceStreamable::vSetupCmds() = 0
   ***************************************************************************/
   /*!
    * \brief   The function sets up the commands supported via trace.
    *          This is a virtual function which has to be overloaded by
    *          deriving, to provide a component specific implementation.
    *          This is used for mapping trace commands provided via the Trace
    *          input channel interface.
    * \param   NONE
    * \retval  NONE
    **************************************************************************/
   virtual tVoid vSetupCmds() {};

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:

   /***************************************************************************
   *******************************PROTECTED************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  tVoid TraceStreamable::vAddCmd(tU16 u16Cmd, spi_..
   ***************************************************************************/
   /*!
    * \brief   Adds the Command & Command functor to the stream map.
    * \param   [u16Cmd]:         (I) Trace Command
    * \param   [poCmdFunctor]:   (->I) Command Functor
    * \retval  NONE
    **************************************************************************/
   tVoid vAddCmd(tU16 u16Cmd, CmdFunctor* poCmdFunctor);

   /***************************************************************************
   ** FUNCTION:  tBool TraceStreamable::bSendMsg(const fi_tclType..
   ***************************************************************************/
   /*!
    * \brief   Posts a Self message
    * \param   [rfcoMsgBase]:   (I) Fi Message Base type object
    * \retval  [tBool]: TRUE if message post is successful, FALSE otherwise.
    **************************************************************************/
   tBool bSendMsg(const fi_tclMessageBase& rfcoMsgBase) const;

   /***************************************************************************
   ** FUNCTION:  tBool TraceStreamable::bTraceStream(tU8 const* const ..
   ***************************************************************************/
   /*!
    * \brief   Function to directly stream the bytes from Trace to component
    *          through
    * \param   [rfcoTypeBase]:   (I) Fi Base type object
    * \retval  [tBool]: TRUE if message post is successful, FALSE otherwise.
    **************************************************************************/
   tBool bTraceStream(tU8 const* const cpu8Buffer) const;

   /***************************************************************************
   ** FUNCTION:  TraceStreamable::TraceStreamable()
   ***************************************************************************/
   /*!
    * \brief   Default Constructor
    * \param   NONE
    * \retval  NONE
    * \note    Default constructor is declared protected to disable it. So
    *          that any attempt to create without parameter will be caught by
    *          the compiler.
    **************************************************************************/
   TraceStreamable();   // No definition exists.

   /***************************************************************************
   ** FUNCTION:  TraceStreamable::TraceStreamable(const audc..
   ***************************************************************************/
   /*!
    * \brief   Copy Constructor
    * \param   [rfcoTraceStrm]: (I) Const reference to object to be copied
    * \retval  NONE
    * \note    Default copy constructor is declared protected to disable it. So
    *          that any attempt to copy will be caught by the compiler.
    **************************************************************************/
   TraceStreamable(const TraceStreamable& rfcoTraceStrm);

   /***************************************************************************
   ** FUNCTION:  TraceStreamable& TraceStreamable::operator=()
   ***************************************************************************/
   /*!
    * \brief   Assignment Operator
    * \param   [rfcoTraceStrm]: (I) Const reference to object to be copied
    * \retval  [TraceStreamable&]: Reference to this pointer.
    * \note    Assignment operator is declared protected to disable it. So
    *          that any attempt for assignment will be caught by the compiler.
    **************************************************************************/
   TraceStreamable& operator=(const TraceStreamable& rfcoTraceStrm);

   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /*!
    * \typedef map<tU16, CmdFunctor*> spi_tCmdMapper
    * \brief Trace command to Functor Mapper data structure.
    */

   typedef std::map<tU16, CmdFunctor*> spi_tCmdMapper;

   /*!
    * \typedef map<tU16, CmdFunctor*>::iterator spi_tCmdMapIter
    * \brief Trace command to Functor Mapper data structure iterator.
    */
   typedef std::map<tU16, CmdFunctor*>::iterator spi_tCmdMapIter;

   /*!
    * \addtogroup tclMem
    */
   /*! @{*/

   /// Main application pointer.
   ahl_tclBaseOneThreadApp* const m_coMainApp;

   /// Trace Streamer Mapper object.
   spi_tCmdMapper m_oCmdMapper;

   /// Major version of FI
   tU16 m_u16MajorVer;

   /*! @}*/

   /***************************************************************************
   ****************************END OF PRIVATE**********************************
   ***************************************************************************/
}; // class TraceStreamable

   }  // namespace spitrace
}  // namespace spi

#endif   // #ifndef TRACESTREAMABLE_H_

////////////////////////////////////////////////////////////////////////////////
// <EOF>
