/***********************************************************************/
/*!
* \file  TraceCmdFunctor.h
* \brief Generic message Sender
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message sender
AUTHOR:         VIJETH
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
11/12/2013  | VIJETH      | Initial Version

\endverbatim
*************************************************************************/

#ifndef TRACECMDFUNCTOR_H_
#define TRACECMDFUNCTOR_H_

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/
#include "SPITypes.h"
/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| forward declarations (scope: global)
|----------------------------------------------------------------------------*/

namespace spi {
   namespace spitrace {

      /*!
      * \class CmdFunctor
      * \brief Command Functor - Abstract base class.
      *
      * Functors are functions with a state.Functors can encapsulate C and C++
      * function pointers employing the concepts templates and polymorphism.
      * Functors can be used to build up a list of pointers to member functions of
      * arbitrary classes and call them all through the same interface without
      * bothering about the need of a pointer to an instance or their class.
      * All the functions should have the same return-type and calling parameters.
      * Functors are also known as closures or Functionoids. Functors can be used to
      * implement callbacks.
      */

      class CmdFunctor {
      public:
         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

         /***************************************************************************
         ** FUNCTION:  CmdFunctor::CmdFunctor()
         ***************************************************************************/
         /*!
         * \brief   Default Constructor
         * \param   NONE
         * \retval  NONE
         **************************************************************************/
         CmdFunctor() {
         }
         ;

         /***************************************************************************
         ** FUNCTION:  virtual CmdFunctor::~CmdFunctor()
         ***************************************************************************/
         /*!
         * \brief   Destructor
         * \param   NONE
         * \retval  NONE
         * \date 17.11.2010 \author Pradeep Chand (CM-AI/PJ-GM55 RBEI)
         **************************************************************************/
         virtual ~CmdFunctor() {
         }
         ;

         /***************************************************************************
         ** FUNCTION:  virtual tBool CmdFunctor::operator()(tU8 const* ..
         ***************************************************************************/
         /*!
         * \brief   The function executes the callback function.
         *          This is a pure virtual function which has to be overloaded by
         *          deriving, to provide an implementation.
         *          This is used by the Trace input channel interface.
         * \param   [cpu8Buffer]: (->I)  Pointer to the input buffer coming from
         *          Trace input channel.
         * \retval  [tBool]: Status of callback mechanism.
         **************************************************************************/
         virtual tBool operator()(tU8 const* const cpu8Buffer) const = 0;
         /***************************************************************************
         ****************************END OF PUBLIC***********************************
         ***************************************************************************/
      }; // class CmdFunctor

      /*!
      * \class TraceCmdFunctor
      * \brief Generic template class implementing the callback mechanism.
      *
      */

      template<class spi_tclGenericClass>
      class TraceCmdFunctor: public CmdFunctor {
      public:

         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

         /*!
         * \typedef  tBool (spi_tclGenericClass::*spi_pfFuncCallbk)(tU8 const* const cpu8Buffer) const
         * \brief Callback function signature definition.
         */
         typedef tBool (spi_tclGenericClass::*spi_pfFuncCallbk)(
            tU8 const* const cpu8Buffer) const;

         /***************************************************************************
         ** FUNCTION:  TraceCmdFunctor(spi_tclGenericClass* poCmd, ihl_..
         ***************************************************************************/
         /*!
         * \brief   Parameterized Constructor - Dependency Injection Principle (DIP)
         * \param   [poCmd]:       (->I) Command Object pointer.
         * \param   [pfCallbk]:    (->I) Function pointer for callback.
         * \retval  NONE
         **************************************************************************/
         TraceCmdFunctor(spi_tclGenericClass* poCmd,
            spi_pfFuncCallbk pfCallbk) :
         m_poCmd(poCmd), m_pfCallbk(pfCallbk) {
         }
         ;

         /***************************************************************************
         ** FUNCTION:  virtual tBool CmdFunctor::operator()(tU8 const* cons..
         ***************************************************************************/
         /*!
         * \brief   The function executes the callback function.
         *          Callback function is called from here using the command object.
         * \param   [cpu8Buffer]: (->I)  Pointer to the input buffer coming from
         *          Trace input channel.
         * \retval  [tBool]: Status of callback mechanism.
         * \date 17.11.2010 \author Pradeep Chand (CM-AI/PJ-GM55 RBEI)
         **************************************************************************/
         virtual tBool operator()(tU8 const* const cpu8Buffer) const {
            tBool bRetVal = FALSE;

            bRetVal = (*m_poCmd.*m_pfCallbk)(cpu8Buffer); // Function callback.

            return bRetVal;
         }
         ;

         /***************************************************************************
         ****************************END OF PUBLIC***********************************
         ***************************************************************************/

      protected:

         /***************************************************************************
         *******************************PROTECTED************************************
         ***************************************************************************/

         /***************************************************************************
         ** FUNCTION:  TraceCmdFunctor::TraceCmdFunctor()
         ***************************************************************************/
         /*!
         * \brief   Default Constructor
         * \param   NONE
         * \retval  NONE
         * \note    Default constructor is declared protected to disable it. So
         *          that any attempt to instantiate w/o parameter will be caught by
         *          the compiler.
         **************************************************************************/
         TraceCmdFunctor(); // No definition exists.

         /***************************************************************************
         ****************************END OF PROTECTED********************************
         ***************************************************************************/

      private:

         /***************************************************************************
         *********************************PRIVATE************************************
         ***************************************************************************/

         /*!
         * \addtogroup tclMem
         */
         /*! @{*/

         /// Command Object pointer
         spi_tclGenericClass* m_poCmd;

         /// Function pointer for callback
         spi_pfFuncCallbk m_pfCallbk;

         /*! @}*/

         /***************************************************************************
         ****************************END OF PRIVATE**********************************
         ***************************************************************************/

      }; // class TraceCmdFunctor : public CmdFunctor

   } // namespace loopback
} // namespace spi

#endif   // #ifndef TRACECMDFUNCTOR_H_
////////////////////////////////////////////////////////////////////////////////
// <EOF>
