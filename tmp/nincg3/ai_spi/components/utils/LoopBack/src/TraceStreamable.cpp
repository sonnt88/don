
/***********************************************************************/
/*!
 * \file  TraceStreamable.cpp
 * \brief Generic message Sender
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Message sender
   AUTHOR:         VIJETH
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      12.11.2013  | VIJETH      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include "TraceStreamable.h"

#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include <common_fi_if.h>

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include <generic_msgs_if.h>

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include <ahl_if.h>

#include <midw_fi_if.h>
#include <utility>
/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/
#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/TraceStreamable.cpp.trc.h"
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

namespace spi {
   namespace spitrace {

/******************************************************************************
** FUNCTION:  TraceStreamable::TraceStreamable(ahl_tc..
******************************************************************************/

/*explicit*/
TraceStreamable::TraceStreamable
(
     ahl_tclBaseOneThreadApp *const cpoApp
   , tU16 u16MajorVer
):m_coMainApp(cpoApp), m_oCmdMapper(), m_u16MajorVer(u16MajorVer)
{}

/******************************************************************************
** FUNCTION:  virtual TraceStreamable::~spi_tclTraceStreamab..
******************************************************************************/

/*virtual*/
TraceStreamable::~TraceStreamable()
{
   try //_BP_TRY_BEGIN  // Currently disabled - try-catch construct giving Lint Prio1
   {
      for (spi_tCmdMapIter iterMap = m_oCmdMapper.begin();
         iterMap != m_oCmdMapper.end(); ++iterMap)
      {

        if(NULL !=iterMap->second)
        {
        	delete (iterMap->second);
        	iterMap->second = NULL;
        }
      }

      m_oCmdMapper.clear();
   }    // _BP_TRY_BEGIN
   catch(...)//_BP_CATCH_ALL
   {
   }
   // _BP_CATCH_ALL
   //_BP_CATCH_END

}  // TraceStreamable::~TraceStreamable()

/******************************************************************************
** FUNCTION:  tBool TraceStreamable::bStream(tU8 const* const cpu..
******************************************************************************/

tBool TraceStreamable::bStream(tU8 const* const cpu8Buffer)
{
   tBool bRetVal  =  FALSE;
   //! Concatinating the two buffer to get the Function ID for mapping
   tU16 u16Cmd    =  ((tU16)cpu8Buffer[2]|((tU16)cpu8Buffer[1]<<8));
   
   ETG_TRACE_USR4(("searching for the u16Cmd in the map %u",u16Cmd));
   spi_tCmdMapIter iterMap = m_oCmdMapper.find(u16Cmd);

   if (m_oCmdMapper.end() != iterMap)
   {
      bRetVal  =  (*(iterMap->second))(cpu8Buffer);
   }  // if(m_oCmdMapper.end() != iterMap)
   else
   {
      // Trace streamer did not find any handler function
      // so using default trace streamer loopback.
      ETG_TRACE_USR4(("Using default trace stream loopback.."));
      ETG_TRACE_USR4(("u16Cmd value = %u",u16Cmd));
      bRetVal  =  bTraceStream(cpu8Buffer);

   }  // End of if-else; if(m_oCmdMapper.end() != iterMap)
   

   return bRetVal;

}  // tBool ihl_tclTraceStreamable::bStream(tU8 const* const cpu8Buffer)

/******************************************************************************
** FUNCTION:  tVoid TraceStreamable::vAddCmd(tU16 u16Cmd, ipodau..
******************************************************************************/

tVoid TraceStreamable::vAddCmd
(
   tU16 u16Cmd
   , CmdFunctor* poCmdFunctor
)
{
	ETG_TRACE_USR4(("TraceStreamable::vAddCmd"));
   spi_tCmdMapIter iterMap = m_oCmdMapper.find(u16Cmd);

   // Command not found, hence add.
   if (m_oCmdMapper.end() != iterMap)
   {
      // Command already exists in the map.
      ETG_TRACE_ERR(("Command already exists in the trace loopback streamer."));
   }
   else
   {
	   ETG_TRACE_USR4(("making pair for u16Cmd = %u ",u16Cmd));
	   ETG_TRACE_USR4(("Creating a pair for service and loopback function"));
      (tVoid)m_oCmdMapper.insert(std::make_pair(u16Cmd, poCmdFunctor));
   }
}  // tVoid TraceStreamable::vAddCmd(tU16 u16Cmd, spi_tclCmdF..


/******************************************************************************
** FUNCTION:  tBool TraceStreamable::bSendMsg(const fi_tclMessageBase..
******************************************************************************/

tBool TraceStreamable::bSendMsg(const fi_tclMessageBase& rfcoMsgBase) const
{
   tBool bRetVal  =  TRUE;

   fi_tclVisitorMessage oMsg(rfcoMsgBase.corfoGetTypeBase(), m_u16MajorVer);

   oMsg.vInitServiceData
   (
      m_coMainApp->u16GetAppId()                // Source app-ID
      , m_coMainApp->u16GetAppId()              // Dest. app-ID
      , AMT_C_U8_CCAMSG_STREAMTYPE_NODATA       // Stream type
      , 0                                       // Stream counter
      , 0                                       // Reg ID
      , 0                                       // Command counter
      , rfcoMsgBase.u16GetServiceID()           // Service-ID
      , rfcoMsgBase.u16GetFunctionID()          // Function-ID
      , rfcoMsgBase.u8GetOpCode()               // OpCode
   );

   if (TRUE == oMsg.bIsValid())
   {
      ETG_TRACE_USR4(("Posting self message."));

      ail_tenCommunicationError enCommError =
         m_coMainApp->enPostMessage(&oMsg, TRUE);

      if (AIL_EN_N_NO_ERROR != enCommError)
      {
         ETG_TRACE_ERR(("Posting the self message returned an error: %d"
            , ETG_ENUM(AIL_ERROR, enCommError)));
         bRetVal  =  FALSE;
      }
   }
   else
   {
      ETG_TRACE_ERR(("Self message is not valid"));
      bRetVal  =  FALSE;
   }

   return bRetVal;
}  // tBool TraceStreamable::bSendMsg(const fi_tclTypeBase &rfcoTyp..

/*******************************************************************************
** FUNCTION:  tBool TraceStreamable::bTraceStream(tU8 const* const ..
*******************************************************************************/

tBool TraceStreamable::bTraceStream(tU8 const* const cpu8Buffer) const
{
   tBool bRetVal  =  TRUE;

   // Create a Stream message.
   gm_tclStreamMessage oMsg
      (
         m_coMainApp->u16GetAppId()                // Source app-ID
         , m_coMainApp->u16GetAppId()              // Dest. app-ID
         , 0                                       // Reg ID
         , 0                                       // Command counter
         , SPI_C_U16_TRACE_STREAM_FID            // ServiceId
         , SPI_C_U16_SRV_TRACE_LOOPBACK              // FID
         , SPI_C_U8_OPCODE_LOOPBACK                // Opcode
         , cpu8Buffer[0]                           // Buffer Size
      );

   // Set the data from trace into the stream.
   oMsg.vSetData((const tChar*)(&cpu8Buffer[1]));

   if (TRUE == oMsg.bIsValid())
   {
      ETG_TRACE_USR4(("Posting Trace stream self message."));

      ail_tenCommunicationError enCommError =
         m_coMainApp->enPostMessage(&oMsg, TRUE);

      if (AIL_EN_N_NO_ERROR != enCommError)
      {
         ETG_TRACE_ERR(("Posting Trace stream returned an error: %d"
            , ETG_ENUM(AIL_ERROR, enCommError)));
         bRetVal  =  FALSE;
      }
   }
   else
   {
      ETG_TRACE_ERR(("Self message is not valid"));
      bRetVal  =  FALSE;
   }

   return bRetVal;

}  // tBool ihl_tclTraceStreamable::bTraceStream(tU8 const* const cpu8Buffer) ..


   }  // namespace loopback
}  // namespace spi

////////////////////////////////////////////////////////////////////////////////

// <EOF>


