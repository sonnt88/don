/***********************************************************************/
/*!
 * \file   RandomIdGenerator.h
 * \brief  RandomIdGenerator
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    RandomIdGenerator
   AUTHOR:         Dhiraj Asopa
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      29.10.2015  | Dhiraj Asopa          | Initial Version

\endverbatim
 *************************************************************************/

#ifndef _RANDOMIDGENERATOR_H_
#define _RANDOMIDGENERATOR_H_

#include "SPITypes.h"


class RandomIdGenerator
{
public:

  /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  RandomIdGenerator::RandomIdGenerator()
   ***************************************************************************/
  /*!
   * \fn      RandomIdGenerator()
   * \brief   Default Constructor
   * \sa      ~RandomIdGenerator()
   ***************************************************************************/
  RandomIdGenerator();

  /***************************************************************************
   ** FUNCTION:  RandomIdGenerator::RandomIdGenerator()
   ***************************************************************************/
  /*!
   * \fn      RandomIdGenerator()
   * \brief   Destructor
   * \sa      RandomIdGenerator()
   ***************************************************************************/
  virtual ~RandomIdGenerator();

  /***************************************************************************
   ** FUNCTION:  t_String RandomIdGenerator::szgenerateRandomId()
   ***************************************************************************/
  /*!
   * \fn      szgenerateRandomId()
   * \brief   This function is implemented to generate a random 16 bytes of string data which can be used as random Id.
   * \param   None.
   * \retval  t_String: returns a string with 16 bytes of data.
   ***************************************************************************/
  t_String szgenerateRandomId();

  /***************************************************************************
   ** FUNCTION:  t_String RandomIdGenerator::szgenerateRandomIdBasedOnUrandom()
   ***************************************************************************/
  /*!
   * \fn      szgenerateRandomIdBasedOnUrandom()
   * \brief   This function is implemented to generate a random 16 bytes of string data which can be used as random Id.
   * \param   None.
   * \retval  t_String: returns a string with 16 bytes of data.
   ***************************************************************************/
  t_String szgenerateRandomIdBasedOnUrandom();

  /***************************************************************************
   ** FUNCTION:  t_String RandomIdGenerator::szgenerateRandomIdBasedSysTime()
   ***************************************************************************/
  /*!
   * \fn      szgenerateRandomIdBasedSysTime()
   * \brief   This function is implemented to generate a random 16 bytes of string data which can be used as random Id
   * using BT mac Address and system Time.
   * \param   None.
   * \retval  t_String: returns a string with 16 bytes of data.
   ***************************************************************************/
  t_String szgenerateRandomIdBasedSysTime();

  /***************************************************************************
   ** FUNCTION:  t_String RandomIdGenerator::szgenerateRandomIdBasedBTAddr()
   ***************************************************************************/
  /*!
   * \fn      szgenerateRandomIdBasedBTAddr()
   * \brief   This function is implemented to generate a random 16 bytes of string data which can be used as random Id
   *           using BT mac Address
   * \param   None.
   * \retval  t_String: returns a string with 16 bytes of data.
   ***************************************************************************/
  t_String szgenerateRandomIdBasedBTAddr();

  /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

private:

  /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


  /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif  // _RANDOMIDGENERATOR_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>

