/***********************************************************************/
/*!
 * \file  RandomIdGenerator.cpp
 * \brief Class for generating random ids.
 *************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Class for generating random ids.
AUTHOR:         Dhiraj Asopa
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
30.10.2015  | Dhiraj Asopa          | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include <RandomIdGenerator.h>
#include <FileHandler.h>
#include "Datapool.h"
#include "crc.h"
#include "StringHandler.h"
#include <time.h>
#include <ctime>
#include <algorithm>
#include <sys/time.h>
//! Includes for Trace files
#include "Trace.h"
#define CHARS_TO_BE_SKIPPED 10 //No. of Characters to be read from Bluetoothmacaddress after Appending system time.

#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/RandomIdGenerator.cpp.trc.h"
#endif
#endif

/***************************************************************************
 ** FUNCTION: RandomIdGenerator::RandomIdGenerator()
 ***************************************************************************/
RandomIdGenerator::RandomIdGenerator()
{
  ETG_TRACE_USR1(("RandomIdGenerator::RandomIdGenerator() entered \n"));
}

/***************************************************************************
 ** FUNCTION: RandomIdGenerator::~RandomIdGenerator()
 ***************************************************************************/
RandomIdGenerator::~RandomIdGenerator()
{
  ETG_TRACE_USR1((" RandomIdGenerator::~RandomIdGenerator() entered \n"));
}

/***************************************************************************
 ** FUNCTION:  t_String RandomIdGenerator::szgenerateRandomIdBasedOnUrandom()
 ** This function is implemented to generate random Id from "/dev/urandom" file
 ** Note:This function was tested on GM iMAX Board and it was taking 22 seconds to
 ** find /dev/urandom and generate a random Id.That is why this function was not used.
 ** This function cannot be used if system is non root.
 ***************************************************************************/
t_String RandomIdGenerator::szgenerateRandomIdBasedOnUrandom()
{
   ETG_TRACE_USR1(("RandomIdGenerator::szgenerateRandomIdBasedOnUrandom entered"));
   t_String szSyscommand;//string to save system command to generate alpha numeric values
   t_String szRandomId; //string to save random string generated
   FILE * poStream = NULL;     //this is used to save the random id generated after system command
   const t_U32 cou32max_buffer =256;
   t_Char szbuffer[cou32max_buffer];
   szSyscommand="strings /dev/urandom | grep -o '[[:alnum:]]' | head -n 16 | tr -d \'\\n\'";
   ETG_TRACE_USR1(("Linux Command to generate Random Id = %s", szSyscommand.c_str()));
   //creating a buffer, opening up a read-only stream, running the command and capturing the output,
   //stuffs it into the buffer and returns it as a string.
   ETG_TRACE_USR1(("szgenerateRandomId function,POSTREAM entered"));
   poStream = popen(szSyscommand.c_str(), "r");
   ETG_TRACE_USR1(("szgenerateRandomId function,POSTREAM EXIT"));
   if(OSAL_NULL != poStream)
   {
      ETG_TRACE_USR1(("INSIDE POSTREAM"));
      while (fgets(szbuffer, cou32max_buffer, poStream) != NULL )
         {
            ETG_TRACE_USR1(("szgenerateRandomId function,WHILE loop entered"));
            szRandomId.append(szbuffer);
            ETG_TRACE_USR1(("Random String generated inside while loop = %s", szRandomId.c_str()));
         }
      ETG_TRACE_USR1(("szgenerateRandomId function,WHILE loop exit"));
      pclose(poStream);
   }
   ETG_TRACE_USR1(("Random String generated = %s", szRandomId.c_str()));
   return szRandomId;
}

/***************************************************************************
 ** FUNCTION:  t_String RandomIdGenerator::szgenerateRandomIdBasedSysTime()
 ** This function is implemented to generate random Id after
 ** appending System Time and Nano seconds
 ***************************************************************************/
t_String RandomIdGenerator::szgenerateRandomIdBasedSysTime()
{
   ETG_TRACE_USR1(("RandomIdGenerator::szgenerateRandomIdBasedSysTime entered"));
   time_t trresult = time(NULL);
   timespec trtimespec;
   memset(&trtimespec, 0, sizeof(timespec)); // Initialized to zero
   t_String szRandomId;
   timeval trtimeval;
   memset(&trtimeval, 0, sizeof(timeval)); // Initialized to zero
   t_String szcurrentTime(asctime(std::localtime(&trresult)));//Get System Current Time
   szcurrentTime.erase (szcurrentTime.begin()+CHARS_TO_BE_SKIPPED, szcurrentTime.end());
   gettimeofday(&trtimeval, 0);//The gettimeofday() function will obtain the current time, expressed as seconds and microseconds since the Epoch
   StringHandler oStringUtil;
   t_String szMicroseconds;//The number of microseconds expired in the current second
   t_String szNanoseconds;//The number of nanoseconds expired in the current second
   oStringUtil.vConvertIntToStr(trtimeval.tv_usec, szMicroseconds,DECIMAL_STRING);
   clock_gettime(CLOCK_REALTIME,&trtimespec);//gets the current time of the clock specified by clock_id, and puts it into the buffer pointed to by trtimespec
   oStringUtil.vConvertIntToStr(trtimespec.tv_nsec, szNanoseconds,DECIMAL_STRING);
   ETG_TRACE_USR2(("[DESC]::szgenerateRandomIdBasedSysTime received System Nano Seconds  = %s", szRandomId.c_str()));
   szRandomId=szNanoseconds+szcurrentTime;//appending System Time and Nano seconds
   szRandomId.append(szMicroseconds.c_str());//appending System Time and Micro seconds
   ETG_TRACE_USR2(("[DESC]::szgenerateRandomIdBasedSysTime received Random Id after appending system time ,microseconds and Nano Seconds  = %s", szRandomId.c_str()));
   szRandomId.erase(remove_if(szRandomId.begin(), szRandomId.end(), ::isspace), szRandomId.end());//Removing extra spaces from the string szRandomId
   szRandomId.erase(remove(szRandomId.begin(), szRandomId.end(), ':'), szRandomId.end());//Removing ":" from szRandomId
   //! Append 16 bit CRC
   ETG_TRACE_USR2(("[DESC]::szgenerateRandomIdBasedSysTime received Random Id after removing extra symbols  = %s", szRandomId.c_str()));
   t_U16 u16VehicleIdCRC = u16CalcCRC16((const t_UChar*) (szRandomId.c_str()),
   szRandomId.size());
   t_String szVehicleIdCrc;
   oStringUtil.vConvertIntToStr(u16VehicleIdCRC, szVehicleIdCrc);
   ETG_TRACE_USR2(("[DESC]::szgenerateRandomIdBasedSysTime received szVehicleIdCrc= %s", szVehicleIdCrc.c_str()));
   szRandomId = szRandomId + szVehicleIdCrc;
   if (0 == szRandomId.length())//In case random id string is empty ,calling other function to generate vehicle Id
   {
      szRandomId=szgenerateRandomIdBasedBTAddr();
   }
   ETG_TRACE_USR1((" t_String RandomIdGenerator::szgenerateRandomIdBasedSysTime left with Serial Number  = %s", szRandomId.c_str()));
   return szRandomId;
}

/***************************************************************************
 ** FUNCTION:  t_String RandomIdGenerator::szgenerateRandomIdBasedBTAddr()
 ** This function is implemented to generate random Id from BluetoothMacAddress alone
 ** Note:This was the previous implementation which was used to get vehicle Id.
 ***************************************************************************/
t_String RandomIdGenerator::szgenerateRandomIdBasedBTAddr()
{
   ETG_TRACE_USR1(("RandomIdGenerator::szgenerateRandomIdBasedBTAddr entered"));
   Datapool oDatapool;
   t_String szRandomId;
   oDatapool.bReadBluetoothMacAddress(szRandomId);//Get BlueTooth Mac Address
   //! Append 16 bit CRC
   t_U16 u16BTAddressCRC = u16CalcCRC16((const t_UChar*) (szRandomId.c_str()),
   szRandomId.size());
   StringHandler oStringUtil;
   t_String szBTAddressCrc;
   oStringUtil.vConvertIntToStr(u16BTAddressCRC, szBTAddressCrc);
   szRandomId = szRandomId + szBTAddressCrc;
   ETG_TRACE_USR1((" Serial number  = %s", szRandomId.c_str()));
   return szRandomId;
}

/***************************************************************************
 ** FUNCTION:  t_String RandomIdGenerator::szgenerateRandomVehicleId()
 ** This function is implemented to generate random Id
 ***************************************************************************/
t_String RandomIdGenerator::szgenerateRandomId()
{
   ETG_TRACE_USR1(("generateRandomId entered"));
   t_String szRandomVehicleId= szgenerateRandomIdBasedSysTime();
   return szRandomVehicleId;
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>




