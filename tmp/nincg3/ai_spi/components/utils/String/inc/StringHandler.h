
/***********************************************************************/
/*!
* \file  StringHandler.h
* \brief String Utitlity
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    String Handler
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version
25.06.2015  | Sameer Chandra        | Added s32ConvertStrToInt()

\endverbatim
*************************************************************************/
#ifndef STRINGHANDLER_H_
#define STRINGHANDLER_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include <vector>
#include <map>
#include <string.h>
#include <stdlib.h>

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
//! defines for itoa conversion
#define OCT_STRING 8
#define DECIMAL_STRING 10
#define HEX_STRING 16
#define MAX_KEYSIZE 256

/****************************************************************************/
/*!
* \class StringHandler
* \brief String Utility
*
* It provides methods to parse the t_Strings, atoi and itoa conversions.
****************************************************************************/
class StringHandler
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  StringHandler::StringHandler()
   ***************************************************************************/
   /*!
   * \fn      StringHandler()
   * \brief   Constructor
   * \param   tVoid
   * \sa      ~StringHandler()
   ***************************************************************************/
   StringHandler();

   /***************************************************************************
   ** FUNCTION:  StringHandler::StringHandler(std::string szStr)
   ***************************************************************************/
   /*!
   * \fn      StringHandler()
   * \brief   Parameterized
   * \param   tVoid
   * \sa      ~StringHandler()
   ***************************************************************************/
   StringHandler(std::string szStr);

   /***************************************************************************
   ** FUNCTION: StringHandler::~StringHandler()
   ***************************************************************************/
   /*!
   * \fn       ~StringHandler()
   * \brief   Destructor
   * \param   tVoid
   * \sa      StringHandler()
   ***************************************************************************/
   ~StringHandler();

   /***************************************************************************
   ** FUNCTION:  void StringHandler::vParseString(const char* cszStr,...
   ***************************************************************************/
   /*!
   * \fn      void vParseString(const char* pcocStr, char cDelim,
   *                               std::vector<unsigned int>& rfrvecList)
   * \brief   Splits a t_String into a vector of Integers separated by
   *          a delimiter. Empty t_Strings will not be added to the vector.
   *
   *          Application list will be recieved in the form of
   *          "appId1;appId2;appId3;..". This t_String will be parsed and stored
   *          to a vector of Integers.
   *
   * \param   cszStr        : [IN] t_String to be parsed
   * \param   cDelim        : [IN] delimiter to split the t_String
   * \param   rfrvecList    : [OUT] list of unsigned int integers after parsing
   * \retval  void
   * \sa      vSplitString()
   ***************************************************************************/
   void vParseString(
      const char* pcocStr,
      char cDelim,
      std::vector<unsigned int>& rfrvecList);
  
   /***************************************************************************
   ** FUNCTION:  void StringHandler::vSplitString(const char* pcocStr, ...
   ***************************************************************************/
   /*!
   * \fn      void vSplitString(const char* pcocStr,char cDelim,
   *                              std::vector<t_String>& rfrvecStrings)
   * \brief   Splits a t_String into a vector of t_Strings separated by
   *          a delimiter. Empty t_Strings will not be added to the vector.
   *
   *          The t_Strings which are recieved in the form of
   *          "appId1;appId2;appId3;..".,will be parsed and stored
   *          to a vector of Strings.
   *
   * \param   pcocStr          : [IN] t_String to be parsed
   * \param   cDelim           : [IN] delimiter to split the t_String
   * \param   rfrvecStrings    : [OUT] list of Strings after parsing
   * \retval  void
   * \sa      vParseString()
   ***************************************************************************/
   void vSplitString(
      const char* pcocStr,
      char cDelim,
      std::vector<std::string>& rfrvecStrings);

   /***************************************************************************
   ** FUNCTION:  void  StringHandler::vParseAppStatus(const char* pcoc...
   ***************************************************************************/
   /*!
   * \fn      void vParseAppStatus(const char* pcocStr,const char* pcocKey,
   *              const char* pcocVal,const char cDelim,const char cocSeperator,
   *              std::map<std::t_String,std::t_String>& rfmapAppStatus)
   * \brief   To parse an application status update.
   *
   *          Application status will be recieved in the from of
   *          "profileId=xx;statusType=FG;profileId=yy;statusType=BG;.."
   *          This t_String will be paresed and stored in map,
   *          where the profile ID is the Key and statusType is it's value.
   *
   * \note    Here cszKey is "profileId" and cszValue is "statusType".
   *          ';' delimiter differentiates the different client profile statuses and
   *          '=' seperator is used to mention the profile value and status value
   *          Ex: profileId=1;statusType=FG;profileId=2;statusType=BG...
   *
   * \param   pcocStr        : [IN] t_String to be parsed
   *          pcocKey        : [IN] key value, here profile Id
   *          pcocVal        : [IN] status of the profile
   *          cDelim         : [IN] separates different profile statuses
   *          cocSeperator   : [IN] separator between profile Id and it's value
   *                               and statusType and it's value
   *          rfmapAppStatus : [OUT] Map of statuses with associated profiles.
   * \retval  void
   * \sa      vParseAllAppsStatuses()
   ***************************************************************************/
   void vParseAppStatus(
      const char* pcocStr,
      const char* pcocKey,
      const char* pcocVal,
      const char cocDelim,
      const char cocSeperator,
      std::map<std::string,std::string>& rfmapAppStatus);

   /***************************************************************************
   ** FUNCTION:  void  StringHandler::vParseAllAppsStatuses(const char* ...
   ***************************************************************************/
   /*!
   * \fn      void vParseAllAppsStatuses(const char* pcocStr,const char* pcocKey,
   *          const char* pcocVal,const char cDelim,const char cocSeperator,
   *          std::map<unsigned int ,std::map<std::t_String,std::t_String> >& rfmapAllAppsStatuses)
   * \brief   To parse the All applications statuses updates recieved.
   *
   *          All Applications status update will be recieved in the form of
   *          "{ appId1: profileId=xxxx;statusType=xxxx;profileId=yyy;statusType=xxxx}
   *           {appId2: profileId=xxxx;statusType=xxxx}{appId3:...}".
   *          Each and every application can have multiple profiles and each profile
   *          will have it's own status. This t_String will be parsed and stored in a map,
   *          where the appId is key and profileId and statusType are the values associated
   *          with appId(key).
   *
   * \note    Here cszKey is "profileId" and cszValue is "statusType".
   *          ';' delimiter differentiates the different client profile statuses and
   *          '=' seperator is used to mention the profile value and status value.
   *          ':' used to differantiate app id ans it's status.
   *
   * \param   pcocStr              : [IN] t_String to be parsed
   *          pcocKey              : [IN] key value, here profile Id
   *          pcocVal              : [IN] status of the profile
   *          cDelim               : [IN] separates different profile statuses
   *          cocSeperator         : [IN] separator between profile Id and it's 
   *                                      value and statusType and it's value
   * \param   rfmapAllAppsStatuses : [OUT] Map of statuses of all apps.
   * \retval  void
   * \sa      vParseAppStatus()
   *****************************************************************************/
   void vParseAllAppsStatuses(
      const char* pcocStr,
      const char* pcocKey,
      const char* pcocVal,
      const char cocDelim, 
      const char cocSeperator,
      std::map<unsigned int ,std::map<std::string,std::string> >& rfmapAllAppsStatuses);

   /***************************************************************************
   ** FUNCTION:  unsigned int  StringHandler::u32ConvertStrToInt(const char* pco..
   ***************************************************************************/
   /*!
   * \fn      unsigned int u32ConvertStrToInt(const char* pcocValue)
   * \brief   To convert the t_String to Integer value
   * \param   pcocValue  : [IN] String to be converted
   * \retval  unsigned int      : Integer value after conversion
   * \sa      vConvertIntToStr()
   **************************************************************************/
   unsigned int u32ConvertStrToInt(const char* pcocValue);

   /***************************************************************************
   ** FUNCTION:  int  StringHandler::s32ConvertStrToInt(const char* pco..
   ***************************************************************************/
   /*!
   * \fn      int s32ConvertStrToInt(const char* pcocValue)
   * \brief   To convert the t_String to Integer value
   * \param   pcocValue  : [IN] String to be converted
   * \retval  int      : Integer value after conversion
   * \sa      vConvertIntToStr()
   **************************************************************************/
   int s32ConvertStrToInt(const char* pcocValue);

   /***************************************************************************
   ** FUNCTION:  unsigned int  StringHandler::u32ConvertStrToInt()
   ***************************************************************************/
   /*!
   * \fn      unsigned int u32ConvertStrToInt()
   * \brief   To convert the t_String to Integer value
   * \retval  unsigned int      : Integer value after conversion
   * \sa      vConvertIntToStr()
   **************************************************************************/
   unsigned int u32ConvertStrToInt();

   /***************************************************************************
   ** FUNCTION:  void  StringHandler::vConvertIntToStr(unsigned int u32Val,..
   ***************************************************************************/
   /*!
   * \fn      const char* vConvertIntToStr(unsigned int u32Val,
   *                 t_String& szAppId.unsigned int u32Radix=HEX_STRING)
   * \brief   Method to convert Integer value to Hex,Octal and decimal value
   *          t_Strings based on radix.
   *
   * \note    To convert Integer to Hex t_String : Ex: To convert to 100 to "0x64"
   *          send value 100 and u32Radix as 16 (HEX_STRING)
   *          In the same way, to convert it to octal t_String("0144",) u32Radix is OCT_STRING
   *          and to convert it to decimal("100"),u32Radix is DECIMAL_STRING.
   *
   * \param   u32Val     : [IN] Value to be converted to t_String
   * \param   szAppId    : [OUT] String after conversion
   * \param   u32Radix   : [IN] Radix
   * \retval  void
   * \sa      u32ConvertStrToInt(const char* pcocValue)
   **************************************************************************/
   void vConvertIntToStr(unsigned int u32Val,std::string& szAppId,unsigned int u32Radix = HEX_STRING);

   /***************************************************************************
   ** FUNCTION: t_Void StringHandler::vSetString(std::string szStr)
   ***************************************************************************/
   /*!
   * \fn      vSetString(std::string szStr)
   * \brief   Parameterized
   * \param   szStr : [IN] string to be set
   ***************************************************************************/
   t_Void vSetString(std::string szStr);

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

protected:


   /***************************************************************************
   *********************************PROTECTED**********************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PROTECTED********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ** FUNCTION:  std::t_String  StringHandler::szGetString(std::t_String szStr ..
   ***************************************************************************/
   /*!
   * \fn       std::t_String szGetString(t_String szStr, const char* pcocKey,
   *                  unsigned int& u32KeyPos,const char cocDelim,bool& bFlag)
   * \brief   To extract the t_String between the cszKey and delimiter,
   *          from the given key position. It extracts only the first t_String from
   *          the given key position in the szStr.
   * \param   szStr      : [IN] t_String to be parsed
   * \param   pcocKey    : [IN] Key value
   * \param   u32KeyPos  : [IN] list of t_Strings after parsing
   * \param   cocDelim   : [IN] delimiter to split the t_String
   * \param   bFlag      : [OUT] returns true,if the EOF t_String is reached.
   * \retval  std::t_String
   ***************************************************************************/
   std::string szGetString(std::string szStr, const char* pcocKey,
      unsigned int& u32KeyPos,const char cocDelim,bool& bFlag);

   //! member variable
   std::string m_szStr;


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif  // STRINGHANDLER_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
