/***********************************************************************/
/*!
* \file  StringHandler.cpp
* \brief String Handler
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    String Handler
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
23.09.2013  | Shiva Kumar Gurija    | Initial Version
20.11.2013  | Shiva Kumar Gurija    | Updated vConvertIntToStr()
25.06.2015  | Sameer Chandra        | Added s32ConvertStrToInt()

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "stdlib.h"
#include "string.h"
#include "StringHandler.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/StringHandler.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  StringHandler::StringHandler()
***************************************************************************/
StringHandler::StringHandler():m_szStr("")
{
   //add code
}

/***************************************************************************
** FUNCTION:  StringHandler::StringHandler(std::string szStr)
***************************************************************************/
StringHandler::StringHandler(std::string szStr):m_szStr(szStr)
{
   //add code
}

/***************************************************************************
** FUNCTION: StringHandler::~StringHandler()
***************************************************************************/
StringHandler::~StringHandler()
{
   //add code
}

   /***************************************************************************
   ** FUNCTION: t_Void StringHandler::vSetString(std::string szStr)
   ***************************************************************************/
t_Void StringHandler::vSetString(std::string szStr)
{
   m_szStr = szStr;
}
/***************************************************************************
** FUNCTION:  unsigned int  StringHandler::u32ConvertStrToInt(const char* pco..
***************************************************************************/
unsigned int StringHandler::u32ConvertStrToInt(const char* pcocValue)
{
   unsigned int u32RetVal = 0 ;

   if(pcocValue != NULL)
   {
      //Convert the String to Decimal Integer.
      u32RetVal = (unsigned int)strtoul(pcocValue, NULL, 0);
   }

   return u32RetVal;
}

/***************************************************************************
** FUNCTION:  unsigned int  StringHandler::u32ConvertStrToInt(const char* pco..
***************************************************************************/
unsigned int StringHandler::u32ConvertStrToInt()
{
   unsigned int u32RetVal = 0 ;

   if("" != m_szStr)
   {
      //Convert the String to Decimal Integer.
      u32RetVal = (unsigned int)strtoul(m_szStr.c_str(), NULL, 0);
   }//if("" != m_szStr)

   return u32RetVal;
}

/***************************************************************************
** FUNCTION:  int  StringHandler::s64ConvertStrToInt(const char* pco..
***************************************************************************/
int StringHandler::s32ConvertStrToInt(const char* pcocValue)
{
   int s32RetVal = 0 ;

   if(pcocValue != NULL)
   {
      //Convert the String to Decimal Integer.
      s32RetVal = strtoul(pcocValue, NULL, 0);
   }

   return s32RetVal;
}

/***************************************************************************
** FUNCTION:  void  StringHandler::vConvertIntToStr(unsigned int u32Val,..
***************************************************************************/
void StringHandler::vConvertIntToStr(
                                       unsigned int u32Val,
                                       std::string& rfrszStr,
                                       unsigned int u32Radix)
{
   char czBuf[MAX_KEYSIZE] = {0};
   bool bFlag = false;

   //Convert the Integer to std::string based on the Radix provided.
   //By default u32Radix is HEX_STRING.
   //It converts 100 to "0x64"
   switch(u32Radix)
   {
   case HEX_STRING:
      {
         //snprintf returns negative value if there is any error in writing.
         bFlag = ( 0 < snprintf(czBuf,MAX_KEYSIZE,"0x%x",u32Val)) ;
      }
      break;
   case DECIMAL_STRING:
      {
         //snprintf returns negative value if there is any error in writing.
         bFlag = ( 0 < snprintf(czBuf,MAX_KEYSIZE,"%d",u32Val));
      }
      break;
   case OCT_STRING:
      {
         //snprintf returns negative value if there is any error in writing.
         bFlag = ( 0 < snprintf(czBuf,MAX_KEYSIZE,"0%o",u32Val));
      }
      break;
   default:
      {
         ETG_TRACE_ERR(("vConvertIntToStr: Invalid conversion \n "));
      }
      break;
   }

   rfrszStr = bFlag?(czBuf):("");

}

/***************************************************************************
** FUNCTION:  void StringHandler::vParseString(const char* cszStr,...
***************************************************************************/
void StringHandler::vParseString(
                                   const char* pcocStr,
                                   char cDelim,
                                   std::vector<unsigned int>& rfrvecList)
{
   ETG_TRACE_USR1(("vParseString() entered \n"));
   //Clear the vector, if any contents
   rfrvecList.clear();

   if (NULL != pcocStr)
   {
      ETG_TRACE_USR2(("vParseString: %s \n",pcocStr));

      const char* pcocTemp = NULL;

      while (*pcocStr)
      {
         if (*pcocStr == cDelim)
         {
            // Delimiter is reached and pcocTemp is empty means, there is no t_String between delimiters
            //If the t_String is empty, skip this. - don't push it to vector
            if (NULL != pcocTemp)
            {
               //Delimiter is reached and cszTemp is not empty, extract the t_String.
               std::string str = std::string(pcocTemp, pcocStr);
               //Convert the t_String to Integer and push it to vector
               rfrvecList.push_back(u32ConvertStrToInt(str.c_str()));
               pcocTemp = NULL;
            }
         }
         else if (NULL == pcocTemp)
         {
            pcocTemp = pcocStr;
         }
         pcocStr++;
      }
      //After the last t_String, delimiter may not be present in the t_String.
      //In that case, convert the t_String in pcocTemp to integer and write to vector.
      //If pcocTemp is null, skip it - don't push it to vector.
      if (NULL != pcocTemp)
      {
         std::string str = std::string(pcocTemp, pcocStr);
         rfrvecList.push_back(u32ConvertStrToInt(str.c_str()));
         pcocTemp = NULL;
      }
   }

}

/***************************************************************************
** FUNCTION:  void StringHandler::vSplitString(const char* pcocStr, ...
***************************************************************************/
void StringHandler::vSplitString(
                                   const char* pcocStr,
                                   char cDelim,
                                   std::vector<std::string>& rfrvecStrings)
{
   ETG_TRACE_USR1(("vSplitString() entered \n"));
   //Clear the vector, if any contents
   rfrvecStrings.clear();

   if (NULL != pcocStr)
   {
      ETG_TRACE_USR2(("vSplitString: %s \n",pcocStr));

      const char* pcocTemp = NULL;

      while (*pcocStr)
      {
         if (*pcocStr == cDelim)
         {
            // Delimiter is reached and pcocTemp is empty means, there is no t_String between delimiters
            //If the t_String is empty, skip this. - don't push it to vector
            if (NULL != pcocTemp)
            {
               //Convert the t_String to Integer and push it to vector
               rfrvecStrings.push_back(std::string(pcocTemp, pcocStr));
               pcocTemp = NULL;
            }
         }
         else if (NULL == pcocTemp)
         {
            pcocTemp = pcocStr;
         }
         pcocStr++;
      }
      //After the last t_String, delimiter may not be present in the t_String.
      //In that case, convert the t_String in pcocTemp to integer and write to vector.
      //If pcocTemp is null, skip it - don't push it to vector.
      if (NULL != pcocTemp)
      {
         rfrvecStrings.push_back(std::string(pcocTemp, pcocStr));
         pcocTemp = NULL;
      }
   }

}


/***************************************************************************
** FUNCTION:  void  StringHandler::vParseAppStatus(const char* pcoc...
***************************************************************************/
void StringHandler::vParseAppStatus(
                                      const char* pcocStr,
                                      const char* pcocKey,
                                      const char* pcocVal,
                                      const char cocDelim,
                                      const char cocSeperator,
                                      std::map<std::string,std::string>& rfrAppStatus)
{
   ETG_TRACE_USR1(("vParseAppStatus() entered \n"));

   //Clear the map if any contents
   rfrAppStatus.clear();

   if(
      (NULL != pcocStr)
      &&
      (NULL != pcocKey)
      &&
      (NULL != pcocVal)
      )
   {
      ETG_TRACE_USR2(("vParseAppStatus: %s \n",pcocStr));

      unsigned int u32KeyPos = 0;
      bool bFlag=true;

      //Append the seperator to the key and values
      unsigned int u32Strlen = strlen(pcocKey);
      char* pcKey = new char[u32Strlen+2];

      if(NULL != pcKey)
      {
         strcpy(pcKey,pcocKey);

         pcKey[u32Strlen]=cocSeperator;
         pcKey[u32Strlen+1] = '\0';

         u32Strlen = strlen(pcocVal);
         char* pcVal = new char[u32Strlen+2];

         if(NULL != pcVal)
         {
            strcpy(pcVal,pcocVal);
            pcVal[u32Strlen]=cocSeperator;
            pcVal[u32Strlen+1]='\0';

            std::string szStr = pcocStr;

            while(bFlag)
            {
               //Find the position of the cszKey
               u32KeyPos = szStr.find(pcKey,u32KeyPos);
               if(u32KeyPos != std::string::npos )
               {
                  //Get the value between cszKey and the delimiter.
                  std::string szKeyValue = szGetString(szStr,pcKey,u32KeyPos,cocDelim,bFlag);

                  //Find the Position of cszVal
                  u32KeyPos = szStr.find(pcVal,u32KeyPos);

                  if(u32KeyPos != std::string::npos)
                  {
                     //Get the value between cszValue and delimiter.
                     std::string szTemp = szGetString(szStr,pcVal,u32KeyPos,cocDelim,bFlag);
                     //Insert the profile Id and statusType to Map
                     rfrAppStatus.insert( std::pair<std::string, std::string> (szKeyValue,szTemp));
                  }

                  else
                  {
                     //found czVal but it is empty, insert empty std::string to Map
                     rfrAppStatus.insert(std::pair<std::string, std::string>(szKeyValue,""));
                     //Next cszVal is not found, exit
                     bFlag=false;
                  }

               }
               else
               {
                  //Next cszKey is not found, exit
                  bFlag=false;
               }
            }//End of while
            delete[] pcVal;
            pcVal=NULL;
       }//End of if(NULL != pcVal)

       delete[] pcKey;
       pcKey = NULL;
      }//End of if(NULL != pcKey)
   }//End of if((NULL != pcocStr)&&...
}

/***************************************************************************
** FUNCTION:  std::string  StringHandler::szGetString(std::t_String szStr ..
***************************************************************************/
std::string StringHandler::szGetString(
                                    std::string szStr,
                                    const char* pcocKey,
                                    unsigned int& rfu32KeyPos,
                                    const char cocDelim,
                                    bool& bFlag)
{
   std::string szTemp;

   if(NULL != pcocKey)
   {
      unsigned int u32DelimPos;

      //Get the length of cszKey
      unsigned int u32Temp = rfu32KeyPos + strlen(pcocKey);

      //Find the delimter position
      u32DelimPos = szStr.find(cocDelim,rfu32KeyPos);

      if(u32DelimPos != std::string::npos)
      {
         //If the delimiter is found, extract the sub t_String between the key and delimiter.
         szTemp = szStr.substr(u32Temp,(u32DelimPos - u32Temp) );
      }
      else
      {
         // Delimiter may not be present at the end of the t_String,So if delimiter is
         // not found, extract the subt_String between key and end of the t_String.
         szTemp = szStr.substr(u32Temp,(szStr.length() - u32Temp));

         //Next delimiter is not found, exit.
         bFlag = false ;
      }
      // Assign the current position of the cursor/pointer of the t_String to Key position
      //to be used by the called function
      rfu32KeyPos = u32DelimPos ;
   }

   return szTemp;

}

/***************************************************************************
** FUNCTION:  void  StringHandler::vParseAllAppsStatuses(const char* ...
***************************************************************************/
void StringHandler::vParseAllAppsStatuses(
   const char* pcocStr,
   const char* pcocKey,
   const char* pcocVal,
   const char cocDelim,
   const char cocSeperator,
   std::map<unsigned int , std::map<std::string,std::string> >& rfrAllAppsStatuses)
{

   ETG_TRACE_USR1(("vParseAllAppsStatuses() entered\n"));
   //Clear the map if any contents
   rfrAllAppsStatuses.clear();

   if(
      (NULL != pcocStr)
      &&
      (NULL != pcocKey)
      &&
      (NULL != pcocVal)
      )
   {
      ETG_TRACE_USR2(("vParseAllAppsStatuses: %s\n",pcocStr));

      std::string szStr = pcocStr ;

      unsigned int u32Pos = 0 ;
      unsigned int u32DelimPos = 0;
      bool bFlag = true;

      while(bFlag)
      {
         //All Applications status t_String starts with '{'
         u32Pos = szStr.find('{',u32Pos);

         if(u32Pos != std::string::npos)
         {
            std::map<std::string, std::string> mapAppStatus;
            unsigned int u32AppId = 0;
            unsigned int u32Temp = u32Pos+1 ;

            //search for '}'
            unsigned int u32EndDelimPos = szStr.find('}',u32Pos);

            bool bVal = true;

            u32DelimPos = szStr.find(':',u32Pos);

            //If the position of '}' is lesser than ':', means that the ':' in the next application status update is pointed.
            //So just write the current app id to map and then repeat the same for the next application id.
            if(
               (std::string::npos != u32EndDelimPos)
               &&
               (u32EndDelimPos<u32DelimPos)
               )
            {
               bVal = false ;
            }

            if(
               (std::string::npos != u32DelimPos )
               &&
               (true == bVal )
               )
            {
               //Extract the appId present between '{' and ':' and convert it to Integer
               // Ex: { appID1:....} Extract appID1 here and convert it to integer.
               u32AppId = u32ConvertStrToInt((szStr.substr(u32Pos+1,u32DelimPos-u32Pos)).c_str());
               u32Pos = szStr.find('}',u32DelimPos );

               if(std::string::npos != u32Pos )
               {
                  u32Temp = u32DelimPos+1 ;
                  //parse the status update of the application
                  vParseAppStatus((szStr.substr(u32Temp,u32Pos-u32Temp).c_str()),pcocKey,pcocVal,cocDelim,cocSeperator,mapAppStatus);
               }
            }
            else
            {
               unsigned int u32EndPos = szStr.find('}',u32Pos );

               if(std::string::npos != u32EndPos )
               {
                  //Extract the appId present between '{' and '}' and convert it to Integer
                  //If the only appId is present between '{' and '}', it will be executed.
                  u32AppId = u32ConvertStrToInt((szStr.substr(u32Temp,u32EndPos-u32Pos)).c_str());
               }
               else
               {
                  //To store the appId when the end '}' is not present.Ex: "{ appId ...."
                  u32AppId = u32ConvertStrToInt(szStr.substr(u32Temp).c_str());
               }
               u32Pos = u32EndPos ;
            }
            //Insert the Application Id and it's status update to vector.
            rfrAllAppsStatuses.insert( std::pair<unsigned int , std::map<std::string,std::string> > (u32AppId ,mapAppStatus) );
         }
         else
         {
            //Next application status is not there or end of the sting reached. exit.
            bFlag = false;
         }
      }
   }

}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
