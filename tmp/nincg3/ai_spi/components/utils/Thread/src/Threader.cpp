/***********************************************************************/
/*!
 * \file  Threader.cpp
 * \brief Generic thread handling based on posix standard
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Thread Handling
   AUTHOR:         Priju K Padiyath
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      10.04.2013  | Priju K Padiyath      | Initial Version
      10.08.2013  | Shihabudheen P M      | Updated
      15.06.2015  | Sameer Chandra        | Fixed thread unsafe cases

\endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/

#include "Threader.h"

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/Threader.cpp.trc.h"
   #endif
#endif
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

namespace shl
{
   namespace thread
   {
      /*************************************************************************
       ** FUNCTION:  Threader::Threader(Threadable * const ...)
       *************************************************************************/
      Threader::Threader(Threadable * const cpoThreadable,
            bool bExecThread) :
            m_ThreadAlive(false), m_ThreadId(0), m_Threadable(cpoThreadable)
      {
         if (true == bExecThread)
         {
            bool bRetVal = bRunThread();
            if (false == bRetVal)
            {
               ETG_TRACE_ERR(
                     ("\nbRunThread failed to start the thread execution\n"));
            }
         }// if (true == bExecThread)
      }

      /*************************************************************************
       ** FUNCTION:  ~Threader()
       *************************************************************************/
      Threader::~Threader()
      {
      }

      /*************************************************************************
       ** FUNCTION:  bRunThread()
       *************************************************************************/
      bool Threader::bRunThread()
      {
         int u16status = -1;
         // Check whether the thread already created
         if (0 == m_ThreadId)
         {
            u16status = pthread_attr_init(&m_ThreadAttr);

            //Create Join able threads by default.
            u16status = pthread_create(&m_ThreadId, &m_ThreadAttr,
                  (void* (*)(void*)) &(Threader::vStartThread), (void *)this);
         }
         else
         {
            ETG_TRACE_ERR(("\n Thread is already created and is in runnig stage"));
         }
         return (0 == u16status);
      }

      /*************************************************************************
       ** FUNCTION: vSetThreadName(const char* czName)
       *************************************************************************/
      void Threader::vSetThreadName(const char* czName)
      {
         if(NULL!= czName)
         {
            ETG_TRACE_USR1(("\n Thread name is set to set to %s",czName));
            pthread_setname_np(m_ThreadId, czName);
         }
      }

      /*************************************************************************
       ** FUNCTION: vStartThread(void *pvArg)
       *************************************************************************/
      void Threader::vStartThread(void *pvArg)
      {
         //check for null pointer
         if (NULL != pvArg)
         {
            Threader* poThreader = (Threader*)(pvArg);
            //Execute the thread function
			poThreader->vExecute();
         }
         else //if(NULL == pvArg)
         {
            SPI_NORMAL_ASSERT_ALWAYS();
         } // if (NULL != pvArg)
      }

      /*************************************************************************
       ** FUNCTION: pGetThreadID()
       *************************************************************************/
      pthread_t Threader::pGetThreadID()
      {
         //Send the pthread_t id for the current thread
         return m_ThreadId;
      }

      /*************************************************************************
       ** FUNCTION: vWaitForThreads()
       *************************************************************************/
      void Threader::vWaitForTermination(pthread_t threadId)
      {
         pthread_join(threadId,NULL);
      }
   } // end of thread
} // end of shl
