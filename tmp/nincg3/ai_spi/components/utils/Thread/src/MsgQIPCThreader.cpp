/***********************************************************************/
/*!
* \file  MsgQIPCThreader.cpp
* \brief Generic thread handling based on posix standard
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Thread Handling
AUTHOR:         Priju K Padiyath
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
10.02.2014  | Priju K Padiyath      | Initial Version
11.06.2015  | Sameer Chandra        | pthread_exit() removed form vExecute()
                                            function

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <cstdio>
#include <cstring>
#include "UniqueName.h"
#include "MsgQIPCThreader.h"

using namespace std;

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/MsgQIPCThreader.cpp.trc.h"
#endif
#endif
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

namespace shl
{
   namespace thread
   {

      /*************************************************************************
      ** FUNCTION:   MsgQIPCThreader::MsgQIPCThreader(tclMsgQThreadable *  ..)
      *************************************************************************/
      MsgQIPCThreader::MsgQIPCThreader(const char *name,MsgQThreadable * const cpoThreadable,
         bool bExecThread = true) :
      Threader((Threadable * const ) cpoThreadable, false), m_cpoMsgQThreadable(
         cpoThreadable)
      {
         ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
         const char *mQName = name;

         // Create the message queue
         m_poMessageQueue = new IPCMessageQueue(mQName,O_RDWR | O_CREAT);
         // null pointer check
         if ((NULL != m_poMessageQueue) && (true == bExecThread))
         {
            bool bRetVal = bRunThread();
            if(false == bRetVal)
            {
               ETG_TRACE_ERR(("\nbRunThread failed to start the thread execution\n"));
            } //if(false == bRetVal)
         }
         else
         {
            ETG_TRACE_ERR(("\n Message queue is not created successfully or thread not started"));
         }//if ((NULL != m_poMessageQueue) && (true == bExecThread))
      }

      /*************************************************************************
      ** FUNCTION:    MsgQIPCThreader::~MsgQIPCThreader()
      *************************************************************************/
      MsgQIPCThreader::~MsgQIPCThreader()
      {
         ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

         if(NULL != m_poMessageQueue)
         {
            m_poMessageQueue->vCloseIPCMsgQueue();
            delete m_poMessageQueue;
            m_poMessageQueue = NULL;
         }
      }

      /*************************************************************************
      ** FUNCTION:  void MsgQIPCThreader::vExecute()
      *************************************************************************/
      void MsgQIPCThreader::vExecute()
      {
         ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
         m_ThreadAlive = true;
         size_t msgSize = 0;
         // enum msgType messageType;

         void* poMessage = NULL;
         tShlMessage *poInternalMsg = NULL;
         eMessageType eMsgType = TCL_DATA_MESSAGE;

         // Loop in while thread is alive
         while ((true == m_ThreadAlive) && (NULL != m_poMessageQueue))
         {
            // wait for the message
            poMessage = m_poMessageQueue->vpWaitMessage(&msgSize,RUN_FOR_EVER,eMsgType);
            // null pointer check
            if (NULL != poMessage)
            {
               if(TCL_THREAD_TERMINATE_MESSAGE != eMsgType)
               {
                  ETG_TRACE_USR2(("MsgQIPCThreader::vExecute() Message type = MSG_DATA "));
                  // to get the message from implementation
                  poInternalMsg = m_cpoMsgQThreadable->poGetMsgBuffer(msgSize);
                  // null pointer and out of bound checking
                  if( (NULL != poInternalMsg) && ((size_t)poInternalMsg->size >= msgSize) )
                  {
                     ETG_TRACE_USR2(("MsgQIPCThreader::vExecute() call vOnMessage "));
                     // copy message from queue to user defined space
                     memcpy(poInternalMsg->pvBuffer, poMessage, msgSize );
                     poInternalMsg->size = msgSize;
                     vOnMessage(poInternalMsg);
                     // delete the message
                     m_poMessageQueue->vDropMessage(poMessage);
                  }
                  else
                  {
                     ETG_TRACE_ERR(("\npoGetMsgBuffer() return NULL"));
                  }
               } //  if(TCL_THREAD_TERMINATE_MESSAGE != eMsgType)
               else
               {
                  ETG_TRACE_USR2(("MsgQIPCThreader::vExecute() Message type = MSG_STOP "));
                  m_ThreadAlive = false;
               }
            }
            else // if (NULL != poMessage)
            {
               ETG_TRACE_ERR(("\n Message queue is not respond with correct message "));
            }
         } // end of while

      }

      /*************************************************************************
      ** FUNCTION:  void MsgQIPCThreader::vOnMessage(tShlMessage *poMessage)
      *************************************************************************/
      void MsgQIPCThreader::vOnMessage(tShlMessage *poMessage)
      {
         ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
         // null pointer check
         if ((NULL != poMessage) && (NULL != m_cpoMsgQThreadable))
         {
            ETG_TRACE_USR2(("MsgQIPCThreader::vOnMessage() call threadable::execute"));
            // dispatch the message for processing by calling the customized
            // thredable function
            m_cpoMsgQThreadable->vExecute(poMessage);

         } //  if(NULL != poMessage)
         else
         {
            ETG_TRACE_ERR(("\n Threadable instance is not available or message is NULL"));
         }
      }


      /*************************************************************************
      ** FUNCTION:  tclMessageQueue * MsgQIPCThreader::m_poGetMessageQueu()
      *************************************************************************/
      IPCMessageQueue* MsgQIPCThreader::poGetMessageQueu()
      {
         return m_poMessageQueue;
      }
   } // end of thread
} // end of shl
