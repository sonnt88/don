/***********************************************************************/
/*!
 * \file  MsgQThreader.cpp
 * \brief Generic thread handling based on posix standard
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Thread Handling
   AUTHOR:         Priju K Padiyath
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      10.04.2013  | Priju K Padiyath      | Initial Version
      09.10.2013  | Shihabudheen P M      | Updated
      11.06.2015  | Sameer Chandra        | pthread_exit() removed form vExecute()
                                            function

\endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/

#include <cstdio>
#include <cstring>
#include "UniqueName.h"
#include "MsgQThreader.h"

using namespace std;

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/MsgQThreader.cpp.trc.h"
   #endif
#endif
/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

namespace shl
{
   namespace thread
   {

      /*************************************************************************
       ** FUNCTION:   MsgQThreader::MsgQThreader(tclMsgQThreadable *  ..)
       *************************************************************************/
      MsgQThreader::MsgQThreader(MsgQThreadable * const cpoThreadable,
            bool bExecThread = true) :
            Threader((Threadable * const ) cpoThreadable, false), m_cpoMsgQThreadable(
                  cpoThreadable)
      {

         const char *mQName = "";
         if(NULL != (UniqueName::getInstance()))
         {
            mQName = (UniqueName::getInstance()->oGetUniqueName()).c_str();
         }
         else // NULL == (UniqueName::getInstance())
         {
            SPI_NORMAL_ASSERT_ALWAYS();
         }

         // Create the message queue
         m_poMessageQueue = new MessageQueue(mQName);
         // null pointer check
         if ((NULL != m_poMessageQueue) && (true == bExecThread))
         {
               bool bRetVal = bRunThread();
               if(false == bRetVal)
               {
                  ETG_TRACE_ERR(("\nbRunThread failed to start the thread execution\n"));
               } //if(false == bRetVal)
         }
         else
         {
            ETG_TRACE_ERR(("\n Message queue is not created successfully or thread not started"));
         }//if ((NULL != m_poMessageQueue) && (true == bExecThread))
      }

      /*************************************************************************
       ** FUNCTION:    MsgQThreader::~MsgQThreader()
       *************************************************************************/
      MsgQThreader::~MsgQThreader()
      {
         if(NULL != m_poMessageQueue)
         {
            delete m_poMessageQueue;
         }
      }

      /*************************************************************************
       ** FUNCTION:  void MsgQThreader::vExecute()
       *************************************************************************/
      void MsgQThreader::vExecute()
      {
         m_ThreadAlive = true;
         size_t msgSize = 0;
         tenMsgType messageType;

         void* poMessage = NULL;
         tShlMessage *poInternalMsg = NULL;

         // Loop in while thread is alive
         while ((true == m_ThreadAlive) && (NULL != m_poMessageQueue))
         {
            // wait for the message
            poMessage = m_poMessageQueue->poWaitForMessage(&msgSize, &messageType,
                  RUN_FOR_EVER);
            // null pointer check
            if (NULL != poMessage)
            {
               // Message from user to kill the thread
               if (e8_TCL_THREAD_TERMINATE_MESSAGE == messageType)
               {
                  m_ThreadAlive = false;
                  ETG_TRACE_USR2(("Received Terminate Message."));
                  break;
               }
               else if (e8_TCL_DATA_MESSAGE   == messageType)
               {
                  // to get the message from implementation
                  poInternalMsg = m_cpoMsgQThreadable->poGetMsgBuffer(msgSize);
                  // null pointer and out of bound checking
                  if( (NULL != poInternalMsg) && ((size_t)poInternalMsg->size >= msgSize) )
                  {
                     // copy message from queue to user defined space
                     memcpy(poInternalMsg->pvBuffer, poMessage, msgSize );
                     poInternalMsg->size = msgSize;
                     vOnMessage(poInternalMsg);
                     // delete the message
                     if(0 != m_poMessageQueue->s16DropMessage(poMessage))
                     {
                        ETG_TRACE_ERR(("\n Message queue clean up failed"));
                     }
                  }
                  else
                  {
                     ETG_TRACE_ERR(("\npoGetMsgBuffer() return NULL"));
                  }
               } //if( (NULL != oInternalMsg) && ((size_t)oInternalMsg->size >= msgSize))
            }
            else // if (NULL != poMessage)
            {
               ETG_TRACE_ERR(("\n Message queue is not respond with correct message "));
            }
         } // end of while

      }

      /*************************************************************************
       ** FUNCTION:  void MsgQThreader::vOnMessage(tShlMessage *poMessage)
       *************************************************************************/
      void MsgQThreader::vOnMessage(tShlMessage *poMessage)
      {
         // null pointer check
         if ((NULL != poMessage) && (NULL != m_cpoMsgQThreadable))
         {
            // dispatch the message for processing by calling the customized
            // thredable function
            m_cpoMsgQThreadable->vExecute(poMessage);

         } //  if(NULL != poMessage)
         else
         {
            ETG_TRACE_ERR(("\n Threadable instance is not available or message is NULL"));
         }
      }


      /*************************************************************************
       ** FUNCTION:  tclMessageQueue * MsgQThreader::m_poGetMessageQueu()
       *************************************************************************/
      MessageQueue * MsgQThreader::poGetMessageQueu()
      {
         return m_poMessageQueue;
      }
   } // end of thread
} // end of shl
