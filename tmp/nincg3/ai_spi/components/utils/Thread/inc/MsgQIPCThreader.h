#ifndef _MSGQIPCTHREADER_H_
#define _MSGQIPCTHREADER_H_

/***********************************************************************/
/*!
 * \file  MsgQIPCThreader.h
 * \brief Generic thread handling based on posix standard
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Thread Handling
   AUTHOR:         Priju K Padiyath
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      10.02.2014  | Priju K Padiyath      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
 | Include headers and namespaces(scope: global)
 |----------------------------------------------------------------------------*/
#include <cstdlib>
#include "IPCMessageQueue.h"
#include "MsgQThreadable.h"
#include "Threader.h"

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define RUN_FOR_EVER 0

namespace shl
{
   namespace thread
   {

      /****************************************************************************/
      /*!
       * \class MsgQIPCThreader
       * \brief TCL message queue thread handling
       *
       * This class implement to handle the threads based on the messages.
       * It inherited from the basic thread handling class tclThreader
       *
       * This is based on design pattern \ref RAII "RAII"
       ****************************************************************************/
      class MsgQIPCThreader: public Threader
      {
         public:

            /***************************************************************************
             *********************************PUBLIC*************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION: MsgQIPCThreader::MsgQIPCThreader(tclMsgQThreadable *const .. )
             *************************************************************************/
            /*!
             * \fn    MsgQIPCThreader(tclMsgQThreadable *const cpoThreadable,..)
             * \brief Constructor
             * \param cpoThreadable : [IN] Pointer to the threadable (base) class
             * \sa    virtual ~MsgQIPCThreader()
             *************************************************************************/
            MsgQIPCThreader(const char *name,MsgQThreadable * const cpoThreadable,
                  bool bExecThread);

            /*************************************************************************
             ** FUNCTION: virtual MsgQIPCThreader::~MsgQIPCThreader()
             *************************************************************************/
            /*!
             * \fn    ~MsgQIPCThreader()
             * \brief Destructor
             * \sa    MsgQIPCThreader()
             *************************************************************************/
            virtual ~MsgQIPCThreader();

            /*************************************************************************
             ** FUNCTION: MessageQueue * MsgQIPCThreader::m_GetMessageQueueu()
             *************************************************************************/
            /*!
             * \fn    MessageQueue * m_poGetMessageQueu()
             * \brief Function to return the message queueu associated with thread
             * \retval MessageQueue * : Pointer to the message queue
             *************************************************************************/
            IPCMessageQueue * poGetMessageQueu();

            /***************************************************************************
             ****************************END OF PUBLIC***********************************
             ***************************************************************************/
            //Function to get msgqhandle() -implement
         protected:

            /***************************************************************************
             *********************************PROTECTED**********************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION: virtual void MsgQIPCThreader::vExecute()
             *************************************************************************/
            /*!
             * \fn    virtual void vExecute()
             * \brief Function which implements logic to handle messages and spawn thread
             *************************************************************************/
            virtual void vExecute();

            /*************************************************************************
             ** FUNCTION: virtual void MsgQIPCThreader::vOnMessage(tShlMessage *poMessage)
             *************************************************************************/
            /*!
             * \fn    virtual void vOnMessage(tShlMessage *poMessage)
             * \brief Function which invoke threadable function.
             * \param tShlMessage *: [IN] Pointer to the message
             *************************************************************************/
            virtual void vOnMessage(tShlMessage *poMessage);

            // Reference to the Threadable.
            MsgQThreadable* const m_cpoMsgQThreadable;

            // Message Queue
            IPCMessageQueue *m_poMessageQueue;

            /***************************************************************************
             ****************************END OF PROTECTED********************************
             ***************************************************************************/
         private:

            /***************************************************************************
             *********************************PRIVATE************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION: virtual MsgQIPCThreader::MsgQIPCThreader()
             *************************************************************************/
            /*!
             * \fn    MsgQIPCThreader()
             * \brief Default constructor
             *************************************************************************/
            MsgQIPCThreader();
           
            /***************************************************************************
             ****************************END OF PRIVATE *********************************
             ***************************************************************************/
      };
   //class MsgQIPCThreader:public tclThreader
   }//namespace thread
} //namespace shl
#endif /* _MSGQIPCTHREADER_H_ */
