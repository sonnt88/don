/***********************************************************************/
/*!
 * \file  ThreadNamer.h
 * \brief Generic thread Namer for naming calling thread
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Thread Naming
   AUTHOR:         Pruthvi Thej Nagaraju
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author               		 | Modification
      26.01.2015  | Pruthvi Thej Nagaraju      	 | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
 | Include headers and namespaces(scope: global)
 |----------------------------------------------------------------------------*/
 #include <pthread.h>
 
/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

#ifndef THREADNAMER_H_
#define THREADNAMER_H_

#define DEFINE_THREAD_NAME(THREADNAME)  \
static bool bIsThreadNamed = false;    \
if(false == bIsThreadNamed)            \
{                                      \
   pthread_setname_np(pthread_self(), THREADNAME);\
   bIsThreadNamed = true;           \
}

#endif /* THREADNAMER_H_ */
