#ifndef _THREADABLE_H_
#define _THREADABLE_H_
/***********************************************************************/
/*!
 * \file  Threadable.h
 * \brief Generic thread handling based on posix standard
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Thread Handling
   AUTHOR:         Priju K Padiyath
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      23.09.2013  | Priju K Padiyath      | Initial Version

\endverbatim
 *************************************************************************/

namespace shl
{
   namespace thread
   {

      /****************************************************************************/
      /*!
       * \class Threadable
       * \brief Thread base class
       *
       * This class can be used for implementing event based thread in future
       ****************************************************************************/

      class Threadable
      {
         public:
            /***************************************************************************
             *********************************PUBLIC*************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  Threadable::Threadable()
             *************************************************************************/
            /*!
             * \fn    Threadable
             * \brief Constructor
             * \sa    ~Threadable()
             *************************************************************************/

            Threadable(){}

            /*************************************************************************
             ** FUNCTION:  Threadable::~Threadable()
             *************************************************************************/
            /*!
             * \fn    ~Threadable()
             * \brief Desctructor
             * \sa    Threadable()
             *************************************************************************/
            virtual ~Threadable(){}

            /*************************************************************************
             ** FUNCTION:  virtual void Threadable::vExecute()
             *************************************************************************/
            /*!
             * \fn    virtual void vExecute();
             * \brief Function to implement threadable functionality
             * \sa
             *************************************************************************/

            virtual void vExecute() = 0;
            /*************************************************************************
             ****************************END OF PUBLIC*********************************
             *************************************************************************/

      };
   // end of class
   }// end of thread
} //end of shl

#endif //TCLTHREADABLE_H_
