#ifndef _THREADER_H_
#define _THREADER_H_

/***********************************************************************/
/*!
 * \file  Threader.h
 * \brief Generic thread handling based on posix standard
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Thread Handling
   AUTHOR:         Priju K Padiyath
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      23.09.2013  | Priju K Padiyath      | Initial Version
      10.08.2013  | Shihabudheen P M      | Updated

\endverbatim
 *************************************************************************/

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | Include headers and namespaces(scope: global)
 |----------------------------------------------------------------------------*/
#include <pthread.h>
#include "Threadable.h"

namespace shl
{
   namespace thread
   {

      /****************************************************************************/
      /*!
       * \class Threader
       * \brief TCL Threader
       *
       * Class used to spawn the thread based on requirement. This holds the thread
       * related data.this acts as a base class for other threader classes.
       *
       ****************************************************************************/
      class Threader
      {
         public:
            /*************************************************************************
             *********************************PUBLIC***********************************
             *************************************************************************/

            /*************************************************************************
             ** FUNCTION :Threader::Threader(tclThreadable * const  ..)
             *************************************************************************/
            /*!
             * \fn    Threader(tclThreadable * const cpoThreadable, bool bExecT....
             * \brief Constructor
             * \param cpoThreadable : [IN] pointer to threadable
             * \param bExecThread   : [IN] true if needs to execute, false otherwise
             * \sa    virtual ~Threader()
             *************************************************************************/
            Threader(Threadable * const cpoThreadable, bool bExecThread =
                  false);

            /*************************************************************************
             ** FUNCTION virtual Threader::~Threader()
             *************************************************************************/
            /*!
             * \fn    ~Threader()
             * \brief Destructor
             * \sa    Threader()
             *************************************************************************/
            virtual ~Threader();

            /*************************************************************************
             ** FUNCTION : static void Threader::vStartThread(void *pvArg)
             *************************************************************************/
            /*!
             * \fn    static void vStartThread(void *pvArg)
             * \brief Function which act as a starting point of spawned thread.
             * \param pvArg : [IN] pointer to the function which has to be called.
             * \sa
             *************************************************************************/
            static void vStartThread(void *pvArg);

            /*************************************************************************
             ** bool Threader::bRunThread()
             *************************************************************************/
            /*!
             * \fn     bool bRunThread()
             * \brief  Function to Spawn a thread
             * \retval bool : true if success, false otherwise
             * \sa
             *************************************************************************/
            bool bRunThread();

            /*************************************************************************
             ** void Threader::vSetThreadName()
             *************************************************************************/
            /*!
             * \fn     void vSetThreadName()
             * \brief  sets the name of the current thread
             * \sa
             *************************************************************************/
            void vSetThreadName(const char* czName);

            /*************************************************************************
            /***************************************************************************
             ** FUNCTION:  pthread_t Threader::vGetThreadID()
             ***************************************************************************/
            /*!
             * \fn     pGetThreadID()
             * \brief  interface function to get thread ID
             *
             **************************************************************************/
             pthread_t pGetThreadID();

            /***************************************************************************
             ** FUNCTION:  void Threader::vWaitForTermination()
             ***************************************************************************/
            /*!
             * \fn     vWaitForTermination()
             * \brief  interface to wait for the thread termination
             *
             **************************************************************************/
             static void vWaitForTermination(pthread_t threadId);
			 
            /*************************************************************************
             ****************************END OF PUBLIC*********************************
             *************************************************************************/
         protected:

            /***************************************************************************
             *********************************PROTECTED**********************************
             ***************************************************************************/

            // Is thread alive?
            bool m_ThreadAlive;

            // Thread id
            pthread_t m_ThreadId;

            // thread attributes
            pthread_attr_t m_ThreadAttr;

            // thread mutex variable
            pthread_mutex_t m_ThreadMutex;

            // conditional variable
            pthread_cond_t m_ThreadCond;

            // Reference to the threadable
            Threadable * const m_Threadable;

            /*************************************************************************
             **  FUNCTION : virtual void vExecute()
             *************************************************************************/
            /*!
             * \fn     virtual void vExecute()
             * \brief  Function should implement to get general thread handling using
             *         events
             * \retval bool : true if success, false otherwise
             *************************************************************************/
            virtual void vExecute() {};

            /***************************************************************************
             ****************************END OF PROTECTED********************************
             ***************************************************************************/

         private:

            /*************************************************************************
             *********************************PRIVATE**********************************
             *************************************************************************/

            /*************************************************************************
             **  FUNCTION : Threader::Threader()
             *************************************************************************/
            /*!
             * \fn     Threader()
             * \brief  Default constructor
             * \sa
             *************************************************************************/
            Threader();

            /*************************************************************************
             **  FUNCTION : Threader::Threader(const Threader &rfcothreader)
             *************************************************************************/
            /*!
             * \fn     Threader(const Threader &rfcothreader)
             * \brief  Copy constructor
             * \param  rfcothreader : [IN] reference to threader class
             *************************************************************************/
            Threader(const Threader &rfcothreader);

            /*************************************************************************
             **  FUNCTION : Threader& operator=(const Threader &rfcothreader) {}
             *************************************************************************/
            /*!
             * \fn     Threader& operator=(const Threader &rfcothreader) {}
             * \brief  Overloaded function
             * \param  rfcothreader : [IN] reference to threader class
             *************************************************************************/

            Threader& operator=(const Threader &rfcothreader);

            /*************************************************************************
             ****************************END OF PRIVATE *******************************
             *************************************************************************/
      };//class Threader

   } //namespace thread

}  //namespace shl
#endif /* TCLTHREADER_H_ */
