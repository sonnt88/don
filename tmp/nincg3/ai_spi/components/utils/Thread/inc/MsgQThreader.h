#ifndef _MSGQTHREADER_H_
#define _MSGQTHREADER_H_

/***********************************************************************/
/*!
 * \file  MsgQThreader.h
 * \brief Generic thread handling based on posix standard
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Thread Handling
   AUTHOR:         Priju K Padiyath
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      10.04.2013  | Priju K Padiyath      | Initial Version
	   10.08.2013  | Shihabudheen P M      | Updated

\endverbatim
 *************************************************************************/

/******************************************************************************
 | Include headers and namespaces(scope: global)
 |----------------------------------------------------------------------------*/
#include <cstdlib>
#include "MessageQueue.h"
#include "MsgQThreadable.h"
#include "Threader.h"

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define RUN_FOR_EVER 0

namespace shl
{
   namespace thread
   {

      /****************************************************************************/
      /*!
       * \class MsgQThreader
       * \brief TCL message queue thread handling
       *
       * This class implement to handle the threads based on the messages.
       * It inherited from the basic thread handling class tclThreader
       *
       * This is based on design pattern \ref RAII "RAII"
       ****************************************************************************/
      class MsgQThreader: public Threader
      {
         public:

            /***************************************************************************
             *********************************PUBLIC*************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION: MsgQThreader::MsgQThreader(tclMsgQThreadable *const .. )
             *************************************************************************/
            /*!
             * \fn    MsgQThreader(tclMsgQThreadable *const cpoThreadable,..)
             * \brief Constructor
             * \param cpoThreadable : [IN] Pointer to the threadable (base) class
             * \sa    virtual ~MsgQThreader()
             *************************************************************************/
            MsgQThreader(MsgQThreadable * const cpoThreadable,
                  bool bExecThread);

            /*************************************************************************
             ** FUNCTION: virtual MsgQThreader::~MsgQThreader()
             *************************************************************************/
            /*!
             * \fn    ~MsgQThreader()
             * \brief Destructor
             * \sa    MsgQThreader()
             *************************************************************************/
            virtual ~MsgQThreader();

            /*************************************************************************
             ** FUNCTION: MessageQueue * MsgQThreader::m_GetMessageQueueu()
             *************************************************************************/
            /*!
             * \fn    MessageQueue * m_poGetMessageQueu()
             * \brief Function to return the message queueu associated with thread
             * \retval MessageQueue * : Pointer to the message queue
             *************************************************************************/
            MessageQueue * poGetMessageQueu();

            /***************************************************************************
             ****************************END OF PUBLIC***********************************
             ***************************************************************************/
            //Function to get msgqhandle() -implement
         protected:

            /***************************************************************************
             *********************************PROTECTED**********************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION: virtual void MsgQThreader::vExecute()
             *************************************************************************/
            /*!
             * \fn    virtual void vExecute()
             * \brief Function which implements logic to handle messages and spawn thread
             *************************************************************************/
            virtual void vExecute();

            /*************************************************************************
             ** FUNCTION: virtual void MsgQThreader::vOnMessage(tShlMessage *poMessage)
             *************************************************************************/
            /*!
             * \fn    virtual void vOnMessage(tShlMessage *poMessage)
             * \brief Function which invoke threadable function.
             * \param tShlMessage *: [IN] Pointer to the message
             *************************************************************************/
            virtual void vOnMessage(tShlMessage *poMessage);

            // Reference to the Threadable.
            MsgQThreadable* const m_cpoMsgQThreadable;

            // Message Queue
            MessageQueue *m_poMessageQueue;

            /***************************************************************************
             ****************************END OF PROTECTED********************************
             ***************************************************************************/
         private:

            /***************************************************************************
             *********************************PRIVATE************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION: virtual MsgQThreader::MsgQThreader()
             *************************************************************************/
            /*!
             * \fn    MsgQThreader()
             * \brief Default constructor
             *************************************************************************/
            MsgQThreader();

            /***************************************************************************
             ****************************END OF PRIVATE *********************************
             ***************************************************************************/
      };
   //class MsgQThreader:public tclThreader
   }//namespace thread
} //namespace shl
#endif /* MSGQTHREADER_H_ */
