#ifndef _UNIQUENAME_H_
#define _UNIQUENAME_H_

/***********************************************************************/
/*!
 * \file  UniqueName.h
 * \brief Generic class to create random name.Can be used with generic thread and message queue implementation
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Randon name generation
   AUTHOR:         Priju K Padiyath
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      10.04.2013  | Priju K Padiyath      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
 | Include headers and namespaces(scope: global)
 |----------------------------------------------------------------------------*/
#include <string>
#include <cstdio>
#include "Lock.h"
#include "GenericSingleton.h"

/****************************************************************************/
/*!
 * \class tclUniqueName
 * \brief tclUniqueName generator
 *
 * Class used to provide a general functionality, which provide unique names
 * upon request.
 ****************************************************************************/

class UniqueName: public GenericSingleton<UniqueName>
{

      friend class GenericSingleton<UniqueName> ;
   public:

      /*************************************************************************
       *********************************PUBLIC***********************************
       *************************************************************************/

      /*************************************************************************
       ** FUNCTION:  tclUniqueName::tclUniqueName()
       *************************************************************************/
      /*!
       * \fn     tclUniqueName()
       * \brief  Constructor
       * \sa     ~tclUniqueName()
       *************************************************************************/
      UniqueName() :
            m_u32NumNames(0)
      {

      }

      /*************************************************************************
       ** FUNCTION:  tclUniqueName::~tclUniqueName()
       *************************************************************************/
      /*!
       * \fn     ~tclUniqueName()
       * \brief  Destructor
       * \sa     tclUniqueName()
       *************************************************************************/
      ~UniqueName()
      {

      }

      /*************************************************************************
       ** FUNCTION:  std::string tclUniqueName::oGetUniqueName()
       *************************************************************************/
      /*!
       * \fn     std::string oGetUniqueName()
       * \brief  Generate a unique name
       * \retval string : Unique name
       *************************************************************************/

      std::string oGetUniqueName()
      {
         m_hLock.s16Lock();
         char szName[20];
         sprintf(szName, "intName_%u", (unsigned int) m_u32NumNames);
         std::string oName(szName);
         m_u32NumNames++;
         m_hLock.vUnlock();
         return oName;
      }

      /*************************************************************************
       ****************************END OF PUBLIC*********************************
       *************************************************************************/

   private:
      /*************************************************************************
       *********************************PRIVATE**********************************
       *************************************************************************/

      Lock m_hLock;

      int m_u32NumNames;

      /*************************************************************************
       *********************************PRIVATE**********************************
       *************************************************************************/

};

#endif /* TCLUNIQUENAME_H_ */
