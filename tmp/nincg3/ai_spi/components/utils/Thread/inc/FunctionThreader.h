/***********************************************************************/
/*!
 * \file  FunctionThreader.h
 * \brief Specialized thread handler for Starting a function in new thread
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Specialized thread handler for Starting a function
 in new thread
 AUTHOR:         Pruthvi Thej Nagaraju
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date        | Author                | Modification
 25.03.2014  | Pruthvi Thej Nagaraju | Initial Version
 \endverbatim
 *************************************************************************/

#ifndef _FUNCTIONTHREADER_H_
#define _FUNCTIONTHREADER_H_

/******************************************************************************
 | defines and macros (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | Include headers and namespaces(scope: global)
 |----------------------------------------------------------------------------*/
#include "Threader.h"

namespace shl
   {
   namespace thread
      {

      /****************************************************************************/
      /*!
       * \class FunctionThreader
       * \brief Specialized thread handler for Starting a function in new thread
       ****************************************************************************/
      class FunctionThreader: public Threader
      {
         public:
            /*************************************************************************
             *********************************PUBLIC***********************************
             *************************************************************************/

            /*************************************************************************
             ** FUNCTION :FunctionThreader::FunctionThreader(tclThreadable * const  ..)
             *************************************************************************/
            /*!
             * \fn    FunctionThreader(tclThreadable * const cpoThreadable, bool bExecT....
             * \brief Constructor
             * \param cpoThreadable : [IN] pointer to threadable
             * \param bExecThread   : [IN] true if needs to execute, false otherwise
             * \sa    virtual ~FunctionThreader()
             *************************************************************************/
            FunctionThreader(Threadable * const cpoThreadable, bool bExecThread = false) :
                     Threader(cpoThreadable, bExecThread)
            {

            }

            /*************************************************************************
             ** FUNCTION virtual FunctionThreader::~FunctionThreader()
             *************************************************************************/
            /*!
             * \fn    ~FunctionThreader()
             * \brief Destructor
             * \sa    FunctionThreader()
             *************************************************************************/
            virtual ~FunctionThreader()
            {

            }

         protected:

            /***************************************************************************
             *********************************PROTECTED**********************************
             ***************************************************************************/

            /*************************************************************************
             **  FUNCTION : virtual void vExecute()
             *************************************************************************/
            /*!
             * \fn     virtual void vExecute()
             * \brief  Calls the Threadable objects vExecute function in a new thread
             *************************************************************************/
            virtual void vExecute()
            {
               if(NULL != m_Threadable)
               {
                  m_Threadable->vExecute();
               }
            }

            /***************************************************************************
             ****************************END OF PROTECTED********************************
             ***************************************************************************/

         private:

            /*************************************************************************
             *********************************PRIVATE**********************************
             *************************************************************************/

            /*************************************************************************
             **  FUNCTION : FunctionThreader::FunctionThreader()
             *************************************************************************/
            /*!
             * \fn     FunctionThreader()
             * \brief  Default constructor
             * \sa
             *************************************************************************/
            FunctionThreader();

            /*************************************************************************
             **  FUNCTION : FunctionThreader::FunctionThreader(const FunctionThreader &rfcothreader)
             *************************************************************************/
            /*!
             * \fn     FunctionThreader(const FunctionThreader &rfcothreader)
             * \brief  Copy constructor: Made private to prevent unintended usage
             * \param  rfcothreader : [IN] reference to threader class
             *************************************************************************/
            FunctionThreader(const FunctionThreader &rfcothreader);

            /*************************************************************************
             **  FUNCTION : FunctionThreader& operator=(const FunctionThreader &rfcothreader) {}
             *************************************************************************/
            /*!
             * \fn     FunctionThreader& operator=(const FunctionThreader &rfcothreader) {}
             * \brief  Overloaded function: Made private to prevent unintended usage
             * \param  rfcothreader : [IN] reference to threader class
             *************************************************************************/
            FunctionThreader& operator=(const FunctionThreader &rfcothreader);

            /*************************************************************************
             ****************************END OF PRIVATE *******************************
             *************************************************************************/
      };//class FunctionThreader
      }//namespace thread
   }  //namespace shl
#endif /* _FUNCTIONTHREADER_H_ */
