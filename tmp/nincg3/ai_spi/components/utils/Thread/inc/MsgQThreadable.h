#ifndef _MSGQTHREADABLE_H_
#define _MSGQTHREADABLE_H_
/***********************************************************************/
/*!
 * \file  MsgQThreadable.h
 * \brief Generic thread handling based on posix standard
 *************************************************************************
 \verbatim

 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    Thread Handling
 AUTHOR:         Priju K Padiyath
 COPYRIGHT:      &copy; RBEI
 HISTORY:
 Date        | Author                | Modification
 10.04.2013  | Priju K Padiyath      | Initial Version

 \endverbatim
 *************************************************************************/
#include <cstddef>

namespace shl
{
   namespace thread
   {

      /*! \struct tInternalMsg
       * Message structure
       */
      typedef struct tInternalMsg
      {
            // size of the actual user data
            int size;
            // Data
            void *pvBuffer;
      } tShlMessage;

      /****************************************************************************/
      /*!
       * \class MsgQThreadable
       * \brief Message Queue parent class
       *
       * File handling is implemented based on the Posix calls.
       * This gives you the abstraction of Posix file system calls and
       * some intelligence to perform operations on files and returns
       * proper error codes on failures.
       *
       ****************************************************************************/
      class MsgQThreadable
      {
         public:
            /***************************************************************************
             *********************************PUBLIC*************************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  MsgQThreadable::MsgQThreadable() {}
             *************************************************************************/
            /*!
             * \fn    MsgQThreadable()
             * \brief Constructor
             * \sa    virtual ~MsgQThreadable()
             *************************************************************************/
            MsgQThreadable()
            {
            };

            /*************************************************************************
             ** FUNCTION:  virtual MsgQThreadable::~MsgQThreadable() {}
             *************************************************************************/
            /*!
             * \fn    virtual ~MsgQThreadable()
             * \brief Destructor
             * \sa    MsgQThreadable()
             *************************************************************************/
            virtual ~MsgQThreadable()
            {
            };

            /*************************************************************************
             ** FUNCTION:  virtual void MsgQThreadable::vExecute(tShlMessage ..)
             *************************************************************************/
            /*!
             * \fn     virtual void vExecute(tShlMessage *poMessage)
             * \brief  Virtual function to write threadable functionality
             * \param  poMessage : [IN] Message to process
             * \sa
             *************************************************************************/
            virtual void vExecute(tShlMessage *poMessage) = 0;

            /*************************************************************************
             ** FUNCTION:  virtual tShlMessage* MsgQThreadable::poGetMsgBuffer()
             *************************************************************************/
            /*!
             * \fn     virtual tShlMessage* poGetMsgBuffer()
             * \brief  function to set up tShlMessage with user defined message type
             * \retval tShlMessage * : Pointer to the message
             * \sa
             *************************************************************************/
            virtual tShlMessage* poGetMsgBuffer(size_t siBuffer) = 0;

            /***************************************************************************
             ****************************END OF PUBLIC***********************************
             ***************************************************************************/
         protected:
            /***************************************************************************
             *********************************PROTECTED**********************************
             ***************************************************************************/

            /*************************************************************************
             ** FUNCTION:  MsgQThreadable::MsgQThreadable(const  ...)
             *************************************************************************/
            /*!
             * \fn     MsgQThreadable(const MsgQThreadable &rfcoMsgQThreadable)
             * \brief  Parametrized constructor
             * \param  rfcoMsgQThreadable : [IN] Odject to copy
             * \sa
             *************************************************************************/
            MsgQThreadable(const MsgQThreadable &rfcoMsgQThreadable);

            /*************************************************************************
             ****************************END OF PROTECTED*********************************
             *************************************************************************/
         private:

      };
   //class MsgQThreadable
   }//namespace thread
} //namespace shl
#endif /* TCLMSGQTHREADABLE_H_ */
