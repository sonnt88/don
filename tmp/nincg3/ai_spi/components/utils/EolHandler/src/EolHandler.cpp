/***********************************************************************/
/*!
* \file  EolHandler.cpp
* \brief EOL Calibration Values Handler
*************************************************************************
 \verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    EOL Handler for reading the system calibration values 
AUTHOR:         Hari Priya E R(RBEI/ECP2)
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                                          | Modification
11.02.2014  |  Hari Priya E R                                  | Initial Version 

 \endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#include "EolHandler.h"


//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/EolHandler.cpp.trc.h"
#endif
#endif


/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

/******************************************************************************
** FUNCTION:  EolHandler::EolHandler()
******************************************************************************/
EolHandler::EolHandler()
:m_ioEolDesc(OSAL_ERROR), m_u32ErrorCode(OSAL_OK)
{
   ETG_TRACE_USR1(("EolHandler() entered \n"));
   // Open the EOL Device.
   m_ioEolDesc =  OSAL_IOOpen(OSAL_C_STRING_DEVICE_DIAG_EOL, OSAL_EN_READWRITE);

   if (OSAL_ERROR == m_ioEolDesc)
   {
      // Error Trace.
      m_u32ErrorCode =  OSAL_u32ErrorCode();
      ETG_TRACE_ERR(("ERROR: EOL device open failed with error %u", m_u32ErrorCode));
   }
}  // EolHandler::EolHandler()

/******************************************************************************
** FUNCTION:   EolHandler::~EolHandler()
******************************************************************************/
EolHandler::~EolHandler()
{
   ETG_TRACE_USR1(("~EolHandler() entered \n"));
   if ((OSAL_ERROR != m_ioEolDesc) && (OSAL_ERROR == OSAL_s32IOClose(m_ioEolDesc)))
   {
      // Trace the error code if any
      ETG_TRACE_ERR(("ERROR: EOL device close failed with error %u", OSAL_u32ErrorCode()));
   }
   m_ioEolDesc =  OSAL_ERROR;
}  // EolHandler::~EolHandler()

/******************************************************************************
** FUNCTION:  tBool EolHandler::bRead(tCU16 cu16TableID,
tCU16 cu16Offset, tU8* pu8Value,tU16 u16Len))
******************************************************************************/
bool EolHandler::bRead(unsigned short int cu16TableID, unsigned short int cu16Offset, 
                       unsigned char* pu8Value, unsigned short int u16Len)
{
   ETG_TRACE_USR1(("EolHandler::bRead() entered: cu16TableID = %d, cu16Offset = %d, u16Len = %d \n",
         cu16TableID, cu16Offset, u16Len));

   tBool bRetVal  =  FALSE;

   if ((OSAL_ERROR != m_ioEolDesc) && (OSAL_NULL != pu8Value))
   {
      OSAL_trDiagEOLEntry oDiagEolEntry;
      oDiagEolEntry.u8Table         =  cu16TableID;
      oDiagEolEntry.u16Offset       =  cu16Offset;
      oDiagEolEntry.u16EntryLength  =  u16Len;
      oDiagEolEntry.pu8EntryData    =  pu8Value;

      if (OSAL_ERROR != OSAL_s32IORead(m_ioEolDesc, (tS8*)&oDiagEolEntry
         , (tS32)sizeof(oDiagEolEntry)))
      {
         bRetVal  =  TRUE;
      }
      else
      {
         m_u32ErrorCode =  OSAL_u32ErrorCode();
         ETG_TRACE_ERR(("ERROR: EolHandler::bRead: Could not read offset:%d from EOL. Error:%d"
            , cu16Offset, m_u32ErrorCode));
      }
   }  // if (OSAL_ERROR != m_ioEolDesc)

   return bRetVal;

}  // tBool EolHandler::bRead(tCU16 cu16Offset, tPU8 pu8Value) const

/******************************************************************************
** FUNCTION:  tU32 EolHandler::u32ErrorCode() const
******************************************************************************/
unsigned int EolHandler::u32GetErrorCode() const
{
   ETG_TRACE_USR1(("EolHandler::u32GetErrorCode() entered \n"));
   return m_u32ErrorCode;
}  // tU32 EolHandler::u32ErrorCode() const


////////////////////////////////////////////////////////////////////////////////
//<EOF>
