/***********************************************************************/
/*!
* \file   IPCMessageQueue.cpp
* \brief  Message queue for inter process communication
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message Queue for IPC
AUTHOR:         Priju K Padiyath
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
12.02.2014  | Priju K Padiyath      | Modified to be used for SPI.Base version from Mediaplayer utilities
14.10.2014  | Shihabudheen P M      | Modified the opening of message queue.
06.05.2015  |Tejaswini HB                 |Lint Fix
26.06.2015  | Shihabudheen P M      | removed vDropBuffer() to solve the lint fix.
                                      The function is not used anywhere and is not logically required.
17.07.2015  | Sameer Chandra        | Added back vDropBuffer since it is logically required and not using
                                      it causes memory leaks.

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

/* ETG definitions */
#define ETRACE_S_IMPORT_INTERFACE_GENERIC
#define ET_TRACE_INFO_ON

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/IPCMessageQueue.cpp.trc.h"
#endif
#endif

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <mqueue.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <features.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "IPCMessageQueue.h"

//#include "Utils.h"

//static int mMQinited = 0; // global flag for init of message queue system parameters  //commented to suppress Lint Warning
static int mSendTimeoutSec = 2;// max time a send will block

/***************************************************************************
 ** FUNCTION:  IPCMessageQueue::IPCMessageQueue()
 ***************************************************************************/
IPCMessageQueue::IPCMessageQueue(const char *name,int iFlag):mq_name(NULL),mq(-1)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
  
   mq_name = name;
   struct mq_attr attr;
   // first remove old queue
   mq_unlink(name);
   // set the queue attributes
   memset(&attr, 0, sizeof(struct mq_attr));
   attr.mq_maxmsg = MAX_MSG;
   attr.mq_msgsize = MSG_SIZE;
   attr.mq_curmsgs = 0;
   attr.mq_flags = 0;          

   // if already created in the constructor, the mq_msgsize and mq_maxmsg cannot be adjusted later on
   mq = mq_open(mq_name, iFlag, 0664, &attr);//0644

   /*
   * if this assert happens: Did you adjust msg_max to MAX_MSG and msgsize_max to MSG_SIZE in /proc/sys/fs/mqueue/?
   */
   if (mq == -1) {
      ETG_TRACE_USR4(("IPCMessageQueue::IPCMessageQueue(%s)", mq_name)); 
      ETG_TRACE_USR4((" error on mq_open: %s",strerror(errno))); 
      ETG_TRACE_USR4(("MAX_MSG=%d, MSG_SIZE=%d",MAX_MSG, MSG_SIZE));

   }
}

/***************************************************************************
 ** FUNCTION:  IPCMessageQueue::IPCMessageQueue(const char *name)
 ***************************************************************************/
IPCMessageQueue::IPCMessageQueue(const char *name):mq_name(NULL),mq(-1)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   mq_name = name;

   // A message quque creator class is used to restrict the message queu open.
   // creator class will ensure that the message queue open() will execute
   // only once per process for a particular message queue.
   MsgQCreator *poMagQInstance = MsgQCreator::getInstance(mq_name);
   if(NULL != poMagQInstance)
   {
      mq = poMagQInstance->poGetMsgQDescriptor();
   }

   /*
   * if this assert happens: Did you adjust msg_max to MAX_MSG and msgsize_max to MSG_SIZE in /proc/sys/fs/mqueue/?
   */
   if (mq == -1) {
      ETG_TRACE_USR4(("IPCMessageQueue::IPCMessageQueue(%s)", mq_name)); 
      ETG_TRACE_USR4((" error on mq_open: %s",strerror(errno))); 
      ETG_TRACE_USR4(("MAX_MSG=%d, MSG_SIZE=%d",MAX_MSG, MSG_SIZE));
   }
}

/***************************************************************************
 ** FUNCTION: bool IPCMessageQueue::bIsOpen()
 ***************************************************************************/
bool IPCMessageQueue::bIsOpen()
{
   return (mq == -1 ? false : true);
}

/***************************************************************************
 ** FUNCTION: IPCMessageQueue::~IPCMessageQueue()
 ***************************************************************************/
IPCMessageQueue::~IPCMessageQueue()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   mq_name = NULL;
}

/***************************************************************************
 ** FUNCTION: IPCMessageQueue::vCloseIPCMsgQueue()
 ***************************************************************************/
void IPCMessageQueue::vCloseIPCMsgQueue()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   //! Close the message from opening end.
   MsgQCreator *poMagQInstance = MsgQCreator::getInstance(mq_name);
   if(NULL != poMagQInstance)
   {
      delete poMagQInstance;
	  poMagQInstance=NULL;
   }

   //Close and unlink the message queue from the creation end.
   if(mq != -1)
   {
      /* close the queue */
      mq_close(mq);
      mq = -1;
   }
   /* try to delete the queue */
   mq_unlink(mq_name);

}

/***************************************************************************
 ** FUNCTION: const char *IPCMessageQueue::cpGetName()
 ***************************************************************************/
const char *IPCMessageQueue::cpGetName()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   return mq_name;
}

/***************************************************************************
 ** FUNCTION:void *IPCMessageQueue::vpCreateBuffer(size_t size)
 ***************************************************************************/
void *IPCMessageQueue::vpCreateBuffer(size_t size)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   unsigned char *buffer;
   tMessageHeader *msgHeader;

   // allocate the buffer (inclusive space for the header)
   buffer = (unsigned char *)malloc(size + sizeof(tMessageHeader));
   if (!buffer) return NULL;

   // set message header
   msgHeader = (tMessageHeader *)buffer;
   msgHeader->token = MAGIC;
   msgHeader->size = size;
   // return the start of the user data space
   return (void *)(buffer + sizeof(tMessageHeader));
}

/***************************************************************************
 ** FUNCTION: int IPCMessageQueue::iSendBuffer()
 ***************************************************************************/
int IPCMessageQueue::iSendBuffer(void* buf, unsigned int prio,eMessageType eMsgType)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   unsigned char *sendPointer = (unsigned char *)buf - sizeof(tMessageHeader);
   tMessageHeader *msgHeader = (tMessageHeader *)sendPointer;
   size_t remainingSize;
   size_t sendSize;
   int ret;
   msgHeader->MsgType = eMsgType;
   // sanity checks
   if (buf == NULL) return -1;
   if (msgHeader->token != MAGIC) return -1;

   // lock sending
   mLock.s16Lock();

   // send loop
   remainingSize = msgHeader->size + sizeof(tMessageHeader);
   while(remainingSize > 0) {
      // send one record
      if (remainingSize > MSG_SIZE) sendSize = MSG_SIZE;
      else sendSize = remainingSize;

      struct timespec abs_timeout;
      abs_timeout.tv_nsec = 0;
      abs_timeout.tv_sec = time(NULL) + mSendTimeoutSec;

      while(1) {
         ret = mq_timedsend(mq, (char *)sendPointer, sendSize, prio, &abs_timeout);
         if (ret == -1 && errno == EINTR) continue;
         break;
      }
      if (ret != 0) {
         ETG_TRACE_USR2(("Message send failed"));
         mLock.vUnlock();
         return -1;
      }

      // move pointer forward
      sendPointer += sendSize;
      remainingSize -= sendSize;
   }

   // unlock sending
   mLock.vUnlock();
   return 0;
}


/***************************************************************************
 ** FUNCTION: void* IPCMessageQueue::vpWaitMessage
 ***************************************************************************/
void *IPCMessageQueue::vpWaitMessage(size_t *size, unsigned int time_out_in_ms,eMessageType &eMsgType)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   unsigned char receiveBuffer[MAX_MSG];
   tMessageHeader *msgHeader = (tMessageHeader *)receiveBuffer;
      memset(receiveBuffer, 0, MSG_SIZE);
   struct timespec abs_timeout = {0};
   memset(&abs_timeout, 0, sizeof(abs_timeout));
   int ret;
   size_t receiveSize;
   unsigned char *returnBuffer;
   unsigned char *writePtr;
   size_t receiveLen;

   // calculate the timeout time
   if (time_out_in_ms != 0) {
      int to_nsecs, to_secs;
      to_nsecs = (time_out_in_ms % 1000) * 1000 * 1000;
      to_secs = (time_out_in_ms / 1000);
      abs_timeout.tv_nsec = to_nsecs; // 1 ms = 1.000 µs = 1.000.000 ns
      abs_timeout.tv_sec = time(NULL) + to_secs;
   }

   // wait for the first packet, which holds the header and in the header the size
   memset(receiveBuffer, 0, MSG_SIZE);
   while(1) {
      if (time_out_in_ms == 0) {

         ret = mq_receive(mq, (char *)receiveBuffer, MSG_SIZE, NULL);

      } else {
         ret = mq_timedreceive(mq, (char *)receiveBuffer, MSG_SIZE, NULL, &abs_timeout);
      }
      if (ret == -1 && errno == EINTR) 
      {
         continue;
      }
      break;
   }

   // check an error on waiting for first packet
   if (ret == -1) {
      ETG_TRACE_USR4(("IPCMessageQueue::IPCMessageQueue(%s)", mq_name)); 
      ETG_TRACE_USR4((" error on mq_open: %s",strerror(errno))); 
      return NULL;
   }

   // check the token in the header
   if (msgHeader->token != MAGIC) {
      ETG_TRACE_USR2(("IPCMessageQueue::IPCMessageQueue: Msg token invalid\n"));
      return NULL;
   }

   // create the output or return buffer (a multiple of MSG_SIZE)
   returnBuffer = (unsigned char *)malloc(((msgHeader->size/MSG_SIZE)+2)*MSG_SIZE);
   if (!returnBuffer) {
      ETG_TRACE_USR2(("IPCMessageQueue::IPCMessageQueue: returnBuffer is null\n"));
      return NULL;
   }

   // calc the received len
   if ((size_t)msgHeader->size > (MSG_SIZE - sizeof(tMessageHeader)))
   {
      receiveLen = (MSG_SIZE - sizeof(tMessageHeader));
   }
   else 
   {
      receiveLen = msgHeader->size;
   }

   // set the ouput parameter size
   *size = msgHeader->size;

   // copy the first received part to the return buffer
   memcpy(returnBuffer, receiveBuffer + sizeof(tMessageHeader), receiveLen);

   // calc the remaining number of bytes to receive
   receiveSize = msgHeader->size - receiveLen;

   // set the next write pointer
   writePtr = returnBuffer + receiveLen;

   // receive loop for the rest
   while(receiveSize > 0) {
      // set the receive len
      if (receiveSize > MSG_SIZE) receiveLen = MSG_SIZE;
      else receiveLen = receiveSize;

      // calculate the timeout time
      if (time_out_in_ms != 0) {
         int to_nsecs, to_secs;
         to_nsecs = (time_out_in_ms % 1000) * 1000 * 1000;
         to_secs = (time_out_in_ms / 1000);
         abs_timeout.tv_nsec = to_nsecs; // 1 ms = 1.000 µs = 1.000.000 ns
         abs_timeout.tv_sec = time(NULL) + to_secs;
      }

      // receive next part of buffer (always read MSG_SIZE)
      while(1) {
         if (time_out_in_ms == 0) {
            ret = mq_receive(mq, (char *)writePtr, MSG_SIZE, NULL);
         } else {
            ret = mq_timedreceive(mq, (char *)writePtr, MSG_SIZE, NULL, &abs_timeout);
         }
         if (ret == -1 && errno == EINTR) continue;
         break;
      }
      if (ret == -1) {
         free(returnBuffer);
         return NULL;
      }

      // increment pointer and decrement size
      writePtr += receiveLen;
      receiveSize -= receiveLen;
   }
   eMsgType = msgHeader->MsgType;
   // return the start of user buffer
   return returnBuffer;
}

/***************************************************************************
 ** FUNCTION: long IPCMessageQueue::lGetCurMessagesCount()
 ***************************************************************************/
long IPCMessageQueue::lGetCurMessagesCount()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   struct mq_attr m_attr;
   mq_getattr(mq, &m_attr);
   return m_attr.mq_curmsgs;
}

/***************************************************************************
 ** FUNCTION: void IPCMessageQueue::vFlush()
 ***************************************************************************/
void IPCMessageQueue::vFlush()
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   int ret;
   char *flushBuffer;

   flushBuffer = (char *)malloc(MSG_SIZE);
   if (!flushBuffer) return;

   while(lGetCurMessagesCount()) {
      ret = mq_receive(mq, flushBuffer, MSG_SIZE, NULL);
      if (ret == -1 && errno != EINTR) break;
   }

   free(flushBuffer);
}

/***************************************************************************
 ** FUNCTION: void IPCMessageQueue::vDropMessage(void *buf)
 ***************************************************************************/
void IPCMessageQueue::vDropMessage(void *buf)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   free(buf);
}

/***************************************************************************
 ** FUNCTION:  MsgQCreator::MsgQCreator(const char * name)
 ***************************************************************************/
MsgQCreator::MsgQCreator(const char * szName):m_QueuDes(-1), m_szName(NULL)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));
   m_szName = szName;
}

/***************************************************************************
 ** FUNCTION:  MsgQCreator::~MsgQCreator()
 ***************************************************************************/
MsgQCreator::~MsgQCreator()
{
   if(-1 != m_QueuDes)
   {
      mq_close(m_QueuDes);
      m_QueuDes = -1;
   }

   m_szName = NULL;
}

/***************************************************************************
 ** FUNCTION:  MsgQCreator::poGetMsgQDescriptor()
 ***************************************************************************/
mqd_t MsgQCreator::poGetMsgQDescriptor()
{
   // Check whether the descriptor attached successfully.
   if(-1 == m_QueuDes)
   {     
     m_QueuDes = mq_open(m_szName,O_WRONLY);//0644
   }//if(-1 == m_QueuDes)
   return m_QueuDes; 
}

/***************************************************************************
 ** FUNCTION: void IPCMessageQueue::vDropBuffer(void *buf)
 ***************************************************************************/
void IPCMessageQueue::vDropBuffer(void *buf)
{
   ETG_TRACE_USR1((" %s entered \n", __PRETTY_FUNCTION__));

   tMessageHeader *msgHeader = (tMessageHeader *)((unsigned char *)buf - sizeof(tMessageHeader));

   // sanity checks
   if (buf == NULL) return;
   if (msgHeader->token != MAGIC) return;

   // free buffer
   free(msgHeader);
}
