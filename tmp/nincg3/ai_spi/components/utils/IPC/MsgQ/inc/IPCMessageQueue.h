/***********************************************************************/
/*!
* \file   IPCMessageQueue.cpp
* \brief  Message queue for inter process communication
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message Queue for IPC
AUTHOR:         Priju K Padiyath
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
12.02.2014  | Priju K Padiyath      | Modified to be used for SPI.Base version from Mediaplayer utilities
14.10.2014  | Shihabudheen P M      | Modified the opening of message queue.
17.07.2015  | Sameer Chandra        | Added back vDropBuffer since, not using it causes mem leaks.

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#ifndef IPCMESSAGEQUEUE_H_
#define IPCMESSAGEQUEUE_H_

#include <pthread.h>
#include <mqueue.h>
#include <Lock.h>
#include "GenericSingleton.h"

#define MSG_SIZE 128  	/**< maximum size of message put on the queue, limited by /proc/sys/fs/mqueue/msgsize_max  */
#define MAX_MSG 128 	    /**< maximum number of messages on a queue, limited by /proc/sys/fs/mqueue/msg_max */
#define MAGIC 0x8976543 /**< some random but fixed number as magic token. Be careful to just use a hex value which fits to the size of int! */


/*! 
 * \enum eMessageType
 * \brief  Type of the message.
 */ 
enum eMessageType
{
   TCL_THREAD_TERMINATE_MESSAGE,
   TCL_DATA_MESSAGE
};


/*! 
 * \struct tMessageHeader
 * \brief  header for the message to be transferred.
 */ 
struct tMessageHeader
{
   size_t size; 	/**< size of the actual user data */
   int token; 	/**< a token to compare if sent data is valid */
   eMessageType MsgType;
};

/****************************************************************************/
/*!
* \class IPCMessageQueue
* \brief mqueue wrapper class
*
****************************************************************************/
class IPCMessageQueue {
public:

  /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::IPCMessageQueue(const char *name)
   ***************************************************************************/
   /*!
   * \fn     IPCMessageQueue(const char *name)
   * \brief  Constructor opens a message queue with the given name in read-write.
   *         Mainly use it for client part which opens the queue in write only 
   *         mode and sends message
   * \param  name : [IN] name name of the mqueue
   * \sa     IPCMessageQueue(const char *name,int iFlag)
   **************************************************************************/
   IPCMessageQueue(const char *name);

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::IPCMessageQueue(const char *name,int iFlag)
   ***************************************************************************/
   /*!
   * \fn     IPCMessageQueue(const char *name,int iFlag)
   * \brief  Overloaded Constructor opens a message queue with the given name 
   *         in read-write modus with permission 0644 and MAX_MSG and 
   *         MSG_SIZE as further attributes to the queue. Mainly use this 
   *         for server part which will create the queue and wait for Messages
   * \param  name : [IN] name name of the mqueue
   * \param  iFlag : [IN] Flag
   * \sa     IPCMessageQueue(const char *name)
   **************************************************************************/
   IPCMessageQueue(const char *name,int iFlag);

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::vCloseIPCMsgQueue()
   ***************************************************************************/
   /*!
   * \fn     vCloseIPCMsgQueue()
   * \brief  To close and deallocate the message queue
   * 
   **************************************************************************/
   void vCloseIPCMsgQueue();

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::~IPCMessageQueue()
   ***************************************************************************/
   /*!
   * \fn     ~IPCMessageQueue()
   * \brief  Destructor closes the message queue
   * 
   **************************************************************************/
   virtual ~IPCMessageQueue();

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::bIsOpen()
   ***************************************************************************/
   /*!
   * \fn     bIsOpen()
   * \brief  Function to check the queue status
   * 
   **************************************************************************/
   bool bIsOpen();

   /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::cpGetName()
   ***************************************************************************/
   /*!
   * \fn     cpGetName()
   * \brief  Gets the name of the mqueue set by the constructor
   * \retval Queue names
   * 
   **************************************************************************/
   const char *cpGetName();

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::vpCreateBuffer(size_t size)
   ***************************************************************************/
   /*!
   * \fn     vpCreateBuffer(size_t size)
   * \brief  creates a buffer of the given size plus the size for a related tMessageHeader
   * \param  size : [IN] size of the buffer
   * \retval pointer to buffer for user data, that is the overall buffer minus 
   *         the size of tMessageHeader
   * 
   **************************************************************************/
   void *vpCreateBuffer(size_t size);

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::iSendBuffer()
   ***************************************************************************/
   /*!
   * \fn     iSendBuffer(
   * \brief  creates a buffer of the given size plus the size for a related tMessageHeader
   * \param  data : [IN] data the data which should be send to the message queue
   * \param  prio : prio non-negative integer that specifies the priority of this message
   *                Messages are placed on the queue in decreasing order of priority
   * \retval 0 if success, non-zero if failed
   * 
   **************************************************************************/
   int iSendBuffer(void* data, unsigned int prio,eMessageType eMsgType = TCL_DATA_MESSAGE );

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::vpWaitMessage()
   ***************************************************************************/
   /*!
   * \fn     vpWaitMessage(size_t *size, unsigned int time_out_in_ms,eMessageType &eMsgType)
   * \brief  catches messages from the queue and puts them in one buffer
   * \param  size : [IN] size contains number of received bytes
   * \param  time_out_in_ms : [IN] time_out_in_ms connection time out defined in milliseconds
   * \param  eMsgType : [IN] message type
   * \retVal pointer of received buffer
   * 
   **************************************************************************/
   void *vpWaitMessage(size_t *size, unsigned int time_out_in_ms,eMessageType &eMsgType);

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::vDropMessage()
   ***************************************************************************/
   /*!
   * \fn     vDropMessage(void *message_ptr)
   * \brief  deletes the message from memory
   * \param  message_ptr : [IN] message_ptr pointer to the message, 
   *                       which was received by waitMessage
   * 
   **************************************************************************/
   void vDropMessage(void *message_ptr);

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::lGetCurMessagesCount()
   ***************************************************************************/
   /*!
   * \fn     lGetCurMessagesCount()
   * \brief  interface function to get the count of messages in the queue
   * \retVal return count of messages
   * 
   **************************************************************************/
   long lGetCurMessagesCount();

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::vFlush()
   ***************************************************************************/
   /*!
   * \fn     vFlush()
   * \brief  interface function to flush the contens of a queue
   * 
   **************************************************************************/
   void vFlush();

  /***************************************************************************
   ** FUNCTION:  IPCMessageQueue::vDropBuffer()
   ***************************************************************************/
  /*!
   * \fn     vDropBuffer(void *buf)
   * \brief  frees the memory from the address of the buffer
   * \param  buf : [IN] buf pointer to the buffer to be deleted
   *
   **************************************************************************/
    void vDropBuffer(void *buf);


private:   
   
   //! name of the message queue. A name for a message queue has to start with a slash 
   const char *mq_name; 

   //! the message queue descriptor 
   mqd_t mq; 
   
   //! lock for the sendBuffer method
   Lock mLock; 

};


/****************************************************************************/
/*!
* \class MsgQCreator
* \brief Message qeueu creator.
*
****************************************************************************/

class MsgQCreator : public GenericSingleton<MsgQCreator>
{
public:
   /***************************************************************************
   *********************************PUBLIC************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  MsgQCreator::~MsgQCreator()
   ***************************************************************************/
   /*!
   * \fn     ~MsgQCreator()
   * \brief  Destructor
   * \param  None
   * \sa     MsgQCreator(const char *name)
   **************************************************************************/
   virtual ~MsgQCreator();

   /***************************************************************************
   ** FUNCTION:  MsgQCreator::poGetMsgQDescriptor()
   ***************************************************************************/
   /*!
   * \fn     poGetMsgQDescriptor()
   * \brief  Function to open the message queu in read/write mode and retuns
   * \       the descriptor.  
   * \param  None
   * \retVal mqd_t : IPC message queue descriptor
   * \sa     
   **************************************************************************/
   mqd_t poGetMsgQDescriptor();

  /*! 
   * \friend class GenericSingleton<MsgQCreator>
   * \brief friend class declaration
   */
   friend class GenericSingleton<MsgQCreator>;

private:

  /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/
   
  /***************************************************************************
   ** FUNCTION:  MsgQCreator::MsgQCreator(const char *name)
   ***************************************************************************/
   /*!
   * \fn     MsgQCreator(const char *name)
   * \brief  Constructor.
   * \param  name : [IN] name name of the mqueue
   * \sa     ~MsgQCreator()
   **************************************************************************/
   MsgQCreator(const char * name);

   //! Message quueu descriptor
   mqd_t m_QueuDes;

   //! Name of the message queue.
   const char * m_szName;

};


#endif /* MQ_H_ */
/** @} */

