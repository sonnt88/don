/***********************************************************************/
/*!
 * \file  Timer.cpp
 * \brief Timer
 *************************************************************************
 \verbatim

    PROJECT:        Gen3
    SW-COMPONENT:   Smart Phone Integration
    DESCRIPTION:    Public interface to create timer functionality
    AUTHOR:         Shihabudheen P M
    COPYRIGHT:      &copy; RBEI

    HISTORY:
    Date        | Author                | Modification
    22.11.2013  | Shihabudheen P M      | Initial version
    30.07.2014  | Shiva Kumar G         | Fixed for the issue in timer_settime
	11.06.2015  | Sameer Chandra        | Fixed issue of pending timer getting called 
                       					  after deletion of timer.

    Note : The base version is from Mediaplayer
 \endverbatim
 *************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Timer.h"

#include <unistd.h>
#include <cerrno>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>

// includes for traces
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/Timer.cpp.trc.h"
#endif
#endif

static const int scoU8TimerInUse = 1;
static const int scoU8TimerNotInUse = 0;

#if SIGACTION_THREAD

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
static pid_t mSigactionThreadID;
static pthread_attr_t mThreadAttr;
static pthread_t mThreadHandle;

/*************************************************************************
** FUNCTION: void *Timer::SigactionThread(void *context)
*************************************************************************/
void *Timer::SigactionThread(void *context)
{
   Timer *timer = (Timer *)context;

   pthread_setcancelstate(PTHREAD_CANCEL_ENABLE ,NULL);
   pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS ,NULL);

   sigemptyset(&timer->mSigAction.sa_mask);

   timer->mSigAction.sa_sigaction = Timer::DefaultCallback;
   timer->mSigAction.sa_flags = SA_SIGINFO;

   // Register for the signal, to invoke the call back function
   sigaction(SIGUSR1, &timer->mSigAction, NULL);

   mSigactionThreadID = syscall(SYS_gettid);

   ETG_TRACE_USR4(("SigactionThread, tid=%x", mSigactionThreadID));

   // endless: processing of incoming signals 
   while(1) 
      sleep(1000);
}

#endif

/*************************************************************************
** FUNCTION: Timer::Timer()
*************************************************************************/
Timer::Timer()
{
   memset(&mSigAction, 0, sizeof(mSigAction));
#if SIGACTION_THREAD
   static int firstStartup = 1;

   // at first startup launch the thread
   if (firstStartup) 
   {
      unsigned int res;
      // start the signal receiver
      mSigactionThreadID = 0;
      res = pthread_attr_init(&mThreadAttr);
      res = pthread_create(&mThreadHandle, &mThreadAttr, &Timer::SigactionThread, (void *)this);
      if (res) {
         ETG_TRACE_ERR(("pthread_create: err=%d", res));
      }
      // wait for tid set 
      while(!mSigactionThreadID) 
         usleep(200);
      firstStartup = 0;
   }

#else
   sigemptyset(&mSigAction.sa_mask);
   mSigAction.sa_sigaction = Timer::DefaultCallback;
   mSigAction.sa_flags = SA_SIGINFO;
   sigaction(SIGUSR1, &mSigAction, NULL);
#endif
}


/*************************************************************************
** FUNCTION: Timer::~Timer()
*************************************************************************/
Timer::~Timer()
{
}

/*************************************************************************
** FUNCTION: void Timer::DefaultCallback(int signo, siginfo_t *info, void...
*************************************************************************/
void Timer::DefaultCallback(int signo, siginfo_t *info, void *context)
{
   (void)context;

   TimerContext * timercontext = (TimerContext*)(info->si_value.sival_ptr);

   if( ( scoU8TimerInUse == (timercontext->used)) && (info->si_code == SI_TIMER) && (signo == SIGUSR1))
   {

      if(!(*timercontext->pCallBackFn)(timercontext->timerId,timercontext->userObject,timercontext->userData))
      {
         ETG_TRACE_ERR(("Failed to invoke Callback function"));
      }
   }
}

/*************************************************************************
** FUNCTION: bool Timer::StartTimer(timer_t &timerIndex ,long timeMilliseconds..
*************************************************************************/
bool Timer::StartTimer(timer_t &timerIndex ,long timeMilliseconds,long intervalMilliseconds, void *pObject,tCallBackFn pCallBackFn, const void *userData)
{
   return StartTimerInternal(timerIndex , timeMilliseconds, intervalMilliseconds , pObject, pCallBackFn, userData);
}

/*************************************************************************
** FUNCTION: bool Timer::StartTimerInternal(timer_t &timerIndex ,long ....
*************************************************************************/
bool Timer::StartTimerInternal(timer_t &timerIndex ,long timeMilliseconds, long intervalMilliseconds, void *pObject,tCallBackFn pCallBackFn, const void *userData)
{

   m_Lock.s16Lock();

   struct sigevent sev;
   struct itimerspec its;
   Timer::TimerContext* poContext = NULL;

   // Find empty entry 
   unsigned int i;
   for(i=0; i<m_contexts.size(); i++) {
      if (m_contexts[i]->used == scoU8TimerNotInUse) {
         poContext = m_contexts[i];
         break;
      }
   }

   // No free slot found, append as new one 
   if (i == m_contexts.size()) {
      poContext = new TimerContext;
      m_contexts.push_back(poContext);
   }

   if(NULL == poContext)
   {
      m_Lock.vUnlock();
      return false;
   }

   poContext->self		 = this;
   poContext->userData 	 = userData;
   poContext->userObject  = pObject;
   poContext->pCallBackFn = pCallBackFn; 
   poContext->timerId     = 0; 

   // Frame sigevent 
   memset(&sev, 0, sizeof(sev));
#if SIGACTION_THREAD
   sev.sigev_notify = SIGEV_THREAD_ID;
   sev._sigev_un._tid = mSigactionThreadID;
#else
   sev.sigev_notify = SIGEV_SIGNAL;
#endif
   sev.sigev_signo = SIGUSR1;
   sev.sigev_value.sival_ptr = (void*) poContext;

   if (timer_create(CLOCK_MONOTONIC, &sev, &poContext->timerId) == -1)
   {
      ETG_TRACE_ERR(("failed to create a timer: error=%s", strerror(errno)));
      delete poContext;
      m_Lock.vUnlock();
      return false;
   }

   // set the systems timer index as return value
   timerIndex = poContext->timerId;

   //The its.it_value.tv_sec member is the elapsed time in whole seconds.  And the its.it_value.tv_nsec 
   //member represents the rest of the elapsed time in Nano seconds
   memset(&its, 0, sizeof(its)); 
   its.it_value.tv_sec = timeMilliseconds/1000;
   its.it_value.tv_nsec = (timeMilliseconds%1000)*1000000;  //1 MilliSecond=1000*1000 Nano seconds

   its.it_interval.tv_sec = intervalMilliseconds/1000;
   its.it_interval.tv_nsec = (intervalMilliseconds%1000)*1000000; //1 MilliSecond=1000*1000 Nano seconds

   if (timer_settime(poContext->timerId, 0, &its, NULL) == -1)
   {
      ETG_TRACE_ERR(("failed to start the timer "));
      delete poContext;
      m_Lock.vUnlock();
      return false;
   }

   // Context is now used and valid 
   poContext->used        = scoU8TimerInUse;

   m_Lock.vUnlock();
   return true;
}

/*************************************************************************
** FUNCTION: void Timer::CancelTimer (timer_t timerIndex)
*************************************************************************/
void Timer::CancelTimer (timer_t timerIndex)
{
   int retVal;

   // only valid id's 
   if (timerIndex == 0) 
      return;

   m_Lock.s16Lock();
   for(unsigned int iter = 0; iter < m_contexts.size(); iter++)
   {
      if(timerIndex == m_contexts[iter]->timerId && m_contexts[iter]->used == scoU8TimerInUse)
      {
         struct itimerspec its;

         //Stop the timer 
         memset(&its, 0, sizeof(its)); 
         if (timer_settime(m_contexts[iter]->timerId, 0, &its, NULL) == -1) {
            ETG_TRACE_ERR(("failed to stop the timer %d: %s", m_contexts[iter]->timerId, strerror(errno)));
         }

         // Delete the timer
         retVal = timer_delete(m_contexts[iter]->timerId);
         if (retVal == -1)
         {
            ETG_TRACE_ERR(("failed to delete the timer %d: %s , may be timer is already stopped", m_contexts[iter]->timerId, strerror(errno)));
         }
         // Mark this slot as not used anymore 
         m_contexts[iter]->used = scoU8TimerNotInUse;

         break;
      }
   }

   m_Lock.vUnlock();
}
