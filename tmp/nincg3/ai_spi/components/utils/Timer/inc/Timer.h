/***********************************************************************/
/*!
 * \file  Timer.h
 * \brief Timer
 *************************************************************************
 \verbatim

    PROJECT:        Gen3
    SW-COMPONENT:   Smart Phone Integration
    DESCRIPTION:    Public interface to create timer functionality
    AUTHOR:         Shihabudheen P M
    COPYRIGHT:      &copy; RBEI

    HISTORY:
    Date        | Author                | Modification
    22.11.2013  | Shihabudheen P M      | Initial version
    09.02.2015  | Ramya Murthy          | Modified for Singleton usage

    Note : The base version is from Mediaplayer
 \endverbatim
 *************************************************************************/
#ifndef _TIMER_H_
#define _TIMER_H_

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
// Use a seperate thread to recieve signal and process it.
#define SIGACTION_THREAD 1

/******************************************************************************
 | Include headers and namespaces(scope: global)
 |----------------------------------------------------------------------------*/
#include <signal.h>
#include <ctime>
#include <vector>
#include "Lock.h"
#include "GenericSingleton.h"

#if SIGACTION_THREAD
#include <sys/types.h>
#endif

/****************************************************************************/
/*!
* \class Timer
* \brief Timer implementation
*
* Class which provide high resolution timer features. It is implemented using 
* linux methods like sigaction, sigevent and the posix timer. Implementation 
* consists of both Thread based signaling and process based signaling. By 
* default, it uses thread based implementation 
****************************************************************************/
class Timer : public GenericSingleton<Timer>
{

public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /*************************************************************************
   ** FUNCTION:  virtual Timer::~Timer()
   *************************************************************************/
   /*!
   * \fn    virtual ~Timer()
   * \brief Destructor
   * \sa    Timer()
   *************************************************************************/
   virtual ~Timer();

   /*************************************************************************
   ** FUNCTION:  typedef bool (*tCallBackFn)(timer_t timerID , void *pObject,..
   *************************************************************************/
   /*!
   * \fn    typedef bool (*tCallBackFn)(timer_t timerID , void *pObject, ....)
   * \brief Signature of timer callback function
   * \param timerID : [IN] Timer user index which differentiates the multiple 
   * \      timer instances
   * \param pObject : [IN] Pointer to object using timer feature
   * \param userData : [IN] pointer to userData supplied during timer start
   *************************************************************************/
   typedef bool (*tCallBackFn)(timer_t timerID , void *pObject, const void *userData);

   /*************************************************************************
   ** FUNCTION:  bool Timer::StartTimer(timer_t &timerIndex, long ....
   *************************************************************************/
   /*!
   * \fn     bool StartTimer(timer_t &timerIndex, long timeMilliseconds, ...
   * \brief  Create a timer and start it immediately
   * \param  timerIndex : [IN] Timer user index to differentiate multiple timers 
   * \       from same user
   * \param  timeMilliseconds : [IN] First timeout in milliseconds
   * \param  intervalMilliseconds : [IN] Timer interval in milliseconds
   * \param  pObject : [IN] Object context for timer callback call(class)
   * \param  pCallBackFn : [IN] Function inside the object that will be 
   * \       called on timer expirin.
   * \param  userData : [IN] additional pointer to user supplied data which 
   * \       will be send back via user callback function when the timer expires
   * \retVal bool : True if suuceess, false otherwise
   * \sa     StartSMTimer()
   *************************************************************************/
   bool StartTimer(timer_t &timerIndex, long timeMilliseconds, long intervalMilliseconds, void *pObject,tCallBackFn pCallBackFn, const void *userData);

   /*************************************************************************
   ** FUNCTION:  void Timer::CancelTimer (timer_t timerIndex)
   *************************************************************************/
   /*!
   * \fn    void CancelTimer (timer_t timerIndex)
   * \brief Stops the timer
   * \param timerIndex : [IN] Timer Index for timer which needs to be cancelled
   * 
   *************************************************************************/
   void CancelTimer (timer_t timerIndex);

   /*!
   * \brief   Generic Singleton class
   */
   friend class GenericSingleton<Timer>;

   /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/
private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/

   /*************************************************************************
   ** FUNCTION:  Timer::Timer()
   *************************************************************************/
   /*!
   * \fn    Timer()
   * \brief Constructor
   * \sa    ~Timer()
   *************************************************************************/
   Timer();

   /*! 
   * \struct TimerContext
   * Timer context, context details
   */
    typedef struct
    {
        timer_t timerId;
        Timer   *self;
        void    *userObject;
        const void 	*userData;
        tCallBackFn pCallBackFn;
        int used;
    }TimerContext;

    // mutexLock
    Lock m_Lock;

    // Vector to hold the contexts
    std::vector <Timer::TimerContext *> m_contexts;

    // Sigaction structure object for timer
    struct sigaction mSigAction;

#if SIGACTION_THREAD
   /*************************************************************************
   ** FUNCTION:   static void *Timer::SigactionThread(void *context)
   *************************************************************************/
   /*!
   * \fn     static void *SigactionThread(void *context)
   * \brief  Sigaction thread 
   * \param  context : [IN] Pointer to the context.
   *************************************************************************/
    static void *SigactionThread(void *context);
#endif

   /*************************************************************************
   ** FUNCTION: static void Timer::DefaultCallback(int signo, siginfo_t *info..
   *************************************************************************/
   /*!
   * \fn     static void DefaultCallback(int signo, siginfo_t *info...
   * \brief  Default call back function to trigger the actual callbacks
   * \param  signo : [IN] Signal number
   * \param  info : [IN] Pointer to the data send by the sigevent
   * \param  context : [IN] Pointer to the context
   *************************************************************************/
    static void DefaultCallback(int signo, siginfo_t *info, void *context);

   /*************************************************************************
   ** FUNCTION: bool Timer::StartTimerInternal(timer_t &timerIndex ,long...
   *************************************************************************/
   /*!
   * \fn     bool StartTimerInternal(timer_t &timerIndex ,long timeMilliseconds
   * \brief  Start the posix timers
   * \param  timerIndex : [IN] Timer user index to differentiate multiple timers 
   * \       from same user
   * \param  timeMilliseconds : [IN] First timeout in milliseconds
   * \param  intervalMilliseconds : [IN] Timer interval in milliseconds
   * \param  isSMtimer : [IN] Differentiate SMtimer and normal timer
   * \param  pObject : [IN] Object context for timer callback call(class)
   * \param  pCallBackFn : [IN] Function inside the object that will be 
   * \       called on timer expirin.
   * \param  userData : [IN] additional pointer to user supplied data which 
   * \       will be send back via user callback function when the timer expires
   * \retVal bool : True if suuceess, false otherwise
   *************************************************************************/
    bool StartTimerInternal(timer_t &timerIndex ,long timeMilliseconds, long intervallMilliseconds, void *pObject,tCallBackFn pCallBackFn, const void *userData);

    /***************************************************************************
    ****************************END OF PRIVATE *********************************
    ***************************************************************************/

};

#endif //_TIMER_H_

/** @} */
