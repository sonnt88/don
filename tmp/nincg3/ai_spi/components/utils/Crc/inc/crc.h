//! Taken from AHL label : DI_COMMONBASE_14.0V05

/*!
 *******************************************************************************
 * \file             crc.h
 * \brief            check sum calculation utility
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:    calculates checksum from a data stream the 32-bit CRC sum.
 The calculation is performed using a 32-bit Table which is split into
 16-bit high part and low part
 COPYRIGHT:      &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 01.02.2014 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef SPI_CRC_H
#define SPI_CRC_H

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// 2x16-Bit-Tables
extern uint16_t gaCrcTable_High[256];
extern uint16_t gaCrcTable_Low[256];

// Calculation of the 32-bit CRC sum with 32bit variable for CRC

/***************************************************************************
** FUNCTION:  u32CalcCRC32
***************************************************************************/
/*!
* \fn     u32CalcCRC32
* \brief   Calculates of the 32-bit CRC sum with 32bit variable for CRC
* \param  pczBlk_Adr: address of the block containing data
* \param  u32Blk_Len: size of data
* \retval calculated crc value
* \sa     ~spi_tclConnMngr()
**************************************************************************/
extern uint32_t u32CalcCRC32(const unsigned char* pczBlk_Adr, uint32_t u32Blk_Len);

/***************************************************************************
** FUNCTION:  u16CalcCRC16
***************************************************************************/
/*!
* \fn     u16CalcCRC16
* \brief   Calculates of the 16-bit CRC 
* \param  u8zMessage: address of the block containing data
* \param  u32MsgSize: size of data
* \retval calculated 16 bit crc value
* \sa     ~spi_tclConnMngr()
**************************************************************************/
extern uint16_t u16CalcCRC16(const unsigned char* pczMessage, uint32_t u32MsgSize);

#ifdef __cplusplus
}
#endif

#endif // SPI_CRC_H
