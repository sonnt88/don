/***********************************************************************/
/*!
* \file   FileHandler.h
* \brief  File Handling
*************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    File Handling
   AUTHOR:         Shiva Kumar Gurija
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      22.07.2013  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/
#ifndef _FILEHANDLER_H_
#define _FILEHANDLER_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <fcntl.h>
//#include "BaseTypes.h"
/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/
#define ERROR ((signed int) -1)
#define OK  ((signed int) 0)
#define UNKNOWN_ERROR 0
#define MAX_BUFFERSIZE 4096


namespace spi {
   namespace io {

      /*! 
      * \enum spi_enFileAccess
      * File access type, extended access types.
      */
      enum spi_enFileAccess
      {
         SPI_EN_RDONLY   =  O_RDONLY,   ///< Enum value for read only access.
         SPI_EN_WRONLY   =  O_WRONLY,   ///< Enum value for write only access.
         SPI_EN_RDWR     =  O_RDWR,     ///< Enum value for RW access.
         SPI_EN_APPEND   =  O_APPEND,   ///< Enum value for appending.
         SPI_EN_TRUNC    =  O_TRUNC,    ///< Enum value for truncating.
         SPI_EN_CREAT    =  O_CREAT,    ///< Enum value for create a file.
         SPI_EN_REMOVE,                 ///< Enum value to remove a file.
      }; // enum spi_enFileAccess

      /****************************************************************************/
      /*!
      * \class FileHandler
      * \brief File Handling
      *
      * File handling is implemented based on the Posix calls.
      * This gives you the abstraction of Posix file system calls and
      * some intelligence to perform operations on files and returns
      * proper error codes on failures.
      *
      * This is based on design pattern \ref RAII "RAII"
      ****************************************************************************/
      class FileHandler
      {
      public:

         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

         /*************************************************************************
         ** FUNCTION:  virtual FileHandler::~FileHandler()
         *************************************************************************/
         /*!
         * \fn    virtual ~FileHandler()
         * \brief Destructor
         * \sa    FileHandler(const char* pcocFileName, spi_enFileAccess enAccess)
         *************************************************************************/
         virtual ~FileHandler();

         /*************************************************************************
         ** FUNCTION:  FileHandler::FileHandler(const char* pcocFileName,..
         *************************************************************************/
         /*!
         * \fn     FileHandler(const char* pcocFileName, spi_enFileAccess enAccess)
         * \brief  Parameterized constructor
         * \param  pcocFileName : [IN] FileName
         * \param  enAccess]    : [IN] File Access Mode
         * \sa     ~FileHandler()
         *************************************************************************/
         explicit FileHandler(const char* pcocFileName, spi_enFileAccess enAccess);

         /**************************************************************************
         ** FUNCTION:  virtual bool FileHandler::bIsValid() const
         **************************************************************************/
         /*!
         * \fn      virtual bool bIsValid() const
         * \brief   This function checks the validity of the file handle.
         * \retval  bool : TRUE if File handle is valid, FALSE otherwise.
         * \sa      bValidateFile(const char* csFileName)
         **************************************************************************/
         virtual bool bIsValid() const;

         /**************************************************************************
         ** FUNCTION:  virtual bool FileHandler::bFRead(signed char* pscBuffer ...)
         **************************************************************************/
         /*!
         * \fn      virtual bool bFRead(signed char* pscBuffer, unsigned int u32MaxLength)
         * \brief   This function reads a number of bytes from a data file.
         * \param   pscBuffer    : [OUT] Pointer to Buffer
         * \param   u32MaxLength : [IN]  Maximal size of the buffer in bytes
         * \retval  bool        : TRUE or FALSE in case of error.
         * \sa      bFWrite()
         ***************************************************************************/
         virtual bool bFRead(signed char* pscBuffer, unsigned int u32MaxLength);

         /**************************************************************************
         ** FUNCTION:  virtual bool FileHandler::bFRead(char* pcBuffer ...)
         **************************************************************************/
         /*!
         * \fn      virtual bool bFRead(signed char* ps8Buffer, unsigned int u32MaxLength)
         * \brief   This function reads a number of bytes from a data file.
         * \param   pcBuffer     : [OUT] Pointer to Buffer
         * \param   u32MaxLength : [IN]  Maximal size of the buffer in bytes
         * \retval  bool       : TRUE or FALSE in case of error.
         * \sa      bFWrite()
         ***************************************************************************/
         virtual bool bFRead(char* pcBuffer, unsigned int u32MaxLength);

         /***************************************************************************
          ** FUNCTION:  virtual bool FileHandler::bFRead(unsigned char* pucBuffer ...)
          ***************************************************************************/
         /*!
         * \fn      virtual bool bFRead(unsigned char* pucBuffer, unsigned int u32MaxLength)
         * \brief   This function reads a number of bytes from a data file.
         * \param   pcBuffer     : [OUT] Pointer to Buffer
         * \param   u32MaxLength : [IN]  Maximal size of the buffer in bytes
         * \retval  bool       : TRUE or FALSE in case of error.
         * \sa      bFWrite()
         ***************************************************************************/
         virtual bool bFRead(unsigned char* pucBuffer, unsigned int u32MaxLength);

         /**************************************************************************
         ** FUNCTION:  virtual bool FileHandler::bFWrite(const signed char* pcoscBuf..
         **************************************************************************/
         /*!
         * \fn      virtual bool bFWrite(const signed char* pcoscBuffer, unsigned int u32Length)
         * \brief   This function writes a number of bytes in to a data file.
         * \param   pcoscBuffer  : [IN] Pointer to Buffer
         * \param   u32Length    : [IN] Size of the buffer in bytes
         * \retval  bool        : TRUE or FALSE in case of error.
         * \sa      bFRead()
         ***************************************************************************/
         virtual bool bFWrite(const signed char* pcoscBuffer, unsigned int u32Length);

         /**************************************************************************
         ** FUNCTION:  virtual bool FileHandler::bFRename(const char* cszNewFi..
         ***************************************************************************/
         /*!
         * \fn      virtual bool bFRename(const char* pcocNewFileName, const char* pcocOldFileName)
         * \brief   This function renames a file which was opened earlier.
         * \param   pcocNewFileName : [IN] New File Name
         * \param   pcocOldFileName : [IN] Old File Name
         * \retval  bool          : TRUE or FALSE in case of error.
         **************************************************************************/
         virtual bool bFRename(const char* pcocNewFileName, const char* pcocOldFileName);

         /**************************************************************************
         ** FUNCTION:  virtual bool FileHandler::bFSeek(signed int s32Offset, t..)
         **************************************************************************/
         /*!
         * \fn      virtual bool bFSeek(signed int s32Offset, signed int s32Origin)
         * \brief   This function sets the data file pointer for a following read-or
         *          write operation.
         *
         * The new position is obtained from offset character relative to the
         * origin Position.
         * SEEK_SET:
         * The offset is set to offset bytes.
         * SEEK_CUR:
         * The offset is set to its current location plus offset bytes.
         * SEEK_END:
         * The offset is set to the size of the file plus offset bytes.
         *
         * \param    s32Offset  :  [IN] Offset to seek from the Reference position
         * \param    s32Origin  :  [IN] Reference position
         * \retval   bool      : TRUE or FALSE in case of error.
         * \sa       s32FTell()
         **************************************************************************/
         virtual bool bFSeek(signed int s32Offset, signed int s32Origin);

         /**************************************************************************
         ** FUNCTION:  virtual signed int FileHandler::s32FTell()
         **************************************************************************/
         /*!
         * \fn      virtual signed int s32FTell()
         * \brief   This function returns the current data file position for an
         *          opened data file.
         * \retval  signed int      : Current position in opened data file or Error.
         * \sa      bFSeek()
         **************************************************************************/
         virtual signed int s32FTell();

         /***************************************************************************
         ** FUNCTION:  virtual signed int FileHandler::s32FPrintf(const char* pcoc...
         ***************************************************************************/
         /*!
         * \fn      virtual signed int s32FPrintf(const char* pcocFormat, ...)
         * \brief   This function writes a formatted string in to an earlier opened
         *          data file. The size of the expanded string should not exceed
         *          MAX_BUFFERSIZE.
         * \param   pcocFormat : [IN] Format string
         * \param    ...       : [IN] Variable parameterized list
         * \retval  signed int      : Number of the written bytes or OSAL_ERROR.
         **************************************************************************/
         virtual signed int s32FPrintf(const char* szFormat, ...);

         /**************************************************************************
         ** FUNCTION:  virtual signed int FileHandler::s32GetSize()
         **************************************************************************/
         /*!
         * \fn      virtual signed int s32GetSize()
         * \brief   This function determines the size of a data file.
         * \retval  signed int     : Size in bytes or OSAL_ERROR.
         **************************************************************************/
         virtual signed int s32GetSize();

         /***************************************************************************
         ** FUNCTION:  static bool bValidateFile(const char* pcocFileName)
         ***************************************************************************/
         /*!
         * \fn      static bool bValidateFile(const char* pcocFileName)
         * \brief   This function checks the validity of a file.
         * \param   pcocFileName :  [IN] File name (inclusive of file path)
         * \retval  bool]      : TRUE if File is valid, FALSE otherwise.
         * \sa      bIsValid()
         **************************************************************************/
         static bool bValidateFile(const char* pcocFileName);

         /***************************************************************************
         ** FUNCTION:  unsigned int FileHandler::u32ErrorCode() const
         ***************************************************************************/
         /*!
         * \fn      unsigned int u32ErrorCode() const
         * \brief   This function returns the last error code during the file
         *          handling
         * \retval  unsigned int : Error code
         * \sa      vErrorCode(tCU32 cu32ErrorCode)
         **************************************************************************/
         unsigned int u32ErrorCode() const;

         /***************************************************************************
         ****************************END OF PUBLIC***********************************
         ***************************************************************************/


      protected:

         /***************************************************************************
         *********************************PROTECTED**********************************
         ***************************************************************************/

         /*************************************************************************
         ** FUNCTION:  FileHandler::FileHandler()
         **************************************************************************/
         /*!
         * \fn     FileHandler()
         * \brief  Constructor
         *
         * This is Protected member function to avoid creation of
         * default constructor.
         *
         * \sa     FileHandler(const char* csFileName, spi_enFileAccess enAccess)
         *************************************************************************/
         FileHandler();

         /************************************************************************
         ** FUNCTION:  FileHandler::FileHandler(const shl_tclFile...
         *************************************************************************/
         /*!
         * \fn     FileHandler(const FileHandler &otrSrc)
         * \brief  Copy constructor
         *
         * This is Protected member function to avoid the usage of
         * default copy constructors.

         * \param  otrSrc& : [IN] Reference to source object
         *************************************************************************/
         FileHandler(const FileHandler &otrSrc);

         /*************************************************************************
         ** FUNCTION: FileHandler::FileHandler& operator=(const ...
         *************************************************************************/
         /*!
         * \fn      FileHandler& operator= (const FileHandler &otrSrc)
         * \brief   Assignment operator
         *
         * This is  Protected member function to avoid the usage of
         * default  assignment constructors.
         *
         * \param   otrSrc& : [IN] Reference to source object
         * \retval  FileHandler&
         *************************************************************************/
         FileHandler& operator= (const FileHandler &otrSrc);

         /**************************************************************************
         ** FUNCTION:  void FileHandler::vOnInit(const char* pcocFileName, enFi..
         **************************************************************************/
         /*!
         * \fn      void vOnInit(const char* pcocFileName, spi_enFileAccess enAcc)
         * \brief   File handler initializer function.
         * \param   pcocFileName  :  [IN] File name (inclusive of file path)
         * \param   enAcc         :  [IN] File Access type
         * \retval  void
         * \sa      vFOpen(),vFCreat(),vFRemove(),vFClose()
         **************************************************************************/
         void vOnInit(const char* pcocFileName, spi_enFileAccess enAcc);

         /***************************************************************************
         ** FUNCTION:  void FileHandler::vFOpen(const char* ...)
         ***************************************************************************/
         /*!
         * \fn      void vFOpen(const char* pcocFileName, spi_enFileAccess enAcc)
         * \brief   This function opens a data file with the specified options.
         * \param   pcocFileName : [IN] File name (inclusive of file path)
         * \param   enAcc        : [IN] File Access type
         * \retval  void
         * \sa      s32OpenFile(),vFClose()
         **************************************************************************/
         void vFOpen(const char* pcocFileName, spi_enFileAccess enAcc);

         /***************************************************************************
         ** FUNCTION:  void FileHandler::vFCreat(const char* ...)
         ***************************************************************************/
         /*!
         * \fn      void vFCreat(const char* pcocFileName, spi_enFileAccess enAcc = SHL_EN_RDWR)
         * \brief   This function creates a new data file with the specified options.
         * \param   pcocFileName : [IN] File name (inclusive of file path)
         * \param   enAcc        : [IN] File Access type
         * \retval  void
         * \sa      s32CreatFile(),vFClose()
         **************************************************************************/
         void vFCreat(const char* pcocFileName, spi_enFileAccess enAcc = SPI_EN_RDWR);

         /***************************************************************************
         ** FUNCTION:  void FileHandler::vFClose()
         ***************************************************************************/
         /*!
         * \fn      void vFClose()
         * \brief   This function closes an earlier opened file and shall deallocate
         *          the file descriptor.
         * \retval  void
         * \sa      vFOpen(),vFCreat()
         **************************************************************************/
         void vFClose();

         /***************************************************************************
         ** FUNCTION:  void FileHandler::vErrorCode(const unsigned int cou32ErrorCode)
         ***************************************************************************/
         /*!
         * \fn      void vErrorCode(const unsigned int cou32ErrorCode) const
         * \brief   This function shall assert the failure reason for opening or
         *          creating a file
         * \param   cou32ErrorCode : Error code
         * \retval  void
         **************************************************************************/
         void vErrorCode(const unsigned int cou32ErrorCode) const;

         /***************************************************************************
         ** FUNCTION:  void FileHandler::vFRemove(const char* pcocFileName)
         ***************************************************************************/
         /*!
         * \fn      void vFRemove(const char* pcocFileName)
         * \brief   This funstion is to delete a file.
         *
         *  This function shall cause the file named by the pathname pointed
         *  to by \c pcocFileName to be no longer accessible by that name.
         *  A subsequent attempt to open the file using that name shall fail,
         *  unless it is created anew.
         *
         * \param   pcocFileName  : [IN] File name (inclusive of file path)
         * \retval  void
         * \sa      vFCreat(),s32CreatFile()
         **************************************************************************/
         void vFRemove(const char* pcocFileName);

         /***************************************************************************
         ** FUNCTION:  signed int FileHandler::s32OpenFile(const char* ...) const
         ***************************************************************************/
         /*!
         * \fn      signed int s32OpenFile(const char* pcocFileName,spi_enFileAccess enAcc) const
         * \brief   This function abstracts the opening of a data file
         *          with the specified access modes.
         * \param   pcocFileName : [IN] File name (inclusive of file path)
         * \param   enAcc        : [IN] File Access type
         * \retval  signed int        : File Descriptor
         * \sa      vFOpen()
         **************************************************************************/
         signed int s32OpenFile(const char* pcocFileName,spi_enFileAccess enAcc) const;

         /***************************************************************************
         ** FUNCTION:  signed int FileHandler::s32CreatFile(const char* ...) const
         ***************************************************************************/
         /*!
         * \fn      signed int s32CreatFile(const char* pcocFileName,spi_enFileAccess enAcc) const
         * \brief   This function abstracts the creation of a data file with the
         * specified access modes and opening the file based on the access mode.
         * \note    Interpretation of File Access type:
         *          - SPI_EN_CREAT  : Creates a new file if the file does not exists.
         *          In case the file exists, the file is truncated and opened.
         *          - SPI_EN_TRUNC  : Truncates a file to '0'. Similar to creating a file
         * \param   pcocFileName : [IN] File name (inclusive of file path)
         * \param   enAcc        : [IN] File Access type
         * \retval  signed int        : File Descriptor
         * \sa      vFCreat()
         ****************************************************************************/
         signed int s32CreatFile(const char* pcocFileName,spi_enFileAccess enAcc) const;

         //! Global File Descriptor
         signed int m_s32IOFileDesc ;

         //! Error Code
         unsigned int m_u32ErrorCode ;

         /***************************************************************************
         ****************************END OF PROTECTED********************************
         ***************************************************************************/

      private:
         /***************************************************************************
         *********************************PRIVATE************************************
         ***************************************************************************/

         /***************************************************************************
         ****************************END OF PRIVATE *********************************
         ***************************************************************************/

      }; // class FileHandler

   } //namespace io
} //namespace spi

#endif

////////////////////////////////////////////////////////////////////////////////
// <EOF>
