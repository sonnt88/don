/***********************************************************************/
/*!
* \file   FileHandler.cpp
* \brief  File Handling
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    File Handling
AUTHOR:         Shiva Kumar Gurija
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
22.07.2013  | Shiva Kumar Gurija    | Initial Version

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>

#include "FileHandler.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/FileHandler.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
namespace spi 
{
   namespace io 
   {
      /*************************************************************************
      ** FUNCTION:  virtual FileHandler::~FileHandler()
      *************************************************************************/
      FileHandler::~FileHandler()
      {
         try
         {
            //Close the opened by file.
            vFClose();
         }
         catch (...)
         {

         }
      }

      /*************************************************************************
      ** FUNCTION:  FileHandler::FileHandler(const char* pcocFileName, ...)
      *************************************************************************/
      FileHandler::FileHandler(const char* pcocFileName,spi_enFileAccess enAccess) :
      m_s32IOFileDesc(ERROR), m_u32ErrorCode(OK)
      {

         if (NULL != pcocFileName)
         {
            ETG_TRACE_USR2(( "Open File - %s  ",pcocFileName));
            ETG_TRACE_USR2(( "File Access Mode - %d\n",enAccess));
            //Perform the requested operation
            vOnInit(pcocFileName, enAccess);
         }
         else
         {
            ETG_TRACE_ERR(("File Name is null\n"));
         }
      }

      /***************************************************************************
      ** FUNCTION:  void FileHandler::vOnInit(const char* pcocFileName, enFi..
      ***************************************************************************/
      void FileHandler::vOnInit(const char* pcocFileName, spi_enFileAccess enAcc)
      {
         ETG_TRACE_USR1(("vOnInit() entered\n"));

         //S_IRWXU  -  read, write, execute/search by owner
         //S_IRWXG  -  read, write, execute/search group
         //S_IRWXO  -  read, write, execute/search others

         if (pcocFileName)
         {
            switch (enAcc)
            {
            case SPI_EN_RDONLY:
               {
                  // Open the file in Read only mode, if it exists
                  vFOpen(pcocFileName, enAcc);
               }
               break;
            case SPI_EN_WRONLY:
            case SPI_EN_RDWR:
            case SPI_EN_APPEND:
               {
                  //Open the file, if it exists
                  //if the file doesn't exist, create a new file and open it.
                  vFOpen(pcocFileName, enAcc);

                  if ((ERROR == m_s32IOFileDesc) && (ENOENT == m_u32ErrorCode))
                  {
                     //By default creates a file in RDWR mode
                     vFCreat(pcocFileName, enAcc);
                  }
               }
               break;
            case SPI_EN_TRUNC:
            case SPI_EN_CREAT:
               {
                  //Truncates the file to size 0 bytes, if it exists
                  //Creates a new file, if it doesn't exist
                  vFCreat(pcocFileName);
               }
               break;
            case SPI_EN_REMOVE:
               {
                  vFRemove(pcocFileName);
               }
               break;
            default:
               {
                  ETG_TRACE_USR2(("vOnInit:default case\n"));
               }
               break;
            }
         }
      }

      /***************************************************************************
      ** FUNCTION:  void FileHandler::vFOpen(const char* ...)
      ***************************************************************************/
      void FileHandler::vFOpen(const char* pcocFileName, spi_enFileAccess enAcc)
      {

         ETG_TRACE_USR1(("vFopen() entered\n"));

         //Open the file with the requested access mode.
         m_s32IOFileDesc = s32OpenFile(pcocFileName, enAcc);
         //Set the error code to NO Error.
         m_u32ErrorCode = OK;

         if (ERROR == m_s32IOFileDesc)
         {
            ETG_TRACE_USR2(("vFopen:could not open the file - %d \n", m_s32IOFileDesc ));

            m_u32ErrorCode = errno;
            //Check whether the file doesn't exist or any other error code returned.
            if (ENOENT != m_u32ErrorCode)
            {
               vErrorCode(m_u32ErrorCode);
            }
            else
            {
               ETG_TRACE_ERR(("vFopen: file doesn't exist\n"));
            }
         }

      }

      /***************************************************************************
      ** FUNCTION:  signed int FileHandler::s32OpenFile(const char* ...) const
      ***************************************************************************/
      signed int FileHandler::s32OpenFile(const char* pcocFileName,
         spi_enFileAccess enAcc) const
      {
         ETG_TRACE_USR1(("vFopen() entered\n"));
         //check OSAL code, what happens if we open file in TEXT,BINARY,APPEND mode.
         unsigned short int u16OpenMode = S_IRWXU | S_IRWXG | S_IRWXO;
         signed int s32IOFileDesc = ERROR;
         switch (enAcc)
         {
         case SPI_EN_RDONLY:
            {
               // Open the file in user Read only mode
               u16OpenMode = S_IRUSR | S_IRWXG | S_IRWXO;
               s32IOFileDesc = open(pcocFileName, enAcc, u16OpenMode);
            }
            break;
         case SPI_EN_WRONLY:
            {
               //Open the file in user Write only mode
               u16OpenMode = S_IWUSR | S_IRWXG | S_IRWXO;
               s32IOFileDesc = open(pcocFileName, enAcc, u16OpenMode);
            }
            break;
         case SPI_EN_RDWR:
            {
               //Open the file in user Read Write mode
               u16OpenMode = S_IRWXU | S_IRWXG | S_IRWXO;
               s32IOFileDesc = open(pcocFileName, enAcc, u16OpenMode);
            }
            break;
         case SPI_EN_APPEND:
            {
               //Open the file in User Read Write mode
               u16OpenMode = S_IRWXU | S_IRWXU | S_IRWXG;
               s32IOFileDesc = open(pcocFileName, O_RDWR | O_APPEND, u16OpenMode);
            }
            break;
         default:
            {
               ETG_TRACE_USR2(("vFopen:default case\n"));
            }
            break;
         }
         return s32IOFileDesc;
      }

      /***************************************************************************
      ** FUNCTION:  void FileHandler::vFCreat(const char* ...)
      ***************************************************************************/
      void FileHandler::vFCreat(const char* pcocFileName, spi_enFileAccess enAcc)
      {
         //Always create and open a file in Read Write Mode
         ETG_TRACE_USR1(("vFCreat() entered\n"));

         m_u32ErrorCode = OK;
         //By default creates a file in RDWR mode
         m_s32IOFileDesc = s32CreatFile(pcocFileName, enAcc);

         if (ERROR == m_s32IOFileDesc)
         {
            ETG_TRACE_ERR(("vFCreat:Creation failed\n"));
            m_u32ErrorCode = errno;
            vErrorCode(m_u32ErrorCode);
         }
      }

      /***************************************************************************
      ** FUNCTION:  signed int FileHandler::s32CreatFile(const char* ...) const
      ***************************************************************************/
      signed int FileHandler::s32CreatFile(const char* pcocFileName,
         spi_enFileAccess enAcc) const
      {
         ETG_TRACE_USR1(("vFopen() entered\n"));

         unsigned short int u16OpenMode = S_IRWXU | S_IRWXG | S_IRWXO;
         signed int s32IOFileDesc = ERROR;

         switch (enAcc)
         {
         case SPI_EN_WRONLY:
            {
               s32IOFileDesc = creat(pcocFileName, u16OpenMode);
               if (ERROR != s32IOFileDesc)
               {
                  //Open the file in user Write only mode
                  u16OpenMode = S_IWUSR | S_IRWXG | S_IRWXO;
                  s32IOFileDesc = open(pcocFileName, enAcc, u16OpenMode);
               }
            }
            break;
         case SPI_EN_RDWR:
         case SPI_EN_CREAT:
         case SPI_EN_APPEND:
         case SPI_EN_TRUNC:
            {
               u16OpenMode = S_IRWXU;
               //Creates a file in read write mode.
               //If the file already exists, truncate the file to 0 bytes.
               s32IOFileDesc = open(pcocFileName,O_RDWR | O_CREAT | O_TRUNC | O_CLOEXEC,u16OpenMode);
            }
            break;
         case SPI_EN_RDONLY:
         default:
            {
               ETG_TRACE_ERR(("vFopen:File will not be created with this access type\n"));
            }
            break;
         }
         return s32IOFileDesc;
      }
      /***************************************************************************
      ** FUNCTION:  void FileHandler::vFClose()
      ***************************************************************************/
      void FileHandler::vFClose()
      {

         ETG_TRACE_USR1(("vFClose() entered\n"));

         if (ERROR != m_s32IOFileDesc)
         {
            close(m_s32IOFileDesc);
            //Set the file descriptor to Error, once it is closed.
            //This will be called from destructor
            m_s32IOFileDesc = ERROR;
            ETG_TRACE_USR2(("vFClose:file closed\n"));
         }
      }

      /***************************************************************************
      ** FUNCTION:  void FileHandler::vErrorCode(const unsigned int cou32ErrorCode)
      ***************************************************************************/
      void FileHandler::vErrorCode(const unsigned int cou32ErrorCode) const
      {
         ETG_TRACE_USR1(("vErrorCode:ErrorCode - %d\n",cou32ErrorCode));

         switch (cou32ErrorCode)
         {
            //This error will occur, when the read or write was not performed well.
            //For Ex: User trying to read 100 bytes, but the file has 90 bytes only.
         case UNKNOWN_ERROR:
            //@Print error reason - Unknown error
            ETG_TRACE_ERR(("vErrorCode:Unknown error\n"));
            break;
         case EACCES:
            //@Print error reason - No access to File
            ETG_TRACE_ERR(("vErrorCode:No access to File\n"));
            break;
         case EEXIST:
            //@Print error reason - pathname already exists
            ETG_TRACE_ERR(("vErrorCode:pathname already exists \n"));
            break;
         case ENOENT:
            //@Print error reason - O_CREAT is not set and the named file does not exist
            ETG_TRACE_ERR(("vErrorCode:O_CREAT is not set and the named file does not exist\n"));
            break;
         case ENXIO:
            //@Print error reason - The current offset is beyond the end of the file or the named file is a FIFO
            ETG_TRACE_ERR(("vErrorCode:The current offset is beyond the end of the file or the named file is a FIFO\n"));
            break;
         case EBADF:
            //@Print error reason - fd is not an open file descriptor
            ETG_TRACE_ERR(("vErrorCode:fd is not an open file descriptor\n"));
            break;
         case EINVAL:
            //@Print error reason - whence is not valid. or beyond the end of a seekable device
            ETG_TRACE_ERR(("vErrorCode:whence is not valid. or beyond the end of a seekable device\n"));
            break;
         case ENOMEM:
            //@Print error reason - Out of memory (i.e., kernel memory
            ETG_TRACE_ERR(("vErrorCode:Out of memory (i.e., kernel memory)\n"));
            break;
         case EFAULT:
            //@Print error reason - pathname points outside your accessible address space
            ETG_TRACE_ERR(("vErrorCode:pathname points outside your accessible address space\n"));
            break;
         case ENOTDIR:
            //@Print error reason - A component of the path prefix of path is not a directory
            ETG_TRACE_ERR(("vErrorCode:A component of the path prefix of path is not a directory\n"));
            break;
         case ENAMETOOLONG:
            //@Print error reason - path is too long
            ETG_TRACE_ERR(("vErrorCode:path is too long\n"));
            break;
         case EFBIG:
         case EOVERFLOW:
            //@Print error reason - pathname refers to a regular file that is too large to be opened
            ETG_TRACE_ERR(("vErrorCode:pathname refers to a regular file that is too large to be opened\n"));
            break;
         case EINTR:
            //@Print error reason - While blocked waiting to complete an open of a slow device
            ETG_TRACE_ERR(("vErrorCode:While blocked waiting to complete an open of a slow device\n"));
            break;
         case EISDIR:
            //@Print error reason - pathname refers to a directory and the access requested involved writing
            ETG_TRACE_ERR(("vErrorCode:pathname refers to a directory and the access requested involved writing\n"));
            break;
         case ELOOP:
            //@Print error reason - Too many symbolic links were encountered in resolving pathname
            ETG_TRACE_ERR(("vErrorCode:Too many symbolic links were encountered in resolving pathname\n"));
            break;
         case EMFILE:
            //@Print error reason - The process already has the maximum number of files open
            ETG_TRACE_ERR(("vErrorCode:The process already has the maximum number of files open\n"));
            break;
         case ENFILE:
            //@Print error reason - Unknown error
            ETG_TRACE_ERR(("vErrorCode:The system limit on the total number of open files has been reached\n"));
            break;
         case ENODEV:
            //@Print error reason - path refers to a device special file and no corresponding device exists
            ETG_TRACE_ERR(("vErrorCode:path refers to a device special file and no corresponding device exists\n"));
            break;
         case ENOSPC:
            //@Print error reason - path was to be created but the device containing pathname has no room for the new file
            ETG_TRACE_ERR(("vErrorCode:path was to be created but the device containing pathname has no room for the new file\n"));
            break;
         case EROFS:
            //@Print error reason - pathname refers to a file on a read-only file system and write access was requested
            ETG_TRACE_ERR(("pathname refers to a file on a read-only file system and write access was requested\n"));
            break;
         case ETXTBSY:
            //@Print error reason - path refers to an executable image which is being executed and write access was requested
            ETG_TRACE_ERR(("vErrorCode:path refers to an executable image which is being executed and write access was requested\n"));
            break;
         case EDQUOT:
            //@Print error reason - the user's quota of disk blocks or inodes on the file system has been exhausted
            ETG_TRACE_ERR(("vErrorCode:the user's quota of disk blocks or inodes on the file system has been exhausted\n"));
            break;
         case ESPIPE:
            //@Print error reason - fd is associated with a pipe, socket, or FIFO
            ETG_TRACE_ERR(("vErrorCode:fd is associated with a pipe, socket, or FIFO\n"));
            break;
         default:
            ETG_TRACE_USR2(("vErrorCode:default case\n"));
            break;
         }
      }

      /***************************************************************************
      ** FUNCTION:  virtual bool FileHandler::bIsValid() const
      ***************************************************************************/
      bool FileHandler::bIsValid() const
      {
         // returns the validity of the opened file file descriptor.
         return (ERROR != m_s32IOFileDesc);
      }

      /***************************************************************************
      ** FUNCTION:  virtual bool FileHandler::bFRead(const signed char* pscBuffer ...)
      ***************************************************************************/
      bool FileHandler::bFRead(signed char* pscBuffer, unsigned int u32MaxLength)
      {
         ETG_TRACE_USR1(("bFRead() entered\n"));

         bool bRetVal = true;
         //Set the Error code NO Error
         m_u32ErrorCode = OK;

         signed int s32NumBytesRead = read(m_s32IOFileDesc, pscBuffer, (signed int) u32MaxLength);

         if (ERROR == s32NumBytesRead)
         {
            bRetVal = false;
            m_u32ErrorCode = errno;
            vErrorCode(m_u32ErrorCode);

         }
         else if (s32NumBytesRead != (signed int) u32MaxLength)
         {
            bRetVal = false;
            //Requested number bytes are not read by the read operation, return UNKNOWN_ERROR.
            m_u32ErrorCode = UNKNOWN_ERROR;
            vErrorCode(m_u32ErrorCode);
         }

         return bRetVal;
      }

      /***************************************************************************
      ** FUNCTION:  virtual bool FileHandler::bFRead(char* pcocBuffer ...)
      ***************************************************************************/
      bool FileHandler::bFRead(char* pcocBuffer, unsigned int u32MaxLength)
      {

         ETG_TRACE_USR1(("bFRead() entered\n"));

         bool bRetVal = true;
         //Set the Error code NO Error
         m_u32ErrorCode = OK;

         signed int s32NumBytesRead = read(m_s32IOFileDesc, pcocBuffer, (signed int) u32MaxLength);

         if (ERROR == s32NumBytesRead)
         {
            bRetVal = false;
            m_u32ErrorCode = errno;
            vErrorCode(m_u32ErrorCode);

         }
         else if (s32NumBytesRead != (signed int) u32MaxLength)
         {
            bRetVal = false;
            //Requested number bytes are not read by the read operation, return UNKNOWN_ERROR.
            m_u32ErrorCode = UNKNOWN_ERROR;
            vErrorCode(m_u32ErrorCode);
         }

         return bRetVal;
      }

      /***************************************************************************
      ** FUNCTION:  virtual bool FileHandler::bFRead(unsigned char* pcoucBuffer ...)
      ***************************************************************************/
      bool FileHandler::bFRead(unsigned char* pcoucBuffer, unsigned int u32MaxLength)
      {

         ETG_TRACE_USR1(("bFRead() entered\n"));
         bool bRetVal = true;
         //Set the Error code NO Error
         m_u32ErrorCode = OK;

         signed int s32NumBytesRead = read(m_s32IOFileDesc, pcoucBuffer, (signed int) u32MaxLength);

         if (ERROR == s32NumBytesRead)
         {
            bRetVal = false;
            m_u32ErrorCode = errno;
            vErrorCode(m_u32ErrorCode);

         }
         else if (s32NumBytesRead != (signed int) u32MaxLength)
         {
            bRetVal = false;
            //Requested number bytes are not read by the read operation, return UNKNOWN_ERROR.
            m_u32ErrorCode = UNKNOWN_ERROR;
            vErrorCode(m_u32ErrorCode);
         }

         return bRetVal;
      }
      /***************************************************************************
      ** FUNCTION:  virtual bool FileHandler::bFWrite(const signed char* pcoscBu ...)
      ***************************************************************************/
      bool FileHandler::bFWrite(const signed char* pcoscBuffer, unsigned int u32Length)
      {

         ETG_TRACE_USR1(("bFWrite() entered\n"));
         bool bRetVal = true;
         //Set the Error Code to NO Error
         m_u32ErrorCode = OK;

         signed int s32NumBytesWrote = write(m_s32IOFileDesc, pcoscBuffer, u32Length);
         if (ERROR == s32NumBytesWrote)
         {
            bRetVal = false;
            m_u32ErrorCode = errno;
            vErrorCode(m_u32ErrorCode);
         }
         else if (s32NumBytesWrote < (signed int) u32Length)
         {
            bRetVal = false;
            //Requested number bytes are not written by the wrote operation, return UNKNOWN_ERROR.
            m_u32ErrorCode = UNKNOWN_ERROR;
            vErrorCode(m_u32ErrorCode);

         }

         return bRetVal;
      }

      /***************************************************************************
      ** FUNCTION:  void FileHandler::vFRemove(const char* pcocFileName)
      ***************************************************************************/
      void FileHandler::vFRemove(const char* pcocFileName)
      {

         ETG_TRACE_USR1(("vFRemove() entered\n"));
         if (pcocFileName)
         {
            //Set the Error code to NO Error
            m_u32ErrorCode = OK;

            if (ERROR == remove(pcocFileName))
            {
               m_u32ErrorCode = errno;
               vErrorCode(m_u32ErrorCode);
            }
            else
            {
               ETG_TRACE_USR2(("vFRemove: %s deleted successfully\n",pcocFileName));
               m_s32IOFileDesc = ERROR;
            }
         }
      }

      /***************************************************************************
      ** FUNCTION:  virtual bool FileHandler::bFRename(const char* cszNewFi..
      ***************************************************************************/
      bool FileHandler::bFRename(const char* pcocNewFileName,
         const char* pcocOldFileName)
      {

         ETG_TRACE_USR1(("bFRename() entered\n"));
         bool bRetVal = true;
         //Set the Error Code to NO Error
         m_u32ErrorCode = OK;

         // If pcocNewFileName already exists, it will be replaced automatically
         // If new path exists but the operation fails for some reason rename() guarantees to leave an instance of new path in place.
         signed int s32FileRenamed = rename(pcocNewFileName, pcocOldFileName);

         if (ERROR == s32FileRenamed)
         {
            m_u32ErrorCode = errno;
            vErrorCode(m_u32ErrorCode);
            bRetVal = false;
         }

         return bRetVal;
      }

      /***************************************************************************
      ** FUNCTION:  virtual bool FileHandler::bFSeek(signed int s32Offset, t..)
      ***************************************************************************/
      bool FileHandler::bFSeek(signed int s32Offset, signed int s32Origin)
      {

         ETG_TRACE_USR1(("bFSeek() entered\n"));
         bool bRetVal = true;
         //Set the Error code to NO Error
         m_u32ErrorCode = OK;
         signed int s32CurrPos = -1;

         s32CurrPos = lseek(m_s32IOFileDesc, s32Offset, s32Origin);

         if (ERROR == s32CurrPos)
         {
            m_u32ErrorCode = errno;
            bRetVal = false;
            vErrorCode(m_u32ErrorCode);
         }

         return bRetVal;
      }

      /***************************************************************************
      ** FUNCTION:  virtual signed int FileHandler::s32FTell()
      ***************************************************************************/
      signed int FileHandler::s32FTell()
      {

         ETG_TRACE_USR1(("s32FTell() entered\n"));
         signed int s32CurrPos = ERROR;
         // Set the Error code to NO Error
         m_u32ErrorCode = OK;
         //Get the current position using lseek.
         s32CurrPos = lseek(m_s32IOFileDesc, 0, SEEK_CUR);

         if (ERROR == s32CurrPos)
         {
            m_u32ErrorCode = errno;
            vErrorCode(m_u32ErrorCode);
         }

         return s32CurrPos;
      }

      /***************************************************************************
      ** FUNCTION:  virtual signed int FileHandler::s32FPrintf(const char* ...
      ***************************************************************************/
      signed int FileHandler::s32FPrintf(const char* pcocFormat, ...)
      {

         ETG_TRACE_USR1(("s32FPrintf() entered\n"));
         signed int s32Size = ERROR;

         if (ERROR != m_s32IOFileDesc)
         {
            char czBuffer[MAX_BUFFERSIZE] = { 0 };
            m_u32ErrorCode = OK;
            va_list argList;

            // argList is a built-in array
            va_start(argList, pcocFormat);
            s32Size = vsnprintf(czBuffer, MAX_BUFFERSIZE, pcocFormat, argList);
            va_end(argList);

            if (s32Size >= 0)
            {
               bFWrite((const signed char*) czBuffer, (unsigned int) s32Size);
            } // if (s32Size >= 0)
            else
            {
               // Query & Set the error code
               m_u32ErrorCode = errno;
               vErrorCode(m_u32ErrorCode);
            }
         }

         return s32Size;
      }

      /***************************************************************************
      ** FUNCTION:  virtual signed int FileHandler::s32GetSize()
      ***************************************************************************/
      signed int FileHandler::s32GetSize()
      {

         ETG_TRACE_USR1(("s32GetSize() entered\n"));

         signed int s32FileSize = ERROR;
         m_u32ErrorCode = OK;

         struct stat rFileStats;

         //fstat fills the structure stat with the file statistics.
         if (ERROR == fstat(m_s32IOFileDesc, &rFileStats))
         {
            m_u32ErrorCode = errno;
            vErrorCode(m_u32ErrorCode);
         }
         else
         {
            // st_size gives the size of the file
            s32FileSize = rFileStats.st_size;
         }
         return s32FileSize;
      }

      /******************************************************************************
      ** FUNCTION:  bool FileHandler::bValidateFile(const char* pcocFileName)
      ******************************************************************************/
      bool FileHandler::bValidateFile(const char* pcocFileName)
      {
         //Create an Object with Read-Only access.
         FileHandler oFile(pcocFileName, SPI_EN_RDONLY);
         //Check if File handle is valid
         bool bRetVal = oFile.bIsValid();

         return bRetVal;

      }

      /******************************************************************************
      ** FUNCTION:  unsigned int FileHandler::u32ErrorCode() const
      ******************************************************************************/
      unsigned int FileHandler::u32ErrorCode() const
      {
         //returns the Error code
         return (m_u32ErrorCode);
      }

   } // io namespace
} //spi namespace

///////////////////////////////////////////////////////////////////////////////
// <EOF>
