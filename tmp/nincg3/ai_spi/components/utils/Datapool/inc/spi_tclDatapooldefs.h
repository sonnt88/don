/*!
*******************************************************************************
* \file              spi_tclDatapooldefs.h
* \brief             Interface to include the user defined types that are used in datapool
*******************************************************************************
\verbatim
PROJECT:       GM Gen3
SW-COMPONENT:  Smart Phone Integration
DESCRIPTION:   Interface to include the user defined types that are used in datapool
COPYRIGHT:     &copy; RBEI

HISTORY:
Date       |  Author                      | Modifications
24.06.2014 |  Shiva Kumar G               | Initial version

\endverbatim
******************************************************************************/

#ifndef SPI_TCLDATAPOOLDEFS_H_
#define SPI_TCLDATAPOOLDEFS_H_

#define NUM_SPI_TECH 3


//! Identifies the different types of devices
typedef enum
{
   //Keep the enumeration values similar to enum tenDeviceCategory defined in SPITypes.h
   e8DP_DEVCAT_DIPO = 1,
   e8DP_DEVCAT_MIRRORLINK = 2,
   e8DP_DEVCAT_ANDROIDAUTO = 3
} tenDPDevCat;


typedef struct rProjLayerAttributes
{
   tenDPDevCat enDPDevCat;
   tU16 u16TouchLayerID;
   tU16 u16TouchSurfaceID;
   tU16 u16VideoLayerID;
   tU16 u16VideoSurfaceID;
   tU16 u16LayerWidth;
   tU16 u16LayerHeight;
   tU16 u16DisplayHeightMm;
   tU16 u16DisplayWidthMm;
}trProjLayerAttributes;

typedef struct rScreenProperties
{
   tU16 u16ScreenHeight;
   tU16 u16ScreenWidth;
   tU16 u16ScreenHeightMm;
   tU16 u16ScreenWidthMm;
   //STL vectors should not be stored in data pool, since vector class includes it's own management structures
   //and size varies dynamically.
   trProjLayerAttributes aProjLayerAttr[NUM_SPI_TECH];
}trScreenProperties;

typedef enum
{
	e8DP_POSITION_UNKNOWN = 0x0,
	e8DP_POSITION_LEFT = 0x1,
	e8DP_POSITION_RIGHT = 0x2
}tenSteeringWheelPosition;

#endif //SPI_TCLDATAPOOLDEFS_H_