/***********************************************************************/
/*!
* \file  Datapool.h
* \brief Wrapper class for Datapool usage
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Wrapper class for Datapool usage
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
13.05.2014  | Ramya Murthy          | Initial Version
11.09.2014  | Shihabudheen P M      | Modified for BT MAC address writing and reading
25.06.2015  | Shiva kaumr G         | Dynamic display configuration
14.03.2016  | Chaitra Srinivasa     | Default settings

\endverbatim
*************************************************************************/

#ifndef _DATAPOOL_H_
#define _DATAPOOL_H_

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "BaseTypes.h"
#include "SPITypes.h"

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/****************************************************************************/
/*!
* \class Datapool
* \brief
* It provides methods to store & retrieve persistent data in SPI using Datapool.
****************************************************************************/
class Datapool
{
public:

   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  Datapool::Datapool()
   ***************************************************************************/
   /*!
   * \fn      Datapool()
   * \brief   Default Constructor
   * \sa      ~Datapool()
   ***************************************************************************/
   Datapool();

   /***************************************************************************
   ** FUNCTION: Datapool::~Datapool()
   ***************************************************************************/
   /*!
   * \fn      ~Datapool()
   * \brief   Destructor
   * \sa      Datapool()
   ***************************************************************************/
   virtual ~Datapool();

   /***************************************************************************
   ** FUNCTION:  tenEnabledInfo Datapool::bReadMLEnableSetting()
   ***************************************************************************/
   /*!
   * \fn      bReadMLEnableSetting
   * \brief   Provides MirrorLink On/Off setting value stored in Datapool.
   * \param   None
   * \retval  tenEnabledInfo:
   *               e8USAGE_DISABLED - to set value as OFF
   *               e8USAGE_ENABLED - to set value as ON
   ***************************************************************************/
   virtual tenEnabledInfo bReadMLEnableSetting() const;

   /***************************************************************************
   ** FUNCTION:  tenEnabledInfo Datapool::bReadDipoEnableSetting()
   ***************************************************************************/
   /*!
   * \fn      bReadDipoEnableSetting
   * \brief   Provides DiPO On/Off setting value stored in Datapool.
   * \param   None
   * \retval  tenEnabledInfo:
   *               e8USAGE_DISABLED - to set value as OFF
   *               e8USAGE_ENABLED - to set value as ON
   ***************************************************************************/
   virtual tenEnabledInfo bReadDipoEnableSetting() const;

   /***************************************************************************
   ** FUNCTION:  tenEnabledInfo Datapool::bReadAAPEnableSetting()
   ***************************************************************************/
   /*!
   * \fn      bReadAAPEnableSetting
   * \brief   Provides AAP On/Off setting value stored in Datapool.
   * \param   None
   * \retval  tenEnabledInfo:
   *               e8USAGE_DISABLED - to set value as OFF
   *               e8USAGE_ENABLED - to set value as ON
   ***************************************************************************/
   virtual tenEnabledInfo bReadAAPEnableSetting() const;

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bReadMLNotificationSetting()
   ***************************************************************************/
   /*!
   * \fn      bReadMLNotificationSetting
   * \brief   Provides MirrorLink Notification On/Off setting value
   *          stored in Datapool.
   * \param   None
   * \retval  t_Bool: true if enabled
   ***************************************************************************/
   virtual t_Bool bReadMLNotificationSetting() const;

   /***************************************************************************
   ** FUNCTION:  t_U8 Datapool::u8ReadVirginStartSetting()
   ***************************************************************************/
   /*!
   * \fn      u8ReadVirginStartSetting
   * \brief   Provides Virgin start setting values
   *          stored in Datapool.
   * \param   None
   * \retval  t_U8
   ***************************************************************************/
   virtual t_U8 u8ReadVirginStartSetting() const;

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteMLEnableSetting
   ***************************************************************************/
   /*!
   * \fn      bWriteMLNotificationSetting(t_Bool bMirrorLinkEnable)
   * \brief   Sets the MirrorLink On/Off setting value in datapool.
   * \param   [IN] enMirrorLinkEnable :
   *               e8USAGE_DISABLED - to set value as OFF
   *               e8USAGE_ENABLED - to set value as ON
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bWriteMLEnableSetting(tenEnabledInfo enMirrorLinkEnable);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteDipoEnableSetting
   ***************************************************************************/
   /*!
   * \fn      bWriteMLNotificationSetting(t_Bool bDipoEnable)
   * \brief   Sets the DiPO On/Off setting value in datapool.
   *          (True - if enabled, else False)
   * \param   [IN] enDipoLinkEnable :
   *               e8USAGE_DISABLED - to set value as OFF
   *               e8USAGE_ENABLED - to set value as ON
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bWriteDipoEnableSetting(tenEnabledInfo enDipoLinkEnable);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteAAPEnableSetting
   ***************************************************************************/
   /*!
   * \fn      bWriteAAPEnableSetting(t_Bool bAAPEnable)
   * \brief   Sets the DiPO On/Off setting value in datapool.
   *          (True - if enabled, else False)
   * \param   [IN] enAAPLinkEnable :
   *               e8USAGE_DISABLED - to set value as OFF
   *               e8USAGE_ENABLED - to set value as ON
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bWriteAAPEnableSetting(tenEnabledInfo enAAPLinkEnable);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteMLNotificationSetting(t_Bool...)
   ***************************************************************************/
   /*!
   * \fn      bWriteMLNotificationSetting
   * \brief   Sets the MirrorLink Notification On/Off setting value in datapool.
   *          (True - if enabled, else False)
   * \param   [IN] bNotificationEnabled :
   *               True - to set Notfications as enabled
   *               False - to set Notfications as disabled
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bWriteMLNotificationSetting(t_Bool bNotificationEnabled);

   /***************************************************************************
   ** FUNCTION:  t_U8 Datapool::u8WriteVirginStartSetting(t_Bool...)
   ***************************************************************************/
   /*!
   * \fn      u8WriteVirginStartSetting
   * \brief   Sets the default value at Virgin Start in datapool.
   * \param   [IN] u8VirginStart : Default value to be set at Virgin Start
   * \retval  t_U8
   ***************************************************************************/
   virtual t_U8 u8WriteVirginStartSetting(t_U8 u8VirginStart);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteBluetoothMacAddress(t_String...)
   ***************************************************************************/
   /*!
   * \fn      bWriteBluetoothMacAddress
   * \brief   Set the accessory bluetooth statck MAC address
   * \param   [IN] szBluetoothMacAddr : Bluetooth MAC address.
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bWriteBluetoothMacAddress(t_String &szBluetoothMacAddr);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bReadBluetoothMacAddress(t_String...)
   ***************************************************************************/
   /*!
   * \fn      bReadBluetoothMacAddress
   * \brief   To read the accessory bluetooth statck MAC address
   * \param   [OUT] szBluetoothMacAddr : Bluetooth MAC address.
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bReadBluetoothMacAddress(t_String &szBluetoothMacAddr);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteScreenAttributes()
   ***************************************************************************/
   /*!
   * \fn      bWriteScreenAttributes(const trDisplayAttributes& corfrDispAttr)
   * \brief   To write screen attributes to data pool
   * \param   [IN] corfrDispAttr : Screen Attributes
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bWriteScreenAttributes(const trDisplayAttributes& corfrDispAttr);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bReadScreenAttributes()
   ***************************************************************************/
   /*!
   * \fn      bReadScreenAttributes(trDisplayAttributes& rfrDispAttr)
   * \brief   To read screen attributes from data pool
   * \param   [IN] rfrDispAttr : Screen Attributes
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bReadScreenAttributes(trDisplayAttributes& rfrDispAttr);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bReadDiPODriveRestrictionInfo()
   ***************************************************************************/
   /*!
   * \fn      bReadDiPODriveRestrictionInfo(t_U8 &cu8DiPORestrictionInfo)
   * \brief   To read DiPO drive restriction info from data pool
   * \param   [OUT] cu8DiPORestrictionInfo : Restriction info.
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bReadDiPODriveRestrictionInfo(t_U8 &cu8DiPORestrictionInfo);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteDiPODriveRestrictionInfo()
   ***************************************************************************/
   /*!
   * \fn      bWriteDiPODriveRestrictionInfo(trDisplayAttributes& rfrDispAttr)
   * \brief   To write DiPO drive restriction info from data pool
   * \param   [IN] cu8DiPORestrictionInfo : Restriction info.
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bWriteDiPODriveRestrictionInfo(t_U8 cu8DiPORestrictionInfo);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bReadDriveSideInfo()
   ***************************************************************************/
   /*!
   * \fn      bReadDriveSideInfo(tenDriveSideInfo &rfenDriveSideInfo)
   * \brief   To read drive side position information.
   * \param   [OUT] rfenDriveSideInfo : Drive side information.
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bReadDriveSideInfo(tenDriveSideInfo &rfenDriveSideInfo);

   /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteDriveSideInfo()
   ***************************************************************************/
   /*!
   * \fn      bWriteDriveSideInfo(tenDriveSideInfo enDriveSideInfo)
   * \brief   Function to write the drive side information
   * \param   [IN] enDriveSideInfo : Drive side information.
   * \retval  t_Bool
   ***************************************************************************/
   virtual t_Bool bWriteDriveSideInfo(const tenDriveSideInfo enDriveSideInfo);

  /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bReadVehicleId()
   ***************************************************************************/
  /*!
   * \fn      bReadVehicleId()
   * \brief   Function to read the Vehicle Id information
   * \retval  t_Bool
   ***************************************************************************/
  virtual t_Bool bReadVehicleId(t_String &szVehicleIdentifierentifier);

  /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteVehicleId()
   ***************************************************************************/
  /*!
   * \fn      bWriteVehicleId()
   * \brief   Function to write the Vehicle Id information
   * \retval  t_Bool
   ***************************************************************************/
  virtual t_Bool bWriteVehicleId(t_String &szVehicleIdentifierentifier);

  /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteTechnologyPreference()
   ***************************************************************************/
  /*!
   * \fn      bWriteTechnologyPreference()
   * \brief   Function to write the TechnologyPreference
   * \retval  t_Bool
   ***************************************************************************/
  virtual t_Bool bWriteTechnologyPreference(tenDeviceCategory enTechnologyPreference);

  /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bReadTechnologyPreference()
   ***************************************************************************/
  /*!
   * \fn      bReadTechnologyPreference()
   * \brief   Function to write the TechnologyPreference
   * \retval  t_Bool
   ***************************************************************************/
  virtual t_Bool bReadTechnologyPreference(tenDeviceCategory &enTechnologyPreference);

  /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bWriteSelectionMode()
   ***************************************************************************/
  /*!
   * \fn      bWriteSelectionMode()
   * \brief   Function to write the SelectionMode
   * \param   [IN] enSelectionMode : Device selection mode
   * \retval  t_Bool
   ***************************************************************************/
  virtual t_Bool bWriteSelectionMode(tenDeviceSelectionMode enSelectionMode);

  /***************************************************************************
   ** FUNCTION:  t_Bool Datapool::bReadSelectionMode()
   ***************************************************************************/
  /*!
   * \fn      bReadSelectionMode()
   * \brief   Function to write the SelectionMode
   * \param   [OUT] enSelectionMode : Device selection mode
   * \retval  t_Bool
   ***************************************************************************/
  virtual t_Bool bReadSelectionMode(tenDeviceSelectionMode &enSelectionMode);


  /***************************************************************************
   ****************************END OF PUBLIC***********************************
   ***************************************************************************/

private:

   /***************************************************************************
   *********************************PRIVATE************************************
   ***************************************************************************/


   /***************************************************************************
   ****************************END OF PRIVATE *********************************
   ***************************************************************************/

};

#endif  // _DATAPOOL_H_

///////////////////////////////////////////////////////////////////////////////
// <EOF>
