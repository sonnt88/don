/***********************************************************************/
/*!
* \file  Datapool.cpp
* \brief Wrapper class for Datapool usage
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Wrapper class for Datapool usage
AUTHOR:         Ramya Murthy
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
13.05.2014  | Ramya Murthy          | Initial Version
11.09.2014  | Shihabudheen P M      | Modified for BT MAC address writing and reading
25.06.2015  | Shiva kaumr G         | Dynamic display configuration
14.03.2016  | Chaitra Srinivasa     | Default settings

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/
#include "Datapool.h"


#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include "osal_if.h"

#define DP_S_IMPORT_INTERFACE_FI
#include "dp_smartphoneintegration_if.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/Datapool.cpp.trc.h"
   #endif
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
typedef dp_tclSpiDpInternalDataMLNotificationSetting     dp_tFiMLNotifSetting;
typedef dp_tclSpiDpInternalDataMirrorLinkEnableSetting   dp_tFiMLEnableSetting;
typedef dp_tclSpiDpInternalDataDipoEnableSetting         dp_tFiDipoEnableSetting;
typedef dp_tclSpiDpInternalDataAAPEnableSetting          dp_tFiAAPEnableSetting;
typedef dp_tclSpiDpInternalDataScreenAttributes          dp_tFiScreenAttr;
typedef dp_tclSpiDpInternalDataDiPODriveRestrictionInfo  dp_tDriveRestInfo;
typedef dp_tclSpiDpInternalDataSteeringWheelPosition     dp_tSteeringWheelPos;
typedef dp_tclSpiDpInternalDataVehicleID                 dp_tVehicleId;
typedef dp_tclSpiDpInternalDataTechnologyPreference      dp_tTechnologyPreference;
typedef dp_tclSpiDpInternalDataSelectionMode             dp_tSelectionMode;
typedef dp_tclSpiDpInternalDataVirginStart               dp_tVirginStart;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

#define MAX_MAC_ADDRESS_STRLEN 20
#define MAX_VEHICLE_ID_STRLEN  20

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
*********************************PUBLIC*************************************
***************************************************************************/

/***************************************************************************
** FUNCTION:  Datapool::Datapool(const trGPSData& rfcorGpsData)
***************************************************************************/
Datapool::Datapool()
{
   ETG_TRACE_USR1(("Datapool() entered \n"));
}

/***************************************************************************
** FUNCTION: Datapool::~Datapool()
***************************************************************************/
Datapool::~Datapool()
{
   ETG_TRACE_USR1(("~Datapool() entered \n"));
}

/***************************************************************************
** FUNCTION:  tenEnabledInfo Datapool::bReadMLEnableSetting()
***************************************************************************/
tenEnabledInfo Datapool::bReadMLEnableSetting() const
{
   //! Create an object of Datapool FI class & read the data
   dp_tFiMLEnableSetting oMLEnableSetting;
   tenEnabledInfo enMLEnabled = (tenEnabledInfo)oMLEnableSetting.tGetData();

   ETG_TRACE_USR4(("Datapool::bReadMLEnableSetting: left with bMLEnabled = %u \n",
         enMLEnabled));
   return enMLEnabled;
}

/***************************************************************************
** FUNCTION:  tenEnabledInfo Datapool::bReadDipoEnableSetting()
***************************************************************************/
tenEnabledInfo Datapool::bReadDipoEnableSetting() const
{
   //! Create an object of Datapool FI class & read the data
   dp_tFiDipoEnableSetting oDipoEnableSetting;
   tenEnabledInfo enDiPOEnabled = (tenEnabledInfo)oDipoEnableSetting.tGetData();

   ETG_TRACE_USR4(("Datapool::bReadDipoEnableSetting: left with bDiPOEnabled = %u \n",
         enDiPOEnabled));
   return enDiPOEnabled;
}
/***************************************************************************
** FUNCTION:  tenEnabledInfo Datapool::bReadAAPEnableSetting()
***************************************************************************/
tenEnabledInfo Datapool::bReadAAPEnableSetting() const
{
   //! TODO Create an object of Datapool FI class & read the data
   dp_tFiAAPEnableSetting oAAPEnableSetting;
   tenEnabledInfo enAAPEnabled = (tenEnabledInfo)oAAPEnableSetting.tGetData();

   ETG_TRACE_USR4(("Datapool::bReadAAPEnableSetting: left with bAAPEnabled = %u \n",
         enAAPEnabled));
   return enAAPEnabled;
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bReadMLNotificationSetting()
***************************************************************************/
t_Bool Datapool::bReadMLNotificationSetting() const
{
   //! Create an object of Datapool FI class & read the data
   dp_tFiMLNotifSetting oMLNotifSetting;
   t_Bool bMLNotifEnabled = oMLNotifSetting.tGetData();

   ETG_TRACE_USR4(("Datapool::bReadMLNotificationSetting: left with bMLNotifEnabled = %u \n",
         bMLNotifEnabled));
   return bMLNotifEnabled;
}

/***************************************************************************
** FUNCTION:  t_U8 Datapool::u8ReadVirginStartSetting()
***************************************************************************/
t_U8 Datapool::u8ReadVirginStartSetting() const
{
   //! Create an object of Datapool FI class & read the data
   dp_tVirginStart oVirginStartSetting;
   t_U8 u8VirginStart = (tenEnabledInfo)oVirginStartSetting.tGetData();

   ETG_TRACE_USR4(("Datapool::u8ReadVirginStartSetting: left with u8VirginStart = %u \n",
         u8VirginStart));
   return u8VirginStart;
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bWriteMLEnableSetting(tenEnabledInfo...)
***************************************************************************/
t_Bool Datapool::bWriteMLEnableSetting(tenEnabledInfo enMirrorLinkEnable)
{
   ETG_TRACE_USR1(("Datapool::bWriteMLEnableSetting: entered with enMirrorLinkEnable = %u \n",
         enMirrorLinkEnable));

   //! Create an object of Datapool FI class & set the data
   dp_tFiMLEnableSetting oMLEnableSetting;
   t_S32 s32WriteError = oMLEnableSetting.s32SetData(enMirrorLinkEnable);

   if (DP_S32_NO_ERR != s32WriteError)
   {
      ETG_TRACE_ERR(("Datapool::bWriteMLEnableSetting: Write error = %u! \n", s32WriteError));
   }
   return (DP_S32_NO_ERR == s32WriteError);
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bWriteDipoEnableSetting(tenEnabledInfo...)
***************************************************************************/
t_Bool Datapool::bWriteDipoEnableSetting(tenEnabledInfo enDipoLinkEnable)
{
   ETG_TRACE_USR1(("Datapool::bWriteDipoEnableSetting: entered with enDipoLinkEnable = %u \n",
         enDipoLinkEnable));

   //! Create an object of Datapool FI class & set the data
   dp_tFiDipoEnableSetting oDipoEnableSetting;
   t_S32 s32WriteError = oDipoEnableSetting.s32SetData(enDipoLinkEnable);

   if (DP_S32_NO_ERR != s32WriteError)
   {
      ETG_TRACE_ERR(("Datapool::bWriteDipoEnableSetting: Write error = %u! \n", s32WriteError));
   }
   return (DP_S32_NO_ERR == s32WriteError);
}
/***************************************************************************
** FUNCTION:  t_Bool Datapool::bWriteAAPEnableSetting(tenEnabledInfo...)
***************************************************************************/
t_Bool Datapool::bWriteAAPEnableSetting(tenEnabledInfo enAAPLinkEnable)
{
   ETG_TRACE_USR1(("Datapool::bWriteAAPEnableSetting: entered with enAAPLinkEnable = %u \n",
         enAAPLinkEnable));
   //! Create an object of Datapool FI class & set the data
   dp_tFiAAPEnableSetting oAAPEnableSetting;
   t_S32 s32WriteError = oAAPEnableSetting.s32SetData(enAAPLinkEnable);

   if (DP_S32_NO_ERR != s32WriteError)
   {
      ETG_TRACE_ERR(("Datapool::bWriteAAPEnableSetting: Write error = %u! \n", s32WriteError));
   }
   return (DP_S32_NO_ERR == s32WriteError);
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bWriteMLNotificationSetting(t_Bool...)
***************************************************************************/
t_Bool Datapool::bWriteMLNotificationSetting(t_Bool bNotificationEnabled)
{
   ETG_TRACE_USR1(("Datapool::bWriteMLNotificationSetting: entered with bNotificationEnabled = %u \n",
         bNotificationEnabled));

   //! Create an object of Datapool FI class & set the data
   dp_tFiMLNotifSetting oMLNotifSetting;
   t_S32 s32WriteError = oMLNotifSetting.s32SetData(bNotificationEnabled);

   if (DP_S32_NO_ERR != s32WriteError)
   {
      ETG_TRACE_ERR(("Datapool::bWriteMLNotificationSetting: Write error = %u! \n", s32WriteError));
   }
   return (DP_S32_NO_ERR == s32WriteError);
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bWriteBluetoothMacAddress(t_String...)
***************************************************************************/
t_Bool Datapool::bWriteBluetoothMacAddress(t_String &szBluetoothMacAddr)
{
   ETG_TRACE_USR1(("Datapool::bWriteBluetoothMacAddress: entered with Bluetooth MAC address = %s \n",
      szBluetoothMacAddr.c_str()));
   
   dp_tclSpiDpInternalDataBluetoothMACAddress oBluetoothMacAddr;
   t_S32 s32WriteError = oBluetoothMacAddr.s32SetData(const_cast<t_Char *>(szBluetoothMacAddr.c_str()));
   
   if (DP_S32_NO_ERR != s32WriteError)
   {
      ETG_TRACE_ERR(("Datapool::bWriteBluetoothMacAddress: Write error = %u! \n", s32WriteError));
   }
   return (DP_S32_NO_ERR == s32WriteError);
}

/***************************************************************************
** FUNCTION:  t_U8 Datapool::u8WriteVirginStartSetting()
***************************************************************************/
t_U8 Datapool::u8WriteVirginStartSetting(t_U8 u8VirginStart)
{
   ETG_TRACE_USR1(("Datapool::u8WriteVirginStartSetting: entered with u8VirginStart = %u \n",
         u8VirginStart));

   //! Create an object of Datapool FI class & set the data
   dp_tVirginStart oVirginStartSetting;
   t_S32 s32WriteError = oVirginStartSetting.s32SetData(u8VirginStart);

   if (DP_S32_NO_ERR != s32WriteError)
   {
      ETG_TRACE_ERR(("Datapool::u8WriteVirginStartSetting: Write error = %u! \n", s32WriteError));
   }
   return (DP_S32_NO_ERR == s32WriteError);
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bReadBluetoothMacAddress(t_String...)
***************************************************************************/
t_Bool Datapool::bReadBluetoothMacAddress(t_String &szBluetoothMacAddr)
 {
    ETG_TRACE_USR1(("Datapool::bReadBluetoothMacAddress: entered"));
    t_Char * szMacAddress = new char[MAX_MAC_ADDRESS_STRLEN];

    dp_tclSpiDpInternalDataBluetoothMACAddress oBluetoothMacAddr;
    t_S32 s32ReadDataLength = oBluetoothMacAddr.s32GetData(szMacAddress, MAX_MAC_ADDRESS_STRLEN);

    if(0 != s32ReadDataLength)
    {
      szBluetoothMacAddr = szMacAddress;
    }
    else
    {
        ETG_TRACE_ERR(("Datapool::bReadBluetoothMacAddress: Read error occured "));
    }
    delete []szMacAddress;
    return (0 != s32ReadDataLength);
 }

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bWriteScreenAttributes()
***************************************************************************/
t_Bool Datapool::bWriteScreenAttributes(const trDisplayAttributes& corfrDispAttr)
{
   ETG_TRACE_USR1(("Datapool::bWriteScreenAttributes() entered"));

   dp_tFiScreenAttr oScreenAttr;
   trScreenProperties rScreenProp;
   rScreenProp.u16ScreenHeight = corfrDispAttr.u16ScreenHeight;
   rScreenProp.u16ScreenWidth = corfrDispAttr.u16ScreenWidth;
   rScreenProp.u16ScreenHeightMm = corfrDispAttr.u16ScreenHeightMm;
   rScreenProp.u16ScreenWidthMm = corfrDispAttr.u16ScreenWidthMm;

   for(t_U8 u8Index=0;u8Index<corfrDispAttr.vecDisplayLayerAttr.size();u8Index++)
   {
      if(u8Index < NUM_SPI_TECH)
      {
         const trDisplayLayerAttributes& corfrDispLayerAttr = corfrDispAttr.vecDisplayLayerAttr[u8Index];
         trProjLayerAttributes& rfrProjLayerAttributes = rScreenProp.aProjLayerAttr[u8Index];
         rfrProjLayerAttributes.enDPDevCat = static_cast<tenDPDevCat>(corfrDispLayerAttr.enDevCat);
         rfrProjLayerAttributes.u16VideoLayerID = corfrDispLayerAttr.u16VideoLayerID;
         rfrProjLayerAttributes.u16VideoSurfaceID = corfrDispLayerAttr.u16VideoSurfaceID;
         rfrProjLayerAttributes.u16TouchLayerID = corfrDispLayerAttr.u16TouchLayerID;
         rfrProjLayerAttributes.u16TouchSurfaceID = corfrDispLayerAttr.u16TouchSurfaceID;
         rfrProjLayerAttributes.u16LayerHeight = corfrDispLayerAttr.u16LayerHeight;
         rfrProjLayerAttributes.u16LayerWidth = corfrDispLayerAttr.u16LayerWidth;
         rfrProjLayerAttributes.u16DisplayHeightMm = corfrDispLayerAttr.u16DisplayHeightMm;
         rfrProjLayerAttributes.u16DisplayWidthMm = corfrDispLayerAttr.u16DisplayWidthMm;
      }//if(u8Index < NUM_SPI_TECH)
   }//for(t_U8 u8Index=0;u8Index<rfrDispAttr;u8Index++)

   t_S32 s32Error = oScreenAttr.s32SetData(rScreenProp);
   if (DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bWriteScreenAttributes: Error = %u", s32Error));
   }//if (DP_S32_NO_ERR != s32Error)
   return (DP_S32_NO_ERR == s32Error);
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bReadScreenAttributes()
***************************************************************************/
t_Bool Datapool::bReadScreenAttributes(trDisplayAttributes& rfrDispAttr)
{
   ETG_TRACE_USR1(("Datapool::bReadScreenAttributes() entered"));

   dp_tFiScreenAttr oScreenAttr;
   trScreenProperties rScreenProp;
   t_S32 s32Error = oScreenAttr.s32GetData(rScreenProp);

   if(DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bReadScreenAttributes: Read error occured - %u ",s32Error));
   }//if(DP_S32_NO_ERR != s32Error)
   else
   {
      rfrDispAttr.u16ScreenHeight = rScreenProp.u16ScreenHeight;
      rfrDispAttr.u16ScreenWidth = rScreenProp.u16ScreenWidth ;
      rfrDispAttr.u16ScreenHeightMm = rScreenProp.u16ScreenHeightMm;
      rfrDispAttr.u16ScreenWidthMm = rScreenProp.u16ScreenWidthMm ;

      for(t_U8 u8Index=0;u8Index<NUM_SPI_TECH;u8Index++)
      {
         trDisplayLayerAttributes rfrDispLayerAttr;
         const trProjLayerAttributes& corfrProjLayerAttributes = rScreenProp.aProjLayerAttr[u8Index];
         rfrDispLayerAttr.enDevCat = static_cast<tenDeviceCategory>(corfrProjLayerAttributes.enDPDevCat);
         rfrDispLayerAttr.u16VideoLayerID = corfrProjLayerAttributes.u16VideoLayerID;
         rfrDispLayerAttr.u16VideoSurfaceID = corfrProjLayerAttributes.u16VideoSurfaceID;
         rfrDispLayerAttr.u16TouchLayerID = corfrProjLayerAttributes.u16TouchLayerID;
         rfrDispLayerAttr.u16TouchSurfaceID = corfrProjLayerAttributes.u16TouchSurfaceID;
         rfrDispLayerAttr.u16LayerHeight = corfrProjLayerAttributes.u16LayerHeight;
         rfrDispLayerAttr.u16LayerWidth = corfrProjLayerAttributes.u16LayerWidth;
         rfrDispLayerAttr.u16DisplayHeightMm = corfrProjLayerAttributes.u16DisplayHeightMm;
         rfrDispLayerAttr.u16DisplayWidthMm = corfrProjLayerAttributes.u16DisplayWidthMm;

         rfrDispAttr.vecDisplayLayerAttr.push_back(rfrDispLayerAttr);

      }//for(t_U8 u8Index=0;u8Index<NUM_SPI_TECH;u8Index++)
   }
   return (DP_S32_NO_ERR == s32Error);
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bReadDiPODriveRestrictionInfo()
***************************************************************************/
t_Bool Datapool::bReadDiPODriveRestrictionInfo(t_U8 &cu8DiPORestrictionInfo)
{
	dp_tDriveRestInfo oDriveRestInfo;
   t_S32 s32Error = oDriveRestInfo.s32GetData(cu8DiPORestrictionInfo);

   if (DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bReadDiPODriveRestrictionInfo: Read error occured - %u ", s32Error));
   }
   else
   {
      ETG_TRACE_USR1(("Datapool::bReadDiPODriveRestrictionInfo() value %d:", cu8DiPORestrictionInfo));
   }
   return (DP_S32_NO_ERR == s32Error);
}


/***************************************************************************
** FUNCTION:  t_Bool Datapool::bWriteDiPODriveRestrictionInfo()
***************************************************************************/
t_Bool Datapool::bWriteDiPODriveRestrictionInfo(t_U8 cu8DiPORestrictionInfo)
{
	dp_tDriveRestInfo oDriveRestInfo;
   t_U8 u8DriveRestInfo = 0;
   t_S32 s32Error = oDriveRestInfo.s32GetData(u8DriveRestInfo);

   if (DP_S32_NO_ERR == s32Error)
   {
      if (cu8DiPORestrictionInfo != u8DriveRestInfo)
      {
         s32Error = oDriveRestInfo.s32SetData(cu8DiPORestrictionInfo);
      }
   }

   if (DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bWrieDiPODriveRestrictionInfo: Write error occured - %u ", s32Error));
   }
   return (DP_S32_NO_ERR == s32Error);
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bReadDriveSideInfo()
***************************************************************************/
t_Bool Datapool::bReadDriveSideInfo(tenDriveSideInfo &rfenDriveSideInfo)
{
	dp_tSteeringWheelPos oSteeringWheelPos;
   tenSteeringWheelPosition enPosition;
   t_S32 s32Error = oSteeringWheelPos.s32GetData(enPosition);
   if (DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bReadDriveSideInfo: Read error occured - %u ", s32Error));
   }
   else
   {
      rfenDriveSideInfo = (tenDriveSideInfo) enPosition;
      ETG_TRACE_USR2(("Datapool::bReadDriveSideInfo: value read  - %d ", rfenDriveSideInfo));
   }
   return (DP_S32_NO_ERR == s32Error);
}

/***************************************************************************
** FUNCTION:  t_Bool Datapool::bWriteDriveSideInfo()
***************************************************************************/
t_Bool Datapool::bWriteDriveSideInfo(const tenDriveSideInfo enDriveSideInfo)
{
	dp_tSteeringWheelPos oSteeringWheelPos;
   tenSteeringWheelPosition enCurData;
   t_S32 s32Error = oSteeringWheelPos.s32GetData(enCurData);
   if (DP_S32_NO_ERR == s32Error)
   {
      if (enDriveSideInfo != (tenDriveSideInfo) enCurData)
      {
         s32Error = oSteeringWheelPos.s32SetData((tenSteeringWheelPosition) enDriveSideInfo);
      }
   }
   if (DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bWrieDiPODriveRestrictionInfo: Write error occured - %u ", s32Error));
   }
   return (DP_S32_NO_ERR == s32Error);
}

/***************************************************************************
 ** FUNCTION:  t_Bool Datapool::bReadVehicleId()
 ***************************************************************************/
t_Bool Datapool::bReadVehicleId(t_String &szVehicleIdentifier)
{
  ETG_TRACE_USR1(("Datapool::bReadVehicleId: entered"));
  t_Char * szVehicleid = new char[MAX_VEHICLE_ID_STRLEN];
  dp_tVehicleId oVehicleId;
  t_S32 s32ReadDataLength = oVehicleId.s32GetData(szVehicleid, MAX_VEHICLE_ID_STRLEN);
  if(0 != s32ReadDataLength)
    {
     szVehicleIdentifier = szVehicleid;
     ETG_TRACE_USR1(("Datapool::bReadVehicleId: entered,length of Vehicle Id is :%d",szVehicleIdentifier.length()));
    }
  else
    {
      ETG_TRACE_ERR(("Datapool::bReadVehicleId: Read error occured "));
    }
  RELEASE_ARRAY_MEM(szVehicleid);
  return (0 != s32ReadDataLength);
}

/***************************************************************************
 ** FUNCTION:  t_Bool Datapool::bWriteVehicleId()
 ***************************************************************************/
t_Bool Datapool::bWriteVehicleId(t_String &szVehicleIdentifier)
{
  ETG_TRACE_USR1(("Datapool::bWriteVehicleId: entered with VehicleId = %s \n",szVehicleIdentifier.c_str()));
  dp_tVehicleId oVehicleId;
  t_S32 s32WriteError = oVehicleId.s32SetData(const_cast<t_Char *>(szVehicleIdentifier.c_str()));

  if (DP_S32_NO_ERR != s32WriteError)
    {
      ETG_TRACE_ERR(("Datapool::bWriteVehicleId: Write error = %u! \n", s32WriteError));
    }
  return (DP_S32_NO_ERR == s32WriteError);
}

/***************************************************************************
 ** FUNCTION:  t_Bool Datapool::bWriteTechnologyPreference()
 ***************************************************************************/
t_Bool Datapool::bWriteTechnologyPreference(tenDeviceCategory enTechnologyPreference)
{
   ETG_TRACE_USR1(("Datapool::bWriteTechnologyPreference: entered with enTechnologyPreference = %d \n",ETG_ENUM(DEVICE_CATEGORY,enTechnologyPreference)));
   dp_tTechnologyPreference oTechnologyPreference;
   t_U8 u8DeviceCategory = enTechnologyPreference;
   t_S32 s32Error = oTechnologyPreference.s32SetData(u8DeviceCategory);
   if(DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bWriteTechnologyPreference: Read error occured - %u ",s32Error));
   }
   return (DP_S32_NO_ERR == s32Error);
}

/***************************************************************************
 ** FUNCTION:  t_Bool Datapool::bReadTechnologyPreference()
 ***************************************************************************/
t_Bool Datapool::bReadTechnologyPreference(tenDeviceCategory &enTechnologyPreference)
{
   dp_tTechnologyPreference oTechnologyPreference;
   t_U8 u8DeviceCategory = 0;
   t_S32 s32Error = oTechnologyPreference.s32GetData(u8DeviceCategory);
   if (DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bReadTechnologyPreference: Read error occured - %u ", s32Error));
   }
   else
   {
      enTechnologyPreference = (tenDeviceCategory) u8DeviceCategory;
      ETG_TRACE_USR2(("Datapool::bReadTechnologyPreference: value read  - %d ", ETG_ENUM(DEVICE_CATEGORY,enTechnologyPreference)));
   }
   return (DP_S32_NO_ERR == s32Error);
}

/***************************************************************************
 ** FUNCTION:  t_Bool Datapool::bWriteSelectionMode()
 ***************************************************************************/
t_Bool Datapool::bWriteSelectionMode(tenDeviceSelectionMode enSelectionMode)
{
   ETG_TRACE_USR1(("Datapool::bWriteSelectionMode: entered with enSelectionMode = %d \n",ETG_ENUM(SELECTION_MODE,enSelectionMode)));
   dp_tSelectionMode oSelectionMode;
   t_U8 u8SelectionMode = enSelectionMode;
   t_S32 s32Error = oSelectionMode.s32SetData(u8SelectionMode);
   if(DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bWriteSelectionMode: Write error occured - %u ",s32Error));
   }
   return (DP_S32_NO_ERR == s32Error);
}

/***************************************************************************
 ** FUNCTION:  t_Bool Datapool::bReadSelectionMode()
 ***************************************************************************/
t_Bool Datapool::bReadSelectionMode(tenDeviceSelectionMode &enSelectionMode)
{
   dp_tSelectionMode oSelectionMode;
   t_U8 u8DeviceSelectionMode = 0;
   t_S32 s32Error = oSelectionMode.s32GetData(u8DeviceSelectionMode);
   if (DP_S32_NO_ERR != s32Error)
   {
      ETG_TRACE_ERR(("Datapool::bReadSelectionMode: Read error occured - %u ", s32Error));
   }
   else
   {
      enSelectionMode = (tenDeviceSelectionMode) u8DeviceSelectionMode;
      ETG_TRACE_USR2(("Datapool::bReadSelectionMode: value read  - %d ", ETG_ENUM(SELECTION_MODE,enSelectionMode)));
   }
   return (DP_S32_NO_ERR == s32Error);
}

///////////////////////////////////////////////////////////////////////////////
// <EOF>
