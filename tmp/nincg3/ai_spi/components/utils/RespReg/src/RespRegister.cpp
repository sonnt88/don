/*!
 *******************************************************************************
 * \file              RespRegister.h
 * \brief             Registration utility class to store object pointers
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Registration utility which provide interface to register and
 unregister objects derived from BaseReg class. interface to
 get registered objects is provided based on a key. It is left to
 the application to call functions on registered objects
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 29.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include "RespRegister.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/RespRegister.cpp.trc.h"
   #endif
#endif
/******************************************************************************
 | defines and macros (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/***************************************************************************
 *********************************PUBLIC*************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  RespRegister::~RespRegister()
 ***************************************************************************/
RespRegister::~RespRegister()
{
   ETG_TRACE_USR1(("RespRegister::~RespRegister() Entered \n"));
   m_MapRespRegister.clear();
}

/***************************************************************************
 ** FUNCTION: bool RespRegister::bRegisterObject(RespBase *poRespBase)
 ***************************************************************************/
bool RespRegister::bRegisterObject(RespBase *poRespBase)
{
   ETG_TRACE_USR1(("RespRegister::bRegisterObject() Entered \n"));
   bool bRetReg = false;
   if (NULL != poRespBase)
   {
      m_MapRespRegister[poRespBase->enGetRegID()].insert(poRespBase);
      bRetReg = true;
   }
   return bRetReg;
}

/***************************************************************************
 ** FUNCTION: bool RespRegister::bUnregisterObject(RespBase *poRespBase)
 ***************************************************************************/
bool RespRegister::bUnregisterObject(RespBase *poRespBase)
{
   ETG_TRACE_USR1(("RespRegister::bUnregisterObject() Entered \n"));
   bool bRetUnReg = false;
   if (m_MapRespRegister[poRespBase->enGetRegID()].end()
         != m_MapRespRegister[poRespBase->enGetRegID()].find(poRespBase))
   {
      m_MapRespRegister[poRespBase->enGetRegID()].erase(poRespBase);
      bRetUnReg = true;
   }
   return bRetUnReg;
}

/***************************************************************************
 ** FUNCTION: unsigned int RespRegister::u32GetRegisteredObjectsCount(tenRegID enRegID)
 ***************************************************************************/
unsigned int RespRegister::u32GetRegisteredObjectsCount(tenRegID enRegID)
{
   return m_MapRespRegister[enRegID].size();
}

/***************************************************************************
 ** FUNCTION: RespBase* poGetRegisteredObject(tenRegID enRegID, unsigned int u32Index)
 ***************************************************************************/
RespBase* RespRegister::poGetRegisteredObject(tenRegID enRegID, const unsigned int u32Index)
{
   RespBase *poRespBaseObj = NULL;
   std::set<RespBase*>::iterator itRegObject =
         m_MapRespRegister[enRegID].begin();
   if (u32Index < m_MapRespRegister[enRegID].size())
   {
      std::advance(itRegObject, u32Index);
      poRespBaseObj = *itRegObject;
   }
   return poRespBaseObj;
}

/***************************************************************************
 ** FUNCTION: void vClearRespRegister();
 ***************************************************************************/
void RespRegister::vClearRespRegister()
{
   ETG_TRACE_USR1(("RespRegister::vClearRespRegister() Entered \n"));
   m_MapRespRegister.clear();
}

/***************************************************************************
 *********************************PRIVATE************************************
 ***************************************************************************/

/***************************************************************************
 ** FUNCTION:  RespRegister::RespRegister()
 ***************************************************************************/
RespRegister::RespRegister()
{
   ETG_TRACE_USR1(("RespRegister::RespRegister()Entered \n"));
   m_MapRespRegister.clear();
}
