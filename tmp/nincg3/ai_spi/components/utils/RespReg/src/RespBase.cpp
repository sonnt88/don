/*!
 *******************************************************************************
 * \file              RespBase.h
 * \brief             Base class from which all responses classes are derived
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Base class which provides madndatory registration ID for the
 components to register. All the response classes derive from this
 base class and the tenRegID is supplied during creation of the derived
 response class object
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 29.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

/******************************************************************************
 | includes:
 | 1)Typedefines
 |----------------------------------------------------------------------------*/
#include "RespBase.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
      #include "trcGenProj/Header/RespBase.cpp.trc.h"
   #endif
#endif

/***************************************************************************
 ** FUNCTION:  RespBase::RespBase(tenRegID etenRegID)
 ***************************************************************************/
RespBase::RespBase(tenRegID enRegID)
{
   ETG_TRACE_USR1(("RespBase::RespBase() Entered \n"));
   m_enRegID = enRegID;
}

/***************************************************************************
 ** FUNCTION:  RespBase::~RespBase()
 ***************************************************************************/
RespBase::~RespBase()
{
   ETG_TRACE_USR1(("RespBase::~RespBase() Entered \n"));
}

/***************************************************************************
 ** FUNCTION:  tenRegID eGettenRegID()
 ***************************************************************************/
tenRegID RespBase::enGetRegID()const
{
   ETG_TRACE_USR1(("RespBase::enGetRegID() Entered \n"));
   return m_enRegID;
}

