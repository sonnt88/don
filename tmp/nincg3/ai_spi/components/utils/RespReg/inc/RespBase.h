/*!
 *******************************************************************************
 * \file              RespBase.h
 * \brief             Base class from which all responses classes are derived
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Base class which provides madndatory registration ID for the
 components to register. All the response classes derive from this
 base class and the tenRegID is supplied during creation of the derived
 response class object
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 29.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version
 26.02.2016 |  Rachana L Achar             | Added navigation element in tenRegID enum
 26.02.2016 |  Rachana L Achar             | Added notification element in tenRegID enum

 \endverbatim
 ******************************************************************************/

#ifndef RESPBASE_H_
#define RESPBASE_H_

/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include "BaseTypes.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/
typedef enum
{
   e16DISCOVERER_REGID = 1,
   e16DAP_REGID  = 2,
   e16VIEWER_REGID  = 3,
   e16AUDIO_REGID  = 4,
   e16CRCB_REGID  =5,
   e16NOTIFICATIONS_REGID  = 6,
   e16CDB_REGID  = 7,
   e16AAP_AUDIO_REGID = 8,
   e16AAP_BT_REGID  = 9,
   e16AAP_DISCOVERER_REGID  = 10,
   e16AAP_INPUT_REGID  = 11,
   e16AAP_SENSOR_REGID  = 12,
   e16AAP_SESSION_REGID  = 13,
   e16AAP_VIDEO_REGID  = 14,
   e16AAP_MEDIAPLAYBACK_REGID = 15,
   e16AAP_NAVIGATION = 16,
   e16AAP_NOTIFICATION = 17
}tenRegID;

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class RespBase
 * \brief Base class which provides madndatory registration ID for the
 *             components to register. All the response classes derive from this
 *              base class and the tenRegID is supplied during creation of the derived
 *               response class object
 */

class RespBase
{
   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  RespBase::RespBase(tenRegID eRegID)
       ***************************************************************************/
      /*!
       * \fn      RespBase(tenRegID eRegID)
       * \brief   Paramaterized constructor
       * \param   enRegID : provides the ID to be identify the type of response
       **************************************************************************/
      RespBase(tenRegID enRegID);

      /***************************************************************************
       ** FUNCTION:  RespBase::~RespBase()
       ***************************************************************************/
      /*!
       * \fn      ~RespBase()
       * \brief   Destructor
       **************************************************************************/
      ~RespBase();

      /***************************************************************************
       ** FUNCTION:  tenRegID enGetRegID()
       ***************************************************************************/
      /*!
       * \fn      tenRegID enGetRegID()
       * \brief   returns the registration ID of the calling object
       **************************************************************************/
      tenRegID enGetRegID()const;
   private:
      /*
       * \brief stores the registration ID of the calling object
       */
      tenRegID m_enRegID;
};

#endif /* RESPBASE_H_ */
