/*!
 *******************************************************************************
 * \file              RespRegister.h
 * \brief             Registration utility class to store object pointers
 *******************************************************************************
 \verbatim
 PROJECT:        Gen3
 SW-COMPONENT:  Smart Phone Integration
 DESCRIPTION:   Registration utility which provide interface to register and
 unregister objects derived from RespBase class. interface to
 get registered objects is provided based on a key. It is left to
 the application to call functions on registered objects
 COPYRIGHT:     &copy; RBEI

 HISTORY:
 Date       |  Author                      | Modifications
 29.08.2013 |  Pruthvi Thej Nagaraju       | Initial Version

 \endverbatim
 ******************************************************************************/

#ifndef RESPREGISTER_H_
#define RESPREGISTER_H_
/******************************************************************************
 | includes:
 | 1)RealVNC sdk - includes
 | 2)Typedefines
 |----------------------------------------------------------------------------*/
#include <set>
#include <map>
#include "RespBase.h"
#include "GenericSingleton.h"

/******************************************************************************
 | defines and macros and constants(scope: module-local)
 |----------------------------------------------------------------------------*/

#define CALL_REG_OBJECTS(RESPCLASS, REGID, FUNCTIONCALL)                           \
do {                                                                                                                                  \
      RespRegister *pRespRegister = RespRegister::getInstance();                             \
      if (NULL != pRespRegister)                                                                                      \
      {                                                                                                                                  \
         unsigned int u32RegObjCount =                                                                                     \
                  pRespRegister->u32GetRegisteredObjectsCount(REGID);                         \
         for (unsigned int u32ObjCnt = 0; u32ObjCnt < u32RegObjCount; u32ObjCnt++)        \
         {                                                                                                                                   \
            RespBase* poRegObj = pRespRegister->poGetRegisteredObject(REGID,       \
            u32ObjCnt);                                                                                                           \
            if(NULL != poRegObj)                                                                                              \
            {                                                                                                                                \
               RESPCLASS *pResp = static_cast<RESPCLASS*>(poRegObj);                         \
               pResp->FUNCTIONCALL;                                                                                   \
           }                                                                                                                                   \
         }                                                                                                                                       \
      }                                                                                                                                           \
   }while(0)                                                                                                                               \
/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: global)
 |----------------------------------------------------------------------------*/

/******************************************************************************
 | variable definition (scope: module-local)
 |----------------------------------------------------------------------------*/

/*!
 * \class RespRegister
 * \brief Registration utility which provide interface to register and
 *        unregister objects derived from RespBase class. interface to
 *        get registered objects is provided based on a key. It is left to
 *        the application to call functions on registered objects
 */

class RespRegister: public GenericSingleton<RespRegister>
{
   public:

      /***************************************************************************
       *********************************PUBLIC*************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  RespRegister::~RespRegister()
       ***************************************************************************/
      /*!
       * \fn      ~RespRegister()
       * \brief   Destructor
       * \sa      RespRegister()
       **************************************************************************/
      ~RespRegister();

      /***************************************************************************
       ** FUNCTION: bool RespRegister::bRegisterObject(RespBase *poRespBase)
       ***************************************************************************/
      /*!
       * \fn      bRegisterObject(RespBase *poRespBase)
       * \brief   Registers an object pointer of RespBase* type and stores it along
       *          with implicit key in RespBase object
       * \param   poRespBase : [IN] pointer to Base Registration class
       * \retval  bool : true on successful registration else false
       * \sa      bUnregisterObject
       **************************************************************************/
      bool bRegisterObject(RespBase *poRespBase);

      /***************************************************************************
       ** FUNCTION: bool RespRegister::bUnregisterObject(RespBase *poRespBase)
       ***************************************************************************/
      /*!
       * \fn      bUnregisterObject(RespBase *poRespBase)
       * \brief   UnRegisters an object pointer of RespBase* type from the container
       *          in RespRegister
       * \param   poRespBase : [IN]
       * \retval  bool : true if unregistration successful. Will return false if
       *          a non existing or already unregistered object is passed
       * \sa      bRegisterObject
       **************************************************************************/
      bool bUnregisterObject(RespBase *poRespBase);

      /***************************************************************************
       ** FUNCTION: unsigned int RespRegister::u32GetRegisteredObjectsCount(tenRegID enRegID)
       ***************************************************************************/
      /*!
       * \fn      u32GetRegisteredObjectsCount(tenRegID enRegID)
       * \brief   Returns the number of registered objects for the RegID specified
       *          in enRegID
       * \param   enRegID : [IN] Registration ID of the response class object
       * \retval  unsigned int : number of registered objects for the specified RegID.
       *          if enRegID is invalid then 0 is returned
       * \sa      bRegisterObject
       **************************************************************************/
      unsigned int u32GetRegisteredObjectsCount(tenRegID enRegID);

      /***************************************************************************
       ** FUNCTION: RespBase* poGetRegisteredObject(tenRegID enRegID, unsigned int u32Index)
       ***************************************************************************/
      /*!
       * \fn      RespBase* poGetRegisteredObject(tenRegID enRegID, unsigned int u32Index)
       * \brief   returns the registered response class object pointer of enRegID type and
       *          at index specified by u32Index
       * \param   enRegID : [IN]  Registration ID of the response class object
       * \param   u32Index : [IN] index of the registered response class object
       * \retval  RespBase* :  response class object's pointer at u32Index of type
       *                      enRegID
       *                      if invalid index is accessed then NULL is returned
       * \sa      bRegisterObject
       **************************************************************************/
      RespBase* poGetRegisteredObject(tenRegID enRegID,const unsigned int u32Index);

      /***************************************************************************
       ** FUNCTION: void vClearRespRegister();
       ***************************************************************************/
      /*!
       * \fn      void vClearRespRegister();
       * \brief   clear all elements in RespRegister container of registered objects
       **************************************************************************/
      void vClearRespRegister();

      //! Base Singleton class
       friend class GenericSingleton<RespRegister> ;

   private:

      /***************************************************************************
       *********************************PRIVATE************************************
       ***************************************************************************/

      /***************************************************************************
       ** FUNCTION:  RespRegister::RespRegister();
       ***************************************************************************/
      /*!
       * \fn      RespRegister()
       * \brief   Default Constructor
       * \sa      ~RespRegister()
       **************************************************************************/
      RespRegister();

      /*
       * container with keys mapped to set of objects to be registered
       */
      std::map<short int, std::set<RespBase*> > m_MapRespRegister;
};

#endif /* RESPREGISTER_H_ */
