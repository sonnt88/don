/*!
*******************************************************************************
* \file             spi_tclMsgIntf.cpp
* \brief            The abstract base class containing the generic message interface
to work with mediator.
*******************************************************************************
\verbatim
PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:   The abstract base class containing the generic message interface
to work with mediator.The minimum functionalities required by 
SPI service classes for message transfer are declared here.

COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                            | Modifications
11.11.2013 |  Priju K Padiyath(RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/


/******************************************************************************
| includes:
| 
|----------------------------------------------------------------------------*/

#include <map>
#include "SPITypes.h"
#include "BaseTypes.h"
#include "spi_tclMediatorBase.h"
#include "spi_tclServiceMediator.h"
#include "spi_tclMsgIntf.h"

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/spi_tclMsgIntf.cpp.trc.h"
#endif
#endif

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/***************************************************************************
** FUNCTION:  spi_tclMsgIntf::spi_tclMsgIntf()
***************************************************************************/
spi_tclMsgIntf::spi_tclMsgIntf()
{
   ETG_TRACE_USR1(("spi_tclMsgIntf() entered\n"));
}

/***************************************************************************
** FUNCTION:  spi_tclMsgIntf::spi_tclMsgIntf(tenServiceId enSrvId,spi_tclMediatorBase *poMediator)
***************************************************************************/
spi_tclMsgIntf::spi_tclMsgIntf
   (
   tenServiceId enSrvId,
   spi_tclMediatorBase *poMediator
   ):m_poMediator(poMediator)
{
   ETG_TRACE_USR1(("spi_tclMsgIntf() overloaded entered\n"));

   /*This base class will be inherited by the concrete service classes.
   On creation of the concrete service class the THIS(always point to the currently active object)
   pointer will point to concrete class and it gets registered with the mediator*/
   if(NULL != m_poMediator)
   {
      m_poMediator->vRegister(enSrvId,this);
   }
   else
   {
      ETG_TRACE_USR2(("spi_tclMsgIntf() registration failed\n"));
   }
}

/***************************************************************************
** FUNCTION:  spi_tclMsgIntf::~spi_tclMsgIntf()
***************************************************************************/
spi_tclMsgIntf::~spi_tclMsgIntf()
{
   ETG_TRACE_USR1(("~spi_tclMsgIntf entered\n"));
   m_poMediator = NULL;
}
