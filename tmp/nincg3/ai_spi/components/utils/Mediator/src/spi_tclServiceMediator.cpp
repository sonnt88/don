/*!
*******************************************************************************
* \file              spi_tclServiceMediator.cpp
* \brief             The concrete mediator implimentation.
*******************************************************************************
\verbatim
PROJECT:         Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    This is the mediator used for communication between different services in SPI core.
This design is selected to ease the complex intercommunication between the various SPI core services

COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                            | Modifications
11.11.2013 |  Priju K Padiyath(RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/


/******************************************************************************
| includes:
| 
|----------------------------------------------------------------------------*/
#include <map>
#include "SPITypes.h"
#include "BaseTypes.h"
#include "spi_tclMediatorBase.h"
#include "spi_tclMsgIntf.h"
#include "spi_tclServiceMediator.h"


//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/spi_tclServiceMediator.cpp.trc.h"
#endif
#endif
/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/
/*!
* \brief   iterator to the Map maintaining service handlers objects with service ID as key
*
* This is the iterator to the request handler map.
*/
typedef map<tenServiceId,spi_tclMsgIntf *>::iterator m_itServiceHandlers;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/


/***************************************************************************
** FUNCTION:  spi_tclServiceMediator::spi_tclServiceMediator()
***************************************************************************/
spi_tclServiceMediator::spi_tclServiceMediator()
{
   ETG_TRACE_USR1(("spi_tclServiceMediator() entered\n"));

}

/***************************************************************************
** FUNCTION:  spi_tclServiceMediator::~spi_tclServiceMediator()
***************************************************************************/
spi_tclServiceMediator::~spi_tclServiceMediator()
{
   ETG_TRACE_USR1(("~spi_tclServiceMediator entered\n"));

}

/***************************************************************************
** FUNCTION:  t_Void vRegister(tenServiceId enSrvId,spi_tclMsgIntf *poReceivers)
***************************************************************************/
t_Void spi_tclServiceMediator::vRegister
   (
   tenServiceId enSrvId,
   spi_tclMsgIntf *poReceivers
   )
{
   ETG_TRACE_USR1(("vRegister entered\n"));

   if(
      (e8SPI_SERVICE_INVALID > enSrvId) //All valid service id are less than the invalid Service ID 		
      &&
      (NULL != poReceivers)
      )
   {
      /*insert returns a pair wherein The second element in the pair is set to 
      true if a new element was inserted or false if an equivalent key already existed.*/
      pair<map<tenServiceId,spi_tclMsgIntf *>::iterator,bool> ret;
      ret = m_mapServiceHandlers.insert(pair<tenServiceId,spi_tclMsgIntf *>(enSrvId,poReceivers));
      if(false ==(ret.second)) //Catch duplicate key
      {
         ETG_TRACE_USR4(("vRegister: Key [service Id = %d] already exists\n",enSrvId));
      }
   }
   else
   {
      ETG_TRACE_USR4(("vRegister failed for service ID = %d\n",enSrvId));
   }
}

/***************************************************************************
** FUNCTION:  t_Bool bPostMsgToService(tenServiceId enSrvId,trMsgBase *prMsg)
***************************************************************************/
t_Bool spi_tclServiceMediator::bPostMsgToService
   (
   tenServiceId enSrvId,
   trMsgBase *prMsg
   )
{
   ETG_TRACE_USR1(("bPostMsgToService entered\n"));
   t_Bool bRetVal = false;
   if(NULL != prMsg)
   {
      //Check whether the service Id is registered with mediator
      m_itServiceHandlers it = m_mapServiceHandlers.find(enSrvId);
      if(m_mapServiceHandlers.end() != it)
      {		
         bRetVal = true;
         spi_tclMsgIntf *poReceiver= it->second;
         if(NULL != poReceiver)
         {
            poReceiver->vReceiveMsg(prMsg);
         }
      }
      else
      {
         ETG_TRACE_USR2(("bPostMsgToService: Msg not send as service not found\n"));	
      }
   }
   else
   {
      ETG_TRACE_USR2(("bPostMsgToService: Msg not send as prMsg==null \n"));
   }
   return bRetVal;
}
/***************************************************************************
** FUNCTION:  t_Void vDistributeMsg(trMsgBase *prMsg)
***************************************************************************/
t_Void spi_tclServiceMediator::vDistributeMsg
   (
   trMsgBase *prMsg
   )
{
   ETG_TRACE_USR1(("vDistributeMsg entered\n"));	

   //To store the index by which the service id has to be retrieved from Data structure
   t_U32 u32Increment = 0;
   //Determine the number of services which requires this message
   t_U32 u32ServiceIdCount = 0;
   //Hold the service Id
   t_U32 u32ServiceId = 0;

   if(NULL != prMsg)
   {	
      u32ServiceIdCount = prMsg -> u32GetServiceIdCount();

      for(;u32ServiceIdCount > 0;u32ServiceIdCount--) //Post the message to all registered services
      {
         u32ServiceId = prMsg->u32GetServiceID(u32Increment);

         if(e8SPI_SERVICE_INVALID > u32ServiceId)
         {
            if(true == bPostMsgToService((tenServiceId)u32ServiceId,prMsg)) 
            {
               ETG_TRACE_USR4(("vDistributeMsg Message posted to %d success\n",ETG_CENUM(tenServiceId, u32ServiceId)));
            }

         }
         u32Increment++;
      }
   }
   else
   {
      ETG_TRACE_USR2(("vDistributeMsg failed NULL == prMsg \n"));
   }
}



