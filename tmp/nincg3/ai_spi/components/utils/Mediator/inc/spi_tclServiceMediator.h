/*!
*******************************************************************************
* \file              spi_tclServiceMediator.h
* \brief             The concrete mediator implimentation.
*******************************************************************************
\verbatim
PROJECT:         Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    This is the mediator used for communication between different services in SPI core.

COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                            | Modifications
11.11.2013 |  Priju K Padiyath(RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/

#ifndef SPI_TCLSERVICEMEDIATOR_H_
#define SPI_TCLSERVICEMEDIATOR_H_

/******************************************************************************
| includes:
| 
|----------------------------------------------------------------------------*/


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
using namespace std;
/*!
* \class spi_tclServiceMediator
* \brief This is the concrete class for the SPI service mediator.
*
*/

class spi_tclServiceMediator:public spi_tclMediatorBase
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclServiceMediator::spi_tclServiceMediator();
   ***************************************************************************/
   /*!
   * \fn      spi_tclServiceMediator()
   * \brief   Default Constructor
   * \param
   * \sa      ~spi_tclServiceMediator()
   **************************************************************************/
   spi_tclServiceMediator();


   /***************************************************************************
   ** FUNCTION:  virtual spi_tclServiceMediator::~spi_tclServiceMediator()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclServiceMediator()
   * \brief   Destructor
   * \sa      spi_tclServiceMediator()
   **************************************************************************/
   virtual ~spi_tclServiceMediator();

   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclServiceMediator::vRegister
   **                 (tenServiceId enSrvId,spi_tclMsgIntf *poReceivers) = 0
   ***************************************************************************/
   /*!
   * \fn      vRegister(tenServiceId enSrvId,spi_tclMsgIntf *poReceivers)
   * \brief   Register the colleagues or the services which requires the Mediator
   *          for inter communication
   * \param   [IN]enSrvId : Unique service ID to identify the service
   * \param   [IN]poReceivers : pointers to the service handlers
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vRegister(tenServiceId enSrvId,spi_tclMsgIntf *poReceivers);

   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclServiceMediator::vDistributeMsg(trMsgBase *prMsg)
   ***************************************************************************/
   /*!
   * \fn      vDistributeMsg(trMsgBase *prMsg)
   * \brief   Function to distribute mesages based on service id.
   * \param   [IN]rMsg : pointer to the  message base class
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vDistributeMsg(trMsgBase *prMsg);

   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclServiceMediator::bPostMsgToService(tenServiceId enSrvId,trMsgBase *prMsg)
   ***************************************************************************/
   /*!
   * \fn      bPostMsgToService(tenServiceId enSrvId,trMsgBase *prMsg)
   * \brief   Helper Function to find the service in the register.Get the handle to the service and post the message
   * \param   [IN]enSrvId : The requested service id
   * \param   [IN]prMsg : pointer to the  message base class
   * \retval  NONE
   **************************************************************************/
   t_Bool bPostMsgToService(tenServiceId enSrvId,trMsgBase *prMsg);
protected:
   //Add protected members here
private:
   //Add private members here
};

#endif /* SPI_TCLSERVICEMEDIATOR_H_ */

