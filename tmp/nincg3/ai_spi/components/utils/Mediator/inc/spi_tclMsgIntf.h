/*!
*******************************************************************************
* \file              spi_tclMsgIntf.h
* \brief             The abstract base class containing the generic message interface
to work with mediator.
*******************************************************************************
\verbatim
PROJECT:         Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    The abstract base class containing the generic message interface
to work with mediator.The minimum functionalities required by 
SPI service classes for message transfer are declared here.

COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                            | Modifications
11.11.2013 |  Priju K Padiyath(RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/

#ifndef SPI_TCLMSGINTF_H_
#define SPI_TCLMSGINTF_H_

/******************************************************************************
| includes:
| 
|----------------------------------------------------------------------------*/


/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
using namespace std;
/*!
* \class spi_tclMediatorBase
* \brief This is the base class for all mediator design implementation in SPI.
*
*/

class spi_tclMsgIntf
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/


   /***************************************************************************
   ** FUNCTION:  spi_tclMsgIntf::spi_tclMsgIntf((tenServiceId enSrvId,spi_tclMediatorBase *poMediator);
   ***************************************************************************/
   /*!
   * \fn      spi_tclMsgIntf(tenServiceId enSrvId,spi_tclMediatorBase *poMediator)
   * \brief   overloaded Constructor
   * \param   enSrvId : Id to identify the various SPI core services
   * \param   poMediator : The handle to the concrete Mediator
   * \sa      ~spi_tclMsgIntf()
   **************************************************************************/
   spi_tclMsgIntf(tenServiceId enSrvId,spi_tclMediatorBase *poMediator);

   /***************************************************************************
   ** FUNCTION:  virtual spi_tclMsgIntf::~spi_tclMsgIntf()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclMsgIntf()
   * \brief   Destructor
   * \sa      spi_tclMsgIntf()
   **************************************************************************/
   virtual ~spi_tclMsgIntf();

   /***************************************************************************
   ** FUNCTION:  virtual t_Void vSendMsg(t_Void *vMsg) = 0
   ***************************************************************************/
   /*!
   * \fn      vSendMsg(t_Void *vMsg) = 0
   * \brief   This function has to be overridden in the concrete class for 
   sending a message over the mediator
   * \param   [IN]vMsg : Actual message to be send
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vSendMsg(t_Void *vMsg) = 0;

   /***************************************************************************
   ** FUNCTION:  virtual t_Void vReceiveMsg(t_Void *vMsg) = 0
   ***************************************************************************/
   /*!
   * \fn      vReceiveMsg(t_Void *vMsg)
   * \brief   This function has to be overridden in the concrete class for 
   receiving a message over the mediator
   * \param   [OUT]vMsg : pointer to the  message base class
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vReceiveMsg(t_Void *vMsg) = 0;

protected:
   /*!
   * \brief   element to store the current mediator instance
   *
   * This can be populated at the beginning and can be used until a different 
   * mediator is used
   */
   spi_tclMediatorBase *m_poMediator;

private:
   /***************************************************************************
   ** FUNCTION:  spi_tclMsgIntf::spi_tclMsgIntf();
   ***************************************************************************/
   /*!
   * \fn      spi_tclServiceMediator()
   * \brief   Default Constructor should not be called
   * \param
   * \sa      ~spi_tclServiceMediator()
   **************************************************************************/
   spi_tclMsgIntf();

};


#endif /* SPI_TCLMSGINTF_H_ */

