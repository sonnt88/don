/*!
*******************************************************************************
* \file              spi_tclMediatorBase.h
* \brief             Base class for the Mediator 
*******************************************************************************
\verbatim
PROJECT:         Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Base class for all the Mediator implementation in SPI.
This can be used as the base for creating other mediators.

COPYRIGHT:      &copy; RBEI

HISTORY:
Date       |  Author                            | Modifications
06.11.2013 |  Priju K Padiyath(RBEI/ECP2)       | Initial Version

\endverbatim
******************************************************************************/

#ifndef SPI_TCLMEDIATORBASE_H_
#define SPI_TCLMEDIATORBASE_H_

/******************************************************************************
| includes:
| 
|----------------------------------------------------------------------------*/

/******************************************************************************
| defines and macros and constants(scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/
using namespace std;

class spi_tclMsgIntf; 
/*!
* \class spi_tclMediatorBase
* \brief This is the base class for all mediator design implementation in SPI.
*
*/

class spi_tclMediatorBase
{
public:
   /***************************************************************************
   *********************************PUBLIC*************************************
   ***************************************************************************/

   /***************************************************************************
   ** FUNCTION:  spi_tclMediatorBase::spi_tclMediatorBase();
   ***************************************************************************/
   /*!
   * \fn      spi_tclMediatorBase()
   * \brief   Default Constructor
   * \param
   * \sa      ~spi_tclMediatorBase()
   **************************************************************************/
   spi_tclMediatorBase()
   {}


   /***************************************************************************
   ** FUNCTION:  virtual spi_tclMediatorBase::~spi_tclMediatorBase()
   ***************************************************************************/
   /*!
   * \fn      virtual ~spi_tclMediatorBase()
   * \brief   Destructor
   * \sa      spi_tclMediatorBase()
   **************************************************************************/
   virtual ~spi_tclMediatorBase()
   {}

   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclMediatorBase::vRegister
   **                 (tenServiceId enSrvId,spi_tclMsgIntf *poReceivers) = 0
   ***************************************************************************/
   /*!
   * \fn      vRegister(tenServiceId enSrvId,spi_tclMsgIntf *poReceivers)
   * \brief   Base class interface to Register the colleagues or the services 		
   *          which requires the Mediator for inter communication
   * \param   [IN]enSrvId : Unique service ID to identify the service
   * \param   [IN]poReceivers : pointers to the service handlers
   * \retval  t_Void
   **************************************************************************/
   virtual t_Void vRegister(tenServiceId enSrvId,spi_tclMsgIntf *poReceivers) = 0;

   /***************************************************************************
   ** FUNCTION:  virtual t_Void spi_tclServiceMediator::vDistributeMsg(trMsgBase *prMsg) = 0
   ***************************************************************************/
   /*!
   * \fn      vDistributeMsg(trMsgBase *prMsg)
   * \brief   Base class interface to distribute messages based on service id.
   * \param   [IN]rMsg : pointer to the  message base class
   * \retval  NONE
   **************************************************************************/
   virtual t_Void vDistributeMsg(trMsgBase *prMsg) = 0;
protected:
   /*!
   * \brief   Map to maintain service handlers objects with service ID as key
   *
   * This is used to maintain the Service handler objects .
   * The handlers will be retrieved using the service ID.
   */
   map<tenServiceId,spi_tclMsgIntf *> m_mapServiceHandlers;  
};


#endif /* SPI_TCLMEDIATORBASE_H_ */

