/***********************************************************************/
/*!
 * \file  FIVisitorMsgExtractor.cpp
 * \brief Generic message Extractor
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Message Extractor
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      26.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
// Includes to utilize the OSAL Interface
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

// Includes to utilize the AMT Interfaces
#define AMT_S_IMPORT_INTERFACE_GENERIC
#include <amt_if.h>

// Includes for Fi Visitor Messages
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE  
#include <common_fi_if.h>

#include "FIVisitorMsgExtractor.h"


/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/
#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/FIVisitorMsgExtractor.cpp.trc.h"
#endif

/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

namespace shl {
   namespace msgHandler {


      /******************************************************************************
      ** FUNCTION:  FIVisitorMsgExtractor::FIVisitorMsgExtractor()
      ******************************************************************************/

      FIVisitorMsgExtractor::FIVisitorMsgExtractor():m_bValid(FALSE)
      {} // ihl_tclFIVisitorMsgExtractor::ihl_tclFIVisitorMsgExtractor()

      /******************************************************************************
      ** FUNCTION:  FIVisitorMsgExtractor::~FIVisitorMsgExtractor()
      ******************************************************************************/

      /*virtual*/
      FIVisitorMsgExtractor::~FIVisitorMsgExtractor()
      {} // ihl_tclFIVisitorMsgExtractor::~ihl_tclFIVisitorMsgExtractor()

      /******************************************************************************
      ** FUNCTION:  void FIVisitorMsgExtractor::vExtractVisitorMsg(amt_tclServiceData..)
      ******************************************************************************/

      void FIVisitorMsgExtractor::vExtractVisitorMsg
         (
         amt_tclServiceData &rfoServData
         , fi_tclTypeBase &rfoTypeBase
         , tU16 u16FiMajVer
         )
      {
         // Create Visitor message object for incoming message
         fi_tclVisitorMessage oInVisitorMsg(&rfoServData);

         ETG_TRACE_USR1(("Extracting msg - FID:%u, RegID:%u, ServiceId:%u, SrcAppId:%u, TargAppId:%u, FI Ver:%u"
            , ETG_ENUM(ihl_FID, rfoServData.u16GetFunctionID())
            , rfoServData.u16GetRegisterID(), rfoServData.u16GetServiceID()
            , rfoServData.u16GetSourceAppID()
            , rfoServData.u16GetTargetAppID(), u16FiMajVer));

         // Extract the data in FI class from Vistor class
         if (OSAL_ERROR == oInVisitorMsg.s32GetData(rfoTypeBase, u16FiMajVer))
         {
            // Error message.
            ETG_TRACE_ERR(("Failed to extract data from incoming message."));
         }  // if (OSAL_ERROR == oInVisitorMsg.s32GetData(rfoTypeBase, u16FiMajVer..
         else
         {
            m_bValid =  true;
         }

      } 

      /******************************************************************************
      ** FUNCTION:  bool FIVisitorMsgExtractor::bIsValid() const
      ******************************************************************************/
      bool FIVisitorMsgExtractor::bIsValid() const
      {
         return m_bValid;
      }  // tBool ihl_tclFIVisitorMsgExtractor::bIsValid() const

   }  // namespace fi_helpers
}  // namespace ihl

