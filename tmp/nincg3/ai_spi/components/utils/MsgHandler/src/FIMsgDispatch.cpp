/***********************************************************************/
/*!
 * \file  FIMsgDispatch.cpp
 * \brief Generic message Sender
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Message sender
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      26.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
 | includes:
 | 1)system- and project- includes
 | 2)needed interfaces from external components
 | 3)internal and external interfaces from this component
 |----------------------------------------------------------------------------*/
#include <midw_fi_if.h>


#include "FIMsgDispatch.h"
/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/
#include "Trace.h"
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/FIMsgDispatch.cpp.trc.h"
#endif

namespace shl
{
   namespace msgHandler
   {
      /******************************************************************************
      ** FUNCTION:  FIMsgDispatch::FIMsgDispatch()
      ******************************************************************************/
      FIMsgDispatch::FIMsgDispatch(ahl_tclBaseOneThreadApp *poBaseeApp):poApp(poBaseeApp)
      {}

      /******************************************************************************
      ** FUNCTION:  virtual FIMsgDispatch::~FIMsgDispatch()
      ******************************************************************************/
      FIMsgDispatch::~FIMsgDispatch()
      {poApp = NULL;}

      /******************************************************************************
      ** FUNCTION:  bool FIMsgDispatch::bSendMessage(const fi_tclMessageBase&  ...)
      ******************************************************************************/
      bool FIMsgDispatch::bSendMessage(const fi_tclMessageBase& rfcoMsgBase,const trMsgContext& 
            rfcoMsgContext, int s16MajorVersion)
      {
         bool bRetVal = true;

         // Create the Visitor Message
         fi_tclVisitorMessage oMsg(rfcoMsgBase.corfoGetTypeBase(), s16MajorVersion);

         // Init the message
         oMsg.vInitServiceData
            (
            rfcoMsgContext.rUserContext.u32SrcAppID,      // Source app-ID
            rfcoMsgContext.rUserContext.u32DestAppID,      // Dest. app-ID
            AMT_C_U8_CCAMSG_STREAMTYPE_NODATA,            // Stream type
            0,                                            // Stream counter
            rfcoMsgContext.rUserContext.u32RegID,         // Registry ID
            rfcoMsgContext.rUserContext.u32CmdCtr,        // Command counter
            rfcoMsgBase.u16GetServiceID(),                // Service-ID    
            rfcoMsgBase.u16GetFunctionID(),               // Function-ID
            rfcoMsgBase.u8GetOpCode()                     // OpCode
            );

         ETG_TRACE_USR4(("Sending Message: Opcode: %d, Version: %d, ServiceId: %d, \
                         RegId: %d, FID: %x, SrcAppId: %d, TargAppId: %d, CmdCntr: %d"
                         , ETG_ENUM(OP_CODES, oMsg.u8GetOpCode())
                         , oMsg.u16GetVersion(), oMsg.u16GetServiceID()
                         , oMsg.u16GetRegisterID()
                         , oMsg.u16GetFunctionID()
                         , ETG_ENUM(ail_u16AppId, oMsg.u16GetSourceAppID())
                         , ETG_ENUM(ail_u16AppId, oMsg.u16GetTargetAppID())
                         , oMsg.u16GetCmdCounter()
                         ));
         
         // Post the message
         ail_tenCommunicationError enResult = poApp->enPostMessage(&oMsg, TRUE);

         if(AIL_EN_N_NO_ERROR != enResult)
         {
            bRetVal = false;
            ETG_TRACE_ERR(( "enPostMessage() failed, 'ail_tenCommunicationError' = %u",
               ETG_ENUM(AIL_ERROR, enResult)));
         }

         return bRetVal;
      } //  bool FIMsgSender::bSendMessage
      
      /******************************************************************************
      ** FUNCTION:  bool FIMsgDispatch::bSendResMessage(const fi_tclMessageBase&  ...)
      ******************************************************************************/
      bool FIMsgDispatch::bSendResMessage(const fi_tclMessageBase& rfcoMsgBase,const trMsgContext& 
            rfcoMsgContext, int s16MajorVersion)
      {
         bool bRetVal = true;

         // Create the Visitor Message
         fi_tclVisitorMessage oMsg(rfcoMsgBase.corfoGetTypeBase(), s16MajorVersion);

          // Init the message
         oMsg.vInitServiceData
            (
            rfcoMsgContext.rUserContext.u32DestAppID,      // Source app-ID
            rfcoMsgContext.rUserContext.u32SrcAppID,      // Dest. app-ID
            AMT_C_U8_CCAMSG_STREAMTYPE_NODATA,            // Stream type
            0,                                            // Stream counter
            rfcoMsgContext.rUserContext.u32RegID,         // Registry ID
            rfcoMsgContext.rUserContext.u32CmdCtr,        // Command counter
            rfcoMsgBase.u16GetServiceID(),                // Service-ID    
            rfcoMsgBase.u16GetFunctionID(),               // Function-ID
            rfcoMsgBase.u8GetOpCode()                     // OpCode
            );

            ETG_TRACE_USR4(("Message sent. Opcode: %d, Version: %d, ServiceId: %d, \
                            RegId: %d, FID: %x, SrcAppId: %d, TargAppId: %d, CmdCntr: %d"
                            , ETG_ENUM(OP_CODES, oMsg.u8GetOpCode())
                            , oMsg.u16GetVersion(), oMsg.u16GetServiceID()
                            , oMsg.u16GetRegisterID()
                            , oMsg.u16GetFunctionID()
                            , ETG_ENUM(ail_u16AppId, oMsg.u16GetSourceAppID())
                            , ETG_ENUM(ail_u16AppId, oMsg.u16GetTargetAppID())
                            , oMsg.u16GetCmdCounter()
                            ));

         // Post the message
         ail_tenCommunicationError enResult =
            poApp->enPostMessage(&oMsg, TRUE);

         if(AIL_EN_N_NO_ERROR != enResult)
         {
            bRetVal = false;
            ETG_TRACE_ERR(( "enPostMessage() failed, 'ail_tenCommunicationError' = %u",
               ETG_ENUM(AIL_ERROR, enResult)));
         }
         return bRetVal;
      } // bool FIMsgSender::bSendResMessage

   } // fi_helper
} // shl
