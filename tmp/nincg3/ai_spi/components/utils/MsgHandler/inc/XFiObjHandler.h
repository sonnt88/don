#ifndef XFIOBJHANDLER_H_
#define XFIOBJHANDLER_H_

/***********************************************************************/
/*!
* \file  FiObjHandler.h
* \brief Fi Object handler
*************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Message Extractor
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
   Date        | Author                | Modification
   26.10.2013  | Shihabudheen P M      | Initial Version
   Note : Initial version is from mediaplayer

\endverbatim
*************************************************************************/

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/

#include "FiObjHandler.h"
#include "FIVisitorMsgExtractor.h"

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------------------*/

namespace shl {
   namespace msgHandler {


      /****************************************************************************/
      /*!
      * \class FiObjHandler
      * \brief Template library for Fi Object Handling using Execute around seqs.
      *
      * C++ Pattern - Executing Around Sequences.
      * Pre- and Post-Sequence Actions - 
      * - Paired actions where a function is called before some statement sequence 
      * and a corresponding function afterwards are commonly associated with resource
      * acquisition and release
      * - Pre- and post-sequence actions are a common feature of block-scoped 
      * resource management� e.g. allocate memory, use it, deallocate it� and this 
      * programming cliche can be found time and time again across many programs.
      *
      * References:
      * - The Design and Evolution of C++, by Bjarne Stroustrup, Addison-Wesley
      * - Wrapping C++ Member Function Calls, by Bjarne Stroustrup, AT&T Labs - 
      * Research, Florham Park, New Jersey, USA
      ***************************************************************************/
      template <class GenericClass>
      class XFiObjHandler : public FiObjHandler<GenericClass>
         , public FIVisitorMsgExtractor
      {
      public:

         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

         /***************************************************************************
         ** FUNCTION:  XFiObjHandler::XFiObjHandler(amt_tc..)
         ***************************************************************************/
         /*!
         * \brief   Constructor
         * \param   [rfoServData]: (I) Reference to service data
         * \param   [u16FiMajVer]: (I) FI Major Version
         * \retval  NONE
         **************************************************************************/
         XFiObjHandler(amt_tclServiceData &rfoServData
            , tU16 u16FiMajVer FI_DEFAULT_VERSION)
            : FiObjHandler<GenericClass>(), FIVisitorMsgExtractor()
         {
            // Extract the Fi visitor message.
            vExtractVisitorMsg(rfoServData, *this, u16FiMajVer);
         }  // ihl_tclXFiObjHandler(amt_tclServiceData &rfoServData)

         /***************************************************************************
         ** FUNCTION:  XFiObjHandler::~XFiObjHandler()
         ***************************************************************************/
         /*!
         * \brief   Destructor
         * \param   NONE
         * \retval  NONE
         **************************************************************************/
         virtual ~XFiObjHandler(){};

         /***************************************************************************
         ****************************END OF PUBLIC***********************************
         ***************************************************************************/

      protected:

         /***************************************************************************
         *******************************PROTECTED************************************
         ***************************************************************************/

         /***************************************************************************
         ** FUNCTION:  XFiObjHandler::XFiObjHandler()
         ***************************************************************************/
         /*!
         * \brief   Default Constructor
         * \param   NONE
         * \retval  NONE
         * \note    Default constructor is declared protected to disable default 
         *          construction.
         **************************************************************************/
         XFiObjHandler();  // No definition exists.

         /***************************************************************************
         ****************************END OF PROTECTED********************************
         ***************************************************************************/

      private:

         /***************************************************************************
         *********************************PRIVATE************************************
         ***************************************************************************/

         /***************************************************************************
         ****************************END OF PRIVATE**********************************
         ***************************************************************************/
      }; // class XFiObjHandler : public FiObjHandler<ipoda..

   }  // namespace msgHandler
}  // namespace shl

#endif   // #ifndef XFIOBJHANDLER_H_
