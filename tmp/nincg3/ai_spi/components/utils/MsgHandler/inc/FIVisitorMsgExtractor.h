#ifndef FIVISITORMSGEXTRACTOR_H_
#define FIVISITORMSGEXTRACTOR_H_
/***********************************************************************/
/*!
 * \file  FIVisitorMsgExtractor.h
 * \brief Generic message Extractor
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Message Extractor
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      26.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/
// forward declarations
class amt_tclServiceData;
class fi_tclTypeBase;

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------------------*/

namespace shl {
   namespace msgHandler {

      /****************************************************************************/
      /*!
      * \class FIVisitorMsgExtractor
      * \brief Message Extractor
      *
      * Interface Class - Performs Message extraction This is an interface 
      * class simialr to a Java Interface. Interface to 
      * extract incoming messages to appropriate FI message types.
      * This interface has to be derived for use in the derived class. 
      ***************************************************************************/

      class FIVisitorMsgExtractor
      {
      public:

         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

         /*************************************************************************
         ** FUNCTION:  FIVisitorMsgExtractor::FIVisitorMsgExtractor()
         *************************************************************************/
         /*!
         * \fn    FIVisitorMsgExtractor()
         * \brief Constructor
         * \sa    virtual ~FIVisitorMsgExtractor()
         *************************************************************************/
         FIVisitorMsgExtractor();

         /*************************************************************************
         ** FUNCTION:  virtual FIVisitorMsgExtractor::~FIVisitorMsgExtractor()
         *************************************************************************/
         /*!
         * \fn    ~FIVisitorMsgExtractor()
         * \brief Constructor
         * \sa    FIVisitorMsgExtractor()
         *************************************************************************/
         virtual ~FIVisitorMsgExtractor();

         /***************************************************************************
         ***************************EXTERNAL INTERFACES here*************************
         ***************************************************************************/

         /*************************************************************************
         ** FUNCTION:  void FIVisitorMsgExtractor::vExtractVisitorMsg(...)
         *************************************************************************/
         /*!
         * \fn     vExtractVisitorMsg()
         * \brief  Interface to extract the visitor message
         * \param  rfoServiceData : [IN] Reference tp service data.
         * \param  rfoTypeBase : [OUT] Reference to Fi message type.
         * \param  u16FiMajVer : [IN] Fi Major version
         *************************************************************************/
         void vExtractVisitorMsg
            (
            amt_tclServiceData &rfoServiceData
            , fi_tclTypeBase &rfoTypeBase
            , tU16 u16FiMajVer FI_DEFAULT_VERSION
            );

         /*************************************************************************
         ** FUNCTION:  bool FIVisitorMsgExtractor::bIsValid() const
         *************************************************************************/
         /*!
         * \fn    bIsValid()
         * \brief Interface to check validity of the message.\
         * \retval bool : TRUE is message is valid, FALSE otherwise.
         * \sa   
         *************************************************************************/
         bool bIsValid() const;

         /***************************************************************************
         ****************************END OF PUBLIC***********************************
         ***************************************************************************/

      protected:

         /***************************************************************************
         *******************************PROTECTED************************************
         ***************************************************************************/

         /***************************************************************************
         ****************************END OF PROTECTED********************************
         ***************************************************************************/

      private:

         /***************************************************************************
         *********************************PRIVATE************************************
         ***************************************************************************/

         /// Message validity flag.
         bool m_bValid;
         /***************************************************************************
         ****************************END OF PRIVATE**********************************
         ***************************************************************************/
      }; // class FIVisitorMsgExtractor

   }  // namespace fi_helpers
}  // namespace sll

#endif   // #ifndef FIVISITORMSGEXTRACTOR_H_
