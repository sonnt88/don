#ifndef FIMSGDISPATCH_H_
#define FIMSGDISPATCH_H_
/***********************************************************************/
/*!
 * \file  FIMsgDispatch.h
 * \brief Generic message Sender
 *************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Message sender
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
      Date        | Author                | Modification
      26.10.2013  | Shihabudheen P M      | Initial Version

\endverbatim
 *************************************************************************/

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/
#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE  
#include <common_fi_if.h>

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include <generic_msgs_if.h>

#define AHL_S_IMPORT_INTERFACE_GENERIC
#define AHL_S_IMPORT_INTERFACE_CCA_EXTENSION
#include <ahl_if.h>

#include "SPITypes.h"
/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/
// forward declarations
class ahl_tclBaseOneThreadApp;
class fi_tclMessageBase;

namespace shl
{
   namespace msgHandler
   {
      /****************************************************************************/
      /*!
      * \class FIMsgDispatch
      * \brief Message Sender
      *
      * Interface Class - Performs Message creation and send the message.
      * This interface has to be derived for use in the derived class.
      ***************************************************************************/
      class FIMsgDispatch: public ahl_tclBaseOneThreadApp
      {
      public:
         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

         /*************************************************************************
         ** FUNCTION:  FIMsgDispatch::FIMsgDispatch()
         *************************************************************************/
         /*!
         * \fn     FIMsgSender()
         * \brief  Constructor
         * \sa     virtual ~FIMsgSender()
         *************************************************************************/
         FIMsgDispatch(ahl_tclBaseOneThreadApp *poBaseeApp);

         /*************************************************************************
         ** FUNCTION:  FIMsgDispatch::~FIMsgDispatch()
         *************************************************************************/
         /*!
         * \fn     ~FIMsgSender()
         * \brief  Constructor
         * \sa     FIMsgSender()
         *************************************************************************/
         virtual ~FIMsgDispatch();

         /*************************************************************************
         ** FUNCTION:  bool FIMsgDispatch::bSendMessage(const fi_tclMessageBase& ...)
         *************************************************************************/
         /*!
         * \fn     bool bSendMessage(const fi_tclMessageBase& rfcoMsgBase,const )
         * \brief  Functions to send the messages.
         * \param  rfcoMsgBase :[IN]  base message
         * \param  rfcoMsgContext : [IN] message context
         * \param  s16MajorVersion : [IN] Major version
         * \retval bool : True if success, false otherwise.
         * \sa    
         *************************************************************************/
         bool bSendMessage(const fi_tclMessageBase& rfcoMsgBase,const trMsgContext& 
            rfcoMsgContext,  int s16MajorVersion);

         /*************************************************************************
         ** FUNCTION:  bool FIMsgDispatch::bSendResMessage(const fi_tclMessageBase& ...)
         *************************************************************************/
         /*!
         * \fn     bool bSendResMessage(const fi_tclMessageBase& rfcoMsgBase,const )
         * \brief  Functions to send the messages.
         * \param  rfcoMsgBase :[IN]  Base message
         * \param  rfcoMsgContext : [IN] Message context
         * \param  s16MajorVersion : [IN] Major version
         * \retval bool : True if success, false otherwise.
         * \sa    
         *************************************************************************/
         bool bSendResMessage(const fi_tclMessageBase& rfcoMsgBase,const trMsgContext& 
            rfcoMsgContext, int s16MajorVersion);
         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

      protected:
         /***************************************************************************
         *******************************PROTECTED************************************
         ***************************************************************************/

         /***************************************************************************
         ****************************END OF PROTECTED********************************
         ***************************************************************************/
      private:
         /***************************************************************************
         *********************************PRIVATE************************************
         ***************************************************************************/
         // pointer to the application
         ahl_tclBaseOneThreadApp * poApp;
         /***************************************************************************
         ****************************END OF PRIVATE**********************************
         ***************************************************************************/
      };
   } // namespace fi_helpers
}// namespace shl

#endif //FIMSGDISPATCH_H_
