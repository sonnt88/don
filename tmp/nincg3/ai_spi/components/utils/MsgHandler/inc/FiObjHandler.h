#ifndef FIOBJHANDLER_H_
#define FIOBJHANDLER_H_
/***********************************************************************/
/*!
* \file  FiObjHandler.h
* \brief Fi Object handler
*************************************************************************
\verbatim

   PROJECT:        Gen3
   SW-COMPONENT:   Smart Phone Integration
   DESCRIPTION:    Object handler for message extractor
   AUTHOR:         Shihabudheen P M
   COPYRIGHT:      &copy; RBEI

   HISTORY:
   Date        | Author                | Modification
   26.10.2013  | Shihabudheen P M      | Initial Version
   Note : Initial version is from media player
\endverbatim
*************************************************************************/

/******************************************************************************
| includes of component-internal interfaces, if necessary
| (scope: component-local)
|----------------------------------------------------------------------------*/

#define OSAL_S_IMPORT_INTERFACE_GENERIC
#include <osal_if.h>

#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#include <common_fi_if.h>

/******************************************************************************
| defines and macros (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| typedefs (scope: global)
|----------------------------------------------------------------------------*/

namespace shl {
   namespace msgHandler {

      /****************************************************************************/
      /*!
      * \class FiObjHandler
      * \brief Template library for Fi Object Handling using Execute around seqs.
      *
      * C++ Pattern - Executing Around Sequences.
      * Pre- and Post-Sequence Actions - 
      * - Paired actions where a function is called before some statement sequence 
      * and a corresponding function afterwards are commonly associated with resource
      * acquisition and release
      * - Pre- and post-sequence actions are a common feature of block-scoped 
      * resource management� e.g. allocate memory, use it, deallocate it� and this 
      * programming cliche can be found time and time again across many programs.
      *
      * References:
      * - The Design and Evolution of C++, by Bjarne Stroustrup, Addison-Wesley
      * - Wrapping C++ Member Function Calls, by Bjarne Stroustrup, AT&T Labs - 
      * Research, Florham Park, New Jersey, USA
      ***************************************************************************/
      template <class GenericClass = fi_tclTypeBase>
      class FiObjHandler : public GenericClass
      {
      public:

         /***************************************************************************
         *********************************PUBLIC*************************************
         ***************************************************************************/

         /***************************************************************************
         ** FUNCTION:  FiObjHandler::FiObjHandler()
         ***************************************************************************/
         /*!
         * \brief   Constructor
         * \param   NONE
         * \retval  NONE
         * 
         **************************************************************************/
         FiObjHandler():GenericClass(){};

         /***************************************************************************
         ** FUNCTION:  FiObjHandler::~FiObjHandler()
         ***************************************************************************/
         /*!
         * \brief   Destructor
         * \param   NONE
         * \retval  NONE
         * 
         **************************************************************************/
         virtual ~FiObjHandler(){this->vDestroy();};

         /***************************************************************************
         ****************************END OF PUBLIC***********************************
         ***************************************************************************/

      protected:

         /***************************************************************************
         *******************************PROTECTED************************************
         ***************************************************************************/

         /***************************************************************************
         ****************************END OF PROTECTED********************************
         ***************************************************************************/

      private:

         /***************************************************************************
         *********************************PRIVATE************************************
         ***************************************************************************/

         /***************************************************************************
         ****************************END OF PRIVATE**********************************
         ***************************************************************************/

      }; // class FiObjHandler

   }  // namespace msgHandler
}  // namespace shl

#endif   // #ifndef FIOBJHANDLER_H_
