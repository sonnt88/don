#!/usr/bin/perl
####################################################################################################
#* FILE:         generate_spi_fi.pl                                                              *#
#* DESCRIPTION:  make_fi_complete.pl wrapper ...                                                  *#
####################################################################################################

use File::Basename;
use File::Find;
use File::Path;
use FindBin qw($Bin);
use strict;

   my $_SWNAVIROOT  = $ENV{"_SWNAVIROOT"}."/";
   my $_SWTOOLROOT  = ($ENV{"_SWTOOLROOT"})? $ENV{"_SWTOOLROOT"}."/" : $_SWNAVIROOT."/";
   my $_SWBUILDROOT = $ENV{"_SWBUILDROOT"}."/";
   my $vXMLVersion  = ((-d $_SWNAVIROOT."config/") || (-d $_SWNAVIROOT."linked/"))? "2.0" : "1.0";
   my @vFileList    = ();  # used for MD5 filelist
   my @vDirList     = ();  # used by if_header_correction()


   # to be adapted in project --------------------------------------------------------------------------------------------------

   my $vFIGroup             = "spi_";
   my $vFIGenAppendix       = "";  # all_fis use "_gen"

   my $vFIGroupPath         = $_SWNAVIROOT."../ai_spi/components/projects/G3G/".$vFIGroup."fi/"; 
   $vFIGroupPath = $Bin."/";

	 my $vFITemplatePath = $vFIGroupPath;
	 if ( -d $_SWNAVIROOT."../di_fi/components/fi" ) {
		 $vFITemplatePath = $_SWNAVIROOT."../di_fi/components/";			# use original files and not linked location
	 }
   my $vFIGenOutPath        = $_SWBUILDROOT."generated/components/".$vFIGroup."fi".$vFIGenAppendix."/";
   $vFIGenOutPath           =~ s(\\)(/)g;
   $vFIGenOutPath           =~ s(//+)(/)g;
   my $vFIListFile          = $vFIGroupPath."used_fis.txt";  # this file holds the FI names of group to be generated
   my $vFICodeGenBinPath    = $_SWTOOLROOT."tools/codegen/bin/";
   if ( -d $_SWNAVIROOT."../di_fi/tools/codegen" ) {
      $vFICodeGenBinPath    = $_SWNAVIROOT."../di_fi/tools/codegen/bin/";
   }
   my $vFICodeGenExecutable = ($^O =~ /mswin/i)? "ficodegen2_exe.exe" : "ficodegen2_out.out";
   my $vFICodeGenOptions    = "";
   my $vFIArchivePath       = "";  # do not use a pre-generated archive instead
   my $vFIArchiveFile       = "";

   my $vForceGeneration     = 0;  # set to 1 if you want to force generation on rebuild,recreate (delete inc and source directories first)
   my $vGeneratePdf         = 0;  # set to 1 if you want to to generate PDF too

   # an example of precreate rules are:
   #   <specialparameter name="PRECREATERULE" prio="10" xmlversionfilter="1.0" value="perl %_SWNAVIROOT%/components/midw_fi/generate_midw_fi.pl"/>
   #   <specialparameter name="PRECREATERULE" prio="10" xmlversionfilter="2.0" value="perl %_SWNAVIROOT%/linked/di_middleware_server/midw_fi/generate_midw_fi.pl"/>

   # use ficodegen *.txt template files and *.pl scripts
   # from either the fi/template folder
   #   or the specialized fi/template/most folder
   print "INFO     perl $vFIGroupPath"."copy_templates.pl $vFITemplatePath $vFIGroupPath\n";
   system('perl ' . $vFIGroupPath . 'copy_templates.pl ' . $vFITemplatePath . " " . $vFIGroupPath);

   # ---------------------------------------------------------------------------------------------------------------------------


   # argument buildmode (debug|rebuild|create|recreate)
   my $vBuildMode = $ARGV[$#ARGV-1];

   # argument logfile
   my $vLogFile   = $ARGV[$#ARGV];
   mkpath(dirname($vLogFile));
   open(LOGFILE, ">".$vLogFile) or die "ERROR    could not open ".$vLogFile." for writing\n";

   # further arguments to pass on make_fi_complete.pl
   my $vNextArgs  = "";
   for (my $vIndex=0; $vIndex < ($#ARGV-1); $vIndex++) {
      $vNextArgs .= " ".$ARGV[$vIndex];
   }

   print LOGFILE "script $0 called : parameter \"".$vNextArgs." ".$vBuildMode." ".$vLogFile."\"\n";
   print LOGFILE "vBuildMode: '$vBuildMode'\n";
   print LOGFILE "vNextArgs:  '$vNextArgs'\n\n";

   my $vState = (-f $vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".xml")? "" : "doesn't ";
   print LOGFILE "INFO     '".$vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".xml' ".$vState." exist\n";
   $vState    = (-f $vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".md5")? "" : "doesn't ";
   print LOGFILE "INFO     '".$vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".md5' ".$vState." exist\n";


   if ($vBuildMode =~ /^(build|rebuild|create|recreate)$/i) {
      if ((-f $vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".xml") &&
          (-f $vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".md5") &&
          (check_md5_file($vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".md5") eq 0)) {
         # files are unchanged, so do nothing
         print STDERR "INFO     *** ".$vFIGroup."fi".$vFIGenAppendix." still valid ***\n";
      } else {
         print STDERR "INFO     *** generate ".$vFIGroup."fi".$vFIGenAppendix." ***\n";
         generate_fi();
         generate_md5_file($vFIGroupPath, $vFIGenOutPath);
      }
   }
   else {
      die "ERROR    unknown build mode '$vBuildMode' found!";
   }
   print LOGFILE "OK       leave script\n";
   close(LOGFILE);
   exit 0;



##############################################################
sub check_md5_file
# return values:
#      0 - files are equal
#  other - files differ
##############################################################
{
   my ($vMD5File) = (@_);

   # parsing md5file
   my $vCmd = "md5sum --check --status $vMD5File";
   my $vRet = system($vCmd);
   print LOGFILE "INFO     '$vCmd'  returned $vRet\n\n";
   return $vRet;
}

##############################################################
sub generate_md5_file
##############################################################
{
   my ($vFiGroupPath, $vFiGenPath) = (@_);
   my $vMD5Content = "";
   my $vFiGroup    = $vFiGroupPath;
      $vFiGroup    =~ s/.*\/([^\/]+)\/$/$1/;
   my $vXMLFile    = $vFiGenPath.$vFiGroup.$vFIGenAppendix.".xml";
   my $vMD5File    = $vFiGenPath.$vFiGroup.$vFIGenAppendix.".md5";

   # glob directory for filelist
   @vFileList = ();
   find(\&filelist_xml, $vFiGroupPath);
   my $vVal1 = $#vFileList + 1;
   print LOGFILE "INFO     found $vVal1 xml files in '$vFiGenPath'\n";

   # parsing xml for filelist
   open(FILE, "<$vXMLFile") or die "ERROR    could not read '$vXMLFile'\n";
   while(<FILE>) {
      if (/<file\s+name=\"(.*?)\"/) {
         my $vFile = $1;
         next if ($vFile !~ /\.(c|cpp|h)$/i);
         push(@vFileList, $vFiGenPath."$vFile");
      }
   }
   close(FILE);
   my $vVal2 = $#vFileList - $vVal1 + 1;
   print LOGFILE "INFO     read $vVal2 files from '$vXMLFile'\n";

   if (($vVal1 < 2) || ($vVal2 < 2)) {
      print STDERR "ERROR    *** generation failed, refer '$vLogFile' for details\n";
      print LOGFILE "ERROR    md5 generation failed, because filelist is empty\n";
      exit 1;
   }

   # generate MD5 content
   foreach my $vFile (@vFileList) {
      $vMD5Content .= qx/md5sum -t $vFile/;
   }
   print LOGFILE "INFO     generate MD5 list to '$vMD5File':\n";
   print LOGFILE $vMD5Content."\n";

   open(MD5FILE, ">$vMD5File") or die "ERROR    could not open '$vMD5File' for writing.\n";
   binmode MD5FILE;
   print MD5FILE $vMD5Content;
   close(MD5FILE);
}

##############################################################
sub generate_fi
##############################################################
{
   my ($vTempForceGeneration) = (@_);

   #~ if (-f $vFIArchivePath.$vFIArchiveFile) {
      #~ extract_fi_archive();
   #~ } else {
      # do generate FI sources
      $ENV{FIGenOutPath}        = "\"$vFIGenOutPath\"";
      $ENV{FICodeGenBinPath}    = "\"$vFICodeGenBinPath\"";
      $ENV{FICodeGenExecutable} = "\"$vFICodeGenExecutable\"";
      $ENV{FICodeGenOptions}    = "$vFICodeGenOptions";
      $ENV{FIGenAppendix}       = "$vFIGenAppendix";  # no " allowed here

      my $vMakeFiCmd = "perl ${vFITemplatePath}fi/template/make_fi_complete.pl".$vNextArgs;
      $vMakeFiCmd   .= " -l ".$vLogFile;
      $vMakeFiCmd   .= " -g $vFIGroup"    if ($vFIGroup ne "");
      $vMakeFiCmd   .= " -r"              if (($vForceGeneration) || ($vTempForceGeneration =~ /force/i));
      $vMakeFiCmd   .= " -d"              if ($vGeneratePdf);
      $vMakeFiCmd   .= " -f $vFIListFile" if ($vFIListFile ne "");
      print LOGFILE "INFO     call command: '$vMakeFiCmd'\n";
      close(LOGFILE);  # close logfile, because make_fi_complete.pl use it too

      print "Run make_fi_complete from \"${vFITemplatePath}fi/template\"\n";
      #~ chdir($vFIGroupPath."template");
      my $vRet = system($vMakeFiCmd);

      open(LOGFILE, ">>".$vLogFile) or die "ERROR    could not open ".$vLogFile." for writing\n";  # reopen logfile
      print LOGFILE "INFO     make_fi_complete.pl returns: $vRet\n\n";
      if ($vRet) {
         close(LOGFILE);
         exit $vRet;
      }
   #~ }

   my $vInterfaceFile = undef;
   if ( ($vXMLVersion eq "1.0") and (-d $_SWBUILDROOT."generated")  ) {
      mkpath($_SWBUILDROOT."generated/interfaces/");
      $vInterfaceFile = $_SWBUILDROOT."generated/interfaces/".$vFIGroup."fi".$vFIGenAppendix."_if.h";
   }
   elsif ($vXMLVersion eq "1.0") {
      # create new link-header (note: there is no vFIGenAppendix "_gen" at XML 1.0)
      $vInterfaceFile = $_SWNAVIROOT."components/interfaces/".$vFIGroup."fi_if.h";
   }
   elsif (-d $_SWBUILDROOT."generated/interfaces/") {
      # create new link-header at XML 2.0
      $vInterfaceFile = $_SWBUILDROOT."generated/interfaces/".$vFIGroup."fi".$vFIGenAppendix."_if.h";
   }
   if ($vInterfaceFile) {
      print LOGFILE "INFO     create '$vInterfaceFile'\n\n";
      open(IFFILE, ">".$vInterfaceFile) or die "ERROR    could not open ".$vInterfaceFile." for writing\n";
      print IFFILE "#include \"".$vFIGenOutPath.$vFIGroup."fi_if.h\"\n";
      close(IFFILE);
   }
}

##############################################################
sub extract_fi_archive
##############################################################
{
   print LOGFILE "INFO     using pre-generated archive '".$vFIArchivePath.$vFIArchiveFile."'\n";

   chdir($vFIGroupPath);
   mkpath($vFIGenOutPath);

   my $vCD = ($^O =~ /mswin/i)? "cd /d ".$vFIArchivePath."&" : "cd ".$vFIArchivePath.";";
   my $vRet = system("$vCD tar -xzf ".$vFIArchiveFile." -C ".$vFIGenOutPath);
   print LOGFILE "INFO     $vCD tar -xzf ".$vFIArchiveFile." -C ".$vFIGenOutPath.": $vRet\n\n";
   if ($vRet) {
      close(LOGFILE);
      exit $vRet;
   }

   if (-f $vFIGroupPath.$vFIGroup."fi".$vFIGenAppendix.".xml") {
      # special treatment, until most_fi.xml is moved to archive-file
      my $vRet = my_copy($vFIGroupPath.$vFIGroup."fi".$vFIGenAppendix.".xml", $vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".xml");
      print LOGFILE "INFO     copy ".$vFIGroupPath.$vFIGroup."fi".$vFIGenAppendix.".xml\n";
      make_unix_form_correction($vFIGenOutPath.$vFIGroup."fi".$vFIGenAppendix.".xml");

   }
}

##############################################################
sub make_unix_form_correction
# search all referenced files with "#include" or "files name"
# an replace every '\' with '/', also make a directory name
# replacement, if they are in wrong case
##############################################################
{
   my ($vFile)      = (@_);
   my $vFileContent = "";
   my $vHit         = 0;

   return if ($^O =~ /mswin/i); # correction is only needed for Linux

   print LOGFILE "\nINFO     enter make_unix_form_correction($vFile)\n";
   # find all FI-directories with pattern <FIGroup-dir>/<FI-dir>/source
   @vDirList = ();
   find(\&dirlist_rel, $vFIGenOutPath);
   print LOGFILE "INFO     \@vDirList:'@vDirList'\n";

   # find all source files (*.h and *.cpp)
   @vFileList = ();
   find(\&filelist_rel, $vFIGenOutPath);
   print LOGFILE "INFO     \@vFileList:'@vFileList'\n";

   # reading '<FI-Group>_if.h' file
   open(FILE, "<".$vFile) or die "ERROR     Not able to open '".$vFile."' for reading :$!\n";
   while (<FILE>) {
      my $line = $_;
      if ((/#include\s+\".*\"/) || (/<file name=\".*\"/)) {
         $vHit = 1 if (/\\/);
         $line =~ s/\\/\//g;

         foreach my $vFiPathName (@vDirList) {
            if (($line =~ /$vFiPathName\//i) && ($line !~ /$vFiPathName\//)) {
               print LOGFILE "  line:$line";
               print LOGFILE "  replace directory name to '$vFiPathName'\n\n";
               $vHit = 1;
               $line =~ s/$vFiPathName\//$vFiPathName\//ig;
               last;
            }
         }
         foreach my $vFiSourceName (@vFileList) {
            if (($line =~ /$vFiSourceName/i) && ($line !~ /$vFiSourceName/)) {
               print LOGFILE "  line:$line";
               print LOGFILE "  replace file name to '$vFiSourceName'\n\n";
               $vHit = 1;
               $line =~ s/$vFiSourceName/$vFiSourceName/ig;
               last;
            }
         }
      }
      $vFileContent .= $line;
   }
   close(FILE);

   if ($vHit) {
      my $vRet = my_copy($vFile, $vFile."_bak");
      print LOGFILE "INFO     copy '".$vFile."' to '".$vFile.".bak': $vRet\n";
      # writing file
      open(FILE, ">".$vFile) or die "ERROR     Not able to open '".$vFile."' for writing :$!\n";
      print FILE $vFileContent;
      close(FILE);
      print LOGFILE "INFO     wrote new file '".$vFile."'\n\n";
      print STDERR "WARNING  file \"".$vFile."\" was converted to fulfill unix path name convention!\n";
   }
}

##############################################################
sub my_copy
##############################################################
{
   my ($vFile1, $vFile2) = (@_);

   open(FILE, "<".$vFile1)  or die "ERROR     Not able to open '$vFile1' for reading :$!\n";
   my @vFileContent = <FILE>;
   close(FILE);
   open(FILE, ">".$vFile2) or die "ERROR     Not able to open '$vFile2' for writing :$!\n";
   print FILE @vFileContent;
   close(FILE);
   return 0;
}

##############################################################
sub dirlist_rel
# return a list of all sub-directories with pattern
# <FIGroup-dir>/<FI-dir>/<FI-dir>.xml, except "template" and
# "types" as relative pathes
##############################################################
{
   push(@vDirList, $_) if (-d && (-d "$File::Find::dir/$_/source/"));
}

##############################################################
sub filelist_rel
# return a list of all *.h and *.cpp files without path
##############################################################
{
   push(@vFileList, $_) if (-f && ((/\.h$/) || (/\.cpp$/)));
}

##############################################################
sub filelist_xml
##############################################################
{
   # collect all *.xml in every sub-directory
   push(@vFileList, $File::Find::name) if (-f && (/\.xml$/i));
}

