#!/usr/bin/perl
####################################################################################################
# copy ficodegen *.txt template files, global navifi*.h
#   and *.pl scripts
# from either the template folder
#   or the specialized templates/most folder
#
#  Martin Koch (Fa. ESE), 2010-10-04
####################################################################################################

use File::Copy;
use File::Path;
use File::Basename;
use strict;

my $_SWNAVIROOT=$ENV{_SWNAVIROOT};
my $sourcedir = $ARGV[0] . "fi/template/";
my $targetDir = $ARGV[1];
my $filename;
my $copyTemplates = 0;
my $copyFITypes = 1;


if ( $copyTemplates ) {
  if ( -d $targetDir.'template' ) {
    print "INFO: recreating ${targetDir}template\n";
    rmtree($targetDir . 'template');
  }
  mkpath($targetDir . 'template');

  foreach $filename (glob("$sourcedir*"))
  {
    if ($filename =~ m/\.(h|txt|pl)$/) {
      my $filenamebase = basename($filename);
      if ( ! -e $targetDir.'template/'.$filenamebase ) {
        copy $filename, $targetDir.'template/'.$filenamebase;
      }
    }
  }
}


if ( $copyFITypes ) {
  my @additionalFiles = (
    "$_SWNAVIROOT/../di_fi/components/fi/types/fi_basetypes.xml",
    "$_SWNAVIROOT/../di_fi/components/fi/errors/fi_errors.xml",
    "$_SWNAVIROOT/../di_fi/components/fi/types/fi_types.xml",
    "$_SWNAVIROOT/../di_fi/components/fi/fi.xsd",
  );

  if ( -d $targetDir.'fi' ) {
    print "INFO: recreating ${targetDir}fi\n";
    rmtree($targetDir.'fi');
  }
  mkpath($targetDir.'fi');

  foreach $filename ( @additionalFiles ) {
    if ( -e $filename ) {
      my $filenamebase=basename($filename);
      if ( ! -e $targetDir.'fi'.$filenamebase ) {
        #~ print "--> copy ".$filename." ".$targetDir."fi/".$filenamebase."\n";
        copy $filename, $targetDir.'fi/'.$filenamebase;
      } else {
        #~ print "--> already there: $filenamebase\n";
      }
    }
  }
}
