/*!
*******************************************************************************
* \file               spi_tclMainApp.cpp
*******************************************************************************
\verbatim
PROJECT:        G3G
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:   CCA Application Spi_tclMainApp
                created with CCA skeleton generator.
COPYRIGHT:      &copy; RBEI
HISTORY:
Date       |  Author                          | Modifications
16.10.2013 |                                  | Initial Version
21.01.2014 |  Shiva Kumar Gurija              | Trace cmd to set the system date
\endverbatim
10.02.2014 | Vinoop U                         | New Trace class implemented for trace cmds.
06.04.2014 | Ramya Murthy                     | Initialisation sequence implementation
31.07.2014 | Ramya Murthy                     | SPI feature configuration via LoadSettings()

*******************************************************************************/

/******************************************************************************
| includes:
|----------------------------------------------------------------------------*/
#include <unistd.h>
#include "spi_tclService.h"
#include "spi_tclTrace.h"
#include "spi_LoopbackTypes.h"
#include "spi_tclConfigReader.h"
#include "spi_tclDefsetHandler.h"
#include "spi_tclMainApp.h"

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#ifdef VARIANT_S_FTR_ENABLE_PROCSTART_DATAPOOL
   #define DP_S_IMPORT_INTERFACE_BASE
   #include "dp_if.h"
#endif

//! Includes for Trace files
#include "Trace.h"
#ifdef TARGET_BUILD
   #ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
      #define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_APPLICATION
      #include "trcGenProj/Header/spi_tclMainApp.cpp.trc.h"
   #endif
#endif

/******************************************************************************
 | defines
 |----------------------------------------------------------------------------*/
/******************************************************************************/
// Check for availability of SPI TRACE trace input channel defines.
#ifdef TR_TTFIS_SMARTPHONE_INT
   // If exists use the existing defines.
   #define SPI_TRACE_CHANNEL         TR_TTFIS_SMARTPHONE_INT
#else
   // Trace channel is not defined yet.
   #define SPI_TRACE_CHANNEL         ((TR_tenTraceChan)178)
#endif


/******************************************************************************
 | GLOBAL VARIABLES
 |----------------------------------------------------------------------------*/
//Initialize the static member variable
spi_tclTrace* spi_tclMainApp::m_poCmdHndlr = OSAL_NULL;

/******************************************************************************
 | METHODS
 |----------------------------------------------------------------------------*/

/*******************************************************************************
 **FUNCTION   : spi_tclMainApp::spi_tclMainApp()
*******************************************************************************/
spi_tclMainApp::spi_tclMainApp()
   : ahl_tclBaseOneThreadApp(CCA_C_U16_APP_SMARTPHONEINTEGRATION),
   m_poService(OSAL_NULL),
   m_poDefSetHandler(OSAL_NULL)
{
   ETG_TRACE_USR1(("Constructor spi_tclMainApp() entered"));

   ETG_TRACE_USR2(("Constructor spi_tclMainApp() left"));

}  //!end of spi_tclMainApp()

/*******************************************************************************
      * \FUNCTION   : spi_tclMainApp::~spi_tclMainApp()
*******************************************************************************/
spi_tclMainApp::~spi_tclMainApp()
{
   ETG_TRACE_USR1(("Destructor ~spi_tclMainApp() entered"));

   //To avoid Lint Warnings;
   m_poService = OSAL_NULL;
   m_poDefSetHandler = OSAL_NULL;

   ETG_TRACE_USR2(("Destructor ~spi_tclMainApp() left"));

}  //!end of ~spi_tclMainApp()

/*******************************************************************************
      * \FUNCTION   : tBool spi_tclMainApp::bOnInit()
*******************************************************************************/
tBool spi_tclMainApp::bOnInit()
{
   ETG_TRACE_USR1(("spi_tclMainApp::bOnInit() entered"));

   //Handle SIGPIPE Signal. Since SPI component is working with Sockets(to exchange data with ML or Carplay Devices),
   //it would get SIGPIPE signal, whenever the broken pipe issue occurs.

   //Broken Pipe Error- if other end of the socket(Phone's side in our case) is down and not able to read/write the data 
   //our component will get SIGPIPE signal. If this signal is not handled, our component will crash.
   //This might have already handled by Platform. But to be independent of it, we are handling in our component also.
   struct sigaction rSigAct;
   memset (&rSigAct, '\0', sizeof(rSigAct));

   // Use the sa_sigaction field because the handles has two additional parameters 
   rSigAct.sa_sigaction = &spi_tclMainApp::vSignalHandler;
   // The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field, not sa_handler. 
   rSigAct.sa_flags = SA_SIGINFO;
   if (sigaction(SIGPIPE, &rSigAct, OSAL_NULL) < 0) 
   {
      ETG_TRACE_ERR(("spi_tclMainApp::bOnInit:Error in registering for Signal Handler:SIGPIPE"));
   }//if (sigaction(SIGPIPE, &rSigAct, OSAL_NULL) < 0) 


   //Plug in the Trace channel
   vPlugTraceIn(SPI_TRACE_CHANNEL, (OSAL_tpfCallback)vHandleTraceCmd);

   m_poService =  new spi_tclService(this);
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poService);

   m_poCmdHndlr = new spi_tclTrace(this);
   SPI_NORMAL_ASSERT(OSAL_NULL == m_poCmdHndlr);

   tBool bSuccess = true;

#ifdef VARIANT_S_FTR_ENABLE_SPI_DEFSET
   m_poDefSetHandler = new spi_tclDefsetHandler(this);
   bSuccess = (bSuccess  && (OSAL_NULL != m_poDefSetHandler));;
   NORMAL_M_ASSERT(OSAL_NULL != m_poDefSetHandler);
#endif

   bSuccess = ((bSuccess )&&(OSAL_NULL != m_poService) &&
              (OSAL_NULL != m_poCmdHndlr) &&
              (m_poService->bInitialize()));

   ETG_TRACE_USR1(("bOnInit() left with result: %u.", ETG_ENUM(BOOL,bSuccess)));

   //! Assert added to check return state of bOnInit
   NORMAL_M_ASSERT(true == bSuccess);
   return true;
} //!  end of bOnInit()

/*******************************************************************************
      * \FUNCTION   : tVoid spi_tclMainApp::vOnApplicationClose()
*******************************************************************************/
tVoid spi_tclMainApp::vOnApplicationClose()
{
   ETG_TRACE_USR1(("vOnApplicationClose() entered"));

   //Deactivate the TTFIS command channel
   vUnplugTrace( TR_TTFIS_SMARTPHONE_INT);

   if (OSAL_NULL != m_poService)
   {
      m_poService->bUnInitialize();
   }
   RELEASE_MEM_OSAL(m_poService);
   RELEASE_MEM_OSAL(m_poCmdHndlr);
#ifdef VARIANT_S_FTR_ENABLE_SPI_DEFSET
   RELEASE_MEM_OSAL(m_poDefSetHandler);
#endif
   ahl_tclBaseOneThreadApp::vOnApplicationClose();

   ETG_TRACE_USR2(("vOnApplicationClose() left"));

}  //!  end of vOnApplicationClose()

/*******************************************************************************
      * \FUNCTION   : tVoid spi_tclMainApp::vOnLoadSettings()
*******************************************************************************/
tVoid spi_tclMainApp::vOnLoadSettings()
{
   ETG_TRACE_USR1(("vOnLoadSettings() entered"));

   spi_tclConfigReader* poConfigReader = spi_tclConfigReader::getInstance();

   if ((OSAL_NULL != m_poService) && (OSAL_NULL != poConfigReader))
   {
      trSpiFeatureSupport rSpiFeatureSupp;
      poConfigReader->vGetSpiFeatureSupport(rSpiFeatureSupp);
      poConfigReader->vReadSysConfigs();
      tenRegion enRegion = poConfigReader->enGetRegion();
      m_poService->vSetRegion(enRegion);
      m_poService->vLoadSettings(rSpiFeatureSupp);
   }

   ETG_TRACE_USR2(("vOnLoadSettings() left"));

}   //!  end of vOnLoadSettings()

/*******************************************************************************
      * \FUNCTION   : tVoid spi_tclMainApp::vOnSaveSettings()
*******************************************************************************/
tVoid spi_tclMainApp::vOnSaveSettings()
{
  ETG_TRACE_USR1(("vOnSaveSettings() entered"));

  if (OSAL_NULL != m_poService)
  {
     m_poService->vSaveSettings();
  }

  ETG_TRACE_USR2(("vOnSaveSettings() left"));

}  //!  end of vOnSaveSettings()

/*******************************************************************************
      * \FUNCTION   : tVoid spi_tclMainApp::vOnTimer(tU16 u16TimerId)
*******************************************************************************/
tVoid spi_tclMainApp::vOnTimer(tU16 u16TimerId)
{
  ETG_TRACE_USR1(("vOnTimer(): entered for TimerID 0x%x.",
                  u16TimerId));

  ETG_TRACE_USR2(("vOnTimer() left"));

}   //!   end of vOnTimer()

/******************************************************************************
** FUNCTION:  tVoid spi_tclMainApp::vHandleTraceCmd(tU8 const* const cpu8B..
******************************************************************************/
/*static*/
tVoid spi_tclMainApp::vHandleTraceCmd(tU8 const* const cpu8Buffer)
{
   ETG_TRACE_USR2(("vHandleTraceCmd entered"));
   if(OSAL_NULL != m_poCmdHndlr)
   {
      m_poCmdHndlr->vProcessTraceCmd(cpu8Buffer);
   }
}

/***************************************************************************
** FUNCTION   : t_Void spi_tclMainApp::vSignalHandler()
***************************************************************************/
t_Void spi_tclMainApp::vSignalHandler(t_S32 s32SigNum, 
                                      siginfo_t *pSiginfo, 
                                      t_Void *pContext)
{
     ETG_TRACE_USR1(("spi_tclMainApp::vSignalHandler:: Received signal %d ",
      ETG_ENUM(SIGNALS,s32SigNum)));

     SPI_INTENTIONALLY_UNUSED(pContext);

   if(OSAL_NULL != pSiginfo)
   {
      ETG_TRACE_USR2(("spi_tclMainApp::vSignalHandler:: Signal is received from PID-%d,UID-%d",
         (t_S32)(pSiginfo->si_pid),(t_S32)(pSiginfo->si_uid)));
   }//if(NULL != pSiginfo)
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclMainApp::vOnEvent(OSAL_tEventMask nEvent)
**************************************************************************/
tVoid spi_tclMainApp::vOnEvent(OSAL_tEventMask nEvent)
{
   ETG_TRACE_USR1(("vOnEvent(): entered for Event 0x%x. \n", (tU32) nEvent));

   //! Forward requested event to service
   if (OSAL_NULL != m_poService)
   {
      switch (nEvent)
      {
         case SPI_C_U32_EVENT_ID_DEFSET_CLEARPRIVATEDATA:
         case SPI_C_U32_EVENT_ID_DEFSET_TEF:
         case SPI_C_U32_EVENT_ID_DEFSET_CUSTOMER:
         {
            m_poService->vRestoreSettings();
         }
            break;
         case SPI_C_U32_EVENT_ID_DEFSET_READEOL:
         {
            ETG_TRACE_USR2(("vOnEvent(): Defet for EOL Event 0x%x. \n", (tU32) nEvent));
         }
            break;
         default:
         {
            ETG_TRACE_ERR(("vOnEvent(): Invalid event: 0x%x! \n", (tU32) nEvent));
         }//default
            break;
      } //switch (nEvent)
   } //if (OSAL_NULL != m_poService)
}


#ifdef VARIANT_S_FTR_ENABLE_BUILD_AS_PROCESS

#define EVENT_SHUTDOWN_NAME "SHUTDOWN_FC_SPI"
#define SCD_S_IMPORT_INTERFACE_GENERIC
#include "scd_if.h"
extern "C" OSAL_DECL tS32 vStartApp(tS32 cPar, tString aPar[])
{
  t_Bool bRetVal = OSAL_OK;
  OSAL_tEventHandle  hEvShutdown = 0;
  OSAL_tEventMask    hEvRequest  = 0x00000001;

  SPI_INTENTIONALLY_UNUSED(cPar);
  SPI_INTENTIONALLY_UNUSED(aPar);

  et_vTraceOpen();
  et_vTraceBuffer(TR_TTFIS_SMARTPHONE_INT, TR_LEVEL_DATA, 9, "START FC_SPI ");
  scd_init();

  if (OSAL_s32EventCreate(EVENT_SHUTDOWN_NAME, &hEvShutdown) == OSAL_ERROR)
  {
    ETG_TRACE_ERR(("spi_tclMainApp: Creation of SPM shutdown event failed!!!"));
    NORMAL_M_ASSERT_ALWAYS();
  }

  // Activate Exception Handling

  NORMAL_M_ASSERT(FALSE != exh_bInitExceptionHandling());

  OSAL_tProcessID pId = OSAL_ProcessWhoAmI();
  printf("\n---------------------------------------------------\n");
  printf("FC_SPI [PID: %d (0x%08X)]\n", (int) pId, pId);
  printf("---------------------------------------------------\n");

#ifdef VARIANT_S_FTR_ENABLE_PROCSTART_DATAPOOL
  DP_vCreateDatapool();
#endif

  spi_tclMainApp *pSpiApp = new spi_tclMainApp;

  if (pSpiApp != OSAL_NULL)
  {
    if (!pSpiApp->bInitInstance(0, CCA_C_U16_APP_SMARTPHONEINTEGRATION))
    {
        printf("\n\npspiApp->bInitInstance() failed!!!\n");
    } 
    else 
    {
        printf("\n\npspiApp->bInitInstance() success!\n");
    }
  }

  // Wait for Shutdown-Signal    
  OSAL_s32EventWait(hEvShutdown, hEvRequest, OSAL_EN_EVENTMASK_OR, OSAL_C_TIMEOUT_FOREVER, &hEvRequest);
  if (pSpiApp != OSAL_NULL)
  {
     pSpiApp->vDeinitInstance();
     delete pSpiApp;
  }
  printf("\n\nspi_tclMainApp: Exiting...\n");
  OSAL_s32EventClose(hEvShutdown);
  OSAL_s32EventDelete(EVENT_SHUTDOWN_NAME);
  et_vTraceClose();
  return bRetVal;
}
#endif


