/*!
 *******************************************************************************
 * \file               spi_tclService.cpp
 *******************************************************************************
 \verbatim
 PROJECT:        G3G
 SW-COMPONENT:   Smart Phone Integration
 DESCRIPTION:     CCA service Spi
 created with CCA skeleton generator.
 COPYRIGHT:      &copy; RBEI
 HISTORY:
 Date       | Author                 | Modifications
 30.10.2013 | Vinoop U               | Initial Version
 03.12.2013 | Ramya Murthy           | Updated vOnMSGetDeviceInfoList()
                                       and vOnMSGetAppList() definitions to 
                                       comply with changes to SpiCmdInterface.
 26.12.2013 | Shiva Kumar G          | Updated with few elements for 
                                       HMI Interface
 05.04.2013 | Shiva Kumar G          | Updated New Methods & Properties
 06.04.2014 | Ramya Murthy           | Initialisation sequence implementation
 10.06.2014 | Ramya Murthy           | Audio policy redesign implementation.
 16.07.2014 | Shiva Kumar G          | Implemented SessionStatusInfo update
 31.07.2014 | Ramya Murthy           | SPI feature configuration via LoadSettings()
 30.09.2014 | Ramya Murthy           | Called vDestory() of FI messages that use dynamic
                                       memory allocation
 01.10.2014 | Ramya Murthy           | Added Telephone client handler (moved from BT Manager)
 25.10.2014 |  Shihabudheen P M      | added vUpdateSessionStatusInfo
 28.10.2014 | Hari Priya E R         | Added changes for subscription of location data
 31.10.2014 | Ramya Murthy           | Implementation for "SameDevice" param in BluetoothDeviceStatus
 17.11.2014 | Hari Priya E R         | Fix for SUZUKI-20652
 05.11.2014 | Ramya Murthy           | Implementation for Application metadata.
 12.03.2014 | Ramkumar Muniraj       | Post notification was not informed. This is fixed.
 29.05.2015 | Ramya Murthy           | Implementation for BTPairingRequired property.
 12.06.2015 | Sameer Chandra         | Adapted clientCapabilities as per new FI.
 19.06.2015 | Shihabudheen P M       | added vOnDataServiceSubscribeRquest()
 25.06.2015 | Sameer Chandra         | Added ML XDeviceKey Support for PSA
 15.06.2014 | Shihabudheen P M       | vOnLbBTAddressUpdate()
 14.07.2015 | Shiva Kumar G          | Adaptations to upadted FI 
                                       1.Adding display width and height in mm to T_ DisplayLayerAttributes.
                                       2.Removal of u16MultiTouchSupport.
16.07.2015  |  Sameer Chandra        | Added method vOnMSSendKnobEncoderChange to support Knob
                                       Encoder.
10.08.2015  | Vinoop U               | Added method result implementation for methods
                                       SetVehicleConfiguration,SetAccessoryDisplayContext,
                                       SetDisplayAttributes,SetDeviceUsagePreference,
                                       SetVehicleBTAddress
09.09.2015  | Shiva Kumar Gurija     | Extended ML Keys support for PSA SOP1
07.09.2015  | Dhiraj Asopa           | Added Implementation of method SetAccessoryAudioContext().
11.09.2015  | Dhiraj Asopa           | Added Implementation of method vOnMSSetFeatureRestrictions().
14.12.2015  | Rachana L Achar        | Modified the prototype of vLaunchAppResult method
26.02.2016  | Rachana L Achar        | AAP Navigation implementation
10.03.2016  | Rachana L Achar        | AAP Notification implementation
 
 \endverbatim
 *******************************************************************************/

/******************************************************************************
 | includes:
 |----------------------------------------------------------------------------*/
#include "StringHandler.h"
#include "TraceStreamable.h"
using namespace spi::spitrace;
#include "spi_tclCmdInterface.h"
#include "XFiObjHandler.h"
#include "spi_tclMainApp.h"
#include "FIMsgDispatch.h"
#include "spi_tclAudioPolicy.h"
#include "spi_tclTelephoneClient.h"
#include "spi_tclPosDataClientHandler.h"
#include "spi_tclSensorDataClientHandler.h"
#include "spi_tclSPMClient.h"
#include "spi_tclService.h"
#include "spi_LoopbackTypes.h"
#include "spi_tclConfigReader.h"

using namespace shl::msgHandler;

//!Include common fi interface
#define FI_S_IMPORT_INTERFACE_BASE_TYPES
#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#define GENERICMSGS_S_IMPORT_INTERFACE_GENERIC
#include "generic_msgs_if.h"

#include "Trace.h"
#ifdef TARGET_BUILD
#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_TCLSERVICE
#include "trcGenProj/Header/spi_tclService.cpp.trc.h"
#endif
#endif

//lint -save -e1055 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e1013 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e63 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e40 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e516 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported
//lint -save -e746 PQM_authorized_multi_492_to_494   Reason: C++11 not fully supported


/******************************************************************************
 | defines
 |----------------------------------------------------------------------------*/

//!Version defines for offered service
#define SPI_SERVICE_FI_MAJOR_VERSION  MIDW_SMARTPHONEINTFI_C_U16_SERVICE_MAJORVERSION
#define SPI_SERVICE_FI_MINOR_VERSION  MIDW_SMARTPHONEINTFI_C_U16_SERVICE_MINORVERSION
#define SPI_SERVICE_FI_PATCH_VERSION  0

#define CLEAR_BTDEVSTATUS_MSG(MESSAGE_OBJ) \
      MESSAGE_OBJ.BluetoothDeviceHandle = 0;  \
      MESSAGE_OBJ.ProjectionDeviceHandle = 0; \
      MESSAGE_OBJ.BTChangeInfo.enType = static_cast<midw_fi_tcl_e8_BTChangeInfo::tenType>(e8NO_CHANGE);   \
      MESSAGE_OBJ.CallActiveStatus = false;

//Macro to set BTPairingRequired message data to default
#define CLEAR_BTPAIR_REQUIRED_MSG(MESSAGE_OBJ)   \
      MESSAGE_OBJ.vDestroy();  \
      MESSAGE_OBJ.bPairingReqd = false;

typedef struct
{
   //! Key Code which we will receive from the HMI
   spi_tenFiKeyCode e32SpiKeyCode;
   //! Corresponding SPI type Switch Code
   tenKeyCode enKeyCode;

}trKeyCode;

const trKeyCode aKeyCode[] =
{
   {spi_tenFiKeyCode::FI_EN_DEV_BACKWARD,e32DEV_BACKWARD},
   {spi_tenFiKeyCode::FI_EN_DEV_MENU,e32DEV_MENU},
   {spi_tenFiKeyCode::FI_EN_MULTIMEDIA_NEXT,e32MULTIMEDIA_NEXT},
   {spi_tenFiKeyCode::FI_EN_MULTIMEDIA_PREVIOUS,e32MULTIMEDIA_PREVIOUS},
   {spi_tenFiKeyCode::FI_EN_DEV_PHONE_FLASH,e32DEV_PHONE_FLASH},
   {spi_tenFiKeyCode::FI_EN_MULTIMEDIA_PLAY,e32MULTIMEDIA_PLAY},
   {spi_tenFiKeyCode::FI_EN_MULTIMEDIA_PAUSE,e32MULTIMEDIA_PAUSE},
   {spi_tenFiKeyCode::FI_EN_MULTIMEDIA_STOP,e32MULTIMEDIA_STOP},
   {spi_tenFiKeyCode::FI_EN_DEV_PHONE_CALL,e32DEV_PHONE_CALL},
   {spi_tenFiKeyCode::FI_EN_DEV_PHONE_END,e32DEV_PHONE_END},
   {spi_tenFiKeyCode::FI_EN_DEV_SOFT_LEFT,e32DEV_SOFT_LEFT},
   {spi_tenFiKeyCode::FI_EN_DEV_SOFT_MIDDLE,e32DEV_SOFT_MIDDLE},
   {spi_tenFiKeyCode::FI_EN_DEV_SOFT_RIGHT,e32DEV_SOFT_RIGHT},
   {spi_tenFiKeyCode::FI_EN_DEV_APPLICATION,e32DEV_APPLICATION},
   {spi_tenFiKeyCode::FI_EN_DEV_OK,e32DEV_OK},
   {spi_tenFiKeyCode::FI_EN_DEV_DELETE,e32DEV_DELETE},
   {spi_tenFiKeyCode::FI_EN_DEV_ZOOM_IN,e32DEV_ZOOM_IN},
   {spi_tenFiKeyCode::FI_EN_DEV_ZOOM_OUT,e32DEV_ZOOM_OUT},
   {spi_tenFiKeyCode::FI_EN_DEV_CLEAR,e32DEV_CLEAR},
   {spi_tenFiKeyCode::FI_EN_DEV_FORWARD,e32DEV_FORWARD},
   {spi_tenFiKeyCode::FI_EN_DEV_HOME,e32DEV_HOME},
   {spi_tenFiKeyCode::FI_EN_DEV_SEARCH,e32DEV_SEARCH},
   {spi_tenFiKeyCode::FI_EN_DEV_PTT,e32DEV_PTT}
};

//To convert Km/h into cm/s
//static const t_Float scf16SpeedConversionValue = 27.78;

//! Middleware FI string Character set
#define MIDWFI_CHAR_SET_UTF8   (midw_fi_tclString::FI_EN_UTF8)

/******************************************************************************
 | typedefs (scope: module-local)
 |----------------------------------------------------------------------------*/

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgGetDeviceInfoListMethodStart> spi_tMsgGetDevInfoListMS
 * \brief Get Device Info List Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgGetDeviceInfoListMethodStart>
         spi_tMsgGetDevInfoListMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSelectDeviceMethodStart> spi_tMsgSelectDeviceMS
 * \brief Select Device Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSelectDeviceMethodStart>
         spi_tMsgSelectDeviceMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgLaunchAppMethodStart> spi_tMsgLaunchAppMS
 * \brief Launch App Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgLaunchAppMethodStart>
         spi_tMsgLaunchAppMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgTerminateAppMethodStart> spi_tMsgTerminateAppMS
 * \brief Terminate App Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgTerminateAppMethodStart>
         spi_tMsgTerminateAppMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgGetAppListMethodStart> spi_tMsgGetAppListMS
 * \brief Get Application List Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgGetAppListMethodStart>
         spi_tMsgGetAppListMS;

/*! 
 * \XFiObjHandler<midw_smartphoneintfi_tclMsgSetAppIconAttributesMethodStart>
 * \brief Get Application Icon data Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgGetAppIconDataMethodStart>
         spi_tMsgGetAppIconDataMS;

/*! 
 * \XFiObjHandler<midw_smartphoneintfi_tclMsgSetAppIconAttributesMethodStart>
 * \brief Set application Icon Attributes Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetAppIconAttributesMethodStart>
        spi_tMsgSetAppIconAttrMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetVehicleConfigurationMethodStart> spi_tMsgSetVehicleConfigMS
 * \brief Set vehicle configuration Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetVehicleConfigurationMethodStart>
        spi_tMsgSetVehicleConfigMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetVehicleMovementStateMethodStart> spi_tMsgSetVehicleMovementMS
 * \brief Set vehicle Movement Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetVehicleMovementStateMethodStart>
        spi_tMsgSetVehicleMovementMS;

/*!
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgVehicleMechanicalSpeedMethodStart> spi_tMsgVehicleMechanicalSpeedMS
 * \brief Vehicle Mechanical Speed Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgVehicleMechanicalSpeedMethodStart>
        spi_tMsgVehicleMechanicalSpeedMS;

/*!
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetVideoBlockingModeMethodStart> spi_tMsgSetVideoBlockingModeMS
 * \brief Set video blocking mode Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetVideoBlockingModeMethodStart>
        spi_tMsgSetVideoBlockingModeMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetAudioBlockingModeMethodStart> spi_tMsgSetAudioBlockingModeMS
 * \brief Set audio blocking mode Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetAudioBlockingModeMethodStart>
       spi_tMsgSetAudioBlockingModeMS;

/*!
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgInvokeBluetoothDeviceActionMethodStart> spi_tMsgInvokeBTDeviceActionMS
 * \brief Invoke Bluetooth Device Action Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgInvokeBluetoothDeviceActionMethodStart>
       spi_tMsgInvokeBTDeviceActionMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetOrientationModeMethodStart> spi_tMsgSetOrientationModeMS
 * \brief Set Orientation mode Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetOrientationModeMethodStart>
       spi_tMsgSetOrientationModeMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetScreenSizeMethodStart> spi_tMsgSetScreenSizeMS
 * \brief Set Screen Size Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetScreenSizeMethodStart>
       spi_tMsgSetScreenSizeMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgGetVideoSettingsMethodStart> spi_tMsgGetVideoSettingsMS
 * \brief Get Video Settings Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgGetVideoSettingsMethodStart>
       spi_tMsgGetVideoSettingsMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetDeviceUsagePreferenceMethodStart> spi_tMsgSetDeviceUsagePreferenceMS
 * \brief set device usage preference Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetDeviceUsagePreferenceMethodStart>
       spi_tMsgSetDeviceUsagePreferenceMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgGetDeviceUsagePreferenceMethodStart> spi_tMsgGetDeviceUsagePreferenceMS
 * \brief get device usage preference Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgGetDeviceUsagePreferenceMethodStart>
       spi_tMsgGetDeviceUsagePreferenceMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgAccessoryDisplayContextMethodStart> spi_tMsgAccDispCntxtMS
 * \brief Accessory display context Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgAccessoryDisplayContextMethodStart>
       spi_tMsgAccDispCntxtMS;

/*! 
 * \XFiObjHandler<midw_smartphoneintfi_tclMsgAccessoryAudioContextMethodStart> spi_tMsgAccAudioCntxtMS
 * \brief Accessory audio context Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgAccessoryAudioContextMethodStart>
       spi_tMsgAccAudioCntxtMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetClientCapabilitiesMethodStart> spi_tMsgSetClientCapabilitiesMS
 * \brief set client capabilities Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetClientCapabilitiesMethodStart>
       spi_tMsgSetClientCapabilitiesMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetMLNotificationEnabledInfoMethodStart> spi_tMsgSetSetMLNotiEnabledInfoMS
 * \brief Set ML Notification Enabled Info Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetMLNotificationEnabledInfoMethodStart>
       spi_tMsgSetSetMLNotiEnabledInfoMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgInvokeNotificationActionMethodStart> spi_tMsgInvokeNotiAction
 * \brief Invoke Notification Action  Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgInvokeNotificationActionMethodStart>
       spi_tMsgInvokeNotiAction;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSendTouchEventMethodStart> spi_tMsgSetSendTouchEventMS
 * \brief seend touch events Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSendTouchEventMethodStart>
       spi_tMsgSetSendTouchEventMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSendKeyEventMethodStart> spi_tMsgSendKeyEventMS
 * \brief Send Key Events Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSendKeyEventMethodStart>
       spi_tMsgSendKeyEventMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetVehicleBTAddressMethodStart> spi_tMsgSetVehicleBTAddressMS
 * \brief set vehicle BT Address Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetVehicleBTAddressMethodStart>
       spi_tMsgSetVehicleBTAddressMS;


/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetRegionMethodStart> spi_tMsgSetRegionMS
 * \brief set Region Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetRegionMethodStart>
       spi_tMsgSetRegionMS;

/*! 
 * \ XFiObjHandler<midw_smartphoneintfi_tclMsgAccessoryAppStateMethodStart> spi_tMsgAccAppStateMS
 * \brief set accessory App state 
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgAccessoryAppStateMethodStart>
       spi_tMsgAccAppStateMS;

/*!
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgDiPORoleSwitchRequiredMethodStart> spi_tMsgRoleSwitchReqMS
 * \brief role switch required Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgDiPORoleSwitchRequiredMethodStart>
      spi_tMsgRoleSwitchReqMS;

/*!
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetAccessoryDisplayContextMethodStart> spi_tMsgSetAccDisplayContextReqMS
 * \brief set accessory display context method start.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetAccessoryDisplayContextMethodStart>
      spi_tMsgSetAccDisplayContextReqMS;

/*! 
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgSetDisplayAttributesMethodStart> spi_tMsgSetSetDisplayAttributesMS
 * \brief set display attributes Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetDisplayAttributesMethodStart>
       spi_tMsgSetDisplayAttributesMS;
/*!
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgGetKeyIconDataMethodStart> spi_tMsgGetKeyIconDataMS
 * \brief get ML key icon data.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgGetKeyIconDataMethodStart>
        spi_tMsgGetKeyIconDataMS;

/*!
 * \XFIObjHandler<midw_smartphoneintfi_tclMsgRotaryControllerEventMethodStart> spi_tMsgRotaryCtrlEventMS
 * \brief Send Rotary Controller event.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgRotaryControllerEventMethodStart>
        spi_tMsgRotaryCtrlEventMS;


/*!
 * \XFiObjHandler<midw_smartphoneintfi_tclMsgSetAccessoryAudioContextMethodStart> spi_tMsgSetAccAudioCntxtMS
 * \brief Set Accessory audio context Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetAccessoryAudioContextMethodStart>
spi_tMsgSetAccAudioCntxtMS;

/*!
 * \XFiObjHandler<midw_smartphoneintfi_tclMsgSetFeatureRestrictionsMethodStart> spi_tMsgSetSetFeatureRestrictionsMS
 * \brief Set Feature Restrictions Method start, with auto extract & auto destroy feature.
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetFeatureRestrictionsMethodStart>
spi_tMsgSetFeatureRestrictionsMS;

/*!
 * \brief Set Technology preference if device supports more than one technology
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetTechnologyPreferenceMethodStart>
spi_tMsgSetTechnologyPrefMS;

/*!
 * \brief Get Technology preference setting
 */
typedef XFiObjHandler<midw_smartphoneintfi_tclMsgGetTechnologyPreferenceMethodStart>
spi_tMsgGetTechnologyPrefMS;
/*
 * \brief Set the Device authorization status
 */

typedef XFiObjHandler<midw_smartphoneintfi_tclMsgProjectionDeviceAuthorizationSet>
spi_tMsgSetDeviceAuth;
/*
 * \brief Set the Device selection mode (automatic/manual)
 */

typedef XFiObjHandler<midw_smartphoneintfi_tclMsgSetDeviceSelectionModeMethodStart>
spi_tMsgSetDeviceSelModeMS;

/*
 * \brief Set the Notification Status
 */

typedef XFiObjHandler<midw_smartphoneintfi_tclMsgAAPNotificationEventSet>
spi_tMsgSetAAPNotifEvent;



/*****************************************************************************
 * \CCA MESSAGE MAP
 ******************************************************************************/
//!Add your ON_MESSAGE_SVCDATA() macros here to define which corresponding
//!method should be called on receiving a specific message.
BEGIN_MSG_MAP(spi_tclService, ahl_tclBaseWork)

ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_GETDEVICEINFOLIST,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSGetDeviceInfoList)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SELECTDEVICE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSelectDevice)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_LAUNCHAPP,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSLaunchApp)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_TERMINATEAPP,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSTerminateApp)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_GETAPPLIST,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSGetAppList)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_GETAPPICONDATA ,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSGetAppIconData)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETAPPICONATTRIBUTES ,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetAppIconAttr)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETVEHICLECONFIGURATION ,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetVehicleConfig)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETVEHICLEMOVEMENTSTATE ,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetVehicleMovementState)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_VEHICLEMECHANICALSPEED ,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSVehicleMechanicalSpeed)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETVIDEOBLOCKINGMODE ,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetVideoBlockingMode)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETAUDIOBLOCKINGMODE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetAudioBlockingMode)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_INVOKEBLUETOOTHDEVICEACTION,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSInvokeBTDeviceAction)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETORIENTATIONMODE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetOrientationMode)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETSCREENSIZE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetScreenSize)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_GETVIDEOSETTINGS,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSGetVideoSettings)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETDEVICEUSAGEPREFERENCE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetDeviceUsagePreference)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_GETDEVICEUSAGEPREFERENCE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSGetDeviceUsagePreference)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_ACCESSORYDISPLAYCONTEXT,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSAccessoryDisplayContext)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETCLIENTCAPABILITIES,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetClientCapabilities)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETMLNOTIFICATIONENABLEDINFO,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetMLNotificationEnabledInfo)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_INVOKENOTIFICATIONACTION,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSInvokeNotificationAction)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SENDTOUCHEVENT,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSendTouchEvent)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SENDKEYEVENT,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSendKeyEvent)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETVEHICLEBTADDRESS,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetVehicleBTAddress)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETREGION,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetRegion)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_ACCESSORYAUDIOCONTEXT,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSAccessoryAudioContext)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_ACCESSORYAPPSTATE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSAccessoryAppState)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_DIPOROLESWITCHREQUIRED,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSDiPoSwitchRequired)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETACCESSORYDISPLAYCONTEXT,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetAccessoryDisplayContext)
ON_MESSAGE_SVCDATA(SPI_C_U16_IFID_DATA_SERVICE_SUBSCRIBE,
         AMT_C_U8_CCAMSG_OPCODE_STATUS,vOnDataServiceSubscribeRquest)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_GETKEYICONDATA,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSGetKeyIconData)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETDISPLAYATTRIBUTES,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetDisplayAttributes)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_ROTARYCONTROLLEREVENT,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSendRotaryCtrlEvent)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETACCESSORYAUDIOCONTEXT,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetAccessoryAudioContext)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETFEATURERESTRICTIONS,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetFeatureRestrictions)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETTECHNOLOGYPREFERENCE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetTechnologyPreference)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_GETTECHNOLOGYPREFERENCE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSGetTechnologyPreference)
ON_MESSAGE_SVCDATA(MIDW_SMARTPHONEINTFI_C_U16_SETDEVICESELECTIONMODE,
         AMT_C_U8_CCAMSG_OPCODE_METHODSTART,vOnMSSetDeviceSelMode)
END_MSG_MAP()

/**************************************************************************
 * \FUNCTION   : spi_tclService::spi_tclService(spi_tclMainApp* poMainAppl)
 **************************************************************************/
spi_tclService::spi_tclService(spi_tclMainApp* poMainAppl)
   : ahl_tclBaseOneThreadService(
      poMainAppl, /* Main Appl Pointer          */
      CCA_C_U16_SRV_SMARTPHONEINTEGRATION, /* ID of offered Service           */
      SPI_SERVICE_FI_MAJOR_VERSION, /* MajorVersion of offered Service */
      SPI_SERVICE_FI_MINOR_VERSION, /* MinorVersion of offered Service */
      SPI_SERVICE_FI_PATCH_VERSION), /* PatchVersion of offered Service */
     m_poMainAppl(poMainAppl),
     m_poSpiCmdIntf(OSAL_NULL),
     m_poAudioPolicy(OSAL_NULL),
     m_poTelephoneClient(OSAL_NULL),
     m_poPosDataClient(OSAL_NULL),
     m_poSensorDataClient(OSAL_NULL),
     m_poSPMClient(OSAL_NULL),
     m_bIsDiPOSupported(false)
{
   ETG_TRACE_USR1(("spi_tclService::spi_tclService() entered"));
   #ifdef GEN3X86
   vSetPrivateServiceAvailableAllowed(false);
   #endif
   NORMAL_M_ASSERT(OSAL_NULL != poMainAppl);
} // end of spi_tclService()

/**************************************************************************
 * \FUNCTION   : spi_tclService::~spi_tclService(tVoid)
 *******************************************************************************/
spi_tclService::~spi_tclService(tVoid)
{
   ETG_TRACE_USR1(("spi_tclService::~spi_tclService() entered"));

   m_NotificationInfo.vDestroy();
   m_AppMediaMetaData.vDestroy();
   m_AppPhoneData.vDestroy();
   m_AppMediaPlayBacktime.vDestroy();
   m_poSPMClient = OSAL_NULL;
   m_poPosDataClient = OSAL_NULL;
   m_poSensorDataClient = OSAL_NULL;
   m_poTelephoneClient = OSAL_NULL;
   m_poAudioPolicy = OSAL_NULL;
   m_poSpiCmdIntf = OSAL_NULL;
   m_poMainAppl = OSAL_NULL;
} // end of ~spi_tclService()

/**************************************************************************
** FUNCTION:  t_Bool spi_tclService::bInitialize();
**************************************************************************/
t_Bool spi_tclService::bInitialize()
{
   ETG_TRACE_USR1(("spi_tclService::bInitialize() entered"));

   t_Bool bInit = false;

   //!Initialize SPI service messages
   m_AppInfoStatus.DeviceHandle = 0;
   m_AppInfoStatus.DeviceConnectionType.enType = midw_fi_tcl_e8_DeviceConnectionType::FI_EN_UNKNOWN_CONNECTION;
   m_AppInfoStatus.AppStatus.enType = midw_fi_tcl_e8_AppStatusInfo::FI_EN_NOT_KNOWN;

   //!Initialise BTDeviceStatus message
   CLEAR_BTDEVSTATUS_MSG(m_BTDeviceStatusMsg);
   CLEAR_BTPAIR_REQUIRED_MSG(m_BTPairingRequiredMsg);

   //! Initialize Session Status info message
   m_SessionStatus.DeviceHandle = 0;
   m_SessionStatus.DeviceCategory.enType = midw_fi_tcl_e8_DeviceCategory::FI_EN_DEV_TYPE_UNKNOWN;
   m_SessionStatus.SessionStatus.enType = midw_fi_tcl_e8_SessionStatus::FI_EN_SESSION_UNKNOWN;
   m_SessionStatus.HandsetInteractionStatus.enType = midw_fi_tcl_e8_HandsetInteraction::FI_EN_HANDSET_INTERACTION_UNKNOWN;

   //! Initialize Device Authorization status
   m_DevAuthStatus.NumDevices = 0;

   //! Initialise Command interface pointer
   if (OSAL_NULL != m_poMainAppl)
   {
      //Invoke SPI C++ Interface class and pass the Response class pointer
      m_poSpiCmdIntf = spi_tclCmdInterface::getInstance(this, m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poSpiCmdIntf);

      if (OSAL_NULL != m_poSpiCmdIntf)
      {
         m_poAudioPolicy = OSAL_NEW spi_tclAudioPolicy(m_poSpiCmdIntf, m_poMainAppl);
      }
      NORMAL_M_ASSERT(OSAL_NULL != m_poAudioPolicy);

      m_poTelephoneClient = OSAL_NEW spi_tclTelephoneClient(m_poMainAppl, CCA_C_U16_SRV_FB_TELEPHONE);
      NORMAL_M_ASSERT(OSAL_NULL != m_poTelephoneClient);

      m_poPosDataClient = OSAL_NEW spi_tclPosDataClientHandler(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poPosDataClient);

      m_poSensorDataClient = OSAL_NEW spi_tclSensorDataClientHandler(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poSensorDataClient);

      m_poSPMClient = OSAL_NEW spi_tclSPMClient(m_poMainAppl);
      NORMAL_M_ASSERT(OSAL_NULL != m_poSPMClient);

      bInit = ((OSAL_NULL != m_poSpiCmdIntf) &&
            (OSAL_NULL != m_poAudioPolicy) &&
            (OSAL_NULL != m_poTelephoneClient) &&
            (OSAL_NULL != m_poPosDataClient) &&
            (OSAL_NULL != m_poSensorDataClient) &&
            (OSAL_NULL != m_poSPMClient) &&
            (m_poSpiCmdIntf->bInitialize()));

   } //if (OSAL_NULL != m_poMainAppl)
   //!Insert the  switch codes received from HMI and the corresponding SPI Key codes to map
   vInsertKeyCodeToMap();

   return bInit;
}//! end of bInitialize()

/**************************************************************************
** FUNCTION:  t_Bool spi_tclService::bUnInitialize();
**************************************************************************/
t_Bool spi_tclService::bUnInitialize()
{
   ETG_TRACE_USR1(("spi_tclService::bUnInitialize() entered"));

   t_Bool bUnInit = false;

   RELEASE_MEM_OSAL(m_poSPMClient);
   RELEASE_MEM_OSAL(m_poSensorDataClient);
   RELEASE_MEM_OSAL(m_poPosDataClient);
   RELEASE_MEM_OSAL(m_poTelephoneClient);
   RELEASE_MEM_OSAL(m_poAudioPolicy);
   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      bUnInit = m_poSpiCmdIntf->bUnInitialize();
   }
   m_poSpiCmdIntf = OSAL_NULL;

   return bUnInit;
}//! end of bUnInitialize()

/***************************************************************************
** FUNCTION:  t_Void spi_tclService::vLoadSettings()
***************************************************************************/
t_Void spi_tclService::vLoadSettings(const trSpiFeatureSupport& rfcrSpiFeatureSupp)
{
  /*lint -esym(40,fvOnTelephoneCallActivity)fvOnTelephoneCallActivity Undeclared identifier */
  /*lint -esym(40,_1)_1 Undeclared identifier */
   ETG_TRACE_USR1(("spi_tclService::vLoadSettings() entered"));

   //! Store DiPO feature support status
   m_bIsDiPOSupported = rfcrSpiFeatureSupp.bDipoSupported();
   
   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vLoadSettings(rfcrSpiFeatureSupp);
      m_poSpiCmdIntf->vSetLocDataAvailablility(true,false);
   }
   //! Register for Telephone events callbacks
   if (OSAL_NULL != m_poTelephoneClient)
   {
     trTelephoneCallbacks rTelephoneCallbacks;
     rTelephoneCallbacks.fvOnTelephoneCallActivity =
           std::bind(&spi_tclService::vOnTelephoneCallActivity,
                 this,
                 SPI_FUNC_PLACEHOLDERS_1);
     //@Note: Only overall call actitity info is needed here, not individual call info.
     m_poTelephoneClient->vRegisterCallbacks(rTelephoneCallbacks);
     ETG_TRACE_USR1(("spi_tclService::vLoadSettings exit"));
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclService::vSaveSettings()
***************************************************************************/
t_Void spi_tclService::vSaveSettings()
{
   ETG_TRACE_USR1(("spi_tclService::vSaveSettings() entered"));
   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vSaveSettings();
   }
}

/*******************************************************************************
 * \FUNCTION   : tVoid spi_tclService::vOnServiceAvailable()
 *******************************************************************************/
tVoid spi_tclService::vOnServiceAvailable(tVoid)
{
   ETG_TRACE_USR1(("spi_tclService::vOnServiceAvailable() entered"));

   //!Add your code here

} //!end of vOnServiceAvailable()

/*******************************************************************************
 * \FUNCTION   : tVoid spi_tclService::vOnServiceUnavailable()
 *******************************************************************************/
tVoid spi_tclService::vOnServiceUnavailable(tVoid)
{
   ETG_TRACE_USR1(("spi_tclService::vOnServiceUnavailable() entered"));

   //!Add your code here

} //!end of vOnServiceUnavailable()

/*******************************************************************************
 * \FUNCTION   : tBool spi_tclService::bStatusMessageFactory(tU16 u16FunctionId...
 *******************************************************************************/
tBool spi_tclService::bStatusMessageFactory(tU16 u16FunctionId,
         amt_tclServiceData& roOutMsg, amt_tclServiceData* poInMsg)
{
   (tVoid) poInMsg; // These lines are added to avoid LINT warnings. Please
   (tVoid) roOutMsg; // remove as soon as the variables are used.

   tBool bSuccess = TRUE; 

   ETG_TRACE_USR1(("pi_tclService::bStatusMessageFactory() entered with FunctionId = 0x%x.", u16FunctionId));

   switch (u16FunctionId)
   {
   case MIDW_SMARTPHONEINTFI_C_U16_DEVICESTATUSINFO:
      {
         midw_smartphoneintfi_tclMsgDeviceStatusInfoStatus oDeviceStatusInfo = m_enDevStatusInfo;

         ETG_TRACE_USR2(("[DESC]:bStatusMessageFactory with DEVICESTATUSINFO - " \
                  "u32DevHandle = %d enDevConnType = %d enDeviceStatus- %d",
                  m_enDevStatusInfo.DeviceHandle,
                  m_enDevStatusInfo.DeviceConnectionType.enType,
                  m_enDevStatusInfo.DeviceStatus.enType));
         //Hand over the message 
         bSuccess = fi_tclVisitorMessage(oDeviceStatusInfo,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_APPSTATUSINFO:
      {
         bSuccess = fi_tclVisitorMessage(m_AppInfoStatus,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_DAPSTATUSINFO:
      {
         bSuccess = fi_tclVisitorMessage(m_enDAPStatusInfo,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_NOTIFICATIONINFO:
      {
         bSuccess = fi_tclVisitorMessage(m_NotificationInfo,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_APPLICATIONMEDIAMETADATA:
      {
         bSuccess = fi_tclVisitorMessage(m_AppMediaMetaData,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_APPLICATIONPHONEDATA:
      {
         bSuccess = fi_tclVisitorMessage(m_AppPhoneData,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_MEDIAPLAYBACKTIME:
      {
         bSuccess = fi_tclVisitorMessage(m_AppMediaPlayBacktime,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_DEVICEDISPLAYCONTEXT:
      {
         bSuccess = fi_tclVisitorMessage(m_DeviceDispCntxt,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
/*   case MIDW_SMARTPHONEINTFI_C_U16_DATASERVICEINFO:
      {
         //Default implementation is provided to avoid Asserts.
         //@todo - revisit during Data service Implementation
         midw_smartphoneintfi_tclMsgDataServiceInfoStatus oDataServiceStatus;
         //Sending status update for Invalid device Id
         oDataServiceStatus.DeviceHandle = 0;
         oDataServiceStatus.DeviceCategory.enType = (midw_fi_tcl_e8_DeviceCategory::tenType)(e8DEV_TYPE_UNKNOWN);
         bSuccess = fi_tclVisitorMessage(oDataServiceStatus,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
*/
   case MIDW_SMARTPHONEINTFI_C_U16_BLUETOOTHDEVICESTATUS:
      {
         bSuccess = fi_tclVisitorMessage(m_BTDeviceStatusMsg,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
         CLEAR_BTDEVSTATUS_MSG(m_BTDeviceStatusMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_DEVICEAUDIOCONTEXT:
      {
          bSuccess = fi_tclVisitorMessage(m_DeviceAudioCntxt,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_DIPOAPPSTATUSINFO:
      {
         bSuccess = fi_tclVisitorMessage(m_DeviceAppState,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_SESSIONSTATUSINFO:
      {
         bSuccess = fi_tclVisitorMessage(m_SessionStatus,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_BTPAIRINGREQUIRED:
      {
         bSuccess = fi_tclVisitorMessage(m_BTPairingRequiredMsg,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
         CLEAR_BTPAIR_REQUIRED_MSG(m_BTPairingRequiredMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_MLSERVERKEYCAPABILITIES:
      {
         bSuccess = fi_tclVisitorMessage(m_MLSrvKeyCapabilities,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
      break;
   case MIDW_SMARTPHONEINTFI_C_U16_PROJECTIONDEVICEAUTHORIZATION:
      {
         bSuccess = fi_tclVisitorMessage(m_DevAuthStatus,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
      }
     break;
   case MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONSTATUSINFO:
         {
            bSuccess = fi_tclVisitorMessage(m_NavStatus,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
         }
         break;
   case MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONNEXTTURNDATA:
         {
            bSuccess = fi_tclVisitorMessage(m_NavNextTurnStatus,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
         }
         break;
   case MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONNEXTTURNDISTANCEDATA:
         {
            bSuccess = fi_tclVisitorMessage(m_NavNextTurnDistanceStatus,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
         }
         break;
   case MIDW_SMARTPHONEINTFI_C_U16_AAPNOTIFICATIONEVENT:
         {
            bSuccess = fi_tclVisitorMessage(m_NotificationEventStatus,SPI_SERVICE_FI_MAJOR_VERSION).bHandOver(&roOutMsg);
         }
         break;
   default:
      {
         ETG_TRACE_ERR(("[ERR]:bStatusMessageFactory() entered with Invalid FunctionId."));
         bSuccess = FALSE;
      }
      break;

   } //!switch (u16FunctionId)

   if (FALSE == bSuccess)
   {
      ETG_TRACE_FATAL(("[ERR]:bStatusMessageFactory() entered and Creation of message with 'FunctionId = 0x%x' failed.", u16FunctionId));
      NORMAL_M_ASSERT_ALWAYS();
   }

   ETG_TRACE_USR1(("bStatusMessageFactory() left with result: %u.", bSuccess));

   return bSuccess;

} /* end of bStatusMessageFactory()*/

/***************************************************************************
 ** FUNCTION:  tBool spi_tclService::bProcessSet(amt_tclServiceData* p...
 ***************************************************************************/
tBool spi_tclService::bProcessSet(amt_tclServiceData* poMessage, tBool& bPropertyChanged, tU16& u16Error)
{
   SPI_INTENTIONALLY_UNUSED(bPropertyChanged);
   SPI_INTENTIONALLY_UNUSED(u16Error);

   tU16 u16FunctionId = poMessage->u16GetFunctionID();
   ETG_TRACE_USR1(("spi_tclService::bProcessSet entered with FunctionID = 0x%4x ", u16FunctionId));

   tBool bMsgValid = FALSE;

   //! Call appropriate method in SPI to process the DevPrj Set message.
   switch (u16FunctionId)
   {
      case MIDW_SMARTPHONEINTFI_C_U16_PROJECTIONDEVICEAUTHORIZATION:
      {
         spi_tMsgSetDeviceAuth oXFiSetDevAuth(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);
         bMsgValid = oXFiSetDevAuth.bIsValid();
         if ((true == bMsgValid) && (OSAL_NULL != m_poSpiCmdIntf))
         {
            ETG_TRACE_USR2(("[DESC] bProcessSet Received Projection Device Authorization Device Handle = %d, Authorization Status = %d ",
                  oXFiSetDevAuth.DeviceHandle, ETG_ENUM(AUTHORIZATION_STATUS, oXFiSetDevAuth.UserAuthorizationStatus.enType)));
            tenUserAuthorizationStatus enUserAuthStatus = static_cast<tenUserAuthorizationStatus>  (oXFiSetDevAuth.UserAuthorizationStatus.enType);
            m_poSpiCmdIntf->vSetDeviceAuthorization(oXFiSetDevAuth.DeviceHandle, enUserAuthStatus);
            //! Clear the list once user accepts the popup
            m_DevAuthStatus.DeviceAuthInfoList.clear();
            m_DevAuthStatus.NumDevices = 0;
         } //if (OSAL_NULL != m_poSpiCmdInterface)
      }
         break;
      case MIDW_SMARTPHONEINTFI_C_U16_AAPNOTIFICATIONEVENT:
      {
         spi_tMsgSetAAPNotifEvent oXFiSetAAPNotf(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);
         bMsgValid = oXFiSetAAPNotf.bIsValid();
         if ((true == bMsgValid) && (OSAL_NULL != m_poSpiCmdIntf))
         {
            t_String szAAPNotifId;
            GET_STRINGDATA_FROM_FI_STRINGOBJ(oXFiSetAAPNotf.AAPNotificationId, midw_fi_tclString::FI_EN_UTF8, szAAPNotifId);
            ETG_TRACE_USR2(("[DESC] bProcessSet Received AAP NotificationId = %s",szAAPNotifId.c_str()));
            //Acknowledge the receipt of notification
            if (false == szAAPNotifId.empty())
            {
               trNotificationAckData rNotificationAckData;
           	   rNotificationAckData.u32DeviceHandle = m_NotificationEventStatus.DeviceHandle;
           	   rNotificationAckData.enDeviceCategory = static_cast<tenDeviceCategory>(m_NotificationEventStatus.DeviceCategory.enType);
           	   rNotificationAckData.szNotifId = szAAPNotifId;
               m_poSpiCmdIntf->vAckNotification(rNotificationAckData);
            }
         } //if (OSAL_NULL != m_poSpiCmdInterface)
      }
         break;


      default:
         ETG_TRACE_ERR(("[ERR]:bProcessSet() entered with Invalid FunctionID: 0x%x ", u16FunctionId));
         break;
   } //switch (u16FunctionId)
   return bMsgValid;
}

/*******************************************************************************
 * \ FUNCTION   : tVoid spi_tclService::vProcessTimer(tU16 u16TimerId)
 *******************************************************************************/
tVoid spi_tclService::vProcessTimer(tU16 u16TimerId)
{
   ETG_TRACE_USR1(("spi_tclService::vProcessTimer entered with TimerID = 0x%x.", u16TimerId));

   //!   Add your code here

   ETG_TRACE_USR1(("spi_tclService::vProcessTimer() left"));

} //! end of vProcessTimer()

/********************************************************************************************
 * \FUNCTION   :tVoid spi_tclService:: vOnMSGetDeviceInfoList(amt_tclServiceData* poMessage)
 *********************************************************************************************/
tVoid spi_tclService::vOnMSGetDeviceInfoList(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSGetDeviceInfoList() entered."));

   spi_tMsgGetDevInfoListMS oDevInfoListMS(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);

   if (
      (TRUE == oDevInfoListMS.bIsValid())
      && 
      (OSAL_NULL != m_poSpiCmdIntf)
      )
   {
      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      tU32 u32NumDevices = 0;
      std::vector<trDeviceInfo> vecrDeviceInfoList;

      m_poSpiCmdIntf->vGetDeviceInfoList((t_U32&)u32NumDevices, vecrDeviceInfoList);

      midw_smartphoneintfi_tclMsgGetDeviceInfoListMethodResult oMRDeviceInfoList;
      oMRDeviceInfoList.NumDevices = u32NumDevices;

      //@todo - find work around to avoid bpstl usage
      bpstl::vector<midw_fi_tcl_DeviceDetails> rvecDeviceInfo;
      vPopulateDeviceInfoList(rvecDeviceInfo,vecrDeviceInfoList);
      oMRDeviceInfoList.DeviceInfoList = rvecDeviceInfo ;
      ETG_TRACE_USR4(("[PARAM]::vOnMSGetDeviceInfoList - Num.Devices-%d",oMRDeviceInfoList.NumDevices));
      for( tU8 u8Index = 0; u8Index < oMRDeviceInfoList.DeviceInfoList.size(); ++u8Index)
      {
         ETG_TRACE_USR4(("[PARAM]::vOnMSGetDeviceInfoList - Dev-0x%x Name-%s ",
            oMRDeviceInfoList.DeviceInfoList[u8Index].u32DeviceHandle,
            oMRDeviceInfoList.DeviceInfoList[u8Index].szDeviceName.szValue));
      }

      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRDeviceInfoList,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSGetDeviceInfoList() left with Error in posting Message"));
      } //if( FALSE == oPostMsg.bSendMessage

      //! De-allocate memory used by message
      oMRDeviceInfoList.vDestroy();

   }//if(TRUE == oDevInfoListMS.bIsValid())
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclService::vPopulateDeviceInfoList()
***************************************************************************/
tVoid spi_tclService::vPopulateDeviceInfoList(bpstl::vector<midw_fi_tcl_DeviceDetails> &rfrvecDeviceInfo,
                                                std::vector<trDeviceInfo> vecrDeviceInfoList)
{
   ETG_TRACE_USR1(("spi_tclService::vPopulateDeviceInfoList() entered"));

   tU16 u16DeviceInfoListSize = vecrDeviceInfoList.size();
   //Clear the vector contents, if any
   if(0 < rfrvecDeviceInfo.size())
   {
      rfrvecDeviceInfo.clear();
   }

   for( tU16 u16Index = 0; u16Index < u16DeviceInfoListSize; ++u16Index)
   {
      midw_fi_tcl_DeviceDetails oDevicedetails;

      oDevicedetails.u32DeviceHandle = vecrDeviceInfoList[u16Index].u32DeviceHandle;
      oDevicedetails.szDeviceName.bSet(vecrDeviceInfoList[u16Index].szDeviceName.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oDevicedetails.enDeviceCategory.enType
            = (midw_fi_tcl_e8_DeviceCategory::tenType) (vecrDeviceInfoList[u16Index].enDeviceCategory);
      oDevicedetails.szDeviceModelName.bSet(vecrDeviceInfoList[u16Index].szDeviceModelName.c_str(),
            midw_fi_tclString::FI_EN_UTF8);
      oDevicedetails.szDeviceManufacturerName.bSet(vecrDeviceInfoList[u16Index].szDeviceManufacturerName.c_str(),
            midw_fi_tclString::FI_EN_UTF8);
      //oDevicedetails.enSessionStatus.enType
      //      = (midw_fi_tcl_e8_SessionStatus::tenType) (vecrDeviceInfoList[u16Index].enSessionStatus);
      oDevicedetails.enDeviceConnectionStatus.enType
            = (midw_fi_tcl_e8_DeviceConnectionStatus::tenType) (vecrDeviceInfoList[u16Index].enDeviceConnectionStatus);
      oDevicedetails.enDeviceConnectionType.enType
            = (midw_fi_tcl_e8_DeviceConnectionType::tenType) (vecrDeviceInfoList[u16Index].enDeviceConnectionType);
      //oDevicedetails.enSessionStatus.enType
      //      = (midw_fi_tcl_e8_SessionStatus::tenType) (vecrDeviceInfoList[u16Index].enSessionStatus);
      oDevicedetails.bDeviceUsageEnabled = vecrDeviceInfoList[u16Index].bDeviceUsageEnabled;
      oDevicedetails.bSelectedDevice = vecrDeviceInfoList[u16Index].bSelectedDevice;
      oDevicedetails.bDAPSupport = vecrDeviceInfoList[u16Index].bDAPSupport;
      oDevicedetails.szBTAddress.bSet(vecrDeviceInfoList[u16Index].szBTAddress.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oDevicedetails.rProjectionCapability.enDeviceType.enType =
               (midw_fi_tcl_e8_DeviceType::tenType) vecrDeviceInfoList[u16Index].rProjectionCapability.enDeviceType;

      ETG_TRACE_USR4(("spi_tclService::Device type for Device 0x%x is %d",  oDevicedetails.u32DeviceHandle,
               ETG_ENUM(DEVICE_TYPE, oDevicedetails.rProjectionCapability.enDeviceType.enType )));

      oDevicedetails.rProjectionCapability.enAndroidProjection.enType =
    		  (midw_fi_tcl_e8_SupportInfo::tenType)vecrDeviceInfoList[u16Index].rProjectionCapability.enAndroidAutoSupport;
      oDevicedetails.rProjectionCapability.enMirrorlinkProjection.enType =
    		  (midw_fi_tcl_e8_SupportInfo::tenType)vecrDeviceInfoList[u16Index].rProjectionCapability.enMirrorlinkSupport;
      oDevicedetails.rProjectionCapability.enCarplayProjection.enType =
      	  	  (midw_fi_tcl_e8_SupportInfo::tenType)vecrDeviceInfoList[u16Index].rProjectionCapability.enCarplaySupport;
      oDevicedetails.rProjectionCapability.enUSBPortType.enType =
    		  (midw_fi_tcl_e8_USBPortType::tenType)vecrDeviceInfoList[u16Index].rProjectionCapability.enUSBPortType;

      //Major,Minor,Patch version parameters are strings in the smartphoneintegrationfi and tU32 valules in SPITypes
      //@todo - needs to be modified
      StringHandler oStrHndlr;
      std::string szTemp;
      oStrHndlr.vConvertIntToStr(vecrDeviceInfoList[u16Index].rVersionInfo.u32MajorVersion,szTemp,DECIMAL_STRING);
      oDevicedetails.rVersionInfo.szMajorVersion.bSet(szTemp.c_str(), midw_fi_tclString::FI_EN_UTF8);

      oStrHndlr.vConvertIntToStr(vecrDeviceInfoList[u16Index].rVersionInfo.u32MinorVersion,szTemp,DECIMAL_STRING);
      oDevicedetails.rVersionInfo.szMinorVersion.bSet(szTemp.c_str(), midw_fi_tclString::FI_EN_UTF8);

      oStrHndlr.vConvertIntToStr(vecrDeviceInfoList[u16Index].rVersionInfo.u32PatchVersion,szTemp,DECIMAL_STRING);
      oDevicedetails.rVersionInfo.szPatchVersion.bSet(szTemp.c_str(), midw_fi_tclString::FI_EN_UTF8);

      //oDevicedetails.rServerCapabilities is not handled currently

      //Push the structure to the container
      rfrvecDeviceInfo.push_back(oDevicedetails);

   }//for( tU8 u8Index = 0; u8Index < u16DeviceInfoListSize; ++u8Index)
}

/********************************************************************************************
 * \FUNCTION   :tVoid vOnMSSelectDevice(amt_tclServiceData* poMessage)
 *********************************************************************************************/
tVoid spi_tclService::vOnMSSelectDevice(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSelectDevice() entered."));
   spi_tMsgSelectDeviceMS oSelectDeviceMS(*poMessage,
            SPI_SERVICE_FI_MAJOR_VERSION);
   if ((TRUE == oSelectDeviceMS.bIsValid()) && (OSAL_NULL != m_poSpiCmdIntf))
   {
      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      m_poSpiCmdIntf->vSelectDevice(oSelectDeviceMS.DeviceHandle,
               static_cast<tenDeviceConnectionType> (oSelectDeviceMS.DeviceConnectionType.enType),
               static_cast<tenDeviceConnectionReq> (oSelectDeviceMS.DeviceConnectionReq.enType),
               static_cast<tenEnabledInfo> (oSelectDeviceMS.DAPUsage.enType),
               static_cast<tenEnabledInfo> (oSelectDeviceMS.CDBUsage.enType),
               static_cast<tenDeviceCategory> (oSelectDeviceMS.DeviceCategory.enType),
               rMsgContext.rUserContext);

   }//if(TRUE == oSelectDeviceMS.bIsValid())
}

/********************************************************************************************
 * \FUNCTION   :tVoid vOnMSLaunchApp(amt_tclServiceData* poMessage)
 *********************************************************************************************/
tVoid spi_tclService::vOnMSLaunchApp(amt_tclServiceData* poMessage)
{
  ETG_TRACE_USR1(("spi_tclService::vOnMSLaunchApp() entered."));
  spi_tMsgLaunchAppMS oLaunchAppMS(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);
  if (
     (TRUE == oLaunchAppMS.bIsValid())
     && 
     (OSAL_NULL != m_poSpiCmdIntf)
     )
  {
     trUserContext rUsrContext;
     CPY_TO_USRCNTXT(poMessage, rUsrContext);

     m_poSpiCmdIntf->vLaunchApp(oLaunchAppMS.DeviceHandle,
        (tenDeviceCategory)(oLaunchAppMS.DeviceCategory.enType),
        oLaunchAppMS.AppHandle,
        (tenDiPOAppType)(oLaunchAppMS.DiPOAppType.enType),
        (t_String)(oLaunchAppMS.TelephoneNumber.szValue),
        (tenEcnrSetting)(oLaunchAppMS.EcnrSetting.enType),
        rUsrContext);
  }//if(TRUE == oLaunchAppMS.bIsValid())
}

/********************************************************************************************
 * \FUNCTION   :tVoid vOnMSTerminateApp(amt_tclServiceData* poMessage)
 *********************************************************************************************/
tVoid spi_tclService::vOnMSTerminateApp(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSTerminateApp() entered."));

   spi_tMsgTerminateAppMS oTerminateAppMS(*poMessage,
            SPI_SERVICE_FI_MAJOR_VERSION);
   if (
      (TRUE == oTerminateAppMS.bIsValid())
      && 
      (OSAL_NULL != m_poSpiCmdIntf)
      )
   {
      trUserContext rUsrContext;
      CPY_TO_USRCNTXT(poMessage, rUsrContext);

      m_poSpiCmdIntf->vTerminateApp(oTerminateAppMS.DeviceHandle,
               oTerminateAppMS.AppHandle,
               rUsrContext);
   }//if(TRUE == oTerminateAppMS.bIsValid())
}

/********************************************************************************************
 * \FUNCTION   :tVoid vOnMSGetAppList(amt_tclServiceData* poMessage)
 *********************************************************************************************/
tVoid spi_tclService::vOnMSGetAppList(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSGetAppList() entered."));

   spi_tMsgGetAppListMS oGetAppListMS(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);

   if (
      (TRUE == oGetAppListMS.bIsValid())
      && 
      (OSAL_NULL != m_poSpiCmdIntf)
      )
   {
      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      tU32 u32NumApps = 0;
      std::vector<trAppDetails> vecrAppDetailsList;

      m_poSpiCmdIntf->vGetAppList((t_U32)oGetAppListMS.DeviceHandle, (t_U32&)u32NumApps, vecrAppDetailsList);

      midw_smartphoneintfi_tclMsgGetAppListMethodResult oMRGetAppList;

      oMRGetAppList.DeviceHandle = oGetAppListMS.DeviceHandle;
      oMRGetAppList.NumAppDetailsList = u32NumApps;

      //@todo - find work around to avoid bpstl usage
      bpstl::vector<midw_fi_tcl_AppDetails> rvecAppDetailsList;
      vPopulateAppInfoList(oGetAppListMS.DeviceHandle,rvecAppDetailsList,vecrAppDetailsList);
      oMRGetAppList.AppDetailsList = rvecAppDetailsList;

      ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"vOnMSGetAppList received Device Handle = 0x%x supports %d UI Apps",
         oMRGetAppList.DeviceHandle,oMRGetAppList.NumAppDetailsList));

      for(tU16 u16Index=0; u16Index < oMRGetAppList.AppDetailsList.size() ; ++u16Index)
      {
         ETG_TRACE_USR1_CLS((TR_CLASS_SMARTPHONEINT_CTS,"vOnMSGetAppList received AppID= 0x%x ,Category=0x%x , Status=%d , AppCertStatus=%d ,AppName = %s"
            ,oMRGetAppList.AppDetailsList[u16Index].u32AppHandle
            ,oMRGetAppList.AppDetailsList[u16Index].enAppCategory.enType
            ,ETG_ENUM(APP_STATUS,oMRGetAppList.AppDetailsList[u16Index].enAppStatus.enType)
            ,ETG_ENUM(APP_CERTIFICATION_STATUS,oMRGetAppList.AppDetailsList[u16Index].e8AppCertificationInfo.enType)
            ,oMRGetAppList.AppDetailsList[u16Index].szAppName.szValue));
      }//for(tU16 u16Index=0; u16Index < oMRGetAppLis...

      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRGetAppList,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:spi_tclService::vOnMSGetAppList() left with Error in posting Message"));
      } //if( FALSE == oPostMsg.bSendMessage

      //! De-allocate memory used by message
      oMRGetAppList.vDestroy();

   }//if(TRUE == oGetAppListMS.bIsValid())
}


/***************************************************************************
** FUNCTION:  tVoid spi_tclService::vPopulateAppInfoList()
***************************************************************************/
tVoid spi_tclService::vPopulateAppInfoList(tU32 u32DevId,
                                             bpstl::vector<midw_fi_tcl_AppDetails>& rfvecAppDetailsList,
                                             std::vector<trAppDetails> vecrAppDetailsList) const
{
   ETG_TRACE_USR1(("spi_tclService::vPopulateAppInfoList() entered with Device Id = 0x%x",u32DevId));

   //Clear the vector contents, if any
   rfvecAppDetailsList.clear();

   for( tU16 u16Index = 0 ; u16Index < vecrAppDetailsList.size() ; ++u16Index )
   {
      midw_fi_tcl_AppDetails oAppDetails;

      //Application basic details.
      oAppDetails.u32AppHandle = vecrAppDetailsList[u16Index].u32AppHandle;
      oAppDetails.szAppName.bSet(vecrAppDetailsList[u16Index].szAppName.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.szAppVariant.bSet(vecrAppDetailsList[u16Index].szAppVariant.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.szAppProviderName.bSet(vecrAppDetailsList[u16Index].szAppProviderName.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.szAppProviderURL.bSet(vecrAppDetailsList[u16Index].szAppProviderURL.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.szAppDescription.bSet(vecrAppDetailsList[u16Index].szAppDescription.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.szAppCertificateURL.bSet(vecrAppDetailsList[u16Index].szAppCertificateURL.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.enAppCategory.enType = (midw_fi_tcl_e32_AppCategory::tenType)(vecrAppDetailsList[u16Index].enAppCategory);
      oAppDetails.enTrustLevel.enType = (midw_fi_tcl_e16_TrustLevel::tenType)(vecrAppDetailsList[u16Index].enTrustLevel);
      oAppDetails.enAppStatus.enType = (midw_fi_tcl_e8_AppStatus::tenType)(vecrAppDetailsList[u16Index].enAppStatus);

      //Copy App Display Info
      oAppDetails.rAppDisplayInfo.enAppDisplayCategory.enType = 
         (midw_fi_tcl_e32_AppDisplayCategory::tenType)(vecrAppDetailsList[u16Index].rAppDisplayInfo.enAppDisplayCategory);
      oAppDetails.rAppDisplayInfo.enTrustLevel.enType = 
         (midw_fi_tcl_e16_TrustLevel::tenType)(vecrAppDetailsList[u16Index].rAppDisplayInfo.enTrustLevel);
      oAppDetails.rAppDisplayInfo.u32AppDisplayRules = vecrAppDetailsList[u16Index].rAppDisplayInfo.u32AppDisplayRules;
      oAppDetails.rAppDisplayInfo.szAppDisplayOrientation.bSet(
         vecrAppDetailsList[u16Index].rAppDisplayInfo.szAppDisplayOrientation.c_str(), midw_fi_tclString::FI_EN_UTF8);


      //copy App Audio Info
      oAppDetails.rAppAudioInfo.szAppAudioType.bSet(
         vecrAppDetailsList[u16Index].rAppAudioInfo.szAppAudioType.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.rAppAudioInfo.enAppAudioCategory.enType = 
         (midw_fi_tcl_e32_AppAudioCategory::tenType)(vecrAppDetailsList[u16Index].rAppAudioInfo.enAppAudioCategory);
      oAppDetails.rAppAudioInfo.enTrustLevel.enType = 
         (midw_fi_tcl_e16_TrustLevel::tenType)(vecrAppDetailsList[u16Index].rAppAudioInfo.enTrustLevel);
      oAppDetails.rAppAudioInfo.u32AppAudioRules = vecrAppDetailsList[u16Index].rAppAudioInfo.u32AppAudioRules;

      //copy App Remoting Info
      oAppDetails.rAppRemotingInfo.szRemotingProtocolID.bSet(
         vecrAppDetailsList[u16Index].rAppRemotingInfo.szRemotingProtocolID.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.rAppRemotingInfo.szRemotingFormat.bSet(
         vecrAppDetailsList[u16Index].rAppRemotingInfo.szRemotingFormat.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.rAppRemotingInfo.szRemotingDirection.bSet(
         vecrAppDetailsList[u16Index].rAppRemotingInfo.szRemotingDirection.c_str(), midw_fi_tclString::FI_EN_UTF8);
      oAppDetails.rAppRemotingInfo.u32RemotingAudioIPL = vecrAppDetailsList[u16Index].rAppRemotingInfo.u32RemotingAudioIPL;
      oAppDetails.rAppRemotingInfo.u32RemotingAudioMPL = vecrAppDetailsList[u16Index].rAppRemotingInfo.u32RemotingAudioMPL;

      //copy Application certification type - Drive or Park certified
      oAppDetails.e8AppCertificationInfo.enType = 
         (midw_fi_tcl_e8_AppCertificationInfo::tenType)(vecrAppDetailsList[u16Index].enAppCertificationInfo);

      //copy App certification entity - CCC or CCC-Member
      oAppDetails.e8AppCertificationEntity.enType = 
         (midw_fi_tcl_e8_AppCertificationEntity::tenType)(vecrAppDetailsList[u16Index].enAppCertificationEntity);

      //copy Icons supported by the Application
      oAppDetails.u16NumAppIcons = vecrAppDetailsList[u16Index].u32NumAppIcons;
      for(tU8 u8Index=0;u8Index<vecrAppDetailsList[u16Index].tvecAppIconList.size();++u8Index)
      {
         midw_fi_tcl_IconAttributes oIconAttr;
         oIconAttr.u32IconWidth = vecrAppDetailsList[u16Index].tvecAppIconList[u8Index].u32IconWidth;
         oIconAttr.u32IconHeight = vecrAppDetailsList[u16Index].tvecAppIconList[u8Index].u32IconHeight;
         oIconAttr.u32IconDepth = vecrAppDetailsList[u16Index].tvecAppIconList[u8Index].u32IconDepth;
         oIconAttr.enIconMimeType.enType = 
         (midw_fi_tcl_e8_IconMimeType::tenType)(vecrAppDetailsList[u16Index].tvecAppIconList[u8Index].enIconMimeType);
         oIconAttr.szIconURL.bSet(
            vecrAppDetailsList[u16Index].tvecAppIconList[u8Index].szIconURL.c_str(), midw_fi_tclString::FI_EN_UTF8);

         oAppDetails.AppIconList.push_back(oIconAttr);
      }//for(tU8 u8Index=0;u8Index<vecrAppDetailsList[u16Index].tvecAppI

      rfvecAppDetailsList.push_back(oAppDetails);
   }//for( tU16 u16Index = 0 ; u16Index <
   
   
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSGetAppIconData()
***************************************************************************/
t_Void spi_tclService::vOnMSGetAppIconData(amt_tclServiceData* poMessage) const
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSGetAppIconData"));

   spi_tMsgGetAppIconDataMS oGetAppIconData(*poMessage,
      SPI_SERVICE_FI_MAJOR_VERSION);

   if (
      (TRUE == oGetAppIconData.bIsValid())
      && 
      (OSAL_NULL != m_poSpiCmdIntf)
      )
   {
      trUserContext rUsrContext;
      CPY_TO_USRCNTXT(poMessage, rUsrContext);

      t_String szIconUrl;
      szIconUrl.assign(oGetAppIconData.AppIconURL.szValue);

      m_poSpiCmdIntf->vGetAppIconData(szIconUrl,rUsrContext);

   }//if(TRUE == oGetAppIconData.bIsValid())
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetAppIconAttr()
***************************************************************************/
t_Void spi_tclService::vOnMSSetAppIconAttr(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetAppIconAttr() entered"));
   spi_tMsgSetAppIconAttrMS oSetAppIconAttr(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);

   if( (TRUE == oSetAppIconAttr.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf))
   {
      trUserContext rUsrContext;
      CPY_TO_USRCNTXT(poMessage, rUsrContext);

      trIconAttributes rIconAttr;
      rIconAttr.u32IconWidth = oSetAppIconAttr.IconAttributes.u32IconWidth;
      rIconAttr.u32IconHeight = oSetAppIconAttr.IconAttributes.u32IconHeight ;
      rIconAttr.u32IconDepth = oSetAppIconAttr.IconAttributes.u32IconDepth;
      rIconAttr.enIconMimeType = static_cast<tenIconMimeType>(oSetAppIconAttr.IconAttributes.enIconMimeType.enType);
      if(OSAL_NULL != oSetAppIconAttr.IconAttributes.szIconURL.szValue)
      {
         rIconAttr.szIconURL.assign(oSetAppIconAttr.IconAttributes.szIconURL.szValue);
      }//if(OSAL_NULL != oSetAppIconAttr.IconAttributes.szIconURL.szValue)

      m_poSpiCmdIntf->vSetAppIconAttributes(oSetAppIconAttr.DeviceHandle,
         oSetAppIconAttr.AppHandle,rIconAttr,rUsrContext);
      ETG_TRACE_USR1(("spi_tclService::vOnMSSetAppIconAttr() left"));
   }//if(TRUE == oSetAppiconAttr.bIsValid())
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetVehicleConfig()
***************************************************************************/
t_Void spi_tclService::vOnMSSetVehicleConfig(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetVehicleConfig() entered"));
   spi_tMsgSetVehicleConfigMS oSetVehicleConfig(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);

   if( (TRUE == oSetVehicleConfig.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      tenVehicleConfiguration enVehicleConfig = 
         static_cast<tenVehicleConfiguration>(oSetVehicleConfig.VehicleConfiguration.enType) ;
      m_poSpiCmdIntf->vSetVehicleConfig(enVehicleConfig,(t_Bool)oSetVehicleConfig.SetConfiguration,rUsrCntxt);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetVehicleConfigurationMethodResult oSetVehicleConfigMR;
      oSetVehicleConfigMR.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oSetVehicleConfigMR.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oSetVehicleConfigMR,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetVehicleConfig() left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oSetVehicleConfig.bIsValid())&
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetVehicleMovementState()
***************************************************************************/
t_Void spi_tclService::vOnMSSetVehicleMovementState(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetVehicleMovementState () entered"));
   spi_tMsgSetVehicleMovementMS oSetVehicleMovementState(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);

   if( (TRUE == oSetVehicleMovementState.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      ETG_TRACE_USR2(("[DESC]:vOnMSSetVehicleMovementState received Park brake = %d,"
         "Movement State = %d, and Gear = %d ", oSetVehicleMovementState.ParkBrakeInfo.enType,
         oSetVehicleMovementState.VehicleState.enType, oSetVehicleMovementState.GearInfo.enType));

      t_U8 u8Gear = 0;

      //Extracting park brake information
      m_rvehicledata.bParkBrakeActive = (midw_fi_tcl_e8_ParkBrake::FI_EN_PARK_BRAKE_ENGAGED == oSetVehicleMovementState.ParkBrakeInfo.enType)
         ? (true) : (false);

      //Extracting vehicle movement state information
      m_rvehicledata.enVehMovState = (midw_fi_tcl_e8_VehicleState::FI_EN_PARK_MODE == oSetVehicleMovementState.VehicleState.enType)
         ? (e8VEHICLE_MOVEMENT_STATE_PARKED) : (e8VEHICLE_MOVEMENT_STATE_FORWARD) ;

      //Extracting Gear information
      switch(oSetVehicleMovementState.GearInfo.enType)
      {
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_1 :
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_2 :
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_3 :
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_4 :
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_5 :
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_6 :
         u8Gear = static_cast<t_U8>(oSetVehicleMovementState.GearInfo.enType);
         m_rvehicledata.enGearPosition = static_cast<tenGearPosition>(u8Gear);
         break;
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_DRIVE :
         m_rvehicledata.enGearPosition = e8TRANS_EST_GEAR_POS_CVT_FORWARD_GEAR;
         break;
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_PARK  :
         m_rvehicledata.enGearPosition = e8TRANS_EST_GEAR_POS_PARK_GEAR;
         break;
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_REVERSE :
         m_rvehicledata.enGearPosition = e8TRANS_EST_GEAR_POS_REVERSE_GEAR;
         break;
      case midw_fi_tcl_e8_GearState::FI_EN_GEAR_NEUTRAL :
         m_rvehicledata.enGearPosition = e8TRANS_EST_GEAR_POS_NEUTRAL_GEAR;
         break;
      default :
         m_rvehicledata.enGearPosition = e8TRANS_EST_GEAR_POS_NOT_SUPPORTED;
         break;
      }

      ETG_TRACE_USR2(("[DESC]:vOnMSSetVehicleMovementState received values of Park brake = %d,"
         "Movement State = %d, Gear = %d after computation", m_rvehicledata.bParkBrakeActive, m_rvehicledata.enVehMovState,
         m_rvehicledata.enGearPosition));

      //sending data to command interface for further processing
      m_poSpiCmdIntf->vOnVehicleData(m_rvehicledata,false);

      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMessage, rMsgCntxt.rUserContext);

      midw_smartphoneintfi_tclMsgSetVehicleMovementStateMethodResult oMRInvokeVehicleMovementActionReq;
      oMRInvokeVehicleMovementActionReq.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRInvokeVehicleMovementActionReq.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRInvokeVehicleMovementActionReq, rMsgCntxt, SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetVehicleMovementState() left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage)
   }//if(TRUE == oSetVehicleMovementState.bIsValid())
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSVehicleMechanicalSpeed()
***************************************************************************/
t_Void spi_tclService::vOnMSVehicleMechanicalSpeed(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSVehicleMechanicalSpeed() Entered"));
   spi_tMsgVehicleMechanicalSpeedMS oSetVehicleMechanicalSpeed(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);

   if( (TRUE == oSetVehicleMechanicalSpeed.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      ETG_TRACE_USR2(("[DESC]: vOnMSVehicleMechanicalSpeed received value of Mechanical Speed = %d ", oSetVehicleMechanicalSpeed.VehicleSpeed));

      tenDeviceCategory enSelDevCat = m_poSpiCmdIntf->enGetDeviceCategory(m_poSpiCmdIntf->u32GetSelectedDeviceHandle());
      t_Bool bSolicited = (e8DEV_TYPE_ANDROIDAUTO == enSelDevCat)? (false) : (true);

      // Extracting the speed information
      m_rvehicledata.s16Speed = static_cast<t_S16>(oSetVehicleMechanicalSpeed.VehicleSpeed);
      m_rvehicledata.bSpeedAvailable = true;

      m_poSpiCmdIntf->vOnVehicleData(m_rvehicledata,bSolicited);

      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMessage, rMsgCntxt.rUserContext);

      midw_smartphoneintfi_tclMsgVehicleMechanicalSpeedMethodResult oMRInvokeVehicleMechanicalSpeedActionReq;
      oMRInvokeVehicleMechanicalSpeedActionReq.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRInvokeVehicleMechanicalSpeedActionReq.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRInvokeVehicleMechanicalSpeedActionReq, rMsgCntxt, SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSVehicleMechanicalSpeed() left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage)
   }
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetVideoBlockingMode()
***************************************************************************/
t_Void spi_tclService::vOnMSSetVideoBlockingMode(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetVideoBlockingMode () entered"));
   spi_tMsgSetVideoBlockingModeMS oSetVideoBlockingMode(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSetVideoBlockingMode.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      m_poSpiCmdIntf->vSetVideoBlockingMode(oSetVideoBlockingMode.DeviceHandle,
         static_cast<tenBlockingMode>(oSetVideoBlockingMode.BlockingMode.enType),rUsrCntxt);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetVideoBlockingModeMethodResult oSetVideoBlockingMR;
      oSetVideoBlockingMR.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oSetVideoBlockingMR.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oSetVideoBlockingMR,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetVehicleConfig() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage


   }//if( (TRUE == oSetVideoBlockingMode.bIsValid())&
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetAudioBlockingMode()
***************************************************************************/
t_Void spi_tclService::vOnMSSetAudioBlockingMode(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetAudioBlockingMode() entered"));
   spi_tMsgSetAudioBlockingModeMS oSetAudioBlockingMode(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);

   if( (TRUE == oSetAudioBlockingMode.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMessage, rMsgCntxt.rUserContext);

      t_Bool bResult = m_poSpiCmdIntf->bSetAudioBlockingMode(oSetAudioBlockingMode.DeviceHandle,
         static_cast<tenBlockingMode>(oSetAudioBlockingMode.BlockingMode.enType));

      midw_smartphoneintfi_tclMsgSetAudioBlockingModeMethodResult oMRSetAudioBlocking;
      oMRSetAudioBlocking.ResponseCode.enType = bResult?(midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS):(midw_fi_tcl_e8_ResponseCode::FI_EN_FAILURE);
      oMRSetAudioBlocking.ErrorCode.enType = bResult?(midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR):(midw_fi_tcl_e8_ErrorType::FI_EN_UNKNOWN_ERROR);

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRSetAudioBlocking,rMsgCntxt,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetAudioBlockingMode() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oMRSetAudioBlocking.bIsValid())&
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSInvokeBTDeviceAction()
***************************************************************************/
t_Void spi_tclService::vOnMSInvokeBTDeviceAction(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSInvokeBTDeviceAction() entered"));

   spi_tMsgInvokeBTDeviceActionMS oInvokeBTDevAction(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);

   if( (TRUE == oInvokeBTDevAction.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      m_poSpiCmdIntf->vInvokeBluetoothDeviceAction(
            oInvokeBTDevAction.BluetoothDeviceHandle,
            oInvokeBTDevAction.ProjectionDeviceHandle,
            static_cast<tenBTChangeInfo>(oInvokeBTDevAction.BTChangeInfo.enType));

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgInvokeBluetoothDeviceActionMethodResult oMRInvokeBTDeviceActionReq;
      oMRInvokeBTDeviceActionReq.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRInvokeBTDeviceActionReq.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRInvokeBTDeviceActionReq,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSInvokeBTDeviceAction() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oInvokeBTDevAction.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf))
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetOrientationMode()
***************************************************************************/
t_Void spi_tclService::vOnMSSetOrientationMode(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetOrientationMode() entered"));

   spi_tMsgSetOrientationModeMS oSetOrientationMode(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);

   if( (TRUE == oSetOrientationMode.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      m_poSpiCmdIntf->vSetOrientationMode(oSetOrientationMode.DeviceHandle,
         static_cast<tenOrientationMode>(oSetOrientationMode.OrientationMode.enType),rUsrCntxt);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetOrientationModeMethodResult oMRSetOrientationMode;
      oMRSetOrientationMode.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRSetOrientationMode.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRSetOrientationMode,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("ERR]:vOnMSSetOrientationMode() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oSetOrientationMode.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetScreenSize()
***************************************************************************/
t_Void spi_tclService::vOnMSSetScreenSize(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetScreenSize() entered"));
   
   spi_tMsgSetScreenSizeMS oSetScreenSize(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSetScreenSize.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      trScreenAttributes rScreenAttributes;
      rScreenAttributes.u32ScreenHeight = oSetScreenSize.ScreenAttributes.u16ScreenHeight;
      rScreenAttributes.u32ScreenWidth = oSetScreenSize.ScreenAttributes.u16ScreenWidth;
      rScreenAttributes.enScreenAspectRatio = 
         static_cast<tenScreenAspectRatio>(oSetScreenSize.ScreenAttributes.enScreenAspectRatio.enType);

      m_poSpiCmdIntf->vSetScreenSize(rScreenAttributes,rUsrCntxt);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetScreenSizeMethodResult oMRSetScreenSizeReq;
      oMRSetScreenSizeReq.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRSetScreenSizeReq.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRSetScreenSizeReq,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetScreenSize() Left With Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oSetScreenSize.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSGetVideoSettings()
***************************************************************************/
t_Void spi_tclService::vOnMSGetVideoSettings(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSGetVideoSettings() entered"));
   spi_tMsgGetVideoSettingsMS oGetVideoSettings(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oGetVideoSettings.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMessage, rMsgCntxt.rUserContext);

      trVideoAttributes rVideoAttr;
      m_poSpiCmdIntf->vGetVideoSettings(oGetVideoSettings.DeviceHandle,rVideoAttr);

      midw_smartphoneintfi_tclMsgGetVideoSettingsMethodResult oMRGetVideoSettings;
      oMRGetVideoSettings.DeviceHandle = oGetVideoSettings.DeviceHandle;
      oMRGetVideoSettings.VideoAttributes.enOrientationMode.enType = 
         static_cast<midw_fi_tcl_e8_OrientationMode::tenType>(rVideoAttr.enOrientationMode);
      oMRGetVideoSettings.VideoAttributes.rScreenAttributes.u16ScreenHeight = (tU16)(rVideoAttr.rScreenAttributes.u32ScreenHeight);
      oMRGetVideoSettings.VideoAttributes.rScreenAttributes.u16ScreenWidth = (tU16)(rVideoAttr.rScreenAttributes.u32ScreenWidth);
      oMRGetVideoSettings.VideoAttributes.rScreenAttributes.enScreenAspectRatio.enType = 
         static_cast<midw_fi_tcl_e8_ScreenAspectRatio::tenType>(rVideoAttr.rScreenAttributes.enScreenAspectRatio);

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRGetVideoSettings,rMsgCntxt,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSGetVideoSettings() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oGetVideoSettingsMS.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )

}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetDeviceUsagePreference()
***************************************************************************/
t_Void spi_tclService::vOnMSSetDeviceUsagePreference(amt_tclServiceData* poMessage)
{
      ETG_TRACE_USR1(("spi_tclService::vOnMSSetDeviceUsagePreference() entered"));
   spi_tMsgSetDeviceUsagePreferenceMS oSetDeviceUsagePreference(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSetDeviceUsagePreference.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      m_poSpiCmdIntf->vSetDeviceUsagePreference(oSetDeviceUsagePreference.DeviceHandle,
         (tenDeviceCategory)oSetDeviceUsagePreference.DeviceCategory.enType,
         (tenEnabledInfo)oSetDeviceUsagePreference.EnabledInfo.enType,rUsrCntxt);
   }//if( (TRUE == oSetDeviceUsagePreference.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )

}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnGetDeviceUsagePreferenceMS()
***************************************************************************/
t_Void spi_tclService::vOnMSGetDeviceUsagePreference(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSGetDeviceUsagePreference() entered"));
   spi_tMsgGetDeviceUsagePreferenceMS oGetDeviceUsagePreference(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oGetDeviceUsagePreference.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trMsgContext rMsgCntxt;
      CPY_TO_USRCNTXT(poMessage, rMsgCntxt.rUserContext);

      tenEnabledInfo enEnabledInfo=e8USAGE_DISABLED;
      m_poSpiCmdIntf->bGetDeviceUsagePreference(oGetDeviceUsagePreference.DeviceHandle,
         (tenDeviceCategory)oGetDeviceUsagePreference.DeviceCategory.enType,enEnabledInfo);

      midw_smartphoneintfi_tclMsgGetDeviceUsagePreferenceMethodResult oMRGetDeviceUsagePreference;
      oMRGetDeviceUsagePreference.DeviceHandle = oGetDeviceUsagePreference.DeviceHandle;
      oMRGetDeviceUsagePreference.DeviceCategory.enType = oGetDeviceUsagePreference.DeviceCategory.enType;
      oMRGetDeviceUsagePreference.EnabledInfo.enType = static_cast<midw_fi_tcl_e8_EnabledInfo::tenType>(enEnabledInfo);

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRGetDeviceUsagePreference,rMsgCntxt,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSGetDeviceUsagePreference()Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oGetDeviceUsagePreference.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )

}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSAccessoryDisplayContext()
***************************************************************************/
t_Void spi_tclService::vOnMSAccessoryDisplayContext(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSAccessoryDisplayContext() entered"));
   spi_tMsgAccDispCntxtMS oAccDispCntxt(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oAccDispCntxt.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      m_poSpiCmdIntf->vSetAccessoryDisplayContext(oAccDispCntxt.DeviceHandle,(t_Bool)oAccDispCntxt.DisplayFlag,
         (tenDisplayContext)oAccDispCntxt.DisplayContext.enType,rUsrCntxt);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgAccessoryDisplayContextMethodResult oMRAccessoryDisplayContextReq;
      oMRAccessoryDisplayContextReq.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRAccessoryDisplayContextReq.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRAccessoryDisplayContextReq,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSAccessoryDisplayContext() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage


   }//if( (TRUE == oMRAccessoryDisplayContextReq.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetAccessoryDisplayContext()
***************************************************************************/
t_Void spi_tclService::vOnMSSetAccessoryDisplayContext(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetAccessoryDisplayContext() entered"));
   spi_tMsgSetAccDisplayContextReqMS oSetAccDispCntxt(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSetAccDispCntxt.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trDisplayContext rDisplayContext;
      rDisplayContext.enDisplayContext = (tenDisplayContext)oSetAccDispCntxt.DisplayContextInfo.DisplayContext.enType;
      rDisplayContext.bDisplayFlag = (t_Bool)oSetAccDispCntxt.DisplayContextInfo.DisplayFlag;

      trDisplayConstraint rDisplayConstraint;
      rDisplayConstraint.enTransferType = (tenDiPOTransferType)oSetAccDispCntxt.DisplayConstraintInfo.TransferType.enType;
      rDisplayConstraint.enTransferPriority = (tenDiPOTransferPriority)oSetAccDispCntxt.DisplayConstraintInfo.TransferPriority.enType;
      rDisplayConstraint.enTakeConstraint = (tenDiPOConstraint)oSetAccDispCntxt.DisplayConstraintInfo.TakeConstraint.enType;
      rDisplayConstraint.enBorrowConstraint = (tenDiPOConstraint)oSetAccDispCntxt.DisplayConstraintInfo.BorrowConstraint.enType;

      m_poSpiCmdIntf->vSetAccessoryDisplayMode(oSetAccDispCntxt.DeviceHandle,
         rDisplayContext,
         rDisplayConstraint,
         (tenDisplayInfo)oSetAccDispCntxt.ContextResponsibility.enType);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetAccessoryDisplayContextMethodResult oSetAccDispCntxtMR;
      oSetAccDispCntxtMR.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oSetAccDispCntxtMR.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oSetAccDispCntxtMR,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetAccessoryDisplayContext() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage
   }
   else
   {
      ETG_TRACE_ERR(("[ERR]:spi_tclService::vOnMSSetAccessoryDisplayContext()Left with Message extraction failed"));
   }
   
}


/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSAccessoryAudioContext()
***************************************************************************/
t_Void spi_tclService::vOnMSAccessoryAudioContext(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSAccessoryAudioContext() entered"));
 
   spi_tMsgAccAudioCntxtMS oAccAudioCntxt(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oAccAudioCntxt.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      m_poSpiCmdIntf->vSetAccessoryAudioContext(oAccAudioCntxt.DeviceHandle,
               (tenAudioContext) oAccAudioCntxt.AudioContext.enType,
               (t_Bool) oAccAudioCntxt.AudioFlag, rUsrCntxt, e8DEV_TYPE_DIPO);

      m_poSpiCmdIntf->vSetAccessoryAudioContext(oAccAudioCntxt.DeviceHandle,
               (tenAudioContext) oAccAudioCntxt.AudioContext.enType,
               (t_Bool) oAccAudioCntxt.AudioFlag, rUsrCntxt, e8DEV_TYPE_ANDROIDAUTO);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgAccessoryAudioContextMethodResult
               oMRAccessoryAudioContextReq;
      oMRAccessoryAudioContextReq.ResponseCode.enType
               = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRAccessoryAudioContextReq.ErrorCode.enType
               = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

       //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if (FALSE == oPostMsg.bSendResMessage(oMRAccessoryAudioContextReq, rMsgContext,
               SPI_SERVICE_FI_MAJOR_VERSION))
       {
          ETG_TRACE_ERR(("[ERR]:vOnMSAccessoryAudioContext left with Error in posting Method Result"));
       } //if( FALSE == oPostMsg.bSendMessage
   }//if( (TRUE == oAccDispCntxt.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSAccessoryAppState()
***************************************************************************/
t_Void spi_tclService::vOnMSAccessoryAppState(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSAccessoryAppState() entered"));

   spi_tMsgAccAppStateMS oAccAppstate(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oAccAppstate.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);
      m_poSpiCmdIntf->vSetAccessoryAppState((tenSpeechAppState)oAccAppstate.AppStateSpeech.enType, 
         (tenPhoneAppState)oAccAppstate.AppStatePhone.enType, (tenNavAppState)oAccAppstate.AppStateNavigation.enType, rUsrCntxt);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgAccessoryAppStateMethodResult oMRAccessoryAppStateReq;
      oMRAccessoryAppStateReq.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRAccessoryAppStateReq.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRAccessoryAppStateReq,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSAccessoryAppState left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage )
   }//if( (TRUE == oAccAudioCntxt.bIsValid())&&(OSAL_NULL != m_pSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetClientCapabilities()
***************************************************************************/
t_Void spi_tclService::vOnMSSetClientCapabilities(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetClientCapabilities() entered"));

   spi_tMsgSetClientCapabilitiesMS oSetClientCapabilities(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSetClientCapabilities.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      trClientCapabilities rClntCapabilities ;
      rClntCapabilities.rKeyCapabilities.u32KnobKeySupport = 
         oSetClientCapabilities.ClientCapabilities.rKeyCapabilities.u32KnobKeySupport;

      rClntCapabilities.rKeyCapabilities.u32DeviceKeySupport  = 
         oSetClientCapabilities.ClientCapabilities.rKeyCapabilities.u32DeviceKeySupport;

      rClntCapabilities.rKeyCapabilities.u32MultimediaKeySupport  = 
         oSetClientCapabilities.ClientCapabilities.rKeyCapabilities.u32MultimediaKeySupport;

      rClntCapabilities.rKeyCapabilities.u32MiscKeySupport = 
         oSetClientCapabilities.ClientCapabilities.rKeyCapabilities.u32MiscKeySupport;

      rClntCapabilities.rKeyCapabilities.u32PointerTouchSupport = 
         oSetClientCapabilities.ClientCapabilities.rKeyCapabilities.u32PointerTouchSupport;

      //implement method in cmdintf && Send the request
      m_poSpiCmdIntf->vSetClientCapabilities(rClntCapabilities,rUsrCntxt);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetClientCapabilitiesMethodResult oMRSetClientCapabilitiesReq;
      oMRSetClientCapabilitiesReq.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRSetClientCapabilitiesReq.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRSetClientCapabilitiesReq,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetClientCapabilities Left With Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oSetClientCapabilities.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetMLNotificationEnabledInfo()
***************************************************************************/
t_Void spi_tclService::vOnMSSetMLNotificationEnabledInfo(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetMLNotificationEnabledInfo() entered"));
   spi_tMsgSetSetMLNotiEnabledInfoMS oSetMLNotiEnabledInfo(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSetMLNotiEnabledInfo.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      tU32 u32ListSize = oSetMLNotiEnabledInfo.NotificationEnableList.size();
      std::vector<trNotiEnable> vecrNotificationEnableList;
      for(tU32 u32Index=0;u32Index<u32ListSize;++u32Index)
      {
         trNotiEnable rNotiEnable;
         rNotiEnable.u32AppHandle = oSetMLNotiEnabledInfo.NotificationEnableList[u32Index].u32AppHandle;
         rNotiEnable.enEnabledInfo = (tenEnabledInfo)oSetMLNotiEnabledInfo.NotificationEnableList[u32Index].enEnabledInfo.enType;
         vecrNotificationEnableList.push_back(rNotiEnable);
      }//for(tU32 u32Index;u32Index<u32ListSize;++u32Index)

      m_poSpiCmdIntf->vSetMLNotificationEnabledInfo(oSetMLNotiEnabledInfo.DeviceHandle,
         oSetMLNotiEnabledInfo.NumNotificationEnableList,
         vecrNotificationEnableList,
         rUsrCntxt);

   }//if( (TRUE == oSetMLNotiEnabledInfo.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )

}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSInvokeNotificationAction()
***************************************************************************/
t_Void spi_tclService::vOnMSInvokeNotificationAction(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSInvokeNotificationAction() emtered"));
   spi_tMsgInvokeNotiAction oInvokeNotiAction(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oInvokeNotiAction.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      ETG_TRACE_USR2(("[DESC]:vInvokeNotificationAction received values of Dev-0x%x NotiID-0x%x and  ActionID-0x%x ",
         oInvokeNotiAction.DeviceHandle,oInvokeNotiAction.NotificationID,oInvokeNotiAction.NotificationActionID));

      m_poSpiCmdIntf->vInvokeNotificationAction(oInvokeNotiAction.DeviceHandle,oInvokeNotiAction.AppHandle,
      oInvokeNotiAction.NotificationID,oInvokeNotiAction.NotificationActionID,rUsrCntxt);
   }//if( (TRUE == oInvokeNotiAction.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSendTouchEvent()
***************************************************************************/
t_Void spi_tclService::vOnMSSendTouchEvent(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSendTouchEvent() entered"));
   spi_tMsgSetSendTouchEventMS oSendTouchEvent(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSendTouchEvent.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      trTouchData rTouchData;
      rTouchData.u32TouchDescriptors = oSendTouchEvent.TouchData.u16TouchDescriptors ;

      tU32 u32TouchInfoListSize = oSendTouchEvent.TouchData.TouchInfoList.size();
      for(tU32 u32Index=0;u32Index<u32TouchInfoListSize;++u32Index)
      {
         trTouchInfo rTouchInfo;
         //u32TimeStamp & u32Pressure to be added in SPI FI

         tU32 u32TouchCoordinatesListLen = oSendTouchEvent.TouchData.TouchInfoList[u32Index].TouchCoordinatesList.size();
         for(tU32 u32Ind=0;u32Ind<u32TouchCoordinatesListLen;++u32Ind)
         {
            trTouchCoordinates rTouchCoordinates;

            rTouchCoordinates.s32XCoordinate = 
               oSendTouchEvent.TouchData.TouchInfoList[u32Index].TouchCoordinatesList[u32Ind].u16XCoordinate;

            rTouchCoordinates.s32YCoordinate = 
               oSendTouchEvent.TouchData.TouchInfoList[u32Index].TouchCoordinatesList[u32Ind].u16YCoordinate;

            rTouchCoordinates.enTouchMode = 
               (tenTouchMode)oSendTouchEvent.TouchData.TouchInfoList[u32Index].TouchCoordinatesList[u32Ind].enTouchMode.enType;

            //u8Identifier to be added in SPI FI
            rTouchInfo.tvecTouchCoordinatesList.push_back(rTouchCoordinates);

         }//for(tU32 u32Ind;u32Ind<u32TouchCoordinatesListLen;++u32Ind)

         rTouchData.tvecTouchInfoList.push_back(rTouchInfo);

      }//for(tU32 u32Index;u32Index<u32TouchInfoListSize;++u32Index)

      m_poSpiCmdIntf->vSendTouchEvent(oSendTouchEvent.DeviceHandle,
         rTouchData,rUsrCntxt);

      //currently send the response from here only. Once it is implemented, response
      //should be called from Response Interface & remove the call from here
      vPostSendTouchEvent(e8FAILURE,e8OPERATION_REJECTED,rUsrCntxt);

   }//if( (TRUE == oSendTouchEvent.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSendKeyEvent()
***************************************************************************/
t_Void spi_tclService::vOnMSSendKeyEvent(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSendKeyEvent() entered"));
   spi_tMsgSendKeyEventMS oSendKeyEvent(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSendKeyEvent.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      ETG_TRACE_USR2(("[DESC]:vOnMSSendKeyEvent() received KeyCode-%d and  keyMode-%d",ETG_ENUM(KEY_CODE,oSendKeyEvent.KeyCode.enType),
         ETG_ENUM(KEY_MODE,oSendKeyEvent.KeyMode.enType)));

      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      tenKeyCode enKeyCode = enGetKeyCode(oSendKeyEvent.KeyCode.enType);
      tenKeyMode enKeyMode = static_cast<tenKeyMode>(oSendKeyEvent.KeyMode.enType);

      tenResponseCode enRespCode = e8SUCCESS;
      tenErrorCode enErrorCode = e8NO_ERRORS;
      if(e32INVALID_KEY != enKeyCode)
      {
         m_poSpiCmdIntf->vSendKeyEvent(oSendKeyEvent.DeviceHandle,enKeyMode,
            enKeyCode,rUsrCntxt);
      }
      else
      {
         enRespCode = e8FAILURE;
         enErrorCode = e8UNSUPPORTED_KEY;
      }


      //currently send the response from here only. Once it is implemented, response
      //should be called from Response Interface & remove the call from here
      vPostSendKeyEvent(enRespCode,enErrorCode,rUsrCntxt);
   }//if( (TRUE == oSendKeyEvent.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}


/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetVehicleBTAddress()
***************************************************************************/
t_Void spi_tclService::vOnMSSetVehicleBTAddress(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetVehicleBTAddress() entered"));
   
   spi_tMsgSetVehicleBTAddressMS oSetVehicleBTAddress(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSetVehicleBTAddress.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      t_String szBTAddress;
      szBTAddress.assign(oSetVehicleBTAddress.BTAddress.szValue);
      m_poSpiCmdIntf->vSetVehicleBTAddress(szBTAddress,rUsrCntxt);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetVehicleBTAddressMethodResult oMRSetVehicleBTAddress;
      oMRSetVehicleBTAddress.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRSetVehicleBTAddress.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRSetVehicleBTAddress,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetVehicleBTAddress() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage
   }//if( (TRUE == oSetVehicleBTAddress.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSetRegion()
***************************************************************************/
t_Void spi_tclService::vOnMSSetRegion(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetRegion() entered"));
   spi_tMsgSetRegionMS oSetRegion(*poMessage,SPI_SERVICE_FI_MAJOR_VERSION);
   if( (TRUE == oSetRegion.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
   {
      tenRegion enRegion = static_cast<tenRegion>(oSetRegion.Region.enType);
      m_poSpiCmdIntf->vSetRegion(enRegion);

     trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetRegionMethodResult oMRSetRegionReq;
      oMRSetRegionReq.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRSetRegionReq.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRSetRegionReq,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetVehicleBTAddress() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage
  
   }//if( (TRUE == oSetVehicleBTAddress.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/***************************************************************************
** FUNCTION: tVoid spi_tclService::vOnMSDiPoSwitchRequired
***************************************************************************/
tVoid spi_tclService::vOnMSDiPoSwitchRequired(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSDiPoSwitchRequired() entered"));

   t_U32 u32DeviceHandle = 0; 
   t_Bool bRoleSwitchReq = false;

   trMsgContext rMsgContext;
   CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

   spi_tMsgRoleSwitchReqMS oRoleSwitchReq(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);

   if ((TRUE == oRoleSwitchReq.bIsValid()) && (OSAL_NULL != m_poSpiCmdIntf))
   {
      u32DeviceHandle = oRoleSwitchReq.u8DeviceTag;
       // This code is not used currently. Now for all requests SPI will return a true.
      // Code is kept for future reference.
      if (true == m_bIsDiPOSupported)
      {
         bRoleSwitchReq = m_poSpiCmdIntf->bIsDiPoRoleSwitchRequired(u32DeviceHandle, rMsgContext.rUserContext);
      }



   }//if( (TRUE == oSetVehicleBTAddress.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )

}


/**************************************************************************
 ** FUNCTION   : tVoid spi_tclService:: vOnMSSetAccessoryAudioContext()
 ***************************************************************************/
t_Void spi_tclService::vOnMSSetAccessoryAudioContext(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetAccessoryAudioContext() entered"));

   spi_tMsgSetAccAudioCntxtMS oSetAccAudioCntxt(*poMessage,
            SPI_SERVICE_FI_MAJOR_VERSION);
   if ((TRUE == oSetAccAudioCntxt.bIsValid()) && (OSAL_NULL != m_poSpiCmdIntf))
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);
      std::vector<trAudioContext> vecrAudioContextInfo;
      tU32 u32ListSize = oSetAccAudioCntxt.AudioContextInfo.size();
      for (tU32 u32Index = 0; u32Index < u32ListSize; ++u32Index)
      {
         trAudioContext rAudContext;
         rAudContext.AudioContext = (tenAudioContext) oSetAccAudioCntxt.AudioContextInfo[u32Index].AudioContext.enType;
         rAudContext.AudioFlag   = oSetAccAudioCntxt.AudioContextInfo[u32Index].AudioFlag;
         m_poSpiCmdIntf->vSetAccessoryAudioContext(oSetAccAudioCntxt.DeviceHandle,
                  rAudContext.AudioContext, rAudContext.AudioFlag, rUsrCntxt, e8DEV_TYPE_DIPO);
         m_poSpiCmdIntf->vSetAccessoryAudioContext(oSetAccAudioCntxt.DeviceHandle,
                  rAudContext.AudioContext, rAudContext.AudioFlag, rUsrCntxt, e8DEV_TYPE_ANDROIDAUTO);
      }

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetAccessoryAudioContextMethodResult
               oMRSetAccessoryAudioContextReq;
      oMRSetAccessoryAudioContextReq.ResponseCode.enType
               = midw_fi_tcl_e8_ResponseCode::FI_EN_FAILURE;
      oMRSetAccessoryAudioContextReq.ErrorCode.enType
               = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if (FALSE == oPostMsg.bSendResMessage(oMRSetAccessoryAudioContextReq,
               rMsgContext,
               SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetAccessoryAudioContext() Left with Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage

   }//if( (TRUE == oAccDispCntxt.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf) )
}

/**************************************************************************
 ** FUNCTION   : tVoid spi_tclService:: vOnMSSetFeatureRestrictions()
 ***************************************************************************/
t_Void spi_tclService::vOnMSSetFeatureRestrictions(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetFeatureRestrictions() entered "));

   spi_tMsgSetFeatureRestrictionsMS oSetFeatureRestrictions(*poMessage,
            SPI_SERVICE_FI_MAJOR_VERSION);
   if ((TRUE == oSetFeatureRestrictions.bIsValid()) && (OSAL_NULL
            != m_poSpiCmdIntf))
   {
      trUserContext rUsrCntxt;
      CPY_TO_USRCNTXT(poMessage, rUsrCntxt);

      ETG_TRACE_USR1(("[PARAM]::vOnMSSetFeatureRestrictions() Device Category = %d, Park Mode Restriction = %d, Drive Mode Restriction = %d",
            ETG_ENUM( DEVICE_CATEGORY, oSetFeatureRestrictions.DeviceCategory.enType),
            oSetFeatureRestrictions.ParkModeRestrictionInfo.FeatureLockout,
            oSetFeatureRestrictions.DriveModeRestrictionInfo.FeatureLockout));

      //send restriction trigger to command interface
      m_poSpiCmdIntf->vSetFeatureRestrictions((tenDeviceCategory) oSetFeatureRestrictions.DeviceCategory.enType,
            (t_U8) oSetFeatureRestrictions.ParkModeRestrictionInfo.FeatureLockout,
            (t_U8) oSetFeatureRestrictions.DriveModeRestrictionInfo.FeatureLockout);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgSetFeatureRestrictionsMethodResult
               oFeatureRestrictionsMR;
      oFeatureRestrictionsMR.ResponseCode.enType
               = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oFeatureRestrictionsMR.ErrorCode.enType
               = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if (FALSE == oPostMsg.bSendResMessage(oFeatureRestrictionsMR,
               rMsgContext, SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("[ERR]:vOnMSSetFeatureRestrictions() Left with Error in posting Method Result"));
      }
   }
}


/**************************************************************************
 ** FUNCTION   : tVoid spi_tclService:: vOnMSSetTechnologyPreference()
 ***************************************************************************/
t_Void spi_tclService::vOnMSSetTechnologyPreference(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetTechnologyPreference() entered "));
   spi_tMsgSetTechnologyPrefMS oSetTechPref(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);
   t_U32 u32DeviceHandle = oSetTechPref.DeviceHandle;
   t_U32 u32NoofPreferences = oSetTechPref.NumofPreferences;
   std::vector<tenDeviceCategory> vecTechPreference;
   //! Copy the list to std::vector
   for(tU16 u16Index = 0; u16Index < oSetTechPref.PreferenceOrderList.size(); u16Index++)
   {
      tenDeviceCategory enTechnologyPref = static_cast<tenDeviceCategory> (oSetTechPref.PreferenceOrderList[u16Index].enType);
      vecTechPreference.push_back(enTechnologyPref);
   }

   ETG_TRACE_USR4(("[PARAM]::vOnMSSetTechnologyPreference() - u32DeviceHandle = 0x%x, Pref size = %d u32NoofPreferences = %d",
         u32DeviceHandle, vecTechPreference.size(), u32NoofPreferences));
   trMsgContext rMsgContext;
   CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

   if(OSAL_NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vSetTechnologyPreference(u32DeviceHandle, vecTechPreference);
   }

   midw_smartphoneintfi_tclMsgSetTechnologyPreferenceMethodResult oSetTechnologyPrefMR;
   oSetTechnologyPrefMR.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;

   //post Method result to HMI. There is no failure case foreseen as of now
   FIMsgDispatch oPostMsg(_poMainAppl);
   if (FALSE == oPostMsg.bSendResMessage(oSetTechnologyPrefMR,
            rMsgContext, SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vOnMSSetTechnologyPreference: Error in posting Method Result"));
   }
}

/**************************************************************************
 ** FUNCTION   : tVoid spi_tclService:: vOnMSGetTechnologyPreference()
 ***************************************************************************/
t_Void spi_tclService::vOnMSGetTechnologyPreference(amt_tclServiceData* poMessage)
{
   spi_tMsgGetTechnologyPrefMS oGetTechPref(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);
   t_U32 u32DeviceHandle = oGetTechPref.DeviceHandle;
   std::vector<tenDeviceCategory> vecTechPreference;
   trMsgContext rMsgContext;
   CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

   if(OSAL_NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vGetTechnologyPreference(u32DeviceHandle, vecTechPreference);
   }

   midw_smartphoneintfi_tclMsgGetTechnologyPreferenceMethodResult oGetTechnologyPrefMR;
   oGetTechnologyPrefMR.NumofPreferences = vecTechPreference.size();

   for(tU16 u16Index = 0; u16Index < vecTechPreference.size(); u16Index++)
   {
      midw_fi_tcl_e8_DeviceCategory enTechnologypref;
      enTechnologypref.enType = (midw_fi_tcl_e8_DeviceCategory::tenType) vecTechPreference[u16Index];
      oGetTechnologyPrefMR.PreferenceOrderList.push_back(enTechnologypref);
   }

   ETG_TRACE_USR1(("spi_tclService::vOnMSGetTechnologyPreference() u32DeviceHandle = 0x%x, Pref size = %d", u32DeviceHandle,  oGetTechnologyPrefMR.PreferenceOrderList.size()));

   //post Method result to HMI. There is no failure case foreseen as of now
   FIMsgDispatch oPostMsg(_poMainAppl);
   if (FALSE == oPostMsg.bSendResMessage(oGetTechnologyPrefMR,
            rMsgContext, SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vOnMSGetTechnologyPreference: Error in posting Method Result"));
   }
}

/**************************************************************************
 ** FUNCTION   : tVoid spi_tclService:: vOnMSSetDeviceSelMode()
 ***************************************************************************/
t_Void spi_tclService::vOnMSSetDeviceSelMode(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSetDeviceSelMode() entered "));
   spi_tMsgSetDeviceSelModeMS oSetDevSelMode(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);
   tenDeviceSelectionMode enDeviceSelectionMode = static_cast<tenDeviceSelectionMode> (oSetDevSelMode.DeviceSelectionMode.enType);

   ETG_TRACE_USR4(("[PARAM]::vOnMSSetDeviceSelMode() - Request to switch device selection mode to %d",
            ETG_ENUM(DEVICE_SELECTION_MODE,enDeviceSelectionMode)));
   trMsgContext rMsgContext;
   CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

   if(OSAL_NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vSetDeviceSelectionMode(enDeviceSelectionMode);
   }

   midw_smartphoneintfi_tclMsgSetDeviceSelectionModeMethodResult oSetDevSelMR;
   oSetDevSelMR.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;

   //post Method result to HMI. There is no failure case foreseen as of now
   FIMsgDispatch oPostMsg(_poMainAppl);
   if (FALSE == oPostMsg.bSendResMessage(oSetDevSelMR,
            rMsgContext, SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vOnMSSetDeviceSelMode: Error in posting Method Result"));
   }
}

/******************************************************************************
** FUNCTION:  tVoid spi_tclService::vOnDataServiceSubscribeRquest(amt_tclSer...
******************************************************************************/
t_Void spi_tclService::vOnDataServiceSubscribeRquest(amt_tclServiceData* poMessage)
{
   /*lint -esym(40,fvOnGpsData)fvOnGpsData Undeclared identifier */
   /*lint -esym(40,fvOnSensorData)fvOnSensorData Undeclared identifier */
   /*lint -esym(40,_1)_1 Undeclared identifier */

   ETG_TRACE_USR1(("spi_tclService::vOnDataServiceSubscribeRequest()"));

    tLbDataServiceSubscribeMsg oDataSrvMsg(poMessage);
   t_U16 u16SubscribeInfo = oDataSrvMsg.u16GetWord();
   //! taking the first bits (9-16) as a boolean value.
   t_Bool bSubscribe = (t_Bool) (u16SubscribeInfo & 0x0100);
   //! taking the second byte (1-8) as a status value
   tenLocationDataType enLocDataType = static_cast<tenLocationDataType> (u16SubscribeInfo & 0x00FF);
   ETG_TRACE_USR1((" SPI_C_U16_IFID_DATA_SERVICE_SUBSCRIBE bStatus : %d  and DataValue : %d", bSubscribe, ETG_ENUM(LOCATION_DATA_TYPE,
            enLocDataType)));

   if (OSAL_NULL != m_poPosDataClient)
    {
      //! Register for Location data notifications
      if (true == bSubscribe)
      {
         //! Initialise callbacks to be handed over to
         //! Pos_Fi client handler for receiving GPS data.
         trLocationDataCallbacks rDataSvcLocDataCb;
         rDataSvcLocDataCb.fvOnGpsData =
               std::bind(&spi_tclService::vOnGPSData,
                         this,
                         SPI_FUNC_PLACEHOLDERS_1);

         m_poPosDataClient->vRegisterForProperties(enLocDataType, rDataSvcLocDataCb);
      }
      //! Unregister for Location data notifications
      else
      {
         m_poPosDataClient->vUnregisterForProperties(enLocDataType);
      }
   } //if (OSAL_NULL != m_poPosDataClient)

   if (OSAL_NULL != m_poSensorDataClient)
   {
      //! Register for Sensor data notifications
      if (true == bSubscribe)
      {
         //! Initialise callbacks to be handed over to
         //! Pos_Fi client handler for receiving GPS data.
         trSensorDataCallbacks rDataSvcSensorDataCb;
         rDataSvcSensorDataCb.fvOnSensorData =
               std::bind(&spi_tclService::vOnSensorData,
                         this,
                         SPI_FUNC_PLACEHOLDERS_1);

         rDataSvcSensorDataCb.fvOnAccSensorData =
            std::bind(&spi_tclService::vOnAccSensorData,
            this,
            SPI_FUNC_PLACEHOLDERS_1);

         rDataSvcSensorDataCb.fvOnGyroSensorData =
            std::bind(&spi_tclService::vOnGyroSensorData,
            this,
            SPI_FUNC_PLACEHOLDERS_1);

         m_poSensorDataClient->vRegisterForProperty(rDataSvcSensorDataCb);

         tenDeviceCategory enSelDevCat = e8DEV_TYPE_UNKNOWN;
         if (OSAL_NULL != m_poSpiCmdIntf)
         {
            enSelDevCat = m_poSpiCmdIntf->enGetDeviceCategory(m_poSpiCmdIntf->u32GetSelectedDeviceHandle());
         }

         if(e8DEV_TYPE_ANDROIDAUTO == enSelDevCat)
         {
            m_poSensorDataClient->vRegisterForGyroAccProperty(rDataSvcSensorDataCb);
         }
      }
      //! Unregister for Sensor data notifications
      else
      {
         m_poSensorDataClient->vUnregisterForProperty();
         m_poSensorDataClient->vUnregisterForGyroAccProperty();
      }
   } //if (OSAL_NULL != m_poSensorDataClient)
}


/******************************************************************************
** FUNCTION:  tVoid spi_tclService::vOnLoopbackService(amt_tclSer...
******************************************************************************/
/*virtual*/
tVoid spi_tclService::vOnLoopback(tU16 u16ServiceID, amt_tclServiceData *poMessage)
{
   ETG_TRACE_USR1(("vOnLoopback() entered with service Id = %d", u16ServiceID));

   if(
     (OSAL_NULL != poMessage)
     &&
     (FALSE == ahl_tclBaseOneThreadService::bDefaultSvcDataHandler(this, poMessage))
     )
   {
      // Trace: Nothing else to do since it is a loopback message.
      // Message will be deleted by the framework.
      // No error message to be sent back if loop back could not find a mapping
      // function - just perform the trace.
      ETG_TRACE_ERR(("vOnLoopback() could not find a mapping function."));
      ETG_TRACE_ERR(("Opcode: %d", ETG_ENUM(OP_CODES, poMessage->u8GetOpCode())
         ));
   }
}

/***************************************************************************
** FUNCTION:  tBool spi_tclService::vOnLbBTAddressUpdate(amt_tclServiceData* poMessage)
***************************************************************************/
t_Void spi_tclService::vOnLbBTAddressUpdate(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnLbBTAddressUpdate()"));
   tLbOnVehicleBTAdressUpdate oBTAddressUpdate(poMessage);
   t_String szBTAddress = (OSAL_NULL != oBTAddressUpdate.pcocGetData())? (oBTAddressUpdate.pcocGetData()) : ("");
   if(NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vSetVehicleBTAddress(szBTAddress);
   }//if(NULL != m_poSpiCmdIntf)
}

/***************************************************************************
** FUNCTION:  tBool spi_tclService::bUpdateClients(tCU16 cu16FunID)
***************************************************************************/
tBool spi_tclService::bUpdateClients(tCU16 cu16FunID)
{
   //Update all clients registered for the property.
   ail_tenCommunicationError enCommError = eUpdateClients(cu16FunID);

   tBool bRet = (AIL_EN_N_NO_ERROR == enCommError);

   if (FALSE == bRet)
   {
      ETG_TRACE_ERR(( "bUpdateClients failed: %d", enCommError));
   }//if(FALSE == bRet)

   return bRet;
}

/***************************************************************************
** FUNCTION: tVoid spi_tclService::vPostDeviceStatusInfo(tenDeviceStatusInfo enDeviceStatusInfo)
***************************************************************************/
tVoid spi_tclService::vPostDeviceStatusInfo(t_U32 u32DevHandle,
         tenDeviceConnectionType enDevConnType,
         tenDeviceStatusInfo enDeviceStatus)
{
   ETG_TRACE_USR1(("spi_tclService::vPostDeviceStatusInfo u32DevHandle = %d"
            " enDevConnType = %d enDeviceStatus- %d \n",
            u32DevHandle, enDevConnType, enDeviceStatus));

   m_enDevStatusInfo.DeviceHandle = u32DevHandle;
   m_enDevStatusInfo.DeviceStatus.enType = (midw_fi_tcl_e8_DeviceStatusInfo::tenType) enDeviceStatus;
   m_enDevStatusInfo.DeviceConnectionType.enType =
            (midw_fi_tcl_e8_DeviceConnectionType::tenType) enDevConnType;

   if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DEVICESTATUSINFO))
   {
      ETG_TRACE_ERR(("spi_tclService::vPostDeviceStatusInfo:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DEVICESTATUSINFO))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostDAPStatusInfo
***************************************************************************/
t_Void spi_tclService::vPostDAPStatusInfo(t_U32 u32DeviceHandle,
         tenDeviceConnectionType enDevConnType,
         tenDAPStatus enDAPStatus)
{
   ETG_TRACE_USR1(("spi_tclService::vPostDAPStatusInfo-0x%x enDAPStatus = %d\n",
            u32DeviceHandle, enDAPStatus));
   m_enDAPStatusInfo.DeviceHandle = u32DeviceHandle;
   m_enDAPStatusInfo.DeviceConnectionType.enType =
            (midw_fi_tcl_e8_DeviceConnectionType::tenType)(enDevConnType);
   m_enDAPStatusInfo.DAPStatus.enType = (midw_fi_tcl_e8_DAPStatus::tenType)(enDAPStatus);
   if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DAPSTATUSINFO))
   {
      ETG_TRACE_ERR(("spi_tclService::vPostDAPStatusInfo:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DAPSTATUSINFO))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostAppStatusInfo()
***************************************************************************/
t_Void spi_tclService::vPostAppStatusInfo(t_U32 u32DeviceHandle,
                          tenDeviceConnectionType enDevConnType,
                          tenAppStatusInfo enAppStatus)
{
   ETG_TRACE_USR1(("spi_tclService::vPostAppStatusInfo-0x%x \n",u32DeviceHandle));
   m_AppInfoStatus.DeviceHandle = u32DeviceHandle;
   m_AppInfoStatus.DeviceConnectionType.enType = (midw_fi_tcl_e8_DeviceConnectionType::tenType)(enDevConnType);
   m_AppInfoStatus.AppStatus.enType = (midw_fi_tcl_e8_AppStatusInfo::tenType)(enAppStatus);

   if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_APPSTATUSINFO))
   {
      ETG_TRACE_ERR(("spi_tclService::vPostAppStatusInfo:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_APPSTATUSINFO))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostNotificationInfo()
***************************************************************************/
t_Void spi_tclService::vPostNotificationInfo(t_U32 u32DeviceHandle,
                                             t_U32 u32AppHandle, 
                                             const trNotiData& corfrNotificationData)
{
   ETG_TRACE_USR1(("spi_tclService::vPostNotificationInfo:Device-0x%x App-0x%x NotiID-0x%x NotiAppID-0x%x",
      u32DeviceHandle,u32AppHandle,corfrNotificationData.u32NotiID,corfrNotificationData.u32NotiAppID));

   ETG_TRACE_USR4(("spi_tclService::vPostNotificationInfo:Notification Title -%s ",
      corfrNotificationData.szNotiTitle.c_str()));

   ETG_TRACE_USR4(("spi_tclService::vPostNotificationInfo:Notification Body -%s ",
      corfrNotificationData.szNotiBody.c_str()));

   //Notification details
   m_NotificationInfo.DeviceHandle = u32DeviceHandle;
   m_NotificationInfo.AppHandle = u32AppHandle;
   //Unique ID of notification
   m_NotificationInfo.NotificationData.u16NotificationID = corfrNotificationData.u32NotiID ;
   //Name of notification
   m_NotificationInfo.NotificationData.szNotificationTitle.bSet(corfrNotificationData.szNotiTitle.c_str(), midw_fi_tclString::FI_EN_UTF8);
   //Body of notification
   m_NotificationInfo.NotificationData.szNotificationBody.bSet(corfrNotificationData.szNotiBody.c_str(), midw_fi_tclString::FI_EN_UTF8);
   m_NotificationInfo.NotificationData.u16NotificationIconCount = corfrNotificationData.u32NotiIconCount ;

   //Notification Icon Details
   tU16 u16IconAttrListLen = corfrNotificationData.tvecNotiIconList.size();

   for(tU16 u16Index=0;u16Index<u16IconAttrListLen;++u16Index)
   {
      midw_fi_tcl_IconAttributes oIconAttr;

      oIconAttr.u32IconWidth = corfrNotificationData.tvecNotiIconList[u16Index].u32IconWidth;
      oIconAttr.u32IconHeight = corfrNotificationData.tvecNotiIconList[u16Index].u32IconHeight;
      oIconAttr.u32IconDepth = corfrNotificationData.tvecNotiIconList[u16Index].u32IconDepth;
      oIconAttr.enIconMimeType.enType = 
         (midw_fi_tcl_e8_IconMimeType::tenType)(corfrNotificationData.tvecNotiIconList[u16Index].enIconMimeType);
      oIconAttr.szIconURL.bSet(
         corfrNotificationData.tvecNotiIconList[u16Index].szIconURL.c_str(), midw_fi_tclString::FI_EN_UTF8);

      m_NotificationInfo.NotificationData.NotificationIconList.push_back(oIconAttr) ;
   }//for(tU16 u16Index=0;u16Index<u16IconAttrListLen;++u16Index)

   m_NotificationInfo.NotificationData.u16NotificationAppID = corfrNotificationData.u32NotiAppID ;

   //details of ACtions that can be performed on thsis Notification
   tU16 u16NotiActionListLen = corfrNotificationData.tvecNotiActionList.size();
   for(tU16 u16Index=0;u16Index<u16NotiActionListLen;++u16Index)
   {
      midw_fi_tcl_NotificationAction oNotiAction;
      ETG_TRACE_USR4(("spi_tclService::vPostNotificationInfo:NotiActionID-0x%x",
         corfrNotificationData.tvecNotiActionList[u16Index].u32NotiActionID));

      ETG_TRACE_USR4(("spi_tclService::vPostNotificationInfo:NotiActionName -%s ",
         corfrNotificationData.tvecNotiActionList[u16Index].szNotiActionName.c_str()));

      //copy notification action id details
      oNotiAction.u16NotificationActionID = corfrNotificationData.tvecNotiActionList[u16Index].u32NotiActionID ;
      oNotiAction.szNotificationActionName.bSet( 
         corfrNotificationData.tvecNotiActionList[u16Index].szNotiActionName.c_str(),midw_fi_tclString::FI_EN_UTF8);
      oNotiAction.u16NotificationActionIconCount = corfrNotificationData.tvecNotiActionList[u16Index].u32NotiActionIconCount ;

      //copy notification action icon details
      tU16 u16NotiActionIconAttrListLen=corfrNotificationData.tvecNotiActionList[u16Index].tvecNotiActionIconList.size();
      for(tU16 u16Ind=0;u16Ind<u16NotiActionIconAttrListLen;++u16Ind)
      {
         midw_fi_tcl_IconAttributes oIconAttr;
         oIconAttr.u32IconWidth = corfrNotificationData.tvecNotiActionList[u16Index].tvecNotiActionIconList[u16Ind].u32IconWidth;
         oIconAttr.u32IconHeight = corfrNotificationData.tvecNotiActionList[u16Index].tvecNotiActionIconList[u16Ind].u32IconHeight;
         oIconAttr.u32IconDepth = corfrNotificationData.tvecNotiActionList[u16Index].tvecNotiActionIconList[u16Ind].u32IconDepth;
         oIconAttr.enIconMimeType.enType = 
            (midw_fi_tcl_e8_IconMimeType::tenType)(corfrNotificationData.tvecNotiActionList[u16Index].tvecNotiActionIconList[u16Ind].enIconMimeType);
         oIconAttr.szIconURL.bSet(
            corfrNotificationData.tvecNotiActionList[u16Index].tvecNotiActionIconList[u16Ind].szIconURL.c_str(), midw_fi_tclString::FI_EN_UTF8);

         oNotiAction.NotificationActionIconList.push_back(oIconAttr);
      }//for(tU16 u16Ind=0;u16Ind<u16NotiActionIconAttrListLen;++u16Ind)

      m_NotificationInfo.NotificationData.NotificationActionList.push_back(oNotiAction);
   }//for(tU16 u16Index=0;u16Index<u16NotiActionListLen;++u16Index)

   if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_NOTIFICATIONINFO))
   {
      ETG_TRACE_ERR(("spi_tclService::vPostNotificationInfo:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_NOTIFICATIONINFO))

}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostApplicationMediaMetaData()
***************************************************************************/
t_Void spi_tclService::vPostApplicationMediaMetaData(
      const trAppMediaMetaData& rfcorApplicationMediaMetaData,
      const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData entered: bMediaMetadataValid = %u ",
         ETG_ENUM(BOOL, rfcorApplicationMediaMetaData.bMediaMetadataValid)));

   SPI_INTENTIONALLY_UNUSED(rfcorUsrCntxt);

   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      //! Retrieving device handle
      m_AppMediaMetaData.DeviceHandle = m_poSpiCmdIntf->u32GetSelectedDeviceHandle();
      //! Set Metadata Validity
      m_AppMediaMetaData.MetaDataValid = rfcorApplicationMediaMetaData.bMediaMetadataValid;
      //! Retrieving Meta Data
      m_AppMediaMetaData.ApplicationMediaMetaData.AppName.bSet( rfcorApplicationMediaMetaData.szAppName.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.Title.bSet( rfcorApplicationMediaMetaData.szTitle.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.Artist.bSet( rfcorApplicationMediaMetaData.szArtist.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.Album.bSet( rfcorApplicationMediaMetaData.szAlbum.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.AlbumArtist.bSet(rfcorApplicationMediaMetaData.szAlbumArtist.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.Genre.bSet( rfcorApplicationMediaMetaData.szGenre.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.Composer.bSet( rfcorApplicationMediaMetaData.szGenre.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.TrackNumber = rfcorApplicationMediaMetaData.u32TrackNumber ;
      m_AppMediaMetaData.ApplicationMediaMetaData.AlbumTrackCount = rfcorApplicationMediaMetaData.u32AlbumTrackCount ;
      m_AppMediaMetaData.ApplicationMediaMetaData.AlbumDiscNumber = rfcorApplicationMediaMetaData.u32AlbumDiscNumber ;
      m_AppMediaMetaData.ApplicationMediaMetaData.AlbumDiscCount = rfcorApplicationMediaMetaData.u32AlbumDiscCount ;
      m_AppMediaMetaData.ApplicationMediaMetaData.ChapterCount = rfcorApplicationMediaMetaData.u32ChapterCount ;
      m_AppMediaMetaData.ApplicationMediaMetaData.ImageMIMEType.bSet( rfcorApplicationMediaMetaData.szImageMIMEType.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.ImageSize = rfcorApplicationMediaMetaData.u32ImageSize ;
      m_AppMediaMetaData.ApplicationMediaMetaData.ImageUrl.bSet( rfcorApplicationMediaMetaData.szImageUrl.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.AlbumArt.bSet( rfcorApplicationMediaMetaData.szAlbumArt.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.PlayBackState.enType = static_cast<midw_fi_tcl_e8_PlayBackState::tenType>(rfcorApplicationMediaMetaData.enMediaPlayBackState) ;
      m_AppMediaMetaData.ApplicationMediaMetaData.ShuffleState.enType = static_cast<midw_fi_tcl_e8_PlayBackShuffleState::tenType>(rfcorApplicationMediaMetaData.enMediaPlayBackShuffleState) ;
      m_AppMediaMetaData.ApplicationMediaMetaData.RepeatState.enType = static_cast<midw_fi_tcl_e8_PlayBackRepeatState::tenType>(rfcorApplicationMediaMetaData.enMediaPlayBackRepeatState) ;
      m_AppMediaMetaData.ApplicationMediaMetaData.MediaType.enType = static_cast<midw_fi_tcl_e8_PlayBackMediaType::tenType>(rfcorApplicationMediaMetaData.enMediaPlayBackMediaType) ;
      m_AppMediaMetaData.ApplicationMediaMetaData.iTunesRadioAd = rfcorApplicationMediaMetaData.bITunesRadioAd ;
      m_AppMediaMetaData.ApplicationMediaMetaData.iTunesRadioStationName.bSet(rfcorApplicationMediaMetaData.szITunesRadioStationName.c_str(), midw_fi_tclString::FI_EN_UTF8);
      m_AppMediaMetaData.ApplicationMediaMetaData.MediaRating = rfcorApplicationMediaMetaData.u8MediaRating;

     //! Printing metadata in single trace
      //! because ETG can't handle more than one string in single trace
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: AppName %s",
            rfcorApplicationMediaMetaData.szAppName.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Title %s",
            rfcorApplicationMediaMetaData.szTitle.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Artist %s",
            rfcorApplicationMediaMetaData.szArtist.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Album %s",
            rfcorApplicationMediaMetaData.szAlbum.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Album Artist %s",
            rfcorApplicationMediaMetaData.szAlbumArtist.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Genre %s",
            rfcorApplicationMediaMetaData.szGenre.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Composer %s",
            rfcorApplicationMediaMetaData.szGenre.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: TrackNumber = 0x%x, AlbumTrackCount = 0x%x, AlbumDiscNumber = 0x%x,AlbumDiscCount = 0x%x, ChapterCount = 0x%x",
            rfcorApplicationMediaMetaData.u32TrackNumber,rfcorApplicationMediaMetaData.u32AlbumTrackCount, rfcorApplicationMediaMetaData.u32AlbumDiscNumber,
            rfcorApplicationMediaMetaData.u32AlbumDiscCount,rfcorApplicationMediaMetaData.u32ChapterCount));

      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Image Mimetype  %s",
            rfcorApplicationMediaMetaData.szImageMIMEType.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: ImageSize = %d", rfcorApplicationMediaMetaData.u32ImageSize));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Image url %s",
            rfcorApplicationMediaMetaData.szImageUrl.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: Album Art %s",
            rfcorApplicationMediaMetaData.szAlbumArt.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: iTunes RadioStation name %s",
            rfcorApplicationMediaMetaData.szITunesRadioStationName.c_str()));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: bITunesRadioAd = %d, u8MediaRating = %d",
            rfcorApplicationMediaMetaData.bITunesRadioAd,rfcorApplicationMediaMetaData.u8MediaRating));
      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaMetaData: PlayBackState = %d, ShuffleState = %d, RepeatState = %d,MediaType = %d",
            ETG_ENUM(PLAYBACK_STATE,rfcorApplicationMediaMetaData.enMediaPlayBackState),
            ETG_ENUM(SHUFFLE_MODE, rfcorApplicationMediaMetaData.enMediaPlayBackShuffleState),
            ETG_ENUM(REPEAT_MODE, rfcorApplicationMediaMetaData.enMediaPlayBackRepeatState),
            rfcorApplicationMediaMetaData.enMediaPlayBackMediaType));
      //! Updating clients
      if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_APPLICATIONMEDIAMETADATA))
      {
         ETG_TRACE_ERR(("spi_tclService::vPostApplicationMediaMetaData:Error in updating clients\n"));
      } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_APPLICATIONMEDIAMETADATA))
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostApplicationMediaPlaytime()
***************************************************************************/
t_Void spi_tclService::vPostApplicationMediaPlaytime(const trAppMediaPlaytime& rfcorApplicationMediaPlaytime,
      const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaPlaytime entered"));

   SPI_INTENTIONALLY_UNUSED(rfcorUsrCntxt);

   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      //! Retrieving device handle
      m_AppMediaPlayBacktime.DeviceHandle = m_poSpiCmdIntf->u32GetSelectedDeviceHandle();
      m_AppMediaPlayBacktime.TotalPlayTime = rfcorApplicationMediaPlaytime.u32TotalPlayTime;
      m_AppMediaPlayBacktime.ElapsedPlayTime = rfcorApplicationMediaPlaytime.u32ElapsedPlayTime;

      ETG_TRACE_USR1(("spi_tclService::vPostApplicationMediaPlaytime: TotalPlayTime = %d, ElapsedPlayTime = %d",
            m_AppMediaPlayBacktime.TotalPlayTime,m_AppMediaPlayBacktime.ElapsedPlayTime));

      //! Updating clients
      if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_MEDIAPLAYBACKTIME))
      {
         ETG_TRACE_ERR(("spi_tclService::vPostApplicationMediaPlaytime:Error in updating clients\n"));
      } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_MEDIAPLAYBACKTIME))
   }
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostApplicationPhoneData()
***************************************************************************/
t_Void spi_tclService::vPostApplicationPhoneData(const trAppPhoneData& rfcorApplicationPhoneData,
      const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostApplicationPhoneData entered: bPhoneMetadataValid = %u",
         ETG_ENUM(BOOL, rfcorApplicationPhoneData.bPhoneMetadataValid)));
   SPI_INTENTIONALLY_UNUSED(rfcorUsrCntxt);

   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      m_AppPhoneData.DeviceHandle = m_poSpiCmdIntf->u32GetSelectedDeviceHandle();
      m_AppPhoneData.PhoneDataValid = rfcorApplicationPhoneData.bPhoneMetadataValid;
      m_AppPhoneData.SignalStrength.enType = static_cast<midw_fi_tcl_e8_PhoneSignalStrength::tenType>(rfcorApplicationPhoneData.enSignalStrength);
      m_AppPhoneData.RegistrationStatus.enType = static_cast<midw_fi_tcl_e8_PhoneRegistrationStatus::tenType>(rfcorApplicationPhoneData.enRegistrationStatus);
      m_AppPhoneData.AirPlaneModeStatus.enType = static_cast<midw_fi_tcl_e8_PhoneAirPlaneModeStatus::tenType>(rfcorApplicationPhoneData.enAirPlaneModeStatus);
      m_AppPhoneData.Mute.enType = static_cast<midw_fi_tcl_e8_Mute::tenType>(rfcorApplicationPhoneData.enMute);
      m_AppPhoneData.CarrierName.bSet(rfcorApplicationPhoneData.szCarrierName.c_str(), midw_fi_tclString::FI_EN_UTF8);

      ETG_TRACE_USR2(("vPostApplicationPhoneData :: Phone data validity = %d ", ETG_ENUM(BOOL,m_AppPhoneData.PhoneDataValid)));
      ETG_TRACE_USR2(("vPostApplicationPhoneData :: Signal Strength = %d ",
            ETG_ENUM(SIGNAL_STRENGTH, rfcorApplicationPhoneData.enSignalStrength)));
      ETG_TRACE_USR2(("vPostApplicationPhoneData :: Registration Status = %d ",
            ETG_ENUM(REGISTRATION_STATUS, rfcorApplicationPhoneData.enRegistrationStatus)));
      ETG_TRACE_USR2(("vPostApplicationPhoneData :: AirPlaneMode Status = %d ",
            ETG_ENUM(AIRPLANE_MODE_STATUS, rfcorApplicationPhoneData.enAirPlaneModeStatus)));
      ETG_TRACE_USR2(("vPostApplicationPhoneData :: Mute Status = %d ",
            ETG_ENUM(PHONE_MUTE_STATUS, rfcorApplicationPhoneData.enMute)));
      ETG_TRACE_USR2(("vPostApplicationPhoneData :: Carrier Name = %s ", rfcorApplicationPhoneData.szCarrierName.c_str()));

      //! Populate the metadata to SPI fi structure
      vPopulatePhoneMetadata( m_AppPhoneData.PhoneCallMetadata,
            rfcorApplicationPhoneData.tvecPhoneCallMetaDataList);

      ETG_TRACE_USR2(("No of items in phonecallmetadata :%d", m_AppPhoneData.PhoneCallMetadata.size()));
      //! Updating clients
      if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_APPLICATIONPHONEDATA))
      {
         ETG_TRACE_ERR(("spi_tclService::vPostApplicationPhoneData:Error in updating clients\n"));
      } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_APPLICATIONPHONEDATA))
   }
}
/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostDeviceDisplayContext()
***************************************************************************/
t_Void spi_tclService::vPostDeviceDisplayContext(t_U32 u32DeviceHandle,
                                                 t_Bool bDisplayFlag,
                                                 tenDisplayContext enDisplayContext,
                                                 const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
   ETG_TRACE_USR1(("spi_tclService::vPostDeviceDisplayContext:Device Id: %d enDisplayContext:%d bDisplayFlag-%d",
         u32DeviceHandle,enDisplayContext,bDisplayFlag));

   m_DeviceDispCntxt.DeviceHandle = u32DeviceHandle ;
   m_DeviceDispCntxt.DisplayFlag = bDisplayFlag ;
   m_DeviceDispCntxt.DisplayContext.enType = static_cast<midw_fi_tcl_e8_DisplayContext::tenType>(enDisplayContext);

   if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DEVICEDISPLAYCONTEXT))
   {
      ETG_TRACE_ERR(("spi_tclService::vPostDeviceDisplayContext:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DEVICEDISPLAYCONTEXT))

}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostDeviceAudioContext()
***************************************************************************/
t_Void spi_tclService::vPostDeviceAudioContext(t_U32 u32DeviceHandle,
      t_Bool bPlayFlag, t_U8 u8AudioContext,
      const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
   ETG_TRACE_USR1(("spi_tclService::vPostDeviceAudioContext:Device Id: %d enAudioContext:%d bPlayFlag-%d",
         u32DeviceHandle,u8AudioContext,bPlayFlag));

   m_DeviceAudioCntxt.DeviceHandle = u32DeviceHandle;
   m_DeviceAudioCntxt.AudioFlag = bPlayFlag;
   m_DeviceAudioCntxt.AudioContext.enType = static_cast<midw_fi_tcl_e8_AudioContext::tenType>(u8AudioContext);
    
   if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DEVICEAUDIOCONTEXT))
   {
      ETG_TRACE_ERR(("spi_tclService::vPostDeviceAudioContext:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DEVICEAUDIOCONTEXT))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostDeviceAppState()
***************************************************************************/
t_Void spi_tclService::vPostDeviceAppState(tenSpeechAppState enSpeechAppState, 
                                           tenPhoneAppState enPhoneAppState, 
                                           tenNavAppState enNavAppState, 
                                           const trUserContext rcUsrCntxt)
{
   SPI_INTENTIONALLY_UNUSED(rcUsrCntxt);
   ETG_TRACE_USR1(("spi_tclService::vPostDeviceAppState:enSpeechAppState: %d tenPhoneAppState:%d tenNavAppState-%d",
       enSpeechAppState,enPhoneAppState,enNavAppState));

   m_DeviceAppState.AppStateSpeech.enType = static_cast<midw_fi_tcl_e8_SpeechAppState::tenType>(enSpeechAppState);
   m_DeviceAppState.AppStatePhone.enType = static_cast<midw_fi_tcl_e8_PhoneAppState::tenType>(enPhoneAppState);
   m_DeviceAppState.AppStateNavigation.enType = static_cast<midw_fi_tcl_e8_NavigationAppState::tenType>(enNavAppState);

   // Fix for SUZUKI-20652
   /*Whenever a Carplay call is in progress,set the SubStateData paramter of the SPM Substate property to true for substate Phone.
     This causes the SPM to Set the Phone Substate.
    When there is no active call,reset the SubStateData paramter of the SPM Substate property to false for substate Phone .
    This causes the SPM to release the Phone Substate.*/
   
   t_Bool bCallActive = ((midw_fi_tcl_e8_PhoneAppState::FI_EN_SPI_APP_STATE_PHONE_ACTIVE )== m_DeviceAppState.AppStatePhone.enType)?true:false;
   if(OSAL_NULL != m_poSPMClient)
   {
      m_poSPMClient->vSendSpmPhoneSubState(bCallActive);
   }

   if(FALSE ==  bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DIPOAPPSTATUSINFO))
   {
      ETG_TRACE_ERR(("spi_tclService::vPostDeviceAppState:Error in updating clients\n"));
   }// if(FALSE ==  bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_DIPOAPPSTATUSINFO))
}


/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostSessionStatusInfo()
***************************************************************************/
t_Void spi_tclService::vPostSessionStatusInfo(t_U32 u32DeviceHandle,
                                              tenDeviceCategory enDevCat,
                                              tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1((" vPostSessionStatusInfo: Dev-0x%x Cat-%d SessionStatus-%d LastSentStatus-%d", 
      u32DeviceHandle,ETG_ENUM(DEVICE_CATEGORY,enDevCat),ETG_ENUM(SESSION_STATUS,enSessionStatus),
      ETG_ENUM(SESSION_STATUS,m_SessionStatus.SessionStatus.enType)));

   if(enSessionStatus != (static_cast<tenSessionStatus>(m_SessionStatus.SessionStatus.enType)))
   {
      m_SessionStatus.DeviceHandle = u32DeviceHandle;
      m_SessionStatus.DeviceCategory.enType = static_cast<midw_fi_tcl_e8_DeviceCategory::tenType>(enDevCat);
      m_SessionStatus.SessionStatus.enType = static_cast<midw_fi_tcl_e8_SessionStatus::tenType>(enSessionStatus);
      //! During session status update , don't change the value for HandsetInteraction as it is not available
      //! can be populated later in vUpdateDevAuthAndAccessInfo when info is available
      if(FALSE ==  bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_SESSIONSTATUSINFO))
      {
         ETG_TRACE_ERR(("spi_tclService::vPostSessionStatusInfo:Error in updating clients\n"));
      }// if(FALSE ==  bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_SESSIONSTATUSINFO))
   }
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclService::vPostSelectDeviceResult
 ***************************************************************************/
t_Void spi_tclService::vPostSelectDeviceResult(t_U32 u32DeviceHandle,
         tenDeviceConnectionType enDevConnType,
         tenDeviceConnectionReq enDevConnReq,
         tenDeviceCategory enDevCat,
         tenResponseCode enRespCode,
         tenErrorCode enErrorCode,
         t_Bool bIsPairingReq,
         const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostSelectDeviceResult-0x%x enDevConnType=%d,"
            " enDevConnReq=%d, enRespCode=%d,enErrorCode = %d,bIsPairingReq=%d \n",
            u32DeviceHandle,enDevConnType,enDevConnReq,enRespCode,enErrorCode,bIsPairingReq));
   trMsgContext rMsgContext;
   rMsgContext.rUserContext = rcUsrCntxt;

   midw_smartphoneintfi_tclMsgSelectDeviceMethodResult oMRSelectDevice;
   oMRSelectDevice.DeviceHandle = u32DeviceHandle;
   oMRSelectDevice.DeviceConnectionType.enType = (midw_fi_tcl_e8_DeviceConnectionType::tenType)(enDevConnType);
   oMRSelectDevice.DeviceConnectionReq.enType = (midw_fi_tcl_e8_DeviceConnectionReq::tenType)(enDevConnReq);
   oMRSelectDevice.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)(enRespCode);
   oMRSelectDevice.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)(enErrorCode);
   oMRSelectDevice.BTPairingRequired = bIsPairingReq;
   oMRSelectDevice.DeviceCategory.enType = (midw_fi_tcl_e8_DeviceCategory::tenType)(enDevCat);

   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRSelectDevice,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostSelectDeviceResult:Error in posting Message"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vLaunchAppResult()
***************************************************************************/
t_Void spi_tclService::vLaunchAppResult(t_U32 u32DeviceHandle, 
                                        t_U32 u32AppHandle, 
                                        tenDiPOAppType enDiPOAppType,
                                        tenResponseCode enResponseCode, 
                                        tenErrorCode enErrorCode, 
                                        const trUserContext& corfrUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vLaunchAppResult: Dev-0x%x App-0x%x Response Code-%d Errorcode-%d\n",
      u32DeviceHandle,u32AppHandle,enResponseCode,enErrorCode));

   trMsgContext rMsgContext;
   rMsgContext.rUserContext = corfrUsrCntxt;

   midw_smartphoneintfi_tclMsgLaunchAppMethodResult oMRLaunchApp;
   oMRLaunchApp.DeviceHandle = u32DeviceHandle;
   oMRLaunchApp.AppHandle = u32AppHandle;
   oMRLaunchApp.DiPOAppType.enType = (midw_fi_tcl_e8_DiPOAppType::tenType)enDiPOAppType;
   oMRLaunchApp.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRLaunchApp.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRLaunchApp,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vLaunchAppResult:Error in posting Message"));
   } //if( FALSE == oPostMsg.bSendMessage

}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vTerminateAppResult()
***************************************************************************/
t_Void spi_tclService::vTerminateAppResult(t_U32 u32DeviceHandle, 
                                           t_U32 u32AppHandle, 
                                           tenResponseCode enResponseCode, 
                                           tenErrorCode enErrorCode, 
                                           const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vTerminateAppResult: Dev-0x%x App-0x%x Response Code-%d Errorcode-%d\n",
      u32DeviceHandle,u32AppHandle,enResponseCode,enErrorCode));

   trMsgContext rMsgContext;
   rMsgContext.rUserContext = rcUsrCntxt;

   midw_smartphoneintfi_tclMsgTerminateAppMethodResult oMRTerminateApp;
   oMRTerminateApp.DeviceHandle = u32DeviceHandle;
   oMRTerminateApp.AppHandle = u32AppHandle;
   oMRTerminateApp.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRTerminateApp.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRTerminateApp,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vTerminateAppResult:Error in posting Message"));
   } //if( FALSE == oPostMsg.bSendMessage

}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostSetAppIconAttributesResult()
***************************************************************************/
t_Void spi_tclService::vPostSetAppIconAttributesResult(tenResponseCode enResponseCode,
                                                       tenErrorCode enErrorCode, 
                                                       const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostSetAppIconAttributesResult()"));
   trMsgContext rMsgContext;
   rMsgContext.rUserContext = rcUsrCntxt;

   midw_smartphoneintfi_tclMsgSetAppIconAttributesMethodResult oMRSetAppIconAttr;

   oMRSetAppIconAttr.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRSetAppIconAttr.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRSetAppIconAttr,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostSetAppIconAttributesResult:Error in posting Message"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostAppIconDataResult()
***************************************************************************/
t_Void spi_tclService::vPostAppIconDataResult(tenIconMimeType enIconMimeType,
                                              const t_U8* pcu8AppIconData, 
                                              t_U32 u32Len,
                                              const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostAppIconDataResult:%d",ETG_ENUM(ICON_MIME_TYPE,enIconMimeType)));

   trMsgContext rMsgContext;
   rMsgContext.rUserContext = rcUsrCntxt;

   t_String szMimeType="image/invalid";
   //! Retrieve Mime type info
   switch (enIconMimeType)
   {
   case e8ICON_PNG:
      szMimeType.assign("image/png");
      break;
   case e8ICON_JPEG:
      szMimeType.assign("image/jpeg");
      break;
   default:
      //Icon MIME Type is already set to invalid
      ETG_TRACE_ERR(("ERROR: vPostAppIconDataResult:  \
                     Invalid enum value encountered for IconMimeType: %d \n", (tU8)enIconMimeType));
      break;
   } //switch (enIconMimeType)

   midw_smartphoneintfi_tclMsgGetAppIconDataMethodResult oMRAppIconData;
   oMRAppIconData.IconMimeType.bSet(szMimeType.c_str(), midw_fi_tclString::FI_EN_UTF8);

   if(OSAL_NULL != pcu8AppIconData)
   {
      for(t_U32 u32Index=0;u32Index<u32Len;++u32Index)
      {
         oMRAppIconData.AppIconData.push_back(pcu8AppIconData[u32Index]);
      }//for(t_U32 u32Index=0;u32Index<u32Len;++u32Index)
   }//if(OSAL_NULL != pcu8AppIconData)

   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRAppIconData,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostAppIconDataResult:Error in posting Message"));
   } //if( FALSE == oPostMsg.bSendMessage(...))

   //! De-allocate memory used by message
   oMRAppIconData.vDestroy();
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostSetVideoBlockingModeResult()
***************************************************************************/
t_Void spi_tclService::vPostSetVideoBlockingModeResult(tenResponseCode enResponseCode,
                                                       tenErrorCode enErrorCode, 
                                                       const trUserContext& corfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostSetVideoBlockingModeResult:enErrorCode:%d",enErrorCode));

   trMsgContext rMsgContext;
   rMsgContext.rUserContext = corfrcUsrCntxt;

   midw_smartphoneintfi_tclMsgSetVideoBlockingModeMethodResult oMRSetVideoVlockingMode;
   oMRSetVideoVlockingMode.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRSetVideoVlockingMode.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRSetVideoVlockingMode,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostSetVideoBlockingModeResult:Error in posting Message"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclService::vPostBluetoothDeviceStatus(...)
 ***************************************************************************/
t_Void spi_tclService::vPostBluetoothDeviceStatus(t_U32 u32BluetoothDevHandle,
                                                  t_U32 u32ProjectionDevHandle,
                                                  tenBTChangeInfo enBTChange,
                                                  t_Bool bSameDevice,
                                                  t_Bool bCallActive)
{
   ETG_TRACE_USR1(("spi_tclService::vPostBluetoothDeviceStatus: "
         "u32BluetoothDevHandle:%d, u32ProjectionDevHandle:%d, enBTChange:%d, bSameDevice:%d, bCallActive:%d",
         u32BluetoothDevHandle, u32ProjectionDevHandle,
         ETG_ENUM(BT_CHANGE_INFO, enBTChange),
         ETG_ENUM(BOOL, bSameDevice),
         ETG_ENUM(BOOL, bCallActive)));

   m_BTDeviceStatusMsg.BluetoothDeviceHandle = u32BluetoothDevHandle;
   m_BTDeviceStatusMsg.ProjectionDeviceHandle = u32ProjectionDevHandle;
   m_BTDeviceStatusMsg.BTChangeInfo.enType = static_cast<midw_fi_tcl_e8_BTChangeInfo::tenType>(enBTChange);
   m_BTDeviceStatusMsg.SameDevice = bSameDevice;
   m_BTDeviceStatusMsg.CallActiveStatus = bCallActive;

   if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_BLUETOOTHDEVICESTATUS))
   {
      ETG_TRACE_ERR(("vPostBluetoothDeviceStatus:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_BLUETOOTHDEVICESTATUS))
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostSetOrientationModeResult()
***************************************************************************/
t_Void spi_tclService::vPostSetOrientationModeResult(tenResponseCode enResponseCode,
                                                     tenErrorCode enErrorCode, 
                                                     const trUserContext& corfrcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostSetOrientationModeResult:enErrorCode:%d",enErrorCode));

   trMsgContext rMsgContext;
   rMsgContext.rUserContext = corfrcUsrCntxt;

   midw_smartphoneintfi_tclMsgSetOrientationModeMethodResult oMRSetOrientationMode;
   oMRSetOrientationMode.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRSetOrientationMode.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRSetOrientationMode,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostSetOrientationModeResult:Error in posting Message"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostSetMLNotificationEnabledInfoResult()
***************************************************************************/
t_Void spi_tclService::vPostSetMLNotificationEnabledInfoResult(t_U32 u32DeviceHandle,
                                                               tenResponseCode enResponseCode, 
                                                               tenErrorCode enErrorCode, 
                                                               const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostSetMLNotificationEnabledInfoResult:enErrorCode:%d",enErrorCode));
   trMsgContext rMsgCntxt;
   rMsgCntxt.rUserContext = rcUsrCntxt;

   midw_smartphoneintfi_tclMsgSetMLNotificationEnabledInfoMethodResult oMRSetMLNotiEnabledInfo;
   oMRSetMLNotiEnabledInfo.DeviceHandle = u32DeviceHandle;
   oMRSetMLNotiEnabledInfo.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRSetMLNotiEnabledInfo.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   //post Method result to HMI
   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRSetMLNotiEnabledInfo,rMsgCntxt,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostSetMLNotificationEnabledInfoResult:Error in posting Method Result"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostInvokeNotificationActionResult()
***************************************************************************/
t_Void spi_tclService::vPostInvokeNotificationActionResult(tenResponseCode enResponseCode,
                                                           tenErrorCode enErrorCode,
                                                           const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostInvokeNotificationActionResult:enErrorCode:%d",enErrorCode));
   trMsgContext rMsgCntxt;
   rMsgCntxt.rUserContext = rcUsrCntxt;

   midw_smartphoneintfi_tclMsgInvokeNotificationActionMethodResult oMRInvokeNotiAction;
   oMRInvokeNotiAction.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRInvokeNotiAction.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   //post Method result to HMI
   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRInvokeNotiAction,rMsgCntxt,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostInvokeNotificationActionResult:Error in posting Method Result"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostSendTouchEvent()
***************************************************************************/
t_Void spi_tclService::vPostSendTouchEvent(tenResponseCode enResponseCode,
                                           tenErrorCode enErrorCode, 
                                           const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostSendTouchEvent:enErrorCode:%d",enErrorCode));
   trMsgContext rMsgCntxt;
   rMsgCntxt.rUserContext = rcUsrCntxt;

   midw_smartphoneintfi_tclMsgSendTouchEventMethodResult oMRSendTouchEvent;
   oMRSendTouchEvent.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRSendTouchEvent.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   //post Method result to HMI
   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRSendTouchEvent,rMsgCntxt,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostSendTouchEvent:Error in posting Method Result"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostSendKeyEvent()
***************************************************************************/
t_Void spi_tclService::vPostSendKeyEvent(tenResponseCode enResponseCode,
                                         tenErrorCode enErrorCode, 
                                         const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostSendKeyEvent:enErrorCode:%d",enErrorCode));
   trMsgContext rMsgCntxt;
   rMsgCntxt.rUserContext = rcUsrCntxt;

   midw_smartphoneintfi_tclMsgSendKeyEventMethodResult oMRSendKeyEvent;
   oMRSendKeyEvent.ResponseCode.enType = (midw_fi_tcl_e8_ResponseCode::tenType)enResponseCode;
   oMRSendKeyEvent.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType)enErrorCode ;

   //post Method result to HMI
   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRSendKeyEvent,rMsgCntxt,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostSendKeyEvent:Error in posting Method Result"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
 ** FUNCTION: t_Void spi_tclService::vPostBTPairingRequired(...)
 ***************************************************************************/
t_Void spi_tclService::vPostBTPairingRequired(const t_String& rfcoszBTAddress,
      t_Bool bPairingRequired)
{
   ETG_TRACE_USR1(("spi_tclService::vPostBTPairingRequired: bPairingRequired:%d, szBTAddress:%s ",
           ETG_ENUM(BOOL, bPairingRequired), rfcoszBTAddress.c_str()));

   m_BTPairingRequiredMsg.bPairingReqd = bPairingRequired;
   m_BTPairingRequiredMsg.sDevBTAddr.bSet(rfcoszBTAddress.c_str(), MIDWFI_CHAR_SET_UTF8);

   //! Update Clients
   if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_BTPAIRINGREQUIRED))
   {
      ETG_TRACE_ERR(("vPostBTPairingRequired:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_BTPAIRINGREQUIRED))
}

/***************************************************************************
 ** FUNCTION:  t_Bool spi_tclService::bRequestAudioActivation(t_U8)
 ***************************************************************************/
t_Bool spi_tclService::bRequestAudioActivation(t_U8 u8SourceNum)
{
   t_Bool bRetVal = false;
   ETG_TRACE_USR1((" bRequestAudioActivation entered %d\n", u8SourceNum));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = (t_Bool)(m_poAudioPolicy->bRequestAudioActivation(u8SourceNum));
   }
   return bRetVal;
}//spi_tclService::bRequestAudioActivation(t_U8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  bool spi_tclService::bRequestAudioDeactivation(t_U8)
 ***************************************************************************/
t_Bool spi_tclService::bRequestAudioDeactivation(t_U8 u8SourceNum)
{
   t_Bool bRetVal = false;
   ETG_TRACE_USR1((" bRequestAudioDeactivation entered %d\n", u8SourceNum));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = (t_Bool)(m_poAudioPolicy->bRequestAudioDeactivation(u8SourceNum));
   }
   return bRetVal;
}//spi_tclService::bRequestAudioDeactivation(t_U8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  bool spi_tclService::bPauseAudioActivity(t_U8)
 ***************************************************************************/
t_Bool spi_tclService::bPauseAudioActivity(t_U8 u8SourceNum)
{
   t_Bool bRetVal = false;
   ETG_TRACE_USR1((" bPauseAudioActivity entered %d\n", u8SourceNum));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = (t_Bool)(m_poAudioPolicy->bPauseAudioActivity(u8SourceNum));
   }
   return bRetVal;
}//spi_tclService::bPauseAudioActivity(t_U8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vStartSourceActivityResult(t_U8,t_Bool)
 ***************************************************************************/
t_Void spi_tclService::vStartSourceActivityResult(t_U8 u8SourceNum,
      t_Bool bError)
{
   ETG_TRACE_USR1((" vStartSourceActivityResult entered %d %d \n", u8SourceNum, bError));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      m_poAudioPolicy->vStartSourceActivityResult(u8SourceNum, bError);
   }
}//spi_tclService::vStartSourceActivityResult(t_U8 t_Bool)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vStopSourceActivityResult(t_U8,t_Bool)
 ***************************************************************************/
t_Void spi_tclService::vStopSourceActivityResult(t_U8 u8SourceNum,
      t_Bool bError)
{
   ETG_TRACE_USR1((" vStopSourceActivityResult entered %d %d\n", u8SourceNum, bError));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      m_poAudioPolicy->vStopSourceActivityResult(u8SourceNum, bError);
   }
}//spi_tclService::vStopSourceActivityResult(t_U8 t_Bool)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vPauseSourceActivityResult(t_U8,t_Bool)
 ***************************************************************************/
t_Void spi_tclService::vPauseSourceActivityResult(t_U8 u8SourceNum,
      t_Bool bError)
{
   ETG_TRACE_USR1((" vPauseSourceActivityResult entered %d %d\n", u8SourceNum, bError));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      m_poAudioPolicy->vPauseSourceActivityResult(u8SourceNum, bError);
   }
}//spi_tclService::vPauseSourceActivityResult(t_U8 t_Bool)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::bSetSourceAvailability(t_U8,t_Bool)
 ***************************************************************************/
t_Bool spi_tclService::bSetSrcAvailability(t_U8 u8SourceNum, t_Bool bAvail)
{
   t_Bool bRetVal = false;
   ETG_TRACE_USR1((" bSetSrcAvailability entered u8SourceNum = %d, bAvail = %d \n", u8SourceNum, bAvail));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = (t_Bool)(m_poAudioPolicy->bSetSrcAvailability(u8SourceNum, bAvail));
   }
   return bRetVal;
}//spi_tclService::bSetSourceAvailability(t_U8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vSetServiceAvailable()
 ***************************************************************************/
t_Void spi_tclService::vSetServiceAvailable(t_Bool bAvail)
{
   ETG_TRACE_USR1((" vSetServiceAvailable entered  bAvail = %d \n", bAvail));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      m_poAudioPolicy->vSetServiceAvailable(bAvail);
   }
}//spi_tclService::vSetServiceAvailable(t_Bool bAvail)

/***************************************************************************
** FUNCTION: t_Bool spi_tclService::bSetAudioDucking()
***************************************************************************/
t_Bool spi_tclService::bSetAudioDucking(const t_U8 cou8SrcNum,const t_U16 cou16RampDuration,
      const t_U8 cou8VolumeindB,
      const tenDuckingType coenDuckingType)
{
   ETG_TRACE_USR1((" bSetAudioDucking entered %d %d %d \n",
         cou16RampDuration, cou8VolumeindB, coenDuckingType));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      m_poAudioPolicy->bSetAudioDucking(cou8SrcNum,cou16RampDuration,
            cou8VolumeindB, coenDuckingType);
   }
   return true;
}//spi_tclService::bSetAudioDucking(const t_U16 cou16RampDuration,...)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vOnRequestAudioRoute(t_U8)
 ***************************************************************************/
t_Bool spi_tclService::bSetSourceMute(t_U8 u8SourceNum)
{
   t_Bool bRetVal = false;
   ETG_TRACE_USR1((" bSetSourceMute entered %d \n", u8SourceNum));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = (t_Bool)(m_poAudioPolicy->bSetSourceMute(u8SourceNum));
   }
   return bRetVal;
}//spi_tclService::bSetSourceMute(t_U8 u8SourceNum)

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::bSetSourceDemute(t_U8)
 ***************************************************************************/
t_Bool spi_tclService::bSetSourceDemute(t_U8 u8SourceNum)
{
   t_Bool bRetVal = false;
   ETG_TRACE_USR1(("bSetSourceDemute entered %d \n", u8SourceNum));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = (t_Bool)(m_poAudioPolicy->bSetSourceDemute(u8SourceNum));
   }
   return bRetVal;
}//spi_tclService::bSetSourceDemute(t_U8 u8SourceNum)


/***************************************************************************
** FUNCTION:  tBool spi_tclService::bOnSrcActivity(
arl_tenSource enSrcNum, const arl_tSrcActivity& rfcoSrcActivity)
 ***************************************************************************/
tBool spi_tclService::bOnSrcActivity(arl_tenSource enSrcNum,
      const arl_tSrcActivity& rfcoSrcActivity)
{
   ETG_TRACE_USR1((" bOnSrcActivity entered %d %d\n", enSrcNum, rfcoSrcActivity.enType));

   tBool bRetVal = false;
   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = m_poAudioPolicy->bOnSrcActivity(enSrcNum, rfcoSrcActivity);
   }
   return bRetVal;
} //spi_tclService::bOnSrcActivityStart(arl_tenSource,const arl_tSrcActivity&)



/***************************************************************************
** FUNCTION:  tBool spi_tclService::bOnAllocate(arl_tenSource enSrcNum,
                        const arl_tAllocRouteResult& rfcoAllocRoute)
***************************************************************************/
tBool spi_tclService::bOnAllocate(arl_tenSource enSrcNum,
                             const arl_tAllocRouteResult& rfcoAllocRoute)
{
   ETG_TRACE_USR1((" bOnAllocate entered %d\n", enSrcNum));

   tBool bRetVal = false;
   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = m_poAudioPolicy->bOnAllocate(enSrcNum, rfcoAllocRoute);
   }
   return bRetVal;
}//spi_tclService::bOnAllocate(arl_tenSource,const arl_tAllocRouteResult&)

/************************************************************************************
 ** FUNCTION:  tBool spi_tclService::bOnDeAllocate(arl_tenSource enSrcNum)
 *************************************************************************************/
tBool spi_tclService::bOnDeAllocate(arl_tenSource enSrcNum)
{
   ETG_TRACE_USR1((" bOnDeAllocate entered %d\n", enSrcNum));

   tBool bRetVal = false;
   if (OSAL_NULL != m_poAudioPolicy)
   {
      bRetVal = m_poAudioPolicy->bOnDeAllocate(enSrcNum);
   }
   return bRetVal;
}//spi_tclService::bOnDeAllocate(arl_tenSource)

/***************************************************************************
** FUNCTION:  tVoid spi_tclService::vOnError()
 ***************************************************************************/
tVoid spi_tclService::vOnError(tU8 u8SrcNum, arl_tenISourceError cenError)
{
   ETG_TRACE_USR1((" vOnError entered %d %d \n", u8SrcNum, cenError));

   if (OSAL_NULL != m_poAudioPolicy)
   {
      m_poAudioPolicy->vOnError(u8SrcNum, cenError);
   }
}// spi_tclService::vOnError(tU8 u8SrcNum,arl_tenISourceError cenError)

/***************************End of ARL methods****************************/


/**************************************************************************
** FUNCTION:  t_Void spi_tclService::vInsertKeyCodeToMap()
**************************************************************************/
t_Void spi_tclService::vInsertKeyCodeToMap()
{
   ETG_TRACE_USR1(("spi_tclService::vInsertKeyCodeToMap entered "));

   t_U32 u32Length = sizeof(aKeyCode)/sizeof(trKeyCode);

   for(t_U32 u32Index=0; u32Index < u32Length ;++u32Index)
   {
      m_mapKeyCode.insert(std::pair<spi_tenFiKeyCode,tenKeyCode>
         (aKeyCode[u32Index].e32SpiKeyCode,aKeyCode[u32Index].enKeyCode));
   }
}

/***************************************************************************
** FUNCTION: tenKeyCode spi_tclService::enGetKeyCode()const
***************************************************************************/
tenKeyCode spi_tclService::enGetKeyCode(spi_tenFiKeyCode e32SpiKeyCode)const
{
   //Default KeyCode
   tenKeyCode enKeyCode = e32INVALID_KEY;
   //Iterator to iterate through the Key Codes map
   std::map<spi_tenFiKeyCode,tenKeyCode>::const_iterator itKeyCode;

   //Find the requested value and return the associated enum value with that.
   itKeyCode = m_mapKeyCode.find(e32SpiKeyCode);
   if (m_mapKeyCode.end() != itKeyCode )
   {
      enKeyCode = itKeyCode->second ;
   }

   ETG_TRACE_USR1(("spi_tclService::enGetKeyCode: KeyCode in SPI Types-0x%x ",enKeyCode));

   return enKeyCode;
}
/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostSessionStatusInfo()
***************************************************************************/
t_Void spi_tclService::vSendSessionStatusInfo(t_U32 u32DeviceHandle,
                                              tenDeviceCategory enDevCat,
                                              tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1(("spi_tclService::vSendSessionStatusInfo:Dev-0x%x Cat-%d Status-%d",
      u32DeviceHandle,ETG_ENUM(DEVICE_CATEGORY,enDevCat),ETG_ENUM(SESSION_STATUS,enSessionStatus)));
   //Update the status to HMI
   vPostSessionStatusInfo(u32DeviceHandle,enDevCat,enSessionStatus);
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vUpdateAppBlockingInfo()
***************************************************************************/
t_Void spi_tclService::vUpdateAppBlockingInfo(t_U32 u32DeviceHandle,
                                              tenDeviceCategory enDevCat,
                                              tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1(("spi_tclService::vUpdateAppBlockingInfo:Dev-0x%x Cat-%d Status-%d",
      u32DeviceHandle,ETG_ENUM(DEVICE_CATEGORY,enDevCat),ETG_ENUM(SESSION_STATUS,enSessionStatus)));
   //Update the status to HMI
   vPostSessionStatusInfo(u32DeviceHandle,enDevCat,enSessionStatus);
}

/***************************************************************************
** FUNCTION: t_Void vUpdateSessionStatusInfo()
***************************************************************************/
t_Void spi_tclService::vUpdateSessionStatusInfo(t_U32 u32DeviceHandle,
                                tenDeviceCategory enDevCat,
                                tenSessionStatus enSessionStatus)
{
   ETG_TRACE_USR1(("spi_tclService::vUpdateSessionStatusInfo:Dev-0x%x Cat-%d Status-%d",
      u32DeviceHandle,ETG_ENUM(DEVICE_CATEGORY,enDevCat),ETG_ENUM(SESSION_STATUS,enSessionStatus)));
   //Update the status to HMI
   vPostSessionStatusInfo(u32DeviceHandle,enDevCat,enSessionStatus);
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vPostSubscribeForLocData(t_Bool...)
 ***************************************************************************/
t_Void spi_tclService::vPostSubscribeForLocData(t_Bool bSubscribe,
      tenLocationDataType enLocDataType)
{
   ETG_TRACE_USR1(("spi_tclService::vPostSubscribeForLocData() entered: bSubscribe = %u, enLocDataType = %u \n",
         ETG_ENUM(BOOL, bSubscribe), ETG_ENUM(LOCATION_DATA_TYPE, enLocDataType)));

   t_U16 u16SubscribeMode = 0;
   u16SubscribeMode  = (t_U16)bSubscribe;
   u16SubscribeMode = u16SubscribeMode << 8; // Set the first 1 byte as a flag value
   u16SubscribeMode = u16SubscribeMode | (t_U16)enLocDataType; // Set the second byte as status value/

   // the subscription message is posted as a loop back message to SPI service.
   // This is to avoid the register/unregister of properties triggered from different
   // thread context other than SPI CCA context.
   tLbDataServiceSubscribeMsg oSSubscribeMsg(
           m_poMainAppl->u16GetAppId(), // Source AppID
           m_poMainAppl->u16GetAppId(), // Target AppID
           0, // RegisterID
           0, // CmdCounter
           CCA_C_U16_SRV_SMARTPHONEINTEGRATION,
           SPI_C_U16_IFID_DATA_SERVICE_SUBSCRIBE,
           AMT_C_U8_CCAMSG_OPCODE_STATUS);

   oSSubscribeMsg.vSetWord(u16SubscribeMode);

   //! Post loopback message
   if (oSSubscribeMsg.bIsValid())
   {
      if (AIL_EN_N_NO_ERROR != m_poMainAppl->enPostMessage(&oSSubscribeMsg, TRUE))
      {
         ETG_TRACE_ERR(("spi_tclService::vOnSubscribeForLocData Loopback message posting failed!"));
      }
   } //if (oDayNightModeLbMsg().bIsValid())
   else
   {
      ETG_TRACE_ERR(("spi_tclService::vOnSubscribeForLocData Loopback message creation failed!"));
   }

}

/***************************************************************************
** FUNCTION:  t_Void spi_tclService::vOnGPSData(trGPSData rGPSData)
***************************************************************************/
t_Void spi_tclService::vOnGPSData(trGPSData rGpsData)
{
   ETG_TRACE_USR1(("spi_tclService::vOnGPSData() entered \n"));

   if(OSAL_NULL!= m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vOnGPSData(rGpsData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclService::vOnSensorData(trSensorData rSensorData)
***************************************************************************/
t_Void spi_tclService::vOnSensorData(trSensorData rSensorData)
{
   ETG_TRACE_USR1(("spi_tclService::vOnSensorData() entered \n"));

   if(OSAL_NULL!= m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vOnSensorData(rSensorData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclService::vOnAccSensorData
** (const std::vector<trAccSensorData>& corfvecrAccSensorData)
***************************************************************************/
t_Void spi_tclService::vOnAccSensorData(const std::vector<trAccSensorData>& corfvecrAccSensorData)
{
   if(OSAL_NULL!= m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vOnAccSensorData(corfvecrAccSensorData);
   }
}

/***************************************************************************
** FUNCTION:  t_Void spi_tclService::vOnGyroSensorData
** (const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
***************************************************************************/
t_Void spi_tclService::vOnGyroSensorData(const std::vector<trGyroSensorData>& corfvecrGyroSensorData)
{
   if(OSAL_NULL!= m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vOnGyroSensorData(corfvecrGyroSensorData);
   }
}

/***************************************************************************
** FUNCTION:  tVoid spi_tclService::vOnTelephoneCallActivity()
***************************************************************************/
tVoid spi_tclService::vOnTelephoneCallActivity(t_Bool bCallActive)
{
   ETG_TRACE_USR4(("spi_tclService::vOnTelephoneCallActivity: bIsCallActive-%d ",
         ETG_ENUM(BOOL, bCallActive)));
   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vOnCallStatus(bCallActive);
   }
}

/**************************************************************************
** FUNCTION   : t_Void spi_tclService:: vSetRegion(tenRegion enRegion)
***************************************************************************/
t_Void spi_tclService::vSetRegion(tenRegion enRegion)
{
   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vSetRegion(enRegion);
   }//if (OSAL_NULL != m_poSpiCmdIntf)
}

/***************************************************************************
** FUNCTION: tVoid spi_tclService::vOnMSSetDisplayAttributes
***************************************************************************/
tVoid spi_tclService::vOnMSSetDisplayAttributes(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR4(("spi_tclService::vOnMSSetDisplayAttributes"));

   spi_tMsgSetDisplayAttributesMS oDispAttrMS(*poMessage, SPI_SERVICE_FI_MAJOR_VERSION);

   if ((TRUE == oDispAttrMS.bIsValid())&&(OSAL_NULL != m_poSpiCmdIntf))
   {
      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);
      trDisplayAttributes rDispAttr;
      rDispAttr.u16ScreenHeight = oDispAttrMS.DisplayAttributes.u16ScreenHeight;
      rDispAttr.u16ScreenWidth = oDispAttrMS.DisplayAttributes.u16ScreenWidth;
      rDispAttr.u16ScreenHeightMm = oDispAttrMS.DisplayAttributes.u16ScreenHeightMm;
      rDispAttr.u16ScreenWidthMm = oDispAttrMS.DisplayAttributes.u16ScreenWidthMm;

      for(t_U8 u8Index=0;u8Index<oDispAttrMS.DisplayAttributes.DisplayLayerAttributes.size();u8Index++)
      {
         trDisplayLayerAttributes rDispLayerAttr;
         midw_fi_tcl_DisplayLayerAttributes oDispLayerAttr = oDispAttrMS.DisplayAttributes.DisplayLayerAttributes[u8Index];
         rDispLayerAttr.enDevCat = static_cast<tenDeviceCategory> (oDispLayerAttr.enDeviceCategory.enType);
         rDispLayerAttr.u16TouchLayerID = oDispLayerAttr.u16TouchLayerID;
         rDispLayerAttr.u16TouchSurfaceID = oDispLayerAttr.u16TouchSurfaceID;
         rDispLayerAttr.u16VideoLayerID = oDispLayerAttr.u16VideoLayerID;
         rDispLayerAttr.u16VideoSurfaceID = oDispLayerAttr.u16VideoSurfaceID;
         rDispLayerAttr.u16LayerHeight = oDispLayerAttr.u16LayerHeight;
         rDispLayerAttr.u16LayerWidth = oDispLayerAttr.u16LayerWidth;
         rDispLayerAttr.u16DisplayWidthMm = oDispLayerAttr.u16LayerWidthInMM;
         rDispLayerAttr.u16DisplayHeightMm = oDispLayerAttr.u16LayerHeightInMM;

         rDispAttr.vecDisplayLayerAttr.push_back(rDispLayerAttr);
      }//for(t_U8 u8Index=0;u8Index<oDispAttrMS.DisplayLayerAttributes.size();u8Index++)

      m_poSpiCmdIntf->vSetDispAttr(rDispAttr);

      midw_smartphoneintfi_tclMsgSetDisplayAttributesMethodResult oMRSetDisplayAttributes;
      oMRSetDisplayAttributes.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRSetDisplayAttributes.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRSetDisplayAttributes,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("vOnMSSetDisplayAttributes:Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage
   }//if (  (TRUE == oDispAttrMS.bIsValid
   else
   {
      ETG_TRACE_ERR(("spi_tclService::vOnMSSetDisplayAttributes- Invalid Update"));
   }
}
/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostKeyIconDataResult()
***************************************************************************/
t_Void spi_tclService::vPostKeyIconDataResult(const t_U32 cou32DevId,
                                              const t_U8* pczKeyIconData,
                                              t_U32 u32Len,
                                              const trUserContext rcUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostKeyIconDataResult"));

   trMsgContext rMsgContext;
   rMsgContext.rUserContext = rcUsrCntxt;

   midw_smartphoneintfi_tclMsgGetKeyIconDataMethodResult oMRKeyIconData;
   oMRKeyIconData.DeviceHandle = cou32DevId;

   if(OSAL_NULL != pczKeyIconData)
   {
      for(t_U32 u32Index=0;u32Index<u32Len;++u32Index)
      {
         oMRKeyIconData.KeyIconData.push_back(pczKeyIconData[u32Index]);
      }
   }

   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRKeyIconData,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostKeyIconDataResult:Error in posting Message"));
   }

   //! De-allocate memory used by message
   oMRKeyIconData.vDestroy();
}
/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostMLServerCapInfo()
***************************************************************************/
t_Void spi_tclService::vPostMLServerCapInfo(const t_U32 cou32DevId,
           t_U16 u16NumXDevices,
           trMLSrvKeyCapabilities rSrvKeyCapabilities)
{
   //! Note: Initialization of the status message are done by CCA framework, so it is
   //! not required to initialize in SPI.

   //! Populate the Server key capabilities
   m_MLSrvKeyCapabilities.DeviceHandle =cou32DevId;
   m_MLSrvKeyCapabilities.NumXDeviceKeys =u16NumXDevices;

   vPopulateSrvKeyCapabilities(m_MLSrvKeyCapabilities.XDeviceKeysList,rSrvKeyCapabilities.vecrXDeviceKeyInfo,
                               m_MLSrvKeyCapabilities.rKeyCapabilities,rSrvKeyCapabilities.rKeyCapabilities,
                               u16NumXDevices);

   if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_MLSERVERKEYCAPABILITIES))
     {
        ETG_TRACE_ERR(("spi_tclService::vPostMLServerCapInfo:Error in updating clients\n"));
     }
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSGetKeyIconData()
***************************************************************************/
t_Void spi_tclService::vOnMSGetKeyIconData(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSGetKeyIconData"));

   spi_tMsgGetKeyIconDataMS oGetKeyIconData(*poMessage,
      SPI_SERVICE_FI_MAJOR_VERSION);

   if (
      (TRUE == oGetKeyIconData.bIsValid())
      &&
      (OSAL_NULL != m_poSpiCmdIntf)
      )
   {
      trUserContext rUsrContext;
      CPY_TO_USRCNTXT(poMessage, rUsrContext);

      t_String szIconUrl;
      szIconUrl.assign(oGetKeyIconData.KeyIconURL.szValue);

      t_U32 u32DeviceHandle = oGetKeyIconData.DeviceHandle;

      m_poSpiCmdIntf->vGetKeyIconData(u32DeviceHandle,szIconUrl,rUsrContext);

   }
}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vPopulateSrvKeyCapabilities()
***************************************************************************/
tVoid spi_tclService::vPopulateSrvKeyCapabilities(bpstl::vector<midw_fi_tcl_XDeviceKeys> &rfrvecXDeviceKeyInfo,
                                    std::vector<trXDeviceKeyDetails> vecrXDeviceKeyInfo,
                                    midw_fi_tcl_KeyCapabilities &rKeyCapabilities,
                                    trKeyCapabilities rMLKeyCapabilities,t_U16 u16NumXDevices)
{
   ETG_TRACE_USR1(("spi_tclService::vPopulateSrvKeyCapabilities Number of XDevice Keys :%d",u16NumXDevices));
   //! Since the data types of CCA framework and SPI are not exactly the same
   //! it is required to individually copy each value.

   //! Copy the Key Capabilities
   rKeyCapabilities.u32KnobKeySupport   = rMLKeyCapabilities.u32KnobKeySupport;
   rKeyCapabilities.u32DeviceKeySupport = rMLKeyCapabilities.u32DeviceKeySupport;
   rKeyCapabilities.u32MiscKeySupport   =   rMLKeyCapabilities.u32MiscKeySupport;
   rKeyCapabilities.u32MultimediaKeySupport = rMLKeyCapabilities.u32MultimediaKeySupport;
   rKeyCapabilities.u32PointerTouchSupport = rMLKeyCapabilities.u32PointerTouchSupport;

   //!Copy the XDevice Key Info

   for (t_U16 u16VIndex = 0 ; u16VIndex < u16NumXDevices; ++u16VIndex)
      {
         midw_fi_tcl_XDeviceKeys oXDeviceKeys;
         oXDeviceKeys.szName.bSet(vecrXDeviceKeyInfo[u16VIndex].szXDeviceName.c_str(), midw_fi_tclString::FI_EN_UTF8);

         //This field is deprectated hence setting default values.
         oXDeviceKeys.bMandatory = false;

         //Set the symbol value for the XDevice Key.
         oXDeviceKeys.e32SymbolValue.enType = static_cast<midw_fi_tcl_e32_KeyCode::tenType>
                                              (vecrXDeviceKeyInfo[u16VIndex].enSymbolValue);


         ETG_TRACE_USR4(("X Device Keys[%d]:KeyName-%s",u16VIndex,vecrXDeviceKeyInfo[u16VIndex].szXDeviceName.c_str()));
         ETG_TRACE_USR4(("X Device Keys[%d]:SymbolValue-0x%x Mandatory-%d iconListLength-%d",u16VIndex,
            vecrXDeviceKeyInfo[u16VIndex].enSymbolValue,oXDeviceKeys.bMandatory,vecrXDeviceKeyInfo[u16VIndex].tvecKeyIconList.size()));

         t_U8 u8IconListSize = vecrXDeviceKeyInfo[u16VIndex].tvecKeyIconList.size();
         if (0 < u8IconListSize)
            {
               for (t_U8 u8Index=0; u8Index <u8IconListSize; ++u8Index )
                  {
                     midw_fi_tcl_IconAttributes oIconAttributes;
                     oIconAttributes.u32IconWidth  =  vecrXDeviceKeyInfo[u16VIndex].tvecKeyIconList[u8Index].u32IconWidth;
                     oIconAttributes.u32IconHeight =  vecrXDeviceKeyInfo[u16VIndex].tvecKeyIconList[u8Index].u32IconHeight;
                     oIconAttributes.u32IconDepth  =  vecrXDeviceKeyInfo[u16VIndex].tvecKeyIconList[u8Index].u32IconDepth;
                     oIconAttributes.enIconMimeType.enType = static_cast<midw_fi_tcl_e8_IconMimeType::tenType>
                                                      (vecrXDeviceKeyInfo[u16VIndex].tvecKeyIconList[u8Index].enIconMimeType);
                     oIconAttributes.szIconURL.bSet(vecrXDeviceKeyInfo[u16VIndex].tvecKeyIconList[u8Index].szIconURL.c_str(),
                                                    midw_fi_tclString::FI_EN_UTF8);

                     //Push back the Icon list into the XDevice Object
                     oXDeviceKeys.IconList.push_back(oIconAttributes);
                  }
            }

         //Populate each XDevice Key
         rfrvecXDeviceKeyInfo.push_back(oXDeviceKeys);
      }

}

/**************************************************************************
** FUNCTION   : tVoid spi_tclService:: vOnMSSendRotaryCtrlEvent()
***************************************************************************/

tVoid spi_tclService::vOnMSSendRotaryCtrlEvent(amt_tclServiceData* poMessage)
{
   ETG_TRACE_USR1(("spi_tclService::vOnMSSendRotaryCtrlEvent"));

   spi_tMsgRotaryCtrlEventMS oRotaryCtrlEvent(*poMessage,
      SPI_SERVICE_FI_MAJOR_VERSION);

   if (
      (TRUE == oRotaryCtrlEvent.bIsValid())
      &&
      (OSAL_NULL != m_poSpiCmdIntf)
      )
   {
      trUserContext rUsrContext;
      CPY_TO_USRCNTXT(poMessage, rUsrContext);

      ETG_TRACE_USR4(("Encoder Delta Counts : %d",oRotaryCtrlEvent.ControllerDeltaCounts));

      m_poSpiCmdIntf->vSendKnobKeyEvent(oRotaryCtrlEvent.DeviceHandle,oRotaryCtrlEvent.ControllerDeltaCounts
                                       ,rUsrContext);

      trMsgContext rMsgContext;
      CPY_TO_USRCNTXT(poMessage, rMsgContext.rUserContext);

      midw_smartphoneintfi_tclMsgRotaryControllerEventMethodResult oMRRotaryControllerEvent;
      oMRRotaryControllerEvent.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
      oMRRotaryControllerEvent.ErrorCode.enType = midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR;

      //post Method result to HMI
      FIMsgDispatch oPostMsg(_poMainAppl);
      if( FALSE == oPostMsg.bSendResMessage(oMRRotaryControllerEvent,rMsgContext,SPI_SERVICE_FI_MAJOR_VERSION))
      {
         ETG_TRACE_ERR(("vOnMSSendRotaryCtrlEvent:Error in posting Method Result"));
      } //if( FALSE == oPostMsg.bSendMessage
   }
}


/***************************************************************************
** FUNCTION: tVoid spi_tclService::vPostDeviceUsagePrefResult(t_U32 u32DeviceHandle...
***************************************************************************/
t_Void spi_tclService::vPostDeviceUsagePrefResult(const t_U32 coU32DeviceHandle,
      tenErrorCode enErrorCode,
      tenDeviceCategory enDeviceCategory,
      tenEnabledInfo enUsagePref,
      const trUserContext &corfrUsrCtxt)
{
   SPI_INTENTIONALLY_UNUSED(enDeviceCategory);
   SPI_INTENTIONALLY_UNUSED(enUsagePref);
   ETG_TRACE_USR1(("spi_tclService::vPostInvokeNotificationActionResult:enErrorCode:%d", enErrorCode));
   //! Send ChangeDeviceProjectionEnable method result
   trMsgContext rMsgCntxt;
   rMsgCntxt.rUserContext = corfrUsrCtxt;

   midw_smartphoneintfi_tclMsgSetDeviceUsagePreferenceMethodResult oMRSetSetDeviceUsagePreference;
   oMRSetSetDeviceUsagePreference.DeviceHandle = coU32DeviceHandle;
   oMRSetSetDeviceUsagePreference.ErrorCode.enType = (midw_fi_tcl_e8_ErrorType::tenType) enErrorCode;
   if (oMRSetSetDeviceUsagePreference.ErrorCode.enType == midw_fi_tcl_e8_ErrorType::FI_EN_NO_ERROR)
   {
      oMRSetSetDeviceUsagePreference.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_SUCCESS;
   }
   else
   {
      oMRSetSetDeviceUsagePreference.ResponseCode.enType = midw_fi_tcl_e8_ResponseCode::FI_EN_FAILURE;
   }
   //post Method result to HMI
   FIMsgDispatch oPostMsg(_poMainAppl);
   if (FALSE == oPostMsg.bSendResMessage(oMRSetSetDeviceUsagePreference, rMsgCntxt, SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vPostDeviceUsagePrefResult:Error in posting Method Result"));
   } //if( FALSE == oPostMsg.bSendMessage

}//! end of vPostDeviceUsagePrefResult()

/***************************************************************************
** FUNCTION: tVoid spi_tclService::vPopulatePhoneMetadata(...
***************************************************************************/
t_Void spi_tclService::vPopulatePhoneMetadata( bpstl::vector<midw_fi_tcl_ApplicationPhoneCallMetadata> &rfrvecPhoneCallMetadata,
                                               std::vector<trPhoneCallMetaData> vecPhoneCallMetaDataList )
{
   t_U8 u8NoOfPhonCallMetaDataSize = vecPhoneCallMetaDataList.size();
   ETG_TRACE_USR1(("spi_tclService::vPopulatePhoneMetadata Number of Phone call metaData list :%d",u8NoOfPhonCallMetaDataSize));

   //clear the vector elements
   rfrvecPhoneCallMetadata.clear();

   //!Copy the phone call metadata info
   if(0 < u8NoOfPhonCallMetaDataSize)
   {
      for (t_U16 u16Index = 0 ; u16Index < u8NoOfPhonCallMetaDataSize; ++u16Index)
      {
         midw_fi_tcl_ApplicationPhoneCallMetadata oApplicationPhoneCallMetadata;

         oApplicationPhoneCallMetadata.PhoneNumber.bSet(vecPhoneCallMetaDataList[u16Index].szPhoneNumber.c_str(),midw_fi_tclString::FI_EN_UTF8);
         oApplicationPhoneCallMetadata.DisplayName.bSet(vecPhoneCallMetaDataList[u16Index].szDisplayName.c_str(),midw_fi_tclString::FI_EN_UTF8);

         oApplicationPhoneCallMetadata.CallState.enType = static_cast<midw_fi_tcl_e8_PhoneCallState::tenType>
                                              (vecPhoneCallMetaDataList[u16Index].enPhoneCallState);

         oApplicationPhoneCallMetadata.CallDirection.enType = static_cast<midw_fi_tcl_e8_PhoneCallDirection::tenType>
                                              (vecPhoneCallMetaDataList[u16Index].enPhoneCallDirection);

         oApplicationPhoneCallMetadata.CallDuration=vecPhoneCallMetaDataList[u16Index].u32CallDuration ;
         oApplicationPhoneCallMetadata.CallerLabel.bSet(vecPhoneCallMetaDataList[u16Index].szCallerLabel.c_str(),midw_fi_tclString::FI_EN_UTF8);
         oApplicationPhoneCallMetadata.ConferencedCall =vecPhoneCallMetaDataList[u16Index].bConferencedCall ;

         ETG_TRACE_USR4(("Phone Call Meta Data for list number : [%d] is DisplayName = %s",u16Index,vecPhoneCallMetaDataList[u16Index].szDisplayName.c_str()));
         ETG_TRACE_USR4(("Phone Call Meta Data for list number : [%d] is PhoneNumber = %s",u16Index,vecPhoneCallMetaDataList[u16Index].szPhoneNumber.c_str()));
         ETG_TRACE_USR4(("Phone Call Meta Data for list number : [%d] is CallerLabel = %s",u16Index,vecPhoneCallMetaDataList[u16Index].szCallerLabel.c_str()));
         ETG_TRACE_USR4(("Phone Call Meta Data for list number : [%d] is Phone call state = %d",
               u16Index, ETG_ENUM(PHONE_CALL_STATE, vecPhoneCallMetaDataList[u16Index].enPhoneCallState)));
         ETG_TRACE_USR4(("Phone Call Meta Data for list number : [%d] is phone call direction = %d",
               u16Index, ETG_ENUM(PHONE_CALL_DIRECTION, vecPhoneCallMetaDataList[u16Index].enPhoneCallDirection)));

         ETG_TRACE_USR4(("Phone Call Meta Data for list number : [%d] is CallDuration : 0x%x, ConferencedCall: %d",u16Index,
            vecPhoneCallMetaDataList[u16Index].u32CallDuration,vecPhoneCallMetaDataList[u16Index].bConferencedCall));

         //Populate each call PhonemetaData
         rfrvecPhoneCallMetadata.push_back(oApplicationPhoneCallMetadata);
      } // for (t_U16 u16Index = 0 ; u16Index < u8NoOfPhonCallMetaDataSize; u16Index++)
   } //if(0 <u8NoOfPhonCallMetaDataSize)

}//! end of vPopulatePhoneMetadata

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vRequestDeviceAuthorization
***************************************************************************/
t_Void spi_tclService::vRequestDeviceAuthorization(std::vector<trDeviceAuthInfo> &rfrvecDevAuthInfo)
{
   ETG_TRACE_USR1(("spi_tclService::vRequestDeviceAuthorization() entered. Size of the list = %d ", rfrvecDevAuthInfo.size()));
   m_DevAuthStatus.DeviceAuthInfoList.clear();
   m_DevAuthStatus.NumDevices = rfrvecDevAuthInfo.size();
   for (t_U16 u16Index = 0 ; u16Index < m_DevAuthStatus.NumDevices; ++u16Index)
   {
      midw_fi_tcl_DeviceAuthInfo rDevAuthInfo;
      rDevAuthInfo.DeviceHandle = rfrvecDevAuthInfo[u16Index].u32DeviceHandle;
      rDevAuthInfo.DeviceType.enType = static_cast<midw_fi_tcl_e8_DeviceType::tenType> (rfrvecDevAuthInfo[u16Index].enDeviceType);
      rDevAuthInfo.UserAuthorizationStatus.enType = static_cast<midw_fi_tcl_e8_UserAuthorizationStatus::tenType> (rfrvecDevAuthInfo[u16Index].enUserAuthorizationStatus);
      m_DevAuthStatus.DeviceAuthInfoList.push_back(rDevAuthInfo);
   }

   if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_PROJECTIONDEVICEAUTHORIZATION))
   {
      ETG_TRACE_ERR(("spi_tclService::vRequestDeviceAuthorization:Error in updating clients\n"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_PROJECTIONDEVICEAUTHORIZATION))

}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vPostDipoRoleSwitchResponse
***************************************************************************/
t_Void spi_tclService::vPostDipoRoleSwitchResponse(t_Bool bRoleSwitchRequired,const t_U32 cou32DeviceHandle,
      const trUserContext& rfcorUsrCntxt)
{
   ETG_TRACE_USR1(("spi_tclService::vPostDipoRoleSwitchResponse() bRoleSwitchRequired = %d ", ETG_ENUM(BOOL, bRoleSwitchRequired)));

   trMsgContext rMsgCntxt;
   rMsgCntxt.rUserContext = rfcorUsrCntxt;
   midw_smartphoneintfi_tclMsgDiPORoleSwitchRequiredMethodResult oMRDiPoRoleSwitchReq;
   oMRDiPoRoleSwitchReq.u8DeviceTag = static_cast<tU8>(cou32DeviceHandle);
   midw_fi_tcl_e8_DiPOSwitchReqResponse::tenType enDiPoSwitchRes =
            (true == bRoleSwitchRequired) ?
                  midw_fi_tcl_e8_DiPOSwitchReqResponse::FI_EN_E8DIPO_ROLE_SWITCH_REQUIRED
                : midw_fi_tcl_e8_DiPOSwitchReqResponse::FI_EN_E8DIPO_ROLE_SWITCH_NOT_REQUIRED;
   oMRDiPoRoleSwitchReq.e8DiPOSwitchReqResponse.enType = enDiPoSwitchRes;

   //post Method result to HMI
   FIMsgDispatch oPostMsg(_poMainAppl);
   if( FALSE == oPostMsg.bSendResMessage(oMRDiPoRoleSwitchReq,rMsgCntxt,SPI_SERVICE_FI_MAJOR_VERSION))
   {
      ETG_TRACE_ERR(("vOnMSDiPoSwitchRequired:Error in posting Method Result"));
   } //if( FALSE == oPostMsg.bSendMessage
}

/***************************************************************************
** FUNCTION: t_Void spi_tclService::vUpdateDevAuthAndAccessInfo()
***************************************************************************/
t_Void spi_tclService::vUpdateDevAuthAndAccessInfo(const t_U32 cou32DeviceHandle,
         const t_Bool cobHandsetInteractionReqd)
{
   ETG_TRACE_USR1(("spi_tclService::vUpdateDevAuthAndAccessInfo() DeviceHandle = 0x%x HandsetInteractionReqd = %d",
                     cou32DeviceHandle, ETG_ENUM(BOOL, cobHandsetInteractionReqd)));

   m_SessionStatus.HandsetInteractionStatus.enType = (true == cobHandsetInteractionReqd) ?
                     midw_fi_tcl_e8_HandsetInteraction::FI_EN_HANDSET_INTERACTION_REQUIRED :
                     midw_fi_tcl_e8_HandsetInteraction::FI_EN_HANDSET_INTERACTION_NOT_REQUIRED;
   if (FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_SESSIONSTATUSINFO))
   {
      ETG_TRACE_ERR(("spi_tclService::vPostSessionStatusInfo:Error in updating clients"));
   } // if(FALSE ==  bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_SESSIONSTATUSINFO))
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vRestoreSettings()
 ***************************************************************************/
t_Void spi_tclService::vRestoreSettings()
{
   ETG_TRACE_USR1(("spi_tclService::vRestoreSettings() entered "));

   if (OSAL_NULL != m_poSpiCmdIntf)
   {
      m_poSpiCmdIntf->vRestoreSettings();
   }//if (OSAL_NULL != m_pSpiCmdIntf)
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vPostNavigationStatus(...)
 ***************************************************************************/
t_Void spi_tclService:: vPostNavigationStatus(
       const trNavStatusData& corfrNavStatusData)
{
   ETG_TRACE_USR1(("spi_tclService::vPostNavigationStatus entered"));
   ETG_TRACE_USR2(("[DESC]: vPostNavigationStatus: DeviceHandle = 0x%x, Device Category = %d, Navigation Status = %d",
                   corfrNavStatusData.u32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY,corfrNavStatusData.enDeviceCategory),
                   ETG_ENUM(NAVIGATION_STATUS,corfrNavStatusData.enNavAppState)));

   m_NavStatus.DeviceHandle = corfrNavStatusData.u32DeviceHandle;
   m_NavStatus.DeviceCategory.enType = static_cast<midw_fi_tcl_e8_DeviceCategory::tenType>(corfrNavStatusData.enDeviceCategory);
   m_NavStatus.NavigationStatus.enType=static_cast<midw_fi_tcl_e8_NavigationAppState::tenType>(corfrNavStatusData.enNavAppState);
   //! Update Clients
   if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONSTATUSINFO))
   {
      ETG_TRACE_ERR(("vPostNavigationStatus: Error in updating clients"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONSTATUSINFO))
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vPostNavigationNextTurnDataStatus(...)
 ***************************************************************************/
t_Void spi_tclService:: vPostNavigationNextTurnDataStatus(const trNavNextTurnData& corfrNavNexTurnData)
{
   ETG_TRACE_USR1(("spi_tclService::vPostNavigationNextTurnDataStatus entered"));
   ETG_TRACE_USR2(("[DESC]:vPostNavigationNextTurnDataStatus: DeviceHandle = 0x%x, Device Category = %d, Road Name = %s",
                    corfrNavNexTurnData.u32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY,corfrNavNexTurnData.enDeviceCategory),
                    corfrNavNexTurnData.szRoadName.c_str()));
   ETG_TRACE_USR2(("[DESC]:vPostNavigationNextTurnDataStatus: Next Turn side = %d, Next Turn Event = %d, Turn Angle = %d,"
                   "Turn Number = %d, Image = %s", ETG_ENUM(NEXTTURN_SIDE,corfrNavNexTurnData.enAAPNavNextTurnSide),
                   ETG_ENUM(NEXTTURN_EVENT,corfrNavNexTurnData.enAAPNavNextTurnType),
                   corfrNavNexTurnData.s32TurnAngle,corfrNavNexTurnData.s32TurnNumber,corfrNavNexTurnData.szImage.c_str()));

   m_NavNextTurnStatus.DeviceHandle = corfrNavNexTurnData.u32DeviceHandle;
   m_NavNextTurnStatus.DeviceCategory.enType = static_cast<midw_fi_tcl_e8_DeviceCategory::tenType>(corfrNavNexTurnData.enDeviceCategory);
   if ( false == corfrNavNexTurnData.szRoadName.empty() )
   {
      m_NavNextTurnStatus.RoadName.bSet(corfrNavNexTurnData.szRoadName.c_str(), midw_fi_tclString::FI_EN_UTF8);
   }
   m_NavNextTurnStatus.TurnSide.enType = static_cast<midw_fi_tcl_e8_TurnSide::tenType>(corfrNavNexTurnData.enAAPNavNextTurnSide);
   m_NavNextTurnStatus.NextTurnEvent.enType = static_cast<midw_fi_tcl_e8_NextTurnEvent::tenType>(corfrNavNexTurnData.enAAPNavNextTurnType);
   if ( false == corfrNavNexTurnData.szImage.empty() )
   {
      m_NavNextTurnStatus.Image.bSet(corfrNavNexTurnData.szImage.c_str(), midw_fi_tclString::FI_EN_UTF8);
   }
   m_NavNextTurnStatus.TurnAngle = corfrNavNexTurnData.s32TurnAngle;
   m_NavNextTurnStatus.TurnNumber = corfrNavNexTurnData.s32TurnNumber;
   //! Update Clients
   if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONNEXTTURNDATA))
   {
      ETG_TRACE_ERR(("vPostNavigationNextTurnDataStatus: Error in updating clients"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONNEXTTURNDATA))
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vPostNavigationNextTurnDistanceDataStatus(...)
 ***************************************************************************/
t_Void spi_tclService:: vPostNavigationNextTurnDistanceDataStatus(
        const trNavNextTurnDistanceData& corfrNavNextTurnDistData)
{
   ETG_TRACE_USR1(("spi_tclService::vPostNavigationNextTurnDistanceDataStatus entered"));
   ETG_TRACE_USR2(("[DESC]: vPostNavigationNextTurnDistanceDataStatus: DeviceHandle = 0x%x, Device Category = %d, Distance = %d, Time = %d",
		   corfrNavNextTurnDistData.u32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY,corfrNavNextTurnDistData.enDeviceCategory),
		   corfrNavNextTurnDistData.s32Distance, corfrNavNextTurnDistData.s32Time));

   m_NavNextTurnDistanceStatus.DeviceHandle = corfrNavNextTurnDistData.u32DeviceHandle;
   m_NavNextTurnDistanceStatus.DeviceCategory.enType = static_cast<midw_fi_tcl_e8_DeviceCategory::tenType>(corfrNavNextTurnDistData.enDeviceCategory);
   m_NavNextTurnDistanceStatus.DistanceInMeters = corfrNavNextTurnDistData.s32Distance;
   m_NavNextTurnDistanceStatus.TimeInSec = corfrNavNextTurnDistData.s32Time;
   //! Update Clients
   if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONNEXTTURNDISTANCEDATA))
   {
      ETG_TRACE_ERR(("vPostNavigationNextTurnDistanceDataStatus: Error in updating clients"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_NAVIGATIONNEXTTURNDISTANCEDATA))
}

/***************************************************************************
 ** FUNCTION:  t_Void spi_tclService::vPostNotificationData(...)
 ***************************************************************************/
t_Void spi_tclService::vPostNotificationData(const trNotificationData& corfrNotificationData)
{
   ETG_TRACE_USR1(("spi_tclService::vPostNotificationData entered"));
   ETG_TRACE_USR2(("[DESC]:vPostNotificationData: DeviceHandle = 0x%x, Device Category = %d, Notification Text = %s",
                    corfrNotificationData.u32DeviceHandle, ETG_ENUM(DEVICE_CATEGORY, corfrNotificationData.enDeviceCategory),
                    corfrNotificationData.szNotifText.c_str()));
   ETG_TRACE_USR2(("[DESC]:vPostNotificationData: Has Notification Id = %d, Notification Id = %s",
                    corfrNotificationData.bHasId, corfrNotificationData.szId.c_str()));
   ETG_TRACE_USR2(("[DESC]:vPostNotificationData: Has Icon = %d, Icon size = %d",
                    ETG_ENUM(BOOL, corfrNotificationData.bHasIcon), corfrNotificationData.u32IconSize));
   m_NotificationEventStatus.DeviceHandle = corfrNotificationData.u32DeviceHandle;
   m_NotificationEventStatus.DeviceCategory.enType = static_cast<midw_fi_tcl_e8_DeviceCategory::tenType>(corfrNotificationData.enDeviceCategory);
   if (false == corfrNotificationData.szId.empty())
   {
	   m_NotificationEventStatus.AAPNotificationId.bSet(corfrNotificationData.szId.c_str(), midw_fi_tclString::FI_EN_UTF8);
   }
   if (false == corfrNotificationData.szNotifText.empty())
   {
	   m_NotificationEventStatus.NotificationText.bSet(corfrNotificationData.szNotifText.c_str(), midw_fi_tclString::FI_EN_UTF8);
   }
   if ((true == corfrNotificationData.bHasIcon) && (NULL != corfrNotificationData.pu8Icon))
   {
	  for(t_U32 u32Index=0; u32Index<corfrNotificationData.u32IconSize; ++u32Index)
	  {
		  m_NotificationEventStatus.NotificationIcon.push_back(corfrNotificationData.pu8Icon[u32Index]);
	  }
   }
   //! Update Clients
   if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_AAPNOTIFICATIONEVENT))
   {
      ETG_TRACE_ERR(("vPostNotificationData:Error in updating clients"));
   } //if(FALSE == bUpdateClients(MIDW_SMARTPHONEINTFI_C_U16_AAPNOTIFICATIONEVENT))
}
///////////////////////////////////////////////////////////////////////////////
// <EOF>
