/***********************************************************************/
/*!
* \file  spi_tclImpTraceStreamable.cpp
* \brief Generic message Sender
*************************************************************************
\verbatim

PROJECT:        Gen3
SW-COMPONENT:   Smart Phone Integration
DESCRIPTION:    Message sender
AUTHOR:         VIJETH
COPYRIGHT:      &copy; RBEI

HISTORY:
Date        | Author                | Modification
12.11.2013  | VIJETH                | Initial Version
17.02.2014  | Shihabudheen P M      | Added 1.bDiPODeviceConnection
                                            2.bDiPODeviceSelectResult

\endverbatim
*************************************************************************/

/******************************************************************************
| includes:
| 1)system- and project- includes
| 2)needed interfaces from external components
| 3)internal and external interfaces from this component
|----------------------------------------------------------------------------*/

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SYSTAFI_TYPES
#define MOST_FI_S_IMPORT_INTERFACE_MOST_SYSTAFI_FUNCTIONIDS
#include <most_fi_if.h>

#define FI_S_IMPORT_INTERFACE_FI_MESSAGE
#include "common_fi_if.h"

#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_SMARTPHONEINTFI_FUNCTIONIDS
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_SMARTPHONEINTFI_TYPES
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_SMARTPHONEINTFI_ERRORCODES
#define MIDW_FI_S_IMPORT_INTERFACE_MIDW_SMARTPHONEINTFI_SERVICEINFO
#include <midw_fi_if.h>

#include "spi_tclImpTraceStreamable.h"
#include "TraceStreamable.h"
#include "Trace.h"


#ifdef VARIANT_S_FTR_ENABLE_TRC_GEN
#define ETG_DEFAULT_TRACE_CLASS TR_CLASS_SMARTPHONEINT_UTILS
#include "trcGenProj/Header/spi_tclImpTraceStreamable.cpp.trc.h"
#endif

/******************************************************************************
| defines and macros (scope: module-local)
|----------------------------------------------------------------------------*/

#define CONVERT_32(u8_data1,u8_data2,u8_data3,u8_data4) ((u8_data1) | (u8_data2 << 8) | (u8_data3 << 16) | (u8_data4 << 24))
#define CONVERT_16(u8_data1,u8_data2) ((u8_data1) | (u8_data2 << 8))
inline tU64 Convert_64(tU32 u32_data1,tU32 u32_data2)
{
    tU64 u64_result;
	u64_result=u32_data1;
	return((u64_result << 32) | (u32_data2));
}




/******************************************************************************
| typedefs (scope: module-local)
|----------------------------------------------------------------------------*/

/*!
* \typedef TraceCmdFunctor<spi_tclImpTraceStreamable> spi_tCommand
* \brief Trace Command object definition.
*/
typedef TraceCmdFunctor<spi_tclImpTraceStreamable> spi_tCommand;

/******************************************************************************
| variable definition (scope: global)
|----------------------------------------------------------------------------*/

/******************************************************************************
| variable definition (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function prototype (scope: module-local)
|----------------------------------------------------------------------------*/

/******************************************************************************
| function implementation (scope: external-interfaces)
|----------------------------------------------------------------------------*/

/******************************************************************************
** FUNCTION:  spi_tclImpTraceStreamable::spi_tclImpTraceStreamable(ahl_.
******************************************************************************/

/*explicit*/
spi_tclImpTraceStreamable::spi_tclImpTraceStreamable(
   ahl_tclBaseOneThreadApp * const cpoApp) :
TraceStreamable(cpoApp) 
{
   //add code
}

/******************************************************************************
** FUNCTION:  virtual spi_tclImpTraceStreamable::~anim_tclImpTraceStrea..
******************************************************************************/

/*virtual*/
spi_tclImpTraceStreamable::~spi_tclImpTraceStreamable() 
{
   //add code
}

/******************************************************************************
** FUNCTION:  virtual tVoid spi_tclImpTraceStreamable::vSetupCmds()
******************************************************************************/

/*virtual*/
tVoid spi_tclImpTraceStreamable::vSetupCmds() 
{
   // Add the command
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::vSetupCmds"));
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_LAUNCHAPP, new spi_tCommand(this,
      &spi_tclImpTraceStreamable::bLaunchApp));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_TERMINATEAPP, new spi_tCommand(this,
      &spi_tclImpTraceStreamable::bTerminateApp));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SELECTDEVICE, new spi_tCommand(this,
      &spi_tclImpTraceStreamable::bSelectDevice));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_GETAPPLIST, new spi_tCommand(this,
      &spi_tclImpTraceStreamable::bGetAppList));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_GETDEVICEINFOLIST, new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bGetDeviceInfoList));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETVEHICLECONFIGURATION, new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bSetVehicleConfig));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETORIENTATIONMODE, new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bSetOrientationMode));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETMLNOTIFICATIONENABLEDINFO, new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bSetNotificationEnabledInfo));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETREGION, new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bSetRegion));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_ACCESSORYDISPLAYCONTEXT, new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bSetDisplayContext));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_ACCESSORYAUDIOCONTEXT, new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bSetAudioContext));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_ACCESSORYAPPSTATE, new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bSetDiPOAppState));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_INVOKENOTIFICATIONACTION,new spi_tCommand(
      this, &spi_tclImpTraceStreamable::bInvokeNotificationAction));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETVIDEOBLOCKINGMODE,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSetVideoBlocking));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETDISPLAYATTRIBUTES,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSetDisplayAttr));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SENDKEYEVENT,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSendKeyEvent));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_GETTECHNOLOGYPREFERENCE,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bGetTechnologyPreference));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETTECHNOLOGYPREFERENCE,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSetTechnologyPreference));
	  
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_GETDEVICEUSAGEPREFERENCE,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bGetDeviceUsagePreference));
	 
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_INVOKEBLUETOOTHDEVICEACTION,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bInvokeBluetoothDeviceAction));
	  
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_DIPOROLESWITCHREQUIRED,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bDiPORoleSwitchRequired));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SENDTOUCHEVENT,new spi_tCommand(
         this,&spi_tclImpTraceStreamable::bSendTouchEvent));
	  
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETDEVICEUSAGEPREFERENCE,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSetDeviceUsagePreference));
	  
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_ROTARYCONTROLLEREVENT,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bRotaryControllerEvent));
	  
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETVEHICLEMOVEMENTSTATE,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSetVehicleMovementState));
	  
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETFEATURERESTRICTIONS,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSetFeatureRestrictions));
	  
   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETENVIRONMENTDATA,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSetEnvironmentData));

   vAddCmd(MIDW_SMARTPHONEINTFI_C_U16_SETDEVICESELECTIONMODE,new spi_tCommand(
      this,&spi_tclImpTraceStreamable::bSetDeviceSelectionMode));

} // tVoid spi_tclImpTraceStreamable::vSetupCmds()

/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bGetDeviceInfoList(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bGetDeviceInfoList(tU8 const* const cpu8Buffer) const 
{
   //bGetDeviceInfoList command should be recieved with 4 bytes of data
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
   
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bGetDeviceInfoList entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgGetDeviceInfoListMethodStart oDeviceinfoList;
   //Add code
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bGetDeviceInfoList exited"));
   return (bSendMsg(oDeviceinfoList));
    
}

/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bLaunchApp(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bLaunchApp(tU8 const* const cpu8Buffer) const 
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bLaunchApp entered - %d ",cpu8Buffer[0]));
 
   midw_smartphoneintfi_tclMsgLaunchAppMethodStart oLaunchApp;

   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //Launch App command should be recieved with 16 bytes of data and 12 valid Bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device ID - 4 Bytes
      // cpu8Buffer[7] to cpu8Buffer[10] - App ID - 4 Bytes
	  // cpu8Buffer[11] - Device Category
	  // cpu8Buffer[12] - DiPO App type
      if ( 12 <= u8NumValidBytes)
      {
         oLaunchApp.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         oLaunchApp.AppHandle = CONVERT_32((cpu8Buffer[7]),(cpu8Buffer[8]),(cpu8Buffer[9]),(cpu8Buffer[10]));
         oLaunchApp.DeviceCategory.enType = (midw_fi_tcl_e8_DeviceCategory::tenType)(cpu8Buffer[11]);
         oLaunchApp.DiPOAppType.enType = (midw_fi_tcl_e8_DiPOAppType::tenType)(cpu8Buffer[12]);
         ETG_TRACE_USR4(("spi_tclImpTraceStreamable::bLaunchApp:Device- %x App- %x App type - %d", oLaunchApp.DeviceHandle,oLaunchApp.AppHandle,
		 oLaunchApp.DiPOAppType.enType));
      } //if ( 10 <= u8NumValidBytes) 
   } // if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bLaunchApp exited"));
   return (bSendMsg(oLaunchApp));
} // tBool spi_tclImpTraceStreamable::bLaunchApp(tU8 const* const cpu8Buffer) const

/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bTerminateApp(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bTerminateApp(tU8 const* const cpu8Buffer) const 
{

   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bTerminateApp entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgTerminateAppMethodStart oTerminateApp;
   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //bTerminateApp command should be recieved with 14bytes of data and 10 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device ID - 4 Bytes
      // cpu8Buffer[7] to cpu8Buffer[10] - App ID - 4 Bytes
      if (10 <= u8NumValidBytes) 
      {
         oTerminateApp.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         oTerminateApp.AppHandle =  CONVERT_32((cpu8Buffer[7]),(cpu8Buffer[8]),(cpu8Buffer[9]),(cpu8Buffer[10]));
         ETG_TRACE_USR4(("spi_tclImpTraceStreamable::bTerminateApp:Device- %x App- %x", oTerminateApp.DeviceHandle,oTerminateApp.AppHandle));
      } //if (10 <= u8NumValidBytes) 
   } //if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bTerminateApp exited"));
   return (bSendMsg(oTerminateApp));

} // tBool spi_tclImpTraceStreamable::bTerminateApp(tU8 const* const cpu8Buffer) const

/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetAnimStatus(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bSelectDevice(tU8 const* const cpu8Buffer) const 
{

   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSelectDevice entered - %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgSelectDeviceMethodStart oSelectDevice;

   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //bSelectDevice command should be recieved with 12bytes of data and 8 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device ID - 4 Bytes
      // cpu8Buffer[7] - Connection type - 1 Byte
      // cpu8Buffer[8] - Connection Req- 1 Byte.
      if (8 <= u8NumValidBytes) 
      {
         oSelectDevice.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));

         oSelectDevice.DeviceConnectionType.enType
            = static_cast<midw_fi_tcl_e8_DeviceConnectionType::tenType> (cpu8Buffer[7]);

         oSelectDevice.DeviceConnectionReq.enType
            = static_cast<midw_fi_tcl_e8_DeviceConnectionReq::tenType> (cpu8Buffer[8]);

         ETG_TRACE_USR4(("spi_tclImpTraceStreamable::bSelectDevice:Device- %x DeviceConnectionType- %d DeviceConnectionReq- %d",
            oSelectDevice.DeviceHandle,oSelectDevice.DeviceConnectionType.enType,oSelectDevice.DeviceConnectionReq.enType));
      }//if (8 <= u8NumValidBytes) 
   } //if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSelectDevice exited"));
   return (bSendMsg(oSelectDevice));

}//tBool spi_tclImpTraceStreamable::bSelectDevice(tU8 const* const cpu8Buffer) const

/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bGetAppList(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bGetAppList(tU8 const* const cpu8Buffer) const 
{

   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bGetAppList entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgGetAppListMethodStart oAppList;

   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //bGetAppList command should be recieved with 14bytes of data and 6 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device ID - 4 Bytes
      if (6 <= u8NumValidBytes) 
      {
         oAppList.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         ETG_TRACE_USR2(("spi_tclImpTraceStreamable::bGetAppList:Device- %x", oAppList.DeviceHandle));
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bGetAppList exited"));
   return (bSendMsg(oAppList));

}//tBool spi_tclImpTraceStreamable::bGetAppList(tU8 const* const cpu8Buffer) const

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetVehicleConfig()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetVehicleConfig(tU8 const* const cpu8Buffer) const
{

   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetVehicleConfig entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgSetVehicleConfigurationMethodStart oSetVehicleConfigMS;
   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //bSetVehicleConfig command should be recieved with 8bytes of data and 4 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] - Vehicle configuration
      // cpu8Buffer[4] - Enable/disable config
      if (4 <= u8NumValidBytes) 
      {
         oSetVehicleConfigMS.VehicleConfiguration.enType = static_cast<midw_fi_tcl_e8_Vehicle_Configuration::tenType>(cpu8Buffer[3]);
         oSetVehicleConfigMS.SetConfiguration = (t_Bool)(cpu8Buffer[4]);
         ETG_TRACE_USR2(("spi_tclImpTraceStreamable::bSetVehicleConfig:VehicleConfig- %x", 
            oSetVehicleConfigMS.VehicleConfiguration.enType));
      }//if (4 <= u8NumValidBytes) 
   }//if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetVehicleConfig exited"));
   return (bSendMsg(oSetVehicleConfigMS));

}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetOrientationMode()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetOrientationMode(tU8 const* const cpu8Buffer) const
{

   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetOrientationMode entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgSetOrientationModeMethodStart oSetOrientationMS;
   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //bSetOrientationMode command should be recieved with 11bytes of data and 7 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device Id
      // cpu8Buffer[7] - Orientation Mode : 0-Invalid , 1- Portrait, 2-Landscape
      if (7 <= u8NumValidBytes) 
      {
         oSetOrientationMS.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         oSetOrientationMS.OrientationMode.enType = static_cast<midw_fi_tcl_e8_OrientationMode::tenType>(cpu8Buffer[7]);
      }//if (4 <= u8NumValidBytes) 
   }//if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetOrientationMode exited"));
   return (bSendMsg(oSetOrientationMS));
}


/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetNotificationEnabledInfo()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetNotificationEnabledInfo(tU8 const* const cpu8Buffer) const
{
   
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetNotificationEnabledInfo entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgSetMLNotificationEnabledInfoMethodStart oSetNotiEnabledMS;
   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //bSetOrientationMode command should be recieved with 11bytes of data and 7 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device Id
      if (7 <= u8NumValidBytes) 
      {
         oSetNotiEnabledMS.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         oSetNotiEnabledMS.NumNotificationEnableList = (0 != cpu8Buffer[7])? (0xFFFF) : (0);
         //Vector of App handle & Enable/Disable info is not implemented
         //Its not required currently. Just it can be used to enable/disable for all apps
      }//if (7 <= u8NumValidBytes) 
   }//if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetNotificationEnabledInfo exited"));
   return (bSendMsg(oSetNotiEnabledMS));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetRegion()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetRegion(tU8 const* const cpu8Buffer) const
{
   
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetRegion entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgSetRegionMethodStart oSetRegion;
   if(NULL != cpu8Buffer)
   {
      tU8 u8NumValidBytes = cpu8Buffer[0];
      //bSetRegion command should be received with 7bytes of data and 3 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to Identify the region
      if (3 <= u8NumValidBytes) 
      {
         oSetRegion.Region.enType = static_cast<midw_fi_tcl_e8_Region::tenType>(cpu8Buffer[3]);
      }//if (3 <= u8NumValidBytes) 
   }//if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetRegion exited"));
   return (bSendMsg(oSetRegion));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDisplayContext()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetDisplayContext(tU8 const* const cpu8Buffer) const
{
  
  ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDisplayContext entered- %d ",cpu8Buffer[0]));
  midw_smartphoneintfi_tclMsgAccessoryDisplayContextMethodStart oDisplayContext;
   //bSetDisplayContext command should be received with 12 bytes of data and 8 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - device handle
	  // cpu8Buffer[7] - device flag
	  // cpu8Buffer[8] - display context type
  if(NULL != cpu8Buffer)
  {
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(8 <= u8ValidBytes)
      {
         oDisplayContext.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         oDisplayContext.DisplayFlag = (tBool)(cpu8Buffer[7]);
         oDisplayContext.DisplayContext.enType = static_cast<midw_fi_tcl_e8_DisplayContext::tenType>(cpu8Buffer[8]);
      }
  }//if(NULL != cpu8Buffer)
  ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDisplayContext exited"));
  return (bSendMsg(oDisplayContext));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetAudioContext()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetAudioContext(tU8 const* const cpu8Buffer)const
{
   
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetAudioContext entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgAccessoryAudioContextMethodStart oAudioContext;
     //bSetAudioContext command should be received with 12 bytes of data and 8 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - device handle
	  // cpu8Buffer[7] - audio flag
	  // cpu8Buffer[8] - audio context type
   if(NULL != cpu8Buffer)
   {
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(8 <= u8ValidBytes)
      {
         oAudioContext.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
         oAudioContext.AudioFlag = (tBool)(cpu8Buffer[7]);
         oAudioContext.AudioContext.enType = static_cast<midw_fi_tcl_e8_AudioContext::tenType>(cpu8Buffer[8]);
      }
   }//if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetAudioContext exited"));
   return (bSendMsg(oAudioContext));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDiPOAppState()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetDiPOAppState(tU8 const* const cpu8Buffer) const
{

  ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDiPOAppState entered - %d ",cpu8Buffer[0]));
  midw_smartphoneintfi_tclMsgAccessoryAppStateMethodStart oAppContext;
 //bSetDiPOAppState command should be received with 9 bytes of data and 5 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] - App state speech
	  // cpu8Buffer[4] - app state of the phone
	  // cpu8Buffer[5] - app navigation type
  if(NULL != cpu8Buffer)
  {
     tU8 u8ValidBytes = cpu8Buffer[0];
     if(5 <= u8ValidBytes)
     {
        oAppContext.AppStateSpeech.enType = static_cast<midw_fi_tcl_e8_SpeechAppState::tenType>(cpu8Buffer[3]);
        oAppContext.AppStatePhone.enType = static_cast<midw_fi_tcl_e8_PhoneAppState::tenType>(cpu8Buffer[4]);
        oAppContext.AppStateNavigation.enType = static_cast<midw_fi_tcl_e8_NavigationAppState::tenType>(cpu8Buffer[5]);
     } // if(8 <= u8ValidBytes) 
  }// if(NULL != cpu8Buffer)
  ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDiPOAppState exited"));
  return (bSendMsg(oAppContext));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bInvokeNotificationAction()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bInvokeNotificationAction(tU8 const* const cpu8Buffer) const
{
 
  ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bInvokeNotificationAction entered- %d ",cpu8Buffer[0]));
  midw_smartphoneintfi_tclMsgInvokeNotificationActionMethodStart oInvokeNotiAction;
  //bInvokeNotificationAction command should be received with 22 bytes of data and 18 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device Handle
	  // cpu8Buffer[7] to cpu8Buffer[10] - App Handle
	  // cpu8Buffer[11] to cpu8Buffer[14] - Notification ID
	  // cpu8Buffer[15] to cpu8Buffer[18] - Notification Action ID
  if(NULL != cpu8Buffer)
  {
     tU8 u8ValidBytes = cpu8Buffer[0];
     if(18 <= u8ValidBytes)
     {
        oInvokeNotiAction.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),
           (cpu8Buffer[6]));
        oInvokeNotiAction.AppHandle = CONVERT_32((cpu8Buffer[7]),(cpu8Buffer[8]),(cpu8Buffer[9]),
           (cpu8Buffer[10]));
        oInvokeNotiAction.NotificationID = CONVERT_32((cpu8Buffer[11]),(cpu8Buffer[12]),(cpu8Buffer[13]),
           (cpu8Buffer[14]));
        oInvokeNotiAction.NotificationActionID =CONVERT_32((cpu8Buffer[15]),(cpu8Buffer[16]),
           (cpu8Buffer[17]),(cpu8Buffer[18])) ;
     } // if(18 <= u8ValidBytes) 
  }// if(NULL != cpu8Buffer)
  ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bInvokeNotificationAction exited"));
  return (bSendMsg(oInvokeNotiAction));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetVideoBlocking()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetVideoBlocking(tU8 const* const cpu8Buffer) const
{
   
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetVideoBlocking entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgSetVideoBlockingModeMethodStart oSetVideoBlocking;
   //bSetVideoBlocking command should be recieved with 11 bytes of data and 7 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[7]  - Blocking mode
   if(NULL != cpu8Buffer)
   {
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(7 <= u8ValidBytes)
      {
         oSetVideoBlocking.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),
            (cpu8Buffer[6]));
         oSetVideoBlocking.BlockingMode.enType = static_cast <midw_fi_tcl_e8_BlockingMode::tenType>(cpu8Buffer[7]);
      } // if(8 <= u8ValidBytes) 
   }// if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetVideoBlocking exited"));
   return (bSendMsg(oSetVideoBlocking));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDisplayAttr()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetDisplayAttr(tU8 const* const cpu8Buffer) const
{
   
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDisplayAttr entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgSetDisplayAttributesMethodStart oSetDisplayAttr;
    //bSetDisplayAttr command should be recieved with 31 bytes of data and 27 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[4] - Screen height
	  // cpu8Buffer[5] to cpu8Buffer[6] - Screen width
	  // cpu8Buffer[7] to cpu8Buffer[8] - Screen height in mm
	  // cpu8Buffer[9] to cpu8Buffer[10] - Screen width in mm
      // cpu8Buffer[11] - device category
	  // cpu8Buffer[12] to cpu8Buffer[13] - touch layer ID
	  // cpu8Buffer[14] to cpu8Buffer[15] - touch surface ID
	  // cpu8Buffer[16] to cpu8Buffer[17] - video layer ID
	  // cpu8Buffer[18] to cpu8Buffer[19] - video surface ID
	  // cpu8Buffer[20] to cpu8Buffer[21] - layer width
	  // cpu8Buffer[22] to cpu8Buffer[23] - layer height
	  // cpu8Buffer[24] to cpu8Buffer[25] - layer width in mm
	  // cpu8Buffer[26] to cpu8Buffer[27] - layer height in mm
   if(NULL != cpu8Buffer)
   {
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(27 <= u8ValidBytes)
      {
         oSetDisplayAttr.DisplayAttributes.u16ScreenHeight = CONVERT_16((cpu8Buffer[3]),(cpu8Buffer[4]));
         oSetDisplayAttr.DisplayAttributes.u16ScreenWidth = CONVERT_16((cpu8Buffer[5]),(cpu8Buffer[6]));
         oSetDisplayAttr.DisplayAttributes.u16ScreenHeightMm = CONVERT_16((cpu8Buffer[7]),(cpu8Buffer[8]));
         oSetDisplayAttr.DisplayAttributes.u16ScreenWidthMm = CONVERT_16((cpu8Buffer[9]),(cpu8Buffer[10]));
         t_U8 u8DevCat = cpu8Buffer[11];
          
         for(t_U8 u8Index=0;u8Index<3;u8Index++)
         {
            midw_fi_tcl_DisplayLayerAttributes oDispLayerAttr;
            oDispLayerAttr.enDeviceCategory.enType = static_cast<midw_fi_tcl_e8_DeviceCategory::tenType>(u8DevCat);
            u8DevCat++;
            oDispLayerAttr.u16TouchLayerID = CONVERT_16((cpu8Buffer[12]),(cpu8Buffer[13]));
            oDispLayerAttr.u16TouchSurfaceID = CONVERT_16((cpu8Buffer[14]),(cpu8Buffer[15])); 
            oDispLayerAttr.u16VideoLayerID = CONVERT_16((cpu8Buffer[16]),(cpu8Buffer[17]));
            oDispLayerAttr.u16VideoSurfaceID = CONVERT_16((cpu8Buffer[18]),(cpu8Buffer[19]));
            oDispLayerAttr.u16LayerWidth = CONVERT_16((cpu8Buffer[20]),(cpu8Buffer[21]));
            oDispLayerAttr.u16LayerHeight = CONVERT_16((cpu8Buffer[22]),(cpu8Buffer[23]));
            oDispLayerAttr.u16LayerWidthInMM = CONVERT_16((cpu8Buffer[24]),(cpu8Buffer[25]));
            oDispLayerAttr.u16LayerHeightInMM = CONVERT_16((cpu8Buffer[26]),(cpu8Buffer[27]));
            oSetDisplayAttr.DisplayAttributes.DisplayLayerAttributes.push_back(oDispLayerAttr);
         }//for(t_U8 u8Index=0;u8Index<3;u8Index++)

      } // if(23 <= u8ValidBytes) 
   }// if(NULL != cpu8Buffer)
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDisplayAttr exited"));
   return (bSendMsg(oSetDisplayAttr));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSendKeyEvent()
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSendKeyEvent(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSendKeyEvent entered"));

   midw_smartphoneintfi_tclMsgSendKeyEventMethodStart oSndKeyEvent;
     //bSendKeyEvent command should be recieved with 15 bytes of data and 11 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device handle
      // cpu8Buffer[7] - key mode
	  // cpu8Buffer[8] to cpu8Buffer[11] - key code
   if(NULL != cpu8Buffer)
   {  
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(11 <= u8ValidBytes)
      {
         oSndKeyEvent.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),
            (cpu8Buffer[6]));
         oSndKeyEvent.KeyMode.enType = static_cast <midw_fi_tcl_e8_KeyMode::tenType> (cpu8Buffer[7]);
         oSndKeyEvent.KeyCode.enType= static_cast <midw_fi_tcl_e32_KeyCode::tenType> (CONVERT_32(cpu8Buffer[8],cpu8Buffer[9],cpu8Buffer[10],cpu8Buffer[11]));
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSendKeyEvent exited"));
   return (bSendMsg(oSndKeyEvent));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetTechnologyPreference(tU8 const* const cp..
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetTechnologyPreference(tU8 const* const cpu8Buffer) const
{

   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetTechnologyPreference entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgSetTechnologyPreferenceMethodStart oSetTechnologyPref;
      //bSetTechnologyPreference command should be recieved with 11 bytes of data and 7 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device handle
      // cpu8Buffer[7] -  : technology preference type
   if(NULL != cpu8Buffer)
   {
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(7 <= u8ValidBytes)
      {
         oSetTechnologyPref.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),
            (cpu8Buffer[6]));
         midw_fi_tcl_e8_DeviceCategory enTechnologyPref;
         enTechnologyPref.enType = (midw_fi_tcl_e8_DeviceCategory::tenType)cpu8Buffer[7];
         oSetTechnologyPref.PreferenceOrderList.push_back(enTechnologyPref);
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetTechnologyPreference exited"));
   return (bSendMsg(oSetTechnologyPref));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bGetTechnologyPreference(tU8 const* const cp..
***************************************************************************/
tBool spi_tclImpTraceStreamable::bGetTechnologyPreference(tU8 const* const cpu8Buffer) const
{

   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bGetTechnologyPreference entered- %d ",cpu8Buffer[0]));
   midw_smartphoneintfi_tclMsgGetTechnologyPreferenceMethodStart oGetTechnologyPref;
      //bGetTechnologyPreference command should be recieved with 10 bytes of data and 6 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[5] - Device handle

   if(NULL != cpu8Buffer)
   {
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(6 <= u8ValidBytes)
      {
         oGetTechnologyPref.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),
            (cpu8Buffer[6]));
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bGetTechnologyPreference exited"));
   return (bSendMsg(oGetTechnologyPref));
}
/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDeviceUsagePreference() 
***************************************************************************/

tBool spi_tclImpTraceStreamable::bSetDeviceUsagePreference(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDeviceUsagePreference entered"));
   midw_smartphoneintfi_tclMsgSetDeviceUsagePreferenceMethodStart oSetDevUsagePref;
   if(NULL != cpu8Buffer)
   {  
   //bSetDeviceUsagePreference command should be recieved with 12 bytes of data and 8 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device Id
      // cpu8Buffer[7] -  : Device category
	  // cpu8Buffer[8] -  : Enabled info, 0- USAGE_DISABLED ,1-USAGE_ENABLED, 2-USAGE_CONF_REQD
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(8 <= u8ValidBytes)
      {
	        ETG_TRACE_USR1(("%d",cpu8Buffer[4]));
            oSetDevUsagePref.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
            oSetDevUsagePref.DeviceCategory.enType = static_cast <midw_fi_tcl_e8_DeviceCategory::tenType>(cpu8Buffer[7]);
		    oSetDevUsagePref.EnabledInfo.enType = static_cast <midw_fi_tcl_e8_EnabledInfo::tenType>(cpu8Buffer[8]);	
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDeviceUsagePreference exited"));
   return (bSendMsg(oSetDevUsagePref));
}

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSendTouchEvent() 
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSendTouchEvent(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSendTouchEvent entered"));
   midw_smartphoneintfi_tclMsgSendTouchEventMethodStart oSendTouchEvent;
   //bSendTouchEvent command should be recieved with 17 bytes of data and 13 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device handle
      // cpu8Buffer[7] to cpu8Buffer[8] - Touch Descriptors
	  // cpu8Buffer[9] - Touch Mode
	  // cpu8Buffer[10] to cpu8Buffer[11] - X coordinate
	  // cpu8Buffer[12] to cpu8Buffer[13] - Y Coordinate
   if(NULL != cpu8Buffer)
   {  
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(13 <= u8ValidBytes)
      {
	     
	        ETG_TRACE_USR1(("%d",cpu8Buffer[4]));
            oSendTouchEvent.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
            oSendTouchEvent.TouchData.u16TouchDescriptors = CONVERT_16((cpu8Buffer[7]),(cpu8Buffer[8]));
		
	        midw_fi_tcl_TouchCoordinates oTouchCoordinates;
			oTouchCoordinates.enTouchMode.enType = static_cast <midw_fi_tcl_e8_TouchMode::tenType>(cpu8Buffer[9]);
			oTouchCoordinates.u16XCoordinate = CONVERT_16((cpu8Buffer[10]),(cpu8Buffer[11]));
		    oTouchCoordinates.u16YCoordinate = CONVERT_16((cpu8Buffer[12]),(cpu8Buffer[13]));
			
			midw_fi_tcl_TouchInfo oTouchInfo;
			oTouchInfo.TouchCoordinatesList.push_back(oTouchCoordinates);
            oSendTouchEvent.TouchData.TouchInfoList.push_back(oTouchInfo);
      }  
	  else
	  {
	        ETG_TRACE_ERR(("the input given exceeds the size of the list."));
	  }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSendTouchEvent exited"));
   return (bSendMsg(oSendTouchEvent));
}
/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bDiPORoleSwitchRequired(tU8 const* const cp..
***************************************************************************************/ 
tBool spi_tclImpTraceStreamable::bDiPORoleSwitchRequired(tU8 const* const cpu8Buffer) const
{
     ETG_TRACE_USR1(("spi_tcl::bDiPORoleSwitchRequired entered"));
//bDiPORoleSwitchRequiredMethodMethodStart command should be recieved with 7 bytes of data and 3 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] - Device Tag
      
 
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bDiPORoleSwitchRequiredMethodStart"));
   midw_smartphoneintfi_tclMsgDiPORoleSwitchRequiredMethodStart oDiPORoleSwitchReq;
   if(NULL != cpu8Buffer)
   {  
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(3<= u8ValidBytes)
      {
            oDiPORoleSwitchReq.u8DeviceTag = cpu8Buffer[3];	    
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bDiPORoleSwitchRequired exited"));
   return (bSendMsg(oDiPORoleSwitchReq));
}
/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetVehicleMovementState(tU8 const* const cp..
***************************************************************************************/   
tBool spi_tclImpTraceStreamable::bSetVehicleMovementState(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetVehicleMovementState entered"));
   midw_smartphoneintfi_tclMsgSetVehicleMovementStateMethodStart oVehicleState;
   if(NULL != cpu8Buffer)
   {  
   //VehicleMovementStateMethodMethodStart command should be recieved with 9 bytes of data and 5 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] -  : Park brake info
      // cpu8Buffer[4] -  : gear state info
	  // cpu8Buffer[5] -  :vehicle state
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(5 <= u8ValidBytes)
      {
	
            oVehicleState.ParkBrakeInfo.enType = static_cast <midw_fi_tcl_e8_ParkBrake::tenType>(cpu8Buffer[3]);
            oVehicleState.GearInfo.enType =  static_cast <midw_fi_tcl_e8_GearState::tenType>(cpu8Buffer[4]);
			oVehicleState.VehicleState.enType =  static_cast <midw_fi_tcl_e8_VehicleState::tenType> (cpu8Buffer[5]);
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetVehicleMovementState exited"));
   return (bSendMsg(oVehicleState));
} 
/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetFeatureRestrictions(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bSetFeatureRestrictions(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetFeatureRestrictions entered"));
   midw_smartphoneintfi_tclMsgSetFeatureRestrictionsMethodStart oSetFeatureRestrictions;
   if(NULL != cpu8Buffer)
   {  
   //bSetFeatureRestrictionsMethodStart command should be recieved with 9 bytes of data and 5 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] -  : DEVICE CATEGORY
      // cpu8Buffer[4] -  : park mode restriction info
	  // cpu8Buffer[5] -  : drive mode restriction info
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(5 <= u8ValidBytes)
      {
	
            oSetFeatureRestrictions.DeviceCategory.enType = static_cast <midw_fi_tcl_e8_DeviceCategory::tenType>(cpu8Buffer[3]);
            oSetFeatureRestrictions.ParkModeRestrictionInfo.FeatureLockout =  cpu8Buffer[4];
			oSetFeatureRestrictions.DriveModeRestrictionInfo.FeatureLockout =  cpu8Buffer[5];
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetFeatureRestrictions exited"));
   return (bSendMsg(oSetFeatureRestrictions));
} 
/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bGetDeviceUsagePreference(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bGetDeviceUsagePreference(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bGetDeviceUsagePreference entered"));
   midw_smartphoneintfi_tclMsgGetDeviceUsagePreferenceMethodStart oGetDevUsagePref;
   if(NULL != cpu8Buffer)
   {  
   //bGetDeviceUsagePreference command should be recieved with 11 bytes of data and 7 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - Device Handle
      // cpu8Buffer[7] -  : Device category
	  
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(7 <= u8ValidBytes)
      {
            oGetDevUsagePref.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
            oGetDevUsagePref.DeviceCategory.enType =  static_cast <midw_fi_tcl_e8_DeviceCategory::tenType>(cpu8Buffer[7]);
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bGetDeviceUsagePreference exited"));
   return (bSendMsg(oGetDevUsagePref));
}
/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bInvokeBluetoothDeviceAction(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bInvokeBluetoothDeviceAction(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bInvokeBluetoothDeviceAction entered"));
   midw_smartphoneintfi_tclMsgInvokeBluetoothDeviceActionMethodStart oInvBTAction;
   if(NULL != cpu8Buffer)
   {  
   //bInvokeBluetoothDeviceAction command should be recieved with 15 bytes of data and 11 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6] - bluetooth device handle
      // cpu8Buffer[7] to cpu8Buffer[10]  : Projection device handle
	  // cpu8Buffer[11] : BTChange info type
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(11 <= u8ValidBytes)
      {
            oInvBTAction.BluetoothDeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
            oInvBTAction.ProjectionDeviceHandle =CONVERT_32((cpu8Buffer[7]),(cpu8Buffer[8]),(cpu8Buffer[9]),(cpu8Buffer[10]));
		    oInvBTAction.BTChangeInfo.enType = static_cast <midw_fi_tcl_e8_BTChangeInfo::tenType>(cpu8Buffer[11]);	
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bInvokeBluetoothDeviceAction exited"));
   return (bSendMsg(oInvBTAction));
}
/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetEnvironmentData(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bSetEnvironmentData(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetEnvironmentData entered"));
   midw_smartphoneintfi_tclMsgSetEnvironmentDataMethodStart oSetEnvData;
   if(NULL != cpu8Buffer)
   {  
   //bSetEnvironmentData command should be recieved with 24 bytes of data and 20 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[10] - outside temperature
      // cpu8Buffer[11]   : valid temperature
	  //cpu8Buffer[12] to cpu8Buffer[19]- barometric pressure
	  //cpu8Buffer[20]    : valid pressure 
	  
	  
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(20 <= u8ValidBytes)
      {
	        tU32 LeastSignificant = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
	        tU32 MostSignificant = CONVERT_32((cpu8Buffer[7]),(cpu8Buffer[8]),(cpu8Buffer[9]),(cpu8Buffer[10]));
	        oSetEnvData.OutsideTemperature = Convert_64(MostSignificant,LeastSignificant);
			ETG_TRACE_USR1(("spi_tclImpTraceStreamable::outside temperature is - %f ",oSetEnvData.OutsideTemperature));
	        oSetEnvData.ValidTemperature = (tBool)(cpu8Buffer[11]);
			LeastSignificant = CONVERT_32((cpu8Buffer[12]),(cpu8Buffer[13]),(cpu8Buffer[14]),(cpu8Buffer[15]));
	        MostSignificant = CONVERT_32((cpu8Buffer[16]),(cpu8Buffer[17]),(cpu8Buffer[18]),(cpu8Buffer[19]));
	        oSetEnvData.BarometricPressure = Convert_64(MostSignificant,LeastSignificant);
			oSetEnvData.ValidPressure = (tBool)(cpu8Buffer[20]);
		
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetEnvironmentData exited"));
   return (bSendMsg(oSetEnvData));
}
/**************************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bRotaryControllerEven(tU8 const* const cp..
***************************************************************************************/
tBool spi_tclImpTraceStreamable::bRotaryControllerEvent(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bRotaryControllerEvent entered"));
   midw_smartphoneintfi_tclMsgRotaryControllerEventMethodStart oRotContrlEvent;
   if(NULL != cpu8Buffer)
   {  
   //RotaryControllerEventMethodStart command should be recieved with 11 bytes of data and 7 Valid bytes
      //remaining 4 Bytes are 0f ff 02 b2 - to identify spi components trace channel
      // cpu8Buffer[1] & cpu8Buffer[2] to identify which trace command it is.
      // cpu8Buffer[3] to cpu8Buffer[6]  : Park brake info
      // cpu8Buffer[7] -  : gear state info
	
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(7 <= u8ValidBytes)
      {
	
            oRotContrlEvent.DeviceHandle = CONVERT_32((cpu8Buffer[3]),(cpu8Buffer[4]),(cpu8Buffer[5]),(cpu8Buffer[6]));
            oRotContrlEvent.ControllerDeltaCounts = static_cast <tS8> (cpu8Buffer[7]);
      }
   }
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bRotaryControllerEvent exited"));
   return (bSendMsg(oRotContrlEvent));
} 

/***************************************************************************
** FUNCTION:  tBool spi_tclImpTraceStreamable::bSetDeviceSelectionMode(tU8 const* const cp..
***************************************************************************/
tBool spi_tclImpTraceStreamable::bSetDeviceSelectionMode(tU8 const* const cpu8Buffer) const
{
   ETG_TRACE_USR1(("spi_tclImpTraceStreamable::bSetDeviceSelectionMode entered"));
   midw_smartphoneintfi_tclMsgSetDeviceSelectionModeMethodStart oDevSelModeMS;
   if(NULL != cpu8Buffer)
   {
      tU8 u8ValidBytes = cpu8Buffer[0];
      if(3<= u8ValidBytes)
      {
         oDevSelModeMS.DeviceSelectionMode.enType = static_cast<midw_fi_tcl_e8_DeviceSelectionMode::tenType>(cpu8Buffer[3]);
      }
   }
   return (bSendMsg(oDevSelModeMS));
}


////////////////////////////////////////////////////////////////////////////////
// <EOF>

